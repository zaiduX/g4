<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'front_site/index';
$route['404_override'] = 'Custom404';
$route['translate_uri_dashes'] = TRUE;
//Login Logout
$route['admin'] = "admin_login/index";
$route['admin/login'] = 'admin_login/validate_login';
//$route['admin/admin/login'] = 'admin_login/validate_login';
$route['admin/logout'] = 'admin_login/logout';
//Dashboard
$route['admin/dashboard'] = 'administration/admin_dashboard';
$route['admin/user-wise-dashboard'] = 'administration/user_wise_dashboard';
$route['admin/admin-dashboard'] = 'administration/dashboard';
//Admin Dashboard Ajax
$route['admin/get-users-count-by-country-id'] = 'administration/get_users_count_by_country_id';
$route['admin/get-job-count-by-filters'] = 'administration/get_job_count_by_filters';
$route['admin/get-other-account-details-by-filters'] = 'administration/get_other_account_details_by_filters';
//Admin Dashboard More - Jobs
$route['admin/total-jobs'] = 'administration/total_jobs';
$route['admin/total-open-jobs'] = 'administration/total_open_jobs';
$route['admin/total-accepted-jobs'] = 'administration/total_accepted_jobs';
$route['admin/total-inprogress-jobs'] = 'administration/total_inprogress_jobs';
$route['admin/total-delivered-jobs'] = 'administration/total_delivered_jobs';
$route['admin/total-cancelled-jobs'] = 'administration/total_cancelled_jobs';
$route['admin/total-expired-jobs'] = 'administration/total_expired_jobs';
//Admin Dashboard More - Users
$route['admin/total-users'] = 'administration/total_users';
$route['admin/business-users'] = 'administration/business_users';
$route['admin/individual-users'] = 'administration/individual_users';
$route['admin/buyer-users'] = 'administration/buyer_users';
$route['admin/seller-users'] = 'administration/seller_users';
$route['admin/both-users'] = 'administration/both_users';
$route['admin/online-users'] = 'administration/online_users';
$route['admin/offline-users'] = 'administration/offline_users';
$route['admin/active-users'] = 'administration/active_users';
$route['admin/inactive-users'] = 'administration/inactive_users';
//Admin Dashboard Recent Orders
$route['admin/recently-open-jobs'] = 'administration/recently_open_jobs';
$route['admin/inprogress-open-jobs'] = 'administration/inprogress_open_jobs';
$route['admin/delivered-open-jobs'] = 'administration/delivered_open_jobs';
$route['admin/cancelled-open-jobs'] = 'administration/cancelled_open_jobs';
$route['admin/get-user-by-currency'] = 'administration/get_user_by_currency';
$route['admin/get-country-phone-code-by-id'] = 'administration/get_country_phone_code_by_id';
//Dashboard More
$route['admin/dashboard-expired-booking-providers'] = 'administration/dashboard_expired_booking_providers';
$route['admin/dashboard-rejected-booking-providers'] = 'administration/dashboard_rejected_booking_providers';
$route['admin/dashboard-accepted-booking-providers'] = 'administration/dashboard_accepted_booking_providers';
$route['admin/dashboard-delivered-booking-providers'] = 'administration/dashboard_delivered_booking_providers';
$route['admin/dashboard-created-booking-customer'] = 'administration/dashboard_created_booking_customer';
$route['admin/dashboard-expired-booking-customer'] = 'administration/dashboard_expired_booking_customer';
$route['admin/dashboard-rejected-booking-customer'] = 'administration/dashboard_rejected_booking_customer';
$route['admin/dashboard-accepted-booking-customer'] = 'administration/dashboard_accepted_booking_customer';
$route['admin/dashboard-cancelled-booking-customer'] = 'administration/dashboard_cancelled_booking_customer';
$route['admin/dashboard-delivered-booking-customer'] = 'administration/dashboard_delivered_booking_customer';
$route['admin/dashboard-recently-connected-users'] = 'administration/dashboard_recently_connected_users';
$route['admin/dashboard-never-connected-users'] = 'administration/dashboard_never_connected_users';
$route['admin/dashboard-frequently-connected-users'] = 'administration/dashboard_frequently_connected_users'; 
$route['admin/dashboard-highest-money-spent-users'] = 'administration/dashboard_highest_money_spent_users'; 
$route['admin/dashboard-lowest-money-spent-users'] = 'administration/dashboard_lowest_money_spent_users';
$route['admin/dashboard-best-rating-users'] = 'administration/dashboard_best_rating_users'; 
$route['admin/dashboard-lowest-rating-users'] = 'administration/dashboard_lowest_rating_users'; 
$route['admin/gonagoo-account-history'] = 'administration/gonagoo_account_history';
$route['admin/currencywise-account-history/(:any)'] = 'administration/currencywise_account_history/$1';
$route['admin/currencywise-gonagoo-comission-history/(:any)/(:any)'] = 'administration/currencywise_gonagoo_comission_history/$1/$1';
$route['admin/currencywise-insurance-history/(:any)'] = 'administration/currencywise_insurance_history/$1';
$route['admin/currencywise-custom-history/(:any)'] = 'administration/currencywise_custom_history/$1';
$route['admin/currencywise-refund-history/(:any)'] = 'administration/currencywise_refund_history/$1';
$route['admin/profile'] = 'administration/profile'; //view
$route['admin/update-profile'] = 'administration/update_profile'; 
$route['admin/update-profile-password'] = 'administration/update_profile_password';
//Category Type
$route['admin/category-type'] = 'categoryType/category_type'; //view
$route['admin/edit-category-type/(:any)'] = 'categoryType/edit_category_type/$1'; //view
$route['admin/update-category-type'] = 'categoryType/update_category_type';
$route['admin/delete-category-type/(:any)'] = 'categorytype/delete_category_type/$1';
$route['admin/active-category-type'] = 'categoryType/active_category_type';
$route['admin/inactive-category-type'] = 'categoryType/inactive_category_type';
$route['admin/add-category-type'] = 'categoryType/add_category_type'; //view
$route['admin/register-category-type'] = 'categoryType/register_category_type';
//Categories
$route['admin/categories'] = 'categories/index'; //view
$route['admin/edit-category'] = 'categories/edit_category'; //view
$route['admin/update-category'] = 'categories/update_category';
$route['admin/delete-category/(:any)'] = 'categories/delete_category/$1';
$route['admin/active-category'] = 'categories/active_category';
$route['admin/inactive-category'] = 'categories/inactive_category';
$route['admin/add-category'] = 'categories/add_category'; //view
$route['admin/register-category'] = 'categories/register_category';
$route['admin/get-categories-by-type'] = 'categories/get_categories_by_type';
//Currency Master
$route['admin/currency-master'] = 'Currency/currency_master'; //view
$route['admin/add-currency-master'] = 'Currency/add_currency_master'; //view
$route['admin/edit-currency-master/(:any)'] = 'Currency/edit_currency_master/$1';
$route['admin/update-currency-master'] = 'Currency/update_currency_master';
$route['admin/register-currency-master'] = 'Currency/register_currency_master';
$route['admin/currency-master/activate'] = 'Currency/active';
$route['admin/currency-master/inactivate'] = 'Currency/inactive';
//Country Currency Master
$route['admin/country-currency'] = 'country_currency/country_currency'; //view
$route['admin/add-country-currency'] = 'country_currency/add_country_currency'; //view
$route['admin/edit-country-currency/(:any)'] = 'country_currency/edit_country_currency/$1';
$route['admin/update-country-currency'] = 'country_currency/update_country_currency';
$route['admin/register-country-currency'] = 'country_currency/register_country_currency';
$route['admin/active-country-currency'] = 'country_currency/active_country_currency';
$route['admin/inactive-country-currency'] = 'country_currency/inactive_country_currency';
//Authority Type
$route['admin/authority-type'] = 'authority_type/authority_type'; //view
$route['admin/edit-authority-type/(:any)'] = 'authority_type/edit_authority_type/$1'; //view
$route['admin/update-authority-type'] = 'authority_type/update_authority_type';
$route['admin/delete-authority-type/(:any)'] = 'authority_type/delete_authority_type/$1';
$route['admin/active-authority-type'] = 'authority_type/active_authority_type';
$route['admin/inactive-authority-type'] = 'authority_type/inactive_authority_type';
$route['admin/add-authority-type'] = 'authority_type/add_authority_type'; //view
$route['admin/register-authority-type'] = 'authority_type/register_authority_type';
//Authority
$route['admin/authority'] = 'Authority/authority_master'; //view
$route['admin/add-authority'] = 'Authority/add_authority'; //view
$route['admin/edit-authority/(:any)'] = 'Authority/edit_authority/$1'; //view
$route['admin/update-authority'] = 'Authority/update_authority';
$route['admin/delete-authority/(:any)'] = 'Authority/delete_authority/$1';
$route['admin/active-authority'] = 'Authority/active_authority';
$route['admin/inactive-authority'] = 'Authority/inactive_authority';
$route['admin/register-authority'] = 'Authority/register_authority';
$route['admin/update-authority-password'] = 'Authority/update_authority_password';
$route['admin/assign-permission/(:any)'] = 'Authority/assign_permission/$1'; //view
$route['admin/update-authority-permissions'] = 'Authority/update_authority_permissions';
//Group Permissions
$route['admin/group-permission'] = 'group_permissions/group_perm'; //view
$route['admin/add-group-permission'] = 'group_permissions/add_group_permission'; //view
$route['admin/register-group-permission'] = 'group_permissions/register_group_permission'; //view
$route['admin/edit-group-permission/(:any)'] = 'group_permissions/edit_group_permission/$1'; //view
$route['admin/active-group-permission'] = 'group_permissions/active_group_permission';
$route['admin/inactive-group-permission'] = 'group_permissions/inactive_group_permission';
$route['admin/update-group-permission'] = 'group_permissions/update_group_permission';
//Users
$route['admin/users'] = 'Users/users_master'; //view
$route['admin/view-users/(:any)'] = 'Users/view_users/$1'; //view
$route['admin/users/individuals/active-user'] = 'Users/active_user';
$route['admin/users/inactive-user'] = 'Users/inactive_user';
$route['admin/users/inactive'] = 'Users/inact_users';
$route['admin/users/inactivate'] = 'Users/inactive_users';
$route['admin/users/activate'] = 'Users/active_users';
$route['admin/users/individuals'] = 'Users/individual_users'; //view
$route['admin/users/individuals/inactive'] = 'Users/inactive_individual_users'; //view
$route['admin/users/business/active-user'] = 'Users/active_user';
$route['admin/users/business'] = 'Users/business_users'; //view
$route['admin/users/business/inactive'] = 'Users/inactive_business_users'; //view
$route['admin/send-personal-notification'] = 'Notification/send_personal_notification'; //view
$route['admin/users/update-shipping-mode'] = 'Users/update_shipping_mode';
$route['admin/user-wallet-history'] = 'Users/user_wallet_history';
$route['admin/user-scrow-history'] = 'Users/user_scrow_history';
//Mark Dedicated
$route['admin/users/activate-dedicated'] = 'Users/active_users_dedicated';
$route['admin/users/inactivate-users-dedicated'] = 'Users/inactivate_users_dedicated';
//Mark Bank/Cheque Payment
$route['admin/users/activate-payment-type'] = 'Users/activate_payment_type';
$route['admin/users/inactivate-payment-type'] = 'Users/inactivate_payment_type';
//Set Commission type for dedicated users
$route['admin/users/set-gonagoo-commission'] = 'Users/set_gonagoo_commission';
//Set Gonagoo Commission for dedicated users
$route['admin/users/define-user-commission'] = 'Users/define_user_commission';
$route['admin/users/define-user-commission/(:any)'] = 'Users/define_user_commission/$i';
//Percentage Based
$route['admin/users/add-define-commission-percentage'] = 'Users/add_define_commission_percentage';
$route['admin/users/add-define-commission-percentage/(:any)'] = 'Users/add_define_commission_percentage/$i';
$route['admin/users/register-commission-percentage'] = 'Users/register_commission_percentage';
$route['admin/users/edit-commission-rate/(:any)'] = 'Users/edit_commission_rate/$i';
$route['admin/users/update-commission-percentage'] = 'Users/update_commission_percentage';
//Absolute Rate Based
$route['admin/users/add-define-commission-rate'] = 'Users/add_define_commission_rate';
$route['admin/users/add-define-commission-rate/(:any)'] = 'Users/add_define_commission_rate/$i';
$route['admin/users/register-commission-rate'] = 'Users/register_commission_rate';
$route['admin/users/delete-user-commission'] = 'Users/delete_user_commission';
//Dedicated users pending orders
$route['admin/users/pending-orders/(:any)'] = 'Users/pending_orders/$1'; //view
$route['admin/users/view-pending-order-details'] = 'Users/view_pending_order_details'; //view
$route['admin/users/confirm-pending-order-details'] = 'Users/confirm_pending_order_details'; //view
$route['admin/users/pending-orders-filter'] = 'Users/pending_orders_filter'; //view
//Notifications
$route['admin/notification'] = 'notification/notification_list';
$route['admin/notification/(:any)'] = 'notification/notification_detail';
$route['admin/send-notification'] = 'notification/send_notification';
$route['admin/get-states'] = 'notification/get_states';
$route['admin/get-cities'] = 'notification/get_cities';
$route['admin/notification-send'] = 'notification/notification_send';
// standard dimension
$route['admin/standard-dimensions'] = 'standard_dimension/index';
$route['admin/standard-dimensions/add'] = 'standard_dimension/add';
$route['admin/standard-dimensions/register'] = 'standard_dimension/register';
$route['admin/standard-dimensions/edit/(:any)'] = 'standard_dimension/edit/$1';
$route['admin/standard-dimensions/update'] = 'standard_dimension/update';
$route['admin/standard-dimensions/delete'] = 'standard_dimension/delete';
// Transport Vehicles
$route['admin/transport-vehicles'] = 'transport_vehicles/index';
$route['admin/transport-vehicles/add'] = 'transport_vehicles/add';
$route['admin/transport-vehicles/register'] = 'transport_vehicles/register';
$route['admin/transport-vehicles/edit/(:any)'] = 'transport_vehicles/edit/$1';
$route['admin/transport-vehicles/update'] = 'transport_vehicles/update';
$route['admin/transport-vehicles/delete'] = 'transport_vehicles/delete';
$route['admin/transport-vehicles/define-seat-type/(:any)'] = 'transport_vehicles/define_seat_type/$1';
$route['admin/transport-vehicles/register-seat-type'] = 'transport_vehicles/register_seat_type';
$route['admin/transport-vehicles/delete-seat-type'] = 'transport_vehicles/delete_seat_type';
// point-to-point-rates
$route['admin/p-to-p-rates'] = 'point_to_point_rates/index';
$route['admin/p-to-p-rates/add/volume-based'] = 'point_to_point_rates/add_volume_based';
$route['admin/p-to-p-rates/add/formula-volume-based'] = 'point_to_point_rates/add_formula_volume_based';
$route['admin/p-to-p-rates/add/weight-based'] = 'point_to_point_rates/add_weight_based';
$route['admin/p-to-p-rates/add/formula-weight-based'] = 'point_to_point_rates/add_formula_weight_based';
$route['admin/p-to-p-rates/add/get-country-currencies'] = 'standard_rates/get_country_currencies';
$route['admin/p-to-p-rates/add/get-states'] = 'point_to_point_rates/get_states';
$route['admin/p-to-p-rates/add/get-cities'] = 'point_to_point_rates/get_cities';
$route['admin/p-to-p-rates/register/volume-based'] = 'point_to_point_rates/register_volume_based';
$route['admin/p-to-p-rates/register/formula-volume-based'] = 'point_to_point_rates/register_formula_volume_based';
$route['admin/p-to-p-rates/register/weight-based'] = 'point_to_point_rates/register_weight_based';
$route['admin/p-to-p-rates/register/formula-weight-based'] = 'point_to_point_rates/register_formula_weight_based';
$route['admin/p-to-p-rates/edit/(:num)'] = 'point_to_point_rates/edit/$1';
$route['admin/p-to-p-rates/update'] = 'point_to_point_rates/update';
$route['admin/p-to-p-rates/delete'] = 'point_to_point_rates/delete';
$route['admin/p-to-p-rates/get-unit-detail'] = 'point_to_point_rates/get_unit_detail';
// standard-rates
$route['admin/standard-rates'] = 'standard_rates/index';
$route['admin/standard-rates/add/volume-based'] = 'standard_rates/add_volume_based';
$route['admin/standard-rates/add/weight-based'] = 'standard_rates/add_weight_based';
$route['admin/standard-rates/add/formula-volume-based'] = 'standard_rates/add_formula_volume_based';
$route['admin/standard-rates/add/formula-weight-based'] = 'standard_rates/add_formula_weight_based';
$route['admin/standard-rates/add/get-country-currencies'] = 'standard_rates/get_country_currencies';
$route['admin/standard-rates/register/volume-based'] = 'standard_rates/register_volume_based';
$route['admin/standard-rates/register/formula-volume-based'] = 'standard_rates/register_formula_volume_based';
$route['admin/standard-rates/register/weight-based'] = 'standard_rates/register_weight_based';
$route['admin/standard-rates/register/formula-weight-based'] = 'standard_rates/register_formula_weight_based';
$route['admin/standard-rates/edit/(:num)'] = 'standard_rates/edit/$1';
$route['admin/standard-rates/update'] = 'standard_rates/update';
$route['admin/standard-rates/delete'] = 'standard_rates/delete';
$route['admin/standard-rates/get-unit-detail'] = 'standard_rates/get_unit_detail';
// driver categories
$route['admin/driver-categories'] = 'driver_categories/index';
$route['admin/driver-categories/add'] = 'driver_categories/add';
$route['admin/driver-categories/register'] = 'driver_categories/register';
$route['admin/driver-categories/edit/(:any)'] = 'driver_categories/edit/$1';
$route['admin/driver-categories/update'] = 'driver_categories/update';
$route['admin/driver-categories/activate'] = 'driver_categories/active_category';
$route['admin/driver-categories/inactivate'] = 'driver_categories/inactive_category';
// advance payment
$route['admin/advance-payment'] = 'advance_payment/index';
$route['admin/advance-payment/add'] = 'advance_payment/add';
$route['admin/advance-payment/register'] = 'advance_payment/register';
$route['admin/advance-payment/edit/(:num)'] = 'advance_payment/edit/$1';
$route['admin/advance-payment/update'] = 'advance_payment/update';
$route['admin/advance-payment/delete'] = 'advance_payment/delete';
// Country Custom Setup
$route['admin/custom-percentage'] = 'country_custom/index';
$route['admin/custom-percentage/add'] = 'country_custom/add';
$route['admin/custom-percentage/register'] = 'country_custom/register';
$route['admin/custom-percentage/edit/(:num)'] = 'country_custom/edit/$1';
$route['admin/custom-percentage/update'] = 'country_custom/update';
$route['admin/custom-percentage/delete'] = 'country_custom/delete';
// Skill Master
$route['admin/skills-master'] = 'Skills/index'; 
$route['admin/add-skill'] = 'Skills/add_skill'; //view
$route['admin/register-skill'] = 'Skills/register_skill';
$route['admin/edit-skill'] = 'Skills/edit_skill'; //view
$route['admin/edit-skill/(:any)'] = 'Skills/edit_skill/$1'; //view
$route['admin/update-skill'] = 'Skills/update_skill';
$route['admin/skills-master/delete'] = 'Skills/delete';
// Insurance Master
$route['admin/insurance-master'] = 'Insurance/index';
$route['admin/add-insurance'] = 'Insurance/add_insurance';
$route['admin/register-insurance'] = 'Insurance/register_insurance';
$route['admin/edit-insurance'] = 'Insurance/edit_insurance'; //view
$route['admin/edit-insurance/(:any)'] = 'Insurance/edit_insurance/$1'; //view
$route['admin/update-insurance'] = 'Insurance/update_insurance';
$route['admin/insurance-master/delete'] = 'Insurance/delete';
// Relay Point
$route['admin/relay-point'] = 'Relay/relay_point';
$route['admin/active-relay-point'] = 'Relay/active_relay_point';
$route['admin/inactive-relay-point'] = 'Relay/inactive_relay_point';
$route['admin/add-relay-point'] = 'Relay/add_relay_point';
$route['admin/relay-email-exists'] = 'Relay/relay_email_exists';
$route['admin/register-relay-point'] = 'Relay/register_relay_point';
$route['admin/edit-relay-point/(:any)'] = 'Relay/edit_relay_point/$1';
$route['admin/edit-relay-point-details/(:any)'] = 'Relay/edit_relay_point_details/$1';
$route['admin/update-relay-point-details'] = 'Relay/update_relay_point_details';
$route['admin/update-relay-point'] = 'Relay/update_relay_point';
$route['admin/get-state-by-country-id'] = "Relay/get_state_by_country_id";
$route['admin/get-cities-by-state-id'] = "Relay/get_city_by_state_id";
// Withdraw request
$route['admin/withdraw-request'] = 'Withdraw/withdraw_request';
$route['admin/reject-withdraw-request'] = 'Withdraw/reject_withdraw_request';
$route['admin/accept-withdraw-request'] = 'Withdraw/accept_withdraw_request';
$route['admin/update-withdraw-transfer-amount'] = 'Withdraw/update_withdraw_transfer_amount';
//User Document Verification
$route['admin/verify-documents'] = 'Verify_docs/verify_documents'; //view
$route['admin/rejected-docs'] = 'Verify_docs/rejected_documents'; //view
$route['admin/view-users-details/(:any)'] = 'Verify_docs/view_users_details/$1'; //view
$route['admin/verify-it'] = 'Verify_docs/verify_it';
$route['admin/reject-it'] = 'Verify_docs/reject_it';
//Order Details
$route['admin/get-all-orders'] = 'Admin_orders/get_all_orders';
$route['admin/view-order-details/(:any)'] = 'Admin_orders/view_order_details/$1';
$route['admin/remove-order/(:num)'] = 'Admin_orders/remove_open_order_view/$1';
$route['admin/make-order-removed'] = 'Admin_orders/make_open_order_remove';
//Customer support Details
$route['admin/customer-support-list'] = 'Customer_support/get_customer_support_list';
$route['admin/reply-support-request'] = 'Customer_support/reply_support_request';
$route['admin/reply-support-request/(:any)'] = 'Customer_support/reply_support_request/$1';
$route['admin/update-support-request'] = 'Customer_support/update_support_request';
//Deliverer Carrier Type
$route['admin/carrier-type'] = 'Carrier_type/carrier_type';
$route['admin/add-carrier-type'] = 'Carrier_type/add_carrier_type';
$route['admin/add-carrier-type-details'] = 'Carrier_type/add_carrier_type_details';
$route['admin/edit-carrier-type'] = 'Carrier_type/edit_carrier_type';
$route['admin/edit-carrier-type/(:any)'] = 'Carrier_type/edit_carrier_type/$1';
$route['admin/update-carrier-type-details'] = 'Carrier_type/update_carrier_type_details';
$route['admin/delete-carrier-type'] = 'Carrier_type/delete_carrier_type';
$route['admin/testing'] = 'notification/testing';
// Dangerous Goods
$route['admin/dangerous-goods'] = 'dangerous_goods/index';
$route['admin/dangerous-goods/add'] = 'dangerous_goods/add';
$route['admin/dangerous-goods/edit/(:num)'] = 'dangerous_goods/edit/$1';
$route['admin/dangerous-goods/update'] = 'dangerous_goods/update';
$route['admin/dangerous-goods/inactivate'] = 'dangerous_goods/make_inactive';
$route['admin/dangerous-goods/activate'] = 'dangerous_goods/make_active';
$route['admin/dangerous-goods/register'] = 'dangerous_goods/register';
// Country Dangerous Goods
$route['admin/country-dangerous-goods'] = 'country_dangerous_goods/index';
$route['admin/country-dangerous-goods/add'] = 'country_dangerous_goods/add';
$route['admin/country-dangerous-goods/register'] = 'country_dangerous_goods/register';
$route['admin/country-dangerous-goods/edit/(:num)'] = 'country_dangerous_goods/edit/$id';
$route['admin/country-dangerous-goods/update'] = 'country_dangerous_goods/update';
//Users Followups Super Admin
$route['admin/user-followups-admin'] = 'Users_followups/user_followups_admin'; //view
$route['admin/followup-assigned-users'] = 'Users_followups/followup_assigned_users'; //View
$route['admin/assign-authority-to-user'] = 'Users_followups/assign_authority_to_user'; //Edit
//Users Followups Authority
$route['admin/user-followups-sales'] = 'Users_followups_sales/user_followups_sales_list'; //view
$route['admin/mark-user-followup'] = 'Users_followups_sales/mark_user_followup'; //Edit
$route['admin/followup-taken-users'] = 'Users_followups_sales/followup_taken_users'; //view
// Gonagoo Address Master
$route['admin/gonagoo-address-admin'] = 'gonagoo_address/index';
$route['admin/add-address'] = 'gonagoo_address/add_address';
$route['admin/register-address'] = 'gonagoo_address/register_address';
$route['admin/edit-address'] = 'gonagoo_address/edit_address'; //view
$route['admin/edit-address/(:any)'] = 'gonagoo_address/edit_address/$1'; //view
$route['admin/update-address'] = 'gonagoo_address/update_address';
$route['admin/address-master/delete'] = 'gonagoo_address/delete';
//App Version
$route['admin/change-app-version'] = 'administration/change_app_version';
$route['admin/update-main-app-version'] = 'administration/update_main_app_version';
$route['admin/update-driver-app-version'] = 'administration/update_driver_app_version';
$route['admin/update-ios-main-app-version'] = 'administration/update_ios_main_app_version';
//start admin panel promocode-------------------------------------------------------------------
	$route['admin/promo-code'] = 'administration/promocode_master'; //view
	$route['admin/edit-promo/(:any)'] = 'administration/edit_promocode/$1'; //view
	$route['admin/active-promo_code'] = 'administration/activate_promo_code';
	$route['admin/inactive-promo_code'] = 'administration/inactivate_promo_code';
	$route['admin/delete-promo/(:any)'] = 'administration/delete_promo_code/$1'; //view
	$route['admin/add-promo'] = 'administration/add_promocode'; //view
	$route['admin/register-promocode'] = 'administration/register_promocode';
	$route['admin/update-promocode'] = 'administration/update_promocode';
	$route['admin/check-promo-code-exists'] = 'administration/check_promo_code_exists';
//end admin panel promocode---------------------------------------------------------------------

// Ticket Commission Refund Request
$route['admin/commission-refund-request'] = 'Ticket_commission_refund/commission_refund_request';
$route['admin/reject-refund-request'] = 'Ticket_commission_refund/reject_refund_request';
$route['admin/accept-refund-request'] = 'Ticket_commission_refund/accept_refund_request';

// Mobile APIs---------------------------------------------------------------------------------------
$route['api/app-version'] = 'api/app_params';
$route['api/registration'] = 'api/registration';
$route['api/login'] = 'api/login';
$route['api/update-avatar'] = 'api/update_avatar';
$route['api/update-cover'] = 'api/update_cover';
$route['api/update-overview'] = 'api/update_overview';
$route['api/forgot-password'] = 'api/forget_password';
$route['api/basic-info-update'] = 'api/basic_info_update';
$route['api/get-portfolios'] = 'api/get_portfolio_details';
$route['api/add-new-portfolio'] = 'api/register_portfolio';
$route['api/update-portfolio'] = 'api/update_portfolio';
$route['api/delete-portfolio'] = 'api/delete_portfolio';
$route['api/update-business-avatar'] = 'api/update_business_avatar';
$route['api/update-business-cover'] = 'api/update_business_cover';
//User Skills
$route['api/skill-list'] = 'api/skill_list';
$route['api/skill-update'] = 'api/skill_update';
$route['api/skill-list-master'] = 'api/skill_list_master';
//Service Category type
$route['api/category-type-master'] = 'api/category_type_master';
//Service Category
$route['api/category-list-master'] = 'api/category_list_master';
//User Language
$route['api/language-list-master'] = 'api/language_list_master';
$route['api/language-update'] = 'api/language_update';
//User Education
$route['api/education-list'] = 'api/education_list';
$route['api/education-add'] = 'api/education_add';
$route['api/education-update'] = 'api/education_update';
$route['api/education-delete'] = 'api/education_delete';
//User Education
$route['api/experience-list'] = 'api/experience_list';
$route['api/experience-add'] = 'api/experience_add';
$route['api/experience-update'] = 'api/experience_update';
$route['api/experience-delete'] = 'api/experience_delete';
//Drivers
$route['api/permitted-cat-list'] = 'api/permitted_cat_list';
$route['api/driver-list'] = 'api/driver_list';
$route['api/inactive-driver-list'] = 'api/inactive_driver_list';
$route['api/driver-add'] = 'api/driver_add';
$route['admin/active-driver'] = 'api/active_driver';
$route['admin/inactive-driver'] = 'api/inactive_driver';
$route['api/driver-update'] = 'api/driver_update';
$route['api/driver-update_avatar'] = 'api/driver_update_avatar';
$route['api/service-area-list'] = 'api/service_area_list';
$route['api/service-area-add'] = 'api/service_area_add';
$route['api/service-area-delete'] = 'api/service_area_delete';
$route['api/add-deliverer'] = 'api/register_deliverer';
$route['api/get-carrier-type'] = 'api/get_carrier_type';
$route['api/update-deliverer'] = 'api/update_deliverer';
$route['api/update-bank-info'] = 'api/update_bank_info';
$route['api/upload-business-photo'] = 'api/upload_business_photo';
$route['api/delete-business-photo'] = 'api/delete_business_photo';
$route['api/get-business-photos'] = 'api/get_business_photos';
$route['api/add-transport-details'] = 'api/add_transport_details';
$route['api/update-transport-details'] = 'api/update_transport_details';
$route['api/delete-transport-details'] = 'api/delete_transport_details';
$route['api/get-transport-list'] = 'api/consumer_transport_list';
$route['api/vehical-master-list'] = 'api/transport_vehical_master';
$route['api/get-profile'] = 'api/get_profile_details';
$route['api/get-addressbook'] = 'api/get_customers_addressbook';
$route['api/add-address'] = 'api/add_address_in_addressbook';
$route['api/update-address'] = 'api/update_address_in_addressbook';
$route['api/add-photo-to-address'] = 'api/upload_address_photo';
$route['api/delete-address'] = 'api/delete_address_from_addressbook';
$route['api/unit-list'] = 'api/unit_master';
$route['api/get-standard-dimensions'] = 'api/get_standard_dimensions';
$route['api/calculate-order-price'] = 'api/calculate_order_price';
$route['api/get-advance-payment'] = 'api/get_advance_payment';
$route['api/book-courier-order'] = 'api/register_new_courier_orders';
$route['api/make-deliverer-favourite'] = 'api/register_new_favourite_deliverer';
$route['api/remove-favourite-deliverer'] = 'api/remove_favourite_deliverer';
$route['api/favourite-deliverer-list'] = 'api/get_favourite_deliverer_list';
//Document Details
$route['api/get-documents'] = 'api/get_documents';
$route['api/add-document'] = 'api/add_document';
$route['api/edit-document'] = 'api/update_document';
$route['api/remove-document'] = 'api/delete_document';
//Payment Details
$route['api/get-paymnet-details'] = 'api/get_paymnet_details';
$route['api/add-paymnet-detail'] = 'api/add_paymnet_detail';
$route['api/edit-paymnet-detail'] = 'api/update_paymnet_detail';
$route['api/remove-paymnet-detail'] = 'api/delete_paymnet_detail';
$route['api/get-reviews'] = 'api/get_reviews';
$route['api/get-insurance-fees'] = 'api/get_insurance_fees';
//Deliverer Document Details
$route['api/get-deliverer-documents'] = 'api/get_deliverer_documents';
$route['api/add-deliverer-document'] = 'api/add_deliverer_document';
$route['api/edit-deliverer-document'] = 'api/update_deliverer_document';
$route['api/remove-deliverer-document'] = 'api/delete_deliverer_document';
//Relay Point List
$route['api/get-relay-point-list'] = 'api/get_relay_point_list';
//Order Payments
$route['api/update-order-payment-by-customer'] = 'api/update_order_payment_by_customer';
$route['api/user-account-withdraw-request'] = 'api/user_account_withdraw_request';
$route['api/get-customer-order-using-code'] = 'api/get_customer_order_using_code';
$route['api/wecashup-payment-gateway'] = 'api/wecashup_payment_gateway';
$route['api/orange-payment-gateway'] = 'api/orange_payment_gateway';
//User Account module
$route['api/get-customer-account-balance'] = 'api/get_customer_account_balance';
$route['api/get-customer-account-history'] = 'api/get_customer_account_history';
$route['api/get-customer-scrow-balance'] = 'api/get_customer_scrow_balance';
$route['api/get-customer-scrow-history'] = 'api/get_customer_scrow_history';
$route['api/get-customer-account-withdraw-request-list'] = 'api/get_customer_account_withdraw_request_list';
$route['api/customer-paid-this-month'] = 'api/customer_paid_this_month';
$route['api/customer-earned-this-month'] = 'api/customer_earned_this_month';
$route['api/customer-paid-to-date'] = 'api/customer_paid_to_date';
$route['api/customer-earned-to-date'] = 'api/customer_earned_to_date';
$route['api/customer-account-details'] = 'api/customer_account_details';
$route['api/get-user-order-invoices'] = 'api/get_user_order_invoices';
$route['api/get-deliverer-comission-invoices'] = 'api/get_deliverer_comission_invoices';
$route['api/customer-paid-this-month'] = 'api/customer_paid_this_month';
$route['api/customer-earned-this-month'] = 'api/customer_earned_this_month';
$route['api/customer-paid-to-date'] = 'api/customer_paid_to_date';
$route['api/customer-earned-to-date'] = 'api/customer_earned_to_date';
$route['api/customer-account-details'] = 'api/customer_account_details';
// OTP 
$route['api/verify-otp'] = 'api/verify_otp';
$route['api/resend-otp'] = 'api/resend_otp';
// Notifications
$route['api/notification-counts'] = 'api/notification_counts';
$route['api/unread-notification-list'] = 'api/unread_notifications';
$route['api/unread-workroom-notification-list'] = 'api/unread_workroom_notifications';
$route['api/unread-chatroom-notification-list'] = 'api/unread_chatroom_notifications';
$route['api/workroom-order-list'] = 'api/workroom_order_list';
// make admin notification read
$route['api/make-notification-read'] = 'api/make_notification_read';
$route['api/make-workroom-notification-read'] = 'api/make_workroom_notification_read';
$route['api/make-chatroom-notification-read'] = 'api/make_chatroom_notification_read';
// update user password
$route['api/update-password'] = 'api/update_password';
$route['api/get-notifications'] = 'api/get_notifications';
// email verification
$route['verify/email/(:any)/(:any)'] = 'verify/email_verification/$1/$2';
$route['verification'] = 'verify/email_verified';
$route['home'] = 'verify/index';
// Dangerous Goods
$route['api/country-wise-dangerous-goods'] = 'api/country_wise_dangerous_goods';
$route['api/register-state-city'] = 'api/register_state_city';
// Driver App--------------------------------------------------------------------------------------
$route['driver_api/app-version'] = 'driver_api/app_params';
$route['driver_api/login'] = 'driver_api/login';
$route['driver_api/forget-password'] = 'driver_api/forget_password';
$route['driver_api/change-password'] = 'driver_api/change_password';
$route['driver_api/get-driver-profile'] = 'driver_api/get_driver_profile';
$route['driver_api/update-driver-profile'] = 'driver_api/update_driver_profile';
$route['driver_api/update-driver-avatar'] = 'driver_api/update_driver_avatar';
$route['driver_api/driver-online-status'] = 'driver_api/driver_online_status';
$route['driver_api/driver-location-update'] = 'driver_api/driver_location_update';
$route['driver_api/driver-order-list'] = 'driver_api/driver_order_list';
$route['driver_api/driver-new-order-count'] = 'driver_api/driver_new_order_count';
$route['driver_api/driver-order-status-update'] = 'driver_api/driver_order_status_update';
$route['driver_api/get-delivery-code'] = 'driver_api/get_delivery_code';
$route['driver_api/driver-delivery-code-confirm'] = 'driver_api/driver_delivery_code_confirm';
$route['driver_api/driver-chat-history'] = 'driver_api/driver_chat_history';
$route['driver_api/driver-chat-post'] = 'driver_api/driver_chat_post';
$route['driver_api/get-customer-details'] = 'driver_api/get_customer_details';
$route['driver_api/register-new-support-request'] = 'driver_api/register_new_support_request';
$route['driver_api/driver-confirm-payment'] = 'driver_api/driver_confirm_payment';
$route['driver_api/driver-order-pickup'] = 'driver_api/driver_order_pickup';
//-------------------------------------------------------------------------------------------------
// Relay Point App --------------------------------------------------------------------------------------------
//$route['relay_api/app-version'] = 'relay_api/app_params';
//$route['relay_api/login'] = 'relay_api/login';
$route['relay_api/forget-password'] = 'relay_api/forget_password';
$route['relay_api/change-password'] = 'relay_api/change_password';
$route['relay_api/get-relay-profile'] = 'relay_api/get_driver_profile';
$route['relay_api/update-relay-profile'] = 'relay_api/update_driver_profile';
$route['relay_api/update-relay-avatar'] = 'relay_api/update_driver_avatar';
$route['relay_api/relay-online-status'] = 'relay_api/driver_online_status';
$route['relay_api/relay-location-update'] = 'relay_api/driver_location_update';
$route['relay_api/get-relay-point-details'] = 'relay_api/get_relay_point_details';
$route['relay_api/update-relay-point'] = 'relay_api/update_relay_point';
$route['relay_api/update-relay-point-avatar'] = 'relay_api/update_relay_avatar';
$route['relay_api/update-relay-point-status'] = 'relay_api/update_relay_point_status';
$route['relay_api/get-relay-warehouse'] = 'relay_api/get_relay_warehouse';
$route['relay_api/add-relay-warehouse'] = 'relay_api/add_relay_warehouse';
$route['relay_api/edit-relay-warehouse'] = 'relay_api/edit_relay_warehouse';
$route['relay_api/relay-order-list'] = 'relay_api/relay_order_list';
$route['relay_api/get-customer-order-using-code'] = 'relay_api/get_customer_order_using_code';
$route['relay_api/get-driver-order-using-code'] = 'relay_api/get_driver_order_using_code';
$route['relay_api/manager-order-status-update'] = 'relay_api/manager_order_status_update';
$route['relay_api/assign-warehouse'] = 'relay_api/assign_warehouse';
$route['relay_api/get-relay-warehouse-details'] = 'relay_api/get_relay_warehouse_details';
$route['relay_api/get-relay-warehouse-history'] = 'relay_api/get_relay_warehouse_history';
$route['relay_api/get-relay-account-balance'] = 'relay_api/get_relay_account_balance';
$route['relay_api/get-relay-account-history'] = 'relay_api/get_relay_account_history';
$route['relay_api/get-relay-account-withdraw-request-list'] = 'relay_api/get_relay_account_withdraw_request_list';
$route['relay_api/update-relay-account-details'] = 'relay_api/update_relay_account_details';
$route['relay_api/relay-earned-this-month'] = 'relay_api/relay_earned_this_month';
$route['relay_api/relay-earned-to-date'] = 'relay_api/relay_earned_to_date';
$route['relay_api/relay-account-details'] = 'relay_api/relay_account_details';
$route['relay_api/relay-commission-invoices'] = 'relay_api/relay_commission_invoices';
$route['relay_api/relay-payment-methods'] = 'relay_api/relay_payment_methods';
$route['relay_api/relay-confirm-payment'] = 'relay_api/relay_confirm_payment';
//--------------------------------------------------------------------------------------------------------
$route['api/deliverer-booking-list'] = 'api/deliverer_booking_list';
$route['api/search-deliverer'] = 'api/search_deliverer';
$route['api/booking-delivery-request'] = 'api/booking_delivery_request';
$route['api/deliverer-order-request-list'] = 'api/deliverer_order_request_list';
$route['api/deliverer-order-list'] = 'api/deliverer_order_list';
$route['api/deliverer-order-accept'] = 'api/deliverer_order_accept';
$route['api/deliverer-assign-driver'] = 'api/deliverer_assign_driver';
$route['api/get-delivery-code'] = 'api/get_delivery_code';
$route['api/submit-review'] = 'api/submit_review';
$route['api/get-driver-location'] = 'api/get_driver_location';
$route['api/get-order-status'] = 'api/get_order_status';
$route['api/workroom-chat-post'] = 'api/workroom_chat_post';
$route['api/get-workroom-chat'] = 'api/get_workroom_chat';
$route['api/customer-chat-post'] = 'api/customer_chat_post';
$route['api/customer-chat-history'] = 'api/customer_chat_history';
$route['api/generate-invoice'] = 'api/generate_invoice';
$route['api/courier-order-payment-update'] = 'api/courier_order_payment_update';
$route['api/update-courier-order'] = 'api/update_courier_order';
$route['api/cancel-courier-order'] = 'api/cancel_courier_order';
$route['api/open-booking-list'] = 'api/open_booking_list';
$route['api/support-request-list'] = 'api/get_support_request_list';
$route['api/register-new-support-request'] = 'api/register_new_support_request';
$route['api/get-recent-relay-list'] = 'api/get_recent_relay_list';
$route['api/get-nearby-relay-point-list'] = 'api/get_nearby_relay_point_list';
$route['api/mark-as-cod'] = 'api/mark_as_cod';
$route['api/courier-order-cleanup'] = 'api/courier_order_cleanup';
$route['api/user-pending-invoices-history'] = 'api/user_pending_invoices_history';
$route['api/get-unit-detail'] = 'api/get_unit_detail';
// User panel Web --------------------------------------------------------------------------------------------
$route['user-panel/dashboard'] = "user_panel/dashboard";
$route['user_panel/test-view'] = "user_panel/test_view";
$route['user-panel/user-profile'] = "user_panel/user_profile";
$route['user-panel/deliverer-profile'] = "user_panel/deliverer_profile";
$route['user-panel/create-deliverer-profile'] = "user_panel/create_deliverer_profile";
$route['user-panel/address-book'] = "user_panel/address_book";
$route['user-panel/business-photos'] = "user_panel/business_photos";
$route['user-panel/get-support-list'] = "user_panel/get_support_list";
$route['user-panel/get-support'] = "user_panel/get_support";
$route['user-panel/add-support-details'] = "user_panel/add_support_details";
$route['user-panel/view-support-reply-details/(:num)'] = "user_panel/view_support_reply_details/$1";
$route['user-panel/driver-list'] = "user_panel/driver_list";
$route['user-panel/view-driver'] = "user_panel/view_driver";
$route['user-panel/view-driver/(:any)'] = 'user_panel/view_driver/$1'; //view
$route['user-panel/driver-inactive-list'] = "user_panel/driver_inactive_list";
$route['user-panel/driver-active'] = "user_panel/driver_active";
$route['user-panel/driver-inactive'] = 'user_panel/driver_inactive';
$route['user-panel/edit-driver'] = "user_panel/edit_driver";
$route['user-panel/edit-driver/(:any)'] = 'user_panel/edit_driver/$1'; //view
$route['user-panel/update-driver-details'] = "user_panel/update_driver_details";
$route['user-panel/driver-avatar-update'] = "user_panel/driver_avatar_update";
$route['user-panel/add-driver'] = "user_panel/add_driver";
$route['user-panel/add-driver-details'] = "user_panel/add_driver_details";
$route['user-panel/vehicle-list'] = "user_panel/vehicle_list";
$route['user-panel/vehicle-add'] = "user_panel/vehicle_add";
$route['user-panel/vehicle-edit'] = "user_panel/vehicle_edit";
$route['user-panel/service-area'] = "user_panel/service_area_list";
$route['user-panel/service-area-add'] = "user_panel/service_area_add";
// My bookings
$route['user-panel/my-bookings'] = "user_panel/my_bookings";
$route['user-panel/add-bookings'] = "user_panel/add_bookings";
$route['user-panel/advance-payment'] = "user_panel/advance_payment";
$route['user-panel/courier-payment'] = "user_panel/courier_payment";
$route['user-panel/confirm-payment'] = "user_panel/confirm_payment";
$route['user-panel/confirm-orange-payment'] = "user_panel/confirm_orange_payment";
$route['user-panel/confirm-orange-payment-process'] = "user_panel/confirm_orange_payment_process";
$route['user-panel/cancel-order'] = "user_panel/cancel_order";
$route['user-panel/my-booking-order-details'] = "user_panel/my_booking_order_details";
$route['user-panel/accepted-bookings'] = "user_panel/accepted_bookings";
$route['user-panel/accept-booking-order-details/(:num)'] = "user_panel/accept_booking_order_details/$1";
$route['user-panel/in-progress-bookings'] = "user_panel/in_progress_bookings";
$route['user-panel/in-progress-booking-order-details/(:num)'] = "user_panel/in_progress_booking_details/$1";
$route['user-panel/delivered-bookings'] = "user_panel/delivered_bookings";
$route['user-panel/delivered-booking-order-details/(:num)'] = "user_panel/delivered_booking_details/$1";
$route['user-panel/open-bookings'] = "user_panel/open_bookings";
$route['user-panel/open-bookings-searched'] = "user_panel/open_bookings_searched";
$route['user-panel/requested-deliverer'] = "user_panel/requested_deliverer";
$route['user-panel/deliverer-details'] = "user_panel/deliverer_details";
$route['user-panel/find-deliverer'] = "user_panel/find_deliverer";
$route['user-panel/order-requests'] = "user_panel/order_requests";
$route['user-panel/requested-order-details/(:any)'] = "user_panel/requested_order_details/$1";
$route['user-panel/accepted-orders'] = "user_panel/accepted_orders";
$route['user-panel/assign-driver'] = "user_panel/assign_driver";
$route['user-panel/assign-driver/(:any)'] = "user_panel/assign_driver/$1";
$route['user-panel/assign-order-driver'] = "user_panel/assign_order_driver";
$route['user-panel/in-progress-orders'] = "user_panel/in_progress_orders";
$route['user-panel/in-progress-order-details/(:any)'] = "user_panel/in_progress_order_details/$1";
$route['user-panel/delivered-orders'] = "user_panel/delivered_orders";
$route['user-panel/delivered-order-details/(:any)'] = "user_panel/delivered_order_details/$1";
$route['user-panel/courier-chat-room'] = "user_panel/chatroom";
$route['user-panel/courier-work-room'] = "user_panel/courier_work_room";
$route['user-panel/courier-work-room/(:num)'] = "user_panel/courier_work_room/$1";
$route['user-panel/courier-workroom'] = "user_panel/courier_work_room";
$route['user-panel/user-notifications'] = "user_panel/user_notifications";
$route['user-panel/notification-detail'] = "user_panel/notification_detail";
//Promo Code Apply Courier and Transport
$route['user-panel/promocode'] = "user_panel/promocode";
$route['user-panel/promocode-with-booking'] = "user_panel/promocode_with_booking";
$route['user-panel/promocode-ajax'] = "user_panel/promocode_ajax";
$route['user-panel-bus/promocode'] = "user_panel_bus/promocode";
$route['user-panel-bus/promocode-ajax'] = "user_panel_bus/promocode_ajax";	
$route['user-panel-laundry/promocode'] = "user_panel_laundry/promocode";
$route['user-panel-laundry/promocode-ajax'] = "user_panel_laundry/promocode_ajax";
//Account Sections
$route['user-panel/my-wallet'] = "user_panel/my_wallet";
$route['user-panel/user-account-history'] = "user_panel/user_account_history";
$route['user-panel/my-scrow'] = "user_panel/my_scrow";
$route['user-panel/user-scrow-history'] = "user_panel/user_scrow_history";
$route['user-panel/my-withdrawals-request'] = "user_panel/my_withdrawals_request";
$route['user-panel/my-withdrawals-accepted'] = "user_panel/my_withdrawals_accepted";
$route['user-panel/my-withdrawals-rejected'] = "user_panel/my_withdrawals_rejected";
$route['user-panel/withdrawal-request-details'] = "user_panel/withdrawal_request_details";
$route['user-panel/create-withdraw-request'] = "user_panel/create_withdraw_request";
$route['user-panel/withdraw-request-sent'] = "user_panel/withdraw_request_sent";
$route['user-panel/customer-order-pickup'] = "user_panel/customer_order_pickup";
$route['user-panel/user-invoices-history'] = "user_panel/user_invoices_history";
$route['user-panel/user-pending-invoices-history'] = "user_panel/user_pending_invoices_history";
$route['user-panel/booking-details'] = "user_panel/booking_details";
$route['user-panel/get-relay-by-location'] = "user_panel/get_relay_by_location";
$route['user-panel/get-relay-by-lat-long'] = "user_panel/get_relay_by_lat_long";
// education
$route['user-panel/basic-profile/:num'] = "user_panel/edit_basic_profile_view";
$route['user-panel/basic-profile/update'] = "user_panel/update_basic_profile";
$route['user-panel/education/add'] = "user_panel/add_education_view";
$route['user-panel/add-education'] = "user_panel/add_education";
$route['user-panel/education/:num'] = "user_panel/edit_education_view/$1";
$route['user-panel/edit-education'] = "user_panel/edit_education";
$route['user-panel/delete-education'] = "user_panel/delete_education";
$route['user-panel/educations'] = "user_panel/education_list_view";
//  experience
$route['user-panel/experience/add'] = "user_panel/add_experience_view";
$route['user-panel/add-experience'] = "user_panel/add_experience";
$route['user-panel/experiences'] = "user_panel/experience_list_view";
$route['user-panel/experience/:num'] = "user_panel/edit_experience_view/$1";
$route['user-panel/edit-experience'] = "user_panel/edit_experience";
// other experience
$route['user-panel/other-experience/add'] = "user_panel/add_other_experience_view";
$route['user-panel/add-other-experience'] = "user_panel/add_other_experience";
$route['user-panel/other-experiences'] = "user_panel/other_experience_list_view";
$route['user-panel/other-experience/:num'] = "user_panel/edit_other_experience_view/$1";
$route['user-panel/edit-other-experience'] = "user_panel/edit_other_experience";
// Portfolio
$route['user-panel/portfolio/add'] = "user_panel/add_portfolio_view";
$route['user-panel/add-portfolio'] = "user_panel/add_portfolio";
$route['user-panel/portfolios'] = "user_panel/portfolio_list_view";
$route['user-panel/portfolio/:num'] = "user_panel/edit_portfolio_view/$1";
$route['user-panel/edit-portfolio'] = "user_panel/edit_portfolio";
$route['user-panel/portfolio/get-category-by-cat-type-id'] = "user_panel/get_category_by_cat_type_id";
$route['user-panel/portfolio/get-subcategories-by-category-id'] = "user_panel/get_subcategories_by_category_id";
// skill
$route['user-panel/skill/add'] = "user_panel/add_skill_view";
$route['user-panel/add-skill'] = "user_panel/add_skill";
$route['user-panel/skills'] = "user_panel/skill_list_view";
$route['user-panel/skill'] = "user_panel/edit_skill_view";
$route['user-panel/edit-skill'] = "user_panel/edit_skill";
$route['user-panel/skill/get-skills-by-category-id'] = "user-panel/get_skills_by_category_id";
// Document
$route['user-panel/document/add'] = "user_panel/add_document_view";
$route['user-panel/add-document'] = "user_panel/add_document";
$route['user-panel/documents'] = "user_panel/document_list_view";
$route['user-panel/document/:num'] = "user_panel/edit_document_view/$1";
$route['user-panel/edit-document'] = "user_panel/edit_document";
// Payment
$route['user-panel/payment-method/add'] = "user_panel/add_payment_method_view";
$route['user-panel/add-payment-method'] = "user_panel/add_payment_method";
$route['user-panel/payment-methods'] = "user_panel/payment_method_list_view";
$route['user-panel/payment-method/:num'] = "user_panel/edit_payment_method_view/$1";
$route['user-panel/edit-payment-method'] = "user_panel/edit_payment_method";
$route['user-panel/get-languages-master'] = "user_panel/get_languages_master";
$route['user-panel/update-languages'] = "user_panel/update_languages";
$route['user-panel/update-overview'] = "user_panel/update_overview";
$route['user-panel/basic-profile/get-state-by-country-id'] = "user_panel/get_state_by_country_id";
$route['user-panel/basic-profile/get-cities-by-state-id'] = "user_panel/get_city_by_state_id";
//Deliverer Address Book
$route['user-panel/address-book/add'] = "user_panel/add_address_book_view";
$route['user-panel/add-address-book'] = "user_panel/add_address_to_book";
$route['transport/add-address-book-by-panel'] = "user_panel/add_address_to_book_by_panel";
$route['user-panel/add-address-book-by-panel'] = "user_panel/add_address_to_book_by_panel";
$route['user-panel/add-address-to-book-from-address'] = "user_panel/add_address_to_book_from_address";
$route['user-panel/add-address-to-book-to-address'] = "user_panel/add_address_to_book_to_address";
$route['user-panel/address-book/:num'] = "user_panel/edit_address_book_view/$1";
$route['user-panel/edit-address-book'] = "user_panel/edit_address";
$route['user-panel/address-book/get-state-by-country-id'] = "user_panel/get_state_by_country_id";
$route['user-panel/address-book/get-cities-by-state-id'] = "user_panel/get_city_by_state_id";
// notifications
$route['user-panel/notifications-read'] = "user_panel/notifications_reads";
$route['user-panel/workroom-read'] = "user_panel/workroom_reads";
// Customer's order list
$route['user-panel/courier-order-list'] = "user_panel/courier_order_list";
$route['user-panel/add-workroom-chat'] = "user_panel/add_workroom_chat";
$route['user-panel/add-workroom-rating'] = "user_panel/add_workroom_rating";
$route['user-panel/add-order-chat'] = "user_panel/add_order_chat";
// Create Order
$route['user-panel/get-transport'] = "user_panel/get_transport_vehicle_list_by_type";
$route['user-panel/insurance-price'] = "user_panel/get_insurance_price";
$route['user-panel/total-price'] = "user_panel/get_total_order_price";
$route['user-panel/register-booking'] = "user_panel/register_booking";
$route['user-panel/get-address-relay-lat-long'] = "user_panel/get_address_relay_lat_long";
$route['user-panel/get-driving-distance'] = "user_panel/get_driving_distance";
$route['user-panel/get-standard-dimensions'] = 'user_panel/get_dimension_list_by_transport_type';
$route['user-panel/mark-as-cod'] = 'user_panel/mark_as_cod';
// favourite deliverer
$route['user-panel/favourite-deliverers'] = "user_panel/favourite_deliverers";
$route['user-panel/make-deliverer-favourite'] = "user_panel/make_deliverer_favourite";
$route['user-panel/remove-deliverer-from-favourite'] = "user_panel/remove_deliverer_from_favourite";
$route['user-panel/search-deliverer-details-view'] = "user_panel/search_deliverer_details_view";
$route['user-panel/search-for-deliverers'] = "user_panel/search_for_deliverers";
// request to deliverer
$route['user-panel/send-request-to-deliverer'] = "user_panel/send_request_to_deliverer";
$route['user-panel/get-cities-by-state-id'] = "user_panel/get_city_by_state_id";
// Change Password
$route['user-panel/change-password'] = "user_panel/change_password";
$route['user-panel/update-change'] = "user_panel/update_password";
// Front site URLs
$route['index'] = 'front_site/index';
$route['courier'] = 'front_site/courier_index';

$route['offers'] = 'front_site/available_offers';
$route['buyoffer'] = 'front_site/buy_offer';
$route['offer-details'] = 'front_site/offers_details';



$route['log-in-booking'] = 'front_site/log_in_booking';
$route['get-transport'] = "front_site/get_transport_vehicle_list_by_type";
$route['get-standard-dimensions'] = 'front_site/get_dimension_list_by_transport_type';
$route['transport'] = 'front_site/transport_index';
$route['ticket'] = 'front_site/ticket_index';
$route['ticket-search'] = 'front_site/ticket_search';
$route['ticket-search-filter'] = 'front_site/ticket_search_filter';
$route['ticket-booking-process'] = 'front_site/ticket_booking_process';
$route['more-passengers-details'] = 'front_site/more_passengers_details';
$route['ticket-booking'] = 'front_site/ticket_booking';
$route['ticket-payment/(:any)'] = 'front_site/bus_ticket_payment_view/$1';
$route['ticket-payment-cod'] = 'front_site/ticket_mark_as_cod';
$route['ticket-payment-mtn'] = 'front_site/ticket_payment_by_mtn';
$route['ticket-payment-stripe'] = 'front_site/ticket_payment_by_stripe';
$route['ticket-payment-orange'] = 'front_site/ticket_payment_by_orange';
$route['ticket-payment-orange-process'] = 'front_site/ticket_payment_by_orange_process';
$route['ticket-payment-confirmation/(:any)'] = 'front_site/ticket_payment_confirmation/$1';
$route['laundry'] = 'front_site/laundry_index';
$route['get-laundry-sub-category-append'] = "front_site/get_laundry_sub_category_append";
$route['add-temp-cloth-count'] = "front_site/add_temp_cloth_count";
$route['subs-temp-cloth-count'] = "front_site/subs_temp_cloth_count";
$route['laundry-providers'] = 'front_site/laundry_providers';
$route['laundry-booking-details'] = 'front_site/laundry_booking_details';

$route['gas'] = 'front_site/page_not_found';
$route['services'] = 'front_site/services_index';

$route['send-android-app-link'] = 'front_site/send_android_app_link';
$route['send-ios-app-link'] = 'front_site/send_ios_app_link';
$route['log-in'] = 'front_site/login';
$route['log-out'] = 'front_site/logout';
$route['confirm/log-in'] = 'front_site/confirm_login';
$route['sign-up'] = 'front_site/signup';
$route['register-new-user'] = 'front_site/signup_customer';
$route['check-email-exists'] = 'front_site/check_email_exists';
$route['search'] = "front_site/search";
$route['resend-password'] = "front_site/resend_password";
$route['resend-otp'] = "front_site/resend_otp";
$route['courier-detail'] = "front_site/courier_detail";
$route['terms-conditions'] = 'front_site/terms_conditions';
$route['about-us'] = 'front_site/about_us';
$route['careers'] = 'front_site/careers';
$route['send-resume'] = 'front_site/send_resume';
$route['verify-email/(:any)'] = 'verify/email_verification/$1';
$route['verification'] = 'verify/email_verified';
$route['resend-email-verification'] = 'verify/resend_verification_email';
$route['verification/otp'] = 'verify/otp';
$route['confirm/otp'] = 'front_site/confirm_otp';
$route['resend/otp'] = 'verify/resend_otp';
$route['recovery/password'] = 'verify/recovery_password';
$route['home'] = 'verify/index';
// set lang
$route['set-lang'] = 'front_site/set_language';
$route['user-panel/set-lang'] = 'user_panel/set_language';
$route['user-panel/(:any)/set-lang'] = 'user_panel/set_language';
//  under construction page
$route['under-construction'] = 'under_construction/index';
$route['under-construction/keep-me-inform'] = 'under_construction/keep_me_inform';
$route['page-not-found'] = 'front_site/page_not_found';
$route['payment/payment-by-mtn-from-mobile'] = "payment/payment_by_mtn_from_mobile";
$route['payment/wecashup'] = "payment/payment_by_wecashup";
$route['payment/android-payment'] = "payment/android_payment_by_wecashup";
$route['payment/ios-payment'] = "payment/ios_payment_by_wecashup";
$route['payment/success'] = "payment/success";
$route['payment/failed'] = "payment/failed";
$route['payment/mtn'] = "payment/payment_by_mtn";
$route['payment/orange'] = "payment/payment_by_orange";
///////////////////////// TRANSPORT Create Order
$route['transport/add-bookings'] = "transport/add_bookings";
$route['transport/get-transport'] = "user_panel/get_transport_vehicle_list_by_type";
$route['transport/get-standard-dimensions'] = 'user_panel/get_dimension_list_by_transport_type';
$route['transport/insurance-price'] = "user_panel/get_insurance_price";
$route['transport/total-price'] = "transport/get_total_order_price";
$route['transport/register-booking'] = "transport/register_booking";
$route['transport/get-address-relay-lat-long'] = "user_panel/get_address_relay_lat_long";
$route['transport/get-driving-distance'] = "user_panel/get_driving_distance";
$route['transport/get-dangerous-goods'] = "transport/get_countries_dangerous_goods";
$route['transport/get-state-by-country-id'] = "user_panel/get_state_by_country_id";
$route['transport/get-relay-by-location'] = "user_panel/get_relay_by_location";
$route['transport/get-cities-by-state-id'] = "user_panel/get_city_by_state_id";
$route['transport/add-address-to-book-from-address'] = "user_panel/add_address_to_book_from_address";
$route['transport/add-address-to-book-to-address'] = "user_panel/add_address_to_book_to_address";
$route['transport/get-address-by-id'] = "transport/get_address_by_id";
///////////////////////// HOME MOVE
$route['home-move/add-booking'] = "home_move/add_bookings";
// standard-rates
$route['user-panel/standard-rates-list'] = 'user_panel/standard_rates_list';
$route['user-panel/add-volume-based'] = 'user_panel/add_volume_based';
$route['user-panel/add-weight-based'] = 'user_panel/add_weight_based';
$route['user-panel/add-formula-volume-based'] = 'user_panel/add_formula_volume_based';
$route['user-panel/add-formula-weight-based'] = 'user_panel/add_formula_weight_based';
$route['user-panel/get-country-currencies'] = 'user_panel/get_country_currencies';
$route['user-panel/register-volume-based'] = 'user_panel/register_volume_based';
$route['user-panel/register-weight-based'] = 'user_panel/register_weight_based';
$route['user-panel/register-formula-volume-based'] = 'user_panel/register_formula_volume_based';
$route['user-panel/register-formula-weight-based'] = 'user_panel/register_formula_weight_based';
$route['user-panel/edit-standard-rates'] = 'user_panel/edit_standard_rates';
$route['user-panel/edit-standard-rates/(:num)'] = 'user_panel/edit_standard_rates/$1';
$route['user-panel/update-standard-rates'] = 'user_panel/update_standard_rates';
$route['user-panel/delete-standard-rates'] = 'user_panel/delete_standard_rates';
$route['user-panel/get-unit-detail'] = 'user_panel/get_unit_detail';
// Point to Point Rates
$route['user-panel/ptop-rates-list'] = 'user_panel/ptop_rates_list';
$route['user-panel/add-ptop-volume-based'] = 'user_panel/add_ptop_volume_based';
$route['user-panel/add-ptop-weight-based'] = 'user_panel/add_ptop_weight_based';
$route['user-panel/add-ptop-formula-volume-based'] = 'user_panel/add_ptop_formula_volume_based';
$route['user-panel/add-ptop-formula-weight-based'] = 'user_panel/add_ptop_formula_weight_based';
$route['user-panel/register-ptop-volume-based'] = 'user_panel/register_ptop_volume_based';
$route['user-panel/register-ptop-weight-based'] = 'user_panel/register_ptop_weight_based';
$route['user-panel/register-ptop-formula-volume-based'] = 'user_panel/register_ptop_formula_volume_based';
$route['user-panel/register-ptop-formula-weight-based'] = 'user_panel/register_ptop_formula_weight_based';
$route['user-panel/edit-ptop-rates'] = 'user_panel/edit_ptop_rates';
$route['user-panel/edit-ptop-rates/(:num)'] = 'user_panel/edit_ptop_rates/$1';
$route['user-panel/update-ptop-rates'] = 'user_panel/update_ptop_rates';
$route['user-panel/delete-ptop-rates'] = 'user_panel/delete_ptop_rates';
/////////////////////////////////////// BUS Reservation /////////////////////////////////////////
//Admin Panel
// advance payment Bus
$route['admin/advance-payment-bus-reservation'] = 'advance_payment_bus_reservation/index';
$route['admin/advance-payment-bus-reservation/add'] = 'advance_payment_bus_reservation/add';
$route['admin/advance-payment-bus-reservation/register'] = 'advance_payment_bus_reservation/register';
$route['admin/advance-payment-bus-reservation/edit/(:num)'] = 'advance_payment_bus_reservation/edit/$1';
$route['admin/advance-payment-bus-reservation/update'] = 'advance_payment_bus_reservation/update';
$route['admin/advance-payment-bus-reservation/delete'] = 'advance_payment_bus_reservation/delete';
// Bus Cancellation and Rescheduling %
$route['admin/cancellation-rescheduling-bus-reservation'] = 'cancellation_rescheduling_bus_reservation/index';
$route['admin/cancellation-rescheduling-bus-reservation/add'] = 'cancellation_rescheduling_bus_reservation/add';
$route['admin/cancellation-rescheduling-bus-reservation/register'] = 'cancellation_rescheduling_bus_reservation/register';
$route['admin/cancellation-rescheduling-bus-reservation/edit/(:num)'] = 'cancellation_rescheduling_bus_reservation/edit/$1';
$route['admin/cancellation-rescheduling-bus-reservation/update'] = 'cancellation_rescheduling_bus_reservation/update';
$route['admin/cancellation-rescheduling-bus-reservation/delete'] = 'cancellation_rescheduling_bus_reservation/delete';
$route['user-panel/landing'] = "user_panel/landing";
//User Profile
$route['user-panel-bus/user-profile'] = "user_panel_bus/user_profile";
$route['user-panel-bus/basic-profile/:num'] = "user_panel_bus/edit_basic_profile_view";
$route['user-panel-bus/basic-profile/update'] = "user_panel_bus/update_basic_profile";
$route['user-panel-bus/basic-profile/get-state-by-country-id'] = "user_panel_bus/get_state_by_country_id";
$route['user-panel-bus/basic-profile/get-cities-by-state-id'] = "user_panel_bus/get_city_by_state_id";
// Payment
$route['user-panel-bus/payment-methods'] = "user_panel_bus/payment_method_list_view";
$route['user-panel-bus/payment-method/add'] = "user_panel_bus/add_payment_method_view";
$route['user-panel-bus/add-payment-method'] = "user_panel_bus/add_payment_method";
$route['user-panel-bus/payment-method/:num'] = "user_panel_bus/edit_payment_method_view/$1";
$route['user-panel-bus/edit-payment-method'] = "user_panel_bus/edit_payment_method";
// education
$route['user-panel-bus/educations'] = "user_panel_bus/education_list_view";
$route['user-panel-bus/education/add'] = "user_panel_bus/add_education_view";
$route['user-panel-bus/add-education'] = "user_panel_bus/add_education";
$route['user-panel-bus/education/:num'] = "user_panel_bus/edit_education_view/$1";
$route['user-panel-bus/edit-education'] = "user_panel_bus/edit_education";
$route['user-panel-bus/delete-education'] = "user_panel_bus/delete_education";
//  experience
$route['user-panel-bus/experiences'] = "user_panel_bus/experience_list_view";
$route['user-panel-bus/experience/add'] = "user_panel_bus/add_experience_view";
$route['user-panel-bus/add-experience'] = "user_panel_bus/add_experience";
$route['user-panel-bus/experience/:num'] = "user_panel_bus/edit_experience_view/$1";
$route['user-panel-bus/edit-experience'] = "user_panel_bus/edit_experience";
// other experience
$route['user-panel-bus/other-experience/add'] = "user_panel_bus/add_other_experience_view";
$route['user-panel-bus/add-other-experience'] = "user_panel_bus/add_other_experience";
$route['user-panel-bus/other-experiences'] = "user_panel_bus/other_experience_list_view";
$route['user-panel-bus/other-experience/:num'] = "user_panel_bus/edit_other_experience_view/$1";
$route['user-panel-bus/edit-other-experience'] = "user_panel_bus/edit_other_experience";
// skill
$route['user-panel-bus/skill/add'] = "user_panel_bus/add_skill_view";
$route['user-panel-bus/add-skill'] = "user_panel_bus/add_skill";
$route['user-panel-bus/skills'] = "user_panel_bus/skill_list_view";
$route['user-panel-bus/skill'] = "user_panel_bus/edit_skill_view";
$route['user-panel-bus/edit-skill'] = "user_panel_bus/edit_skill";
$route['user-panel-bus/skill/get-skills-by-category-id'] = "user_panel_bus/get_skills_by_category_id";
// Portfolio
$route['user-panel-bus/portfolio/add'] = "user_panel_bus/add_portfolio_view";
$route['user-panel-bus/add-portfolio'] = "user_panel_bus/add_portfolio";
$route['user-panel-bus/portfolios'] = "user_panel_bus/portfolio_list_view";
$route['user-panel-bus/portfolios/:num'] = "user_panel_bus/edit_portfolio_view/$1";
$route['user-panel-bus/edit-portfolio'] = "user_panel_bus/edit_portfolio";
$route['user-panel-bus/portfolio/get-category-by-cat-type-id'] = "user_panel_bus/get_category_by_cat_type_id";
$route['user-panel-bus/portfolio/get-subcategories-by-category-id'] = "user_panel_bus/get_subcategories_by_category_id";
// Language
$route['user-panel-bus/get-languages-master'] = "user_panel_bus/get_languages_master";
$route['user-panel-bus/update-languages'] = "user_panel_bus/update_languages";
//Overview
$route['user-panel-bus/update-overview'] = "user_panel_bus/update_overview";
// Document
$route['user-panel-bus/document/add'] = "user_panel_bus/add_document_view";
$route['user-panel-bus/add-document'] = "user_panel_bus/add_document";
$route['user-panel-bus/documents'] = "user_panel_bus/document_list_view";
$route['user-panel-bus/document/:num'] = "user_panel_bus/edit_document_view/$1";
$route['user-panel-bus/edit-document'] = "user_panel_bus/edit_document";
// Change Password
$route['user-panel-bus/change-password'] = "user_panel_bus/change_password";
$route['user-panel-bus/update-change'] = "user_panel_bus/update_password";
//Suggession and support
$route['user-panel-bus/get-support-list'] = "user_panel_bus/get_support_list";
$route['user-panel-bus/get-support'] = "user_panel_bus/get_support";
$route['user-panel-bus/add-support-details'] = "user_panel_bus/add_support_details";
$route['user-panel-bus/view-support-reply-details/(:num)'] = "user_panel_bus/view_support_reply_details/$1";
//Notifications
$route['user-panel-bus/user-notifications'] = "user_panel_bus/user_notifications";
$route['user-panel-bus/notification-detail'] = "user_panel_bus/notification_detail";
$route['user-panel-bus/notifications-read'] = "user_panel_bus/notifications_reads";
//Workroom Notifications
$route['user-panel-bus/bus-work-room/(:num)'] = "user_panel_bus/bus_work_room/$1";
$route['user-panel-bus/ticket-booking-list'] = "user_panel_bus/ticket_booking_list";
//Operator Profile Sections
$route['user-panel-bus/dashboard-bus'] = "user_panel_bus/dashboard_bus";
$route['user-panel-bus/vehicle-master-list'] = "user_panel_bus/vehicle_master_list";
//Manage Bus
$route['user-panel-bus/bus-list'] = "user_panel_bus/bus_list";
$route['user-panel-bus/get-vehicle-seat-type'] = "user_panel_bus/get_vehicle_seat_type";
$route['user-panel-bus/bus-add'] = "user_panel_bus/bus_add";
$route['user-panel-bus/bus-add-details'] = "user_panel_bus/bus_add_details";
$route['user-panel-bus/bus-edit'] = "user_panel_bus/bus_edit";
$route['user-panel-bus/bus-edit/(:num)'] = 'user_panel_bus/bus_edit/$1';
$route['user-panel-bus/bus-edit-details'] = "user_panel_bus/bus_edit_details";
$route['user-panel-bus/bus-delete'] = "user_panel_bus/bus_delete";
$route['user-panel-bus/bus-layout-manager'] = "user_panel_bus/bus_layout_manager";
//Manage Drivers
$route['user-panel-bus/driver-list'] = "user_panel_bus/driver_list";
$route['user-panel-bus/view-driver'] = "user_panel_bus/view_driver";
$route['user-panel-bus/view-driver/(:any)'] = 'user_panel_bus/view_driver/$1'; //view
$route['user-panel-bus/add-driver'] = "user_panel_bus/add_driver";
$route['user-panel-bus/get-vehicle-by-type-id'] = "user_panel_bus/get_vehicle_by_type_id";
$route['user-panel-bus/add-driver-details'] = "user_panel_bus/add_driver_details";
$route['user-panel-bus/driver-inactive-list'] = "user_panel_bus/driver_inactive_list";
$route['user-panel-bus/driver-active'] = "user_panel_bus/driver_active";
$route['user-panel-bus/driver-inactive'] = 'user_panel_bus/driver_inactive';
$route['user-panel-bus/edit-driver'] = "user_panel_bus/edit_driver";
$route['user-panel-bus/edit-driver/(:any)'] = 'user_panel_bus/edit_driver/$1'; //view
$route['user-panel-bus/update-driver-details'] = "user_panel_bus/update_driver_details";
$route['user-panel-bus/driver-avatar-update'] = "user_panel_bus/driver_avatar_update";
$route['user-panel-bus/driver-trip-passengers-view'] = "user_panel_bus/driver_trip_passengers_view";
// Deliverer Profile
$route['user-panel-bus/bus-operator-profile'] = "user_panel_bus/bus_operator_profile";
$route['user-panel-bus/edit-operator-profile'] = "user_panel_bus/edit_operator_profile";
$route['user-panel-bus/get-state-by-country-id'] = "user_panel_bus/get_state_by_country_id";
$route['user-panel-bus/get-cities-by-state-id'] = "user_panel_bus/get_city_by_state_id";
$route['user-panel-bus/add-operator-document'] = "user_panel_bus/add_operator_document";
$route['user-panel-bus/edit-operator-document'] = "user_panel_bus/edit_operator_document";
$route['user-panel-bus/operator-document-delete'] = "user_panel_bus/operator_document_delete";
$route['user-panel-bus/bus-operator-business-photos-upload'] = "user_panel_bus/bus_operator_business_photos_upload";
$route['user-panel-bus/bus-operator-business-photos-delete'] = "user_panel_bus/bus_operator_business_photos_delete";
//Bus Operator Profile #zaidu
$route['api-bus/register-bus-operator'] = 'api-bus/register_bus_operator';
$route['api-bus/add-bus-operator-document'] = 'api-bus/add_bus_operator_document';
$route['api-bus/update-bus-operator']='api-bus/update_bus_operator';
$route['api-bus/update-bus-operator-avatar']='api-bus/update_bus_operator_avatar';
$route['api-bus/update-bus-operator-cover']='api-bus/update_bus_operator_cover';
$route['api-bus/update-bus-operator-bank-info']='api-bus/update_bus_operator_bank_info';
$route['api-bus/get-bus-operator-business-photos']='api-bus/get_bus_operator_business_photos';
$route['api-bus/get-bus-operator-documents']='api-bus/get_bus_operator_documents';
$route['api-bus/delete-bus-operator-business-photo']='api-bus/delete_bus_operator_business_photo';
$route['api-bus/delete-bus-operator-document']='api-bus/delete_bus_operator_document';
$route['api-bus/upload-bus-operator-business-photo']='api-bus/upload_bus_operator_business_photo';
// Bus Cancellation and Rescheduling charges
$route['user-panel-bus/bus-operator-cancellation-charges-list'] = "user_panel_bus/bus_operator_cancellation_charges_list";
$route['user-panel-bus/add-bus-operator-cancellation-charges'] = "user_panel_bus/add_bus_operator_cancellation_charges";
$route['user-panel-bus/add-bus-operator-cancellation-charges-details'] = "user_panel_bus/add_bus_operator_cancellation_charges_details";
$route['user-panel-bus/edit-bus-operator-cancellation-charges'] = "user_panel_bus/edit_bus_operator_cancellation_charges";
$route['user-panel-bus/edit-bus-operator-cancellation-charges/(:any)'] = 'user_panel_bus/edit_bus_operator_cancellation_charges/$1'; //view
$route['user-panel-bus/edit-bus-operator-cancellation-charges-details'] = "user_panel_bus/edit_bus_operator_cancellation_charges_details";
$route['user-panel-bus/cencellation-rescheduling-charges-delete'] = "user_panel_bus/cencellation_rescheduling_charges_delete";
//Agent Permissions
$route['user-panel-bus/agent-permissions-list'] = "user_panel_bus/agent_permissions_list";
$route['user-panel-bus/add-agent-permissions'] = "user_panel_bus/add_agent_permissions";
$route['user-panel-bus/group-permission-add-details'] = "user_panel_bus/group_permission_add_details";
//Bus Agents
$route['user-panel-bus/bus-operator-agent-active-list'] = "user_panel_bus/bus_operator_agent_active_list";
$route['user-panel-bus/bus-operator-agent-inactive-list'] = "user_panel_bus/bus_operator_agent_inactive_list";
$route['user-panel-bus/bus-operator-agent-add'] = "user_panel_bus/bus_operator_agent_add";
$route['user-panel-bus/bus-operator-agent-add-details'] = "user_panel_bus/bus_operator_agent_add_details";
$route['user-panel-bus/bus-operator-agent-edit'] = "user_panel_bus/bus_operator_agent_edit";
$route['user-panel-bus/bus-operator-agent-edit/(:any)'] = 'user_panel_bus/bus_operator_agent_edit/$1';
$route['user-panel-bus/bus-operator-agent-edit-details'] = 'user_panel_bus/bus_operator_agent_edit_details'; 
$route['user-panel-bus/bus-operator-agent-activate'] = "user_panel_bus/bus_operator_agent_activate";
$route['user-panel-bus/bus-operator-agent-inactivate'] = "user_panel_bus/bus_operator_agent_inactivate";
//Pickup Drop Points
$route['user-panel-bus/bus-pick-drop-point-list'] = "user_panel_bus/bus_pick_drop_point_list";
$route['user-panel-bus/bus-pick-drop-point-list-view'] = "user_panel_bus/bus_pick_drop_point_list_view";
$route['user-panel-bus/point-remove-as-pickup'] = "user_panel_bus/point_remove_as_pickup";
$route['user-panel-bus/point-mark-as-pickup'] = "user_panel_bus/point_mark_as_pickup";
$route['user-panel-bus/point-remove-as-drop'] = "user_panel_bus/point_remove_as_drop";
$route['user-panel-bus/point-mark-as-drop'] = "user_panel_bus/point_mark_as_drop";
$route['user-panel-bus/bus-pick-drop-point-add-details'] = "user_panel_bus/bus_pick_drop_point_add_details";
$route['user-panel-bus/bus-location-activate'] = "user_panel_bus/bus_location_activate";
$route['user-panel-bus/bus-location-inactivate'] = "user_panel_bus/bus_location_inactivate";
$route['user-panel-bus/bus-pick-drop-location-and-point-add'] = "user_panel_bus/bus_pick_drop_location_and_point_add";
$route['user-panel-bus/bus-pick-drop-location-and-point-add-details'] = "user_panel_bus/bus_pick_drop_location_and_point_add_details";
$route['user-panel-bus/bus-location-point-delete'] = "user_panel_bus/bus_location_point_delete";
$route['user-panel-bus/get-locations-by-vehicle-type-id'] = "user_panel_bus/get_locations_by_vehicle_type_id";
//Address Book
$route['user-panel-bus/address-book-list'] = "user_panel_bus/address_book_list";
$route['user-panel-bus/address-book-add'] = "user_panel_bus/address_book_add";
$route['user-panel-bus/address-book-add-details'] = "user_panel_bus/address_book_add_details";
$route['user-panel-bus/bus-address-book-edit'] = "user_panel_bus/bus_address_book_edit"; //view
$route['user-panel-bus/bus-address-book-edit/(:any)'] = 'user_panel_bus/bus_address_book_edit/$1'; //view
$route['user-panel-bus/bus-address-book-edit-details'] = 'user_panel_bus/bus_address_book_edit_details'; 
$route['user-panel-bus/bus-address-delete'] = "user_panel_bus/bus_address_delete";
//Trip Master
$route['user-panel-bus/bus-trip-master-list'] = "user_panel_bus/bus_trip_master_list";
$route['user-panel-bus/bus-trip-master-add'] = "user_panel_bus/bus_trip_master_add";
$route['user-panel-bus/bus-trip-master-add-details'] = "user_panel_bus/bus_trip_master_add_details";
$route['user-panel-bus/bus-trip-master-edit'] = "user_panel_bus/bus_trip_master_edit"; //view
$route['user-panel-bus/bus-trip-master-edit/(:any)'] = 'user_panel_bus/bus_trip_master_edit/$1'; //view
$route['user-panel-bus/bus-trip-master-edit-details'] = 'user_panel_bus/bus_trip_master_edit_details'; 
$route['user-panel-bus/bus-trip-master-delete'] = "user_panel_bus/bus_trip_master_delete";
$route['user-panel-bus/get-locations-pickup-points'] = "user_panel_bus/get_locations_pickup_points";
$route['user-panel-bus/get-locations-drop-points'] = "user_panel_bus/get_locations_drop_points";
$route['user-panel-bus/bus-trip-location-list'] = "user_panel_bus/bus_trip_location_list";
$route['user-panel-bus/bus-trip-location-list/(:any)'] = 'user_panel_bus/bus_trip_location_list/$1'; //view
$route['user-panel-bus/bus-trip-location-add-details'] = "user_panel_bus/bus_trip_location_add_details";
$route['user-panel-bus/bus-trip-location-delete'] = "user_panel_bus/bus_trip_location_delete";
$route['user-panel-bus/bus-trip-master-disable'] = "user_panel_bus/bus_trip_master_disable";
$route['user-panel-bus/bus-trip-master-disable/(:any)'] = 'user_panel_bus/bus_trip_master_disable/$1'; //view
$route['user-panel-bus/bus-trip-master-disable-details'] = "user_panel_bus/bus_trip_master_disable_details";
$route['user-panel-bus/bus-trip-master-enable'] = "user_panel_bus/bus_trip_master_enable";
$route['user-panel-bus/bus-trip-master-enable/(:any)'] = 'user_panel_bus/bus_trip_master_enable/$1'; //view
$route['user-panel-bus/bus-trip-master-enable-details'] = "user_panel_bus/bus_trip_master_enable_details";
//Trip Special Rate
$route['user-panel-bus/bus-trip-special-rate-list'] = "user_panel_bus/bus_trip_special_rate_list";
$route['user-panel-bus/bus-trip-special-rate-edit'] = "user_panel_bus/bus_trip_special_rate_edit";
$route['user-panel-bus/bus-trip-special-rate-edit/(:any)'] = "user_panel_bus/bus_trip_special_rate_edit/$1";
$route['user-panel-bus/bus-trip-special-rate-edit-details'] = "user_panel_bus/bus_trip_special_rate_edit_details";
//Search Ticket and create booking Buyer
$route['user-panel-bus/bus-ticket-search'] = "user_panel_bus/bus_ticket_search";
$route['user-panel-bus/bus-trip-search-result-buyer'] = "user_panel_bus/bus_trip_search_result_buyer";
$route['user-panel-bus/bus-trip-details-buyer'] = "user_panel_bus/bus_trip_details_buyer";
$route['user-panel-bus/bus-trip-details-buyer/(:any)'] = "user_panel_bus/bus_trip_details_buyer/$1";
$route['user-panel-bus/bus-trip-booking-buyer'] = "user_panel_bus/bus_trip_booking_buyer";
$route['user-panel-bus/bus-trip-booking-process-buyer'] = "user_panel_bus/bus_trip_booking_process_buyer";
$route['user-panel-bus/bus-ticket-payment-buyer'] = "user-panel-bus/bus_ticket_payment_buyer";
$route['user-panel-bus/bus-single-ticket-payment-buyer'] = "user-panel-bus/bus_single_ticket_payment_buyer";
$route['user-panel-bus/buyer-ticket-mark-as-cod'] = "user-panel-bus/buyer_ticket_mark_as_cod";
$route['user-panel-bus/buyer-single-ticket-mark-as-cod'] = "user-panel-bus/buyer_single_ticket_mark_as_cod";
$route['user-panel-bus/buyer-ticket-confirm-payment'] = "user-panel-bus/buyer_ticket_confirm_payment";
$route['user-panel-bus/buyer-single-ticket-confirm-payment'] = "user-panel-bus/buyer_single_ticket_confirm_payment";
$route['payment-bus/mtn'] = "payment_bus/payment_by_mtn";
$route['payment-bus/mtn-single'] = "payment_bus/payment_by_mtn_single";
$route['user-panel-bus/cancel-ticket-buyer'] = "user-panel-bus/cancel_ticket_buyer";
$route['user-panel-bus/buyer-ticket-orange-payment'] = "user-panel-bus/buyer_ticket_orange_payment";
$route['user-panel-bus/orange-payment-process'] = "user-panel-bus/orange_payment_process";
$route['user-panel-bus/buyer-single-ticket-orange-payment'] = "user-panel-bus/buyer_single_ticket_orange_payment";
$route['user-panel-bus/single-orange-payment-process'] = "user-panel-bus/single_orange_payment_process";
//Upcoming Ticket List Buyers
$route['user-panel-bus/buyer-upcoming-trips'] = "user_panel_bus/buyer_upcoming_trips";
$route['user-panel-bus/bus-upcoming-trip-details-buyer'] = "user_panel_bus/bus_upcoming_trip_details_buyer";
//Current Ticket List Buyers
$route['user-panel-bus/buyer-current-trips'] = "user_panel_bus/buyer_current_trips";
$route['user-panel-bus/bus-current-trip-details-buyer'] = "user_panel_bus/bus_current_trip_details_buyer";
//Previous Ticket List Buyers
$route['user-panel-bus/buyer-previous-trips'] = "user_panel_bus/buyer_previous_trips";
$route['user-panel-bus/bus-previous-trip-details-buyer'] = "user_panel_bus/bus_previous_trip_details_buyer";
//cancelled Ticket List Buyers
$route['user-panel-bus/buyer-cancelled-trips'] = "user_panel_bus/buyer_cancelled_trips";
$route['user-panel-bus/bus-cancelled-trip-details-buyer'] = "user_panel_bus/bus_cancelled_trip_details_buyer";
//Search Ticket and create booking seller
$route['user-panel-bus/seller-upcoming-trips'] = "user_panel_bus/seller_upcoming_trips";
$route['user-panel-bus/seller-upcoming-trips-filter'] = "user_panel_bus/seller_upcoming_trips_filter";
$route['user-panel-bus/seller-upcoming-trips-view-passengers'] = "user_panel_bus/seller_upcoming_trips_view_passengers";
$route['user-panel-bus/seller-upcoming-trips-view-passengers/(:any)'] = "user_panel_bus/seller_upcoming_trips_view_passengers/$1";
$route['user-panel-bus/seller-ticket-mark-as-cod'] = "user_panel_bus/seller_ticket_mark_as_cod";
$route['user-panel-bus/bus-upcoming-trip-details-seller'] = "user_panel_bus/bus_upcoming_trip_details_seller";
$route['user-panel-bus/bus-ticket-payment-seller'] = "user_panel_bus/bus_ticket_payment_seller";
$route['user-panel-bus/bus-single-ticket-payment-seller'] = "user_panel_bus/bus_single_ticket_payment_seller";
$route['user-panel-bus/seller-ticket-confirm-payment'] = "user_panel_bus/seller_ticket_confirm_payment";
$route['user-panel-bus/seller-single-ticket-confirm-payment'] = "user_panel_bus/seller_single_ticket_confirm_payment";
$route['user-panel-bus/seller-cancelled-trip'] = "user_panel_bus/seller_cancelled_trip";
$route['user-panel-bus/cancel-bus-trip-seller'] = "user_panel_bus/cancel_bus_trip_seller";
//seller current trips
$route['user-panel-bus/seller-current-trips'] = "user_panel_bus/seller_current_trips";
$route['user-panel-bus/seller-current-trips-view-passengers'] = "user_panel_bus/seller_current_trips_view_passengers";
$route['user-panel-bus/seller-current-trips-filter'] = "user_panel_bus/seller_current_trips_filter";
$route['user-panel-bus/bus-current-trip-details-seller'] = "user_panel_bus/bus_current_trip_details_seller";
$route['user-panel-bus/delete-bus-ticket-seller'] = "user_panel_bus/delete_bus_ticket_seller";
//Create booking by Operator
$route['user-panel-bus/bus-trip-ticket-booking-seller'] = "user_panel_bus/bus_trip_ticket_booking_seller";
$route['user-panel-bus/bus-trip-search-result-seller'] = "user_panel_bus/bus_trip_search_result_seller";
$route['user-panel-bus/bus-trip-details-seller'] = "user_panel_bus/bus_trip_details_seller";
$route['user-panel-bus/bus-trip-details-seller/(:any)'] = "user_panel_bus/bus_trip_details_seller/$1";
$route['user-panel-bus/bus-trip-booking-pickup-drop-details'] = "user_panel_bus/bus_trip_booking_pickup_drop_details";
$route['user-panel-bus/bus-trip-seat-booking'] = "user_panel_bus/bus_trip_seat_booking";
$route['user-panel-bus/get-seat'] = "user_panel_bus/get_seat";
$route['user-panel-bus/bus-trip-search-result-seller-filter'] = "user_panel_bus/bus_trip_search_result_seller_filter";
$route['user-panel-bus/search-pick-drop-points'] = "user_panel_bus/search_pick_drop_points";
$route['user-panel-bus/address-book-add-details-from-booking-page'] = "user_panel_bus/address_book_add_details_from_booking_page";
$route['user-panel-bus/get-bus-address-book'] = "user_panel_bus/get_bus_address_book";
$route['user-panel-bus/bus-trip-master-details'] = "user_panel_bus/bus_trip_master_details";
$route['user-panel-bus/bus-trip-master-details/(:any)'] = "user_panel_bus/bus_trip_master_details/$1";
$route['user-panel-bus/bus-ticket-payment'] = "user-panel-bus/bus_ticket_payment";
$route['user-panel-bus/ticket-mark-as-cod'] = "user-panel-bus/ticket_mark_as_cod";
$route['user-panel-bus/ticket-confirm-payment'] = "user-panel-bus/ticket_confirm_payment";
$route['user-panel-bus/get-trip-master-source-list'] = "user_panel_bus/get_trip_master_source_list";
// Bus Mobile API ---------------------------------------------------------------------------
$route['api-bus/ticket-booking-crone-alert'] = 'api-bus/ticket_booking_crone_alert';
//Manage Bus
$route['api-bus/bus-amenities-list'] = "api_bus/bus_amenities_list";
$route['api-bus/bus-amenities-list'] = "api_bus/bus_amenities_list";
$route['api-bus/bus-layout-list'] = "api_bus/bus_layout_list";
$route['api-bus/operator-bus-list'] = "api_bus/operator_bus_list";
$route['api-bus/operator-bus-add'] = "api_bus/operator_bus_add";
$route['api-bus/operator-bus-edit'] = "api_bus/operator_bus_edit";
//Manage Cancelation and Rescheduling
$route['api-bus/cancellation-charge-list'] = "api_bus/cancellation_charge_list";
$route['api-bus/cancellation-charge-add'] = "api_bus/cancellation_charge_add";
$route['api-bus/cancellation-charge-edit'] = "api_bus/cancellation_charge_edit";
$route['api-bus/cancellation-charge-delete'] = "api_bus/cancellation_charge_delete";
//Agents
$route['api-bus/agent-group-list'] = "api_bus/agent_group_list";
$route['api-bus/bus-operator-agent-active-list'] = "api_bus/bus_operator_agent_active_list";
$route['api-bus/bus-operator-agent-inactive-list'] = "api_bus/bus_operator_agent_inactive_list";
$route['api-bus/bus-operator-agent-add'] = "api_bus/bus_operator_agent_add";
$route['api-bus/bus-operator-agent-edit'] = "api_bus/bus_operator_agent_edit";
$route['api-bus/bus-operator-agent-activate'] = "api_bus/bus_operator_agent_activate";
$route['api-bus/bus-operator-agent-inactivate'] = "api_bus/bus_operator_agent_inactivate";
//Pickup Drop Points
$route['api-bus/bus-location-list'] = "api_bus/bus_location_list";
$route['api-bus/bus-pick-drop-point-list'] = "api_bus/bus_pick_drop_point_list";
$route['api-bus/point-remove-as-pickup'] = "api_bus/point_remove_as_pickup";
$route['api-bus/point-mark-as-pickup'] = "api_bus/point_mark_as_pickup";
$route['api-bus/point-remove-as-drop'] = "api_bus/point_remove_as_drop";
$route['api-bus/point-mark-as-drop'] = "api_bus/point_mark_as_drop";
$route['api-bus/bus-pick-drop-point-delete'] = "api_bus/bus_pick_drop_point_delete";
$route['api-bus/bus-pick-drop-point-add'] = "api_bus/bus_pick_drop_point_add";
$route['api-bus/bus-pick-drop-location-and-point-add'] = "api_bus/bus_pick_drop_location_and_point_add";
$route['api-bus/get-location-pickup-point'] = "api_bus/get_location_pickup_point";
//Bus user contact book
$route['api-bus/get-customers-bus-address-book'] = "api_bus/get_customers_bus_address_book";
$route['api-bus/add-customers-bus-address-book'] = "api_bus/add_customers_bus_address_book";
$route['api-bus/update-customers-bus-address-book'] = "api_bus/update_customers_bus_address_book";
$route['api-bus/delete-customers-bus-address-book'] = "api_bus/delete_customers_bus_address_book";
//Manage Trip Master
$route['api-bus/bus-trip-master-list'] = "api_bus/bus_trip_master_list";
$route['api-bus/bus-trip-master-details'] = "api_bus/bus_trip_master_details";
$route['api-bus/vehicle-type-name-seats-list'] = "api_bus/vehicle_type_name_seats_list";
$route['api-bus/get-locations-by-vehicle-type-id'] = "api_bus/get_locations_by_vehicle_type_id";
$route['api-bus/get-pickup-drop-points-by-locations-id'] = "api_bus/get_pickup_drop_points_by_locations_id";
$route['api-bus/bus-trip-master-add-details'] = "api_bus/bus_trip_master_add_details";
$route['api-bus/bus-trip-master-edit-details'] = "api_bus/bus_trip_master_edit_details";
$route['api-bus/bus-trip-location-add-details'] = "api_bus/bus_trip_location_add_details";
$route['api-bus/bus-trip-location-delete'] = "api_bus/bus_trip_location_delete";
$route['api-bus/bus-trip-master-delete'] = "api_bus/bus_trip_master_delete";
$route['api-bus/bus-trip-master-details-with-booking-list'] = "api_bus/bus_trip_master_details_with_booking_list";
$route['api-bus/bus-trip-master-disable'] = "api_bus/bus_trip_master_disable";
$route['api-bus/bus-trip-master-enable'] = "api_bus/bus_trip_master_enable";
//Trip Special Rate
$route['api-bus/bus-trip-special-rate-list'] = "api_bus/bus_trip_special_rate_list";
$route['api-bus/bus-trip-special-rate-edit-details'] = "api_bus/bus_trip_special_rate_edit_details";
//Driver
$route['api-bus/get-operator-vehicles-by-category'] = "api_bus/get_operator_vehicles_by_category";
$route['api-bus/permitted-cat-list'] = 'api_bus/permitted_cat_list';
$route['api-bus/driver-list'] = 'api_bus/driver_list';
$route['api-bus/inactive-driver-list'] = 'api_bus/inactive_driver_list';
$route['api-bus/driver-add'] = 'api_bus/driver_add';
$route['api-bus/active-driver'] = 'api_bus/active_driver';
$route['api-bus/inactive-driver'] = 'api_bus/inactive_driver';
$route['api-bus/driver-update'] = 'api_bus/driver_update';
$route['api-bus/driver-update_avatar'] = 'api_bus/driver_update_avatar';
$route['api-bus/get-bus-operator-profile'] = "api_bus/get_bus_operator_profile";
$route['api-bus/get-profile-bus-operator'] = "api_bus/get_profile_bus_operator";
$route['api-bus/upcoming-bus-trip-seller'] = "api_bus/upcoming_bus_trip_seller";
$route['api-bus/current-bus-trip-seller'] = "api_bus/current_bus_trip_seller";
$route['api-bus/previous-bus-trip-seller'] = "api_bus/previous_bus_trip_seller";
//bus workroom
$route['api-bus/get-global-workroom'] = 'api_bus/get_global_workroom';
$route['api-bus/get-bus-work-room'] = "api_bus/get_bus_work_room";
$route['api-bus/add-chat-bus-workroom'] = "api_bus/add_chat_bus_workroom";
$route['api-bus/add-review-comments-bus-workroom'] = "api_bus/add_review_comments_bus_workroom";
$route['api-bus/make-workroom-notification-read'] = 'api_bus/make_workroom_notification_read';
$route['user-panel-bus/seller-previous-trips'] = "user_panel_bus/seller_previous_trips";
$route['user-panel-bus/seller-previous-trips-filter'] = "user_panel_bus/seller_previous_trips_filter";
$route['user-panel-bus/seller-previous-trips-view-passengers'] = "user_panel_bus/selle_previous_trips_view_passengers";
$route['user-panel-bus/seller-previous-trips-view-passengers/:num'] = "user_panel_bus/selle_previous_trips_view_passengers/$1";
$route['user-panel-bus/bus-previous-trip-details-seller'] = "user_panel_bus/bus_previous_trip_details_seller";
$route['user-panel-bus/bus-work-room'] = "user_panel_bus/bus_work_room";
$route['user-panel-bus/add-bus-workroom-chat'] = "user_panel_bus/add_bus_workroom_chat";
$route['user-panel-bus/add-bus-workroom-rating'] = "user_panel_bus/add_bus_workroom_rating";
//bus account/withdrawals/E-Wallet
$route['user-panel-bus/bus-wallet'] = "user_panel_bus/bus_wallet";
$route['user-panel-bus/bus-account-history'] = "user_panel_bus/bus_account_history";
$route['user-panel-bus/my-scrow'] = "user_panel_bus/my_scrow";
$route['user-panel-bus/user-scrow-history'] = "user_panel_bus/user_scrow_history";
$route['user-panel-bus/bus-invoices-history'] = "user_panel_bus/bus_invoices_history";
$route['user-panel-bus/create-bus-withdraw-request'] = "user_panel_bus/create_bus_withdraw_request";
$route['user-panel-bus/withdraw-request-sent-bus'] = "user_panel_bus/withdraw_request_sent_bus";
$route['user-panel-bus/bus-withdrawals-request'] = "user_panel_bus/bus_withdrawals_request";
$route['user-panel-bus/bus-withdrawals-accepted'] = "user_panel_bus/bus_withdrawals_accepted";
$route['user-panel-bus/bus-withdrawals-rejected'] = "user_panel_bus/bus_withdrawals_rejected";
$route['user-panel-bus/bus-withdrawal-request-details'] = "user_panel_bus/bus_withdrawal_request_details";
//Bus account/withdrawals/E-Wallet/WebService 
$route['api-bus/bus-wallet'] = 'api_bus/bus_wallet';
$route['api-bus/bus-account-history'] = 'api_bus/bus_account_history';
$route['api-bus/bus-invoices-history'] = 'api_bus/bus_invoices_history';
$route['api-bus/create-bus-withdraw-request'] = 'api_bus/create_bus_withdraw_request';
$route['api-bus/withdraw-request-sent-bus'] = 'api_bus/withdraw_request_sent_bus';
$route['api-bus/bus-withdrawals-request-list'] = 'api_bus/bus_withdrawals_request_list';
$route['api-bus/bus-withdrawals-list'] = 'api_bus/bus_withdrawals_list';
$route['api-bus/bus-withdrawals-rejected-list'] = 'api_bus/bus_withdrawals_rejected_list';
$route['api-bus/bus-withdrawal-request-details'] = 'api_bus/bus_withdrawal_request_details';
$route['api-bus/get-customer-scrow-balance'] = 'api_bus/get_customer_scrow_balance';
$route['api-bus/get-customer-scrow-history'] = 'api_bus/get_customer_scrow_history';
//Incident
//$route['api-bus/register-new-bus-support-request'] = 'api_bus/register_new_bus_support_request';
//Ticket/Trip Cancellation
$route['user-panel-bus/seller-cancelled-tickets'] = "user_panel_bus/seller_cancelled_tickets";
$route['user-panel-bus/seller-cancelled-trips'] = "user_panel_bus/seller_cancelled_trips";
$route['user-panel-bus/seller-cancelled-trips-view-passengers'] = "user_panel_bus/seller_cancelled_trips_view_passengers";
$route['api-bus/get-seller-source-destination'] = 'api_bus/get_seller_source_destination';
$route['api-bus/get-buyer-source-destination'] = 'api_bus/get_buyer_source_destination';
//Seller (search /book/payment/cancel) trip 
$route['api-bus/seller-bus-trip-search'] = "api_bus/seller_bus_trip_search";
$route['api-bus/get-pickup-drop-location'] = 'api_bus/get_pickup_drop_location';
$route['api-bus/bus-trip-booking-seller'] = "api_bus/bus_trip_booking_seller";
$route['api-bus/ticket-confirm-payment-operator'] = "api_bus/ticket_confirm_payment_operator";
$route['api-bus/single-ticket-confirm-payment-operator'] = "api_bus/single_ticket_confirm_payment_operator";
$route['api-bus/bus-operator-cancellation-charges-list'] = "api_bus/bus_operator_cancellation_charges_list";
$route['api-bus/seller-cancel-ticket'] = "api_bus/seller_cancel_ticket";
$route['api-bus/cancel-bus-trip-seller'] = "api_bus/cancel_bus_trip_seller";
$route['api-bus/seller-cancelled-tickets'] = "api_bus/seller_cancelled_tickets";
$route['api-bus/seller-trips-all-filter'] = "api_bus/seller_trips_all_filter";
//buyer (search /book/payment/cancel) trip 
$route['api-bus/buyer-bus-trip-search'] = "api_bus/buyer_bus_trip_search";
$route['api-bus/buyer-bus-trip-search-filter'] = "api_bus/buyer_bus_trip_search_filter";
$route['api-bus/buyer-bus-trip-booking'] = "api_bus/buyer_bus_trip_booking";
$route['api-bus/buyer-cancel-ticket'] = "api_bus/buyer_cancel_ticket";
$route['api-bus/buyer-upcoming-current-previous-with-filter'] = "api_bus/buyer_upcoming_current_previous_with_filter";
$route['api-bus/buyer-trips-all-filter'] = "api_bus/buyer_trips_all_filter";
$route['api-bus/buyer-ticket-mark-as-cod'] = "api_bus/buyer_ticket_mark_as_cod";
$route['api-bus/buyer-single-ticket-mark-as-cod'] = "api_bus/buyer_single_ticket_mark_as_cod";
$route['api-bus/buyer-ticket-stripe-payment'] = "api_bus/buyer_ticket_stripe_payment";
$route['api-bus/buyer-single-ticket-stripe-payment'] = "api_bus/buyer_single_ticket_stripe_payment";
$route['payment-bus/payment-by-mtn-from-mobile'] = "payment_bus/payment_by_mtn_from_mobile";
$route['payment-bus/single-payment-by-mtn-from-mobile'] = "payment_bus/single_payment_by_mtn_from_mobile";
$route['api-bus/cancel-ticket-buyer'] = "api_bus/cancel_ticket_buyer";
$route['api-bus/bus-operator-cancellation-charges-with-return-list'] = "api_bus/bus_operator_cancellation_charges_with_return_list";
$route['api-bus/orange-payment-process'] = "api_bus/orange_payment_process";
$route['api-bus/single-ticket-orange-payment-process'] = "api_bus/single_ticket_orange_payment_process";
//seller ticket payment
$route['user-panel-bus/seller-ticket-orange-payment'] = "user-panel-bus/seller_ticket_orange_payment";
$route['user-panel-bus/seller-orange-payment-process'] = "user-panel-bus/seller_orange_payment_process";
$route['user-panel-bus/seller-single-ticket-orange-payment'] = "user-panel-bus/seller_single_ticket_orange_payment";
$route['user-panel-bus/seller-single-orange-payment-process'] = "user-panel-bus/seller_single_orange_payment_process";
$route['user-panel-bus/seller-mtn'] = "user-panel-bus/seller_payment_by_mtn";
$route['user-panel-bus/seller-mtn-single'] = "user-panel-bus/seller_payment_by_mtn_single";
//Seller commission refund request
$route['user-panel-bus/commission-refund-request'] = "user-panel-bus/commission_refund_request";
$route['api-bus/commission-refund-request'] = "api_bus/commission_refund_request";
//Driver App
$route['driver_api/driver-trips-all-filter'] = "driver_api/driver_trips_all_filter";
$route['driver_api/driver-trip-status-update'] = 'driver_api/driver_trip_status_update';
$route['api-bus/ticket-booking-cleanup'] = "api_bus/ticket_booking_cleanup";

//Start Laundry Service Web-----------------------------------------------------------------------------------------
	$route['user-panel-laundry/dashboard-laundry'] = "user_panel_laundry/dashboard_laundry";
	$route['user-panel-laundry/dashboard-laundry-users'] = "user_panel_laundry/dashboard_laundry_users";
	$route['user-panel-laundry/today-laundry-orders'] = "user_panel_laundry/today_laundry_orders";
	$route['user-panel-laundry/current-week-laundry-orders'] = "user_panel_laundry/current_week_laundry_orders";
	$route['user-panel-laundry/current-month-laundry-orders'] = "user_panel_laundry/current_month_laundry_orders";
	$route['user-panel-laundry/till-date-laundry-orders'] = "user_panel_laundry/till_date_laundry_orders";
	$route['user-panel-laundry/today-laundry-sales'] = "user_panel_laundry/today_laundry_sales";
	$route['user-panel-laundry/current-week-laundry-sales'] = "user_panel_laundry/current_week_laundry_sales";
	$route['user-panel-laundry/current-month-laundry-sales'] = "user_panel_laundry/current_month_laundry_sales";
	$route['user-panel-laundry/till-date-laundry-sales'] = "user_panel_laundry/till_date_laundry_sales";
	$route['user-panel-laundry/best-customers'] = "user_panel_laundry/best_customers";
	$route['user-panel-laundry/laundry-customer-consumption-history'] = "user_panel_laundry/laundry_customer_consumption_history";
	//Operator basic profile
	//User Profile
	$route['user-panel-laundry/user-profile'] = "user_panel_laundry/user_profile";
	$route['user-panel-laundry/basic-profile/:num'] = "user_panel_laundry/edit_basic_profile_view";
	$route['user-panel-laundry/basic-profile/update'] = "user_panel_laundry/update_basic_profile";
	$route['user-panel-laundry/basic-profile/get-state-by-country-id'] = "user_panel_laundry/get_state_by_country_id";
	$route['user-panel-laundry/basic-profile/get-cities-by-state-id'] = "user_panel_laundry/get_city_by_state_id";
	// Payment
	$route['user-panel-laundry/payment-methods'] = "user_panel_laundry/payment_method_list_view";
	$route['user-panel-laundry/payment-method/add'] = "user_panel_laundry/add_payment_method_view";
	$route['user-panel-laundry/add-payment-method'] = "user_panel_laundry/add_payment_method";
	$route['user-panel-laundry/payment-method/:num'] = "user_panel_laundry/edit_payment_method_view/$1";
	$route['user-panel-laundry/edit-payment-method'] = "user_panel_laundry/edit_payment_method";
	// education
	$route['user-panel-laundry/educations'] = "user_panel_laundry/education_list_view";
	$route['user-panel-laundry/education/add'] = "user_panel_laundry/add_education_view";
	$route['user-panel-laundry/add-education'] = "user_panel_laundry/add_education";
	$route['user-panel-laundry/education/:num'] = "user_panel_laundry/edit_education_view/$1";
	$route['user-panel-laundry/edit-education'] = "user_panel_laundry/edit_education";
	$route['user-panel-laundry/delete-education'] = "user_panel_laundry/delete_education";
	//  experience
	$route['user-panel-laundry/experiences'] = "user_panel_laundry/experience_list_view";
	$route['user-panel-laundry/experience/add'] = "user_panel_laundry/add_experience_view";
	$route['user-panel-laundry/add-experience'] = "user_panel_laundry/add_experience";
	$route['user-panel-laundry/experience/:num'] = "user_panel_laundry/edit_experience_view/$1";
	$route['user-panel-laundry/edit-experience'] = "user_panel_laundry/edit_experience";
	// other experience
	$route['user-panel-laundry/other-experience/add'] = "user_panel_laundry/add_other_experience_view";
	$route['user-panel-laundry/add-other-experience'] = "user_panel_laundry/add_other_experience";
	$route['user-panel-laundry/other-experiences'] = "user_panel_laundry/other_experience_list_view";
	$route['user-panel-laundry/other-experience/:num'] = "user_panel_laundry/edit_other_experience_view/$1";
	$route['user-panel-laundry/edit-other-experience'] = "user_panel_laundry/edit_other_experience";
	// skill
	$route['user-panel-laundry/skill/add'] = "user_panel_laundry/add_skill_view";
	$route['user-panel-laundry/add-skill'] = "user_panel_laundry/add_skill";
	$route['user-panel-laundry/skills'] = "user_panel_laundry/skill_list_view";
	$route['user-panel-laundry/skill'] = "user_panel_laundry/edit_skill_view";
	$route['user-panel-laundry/edit-skill'] = "user_panel_laundry/edit_skill";
	$route['user-panel-laundry/skill/get-skills-by-category-id'] = "user_panel_laundry/get_skills_by_category_id";
	// Portfolio
	$route['user-panel-laundry/portfolio/add'] = "user_panel_laundry/add_portfolio_view";
	$route['user-panel-laundry/add-portfolio'] = "user_panel_laundry/add_portfolio";
	$route['user-panel-laundry/portfolios'] = "user_panel_laundry/portfolio_list_view";
	$route['user-panel-laundry/portfolios/:num'] = "user_panel_laundry/edit_portfolio_view/$1";
	$route['user-panel-laundry/edit-portfolio'] = "user_panel_laundry/edit_portfolio";
	$route['user-panel-laundry/portfolio/get-category-by-cat-type-id'] = "user_panel_laundry/get_category_by_cat_type_id";
	$route['user-panel-laundry/portfolio/get-subcategories-by-category-id'] = "user_panel_laundry/get_subcategories_by_category_id";
	// Language
	$route['user-panel-laundry/get-languages-master'] = "user_panel_laundry/get_languages_master";
	$route['user-panel-laundry/update-languages'] = "user_panel_laundry/update_languages";
	// Overview
	$route['user-panel-laundry/update-overview'] = "user_panel_laundry/update_overview";
	// Document
	$route['user-panel-laundry/document/add'] = "user_panel_laundry/add_document_view";
	$route['user-panel-laundry/add-document'] = "user_panel_laundry/add_document";
	$route['user-panel-laundry/documents'] = "user_panel_laundry/document_list_view";
	$route['user-panel-laundry/document/:num'] = "user_panel_laundry/edit_document_view/$1";
	$route['user-panel-laundry/edit-document'] = "user_panel_laundry/edit_document";
	// Change Password
	$route['user-panel-laundry/change-password'] = "user_panel_laundry/change_password";
	$route['user-panel-laundry/update-change'] = "user_panel_laundry/update_password";
	//Address Book
	$route['user-panel-laundry/address-book-list'] = "user_panel_laundry/address_book_list";
	$route['user-panel-laundry/address-book/add'] = "user_panel_laundry/add_address_book_view";
	$route['user-panel-laundry/add-address-book'] = "user_panel_laundry/add_address_to_book";
	$route['user-panel-laundry/address-book/:num'] = "user_panel_laundry/edit_address_book_view/$1";
	$route['user-panel-laundry/edit-address-book'] = "user_panel_laundry/edit_address";
	$route['user-panel-laundry/delete-address'] = "user_panel_laundry/delete_address";
	// Laundry Provider Profile
	$route['user-panel-laundry/provider-profile'] = "user_panel_laundry/provider_profile";
	$route['user-panel-laundry/create-laundry-provider-profile'] = "user_panel_laundry/create_laundry_provider_profile";
	$route['user-panel-laundry/get-state-by-country-id'] = "user_panel_laundry/get_state_by_country_id";
	$route['user-panel-laundry/get-cities-by-state-id'] = "user_panel_laundry/get_city_by_state_id";
	$route['user-panel-laundry/edit-provider-profile'] = "user_panel_laundry/edit_provider_profile";
	$route['user-panel-laundry/update-provider-profile'] = "user_panel_laundry/update_provider_profile";
	// Laundry Provider Documents
	$route['user-panel-laundry/edit-provider-document'] = "user_panel_laundry/edit_provider_document";
	$route['user-panel-laundry/add-provider-document'] = "user_panel_laundry/add_provider_document";
	$route['user-panel-laundry/provider-document-delete'] = "user_panel_laundry/provider_document_delete";
	// Laundry Provider Business Photos
	$route['user-panel-laundry/business-photos'] = "user_panel_laundry/business_photos";
	$route['user-panel-laundry/laundry-provider-business-photos-upload'] = "user_panel_laundry/laundry_provider_business_photos_upload";
	$route['user-panel-laundry/laundry-provider-business-photos-delete'] = "user_panel_laundry/laundry_provider_business_photos_delete";
	// Laundry Charges
	$route['user-panel-laundry/laundry-charges-list'] = "user_panel_laundry/laundry_charges_list";
	$route['user-panel-laundry/get-laundry-sub-category'] = "user_panel_laundry/get_laundry_sub_category";
	$route['user-panel-laundry/laundry-charges-add-details'] = "user_panel_laundry/laundry_charges_add_details";
	$route['user-panel-laundry/laundry-charges-delete'] = "user_panel_laundry/laundry_charges_delete";
	$route['user-panel-laundry/update-laundry-charge-amount'] = "user_panel_laundry/update_laundry_charge_amount";
	// Laundry cancellation Charges
	$route['user-panel-laundry/laundry-provider-cancellation-charges-list'] = "user_panel_laundry/laundry_provider_cancellation_charges_list";
	$route['user-panel-laundry/laundry-cancellation-charges-add'] = "user_panel_laundry/laundry_cancellation_charges_add";
	$route['user-panel-laundry/laundry-cancellation-charges-delete'] = "user_panel_laundry/laundry_cancellation_charges_delete";
	$route['user-panel-laundry/update-laundry-cancellation-percentage'] = "user_panel_laundry/update_laundry_cancellation_percentage";
	// Favourite Deliverer For Laundry Provider
	$route['user-panel-laundry/favourite-deliverers'] = "user_panel_laundry/favourite_deliverers";
	$route['user-panel-laundry/remove-deliverer-from-favourite'] = "user_panel_laundry/remove_deliverer_from_favourite";
	$route['user-panel-laundry/search-deliverer-details-view'] = "user_panel_laundry/search_deliverer_details_view";
	$route['user-panel-laundry/search-for-deliverers'] = "user_panel_laundry/search_for_deliverers";
	$route['user-panel-laundry/make-deliverer-favourite'] = "user_panel_laundry/make_deliverer_favourite";
	// Favourite Provider For Laundry Buyer
	$route['user-panel-laundry/favourite-provider'] = "user_panel_laundry/favourite_provider";
	$route['user-panel-laundry/search-for-provider'] = "user_panel_laundry/search_for_provider";
	$route['user-panel-laundry/make-provider-favourite'] = "user_panel_laundry/make_provider_favourite";
	$route['user-panel-laundry/remove-provider-from-favourite'] = "user_panel_laundry/remove_provider_from_favourite";
	$route['user-panel-laundry/search-provider-details-view'] = "user_panel_laundry/search_provider_details_view";
	//Pending Payment Buyer
	$route['user-panel-laundry/customer-laundry-pending-payments'] = "user_panel_laundry/customer_laundry_pending_payments";
	// Suggession and support
	$route['user-panel-laundry/get-support-list'] = "user_panel_laundry/get_support_list";
	$route['user-panel-laundry/get-support'] = "user_panel_laundry/get_support";
	$route['user-panel-laundry/add-support-details'] = "user_panel_laundry/add_support_details";
	$route['user-panel-laundry/view-support-reply-details/(:num)'] = "user_panel_laundry/view_support_reply_details/$1";
	// Notifications
	$route['user-panel-laundry/user-notifications'] = "user_panel_laundry/user_notifications";
	$route['user-panel-laundry/notification-detail'] = "user_panel_laundry/notification_detail";
	$route['user-panel-laundry/notifications-read'] = "user_panel_laundry/notifications_reads";
	// Laundry account/withdrawals/E-Wallet
	$route['user-panel-laundry/laundry-wallet'] = "user_panel_laundry/laundry_wallet";
	$route['user-panel-laundry/laundry-account-history'] = "user_panel_laundry/laundry_account_history";
	$route['user-panel-laundry/laundry-invoices-history'] = "user_panel_laundry/laundry_invoices_history";
	$route['user-panel-laundry/my-scrow'] = "user_panel_laundry/my_scrow";
	$route['user-panel-laundry/user-scrow-history'] = "user_panel_laundry/user_scrow_history";
	$route['user-panel-laundry/laundry-withdrawals-request'] = "user_panel_laundry/laundry_withdrawals_request";
	$route['user-panel-laundry/laundry-withdrawals-accepted'] = "user_panel_laundry/laundry_withdrawals_accepted";
	$route['user-panel-laundry/laundry-withdrawals-rejected'] = "user_panel_laundry/laundry_withdrawals_rejected";
	$route['user-panel-laundry/laundry-withdrawal-request-details'] = "user_panel_laundry/laundry_withdrawal_request_details";
	$route['user-panel-laundry/create-laundry-withdraw-request'] = "user_panel_laundry/create_laundry_withdraw_request";
	$route['user-panel-laundry/withdraw-request-sent-laundry'] = "user_panel_laundry/withdraw_request_sent_laundry";
	//Provider Create Laundry Booking---------------------------------------------------
	$route['user-panel-laundry/provider-create-laundry-bookings'] = "user_panel_laundry/provider_create_laundry_bookings";
	$route['user-panel-laundry/provider-sub-category-append'] = "user_panel_laundry/provider_sub_category_append";
	$route['user-panel-laundry/get-cloth-details-provider'] = "user_panel_laundry/get_cloth_details_provider";
	$route['user-panel-laundry/update-provider-cart-price'] = "user_panel_laundry/update_provider_cart_price";
	$route['user-panel-laundry/provider-laundry-booking'] = "user_panel_laundry/provider_laundry_booking";
	$route['user-panel-laundry/set-customers-info'] = "user_panel_laundry/set_customers_info";
	$route['user-panel-laundry/accepted-laundry-bookings'] = "user_panel_laundry/accepted_laundry_bookings";
	$route['user-panel-laundry/mark-is-drop-view'] = "user_panel_laundry/mark_is_drop_view";
	$route['user-panel-laundry/mark-is-drop-process'] = "user_panel_laundry/mark_is_drop_process";
	$route['user-panel-laundry/laundry-booking-payment-by-provider'] = "user_panel_laundry/laundry_booking_payment_by_provider";
	$route['user-panel-laundry/laundry-booking-payment-by-provider/(:num)'] = "user_panel_laundry/laundry_booking_payment_by_provider/$1";
	$route['user-panel-laundry/laundry-booking-mark-complete-paid'] = "user_panel_laundry/laundry_booking_mark_complete_paid";
	$route['user-panel-laundry/provider-confirm-payment-stripe'] = "user_panel_laundry/provider_confirm_payment_stripe";
	$route['user-panel-laundry/provider-confirm-payment-mtn'] = "user_panel_laundry/provider_confirm_payment_mtn";
	$route['user-panel-laundry/provider-confirm-payment-orange'] = "user_panel_laundry/provider_confirm_payment_orange";
	$route['user-panel-laundry/payment-by-provider-orange-process'] = "user_panel_laundry/payment_by_provider_orange_process";
	$route['user-panel-laundry/laundry-booking-mark-in-progress'] = "user_panel_laundry/laundry_booking_mark_in_progress";
	$route['user-panel-laundry/in-progress-laundry-bookings'] = "user_panel_laundry/in_progress_laundry_bookings";
	$route['user-panel-laundry/create-drop-booking'] = "user_panel_laundry/create_drop_booking";
	$route['user-panel-laundry/laundry-booking-mark-completed'] = "user_panel_laundry/laundry_booking_mark_completed";
	$route['user-panel-laundry/completed-laundry-bookings'] = "user_panel_laundry/completed_laundry_bookings";
	$route['user-panel-laundry/cancelled-laundry-bookings'] = "user_panel_laundry/cancelled_laundry_bookings";
	$route['user-panel-laundry/provider-laundry-pending-invoices'] = "user_panel_laundry/provider_laundry_pending_invoices";
	$route['user-panel-laundry/provider-laundry-complete-payment'] = "user_panel_laundry/provider_laundry_complete_payment";
	//Provider Create Laundry Booking---------------------------------------------------
	// Laundry Customer Bookings
	$route['user-panel-laundry/customer-accepted-laundry-bookings'] = "user_panel_laundry/customer_accepted_laundry_bookings";
	$route['user-panel-laundry/customer-mark-is-drop-view'] = "user_panel_laundry/customer_mark_is_drop_view";
	$route['user-panel-laundry/customer-mark-is-drop-process'] = "user_panel_laundry/customer_mark_is_drop_process";
	$route['user-panel-laundry/customer-in-progress-laundry-bookings'] = "user_panel_laundry/customer_in_progress_laundry_bookings";
	$route['user-panel-laundry/customer-completed-laundry-bookings'] = "user_panel_laundry/customer_completed_laundry_bookings";
	$route['user-panel-laundry/customer-cancelled-laundry-bookings'] = "user_panel_laundry/customer_cancelled_laundry_bookings";
	// Driver Chat Laundry
	$route['user-panel-laundry/laundry-chat-room'] = "user_panel_laundry/chatroom";
	$route['user-panel-laundry/add-order-chat'] = "user_panel_laundry/add_order_chat";
	// Laundry Provider Dashboard
	$route['user-panel-laundry/dashboard-laundry'] = "user_panel_laundry/dashboard_laundry";
	// Laundry Booking Cancellation by Customer
	$route['user-panel-laundry/laundry-booking-cancel'] = 'user_panel_laundry/laundry_booking_cancel';
	//Laundry Booking Customer
	$route['user-panel-laundry/customer-create-laundry-bookings'] = "user_panel_laundry/booking_laundry_ui";
	$route['user-panel-laundry/add-temp-cloth-count'] = "user_panel_laundry/add_temp_cloth_count";
	$route['user-panel-laundry/subs-temp-cloth-count'] = "user_panel_laundry/subs_temp_cloth_count";
	$route['user-panel-laundry/get-cloth-details'] = "user_panel_laundry/get_cloth_details";
	$route['user-panel-laundry/get-laundry-provider-append'] = "user_panel_laundry/get_laundry_provider_append";
	$route['user-panel-laundry/update-my-cart-price'] = "user_panel_laundry/update_my_cart_price";
	$route['user-panel-laundry/laundry-booking'] = "user_panel_laundry/laundry_booking";
	$route['user-panel-laundry/get-laundry-sub-category-append'] = "user_panel_laundry/get_laundry_sub_category_append";
	//Laundry Payment By Customer
	$route['user-panel-laundry/laundry-booking-payment-by-cutomer'] = "user_panel_laundry/laundry_booking_payment_by_cutomer";
	$route['user-panel-laundry/laundry-booking-payment-by-cutomer/(:num)'] = "user_panel_laundry/laundry_booking_payment_by_cutomer/$1";
	$route['user-panel-laundry/laundry-mark-cod-by-customer'] = "user_panel_laundry/laundry_mark_cod_by_customer";
	$route['user-panel-laundry/confirm-payment-stripe'] = "user_panel_laundry/confirm_payment_stripe";
	$route['user-panel-laundry/customer-mtn'] = "user_panel_laundry/payment_by_customer_mtn";
	$route['user-panel-laundry/payment-by-customer-orange'] = "user_panel_laundry/payment_by_customer_orange";
	$route['user-panel-laundry/payment-by-customer-orange-process'] = "user_panel_laundry/payment_by_customer_orange_process";
	//Laundry Workroom
	$route['user-panel-laundry/laundry-work-room/(:num)'] = "user_panel_laundry/laundry_work_room/$1";
	$route['user-panel-laundry/laundry-work-room'] = "user_panel_laundry/laundry_work_room";
	$route['user-panel-laundry/add-laundry-workroom-chat'] = "user_panel_laundry/add_laundry_workroom_chat";
	$route['user-panel-laundry/add-laundry-workroom-rating'] = "user_panel_laundry/add_laundry_workroom_rating";
	$route['user-panel-laundry/laundry-workroom'] = "user_panel_laundry/laundry_workroom";
	//Laundry Claims Provider
	$route['user-panel-laundry/update-laundry-claim-delay'] = "user_panel_laundry/update_laundry_claim_delay";
	$route['user-panel-laundry/provider-laundry-claims'] = "user_panel_laundry/provider_laundry_claims";
	$route['user-panel-laundry/provider-claim-details'] = "user_panel_laundry/provider_claim_details";
	$route['user-panel-laundry/provider-claim-response'] = "user_panel_laundry/provider_claim_response";
	$route['user-panel-laundry/provider-laundry-claims-closed'] = "user_panel_laundry/provider_laundry_claims_closed";
	$route['user-panel-laundry/provider-closed-claim-details'] = "user_panel_laundry/provider_closed_claim_details";
	//Laundry Claims Customer
	$route['user-panel-laundry/customer-mark-claim'] = "user_panel_laundry/customer_mark_claim";
	$route['user-panel-laundry/mark-claim-process'] = "user_panel_laundry/mark_claim_process";
	$route['user-panel-laundry/customer-laundry-claims'] = "user_panel_laundry/customer_laundry_claims";
	$route['user-panel-laundry/customer-claim-details'] = "user_panel_laundry/customer_claim_details";
	$route['user-panel-laundry/customer-laundry-claims-closed'] = "user_panel_laundry/customer_laundry_claims_closed";
	$route['user-panel-laundry/customer-closed-claim-details'] = "user_panel_laundry/customer_closed_claim_details";
	$route['user-panel-laundry/customer-reopen-claim'] = "user_panel_laundry/customer_reopen_claim";
//End Laundry Service Web-------------------------------------------------------------------------------------------

//Start Laundry Service Mobile--------------------------------------------------------------------------------------
	//Provider Profile
	$route['api-laundry/get-laundry-operator-profile'] = "api_laundry/get_laundry_operator_profile";
	$route['api-laundry/register-laundry-provider'] = "api_laundry/register_laundry_provider";
	$route['api-laundry/update-laundry-provider'] = "api_laundry/update_laundry_provider";
	$route['api-laundry/update-laundry-provider-avatar'] = "api_laundry/update_laundry_provider_avatar";
	$route['api-laundry/update-laundry-provider-cover'] = "api_laundry/update_laundry_provider_cover";
	//Provider Business Photos
	$route['api-laundry/get-laundry-provider-business-photos']='api_laundry/get_laundry_provider_business_photos';
	$route['api-laundry/delete-laundry-provider-business-photo']='api_laundry/delete_laundry_provider_business_photo';
	$route['api-laundry/upload-laundry-provider-business-photo']='api_laundry/upload_laundry_provider_business_photo';
	//Provider Document
	$route['api-laundry/get-laundry-provider-document']='api_laundry/get_laundry_provider_document';
	$route['api-laundry/add-laundry-provider-document']='api_laundry/add_laundry_provider_document';
	$route['api-laundry/delete-laundry-provider-document']='api_laundry/delete_laundry_provider_document';
	//Laundry charges
	$route['api-laundry/laundry-charges-list'] = "api_laundry/laundry_charges_list";
	$route['api-laundry/laundry-charges-category-list'] = "api_laundry/laundry_charges_category_list";
	$route['api-laundry/laundry-charges-sub-category-list'] = "api_laundry/laundry_charges_sub_category_list";
	$route['api-laundry/laundry-charges-add-details'] = "api_laundry/laundry_charges_add_details";
	$route['api-laundry/update-laundry-charge-amount'] = "api_laundry/update_laundry_charge_amount";
	$route['api-laundry/laundry-charges-delete'] = "api_laundry/laundry_charges_delete";
	//Laundry cancellation Charges
	$route['api-laundry/laundry-provider-cancellation-charges-list'] = "api_laundry/laundry_provider_cancellation_charges_list";
	$route['api-laundry/laundry-cancellation-charges-add'] = "api_laundry/laundry_cancellation_charges_add";
	$route['api-laundry/update-laundry-cancellation-percentage'] = "api_laundry/update_laundry_cancellation_percentage";
	$route['api-laundry/laundry-cancellation-charges-delete'] = "api_laundry/laundry_cancellation_charges_delete";
	//Favourite Provider For Laundry Buyer
	$route['api-laundry/favourite-provider-list'] = 'api_laundry/get_favourite_provider_list';
	$route['api-laundry/make-provider-favourite'] = 'api_laundry/register_new_favourite_provider';
	$route['api-laundry/remove-favourite-provider'] = 'api_laundry/remove_favourite_provider';
	//Laundry Customer Bookings
	$route['api-laundry/customer-accepted-laundry-bookings'] = "api_laundry/customer_accepted_laundry_bookings";
	$route['api-laundry/customer-in-progress-laundry-bookings'] = "api_laundry/customer_in_progress_laundry_bookings";
	$route['api-laundry/customer-completed-laundry-bookings'] = "api_laundry/customer_completed_laundry_bookings";
	$route['api-laundry/customer-cancelled-laundry-bookings'] = "api_laundry/customer_cancelled_laundry_bookings";
	//Laundry Provider Orders
	$route['api-laundry/provider-accepted-laundry-bookings'] = "api_laundry/provider_accepted_laundry_bookings";
	$route['api-laundry/provider-in-progress-laundry-bookings'] = "api_laundry/provider_in_progress_laundry_bookings";
	$route['api-laundry/provider-completed-laundry-bookings'] = "api_laundry/provider_completed_laundry_bookings";
	$route['api-laundry/provider-cancelled-laundry-bookings'] = "api_laundry/provider_cancelled_laundry_bookings";
	$route['api-laundry/laundry-bookings-list'] = "api_laundry/laundry_bookings_list";
	//Laundry Workroom
	$route['api-laundry/get-laundry-workroom'] = "api_laundry/get_laundry_workroom";
	$route['api-laundry/add-chat-laundry-workroom'] = "api_laundry/add_chat_laundry_workroom";
	$route['api-laundry/add-review-comments-laundry-workroom'] = "api_laundry/add_review_comments_laundry_workroom";
	$route['api-laundry/make-workroom-notification-read'] = 'api_laundry/make_workroom_notification_read';
	//Laundry Booking Cancellation by Customer
	$route['api-laundry/laundry-booking-cancel'] = 'api_laundry/laundry_booking_cancel';
	//Create Laundry Booking by Customer
	$route['api-laundry/laundry-provider-list'] = "api_laundry/laundry_provider_list";
	$route['api-laundry/laundry-booking'] = "api_laundry/laundry_booking";
	//Laundry Cleanup
	$route['api-laundry/laundry-booking-cleanup'] = "api_laundry/laundry_booking_cleanup";
	//Laundry Provider Order
	$route['api-laundry/laundry-booking-mark-complete-paid'] = "api_laundry/laundry_booking_mark_complete_paid";

	//api laundry provider booking--------------------------------------------------------------------------------------
	$route['api-laundry/get-walkin-customers'] = "api_laundry/get_walkin_customers";
	$route['api-laundry/laundry-sub-category-list-provider'] = "api_laundry/laundry_sub_category_list_provider";
	$route['api-laundry/laundry-pickup-cart-chareges-list-provider'] = "api_laundry/laundry_pickup_cart_chareges_list_provider";
	$route['api-laundry/provider-laundry-booking'] = "api_laundry/provider_laundry_booking";
	//api laundry provider booking--------------------------------------------------------------------------------------
	//api laundry payment by customer------------------------------------------------------------
	$route['api-laundry/payment-by-customer-mtn'] = "api_laundry/payment_by_customer_mtn";
	$route['api-laundry/payment-by-customer-stripe'] = "api_laundry/payment_by_customer_stripe";
	$route['api-laundry/payment-by-customer-orange'] = "api_laundry/payment_by_customer_orange";
	$route['api-laundry/payment-by-customer-cod'] = "api_laundry/payment_by_customer_cod";
	//api laundry payment by customer------------------------------------------------------------
	//api laundry payment by provider------------------------------------------------------------
	$route['api-laundry/laundry-provider-booking-mark-complete-paid'] = "api_laundry/laundry_provider_booking_mark_complete_paid";
	$route['api-laundry/payment-by-provider-stripe'] = "api_laundry/payment_by_provider_stripe";
	$route['api-laundry/payment-by-provider-mtn'] = "api_laundry/payment_by_provider_mtn";
	$route['api-laundry/payment-by-provider-orange'] = "api_laundry/payment_by_provider_orange";
	//api laundry payment by provider------------------------------------------------------------
	//api laundry mark as accepted in progress completed ----------------------------------------
	$route['api-laundry/laundry-booking-mark-in-progress'] = "api_laundry/laundry_booking_mark_in_progress";
	$route['api-laundry/laundry-booking-mark-completed'] = "api_laundry/laundry_booking_mark_completed";
	$route['api-laundry/laundry-mark-is-drop'] = "api_laundry/laundry_mark_is_drop";
	$route['api-laundry/create-drop-booking'] = "api_laundry/create_drop_booking";
	//api laundry mark as accepted in progress completed ----------------------------------------
	$route['api-laundry/laundry-pending-payments-invoices'] = "api_laundry/laundry_pending_payments_invoices";

	//Laundry Claims Provider
	$route['api-laundry/provider-laundry-claims'] = "api_laundry/provider_laundry_claims";
	$route['api-laundry/provider-claim-response'] = "api_laundry/provider_claim_response";
	$route['api-laundry/provider-laundry-claims-closed'] = "api_laundry/provider_laundry_claims_closed";
	$route['api-laundry/update-laundry-claim-delay'] = "api_laundry/update_laundry_claim_delay";
	//Laundry Claims Customer
	$route['api-laundry/mark-claim-process'] = "api_laundry/mark_claim_process";
	$route['api-laundry/customer-laundry-claims'] = "api_laundry/customer_laundry_claims";
	$route['api-laundry/customer-claim-details'] = "api_laundry/customer_claim_details";
	$route['api-laundry/customer-laundry-claims-closed'] = "api_laundry/customer_laundry_claims_closed";
	$route['api-laundry/customer-reopen-claim'] = "api_laundry/customer_reopen_claim";
//End Laundry Service Mobile----------------------------------------------------------------------------------------

//Start Laundry Admin Panel-----------------------------------------------------------------------------------------
	//Laundry Main & Sub Category
	$route['admin/laundry-categories'] = 'laundry_categories/index';
	$route['admin/laundry-categories-add'] = 'laundry_categories/add';
	$route['admin/laundry-categories-register'] = 'laundry_categories/register';
	$route['admin/laundry-categories/edit/(:any)'] = 'laundry_categories/edit/$1';
	$route['admin/laundry-categories/update'] = 'laundry_categories/update';
	$route['admin/laundry-categories/activate'] = 'laundry_categories/active_sub_category';
	$route['laundry-categories/inactivate'] = 'laundry_categories/inactive_sub_category';
	$route['admin/laundry-main-categories-edit'] = 'laundry_categories/main_category_edit';
	$route['admin/laundry-main-categories/update'] = 'laundry_categories/main_category_update';
	//Advance Payment and Gonagoo Commission
	// advance payment Bus
	$route['admin/advance-payment-laundry'] = 'advance_payment_laundry/index';
	$route['admin/advance-payment-laundry/add'] = 'advance_payment_laundry/add';
	$route['admin/advance-payment-laundry/register'] = 'advance_payment_laundry/register';
	$route['admin/advance-payment-laundry/edit/(:num)'] = 'advance_payment_laundry/edit/$1';
	$route['admin/advance-payment-laundry/update'] = 'advance_payment_laundry/update';
	$route['admin/advance-payment-laundry/delete'] = 'advance_payment_laundry/delete';

	$route['admin/recently-current-trips'] = 'administration/recently_current_trips';
	$route['admin/recently-upcoming-trips'] = 'administration/recently_upcoming_trips';
	$route['admin/recently-previous-trips'] = 'administration/recently_previous_trips';
	$route['admin/recently-cancelled-trips'] = 'administration/recently_cancelled_trips';

	$route['admin/user-wise-bus-dashboard'] = 'administration/user_wise_bus_dashboard';
	$route['admin/user-wise-laundry-dashboard'] = 'administration/user_wise_laundry_dashboard';
	
	$route['admin/dashboard-bus-current-trips-operator'] = 'administration/dashboard_bus_current_trips_operator';
	$route['admin/dashboard-bus-upcoming-trips-operator'] = 'administration/dashboard_bus_upcoming_trips_operator';
	$route['admin/dashboard-bus-previous-trips-operator'] = 'administration/dashboard_bus_previous_trips_operator';
	$route['admin/dashboard-bus-completed-trips-operator'] = 'administration/dashboard_bus_completed_trips_operator';
	
	$route['admin/dashboard-bus-current-trips-customer'] = 'administration/dashboard_bus_current_trips_customer';
	$route['admin/dashboard-bus-upcoming-trips-customer'] = 'administration/dashboard_bus_upcoming_trips_customer';
	$route['admin/dashboard-bus-previous-trips-customer'] = 'administration/dashboard_bus_previous_trips_customer';
	$route['admin/dashboard-bus-completed-trips-customer'] = 'administration/dashboard_bus_completed_trips_customer';
	$route['admin/view-bus-details/(:any)'] = 'Admin_orders/view_bus_details/$1';
	
	$route['admin/dashboard-laundry-total-bookings-providers'] = 'administration/dashboard_laundry_total_bookings_providers';
	$route['admin/dashboard-laundry-accepted-bookings-providers'] = 'administration/dashboard_laundry_accepted_bookings_providers';
	$route['admin/dashboard-laundry-in-progress-bookings-providers'] = 'administration/dashboard_laundry_in_progress_bookings_providers';
	$route['admin/dashboard-laundry-completed-bookings-providers'] = 'administration/dashboard_laundry_completed_bookings_providers';
	$route['admin/dashboard-laundry-cancelled-bookings-providers'] = 'administration/dashboard_laundry_cancelled_bookings_providers';

	$route['admin/dashboard-laundry-total-bookings-customers'] = 'administration/dashboard_laundry_total_bookings_customers';
	$route['admin/dashboard-laundry-accepted-bookings-customers'] = 'administration/dashboard_laundry_accepted_bookings_customers';
	$route['admin/dashboard-laundry-in-progress-bookings-customers'] = 'administration/dashboard_laundry_in_progress_bookings_customers';
	$route['admin/dashboard-laundry-completed-bookings-customers'] = 'administration/dashboard_laundry_completed_bookings_customers';
	$route['admin/dashboard-laundry-cancelled-bookings-customers'] = 'administration/dashboard_laundry_cancelled_bookings_customers';

	$route['admin/view-laundry-details/(:any)'] = 'Admin_orders/view_laundry_details/$1';
	$route['admin/recently-accepted-laundry'] = 'administration/recently_accepted_laundry';
	$route['admin/recently-in-progress-laundry'] = 'administration/recently_in_progress_laundry';
	$route['admin/recently-completed-laundry'] = 'administration/recently_completed_laundry';
	$route['admin/recently-cancelled-laundry'] = 'administration/recently_cancelled_laundry';

	//Laundry Order Details
	$route['admin/get-all-laundry-orders'] = 'Admin_orders/get_all_laundry_orders';
	$route['admin/view-laundry-order-details/(:any)'] = 'Admin_orders/view_laundry_order_details/$1';

	//Ticket Order Details
	$route['admin/get-all-ticket-orders'] = 'Admin_orders/get_all_ticket_orders';
	$route['admin/view-ticket-order-details/(:any)'] = 'Admin_orders/view_ticket_order_details/$1';
//End Laundry Admin Panel--------------------------------------

//Bus Seller payment Web Services------------------------------
	$route['api-bus/seller-payment-mtn'] = "api_bus/seller_payment_by_mtn";
	$route['api-bus/seller-payment-stripe'] = "api_bus/seller_payment_by_stripe";
	$route['api-bus/seller-payment-orange'] = "api_bus/seller_payment_by_orange";
	$route['api-bus/seller-single-payment-mtn'] = "api_bus/seller_single_payment_by_mtn";
	$route['api-bus/seller-single-payment-stripe'] = "api_bus/seller_single_payment_by_stripe";
	$route['api-bus/seller-single-payment-orange'] = "api_bus/seller_single_payment_by_orange";
//Bus Seller payment Web Services------------------------------


$route['api/update-order-payment-by-customer-orange'] = 'api/update_order_payment_by_customer_orange';

$route['api-bus/promocode'] = "api_bus/promocode";
$route['api-bus/promocode-check'] = "api_bus/promocode_check";
$route['api-laundry/promocode'] = "api_laundry/promocode";
$route['api-laundry/promocode-check'] = "api_laundry/promocode_check";
$route['api/promocode'] = "api/promocode";
$route['api/promocode-check'] = "api/promocode_check";
$route['api/promocode-with-booking'] = "api/promocode_with_booking";

//laundry special charges-------------------------------------
    $route['user-panel-laundry/laundry-special-charges-list'] = "user_panel_laundry/laundry_special_charges_list";
    $route['user-panel-laundry/laundry-special-charges-add'] = "user_panel_laundry/laundry_special_charges_add";
    $route['user-panel-laundry/laundry-special-charges-register'] = "user_panel_laundry/laundry_special_charges_register";
    $route['user-panel-laundry/laundry-special-charges-edit'] = "user_panel_laundry/laundry_special_charges_edit";
    $route['user-panel-laundry/laundry-special-charges-update'] = "user_panel_laundry/laundry_special_charges_update";
    $route['user-panel-laundry/laundry-special-charges-edit/(:any)'] = "user_panel_laundry/laundry_special_charges_edit/$1";
    $route['user-panel-laundry/laundry-special-charges-delete'] = "user_panel_laundry/laundry_special_charges_delete";
//laundry special charges-------------------------------------

//laundry special charges api---------------------------------
    $route['api-laundry/laundry-special-charges-list'] = "api_laundry/laundry_special_charges_list";
    $route['api-laundry/add-laundry-special-charges'] = "api_laundry/add_laundry_special_charges";
    $route['api-laundry/update-laundry-special-charges'] = "api_laundry/update_laundry_special_charges";
    $route['api-laundry/delete-laundry-special-charges'] = "api_laundry/delete_laundry_special_charges";
//laundry special charges api---------------------------------

/*----------------------------------Gonagoo Module 4 Web--------------------------------------*/
	$route['user-panel-services/dashboard-services'] = "user_panel_services/dashboard_services";
	//User Profile----------------------------------------------------------------
		$route['user-panel-services/user-profile'] = "user_panel_services/user_profile";
		$route['user-panel-services/basic-profile/:num'] = "user_panel_services/edit_basic_profile_view";
		$route['user-panel-services/basic-profile/update'] = "user_panel_services/update_basic_profile";
		$route['user-panel-services/basic-profile/get-state-by-country-id'] = "user_panel_services/get_state_by_country_id";
		$route['user-panel-services/basic-profile/get-cities-by-state-id'] = "user_panel_services/get_city_by_state_id";
	//User Profile----------------------------------------------------------------
	//User payment methods--------------------------------------------------------
		$route['user-panel-services/payment-methods'] = "user_panel_services/payment_method_list_view";
		$route['user-panel-services/payment-method/add'] = "user_panel_services/add_payment_method_view";
		$route['user-panel-services/add-payment-method'] = "user_panel_services/add_payment_method";
		$route['user-panel-services/payment-method/:num'] = "user_panel_services/edit_payment_method_view/$1";
		$route['user-panel-services/edit-payment-method'] = "user_panel_services/edit_payment_method";
		$route['user-panel-services/delete-payment-method'] = "user_panel_services/delete_payment_method";
	//User payment methods--------------------------------------------------------
	//User Education--------------------------------------------------------------
		$route['user-panel-services/educations'] = "user_panel_services/education_list_view";
		$route['user-panel-services/education/add'] = "user_panel_services/add_education_view";
		$route['user-panel-services/add-education'] = "user_panel_services/add_education";
		$route['user-panel-services/education/:num'] = "user_panel_services/edit_education_view/$1";
		$route['user-panel-services/edit-education'] = "user_panel_services/edit_education";
		$route['user-panel-services/delete-education'] = "user_panel_services/delete_education";
	//User Education--------------------------------------------------------------
	//User Experience-------------------------------------------------------------
		$route['user-panel-services/experiences'] = "user_panel_services/experience_list_view";
		$route['user-panel-services/experience/add'] = "user_panel_services/add_experience_view";
		$route['user-panel-services/add-experience'] = "user_panel_services/add_experience";
		$route['user-panel-services/experience/:num'] = "user_panel_services/edit_experience_view/$1";
		$route['user-panel-services/edit-experience'] = "user_panel_services/edit_experience";
		$route['user-panel-services/delete-experience'] = "user_panel_services/delete_experience";
	//User Experience-------------------------------------------------------------
	//User Other Experience-------------------------------------------------------
		$route['user-panel-services/other-experiences'] = "user_panel_services/other_experience_list_view";
		$route['user-panel-services/other-experience/add'] = "user_panel_services/add_other_experience_view";
		$route['user-panel-services/add-other-experience'] = "user_panel_services/add_other_experience";
		$route['user-panel-services/other-experience/:num'] = "user_panel_services/edit_other_experience_view/$1";
		$route['user-panel-services/edit-other-experience'] = "user_panel_services/edit_other_experience";
	//User Other Experience-------------------------------------------------------
	//User Skills-----------------------------------------------------------------
		$route['user-panel-services/skill'] = "user_panel_services/edit_skill_view";
		$route['user-panel-services/edit-skill'] = "user_panel_services/edit_skill";
		$route['user-panel-services/skill/get-skills-by-category-id'] = "user_panel_services/get_skills_by_category_id";
	//User Skills-----------------------------------------------------------------
	//User Portfolios-------------------------------------------------------------
		$route['user-panel-services/portfolios'] = "user_panel_services/portfolio_list_view";
		$route['user-panel-services/portfolio/add'] = "user_panel_services/add_portfolio_view";
		$route['user-panel-services/add-portfolio'] = "user_panel_services/add_portfolio";
		$route['user-panel-services/portfolio/get-category-by-cat-type-id'] = "user_panel_services/get_category_by_cat_type_id";
		$route['user-panel-services/portfolio/get-subcategories-by-category-id'] = "user_panel_services/get_subcategories_by_category_id";
		$route['user-panel-services/portfolios/:num'] = "user_panel_services/edit_portfolio_view/$1";
		$route['user-panel-services/edit-portfolio'] = "user_panel_services/edit_portfolio";
		$route['user-panel-services/delete-portfolio'] = "user_panel_services/delete_portfolio";
	//User Portfolios-------------------------------------------------------------
	//User Languages--------------------------------------------------------------
		$route['user-panel-services/get-languages-master'] = "user_panel_services/get_languages_master";
		$route['user-panel-services/update-languages'] = "user_panel_services/update_languages";
	//User Languages--------------------------------------------------------------
	//User Overview---------------------------------------------------------------
		$route['user-panel-services/update-overview'] = "user_panel_services/update_overview";
	//User Overview---------------------------------------------------------------
	//User Document---------------------------------------------------------------
		$route['user-panel-services/documents'] = "user_panel_services/document_list_view";
		$route['user-panel-services/document/add'] = "user_panel_services/add_document_view";
		$route['user-panel-services/add-document'] = "user_panel_services/add_document";
		$route['user-panel-services/document/:num'] = "user_panel_services/edit_document_view/$1";
		$route['user-panel-services/edit-document'] = "user_panel_services/edit_document";
		$route['user-panel-services/delete-document'] = "user_panel_services/delete_document";
	//User Document---------------------------------------------------------------
	//User change Password--------------------------------------------------------
		$route['user-panel-services/change-password'] = "user_panel_services/change_password";
		$route['user-panel-services/update-change'] = "user_panel_services/update_password";
	//User change Password--------------------------------------------------------
	//Provider Profile------------------------------------------------------------
		$route['user-panel-services/provider-profile'] = "user_panel_services/provider_profile";
		$route['user-panel-services/create-service-provider-profile'] = "user_panel_services/create_service_provider_profile";
		$route['user-panel-services/get-state-by-country-id'] = "user_panel_services/get_state_by_country_id";
		$route['user-panel-services/get-cities-by-state-id'] = "user_panel_services/get_city_by_state_id";
		$route['user-panel-services/edit-provider-profile'] = "user_panel_services/edit_provider_profile";
		$route['user-panel-services/update-provider-profile'] = "user_panel_services/update_provider_profile";
		$route['user-panel-services/provider-profile-view/(:any)'] = "user_panel_services/provider_profile_view/$1";
	//Provider Profile------------------------------------------------------------
	//Provider Documents----------------------------------------------------------
		$route['user-panel-services/edit-provider-document'] = "user_panel_services/edit_provider_document";
		$route['user-panel-services/add-provider-document'] = "user_panel_services/add_provider_document";
		$route['user-panel-services/provider-document-delete'] = "user_panel_services/provider_document_delete";
	//Provider Documents----------------------------------------------------------
	//Provider Settings-----------------------------------------------------------
		$route['user-panel-services/edit-provider-profile-settings'] = "user_panel_services/edit_provider_profile_settings";
		$route['user-panel-services/edit-provider-profile-settings/:num'] = "user_panel_services/edit_provider_profile_settings/$1";
		$route['user-panel-services/update-working-days'] = "user_panel_services/update_working_days";
		$route['user-panel-services/update-off-days'] = "user_panel_services/update_off_days";
		$route['user-panel-services/delete-off-days'] = "user_panel_services/delete_off_days";
		$route['user-panel-services/update-max-open-order'] = "user_panel_services/update_max_open_order";
		$route['user-panel-services/update-rate-hour'] = "user_panel_services/update_rate_hour";
		$route['user-panel-services/update-work-location'] = "user_panel_services/update_work_location";
		$route['user-panel-services/proposal-credit-payment'] = "user_panel_services/proposal_credit_payment";
		$route['user-panel-services/proposal-credit-payment-stripe'] = "user_panel_services/proposal_credit_payment_stripe";
		$route['user-panel-services/proposal-credit-payment-mtn'] = "user_panel_services/proposal_credit_payment_mtn";
		$route['user-panel-services/proposal-credit-payment-orange'] = "user_panel_services/proposal_credit_payment_orange";
		$route['user-panel-services/proposal-credit-payment-orange-process'] = "user_panel_services/proposal_credit_payment_orange_process";
		$route['user-panel-services/update-social-media'] = "user_panel_services/update_social_media";
	//Provider Settings-----------------------------------------------------------
	//Provider Business Photos----------------------------------------------------
		$route['user-panel-services/business-photos'] = "user_panel_services/business_photos";
		$route['user-panel-services/laundry-provider-business-photos-upload'] = "user_panel_services/laundry_provider_business_photos_upload";
		$route['user-panel-services/laundry-provider-business-photos-delete'] = "user_panel_services/laundry_provider_business_photos_delete";
	//Provider Business Photos----------------------------------------------------
	//Provider Offers-------------------------------------------------------------
		$route['user-panel-services/provider-offers'] = "user_panel_services/provider_offers_list_view";
		$route['user-panel-services/add-provider-offers'] = "user_panel_services/add_provider_offers_list_view";
		$route['user-panel-services/get-categories-by-type'] = "user_panel_services/get_categories_by_type";
		$route['user-panel-services/provider-pause-offer'] = "user_panel_services/provider_pause_offer";
		$route['user-panel-services/provider-resume-offer'] = "user_panel_services/provider_resume_offer";
		$route['user-panel-services/provider-delete-offer'] = "user_panel_services/provider_delete_offer";
		$route['user-panel-services/provider-add-offers'] = "user_panel_services/provider_add_offers";
		$route['user-panel-services/get-offer-tags'] = "user_panel_services/get_offer_tags";
		$route['user-panel-services/provider-add-offer-details'] = "user_panel_services/provider_add_offer_details";
		$route['user-panel-services/provider-offer-details'] = "user_panel_services/provider_offer_details";
		$route['user-panel-services/provider-offer-details/:num'] = "user_panel_services/provider_offer_details/$1";
		$route['user-panel-services/provider-edit-offer'] = "user_panel_services/provider_edit_offer";
		$route['user-panel-services/provider-delete-offer-add-on'] = "user_panel_services/provider_delete_offer_add_on";
		$route['user-panel-services/provider-delete-offer-images'] = "user_panel_services/provider_delete_offer_images";
		$route['user-panel-services/provider-add-offer-images'] = "user_panel_services/provider_add_offer_images";
		$route['user-panel-services/provider-edit-offer-details'] = "user_panel_services/provider_edit_offer_details";
	//Provider Offers-------------------------------------------------------------
	//Customer offer search and buy-----------------------------------------------
		$route['user-panel-services/available-offers-list-view'] = "user_panel_services/available_offers_list_view";
		$route['user-panel-services/customer-offer-details'] = "user_panel_services/customer_offer_details";
		$route['user-panel-services/buy-offer'] = "user_panel_services/buy_offer";
		$route['user-panel-services/customer-offer-payment'] = "user_panel_services/customer_offer_payment";
		$route['user-panel-services/customer-offer-payment/:num'] = "user_panel_services/customer_offer_payment/$1";
		$route['user-panel-services/offer-promocode'] = "user_panel_services/offer_promocode";
		$route['user-panel-services/offer-promocode-ajax'] = "user_panel_services/offer_promocode_ajax";
		$route['user-panel-services/offer-payment-bank'] = "user_panel_services/offer_payment_bank";
		$route['user-panel-services/offer-payment-mtn'] = "user_panel_services/offer_payment_mtn";
		$route['user-panel-services/offer-payment-stripe'] = "user_panel_services/offer_payment_stripe";
		$route['user-panel-services/offer-payment-orange'] = "user_panel_services/offer_payment_orange";
		$route['user-panel-services/offer-payment-orange-process'] = "user_panel_services/offer_payment_orange_process";
	//Customer offer search and buy-----------------------------------------------
	//Favorite offer--------------------------------------------------------------
		$route['user-panel-services/my-favorite-offers'] = "user_panel_services/my_favorite_offers";
		$route['user-panel-services/offer-add-to-favorite'] = "user_panel_services/offer_add_to_favorite";
		$route['user-panel-services/offer-remove-from-favorite'] = "user_panel_services/offer_remove_from_favorite";
	//Favorite offer--------------------------------------------------------------

	//My Jobs---------------------------------------------------------------------
		$route['user-panel-services/my-jobs'] = "user_panel_services/my_jobs";
		$route['user-panel-services/my-inprogress-jobs'] = "user_panel_services/my_inprogress_jobs";
		$route['user-panel-services/my-completed-jobs'] = "user_panel_services/my_completed_jobs";
		$route['user-panel-services/my-cancelled-jobs'] = "user_panel_services/my_cancelled_jobs";
		$route['user-panel-services/customer-job-offer-workroom'] = "user_panel_services/customer_job_offer_workroom";
		$route['user-panel-services/add-services-workroom-chat'] = "user_panel_services/add_services_workroom_chat";
		$route['user-panel-services/customer-job-offer-workroom-ajax'] = "user_panel_services/customer_job_offer_workroom_ajax";
		$route['user-panel-services/add-services-workroom-chat-ajax'] = "user_panel_services/add_services_workroom_chat_ajax";
		$route['user-panel-services/add-services-workroom-rating'] = "user_panel_services/add_services_workroom_rating";
		$route['user-panel-services/customer-offer-cancel'] = "user_panel_services/customer_offer_cancel";
		$route['user-panel-services/customer-withdraw-cancel-request'] = "user_panel_services/customer_withdraw_cancel_request";
		$route['user-panel-services/customer-job-details'] = "user_panel_services/customer_job_details";
		$route['user-panel-services/customer-accept-job-completed'] = "user_panel_services/customer_accept_job_completed";
		$route['user-panel-services/customer-post-job-review'] = "user_panel_services/customer_post_job_review";
		$route['user-panel-services/customer-create-dispute-for-job'] = "user_panel_services/customer_create_dispute_for_job";
	//My Jobs---------------------------------------------------------------------

	//Provider My Projects--------------------------------------------------------
		$route['user-panel-services/provider-jobs'] = "user_panel_services/provider_jobs";
		$route['user-panel-services/provider-inprogress-jobs'] = "user_panel_services/provider_inprogress_jobs";
		$route['user-panel-services/provider-completed-jobs'] = "user_panel_services/provider_completed_jobs";
		$route['user-panel-services/provider-cancelled-jobs'] = "user_panel_services/provider_cancelled_jobs";
		$route['user-panel-services/accept-cancellation-request'] = "user_panel_services/accept_cancellation_request";
		$route['user-panel-services/provider-mark-job-complete'] = "user_panel_services/provider_mark_job_complete";
		$route['user-panel-services/provider-reply-to-review'] = "user_panel_services/provider_reply_to_review";
	//Provider My Projects--------------------------------------------------------

	//Provider Dispute job--------------------------------------------------------
		$route['user-panel-services/get-dispute-sub-category'] = "user_panel_services/get_dispute_sub_category";
		$route['user-panel-services/create-dispute-for-job'] = "user_panel_services/create_dispute_for_job";
		$route['user-panel-services/provider-withdraw-dispute'] = "user_panel_services/provider_withdraw_dispute";
	//Provider Dispute job--------------------------------------------------------
/*----------------------------------Gonagoo Module 4 Web--------------------------------------*/

/*---------------------------------Gonagoo Module 4 Mobile------------------------------------*/
	//Provider Profile-----------------------------------------------------------
		$route['api-services/get-service-provider-profile'] = "api_services/get_service_provider_profile";
		$route['api-services/register-service-provider'] = "api_services/register_service_provider";
		$route['api-services/update-service-provider'] = "api_services/update_service_provider";
		$route['api-services/update-service-provider-avatar'] = "api_services/update_service_provider_avatar";
		$route['api-services/update-service-provider-cover'] = "api_services/update_service_provider_cover";
		$route['api-services/update-working-days'] = "api_services/update_working_days";
		$route['api-services/get-service-provider-off-days'] = "api_services/get_service_provider_off_days";
		$route['api-services/update-off-days'] = "api_services/update_off_days";
		$route['api-services/delete-off-days'] = "api_services/delete_off_days";
		$route['api-services/update-max-open-order'] = "api_services/update_max_open_order";
		$route['api-services/get-all-currencies-master'] = "api_services/get_all_currencies_master";
		$route['api-services/update-rate-hour'] = "api_services/update_rate_hour";
		$route['api-services/update-work-location'] = "api_services/update_work_location";
		$route['api-services/update-social-media'] = "api_services/update_social_media";
		$route['api-services/get-profile-subscription-master'] = "api_services/get_profile_subscription_master";
		$route['api-services/proposal-credit-payment-stripe'] = "api_services/proposal_credit_payment_stripe";
		$route['api-services/proposal-credit-payment-mtn'] = "api_services/proposal_credit_payment_mtn";
		$route['api-services/proposal-credit-payment-orange'] = "api_services/proposal_credit_payment_orange";
	//Provider Profile-----------------------------------------------------------
	//Provider Document----------------------------------------------------------
		$route['api-services/get-service-provider-document'] = 'api_services/get_service_provider_document';
		$route['api-services/add-service-provider-document'] = 'api_services/add_service_provider_document';
		$route['api-services/delete-service-provider-document'] = 'api_services/delete_service_provider_document';
	//Provider Document----------------------------------------------------------
	//Provider Business Photos---------------------------------------------------
		$route['api-services/get-service-provider-business-photos'] = 'api_services/get_service_provider_business_photos';
		$route['api-services/upload-service-provider-business-photo'] = 'api_services/upload_service_provider_business_photo';
		$route['api-services/delete-service-provider-business-photo'] = 'api_services/delete_service_provider_business_photo';
	//Provider Business Photos---------------------------------------------------
	//Provider Offers------------------------------------------------------------
		$route['api-services/provider-offers'] = "api_services/provider_offers_list";
		$route['api-services/check-offer-tags'] = "api_services/check_offer_tags";
		$route['api-services/get-category-type-with-category'] = "api_services/get_category_type_with_category";
		$route['api-services/provider-add-offer-details'] = "api_services/provider_add_offer_details";
		$route['api-services/provider-pause-resume-offer'] = "api_services/provider_pause_resume_offer";
		$route['api-services/provider-delete-offer'] = "api_services/provider_delete_offer";
		$route['api-services/provider-offer-details'] = "api_services/provider_offer_details";
		$route['api-services/provider-edit-offer-details'] = "api_services/provider_edit_offer_details";
		$route['api-services/provider-delete-offer-add-on'] = "api_services/provider_delete_offer_add_on";
	//Provider Offers------------------------------------------------------------
	//Customer offer search and buy----------------------------------------------
		$route['api-services/available-offers-list-view'] = "api_services/available_offers_list_view";
		$route['api-services/customer-offer-details'] = "api_services/customer_offer_details";
		$route['api-services/buy-offer'] = "api_services/buy_offer";
		$route['api-services/check-promocode'] = "api_services/check_promocode";
		$route['api-services/apply-promocode'] = "api_services/apply_promocode";
		$route['api-services/offer-payment-bank'] = "api_services/offer_payment_bank";
		$route['api-services/offer-payment-mtn'] = "api_services/offer_payment_mtn";
		$route['api-services/offer-payment-stripe'] = "api_services/offer_payment_stripe";
		$route['api-services/offer-payment-orange'] = "api_services/offer_payment_orange";
	//Customer offer search and buy----------------------------------------------
	//Favorite offer-------------------------------------------------------------
		$route['api-services/my-favorite-offers'] = "api_services/my_favorite_offers";
		$route['api-services/offer-add-to-favorite'] = "api_services/offer_add_to_favorite";
		$route['api-services/offer-remove-from-favorite'] = "api_services/offer_remove_from_favorite";
	//Favorite offer-------------------------------------------------------------

	//My Jobs---------------------------------------------------------------------
		$route['api-services/job-cancellation-reason-master'] = "api_services/job_cancellation_reason_master";
		$route['api-services/my-jobs'] = "api_services/my_jobs";
		$route['api-services/customer-offer-cancel'] = "api_services/customer_offer_cancel";
		$route['api-services/customer-withdraw-cancel-request'] = "api_services/customer_withdraw_cancel_request";



		/*
		$route['api-services/customer-job-offer-workroom'] = "api_services/customer_job_offer_workroom";
		$route['api-services/add-services-workroom-chat'] = "api_services/add_services_workroom_chat";
		$route['api-services/customer-job-offer-workroom-ajax'] = "api_services/customer_job_offer_workroom_ajax";
		$route['api-services/add-services-workroom-chat-ajax'] = "api_services/add_services_workroom_chat_ajax";
		$route['api-services/add-services-workroom-rating'] = "api_services/add_services_workroom_rating";
		$route['api-services/customer-job-details'] = "api_services/customer_job_details";
		$route['api-services/customer-accept-job-completed'] = "api_services/customer_accept_job_completed";
		$route['api-services/customer-post-job-review'] = "api_services/customer_post_job_review";
		$route['api-services/customer-create-dispute-for-job'] = "api_services/customer_create_dispute_for_job";
		*/
	//My Jobs---------------------------------------------------------------------

	//Provider My Projects--------------------------------------------------------
		$route['api-services/provider-jobs'] = "api_services/provider_jobs";
		$route['api-services/accept-cancellation-request'] = "api_services/accept_cancellation_request";

		/*
		$route['api-services/provider-reply-to-review'] = "api_services/provider_reply_to_review";
		*/
	//Provider My Projects--------------------------------------------------------


	//Provider Dispute job--------------------------------------------------------
		$route['api-services/create-dispute-for-job'] = "api_services/create_dispute_for_job";
		$route['api-services/provider-withdraw-dispute'] = "api_services/provider_withdraw_dispute";

		
		
	//Provider Dispute job--------------------------------------------------------


	//Clean Jobs-----------------------------------------------------------------
		$route['api-services/services-job-cleanup'] = "api_services/services_job_cleanup";
	//Clean Jobs-----------------------------------------------------------------
/*---------------------------------Gonagoo Module 4 Mobile------------------------------------*/

/*---------------------------------Gonagoo Module 4 Admin------------------------------------*/
	//Advance Payment------------------------------------------------------------
		$route['admin/advance-payment-services'] = 'advance_payment_services/index';
		$route['admin/advance-payment-services/add'] = 'advance_payment_services/add';
		$route['admin/advance-payment-services/get-country-currencies'] = 'advance_payment_services/get_country_currencies';
		$route['admin/advance-payment-services/get-categories-by-type'] = 'advance_payment_services/get_categories_by_type';
		$route['admin/advance-payment-services/register'] = 'advance_payment_services/register';
		$route['admin/advance-payment-services/edit/(:num)'] = 'advance_payment_services/edit/$1';
		$route['admin/advance-payment-services/update'] = 'advance_payment_services/update';
		$route['admin/advance-payment-services/delete'] = 'advance_payment_services/delete';
	//Advance Payment------------------------------------------------------------
	//Provider Question Master---------------------------------------------------
		$route['admin/provider-question-master'] = 'provider_question_master/index';
		$route['admin/provider-question-master/edit/(:num)'] = 'provider_question_master/edit/$1';
		$route['admin/provider-question-master/update'] = 'provider_question_master/update';
		$route['admin/provider-question-master/delete'] = 'provider_question_master/delete';
		$route['admin/provider-question-master/add'] = 'provider_question_master/add';
		$route['admin/provider-question-master/get-categories-by-type'] = 'provider_question_master/get_categories_by_type';
		$route['admin/provider-question-master/register'] = 'provider_question_master/register';
	//Provider Question Master---------------------------------------------------
	//Cancellation Reason Master-------------------------------------------------
		$route['admin/job-cancellation-reason-master'] = 'job_cancellation_reason_master/index';
		$route['admin/job-cancellation-reason-master/register'] = 'job_cancellation_reason_master/register';
		$route['admin/job-cancellation-reason-master/delete'] = 'job_cancellation_reason_master/delete';
	//Cancellation Reason Master-------------------------------------------------
	//Provider Profile Subscription Master---------------------------------------
		$route['admin/profile-subscription'] = 'profile_subscription/index';
		$route['admin/profile-subscription/add'] = 'profile_subscription/add';
		$route['admin/profile-subscription/get-country-currencies'] = 'profile_subscription/get_country_currencies';
		$route['admin/profile-subscription/register'] = 'profile_subscription/register';
		$route['admin/profile-subscription/edit/(:num)'] = 'profile_subscription/edit/$1';
		$route['admin/profile-subscription/update'] = 'profile_subscription/update';
		$route['admin/profile-subscription/delete'] = 'profile_subscription/delete';
	//Provider Profile Subscription Master---------------------------------------
	//Dispute Category-----------------------------------------------------------
		$route['admin/dispute-category-master'] = 'dispute_category_master/index';
		$route['admin/dispute-category-master/register'] = 'dispute_category_master/register';
		$route['admin/dispute-category-master/activate-dispute-category'] = 'dispute_category_master/activate_dispute_category';
		$route['admin/dispute-category-master/inactivate-dispute-category'] = 'dispute_category_master/inactivate_dispute_category';
	//Dispute Category-----------------------------------------------------------
	//Dispute Sub Category-------------------------------------------------------
		$route['admin/dispute-sub-category-master'] = 'dispute_sub_category_master/index';
		$route['admin/dispute-sub-category-master/register'] = 'dispute_sub_category_master/register';
		$route['admin/dispute-sub-category-master/activate-dispute-sub-category'] = 'dispute_sub_category_master/activate_dispute_sub_category';
		$route['admin/dispute-sub-category-master/inactivate-dispute-sub-category'] = 'dispute_sub_category_master/inactivate_dispute_sub_category';
	//Dispute Sub Category-------------------------------------------------------
	//Contract Expiry Delay Hours------------------------------------------------
		$route['admin/contract-expiry-delay'] = 'contract_expiry_delay/index';
		$route['admin/contract-expiry-delay/register'] = 'contract_expiry_delay/register';
		$route['admin/contract-expiry-delay/delete'] = 'contract_expiry_delay/delete';
	//Contract Expiry Delay Hours------------------------------------------------
	//Manage Offers--------------------------------------------------------------
		$route['admin/provider-offer-manage'] = 'provider_offer_manage/index';
		$route['admin/provider-offer-manage/activate-offer'] = 'provider_offer_manage/activate_offer';
		$route['admin/provider-offer-manage/de-activate-offer'] = 'provider_offer_manage/de_activate_offer';
		$route['admin/provider-offer-manage/offer-details'] = 'provider_offer_manage/offer_details';
		$route['admin/provider-offer-manage/active-offers'] = 'provider_offer_manage/active_offers';
		$route['admin/provider-offer-manage/rejected-offers'] = 'provider_offer_manage/rejected_offers';
	//Manage Offers--------------------------------------------------------------
/*---------------------------------Gonagoo Module 4 Admin------------------------------------*/





//zaid G4 ----------------------------------------------------------------------------
	$route['user-panel-services/post-customer-job'] = "user_panel_services/post_customer_job";
	$route['user-panel-services/get-sub-category'] = "user_panel_services/get_sub_category";
	$route['user-panel-services/get-questions-know-freelancer'] = "user_panel_services/get_questions_know_freelancer";
	$route['user-panel-services/register-customer-job'] = "user_panel_services/register_customer_job";
	$route['user-panel-services/list-of-freelancers/(:num)'] = "user_panel_services/list_of_freelancers/$1";
	$route['user-panel-services/filter-list-of-freelancers'] = "user_panel_services/filter_list_of_freelancers";
	$route['user-panel-services/list-of-fav-'] = "user_panel_services/list_of_fav_freelancers";
	$route['user-panel-services/my-favorite-freelancers'] = "user_panel_services/my_favorite_freelancers";
	$route['user-panel-services/make-freelancer-favourite'] = "user_panel_services/make_freelancer_favourite";
	$route['user-panel-services/freelancer-remove-from-favorite'] = "user_panel_services/freelancer_remove_from_favorite";
	$route['user-panel-services/customer-invite-job'] = "user_panel_services/customer_invite_job";
	$route['user-panel-services/customer-invite-job-all'] = "user_panel_services/customer_invite_job_all";
	$route['user-panel-services/job-workstream-list/(:num)'] = "user_panel_services/job_workstream_list/$1";
	$route['user-panel-services/job-workstream-list'] = "user_panel_services/job_workstream_list";
	$route['user-panel-services/job-proposal-decline'] = "user_panel_services/job_proposal_decline";
	$route['user-panel-services/browse-project'] = "user_panel_services/browse_project";
	$route['user-panel-services/my-favorite-jobs'] = "user_panel_services/my_favorite_job";
	$route['user-panel-services/make-job-favourite'] = "user_panel_services/make_job_favourite";
	$route['user-panel-services/job-remove-from-favorite'] = "user_panel_services/job_remove_from_favorite";
	$route['user-panel-services/send-proposal-view'] = "user_panel_services/send_proposal_view";
	$route['user-panel-services/send-proposal'] = "user_panel_services/send_proposal";
	$route['user-panel-services/customer-job-details-view'] = "user_panel_services/customer_job_details_view";
	$route['user-panel-services/customer-job-workroom'] = "user_panel_services/customer_job_workroom";
	$route['user-panel-services/customer-job-workroom-ajax']= "user_panel_services/customer_job_workroom_ajax";
	$route['user-panel-services/add-job-workroom-chat-ajax'] = "user_panel_services/add_job_workroom_chat_ajax";
	$route['user-panel-services/add-job-workroom-chat'] = "user_panel_services/add_job_workroom_chat";
	$route['user-panel-services/pending-job-proposals'] = "user_panel_services/pending_job_proposals";
	$route['user-panel-services/job-invitations'] = "user_panel_services/job_invitations";
	$route['user-panel-services/provider-job-workroom'] = "user_panel_services/provider_job_workroom";
	$route['user-panel-services/provider-job-workroom-ajax'] = "user_panel_services/provider_job_workroom_ajax";
	$route['user-panel-services/add-provider-job-workroom-chat'] = "user_panel_services/add_provider_job_workroom_chat";
	$route['user-panel-services/add-provider-job-workroom-chat-ajax'] = "user_panel_services/add_provider_job_workroom_chat_ajax";
	$route['user-panel-services/accept-job-proposal-payment-view'] = "user_panel_services/accept_job_proposal_payment_view";
	$route['user-panel-services/job-payment-bank'] = "user_panel_services/job_payment_bank";
	$route['user-panel-services/job-payment-mtn'] = "user_panel_services/job_payment_mtn";
	$route['user-panel-services/job-payment-stripe'] = "user_panel_services/job_payment_stripe";
	$route['user-panel-services/job-payment-orange'] = "user_panel_services/job_payment_orange";
	$route['user-panel-services/job-payment-orange-process'] = "user_panel_services/job_payment_orange_process";
	$route['user-panel-services/job-milestone-payment-request-view'] = "user_panel_services/job_milestone_payment_request_view";
	$route['user-panel-services/job_milestone-payment-request'] = "user_panel_services/job_milestone_payment_request";
	$route['user-panel-services/customer-milestone-details'] = "user_panel_services/customer_milestone_details";
	$route['user-panel-services/job-milestone-payment-stripe'] = "user_panel_services/job_milestone_payment_stripe";
	$route['user-panel-services/provider-mark-job-completed'] = "user_panel_services/provider_mark_job_completed";
	$route['user-panel-services/customer-accepted-job-complete'] = "user_panel_services/customer_accepted_job_complete";
//zaid G4-----------------------------------------------------------------------------
	$route['user-panel-services/customer-job-cancel'] = "user_panel_services/customer_job_cancel";
	$route['user-panel-services/provider-job-cancel'] = "user_panel_services/provider_job_cancel";
	$route['user-panel-services/provider-withdraw-cancel-request'] = "user_panel_services/provider_withdraw_cancel_request";
	$route['user-panel-services/provider-accept-cancellation-request'] = "user_panel_services/provider_accept_cancellation_request";
	$route['user-panel-services/customer-accept-cancellation-request'] = "user_panel_services/customer_accept_cancellation_request";
	$route['user-panel-services/customer-create-dispute-job'] = "user_panel_services/customer_create_dispute_job";
	$route['user-panel-services/provider-create-dispute-job'] = "user_panel_services/provider_create_dispute_job";
	$route['user-panel-services/browse-provider'] = "user_panel_services/browse_provider";

//maaz front side---------------------------------------------------------------------
	$route['projects'] = "front_site/projects";
	$route['get-state-by-country-id'] = "front_site/get_state_by_country_id";
	$route['get-cities-by-state-id'] = "front_site/get_cities_by_state_id";
	$route['get-categories-by-type'] = "front_site/get_categories_by_type";
	$route['view-proposal-detail'] = "front_site/view_proposal_detail";
	$route['login-provider'] = "front_site/login_provider";
	$route['confirm-login-provider'] = "front_site/confirm_login_provider";
	$route['register-new-provider'] = "front_site/register_new_provider";
	$route['user-panel-services/dashboard-services-provider'] = "user_panel_services/dashboard_services_provider";
	$route['troubleshooter'] = "front_site/troubleshooter";
	$route['user-panel-services/send-proposal-view-per-hour'] = "user_panel_services/send_proposal_view_per_hour";
	$route['user-panel-services/send-proposal-per-hour'] = "user_panel_services/send_proposal_per_hour";
	$route['user-panel-services/accept-per-hour-job-proposal'] = "user_panel_services/accept_per_hour_job_proposal";
	$route['user-panel-services/provider-submit-hours-for-payment'] = "user_panel_services/provider_submit_hours_for_payment";
	$route['user-panel-services/projects-workroom'] = "user_panel_services/projects_workroom";
	$route['user-panel-services/provider-submit-hours'] = "user_panel_services/provider_submit_hours";
	$route['user-panel-services/customer-submitted-hours-details'] = "user_panel_services/customer_submitted_hours_details";
	$route['user-panel-services/accept-job-work-hours-milestone-payment-view'] = "user_panel_services/accept_job_work_hours_milestone_payment_view";
	$route['user-panel-services/accept-job-milestone-payment-view'] = "user_panel_services/accept_job_milestone_payment_view";
	$route['user-panel-services/job-per-hour-milestone-payment-stripe'] = "user_panel_services/job_per_hour_milestone_payment_stripe";
	$route['user-panel-services/customer-per-hour-job-details'] = "user_panel_services/customer_per_hour_job_details";
	$route['post-troubleshoot'] = "front_site/post_troubleshoot";
	$route['troubleshoot-job-post'] = "front_site/troubleshoot_job_post";
	$route['get-questions-freelancer'] = "front_site/get_questions_know_freelancer";
	$route['confirm-login-troubleshoot'] = "front_site/confirm_login_troubleshoot";
	$route['troubleshoot-sign-up'] = "front_site/troubleshoot_sign_up";
	$route['post-project-job'] = "front_site/post_project_job";
	$route['post-project-login'] = "front_site/post_project_login";
	$route['search-freelancer'] = "front_site/search_freelancer";
	$route['provider-profile-view/(:any)'] = "front_site/provider_profile_detail/$1";
	//WS------------------------------------------------------------------
	$route['api-services/register-troubleshoot-job'] = "api_services/register_troubleshoot_job";
	$route['api-services/get-troubleshoot-by-id'] = "api_services/get_troubleshoot_by_id";
	$route['api-services/notification-counts'] = "api_services/notification_counts";
	$route['api-services/get-notifications'] = "api_services/get_notifications";
	$route['api-services/unread-notifications'] = "api_services/unread_notifications";
	$route['api-services/unread-workroom-notifications'] = "api_services/unread_workroom_notifications";
	$route['api-services/make-notification-read'] = "api_services/make_notification_read";
	$route['api-services/make-workroom-notification-read'] = "api_services/make_workroom_notification_read";
	$route['api-services/projects-workroom'] = "api_services/projects_workroom";
	$route['api-services/send-proposal'] = "api_services/send_proposal";
	$route['api-services/browse-project'] = "api_services/browse_project";
	$route['api-services/browse-provider'] = "api_services/browse_provider";
	$route['api-services/browse-troubleshooter'] = "api_services/browse_troubleshooter";
	//WS------------------------------------------------------------------
//maaz front side---------------------------------------------------------------------
//Service Account wallet--------------------------------------------------------------
	$route['user-panel-services/services-wallet'] = "user_panel_services/services_wallet";
	$route['user-panel-services/services-account-history'] = "user_panel_services/services_account_history";
	$route['user-panel-services/services-invoices-history'] = "user_panel_services/services_invoices_history";
	$route['user-panel-services/my-scrow'] = "user_panel_services/my_scrow";
	$route['user-panel-services/user-scrow-history'] = "user_panel_services/user_scrow_history";
	$route['user-panel-services/services-withdrawals-request'] = "user_panel_services/services_withdrawals_request";
	$route['user-panel-services/services-withdrawals-accepted'] = "user_panel_services/services_withdrawals_accepted";
	$route['user-panel-services/services-withdrawals-rejected'] = "user_panel_services/services_withdrawals_rejected";
	$route['user-panel-services/services-withdrawal-request-details'] = "user_panel_services/services_withdrawal_request_details";
	$route['user-panel-services/create-services-withdraw-request'] = "user_panel_services/create_services_withdraw_request";
	$route['user-panel-services/withdraw-request-sent-services'] = "user_panel_services/withdraw_request_sent_services";
	$route['user-panel-services/avalaible-milestone-for-withdraw'] = "user_panel_services/avalaible_milestone_for_withdraw";
	$route['user-panel-services/job-milestone-for-withdraw'] = "user_panel_services/job_milestone_for_withdraw";
//Service Account wallet--------------------------------------------------------------
	$route['api-services/send-proposal-view-details'] = "api_services/send_proposal_view_details";
	$route['api-services/job-workstream-list'] = "api_services/job_workstream_list";
	$route['api-services/provider-delete-offer-images'] = "api_services/provider_delete_offer_images";
	$route['api-services/provider-inprogress-jobs'] = "api_services/provider_inprogress_jobs";
	$route['api-services/my-inprogress-jobs'] = "api_services/my_inprogress_jobs";
	$route['api-services/job-invitations'] = "api_services/job_invitations";
	$route['api-services/pending-job-proposals'] = "api_services/pending_job_proposals";
	$route['user-panel-services/skill_old'] = "user_panel_services/edit_skill_view_old";
	// Customer profile Api's
	$route['api-services/get-profile-details'] = "api_services/get_profile_details";
	$route['api-services/basic-info-update'] = "api_services/basic_info_update";
	$route['api-services/update-avatar'] = "api_services/update_avatar";
	$route['api-services/update-cover'] = "api_services/update_cover";
	$route['api-services/get-portfolio-details'] = "api_services/get_portfolio_details";
	$route['api-services/register-portfolio'] = "api_services/register_portfolio";
	$route['api-services/update-portfolio'] = "api_services/update_portfolio";
	$route['api-services/delete-portfolio'] = "api_services/delete_portfolio";
	$route['api-services/skill-list'] = "api_services/skill_list";
	$route['api-services/skill-update'] = "api_services/skill_update";
	$route['api-services/get-paymnet-details'] = "api_services/get_paymnet_details";
	$route['api-services/add-paymnet-detail'] = "api_services/add_paymnet_detail";
	$route['api-services/update-paymnet-detail'] = "api_services/update_paymnet_detail";
	$route['api-services/delete-paymnet-detail'] = "api_services/delete_paymnet_detail";
	$route['api-services/category-type-master'] = "api_services/category_type_master";
	$route['api-services/category-list-master'] = "api_services/category_list_master";
	$route['api-services/language-list-master'] = "api_services/language_list_master";
	$route['api-services/language-update'] = "api_services/language_update";
	$route['api-services/education-list'] = "api_services/education_list";
	$route['api-services/education-add'] = "api_services/education_add";
	$route['api-services/education-update'] = "api_services/education_update";
	$route['api-services/education-delete'] = "api_services/education_delete";
	$route['api-services/experience-list'] = "api_services/experience_list";
	$route['api-services/experience-add'] = "api_services/experience_add";
	$route['api-services/experience-update'] = "api_services/experience_update";
	$route['api-services/experience-delete'] = "api_services/experience_delete";
	$route['api-services/get-documents'] = "api_services/get_documents";
	$route['api-services/add-document'] = "api_services/add_document";
	$route['api-services/update-document'] = "api_services/update_document";
	$route['api-services/delete-document'] = "api_services/delete_document";
	$route['api-services/update-overview'] = "api_services/update_overview";
	$route['api-services/list-of-fav-freelancers'] = "api_services/list_of_fav_freelancers";
	$route['api-services/make-freelancer-favourite'] = "api_services/make_freelancer_favourite";
	$route['api-services/freelancer-remove-from-favorite'] = "api_services/freelancer_remove_from_favorite";
	$route['api-services/invite-freelancers-for-job'] = "api_services/invite_freelancers_for_job";
	$route['api-services/get-sub-category-details'] = "api_services/get_sub_category_details";
	$route['api-services/invite-all-freelancers-for-job'] = "api_services/invite_all_freelancers_for_job";
	$route['api-services/services-wallet'] = "api_services/services_wallet";
	$route['api-services/services-account-history'] = "api_services/services_account_history";
	$route['api-services/services-invoices-history'] = "api_services/services_invoices_history";
	$route['api-services/my-scrow'] = "api_services/my_scrow";
	$route['api-services/user-scrow-history'] = "api_services/user_scrow_history";
	$route['api-services/services-withdrawals-request'] = "api_services/services_withdrawals_request";
	$route['api-services/services-withdrawals-accepted'] = "api_services/services_withdrawals_accepted";
	$route['api-services/services-withdrawals-rejected'] = "api_services/services_withdrawals_rejected";	
	$route['api-services/my-favorite-freelancers'] = "api_services/my_favorite_freelancers";	
	$route['api-services/make-freelancer-favourite'] = "api_services/make_freelancer_favourite";
	$route['api-services/freelancer-remove-from-favorite'] = "api_services/freelancer_remove_from_favorite";	
	$route['api-services/wallet-login-confirm'] = "api_services/wallet_login_confirm";	
	$route['api-services/avalaible-milestone-for-withdraw'] = "api_services/avalaible_milestone_for_withdraw";	
	$route['api-services/job-milestone-for-withdraw'] = "api_services/job_milestone_for_withdraw";	
	$route['api-services/my-favorite-job'] = "api_services/my_favorite_job";	
	$route['api-services/make-job-favourite'] = "api_services/make_job_favourite";	
	$route['api-services/job-remove-from-favorite'] = "api_services/job_remove_from_favorite";	
	$route['api-services/job-workroom'] = "api_services/job_workroom";	
	$route['api-services/send-job-workroom-chat'] = "api_services/send_job_workroom_chat";	
	//zaid admin
	$route['user-panel-services/account-login/(:num)'] = "user_panel_services/account_login/$1";
	$route['user-panel-services/account-login-confirm'] = "user_panel_services/account_login_confirm";
	//Job Dispute Manage --------------------------------------------------------
		$route['admin/manage-dispute'] = 'manage_dispute/index';
		$route['admin/customer-chat'] = 'manage_dispute/customer_chat';
		$route['admin/add-job-workroom-chat-ajax'] = 'manage_dispute/add_job_workroom_chat_ajax';
		$route['admin/customer-job-workroom-ajax']= "manage_dispute/customer_job_workroom_ajax";
		$route['admin/provider-job-workroom-ajax']= "manage_dispute/provider_job_workroom_ajax";
		$route['admin/freelancer-chat'] = 'manage_dispute/freelancer_chat';
		$route['admin/group-chat'] = 'manage_dispute/group_chat';
		$route['admin/add-job-workroom-group-chat-ajax'] = 'manage_dispute/add_job_workroom_group_chat_ajax';
		$route['admin/group-job-workroom-ajax'] = 'manage_dispute/group_job_workroom_ajax';
		$route['admin/about-dispute'] = 'manage_dispute/about_dispute';
		$route['admin/dispute-job-cancel'] = 'manage_dispute/dispute_job_cancel';
	//Job Dispute Manage---------------------------------------------------------
	$route['user-panel-services/customer-admin-workroom'] = "user_panel_services/customer_admin_workroom";
	$route['user-panel-services/provider-admin-workroom'] = "user_panel_services/provider_admin_workroom";
	$route['user-panel-services/admin-add-workroom-chat-ajax'] = "user_panel_services/admin_add_workroom_chat_ajax";
	$route['user-panel-services/admin_customer-job-workroom-ajax']= "user_panel_services/admin_customer_job_workroom_ajax";
	$route['user-panel-services/admin-provider_job-workroom-ajax']= "user_panel_services/admin_provider_job_workroom_ajax";

