<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('database', 'pagination', 'session', 'user_agent', 'form_validation');
$autoload['drivers'] = array();
$autoload['helper'] = array('url', 'form', 'cookie','date');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();
