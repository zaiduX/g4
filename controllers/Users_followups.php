<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_followups extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Users_followups_model', 'users');		
			$this->load->model('web_user_model', 'user');		
			$this->load->model('Notification_model', 'notice');
			$this->load->model('Withdraw_model', 'withdraw');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function user_followups_admin()
	{		
	    $userid = $this->session->userdata('userid');
	    if($userid > 1) {   $admin_country_id = $this->session->userdata('admin_country_id'); }
	    else { $admin_country_id = 0; }
		$users_list = $this->users->get_users($admin_country_id);
		$authority_list = $this->users->get_authority_list();
		$this->load->view('admin/users_followups_list_view', compact('users_list','authority_list'));			
		$this->load->view('admin/footer_view');	
	}

	public function followup_assigned_users()
	{		
	    $userid = $this->session->userdata('userid');
	    if($userid > 1) {   $admin_country_id = $this->session->userdata('admin_country_id'); }
	    else { $admin_country_id = 0; }
		$users_list = $this->users->get_users_assigned($admin_country_id);
		$authority_list = $this->users->get_authority_list();
		$this->load->view('admin/assigned_users_followups_list_view', compact('users_list','authority_list'));			
		$this->load->view('admin/footer_view');		
	}
	
	public function assign_authority_to_user()
	{
		$cust_id = $this->input->post('cust_id', TRUE);
		$auth_id = $this->input->post('auth_id', TRUE);
		$type = $this->input->post('type', TRUE);
		if($this->users->assign_authority_to_user($cust_id, $auth_id)) {
			$this->session->set_flashdata('success','Authority assigned successfully!');
		} else { $this->session->set_flashdata('error','Unable to assign authority! Try again...');	} 

		if($type == 'assign') {
			redirect('admin/user-followups-admin');
		} else { redirect('admin/followup-assigned-users');	} 
	}
	
}

/* End of file Users_followups.php */
/* Location: ./application/controllers/Users_followups.php */