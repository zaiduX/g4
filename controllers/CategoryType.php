<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryType extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('Admin_model', 'admin');	
			$this->load->model('Category_type_model', 'catType');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function category_type()
	{		
		$category_type = $this->catType->get_category_types();
		$this->load->view('admin/category_type_list_view', compact('category_type'));			
		$this->load->view('admin/footer_view');		
	}
	public function edit_category_type()
	{	
		$cat_type_id = (int) $this->uri->segment('3');
		$cat_type = $this->catType->get_category_type_detail($cat_type_id);

		$this->load->view('admin/category_type_edit_view', compact('cat_type'));
		$this->load->view('admin/footer_view');
	}
	public function update_category_type()
	{
		$cat_type_id = $this->input->post('cat_type_id', TRUE);
		$cat_type = $this->input->post('cat_type', TRUE);
		$udpate_data = array(
		'cat_type_id' => (int) $cat_type_id,
		'cat_type' => $cat_type
		);
		if(! $this->catType->get_category_type_by_type($cat_type) ) {
			if( $this->catType->update_category_type($udpate_data)){
				$this->session->set_flashdata('success','Category type successfully updated!');
			}	else { 	$this->session->set_flashdata('error','Unable to update category type! Try again...');	}
		} else { 	$this->session->set_flashdata('error','Category type already exists! Try again with different type...');	}
		redirect('admin/edit-category-type/'. $cat_type_id);
	}
	public function active_category_type()
	{
		$cat_type_id = (int) $this->input->post('id', TRUE);
		if( $this->catType->activate_category_type($cat_type_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	public function inactive_category_type()
	{
		$cat_type_id = (int) $this->input->post('id', TRUE);
		if( $this->catType->inactivate_category_type($cat_type_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	public function delete_category_type()
	{
		$cat_type_id = (int) $this->uri->segment('3');
		if( $this->catType->delete_category_type($cat_type_id) ) {
			$this->session->set_flashdata('success','Category type successfully deleted!');
		}	else { 	$this->session->set_flashdata('error','Unable to delete category type! Try again...');	}
		redirect('category_type');					
	}
	public function add_category_type()
	{
		$this->load->view('admin/category_type_add_view');
		$this->load->view('admin/footer_view');
	}
	public function register_category_type()
	{
		$cat_type = $this->input->post('cat_type', TRUE);
		if(! $this->catType->get_category_type_by_type($cat_type) ) {
			if($this->catType->register_category_type($cat_type)){
				$this->session->set_flashdata('success','Category type successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new category type! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Category type already exists! Try again with different type...');	}
		redirect('admin/add-category-type');
	}
}

/* End of file CategoryType.php */
/* Location: ./application/controllers/CategoryType.php */