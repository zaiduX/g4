<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_laundry extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('api_model_laundry', 'api');   
    if( $this->input->method() != 'post' ) exit('No direct script access allowed');
    if( $this->input->is_ajax_request() ) exit('No direct script access allowed');
  }
  
  public function index() { exit('No direct script access allowed.'); }

  






  public function payment_by_wecashup()
  { 
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $merchant_uid = 'kZudktr3nxX0AMM633dXyHEesV13';            // Replace with your merchant_uid
    $merchant_public_key = 'DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl'; // Replace with your merchant_public_key !!!
    $merchant_secret = 'XKjy7SzenXCvF9N0';                       // Replace with your merchant_private_key !!!
    $transaction_uid = '';// create an empty transaction_uid
    $transaction_token  = '';// create an empty transaction_token
    $transaction_provider_name  = ''; // create an empty transaction_provider_name
    $transaction_confirmation_code  = ''; // create an empty confirmation code
    $today = date('Y-m-d h:i:s');

    if(isset($_POST['transaction_uid'])){ $transaction_uid = $this->input->post('transaction_uid', TRUE);   } 
    if(isset($_POST['transaction_token'])){  $transaction_token  = $this->input->post('transaction_token', TRUE);  } 
    if(isset($_POST['transaction_provider_name'])){  $transaction_provider_name  = $this->input->post('transaction_provider_name', TRUE);   }
    if(isset($_POST['transaction_confirmation_code'])){  $transaction_confirmation_code  = $this->input->post('transaction_confirmation_code', TRUE); } 
    $url = 'https://www.wecashup.com/api/v1.0/merchants/'.$merchant_uid.'/transactions/'.$transaction_uid.'/?merchant_public_key='.$merchant_public_key;

    $fields_string = '';
    $fields = array(
      'merchant_secret' => urlencode($merchant_secret),
      'transaction_token' => urlencode($transaction_token),
      'transaction_uid' => urlencode($transaction_uid),
      'transaction_confirmation_code' => urlencode($transaction_confirmation_code),
      'transaction_provider_name' => urlencode($transaction_provider_name),
      '_method' => urlencode('PATCH')
    );
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);

    $data = json_decode($server_output, true);
    $payment_data = array();
    if($data['response_status'] =="success"){

      $payment_data = array(
        "cust_id" => (int)$data['response_content']['transaction']['transaction_sender_reference'],
        "order_id" => (int)$data['response_content']['transaction']['transaction_receiver_reference'],
        "payment_method" => trim($data['response_content']['transaction']['transaction_provider_name']),
        "transaction_id" => trim($data['response_content']['transaction']['transaction_uid']),
        "total_amount_paid" => trim($data['response_content']['transaction']['transaction_receiver_total_amount']),
        "transfer_account_number" => trim($data['response_content']['transaction']['transaction_sender_uid']),
        "currency_sign" => trim($data['response_content']['transaction']['transaction_receiver_currency']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => "NULL",
        "payment_response" => json_encode($data),
        "payment_by" => "web",
      );
      $this->update_order_payment_for_wecashup($payment_data);
      
      $this->session->set_flashdata('success', 'Payment successfully done!');
    } 
    else {  $this->session->set_flashdata('error', 'Payment failed! Try again...'); }

    $location = base_url('user-panel/my-bookings');
    echo '<script>top.window.location = "'.$location.'"</script>';
  }
  public function payment_by_mtn_from_mobile()
  {  
    //echo json_encode($_POST); die();
    $order_id = $this->input->post('order_id', TRUE);
    $phone_no = $this->input->post('phone_no', TRUE);
    $device_type = $this->input->post('device_type', TRUE); //android/ios
    $order_details = $this->api->get_order_detail($order_id);
    $order_amount = $order_details['order_price'];
    if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
    
    $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$order_amount."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    $mtn_pay_res = json_decode($contents, TRUE);

    $payment_data = array();
    if($mtn_pay_res['StatusCode'] == "01") {
      $payment_data = array(
        "cust_id" => (int)$order_details['cust_id'],
        "order_id" => (int)$order_details['order_id'],
        "payment_method" => 'mtn',
        "transaction_id" => trim($mtn_pay_res['TransactionID']),
        "total_amount_paid" => trim($order_amount),
        "transfer_account_number" => trim($mtn_pay_res['SenderNumber']),
        "currency_sign" => trim($order_details['currency_title']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => trim($mtn_pay_res['SenderNumber']),
        "payment_response" => json_encode($mtn_pay_res),
        "payment_by" => trim($device_type),
      );
      $this->update_order_payment_for_wecashup($payment_data);
      $this->response = ['response' => 'success'];
    } else { $this->response = ['response' => 'failed']; }
    echo json_encode($this->response);
  }
  public function update_order_payment_for_wecashup($data=null)
  {
    if(!is_null($data)){
      $cust_id = (int) $data['cust_id'];
      $order_id = (int) $data['order_id'];
      $payment_method = trim($data['payment_method']);
      $transaction_id = trim($data['transaction_id']);
      $today = date('Y-m-d h:i:s');
      $order_details = $this->api->get_order_detail($order_id);
      $total_amount_paid = trim($data['total_amount_paid']);
      $transfer_account_number = trim($data['transfer_account_number']);
      $bank_name = trim($data['bank_name']);
      $account_holder_name = trim($data['account_holder_name']);
      $iban = trim($data['iban']);
      $email_address = trim($data['email_address']);
      $mobile_number = trim($data['mobile_number']);
      $currency_sign = trim($data['currency_sign']);
      $payment_response = trim($data['payment_response']);
      $payment_by = trim($data['payment_by']);

      $update_data_order = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "transaction_id" => trim($transaction_id),
        "payment_response" => trim($payment_response),
        "payment_by" => trim($payment_by),
        "payment_datetime" => $today,
        "complete_paid" => "1",
        "payment_mode" => "payment",
        "cod_payment_type" => "NULL",
      );
      //echo json_encode($update_data_order); die();
      if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {
        /*************************************** Payment Section ****************************************/
          //Add core price to gonagoo master----------------------
            if($this->api->gonagoo_master_details(trim($data['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($data['currency_sign']),
              );
              $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'standard_price',
              "amount" => trim($order_details['standard_price']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------
            
          //Update urgent fee in gonagoo account master-----------
            if(trim($order_details['urgent_fee']) > 0) {
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'urgent_fee',
                "amount" => trim($order_details['urgent_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($data['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
          //------------------------------------------------------
          //Update insurance fee in gonagoo account master--------
            if(trim($order_details['insurance_fee']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'insurance_fee',
                "amount" => trim($order_details['insurance_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($data['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
          //------------------------------------------------------

          //Update handling fee in gonagoo account master---------
            if(trim($order_details['handling_fee']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'handling_fee',
                "amount" => trim($order_details['handling_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($data['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
          //------------------------------------------------------

          //Update vehicle fee in gonagoo account master----------
            if(trim($order_details['vehicle_fee']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
            
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'vehicle_fee',
                "amount" => trim($order_details['vehicle_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($data['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
          //------------------------------------------------------

          //Update custome clearance fee in gonagoo account master
            if(trim($order_details['custom_clearance_fee']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'custom_clearance_fee',
                "amount" => trim($order_details['custom_clearance_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
          //------------------------------------------------------
          //Update loading_unloading_charges in gonagoo account master
            if(trim($order_details['loading_unloading_charges']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'loading_unloading_charges',
                "amount" => trim($order_details['loading_unloading_charges']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
          //------------------------------------------------------

          //Add payment details in customer account master and history
            if($this->api->customer_account_master_details($cust_id, trim($data['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($data['currency_sign']),
              );
              $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
            }

            $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
     
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($total_amount_paid),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($data['currency_sign']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------

          //Deduct payment details from customer account master and history
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));

            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($total_amount_paid),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($data['currency_sign']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------
        /*************************************** Payment Section ****************************************/ 
      }         
    }
  }
  public function android_payment_by_wecashup()
  { 
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $merchant_uid = 'kZudktr3nxX0AMM633dXyHEesV13';            // Replace with your merchant_uid
    $merchant_public_key = 'DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl'; // Replace with your merchant_public_key !!!
    $merchant_secret = 'XKjy7SzenXCvF9N0';                       // Replace with your merchant_private_key !!!
    $transaction_uid = '';// create an empty transaction_uid
    $transaction_token  = '';// create an empty transaction_token
    $transaction_provider_name  = ''; // create an empty transaction_provider_name
    $transaction_confirmation_code  = ''; // create an empty confirmation code
    $today = date('Y-m-d h:i:s');

    if(isset($_POST['transaction_uid'])){ $transaction_uid = $this->input->post('transaction_uid', TRUE);   } 
    if(isset($_POST['transaction_token'])){  $transaction_token  = $this->input->post('transaction_token', TRUE);  } 
    if(isset($_POST['transaction_provider_name'])){  $transaction_provider_name  = $this->input->post('transaction_provider_name', TRUE);   }
    if(isset($_POST['transaction_confirmation_code'])){  $transaction_confirmation_code  = $this->input->post('transaction_confirmation_code', TRUE); } 
    $url = 'https://www.wecashup.com/api/v1.0/merchants/'.$merchant_uid.'/transactions/'.$transaction_uid.'/?merchant_public_key='.$merchant_public_key;

    $fields_string = '';
    $fields = array(
      'merchant_secret' => urlencode($merchant_secret),
      'transaction_token' => urlencode($transaction_token),
      'transaction_uid' => urlencode($transaction_uid),
      'transaction_confirmation_code' => urlencode($transaction_confirmation_code),
      'transaction_provider_name' => urlencode($transaction_provider_name),
      '_method' => urlencode('PATCH')
    );
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);

    $data = json_decode($server_output, true);
    $payment_data = array();
    if($data['response_status'] =="success"){

      $payment_data = array(
        "cust_id" => (int)$data['response_content']['transaction']['transaction_sender_reference'],
        "order_id" => (int)$data['response_content']['transaction']['transaction_receiver_reference'],
        "payment_method" => trim($data['response_content']['transaction']['transaction_provider_name']),
        "transaction_id" => trim($data['response_content']['transaction']['transaction_uid']),
        "total_amount_paid" => trim($data['response_content']['transaction']['transaction_receiver_total_amount']),
        "transfer_account_number" => trim($data['response_content']['transaction']['transaction_sender_uid']),
        "currency_sign" => trim($data['response_content']['transaction']['transaction_receiver_currency']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => "NULL",
        "payment_response" => json_encode($data),
        "payment_by" => "android",
        "complete_paid" => 1,
      );
      $this->update_order_payment_for_wecashup($payment_data);
            
      $location = base_url('payment/success');
    } 
    else {  $location = base_url('payment/failed');  }

    echo '<script>top.window.location = "'.$location.'"</script>';
  }
  public function ios_payment_by_wecashup()
  { 
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $merchant_uid = 'kZudktr3nxX0AMM633dXyHEesV13';            // Replace with your merchant_uid
    $merchant_public_key = 'DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl'; // Replace with your merchant_public_key !!!
    $merchant_secret = 'XKjy7SzenXCvF9N0';                       // Replace with your merchant_private_key !!!
    $transaction_uid = '';// create an empty transaction_uid
    $transaction_token  = '';// create an empty transaction_token
    $transaction_provider_name  = ''; // create an empty transaction_provider_name
    $transaction_confirmation_code  = ''; // create an empty confirmation code
    $today = date('Y-m-d h:i:s');

    if(isset($_POST['transaction_uid'])){ $transaction_uid = $this->input->post('transaction_uid', TRUE);   } 
    if(isset($_POST['transaction_token'])){  $transaction_token  = $this->input->post('transaction_token', TRUE);  } 
    if(isset($_POST['transaction_provider_name'])){  $transaction_provider_name  = $this->input->post('transaction_provider_name', TRUE);   }
    if(isset($_POST['transaction_confirmation_code'])){  $transaction_confirmation_code  = $this->input->post('transaction_confirmation_code', TRUE); } 
    $url = 'https://www.wecashup.com/api/v1.0/merchants/'.$merchant_uid.'/transactions/'.$transaction_uid.'/?merchant_public_key='.$merchant_public_key;

    $fields_string = '';
    $fields = array(
      'merchant_secret' => urlencode($merchant_secret),
      'transaction_token' => urlencode($transaction_token),
      'transaction_uid' => urlencode($transaction_uid),
      'transaction_confirmation_code' => urlencode($transaction_confirmation_code),
      'transaction_provider_name' => urlencode($transaction_provider_name),
      '_method' => urlencode('PATCH')
    );
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);

    $data = json_decode($server_output, true);
    $payment_data = array();
    if($data['response_status'] =="success"){

      $payment_data = array(
        "cust_id" => (int)$data['response_content']['transaction']['transaction_sender_reference'],
        "order_id" => (int)$data['response_content']['transaction']['transaction_receiver_reference'],
        "payment_method" => trim($data['response_content']['transaction']['transaction_provider_name']),
        "transaction_id" => trim($data['response_content']['transaction']['transaction_uid']),
        "total_amount_paid" => trim($data['response_content']['transaction']['transaction_receiver_total_amount']),
        "transfer_account_number" => trim($data['response_content']['transaction']['transaction_sender_uid']),
        "currency_sign" => trim($data['response_content']['transaction']['transaction_receiver_currency']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => "NULL",
        "payment_response" => json_encode($data),
        "payment_by" => "ios",
        "complete_paid" => 1,
      );
      $this->update_order_payment_for_wecashup($payment_data);
            
      $location = base_url('payment/success');
    } 
    else {  $location = base_url('payment/failed');  }

    echo '<script>top.window.location = "'.$location.'"</script>';
  }
  public function payment_by_orange()
  {
    //echo json_encode($_POST); die();
    $order_id = $this->input->post('order_id',true);
    $order_details = $this->api->get_order_detail($order_id);
    $order_payment_id = $this->input->post('order_payment_id',true); //'gonagoo_'.time().'_'.$order_id
    $amount = $this->input->post('amount',true);
    $pay_token = $this->input->post('pay_token',true);
    $payment_method = 'orange';
    $device_type = $this->input->post('device_type',true);
    $today = date('Y-m-d h:i:s');
    /*//get access token
    $ch = curl_init("https://api.orange.com/oauth/v2/token?");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
    $orange_token = curl_exec($ch);
    curl_close($ch);
    $token_details = json_decode($orange_token, TRUE);
    $token = 'Bearer '.$token_details['access_token'];
    // get payment link
    $json_upost_data = json_encode(
                        array(
                          "order_id" => $order_payment_id, 
                          "amount" => $amount, 
                          "pay_token" => $pay_token
                        )
                      );
    //echo $json_post_data; die();
    $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
      "Athorization: ".$token,
      "Accept: application/json"
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
    $payment_res = curl_exec($ch);
    curl_close($ch);
    $payment_details = json_decode($payment_res, TRUE);
    $transaction_id = $payment_details['txnid'];
    echo json_encode($payment_details); die();*/

    $payment_details = array('payment'=>'123');
    $transaction_id = 123;

    //if($payment_details['status'] == 'SUCCESS') {
    if(1) {
      $payment_data = array(
        "cust_id" => (int)$order_details['cust_id'],
        "order_id" => (int)$order_details['order_id'],
        "payment_method" => 'orange',
        "transaction_id" => trim($transaction_id),
        "total_amount_paid" => trim($amount),
        "transfer_account_number" => 'NULL',
        "currency_sign" => trim($order_details['currency_sign']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => "NULL",
        "payment_response" => json_encode($payment_details),
        "payment_by" => $device_type,
        "complete_paid" => 1,
      );
      $this->update_order_payment_for_wecashup($payment_data);
      $order_details = $this->api->get_order_detail((int)$order_id);
      //echo json_encode($order_details); die();
      $this->response = ['response' => 'success'];
    } else { $this->response = ['response' => 'failed']; }
    echo json_encode($this->response);
  }
  public function success() { echo 'success'; }
  public function failed() {  echo 'failed'; }
}

/* End of file Payment_laundry.php */
/* Location: ./application/controllers/Payment_laundry.php */