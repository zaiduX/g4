<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_panel extends CI_Controller { 
  private $cust_id = 0; 
  private $cust = array();

  public function __construct()
  {
    parent::__construct();
    
    $this->cust_id = $this->session->userdata("cust_id"); $this->cust_id = (int) $this->cust_id;

    $this->load->model('api_model', 'api');
    $this->load->model('Notification_model', 'notification');
    $this->load->model('Web_user_model', 'user');
    $this->load->model('Transport_model', 'transport'); 
    $this->load->model('Standard_dimension_model', 'dimension');
    $this->load->model('api_model_sms', 'api_sms'); 
  
    $this->cust = $this->api->get_consumer_datails($this->cust_id, true); $cust = $this->cust;
    
    if( ( $this->session->userdata("is_logged_in") == "NULL" ) OR ( $this->session->userdata("is_logged_in") !== 1 ) ) { redirect('log-in'); }
    
    if($cust['mobile_verified'] == 0) { redirect('verification/otp'); }

    $lang = $this->session->userdata('language');
    $this->config->set_item('language', strtolower($lang));

    if(trim($lang) == 'french'){
      $this->lang->load('frenchApi_lang','french');
      $this->lang->load('frenchFront_lang','french');
    } else if(trim($lang) == 'spanish'){  
      $this->lang->load('spanishApi_lang','spanish');
      $this->lang->load('spanishFront_lang','spanish');
    } else { 
      $this->lang->load('englishApi_lang','english');
      $this->lang->load('englishFront_lang','english');
    }
    
    $this->api->update_last_login($cust['cust_id']);
    
    //echo json_encode($cust); die();
    if (!$this->input->is_ajax_request()){ 
      // var_dump($_POST); 
      $order_id = $this->input->post('order_id');
      $type = $this->input->post('type');

      // if($order_id && !$ow_id) { 
      if($order_id && ($type =="workroom")) { 
        $ids = $this->api->get_total_unread_order_workroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) {  $this->api->make_workroom_notification_read($v['ow_id'], $cust['cust_id']);  }
      }
      if($order_id && ($type =="chatroom")) { 
        $ids = $this->api->get_total_unread_order_chatroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) { $this->api->make_chatrooom_notification_read($v['chat_id'], $cust['cust_id']);  }
      }
      if($cust['user_type'] == 1 ){ $user_type ="Individuals"; } else { $user_type ="Business"; }
      $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust['cust_id'],$user_type );
      //echo $this->db->last_query(); die();
      $notification = $this->api->get_total_unread_notifications($cust['cust_id'],$user_type, 5);
      $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust['cust_id']);      
      $nt_workroom = $this->api->get_total_unread_workroom_notifications($cust['cust_id'], 5);  
      //echo json_encode($total_workroom_notifications); die();
      $total_chatroom_notifications = $this->api->get_total_unread_chatroom_notifications_count($cust['cust_id']);      
      $nt_chatroom = $this->api->get_total_unread_chatroom_notifications($cust['cust_id'], 5);  
      $compact = ["total_unread_notifications","notification","total_workroom_notifications","nt_workroom","total_chatroom_notifications","nt_chatroom"];

      $method_name = $this->router->fetch_method(); 
      if($method_name == 'landing') { 
        $this->load->view('user_panel/header-landing',compact($compact)); 
        $this->load->view('user_panel/menu-landing', compact("cust"));
      } else { 
        $this->load->view('user_panel/header',compact($compact)); 
        $this->load->view('user_panel/menu', compact("cust")); }
    }
  }

  public function landing_old()
  {   
    $cust = $this->cust;
    $category = $this->api->get_category();  
    $this->load->view('user_panel/landing_view', compact('category'));
    $this->load->view('user_panel/footer');   
  }

  public function landing()
  { 
    $cust = $this->cust;
    $category_type = $this->api->get_category_type();
    $category = $this->api->get_category();  
    $this->load->view('user_panel/dashboard', compact('category_type','category'));   

    $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
    $current_balance = ($current_balance !=NULL && $current_balance !=null)?$current_balance['account_balance']:0;

    $total_earned = $this->user->get_total_earned($cust['cust_id'],'XAF'); 
    $total_earned = $total_earned['amount'];

    //total earned in service
    $total_earned_service = $this->user->get_total_earned_service_courier_transport($cust['cust_id'], 'XAF', 7); 
    $total_earned_service = $total_earned_service['amount'];
    //total spend in service
    $total_spend_service = $this->user->get_total_spend_service_courier_transport($cust['cust_id'], 'XAF', 7); 
    $total_spend_service = $total_spend_service['amount'];

    $jobs = $this->user->get_total_request($cust['cust_id']);   
    $total_jobs = ($jobs>0)?COUNT($jobs):0;

    $accepted_jobs = $this->user->get_booking_list($cust['cust_id'],'accept');  
    $total_accepted_jobs = ($accepted_jobs>0)?COUNT($accepted_jobs):0;

    $in_progress_jobs = $this->user->get_booking_list($cust['cust_id'],'in_progress');  
    $total_in_progress_jobs = ($in_progress_jobs)?COUNT($in_progress_jobs):0;

    $delivered_jobs = $this->user->get_booking_list($cust['cust_id'],'delivered');  
    $total_delivered_jobs = ($delivered_jobs)?COUNT($delivered_jobs):0;

    $open_jobs = $this->user->get_booking_list($cust['cust_id'],'open');  
    $total_inmarket_jobs = ($open_jobs>0)?COUNT($open_jobs):0;

    $accepted_inprogress_jobs = $total_accepted_jobs + $total_in_progress_jobs;
    $total_awarded_jobs = $total_accepted_jobs + $total_in_progress_jobs + $total_delivered_jobs;

    if($total_awarded_jobs>0){  
      $posted_progress = number_format((($total_delivered_jobs * 100 ) / $total_awarded_jobs),'2'); 
      $total_awareded = number_format((($accepted_inprogress_jobs * 100 ) / $total_awarded_jobs),'2'); 
      $total_delivered = number_format((($total_delivered_jobs * 100 ) / $total_awarded_jobs),'2'); 
    } else { $posted_progress=0; $total_awareded = 0; $total_delivered = 0; }

    $total_working_jobs = $total_accepted_jobs + $total_in_progress_jobs;
    if($total_working_jobs>0){
      $total_accepted = number_format((($total_accepted_jobs * 100 ) /  $total_working_jobs),'2');
      $working_progress = number_format((($total_in_progress_jobs * 100 ) /  $total_working_jobs),'2');
      $completed_progress =  number_format((($total_in_progress_jobs * 100 ) / $total_working_jobs),'2');
    } else { $working_progress = 0; $completed_progress = 0; $total_accepted = 0; }

    $i = $j = 0; 
    $latest_jobs = $latest_inprogress_jobs = array();
    foreach ($accepted_jobs as $v) {  if($i < 5 ) { $latest_jobs[] = $v;} else { break; }  $i++; }
    foreach ($in_progress_jobs as $v) { if($j < 5 ) { $latest_inprogress_jobs[] = $v;} else { break; }  $j++; }

    // buyer
    $total_spend = $this->user->get_total_spend($cust['cust_id'],'XAF');
    //echo json_encode($total_spend);
    //$total_spend = $total_spend['amount']; 
    $total_spend = ($total_spend !=NULL && $total_spend !=null)?$total_spend['amount']:0;

    $jobs2 = $this->user->get_order_list($cust['cust_id']);   
    $total_jobs2 = ($jobs2>0)?COUNT($jobs2):0;

    $accepted_jobs2 = $this->user->get_order_list($cust['cust_id'],'accept'); 
    $total_accepted_jobs2 = ($accepted_jobs2>0)?COUNT($accepted_jobs2):0;

    $in_progress_jobs2 = $this->user->get_order_list($cust['cust_id'],'in_progress'); 
    $total_in_progress_jobs2 = ($in_progress_jobs2)?COUNT($in_progress_jobs2):0;

    $delivered_jobs2 = $this->user->get_order_list($cust['cust_id'],'delivered'); 
    $total_delivered_jobs2 = ($delivered_jobs2)?COUNT($delivered_jobs2):0;

    $open_jobs2 = $this->user->get_order_list($cust['cust_id'],'open'); 
    $total_inmarket_jobs2 = ($open_jobs2>0)?COUNT($open_jobs2):0;

    $total_awarded_jobs2 = $total_accepted_jobs2 + $total_in_progress_jobs2 + $total_delivered_jobs2;
    if($total_jobs2>0){ 
      $total_inmarket_jobs2 = number_format((($total_inmarket_jobs2 * 100 ) / $total_jobs2),'2'); 
      $posted_progress2 = number_format((($total_awarded_jobs2 * 100 ) / $total_jobs2),'2'); 
      $total_delivered2 = number_format((($total_delivered_jobs2 * 100 ) / $total_jobs2),'2'); 
    } else{ $posted_progress2 =0; $total_inmarket_jobs2 = 0; $total_delivered2 = 0;}

    $total_working_jobs2 = $total_accepted_jobs + $total_in_progress_jobs;
    if($total_working_jobs2>0){
      $total_accepted_jobs2 = number_format((($total_accepted_jobs2 * 100 ) /  $total_working_jobs2),'2');
      $working_progress2 = number_format((($total_in_progress_jobs2 * 100 ) /  $total_working_jobs2),'2');
      $completed_progress2 =  number_format((($total_in_progress_jobs2 * 100 ) / $total_working_jobs2),'2');
    } else{ $working_progress2 = 0; $completed_progress2 = 0; }

    $i2 = $j2 = 0; 
    $latest_jobs2 = $latest_inprogress_jobs2 = array();
    foreach ($jobs2 as $v) {  if($i2 < 5 ) { $latest_jobs2[] = $v;} else { break; }  $i2++; }
    foreach ($in_progress_jobs2 as $v) {  if($j2 < 5 ) { $latest_inprogress_jobs2[] = $v;} else { break; }  $j2++; }

    $recent_order_reviews = $this->user->get_recent_order_reviews($cust['cust_id']);

    $compact = compact('cust','total_jobs','total_awareded','total_inmarket_jobs','posted_progress','total_accepted_jobs', 'total_in_progress_jobs','working_progress','total_working_jobs','total_delivered','completed_progress', 'latest_inprogress_jobs','latest_jobs','current_balance','total_earned','total_awarded_jobs','total_accepted','total_jobs2','total_awarded_jobs2','total_inmarket_jobs2','posted_progress2','total_accepted_jobs2', 'total_in_progress_jobs2','working_progress2','total_working_jobs2','total_delivered_jobs2','completed_progress2','latest_inprogress_jobs2','latest_jobs2','total_spend','total_delivered2','recent_order_reviews', 'total_earned_service', 'total_spend_service');
    
    $this->load->view('user_panel/landing_view', $compact);
    $this->load->view('user_panel/footer');   
  }

  public function dashboard()
  {   
    //echo json_encode($_SESSION); die();
    $cust = $this->cust;
    $category_type = $this->api->get_category_type();
    $category = $this->api->get_category();
    $this->load->view('user_panel/dashboard_courier', compact('category_type','category'));
    
    if($_SESSION['acc_type'] == 'buyer') {
      $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
      $current_balance = ($current_balance != NULL ) ? $current_balance['account_balance'] : 0;

      $total_spend = $this->user->get_total_spend($cust['cust_id'],'XAF');    
      $total_spend = ($total_spend !=NULL)?$total_spend['amount']:0;

      $jobs = $this->user->get_booking_list($cust['cust_id']);    
      $total_jobs = ($jobs>0)?COUNT($jobs):0;

      $accepted_jobs = $this->user->get_booking_list($cust['cust_id'],'accept');  
      $total_accepted_jobs = ($accepted_jobs>0)?COUNT($accepted_jobs):0;
      $in_progress_jobs = $this->user->get_booking_list($cust['cust_id'],'in_progress');  
      $total_in_progress_jobs = ($in_progress_jobs)?COUNT($in_progress_jobs):0;
      $delivered_jobs = $this->user->get_booking_list($cust['cust_id'],'delivered');  
      $total_delivered_jobs = ($delivered_jobs)?COUNT($delivered_jobs):0;
      $open_jobs = $this->user->get_booking_list($cust['cust_id'],'open');  
      $total_inmarket_jobs = ($open_jobs>0)?COUNT($open_jobs):0;

      $total_awarded_jobs = $total_accepted_jobs + $total_in_progress_jobs + $total_delivered_jobs;
      if($total_jobs>0){  
        $total_inmarket_jobs = number_format((($total_inmarket_jobs * 100 ) / $total_jobs),'2'); 
        $posted_progress = number_format((($total_awarded_jobs * 100 ) / $total_jobs),'2'); 
        $total_delivered = number_format((($total_delivered_jobs * 100 ) / $total_jobs),'2'); 
      } else{ $posted_progress =0; $total_inmarket_jobs = 0; $total_delivered = 0;}

      $total_working_jobs = $total_accepted_jobs + $total_in_progress_jobs;
      if($total_working_jobs>0){
        $total_accepted_jobs = number_format((($total_accepted_jobs * 100 ) /  $total_working_jobs),'2');
        $working_progress = number_format((($total_in_progress_jobs * 100 ) /  $total_working_jobs),'2');
        $completed_progress =  number_format((($total_in_progress_jobs * 100 ) / $total_working_jobs),'2');
      } else{ $working_progress = 0; $completed_progress = 0; }

      $i = $j = 0; 
      $latest_jobs = $latest_inprogress_jobs = array();
      foreach ($jobs as $v) { if($i < 5 ) { $latest_jobs[] = $v;} else { break; }  $i++; }
      foreach ($in_progress_jobs as $v) { if($j < 5 ) { $latest_inprogress_jobs[] = $v;} else { break; }  $j++; }

      $compact = compact('cust','total_jobs','total_awarded_jobs','total_inmarket_jobs','posted_progress','total_accepted_jobs', 'total_in_progress_jobs','working_progress','total_working_jobs','total_delivered_jobs','completed_progress', 'latest_inprogress_jobs','latest_jobs', 'current_balance','total_spend','total_delivered');

      $this->load->view('user_panel/buyer_dashboard',$compact);
    } else if($_SESSION['acc_type'] == 'seller') {  
      $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
      
      $total_earned = $this->user->get_total_earned($cust['cust_id'],'XAF'); 
      $total_earned = $total_earned['amount'];
      
      //total earned in service
      $total_earned_service = $this->user->get_total_earned_service_courier_transport($cust['cust_id'], 'XAF', 7); 
      $total_earned_service = $total_earned_service['amount'];
      //total spend in service
      $total_spend_service = $this->user->get_total_spend_service_courier_transport($cust['cust_id'], 'XAF', 7); 
      $total_spend_service = $total_spend_service['amount'];

      $jobs = $this->user->get_total_request($cust['cust_id']);   
      $total_jobs = ($jobs>0)?COUNT($jobs):0;

      $accepted_jobs = $this->user->get_order_list($cust['cust_id'],'accept');  
      $total_accepted_jobs = ($accepted_jobs>0)?COUNT($accepted_jobs):0;

      $in_progress_jobs = $this->user->get_order_list($cust['cust_id'],'in_progress');  
      $total_in_progress_jobs = ($in_progress_jobs)?COUNT($in_progress_jobs):0;

      $delivered_jobs = $this->user->get_order_list($cust['cust_id'],'delivered');  
      $total_delivered_jobs = ($delivered_jobs)?COUNT($delivered_jobs):0;

      $open_jobs = $this->user->get_order_list($cust['cust_id'],'open');  
      $total_inmarket_jobs = ($open_jobs>0)?COUNT($open_jobs):0;

      $accepted_inprogress_jobs = $total_accepted_jobs + $total_in_progress_jobs;
      $total_awarded_jobs = $total_accepted_jobs + $total_in_progress_jobs + $total_delivered_jobs;

      if($total_awarded_jobs>0){  
        $posted_progress = number_format((($total_delivered_jobs * 100 ) / $total_awarded_jobs),'2'); 
        $total_awareded = number_format((($accepted_inprogress_jobs * 100 ) / $total_awarded_jobs),'2'); 
        $total_delivered = number_format((($total_delivered_jobs * 100 ) / $total_awarded_jobs),'2'); 
      } else{ $posted_progress=0; $total_awareded = 0; $total_delivered = 0; }

      $total_working_jobs = $total_accepted_jobs + $total_in_progress_jobs;
      if($total_working_jobs>0){
        $total_accepted = number_format((($total_accepted_jobs * 100 ) /  $total_working_jobs),'2');
        $working_progress = number_format((($total_in_progress_jobs * 100 ) /  $total_working_jobs),'2');
        $completed_progress =  number_format((($total_in_progress_jobs * 100 ) / $total_working_jobs),'2');
      } else{ $working_progress = 0; $completed_progress = 0; $total_accepted = 0; }

      $i = $j = 0; 
      $latest_jobs = $latest_inprogress_jobs = array();
      foreach ($accepted_jobs as $v) {  if($i < 5 ) { $latest_jobs[] = $v;} else { break; }  $i++; }
      foreach ($in_progress_jobs as $v) { if($j < 5 ) { $latest_inprogress_jobs[] = $v;} else { break; }  $j++; }

      $recent_order_reviews = $this->user->get_recent_order_reviews($cust['cust_id']);
    
      $compact = compact('cust','total_jobs','total_awareded','total_inmarket_jobs','posted_progress','total_accepted_jobs', 'total_in_progress_jobs','working_progress','total_working_jobs','total_delivered','completed_progress', 'latest_inprogress_jobs','latest_jobs','current_balance','total_earned','total_awarded_jobs','total_accepted','recent_order_reviews', 'total_earned_service', 'total_spend_service');
      
      //echo json_encode($compact); die();
      
      $this->load->view('user_panel/seller_dashboard', $compact);
    } else{ //Both      
      $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
      $current_balance = ($current_balance !=NULL && $current_balance !=null)?$current_balance['account_balance']:0;

      $total_earned = $this->user->get_total_earned($cust['cust_id'],'XAF'); 
      $total_earned = $total_earned['amount'];

      //total earned in service
      $total_earned_service = $this->user->get_total_earned_service_courier_transport($cust['cust_id'], 'XAF', 7); 
      $total_earned_service = $total_earned_service['amount'];
      //total spend in service
      $total_spend_service = $this->user->get_total_spend_service_courier_transport($cust['cust_id'], 'XAF', 7); 
      $total_spend_service = $total_spend_service['amount'];

      $jobs = $this->user->get_total_request($cust['cust_id']);   
      $total_jobs = ($jobs>0)?COUNT($jobs):0;

      $accepted_jobs = $this->user->get_booking_list($cust['cust_id'],'accept');  
      $total_accepted_jobs = ($accepted_jobs>0)?COUNT($accepted_jobs):0;

      $in_progress_jobs = $this->user->get_booking_list($cust['cust_id'],'in_progress');  
      $total_in_progress_jobs = ($in_progress_jobs)?COUNT($in_progress_jobs):0;

      $delivered_jobs = $this->user->get_booking_list($cust['cust_id'],'delivered');  
      $total_delivered_jobs = ($delivered_jobs)?COUNT($delivered_jobs):0;

      $open_jobs = $this->user->get_booking_list($cust['cust_id'],'open');  
      $total_inmarket_jobs = ($open_jobs>0)?COUNT($open_jobs):0;

      $accepted_inprogress_jobs = $total_accepted_jobs + $total_in_progress_jobs;
      $total_awarded_jobs = $total_accepted_jobs + $total_in_progress_jobs + $total_delivered_jobs;

      if($total_awarded_jobs>0){  
        $posted_progress = number_format((($total_delivered_jobs * 100 ) / $total_awarded_jobs),'2'); 
        $total_awareded = number_format((($accepted_inprogress_jobs * 100 ) / $total_awarded_jobs),'2'); 
        $total_delivered = number_format((($total_delivered_jobs * 100 ) / $total_awarded_jobs),'2'); 
      } else { $posted_progress=0; $total_awareded = 0; $total_delivered = 0; }

      $total_working_jobs = $total_accepted_jobs + $total_in_progress_jobs;
      if($total_working_jobs>0){
        $total_accepted = number_format((($total_accepted_jobs * 100 ) /  $total_working_jobs),'2');
        $working_progress = number_format((($total_in_progress_jobs * 100 ) /  $total_working_jobs),'2');
        $completed_progress =  number_format((($total_in_progress_jobs * 100 ) / $total_working_jobs),'2');
      } else { $working_progress = 0; $completed_progress = 0; $total_accepted = 0; }

      $i = $j = 0; 
      $latest_jobs = $latest_inprogress_jobs = array();
      foreach ($accepted_jobs as $v) {  if($i < 5 ) { $latest_jobs[] = $v;} else { break; }  $i++; }
      foreach ($in_progress_jobs as $v) { if($j < 5 ) { $latest_inprogress_jobs[] = $v;} else { break; }  $j++; }

      // buyer
      $total_spend = $this->user->get_total_spend($cust['cust_id'],'XAF');
      //echo json_encode($total_spend);
      //$total_spend = $total_spend['amount']; 
      $total_spend = ($total_spend !=NULL && $total_spend !=null)?$total_spend['amount']:0;

      $jobs2 = $this->user->get_order_list($cust['cust_id']);   
      $total_jobs2 = ($jobs2>0)?COUNT($jobs2):0;

      $accepted_jobs2 = $this->user->get_order_list($cust['cust_id'],'accept'); 
      $total_accepted_jobs2 = ($accepted_jobs2>0)?COUNT($accepted_jobs2):0;

      $in_progress_jobs2 = $this->user->get_order_list($cust['cust_id'],'in_progress'); 
      $total_in_progress_jobs2 = ($in_progress_jobs2)?COUNT($in_progress_jobs2):0;

      $delivered_jobs2 = $this->user->get_order_list($cust['cust_id'],'delivered'); 
      $total_delivered_jobs2 = ($delivered_jobs2)?COUNT($delivered_jobs2):0;

      $open_jobs2 = $this->user->get_order_list($cust['cust_id'],'open'); 
      $total_inmarket_jobs2 = ($open_jobs2>0)?COUNT($open_jobs2):0;

      $total_awarded_jobs2 = $total_accepted_jobs2 + $total_in_progress_jobs2 + $total_delivered_jobs2;
      if($total_jobs2>0){ 
        $total_inmarket_jobs2 = number_format((($total_inmarket_jobs2 * 100 ) / $total_jobs2),'2'); 
        $posted_progress2 = number_format((($total_awarded_jobs2 * 100 ) / $total_jobs2),'2'); 
        $total_delivered2 = number_format((($total_delivered_jobs2 * 100 ) / $total_jobs2),'2'); 
      } else{ $posted_progress2 =0; $total_inmarket_jobs2 = 0; $total_delivered2 = 0;}

      $total_working_jobs2 = $total_accepted_jobs + $total_in_progress_jobs;
      if($total_working_jobs2>0){
        $total_accepted_jobs2 = number_format((($total_accepted_jobs2 * 100 ) /  $total_working_jobs2),'2');
        $working_progress2 = number_format((($total_in_progress_jobs2 * 100 ) /  $total_working_jobs2),'2');
        $completed_progress2 =  number_format((($total_in_progress_jobs2 * 100 ) / $total_working_jobs2),'2');
      } else{ $working_progress2 = 0; $completed_progress2 = 0; }

      $i2 = $j2 = 0; 
      $latest_jobs2 = $latest_inprogress_jobs2 = array();
      foreach ($jobs2 as $v) {  if($i2 < 5 ) { $latest_jobs2[] = $v;} else { break; }  $i2++; }
      foreach ($in_progress_jobs2 as $v) {  if($j2 < 5 ) { $latest_inprogress_jobs2[] = $v;} else { break; }  $j2++; }

      $recent_order_reviews = $this->user->get_recent_order_reviews($cust['cust_id']);

      $compact = compact('cust','total_jobs','total_awareded','total_inmarket_jobs','posted_progress','total_accepted_jobs', 'total_in_progress_jobs','working_progress','total_working_jobs','total_delivered','completed_progress', 'latest_inprogress_jobs','latest_jobs','current_balance','total_earned','total_awarded_jobs','total_accepted','total_jobs2','total_awarded_jobs2','total_inmarket_jobs2','posted_progress2','total_accepted_jobs2', 'total_in_progress_jobs2','working_progress2','total_working_jobs2','total_delivered_jobs2','completed_progress2','latest_inprogress_jobs2','latest_jobs2','total_spend','total_delivered2','recent_order_reviews', 'total_earned_service', 'total_spend_service');

      $this->load->view('user_panel/both_dashboard',$compact);    
    }
    $this->load->view('user_panel/footer'); 
  }

  public function user_profile()
  { 
    $cust = $this->cust;

    $total_edu = $this->user->get_total_educations($cust['cust_id']);
    $education = $this->user->get_customer_educations($cust['cust_id'],1);

    $total_exp = $this->user->get_total_experiences($cust['cust_id'],false);
    $total_other_exp = $this->user->get_total_experiences($cust['cust_id'],true);

    $experience = $this->user->get_customer_experiences($cust['cust_id'],false, 1);
    $other_experience = $this->user->get_customer_experiences($cust['cust_id'],true, 1);

    $skills = $this->user->get_customer_skills($cust['cust_id'], 1);

    $total_portfolios = $this->user->get_total_portfolios($cust['cust_id']);
    $portfolio = $this->user->get_customer_portfolio($cust['cust_id'], 1);

    $total_docs = $this->user->get_total_documents($cust['cust_id']);
    $docs = $this->user->get_customer_documents($cust['cust_id'], 1);

    $total_pay_methods = $this->user->get_total_payment_methods($cust['cust_id']);
    $pay_methods = $this->user->get_customer_payment_methods($cust['cust_id'], 1);

    $per_counter = 0; 
    $total_counters = 15;

    if($cust['email_verified'] == 1) { $per_counter += 1; }
    if($cust['mobile_verified'] == 1) { $per_counter += 1; }
    if($cust['firstname'] != "NULL") { $per_counter += 1; }
    if($cust['lastname'] != "NULL") { $per_counter += 1; }
    if($cust['gender'] != "NULL") { $per_counter += 1; }
    if($cust['overview'] != "NULL") { $per_counter += 1; }
    if($cust['lang_known'] != "NULL") { $per_counter += 1; }
    if($cust['country_id'] > 0) { $per_counter += 1; }
    if($cust['state_id'] > 0) { $per_counter += 1; }
    if($cust['city_id'] > 0) { $per_counter += 1; }
    if($cust['avatar_url'] != "NULL") { $per_counter += 1; }
    if($total_edu > 0) { $per_counter += 1; }
    if($total_exp > 0) { $per_counter += 1; }
    if($experience) { $per_counter += 1; }
    if($skills) { $per_counter += 1; }

    $percentage = (int) (((int) $per_counter / (int) $total_counters ) *100 );

    $compact = compact('total_edu','education','total_exp','total_other_exp','experience','other_experience','skills','portfolio','total_portfolios','total_docs','docs','total_pay_methods','pay_methods', 'percentage');
    // echo json_encode($skills); die();
    $this->load->view('user_panel/user_profile_view', $compact);
    $this->load->view('user_panel/footer');
  }

  public function deliverer_profile_old()
  {
    $deliverer = $this->api->get_deliverer_profile($this->cust_id);
    $documents = $this->api->get_deliverer_documents($this->cust_id);
    $this->load->view('user_panel/deliverer_profile_view', compact('deliverer','documents'));
    $this->load->view('user_panel/footer');
  }

  public function deliverer_profile()
  {
    if($deliverer = $this->api->get_deliverer_profile($this->cust_id)) {
      $documents = $this->api->get_deliverer_documents($this->cust_id);
      $this->load->view('user_panel/deliverer_profile_view', compact('deliverer','documents'));
    } else {
      $cust = $this->cust;
      $countries = $this->user->get_countries($this->cust_id);
      $carrier_type = $this->user->carrier_type_list(); 
      $cust_id = $this->cust_id;
      $this->load->view('user_panel/create_deliverer_profile_view', compact('cust_id','countries','cust','carrier_type'));
    }
    $this->load->view('user_panel/footer');
  }

  public function create_deliverer_profile()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->cust_id;
    $firstname = $this->input->post('firstname', TRUE);
    $lastname = $this->input->post('lastname', TRUE);
    $email_id = $this->input->post('email_id', TRUE);
    $contact_no = $this->input->post('contact_no', TRUE);
    $carrier_type = $this->input->post('carrier_type', TRUE);
    $address = $this->input->post('address', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $state_id = $this->input->post('state_id', TRUE);
    
    if($state_id<=0) {
      $state_id = $this->input->post('state_id_hidden', TRUE);
    }
    $city_id = $this->input->post('city_id', TRUE);
    if($city_id<=0) {
      $city_id = $this->input->post('city_id_hidden', TRUE);
    }

    $zipcode = $this->input->post('zipcode', TRUE);
    $promocode = $this->input->post('promocode', TRUE);
    $company_name = $this->input->post('company_name', TRUE);
    $introduction = $this->input->post('introduction', TRUE);
    $latitude = $this->input->post('latitude', TRUE);
    $longitude = $this->input->post('longitude', TRUE);
    $gender = $this->input->post('gender', TRUE);
    $service_list = $this->input->post('service_list', TRUE); $service_list = implode(',',$service_list);
    $today = date("Y-m-d H:i:s");

    //$profile = $this->api->get_business_datails($cust_id);
    if( !empty($_FILES["avatar"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/profile-images/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('avatar')) {   
        $uploads    = $this->upload->data();  
        $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
      } else { $avatar_url = "NULL"; }
    } else { $avatar_url = "NULL"; }

    if( !empty($_FILES["cover"]["tmp_name"]) ) {
    $config['upload_path']    = 'resources/cover-images/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('cover')) {   
        $uploads    = $this->upload->data();  
        $cover_url =  $config['upload_path'].$uploads["file_name"]; 
      } else{ $cover_url = "NULL"; }
    } else{ $cover_url = "NULL"; }

    $insert_data = array(
      "firstname" => trim($firstname),
      "lastname" => trim($lastname),
      "email_id" => trim($email_id),
      "contact_no" => trim($contact_no),
      "address" => trim($address),
      "zipcode" => trim($zipcode),
      "promocode" => trim($promocode),
      "carrier_type" => trim($carrier_type),
      "country_id" => (int)($country_id),
      "state_id" => (int)($state_id),
      "city_id" => (int)($city_id),
      "company_name" => trim($company_name),
      "introduction" => trim($introduction),
      "latitude" => trim($latitude),
      "longitude" => trim($longitude),
      "gender" => trim($gender),
      "avatar_url" => trim($avatar_url),
      "cover_url" => trim($cover_url),
      "service_list" => $service_list,
      "cust_id" => (int)$cust_id,
      "shipping_mode" => 0,
      "cre_datetime" => $today,
    );
    //echo json_encode($insert_data); die();
    if($this->user->create_deliverer_profile($insert_data)) {
      $this->api->make_consumer_deliverer($cust_id);
      $this->session->set_flashdata('success', $this->lang->line('profile_created_successfully!'));
    } else{ $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found!')); }
    redirect('user-panel/deliverer-profile');
  }

  public function edit_deliverer_profile()
  {
    $deliverer = $this->api->get_deliverer_profile($this->cust_id);
    $countries = $this->user->get_countries($this->cust_id);
    $carrier_type = $this->user->carrier_type_list();
    $this->load->view('user_panel/edit_deliverer_profile_view', compact('deliverer','countries','carrier_type'));
    $this->load->view('user_panel/footer');
  }

  public function update_deliverer_profile()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->cust_id;
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $email_id = $this->input->post('email_id', TRUE);
      $contact_no = $this->input->post('contact_no', TRUE);
      $carrier_type = $this->input->post('carrier_type', TRUE);
      $address = $this->input->post('address', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      if($state_id<=0) {
        $state_id = $this->input->post('state_id_hidden', TRUE);
      }
      $city_id = $this->input->post('city_id', TRUE);
      if($city_id<=0) {
        $city_id = $this->input->post('city_id_hidden', TRUE);
      }
      $zipcode = $this->input->post('zipcode', TRUE);
      $promocode = $this->input->post('promocode', TRUE);
      $company_name = $this->input->post('company_name', TRUE);
      $introduction = $this->input->post('introduction', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      $gender = $this->input->post('gender', TRUE);
      $service_list = $this->input->post('service_list', TRUE); $service_list = implode(',',$service_list);
      $today = date("Y-m-d H:i:s");

      $profile = $this->api->get_business_datails($cust_id);
      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('avatar')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];  
          if( $profile['avatar_url'] != "NULL" ){ unlink($profile['avatar_url']); }
        }
        $avatar_update = $this->api->update_business_avatar($avatar_url, $cust_id);
      } else $avatar_update = 0;

      if( !empty($_FILES["cover"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/cover-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('cover')) {   
          $uploads    = $this->upload->data();  
          $cover_url =  $config['upload_path'].$uploads["file_name"]; 
          if( $profile['cover_url'] != "NULL" ){  unlink($profile['cover_url']);  }
        }
        $cover_update = $this->api->update_business_cover($cover_url, $cust_id);
      } else $cover_update = 0;

      $update_data = array(
        "firstname" => trim($firstname),
        "lastname" => trim($lastname),
        "email_id" => trim($email_id),
        "contact_no" => trim($contact_no),
        "address" => trim($address),
        "zipcode" => trim($zipcode),
        "promocode" => trim($promocode),
        "carrier_type" => trim($carrier_type),
        "country_id" => (int)($country_id),
        "state_id" => (int)($state_id),
        "city_id" => (int)($city_id),
        "company_name" => trim($company_name),
        "introduction" => trim($introduction),
        "latitude" => trim($latitude),
        "longitude" => trim($longitude),
        "service_list" => $service_list,
        "gender" => trim($gender),
      );

      //var_dump($update_data); die();

      //if(!$this->api->is_deliverer_email_exists($email_id, $cust_id)){
      if($this->api->update_deliverer($update_data, $cust_id) || $avatar_update || $cover_update ){
        $this->session->set_flashdata('success', $this->lang->line('profile_created_successfully!'));
      } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
      //} else{ $this->session->set_flashdata('error','Email already exists.'); }
      redirect('user-panel/edit-deliverer-profile');
    }
  }

  public function edit_deliverer_bank()
  {
    if( !is_null($this->input->post('cust_id')) ) { $cust_id = (int) $this->input->post('cust_id'); }
    else if(!is_null($this->uri->segment(3))) { $cust_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/deliverer-profile'); }
    $deliverer = $this->api->get_deliverer_profile($cust_id);
    $this->load->view('user_panel/edit_deliverer_bank_view', compact('deliverer'));
    $this->load->view('user_panel/footer');
  }

  public function update_deliverer_bank()
  {
    $cust_id = (int) $this->input->post('cust_id');
    $shipping_mode = $this->input->post('shipping_mode', TRUE);
    $iban = $this->input->post('iban', TRUE);
    $bank_name = $this->input->post('bank_name', TRUE);
    $bank_address = $this->input->post('bank_address', TRUE);
    $bank_title = $this->input->post('bank_title', TRUE);
    $bank_short_code = $this->input->post('bank_short_code', TRUE);
    $mobile_money_acc1 = $this->input->post('mobile_money_acc1', TRUE);
    $mobile_money_acc2 = $this->input->post('mobile_money_acc2', TRUE);

    $update_data = array(
      "shipping_mode" => (int)$shipping_mode,
      "iban" => trim($iban),
      "bank_name" => trim($bank_name),
      "bank_address" => trim($bank_address),
      "bank_title" => trim($bank_title),
      "bank_short_code" => trim($bank_short_code),
      "mobile_money_acc1" => trim($mobile_money_acc1),
      "mobile_money_acc2" => trim($mobile_money_acc2),
    );

    if($this->api->update_business_info($update_data, $cust_id)) {
      $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
    } else {  $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found'));  } 
    redirect('user-panel/edit-deliverer-bank/'.$cust_id);
  }

  public function edit_deliverer_document()
  {
    $document = $this->api->get_deliverer_documents($this->cust_id);
    $this->load->view('user_panel/edit_deliverer_document_view', compact('document'));
    $this->load->view('user_panel/footer');
  }

  public function add_deliverer_document()
  { 
    //var_dump($_POST); die();
    $cust_id = $this->cust_id;
    $doc_type = $this->input->post('doc_type', TRUE);
    $deliverer_id = (int) $this->user->get_deliverer_id($this->cust_id);
    
    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachment')) {   
        $uploads    = $this->upload->data();  
        $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
      } else{ $attachement_url = "NULL"; }
    } else { $attachement_url = "NULL"; }

    $insert_data = array(
      "attachement_url" => trim($attachement_url), 
      "cust_id" => $cust_id, 
      "deliverer_id" => $deliverer_id, 
      "is_verified" => "0", 
      "doc_type" => $doc_type, 
      "is_rejected" => "0",
      "upload_datetime" => date('Y-m-d H:i:s'),
      "verify_datetime" => "NULL",
    );  
    //var_dump($insert_data); die();
    if( $id = $this->user->register_deliverer_document($insert_data) ) {  
      $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
    } else {  $this->session->set_flashdata('error',$this->lang->line('upload_failed')); }
    redirect('user-panel/edit-deliverer-document');
  }

  public function update_deliverer_doc()
  { 
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;  
    $doc_id = $this->input->post('doc_id', TRUE); $doc_id = (int) $doc_id;

    if( !empty($_FILES["attachement"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachement')) {   
        $uploads    = $this->upload->data();  
        $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
      } else{ $attachement_url = "NULL"; }
    } else { $attachement_url = "NULL"; }

    $update_data = array( "attachement_url" => trim($attachement_url), "is_verified" => "0", "is_rejected" => "0"); 
    $old_doc_url = $this->api->get_deliverer_old_document_url($doc_id);

    if( $this->api->update_deliverer_document($doc_id, $update_data) ) { 
      if($attachement_url != "NULL"){ unlink(trim($old_doc_url)); }
      $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
    } else {  $this->session->set_flashdata('error',$this->lang->line('unable_to_update')); }
    redirect('user-panel/deliverer-profile'); 
  }

  public function deliverer_document_delete()
  {
    $doc_id = $this->input->post('id', TRUE);
    if($this->api->delete_deliverer_document($doc_id)){ echo 'success'; } else { echo "failed"; } 
  }

  public function business_photos()
  {
    $photos = $this->api->get_business_photos($this->cust_id);
    $this->load->view('user_panel/business_photos_view', compact('photos'));
    $this->load->view('user_panel/footer');
  }

  public function business_photos_delete()
  {
    $photo_id = $this->input->post('id', TRUE);
    if($this->user->business_photos_delete($photo_id)){ echo 'success'; } else { echo "failed"; } 
  }

  public function business_photos_upload()
  {
    $cust_id = $this->cust_id;
    if( !empty($_FILES["business_photo"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/business-photos/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('business_photo')) {   
        $uploads    = $this->upload->data();  
        $photo_url =  $config['upload_path'].$uploads["file_name"];  
      }      

      if( $photo_id = $this->api->add_business_photo($photo_url, $cust_id)){          
        $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
      } else {  $this->session->set_flashdata('error',$this->lang->line('unable_to_update')); }
    } 
    // update user last login
    $this->api->update_last_login($cust_id);
    redirect('user-panel/business-photos');
  }

  public function driver_list()
  {
    $drivers = $this->api->get_customer_drivers($this->cust_id);
    $this->load->view('user_panel/driver_list_view', compact('drivers'));
    $this->load->view('user_panel/footer');
  }

  public function driver_inactive_list()
  {
    $drivers = $this->api->get_customer_drivers_inactive($this->cust_id);
    $this->load->view('user_panel/driver_inactive_list_view', compact('drivers'));
    $this->load->view('user_panel/footer');
  }

  public function driver_inactive()
  {
    $cust = $this->cust;
    $cd_id = $this->input->post('id', TRUE);
    if($this->api->inactivate_driver($cd_id)){ 
      $driver_count = (int)$cust['driver_count'];
      if($driver_count > 0 ) {  $driver_count -= 1;  $this->api->update_driver_count($driver_count, $cust['cust_id']); }
      echo 'success'; 
    } else { echo "failed";  } 
  }

  public function driver_active()
  {
    $cust = $this->cust;
    $cd_id = $this->input->post('id', TRUE);
    if($this->api->activate_driver($cd_id)){ 
      $driver_count = (int)$cust['driver_count']; $driver_count += 1;  
      $this->api->update_driver_count($driver_count, $cust['cust_id'] );
      echo 'success'; 
    } else { echo "failed";  } 
  }

  public function view_driver()
  {
    if( !is_null($this->input->post('cd_id')) ) { $cd_id = (int) $this->input->post('cd_id'); }
    else if(!is_null($this->uri->segment(3))) { $cd_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/driver-list'); }
    $drivers = $this->api->get_driver_datails($cd_id);
    $category = explode(',', $drivers[0]['cat_ids']); 
    $category_name = array();           
    for ($i=0; $i < sizeof($category); $i++) {
      $cname = $this->user->get_driver_category_name($category[$i]);
      array_push($category_name, $cname['short_name']);
    }
    $assign_orders = $this->api->get_driver_order_list($cd_id, 0, 'assign');
    $accept_orders = $this->api->get_driver_order_list($cd_id, 0, 'accept');
    $in_progress_orders = $this->api->get_driver_order_list($cd_id, 0, 'in_progress');
    $delivered_orders = $this->api->get_driver_order_list($cd_id, 0, 'delivered');
    $this->load->view('user_panel/driver_details_view', compact('drivers','assign_orders','accept_orders','delivered_orders','in_progress_orders','category_name'));
    $this->load->view('user_panel/footer');
  }

  public function add_driver()
  {      
    $categories = $this->api->get_permitted_cat();
    $countries = $this->user->get_countries($this->cust_id);
    $this->load->view('user_panel/driver_add_view', compact('categories','countries'));
    $this->load->view('user_panel/footer');
  }

  public function add_driver_details()
  {
    $cust = $this->cust;
    $cust_id = (int)$this->cust_id;
    $first_name = $this->input->post('first_name', TRUE);
    $last_name = $this->input->post('last_name', TRUE);
    $email = $this->input->post('email', TRUE); 
    $country_id = $this->input->post('country_id', TRUE); 
    $country_code = $this->input->post('country_code', TRUE); 
    $password = $this->input->post('password', TRUE);
    $mobile1 = $this->input->post('mobile1', TRUE);
    $mobile2 = $this->input->post('mobile2', TRUE);
    $cid = $this->input->post('cat_ids', TRUE);
    if(isset($cid) && !empty($cid)) { $cat_ids = implode(',',$this->input->post('cat_ids', TRUE)); } else $cat_ids = 'NULL';
    $avail = $this->input->post('available', TRUE);
    if(isset($avail) && !empty($avail)) { $available = implode(',',$this->input->post('available', TRUE)); } else $available = 'NULL';
    $country_name = $this->api->get_country_name_by_id((int)$country_id);
    $today = date('Y-m-d H:i:s');

    if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/profile-images/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('profile_image')) {   
        $uploads    = $this->upload->data();  
        $avatar_url =  $config['upload_path'].$uploads["file_name"];  
      }         
    } else { $avatar_url ="NULL"; }

    $driver_insert_data = array(
      "cust_id" => trim($cust_id),
      "first_name" => trim($first_name),
      "last_name" => trim($last_name),
      "email" => trim($email),
      "country_id" => (int)$country_id,
      "country_code" => (int)$country_code,
      "country_name" => trim($country_name),
      "password" => trim($password),
      "mobile1" => trim($mobile1),
      "mobile2" => trim($mobile2),
      "cat_ids" => trim($cat_ids),
      "available" => trim($available),
      "avatar" => trim($avatar_url),
      "latitude" => 'NULL',
      "longitude" => 'NULL',
      "loc_update_datetime" => $today,
      "cre_datetime" => $today,
      "mod_datetime" => $today
    );

    if(!$this->api->is_driver_exists(trim($email))){ 
      if( $cd_id = $this->api->register_driver($driver_insert_data) ){
        $driver_count = ((int)$cust['driver_count'] + 1);      
        $this->api->update_driver_count($driver_count, $cust['cust_id']);
        $subject = $this->lang->line('create_driver_title');
        $message ="<p class='lead'>".$this->lang->line('create_driver_email_body1')."</p>";
        $message ="<p class='lead'>".$this->lang->line('driver_email')." : ".trim($email)."</p>";
        $message ="<p class='lead'>".$this->lang->line('driver_password')." : ".trim($password)."</p>";
        $message .="</td></tr><tr><td align='center'><br />";
        // Send email to given email id
        $this->api_sms->send_email_text(trim($first_name), trim($email), $subject, trim($message));
        $sms_msg = $this->lang->line('driver_sms_msg1') . trim($email) . ' ' . $this->lang->line('driver_sms_msg2') . trim($password);
        if(substr($mobile1, 0, 1) == 0) { $mobile1 = ltrim($mobile1, 0); }
        $this->api->sendSMS((int)$country_code.trim($mobile1), $sms_msg);
        //$link = "https://tinyurl.com/y932j77c";
        $link = $this->config->item('driver_app_link');
        $sms_app = $this->lang->line('download_driver_app') . $link;
        $this->api->sendSMS((int)$country_code.trim($mobile1), $sms_app);
        $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); }
    } else { $this->session->set_flashdata('error',$this->lang->line('email_already_registered')); }
    redirect('user-panel/add-driver');
  }

  public function edit_driver()
  {
    if( !is_null($this->input->post('cd_id')) ) { $cd_id = (int) $this->input->post('cd_id'); }
    else if(!is_null($this->uri->segment(3))) { $cd_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/driver-list'); }
    $categories = $this->api->get_permitted_cat();
    $driver_details = $this->api->get_driver_datails($cd_id);
    $countries = $this->user->get_countries($this->cust_id);
    $this->load->view('user_panel/driver_edit_view', compact('categories','driver_details','countries'));
    $this->load->view('user_panel/footer');
  }

  public function update_driver_details()
  { 
    //echo json_encode($_POST); die(); 
    $cd_id = (int)$this->input->post('cd_id', TRUE);
    $first_name = $this->input->post('first_name', TRUE);
    $last_name = $this->input->post('last_name', TRUE);
    $email = $this->input->post('email', TRUE); 
    $country_id = $this->input->post('country_id', TRUE); 
    $country_code = $this->input->post('country_code', TRUE);
    $mobile1 = $this->input->post('mobile1', TRUE);
    $mobile2 = $this->input->post('mobile2', TRUE);
    $cid = $this->input->post('cat_ids', TRUE);
    if(isset($cid) && !empty($cid)) { $cat_ids = implode(',',$this->input->post('cat_ids', TRUE)); } else $cat_ids = 'NULL';
    $avail = $this->input->post('available', TRUE);
    if(isset($avail) && !empty($avail)) { $available = implode(',',$this->input->post('available', TRUE)); } else $available = 'NULL';
    $country_name = $this->api->get_country_name_by_id((int)$country_id);
    $today = date('Y-m-d H:i:s');
    $password = $this->input->post('password', TRUE);
    
    if($password != '' ){
      $password = $this->input->post('password', TRUE);
      $update_data = array(
        "first_name" => trim($first_name),
        "last_name" => trim($last_name),
        "email" => trim($email),
        "country_id" => trim($country_id),
        "country_code" => (int)$country_code,
        "country_name" => trim($country_name),
        "password" => trim($password),
        "mobile1" => trim($mobile1),
        "mobile2" => trim($mobile2),
        "cat_ids" => trim($cat_ids),
        "available" => trim($available),
        "mod_datetime" => $today
      );
    } else {
      $update_data = array(
        "first_name" => trim($first_name),
        "last_name" => trim($last_name),
        "email" => trim($email),
        "country_id" => trim($country_id),
        "country_code" => (int)$country_code,
        "country_name" => trim($country_name),
        "mobile1" => trim($mobile1),
        "mobile2" => trim($mobile2),
        "cat_ids" => trim($cat_ids),
        "available" => trim($available),
        "mod_datetime" => $today
      );
    } 
    //var_dump($update_data); die();
    if(!$this->api->is_driver_exists_excluding(trim($email), $cd_id)) { 
      if( $this->api->update_driver($update_data, $cd_id) ){
        $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('upload_failed')); }
    } else { $this->session->set_flashdata('error',$this->lang->line('email_already_registered')); }
    redirect($this->config->item('base_url').'user-panel/edit-driver/'.$cd_id);
  }

  public function driver_avatar_update()
  {
    $cd_id = $this->input->post('cd_id', TRUE);
    if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/profile-images/'; 
      $config['allowed_types']  = '*'; 
      $config['max_size']       = '0'; 
      $config['max_width']      = '0';
      $config['max_height']     = '0'; 
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('profile_image')) {
        $uploads    = $this->upload->data();  
        $avatar_url =  $config['upload_path'].$uploads["file_name"];  
        $this->api->delete_old_avatar_driver($cd_id);
      }      

      if( $this->api->update_avatar_driver($avatar_url, $cd_id)) {
      $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('update_failed')); } 

      redirect($this->config->item('base_url').'user-panel/edit-driver/'.$cd_id);
    } else { 
      $this->session->set_flashdata('error',$this->lang->line('update_failed')); 
      redirect($this->config->item('base_url').'user-panel/edit-driver/'.$cd_id); 
    }
  }

  public function vehicle_list()
  {
    $vehicles = $this->api->get_consumers_transport_list($this->cust_id);
    $this->load->view('user_panel/vehicle_list_view',compact('vehicles'));
    $this->load->view('user_panel/footer');
  }
  
  public function vehicle_add()
  {
    $transport = $this->api->get_transport_master();
    $unit = $this->api->get_unit_master();
    $this->load->view('user_panel/vehicle_add_view', compact('transport','unit'));
    $this->load->view('user_panel/footer');
  }

  public function vehicle_add_details()
  {
    $cust_id = $this->cust_id;
    $transport_mode = $this->input->post('mode', TRUE);
    $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
    $registration_no = $this->input->post('registration_no', TRUE);
    $max_volume = $this->input->post('max_volume', TRUE);
    $max_weight = $this->input->post('max_weight', TRUE);
    $mark = $this->input->post('mark', TRUE);
    $model = $this->input->post('model', TRUE);
    $symbolic_name = $this->input->post('symbolic_name', TRUE);
    $unit_id = $this->input->post('unit_id', TRUE);

    $insert_data = array(
      "cust_id" => (int)$cust_id,
      "transport_mode" => trim($transport_mode),
      "vehical_type_id" => (int)$vehical_type_id,
      "registration_no" => trim($registration_no),
      "max_volume" => trim($max_volume),
      "max_weight" => trim($max_weight),
      "mark" => trim($mark),
      "model" => trim($model),
      "symbolic_name" => trim($symbolic_name),
      "unit_id" => (int)$unit_id,
    );

    if($this->api->add_transport_details($insert_data)){
      $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
    } else {  $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  } 
    redirect('user-panel/vehicle-add');
  }

  public function vehicle_edit()
  {
    if( !is_null($this->input->post('transport_id')) ) { $transport_id = (int) $this->input->post('transport_id'); }
    else if(!is_null($this->uri->segment(3))) { $transport_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/vehicle-list'); }
    $transport_details = $this->api->get_consumers_transport_details($transport_id);
    $transport = $this->api->get_transport_master();
    $unit = $this->api->get_unit_master();
    $this->load->view('user_panel/vehicle_edit_view', compact('transport_details','transport','unit'));
    $this->load->view('user_panel/footer');
  }

  public function vehicle_edit_details()
  {
    $cust_id = $this->cust_id;
    $transport_id = $this->input->post('transport_id', TRUE);
    $transport_mode = $this->input->post('mode', TRUE);
    $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
    $registration_no = $this->input->post('registration_no', TRUE);
    $max_volume = $this->input->post('max_volume', TRUE);
    $max_weight = $this->input->post('max_weight', TRUE);
    $mark = $this->input->post('mark', TRUE);
    $model = $this->input->post('model', TRUE);
    $symbolic_name = $this->input->post('symbolic_name', TRUE);
    $unit_id = $this->input->post('unit_id', TRUE);

    $update_data = array(
      "transport_mode" => trim($transport_mode),
      "vehical_type_id" => (int)$vehical_type_id,
      "registration_no" => trim($registration_no),
      "max_volume" => trim($max_volume),
      "max_weight" => trim($max_weight),
      "mark" => trim($mark),
      "model" => trim($model),
      "symbolic_name" => trim($symbolic_name),
      "unit_id" => (int)$unit_id,
    );

    if($this->api->update_transport_details($update_data, $cust_id, $transport_id)){
      $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
    } else {  $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  } 
    redirect('user-panel/vehicle-edit/'.$transport_id);
  }

  public function vehicle_delete()
  {
    $transport_id = $this->input->post('id', TRUE);
    if($this->api->delete_transport_details($transport_id)){ echo 'success'; } else { echo "failed";  } 
  }

  public function service_area_list()
  {
    $service_area = $this->api->get_customer_service_area($this->cust_id);
    $this->load->view('user_panel/service_area_list_view',compact('service_area'));
    $this->load->view('user_panel/footer');
  }

  public function service_area_add()
  {
    $countries = $this->user->get_countries($this->cust_id);
    $this->load->view('user_panel/service_area_add_view', compact('countries'));
    $this->load->view('user_panel/footer');
  }

  public function service_area_add_details()
  {
    $cust_id = (int)$this->cust_id;
    $type = $this->input->post('mode', TRUE);
    if($type == '' || !isset($type)) { $this->session->set_flashdata('error','Error! Invalid data!'); redirect('user-panel/service-area-add'); }
    $country_id = $this->input->post('country_id', TRUE);
    $state_id = $this->input->post('state_id', TRUE); 
    $city_id = $this->input->post('city_id', TRUE);
    $this->input->post('zip', TRUE) == '' ? $zip = 'NULL' : $zip = $this->input->post('zip', TRUE);
    $this->input->post('area_name', TRUE) == '' ? $area_name = 'NULL' : $area_name = $this->input->post('area_name', TRUE);
    $today = date('Y-m-d H:i:s');

    $service_insert_data = array(
      "cust_id" => (int)$cust_id,
      "type" => $type,
      "country_id" => (int)$country_id,
      "state_id" => (int)$state_id,
      "city_id" => (int)$city_id,
      "area_name" => trim($area_name),
      "zip" => trim($zip),
      "cre_datetime" => $today
    );

    if( $sa_id = $this->api->register_service_area($service_insert_data) ) {
      $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
    } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); }
    redirect('user-panel/service-area-add');
  }

  public function service_area_delete()
  {
    $sa_id = $this->input->post('id', TRUE);
    if($this->api->delete_service_area($sa_id)){ echo 'success'; } else { echo "failed";  } 
  }

  // Bookings
  public function get_transport_vehicle_list_by_type()
  {
    $type = $this->input->post('type');
    $cat_id = $this->input->post('cat_id');
    echo json_encode($this->user->get_transport_vehicle_list_by_type($type, (int)$cat_id));
  }
  public function get_dimension_list_by_transport_type()
  {
    $type = $this->input->post('type');
    $cat_id = $this->input->post('cat_id');
    echo json_encode($this->api->get_standard_dimension_masters_cat_id($cat_id, $type));
  }

  public function my_bookings()
  { 
    // echo json_encode($_POST); die();
    $countries = $this->user->get_countries();
    $categories = $this->api->get_category();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id);   
    //Filters
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');

    //  New params in filter
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || ($cat_id > 0)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
        $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
        $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
      } else { $creation_start_date = $creation_end_date = null; }
      
      if(trim($pickup_start) != "" && trim($pickup_end) != "") {
        $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
        $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
      } else { $pickup_start_date = $pickup_end_date = null; }

      if(trim($delivery_start) != "" && trim($delivery_end) != "") {
        $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
        $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
      } else { $delivery_start_date = $delivery_end_date = null; }

      if(trim($expiry_start) != "" && trim($expiry_end) != "") {
        $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
        $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
      } else { $expiry_start_date = $expiry_end_date = null; }

      $filter_array = array(
        "user_id" => $this->cust_id,
        "user_type" => "cust",
        "order_status" => 'open',
        "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
        "from_address" => ($from_address!=null)? $from_address: "NULL",
        "to_address" => ($to_address!=null)? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!=null)? $pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
        "price" => ($price!=null)?$price:"0",
        "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
        "order_type" => ($order_type!=null)? $order_type: "NULL",
        // new filters
        "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=null)? $distance_start: "0",
        "distance_end" => ($distance_end!=null)? $distance_end: "0",
        "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=null)?$max_weight:"0",
        "unit_id" => ($unit_id != null)?$unit_id:"0",
        "cat_id" => ($cat_id != null)?$cat_id:"0",
      );
      // echo json_encode($filter_array); die();
      $orders = $this->user->filtered_list($filter_array);
      //echo $this->db->last_query(); die();
      $filter = 'advance';
      $this->load->view('user_panel/my_bookings_list_view', compact('orders','countries','categories','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id'));
    } else {
      $orders = $this->user->get_booking_list($this->cust_id,'open');
      // echo json_encode($orders); die();
      $filter = 'basic';
      $cat_id = 0;
      $this->load->view('user_panel/my_bookings_list_view', compact('orders','countries','categories','dimensions','filter','unit','earth_trans','air_trans','sea_trans','cat_id'));
    }   
    $this->load->view('user_panel/footer');
  }

  public function cancel_order()
  {
    $order_id = $this->input->post("id");
    $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
    $detail = $this->api->get_order_detail($order_id);
    if($detail['order_status'] == 'open' || $detail['driver_status_update'] == 'NULL' || $detail['order_status'] == 'assign') {
      $status = $detail['order_status'];
      if( $this->api->cancel_courier_order($order_id) ) {
        if($detail['is_bank_payment'] == 0) {
          if($status == 'open' || $status == 'NULL') {
            //Payment Module - transfer complete payment to customer main account.---------------------------------------------------
            $today = date('Y-m-d h:i:s');
            //deduct refund amount from gonagoo account
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($detail['order_price']);
            $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 0,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($detail['order_price']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($detail['currency_sign']),
              "country_id" => trim($detail['from_country_id']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

            //Add refund amount in customer account
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($detail['order_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));

            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($detail['order_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($detail['currency_sign']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);

            //post payment in workroom
            $this->api->post_to_workroom($order_id, trim($detail['deliverer_id']), $cust_id, $this->lang->line('order_cancel_refund'), 'sr', 'payment', 'NULL', trim($detail['cust_name']), trim($detail['deliverer_name']), 'NULL', 'NULL', 'NULL');
            //Payment Module End ----------------------------------------------------------
          } else {
            //transfer complete payment to customer main account, deduct gongagoo commmission from customer account, add commission amount in gonagoo.
            $today = date('Y-m-d h:i:s');
            $detail = $this->api->get_order_detail($order_id);
            //deduct refund amount from gonagoo account
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($detail['order_price']);
            $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 0,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($detail['order_price']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($detail['currency_sign']),
              "country_id" => trim($detail['from_country_id']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

            //Add refund amount in customer account
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($detail['order_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));

            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($detail['order_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($detail['currency_sign']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);

            //Deduct commission from customer account master and history
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($detail['commission_amount']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));

            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'commission_amount',
              "amount" => trim($detail['commission_amount']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($detail['currency_sign']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);

            //add commission to gonagoo account master and history
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($detail['commission_amount']);
            $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 0,
              "transaction_type" => 'commission_amount',
              "amount" => trim($detail['commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($detail['currency_sign']),
              "country_id" => trim($detail['from_country_id']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            
            //send commission invoice to deliverer
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($detail['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference("$order_id");
            $invoice->setDate(date('M dS ,Y',time()));
            
            $customer_details = $this->api->get_user_details($cust_id);
            $customer_country = $this->api->get_country_details(trim($customer_details['country_id']));
            $customer_state = $this->api->get_state_details(trim($customer_details['state_id']));
            $customer_city = $this->api->get_city_details(trim($customer_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($customer_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setFrom(array(trim($customer_details['firstname']) . " " . trim($customer_details['lastname']),trim($customer_city['city_name']),trim($customer_state['state_name']),trim($customer_country['country_name']),trim($customer_details['email1'])));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            /* Adding Items in table */
            $order_for = $this->lang->line('gonagoo_commission');
            $rate = round(trim($detail['commission_amount']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
                
            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('commission_paid'));

            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            //$invoice->addTitle("Other Details");
            /* Add Paragraph */
            //$invoice->addParagraph("Any Other Details");
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($order_id.'_refund_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $order_id.'_refund_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/order-invoices/".$pdf_name);
            
            //Update Order Invoice and post to workroom
            $this->api->update_deliverer_invoice_url($order_id, "order-invoices/".$pdf_name);
            /* ----------------------------------------------------- */
            /* -----------------Email Invoice to customer--------------------- */
            $eol = PHP_EOL;
            $messageBody = $eol . $this->lang->line('invoice_email_message') . $eol;
            $file = "resources/order-invoices/".$pdf_name;
            $file_size = filesize($file);
            $handle = fopen($file, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
            $uid = md5(uniqid(time()));
            $name = basename($file);
            //$eol = PHP_EOL;
            $from_mail = $this->config->item('from_email');
            $replyto = $this->config->item('from_email');
            // Basic headers
            $header = "From: ".$gonagoo_address['company_name']." <".$from_mail.">".$eol;
            $header .= "Reply-To: ".$replyto.$eol;
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";
            // Put everything else in $message
            $message = "--".$uid.$eol;
            $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
            $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
            $message .= $messageBody.$eol;
            $message .= "--".$uid.$eol;
            $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
            $message .= "Content-Transfer-Encoding: base64".$eol;
            $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
            $message .= $content.$eol;
            $message .= "--".$uid."--";
            mail($customer_details['email1'], $this->lang->line('commission_invoice_email_subject'), $message, $header);
            /* -----------------------Email Invoice to customer----------------------- */

            //post payment in workroom
            $this->api->post_to_workroom($order_id, trim($customer_details['cust_id']), $cust_id, $this->lang->line('advance_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($detail['deliverer_name']), 'NULL', 'NULL', 'NULL');
            //Payment Module End ----------------------------------------------------------
          }
        }
        $this->api->insert_order_status_customer($cust_id, $order_id, 'cancel');
        echo "success";
      } else {  echo "failed"; }
    } else { echo "failed"; }
  }

  public function my_booking_order_details()
  {
    $id = $this->uri->segment(3);
    $order = $this->api->get_order_detail($id);
    $packages = $this->user->get_order_packages($id);
    $order_status = $this->api->order_status_list($id);
    $this->load->view('user_panel/my_booking_order_details', compact('order', 'order_status','packages'));
    $this->load->view('user_panel/footer');
  }
  
  // Create new booking view
  public function add_bookings()
  {
    //echo json_encode($_REQUEST); die();
    $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
    $category_id = $this->input->post("category_id"); $category_id = (int)$category_id;
    if(isset($category_id) && $category_id == 7 ) {
      $transport_type = 'earth'; 
      $this->session->unset_userdata('cat_id');
      $this->session->unset_userdata('vehicle');
      $this->session->unset_userdata('pickupdate');
      $this->session->unset_userdata('pickuptime');
      $this->session->unset_userdata('c_quantity');
      $this->session->unset_userdata('dimension_id');
      $this->session->unset_userdata('deliverdate');
      $this->session->unset_userdata('delivertime');
      $this->session->unset_userdata('c_weight');
      $this->session->unset_userdata('image_url');
      $this->session->unset_userdata('c_width');
      $this->session->unset_userdata('c_height');
      $this->session->unset_userdata('c_length');
      $this->session->unset_userdata('order_mode');

      $this->session->unset_userdata('from_address');
      $this->session->unset_userdata('frm_latitude');
      $this->session->unset_userdata('frm_longitude');
      $this->session->unset_userdata('frm_formatted_address');
      $this->session->unset_userdata('frm_postal_code');
      $this->session->unset_userdata('frm_country');
      $this->session->unset_userdata('frm_state');
      $this->session->unset_userdata('frm_city');

      $this->session->unset_userdata('to_address');
      $this->session->unset_userdata('to_latitude');
      $this->session->unset_userdata('to_longitude');
      $this->session->unset_userdata('to_formatted_address');
      $this->session->unset_userdata('to_postal_code');
      $this->session->unset_userdata('to_country');
      $this->session->unset_userdata('to_state');
      $this->session->unset_userdata('to_city');

      $this->session->unset_userdata('transport_type');
    } else {
      $category_id = (int)$_SESSION['cat_id']; 
      $transport_type = $_SESSION['transport_type'];
    }

    $countries = $this->user->get_countries();
    $recent_relay = $this->user->get_recent_relay_list($cust_id);
    $default_trans = $this->user->get_transport_vehicle_list_by_type(trim($transport_type), $category_id);
    $address = $this->api->get_consumers_addressbook($this->cust['cust_id'],array());
    $relay = $this->user->get_relay_points();
    // $transport = $this->api->get_transport_master(true);
    $dimension = $this->api->get_standard_dimension_masters_cat_id($category_id, trim($transport_type));
    $unit = $this->api->get_unit_master(true);
    $user_details = $this->api->get_user_details($cust_id);
    $this->load->view('user_panel/my_bookings_add_view',compact('address','transport','dimension','unit','relay','default_trans','countries','category_id','recent_relay','user_details'));
    $this->load->view('user_panel/footer');
  }
  // Courier Order List
  public function courier_order_list()
  {
    $cust_id = $this->cust['cust_id'];
    //$orders = $this->api->get_workorder_list($cust_id); 
    $orders = $this->api->get_global_workroom_list((int)$cust_id);
    //echo $this->db->last_query();
    //echo json_encode($orders);  die();
    for ($i=0; $i < sizeof($orders); $i++) { 
      if($orders[$i]['cat_id'] == 6 || $orders[$i]['cat_id'] == 7) {
        if($service_details = $this->api->get_courier_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
          $orders[$i] += array('service_details' => $service_details);
        } else { $orders[$i] += array('service_details' => array()); }
      } else if($orders[$i]['cat_id'] == 9) {
        if($service_details = $this->api->get_laundry_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
          $service_details_details = $this->api->laundry_booking_details_list($service_details['booking_id']);
          $service_details += ['bookings_details' => $service_details_details];
          $orders[$i] += array('service_details' => $service_details);
        } else { $orders[$i] += array('service_details' => array()); }
      } else {
        if($service_details = $this->api->get_booking_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
          $orders[$i] += array('service_details' => $service_details);
        } else { $orders[$i] += array('service_details' => array()); }
      }
    }
    //echo json_encode($orders); die();
    $this->load->view('user_panel/order_list_view', compact('orders'));
    $this->load->view('user_panel/footer');
  }
  // get insurance price 
  public function get_insurance_price()
  {
    $addr_id = $this->input->post('addr_id');
    $addr_type = $this->input->post('addr_type');
    $value = $this->input->post('value');
    $category_id = $this->input->post('category_id');

    if($addr_type =="address"){ 
      $addr = $this->user->get_address_from_book($addr_id);
      $country_id = $addr['country_id'];
    }
    if($addr_type =="relay"){ 
      $addr = $this->user->get_relay_pint_details($addr_id); 
      $country_id = $addr['country_id'];
    }
    echo json_encode($this->api->get_insurance_fees($country_id, $value,$category_id));
  }
  // get Relay, address lat long 
  public function get_address_relay_lat_long()
  {
    $addr_id = $this->input->post('addr_id');
    $addr_type = $this->input->post('type');
    $lat_long = '';

    if($addr_type == "address"){  
      $addr = $this->user->get_address_from_book((int)$addr_id);
      $lat_long = $addr['latitude'] .','. $addr['longitude'] ;
    }
    if($addr_type == "relay"){  
      $addr = $this->user->get_relay_pint_details((int)$addr_id); 
      $lat_long = $addr['latitude'] .','. $addr['longitude'] ;
    }
    echo json_encode($lat_long);
  }
  // get Travelling Distance 
  public function get_driving_distance()
  {
    $from_lat_long = explode(',',$this->input->post('from_lat_long'));
    $from_lat = $from_lat_long[0];
    $from_long = $from_lat_long[1];
    $to_lat_long = explode(',',$this->input->post('to_lat_long'));
    $to_lat = $to_lat_long[0];
    $to_long = $to_lat_long[1];
  
    $distance = $this->api->GetDrivingDistance($from_lat,$from_long,$to_lat,$to_long);
    echo json_encode($distance);
  }
  // get total order price
  public function get_total_order_price()
  {
    //echo json_encode($_POST); die();
    $category_id = $this->input->post('category_id');
    $delivery_datetime = $this->input->post('delivery_datetime');
    $pickup_datetime = $this->input->post('pickup_datetime');
    $transport_type = $this->input->post('transport_type');
    $service_area_type = $this->input->post('service_area_type');
    $frm_addr_id = $this->input->post('frm_addr_id');
    $to_addr_id = $this->input->post('to_addr_id');
    $frm_addr_type = $this->input->post('frm_addr_type');
    $to_addr_type = $this->input->post('to_addr_type');   
    $total_quantity = $this->input->post('total_quantity');
    $width = $this->input->post('c_width');
    $height = $this->input->post('c_height');
    $length = $this->input->post('c_length');
    $total_weight = $this->input->post('total_weight');
    $unit_id = $this->input->post('c_unit_id');
    $insurance_check = $this->input->post('insurance_check');
    $package_value = $this->input->post('package_value');
    $handling_by = $this->input->post('handling_by');
    $dedicated_vehicle = $this->input->post('dedicated_vehicle');
    $distance_in_km = 0;

    //  Newly added keys
    $old_new_goods = $this->input->post('old_new_goods', TRUE); 
    if(empty($old_new_goods)){ $old_new_goods = "NULL"; }
    $custom_clearance_by = $this->input->post('custom_clearance', TRUE);
    if(empty($custom_clearance_by)){ $custom_clearance_by = "NULL"; }
    $custom_package_value = $this->input->post('custom_package_value', TRUE);
    if(empty($custom_package_value)){ $custom_package_value = 0; }

    if($frm_addr_type =="address"){ 
      $frm_addr = $this->user->get_address_from_book($frm_addr_id);
      $from_latitude = $frm_addr['latitude'];
      $from_longitude = $frm_addr['longitude'];
      $from_country_id = $frm_addr['country_id'];
      $from_state_id = $frm_addr['state_id'];
      $from_city_id = $frm_addr['city_id'];
    } 
    else if($frm_addr_type =="relay"){  
      $frm_addr = $this->user->get_relay_pint_details($frm_addr_id);
      $from_latitude = $frm_addr['latitude'];
      $from_longitude = $frm_addr['longitude'];     
      $from_country_id = $frm_addr['country_id'];
      $from_state_id = $frm_addr['state_id'];
      $from_city_id = $frm_addr['city_id'];
    } else { $from_latitude = $from_longitude = 0; $from_country_id = 0; $from_state_id = 0; $from_city_id = 0; }

    if($to_addr_type =="address"){  
      $to_addr = $this->user->get_address_from_book($to_addr_id);
      $to_latitude = $to_addr['latitude'];
      $to_longitude = $to_addr['longitude'];
      $to_country_id = $to_addr['country_id'];
      $to_state_id = $to_addr['state_id'];
      $to_city_id = $to_addr['city_id'];
    } 
    else if($to_addr_type =="relay"){ 
      $to_addr = $this->user->get_relay_pint_details($to_addr_id); 
      $to_latitude = $to_addr['latitude'];
      $to_longitude = $to_addr['longitude'];
      $to_country_id = $to_addr['country_id'];
      $to_state_id = $to_addr['state_id'];
      $to_city_id = $to_addr['city_id'];
    } else { $to_latitude = $to_longitude = 0; $to_country_id = 0; $to_state_id = 0; $to_city_id = 0; }

    $calculate_price =  array (
      "cust_id" => $this->cust_id,
      "category_id" => $category_id,
      
      "delivery_datetime" => $delivery_datetime,
      "pickup_datetime" => $pickup_datetime,
      
      "total_quantity" => $total_quantity,
      
      "width" => $width,
      "height" => $height,
      "length" => $length,
      
      "total_weight" => $total_weight,
      "unit_id" => $unit_id,
      
      "from_country_id" => $from_country_id,
      "from_state_id" => $from_state_id,
      "from_city_id" => $from_city_id,
      "from_latitude" => $from_latitude,
      "from_longitude" => $from_longitude,

      "to_country_id" => $to_country_id,
      "to_state_id" => $to_state_id,
      "to_city_id" => $to_city_id,
      "to_latitude" => $to_latitude,
      "to_longitude" => $to_longitude,

      "service_area_type" => $service_area_type,
      "transport_type" => $transport_type,
      "handling_by" => $handling_by,
      "dedicated_vehicle" => $dedicated_vehicle,
      "package_value" => $package_value,

      "custom_clearance_by" => $custom_clearance_by,
      "old_new_goods" => $old_new_goods,
      "custom_package_value" => $custom_package_value,
      "loading_time" => 0,
    );

    echo json_encode($this->api->calculate_order_price($calculate_price));
  }
  // insert new booking
  public function register_booking()
  {
    //echo json_encode($_POST['total_price_with_promocode']); die();
    if( isset($_SESSION['cat_id'])) {
      $this->session->unset_userdata('cat_id');
      $this->session->unset_userdata('vehicle');
      $this->session->unset_userdata('pickupdate');
      $this->session->unset_userdata('pickuptime');
      $this->session->unset_userdata('c_quantity');
      $this->session->unset_userdata('dimension_id');
      $this->session->unset_userdata('deliverdate');
      $this->session->unset_userdata('delivertime');
      $this->session->unset_userdata('c_weight');
      $this->session->unset_userdata('image_url');
      $this->session->unset_userdata('c_width');
      $this->session->unset_userdata('c_height');
      $this->session->unset_userdata('c_length');
    }

    $payment_mode = $this->input->post('payment_mode', TRUE);  // cod/payment
    if($payment_mode == 'payment') $payment_mode = 'NULL';
    $cod_payment_type = $this->input->post('cod_payment_type', TRUE);
    if(!isset($cod_payment_type)) $cod_payment_type = 'NULL';
    $frm_addr_type = $this->input->post('frm_addr_type', TRUE); 
    $to_addr_type = $this->input->post('to_addr_type', TRUE); 
    $transport_type = $this->input->post('transport_type', TRUE); 
    $service_area_type = $this->input->post('order_mode', TRUE); 
    $vehical_type_id = $this->input->post('vehicle', TRUE); 
    $order_type = $this->input->post('shipping_mode', TRUE); 
    $pickup_datetime = $this->input->post('pickupdate', TRUE) . ' '. $this->input->post('pickuptime', TRUE);  $pickup_datetime = date('Y-m-d H:i:s', strtotime($pickup_datetime));
    $delivery_datetime = $this->input->post('deliverdate', TRUE) . ' '. $this->input->post('delivertime', TRUE); $delivery_datetime = date('Y-m-d H:i:s', strtotime($delivery_datetime));
    $expiry_date = $this->input->post('expiry_date', TRUE);  $expiry_date = date('Y-m-d H:i:s', strtotime($expiry_date));
    $total_quantity = $this->input->post('c_quantity', TRUE); 
    $order_contents = $this->input->post('content', TRUE);
    if(empty($order_contents)){ $order_contents = "NULL"; }
    $dimension_id = $this->input->post('dimension_id', TRUE); 
    $width = $this->input->post('c_width', TRUE); 
    $height = $this->input->post('c_height', TRUE); 
    $length = $this->input->post('c_length', TRUE); 
    $volume = ( $width * $height * $length );
    $total_volume = ($volume * $total_quantity);
    $total_weight = $this->input->post('c_weight', TRUE); 
    $unit_id = $this->input->post('c_unit_id', TRUE); 
    $insurance_check = $this->input->post('insurance', TRUE); 
    $package_value = $this->input->post('package_value', TRUE); 
    $handling_by = $this->input->post('handle_by', TRUE); 
    $with_driver = $this->input->post('travel_with', TRUE); 
    $dedicated_vehicle = $this->input->post('dedicated_vehicle', TRUE); 
    $ref_no = $this->input->post('ref_no', TRUE); 
    $deliver_instructions = $this->input->post('deliver_instruction', TRUE);      
    if(empty($deliver_instructions)){ $deliver_instructions = "NULL"; }   
    $pickup_instructions = $this->input->post('pickup_instruction', TRUE);      
    if(empty($pickup_instructions)){ $pickup_instructions = "NULL"; }
    $description = $this->input->post('description', TRUE);     
    if(empty($description)){ $description = "NULL"; }
    $advance_payment = $this->input->post('advance_pay', TRUE);
    $pickup_payment = $this->input->post('pickup_pay', TRUE);
    $deliver_payment = $this->input->post('deliver_pay', TRUE);
    $visible_by = $this->input->post('visible_by', TRUE);
    //my_code_is_here
    //For COD, Bank and Payment
    if($payment_mode == 'bank') {
      $dvncPay = 0;
      $pckupPay = 0;
      $dlvrPay = $advance_payment + $pickup_payment + $deliver_payment;
      $is_bank_payment = 1;
      $balance_amount = $dlvrPay;
    } else {
      $is_bank_payment = 0;
      if($payment_mode == 'cod' && $cod_payment_type == 'at_pickup') {
        $dvncPay = 0;
        $pckupPay = $advance_payment + $pickup_payment + $deliver_payment;
        $dlvrPay = 0;
      } else if($payment_mode == 'cod' && $cod_payment_type == 'at_deliver') {
        $dvncPay = 0;
        $pckupPay = 0;
        $dlvrPay = $advance_payment + $pickup_payment + $deliver_payment;
      } else {
        $dvncPay = $advance_payment;
        $pckupPay = $pickup_payment; 
        $dlvrPay = $deliver_payment;
      }
      $balance_amount = $advance_payment + $pickup_payment + $deliver_payment;
    }

    //  Newly added keys
    $category_id = $this->input->post('category_id', TRUE);
    $old_new_goods = $this->input->post('old_new_goods', TRUE);
    if(empty($old_new_goods)){ $old_new_goods = "NULL"; }
    $custom_clearance_by = $this->input->post('custom_clearance', TRUE);
    if(empty($custom_clearance_by)){ $custom_clearance_by = "NULL"; }
    $custom_package_value = $this->input->post('custom_package_value', TRUE);
    if(empty($custom_package_value)){ $custom_package_value = 0; }
    
    $cust = $this->cust; 
    $cust_id = (int) $cust['cust_id'];
    $cust_email = $cust['email1'];
    $parts = explode("@", $cust_email);
    $cust_email = $parts[0];
    $cust_name = ($cust['firstname'] !="NULL") ? ($cust['firstname']. ' ' .$cust['lastname']):$cust_email;
    $cust_name = ($cust_name!="NULL" && $cust['company_name']!="NULL")?($cust_name.' ['.trim($cust['company_name']).']'):( ($cust_name=="NULL")?trim($cust['company_name']):$cust_name);

    $distance_in_km = 0;

    if($frm_addr_type =="address"){ 
      $from_addr_type = 0; $from_relay_id = 0;
      $frm_addr_id = $this->input->post('from_address', TRUE);      
      $frm_addr = $this->user->get_address_from_book($frm_addr_id);   
      $from_address_name = trim($frm_addr['firstname']). ' '. trim($frm_addr['lastname']);
      $from_address_contact = trim($frm_addr['mobile_no']);
      $from_full_address = ($frm_addr['addr_line1'] !="NULL")? trim($frm_addr['addr_line1']):"". ' ';
      $from_full_address .= ($frm_addr['addr_line2'] !="NULL")? trim($frm_addr['addr_line2']):"". ' ';
      $from_latitude = $frm_addr['latitude'];
      $from_longitude = $frm_addr['longitude'];
      $from_country_id = $frm_addr['country_id'];
      $from_state_id = $frm_addr['state_id'];
      $from_city_id = $frm_addr['city_id'];   
      // Format1: <Street>, <Zip code>, <City>, <State>, <Country>
      $frm_addr_type1 = trim($frm_addr['street_name']);
      $from_address_email = $frm_addr['email'];
      //$frm_addr_type1 = $frm_addr['street_name'].', '.$frm_addr['zipcode'].', '.$this->api->get_city_name_by_id($frm_addr['city_id']).', '.$this->api->get_state_name_by_id($frm_addr['state_id']).', '.$this->api->get_country_name_by_id($frm_addr['country_id']) ; 
    } else if($frm_addr_type =="relay"){  
      $frm_addr_id = $this->input->post('from_relay', TRUE);
      $from_addr_type = 1; $from_relay_id = $frm_addr_id;
      $frm_addr = $this->user->get_relay_pint_details($frm_addr_id);
      $from_address_name = trim($frm_addr['firstname']). ' '. trim($frm_addr['lastname']);
      $from_address_contact = trim($frm_addr['mobile_no']);
      $from_full_address = ($frm_addr['addr_line1'] !="NULL")? trim($frm_addr['addr_line1']):"". ' ';
      $from_full_address .= ($frm_addr['addr_line2'] !="NULL")? trim($frm_addr['addr_line2']):"". ' ';
      $from_latitude = $frm_addr['latitude'];
      $from_longitude = $frm_addr['longitude'];     
      $from_country_id = $frm_addr['country_id'];
      $from_state_id = $frm_addr['state_id'];
      $from_city_id = $frm_addr['city_id'];
      // Format1: <Street>, <Zip code>, <City>, <State>, <Country>
      $frm_addr_type1 = $frm_addr['street_name'].', '.$frm_addr['zipcode'].', '.$this->api->get_city_name_by_id($frm_addr['city_id']).', '.$this->api->get_state_name_by_id($frm_addr['state_id']).', '.$this->api->get_country_name_by_id($frm_addr['country_id']); 
    } else { $from_latitude = $from_longitude = 0; }

    if($to_addr_type =="address"){  
      $to_addr_type = 0; $to_relay_id = 0;
      $to_addr_id = $this->input->post('to_address', TRUE);
      $to_addr = $this->user->get_address_from_book($to_addr_id);
      $to_address_name = trim($to_addr['firstname']). ' '. trim($to_addr['lastname']);
      $to_address_contact = trim($to_addr['mobile_no']);
      $to_full_address = ($to_addr['addr_line1'] !="NULL")? trim($to_addr['addr_line1']):"". ' ';
      $to_full_address .= ($to_addr['addr_line1'] !="NULL")? trim($to_addr['addr_line2']):"". ' ';
      $to_latitude = $to_addr['latitude'];
      $to_longitude = $to_addr['longitude'];
      $to_country_id = $to_addr['country_id'];
      $to_state_id = $to_addr['state_id'];
      $to_city_id = $to_addr['city_id'];
      // Format1: <Street>, <Zip code>, <City>, <State>, <Country>
      $to_addr_type1 = trim($to_addr['street_name']); 
      $to_address_email = $to_addr['email'];
      //$to_addr_type1 = $to_addr['street_name'].', '.$to_addr['zipcode'].', '.$this->api->get_city_name_by_id($to_addr['city_id']).', '.$this->api->get_state_name_by_id($to_addr['state_id']).', '.$this->api->get_country_name_by_id($to_addr['country_id']); 
    } else if($to_addr_type =="relay"){ 
      $to_addr_id = $this->input->post('to_relay', TRUE);
      $to_addr_type = 1; $to_relay_id = $to_addr_id;
      $to_addr = $this->user->get_relay_pint_details($to_addr_id); 
      $to_address_name = trim($to_addr['firstname']). ' '. trim($to_addr['lastname']);
      $to_address_contact = trim($to_addr['mobile_no']);
      $to_full_address = ($to_addr['addr_line1'] !="NULL")? trim($to_addr['addr_line1']):"". ' ';
      $to_full_address .= ($to_addr['addr_line2'] !="NULL")? trim($to_addr['addr_line2']):"". ' ';
      $to_latitude = $to_addr['latitude'];
      $to_longitude = $to_addr['longitude'];
      $to_country_id = $to_addr['country_id'];
      $to_state_id = $to_addr['state_id'];
      $to_city_id = $to_addr['city_id'];
      // Format1: <Street>, <Zip code>, <City>, <State>, <Country>
      $to_addr_type1 = $to_addr['street_name'].', '.$to_addr['zipcode'].', '.$this->api->get_city_name_by_id($to_addr['city_id']).', '.$this->api->get_state_name_by_id($to_addr['state_id']).', '.$this->api->get_country_name_by_id($to_addr['country_id']); 
    } else { $to_latitude = $to_longitude = 0; }

    $calculate_price =  array (
      "cust_id" => $this->cust_id,
      "category_id" => $category_id,
      "delivery_datetime" => $delivery_datetime,
      "pickup_datetime" => $pickup_datetime,
      "total_quantity" => $total_quantity,
      "width" => $width,
      "height" => $height,
      "length" => $length,
      "total_weight" => $total_weight,
      "unit_id" => $unit_id,
      "from_country_id" => $from_country_id,
      "from_state_id" => $from_state_id,
      "from_city_id" => $from_city_id,
      "from_latitude" => $from_latitude,
      "from_longitude" => $from_longitude,
      "to_country_id" => $to_country_id,
      "to_state_id" => $to_state_id,
      "to_city_id" => $to_city_id,
      "to_latitude" => $to_latitude,
      "to_longitude" => $to_longitude,
      "service_area_type" => $service_area_type,
      "transport_type" => $transport_type,
      "handling_by" => $handling_by,
      "dedicated_vehicle" => $dedicated_vehicle,
      "package_value" => $package_value,
      "custom_clearance_by" => $custom_clearance_by,
      "old_new_goods" => $old_new_goods,
      "custom_package_value" => $custom_package_value,
      "loading_time" => 0,
    );

    // echo json_encode($calculate_price); die();

    $prices = $this->api->calculate_order_price($calculate_price);
    //echo json_encode($prices); die();
    $order_price = $prices['total_price']; 
    $standard_price = $prices['standard_price']; 
    $handling_fee = $prices['handling_fee']; 
    $insurance_fee = $prices['ins_fee']; 
    $urgent_fee = $prices['urgent_fee']; 
    $vehicle_fee = $prices['dedicated_vehicle_fee']; 
    $advance_percent = $prices['advance_percent']; 
    $distance_in_km = $prices['distance_in_km']; 
    $commission_percent = $prices['commission_percent']; 

    $currency_id = $prices['currency_id']; 
    $currency_sign = $prices['currency_sign']; 
    $currency_title = $prices['currency_title'];  
    
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $order_price = (int)$order_price;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $standard_price = (int)$standard_price;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $handling_fee = (int)$handling_fee;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $insurance_fee = (int)$insurance_fee;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $urgent_fee = (int)$urgent_fee;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $vehicle_fee = (int)$vehicle_fee;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $dvncPay = (int)$dvncPay;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $pckupPay = (int)$pckupPay;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $dlvrPay = (int)$dlvrPay;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $balance_amount = (int)$balance_amount;
    
    // New keys
    $old_goods_custom_commission_percent = $prices['old_goods_custom_commission_percent'];  
    $old_goods_custom_commission_fee = $prices['old_goods_custom_commission_fee'];  
    $new_goods_custom_commission_percent = $prices['new_goods_custom_commission_percent'];  
    $new_goods_custom_commission_fee = $prices['new_goods_custom_commission_fee'];  
    $loading_unloading_charges = $prices['loading_unloading_charge'];  
    //echo json_encode($service_area_type == "international" && ($old_new_goods == 'New')); die();
    if($service_area_type == "international" && ($old_new_goods == 'New')){  
      $custom_clearance_percent = $new_goods_custom_commission_percent;
      $custom_clearance_fee = $new_goods_custom_commission_fee;
    } else if($service_area_type == "international" && ($old_new_goods == 'Old')){
      $custom_clearance_percent = $old_goods_custom_commission_percent;
      $custom_clearance_fee = $old_goods_custom_commission_fee;
    } else{ $custom_clearance_percent = 0;  $custom_clearance_fee = 0;  }

    // echo json_encode($prices); die();
    if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/booking-images/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('profile_image')) {   
        $uploads    = $this->upload->data();  
        $order_picture_url =  $config['upload_path'].$uploads["file_name"];  
      } else { $order_picture_url = "NULL"; }
    } else { $order_picture_url = "NULL"; }

    //Gonagoo Commission
    $user_details = $this->api->get_user_details($this->cust_id);
    //echo json_encode($user_details);die();
    if($user_details['is_dedicated'] == 1) {
      //echo 'hi'; die();
      if($user_details['commission_type'] == 3) {
        $data = array(
          "cust_id" => $this->cust_id,
          "rate_type" => 1,
          "category_id" => $category_id,
          "country_id" => $from_country_id,
        );
        if($user_gonagoo_commission_details = $this->api->get_user_gonagoo_commission_details($data)) {
          if($transport_type == 'earth' && $service_area_type == 'local') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['ELGCP'];
          } else if ($transport_type == 'earth' && $service_area_type == 'national') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['ENGCP'];
          } else if ($transport_type == 'earth' && $service_area_type == 'international') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['EIGCP'];
          } else if ($transport_type == 'sea' && $service_area_type == 'local') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['SLGCP'];
          } else if ($transport_type == 'sea' && $service_area_type == 'national') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['SNGCP'];
          } else if ($transport_type == 'sea' && $service_area_type == 'international') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['SIGCP'];
          } else if ($transport_type == 'air' && $service_area_type == 'local') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['ALGCP'];
          } else if ($transport_type == 'air' && $service_area_type == 'national') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['ANGCP'];
          } else if ($transport_type == 'air' && $service_area_type == 'international') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['AIGCP'];
          } else {
            $commission_percent = $commission_percent;
            $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
          }
        } else {
          $commission_percent = $commission_percent;
          $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
        }
      } else if($user_details['commission_type'] == 2) {
        $data = array(
          "cust_id" => $this->cust_id,
          "rate_type" => 0,
          "category_id" => $category_id,
          "country_id" => $from_country_id,
        );
        if($user_gonagoo_commission_details = $this->api->get_user_gonagoo_commission_details($data)) {
          if($transport_type == 'earth' && $service_area_type == 'local') {
            $commission_percent = $user_gonagoo_commission_details['ELGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ELGCP'] / 100 ),2);
          } else if ($transport_type == 'earth' && $service_area_type == 'national') {
            $commission_percent = $user_gonagoo_commission_details['ENGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ENGCP'] / 100 ),2);
          } else if ($transport_type == 'earth' && $service_area_type == 'international') {
            $commission_percent = $user_gonagoo_commission_details['EIGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['EIGCP'] / 100 ),2);
          } else if ($transport_type == 'sea' && $service_area_type == 'local') {
            $commission_percent = $user_gonagoo_commission_details['SLGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SLGCP'] / 100 ),2);
          } else if ($transport_type == 'sea' && $service_area_type == 'national') {
            $commission_percent = $user_gonagoo_commission_details['SNGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SNGCP'] / 100 ),2);
          } else if ($transport_type == 'sea' && $service_area_type == 'international') {
            $commission_percent = $user_gonagoo_commission_details['SIGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SIGCP'] / 100 ),2);
          } else if ($transport_type == 'air' && $service_area_type == 'local') {
            $commission_percent = $user_gonagoo_commission_details['ALGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ALGCP'] / 100 ),2);
          } else if ($transport_type == 'air' && $service_area_type == 'national') {
            $commission_percent = $user_gonagoo_commission_details['ANGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ANGCP'] / 100 ),2);
          } else if ($transport_type == 'air' && $service_area_type == 'international') {
            $commission_percent = $user_gonagoo_commission_details['AIGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['AIGCP'] / 100 ),2);
          } else {
            $commission_percent = $commission_percent;
            $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
          }
        } else {
          $commission_percent = $commission_percent;
          $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
        }
      } else {
        $commission_percent = $commission_percent;
        $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
      }
    } else {
      //echo 'else'; die();
      $commission_percent = $commission_percent;
      $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
    }

    if($currency_sign == 'XAF' || 'XOF') $commission_amount = (int)$commission_amount;

    //echo $commission_percent; 
    //echo $commission_amount;  die();
    $insert_data = array(
      "cust_id" => $this->cust_id,
      "from_address" => $frm_addr_type1,
      "from_address_name" => $from_address_name,
      "from_address_contact" => $from_address_contact,
      "from_latitude" => $from_latitude,
      "from_longitude" => $from_longitude,
      "from_country_id" => $from_country_id,
      "from_state_id" => $from_state_id,
      "from_city_id" => $from_city_id,
      "from_addr_type" => (int)$from_addr_type,
      "from_relay_id" => (int)$from_relay_id,         
      "to_address" => $to_addr_type1,
      "to_address_name" => $to_address_name,
      "to_address_contact" => $to_address_contact,
      "to_latitude" => $to_latitude,
      "to_longitude" => $to_longitude,
      "to_country_id" => $to_country_id,
      "to_state_id" => $to_state_id,
      "to_city_id" => $to_city_id,                 
      "dimension_id" => $dimension_id,
      "distance_in_km" => $distance_in_km,
      "pickup_datetime" => $pickup_datetime,
      "delivery_datetime" => $delivery_datetime,
      "vehical_type_id" => (int)$vehical_type_id,
      "order_type" => $order_type,
      "order_contents" => $order_contents,
      "total_quantity" => $total_quantity,
      "width" => "NULL",
      "height" => "NULL",
      "length" => "NULL",
      "volume" => "NULL",
      "total_volume" => "NULL",
      /*
      "width" => $width,
      "height" => $height,
      "length" => $length,
      "volume" => ($width * $height * $length ),        
      "total_volume" => ($width * $height * $length ) * $total_quantity,
      */
      "total_weight" => $total_weight,
      // "unit_id" => (int) $unit_id,
      "unit_id" => 1,
      "order_picture_url" => $order_picture_url,
      "order_description" => nl2br($description),
      "order_price" => trim($order_price),
      "order_status" => "open",
      "advance_payment" => $dvncPay,
      "pickup_payment" => $pckupPay,
      "deliver_payment" => $dlvrPay,
      "cre_datetime" => date('Y-m-d H:i:s'),
      "cust_id" => (int) $cust_id,
      "currency_id" => $currency_id,
      "currency_sign" => $currency_sign,
      "currency_title" => $currency_title,
      "cust_name" => $cust_name,
      "insurance" => (int)$insurance_check,
      "package_value" => $package_value,
      "insurance_fee" => $insurance_fee,
      "handling_by" => (int)$handling_by,
      "dedicated_vehicle" => (int)$dedicated_vehicle,
      "with_driver" => (int)$with_driver,
      "ref_no" => $ref_no,
      "service_area_type" => $service_area_type,
      "expiry_date" => $expiry_date,
      "deliver_instructions" => $deliver_instructions,
      "pickup_instructions" => $pickup_instructions,
      "to_addr_type" => (int)$to_addr_type,
      "to_relay_id" => (int)$to_relay_id,
      "commission_percent" => $commission_percent,
      "commission_amount" => $commission_amount,
      "standard_price" => trim($standard_price),
      "urgent_fee" => trim($urgent_fee),
      "handling_fee" => trim($handling_fee),
      "vehicle_fee" => trim($vehicle_fee),
      "deliverer_id" => 0,
      "driver_id" => 0,
      "status_update_datetime" => "NULL",
      "accept_datetime" => "NULL",
      "assigned_datetime" => "NULL",
      "driver_status_update" => "NULL",
      "delivery_code" => "NULL",
      "delivered_datetime" => "NULL",
      "review_comment" => "NULL",
      "rating" => "NULL",
      "review_datetime" => "NULL",
      "status_updated_by" => "NULL",
      "delivery_notes" => "NULL",
      "invoice_url" => "NULL",
      "paid_amount" => "0",
      "balance_amount" => $balance_amount,
      "payment_method" => "NULL",
      "transaction_id" => "NULL",
      "payment_datetime" => "NULL",
      "cd_name" => "NULL",
      "assigned_accept_datetime" => "NULL",
      "deliverer_company_name" => "NULL",
      "deliverer_contact_name" => "NULL",
      "deliverer_name" => "NULL",
      "driver_code" => "NULL",
      "from_relay_status" => "NULL",
      "from_relay_status_datetime" => "NULL",
      "to_relay_status" => "NULL",
      "to_relay_status_datetime" => "NULL",
      "complete_paid" => "0",
      "deliverer_invoice_url" => "NULL",
      "src_relay_commission_invoice_url" => "NULL",
      "dest_relay_commission_invoice_url" => "NULL",
      "transport_type" => (!empty($transport_type)?strtolower($transport_type):"NULL"),
      "custom_clearance_by" => $custom_clearance_by,
      "custom_package_value" => $custom_package_value,
      "old_new_goods" => $old_new_goods,
      "custom_clearance_fee" => $custom_clearance_fee,
      "custom_clearance_percent" => $custom_clearance_percent,
      "need_tailgate" => 0,
      "category_id" => $category_id,
      "package_count" => 1,
      "loading_unloading_charges" => 0,
      "from_address_info" => trim($from_full_address),
      "to_address_info" => trim($to_full_address),
      "payment_mode" => trim($payment_mode),
      "cod_payment_type" => trim($cod_payment_type),
      "pickup_code" => "NULL",
      "from_address_email" => trim($from_address_email),
      "to_address_email" => trim($to_address_email),
      "visible_by" => (int)trim($visible_by),
      "is_bank_payment" => (int)$is_bank_payment,
    );
    //echo json_encode($insert_data); die();
    if( $order_id = $this->api->register_new_courier_orders($insert_data) ) {
      $package_array = array(
        "cust_id" => $cust_id,
        "order_id" => $order_id,
        "quantity" => $total_quantity,
        "dimension_id" => $dimension_id,
        "width" => $width,
        "height" => $height,
        "length" => $length,
        "total_weight" => $total_weight,
        "unit_id" => $unit_id,
        "contents" => $order_contents,
        "dangerous_goods_id" => 0,
      );
      $this->transport->insert_package($package_array);
      $this->api->insert_order_status($cust_id, $order_id, 'open');
      //Update Recent Relay
      if($from_relay_id > 0) {
        //check customer recent relay
        $relay_count = $this->user->check_recent_relay($from_relay_id, $cust_id);
        if($relay_count <= 0) {
          $relay_array = array(
            "cust_id" => $cust_id,
            "relay_id" => $from_relay_id,
          );
          $this->api->register_user_new_recent_relay($relay_array);
        }
      }
      if($to_relay_id > 0) {
        //check customer recent relay
        $relay_count = $this->user->check_recent_relay($to_relay_id, $cust_id);
        if($relay_count <= 0) {
           $relay_array = array(
            "cust_id" => $cust_id,
            "relay_id" => $to_relay_id,
          );
          $this->api->register_user_new_recent_relay($relay_array);
        }
      }
      
      //promocode section--------------------------------------------
        if($_POST['total_price_with_promocode'] != "0"){
          $details = $this->api->get_order_detail($order_id);
          $promo_code_name = $this->input->post("hidden_promo_code");
          $promocode = $this->api->get_promocode_by_name($promo_code_name); 
          //code start here
            $discount_amount = round(($details['order_price']/100)*$promocode['discount_percent'] , 2); 
            if($details['currency_sign'] == 'XAF'){
              $discount_amount = round($discount_amount , 0);
            }
            $order_price =  round($details['order_price']-$discount_amount ,2);
            if($details['currency_sign'] == 'XAF'){
              $order_price = round($order_price , 0);
            }
            $advance_payment = round(($details['advance_payment']/100)*$promocode['discount_percent'] ,2);
            if($details['currency_sign'] == 'XAF'){
              $advance_payment = round($advance_payment , 0);
            }
            $pickup_payment = round( ($details['pickup_payment']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $pickup_payment = round($pickup_payment , 0);
            }
            $deliver_payment = round( ($details['deliver_payment']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $deliver_payment = round($deliver_payment , 0);
            }
            $insurance_fee = round( ($details['insurance_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $insurance_fee = round($insurance_fee , 0);
            }
            $commission_amount = round( ($details['commission_amount']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $commission_amount = round($commission_amount , 0);
            }

            $standard_price = round( ($details['standard_price']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $standard_price = round($standard_price , 0);
            }
            $urgent_fee = round( ($details['urgent_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $urgent_fee = round($urgent_fee , 0);
            }

            $handling_fee = round( ($details['handling_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $handling_fee = round($handling_fee , 0);
            }
            $vehicle_fee = round( ($details['vehicle_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $vehicle_fee = round($vehicle_fee , 0);
            }

            $custom_clearance_fee = round( ($details['custom_clearance_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $custom_clearance_fee = round($custom_clearance_fee , 0);
            }

            $loading_unloading_charges = round( ($details['loading_unloading_charges']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $loading_unloading_charges = round($loading_unloading_charges , 0);
            }
            $courier_order = array(
              'promo_code' => $promocode['promo_code'],
              'promo_code_id' => $promocode['promo_code_id'],
              'promo_code_version' => $promocode['promo_code_version'],
              'discount_amount' => $discount_amount,
              'discount_percent' => $promocode['discount_percent'],
              'actual_order_price' => $details['order_price'],
              'order_price' => $order_price ,
              //'advance_payment' => ($details['advance_payment']-$advance_payment),
              //'pickup_payment' => ($details['pickup_payment']-$pickup_payment),
              //'deliver_payment' => ($details['deliver_payment']-$deliver_payment),
              'balance_amount' => $order_price,
              'insurance_fee' => ($details['insurance_fee']-$insurance_fee),
              'commission_amount' => ($details['commission_amount']-$commission_amount),
              'standard_price' => ($details['standard_price']-$standard_price),
              'urgent_fee' => ($details['urgent_fee']-$urgent_fee),
              'handling_fee' =>  ($details['handling_fee']-$handling_fee),
              'vehicle_fee' => ($details['vehicle_fee']-$vehicle_fee),
              'custom_clearance_fee'=>($details['custom_clearance_fee']-$custom_clearance_fee),
              'loading_unloading_charges'=>($details['loading_unloading_charges']-$loading_unloading_charges),
              'promo_code_datetime'=> date('Y-m-d h:i:s'),
            );
            $this->api->update_review($courier_order,$details['order_id']);
            $used_promo_update = array(
              'promo_code_id' => $promocode['promo_code_id'],
              'promo_code_version' => $promocode['promo_code_version'],
              'cust_id' => $this->cust_id,
              'customer_email' => "",
              'discount_percent' => $promocode['discount_percent'],
              'discount_amount' => $discount_amount,
              'discount_currency' => $details['currency_sign'],
              'cre_datetime' => date('Y-m-d h:i:s'),
              'service_id' => '7',
              'booking_id' => $details['order_id'],
            );
            $this->api->register_user_used_prmocode($used_promo_update);
            $this->session->set_flashdata('success_promo', $this->lang->line('Promocode Applied successfully'));
          //code end here
        }
      //promocode section--------------------------------------------

      //Create Pickup Code
      if($order_id <= 99999) { $pickup_code = 'PC'.$cust_id.sprintf("%06s", $order_id); } else { $pickup_code = 'PC'.$cust_id.$order_id; }
      $insert_data = array(
          "pickup_code" => $pickup_code,
      );
      $this->api->update_courier_orders($insert_data, $order_id);
      
      if(trim($payment_mode) == 'bank') {
        $this->session->set_flashdata('success', $this->lang->line('order_created_successfully'));
        redirect('user-panel/my-bookings');
      }
      else if(trim($payment_mode) == 'cod') {
        $this->session->set_flashdata('success', $this->lang->line('order_created_successfully'));
        redirect('user-panel/my-bookings');
      } else {
        $this->session->set_flashdata('success', ['msg' => $this->lang->line('order_created_successfully'), 'courier_order_id' => $order_id]);
        redirect('user-panel/courier-payment');
      }
    } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); redirect('user-panel/add-bookings');   }  
  }
  
  // payment section
  public function courier_payment_old()
  {
    // { ["success"]=> { ["msg"]=> string(27) "Order created successfully!" ["courier_order_id"]=> int(5) } }
    // var_dump($this->session->flashdata()); die();

    if($msg = $this->session->flashdata('success')){  $order_id = $msg['courier_order_id']; } 
    else if( $this->input->post('courier_order_id') > 0 ) { $order_id = $this->input->post('courier_order_id'); } 
    else{ redirect('user-panel/my-bookings'); }
    
    $details =  $this->api->get_order_detail($order_id);
    $this->load->view('user_panel/courier_payment_view',compact('details'));
    $this->load->view('user_panel/footer');
  }

  public function courier_payment()
  {
    if($msg = $this->session->flashdata('success')) { $order_id = $msg['courier_order_id']; 
    } else if( $this->input->post('courier_order_id') > 0 ) { $order_id = $this->input->post('courier_order_id'); 
    } else if($this->session->userdata('order_id')) { $order_id = $this->session->userdata('order_id');
    } else{ redirect('user-panel/my-bookings'); }

    $details =  $this->api->get_order_detail($order_id);
    $this->load->view('user_panel/courier_payment_view',compact('details'));
    $this->load->view('user_panel/footer');
  }

  public function mark_as_cod()
  { 
    //echo json_encode($_POST); die();
    $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
    $cod_payment_type = $this->input->post('cod_payment_type', TRUE);
    $order_details = $this->api->get_order_detail($order_id);
    if($cod_payment_type == 'at_pickup') {
      $update_data_order = array(
        "payment_mode" => 'cod',
        "cod_payment_type" => trim($cod_payment_type),
        "advance_payment" => 0,
        "pickup_payment" => $order_details['order_price'],
        "deliver_payment" => 0,
      );
    }

    if($cod_payment_type == 'at_deliver') {
      $update_data_order = array(
        "payment_mode" => 'cod',
        "cod_payment_type" => trim($cod_payment_type),
        "advance_payment" => 0,
        "pickup_payment" => 0,
        "deliver_payment" => $order_details['order_price'],
      );
    }

    //echo json_encode($update_data_order); die();
    if($this->api->update_payment_type_of_order($update_data_order, $order_id)) {
      $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
    } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }

    redirect('user-panel/my-bookings');
  }
  
  public function confirm_payment()
  {
    $cust_id = (int) $this->cust_id;
    $order_id = $this->input->post('current_order_id', TRUE); $order_id = (int) $order_id;
    $payment_method = $this->input->post('payment_method', TRUE);
    $transaction_id = $this->input->post('stripeToken', TRUE);

    $today = date('Y-m-d h:i:s');
    $order_details = $this->api->get_order_detail($order_id);
    $total_amount_paid = $order_details['order_price'];
    $transfer_account_number = "NULL";
    $bank_name = "NULL";
    $account_holder_name = "NULL";
    $iban = "NULL";
    $email_address = $this->input->post('stripeEmail', TRUE);
    $mobile_number = "NULL";    

    $update_data_order = array(
      "payment_method" => trim($payment_method),
      "paid_amount" => trim($total_amount_paid),
      "transaction_id" => trim($transaction_id),
      "payment_datetime" => $today,
      "complete_paid" => "1",
      "payment_mode" => "payment",
      "balance_amount" => 0,
      "cod_payment_type" => "NULL",
    );

    if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {

      /*************************************** Payment Section ****************************************/
      //Add core price to gonagoo master
        if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
        } else {
          $insert_data_gonagoo_master = array(
            "gonagoo_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($order_details['currency_sign']),
          );
          $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
        }

        $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
        $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
        $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

        $update_data_gonagoo_history = array(
          "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
          "order_id" => (int)$order_id,
          "user_id" => (int)$cust_id,
          "type" => 1,
          "transaction_type" => 'standard_price',
          "amount" => trim($order_details['standard_price']),
          "datetime" => $today,
          "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
          "transfer_account_type" => trim($payment_method),
          "transfer_account_number" => trim($transfer_account_number),
          "bank_name" => trim($bank_name),
          "account_holder_name" => trim($account_holder_name),
          "iban" => trim($iban),
          "email_address" => trim($email_address),
          "mobile_number" => trim($mobile_number),
          "transaction_id" => trim($transaction_id),
          "currency_code" => trim($order_details['currency_sign']),
          "country_id" => trim($order_details['from_country_id']),
          "cat_id" => trim($order_details['category_id']),
        );
        $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
      
      //Update urgent fee in gonagoo account master
        if(trim($order_details['urgent_fee']) > 0) {
          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'urgent_fee',
            "amount" => trim($order_details['urgent_fee']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

      //Update insurance fee in gonagoo account master
        if(trim($order_details['insurance_fee']) > 0) {
          $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'insurance_fee',
            "amount" => trim($order_details['insurance_fee']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

      //Update handling fee in gonagoo account master
        if(trim($order_details['handling_fee']) > 0) {
          $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'handling_fee',
            "amount" => trim($order_details['handling_fee']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

      //Update vehicle fee in gonagoo account master
        if(trim($order_details['vehicle_fee']) > 0) {
          $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
        
          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'vehicle_fee',
            "amount" => trim($order_details['vehicle_fee']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

      //Update custome clearance fee in gonagoo account master
        if(trim($order_details['custom_clearance_fee']) > 0) {
          $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
        
          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'custom_clearance_fee',
            "amount" => trim($order_details['custom_clearance_fee']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

      //Update custome clearance fee in gonagoo account master
        if(trim($order_details['loading_unloading_charges']) > 0) {
          $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
        
          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'loading_unloading_charges',
            "amount" => trim($order_details['loading_unloading_charges']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

      //Add payment details in customer account master and history
        if($this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
        } else {
          $insert_data_customer_master = array(
            "user_id" => $cust_id,
            "account_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($order_details['currency_sign']),
          );
          $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
        }

        $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
        $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$order_id,
          "user_id" => (int)$cust_id,
          "datetime" => $today,
          "type" => 1,
          "transaction_type" => 'add',
          "amount" => trim($total_amount_paid),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "withdraw_request_id" => 0,
          "currency_code" => trim($order_details['currency_sign']),
          "cat_id" => trim($order_details['category_id']),
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);

      //Deduct payment details from customer account master and history
        $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
        $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$order_id,
          "user_id" => (int)$cust_id,
          "datetime" => $today,
          "type" => 0,
          "transaction_type" => 'payment',
          "amount" => trim($total_amount_paid),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "withdraw_request_id" => 0,
          "currency_code" => trim($order_details['currency_sign']),
          "cat_id" => trim($order_details['category_id']),
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);

      /*************************************** Payment Section ****************************************/

      $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
    } else {$this->session->set_flashdata('error', $this->lang->line('payment_failed')); }

    redirect('user-panel/my-bookings');
    $this->load->view('user_panel/confirm_payment_view',compact('response'));
    $this->load->view('user_panel/footer');
  }

  public function confirm_orange_payment()
  {
    //echo json_encode($_POST); die();
    $cust_id = (int) $this->cust_id;
    $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
    $payment_method = $this->input->post('payment_method', TRUE);
    $order_price = $this->input->post('order_price', TRUE);
    $currency_sign = $this->input->post('currency_sign', TRUE);

    $order_payment_id = 'gonagoo_'.time().'_'.$order_id;

    //get access token
    $ch = curl_init("https://api.orange.com/oauth/v2/token?");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
    $orange_token = curl_exec($ch);
    curl_close($ch);
    $token_details = json_decode($orange_token, TRUE);
    //echo json_encode($token_details); die();
    $token = 'Bearer '.$token_details['access_token'];

    $lang = $this->session->userdata('language');
    if($lang=='french') $lang = 'fr';
    else $lang = 'en';

    // get payment link
    $json_post_data = json_encode(
                        array(
                          "merchant_key" => "a8f8c61e", 
                          "currency" => "OUV", 
                          "order_id" => $order_payment_id, 
                          "amount" => "1",  
                          "return_url" => base_url('user-panel/confirm-orange-payment-process'), 
                          "cancel_url" => base_url('user-panel/confirm-orange-payment-process'), 
                          "notif_url" => base_url(), 
                          "lang" => $lang, 
                          "reference" => $this->lang->line('Gonagoo - Service Payment') 
                        )
                      );
    //echo $json_post_data; die();
    $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: ".$token,
        "Accept: application/json"
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
    $orange_url = curl_exec($ch);
    curl_close($ch);
    $url_details = json_decode($orange_url, TRUE);
    //echo json_encode($url_details); die();
    
    if(isset($url_details)) {
      $this->session->set_userdata('notif_token', $url_details['notif_token']);
      $this->session->set_userdata('order_id', $order_id);
      $this->session->set_userdata('order_payment_id', $order_payment_id);
      $this->session->set_userdata('amount', 1);
      $this->session->set_userdata('pay_token', $url_details['pay_token']);
      //echo json_encode($url_details); die();
      header('Location: '.$url_details['payment_url']);
    } else {
      $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
      redirect('user-panel/my-bookings');
    }
  }
  public function confirm_orange_payment_process()
  {
    $cust_id = (int) $this->cust_id;
    $session_token = $_SESSION['notif_token'];
    $order_id = $_SESSION['order_id'];
    $order_details = $this->api->get_order_detail($order_id);
    $order_payment_id = $_SESSION['order_payment_id'];
    $amount = $_SESSION['amount'];
    $pay_token = $_SESSION['pay_token'];
    $payment_method = 'orange';
    $today = date('Y-m-d h:i:s');
    //get access token
    $ch = curl_init("https://api.orange.com/oauth/v2/token?");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
    $orange_token = curl_exec($ch);
    curl_close($ch);
    $token_details = json_decode($orange_token, TRUE);
    $token = 'Bearer '.$token_details['access_token'];

    // get payment link
    $json_post_data = json_encode(
                        array(
                          "order_id" => $order_payment_id, 
                          "amount" => $amount, 
                          "pay_token" => $pay_token
                        )
                      );
    //echo $json_post_data; die();
    $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: ".$token,
        "Accept: application/json"
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
    $payment_res = curl_exec($ch);
    curl_close($ch);
    $payment_details = json_decode($payment_res, TRUE);
    $transaction_id = $payment_details['txnid'];
    //echo json_encode($payment_details); die();

    if($payment_details['status'] == 'SUCCESS' && $order_details['complete_paid'] == 0) {
      $total_amount_paid = $order_details['order_price'];
      $transfer_account_number = "NULL";
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = $this->input->post('stripeEmail', TRUE);
      $mobile_number = "NULL";    

      $update_data_order = array(
        "payment_method" => trim($payment_method),
        "balance_amount" => 0,
        "paid_amount" => trim($total_amount_paid),
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "complete_paid" => "1",
        "payment_mode" => "payment",
        "cod_payment_type" => "NULL",
      );
      if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {

        /*************************************** Payment Section ****************************************/
        //Add core price to gonagoo master-----------------
          if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'standard_price',
            "amount" => trim($order_details['standard_price']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        //-------------------------------------------------
        
        //Update urgent fee in gonagoo account master------
          if(trim($order_details['urgent_fee']) > 0) {
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'urgent_fee',
              "amount" => trim($order_details['urgent_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------
        //Update insurance fee in gonagoo account master---
          if(trim($order_details['insurance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'insurance_fee',
              "amount" => trim($order_details['insurance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------
        //Update handling fee in gonagoo account master----
          if(trim($order_details['handling_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'handling_fee',
              "amount" => trim($order_details['handling_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------
        //Update vehicle fee in gonagoo account master-----
          if(trim($order_details['vehicle_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'vehicle_fee',
              "amount" => trim($order_details['vehicle_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------

        //Update custome clearance fee in gonagoo account master
          if(trim($order_details['custom_clearance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'custom_clearance_fee',
              "amount" => trim($order_details['custom_clearance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------

        //Update custome clearance fee in gonagoo account master
          if(trim($order_details['loading_unloading_charges']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'loading_unloading_charges',
              "amount" => trim($order_details['loading_unloading_charges']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------

        //Add payment details in customer account master and history
          if($this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          }

          $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        //-------------------------------------------------

        //Deduct payment details from customer account master and history
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        //-------------------------------------------------
        /*************************************** Payment Section ****************************************/
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } 
    } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }

    redirect('user-panel/my-bookings');
    $this->load->view('user_panel/confirm_payment_view',compact('response'));
    $this->load->view('user_panel/footer');
  }
  
  // removed - not in use
  public function advance_payment()
  {
    $this->load->view('user_panel/advance_payment_view');
    $this->load->view('user_panel/footer');
  }
  // removed - not in use
  public function edit_my_booking()
  {
    $this->load->view('user_panel/my_bookings_edit_view');
    $this->load->view('user_panel/footer');
  }

  public function requested_deliverer()
  {   
    $order_id = $this->input->post('courier_order_id'); 
    $cust_id = $this->cust_id;
    $deliverers = $this->user->get_requested_deliverers($cust_id, $order_id);
    // echo json_encode($deliverers); die();
    $this->load->view('user_panel/requested_deliverer_list_view', compact('deliverers', 'order_id'));
    $this->load->view('user_panel/footer');
  }

  public function deliverer_details()
  {
    $cust_id = $this->cust_id;
    $id = $this->input->post('deliverer_id');
    $courier_order_id = $this->input->post('courier_order_id');
    $requested = $this->input->post('requested_deliverer');
    $profile = $this->api->get_deliverer_profile($id);
    $customer = $this->api->get_consumer_datails($id);
    $reviews = $this->api->get_reviews($id);
    $accepted = $this->user->get_statuswise_order_counts($id,'accept');
    $in_progress = $this->user->get_statuswise_order_counts($id,'in_progress');
    $delivered = $this->user->get_statuswise_order_counts($id,'delivered');
    $verify_insurence = $this->user->verify_deliverer_document($id,'insurance');
    $verify_certificates = $this->user->verify_deliverer_document($id,'business');
    $is_delivered = $this->user->is_deliverer_delivered($id);

    // echo json_encode($is_delivered); die();
    $compact = compact('id','requested','profile','customer','reviews','accepted','in_progress','delivered', 'courier_order_id','cust_id','verify_insurence','verify_certificates','is_delivered');
    $this->load->view('user_panel/deliverer_details_view',$compact);
    $this->load->view('user_panel/footer');
  }

  public function find_deliverer()
  { 
    //echo json_encode($_POST); //die();
    $courier_order_id = $this->input->post('courier_order_id');
    $order_details = $this->api->get_order_detail($courier_order_id);
    $customer_details = $this->api->get_user_details($order_details['cust_id']);
    
    $country_id = $this->input->post('country_id_src'); $country_id = (!empty($country_id))? (int) $country_id : 0;
    $state_id = $this->input->post('state_id_src'); $state_id = (!empty($state_id))? (int) $state_id : 0;
    $city_id = $this->input->post('city_id_src'); $city_id = (!empty($city_id))? (int) $city_id : 0;
    $rating = $this->input->post('rating_filter'); $rating = (!empty($rating))? (int) $rating : "0";
    $favorite = $this->input->post('favorite'); $favorite = ($favorite == 'on')? 1 : 1;

    $countries = $this->user->get_countries();
    $country_name = $this->user->get_country_name_by_id($country_id);
    $state_name = $this->user->get_state_name_by_id($state_id);
    $city_name = $this->user->get_city_name_by_id($city_id);
    
    $country_id1 = ($country_id>0)? (int) $country_id : $customer_details['country_id'];
    
    $filter = array(
      "country_id" => $country_id1,
      "state_id" => $state_id,
      "city_id" => $city_id,
      "rating" => $rating,
      "favorite" => $favorite,
    );  

    $deliverers = $this->user->find_deliverer_against_order($courier_order_id, $this->cust_id, $filter);
    $favourite_deliverer_ids = $this->user->get_favourite_deliverers_ids($this->cust_id);
     //echo json_encode($deliverers); die();
    $this->load->view('user_panel/find_deliverer_list_view',compact("courier_order_id","deliverers","favourite_deliverer_ids","countries","filter","country_id","state_id","city_id","rating","country_name","state_name","city_name","favorite"));
    $this->load->view('user_panel/footer');
  }
  public function make_deliverer_favourite()
  {
    $deliverer_id = $this->input->post('id');
    if($this->api->register_new_favourite_deliverer($this->cust_id, $deliverer_id)){ echo 'success'; } else{ echo 'failed'; }
  }

  public function remove_deliverer_from_favourite()
  {
    $deliverer_id = $this->input->post('id');
    if($this->api->remove_favourite_deliverer($this->cust_id, $deliverer_id)){ echo 'success'; } else{ echo 'failed'; }
  }

  public function send_request_to_deliverer()
  {   
    //echo json_encode($_POST); die();
    $cust_id = (int) $this->cust_id;
    $deliverer_id = $this->input->post('deliverer_id', TRUE); $deliverer_id = (int) $deliverer_id;
    $order_id = $this->input->post('courier_order_id', TRUE); $order_id = (int) $order_id;
    $order_details = $this->api->get_order_detail($order_id);
    $shipping_mode = $this->api->get_deliverer_shipping_mode($deliverer_id);
    $request_status = $this->api->get_deliverer_request_status($cust_id, $deliverer_id, $order_id);
    $today = date("Y-m-d H:i:s");   
    
    if($shipping_mode['shipping_mode'] == '0') {      
      $register_data = array( "cust_id" => $cust_id, "deliverer_id" => $deliverer_id, "order_id" => $order_id, "cre_datetime" => $today );

      if($request_status == 0 ) {
        if( $id = $this->api->booking_delivery_request($register_data) ) {
        //echo json_encode($id); die();
          //Get Deliverer Device ID's
          $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
          // var_dump($device_details_deleverer); die();
          if(!is_null($device_details_deleverer)) {
            $arr_deleverer_fcm_ids = array();
            $arr_deleverer_apn_ids = array();
            foreach ($device_details_deleverer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_deleverer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_deleverer_apn_ids, $value['reg_id']); }
            }
            $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
            $deleverer_apn_ids = $arr_deleverer_apn_ids;

            //Get PN API Keys and PEM files
            $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
            //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
            
            $order_details = $this->api->get_order_detail($order_id);
            
            if( trim($order_details['cust_name']) == 'NULL') {
              $customer_details = $this->api->get_user_details($order_details['cust_id']);
              if($customer_details['firstname']!='NULL'){
                $cust_name = trim($customer_details['firstname'])." ".trim($customer_details['lastname']);
              } else {
                $email_cut = explode("@", $customer_details['email1']);
                $cust_name = strtoupper($email_cut[0]);
              }
            } else {
              $cust_name = trim($order_details['cust_name']);
            }

            $message = $this->lang->line('Please, accept the new order No ').$order_id.". ".$this->lang->line('Customer ').$cust_name.". ".$this->lang->line('From ').$order_details['from_address']." ".$this->lang->line('To ').$order_details['to_address'].". ".$order_details['total_weight'].$this->api->get_unit_master_details($order_details['unit_id']);
            
            //$message = $this->lang->line('order_delivery_request_msg');
            $msg =  array('title' => $this->lang->line('order_delivery_request'), 'type' => 'delivery-request', 'notice_date' => $today, 'desc' => $message);

            $this->api->sendFCM($msg, $deleverer_fcm_ids, $api_key_deliverer);
              //Push Notification APN
              $msg_apn_deliverer =  array('title' => $this->lang->line('order_delivery_request'), 'text' => $message);
              if(is_array($deleverer_apn_ids)) { 
                $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
                $this->notification->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
              }
            //send SMS to deliverer
            $deliverer_details = $this->api->get_user_details($deliverer_id);           
            $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
            if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
            $this->api->sendSMS((int)$country_code.trim($mobile1), $message);
          }
          echo 'success';
        } else {  echo 'failed'; }
      } else { echo "exists"; }
    } else {
      if($order_id <= 99999) { $delivery_code = 'DC'.$cust_id.sprintf("%06s", $order_id); } else { $delivery_code = 'DC'.$cust_id.$order_id; }
      //Get customer and deliverer contact and personal details
      $customer_details = $this->api->get_user_details($cust_id);
      $deliverer_details = $this->api->get_user_details($deliverer_id);

      if($deliverer_details['firstname'] == 'NULL') {
        if($deliverer_details['company_name'] == 'NULL') {
          $email_cut = explode('@', $deliverer_details['email1']);  
          $dname = $email_cut[0];
        } else {
          $dname = $deliverer_details['company_name'];
        }
      } else {
        $dname = $deliverer_details['firstname'] . ' ' . $deliverer_details['lastname'];
      }

      if($customer_details['firstname'] == 'NULL') {
        if($customer_details['company_name'] == 'NULL') {
          $email_cut = explode('@', $customer_details['email1']);  
          $cname = $email_cut[0];
        } else {
          $cname = $customer_details['company_name'];
        }
      } else {
        $cname = $customer_details['firstname'] . ' ' . $customer_details['lastname'];
      }
      $deliverer_name = $dname;
      $deliverer_contact = $deliverer_details['mobile1'];
      $deliverer_profile_details = $this->api->get_deliverer_profile($deliverer_id);
      $deliverer_company = $deliverer_profile_details['company_name'];
      if( $this->api->update_order_status($deliverer_id, $order_id, 'accept', $delivery_code, $deliverer_name, $deliverer_company, $deliverer_contact) ) {
        $this->api->insert_order_status($deliverer_id, $order_id, 'accept');
        $this->api->remove_order_request($order_id, $deliverer_id);

        //Get Users device Details
        $device_details_customer = $this->api->get_user_device_details($cust_id);
        //Get Customer Device Reg ID's
        $arr_customer_fcm_ids = array();
        $arr_customer_apn_ids = array();
        foreach ($device_details_customer as $value) {
          if($value['device_type'] == 1) {
            array_push($arr_customer_fcm_ids, $value['reg_id']);
          } else {
            array_push($arr_customer_apn_ids, $value['reg_id']);
          }
        }
        $customer_fcm_ids = $arr_customer_fcm_ids;
        $customer_apn_ids = $arr_customer_apn_ids;
        //Get Deliverer Device ID's
        $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
        $arr_deleverer_fcm_ids = array();
        $arr_deleverer_apn_ids = array();
        foreach ($device_details_deleverer as $value) {
          if($value['device_type'] == 1) {
            array_push($arr_deleverer_fcm_ids, $value['reg_id']);
          } else {
            array_push($arr_deleverer_apn_ids, $value['reg_id']);
          }
        }
        $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
        $deleverer_apn_ids = $arr_deleverer_apn_ids;
        //Get PN API Keys and PEM files
        $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
        //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

        //Send Notifications as per order status
        $sms_msg_customer = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . $delivery_code;

        $sms_msg_from_contact = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . trim($order_details['pickup_code']);
        
        $sms_msg_to_contact = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . $delivery_code;
        
        $sms_msg_deliverer = $this->lang->line('order_accept_deliverer_msg1') . $cname . $this->lang->line('order_accept_deliverer_msg2') . $order_id . $this->lang->line('order_accept_msg3') . $delivery_code . $this->lang->line('order_accept_msg4') . trim($order_details['pickup_code']);
        
        $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
        if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
        $this->api->sendSMS($country_code.trim($mobile1), $sms_msg_customer);

        if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
        $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
        $this->api->sendSMS($country_code.trim($mobile1), $sms_msg_deliverer);

        if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
        $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg_from_contact);

        if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
        $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg_to_contact);

        //Email Order Placed confirmation deliverer
        $subject_deliverer = $this->lang->line('order_accept_deliverer_email_subject');
        $message ="<p class='lead'>". $sms_msg_deliverer . "</p>";
        $message .="</td></tr><tr><td align='center'><br />";
        $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
        $email1 = trim($deliverer_details['email1']);
        $this->api_sms->send_email_text($username, $email1, $subject_deliverer, trim($message));

        //Email Order Placed confirmation Customer
        $subject_customer = $this->lang->line('order_accept_email_subject');
        $message ="<p class='lead'>". $sms_msg_customer . "</p>";
        $message .="</td></tr><tr><td align='center'><br />";
        $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
        $email1 = trim($customer_details['email1']);
        $this->api_sms->send_email_text($username, $email1, $subject_customer, trim($message));

        //Email Order accept confirmation From contact
        $message ="<p class='lead'>". $sms_msg_from_contact . "</p>";
        $message .="</td></tr><tr><td align='center'><br />";
        $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject_customer, trim($message));

        //Email Order accept confirmation To contact
        $message ="<p class='lead'>". $sms_msg_to_contact . "</p>";
        $message .="</td></tr><tr><td align='center'><br />";
        $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject_customer, trim($message));

        //Push Notification FCM
        $msg_pn_customer =  array('title' => $subject_customer, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_customer);
        $msg_pn_deliverer =  array('title' => $subject_deliverer, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_deliverer);
        $this->api->sendFCM($msg_pn_customer, $customer_fcm_ids, $api_key_deliverer);
        $this->api->sendFCM($msg_pn_deliverer, $deleverer_fcm_ids, $api_key_deliverer);
        
        //Push Notification APN
        $msg_apn_customer =  array('title' => $subject_customer, 'text' => $sms_msg_customer);
        if(is_array($customer_apn_ids)) { 
          $customer_apn_ids = implode(',', $customer_apn_ids);
          $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
        }
        $msg_apn_deliverer =  array('title' => $subject_deliverer, 'text' => $sms_msg_deliverer);
        if(is_array($deleverer_apn_ids)) { 
          $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
          $this->notification->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
        }
        
        //Update to workroom
        $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('order_accepted_workroom'), 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');

        //Update to Global Workroom
        $global_workroom_update = array(
          'service_id' => $order_id,
          'cust_id' => $cust_id,
          'deliverer_id' => $deliverer_id,
          'sender_id' => $deliverer_id,
          'cat_id' => $order_details['category_id']
        );
        $this->api->create_global_workroom($global_workroom_update);
        echo 'success';
      } else { echo 'failed'; }
    }
  }
  
  public function favourite_deliverers()
  {
    $deliverers = $this->api->get_favourite_deliverers($this->cust_id);
    // echo json_encode($deliverers); die();
    $this->load->view('user_panel/favourite_deliverers_view',compact('deliverers'));    
    $this->load->view('user_panel/footer');
  }

  public function search_for_deliverers()
  { 
    //echo json_encode($_POST); die();
    $cust_id = (int) $this->cust_id;
    $customer_details = $this->api->get_user_details((int)$cust_id);
    
    $country_id = $this->input->post('country_id_src'); $country_id = (!empty($country_id))? (int) $country_id : 0;
    $state_id = $this->input->post('state_id_src'); $state_id = (!empty($state_id))? (int) $state_id : 0;
    $city_id = $this->input->post('city_id_src'); $city_id = (!empty($city_id))? (int) $city_id : 0;
    $rating = $this->input->post('rating_filter'); $rating = (!empty($rating))? (int) $rating : "0";
    $favorite = $this->input->post('favorite'); $favorite = ($favorite == 'on')? 1 : 1;

    $countries = $this->user->get_countries();
    $country_name = $this->user->get_country_name_by_id($country_id);
    $state_name = $this->user->get_state_name_by_id($state_id);
    $city_name = $this->user->get_city_name_by_id($city_id);
    
    $country_id1 = ($country_id>0)? (int) $country_id : $customer_details['country_id'];
    
    $filter = array(
      "country_id" => $country_id1,
      "state_id" => $state_id,
      "city_id" => $city_id,
      "rating" => $rating,
      "favorite" => $favorite,
    );  

    $deliverers = $this->user->search_for_deliverers($cust_id, $filter);
    $favourite_deliverer_ids = $this->user->get_favourite_deliverers_ids($this->cust_id);
     //echo json_encode($deliverers); die();
    $this->load->view('user_panel/search_for_deliverer_list_view',compact("deliverers","favourite_deliverer_ids","countries","filter","country_id","state_id","city_id","rating","country_name","state_name","city_name","favorite"));
    $this->load->view('user_panel/footer');
  }

  public function search_deliverer_details_view()
  {
    $cust_id = $this->cust_id;
    $id = $this->input->post('deliverer_id');
    $courier_order_id = $this->input->post('courier_order_id');
    $requested = $this->input->post('requested_deliverer');
    $profile = $this->api->get_deliverer_profile($id);
    $customer = $this->api->get_consumer_datails($id);
    $reviews = $this->api->get_reviews($id);
    $accepted = $this->user->get_statuswise_order_counts($id,'accept');
    $in_progress = $this->user->get_statuswise_order_counts($id,'in_progress');
    $delivered = $this->user->get_statuswise_order_counts($id,'delivered');
    $verify_insurence = $this->user->verify_deliverer_document($id,'insurance');
    $verify_certificates = $this->user->verify_deliverer_document($id,'business');
    $is_delivered = $this->user->is_deliverer_delivered($id);

    // echo json_encode($is_delivered); die();
    $compact = compact('id','requested','profile','customer','reviews','accepted','in_progress','delivered', 'courier_order_id','cust_id','verify_insurence','verify_certificates','is_delivered');
    $this->load->view('user_panel/search_deliverer_details_view',$compact);
    $this->load->view('user_panel/footer');
  }

  public function accepted_bookings()
  {
    $countries = $this->user->get_countries();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id); 
    //Filters
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');

    //  New params in filter
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || ($cat_id > 0)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
        $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
        $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
      } else { $creation_start_date = $creation_end_date = null; }
      
      if(trim($pickup_start) != "" && trim($pickup_end) != "") {
        $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
        $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
      } else { $pickup_start_date = $pickup_end_date = null; }

      if(trim($delivery_start) != "" && trim($delivery_end) != "") {
        $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
        $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
      } else { $delivery_start_date = $delivery_end_date = null; }

      if(trim($expiry_start) != "" && trim($expiry_end) != "") {
        $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
        $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
      } else { $expiry_start_date = $expiry_end_date = null; }

      $filter_array = array(
        "user_id" => $this->cust_id,
        "user_type" => "cust",
        "order_status" => 'accept',
        "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
        "from_address" => ($from_address!=null)? $from_address: "NULL",
        "to_address" => ($to_address!=null)? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!=null)? $$pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
        "price" => ($price!=null)?$price:"0",
        "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
        "order_type" => ($order_type!=null)? $order_type: "NULL",
        // new filters
        "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=null)? $distance_start: "0",
        "distance_end" => ($distance_end!=null)? $distance_end: "0",
        "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=null)?$max_weight:"0",
        "unit_id" => ($unit_id != null)?$unit_id:"0",
        "cat_id" => ($cat_id != null)?$cat_id:"0",
      );
      
      $orders = $this->user->filtered_list($filter_array);      
      $filter = 'advance';
      $this->load->view('user_panel/accepted_bookings_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id'));
    } else {
      $orders = $this->user->get_booking_list($this->cust_id, 'accept');
      $filter = 'basic'; $cat_id = 0;
      $this->load->view('user_panel/accepted_bookings_list_view', compact('orders','countries','dimensions','filter','unit','earth_trans','air_trans','sea_trans','cat_id'));
    }   
    $this->load->view('user_panel/footer');
  }
  
  
  public function accept_booking_order_details()
  {
    $id = $this->uri->segment(3);
    $order = $this->api->get_order_detail($id);
    $packages = $this->user->get_order_packages($id);
    $order_status = $this->api->order_status_list($id);
    $this->load->view('user_panel/accept_booking_order_details', compact('order', 'order_status','packages'));
    $this->load->view('user_panel/footer');
  }

  public function in_progress_bookings()
  {   
    $countries = $this->user->get_countries();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id);   
    //Filters
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || ($cat_id > 0)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
          $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
          $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
        } else { $creation_start_date = $creation_end_date = null; }
        
        if(trim($pickup_start) != "" && trim($pickup_end) != "") {
          $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
          $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
        } else { $pickup_start_date = $pickup_end_date = null; }

        if(trim($delivery_start) != "" && trim($delivery_end) != "") {
          $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
          $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
        } else { $delivery_start_date = $delivery_end_date = null; }

        if(trim($expiry_start) != "" && trim($expiry_end) != "") {
          $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
          $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
        } else { $expiry_start_date = $expiry_end_date = null; }

      $filter_array = array(
        "user_id" => $this->cust_id,
        "user_type" => "cust",
        "order_status" => 'in_progress',
        "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
        "from_address" => ($from_address!=null)? $from_address: "NULL",
        "to_address" => ($to_address!=null)? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!=null)? $pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
        "price" => ($price!=null)?$price:"0",
        "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
        "order_type" => ($order_type!=null)? $order_type: "NULL",
        "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=null)? $distance_start: "0",
        "distance_end" => ($distance_end!=null)? $distance_end: "0",
        "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=null)?$max_weight:"0",
        "unit_id" => ($unit_id != null)?$unit_id:"0",
        "cat_id" => ($cat_id != null)?$cat_id:"0",
      );
      
      $orders = $this->user->filtered_list($filter_array);
      $filter = 'advance';
      $this->load->view('user_panel/in_progress_bookings_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id'));
    } else {
      $orders = $this->user->get_booking_list($this->cust_id, 'in_progress');
      // echo json_encode($orders); die();
      $filter = 'basic'; $cat_id = 0;
      $this->load->view('user_panel/in_progress_bookings_list_view', compact('orders','countries','dimensions','filter','unit','earth_trans','air_trans','sea_trans','cat_id'));
    }   
    $this->load->view('user_panel/footer');
  }
  
  public function in_progress_orders_details()
  {
    $id = $this->uri->segment(3);
    $order = $this->api->get_order_detail($id);
    $packages = $this->user->get_order_packages($id);
    $order_status = $this->api->order_status_list($id);
    $this->load->view('user_panel/in_progress_orders_details', compact('order', 'order_status','packages'));
    $this->load->view('user_panel/footer');
  }
  
  public function delivered_bookings()
  {   
    $countries = $this->user->get_countries();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id);  
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || ($cat_id > 0)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
          $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
          $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
        } else { $creation_start_date = $creation_end_date = null; }
        
        if(trim($pickup_start) != "" && trim($pickup_end) != "") {
          $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
          $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
        } else { $pickup_start_date = $pickup_end_date = null; }

        if(trim($delivery_start) != "" && trim($delivery_end) != "") {
          $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
          $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
        } else { $delivery_start_date = $delivery_end_date = null; }

        if(trim($expiry_start) != "" && trim($expiry_end) != "") {
          $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
          $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
        } else { $expiry_start_date = $expiry_end_date = null; }

      $filter_array = array(
        "user_id" => $this->cust_id,
        "user_type" => "cust",
        "order_status" => 'delivered',
        "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
        "from_address" => ($from_address!=null)? $from_address: "NULL",
        "to_address" => ($to_address!=null)? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!=null)? $pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
        "price" => ($price!=null)?$price:"0",
        "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
        "order_type" => ($order_type!=null)? $order_type: "NULL",
        "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=null)? $distance_start: "0",
        "distance_end" => ($distance_end!=null)? $distance_end: "0",
        "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=null)?$max_weight:"0",
        "unit_id" => ($unit_id != null)?$unit_id:"0",
        "cat_id" => ($cat_id != null)?$cat_id:"0",
      );
      
      $orders = $this->user->filtered_list($filter_array);
      $filter = 'advance';
      $this->load->view('user_panel/delivered_bookings_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id'));
    } else {
      $orders = $this->user->get_booking_list($this->cust_id, 'delivered');
      // echo json_encode($orders); die();
      $filter = 'basic'; $cat_id = 0;
      $this->load->view('user_panel/delivered_bookings_list_view', compact('orders','countries','dimensions','filter','unit','earth_trans','air_trans','sea_trans','cat_id'));
    }   
    $this->load->view('user_panel/footer');
  }
  
  public function delivered_orders_details()
  {
    $id = $this->uri->segment(3);
    $order = $this->api->get_order_detail($id);
    $order_status = $this->api->order_status_list($id);
    $this->load->view('user_panel/delivered_orders_details', compact('order', 'order_status'));
    $this->load->view('user_panel/footer');
  }

  public function open_bookings()
  {
    if($deliverer = $this->api->get_deliverer_profile($this->cust_id)) 
    { 
      $cust = $this->cust; 
      $driver_count = (int) $cust['driver_count'];
      $cust_id = $this->cust_id;

      if( $driver_count > 0) 
      { 
        $countries = $this->user->get_countries();
        $dimensions = $this->api->get_standard_dimension_masters($this->cust_id);   
        //Filters
        $from_country_id = $this->input->post('country_id_src');
        $from_state_id = $this->input->post('state_id_src');
        $from_city_id = $this->input->post('city_id_src');
        $to_country_id = $this->input->post('country_id_dest');
        $to_state_id = $this->input->post('state_id_dest');
        $to_city_id = $this->input->post('city_id_dest');
        $from_address = $this->input->post('from_address');
        $to_address = $this->input->post('to_address');   
        $delivery_start = $this->input->post('delivery_start');
        $delivery_end = $this->input->post('delivery_end');
        $pickup_start = $this->input->post('pickup_start');
        $pickup_end = $this->input->post('pickup_end');
        $price = $this->input->post('price');
        $dimension_id = $this->input->post('dimension_id');
        $order_type = $this->input->post('order_type');
        $max_weight = $this->input->post('max_weight');
        $unit_id = $this->input->post('c_unit_id');
        $creation_start = $this->input->post('creation_start');
        $creation_end = $this->input->post('creation_end');   
        $expiry_start = $this->input->post('expiry_start');
        $expiry_end = $this->input->post('expiry_end');
        $distance_start = $this->input->post('distance_start');
        $distance_end = $this->input->post('distance_end');
        $transport_type = $this->input->post('transport_type');
        $vehicle_id = $this->input->post('vehicle');
        $cat_id = $this->input->post('cat_id');

        $unit = $this->api->get_unit_master(true);
        $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
        $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
        $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

        if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || $cat_id > 0) {

          if(trim($creation_start) != "" && trim($creation_end) != "") {
            $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
            $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
          } else { $creation_start_date = $creation_end_date = null; }

          if(trim($pickup_start) != "" && trim($pickup_end) != "") {
            $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
            $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
          } else { $pickup_start_date = $pickup_end_date = null; }

          if(trim($delivery_start) != "" && trim($delivery_end) != "") {
            $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
            $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
          } else { $delivery_start_date = $delivery_end_date = null; }

          if(trim($expiry_start) != "" && trim($expiry_end) != "") {
            $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
            $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
          } else { $expiry_start_date = $expiry_end_date = null; }

          $filter_array = array(
            "user_id" => $this->cust_id,
            "user_type" => "deliverer",
            "order_status" => 'open',
            "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
            "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
            "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
            "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
            "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
            "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
            "from_address" => ($from_address!=null)? $from_address: "NULL",
            "to_address" => ($to_address!=null)? $to_address: "NULL",
            "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
            "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
            "pickup_start_date" => ($pickup_start_date!=null)? $pickup_start_date: "NULL",
            "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
            "price" => ($price!=null)?$price:"0",
            "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
            "order_type" => ($order_type!=null)? $order_type: "NULL",
            "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
            "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
            "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
            "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
            "distance_start" => ($distance_start!=null)? $distance_start: "0",
            "distance_end" => ($distance_end!=null)? $distance_end: "0",
            "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
            "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
            "max_weight" => ($max_weight!=null)?$max_weight:"0",
            "unit_id" => ($unit_id != null)?$unit_id:"0",
            "cat_id" => ($cat_id != null)?$cat_id:"0",
          );

          //$orders = $this->user->open_booking_list_filtered($this->cust_id, 'open', $country_id_src, $state_id_src,$city_id_src,$country_id_dest,$state_id_dest,$city_id_dest,$from_address,$to_address,$delivery_start,$delivery_end,$pickup_start,$pickup_end,$price,$dimension_id,$order_type);
          $orders = $this->user->filtered_list($filter_array);
          //echo $this->db->last_query(); die();
          $filter = 'advance';
          $this->load->view('user_panel/open_bookings_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id','cust_id'));
        } else {
          $orders = $this->user->open_booking_list($this->cust_id, 'open');
          //echo $this->db->last_query(); die();
          $filter = 'basic'; $cat_id = 0;
          $this->load->view('user_panel/open_bookings_list_view', compact('orders','countries','dimensions','filter','unit','earth_trans','air_trans','sea_trans','cat_id','cust_id'));
        }
      } else{ redirect(base_url('user-panel/driver-list'));  }
    } else {
      $countries = $this->user->get_countries($this->cust_id);
      $carrier_type = $this->user->carrier_type_list();
      $cust_id = $this->cust_id;
      $this->load->view('user_panel/create_deliverer_profile_view', compact('cust_id','countries','cust','carrier_type'));
    }
    $this->load->view('user_panel/footer');
  }

  public function open_bookings___()
  {
    if($deliverer = $this->api->get_deliverer_profile($this->cust_id)) {
      $cust = $this->cust; 
      $driver_count = (int) $cust['driver_count'];

      if( $driver_count > 0) { 
        $countries = $this->user->get_countries();
        $dimensions = $this->api->get_standard_dimension_masters($this->cust_id); 
        $from_country_id = $this->input->post('country_id_src');
        $from_state_id = $this->input->post('state_id_src');
        $from_city_id = $this->input->post('city_id_src');
        $to_country_id = $this->input->post('country_id_dest');
        $to_state_id = $this->input->post('state_id_dest');
        $to_city_id = $this->input->post('city_id_dest');
        $from_address = $this->input->post('from_address');
        $to_address = $this->input->post('to_address');   
        $delivery_start = $this->input->post('delivery_start');
        $delivery_end = $this->input->post('delivery_end');
        $pickup_start = $this->input->post('pickup_start');
        $pickup_end = $this->input->post('pickup_end');
        $price = $this->input->post('price');
        $dimension_id = $this->input->post('dimension_id');
        $order_type = $this->input->post('order_type');
        $max_weight = $this->input->post('max_weight');
        $unit_id = $this->input->post('c_unit_id');
        $creation_start = $this->input->post('creation_start');
        $creation_end = $this->input->post('creation_end');   
        $expiry_start = $this->input->post('expiry_start');
        $expiry_end = $this->input->post('expiry_end');
        $distance_start = $this->input->post('distance_start');
        $distance_end = $this->input->post('distance_end');
        $transport_type = $this->input->post('transport_type');
        $vehicle_id = $this->input->post('vehicle');

        $unit = $this->api->get_unit_master(true);
        $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
        $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
        $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

        if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type)) {

          if(trim($creation_start) != "" && trim($creation_end) != "") {
              $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
              $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
            } else { $creation_start_date = $creation_end_date = null; }
            
            if(trim($pickup_start) != "" && trim($pickup_end) != "") {
              $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
              $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
            } else { $pickup_start_date = $pickup_end_date = null; }

            if(trim($delivery_start) != "" && trim($delivery_end) != "") {
              $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
              $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
            } else { $delivery_start_date = $delivery_end_date = null; }

            if(trim($expiry_start) != "" && trim($expiry_end) != "") {
              $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
              $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
            } else { $expiry_start_date = $expiry_end_date = null; }

            $filter_array = array(
              "user_id" => $this->cust_id,
              "user_type" => "deliverer",
              "order_status" => 'open',
              "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
              "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
              "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
              "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
              "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
              "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
              "from_address" => ($from_address!=null)? $from_address: "NULL",
              "to_address" => ($to_address!=null)? $to_address: "NULL",
              "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
              "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
              "pickup_start_date" => ($pickup_start_date!=null)? $$pickup_start_date: "NULL",
              "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
              "price" => ($price!=null)?$price:"0",
              "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
              "order_type" => ($order_type!=null)? $order_type: "NULL",
              "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
              "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
              "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
              "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
              "distance_start" => ($distance_start!=null)? $distance_start: "0",
              "distance_end" => ($distance_end!=null)? $distance_end: "0",
              "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
              "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
              "max_weight" => ($max_weight!=null)?$max_weight:"0",
              "unit_id" => ($unit_id != null)?$unit_id:"0",
            );

            //$orders = $this->user->filtered_list($filter_array);
            //echo $this->db->last_query(); die();
            $orders = $this->user->open_booking_list_filtered($this->cust_id, 'open', $country_id_src, $state_id_src,$city_id_src,$country_id_dest,$state_id_dest,$city_id_dest,$from_address,$to_address,$delivery_start,$delivery_end,$pickup_start,$pickup_end,$price,$dimension_id,$order_type);
            $filter = 'advance';
            $this->load->view('user_panel/open_bookings_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans'));
          } else{ redirect(base_url('user-panel/driver-list'));
        }
      } else {
        $orders = $this->user->open_booking_list($this->cust_id, 'open');
        $filter = 'basic';
        $this->load->view('user_panel/open_bookings_list_view', compact('orders','countries','dimensions','filter','unit','earth_trans','air_trans','sea_trans'));
      }        
    } else {
      $countries = $this->user->get_countries($this->cust_id);
      $carrier_type = $this->user->carrier_type_list();
      $cust_id = $this->cust_id;
      $this->load->view('user_panel/create_deliverer_profile_view', compact('cust_id','countries','cust','carrier_type'));
    }
    $this->load->view('user_panel/footer');
  }

  public function open_bookings_searched()
  { 
    $cat_name = $this->input->post('cat_id');
    $location = $this->input->post('address');
    $orders = $this->api->get_open_booking_list_front($cat_name, $location);
    $this->load->view('user_panel/open_bookings_searched_list_view', compact('orders'));
    $this->load->view('user_panel/footer');

  }

  public function open_order_details()
  {
    if( !is_null($this->input->post('order_id')) ) { $order_id = (int) $this->input->post('order_id'); }
    else if(!is_null($this->uri->segment(3))) { $order_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/open-bookings'); }
    $order = $this->api->get_order_detail($order_id);
    $packages = $this->user->get_order_packages($order_id);
    if($order['order_status'] == 'open') {
      $order_status = $this->api->order_status_list($order_id);
      $this->load->view('user_panel/open_order_details_view', compact('order','order_status','packages'));
      $this->load->view('user_panel/footer');
    } else {
      redirect('user-panel/open-bookings','refresh');
    }
  }

  public function order_requests()
  {
    $countries = $this->user->get_countries();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id);  
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || ($cat_id > 0)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
      $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
      $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
      } else { $creation_start_date = $creation_end_date = null; }

      if(trim($pickup_start) != "" && trim($pickup_end) != "") {
      $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
      $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
      } else { $pickup_start_date = $pickup_end_date = null; }

      if(trim($delivery_start) != "" && trim($delivery_end) != "") {
      $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
      $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
      } else { $delivery_start_date = $delivery_end_date = null; }

      if(trim($expiry_start) != "" && trim($expiry_end) != "") {
      $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
      $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
      } else { $expiry_start_date = $expiry_end_date = null; }

      $country_id_src = $from_country_id = ($from_country_id!=null)? $from_country_id: "0";
      $state_id_src = $from_state_id = ($from_state_id!=null)? $from_state_id: "0";
      $city_id_src = $from_city_id = ($from_city_id!=null)? $from_city_id: "0";
      $country_id_dest = $to_country_id = ($to_country_id!=null)? $to_country_id: "0";
      $state_id_dest = $to_state_id = ($to_state_id!=null)? $to_state_id: "0";
      $city_id_dest = $to_city_id = ($to_city_id!=null)? $to_city_id: "0";
      $from_address = ($from_address!=null)? $from_address: "NULL";
      $to_address = ($to_address!=null)? $to_address: "NULL";
      $delivery_start_form = $delivery_start_date = ($delivery_start_date!=null)? $delivery_start_date: "NULL";
      $delivery_end_form = $delivery_end_date = ($delivery_end_date!=null)? $delivery_end_date: "NULL";
      $pickup_start_form = $pickup_start_date = ($pickup_start_date!=null)? $pickup_start_date: "NULL";
      $pickup_end_form = $pickup_end_date = ($pickup_end_date!=null)? $pickup_end_date: "NULL";
      $price = ($price!=null)?$price:"0";
      $dimension_id = ($dimension_id!=null)?$dimension_id:"0";
      $order_type = ($order_type!=null)? $order_type: "NULL";
      $creation_start = $creation_start_date = ($creation_start_date!=null)? $creation_start_date: "NULL";
      $creation_end = $creation_end_date = ($creation_end_date!=null)? $creation_end_date: "NULL";
      $expiry_start = $expiry_start_date = ($expiry_start_date!=null)? $expiry_start_date: "NULL";
      $expiry_end = $expiry_end_date = ($expiry_end_date!=null)? $expiry_end_date: "NULL";
      $distance_start = ($distance_start!=null)? $distance_start: "0";
      $distance_end = ($distance_end!=null)? $distance_end: "0";
      $transport_type = ($transport_type!=null)? $transport_type: "NULL";
      $vehicle_id = ($vehicle_id!=null)? $vehicle_id: "0";
      $max_weight = ($max_weight!=null)?$max_weight:"0";
      $unit_id = ($unit_id != null)?$unit_id:"0";
      $cat_id = ($cat_id != null)?$cat_id:"0";
     

      $orders = $this->user->deliverer_order_request_list_filtered($this->cust_id, $from_country_id, $from_state_id,$from_city_id,$to_country_id,$to_state_id,$to_city_id,$from_address,$to_address,$delivery_start_date,$delivery_end_date,$pickup_start_date,$pickup_end_date,$price,$dimension_id,$order_type,$creation_start,$creation_end,$expiry_start,$expiry_end,$distance_start,$distance_end,$transport_type,$vehicle_id,$max_weight,$unit_id,$cat_id);
      //var_dump($this->db->last_query()); die();
      $filter = 'advance';
      $this->load->view('user_panel/order_requests_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id'));
    } else {
      $orders = $this->user->deliverer_order_request_list($this->cust_id);
      $filter = 'basic'; $cat_id = 0;
      $this->load->view('user_panel/order_requests_list_view', compact('orders','countries','dimensions','filter','unit','default_trans','cat_id'));
    }
    $this->load->view('user_panel/footer');
  }

  public function order_accepted_by_deliverer()
  {
    $deliverer_id = (int) $this->cust_id;
    $order_id = $this->input->post('id', TRUE);
    $order_status = $this->input->post('status', TRUE);
    $order_details = $this->api->get_order_detail($order_id);
    $cust_id = (int) $order_details['cust_id'];
    $today = date('Y-m-d h:i:s');
    
    if($order_details['order_status'] == 'open') {
      if($order_status == 'accept') { 
        if($order_id <= 99999) { $delivery_code = 'DC'.$cust_id.sprintf("%06s", $order_id); } else { $delivery_code = 'DC'.$cust_id.$order_id; }
        //Get customer and deliverer contact and personal details
        $customer_details = $this->api->get_user_details($cust_id);
        $deliverer_details = $this->api->get_user_details($deliverer_id);

        if($deliverer_details['firstname'] == 'NULL') {
          if($deliverer_details['company_name'] == 'NULL') {
            $email_cut = explode('@', $deliverer_details['email1']);  
            $dname = $email_cut[0];
          } else {
            $dname = $deliverer_details['company_name'];
          }
        } else {
          $dname = $deliverer_details['firstname'] . ' ' . $deliverer_details['lastname'];
        }

        if($customer_details['firstname'] == 'NULL') {
          if($customer_details['company_name'] == 'NULL') {
            $email_cut = explode('@', $customer_details['email1']);  
            $cname = $email_cut[0];
          } else {
            $cname = $customer_details['company_name'];
          }
        } else {
          $cname = $customer_details['firstname'] . ' ' . $customer_details['lastname'];
        }
        $deliverer_name = $dname;

        $deliverer_contact = $deliverer_details['mobile1'];
        $deliverer_profile_details = $this->api->get_deliverer_profile($deliverer_id);
        $deliverer_company = $deliverer_profile_details['company_name'];
        if($this->api->update_order_status($deliverer_id, $order_id, 'accept', $delivery_code, $deliverer_name, $deliverer_company, $deliverer_contact)) { 
          $this->api->insert_order_status($deliverer_id, $order_id, 'accept');
          $this->api->remove_order_request($order_id, $deliverer_id);
          //Get Users device Details
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          //Get Customer Device Reg ID's
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_customer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_customer_apn_ids, $value['reg_id']);
            }
          }
          $customer_fcm_ids = $arr_customer_fcm_ids;
          $customer_apn_ids = $arr_customer_apn_ids;
          //Get Deliverer Device ID's
          $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
          $arr_deleverer_fcm_ids = array();
          $arr_deleverer_apn_ids = array();
          foreach ($device_details_deleverer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_deleverer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_deleverer_apn_ids, $value['reg_id']);
            }
          }
          $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
          $deleverer_apn_ids = $arr_deleverer_apn_ids;
          //Get PN API Keys and PEM files
          $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
          $api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
          //Send Notifications as per order status

          $sms_msg_customer = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . $delivery_code . $this->lang->line('order_accept_msg4') . trim($order_details['pickup_code']);
          
          $sms_msg_from_contact = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . trim($order_details['pickup_code']);
          
          $sms_msg_to_contact = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . $delivery_code;

          $sms_msg_deliverer = $this->lang->line('order_accept_deliverer_msg1') . $cname . $this->lang->line('order_accept_deliverer_msg2') . $order_id . $this->lang->line('order_accept_msg3') . $delivery_code . $this->lang->line('order_accept_msg4') . trim($order_details['pickup_code']);

          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($customer_details['country_id'])).trim($mobile1), $sms_msg_customer);

          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($deliverer_details['country_id'])).trim($mobile1), $sms_msg_deliverer);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg_from_contact);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg_to_contact);

          //Email Order accept confirmation deliverer
          $subject_deliverer = $this->lang->line('order_accept_deliverer_email_subject');
          $message ="<p class='lead'>". $sms_msg_deliverer . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject_deliverer, trim($message));
          
          //Email Order accept confirmation Customer
          $subject_customer = $this->lang->line('order_accept_email_subject');
          $message ="<p class='lead'>". $sms_msg_customer . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
          $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject_customer, trim($message));

          //Email Order accept confirmation From contact
          $message ="<p class='lead'>". $sms_msg_from_contact . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject_customer, trim($message));

          //Email Order accept confirmation To contact
          $message ="<p class='lead'>". $sms_msg_to_contact . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject_customer, trim($message));

          //Push Notification FCM
          $msg_pn_customer =  array('title' => $subject_customer, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_customer);
          $this->api->sendFCM($msg_pn_customer, $customer_fcm_ids, $api_key_deliverer);
          //APN
          $msg_apn_customer =  array('title' => $subject_customer, 'text' => $sms_msg_customer);
          
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          
          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('order_accepted_workroom'), 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
          //Update to Global Workroom
          $global_workroom_update = array(
            'service_id' => $order_id,
            'cust_id' => $cust_id,
            'deliverer_id' => $deliverer_id,
            'sender_id' => $deliverer_id,
            'cat_id' => $order_details['category_id']
          );
          $this->api->create_global_workroom($global_workroom_update);

          echo 'order_accepted_workroom';
        } else { echo 'order_accept_failed'; }
      } else { 
        if( $this->api->insert_order_status($deliverer_id, $order_id, 'reject') ) {
          $reject_history = array(
            "order_id" => (int)$order_id,
            "cust_id" => (int)$cust_id,
            "deliverer_id" => (int)$deliverer_id,
            "rejected_datetime" => $today,
            "from_country_id" => (int) $order_details['from_country_id'],
            "cat_id" => (int) $order_details['category_id'],
          );
          $this->api->register_rejected_order_history($reject_history);
          $this->api->remove_order_request($order_id, $deliverer_id);
          //Get customer and deliverer contact and personal details
          $customer_details = $this->api->get_user_details($cust_id);
          $deliverer_details = $this->api->get_user_details($deliverer_id);
          //Get Users device Details
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          //Get Customer Device Reg ID's
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_customer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_customer_apn_ids, $value['reg_id']);
            }
          }
          $customer_fcm_ids = $arr_customer_fcm_ids;
          $customer_apn_ids = $arr_customer_apn_ids;
          //Get Deliverer Device ID's
          $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
          $arr_deleverer_fcm_ids = array();
          $arr_deleverer_apn_ids = array();
          foreach ($device_details_deleverer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_deleverer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_deleverer_apn_ids, $value['reg_id']);
            }
          }
          $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
          $deleverer_apn_ids = $arr_deleverer_apn_ids;
          //Get PN API Keys and PEM files
          $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
          $api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
          //Send Notifications as per order status
          if($deliverer_details['firstname'] == 'NULL') {
            if($deliverer_details['company_name'] == 'NULL') {
              $email_cut = explode('@', $deliverer_details['email1']);  
              $dname = $email_cut[0];
            } else {
              $dname = $deliverer_details['company_name'];
            }
          } else {
              $dname = $deliverer_details['firstname'] . ' ' . $deliverer_details['lastname'];
          }
          $sms_msg_customer = $this->lang->line('order_reject_msg1') . $order_id . $this->lang->line('order_reject_msg2') . $dname;
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($customer_details['country_id'])).trim($mobile1), $sms_msg_customer);
          
          //Email Order Reject confirmation Customer
          $subject_customer = $this->lang->line('order_reject_email_subject');
          $message ="<p class='lead'>". $sms_msg_customer . "</p>";
            $message .="</td></tr><tr><td align='center'><br />";
            $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
            $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject_customer, trim($message));

          //Push Notification FCM
          $msg_pn_customer =  array('title' => $subject_customer, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_customer);
          $this->api->sendFCM($msg_pn_customer, $customer_fcm_ids, $api_key_deliverer);
          
          //Push Notification APN
          $msg_apn_customer =  array('title' => $subject_customer, 'text' => $sms_msg_customer);
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          echo 'order_rejected_deliverer'; 
        } else { echo 'order_reject_failed'; }
      }
    } else { echo 'already_accepted'; }
  }

  public function requested_order_details()
  {
    if( !is_null($this->input->post('order_id')) ) { $order_id = (int) $this->input->post('order_id'); }
    else if(!is_null($this->uri->segment(3))) { $order_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/order-requests'); }
    // check deliverer vs order
    if($this->user->verify_deliverer_in_order_request($order_id, $this->cust_id)) {
      $order = $this->api->get_order_detail($order_id);
    $packages = $this->user->get_order_packages($order_id);
      $order_status = $this->api->order_status_list($order_id);
      $this->load->view('user_panel/requested_order_details_view', compact('order','order_status','packages'));
      $this->load->view('user_panel/footer');
    } else {
      redirect('user-panel/order-requests','refresh');
    }
  }

  public function accepted_orders()
  {
    $countries = $this->user->get_countries();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id); 
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || ($cat_id > 0)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
        $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
        $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
      } else { $creation_start_date = $creation_end_date = null; }

      if(trim($pickup_start) != "" && trim($pickup_end) != "") {
        $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
        $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
      } else { $pickup_start_date = $pickup_end_date = null; }

      if(trim($delivery_start) != "" && trim($delivery_end) != "") {
        $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
        $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
      } else { $delivery_start_date = $delivery_end_date = null; }

      if(trim($expiry_start) != "" && trim($expiry_end) != "") {
        $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
        $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
      } else { $expiry_start_date = $expiry_end_date = null; }

      $filter_array = array(
        "user_id" => $this->cust_id,
        "user_type" => "deliverer",
        "order_status" => 'accept',
        "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
        "from_address" => ($from_address!=null)? $from_address: "NULL",
        "to_address" => ($to_address!=null)? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!=null)? $pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
        "price" => ($price!=null)?$price:"0",
        "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
        "order_type" => ($order_type!=null)? $order_type: "NULL",
        "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=null)? $distance_start: "0",
        "distance_end" => ($distance_end!=null)? $distance_end: "0",
        "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=null)?$max_weight:"0",
        "unit_id" => ($unit_id != null)?$unit_id:"0",
        "cat_id" => ($cat_id != null)?$cat_id:"0",
      );

      //$orders = $this->user->deliverer_order_list_filtered($this->cust_id, 'accept', $country_id_src, $state_id_src,$city_id_src,$country_id_dest,$state_id_dest,$city_id_dest,$from_address,$to_address,$delivery_start,$delivery_end,$pickup_start,$pickup_end,$price,$dimension_id,$order_type);
      $orders = $this->user->filtered_list($filter_array);
      $filter = 'advance';
      $this->load->view('user_panel/accepted_orders_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id'));
    } else {
      $orders = $this->user->deliverer_order_list($this->cust_id, 'accept');
      $filter = 'basic'; $cat_id = 0;
      $this->load->view('user_panel/accepted_orders_list_view', compact('orders','countries','dimensions','filter','unit','default_trans','cat_id'));
    }
    $this->load->view('user_panel/footer');
  }

  public function assign_driver()
  {
    if( !is_null($this->input->post('order_id')) ) { $order_id = (int) $this->input->post('order_id'); }
    else if(!is_null($this->uri->segment(3))) { $order_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/accepted-orders'); }
    if($this->user->verify_deliverer_in_order($order_id, $this->cust_id)) {
      $order = $this->api->get_order_detail($order_id);
      $packages = $this->user->get_order_packages($order_id);
      $order_status = $this->api->order_status_list($order_id);
      $drivers = $this->api->get_customer_drivers($this->cust_id);
      $this->load->view('user_panel/accepted_order_details_view', compact('order','drivers','order_status','packages'));
      $this->load->view('user_panel/footer');
    } else {
      redirect('user-panel/accepted-orders','refresh');
    }
  }

  public function assign_order_driver()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->cust_id;
    $cd_id = $this->input->post('driver_id', TRUE); $cd_id = (int) $cd_id;
    $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
    $cd_name = $this->user->get_driver_name_by_id($cd_id);
    $order_details = $this->api->get_order_detail((int)$order_id);

    if( $this->api->update_order_status_deliverer($cd_id, $order_id, trim($cd_name)) ) {
      $this->api->insert_order_status($cust_id, $order_id, 'assign');
      //Get Driver contact and personal details
      $driver_details = $this->api->get_driver_details($cd_id);
      $deliverer_details = $this->api->get_user_details($cust_id);
      //Get Driver device Details
      $device_details_driver = $this->api->get_driver_device_details($cd_id);
      //Get Driver Device Reg ID's
      $arr_driver_fcm_ids = array();
      $arr_driver_apn_ids = array();
      foreach ($device_details_driver as $value) {
        if($value['device_type'] == 1) {
          array_push($arr_driver_fcm_ids, $value['reg_id']);
        } else {
          array_push($arr_driver_apn_ids, $value['reg_id']);
        }
      }
      $driver_fcm_ids = $arr_driver_fcm_ids;
      $driver_apn_ids = $arr_driver_apn_ids;
      //Get PN API Keys and PEM files
      $api_key_driver = $this->config->item('driverAppGoogleKey');
      //$api_key_driver_pem = $this->config->item('driverAppPemFile');
      //Send Notifications as per order status
      if($deliverer_details['firstname'] == 'NULL') {
        if($deliverer_details['company_name'] == 'NULL') {
          $email_cut = explode('@', $deliverer_details['email1']);  
          $dname = $email_cut[0];
        } else {
          $dname = $deliverer_details['company_name'];
        }
      } else {
        $dname = $deliverer_details['firstname'] . ' ' . $deliverer_details['lastname'];
      }
      $cname = 'User';
      if($order_details['cust_name'] == 'NULL' || $order_details['cust_name'] == 'NULL NULL') {
        $canme = $this->api->get_customer_email_name_by_order_id($order_details['cust_id']);
      } else {
        $cname = $order_details['cust_name'];
      }

      $sms_msg_driver = $this->lang->line('order_assign_msg1') . $dname . $this->lang->line('order_assign_msg2') . $order_id . ' ' 
                      . $this->lang->line('customer_name') . $cname . ' ' 
                      . $this->lang->line('from:') . $order_details['from_address'] . ' ' 
                      . $this->lang->line('to:') . $order_details['to_address'] . ' ' 
                      . $this->lang->line('pickup_at') . $order_details['pickup_datetime'] . ' ' 
                      . $this->lang->line('deliver_at') . $order_details['delivery_datetime'];

      if(substr(trim($driver_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($driver_details['mobile1']), 0); } else { $mobile1 = trim($driver_details['mobile1']); }
      if(!empty($driver_details['country_code']) && !is_null($driver_details['country_code'])) { $this->api->sendSMS(trim($driver_details['country_code']).trim($mobile1), $sms_msg_driver); }

      //Email Order assign to driver
      $subject_driver = $this->lang->line('order_assign_driver_email_subject');
      $message ="<p class='lead'>". $sms_msg_driver . "</p>";
      $message .="</td></tr><tr><td align='center'><br />";
      $username = trim($driver_details['first_name']) . trim($driver_details['last_name']);
      $email1 = trim($driver_details['email']);
      $this->api_sms->send_email_text($username, $email1, $subject_driver, trim($message));
      //Push Notification FCM
      $msg_pn_driver =  array('title' => $subject_driver, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_driver);
      $this->api->sendFCM($msg_pn_driver, $driver_fcm_ids, $api_key_driver);
      //Push Notification APN
      $msg_apn_driver =  array('title' => $subject_driver, 'text' => $sms_msg_driver);
      if(is_array($driver_apn_ids)) { 
        $driver_apn_ids = implode(',', $driver_apn_ids);
        $return = $this->notification->sendPushIOS($msg_apn_driver, $driver_apn_ids);
      }
      
      $order_details = $this->api->get_order_detail($order_id);
      //check order of dedicated users
      if(trim($order_details['is_bank_payment']) == 0) {
        //Payment Module-------------------------------------------------------------------------------------
          $advance_payment_count = $this->api->check_advance_transfer_in_deliverer_account_master($order_id);
          if($advance_payment_count == 0) {
            $today = date('Y-m-d h:i:s');
            if(trim($order_details['advance_payment']) > 0 && trim($order_details['payment_mode']) != 'cod') {
              //update payment details in gonagoo account master and history
              if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - (trim($order_details['advance_payment'])-trim($order_details['insurance_fee'])-trim($order_details['custom_clearance_fee']));
              $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['deliverer_id'],
                "type" => 0,
                "transaction_type" => 'advance_payment',
                "amount" => (trim($order_details['advance_payment'])-trim($order_details['insurance_fee'])-trim($order_details['custom_clearance_fee'])),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

              //Add advance payment details in deliverer account master and history
              if($this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']))) {
                $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
              } else {
                $insert_data_customer_master = array(
                  "user_id" => (int)$order_details['deliverer_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
              }

              $account_balance = trim($customer_account_master_details['account_balance']) + (trim($order_details['advance_payment'])-trim($order_details['insurance_fee'])-trim($order_details['custom_clearance_fee']));
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], (int)$order_details['deliverer_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));

              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['deliverer_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance_payment',
                "amount" => (trim($order_details['advance_payment'])-trim($order_details['insurance_fee'])-trim($order_details['custom_clearance_fee'])),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            }

            //Deduct deliverer commission from deliverer account master and history
            if($customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']))) {
            } else {
              $insert_data_customer_master = array(
                "user_id" => (int)$order_details['deliverer_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($order_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
            }

            $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($order_details['commission_amount']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], (int)$order_details['deliverer_id'], $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));

            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['deliverer_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'commission_amount',
              "amount" => trim($order_details['commission_amount']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($order_details['currency_sign']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);

            //add deiverer commission to gonagoo account master and history
            if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($order_details['currency_sign']),
              );
              $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['commission_amount']);
            $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['deliverer_id'],
              "type" => 1,
              "transaction_type" => 'commission_amount',
              "amount" => trim($order_details['commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

            if(trim($order_details['payment_mode']) != 'cod') {
              //Add pickup amount in deliverer scrow account and history
              if($this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']))) {
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
              } else {
                $insert_data_customer_scrow = array(
                  "deliverer_id" => (int)$order_details['deliverer_id'],
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api->insert_gonagoo_customer_scrow_record($insert_data_customer_scrow);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
              }
              if( trim($order_details['pickup_payment']) > 0 ) {
                $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) + trim($order_details['pickup_payment']);
                $this->api->update_payment_in_deliverer_scrow((int)$deliverer_scrow_master_details['scrow_id'], (int)$order_details['deliverer_id'], $scrow_balance);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));

                $update_data_scrow_history = array(
                  "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                  "order_id" => (int)$order_id,
                  "deliverer_id" => (int)$order_details['deliverer_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'pickup_payment',
                  "amount" => trim($order_details['pickup_payment']),
                  "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_scrow_history($update_data_scrow_history);
              }

              //Add delivery amount in deliverer scrow account and history
              if( trim($order_details['deliver_payment']) > 0 ) {
                $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) + trim($order_details['deliver_payment']);
                $this->api->update_payment_in_deliverer_scrow((int)$deliverer_scrow_master_details['scrow_id'], (int)$order_details['deliverer_id'], $scrow_balance);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));

                $update_data_scrow_history = array(
                  "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                  "order_id" => (int)$order_id,
                  "deliverer_id" => (int)$order_details['deliverer_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'deliver_payment',
                  "amount" => trim($order_details['deliver_payment']),
                  "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_scrow_history($update_data_scrow_history);
              }
            }
             
            /*send commission invoice to deliverer---------------------*/
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference("$order_id");
              $invoice->setDate(date('M dS ,Y',time()));

              $deliverer_country = $this->api->get_country_details(trim($deliverer_details['country_id']));
              $deliverer_state = $this->api->get_state_details(trim($deliverer_details['state_id']));
              $deliverer_city = $this->api->get_city_details(trim($deliverer_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($deliverer_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setFrom(array(trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']),trim($deliverer_city['city_name']),trim($deliverer_state['state_name']),trim($deliverer_country['country_name']),trim($deliverer_details['email1'])));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));

              /* Adding Items in table */
              $order_for = $this->lang->line('gonagoo_commission');
              $rate = round(trim($order_details['commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('commission_paid'));

              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              //$invoice->addTitle("Other Details");
              /* Add Paragraph */
              //$invoice->addParagraph("Any Other Details");
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($order_id.'_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

              //Update File path
              $pdf_name = $order_id.'_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/order-invoices/".$pdf_name);

              //Update Order Invoice and post to workroom
              $this->api->update_deliverer_invoice_url($order_id, "order-invoices/".$pdf_name);
            /*---------------------------------------------------------*/
            /*-----------------Email Invoice to customer---------------*/
              $eol = PHP_EOL;
              $messageBody = $eol . $this->lang->line('invoice_email_message') . $eol;
              $file = "resources/order-invoices/".$pdf_name;
              $file_size = filesize($file);
              $handle = fopen($file, "r");
              $content = fread($handle, $file_size);
              fclose($handle);
              $content = chunk_split(base64_encode($content));
              $uid = md5(uniqid(time()));
              $name = basename($file);
              //$eol = PHP_EOL;
              $from_mail = $this->config->item('from_email');
              $replyto = $this->config->item('from_email');
              // Basic headers
              $header = "From: ".$gonagoo_address['company_name']." <".$from_mail.">".$eol;
              $header .= "Reply-To: ".$replyto.$eol;
              $header .= "MIME-Version: 2.0\r\n";
              $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";

              // Put everything else in $message
              $message = "--".$uid.$eol;
              $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
              $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
              $message .= $messageBody.$eol;
              $message .= "--".$uid.$eol;
              $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
              $message .= "Content-Transfer-Encoding: base64".$eol;
              $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
              $message .= $content.$eol;
              $message .= "--".$uid."--";
              mail($deliverer_details['email1'], $this->lang->line('commission_invoice_email_subject'), $message, $header);
              //mail('fade@datasoma.com', $this->lang->line('commission_invoice_email_subject'), $message, $header);
            /*-----------------Email Invoice to customer---------------*/
            //die();
            //post payment in workroom
            $this->api->post_to_workroom($order_id, trim($order_details['cust_id']), $cust_id, $this->lang->line('advance_payment_recieved'), 'sr', 'payment', 'NULL', trim($order_details['cust_name']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL'); 
          }
        //Payment Module End----------------------------------------------------------
        $this->session->set_flashdata('success', $this->lang->line('order_assigned_driver_success'));
      } else { $this->session->set_flashdata('success', $this->lang->line('order_assigned_driver_success')); }
    } else { $this->session->set_flashdata('error',$this->lang->line('order_assigned_driver_failed')); }
    redirect('user-panel/assign-driver/'.$order_id);
  }

  public function in_progress_orders()
  {
    $countries = $this->user->get_countries();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id); 
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || ($cat_id > 0)) {

        if(trim($creation_start) != "" && trim($creation_end) != "") {
          $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
          $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
        } else { $creation_start_date = $creation_end_date = null; }

        if(trim($pickup_start) != "" && trim($pickup_end) != "") {
          $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
          $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
        } else { $pickup_start_date = $pickup_end_date = null; }

        if(trim($delivery_start) != "" && trim($delivery_end) != "") {
          $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
          $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
        } else { $delivery_start_date = $delivery_end_date = null; }

        if(trim($expiry_start) != "" && trim($expiry_end) != "") {
          $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
          $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
        } else { $expiry_start_date = $expiry_end_date = null; }

        $filter_array = array(
          "user_id" => $this->cust_id,
          "user_type" => "deliverer",
          "order_status" => 'in_progress',
          "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
          "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
          "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
          "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
          "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
          "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
          "from_address" => ($from_address!=null)? $from_address: "NULL",
          "to_address" => ($to_address!=null)? $to_address: "NULL",
          "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
          "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
          "pickup_start_date" => ($pickup_start_date!=null)? $$pickup_start_date: "NULL",
          "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
          "price" => ($price!=null)?$price:"0",
          "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
          "order_type" => ($order_type!=null)? $order_type: "NULL",
          "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
          "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
          "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
          "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
          "distance_start" => ($distance_start!=null)? $distance_start: "0",
          "distance_end" => ($distance_end!=null)? $distance_end: "0",
          "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
          "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
          "max_weight" => ($max_weight!=null)?$max_weight:"0",
          "unit_id" => ($unit_id != null)?$unit_id:"0",
          "cat_id" => ($cat_id != null)?$cat_id:"0",
        );

      //$orders = $this->user->in_progress_order_list_filtered($this->cust_id, 'in_progress', $country_id_src, $state_id_src,$city_id_src,$country_id_dest,$state_id_dest,$city_id_dest,$from_address,$to_address,$delivery_start,$delivery_end,$pickup_start,$pickup_end,$price,$dimension_id,$order_type);
            $orders = $this->user->filtered_list($filter_array);
      $filter = 'advance';
      $this->load->view('user_panel/in_progress_orders_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id'));
    } else {
      $orders = $this->user->in_progress_order_list($this->cust_id, 'in_progress');
      $filter = 'basic';  $cat_id = 0;
      $this->load->view('user_panel/in_progress_orders_list_view', compact('orders','countries','dimensions','filter','unit','default_trans','cat_id'));
    }   
    $this->load->view('user_panel/footer');
  }

  public function in_progress_order_details()
  {
    if( !is_null($this->input->post('order_id')) ) { $order_id = (int) $this->input->post('order_id'); }
    else if(!is_null($this->uri->segment(3))) { $order_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/in-progress-orders','refresh'); }
    // check deliverer vs order
    if($this->user->verify_deliverer_in_order($order_id, $this->cust_id)) {
      $order = $this->api->get_order_detail($order_id);
    $packages = $this->user->get_order_packages($order_id);
      $order_status = $this->api->order_status_list($order_id);
      //var_dump($order); die();
      $this->load->view('user_panel/in_progress_order_details_view', compact('order','order_status','packages'));
      $this->load->view('user_panel/footer');
    } else {
      redirect('user-panel/in-progress-orders','refresh');
    }
  }

  public function delivered_orders()
  {
    $countries = $this->user->get_countries();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id);
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type) || ($cat_id > 0)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
        $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
        $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
      } else { $creation_start_date = $creation_end_date = null; }

      if(trim($pickup_start) != "" && trim($pickup_end) != "") {
        $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
        $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
      } else { $pickup_start_date = $pickup_end_date = null; }

      if(trim($delivery_start) != "" && trim($delivery_end) != "") {
        $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
        $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
      } else { $delivery_start_date = $delivery_end_date = null; }

      if(trim($expiry_start) != "" && trim($expiry_end) != "") {
        $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
        $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
      } else { $expiry_start_date = $expiry_end_date = null; }

      $filter_array = array(
        "user_id" => $this->cust_id,
        "user_type" => "deliverer",
        "order_status" => 'delivere',
        "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
        "from_address" => ($from_address!=null)? $from_address: "NULL",
        "to_address" => ($to_address!=null)? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!=null)? $$pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
        "price" => ($price!=null)?$price:"0",
        "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
        "order_type" => ($order_type!=null)? $order_type: "NULL",
        "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=null)? $distance_start: "0",
        "distance_end" => ($distance_end!=null)? $distance_end: "0",
        "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=null)?$max_weight:"0",
        "unit_id" => ($unit_id != null)?$unit_id:"0",
        "cat_id" => ($cat_id != null)?$cat_id:"0",
      );

      //$orders = $this->user->delivered_order_list_filtered($this->cust_id, 'delivered', $country_id_src, $state_id_src,$city_id_src,$country_id_dest,$state_id_dest,$city_id_dest,$from_address,$to_address,$delivery_start,$delivery_end,$pickup_start,$pickup_end,$price,$dimension_id,$order_type);
      $orders = $this->user->filtered_list($filter_array);
      $filter = 'advance';
      $this->load->view('user_panel/delivered_orders_list_view', compact('orders','countries','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans','cat_id'));
    } else {
      $orders = $this->user->delivered_order_list($this->cust_id, 'delivered');
      $filter = 'basic'; $cat_id = 0;
      $this->load->view('user_panel/delivered_orders_list_view', compact('orders','countries','dimensions','filter','unit','default_trans','cat_id'));
    }   
    $this->load->view('user_panel/footer');
  }

  public function delivered_order_details()
  {
    if( !is_null($this->input->post('order_id')) ) { $order_id = (int) $this->input->post('order_id'); }
    else if(!is_null($this->uri->segment(3))) { $order_id = (int)$this->uri->segment(3); }
    else { redirect('user-panel/delivered-orders','refresh'); }
    // check deliverer vs order
    if($this->user->verify_deliverer_in_order($order_id, $this->cust_id)) {
      $order = $this->api->get_order_detail($order_id);
    $packages = $this->user->get_order_packages($order_id);
      $order_status = $this->api->order_status_list($order_id);
      //var_dump($order); die();
      $this->load->view('user_panel/delivered_order_details_view', compact('order','order_status','packages'));
      $this->load->view('user_panel/footer');
    } else {
      redirect('user-panel/delivered-orders','refresh');
    }
  }

  public function in_progress_booking_details()
  {
    $id = $this->uri->segment(3);
    $order = $this->api->get_order_detail($id);
    $order_status = $this->api->order_status_list($id);
    $packages = $this->user->get_order_packages($id);
    $this->load->view('user_panel/in_progress_booking_details', compact('order', 'order_status','packages'));
    $this->load->view('user_panel/footer');
  }

  public function delivered_booking_details()
  {
    $id = $this->uri->segment(3);
    $order = $this->api->get_order_detail($id);
    $packages = $this->user->get_order_packages($id);
    $order_status = $this->api->order_status_list($id);
    $this->load->view('user_panel/delivered_booking_details', compact('order', 'order_status','packages'));
    $this->load->view('user_panel/footer');
  }

  public function booking_details()
  {
    $this->load->view('user_panel/booking_details_view');
    $this->load->view('user_panel/footer');
  }

  // NOTIFICATIONS
  public function user_notifications()
  {
    $cust = $this->cust;
    if($cust['user_type']) { $type = "Individuals"; } else { $type = "Business"; }
    $notifications = $this->api->get_notifications($cust['cust_id'], $type);        
    //echo $this->db->last_query();  die();
    $this->load->view('user_panel/notifications_view',compact('notifications'));
    $this->load->view('user_panel/footer'); 
  }

  public function notification_detail()
  {
    $id = $this->input->post("id");
    $notice = $this->api->get_notification($id);
    $this->load->view('user_panel/notifications_detail_view', compact('notice'));
    $this->load->view('user_panel/footer'); 
  }

  public function notifications_reads()
  {
    $cust = $this->cust;
    if($cust['user_type'] == 1 ){ $type ="Individuals"; } else { $type ="Business"; }
    $notification = $this->api->get_total_unread_notifications($cust['cust_id'], $type);
    foreach ($notification as $n) {
      $this->api->make_notification_read($n['id'], $cust['cust_id']);
    }
    echo 'success';
  }

  public function chatroom()
  {   
    $order_id = $this->input->post('order_id');
    if(!$order_id) { $order_id = $this->session->userdata('workroom_order_id'); }

    $order = $this->api->get_order_detail($order_id);   
    if($order){ 
      $driver_id = $order['driver_id'];
      $keys = ['driver_id' => $driver_id, 'cust_id' => $this->cust['cust_id'], 'order_id' => $order_id];
      $chats = $this->api->get_order_chat($order_id);
      $this->load->view('user_panel/chatroom_view', compact('chats','keys'));     
      $this->load->view('user_panel/footer'); 
    } else { redirect('user-panel/in-progress-bookings'); }
  } 

  public function add_order_chat()
  {
    $cust = $this->cust;
    $order_id = $this->input->post('order_id', TRUE);
    $text_msg = $this->input->post('message_text', TRUE);

    $order = $this->api->get_order_detail($order_id);

    $cust_id = $order['cust_id'];
    $driver_id = $order['driver_id'];
    $sender_id = $cust['cust_id'];
    $cust_name = $order['cust_name'];
    $cd_name = $order['cd_name'];
    $today = date("Y-m-d H:i:s");

    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachment')) {   
        $uploads    = $this->upload->data();  
        $attachment_url =  $config['upload_path'].$uploads["file_name"];  
      } else { $attachment_url = "NULL"; }
    } else { $attachment_url = "NULL"; }


    $insert_data = array(
      "sender_id" => $sender_id,
      "receiver_id" => $driver_id,
      "cd_id" => $order['driver_id'],
      "cust_id" => $cust_id,
      "text_msg" => trim($text_msg),
      "attachment_url" => $attachment_url,
      "cre_datetime" => $today,
      "file_type" => "text",
      "order_id" => $order_id,
      "cd_name" => $cd_name,
      "cust_name" => $cust_name,
    );

    if( $chat_id = $this->user->add_chat_to_chatroom($insert_data)){

      $device_details = $this->api->get_driver_device_details($driver_id); 

      //Get Customer Device Reg ID's
      $arr_fcm_ids = $arr_apn_ids = array();
      foreach ($device_details as $value) {
        if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
        else {  array_push($arr_apn_ids, $value['reg_id']); }
      }

      $fcm_ids = $arr_fcm_ids;
      $apn_ids = $arr_apn_ids;

      //Get PN API Keys and PEM files
      $api_key_customer = $this->config->item('delivererAppGoogleKey');
      //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

      $msg =  array(
        'title' => 'Gonagoo - New message', 
        'type' => 'driver_chat', 
        'notice_date' => $today, 
        'desc' => $text_msg,
        'attachment' => $attachment_url,
        'cust_name' => $cust_name, 
        'order_id' => $order_id, 
        'cust_id' => $cust_id, 
        'file_type' => "text", 
      );
            //Push Notification FCM
      $this->api->sendFCM($msg, $fcm_ids, $api_key_customer);
      //Push Notification APN
      $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
      if(is_array($apn_ids)) { 
        $apn_ids = implode(',', $apn_ids);
        $this->notification->sendPushIOS($msg_apn_customer, $apn_ids);
      }
    }

    $array = array('workroom_order_id' => $order_id );      
    $this->session->set_userdata( $array );
    redirect('user-panel/courier-chat-room');

  }

  public function courier_work_room()
  {       
    $order_id = $this->input->post('order_id');
    if(!$order_id) { $order_id = $this->session->userdata('workroom_order_id'); }

    $order = $this->api->get_order_detail($order_id); 
    if($order) { 
      $deliverer_id = $order['deliverer_id'];
      $keys = ['deliverer_id' => $deliverer_id, 'cust_id' => $this->cust['cust_id'], 'order_id' => $order_id];
      $workroom = $this->api->get_workroom_chat($order_id);
      $this->load->view('user_panel/workroom_view', compact('order', 'workroom','keys'));
      $this->load->view('user_panel/footer');   
    } else { redirect('user-panel/courier-order-list'); }
  }

  public function workroom_reads()
  {
    $cust = $this->cust;
    $notification = $this->api->get_total_unread_workroom_notifications($cust['cust_id']);
    foreach ($notification as $n) {
      $this->api->make_workroom_notification_read($n['ow_id'], $cust['cust_id']);
    }
    echo 'success';
  }

  public function add_workroom_chat()
  {
    $cust = $this->cust;
    $order_id = $this->input->post('order_id', TRUE);
    $text_msg = $this->input->post('message_text', TRUE);

    $order = $this->api->get_order_detail($order_id);

    $cust_id = $order['cust_id'];
    $deliverer_id = $order['deliverer_id'];
    $sender_id = $cust['cust_id'];
    $cust_name = ($order['cust_name']!="NULL")?$order['cust_name']:"User";
    $deliverer_name = ($order['deliverer_name']!="NULL")?$order['deliverer_name']:"Deliverer";
    $today = date("Y-m-d H:i:s");
    // $user_type = $this->input->post('user_type', TRUE);

    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachment')) {   
        $uploads    = $this->upload->data();  
        $attachment_url =  $config['upload_path'].$uploads["file_name"];  
      } else { $attachment_url = "NULL"; }
    } else { $attachment_url = "NULL"; }

    $insert_data = array(
      "order_id" => $order_id,
      "cust_id" => $cust_id,
      "deliverer_id" => $deliverer_id,
      "text_msg" => $text_msg,
      "attachment_url" => $attachment_url,
      "cre_datetime" => $today,
      "sender_id" => $sender_id,
      "type" => "chat",
      "file_type" => "NULL",
      "cust_name" => $cust_name,
      "deliverer_name" => $deliverer_name,
      "thumbnail" => "NULL",
      "ratings" => "NULL",
    );

    if( $ow_id = $this->user->add_chat_to_workroom($insert_data)){
      if($sender_id == $cust_id) {  $device_details = $this->api->get_user_device_details($cust_id); } 
      else {  $device_details = $this->api->get_user_device_details($deliverer_id); }

      //Get Customer Device Reg ID's
      $arr_fcm_ids = $arr_apn_ids = array();
      foreach ($device_details as $value) {
        if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
        else {  array_push($arr_apn_ids, $value['reg_id']); }
      }

      $fcm_ids = $arr_fcm_ids;
      $apn_ids = $arr_apn_ids;

      //Get PN API Keys and PEM files
      $api_key_customer = $this->config->item('delivererAppGoogleKey');
      //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

      $msg =  array(
        'title' => $this->lang->line('new_message'), 
        'type' => 'chat', 
        'notice_date' => $today, 
        'desc' => $text_msg,
        'attachment' => $attachment_url,
        'cust_name' => $cust_name, 
        'deliverer_name' => $deliverer_name, 
        'order_id' => $order_id, 
        'cust_id' => $cust_id, 
        'deliverer_id' => $deliverer_id, 
        'file_type' => "NULL", 
        'thumbnail' => "NULL",
        "ratings" => "NULL",
      );
      $this->api->sendFCM($msg, $fcm_ids, $api_key_customer);
      //Push Notification APN
      $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
      if(is_array($apn_ids)) { 
        $apn_ids = implode(',', $apn_ids);
        $this->notification->sendPushIOS($msg_apn_customer, $apn_ids);
      }
    }

    $array = array('workroom_order_id' => $order_id );      
    $this->session->set_userdata( $array );
    redirect('user-panel/courier-work-room');

  }

  public function add_workroom_rating()
  {
    $cust = $this->cust;
    $order_id = $this->input->post('order_id', TRUE);
    $rating = $this->input->post('rating', TRUE); $rating = (int) $rating;
    $text_msg = $this->input->post('review_text', TRUE);
    $order = $this->api->get_order_detail($order_id);
    $cust_id = $order['cust_id'];
    $deliverer_id = $order['deliverer_id'];
    $sender_id = $cust['cust_id'];
    $cust_name = $order['cust_name'];
    $deliverer_name = $order['deliverer_name'];
    $today = date("Y-m-d H:i:s");

    $update_data = array(
      "review_comment" => $text_msg,
      "rating" => $rating,
      "review_datetime" => $today,
    );

    if( $this->api->update_review($update_data, $order_id) ) {
      $old_ratings = $this->api->get_user_review_details($deliverer_id);
      $no_of_ratings = $old_ratings['no_of_ratings']+1;
      $total_ratings = $old_ratings['total_ratings']+$rating;
      $ratings = round($total_ratings/$no_of_ratings,1);
      $deliverer_review = array(
        "ratings" => $ratings,
        "total_ratings" => $total_ratings,
        "no_of_ratings" => $no_of_ratings,
      );
      $this->api->update_deliverer_rating($deliverer_review, $deliverer_id);
      if($cust_courier_rating = $this->api->get_courier_rating($deliverer_id)) {
        $courier_no_of_ratings = $cust_courier_rating['no_of_ratings']+1;
        $courier_total_ratings = $cust_courier_rating['total_ratings']+$rating;
        $courier_ratings = round($courier_total_ratings/$courier_no_of_ratings,1);
        $key = 'update';
        $courier_deliverer_rating = array(
          "deliverer_id" => $deliverer_id,
          "update_date" => $today,
          "ratings" => $courier_ratings,
          "total_ratings" => $courier_total_ratings,
          "no_of_ratings" => $courier_no_of_ratings,
        );
        $this->api->manage_courier_ratings($courier_deliverer_rating, $key, $cust_courier_rating['rating_id']);
      } else {
        $key = 'insert';
        $courier_deliverer_rating = array(
          "deliverer_id" => $deliverer_id,
          "update_date" => $today,
          "ratings" => $ratings,
          "total_ratings" => $total_ratings,
          "no_of_ratings" => $no_of_ratings,
        );
        $this->api->manage_courier_ratings($courier_deliverer_rating, $key, 0);
      }

      $cust_details = $this->api->get_consumer_datails($cust_id);
      $review_data = array(
        "order_id" => $order_id,
        "cust_id" => $cust_id,
        "cust_name" => $cust_details['firstname']. ' ' .$cust_details['lastname'],
        "cust_avatar" => $cust_details['avatar_url'],
        "review_comment" => $text_msg,
        "review_rating" => $rating,
        "review_datetime" => date('Y-m-d H:i:s'),
      );
      $this->api->post_to_review($review_data);

      $insert_data = array(
        "order_id" => $order_id,
        "cust_id" => $cust_id,
        "deliverer_id" => $deliverer_id,
        "text_msg" => $text_msg,
        "attachment_url" => "NULL",
        "cre_datetime" => $today,
        "sender_id" => $sender_id,
        "type" => "review_rating",
        "file_type" => "NULL",
        "cust_name" => $cust_name,
        "deliverer_name" => $deliverer_name,
        "thumbnail" => "NULL",
        "ratings" => $rating,
      );

      if( $ow_id = $this->user->add_chat_to_workroom($insert_data)){
        if($sender_id == $cust_id) {  $device_details = $this->api->get_user_device_details($cust_id); } 
        else {  $device_details = $this->api->get_user_device_details($deliverer_id); }

        //Get Customer Device Reg ID's
        $arr_fcm_ids = $arr_apn_ids = array();
        foreach ($device_details as $value) {
          if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_apn_ids, $value['reg_id']); }
        }

        $fcm_ids = $arr_fcm_ids;
        $apn_ids = $arr_apn_ids;

        //Get PN API Keys and PEM files
        $api_key_customer = $this->config->item('delivererAppGoogleKey');
        //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

        $msg =  array(
          'title' => 'Gonagoo - New message', 
          'type' => 'review_rating', 
          'notice_date' => $today, 
          'desc' => $text_msg,
          'attachment' => "NULL",
          'cust_name' => $cust_name, 
          'deliverer_name' => $deliverer_name, 
          'order_id' => $order_id, 
          'cust_id' => $cust_id, 
          'deliverer_id' => $deliverer_id, 
          'file_type' => "NULL", 
          'thumbnail' => "NULL",
          "ratings" => $rating
        );
        $this->api->sendFCM($msg, $fcm_ids, $api_key_customer);
        //Push Notification APN
        $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
        if(is_array($apn_ids)) { 
          $apn_ids = implode(',', $apn_ids);
          $this->notification->sendPushIOS($msg_apn_customer, $apn_ids);
        }
      }
    }
    
    $array = array('workroom_order_id' => $order_id );      
    $this->session->set_userdata( $array );
    redirect('user-panel/courier-work-room');
  }

  public function search()
  {
    $this->load->view('user_panel/search_view');
    $this->load->view('user_panel/footer');   
  }
  
  
  public function update_profile_password() 
  {   
    $auth_email = $this->session->userdata('email');
    $old_pass = $this->input->post('auth_old_pass', TRUE);

    $auth = $this->user->user_authentication($auth_email,$old_pass);
    if($auth['auth_id'] > 0 ) {
      $new_pass = $this->input->post('auth_pass', TRUE);
      if( $this->user->update_profile_password(trim($new_pass))){
        $this->session->set_flashdata('success1',$this->lang->line('updated_successfully'));
      } else {  $this->session->set_flashdata('error1',$this->lang->line('update_failed')); }
    } else {  $this->session->set_flashdata('error1',$this->lang->line('old_password_not_matched'));  }
    
    redirect('user/profile');
  }

  public function get_languages_master()
  {
    $filter = $this->input->post('q');
    echo json_encode($this->user->get_language_master($filter));
  }

  public function update_languages()
  {
    $udate_data = array(
      "cust_id" => $this->session->userdata('cust_id'),
      "lang_known" => $this->input->post('langs'),
    );

    if( $this->api->update_user_language($udate_data) ){ echo 'success'; }
    else { echo 'failed'; }
  }

  public function update_overview()
  {
    $cust_id = $this->session->userdata('cust_id');
    $udate_data = [ "overview" => $this->input->post('overview')];

    if( $this->api->update_overview($cust_id, $udate_data) ){ echo 'success'; }
    else { echo 'failed'; }
  }

  public function edit_basic_profile_view()
  {
    $countries = $this->user->get_countries();
    $states = $this->user->get_state_by_country_id($this->cust['country_id']);
    if($this->cust['state_id'] > 0 ){ $cities = $this->user->get_city_by_state_id($this->cust['state_id']); } else{ $cities = array(); }
    $this->load->view('user_panel/edit_basic_profile_view', compact('countries','states','cities'));
    $this->load->view('user_panel/footer');
  }

  public function get_state_by_country_id()
  {
    $country_id = $this->input->post('country_id');
    echo json_encode($this->user->get_state_by_country_id($country_id));
  }

  public function get_city_by_state_id()
  {
    $state_id = $this->input->post('state_id');
    echo json_encode($this->user->get_city_by_state_id($state_id));
  }

  public function update_basic_profile()
  {
    $mobile_no = $this->input->post('mobile_no');
    $firstname = $this->input->post('firstname');
    $lastname = $this->input->post('lastname');
    $country_id = $this->input->post('country_id');
    $state_id = $this->input->post('state_id');
    $city_id = $this->input->post('city_id');
    $gender = $this->input->post('gender');
    $session_mobile_no = $this->session->userdata('mobile_no');

    $cust = $this->cust; 
    $old_avatar_url = $cust['avatar_url'];
    $old_cover_url = $cust['cover_url'];
    
    if($state_id == NULL) { $state_id = $cust['state_id']; }
    if($city_id == NULL) { $city_id = $cust['city_id']; }

    if( !empty($_FILES["avatar"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/profile-images/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('avatar')) {   
        $uploads    = $this->upload->data();  
        $avatar_url =  $config['upload_path'].$uploads["file_name"];  

        if( $old_avatar_url != "NULL") { unlink($old_avatar_url); }           
      } else { $avatar_url = $old_avatar_url; }
    } else { $avatar_url = $old_avatar_url; }


    if( !empty($_FILES["cover"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/cover-images/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('cover')) {   
        $uploads    = $this->upload->data();  
        $cover_url =  $config['upload_path'].$uploads["file_name"];  

        if( $old_cover_url != "NULL") { unlink($old_cover_url); }           
      } else { $cover_url = $old_cover_url; }
    } else { $cover_url = $old_cover_url; }


    if($mobile_no === $session_mobile_no ) {
      $update_data = array(
        "firstname" => $firstname,
        "lastname" => $lastname,
        "mobile1" => $mobile_no,
        "city_id" => $city_id,
        "state_id" => $state_id,
        "country_id" => $country_id,
        "gender" => $gender,
        "avatar_url" => $avatar_url,
        "cover_url" => $cover_url,
      );    
    }
    else {
      $OTP = mt_rand(100000,999999);
      $update_data = array(
        "firstname" => $firstname,
        "lastname" => $lastname,
        "mobile1" => $mobile_no,
        "city_id" => $city_id,
        "state_id" => $state_id,
        "country_id" => $country_id,
        "avatar_url" => $avatar_url,
        "cover_url" => $cover_url,
        "gender" => $gender,
        "cust_otp" => $OTP,
        "mobile_verified" => 0,
      );
    }

    if($this->user->update_basic_profile($update_data)){
      if($mobile_no == $session_mobile_no ) { 
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully')); 
        redirect('user-panel/basic-profile/'. $this->cust_id);
      }
      else{
        $this->session->set_userdata('mobile_no', $mobile_no);
        if(substr($mobile_no, 0, 1) == 0) { $mobile_no = ltrim($mobile_no, 0); }
        $this->api->sendSMS($this->api->get_country_code_by_id((int)$country_id).$mobile_no, $this->lang->line('lbl_verify_otp'). ': ' . $OTP);
        redirect('verification/otp');
      }
    } else{
      $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found'));  
      redirect('user-panel/basic-profile/'. $this->session->userdata('cust_id'));
    }
  }

  // EDUCATION CONTROLLER
  public function add_education_view()
  {
    $this->load->view('user_panel/add_education_view');   
    $this->load->view('user_panel/footer');
  }

  public function add_education()
  {
    $cust = $this->cust;

    $institute_name = $this->input->post("institute_name");
    $qualification = $this->input->post("qualification");
    $title = $this->input->post("title");
    $yr_qualification = $this->input->post("yr_qualification");
    $grade = $this->input->post("grade");
    $yr_attend = $this->input->post("yr_attend");
    $yr_attend_to = $this->input->post("yr_attend_to");
    $additional_info = $this->input->post("additional_info");
    $today = date('Y-m-d H:i:s');

    $add_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'institute_name' => $institute_name, 
      'qualification' => $qualification, 
      'description' => $additional_info,
      'start_date' => $yr_attend,
      'end_date' => $yr_attend_to,
      'grade' => $grade, 
      'qualify_year' => $yr_qualification, 
      'certificate_title' => $title, 
      'institute_address' => "NULl", 
      'activities' => "NULL",
      'specialization' => "NULL",
      'remark' => "NULL", 
      'cre_datetime' => $today, 
      'mod_datetime' => $today
    );

    if( $id = $this->api->add_user_education($add_data)) {
      $this->session->set_flashdata('success',$this->lang->line('added_successfully')); 
    } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }
    redirect('user-panel/education/add');
  }

  public function education_list_view()
  {   
    $educations = $this->user->get_customer_educations($this->cust['cust_id'],0);
    $this->load->view('user_panel/eduction_list_view', compact('educations'));    
    $this->load->view('user_panel/footer');
  }

  public function edit_education_view()
  {
    $edu_id = $this->uri->segment(3);
    if( $education = $this->user->get_customer_education_by_id($edu_id)){
      $this->load->view('user_panel/edit_education_view',compact('education'));   
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function edit_education()
  {   
    $edu_id = $this->input->post("education_id");
    $institute_name = $this->input->post("institute_name");
    $qualification = $this->input->post("qualification");
    $title = $this->input->post("title");
    $yr_qualification = $this->input->post("yr_qualification");
    $grade = $this->input->post("grade");
    $yr_attend = $this->input->post("yr_attend");
    $yr_attend_to = $this->input->post("yr_attend_to");
    $additional_info = $this->input->post("additional_info");
    $today = date('Y-m-d H:i:s');

    $edit_data = array(
      'edu_id' => (int) $edu_id,
      'institute_name' => $institute_name, 
      'qualification' => $qualification, 
      'description' => $additional_info,
      'start_date' => $yr_attend,
      'end_date' => $yr_attend_to,
      'grade' => $grade, 
      'qualify_year' => $yr_qualification, 
      'certificate_title' => $title, 
      'institute_address' => "NULl", 
      'activities' => "NULL",
      'specialization' => "NULL",
      'remark' => "NULL", 
      'mod_datetime' => $today
    );

    if( $id = $this->api->update_user_education($edit_data)) {
      $this->session->set_flashdata('success',$this->lang->line('updated_successfully')); 
    } else { $this->session->set_flashdata('error',$this->lang->line('update_failed'));  }
    redirect('user-panel/education/'.$edu_id);
  }

  public function delete_education()
  {
    $edu_id = $this->input->post("id");
    if( $id = $this->api->delete_user_education($edu_id)) { echo "success"; } else { echo "failed"; }
  }
  // END: EDUCATION CONTROLLER

  // EXPERIENCE CONTROLLER
  public function experience_list_view()
  {     
    $experiences = $this->user->get_customer_experiences($this->cust['cust_id'],false, 0);
    $this->load->view('user_panel/experience_list_view', compact('experiences'));   
    $this->load->view('user_panel/footer');
  }

  public function add_experience_view()
  {
    $this->load->view('user_panel/add_experience_view');    
    $this->load->view('user_panel/footer');
  }

  public function add_experience()
  {
    $cust = $this->cust;

    $exp_id = $this->input->post("experience_id");
    $org_name = $this->input->post("org_name");
    $location = $this->input->post("location");
    $title = $this->input->post("title");
    $job_desc = $this->input->post("description");
    $grade = $this->input->post("grade");
    $start_date = $this->input->post("start_date");
    $end_date = $this->input->post("end_date");
    $currently_working = $this->input->post("currently_working");
    $today = date('Y-m-d H:i:s');
    
    $add_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'org_name' => $org_name, 
      'job_title' => $title, 
      'job_location' => $location, 
      'job_desc' => $job_desc,
      'start_date' => date('Y-m-d',strtotime($start_date)),
      'end_date' => ($currently_working == "on" ) ? "NULL": date('Y-m-d',strtotime($end_date)),
      'currently_working' => ($currently_working == "on" ) ? 1: 0,
      'other' => 0, 
      'grade' => "NULL", 
      'institute_address' => "NULl", 
      'activities' => "NULL",
      'specialization' => "NULL",
      'remark' => "NULL", 
      'cre_datetime' => $today,
      'mod_datetime' => $today
    );
    //echo json_encode($add_data); die();
    if( $id = $this->api->add_user_experience($add_data)) {
      $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
    redirect('user-panel/experience/add');
  }

  public function edit_experience_view()
  {
    $exp_id = $this->uri->segment(3);
    if( $experience = $this->user->get_customer_experience_by_id($exp_id)){
      $this->load->view('user_panel/edit_experience_view',compact('experience'));   
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function edit_experience()
  {   
    $exp_id = $this->input->post("experience_id");
    $org_name = $this->input->post("org_name");
    $location = $this->input->post("location");
    $title = $this->input->post("title");
    $job_desc = $this->input->post("description");
    $grade = $this->input->post("grade");
    $start_date = $this->input->post("start_date");
    $end_date = $this->input->post("end_date");
    $currently_working = $this->input->post("currently_working");
    $today = date('Y-m-d H:i:s');

    $edit_data = array(
      'exp_id' => (int) $exp_id,
      'org_name' => $org_name, 
      'job_title' => $title, 
      'job_location' => $location, 
      'job_desc' => $job_desc,
      'start_date' => $start_date,
      'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
      'currently_working' => ($currently_working == "on" ) ? 1: 0,
      'grade' => "NULL", 
      'institute_address' => "NULl", 
      'activities' => "NULL",
      'specialization' => "NULL",
      'remark' => "NULL", 
      'mod_datetime' => $today
    );

    if( $id = $this->api->update_user_experience($edit_data)) {
      $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

    redirect('user-panel/experience/'.$exp_id);
  }

  public function delete_experience()
  {
    $exp_id = $this->input->post("id");
    if( $id = $this->api->delete_user_experience($exp_id)) { echo "success"; } else { echo "failed"; }
  }
  // END: EXPERIENCE CONTROLLER

  // OTHER EXPERIENCE CONTROLLER
  public function other_experience_list_view()
  {
    $experiences = $this->user->get_customer_experiences($this->cust['cust_id'],true, 0);
    if($experiences) {
      $this->load->view('user_panel/other_experience_list_view', compact('experiences'));   
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function add_other_experience_view()
  {
    $this->load->view('user_panel/add_other_experience_view');    
    $this->load->view('user_panel/footer');
  }

  public function add_other_experience()
  {
    $cust = $this->cust;

    $exp_id = $this->input->post("experience_id");
    $org_name = $this->input->post("org_name");
    $location = $this->input->post("location");
    $title = $this->input->post("title");
    $job_desc = $this->input->post("description");
    $grade = $this->input->post("grade");
    $start_date = $this->input->post("start_date");
    $end_date = $this->input->post("end_date");
    $currently_working = $this->input->post("currently_working");
    $today = date('Y-m-d H:i:s');

    $add_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'org_name' => $org_name, 
      'job_title' => $title, 
      'job_location' => $location, 
      'job_desc' => $job_desc,
      'start_date' => $start_date,
      'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
      'currently_working' => ($currently_working == "on" ) ? 1: 0,
      'other' => 1, 
      'grade' => "NULL", 
      'institute_address' => "NULl", 
      'activities' => "NULL",
      'specialization' => "NULL",
      'remark' => "NULL", 
      'cre_datetime' => $today,
      'mod_datetime' => $today
    );

    if( $id = $this->api->add_user_experience($add_data)) {
      $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

    redirect('user-panel/other-experience/add');
  }

  public function edit_other_experience_view()
  {
    $exp_id = $this->uri->segment(3);
    if( $experience = $this->user->get_customer_experience_by_id($exp_id)){
      $this->load->view('user_panel/edit_other_experience_view',compact('experience'));   
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function edit_other_experience()
  {   
    $exp_id = $this->input->post("experience_id");
    $org_name = $this->input->post("org_name");
    $location = $this->input->post("location");
    $title = $this->input->post("title");
    $job_desc = $this->input->post("description");
    $grade = $this->input->post("grade");
    $start_date = $this->input->post("start_date");
    $end_date = $this->input->post("end_date");
    $currently_working = $this->input->post("currently_working");
    $today = date('Y-m-d H:i:s');

    $edit_data = array(
      'exp_id' => (int) $exp_id,
      'org_name' => $org_name, 
      'job_title' => $title, 
      'job_location' => $location, 
      'job_desc' => $job_desc,
      'start_date' => $start_date,
      'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
      'currently_working' => ($currently_working == "on" ) ? 1: 0,
      'other' => 1, 
      'grade' => "NULL", 
      'institute_address' => "NULl", 
      'activities' => "NULL",
      'specialization' => "NULL",
      'remark' => "NULL", 
      'mod_datetime' => $today
    );

    if( $id = $this->api->update_user_experience($edit_data)) {
      $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
    redirect('user-panel/other-experience/'.$exp_id);
  }

  public function delete_other_experience()
  {
    $exp_id = $this->input->post("id");
    if( $id = $this->api->delete_user_experience($exp_id)) { echo "success"; } else { echo "failed"; }
  }
  // END: OTHER EXPERIENCE CONTROLLER

  // SKILL CONTROLLER
  public function get_skills_by_category_id()
  {
    $cat_id = $this->input->post("category_id");
    echo json_encode($this->user->get_skill_by_cat_id($cat_id));
  }

  public function edit_skill_view()
  { 
    $skills = $this->user->get_customer_skills($this->cust['cust_id']);

    if($skills) {
      $junior_skills = explode(',', $skills['junior_skills']);
      $confirmed_skills = explode(',', $skills['confirmed_skills']);
      $senior_skills = explode(',', $skills['senior_skills']);
      $expert_skills = explode(',', $skills['expert_skills']);
    } 
    $category = $this->api->get_category();
    
    if( $this->cust['cust_id'] > 0 ){
      $this->load->view('user_panel/edit_skill_view',compact('category','junior_skills', 'confirmed_skills', 'senior_skills','expert_skills'));   
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function edit_skill()
  { 
    $cust = $this->cust;

    $cat_id = $this->input->post("category_id");
    $skill_level = $this->input->post("skill_level");
    $skills = $this->input->post("skills");   
    $today = date('Y-m-d H:i:s');
    $cust_skills = $this->user->get_customer_skills($this->cust['cust_id']);

    if($skills != NULL AND !empty($skills)){ $skills = implode(',', $skills); } else{ $skills = "NULL"; }

    $edit_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'cs_id' => (int) $cust_skills['cs_id'],
      'cat_id' => $cat_id, 
      'skill_level' => $skill_level, 
      'junior_skills' => ($skill_level == "junior") ? $skills : $cust_skills['junior_skills'], 
      'confirmed_skills' => ($skill_level == "confirmed") ? $skills : $cust_skills['confirmed_skills'],
      'senior_skills' => ($skill_level == "senior") ? $skills : $cust_skills['senior_skills'],
      'expert_skills' => ($skill_level == "expert") ? $skills : $cust_skills['expert_skills'],
      'ext1' => "NULL", 
      'ext2' => "NULL", 
    );

    if( $this->api->update_user_skill($edit_data)) {
      $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

    redirect('user-panel/skill');
  } 
  // END: SKILL CONTROLLER

  // PORTFOLIO CONTROLLER
  public function portfolio_list_view()
  { 
    $portfolios = $this->user->get_customer_portfolio($this->cust['cust_id']);
    
    if($portfolios) {
      $this->load->view('user_panel/portfolio_list_view', compact('portfolios'));   
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function get_category_by_cat_type_id()
  { 
    $cat_type_id = $this->input->post("cat_type_id");
    echo json_encode($this->user->get_category_by_cat_type_id($cat_type_id));
  }

  public function get_subcategories_by_category_id($value='')
  { 
    $category_id = $this->input->post("category_id");
    echo json_encode($this->user->get_subcategory_by_category_id($category_id));
  }

  public function add_portfolio_view()
  { 
    $category_types = $this->user->get_category_types();
    $this->load->view('user_panel/add_portfolio_view', compact('category_types'));    
    $this->load->view('user_panel/footer');
  }

  public function add_portfolio()
  { 
    $cust = $this->cust;

    $title = $this->input->post("title");
    $category_id = $this->input->post("category_id");
    $subcat_ids = $this->input->post("subcat_ids");
    $description = $this->input->post("description");
    $today = date('Y-m-d H:i:s');
    
    if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = "NULL"; }

    if( !empty($_FILES["portfolio"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('portfolio')) {   
        $uploads    = $this->upload->data();  
        $portfolio_url =  $config['upload_path'].$uploads["file_name"];  

        $add_data = array(
          'cust_id' => (int) $cust['cust_id'],
          'title' => $title, 
          'cat_id' => $category_id, 
          'subcat_ids' => $subcat_ids,
          'attachement_url' => $portfolio_url,
          'description' => $description,
          'tags' => "NULL",
          'cre_datetime' => $today,
        );
        
        if( $id = $this->api->register_portfolio($add_data)) {
          $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
        } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }
      }  else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));   }
    } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }

    redirect('user-panel/portfolio/add');
  }

  public function edit_portfolio_view()
  { 
    $id = $this->uri->segment(3);
    if( $portfolio = $this->api->get_portfolio_detail($id)){
      $category_types = $this->user->get_category_types();
      $this->load->view('user_panel/edit_portfolio_view',compact('portfolio','category_types'));    
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function edit_portfolio()
  { 
    $portfolio_id = $this->input->post("portfolio_id");
    $title = $this->input->post("title");
    $category_id = $this->input->post("category_id");
    $subcat_ids = $this->input->post("subcat_ids");
    $description = $this->input->post("description");
    $today = date('Y-m-d H:i:s');
    
    $portfolio = $this->api->get_portfolio_detail($portfolio_id);

    if($category_id == NULL OR empty($category_id)) { 
      $category_id = $portfolio['cat_id']; 
      if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = "NULL"; }
    }else{ if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = $portfolio['subcat_ids']; } }

    $old_attachement_url = $portfolio["attachement_url"];

    if( !empty($_FILES["portfolio"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('portfolio')) {   
        $uploads    = $this->upload->data();  
        $portfolio_url =  $config['upload_path'].$uploads["file_name"];  
        
        if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
      } else { $portfolio_url = $old_attachement_url; }  
    } else { $portfolio_url = $old_attachement_url; }  
    
    $edit_data = array(
      'title' => $title, 
      'cat_id' => $category_id, 
      'subcat_ids' => $subcat_ids,
      'attachement_url' => $portfolio_url,
      'description' => nl2br(trim($description)),
    );
    
    if(  $this->api->update_portfolio($portfolio_id, $edit_data)) {
      $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

    redirect('user-panel/portfolio/'.$portfolio_id);
  }

  public function delete_portfolio()
  {
    $id = $this->input->post("id");
    $portfolio = $this->api->get_portfolio_detail($id);
    $old_attachement_url = $portfolio["attachement_url"];

    if( $id = $this->api->delete_portfolio($id)) { 
      if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        echo "success"; 
    } else { echo "failed"; }
  }
  // END: PORTFOLIO CONTROLLER

  // DOCUMENT CONTROLLER
  public function document_list_view()
  {     
    $documents = $this->api->get_documents($this->cust['cust_id']);
    
    if($documents) {
      $this->load->view('user_panel/document_list_view', compact('documents'));   
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }
  
  public function add_document_view()
  {
    $this->load->view('user_panel/add_document_view');    
    $this->load->view('user_panel/footer');
  }

  public function add_document()
  {
    $cust = $this->cust;

    $title = $this->input->post("title");
    $description = $this->input->post("description");
    $today = date('Y-m-d H:i:s');
    
    if( !empty($_FILES["document"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('document')) {   
        $uploads    = $this->upload->data();  
        $document_url =  $config['upload_path'].$uploads["file_name"];  

        $add_data = array(
          'cust_id' => (int) $cust['cust_id'],
          'doc_title' => $title, 
          'attachement_url' => $document_url,
          'doc_desc' => $description,
          'cre_datetime' => $today,
        );
        
        if( $id = $this->api->register_document($add_data)) {
          $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
        } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      }  else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));  }
    } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

    redirect('user-panel/document/add');
  }

  public function edit_document_view()
  {
    $id = $this->uri->segment(3);
    if( $document = $this->user->get_document_detail($id)){
      $this->load->view('user_panel/edit_document_view',compact('document'));   
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function edit_document()
  {   
    $document_id = $this->input->post("document_id");
    $title = $this->input->post("title");
    $description = $this->input->post("description");
    
    $document = $this->user->get_document_detail($document_id);
    $old_attachement_url = $document["attachement_url"];

    if( !empty($_FILES["document"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('document')) {   
        $uploads    = $this->upload->data();  
        $document_url =  $config['upload_path'].$uploads["file_name"];  
        
        if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
      } else { $document_url = $old_attachement_url; }  
    } else { $document_url = $old_attachement_url; }  
    
    $edit_data = array(
      'doc_title' => $title, 
      'attachement_url' => $document_url,
      'doc_desc' => trim(nl2br($description)),
    );
    
    if(  $this->api->update_document($document_id, $edit_data)) {
      $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

    redirect('user-panel/document/'.$document_id);
  }

  public function delete_document()
  {
    $id = $this->input->post("id");
    
    $document = $this->user->get_document_detail($id);
    $old_attachement_url = $document["attachement_url"];
    
    if( $this->api->delete_document($this->cust['cust_id'], $id)) { 
      if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
      echo "success"; 
    } else { echo "failed"; }
  }
  // END: DOCUMENT CONTROLLER

  // PAYMENT METHOD CONTROLLER
  public function payment_method_list_view()
  {     
    $payments = $this->api->get_payment_details($this->cust['cust_id']);
    
    if($payments) {
      $this->load->view('user_panel/payment_method_list_view', compact('payments'));    
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }
  
  public function add_payment_method_view()
  {
    $this->load->view('user_panel/add_payment_method_view');    
    $this->load->view('user_panel/footer');
  }

  public function add_payment_method()
  {
    $cust = $this->cust;

    $pay_method = $this->input->post("pay_method");
    $brand_name = $this->input->post("brand_name");
    $card_no = $this->input->post("card_no");
    $month = $this->input->post("month");
    $year = $this->input->post("year");
    $bank_name = $this->input->post("bank_name");
    $bank_address = $this->input->post("bank_address");
    $account_title = $this->input->post("account_title");
    $bank_short_code = $this->input->post("bank_short_code");
    $today = date('Y-m-d H:i:s');

    $add_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'payment_method' => trim($pay_method), 
      'brand_name' => trim($brand_name),
      'bank_name' => trim($bank_name),
      'account_title' => trim($account_title),
      'account_title' => trim($account_title),
      'bank_short_code' => trim($bank_short_code),
      'pay_status' => 1,
      'card_number' => trim($card_no),
      'expirty_date' => trim($month). '/'.trim($year),
      'cre_datetime' => $today,
    );
        
    if( $id = $this->api->register_payment_details($add_data)) {
      $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

    redirect('user-panel/payment-method/add');
  }

  public function edit_payment_method_view()
  {
    $id = $this->uri->segment(3);
    if( $pay = $this->user->get_payment_method_detail($id)){
      $this->load->view('user_panel/edit_payment_method_view',compact('pay'));    
      $this->load->view('user_panel/footer');
    } else { redirect('user-panel/user-profile'); }
  }

  public function edit_payment_method()
  {   
    $pay_id = $this->input->post("payment_id");
    $pay_method = $this->input->post("pay_method");
    $brand_name = $this->input->post("brand_name");
    $card_no = $this->input->post("card_no");
    $month = $this->input->post("month");
    $year = $this->input->post("year");
    $bank_name = $this->input->post("bank_name");
    $bank_address = $this->input->post("bank_address");
    $account_title = $this->input->post("account_title");
    $bank_short_code = $this->input->post("bank_short_code");

    $edit_data = array(
      'payment_method' => trim($pay_method), 
      'brand_name' => trim($brand_name),
      'bank_name' => trim($bank_name),
      'bank_address' => trim($bank_address),
      'account_title' => trim($account_title),
      'bank_short_code' => trim($bank_short_code),
      'card_number' => trim($card_no),
      'expirty_date' => trim($month). '/'.trim($year),
    );
        
    if( $id = $this->api->update_payment_details($pay_id, $edit_data)) {
      $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

    redirect('user-panel/payment-method/'. $pay_id);
  }

  public function delete_payment_method()
  {
    $id = $this->input->post("id");     
    if( $this->api->delete_payment_detail($this->cust['cust_id'], $id)) { echo "success"; } else { echo "failed"; }
  }
  // END: PAYMENT METHOD CONTROLLER

  // ADDRESS BOOK CONTROLLER
  public function address_book()
  {
    $address_details = $this->api->get_consumers_addressbook($this->cust_id,array());
    $this->load->view('user_panel/address_book_view', compact('address_details'));
    $this->load->view('user_panel/footer');
  }

  public function add_address_book_view()
  {
    $countries = $this->user->get_countries();
    $this->load->view('user_panel/add_address_book_view',compact('countries'));   
    $this->load->view('user_panel/footer');
  }
  
  public function add_address_to_book()
  {   
    //echo json_encode($_POST); die();
    $cust = $this->cust;
    $firstname = $this->input->post("firstname");
    $lastname = $this->input->post("lastname");
    $mobile_no = $this->input->post("mobile");
    $zipcode = $this->input->post("zipcode");
    $street = $this->input->post("street");
    $country = $this->input->post("country");
    $state = $this->input->post("state");
    $city = $this->input->post("city");
    $deliver_instruction = $this->input->post("deliver_instruction");
    $pickup_instruction = $this->input->post("pickup_instruction");
    $address1 = $this->input->post("address1");
    $address2 = $this->input->post("toMapID");
    $latitude = $this->input->post("latitude");
    $longitude = $this->input->post("longitude");
    $address_type = $this->input->post("address_type");
    $company = $this->input->post("company");
    $email_id = $this->input->post("email_id");
    $today = date('Y-m-d H:i:s');

    $country_id = $this->user->getCountryIdByName($country);
    $state_id = $this->user->getStateIdByName($state,$country_id);

    if($state_id == 0) {
      //add state and get state id
      $state_id = $this->api->register_state($country_id, $state);
      //add city under state id
      $city_id = $this->api->register_city($state_id, $city);
    } else {
      $city_id = $this->user->getCityIdByName($city,$state_id);
      if($city_id == 0) {
        //add city and get city id
        $city_id = $this->api->register_city($state_id, $city);
      }
    }

    if( !empty($_FILES["address_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/address-images/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('address_image')) {   
        $uploads    = $this->upload->data();  
        $image_url =  $config['upload_path'].$uploads["file_name"];  
      } else {  $image_url = "NULL"; }
    } else {  $image_url = "NULL"; }

    $add_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'firstname' => trim($firstname), 
      'lastname' => trim($lastname), 
      'mobile_no' => trim($mobile_no), 
      'street_name' => trim($street), 
      'addr_line1' => trim($address1), 
      'addr_line2' => (trim($address2) != "") ? trim($address2) : "NULL", 
      'city_id' => (int)($city_id), 
      'state_id' => (int)($state_id), 
      'country_id' => (int)($country_id), 
      'zipcode' => $zipcode,
      'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
      'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
      'image_url' => $image_url,
      'latitude' => trim($latitude), 
      'longitude' => trim($longitude), 
      'addr_type' => trim($address_type), 
      'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
      'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
      'cre_datetime' => $today,
    );
    //echo json_encode($add_data); die();
    if( $id = $this->api->register_new_address($add_data)) {
      $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
    redirect('user-panel/address-book/add');
  }
  
  public function add_address_to_book_by_panel()
  {   
    $cust = $this->cust;
    $firstname = $this->input->post("firstname");
    $lastname = $this->input->post("lastname");
    $mobile_no = $this->input->post("mobile");
    $zipcode = $this->input->post("zipcode");
    $street = $this->input->post("street");
    $country = $this->input->post("country");
    $country_id = $this->user->getCountryIdByName($country); //$this->input->post("country_id");
    $state = $this->input->post("state");
    $state_id = $this->user->getStateIdByName($state,$country_id);
    $city = $this->input->post("city");
    $city_id = $this->user->getCityIdByName($city,$state_id);
    $deliver_instruction = $this->input->post("deliver_instruction");
    $pickup_instruction = $this->input->post("pickup_instruction");
    $address1 = $this->input->post("address1");
    $address2 = $this->input->post("address2");
    $latitude = $this->input->post("latitude");
    $longitude = $this->input->post("longitude");
    $address_type = $this->input->post("address_type");
    $company = $this->input->post("company");
    $email_id = $this->input->post("email_id");
    $today = date('Y-m-d H:i:s');
    
    if( !empty($_FILES["address_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/address-images/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('address_image')) {   
        $uploads    = $this->upload->data();  
        $image_url =  $config['upload_path'].$uploads["file_name"];  
      } else {  $image_url = "NULL"; }
    } else {  $image_url = "NULL"; }

    $add_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'firstname' => trim($firstname), 
      'lastname' => trim($lastname), 
      'mobile_no' => trim($mobile_no), 
      'street_name' => trim($street), 
      'addr_line1' => trim($address1), 
      'addr_line2' => (trim($address2) != "") ? trim($address2) : "NULL", 
      'city_id' => (int)($city_id), 
      'state_id' => (int)($state_id), 
      'country_id' => (int)($country_id), 
      'zipcode' => $zipcode,
      'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
      'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
      'image_url' => $image_url,
      'latitude' => trim($latitude), 
      'longitude' => trim($longitude), 
      'addr_type' => trim($address_type), 
      'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
      'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
      'cre_datetime' => $today,
    );
    //echo json_encode($add_data); die();
    if( $id = $this->api->register_new_address($add_data)) {
      $company = (trim($company) !="") ? trim($company): "NULL";
      if($company == 'NULL') {
        echo $id .'~'. trim($firstname) . ' ' . trim($lastname) . ' ( ' . trim($street) . ' )';  
      } else {
        echo $id .'~'. $company . ' | ' . trim($firstname) . ' ' . trim($lastname) . ' ( ' . trim($street) . ' )';  
      }
    } else { echo false; }
  }
  
  public function add_address_to_book_from_address()
  {   
    //echo json_encode($_POST); die();
    $cust = $this->cust;
    $firstname = $this->input->post("frm_firstname");
    $lastname = $this->input->post("frm_lastname");
    $mobile_no = $this->input->post("frm_mobile");
    $zipcode = $this->input->post("frm_zipcode");
    $street = $this->input->post("frm_street");
    $country = $this->input->post("frm_country");
    $country_id = $this->user->getCountryIdByName($country); //$this->input->post("country_id");
    //echo json_encode($country); die();
    $state = $this->input->post("frm_state");
    $state_id = $this->user->getStateIdByName($state,$country_id);
    $city = $this->input->post("frm_city");
    $city_id = $this->user->getCityIdByName($city,$state_id);
    //echo json_encode($country.$state.$city); die();
    $deliver_instruction = $this->input->post("frm_deliver_instruction");
    $pickup_instruction = $this->input->post("frm_pickup_instruction");
    $address1 = $this->input->post("frm_address1");
    $address2 = $this->input->post("frm_street");
    $latitude = $this->input->post("frm_latitude");
    $longitude = $this->input->post("frm_longitude");
    $address_type = $this->input->post("frm_address_type");
    $company = $this->input->post("frm_company");
    $email_id = $this->input->post("frm_email_id");
    $today = date('Y-m-d H:i:s');
    
    if( !empty($_FILES["frm_address_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/address-images/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('frm_address_image')) {   
        $uploads    = $this->upload->data();  
        $image_url =  $config['upload_path'].$uploads["file_name"];  
      } else {  $image_url = "NULL"; }
    } else {  $image_url = "NULL"; }

    $add_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'firstname' => trim($firstname), 
      'lastname' => trim($lastname), 
      'mobile_no' => trim($mobile_no), 
      'street_name' => trim($street), 
      'addr_line1' => trim($address1), 
      'addr_line2' => (trim($address2) != "") ? trim($address2) : "NULL", 
      'city_id' => (int)($city_id), 
      'state_id' => (int)($state_id), 
      'country_id' => (int)($country_id), 
      'zipcode' => $zipcode,
      'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
      'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
      'image_url' => $image_url,
      'latitude' => trim($latitude), 
      'longitude' => trim($longitude), 
      'addr_type' => trim($address_type), 
      'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
      'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
      'cre_datetime' => $today,
    );
    //echo json_encode($add_data); die();
    if( $id = $this->api->register_new_address($add_data)) {
      $company = (trim($company) !="") ? trim($company): "NULL";
      if($company == 'NULL') {
        echo $id .'~'. trim($firstname) . ' ' . trim($lastname) . ' ( ' . trim($street) . ' )' .'~'. trim($latitude) . ',' . trim($longitude);  
      } else {
        echo $id .'~'. $company . ' | ' . trim($firstname) . ' ' . trim($lastname) . ' ( ' . trim($street) . ' )' .'~'. trim($latitude) . ',' . trim($longitude);  
      }
    } else { echo false; }
  }
  
  public function add_address_to_book_to_address()
  {   
    //echo json_encode($_POST); die();
    $cust = $this->cust;
    $firstname = $this->input->post("t_firstname");
    $lastname = $this->input->post("t_lastname");
    $mobile_no = $this->input->post("t_mobile");
    $zipcode = $this->input->post("t_zipcode");
    $street = $this->input->post("t_street");
    $country = $this->input->post("t_country");
    $country_id = $this->user->getCountryIdByName($country); //$this->input->post("country_id");
    //echo json_encode($country); die();
    $state = $this->input->post("t_state");
    $state_id = $this->user->getStateIdByName($state,$country_id);
    $city = $this->input->post("t_city");
    $city_id = $this->user->getCityIdByName($city,$state_id);
    //echo json_encode($country.$state.$city); die();
    $deliver_instruction = $this->input->post("t_deliver_instruction");
    $pickup_instruction = $this->input->post("t_pickup_instruction");
    $address1 = $this->input->post("t_address1");
    $address2 = $this->input->post("t_street");
    $latitude = $this->input->post("t_latitude");
    $longitude = $this->input->post("t_longitude");
    $address_type = $this->input->post("t_address_type");
    $company = $this->input->post("t_company");
    $email_id = $this->input->post("t_email_id");
    $today = date('Y-m-d H:i:s');
    
    if( !empty($_FILES["t_address_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/address-images/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('t_address_image')) {   
        $uploads    = $this->upload->data();  
        $image_url =  $config['upload_path'].$uploads["file_name"];  
      } else {  $image_url = "NULL"; }
    } else {  $image_url = "NULL"; }

    $add_data = array(
      'cust_id' => (int) $cust['cust_id'],
      'firstname' => trim($firstname), 
      'lastname' => trim($lastname), 
      'mobile_no' => trim($mobile_no), 
      'street_name' => trim($street), 
      'addr_line1' => trim($address1), 
      'addr_line2' => (trim($address2) != "") ? trim($address2) : "NULL", 
      'city_id' => (int)($city_id), 
      'state_id' => (int)($state_id), 
      'country_id' => (int)($country_id), 
      'zipcode' => $zipcode,
      'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
      'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
      'image_url' => $image_url,
      'latitude' => trim($latitude), 
      'longitude' => trim($longitude), 
      'addr_type' => trim($address_type), 
      'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
      'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
      'cre_datetime' => $today,
    );
    //echo json_encode($add_data); die();
    if( $id = $this->api->register_new_address($add_data)) {
      $company = (trim($company) !="") ? trim($company): "NULL";
      if($company == 'NULL') {
        echo $id .'~'. trim($firstname) . ' ' . trim($lastname) . ' ( ' . trim($street) . ' )' .'~'. trim($latitude) . ',' . trim($longitude);  
      } else {
        echo $id .'~'. $company . ' | ' . trim($firstname) . ' ' . trim($lastname) . ' ( ' . trim($street) . ' )' .'~'. trim($latitude) . ',' . trim($longitude);  
      }
    } else { echo false; }
  }

  public function edit_address_book_view()
  {
    $id = $this->uri->segment(3);
    $address = $this->user->get_address_from_book($id);     
    $countries = $this->user->get_countries();
    $this->load->view('user_panel/edit_address_book_view',compact('countries','address'));    
    $this->load->view('user_panel/footer');
  }
  
  public function edit_address()
  {
    $cust = $this->cust;

    $id = $this->input->post("address_id");
    $firstname = $this->input->post("firstname");
    $lastname = $this->input->post("lastname");
    $mobile_no = $this->input->post("mobile");
    $zipcode = $this->input->post("zipcode");
    $street = $this->input->post("street");
    $country = $this->input->post("country");
    $state = $this->input->post("state");
    $city = $this->input->post("city");
    $deliver_instruction = $this->input->post("deliver_instruction");
    $pickup_instruction = $this->input->post("pickup_instruction");
    $address1 = $this->input->post("address1");
    $address2 = $this->input->post("toMapID");
    $latitude = $this->input->post("latitude");
    $longitude = $this->input->post("longitude");
    $address_type = $this->input->post("address_type");
    $company = $this->input->post("company");
    $email_id = $this->input->post("email_id");

    $country_id = $this->user->getCountryIdByName($country);
    $state_id = $this->user->getStateIdByName($state,$country_id);

    if($state_id == 0) {
      //add state and get state id
      $state_id = $this->api->register_state($country_id, $state);
      //add city under state id
      $city_id = $this->api->register_city($state_id, $city);
    } else {
      $city_id = $this->user->getCityIdByName($city,$state_id);
      if($city_id == 0) {
        //add city and get city id
        $city_id = $this->api->register_city($state_id, $city);
      }
    }
    $address = $this->user->get_address_from_book($id);
    $today = date('Y-m-d H:i:s');
    
    if( !empty($_FILES["address_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/address-images/';                 
      $config['allowed_types']  = 'gif|jpg|png';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('address_image')) {   
        $uploads    = $this->upload->data();  
        $image_url =  $config['upload_path'].$uploads["file_name"];  
        if($address['image_url'] != "NULL" AND !empty($address['image_url'])) { unlink($address['image_url']); }
      } else {  $image_url = $address['image_url']; }
    } else {  $image_url = $address['image_url']; }

    $edit_data = array(
      'firstname' => trim($firstname), 
      'lastname' => trim($lastname), 
      'mobile_no' => trim($mobile_no), 
      'street_name' => trim($street), 
      'addr_line1' => trim($address1), 
      'addr_line2' => trim($address2), 
      'city_id' => (int)($city_id), 
      'state_id' => (int)($state_id), 
      'country_id' => (int)($country_id), 
      'zipcode' => $zipcode,
      'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
      'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
      'image_url' => $image_url,
      'latitude' => trim($latitude), 
      'longitude' => trim($longitude), 
      'addr_type' => trim($address_type), 
      'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
      'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
    );

    if( $this->api->update_existing_address($edit_data, $id)) {
      $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

    redirect('user-panel/address-book/'.$id);
  }

  public function delete_address()
  {
    $id = $this->input->post('id');
    $address = $this->user->get_address_from_book($id);
    if( $this->api->delete_address_from_addressbook($id)) { 
      if($address['image_url']!="NULL" && !empty($address['image_url'])) { unlink($address['image_url']); }
      echo "success"; 
    } else { echo "failed"; }
  }
  
  public function change_password() {
    $this->load->view('user_panel/change_password_view');   
    $this->load->view('user_panel/footer');
  }

  public function update_password()
  {   
    $old_pass = $this->input->post('old_password');
    $new_pass = $this->input->post('new_password');
    if($this->api->validate_old_password($this->cust_id, $old_pass)){
      if($this->api->update_new_password(['cust_id' => $this->cust_id, 'password' => $new_pass])){
        $this->session->set_flashdata('success', $this->lang->line('password_changed'));          
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
    } else { $this->session->set_flashdata('error', $this->lang->line('old_password_not_matched'));  }    
    redirect('user-panel/change-password');
  }

  public function my_wallet()
  {
    $amount = $this->user->get_customer_account_withdraw_request_sum($this->cust_id, 'request');
    $balance = $this->user->check_user_account_balance($this->cust_id);
    $this->load->view('user_panel/account_history_list_view', compact('balance','amount'));
    $this->load->view('user_panel/footer');
  }

  public function user_account_history()
  {
    $history = $this->user->get_customer_account_history($this->cust_id);
    $this->load->view('user_panel/transaction_history_list_view', compact('history'));
    $this->load->view('user_panel/footer');
  }

  public function my_scrow()
  {
    $balance = $this->api->deliverer_scrow_master_list($this->cust_id);
    $this->load->view('user_panel/scrow_history_list_view', compact('balance'));
    $this->load->view('user_panel/footer');
  }

  public function user_scrow_history()
  {
    $history = $this->user->get_customer_scrow_history($this->cust_id);
    $this->load->view('user_panel/scrow_account_history_list_view', compact('history'));
    $this->load->view('user_panel/footer');
  }

  public function my_withdrawals_request()
  {
    $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'request');
    $this->load->view('user_panel/withdrawal_request_list_view', compact('history'));
    $this->load->view('user_panel/footer');
  }

  public function my_withdrawals_accepted()
  {
    $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'accept');
    $this->load->view('user_panel/withdrawal_accepted_request_list_view', compact('history'));
    $this->load->view('user_panel/footer');
  }

  public function my_withdrawals_rejected()
  {
    $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'reject');
    $this->load->view('user_panel/withdrawal_rejected_request_list_view', compact('history'));
    $this->load->view('user_panel/footer');
  }

  public function withdrawal_request_details()
  {
    $req_id = $this->input->post('req_id'); 
    $details = $this->user->withdrawal_request_details((int)$req_id);
    $this->load->view('user_panel/withdrawal_request_detail_view', compact('details'));
    $this->load->view('user_panel/footer');
  }

  public function create_withdraw_request()
  {
    $account_id = $this->input->post('account_id', TRUE);
    $currency_code = $this->input->post('currency_code', TRUE);
    $withdraw_amount = $this->input->post('amount', TRUE);
    $amount = $this->user->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
    //$accounts = $this->user->get_deliverer_accounts($this->cust_id);
    $accounts = $this->api->get_payment_details($this->cust_id);
    if(!empty($accounts)){
      $balance = $this->user->check_user_account_balance_by_id((int)$account_id);
      $final_balance = $balance['account_balance'] - $amount['amount'];
      $currency_code = $balance['currency_code'];
      $this->load->view('user_panel/withdraw_request_create_view',compact('final_balance','accounts','amount','currency_code'));
      $this->load->view('user_panel/footer');
    } else {
      redirect(base_url('user-panel/payment-method/add'));
    }
  }

  public function withdraw_request_sent()
  {
    //echo json_encode($_POST); die();
    $cust_id = (int)$this->cust_id;
    $password = $this->input->post('password', TRUE);
    $user_details = $this->api->get_user_details($this->cust_id);
      if($user_details['password'] == trim($password)) {
      $amount = $this->input->post('amount', TRUE);
      $currency_code= $this->input->post('currency_code', TRUE);
      $transfer_account_type = $this->input->post('transfer_account_type', TRUE);
      $balance = $this->api->check_user_account_balance($this->cust_id, $currency_code);
      $requested_amount = $this->user->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
      $withdrawable_amount = $balance - $requested_amount['amount'];

      if($amount <= $withdrawable_amount) {
        $accounts = $this->user->get_deliverer_accounts_details($transfer_account_type);
        
        if($accounts['payment_method']=='Bank Account') { $account_type = 'bank'; }
        if($accounts['payment_method']=='Mobile Wallet' && $accounts['brand_name']=='MTN') { $account_type = 'mtn'; }
        if($accounts['payment_method']=='Mobile Wallet' && $accounts['brand_name']=='Orange') { $account_type = 'orange'; }
        if($accounts['payment_method']=='Credit Card' && $accounts['brand_name']=='Stripe') { $account_type = 'stripe'; }
        if($accounts['payment_method']=='Debit Card' && $accounts['brand_name']=='Stripe') { $account_type = 'stripe'; }
        
        $transfer_account_number = $accounts["card_number"];
        $today = date('Y-m-d h:i:s');
        $data = array(
          "user_id" => (int)$cust_id,
          "amount" => trim($amount),
          "req_datetime" => $today,
          "user_type" => 0,
          "response" => 'request',
          "response_datetime" => 'NULL',
          "reject_remark" => 'NULL',
          "transfer_account_type" => $account_type,
          "transfer_account_number" => trim($transfer_account_number),
          "currency_code" => trim($currency_code),
        );
        //echo json_encode($data); die();
        if( $sa_id = $this->api->user_account_withdraw_request($data) ) {
          $this->session->set_flashdata('success', $this->lang->line('withdraw_request_sent'));
        } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_send_request')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('entered_wrong_amount')); }
    } else { $this->session->set_flashdata('error', $this->lang->line('unauthorized_access')); }
    redirect('user-panel/my-wallet');
  }

  public function customer_order_pickup()
  {
    $cd_id = $this->input->post('cd_id', TRUE);
    $order_id = $this->input->post('order_id', TRUE);
    $order_status = $this->input->post('order_status', TRUE);
    $cust_id = $this->input->post('cust_id', TRUE);
    $deliverer_id = $this->input->post('deliverer_id', TRUE);
    $driver_code = $this->input->post('driver_code', TRUE);
    $order_details = $this->api->get_order_detail($order_id);
    if($order_details['driver_code'] == trim($driver_code)) {
      $customer_fcm_ids = array();
      $customer_apn_ids = array();
      if ( trim($order_status) == 'in_progress' ){
        //var_dump($order_status); die();
        if( $this->user->update_order_status($cd_id, $order_id, $order_status) ){
          $this->api->insert_order_status($cd_id, $order_id, $order_status);
          //Get customer and deliverer contact and personal details
          
          $customer_details = $this->api->get_user_details($order_details['cust_id']);
          $deliverer_details = $this->api->get_user_details($deliverer_id);

          //Get Users device Details
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          if(!empty($device_details_customer)) {
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {
                array_push($arr_customer_fcm_ids, $value['reg_id']);
              } else {
                array_push($arr_customer_apn_ids, $value['reg_id']);
              }
            }
            //$customer_fcm_ids = $arr_customer_fcm_ids;
            $customer_apn_ids = $arr_customer_apn_ids;
          }
          
          //Get Deliverer Device ID's
          $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
          $arr_deleverer_fcm_ids = array();
          $arr_deleverer_apn_ids = array();
          if(!empty($device_details_deleverer)) {
            //var_dump($device_details_deleverer); die();
            foreach ($device_details_deleverer as $value) {
              if($value['device_type'] == 1) {
                array_push($arr_deleverer_fcm_ids, $value['reg_id']);
              } else {
                array_push($arr_deleverer_apn_ids, $value['reg_id']);
              }
            }
            //$deleverer_fcm_ids = $arr_deleverer_fcm_ids;
            $deleverer_apn_ids = $arr_deleverer_apn_ids;
          }
          
          //Get PN API Keys and PEM files
          $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
          $api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
          
          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('driver_inprogress_order_msg');

          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($customer_details['country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($deliverer_details['country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg);
          
          if($customer_details['firstname']!='NULL'){
            $cust_full_name = trim($customer_details['firstname'])." ".trim($customer_details['lastname']);
          } else {
            $email_cut = explode("@", $customer_details['email1']);
            $cust_full_name = strtoupper($email_cut[0]);
          }
          if($deliverer_details['firstname']!='NULL'){
            $deliverer_full_name = trim($deliverer_details['firstname'])." ".trim($deliverer_details['lastname']);
          } else {
            $email_cut = explode("@", $deliverer_details['email1']);
            $deliverer_full_name = strtoupper($email_cut[0]);
          }

          //Email confirmation deliverer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($deliverer_full_name) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_inprogress_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_full_name);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          //Email confirmation Customer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($cust_full_name) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_inprogress_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($cust_full_name);
          $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject, trim($message));
          //Push Notification FCM
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          if(!empty($arr_customer_fcm_ids)) { $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key_deliverer); }
          //Push Notification APN
          $msg_apn_customer =  array('title' => $subject, 'text' => $sms_msg);
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          
          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($cust_full_name), trim($deliverer_full_name), 'NULL', 'NULL', 'NULL');
          //check order of dedicated users
          if(trim($order_details['is_bank_payment']) == 0) {
            //Payment Module-------------------------------------------------------------------------------------
              $today = date('Y-m-d h:i:s');
              if(trim($order_details['pickup_payment']) > 0) {
                //deduct pickup_payment details in gonagoo account master and history
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($order_details['pickup_payment']);
                $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$deliverer_id,
                  "type" => 0,
                  "transaction_type" => 'pickup_payment',
                  "amount" => trim($order_details['pickup_payment']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => 'NULL',
                  "transfer_account_number" => 'NULL',
                  "bank_name" => 'NULL',
                  "account_holder_name" => 'NULL',
                  "iban" => 'NULL',
                  "email_address" => 'NULL',
                  "mobile_number" => 'NULL',
                  "transaction_id" => 'NULL',
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

                //Add pickup_payment details in deliverer account master and history
                $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($order_details['pickup_payment']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $deliverer_id, $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));

                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$deliverer_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'pickup_payment',
                  "amount" => trim($order_details['pickup_payment']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              
                //Deduct pickup amount from scrow and history
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));
                $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) - trim($order_details['pickup_payment']);
                $this->api->update_payment_in_deliverer_scrow((int)$deliverer_scrow_master_details['scrow_id'], $deliverer_id, $scrow_balance);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));

                $update_data_scrow_history = array(
                  "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                  "order_id" => (int)$order_id,
                  "deliverer_id" => (int)$deliverer_id,
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'pickup_payment',
                  "amount" => trim($order_details['pickup_payment']),
                  "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_scrow_history($update_data_scrow_history);
              }
              //post payment in workroom
              $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('pickup_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
            //Payment Module End --------------------------------------------------------------------------------
          }
          $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
        } else {  $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      } else {  $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
    } else {  $this->session->set_flashdata('error', $this->lang->line('driver_code_invalid')); }
    redirect('user-panel/in-progress-bookings');
  }
  
  public function set_language()
  {
    $lang = $this->input->post('lang');
    $this->session->set_userdata('language',$lang);
    echo 'success';
  }

  public function user_invoices_history()
  {
    $details = $this->user->get_user_order_invoices($this->cust_id);
    $this->load->view('user_panel/invoice_history_list_view', compact('details'));
    $this->load->view('user_panel/footer');
  }

  public function get_relay_by_location()
  {
    $country_id = $this->input->post('country_id'); $country_id = (int)$country_id;
    $state_id = $this->input->post('state_id'); $state_id = (int)$state_id;
    $city_id = $this->input->post('city_id'); $city_id = (int)$city_id;
    echo json_encode($this->user->get_relay_by_location($country_id, $state_id, $city_id));
  }

  public function get_relay_by_lat_long()
  { 
    $frm_relay_latitude = $this->input->post('frm_relay_latitude'); $frm_relay_latitude = trim($frm_relay_latitude);
    $frm_relay_longitude = $this->input->post('frm_relay_longitude'); $frm_relay_longitude = trim($frm_relay_longitude);
    echo json_encode($this->user->get_relay_point_by_lat_long($frm_relay_latitude, $frm_relay_longitude));
  }

  public function get_support()
  {
    $this->load->view('user_panel/get_support_view');
    $this->load->view('user_panel/footer');
  }

  public function add_support_details()
  {
    $type = $this->input->post('type', TRUE);
    $description = $this->input->post('description', TRUE);
    $cust_id = $this->cust_id;
    $today = date('Y-m-d h:i:s');

    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachment')) {   
        $uploads    = $this->upload->data();  
        $file_url =  $config['upload_path'].$uploads["file_name"];  
      } else {  $file_url = "NULL"; }
    } else {  $file_url = "NULL"; }

    $add_data = array(
      'type' => trim($type), 
      'description' => trim($description), 
      'file_url' => trim($file_url), 
      'cust_id' => (int) $cust_id, 
      'cre_datetime' => $today, 
      'response' => 'NULL', 
      'res_datetime' => 'NULL', 
      'status' => 'open', 
      'updated_by' => 0, 
    );
        //echo json_encode($add_data); die();
    if( $id = $this->user->register_new_support($add_data)) {
      $ticket_id = 'G-'.time().$id;
      $this->user->update_support_ticket($id, $ticket_id);
      $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
    } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
    redirect('user-panel/get-support');
  }

  public function get_support_list()
  {
    $support_list = $this->user->get_support_list($this->cust_id);
    $this->load->view('user_panel/support_list_view', compact('support_list'));
    $this->load->view('user_panel/footer');
  }

  public function view_support_reply_details()
  {
    $id = $this->uri->segment(3);
    $details = $this->user->get_support_details($id);
    $this->load->view('user_panel/view_support_reply_details', compact('details'));   
    $this->load->view('user_panel/footer');
  }

  // Standard Rates----------------------------
    public function standard_rates_list()
    {
      $cust_id = (int)$this->cust_id;
      $rates = $this->api->get_standard_rates(0, $cust_id);
      //echo json_encode($rates); die();
      $this->load->view('user_panel/standard_rates_list_view', compact('rates'));      
      $this->load->view('user_panel/footer');   
    }
    public function add_volume_based()
    {
      $countries_list = $this->api->get_countries();
      $categories = $this->dimension->get_categories();
      $dimensions_list = $this->dimension->get_standard_dimension(); 
      $this->load->view('user_panel/standard_rates_add_volume_based_view', compact('countries_list','dimensions_list','categories'));
      $this->load->view('user_panel/footer'); 
    }
    public function get_country_currencies()
    {
      $country_id = $this->input->post('country_id', TRUE); $country_id = (int) $country_id;
      $currencies = $this->api->get_country_currencies($country_id);
      echo json_encode($currencies);
    }
    public function register_volume_based()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $categories = $this->input->post('category', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);
      $min_distance = $this->input->post('min_distance', TRUE);
      $max_distance = $this->input->post('max_distance', TRUE);
      $max_duration = $this->input->post('max_duration', TRUE);
      $min_dimension = $this->input->post('min_dimension', TRUE);
      $max_dimension = $this->input->post('max_dimension', TRUE);
      $min_width = $this->input->post('min_width', TRUE);
      $min_height = $this->input->post('min_height', TRUE);
      $min_length = $this->input->post('min_length', TRUE);
      $max_width = $this->input->post('max_width', TRUE);
      $max_height = $this->input->post('max_height', TRUE);
      $max_length = $this->input->post('max_length', TRUE);

      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);

      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $currency = $this->api->get_currency_detail((int) $currency_id);
      $insert_flag = false;

      $post_data = array(
        "category_id" =>  $categories,
        "country_id" =>  (int)($country_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),       
        "min_distance" =>  trim($min_distance), 
        "max_distance" =>  trim($max_distance), 
        "min_dimension_id" => (trim($min_dimension) == "other" ) ? 0 : $min_dimension , 
        "min_width" =>  trim($min_width), 
        "min_height" =>  trim($min_height), 
        "min_length" =>  trim($min_length), 
        "min_volume" =>  ( $min_width * $min_height * $min_length ), 
        "max_dimension_id" => (trim($max_dimension) == "other" ) ? 0 : $max_dimension ,
        "max_width" =>  trim($max_width), 
        "max_height" =>  trim($max_height), 
        "max_length" =>  trim($max_length), 
        "max_volume" =>  ( $max_width * $max_height * $max_length ), 
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
      );

      foreach ($categories as $c) {
        $insert_data = array(
          "category_id" =>  (int)($c), 
          "country_id" =>  (int)($country_id), 
          "currency_id" =>  (int)($currency_id), 
          "currency_sign" =>  trim($currency['currency_sign']), 
          "currency_title" =>  trim($currency['currency_title']),       
          "min_distance" =>  trim($min_distance), 
          "max_distance" =>  trim($max_distance), 
          "min_dimension_id" => (trim($min_dimension) == "other" ) ? 0 : $min_dimension , 
          "min_width" =>  trim($min_width), 
          "min_height" =>  trim($min_height), 
          "min_length" =>  trim($min_length), 
          "min_volume" =>  ( $min_width * $min_height * $min_length ), 
          "max_dimension_id" => (trim($max_dimension) == "other" ) ? 0 : $max_dimension ,
          "max_width" =>  trim($max_width), 
          "max_height" =>  trim($max_height), 
          "max_length" =>  trim($max_length), 
          "max_volume" =>  ( $max_width * $max_height * $max_length ), 
          "min_weight" =>  0, 
          "max_weight" =>  0, 
          "unit_id" =>  0, 
          
          "earth_local_rate" =>  trim($earth_local_rate), 
          "earth_national_rate" =>  trim($earth_national_rate), 
          "earth_international_rate" =>  trim($earth_international_rate), 
          "air_local_rate" =>  trim($air_local_rate), 
          "air_national_rate" =>  trim($air_national_rate), 
          "air_international_rate" =>  trim($air_international_rate), 
          "sea_local_rate" =>  trim($sea_local_rate), 
          "sea_national_rate" =>  trim($sea_national_rate), 
          "sea_international_rate" =>  trim($sea_international_rate),

          "earth_local_duration" =>  trim($earth_local_duration), 
          "earth_national_duration" =>  trim($earth_national_duration), 
          "earth_international_duration" =>  trim($earth_international_duration), 
          "air_local_duration" =>  trim($air_local_duration), 
          "air_national_duration" =>  trim($air_national_duration), 
          "air_international_duration" =>  trim($air_international_duration), 
          "sea_local_duration" =>  trim($sea_local_duration), 
          "sea_national_duration" =>  trim($sea_national_duration), 
          "sea_international_duration" =>  trim($sea_international_duration),
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "cust_id" =>  trim($cust_id),
        );
        
        if(!$this->api->check_min_volume_based_standard_rates($insert_data)  && !$this->api->check_max_volume_based_standard_rates($insert_data)) {      
          if($this->api->register_standard_rates($insert_data)){ $insert_flag = true; }
          else { $insert_flag = false;   }                 
        } 
      }
      
      if($insert_flag){ $this->session->set_flashdata('success', $this->lang->line('Rate successfully added!')); }
      else{ $this->session->set_flashdata('error',array('error_msg' => $this->lang->line('Unable to add rates! Try again...'), 'data' => $post_data));  }

      redirect('user-panel/add-volume-based');
    }
    public function edit_standard_rates()
    {
      $cust_id = (int)$this->cust_id;
      if( !is_null($this->input->post('rate_id')) ) { $rate_id = (int) $this->input->post('rate_id'); $rate_id = (int)$rate_id; }
      else if(!is_null($this->uri->segment(3))) { $rate_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel/standard-rates-list','refresh'); }

      $rates = $this->api->get_standard_rates($rate_id, $cust_id);
      //echo json_encode($rates); die();
      if($rates['is_formula_volume_rate'] == 0 && $rates['is_formula_weight_rate'] == 0):
        $this->load->view('user_panel/standard_rates_edit_view', compact('rates'));
      else:
        if($rates['is_formula_volume_rate'] == 1):
          $this->load->view('user_panel/formula_volume_based_rates_edit_view', compact('rates'));
        else:
          $this->load->view('user_panel/formula_weight_based_rates_edit_view', compact('rates'));
        endif;
      endif;
      $this->load->view('user_panel/footer');
    }
    public function update_standard_rates()
    {
      //echo json_encode($_POST); die();
      $rate_id = $this->input->post('rate_id', TRUE);   
      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);

      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $update_data = array( 
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),
        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration), 
      );

      if($this->api->update_standard_rates($update_data, $rate_id)){
        $this->session->set_flashdata('success', $this->lang->line('Rates successfully updated!'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to update rates! Try again...'));  }
      redirect(base_url('user-panel/edit-standard-rates/'.$rate_id));
    }
    public function delete_standard_rates()
    {
      $rates_id = $this->input->post('id', TRUE);
      if($this->api->delete_standard_rates($rates_id)){ echo 'success'; } else { echo "failed";  } 
    }
    public function add_weight_based()
    {
      $countries_list = $this->api->get_countries();
      $categories = $this->dimension->get_categories();
      $unit_list = $this->api->get_unit_master(); 
      $this->load->view('user_panel/standard_rates_add_weight_based_view', compact('countries_list','unit_list','categories'));
      $this->load->view('user_panel/footer'); 
    }
    public function register_weight_based()
    {
      //echo json_encode($_POST); die(); 
      $cust_id = (int)$this->cust_id;
      $categories = $this->input->post('category', TRUE); 
      $country_id = $this->input->post('country_id', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);
      $min_distance = $this->input->post('min_distance', TRUE);
      $max_distance = $this->input->post('max_distance', TRUE);
      $min_weight = $this->input->post('min_weight', TRUE);
      $max_weight = $this->input->post('max_weight', TRUE);
      $unit_id = $this->input->post('unit_id', TRUE);     
      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $currency = $this->api->get_currency_detail((int) $currency_id);
      $insert_flag = false;

      $post_data = array(
        "category_id" =>  $categories,
        "country_id" =>  (int)($country_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']), 
        "min_distance" =>  trim($min_distance), 
        "max_distance" =>  trim($max_distance), 
        "min_weight" => trim($min_weight), 
        "max_weight" => trim($max_weight),
        "unit_id" => trim($unit_id),         
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),
        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),       
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );

      foreach ($categories as $c) {
        $insert_data = array(
          "category_id" =>  (int)($c), 
          "country_id" =>  (int)($country_id), 
          "currency_id" =>  (int)($currency_id), 
          "currency_sign" =>  trim($currency['currency_sign']), 
          "currency_title" =>  trim($currency['currency_title']), 
          "min_distance" =>  trim($min_distance), 
          "max_distance" =>  trim($max_distance), 
          "min_weight" => trim($min_weight), 
          "max_weight" => trim($max_weight),
          "unit_id" => trim($unit_id), 
          "min_dimension_id" => 0, 
          "min_width" => 0, 
          "min_height" => 0, 
          "min_length" => 0, 
          "min_volume" => 0, 
          "max_dimension_id" => 0, 
          "max_width" => 0, 
          "max_height" => 0, 
          "max_length" => 0, 
          "max_volume" => 0, 
          "earth_local_rate" =>  trim($earth_local_rate), 
          "earth_national_rate" =>  trim($earth_national_rate), 
          "earth_international_rate" =>  trim($earth_international_rate), 
          "air_local_rate" =>  trim($air_local_rate), 
          "air_national_rate" =>  trim($air_national_rate), 
          "air_international_rate" =>  trim($air_international_rate), 
          "sea_local_rate" =>  trim($sea_local_rate), 
          "sea_national_rate" =>  trim($sea_national_rate), 
          "sea_international_rate" =>  trim($sea_international_rate),
          "earth_local_duration" =>  trim($earth_local_duration), 
          "earth_national_duration" =>  trim($earth_national_duration), 
          "earth_international_duration" =>  trim($earth_international_duration), 
          "air_local_duration" =>  trim($air_local_duration), 
          "air_national_duration" =>  trim($air_national_duration), 
          "air_international_duration" =>  trim($air_international_duration), 
          "sea_local_duration" =>  trim($sea_local_duration), 
          "sea_national_duration" =>  trim($sea_national_duration), 
          "sea_international_duration" =>  trim($sea_international_duration),       
          "cre_datetime" =>  date("Y-m-d H:i:s"),
          "cust_id" =>  trim($cust_id),
        );

        if(!$this->api->check_min_weight_based_standard_rates($insert_data) && !$this->api->check_max_weight_based_standard_rates($insert_data)) {
          if($this->api->register_standard_rates($insert_data)){ $insert_flag = true; } 
          else { $insert_flag = false;   }  
        }
      }

      if($insert_flag){ $this->session->set_flashdata('success', $this->lang->line('Rate successfully added!')); }
      else{ $this->session->set_flashdata('error',array('error_msg' => $this->lang->line('Unable to add rates! Try again...'), 'data' => $post_data));  }
      redirect('user-panel/add-weight-based');
    }
    public function add_formula_volume_based()
    {
      $countries_list = $this->api->get_countries();
      $categories = $this->dimension->get_categories();
      $dimensions_list = $this->dimension->get_standard_dimension(); 
      $this->load->view('user_panel/standard_rates_add_formula_volume_based_view', compact('countries_list','dimensions_list','categories'));
      $this->load->view('user_panel/footer'); 
    }
    public function register_formula_volume_based()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $categories = $this->input->post('category', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);
      $min_distance = $this->input->post('min_distance', TRUE);
      $max_distance = $this->input->post('max_distance', TRUE);

      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);

      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $currency = $this->api->get_currency_detail((int) $currency_id);
      $insert_flag = false;

      $post_data = array(
        "category_id" =>  $categories,
        "country_id" =>  (int)($country_id), 
        "currency_id" =>  (int)($currency_id),        
        "min_distance" =>  trim($min_distance), 
        "max_distance" =>  trim($max_distance),
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
      );

      foreach ($categories as $c) {
        $insert_data = array(
          "category_id" =>  (int)($c), 
          "country_id" =>  (int)($country_id), 
          "currency_id" =>  (int)($currency_id), 
          "currency_sign" =>  trim($currency['currency_sign']), 
          "currency_title" =>  trim($currency['currency_title']),      
          "min_distance" =>  trim($min_distance), 
          "max_distance" =>  trim($max_distance), 
          "min_dimension_id" => 0, 
          "min_width" =>  0, 
          "min_height" =>  0, 
          "min_length" =>  0, 
          "min_volume" =>  0, 
          "max_dimension_id" => 0,
          "max_width" =>  0, 
          "max_height" =>  0, 
          "max_length" =>  0, 
          "max_volume" =>  0, 
          "min_weight" =>  0, 
          "max_weight" =>  0, 
          "unit_id" =>  0, 
          
          "earth_local_rate" =>  trim($earth_local_rate), 
          "earth_national_rate" =>  trim($earth_national_rate), 
          "earth_international_rate" =>  trim($earth_international_rate), 
          "air_local_rate" =>  trim($air_local_rate), 
          "air_national_rate" =>  trim($air_national_rate), 
          "air_international_rate" =>  trim($air_international_rate), 
          "sea_local_rate" =>  trim($sea_local_rate), 
          "sea_national_rate" =>  trim($sea_national_rate), 
          "sea_international_rate" =>  trim($sea_international_rate),

          "earth_local_duration" =>  trim($earth_local_duration), 
          "earth_national_duration" =>  trim($earth_national_duration), 
          "earth_international_duration" =>  trim($earth_international_duration), 
          "air_local_duration" =>  trim($air_local_duration), 
          "air_national_duration" =>  trim($air_national_duration), 
          "air_international_duration" =>  trim($air_international_duration), 
          "sea_local_duration" =>  trim($sea_local_duration), 
          "sea_national_duration" =>  trim($sea_national_duration), 
          "sea_international_duration" =>  trim($sea_international_duration),
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "is_formula_volume_rate" => 1,
          "cust_id" => (int)$cust_id,
        );
        
        if(!$this->api->check_formula_min_volume_based_standard_rates($insert_data)  && !$this->api->check_formula_max_volume_based_standard_rates($insert_data)) { 
          if($this->api->register_standard_rates($insert_data)){ $insert_flag = true; }
          else { $insert_flag = false; } 
        } 
      }
      
      if($insert_flag){ $this->session->set_flashdata('success', $this->lang->line('Rate successfully added!')); }
      else{ $this->session->set_flashdata('error',array('error_msg' => $this->lang->line('Unable to add rates! Try again...'), 'data' => $post_data));  }

      redirect('user-panel/add-formula-volume-based');
    }
    public function add_formula_weight_based()
    {
      $countries_list = $this->api->get_countries();
      $categories = $this->dimension->get_categories();
      $unit_list = $this->api->get_unit_master(); 
      $this->load->view('user_panel/standard_rates_add_formula_weight_based_view', compact('countries_list','unit_list','categories'));
      $this->load->view('user_panel/footer'); 
    }
    public function register_formula_weight_based()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $categories = $this->input->post('category', TRUE); 
      $country_id = $this->input->post('country_id', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);
      $min_distance = $this->input->post('min_distance', TRUE);
      $max_distance = $this->input->post('max_distance', TRUE);
      $unit_id = $this->input->post('unit_id', TRUE);     
      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $currency = $this->api->get_currency_detail((int) $currency_id);
      $insert_flag = false;

      $post_data = array(
        "category_id" =>  $categories,
        "country_id" =>  (int)($country_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),  
        "min_distance" =>  trim($min_distance), 
        "max_distance" =>  trim($max_distance), 
        "unit_id" => trim($unit_id),         
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),
        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),       
        "cre_datetime" =>  date("Y-m-d H:i:s"),    
      );

      foreach ($categories as $c) {
        $insert_data = array(
          "category_id" =>  (int)($c), 
          "country_id" =>  (int)($country_id), 
          "currency_id" =>  (int)($currency_id), 
          "currency_sign" =>  trim($currency['currency_sign']), 
          "currency_title" =>  trim($currency['currency_title']), 
          "min_distance" =>  trim($min_distance), 
          "max_distance" =>  trim($max_distance), 
          "min_weight" => 0, 
          "max_weight" => 0,
          "unit_id" => trim($unit_id), 
          "min_dimension_id" => 0, 
          "min_width" => 0, 
          "min_height" => 0, 
          "min_length" => 0, 
          "min_volume" => 0, 
          "max_dimension_id" => 0, 
          "max_width" => 0, 
          "max_height" => 0, 
          "max_length" => 0, 
          "max_volume" => 0, 
          "earth_local_rate" =>  trim($earth_local_rate), 
          "earth_national_rate" =>  trim($earth_national_rate), 
          "earth_international_rate" =>  trim($earth_international_rate), 
          "air_local_rate" =>  trim($air_local_rate), 
          "air_national_rate" =>  trim($air_national_rate), 
          "air_international_rate" =>  trim($air_international_rate), 
          "sea_local_rate" =>  trim($sea_local_rate), 
          "sea_national_rate" =>  trim($sea_national_rate), 
          "sea_international_rate" =>  trim($sea_international_rate),
          "earth_local_duration" =>  trim($earth_local_duration), 
          "earth_national_duration" =>  trim($earth_national_duration), 
          "earth_international_duration" =>  trim($earth_international_duration), 
          "air_local_duration" =>  trim($air_local_duration), 
          "air_national_duration" =>  trim($air_national_duration), 
          "air_international_duration" =>  trim($air_international_duration), 
          "sea_local_duration" =>  trim($sea_local_duration), 
          "sea_national_duration" =>  trim($sea_national_duration), 
          "sea_international_duration" =>  trim($sea_international_duration),
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "is_formula_weight_rate" =>  1,
          "cust_id" => (int)$cust_id,
        );

        if(!$this->api->check_formula_min_weight_based_standard_rates($insert_data) && !$this->api->check_formula_max_weight_based_standard_rates($insert_data) ) {
          if($this->api->register_standard_rates($insert_data)){ $insert_flag = true; } 
          else { $insert_flag = false;   }  
        }
      }

      if($insert_flag){ $this->session->set_flashdata('success', $this->lang->line('Rate successfully added!')); }
      else{ $this->session->set_flashdata('error',array('error_msg' => $this->lang->line('Unable to add rates! Try again...'), 'data' => $post_data));  }
      redirect('user-panel/add-formula-weight-based');
    }
  // Standard Rates----------------------------

  // Point to Point Rates----------------------
    public function ptop_rates_list()
    {
      $cust_id = (int)$this->cust_id;
      $rates = $this->api->get_pp_rates(0, $cust_id);
      //echo json_encode($rates); die();
      $this->load->view('user_panel/point_to_point_list_view', compact('rates'));      
      $this->load->view('user_panel/footer');   
    }
    public function get_states()
    {
      $country_id = $this->input->post('country_id', TRUE);
      $states = $this->api->get_states((int)$country_id);
      echo json_encode($states);
    }
    public function get_cities()
    {
      $state_id = $this->input->post('state_id', TRUE);
      $cities = $this->api->get_cities($state_id);
      echo json_encode($cities);
    }
    public function add_ptop_volume_based()
    {
      $countries_list = $this->api->get_countries();
      $categories = $this->dimension->get_categories();
      $dimensions_list = $this->dimension->get_standard_dimension(); 
      $this->load->view('user_panel/point_to_point_add_volume_based_view', compact('countries_list','dimensions_list','categories'));
      $this->load->view('user_panel/footer'); 
    }
    public function register_ptop_volume_based()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $categories = $this->input->post('category', TRUE);    
      $from_country_id = $this->input->post('from_country_id', TRUE);
      $from_state_id = $this->input->post('from_state_id', TRUE);
      $from_city_id = $this->input->post('from_city_id', TRUE);
      $to_country_id = $this->input->post('to_country_id', TRUE);
      $to_state_id = $this->input->post('to_state_id', TRUE);
      $to_city_id = $this->input->post('to_city_id', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);

      $min_width = $this->input->post('min_width', TRUE);
      $min_height = $this->input->post('min_height', TRUE);
      $min_length = $this->input->post('min_length', TRUE);
      $min_dimension = $this->input->post('min_dimension', TRUE);

      $max_width = $this->input->post('max_width', TRUE);
      $max_height = $this->input->post('max_height', TRUE);
      $max_length = $this->input->post('max_length', TRUE);
      $max_dimension = $this->input->post('max_dimension', TRUE);

      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);  
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);  
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);

      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);

      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $currency = $this->api->get_currency_detail((int) $currency_id);
      $insert_flag = false;
      $post_data = array(
        "category_id" =>  $categories, 
        "from_country_id" =>  (int)($from_country_id), 
        "from_state_id" =>  (int)($from_state_id), 
        "from_city_id" =>  (int)($from_city_id), 
        "to_country_id" =>  (int)($to_country_id), 
        "to_state_id" =>  (int)($to_state_id), 
        "to_city_id" =>  (int)($to_city_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),       

        "min_width" =>  trim($min_width), 
        "min_height" =>  trim($min_height), 
        "min_length" =>  trim($min_length), 
        "min_volume" =>  ( $min_width * $min_height * $min_length ), 
        "min_dimension_id" => (trim($min_dimension) == "other" ) ? 0 : $min_dimension , 
        
        "max_width" =>  trim($max_width), 
        "max_height" =>  trim($max_height), 
        "max_length" =>  trim($max_length), 
        "max_volume" =>  ( $max_width * $max_height * $max_length ), 
        "max_dimension_id" => (trim($max_dimension) == "other" ) ? 0 : $max_dimension ,
             
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );

      foreach ($categories as $c) {
        $insert_data = array(
          "category_id" =>  (int)($c), 
          "from_country_id" =>  (int)($from_country_id), 
          "from_state_id" =>  (int)($from_state_id), 
          "from_city_id" =>  (int)($from_city_id), 
          "to_country_id" =>  (int)($to_country_id), 
          "to_state_id" =>  (int)($to_state_id), 
          "to_city_id" =>  (int)($to_city_id), 
          "currency_id" =>  (int)($currency_id), 
          "currency_sign" =>  trim($currency['currency_sign']), 
          "currency_title" =>  trim($currency['currency_title']),       

          "min_width" =>  trim($min_width), 
          "min_height" =>  trim($min_height), 
          "min_length" =>  trim($min_length), 
          "min_volume" =>  ( $min_width * $min_height * $min_length ), 
          "min_dimension_id" => (trim($min_dimension) == "other" ) ? 0 : $min_dimension , 
          
          "max_width" =>  trim($max_width), 
          "max_height" =>  trim($max_height), 
          "max_length" =>  trim($max_length), 
          "max_volume" =>  ( $max_width * $max_height * $max_length ), 
          "max_dimension_id" => (trim($max_dimension) == "other" ) ? 0 : $max_dimension ,
          
          "min_weight" =>  0, 
          "max_weight" =>  0, 
          "unit_id" =>  0, 
          
          "earth_local_rate" =>  trim($earth_local_rate), 
          "earth_national_rate" =>  trim($earth_national_rate), 
          "earth_international_rate" =>  trim($earth_international_rate), 
          "air_local_rate" =>  trim($air_local_rate), 
          "air_national_rate" =>  trim($air_national_rate), 
          "air_international_rate" =>  trim($air_international_rate), 
          "sea_local_rate" =>  trim($sea_local_rate), 
          "sea_national_rate" =>  trim($sea_national_rate), 
          "sea_international_rate" =>  trim($sea_international_rate),

          "earth_local_duration" =>  trim($earth_local_duration), 
          "earth_national_duration" =>  trim($earth_national_duration), 
          "earth_international_duration" =>  trim($earth_international_duration), 
          "air_local_duration" =>  trim($air_local_duration), 
          "air_national_duration" =>  trim($air_national_duration), 
          "air_international_duration" =>  trim($air_international_duration), 
          "sea_local_duration" =>  trim($sea_local_duration), 
          "sea_national_duration" =>  trim($sea_national_duration), 
          "sea_international_duration" =>  trim($sea_international_duration),
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "cust_id" =>  (int)trim($cust_id),
        );
        
        if( !$this->api->check_volume_based_p_to_p_rates($insert_data,"min")  && !$this->api->check_volume_based_p_to_p_rates($insert_data,"max")) {      
          if($this->api->register_p_to_p_rates($insert_data)){ $insert_flag = true; } 
          else { $insert_flag = false;   } 
        } 
      }    

      if($insert_flag){ $this->session->set_flashdata('success', $this->lang->line('Rate successfully added!')); }
      else{ $this->session->set_flashdata('error',array('error_msg' => $this->lang->line('Unable to add rates! Try again...'), 'data' => $post_data));  }
      redirect('user-panel/add-ptop-volume-based');
    }
    public function delete_ptop_rates()
    {
      $rates_id = $this->input->post('id', TRUE);
      if($this->api->delete_pp_rates($rates_id)){ echo 'success'; } else { echo "failed";  } 
    }
    public function edit_ptop_rates()
    {   
      if( !is_null($this->input->post('rate_id')) ) { $rate_id = (int) $this->input->post('rate_id'); $rate_id = (int)$rate_id; }
      else if(!is_null($this->uri->segment(3))) { $rate_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel/ptop-rates-list','refresh'); }
      //echo $rate_id; die();
      if($rates = $this->api->get_pp_rates($rate_id)){
        //echo json_encode($rates); die();
        $this->load->view('user_panel/point_to_point_rates_edit_view', compact('rates'));
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel/ptop-rates-list'); }
    }
    public function update_ptop_rates()
    {
      //echo json_encode($_POST); die();
      $rate_id = $this->input->post('rate_id', TRUE);   
      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);

      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $update_data = array( 
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),
        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration), 
      );
      // echo json_encode($insert_data); die();
      if($this->api->update_pp_rates($update_data, $rate_id)){
        $this->session->set_flashdata('success', $this->lang->line('Rates successfully updated!'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to update rates! Try again...'));  }

      redirect('user-panel/edit-ptop-rates/'.$rate_id);
    }
    public function add_ptop_weight_based()
    {
      $categories = $this->dimension->get_categories();
      $countries_list = $this->api->get_countries();
      $unit_list = $this->api->get_unit_master(); 
      $this->load->view('user_panel/point_to_point_add_weight_based_view', compact('countries_list','unit_list','categories'));
      $this->load->view('user_panel/footer'); 
    }
    public function register_ptop_weight_based()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $categories = $this->input->post('category', TRUE);    
      $from_country_id = $this->input->post('from_country_id', TRUE);
      $from_state_id = $this->input->post('from_state_id', TRUE);
      $from_city_id = $this->input->post('from_city_id', TRUE);
      $to_country_id = $this->input->post('to_country_id', TRUE);
      $to_state_id = $this->input->post('to_state_id', TRUE);
      $to_city_id = $this->input->post('to_city_id', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);

      $min_weight = $this->input->post('min_weight', TRUE);
      $max_weight = $this->input->post('max_weight', TRUE);
      $unit_id = $this->input->post('unit_id', TRUE);

      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);  
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);  
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);

      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);

      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $currency = $this->api->get_currency_detail((int) $currency_id);
      $insert_flag = false;

      $post_data = array(
        "category_id" =>  $categories,
        "from_country_id" =>  (int)($from_country_id), 
        "from_state_id" =>  (int)($from_state_id), 
        "from_city_id" =>  (int)($from_city_id), 
        "to_country_id" =>  (int)($to_country_id), 
        "to_state_id" =>  (int)($to_state_id), 
        "to_city_id" =>  (int)($to_city_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),       
        
        "min_weight" =>  (int) $min_weight, 
        "max_weight" =>  (int) $max_weight, 
        "unit_id" =>  (int) $unit_id, 
        
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );

      foreach ($categories as $c) {
        $insert_data = array(
          "category_id" =>  (int)($c),
          "from_country_id" =>  (int)($from_country_id), 
          "from_state_id" =>  (int)($from_state_id), 
          "from_city_id" =>  (int)($from_city_id), 
          "to_country_id" =>  (int)($to_country_id), 
          "to_state_id" =>  (int)($to_state_id), 
          "to_city_id" =>  (int)($to_city_id), 
          "currency_id" =>  (int)($currency_id), 
          "currency_sign" =>  trim($currency['currency_sign']), 
          "currency_title" =>  trim($currency['currency_title']),       

          "min_width" =>  0, 
          "min_height" =>  0, 
          "min_length" =>  0, 
          "min_volume" =>  0, 
          "min_dimension_id" => 0, 
          
          "max_width" =>  0, 
          "max_height" =>  0, 
          "max_length" =>  0, 
          "max_volume" =>  0, 
          "max_dimension_id" => 0 ,
          
          "min_weight" =>  (int) $min_weight, 
          "max_weight" =>  (int) $max_weight, 
          "unit_id" =>  (int) $unit_id, 
          
          "earth_local_rate" =>  trim($earth_local_rate), 
          "earth_national_rate" =>  trim($earth_national_rate), 
          "earth_international_rate" =>  trim($earth_international_rate), 
          "air_local_rate" =>  trim($air_local_rate), 
          "air_national_rate" =>  trim($air_national_rate), 
          "air_international_rate" =>  trim($air_international_rate), 
          "sea_local_rate" =>  trim($sea_local_rate), 
          "sea_national_rate" =>  trim($sea_national_rate), 
          "sea_international_rate" =>  trim($sea_international_rate),

          "earth_local_duration" =>  trim($earth_local_duration), 
          "earth_national_duration" =>  trim($earth_national_duration), 
          "earth_international_duration" =>  trim($earth_international_duration), 
          "air_local_duration" =>  trim($air_local_duration), 
          "air_national_duration" =>  trim($air_national_duration), 
          "air_international_duration" =>  trim($air_international_duration), 
          "sea_local_duration" =>  trim($sea_local_duration), 
          "sea_national_duration" =>  trim($sea_national_duration), 
          "sea_international_duration" =>  trim($sea_international_duration),
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "cust_id" =>  (int)trim($cust_id),
        );

        if( !$this->api->check_weight_based_p_to_p_rates($insert_data,"min")  && !$this->api->check_weight_based_p_to_p_rates($insert_data,"max")) {      
          if($this->api->register_p_to_p_rates($insert_data)){ $insert_flag = true;  } 
          else { $insert_flag = false;  } 
        } 
      }
      if($insert_flag){ $this->session->set_flashdata('success', $this->lang->line('Rate successfully added!')); }
      else{ $this->session->set_flashdata('error',array('error_msg' => $this->lang->line('Unable to add rates! Try again...'), 'data' => $post_data));  }
      redirect('user-panel/add-ptop-weight-based');
    }
    public function add_ptop_formula_volume_based()
    {
      $countries_list = $this->api->get_countries();
      $categories = $this->dimension->get_categories();
      $dimensions_list = $this->dimension->get_standard_dimension(); 
      $this->load->view('user_panel/point_to_point_add_formula_volume_based_view', compact('countries_list','dimensions_list','categories'));
      $this->load->view('user_panel/footer'); 
    }
    public function register_ptop_formula_volume_based()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $categories = $this->input->post('category', TRUE);    
      $from_country_id = $this->input->post('from_country_id', TRUE);
      $from_state_id = $this->input->post('from_state_id', TRUE);
      $from_city_id = $this->input->post('from_city_id', TRUE);
      $to_country_id = $this->input->post('to_country_id', TRUE);
      $to_state_id = $this->input->post('to_state_id', TRUE);
      $to_city_id = $this->input->post('to_city_id', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);

      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);  
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);  
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);

      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);

      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $currency = $this->api->get_currency_detail((int) $currency_id);
      $insert_flag = false;
      $post_data = array(
        "category_id" =>  $categories, 
        "from_country_id" =>  (int)($from_country_id), 
        "from_state_id" =>  (int)($from_state_id), 
        "from_city_id" =>  (int)($from_city_id), 
        "to_country_id" =>  (int)($to_country_id), 
        "to_state_id" =>  (int)($to_state_id), 
        "to_city_id" =>  (int)($to_city_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),
             
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );

      foreach ($categories as $c) {
        $insert_data = array(
          "category_id" =>  (int)($c), 
          "from_country_id" =>  (int)($from_country_id), 
          "from_state_id" =>  (int)($from_state_id), 
          "from_city_id" =>  (int)($from_city_id), 
          "to_country_id" =>  (int)($to_country_id), 
          "to_state_id" =>  (int)($to_state_id), 
          "to_city_id" =>  (int)($to_city_id), 
          "currency_id" =>  (int)($currency_id), 
          "currency_sign" =>  trim($currency['currency_sign']), 
          "currency_title" =>  trim($currency['currency_title']),       

          "min_width" =>  0, 
          "min_height" =>  0, 
          "min_length" =>  0, 
          "min_volume" =>  0, 
          "min_dimension_id" => 0, 
          
          "max_width" =>  0, 
          "max_height" =>  0, 
          "max_length" =>  0, 
          "max_volume" =>  0, 
          "max_dimension_id" => 0,
          
          "min_weight" =>  0, 
          "max_weight" =>  0, 
          "unit_id" =>  0, 
          
          "earth_local_rate" =>  trim($earth_local_rate), 
          "earth_national_rate" =>  trim($earth_national_rate), 
          "earth_international_rate" =>  trim($earth_international_rate), 
          "air_local_rate" =>  trim($air_local_rate), 
          "air_national_rate" =>  trim($air_national_rate), 
          "air_international_rate" =>  trim($air_international_rate), 
          "sea_local_rate" =>  trim($sea_local_rate), 
          "sea_national_rate" =>  trim($sea_national_rate), 
          "sea_international_rate" =>  trim($sea_international_rate),

          "earth_local_duration" =>  trim($earth_local_duration), 
          "earth_national_duration" =>  trim($earth_national_duration), 
          "earth_international_duration" =>  trim($earth_international_duration), 
          "air_local_duration" =>  trim($air_local_duration), 
          "air_national_duration" =>  trim($air_national_duration), 
          "air_international_duration" =>  trim($air_international_duration), 
          "sea_local_duration" =>  trim($sea_local_duration), 
          "sea_national_duration" =>  trim($sea_national_duration), 
          "sea_international_duration" =>  trim($sea_international_duration),
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "is_formula_volume_rate" =>  1,
          "cust_id" =>  (int)trim($cust_id),
        );
        
        if( !$this->api->check_formula_volume_based_p_to_p_rates($insert_data) ) {      
          if($this->api->register_p_to_p_rates($insert_data)){ $insert_flag = true; } 
          else { $insert_flag = false;   } 
        } 
      }    

      if($insert_flag){ $this->session->set_flashdata('success', $this->lang->line('Rate successfully added!')); }
      else{ $this->session->set_flashdata('error',array('error_msg' => $this->lang->line('Unable to add rates! Try again...'), 'data' => $post_data));  }
      redirect('user-panel/add-ptop-formula-volume-based');
    }
    public function add_ptop_formula_weight_based()
    {
      $categories = $this->dimension->get_categories();
      $countries_list = $this->api->get_countries();
      $unit_list = $this->api->get_unit_master(); 
      $this->load->view('user_panel/point_to_point_formula_add_weight_based_view', compact('countries_list','unit_list','categories'));
      $this->load->view('user_panel/footer'); 
    }
    public function get_unit_detail()
    {
      $unit_id = $this->input->post('unt');
      echo json_encode($this->api->get_unit_detail($unit_id));
    }
    public function register_ptop_formula_weight_based()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $categories = $this->input->post('category', TRUE);    
      $from_country_id = $this->input->post('from_country_id', TRUE);
      $from_state_id = $this->input->post('from_state_id', TRUE);
      $from_city_id = $this->input->post('from_city_id', TRUE);
      $to_country_id = $this->input->post('to_country_id', TRUE);
      $to_state_id = $this->input->post('to_state_id', TRUE);
      $to_city_id = $this->input->post('to_city_id', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);

      $unit_id = $this->input->post('unit_id', TRUE);

      $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
      $earth_local_duration = $this->input->post('earth_local_duration', TRUE);  
      $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
      $earth_national_duration = $this->input->post('earth_national_duration', TRUE);  
      $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
      $earth_international_duration = $this->input->post('earth_international_duration', TRUE);

      $air_local_rate = $this->input->post('air_local_rate', TRUE);
      $air_local_duration = $this->input->post('air_local_duration', TRUE);
      $air_national_rate = $this->input->post('air_national_rate', TRUE);
      $air_national_duration = $this->input->post('air_national_duration', TRUE);
      $air_international_rate = $this->input->post('air_international_rate', TRUE);
      $air_international_duration = $this->input->post('air_international_duration', TRUE);

      $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
      $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
      $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
      $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
      $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
      $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

      $currency = $this->api->get_currency_detail((int) $currency_id);
      $insert_flag = false;

      $post_data = array(
        "category_id" =>  $categories,
        "from_country_id" =>  (int)($from_country_id), 
        "from_state_id" =>  (int)($from_state_id), 
        "from_city_id" =>  (int)($from_city_id), 
        "to_country_id" =>  (int)($to_country_id), 
        "to_state_id" =>  (int)($to_state_id), 
        "to_city_id" =>  (int)($to_city_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),       
        
        "unit_id" =>  (int) $unit_id, 
        
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );

      foreach ($categories as $c) {
        $insert_data = array(
          "category_id" =>  (int)($c),
          "from_country_id" =>  (int)($from_country_id), 
          "from_state_id" =>  (int)($from_state_id), 
          "from_city_id" =>  (int)($from_city_id), 
          "to_country_id" =>  (int)($to_country_id), 
          "to_state_id" =>  (int)($to_state_id), 
          "to_city_id" =>  (int)($to_city_id), 
          "currency_id" =>  (int)($currency_id), 
          "currency_sign" =>  trim($currency['currency_sign']), 
          "currency_title" =>  trim($currency['currency_title']),       

          "min_width" =>  0, 
          "min_height" =>  0, 
          "min_length" =>  0, 
          "min_volume" =>  0, 
          "min_dimension_id" => 0, 
          
          "max_width" =>  0, 
          "max_height" =>  0, 
          "max_length" =>  0, 
          "max_volume" =>  0, 
          "max_dimension_id" => 0 ,
          
          "min_weight" =>  0, 
          "max_weight" =>  0, 
          "unit_id" =>  (int) $unit_id, 
          
          "earth_local_rate" =>  trim($earth_local_rate), 
          "earth_national_rate" =>  trim($earth_national_rate), 
          "earth_international_rate" =>  trim($earth_international_rate), 
          "air_local_rate" =>  trim($air_local_rate), 
          "air_national_rate" =>  trim($air_national_rate), 
          "air_international_rate" =>  trim($air_international_rate), 
          "sea_local_rate" =>  trim($sea_local_rate), 
          "sea_national_rate" =>  trim($sea_national_rate), 
          "sea_international_rate" =>  trim($sea_international_rate),

          "earth_local_duration" =>  trim($earth_local_duration), 
          "earth_national_duration" =>  trim($earth_national_duration), 
          "earth_international_duration" =>  trim($earth_international_duration), 
          "air_local_duration" =>  trim($air_local_duration), 
          "air_national_duration" =>  trim($air_national_duration), 
          "air_international_duration" =>  trim($air_international_duration), 
          "sea_local_duration" =>  trim($sea_local_duration), 
          "sea_national_duration" =>  trim($sea_national_duration), 
          "sea_international_duration" =>  trim($sea_international_duration),
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "is_formula_weight_rate" =>  1,
          "cust_id" =>  (int)trim($cust_id),
        );

        if( !$this->api->check_formula_weight_based_p_to_p_rates($insert_data) ) {      
          if($this->api->register_p_to_p_rates($insert_data)){ $insert_flag = true;  } 
          else { $insert_flag = false;  } 
        } 
      }
      if($insert_flag){ $this->session->set_flashdata('success', $this->lang->line('Rate successfully added!')); }
      else{ $this->session->set_flashdata('error',array('error_msg' => $this->lang->line('Unable to add rates! Try again...'), 'data' => $post_data));  }
      redirect('user-panel/add-ptop-formula-weight-based');
    }
  // Point to Point Rates----------------------

  public function user_pending_invoices_history()
  {
    $details = $this->api->get_user_pending_invoices_history($this->cust_id);
    $this->load->view('user_panel/pending_payment_invoice_list_view', compact('details'));
    $this->load->view('user_panel/footer');
  }

  // Promo Code----------------------
    public function promocode()
    {
      if(isset($_POST["promo_code"])){
        $order_id = $this->input->post('courier_order_id');
        $details =  $this->api->get_order_detail($order_id);
        $promo_code_name = $this->input->post("promo_code");
        if(!$details['promo_code']){
          if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
            $used_codes = $this->api->get_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version']);
            if(sizeof($used_codes) < $promocode['no_of_limit']){
              if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$this->cust_id)){
                  //code start here
                    $discount_amount = round(($details['order_price']/100)*$promocode['discount_percent'] , 2); 
                    if($details['currency_sign'] == 'XAF'){
                      $discount_amount = round($discount_amount , 0);
                    }
                    $order_price =  round($details['order_price']-$discount_amount ,2);
                    if($details['currency_sign'] == 'XAF'){
                      $order_price = round($order_price , 0);
                    }
                    $advance_payment = round(($details['advance_payment']/100)*$promocode['discount_percent'] ,2);
                    if($details['currency_sign'] == 'XAF'){
                      $advance_payment = round($advance_payment , 0);
                    }
                    $pickup_payment = round( ($details['pickup_payment']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $pickup_payment = round($pickup_payment , 0);
                    }
                    $deliver_payment = round( ($details['deliver_payment']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $deliver_payment = round($deliver_payment , 0);
                    }
                    $insurance_fee = round( ($details['insurance_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $insurance_fee = round($insurance_fee , 0);
                    }
                    $commission_amount = round( ($details['commission_amount']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $commission_amount = round($commission_amount , 0);
                    }

                    $standard_price = round( ($details['standard_price']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $standard_price = round($standard_price , 0);
                    }
                    $urgent_fee = round( ($details['urgent_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $urgent_fee = round($urgent_fee , 0);
                    }

                    $handling_fee = round( ($details['handling_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $handling_fee = round($handling_fee , 0);
                    }
                    $vehicle_fee = round( ($details['vehicle_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $vehicle_fee = round($vehicle_fee , 0);
                    }

                    $custom_clearance_fee = round( ($details['custom_clearance_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $custom_clearance_fee = round($custom_clearance_fee , 0);
                    }

                    $loading_unloading_charges = round( ($details['loading_unloading_charges']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $loading_unloading_charges = round($loading_unloading_charges , 0);
                    }

                   
                    $courier_order = array(
                      'promo_code' => $promocode['promo_code'],
                      'promo_code_id' => $promocode['promo_code_id'],
                      'promo_code_version' => $promocode['promo_code_version'],
                      'discount_amount' => $discount_amount,
                      'discount_percent' => $promocode['discount_percent'],
                      'actual_order_price' => $details['order_price'],
                      'order_price' => $order_price ,
                      'advance_payment' => ($details['advance_payment']-$advance_payment),
                      'pickup_payment' => ($details['pickup_payment']-$pickup_payment),
                      'deliver_payment' => ($details['deliver_payment']-$deliver_payment),
                      'balance_amount' => $order_price,
                      'insurance_fee' => ($details['insurance_fee']-$insurance_fee),
                      'commission_amount' => ($details['commission_amount']-$commission_amount),
                      'standard_price' => ($details['standard_price']-$standard_price),
                      'urgent_fee' => ($details['urgent_fee']-$urgent_fee),
                      'handling_fee' =>  ($details['handling_fee']-$handling_fee),
                      'vehicle_fee' => ($details['vehicle_fee']-$vehicle_fee),
                      'custom_clearance_fee'=>($details['custom_clearance_fee']-$custom_clearance_fee),
                      'loading_unloading_charges'=>($details['loading_unloading_charges']-$loading_unloading_charges),
                      'promo_code_datetime'=> date('Y-m-d h:i:s'),
                    );

                    $this->api->update_review($courier_order,$details['order_id']);
                    $used_promo_update = array(
                      'promo_code_id' => $promocode['promo_code_id'],
                      'promo_code_version' => $promocode['promo_code_version'],
                      'cust_id' => $this->cust_id,
                      'customer_email' => "",
                      'discount_percent' => $promocode['discount_percent'],
                      'discount_amount' => $discount_amount,
                      'discount_currency' => $details['currency_sign'],
                      'cre_datetime' => date('Y-m-d h:i:s'),
                      'service_id' => '7',
                      'booking_id' => $details['order_id'],
                    );
                    $this->api->register_user_used_prmocode($used_promo_update);
                    //echo json_encode($courier_order); die();
                    $this->session->set_flashdata('success_promo', $this->lang->line('Promocode Applied successfully'));
                  //code end here
                }else{
                  $this->session->set_flashdata('error_promo', $this->lang->line('You are already used this promocode'));
                }
              }else{
                $this->session->set_flashdata('error_promo', $this->lang->line('This Promocode has been Expired'));
              }
            }else{
              $this->session->set_flashdata('error_promo', $this->lang->line('This Promo code is used maximum number of limit'));
            }
          }else{
            $this->session->set_flashdata('error_promo', $this->lang->line('Invalid Promocode Entered'));
          }
        }else{
            $this->session->set_flashdata('error_promo', $this->lang->line('You are already used promocode for this order'));
        }
      }
      $this->session->set_userdata('order_id',$order_id);
      redirect('user-panel/courier-payment');
    }

    public function promocode_ajax()
    {
      $promo_code_name =  $this->input->post('promocode');
      $order_id =  $this->input->post('order_id');
      $details =  $this->api->get_order_detail($order_id);
      
      if($promo_code = $this->api->get_promocode_by_name($promo_code_name)) {
        $service_id = explode(',',$promo_code['service_id']);
        $used_codes = $this->api->get_used_promo_code($promo_code['promo_code_id'],$promo_code['promo_code_version']);
        if(sizeof($used_codes) < $promo_code['no_of_limit']){
        
          if(!$details['promo_code']){
            if(in_array("6", $service_id) || in_array("7", $service_id) || in_array("280", $service_id)){
              if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
                //echo json_encode($promocode); die();
                if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                  if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$this->cust_id)){
                    $discount_amount = round(($details['order_price']/100)*$promocode['discount_percent'] , 2); 
                    if($details['currency_sign'] == 'XAF'){
                      $discount_amount = round($discount_amount , 0);
                    }
                    $order_price =  round($details['order_price']-$discount_amount ,2);
                    if($details['currency_sign'] == 'XAF'){
                      $order_price = round($order_price , 0);
                    }

                    echo "success@".$this->lang->line('You will get')." ".$discount_amount." ".$details['currency_sign']." ".$this->lang->line('Discount')."$".$this->lang->line('Use this promocode and pay')." ".$order_price." ".$details['currency_sign']; 
                  }else{
                    echo "error@".$this->lang->line('You are already used this promocode');
                  }
                }else{
                  echo "error@".$this->lang->line('This Promocode has been Expired');
                }
              }else{
                echo "error@".$this->lang->line('Invalid Promocode Entered');
              }
            }else{
              echo "error@".$this->lang->line('This promocode is not valid for this category');
            }
          }else{
            echo "error@".$this->lang->line('You are already used promocode for this order');
          }
        }else{
          echo "error@".$this->lang->line('This Promo code is used maximum number of limit');
        }
      } else { echo "error@".$this->lang->line('Invalid Promocode Entered'); }
    }
    
    public function promocode_with_booking()
    {
      $promo_code_name =  $this->input->post('promocode');
      if($promocode = $this->api->get_promocode_by_name($promo_code_name)) {
        $service_id = explode(',',$promocode['service_id']);
        $used_codes = $this->api->get_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version']);
        if(sizeof($used_codes) < $promocode['no_of_limit']){
          if(in_array("6", $service_id) || in_array("7", $service_id) || in_array("280", $service_id)){
              //echo json_encode($promocode); die();
              if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$this->cust_id)){
                  echo "success@".$promocode['discount_percent']; 
                }else{
                  echo "error@".$this->lang->line('You are already used this promocode');
                }
              }else{
                echo "error@".$this->lang->line('This Promocode has been Expired');
              }  
          }else{
            echo "error@".$this->lang->line('This promocode is not valid for this category');
          }
        }else{
          echo "error@".$this->lang->line('This Promo code is used maximum number of limit');
        }
      } else { echo "error@".$this->lang->line('Invalid Promocode Entered'); }
    }
  // Promo Code----------------------

  public function test_view()
  {
    $this->load->view('user_panel/test_view');
  }
  // END: ADDRESS BOOK CONTROLLER

  public function get_currency_by_country_relay_id()
  {
    $from_id = $this->input->post('from_id');
    $address_type = $this->input->post('address_type');
    //echo json_encode($this->api->get_currency_by_country_relay_id($from_id, $address_type, $this->cust_id));
    $currencies = $this->api->get_country_currencies((int)$this->api->get_currency_by_country_relay_id($from_id, $address_type, $this->cust_id)['country_id']);
    echo json_encode($currencies[0]['currency_sign']);

  }

}

/* End of file User_panel.php */
/* Location: ./application/controllers/User_panel.php */