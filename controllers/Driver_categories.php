<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver_categories extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('driver_categories_model', 'category');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$user = $this->admin->logged_in_user_details($user['type_id']);
			$permissions = $this->admin->get_auth_permissions($id);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}

	public function index()
	{
		$categories = $this->category->get_categories();
		$this->load->view('admin/driver_category_list_view', compact('categories'));			
		$this->load->view('admin/footer_view');		
	}

	public function add()
	{
		$this->load->view('admin/driver_category_add_view');
		$this->load->view('admin/footer_view');
	}
	
	public function register()
	{
		$short_name = $this->input->post('short_name', TRUE);
 		$full_name = $this->input->post('full_name', TRUE);
 		$description = $this->input->post('description', TRUE);
 		if(trim($description) == ""){ $description = "NULL"; }

 		$insert_data = array(
 			"short_name" => trim($short_name), 
 			"full_name" =>  trim($full_name), 
 			"description" => trim($description), 
 			"cre_datetime" =>  date("Y-m-d H:i:s"), 
 			"mod_datetime" =>  date("Y-m-d H:i:s"), 
 		);

		if(! $this->category->check_category($insert_data) ) {
			if($this->category->register_category($insert_data)){
				$this->session->set_flashdata('success','Driver category successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new driver category! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Driver category already exists!');	}

		redirect('admin/driver-categories/add');
	}

	public function edit()
	{	
		if( $id = $this->uri->segment(4) ) { 			
			if( $category = $this->category->get_category_detail($id)){
				$this->load->view('admin/driver_category_edit_veiw', compact('category'));
				$this->load->view('admin/footer_view');
			} else { redirect('admin/driver-categories'); }
		} else { redirect('admin/driver-categories'); }
	}

	public function update()
	{
		$pc_id = $this->input->post('pc_id', TRUE);
		$short_name = $this->input->post('short_name', TRUE);
		$full_name = $this->input->post('full_name', TRUE);
		$description = $this->input->post('description', TRUE);

 		$update_data = array(
 			"short_name" =>  trim($short_name), 
 			"full_name" => trim($full_name), 
 			"description" => trim($description), 
 			"mod_datetime" => date("Y-m-d H:i:s"),
 		);

		if(! $this->category->check_category($update_data) ) {
			if($this->category->update_category($update_data, $pc_id)){
				$this->session->set_flashdata('success','Driver category successfully updated!');
			} else { 	$this->session->set_flashdata('error','Unable to update driver category! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Driver category already exists!');	}

		redirect('admin/driver-categories/edit/'.$pc_id);
	}

	public function active_category()
	{
		$id = (int) $this->input->post('id', TRUE);
		if( $this->category->activate_category($id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	public function inactive_category()
	{
		$id = (int) $this->input->post('id', TRUE);
		if( $this->category->inactivate_category($id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

}

/* End of file Driver_categories.php */
/* Location: ./application/controllers/Driver_categories.php */