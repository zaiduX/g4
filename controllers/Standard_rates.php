<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standard_rates extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('standard_rates_model', 'standard_rates');   
      $this->load->model('Standard_dimension_model', 'dimension');    
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $rates = $this->standard_rates->get_standard_rates();
    $this->load->view('admin/standard_rates_list_view', compact('rates'));      
    $this->load->view('admin/footer_view');   
  }

  public function add_volume_based()
  {
    $countries_list = $this->standard_rates->get_countries();
    $categories = $this->dimension->get_categories();
    $dimensions_list = $this->dimension->get_standard_dimension(); 
    $this->load->view('admin/standard_rates_add_volume_based_view', compact('countries_list','dimensions_list','categories'));
    $this->load->view('admin/footer_view'); 
  }

  public function add_weight_based()
  {
    $countries_list = $this->standard_rates->get_countries();
    $categories = $this->dimension->get_categories();
    $unit_list = $this->standard_rates->get_unit_master(); 
    $this->load->view('admin/standard_rates_add_weight_based_view', compact('countries_list','unit_list','categories'));
    $this->load->view('admin/footer_view'); 
  }

  public function add_formula_weight_based()
  {
    $countries_list = $this->standard_rates->get_countries();
    $categories = $this->dimension->get_categories();
    $unit_list = $this->standard_rates->get_unit_master(); 
    $this->load->view('admin/standard_rates_add_formula_weight_based_view', compact('countries_list','unit_list','categories'));
    $this->load->view('admin/footer_view'); 
  }

  public function add_formula_volume_based()
  {
    $countries_list = $this->standard_rates->get_countries();
    $categories = $this->dimension->get_categories();
    $dimensions_list = $this->dimension->get_standard_dimension(); 
    $this->load->view('admin/standard_rates_add_formula_volume_based_view', compact('countries_list','dimensions_list','categories'));
    $this->load->view('admin/footer_view'); 
  }

  public function get_country_currencies()
  {
    $country_id = $this->input->post('country_id', TRUE); $country_id = (int) $country_id;
    $currencies = $this->standard_rates->get_country_currencies($country_id);

    echo json_encode($currencies);
  }

  public function register_volume_based()
  {
    //echo json_encode($_POST); die();
    $categories = $this->input->post('category', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $currency_id = $this->input->post('currency_id', TRUE);
    $min_distance = $this->input->post('min_distance', TRUE);
    $max_distance = $this->input->post('max_distance', TRUE);
    $max_duration = $this->input->post('max_duration', TRUE);
    $min_dimension = $this->input->post('min_dimension', TRUE);
    $max_dimension = $this->input->post('max_dimension', TRUE);
    $min_width = $this->input->post('min_width', TRUE);
    $min_height = $this->input->post('min_height', TRUE);
    $min_length = $this->input->post('min_length', TRUE);
    $max_width = $this->input->post('max_width', TRUE);
    $max_height = $this->input->post('max_height', TRUE);
    $max_length = $this->input->post('max_length', TRUE);

    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);

    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $currency = $this->standard_rates->get_currency_detail((int) $currency_id);
    $insert_flag = false;

    $post_data = array(
      "category_id" =>  $categories,
      "country_id" =>  (int)($country_id), 
      "currency_id" =>  (int)($currency_id), 
      "currency_sign" =>  trim($currency['currency_sign']), 
      "currency_title" =>  trim($currency['currency_title']),
      "min_distance" =>  trim($min_distance), 
      "max_distance" =>  trim($max_distance), 
      "min_dimension_id" => (trim($min_dimension) == "other" ) ? 0 : $min_dimension , 
      "min_width" =>  trim($min_width), 
      "min_height" =>  trim($min_height), 
      "min_length" =>  trim($min_length), 
      "min_volume" =>  ( $min_width * $min_height * $min_length ), 
      "max_dimension_id" => (trim($max_dimension) == "other" ) ? 0 : $max_dimension ,
      "max_width" =>  trim($max_width), 
      "max_height" =>  trim($max_height), 
      "max_length" =>  trim($max_length), 
      "max_volume" =>  ( $max_width * $max_height * $max_length ), 
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),

      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration),
    );

    foreach ($categories as $c) {
      $insert_data = array(
        "category_id" =>  (int)($c), 
        "country_id" =>  (int)($country_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),
        "min_distance" =>  trim($min_distance), 
        "max_distance" =>  trim($max_distance), 
        "min_dimension_id" => (trim($min_dimension) == "other" ) ? 0 : $min_dimension , 
        "min_width" =>  trim($min_width), 
        "min_height" =>  trim($min_height), 
        "min_length" =>  trim($min_length), 
        "min_volume" =>  ( $min_width * $min_height * $min_length ), 
        "max_dimension_id" => (trim($max_dimension) == "other" ) ? 0 : $max_dimension ,
        "max_width" =>  trim($max_width), 
        "max_height" =>  trim($max_height), 
        "max_length" =>  trim($max_length), 
        "max_volume" =>  ( $max_width * $max_height * $max_length ), 
        "min_weight" =>  0, 
        "max_weight" =>  0, 
        "unit_id" =>  0, 
        
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );
      
      if(!$this->standard_rates->check_min_volume_based_standard_rates($insert_data)  && !$this->standard_rates->check_max_volume_based_standard_rates($insert_data)) {      
        if($this->standard_rates->register_standard_rates($insert_data)){ $insert_flag = true; }
        else { $insert_flag = false;   }                 
      } 
    }
    
    if($insert_flag){ $this->session->set_flashdata('success','Rate successfully added!'); }
    else{ $this->session->set_flashdata('error',array('error_msg' => 'Unable to add rates! Try again...', 'data' => $post_data));  }

    redirect('admin/standard-rates/add/volume-based');
  }

  public function register_weight_based()
  {
    // echo json_encode($_POST); die();
    $categories = $this->input->post('category', TRUE); 
    $country_id = $this->input->post('country_id', TRUE);
    $currency_id = $this->input->post('currency_id', TRUE);
    $min_distance = $this->input->post('min_distance', TRUE);
    $max_distance = $this->input->post('max_distance', TRUE);
    $min_weight = $this->input->post('min_weight', TRUE);
    $max_weight = $this->input->post('max_weight', TRUE);
    $unit_id = $this->input->post('unit_id', TRUE);     
    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $currency = $this->standard_rates->get_currency_detail((int) $currency_id);
    $insert_flag = false;

    $post_data = array(
      "category_id" =>  $categories,
      "country_id" =>  (int)($country_id), 
      "currency_id" =>  (int)($currency_id), 
      "currency_sign" =>  trim($currency['currency_sign']), 
      "currency_title" =>  trim($currency['currency_title']), 
      "min_distance" =>  trim($min_distance), 
      "max_distance" =>  trim($max_distance), 
      "min_weight" => trim($min_weight), 
      "max_weight" => trim($max_weight),
      "unit_id" => trim($unit_id),         
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),
      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration),       
      "cre_datetime" =>  date("Y-m-d H:i:s"), 
    );

    foreach ($categories as $c) {

      $insert_data = array(
        "category_id" =>  (int)($c), 
        "country_id" =>  (int)($country_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']), 
        "min_distance" =>  trim($min_distance), 
        "max_distance" =>  trim($max_distance), 
        "min_weight" => trim($min_weight), 
        "max_weight" => trim($max_weight),
        "unit_id" => trim($unit_id), 
        "min_dimension_id" => 0, 
        "min_width" => 0, 
        "min_height" => 0, 
        "min_length" => 0, 
        "min_volume" => 0, 
        "max_dimension_id" => 0, 
        "max_width" => 0, 
        "max_height" => 0, 
        "max_length" => 0, 
        "max_volume" => 0, 
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),
        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),       
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );

      if(!$this->standard_rates->check_min_weight_based_standard_rates($insert_data) && !$this->standard_rates->check_max_weight_based_standard_rates($insert_data) ) {
        if($this->standard_rates->register_standard_rates($insert_data)){ $insert_flag = true; } 
        else { $insert_flag = false;   }  
      }
    }

    if($insert_flag){ $this->session->set_flashdata('success','Rate successfully added!'); }
    else{ $this->session->set_flashdata('error',array('error_msg' => 'Unable to add rates! Try again...', 'data' => $post_data));  }
    redirect('admin/standard-rates/add/volume-based');
  }

  public function register_formula_volume_based()
  {
    //echo json_encode($_POST); die();
    $categories = $this->input->post('category', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $currency_id = $this->input->post('currency_id', TRUE);
    $min_distance = $this->input->post('min_distance', TRUE);
    $max_distance = $this->input->post('max_distance', TRUE);

    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);

    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $currency = $this->standard_rates->get_currency_detail((int) $currency_id);
    $insert_flag = false;

    $post_data = array(
      "category_id" =>  $categories,
      "country_id" =>  (int)($country_id), 
      "currency_id" =>  (int)($currency_id), 
      "min_distance" =>  trim($min_distance), 
      "max_distance" =>  trim($max_distance), 
      "currency_sign" =>  trim($currency['currency_sign']), 
      "currency_title" =>  trim($currency['currency_title']),
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),

      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration),
    );

    foreach ($categories as $c) {
      $insert_data = array(
        "category_id" =>  (int)($c), 
        "country_id" =>  (int)($country_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),
        "min_distance" =>  trim($min_distance), 
        "max_distance" =>  trim($max_distance), 
        "min_dimension_id" => 0, 
        "min_width" =>  0, 
        "min_height" =>  0, 
        "min_length" =>  0, 
        "min_volume" =>  0, 
        "max_dimension_id" => 0,
        "max_width" =>  0, 
        "max_height" =>  0, 
        "max_length" =>  0, 
        "max_volume" =>  0, 
        "min_weight" =>  0, 
        "max_weight" =>  0, 
        "unit_id" =>  0, 
        
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
        "is_formula_volume_rate" => 1,
      );
      
      if(!$this->standard_rates->check_formula_min_volume_based_standard_rates($insert_data) && !$this->standard_rates->check_formula_max_volume_based_standard_rates($insert_data)) {      
        if($this->standard_rates->register_standard_rates($insert_data)){ $insert_flag = true; }
        else { $insert_flag = false; }                 
      } 
    }
    
    if($insert_flag){ $this->session->set_flashdata('success','Rate successfully added!'); }
    else{ $this->session->set_flashdata('error',array('error_msg' => 'Unable to add rates! Rate exists for given category and country.', 'data' => $post_data));  }

    redirect('admin/standard-rates/add/formula-volume-based');
  }

  public function register_formula_weight_based()
  {
    //echo json_encode($_POST); die();
    $categories = $this->input->post('category', TRUE); 
    $country_id = $this->input->post('country_id', TRUE);
    $currency_id = $this->input->post('currency_id', TRUE);
    $min_distance = $this->input->post('min_distance', TRUE);
    $max_distance = $this->input->post('max_distance', TRUE);
    $unit_id = $this->input->post('unit_id', TRUE); 
    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $currency = $this->standard_rates->get_currency_detail((int) $currency_id);
    $insert_flag = false;

    $post_data = array(
      "category_id" =>  $categories,
      "country_id" =>  (int)($country_id), 
      "currency_id" =>  (int)($currency_id), 
      "currency_sign" =>  trim($currency['currency_sign']), 
      "currency_title" =>  trim($currency['currency_title']), 
      "min_distance" =>  trim($min_distance), 
      "max_distance" =>  trim($max_distance), 
      "unit_id" => trim($unit_id),         
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),
      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration),       
      "cre_datetime" =>  date("Y-m-d H:i:s"),    
    );

    foreach ($categories as $c) {
      $insert_data = array(
        "category_id" =>  (int)($c), 
        "country_id" =>  (int)($country_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),
        "min_distance" =>  trim($min_distance), 
        "max_distance" =>  trim($max_distance), 
        "min_weight" => 0, 
        "max_weight" => 0,
        "unit_id" => trim($unit_id), 
        "min_dimension_id" => 0, 
        "min_width" => 0, 
        "min_height" => 0, 
        "min_length" => 0, 
        "min_volume" => 0, 
        "max_dimension_id" => 0, 
        "max_width" => 0, 
        "max_height" => 0, 
        "max_length" => 0, 
        "max_volume" => 0, 
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),
        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),       
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
        "is_formula_weight_rate" =>  1,    
      );

      if(!$this->standard_rates->check_formula_min_weight_based_standard_rates($insert_data) && !$this->standard_rates->check_formula_max_weight_based_standard_rates($insert_data)) {
        if($this->standard_rates->register_standard_rates($insert_data)){ $insert_flag = true; } 
        else { $insert_flag = false;   }  
      }
    }

    if($insert_flag){ $this->session->set_flashdata('success','Rate successfully added!'); }
    else{ $this->session->set_flashdata('error',array('error_msg' => 'Unable to add rates! Rate exists for given category and country.', 'data' => $post_data));  }
    redirect('admin/standard-rates/add/formula-weight-based');
  }

  public function edit()
  {   
    if( $id = $this->uri->segment(4) ) {
      $rates = $this->standard_rates->get_standard_rates($id);
      if($rates['is_formula_volume_rate'] == 0 && $rates['is_formula_weight_rate'] == 0):
        $this->load->view('admin/standard_rates_edit_view', compact('rates'));
      else:
        if($rates['is_formula_volume_rate'] == 1):
          $this->load->view('admin/formula_volume_based_rates_edit_view', compact('rates'));
        else:
          $this->load->view('admin/formula_weight_based_rates_edit_view', compact('rates'));
        endif;
      endif;
      $this->load->view('admin/footer_view');
    } else { redirect('admin/standard-rates'); }
  }

  public function update()
  {
    // echo json_encode($_POST); die();
    $rate_id = $this->input->post('rate_id', TRUE);   
    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);

    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $update_data = array( 
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),
      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration), 
    );

    if($this->standard_rates->update_standard_rates($update_data, $rate_id)){
      $this->session->set_flashdata('success','Rates successfully updated!');
    } else { $this->session->set_flashdata('error', 'Unable to update rates! Try again...');  }

    redirect('admin/standard-rates/edit/'.$rate_id);
  }

  public function delete()
  {
    $rates_id = $this->input->post('id', TRUE);
    if($this->standard_rates->delete_standard_rates($rates_id)){ echo 'success'; } else { echo "failed";  } 
  }
  
  public function get_unit_detail()
  {
    $unit_id = $this->input->post('unt');
    echo json_encode($this->standard_rates->get_unit_detail($unit_id));
  }

  // REMOVED

  // public function add()
  // {
  //  $countries_list = $this->standard_rates->get_countries();
  //  $unit_list = $this->standard_rates->get_unit_master(); 
  //  $this->load->view('admin/standard_rates_add_view', compact('countries_list','unit_list'));
  //  $this->load->view('admin/footer_view');
  // }

  // public function edit()
  // {  
  //  if( $id = $this->uri->segment(4) ) {      
  //    $rates = $this->standard_rates->get_standard_rates($id);
  //    $countries = $this->standard_rates->get_countries();
  //    $units = $this->standard_rates->get_unit_master(); 
  //    $this->load->view('admin/standard_rates_edit_view', compact('rates','countries','units'));
  //    $this->load->view('admin/footer_view');
  //  } else { redirect('admin/standard-rates'); }
  // }

  // public function update()
  // {
  //  $rate_id = $this->input->post('rate_id', TRUE);   
 //     $max_duration = $this->input->post('max_duration', TRUE);
 //     $rate = $this->input->post('rate', TRUE);

    

  //    $update_data = array( "max_duration_days" =>  trim($max_duration), "service_rate" =>  trim($rate));
 
  //  if($this->standard_rates->update_standard_rates($update_data, $rate_id)){
  //    $this->session->set_flashdata('success','rates successfully updated!');
  //  } else { $this->session->set_flashdata('error', 'Unable to update rates! Try again...');  } 
      

  //  redirect('admin/standard-rates/edit/'.$rate_id);
  // }
}

/* End of file Standard_rates.php */
/* Location: ./application/controllers/Standard_rates.php */