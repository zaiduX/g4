<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class profile_subscription extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('profile_subscription_model', 'subscription');
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
  
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else { redirect('admin');}
  }
  public function index() {
    $subscriptions = $this->subscription->get_subscription();
    $this->load->view('admin/services/profile_subscription_list_view', compact('subscriptions'));      
    $this->load->view('admin/footer_view');   
  }
  public function add() {
    $countries = $this->subscription->get_countries();
    $this->load->view('admin/services/profile_subscription_add_view', compact('countries'));
    $this->load->view('admin/footer_view');
  }
  public function get_country_currencies() {
    $country_id = $this->input->post('country_id', TRUE); $country_id = (int) $country_id;
    $currencies = $this->subscription->get_country_currencies($country_id);
    echo json_encode($currencies);
  }
  public function register() { 
    //echo json_encode($_POST); die();
    $currency_id = $this->input->post('currency_id', TRUE);
    $currency_code = $this->input->post('currency_code', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $subscription_title = $this->input->post('subscription_title', TRUE);
    $price = $this->input->post('price', TRUE);
    $proposal_credit = $this->input->post('proposal_credit', TRUE);
    $subscription_desc = $this->input->post('subscription_desc', TRUE);
    
    $insert_data = array(
      "subscription_title" => trim($subscription_title), 
      "subscription_desc" => trim($subscription_desc), 
      "proposal_credit" => (int)trim($proposal_credit), 
      "add_team_members" => 1, 
      "cre_datetime" =>  date("Y-m-d H:i:s"), 
      "status" => 1, 
      "update_datetime" =>  date("Y-m-d H:i:s"), 
      "country_id" => (int)trim($country_id), 
      "price" => trim($price), 
      "currency_id" => (int)trim($currency_id), 
      "currency_code" => trim($currency_code), 
      "proposal_forward" => 1,
    );
    //echo json_encode($insert_data); die();
    if($this->subscription->register_subscription($insert_data)) { $this->session->set_flashdata('success','Subscription successfully added!'); } 
    else { $this->session->set_flashdata('error','Unable to add subscription details!'); }
    redirect('admin/profile-subscription/add');
  }
  public function edit() { 
    if( $id = $this->uri->segment(4) ) {      
      if( $subscription = $this->subscription->get_subscription_detail($id)){
        $this->load->view('admin/services/profile_subscription_edit_veiw', compact('subscription'));
        $this->load->view('admin/footer_view');
      } else { redirect('admin/profile-subscription'); }
    } else { redirect('admin/profile-subscription'); }
  }
  public function update() { 
    //echo json_encode($_POST); die();
    $subscription_id = $this->input->post('subscription_id', TRUE);
    $subscription_title = $this->input->post('subscription_title', TRUE);
    $price = $this->input->post('price', TRUE);
    $proposal_credit = $this->input->post('proposal_credit', TRUE);
    $subscription_desc = $this->input->post('subscription_desc', TRUE);

    $update_data = array(       
      "subscription_title" => trim($subscription_title),
      "price" => trim($price),
      "proposal_credit" => trim($proposal_credit),
      "subscription_desc" => trim($subscription_desc),
    );
    
    if($this->subscription->update_subscription($update_data, $subscription_id)){
      $this->session->set_flashdata('success','Subscription details successfully updated!');
    } else { $this->session->set_flashdata('error','Unable to update subscription details! Try again...');  }
    redirect('admin/profile-subscription/edit/'.$subscription_id);
  }
  public function delete() {
    $id = $this->input->post('id', TRUE);
    if($this->subscription->delete_subscription($id)){ echo 'success'; } else { echo "failed";  } 
  }
}

/* End of file profile_subscription.php */
/* Location: ./application/controllers/profile_subscription.php */