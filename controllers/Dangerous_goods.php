<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dangerous_goods extends CI_Controller {

   public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('advance_payment_model', 'payment');
      $this->load->model('standard_dimension_model', 'standard_dimension');   
      $this->load->model('Web_user_model', 'user');   
      $this->load->model('dangerous_goods_model', 'goods');   
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $goods = $this->goods->get_dangerous_goods();
    $this->load->view('admin/dangerous_goods_list_view',compact('goods'));      
    $this->load->view('admin/footer_view');   
  }

  public function add()
  {
    $this->load->view('admin/dangerous_goods_add_view');
    $this->load->view('admin/footer_view');
  }

  public function register()
  {
    // echo json_encode($_POST); die();
    $name = $this->input->post('name', TRUE);
    $code = $this->input->post('code', TRUE);
    $description = $this->input->post('description', TRUE);

    if(!empty($_FILES["goods_icon"]["tmp_name"])) {
      $config['upload_path']    = 'resources/dangerous-goods/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('goods_icon')) {   
        $uploads    = $this->upload->data();  
        $image_url =  $config['upload_path'].$uploads["file_name"];  
      } 
    } else { $image_url = "NULL"; }

    $insert_data = array(
      "name" => trim($name), 
      "code" => $code, 
      "description" => nl2br($description), 
      "image_url" => $image_url,
      "cre_datetime" => date('Y-m-d H:i:s'),
    );

    if(!$this->goods->check_existance($insert_data) ) {
      if($this->goods->register_goods($insert_data)){
        $this->session->set_flashdata('success','Dangerous goods successfully added!');
      } else {  $this->session->set_flashdata('error','Unable to add Dangerous goods! Try again...');  } 
    } else {  $this->session->set_flashdata('error','Dangerous goods name / code already exists!');  }
    redirect('admin/dangerous-goods/add');
  }

  public function edit()
  {
    $id = $this->uri->segment(4);
    if( $goods = $this->goods->get_dangerous_goods($id)){
      $this->load->view('admin/dangerous_goods_edit_view',compact('goods'));
      $this->load->view('admin/footer_view');
    } else{ $this->session->set_flashdata('error','No Dangerous goods found!'); redirect('admin/dangerous-goods'); }
  }

  public function update()
  { //echo json_encode($_POST); die();
    $id = $this->input->post('id', TRUE);
    $name = $this->input->post('name', TRUE);
    $code = $this->input->post('code', TRUE);
    $description = $this->input->post('description', TRUE);

    if( $goods = $this->goods->get_dangerous_goods($id)){

      if(!empty($_FILES["goods_icon"]["tmp_name"])) {
        $config['upload_path']    = 'resources/dangerous-goods/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('goods_icon')) {   
          $uploads    = $this->upload->data();  
          $image_url =  $config['upload_path'].$uploads["file_name"];  
        } else { $image_url = $goods['image_url']; }
      } else { $image_url = $goods['image_url']; }

      $update_data = array(
        "name" => trim($name), 
        "code" => $code, 
        "description" => nl2br($description), 
        "image_url" => $image_url,
      );
// echo json_encode($update_data); die();
      if(!$this->goods->check_existance_for_update($update_data, $goods['id'])){
        if($this->goods->update_goods($update_data, $goods['id'])){
          $this->session->set_flashdata('success','Dangerous goods successfully added!');
        } else {  $this->session->set_flashdata('error','Unable to update Dangerous goods! Try again...');  } 
      } else {  $this->session->set_flashdata('error','Unable to update! Dangerous goods code / name already exists!');  }       
    } else{ $this->session->set_flashdata('error','Dangerous goods not found!'); }
    redirect('admin/dangerous-goods/edit/'.$id);
  }

  public function make_active()
  {
    $id = (int) $this->input->post('id', TRUE);
    if( $this->goods->update_goods(['status' => 1],$id) ) { echo 'success'; } else {  echo 'failed';  }
  }

  public function make_inactive()
  {
    $id = (int) $this->input->post('id', TRUE);
    if( $this->goods->update_goods(['status' => 0],$id) ) { echo 'success'; } else {  echo 'failed';  }
  }
}

/* End of file dangerous_goods.php */
/* Location: ./application/controllers/dangerous_goods.php */