<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		/*
		$uri = $this->uri->segment(1);
		$lang = isset($uri) ? $this->uri->segment(1) : "en";

		if( $lang == 'fr' ) {
			$this->config->set_item('lang', 'fr');
			$this->lang->load('frenchFront_lang','french');
		}
		else if( $lang == 'es' ) {
			$this->config->set_item('lang', 'es');
			$this->lang->load('spanishFront_lang','spanish');
		}
		else{
			$this->config->set_item('lang', 'en');
			$this->lang->load('englishFront_lang','english');
		}
		*/
		
		$this->load->model('api_model', 'api');
		
		//$this->config->set_item('lang', 'en');
		//$this->lang->load('englishApi_lang','english');
		//$this->lang->load('englishFront_lang','english');
        
        $lang = $this->session->userdata('language');
        if($lang){ $this->config->set_item('language', strtolower($lang)); }
        else{ $lang = "french"; $this->config->set_item('language', 'french'); } 
    
        if(trim(strtolower($lang)) == 'french'){
          $this->lang->load('frenchApi_lang','french');
          $this->lang->load('frenchFront_lang','french');
        } 
        else if(trim(strtolower($lang)) == 'spanish'){  
          $this->lang->load('spanishApi_lang','spanish');
          $this->lang->load('spanishFront_lang','spanish');
        }
        else { 
          $this->lang->load('englishApi_lang','english');
          $this->lang->load('englishFront_lang','english');
        }
        

		$this->load->view('front_header_view');

	}

	public function index()
	{
		
	}

	public function email_verification()
	{
		$hashcode =  $this->uri->segment(2);
		
		$check = $this->api->check_verified($hashcode);
		if($check['email_verified'] == 0){
			if($this->api->verify_email($hashcode)){
				$this->session->set_flashdata('success', 'Email successfully verified!');
			} else { $this->session->set_flashdata('error', 'Invalid link! Check your email.'); }
		} else { $this->session->set_flashdata('warning', 'Email already verified.'); }
		redirect('log-in');
	}

	public function email_verified()
	{
		
	}

	// public function otp()
	// {
	// 	$cust_id =  $this->session->userdata('cust_id');
 //    	$cust = $this->api->get_consumer_datails($cust_id,true);
 //    	$support_details = $this->api->get_gonagoo_address($cust['country_id']);
	// 	$this->load->view('verify_otp_view', compact('support_details'));
	// 	$this->load->view('front_footer_view');
	// }

	public function otp()
	{
		// echo json_encode($_POST);
		$cust_id =  90;
		$cust = $this->api->get_consumer_datails($cust_id,true);
		$support_details = $this->api->get_gonagoo_address($cust['country_id']);
		// echo json_encode($support_details);die();
		$support_details['job_id']=(isset($_POST))?$_POST['job_id']:"NULL";
		$this->load->view('verify_otp_view', compact('support_details'));
		$this->load->view('front_footer_view');
	}

	public function resend_otp()
	{
		$this->load->view('resend_otp_view');
		$this->load->view('front_footer_view');	
	}

	public function recovery_password()
	{
		$this->load->view('recovery_password_view');
		$this->load->view('front_footer_view');	
	}
	
	public function resend_verification_email()
	{
		$cust_id = $this->session->userdata('cust_id');
		$cust = $this->api->get_consumer_datails($cust_id,true);
				
		$activation_hash = trim($cust['activation_hash']);

		$subject = $this->lang->line('email_verification_title');

		$message = $this->lang->line('verify_email_text1').$cust['email1'].$this->lang->line('verify_email_text2');
    		$message .="</td></tr><tr><td align='center'><br />";
    		$message .="<a href='".$this->config->item('base_url')."verify-email/".$activation_hash."' class='callout'>".$this->lang->line('verify_email_btn')."</a>";				    

    		if($cust['firstname'] != "NULL"){   $username = trim($cust['firstname']) .' '.trim($cust['lastname']); }
		else{ $username = " User"; }

		// Send activation email to given email id
		$this->api->sendEmail($username, $cust['email1'], $subject, trim($message));
		
		redirect('user-panel/landing');
	}

}

/* End of file Verify.php */
/* Location: ./application/controllers/Verify.php */