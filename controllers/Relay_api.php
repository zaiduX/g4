<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relay_api extends CI_Controller {

  private $errors = array();

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Relay_api_model', 'api');
    $this->load->model('Notification_model', 'notification');
    $this->load->model('Users_model', 'users');
    $this->load->model('Api_model', 'mainapi');
    $this->load->model('api_model_sms', 'api_sms'); 
    header('Content-Type: Application/json'); 
    if( $this->input->method() != 'post' ) exit('No direct script access allowed');
    if( $this->input->is_ajax_request() ) exit('No direct script access allowed');
    if(isset($_POST)){
      $lang = $this->input->post('lang', TRUE);
      if( $lang == 'fr' ) {
        $this->config->set_item('language', 'french');
        $this->lang->load('frenchApi_lang','french');
      }
      else if( $lang == 'sp' ) {
        $this->config->set_item('language', 'spanish');
        $this->lang->load('spanishApi_lang','spanish');
      }
      else{
        $this->config->set_item('language', 'english');
        $this->lang->load('englishApi_lang','english');
      }

      foreach($_POST as $key => $value){ 
        if(trim($value) == "") { $this->errors[] = ucwords(str_replace('_', ' ', $key)); }        
      }
      if(!empty($this->errors)) { 
        echo json_encode(array('response' => 'failed', 'message' => 'Empty field(s) - '.implode(', ', $this->errors))); 
      }
    }
  }

  public function index() { exit('No direct script access allowed'); }

  /*
  public function app_params()
  {
    if(empty($this->errors)){ 
      $cd_id = $this->input->post('cd_id', TRUE);
      $app_ver = $this->input->post('app_ver', TRUE);
      $params = $this->api->get_application_parameters();
      
      if($cd_id > 0){ if( $this->api->is_driver_active($cd_id)){ $driver_active = "active"; } else { $driver_active = "inactive"; } }
      else { $driver_active = "NULL";}

      if($app_ver != $params->app_version) {
        if($params->forcefully_update == 1) {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('app_update_found'), 'forcefully_update' => 1 , "driver_active" => $driver_active];
        } else{ $this->response = ['response' => 'success', 'message' => $this->lang->line('app_update_no_force'), 'forcefully_update' => 0, "driver_active" => $driver_active ]; }
      }
      else {  $this->response = ['response' => 'success', 'message' => $this->lang->line('app_update_not_found'), 'force_update' => 0, "driver_active" => $driver_active]; }

      echo json_encode($this->response);
    }
  }

  public function login()
  {
    if(empty($this->errors)){ 
      $device_id = $this->input->post('device_id', TRUE);
      $email = $this->input->post('email', TRUE);
      $password = $this->input->post('password', TRUE);
      $device_type = $this->input->post('device_type', TRUE); $device_type = (int) $device_type; // 1 : android, 2: ios
      $reg_id = $this->input->post('reg_id', TRUE);

      if( (trim($email) != "NULL") AND ($user = $this->api->is_emailId_exists(trim($email))) ) {
        if( $cd_id = $this->api->validate_login(trim($email), trim($password)) ) {
          if( $this->api->is_driver_active(trim($cd_id)) ){ 
            if(!$this->api->is_deviceId_exists($device_id)) { 
              // insert into device table
              $device_insert_data = array(
                "device_id" => trim($device_id),
                "cd_id" => (int)($cd_id),
                "reg_id" => trim($reg_id),
                "device_type" => ($device_type == 2 ) ? 0: 1,
              );
              $this->api->register_new_device($device_insert_data);
            } 
            else {
              $device_update_data = array(
                "cd_id" => (int)($cd_id),
                "reg_id" => trim($reg_id),
                "device_type" => ($device_type == 2 ) ? 0: 1,
              );  
              // update device 
              $this->api->update_device_detail($device_id, $device_update_data);
            }
            // udpate last login 
            $this->api->update_last_login($cd_id);
            $this->response = ['response' => 'success','message'=> $this->lang->line('driver_login_success'), "user" => $user];
          } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_login_failed')]; }
      } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_email_not_exists')]; }
      echo json_encode($this->response);
    }
  }
  */

  public function forget_password()
  {
    if(empty($this->errors)) {  
      $email_id = $this->input->post('email_id', TRUE);
      if( $driver = $this->api->is_emailId_exists($email_id)) {
        $subject = $this->lang->line('driver_forget_pass_title');
        $message ="<p class='lead'>".$this->lang->line('driver_forget_pass_msg1')."</p>";
        $message .="</td></tr><tr><td align='center'><br />";
        $message .="<a class='callout'>".$this->lang->line('driver_forget_pass_msg2')."<strong>".$driver['password']."</strong></a>";
        $username = trim($driver['first_name']) . trim($driver['last_name']);
        // Send activation email to given email id
        if( $this->api_sms->send_email_text($username, $email_id, $subject, trim($message))){
          $sms_msg = $this->lang->line('driver_forget_pass_msg2') . $driver['password'];
          if(substr($driver['mobile1'], 0, 1) == 0) { $mobile1 = ltrim($driver['mobile1'], 0); } else { $mobile1 = $driver['mobile1']; }
          $this->api->sendSMS(trim($driver['country_code']).trim($mobile1), $sms_msg);
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_credential_success') ];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_credential_failed')]; }
        }  else{  $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_not_exists')]; }
        echo json_encode($this->response);
      }
  }

  public function change_password()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $password = $this->input->post('password', TRUE);
      $new_password = $this->input->post('new_password', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        if($this->api->is_driver_password_match($cd_id, $password)){
          $update_data = array(
            "password" => trim($new_password),
          );
          if( $this->api->update_password($cd_id, $update_data) ){
            $this->response = ['response' => 'success','message'=>$this->lang->line('driver_pass_update_success')];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_pass_update_failed')]; }
          $this->api->update_last_login($cd_id);// update user last login
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_old_pass_match')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function get_driver_profile()
  {
    if(empty($this->errors)){ 
      $cd_id = $this->input->post('cd_id', TRUE);
      $driver_profile_details = $this->api->get_profile_details($cd_id);
      if(empty($driver_profile_details)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_details_failed'), 'profile_details' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_details_success'), 'profile_details' => $driver_profile_details]; 
      }
      echo json_encode($this->response);
    }
  }

  public function update_driver_profile()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $first_name = $this->input->post('first_name', TRUE);
      $last_name = $this->input->post('last_name', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $mobile2 = $this->input->post('mobile2', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        $update_data = array(
          "first_name" => trim($first_name),
          "last_name" => trim($last_name),
          "mobile1" => trim($mobile1),
          "mobile2" => trim($mobile2),
          "mod_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_profile_details($cd_id, $update_data) ){
          $driver = $this->api->get_profile_details(trim($cd_id));
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_driver_update_success'), "relay" => $driver];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_driver_update_failed')]; }
        $this->api->update_last_login($cd_id);// update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function update_driver_avatar()
  {
    if(empty($this->errors)) {
      $cd_id = $this->input->post('cd_id', TRUE);
      if($this->api->is_driver_active($cd_id)){
        if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('profile_image')) { 
            $uploads    = $this->upload->data();
            $avatar_url =  $config['upload_path'].$uploads["file_name"];
            $this->api->delete_old_profile_image($cd_id);
          }
          if( $this->api->update_profile_image($avatar_url, $cd_id)){
            $driver = $this->api->get_profile_details(trim($cd_id));
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('driver_avatar_success'), "relay" => $driver];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_avatar_failed')]; }
        } 
        // update user last login
        $this->api->update_last_login($cd_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function driver_online_status()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $online_status = (int)$this->input->post('online_status', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        $update_data = array(
          "online_status" => trim($online_status),
          "last_online_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_online_status($cd_id, $update_data) ){
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_online_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_online_failed')]; }
        $this->api->update_last_login($cd_id);// update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function driver_location_update()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        $update_data = array(
          "latitude" => trim($latitude),
          "longitude" => trim($longitude),
          "loc_update_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_driver_location($cd_id, $update_data) ){
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_location_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_location_failed')]; }
        $this->api->update_last_login($cd_id);// update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function get_relay_point_details()
  {
    if(empty($this->errors)){ 
      $cd_id = $this->input->post('cd_id', TRUE);
      $relay_point_details = $this->api->get_relay_point_details($cd_id);
      if(empty($relay_point_details)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_details_failed'), 'relay_point_details' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_details_success'), 'relay_point_details' => $relay_point_details]; 
      }
      echo json_encode($this->response);
    }
  }

  public function update_relay_point()
  {
    if(empty($this->errors)) {  
      $relay_id = $this->input->post('relay_id', TRUE);
      $cd_id = $this->input->post('cd_id', TRUE);
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $email = $this->input->post('email', TRUE);
      $mobile_no = $this->input->post('mobile_no', TRUE);
      $comapny_name = $this->input->post('comapny_name', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      $city_id = $this->input->post('city_id', TRUE);
      $zipcode = $this->input->post('zipcode', TRUE);
      $street_name = $this->input->post('street_name', TRUE);
      $addr_line1 = $this->input->post('addr_line1', TRUE);
      $addr_line2 = $this->input->post('addr_line2', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      $deliver_instructions = $this->input->post('deliver_instructions', TRUE);
      $pickup_instructions = $this->input->post('pickup_instructions', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        $update_data = array(
          "firstname" => trim($firstname),
          "lastname" => trim($lastname),
          "email" => trim($email),
          "mobile_no" => trim($mobile_no),
          "comapny_name" => trim($comapny_name),
          "country_id" => trim($country_id),
          "state_id" => (int)trim($state_id),
          "city_id" => (int)trim($city_id),
          "zipcode" => (int)trim($zipcode),
          "street_name" => trim($street_name),
          "addr_line1" => trim($addr_line1),
          "addr_line2" => trim($addr_line2),
          "latitude" => trim($latitude),
          "longitude" => trim($longitude),
          "deliver_instructions" => trim($deliver_instructions),
          "pickup_instructions" => trim($pickup_instructions),
        );

        if( $this->api->update_relay_details($relay_id, $update_data) ){
          $relay = $this->api->get_relay_point_details(trim($cd_id));
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_driver_update_success'), "relay" => $relay];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_driver_update_failed')]; }
        $this->api->update_last_login($cd_id);// update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function update_relay_avatar()
  {
    if(empty($this->errors)) {
      $relay_id = $this->input->post('relay_id', TRUE);
      $relay_mgr_id = $this->input->post('relay_mgr_id', TRUE);
      if($this->api->is_driver_active($relay_mgr_id)){
        if( !empty($_FILES["image_url"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('image_url')) { 
            $uploads    = $this->upload->data();
            $avatar_url =  $config['upload_path'].$uploads["file_name"];
            $this->api->delete_old_relay_image($relay_id);
          }
          if( $this->api->update_relay_image($avatar_url, $relay_id)){
            $relay = $this->api->get_relay_point_details(trim($relay_mgr_id));
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('driver_avatar_success'), "relay" => $relay];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_avatar_failed')]; }
        } 
        // update user last login
        $this->api->update_last_login($cd_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function update_relay_point_status()
  {
    if(empty($this->errors)) {  
      $relay_id = $this->input->post('relay_id', TRUE);
      $relay_mgr_id = $this->input->post('relay_mgr_id', TRUE);
      $status = $this->input->post('status', TRUE);
      
      if($this->api->is_driver_active($relay_mgr_id)){
        if( $this->api->update_relay_status($relay_id, $status) ){
          $relay = $this->api->get_relay_point_details(trim($relay_mgr_id));
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_status_update_success'), "relay" => $relay];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_status_update_failed')]; }
        $this->api->update_last_login($relay_mgr_id);// update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function get_relay_warehouse()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE);
      $relay_point_warehouse = $this->api->get_relay_warehouse($relay_id);
      if(empty($relay_point_warehouse)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed'), 'relay_point_warehouse' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'relay_point_warehouse' => $relay_point_warehouse]; 
      }
      echo json_encode($this->response);
    }
  }

  public function add_relay_warehouse()
  {
    if(empty($this->errors)){
      $relay_id = $this->input->post('relay_id', TRUE);
      $relay_mgr_id = $this->input->post('relay_mgr_id', TRUE);
      $name = $this->input->post('name', TRUE);
      $height = $this->input->post('height', TRUE);
      $width = $this->input->post('width', TRUE);
      $length = $this->input->post('length', TRUE);
      $max_volume = round($height*$width*$length, 0);

      $insert_data = array(
        "relay_id" => (int)trim($relay_id),
        "name" => trim($name),
        "height" => trim($height),
        "width" => trim($width),
        "length" => trim($length),
        "max_volume" => trim($max_volume),
        "volume_used" => 0,
        "cre_datetime" => date('Y-m-d h:i:s'),
        "status" => 1,
      );

      if( $rpw_id = $this->api->register_warehouse($insert_data) ) {
        $relay_point_details = $this->api->get_relay_point_details($relay_mgr_id);
        $wh_count = (int) $relay_point_details['wh_count'] + 1;
        $this->api->update_relay_warehouse_count($relay_id, $wh_count);

        $warehouse_details = $this->api->get_warehouse_details($rpw_id);
        $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_status_add_success'), "warehouse_details" => $warehouse_details];
      } else {  $this->response = ['response' => 'failed', 'message'=>$this->lang->line('driver_status_add_failed'), "warehouse_details" => array()]; 
      }
      echo json_encode($this->response);
    }
  }

  public function edit_relay_warehouse()
  {
    if(empty($this->errors)) {  
      $rpw_id = $this->input->post('rpw_id', TRUE);
      $name = $this->input->post('name', TRUE);
      $height = $this->input->post('height', TRUE);
      $width = $this->input->post('width', TRUE);
      $length = $this->input->post('length', TRUE);
      $max_volume = round($height*$width*$length, 0);
      
      $update_data = array(
        "name" => trim($name),
        "height" => trim($height),
        "width" => trim($width),
        "length" => trim($length),
        "max_volume" => trim($max_volume),
      );

      if( $this->api->update_relay_warehouse($rpw_id, $update_data) ){
        $warehouse = $this->api->get_warehouse_details(trim($rpw_id));
        $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_status_update_success'), "warehouse" => $warehouse];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_status_update_failed')]; }
      echo json_encode($this->response);
    }
  }
  
  public function relay_order_list()
  {
    if(empty($this->errors)){ 
      $last_id = $this->input->post('last_id', TRUE);
      $relay_id = $this->input->post('relay_id', TRUE);
      $relay_order = $this->api->get_order_list($last_id, $relay_id);
      if(empty($relay_order)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed'), 'relay_order' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'relay_order' => $relay_order]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function get_customer_order_using_code_old()
  {
    if(empty($this->errors)){ 
      $delivery_code = $this->input->post('delivery_code', TRUE);
      $relay_id = $this->input->post('relay_id', TRUE);
      $code = substr($delivery_code, 0, 2);
      if($code == 'DC') {
        $order_details = $this->api->get_order_details_customer($delivery_code, $relay_id);
        $warehouse_id = $this->api->get_relay_point_warehouse_id($order_details['order_id'], $relay_id);
        $warehouse_details = $this->api->get_warehouse_details($warehouse_id);
        if(!is_null($warehouse_id)) {
          $id = array("rpw_id" => $warehouse_id, "volume_used" => $warehouse_details['volume_used']);
        } else {
          $id = array("rpw_id" => "NULL", "volume_used" => "0");
        }
        $order_detail = array_merge($order_details, $id);

        if(empty($order_detail)) {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed')]; 
        } else {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'order_details' => $order_detail]; 
        }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('not_a_delivery_code')]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function get_customer_order_using_code()
  {
    if(empty($this->errors)){ 
      $delivery_code = $this->input->post('delivery_code', TRUE);
      $relay_id = $this->input->post('relay_id', TRUE);
      $code = substr($delivery_code, 0, 2);
      if($code == 'DC') {
        $order_details = $this->api->get_order_details_customer($delivery_code, $relay_id);
        if(!empty($order_details)) {
          $warehouse_id = $this->api->get_relay_point_warehouse_id($order_details['order_id'], $relay_id);
          $warehouse_details = $this->api->get_warehouse_details($warehouse_id);
          if(!is_null($warehouse_id)) {
            $id = array("rpw_id" => $warehouse_id, "volume_used" => $warehouse_details['volume_used']);
          } else {
            $id = array("rpw_id" => "NULL", "volume_used" => "0");
          }
          $order_detail = array_merge($order_details, $id);
          
          $packages = $this->api->get_order_packages($order_detail['order_id']);
          for ($j=0; $j < sizeof($packages); $j++){
            if(!is_null($this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'])) {
            $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
            } else {
              $packages[$j]['dangrous_goods_name'] = 'NULL';
            }
          }
          $order_detail += ['packages' => $packages];
          
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'order_details' => $order_detail]; 
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('not_a_delivery_code')]; }
      echo json_encode($this->response);
    }
  }
  
  public function get_driver_order_using_code_old()
  {
    if(empty($this->errors)){ 
      $driver_code = $this->input->post('driver_code', TRUE);
      $relay_id = $this->input->post('relay_id', TRUE);
      $code = substr($driver_code, 0, 2);
      if($code == 'CD') {
        $order_details = $this->api->get_order_details_driver($driver_code, $relay_id);
        $warehouse_id = $this->api->get_relay_point_warehouse_id($order_details['order_id'], $relay_id);
        $warehouse_details = $this->api->get_warehouse_details($warehouse_id);
        if(!is_null($warehouse_id)) {
          $id = array("rpw_id" => $warehouse_id, "volume_used" => $warehouse_details['volume_used']);
        } else {
          $id = array("rpw_id" => "NULL", "volume_used" => "0");
        }
        $order_detail = array_merge($order_details, $id);
        
        $packages = $this->api->get_order_packages($order_detail['order_id']);
        for ($j=0; $j < sizeof($packages); $j++){
          if(!is_null($this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'])) {
          $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
          } else {
              $packages[$j]['dangrous_goods_name'] = 'NULL';
          }
        }
        $order_detail += ['packages' => $packages];    
        
        if(empty($order_detail)) {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed')]; 
        } else {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'order_details' => $order_detail]; 
        }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('not_a_driver_code')]; }
      echo json_encode($this->response);
    }
  }
  
  public function get_driver_order_using_code()
  {
    if(empty($this->errors)){ 
      $driver_code = $this->input->post('driver_code', TRUE);
      $relay_id = $this->input->post('relay_id', TRUE);
      $code = substr($driver_code, 0, 2);
      if($code == 'CD') {
        $order_details = $this->api->get_order_details_driver($driver_code, $relay_id);

        if(!empty($order_details)) {
          $warehouse_id = $this->api->get_relay_point_warehouse_id($order_details['order_id'], $relay_id);
          $warehouse_details = $this->api->get_warehouse_details($warehouse_id);
          if(!is_null($warehouse_id)) {
            $id = array("rpw_id" => $warehouse_id, "volume_used" => $warehouse_details['volume_used']);
          } else {
            $id = array("rpw_id" => "NULL", "volume_used" => "0");
          }
          $order_detail = array_merge($order_details, $id);
          $packages = $this->api->get_order_packages($order_detail['order_id']);
          for ($j=0; $j < sizeof($packages); $j++){
            if(!is_null($this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'])) {
            $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
            } else {
              $packages[$j]['dangrous_goods_name'] = 'NULL';
            }
          }
          $order_detail += ['packages' => $packages]; 
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'order_details' => $order_detail]; 
        } else {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed')]; 
        }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('not_a_driver_code')]; }
      echo json_encode($this->response);
    }
  }

  public function manager_order_status_update()
  {
    if(empty($this->errors)) { 
      $cd_id = (int) $this->input->post('cd_id', TRUE);
      $order_id = (int) $this->input->post('order_id', TRUE);
      $order_status = $this->input->post('order_status', TRUE); // src_relay_in/src_relay_out/dest_relay_in/dest_relay_out
      $cust_id = (int) $this->input->post('cust_id', TRUE);
      $deliverer_id = (int) $this->input->post('deliverer_id', TRUE);
      $customer_fcm_ids = array();
      $customer_apn_ids = array();
      $order_details = $this->api->get_order_detail($order_id);

      $relay_id = (int) $this->input->post('relay_id', TRUE);
      $rpw_id = (int) $this->input->post('rpw_id', TRUE);
      $volume_used = (int) $this->input->post('volume_used', TRUE);
      $order_total_volume = (int) $this->input->post('order_total_volume', TRUE);

      //for warehouse assign-----------------------------------------------------
      $relay_mgr_id = (int) $this->input->post('relay_mgr_id', TRUE);
      $order_width = $this->input->post('order_width', TRUE);
      $order_height = $this->input->post('order_height', TRUE);
      $order_length = $this->input->post('order_length', TRUE);
      $order_total_quantity = $this->input->post('order_total_quantity', TRUE);
      //-------------------------------------------------------------------------

      if( $this->api->update_order_status($cd_id, $order_id, $order_status) ) {
        $this->api->insert_order_status($cd_id, $order_id, $order_status);
        
        //Get customer and deliverer contact and personal details
        $customer_details = $this->api->get_user_details($cust_id);
        $deliverer_details = $this->api->get_user_details($deliverer_id);

        //Get Users device Details
        $device_details_customer = $this->api->get_user_device_details($cust_id);
        //Get Customer Device Reg ID's
        $arr_customer_fcm_ids = array();
        $arr_customer_apn_ids = array();
        foreach ($device_details_customer as $value) {
          if($value['device_type'] == 1) {
            array_push($arr_customer_fcm_ids, $value['reg_id']);
          } else {
            array_push($arr_customer_apn_ids, $value['reg_id']);
          }
        }
        $customer_fcm_ids = $arr_customer_fcm_ids;
        $customer_apn_ids = $arr_customer_apn_ids;
        //Get Deliverer Device ID's
        $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
        $arr_deleverer_fcm_ids = array();
        $arr_deleverer_apn_ids = array();
        foreach ($device_details_deleverer as $value) {
          if($value['device_type'] == 1) {
            array_push($arr_deleverer_fcm_ids, $value['reg_id']);
          } else {
            array_push($arr_deleverer_apn_ids, $value['reg_id']);
          }
        }
        $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
        $deleverer_apn_ids = $arr_deleverer_apn_ids;
        //Get PN API Keys and PEM files
        $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
        //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
        
        //Send Notifications as per order status
        if ( trim($order_status) == 'src_relay_in' ) {
          $update_data = array(
            "rpw_id" => trim($rpw_id),
            "relay_id" => trim($relay_id),
            "relay_mgr_id" => trim($relay_mgr_id),
            "order_id" => trim($order_id),
            "order_width" => trim($order_width),
            "order_height" => trim($order_height),
            "order_length" => trim($order_length),
            "order_total_quantity" => trim($order_total_quantity),
          );
          $assign_response = $this->assign_warehouse($update_data);
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('recived_at_relay_src_msg');
          
          $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);
          
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg);
          
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear') . trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('recived_at_relay_src_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username_customer = trim($customer_details['firstname']) . trim($deliverer_details['lastname']);
          $username_deliverer = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1_customer = trim($customer_details['email1']);
          $email1_deliverer = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username_customer, $email1_customer, $subject, trim($message));
          $this->api_sms->send_email_text($username_deliverer, $email1_deliverer, $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject, trim($message));
          
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          $this->api->sendFCM($msg, $customer_fcm_ids, $api_key_deliverer);
          //Push Notification APN
          $msg_apn_deliverer =  array('title' => $subject, 'text' => $sms_msg);
          if(is_array($deleverer_apn_ids)) { 
            $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
          }
          
          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
        
          echo $assign_response;
        } 

        if ( trim($order_status) == 'dest_relay_in' ) {
          $update_data = array(
            "rpw_id" => trim($rpw_id),
            "relay_id" => trim($relay_id),
            "relay_mgr_id" => trim($relay_mgr_id),
            "order_id" => trim($order_id),
            "order_width" => trim($order_width),
            "order_height" => trim($order_height),
            "order_length" => trim($order_length),
            "order_total_quantity" => trim($order_total_quantity),
          );
          $assign_response = $this->assign_warehouse($update_data);
          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('recived_at_relay_dest_msg');

          $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);
          
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg);

          //Email confirmation
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear') . trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('recived_at_relay_dest_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username_customer = trim($customer_details['firstname']) . trim($deliverer_details['lastname']);
          $username_deliverer = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1_customer = trim($customer_details['email1']);
          $email1_deliverer = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username_customer, $email1_customer, $subject, trim($message));
          $this->api_sms->send_email_text($username_deliverer, $email1_deliverer, $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject, trim($message));
          //Push Notification FCM
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          $this->api->sendFCM($msg, $customer_fcm_ids, $api_key_deliverer);
          //Push Notification APN
          $msg_apn_deliverer =  array('title' => $subject, 'text' => $sms_msg);
          if(is_array($deleverer_apn_ids)) { 
            $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
          }
          
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
          echo $assign_response;
        } 

        if ( trim($order_status) == 'src_relay_out' ) {
  
          $this->api->out_from_warehouse($relay_id, $order_id); // set out datetime in database
          // recalculate warehouse space
          $current_volume_used = $volume_used - $order_total_volume; // warehouse volume used - order total volume
          $this->api->update_warehouse_volume_used($rpw_id, $current_volume_used); // update warehouse volume used

          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('driver_inprogress_order_msg');
          
          $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);
          
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg);

          //Email confirmation deliverer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_inprogress_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          //Email confirmation Customer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($customer_details['firstname']) . " " . trim($customer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_inprogress_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
          $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject, trim($message));
          //Push Notification FCM
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          $this->api->sendFCM($msg, $customer_fcm_ids, $api_key_deliverer);
          $this->api->sendFCM($msg, $deleverer_fcm_ids, $api_key_deliverer);
          //Push Notification APN
          $msg_apn_customer =  array('title' => $subject, 'text' => $sms_msg);
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          if(is_array($deleverer_apn_ids)) { 
            $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $deleverer_apn_ids);
          }
                
          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
          
          $order_details = $this->api->get_order_detail($order_id);
          if(trim($order_details['is_bank_payment']) == 0) {
            //Payment Module-------------------------------------------------------------------------------------
              $today = date('Y-m-d h:i:s');
              
              if(trim($order_details['pickup_payment']) > 0) {
                //deduct pickup_payment details from gonagoo account master and history
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($order_details['pickup_payment']);
                $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$deliverer_id,
                  "type" => 0,
                  "transaction_type" => 'pickup_payment',
                  "amount" => trim($order_details['pickup_payment']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => 'NULL',
                  "transfer_account_number" => 'NULL',
                  "bank_name" => 'NULL',
                  "account_holder_name" => 'NULL',
                  "iban" => 'NULL',
                  "email_address" => 'NULL',
                  "mobile_number" => 'NULL',
                  "transaction_id" => 'NULL',
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

                //Add pickup_payment details in deliverer account master and history
                $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($order_details['pickup_payment']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $deliverer_id, $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));

                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$deliverer_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'pickup_payment',
                  "amount" => trim($order_details['pickup_payment']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              
                //Deduct pickup amount from scrow and history
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));
                $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) - trim($order_details['pickup_payment']);
                $this->api->update_payment_in_deliverer_scrow((int)$deliverer_scrow_master_details['scrow_id'], $deliverer_id, $scrow_balance);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));

                $update_data_scrow_history = array(
                  "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                  "order_id" => (int)$order_id,
                  "deliverer_id" => (int)$deliverer_id,
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'pickup_payment',
                  "amount" => trim($order_details['pickup_payment']),
                  "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_scrow_history($update_data_scrow_history);
              }

              //relay point commission---------------------------------------------------------------------------------------
              //deduct commission amount from gonagoo account master and history
              $relay_point_details = $this->api->get_relay_point_details($cd_id);
              $commission_percent = trim($relay_point_details['relay_commission']);
              $relay_commission_amount = round((trim($order_details['commission_amount'])/100)*trim($commission_percent),2);
              $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($relay_commission_amount);
              $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$relay_id,
                "type" => 0,
                "transaction_type" => 'relay_commission',
                "amount" => trim($relay_commission_amount),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

              //Add commission amount in relay account master and history
              //Check for new currency in account and add new currency row in account table
              if($this->api->relay_account_master_details($relay_id, trim($order_details['currency_sign']))) {
                $relay_account_master_details = $this->api->relay_account_master_details($relay_id, trim($order_details['currency_sign']));
              } else {
                $insert_data_relay_master = array(
                  "user_id" => (int)$relay_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $this->api->insert_relay_master_record($insert_data_relay_master);
              }
              $relay_account_master_details = $this->api->relay_account_master_details($relay_id, trim($order_details['currency_sign']));
              $account_balance = trim($relay_account_master_details['account_balance']) + trim($relay_commission_amount);
              $this->api->update_payment_in_relay_master($relay_account_master_details['account_id'], $relay_id, $account_balance);
              $relay_account_master_details = $this->api->relay_account_master_details($relay_id, trim($order_details['currency_sign']));

              $update_data_account_history = array(
                "account_id" => (int)$relay_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$relay_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'relay_commission',
                "amount" => trim($relay_commission_amount),
                "account_balance" => trim($relay_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_relay_account_history($update_data_account_history);

              //send commission invoice to Gonagoo--------------------
                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference("$order_id");
                $invoice->setDate(date('M dS ,Y',time()));
                
                $relay_country = $this->api->get_country_details(trim($relay_point_details['country_id']));
                $relay_state = $this->api->get_state_details(trim($relay_point_details['state_id']));
                $relay_city = $this->api->get_city_details(trim($relay_point_details['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($relay_point_details['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array(trim($relay_point_details['firstname']) . " " . trim($relay_point_details['lastname']),trim($relay_city['city_name']),trim($relay_state['state_name']),trim($relay_country['country_name']),trim($relay_point_details['email'])));

                $invoice->setFrom(array($gonagoo_address['company_name'], $gonagoo_city['city_name'], $gonagoo_address['no_street_name'], $gonagoo_country['country_name'], $gonagoo_address['email']));

                /* Adding Items in table */
                $order_for = $this->lang->line('gonagoo_commission');
                $rate = round(trim($relay_commission_amount),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
                
                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('commission_paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                //$invoice->addTitle("Other Details");
                /* Add Paragraph */
                //$invoice->addParagraph("Any Other Details");
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($order_id.'_relay_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

                //Update File path
                $pdf_name = $order_id.'_relay_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/order-invoices/".$pdf_name);
                
                //Update Order Invoice and post to workroom
                $this->api->update_src_relay_commission_invoice_url($order_id, "order-invoices/".$pdf_name);
              /* ----------------------------------------------------- */
              /* -----------------Email Invoice to customer--------------------- */
                $eol = PHP_EOL;
                $messageBody = $eol . $this->lang->line('invoice_email_message') . $eol;
                $file = "resources/order-invoices/".$pdf_name;
                $file_size = filesize($file);
                $handle = fopen($file, "r");
                $content = fread($handle, $file_size);
                fclose($handle);
                $content = chunk_split(base64_encode($content));
                $uid = md5(uniqid(time()));
                $name = basename($file);
                //$eol = PHP_EOL;
                $from_mail = $this->config->item('from_email');
                $replyto = trim($relay_point_details['email']);
                // Basic headers
                $header = "From: Gonagoo <".$from_mail.">".$eol;
                $header .= "Reply-To: ".$replyto.$eol;
                $header .= "MIME-Version: 1.0\r\n";
                $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";

                // Put everything else in $message
                $message = "--".$uid.$eol;
                $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
                $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
                $message .= $messageBody.$eol;
                $message .= "--".$uid.$eol;
                $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
                $message .= "Content-Transfer-Encoding: base64".$eol;
                $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
                $message .= $content.$eol;
                $message .= "--".$uid."--";

                mail($gonagoo_address['email'], $this->lang->line('commission_invoice_email_subject'), $message, $header);
              /* -----------------------Email Invoice to customer----------------------- */

              //post payment in workroom
              $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('pickup_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
            //Payment Module End --------------------------------------------------------------------------------
          }         
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_status_update_success')];
          echo json_encode($this->response);
        } 

        if ( trim($order_status) == 'dest_relay_out' ) {
          $this->api->out_from_warehouse($relay_id, $order_id); // set out datetime in database
          // recalculate warehouse space
          $current_volume_used = $volume_used - $order_total_volume; // warehouse volume used - order total volume
          $this->api->update_warehouse_volume_used($rpw_id, $current_volume_used); // update warehouse volume used

          $this->api->insert_order_status_deliverer($deliverer_id, $order_id, 'delivered');
          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('driver_delivered_order_msg');
      
          $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);
          
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg);

          //Email confirmation deliverer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_delivered_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          //Email confirmation Customer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($customer_details['firstname']) . " " . trim($customer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_delivered_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
          $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject, trim($message));
          //Push Notification FCM
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          $this->api->sendFCM($msg, $customer_fcm_ids, $api_key_deliverer);
          $this->api->sendFCM($msg, $deleverer_fcm_ids, $api_key_deliverer);
          //Push Notification APN
          $msg_apn_customer =  array('title' => $subject, 'text' => $sms_msg);
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          if(is_array($deleverer_apn_ids)) { 
            $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $deleverer_apn_ids);
          }
          
          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');

          $order_details = $this->api->get_order_detail($order_id);
          //Generate Invoice and Email to Customer----------------
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference("$order_id");
            $invoice->setDate(date('M dS ,Y',time()));
            
            $customer_country = $this->api->get_country_details(trim($customer_details['country_id']));
            $customer_state = $this->api->get_state_details(trim($customer_details['state_id']));
            $customer_city = $this->api->get_city_details(trim($customer_details['city_id']));
            $deliverer_country = $this->api->get_country_details(trim($deliverer_details['country_id']));
            $deliverer_state = $this->api->get_state_details(trim($deliverer_details['state_id']));
            $deliverer_city = $this->api->get_city_details(trim($deliverer_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($deliverer_details['country_id']));

            $invoice->setFrom(array(trim($customer_details['firstname']) . " " . trim($customer_details['lastname']),trim($customer_city['city_name']),trim($customer_state['state_name']),trim($customer_country['country_name']),trim($customer_details['email1'])));

            $invoice->setTo(array(trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']),trim($deliverer_city['city_name']),trim($deliverer_state['state_name']),trim($deliverer_country['country_name']),trim($deliverer_details['email1'])));
            
            /* Adding Items in table */
            $order_for = $this->lang->line('courier_delivery');
            $rate = round($order_details['advance_payment'] + $order_details['pickup_payment'] + $order_details['deliver_payment'],2);
            $total = $rate;
            $payment_datetime = substr($order_details['payment_datetime'], 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('payment_paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            //$invoice->addTitle("Other Details");
            /* Add Paragraph */
            //$invoice->addParagraph("Any Other Details");
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch')."\r\n".$deliverer_details['firstname'].' '.$deliverer_details['lastname']."\r\n".$this->lang->line('telephone').$deliverer_details['mobile1']."\r\n".$this->lang->line('email').$deliverer_details['email1']."");
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($order_id.'.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $order_id.'.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/order-invoices/".$pdf_name);
            //Update Order Invoice and post to workroom
            $this->api->update_order_invoice_url($order_id, "order-invoices/".$pdf_name);
          /* ----------------------------------------------------- */
          /* -----------------Email Invoice to customer----------- */
            $eol = PHP_EOL;
            $messageBody = $eol . $this->lang->line('invoice_email_message') . $eol;
            $file = "resources/order-invoices/".$pdf_name;
            $file_size = filesize($file);
            $handle = fopen($file, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
            $uid = md5(uniqid(time()));
            $name = basename($file);
            //$eol = PHP_EOL;
            $from_mail = $this->config->item('from_email');
            $replyto = $deliverer_details['email1'];
            // Basic headers
            $header = "From: ". $deliverer_details['firstname'].' '.$deliverer_details['lastname'] ." <".$from_mail.">".$eol;
            $header .= "Reply-To: ".$replyto.$eol;
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";
            // Put everything else in $message
            $message = "--".$uid.$eol;
            $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
            $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
            $message .= $messageBody.$eol;
            $message .= "--".$uid.$eol;
            $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
            $message .= "Content-Transfer-Encoding: base64".$eol;
            $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
            $message .= $content.$eol;
            $message .= "--".$uid."--";
            mail($customer_details['email1'], $this->lang->line('invoice_email_subject'), $message, $header); 
          /* -----------------------Email Invoice to customer----- */
          //Push Notification FCM
          $subject = $this->lang->line('invoice_email_subject');
          $msg =  array('title' => $subject, 'type' => 'raise_invoice', 'notice_date' => date("Y-m-d H:i:s"), 'attachment_url' => "order-invoices/".$pdf_name, "pdf");
          $this->api->sendFCM($msg, $customer_fcm_ids, $api_key_deliverer);
          //APN
          $msg_apn_customer =  array('title' => $subject, 'text' => 'Invoice raised');
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, 'raise_invoice', 'sp', 'raise_invoice', 'order-invoices/'.$pdf_name, trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'pdf', 'NULL', 'NULL');

          if(trim($order_details['is_bank_payment']) == 0) {
            //Payment Module-------------------------------------------------------------------------------------
              $today = date('Y-m-d h:i:s');
              if(trim($order_details['deliver_payment']) > 0) {
                //deduct deliver_payment details from gonagoo account master and history
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($order_details['deliver_payment']);
                $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$deliverer_id,
                  "type" => 0,
                  "transaction_type" => 'deliver_payment',
                  "amount" => trim($order_details['deliver_payment']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => 'NULL',
                  "transfer_account_number" => 'NULL',
                  "bank_name" => 'NULL',
                  "account_holder_name" => 'NULL',
                  "iban" => 'NULL',
                  "email_address" => 'NULL',
                  "mobile_number" => 'NULL',
                  "transaction_id" => 'NULL',
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

                //Add deliver_payment details in deliverer account master and history
                $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($order_details['deliver_payment']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $deliverer_id, $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));

                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$deliverer_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'deliver_payment',
                  "amount" => trim($order_details['deliver_payment']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              
                //Deduct pickup amount from scrow and history
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));
                $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) - trim($order_details['deliver_payment']);
                $this->api->update_payment_in_deliverer_scrow($deliverer_scrow_master_details['scrow_id'], $deliverer_id, $scrow_balance);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));

                $update_data_scrow_history = array(
                  "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                  "order_id" => (int)$order_id,
                  "deliverer_id" => (int)$deliverer_id,
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'deliver_payment',
                  "amount" => trim($order_details['deliver_payment']),
                  "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_scrow_history($update_data_scrow_history);
              }

              //relay point commission---------------------------------------------------------------------------------------
              //deduct commission amount from gonagoo account master and history
              $relay_point_details = $this->api->get_relay_point_details($cd_id);
              $commission_percent = trim($relay_point_details['relay_commission']);
              $relay_commission_amount = round((trim($order_details['commission_amount'])/100)*trim($commission_percent),2);
              $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($relay_commission_amount);
              $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$relay_id,
                "type" => 0,
                "transaction_type" => 'relay_commission',
                "amount" => trim($relay_commission_amount),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

              //Add commission amount in relay account master and history
              
              //Check for new currency in account and add new currency row in account table
              if($this->api->relay_account_master_details($relay_id, trim($order_details['currency_sign']))) {
                $relay_account_master_details = $this->api->relay_account_master_details($relay_id, trim($order_details['currency_sign']));
              } else {
                $insert_data_relay_master = array(
                  "user_id" => (int)$relay_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $this->api->insert_relay_master_record($insert_data_relay_master);
              }
              
              $relay_account_master_details = $this->api->relay_account_master_details($relay_id, trim($order_details['currency_sign']));
              $account_balance = trim($relay_account_master_details['account_balance']) + trim($relay_commission_amount);
              $this->api->update_payment_in_relay_master($relay_account_master_details['account_id'], $relay_id, $account_balance);
              $relay_account_master_details = $this->api->relay_account_master_details($relay_id, trim($order_details['currency_sign']));

              $update_data_account_history = array(
                "account_id" => (int)$relay_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$relay_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'relay_commission',
                "amount" => trim($relay_commission_amount),
                "account_balance" => trim($relay_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_relay_account_history($update_data_account_history);

              //send commission invoice to Gonagoo -----------------------------
                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference("$order_id");
                $invoice->setDate(date('M dS ,Y',time()));
                
                $relay_country = $this->api->get_country_details(trim($relay_point_details['country_id']));
                $relay_state = $this->api->get_state_details(trim($relay_point_details['state_id']));
                $relay_city = $this->api->get_city_details(trim($relay_point_details['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($relay_point_details['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array(trim($relay_point_details['firstname']) . " " . trim($relay_point_details['lastname']),trim($relay_city['city_name']),trim($relay_state['state_name']),trim($relay_country['country_name']),trim($relay_point_details['email'])));

                $invoice->setFrom(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                /* Adding Items in table */
                $order_for = $this->lang->line('gonagoo_commission');
                $rate = round(trim($relay_commission_amount),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
                
                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('commission_paid'));

                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                //$invoice->addTitle("Other Details");
                /* Add Paragraph */
                //$invoice->addParagraph("Any Other Details");
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($order_id.'_dest_relay_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

                //Update File path
                $pdf_name = $order_id.'_dest_relay_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/order-invoices/".$pdf_name);
                
                //Update Order Invoice and post to workroom
                $this->api->update_dest_relay_commission_invoice_url($order_id, "order-invoices/".$pdf_name);
              /* ---------------------------------------------------------------*/
              /* -----------------Email Invoice to customer---------------------*/
                $eol = PHP_EOL;
                $messageBody = $eol . $this->lang->line('invoice_email_message') . $eol;
                $file = "resources/order-invoices/".$pdf_name;
                $file_size = filesize($file);
                $handle = fopen($file, "r");
                $content = fread($handle, $file_size);
                fclose($handle);
                $content = chunk_split(base64_encode($content));
                $uid = md5(uniqid(time()));
                $name = basename($file);
                //$eol = PHP_EOL;
                $from_mail = $this->config->item('from_email');
                $replyto = trim($relay_point_details['email']);
                // Basic headers
                $header = "From: Gonagoo <".$from_mail.">".$eol;
                $header .= "Reply-To: ".$replyto.$eol;
                $header .= "MIME-Version: 1.0\r\n";
                $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";

                // Put everything else in $message
                $message = "--".$uid.$eol;
                $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
                $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
                $message .= $messageBody.$eol;
                $message .= "--".$uid.$eol;
                $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
                $message .= "Content-Transfer-Encoding: base64".$eol;
                $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
                $message .= $content.$eol;
                $message .= "--".$uid."--";

                mail($gonagoo_address['email'], $this->lang->line('commission_invoice_email_subject'), $message, $header);
              /* -----------------------Email Invoice to customer---------------*/
              
              //post payment in workroom
              $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('pickup_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
            //Payment Module End --------------------------------------------------------------------------------
          }
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_status_update_success')];
          echo json_encode($this->response);
        }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_status_update_failed')]; echo json_encode($this->response);  }
    }
  }

  public function relay_confirm_payment()
  {
    //echo json_encode($_POST); die();
    $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
    $transaction_id = $this->input->post('relay_id', TRUE);

    $today = date('Y-m-d h:i:s');
    $order_details = $this->mainapi->get_order_detail($order_id);
    $total_amount_paid = $order_details['order_price'];
    $transfer_account_number = "NULL";
    $bank_name = "NULL";
    $account_holder_name = "NULL";
    $iban = "NULL";
    $email_address = "NULL";
    $mobile_number = "NULL";    

    $update_data_order = array(
      "payment_method" => 'cod',
      "paid_amount" => trim($total_amount_paid),
      "transaction_id" => trim($transaction_id),
      "payment_datetime" => $today,
      "complete_paid" => "1",
    );

    //echo json_encode($update_data_order); die();

    if($this->mainapi->update_payment_details_in_order($update_data_order, $order_id)) {
      if(trim($order_details['is_bank_payment']) == 0) {
        /*************************************** Payment Section ****************************************/
          //Add core price to gonagoo master
          if($this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']))) {
            $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->mainapi->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
          $this->mainapi->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$order_details['cust_id'],
            "type" => 1,
            "transaction_type" => 'standard_price',
            "amount" => trim($order_details['standard_price']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => 'cod',
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->mainapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          
          //Update urgent fee in gonagoo account master
          if(trim($order_details['urgent_fee']) > 0) {
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
            $this->mainapi->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['cust_id'],
              "type" => 1,
              "transaction_type" => 'urgent_fee',
              "amount" => trim($order_details['urgent_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'cod',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->mainapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update insurance fee in gonagoo account master
          if(trim($order_details['insurance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
            $this->mainapi->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['cust_id'],
              "type" => 1,
              "transaction_type" => 'insurance_fee',
              "amount" => trim($order_details['insurance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'cod',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->mainapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update handling fee in gonagoo account master
          if(trim($order_details['handling_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
            $this->mainapi->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['cust_id'],
              "type" => 1,
              "transaction_type" => 'handling_fee',
              "amount" => trim($order_details['handling_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'cod',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->mainapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update vehicle fee in gonagoo account master
          if(trim($order_details['vehicle_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
            $this->mainapi->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['cust_id'],
              "type" => 1,
              "transaction_type" => 'vehicle_fee',
              "amount" => trim($order_details['vehicle_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'cod',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->mainapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update custome clearance fee in gonagoo account master
          if(trim($order_details['custom_clearance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
            $this->mainapi->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['cust_id'],
              "type" => 1,
              "transaction_type" => 'custom_clearance_fee',
              "amount" => trim($order_details['custom_clearance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'cod',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->mainapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update loading Unloading in gonagoo account master
          if(trim($order_details['loading_unloading_charges']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
            $this->mainapi->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->mainapi->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['cust_id'],
              "type" => 1,
              "transaction_type" => 'loading_unloading_charges',
              "amount" => trim($order_details['loading_unloading_charges']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'cod',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->mainapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          $cust_id = (int)$order_details['cust_id'];
          //Add payment details in customer account master and history
          if($this->mainapi->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
            $customer_account_master_details = $this->mainapi->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->mainapi->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->mainapi->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          }

          $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
          $this->mainapi->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->mainapi->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->mainapi->insert_payment_in_account_history($update_data_account_history);

          //Deduct payment details from customer account master and history
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
          $this->mainapi->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->mainapi->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->mainapi->insert_payment_in_account_history($update_data_account_history);
        /*************************************** Payment Section ****************************************/
      }
      $this->response = ['response' => 'success', 'message'=> $this->lang->line('payment_success')];
    } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('payment_failed')]; }
    echo json_encode($this->response);
  }

  public function assign_warehouse($data)
  {
    if(is_array($data)){
      $rpw_id = (int) $data['rpw_id'];
      $relay_id = (int) $data['relay_id'];
      $relay_mgr_id = (int) $data['relay_mgr_id'];
      $order_id = (int) $data['order_id'];
      $order_width = trim($data['order_width']);
      $order_height = trim($data['order_height']);
      $order_length = trim($data['order_length']);
      $order_total_quantity = (int) $data['order_total_quantity'];

      $order_volume = round($order_width*$order_height*$order_length, 0);
      $total_order_volume = round($order_width*$order_height*$order_length*$order_total_quantity, 0);
      $warehouse_details = $this->api->get_warehouse_details($rpw_id);
      $warehouse_volume_not_used = $warehouse_details['max_volume'] - $warehouse_details['volume_used'];

      if($order_height <= $warehouse_details['height']) {
        if($order_width <= $warehouse_details['width']) {
          if($order_length <= $warehouse_details['length']) {
            if($total_order_volume <= $warehouse_volume_not_used) {
              for ($i=0; $i < $order_total_quantity; $i++) { 
                $insert_data = array(
                  "rpw_id" => (int)trim($rpw_id),
                  "relay_id" => (int)trim($relay_id),
                  "relay_mgr_id" => (int)trim($relay_mgr_id),
                  "order_id" => (int)trim($order_id),
                  "width" => trim($order_width),
                  "height" => trim($order_height),
                  "length" => trim($order_length),
                  "volume" => trim($order_volume),
                  "in_datetime" => date('Y-m-d h:i:s'),
                );
                $rpwd_id = $this->api->assign_warehouse($insert_data);
              }
              $updated_warehouse_volume = $warehouse_details['volume_used'] + $total_order_volume;
              if( $this->api->update_warehouse_volume_used($rpw_id, $updated_warehouse_volume) ) {
                $this->response = ['response' => 'success', 'message'=>$this->lang->line('added_to_warehouse'), "warehouse_details" => $warehouse_details];
              } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('added_to_warehouse_failed')];  }
            } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('no_enough_space_in_warehouse')];  }
          } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('length_exceeds_warehouse_length')];  }
        } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('width_exceeds_warehouse_width')];  }
      } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('height_exceeds_warehouse_height')];  }
      return json_encode($this->response);
    }
  }

  public function assign_warehouse_old()
  {
    if(empty($this->errors)){
      $rpw_id = (int) $this->input->post('rpw_id', TRUE);
      $relay_id = (int) $this->input->post('relay_id', TRUE);
      $relay_mgr_id = (int) $this->input->post('relay_mgr_id', TRUE);
      $order_id = (int) $this->input->post('order_id', TRUE);
      $order_width = $this->input->post('order_width', TRUE);
      $order_height = $this->input->post('order_height', TRUE);
      $order_length = $this->input->post('order_length', TRUE);
      $order_total_quantity = $this->input->post('order_total_quantity', TRUE);

      $order_volume = round($order_width*$order_height*$order_length, 0);
      $total_order_volume = round($order_width*$order_height*$order_length*$order_total_quantity, 0);
      $warehouse_details = $this->api->get_warehouse_details($rpw_id);
      $warehouse_volume_not_used = $warehouse_details['max_volume'] - $warehouse_details['volume_used'];

      if($order_height <= $warehouse_details['height']) {
        if($order_width <= $warehouse_details['width']) {
          if($order_length <= $warehouse_details['length']) {
            if($total_order_volume <= $warehouse_volume_not_used) {
              for ($i=0; $i < $order_total_quantity; $i++) { 
                $insert_data = array(
                  "rpw_id" => (int)trim($rpw_id),
                  "relay_id" => (int)trim($relay_id),
                  "relay_mgr_id" => (int)trim($relay_mgr_id),
                  "order_id" => (int)trim($order_id),
                  "width" => trim($order_width),
                  "height" => trim($order_height),
                  "length" => trim($order_length),
                  "volume" => trim($order_volume),
                  "in_datetime" => date('Y-m-d h:i:s'),
                );
                $rpwd_id = $this->api->assign_warehouse($insert_data);
              }
              $updated_warehouse_volume = $warehouse_details['volume_used'] + $total_order_volume;
              if( $this->api->update_warehouse_volume_used($rpw_id, $updated_warehouse_volume) ) {
                $this->response = ['response' => 'success', 'message'=>$this->lang->line('added_to_warehouse'), "warehouse_details" => $warehouse_details];
              } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('added_to_warehouse_failed')];  }
            } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('no_enough_space_in_warehouse')];  }
          } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('length_exceeds_warehouse_length')];  }
        } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('width_exceeds_warehouse_width')];  }
      } else { $this->response = ['response' => 'failed', 'message'=>$this->lang->line('height_exceeds_warehouse_height')];  }
      echo json_encode($this->response);
    }
  }

  public function get_relay_warehouse_details()
  {
    if(empty($this->errors)){ 
      $rpw_id = $this->input->post('rpw_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $warehouse_order_details = array();
      $relay_point_warehouse_details = $this->api->get_relay_warehouse_details($rpw_id, $last_id);
      for ($i=0; $i < sizeof($relay_point_warehouse_details); $i++) { 
        $order_details = $this->api->get_order_detail($relay_point_warehouse_details[$i]['order_id']);
        array_push($warehouse_order_details, $order_details);
        //$warehouse_order_details += $order_details;
      }

      if(empty($order_details)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'order_details' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'order_details' => $warehouse_order_details]; 
      }
      echo json_encode($this->response);
    }
  }

  public function get_relay_warehouse_history()
  {
    if(empty($this->errors)){ 
      $rpw_id = $this->input->post('rpw_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $warehouse_order_details = array();
      $relay_warehouse_history = $this->api->get_relay_warehouse_history($rpw_id, $last_id);
      for ($i=0; $i < sizeof($relay_warehouse_history); $i++) { 
        $order_details = $this->api->get_order_detail($relay_warehouse_history[$i]['order_id']);
        array_push($warehouse_order_details, $order_details);
      }
      
      if(empty($relay_warehouse_history)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'relay_warehouse_history' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'relay_warehouse_history' => $warehouse_order_details]; 
      }
      echo json_encode($this->response);
    }
  }

  public function get_relay_account_balance()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE);
      $account_master_details = $this->api->relay_account_master_list($relay_id);
      if(!empty($account_master_details)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'account_master_details' => $account_master_details]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_relay_account_history()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $account_history_details = $this->api->get_relay_account_history($relay_id, $last_id);
      if(!empty($account_history_details)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'account_history_details' => $account_history_details]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_relay_account_withdraw_request_list()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $account_withdraw_request_list = $this->api->get_relay_account_withdraw_request_list($relay_id, $last_id);
      if(!empty($account_withdraw_request_list)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'account_withdraw_request_list' => $account_withdraw_request_list]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function update_relay_account_details()
  {
    if(empty($this->errors)) {  
      $relay_id = $this->input->post('relay_id', TRUE);
      $relay_mgr_id = $this->input->post('relay_mgr_id', TRUE);
      $iban = $this->input->post('iban', TRUE);
      $mobile_money_acc1 = $this->input->post('mobile_money_acc1', TRUE);
      $mobile_money_acc2 = $this->input->post('mobile_money_acc2', TRUE);

      $insert_data = array(
        "iban" => trim($iban),
        "mobile_money_acc1" => trim($mobile_money_acc1),
        "mobile_money_acc2" => trim($mobile_money_acc2),
      );
      
      if($this->api->is_driver_active($relay_mgr_id)){
        if( $this->api->update_relay_account_details($relay_id, $insert_data) ){
          $relay = $this->api->get_relay_point_details(trim($relay_mgr_id));
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_status_update_success'), "relay" => $relay];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_status_update_failed')]; }
        $this->api->update_last_login($relay_mgr_id); // update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function relay_earned_this_month()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE);
      $earned_this_month_by_relay = $this->api->relay_earned_this_month($relay_id);
      if(!empty($earned_this_month_by_relay)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'earned_this_month_by_relay' => $earned_this_month_by_relay]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function relay_earned_to_date()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE);
      $earned_to_date_by_relay = $this->api->relay_earned_to_date($relay_id);
      if(!empty($earned_to_date_by_relay)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'earned_to_date_by_relay' => $earned_to_date_by_relay]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function relay_account_details()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE);
      $relay_account_master_list = $this->api->relay_account_master_list($relay_id);
      $relay_earned_this_month = $this->api->relay_earned_this_month($relay_id);
      $relay_earned_to_date = $this->api->relay_earned_to_date($relay_id);
      $relay_payment_methods = $this->api->relay_payment_methods($relay_id);

      if(!empty($relay_account_master_list) || !empty($relay_earned_this_month) || !empty($relay_earned_to_date) || !empty($customer_earned_this_month) || !empty($customer_paid_to_date) || !empty($customer_earned_to_date)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'relay_account_master_list' => $relay_account_master_list, 'relay_earned_this_month' => $relay_earned_this_month, 'relay_earned_to_date' => $relay_earned_to_date, 'relay_payment_methods' => $relay_payment_methods]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function relay_commission_invoices()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE); $relay_id = (int) $relay_id;
      $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
      if( $invoice_details = $this->api->relay_commission_invoices($relay_id, $last_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "deliverer_comission_invoices" => $invoice_details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function relay_payment_methods()
  {
    if(empty($this->errors)){ 
      $relay_id = $this->input->post('relay_id', TRUE); $relay_id = (int) $relay_id;
      if( $relay_payment_methods = $this->api->relay_payment_methods($relay_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "relay_payment_methods" => $relay_payment_methods];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

}

/* End of file Relay_api.php */
/* Location: ./application/controllers/Relay_api.php */