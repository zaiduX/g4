<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrier_type extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Carrier_type_model', 'carrier');

			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function carrier_type()
	{
		$carrier_type_list = $this->carrier->get_carrier_type_list();
		$this->load->view('admin/carrier_type_list_view', compact('carrier_type_list')); 
		$this->load->view('admin/footer_view');	
	}
	
	public function delete_carrier_type()
	{
		$carrier_id = (int) $this->input->post('id', TRUE);
		if( $this->carrier->delete_carrier_type($carrier_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

	public function add_carrier_type()
	{
		$this->load->view('admin/carrier_type_add_view');
		$this->load->view('admin/footer_view');
	}
	
	public function add_carrier_type_details()
	{
		$carrier_title = $this->input->post('carrier_title', TRUE);
		$carrier_desc = $this->input->post('carrier_desc', TRUE);
		if(! $this->carrier->check_carrier_type_existance(strtolower($carrier_title)) ) {
			if($this->carrier->add_carrier_type_details($carrier_title, $carrier_desc)) {
				$this->session->set_flashdata('success','Carrier type successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new carrier type! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Carrier type already exists!');	}
		redirect('admin/add-carrier-type');
	}

	public function edit_carrier_type()
	{
		if( !is_null($this->input->post('carrier_id')) ) { $carrier_id = (int) $this->input->post('carrier_id'); }
		else if(!is_null($this->uri->segment(3))) { $carrier_id = (int)$this->uri->segment(3); }
		else { redirect('user-panel/carrier-type'); }
		$carrier_type_details = $this->carrier->get_carrier_type_details($carrier_id);
		$this->load->view('admin/carrier_type_edit_view', compact('carrier_type_details'));
		$this->load->view('admin/footer_view');
	}
	
	public function update_carrier_type_details()
	{
		$carrier_id = $this->input->post('carrier_id', TRUE); $carrier_id = (int) $carrier_id;
		$carrier_title = $this->input->post('carrier_title', TRUE);
		$carrier_desc = $this->input->post('carrier_desc', TRUE);
		$update_data = array();
		$today = date('Y-m-d h:i:s');
		
		$update_data = array(
			"carrier_title" => trim($carrier_title),
			"carrier_desc" => trim($carrier_desc),
			"cre_datetime" => $today,
		);

		if( $this->carrier->update_carrier_type_details($carrier_id, $update_data)){
			$this->session->set_flashdata('success','Carrier type details saved successfully!');
		} else { $this->session->set_flashdata('error','Unable to update details! Try again...'); }
			
		redirect('admin/edit-carrier-type/'.$carrier_id);
	}

}

/* End of file Carrier_type.php */
/* Location: ./application/controllers/Carrier_type.php */