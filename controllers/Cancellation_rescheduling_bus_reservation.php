<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cancellation_rescheduling_bus_reservation extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('cancellation_rescheduling_bus_reservation_model', 'charges');
      $this->load->model('standard_dimension_model', 'standard_dimension');   
      $this->load->model('Web_user_model', 'user');   
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $charge_list = $this->charges->get_list();
    $this->load->view('admin/cancellation_rescheduling_bus_reservation_list_view', compact('charge_list'));      
    $this->load->view('admin/footer_view');   
  }

  public function add()
  {
    $countries = $this->charges->get_countries();
    $vehicle_types = $this->charges->get_category_vehicles_master();
    $this->load->view('admin/cancellation_rescheduling_bus_reservation_add_view',compact('countries','vehicle_types'));
    $this->load->view('admin/footer_view');
  }
  
  public function register()
  { //echo json_encode($_POST); die();
    $country_id = $this->input->post('country_id', TRUE);
    $bcr_min_hours = $this->input->post('bcr_min_hours', TRUE);
    $bcr_max_hours = $this->input->post('bcr_max_hours', TRUE);
    $bcr_cancellation = $this->input->post('bcr_cancellation', TRUE);
    $bcr_rescheduling = $this->input->post('bcr_rescheduling', TRUE);
    $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
    $insert_flag = false;

    if($country_id > 0 ) {
      $insert_data = array(
        "country_id" => (int)trim($country_id), 
        "bcr_cancellation" => number_format($bcr_cancellation,2,'.',''),
        "bcr_rescheduling" => number_format($bcr_rescheduling,2,'.',''),
        "bcr_min_hours" => (int)trim($bcr_min_hours), 
        "bcr_max_hours" => (int)trim($bcr_max_hours), 
        "bcr_cre_datetime" =>  date("Y-m-d H:i:s"), 
        "bcr_status" =>  1, 
        "vehical_type_id" => (int)trim($vehical_type_id), 
      );
      //echo json_encode($insert_data); die();
      if(!$this->charges->check_payment($insert_data, 'min') && !$this->charges->check_payment($insert_data, 'max')) {
        if($this->charges->register_payment($insert_data)){ $insert_flag = true;  } 
      } else {  $insert_flag = false;  }
    } else {  $this->session->set_flashdata('error','Please select country!');  }

    if($insert_flag){ $this->session->set_flashdata('success','Details successfully added!'); }
    else{ $this->session->set_flashdata('error','Unable to add or configuration already exists for selected country or vehicle type!');  }

    redirect('admin/cancellation-rescheduling-bus-reservation/add');
  }

  public function edit()
  { 
    if( $id = $this->uri->segment(4) ) {      
      if( $cancellation = $this->charges->get_cancellation_detail($id)){
        $countries = $this->charges->get_countries();
        $this->load->view('admin/cancellation_rescheduling_bus_reservation_edit_view', compact('cancellation','countries'));
        $this->load->view('admin/footer_view');
      } else { redirect('admin/cancellation_rescheduling_bus_reservation'); }
    } else { redirect('admin/cancellation_rescheduling_bus_reservation'); }
  }

  public function update()
  { //echo json_encode($_POST); die();
    $bcr_id = $this->input->post('bcr_id', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $bcr_cancellation = $this->input->post('bcr_cancellation', TRUE);
    $bcr_rescheduling = $this->input->post('bcr_rescheduling', TRUE);
    $bcr_min_hours = $this->input->post('bcr_min_hours', TRUE);
    $bcr_max_hours = $this->input->post('bcr_max_hours', TRUE);
    $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
    $insert_flag = false;

    $update_data = array(       
      "country_id" => (int)trim($country_id), 
      "bcr_cancellation" => number_format($bcr_cancellation,2,'.',''),
      "bcr_rescheduling" => number_format($bcr_rescheduling,2,'.',''),
      "bcr_min_hours" => (int)trim($bcr_min_hours), 
      "bcr_max_hours" => (int)trim($bcr_max_hours), 
      "bcr_cre_datetime" =>  date("Y-m-d H:i:s"), 
      "bcr_status" =>  1, 
      "vehical_type_id" => (int)trim($vehical_type_id),
    );
    //echo json_encode($update_data); die();
    if(!$this->charges->check_payment_expect_self($update_data, 'min', $bcr_id) && !$this->charges->check_payment_expect_self($update_data, 'max', $bcr_id)) {
      if($this->charges->update_cencellation($update_data, $bcr_id)){
        $this->session->set_flashdata('success','Configuration successfully updated!');
      } else { $this->session->set_flashdata('error','Failed to update configuration details!'); }
    } else {  $this->session->set_flashdata('error','Unable to update configuration, same configuration found! Try with different configuration...');  }
    redirect('admin/cancellation-rescheduling-bus-reservation/edit/'.$bcr_id);
  }

  public function delete()
  {
    $id = $this->input->post('id', TRUE);
    if($this->charges->delete_details($id)){ echo 'success'; } else { echo "failed";  } 
  }

}

/* End of file Cancellation_rescheduling_bus_reservation.php */
/* Location: ./application/controllers/Cancellation_rescheduling_bus_reservation.php */