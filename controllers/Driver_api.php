<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver_api extends CI_Controller {

  private $errors = array();

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Driver_Api_model', 'api');
    $this->load->model('Api_model', 'commonapi');
    $this->load->model('Api_model_bus', 'commonapi_bus');
    $this->load->model('Notification_model', 'notification');
    $this->load->model('Relay_api_model', 'relay');
    $this->load->model('Users_model', 'users');
    $this->load->model('Web_user_model', 'user');
    $this->load->model('Api_model_sms', 'api_sms');
    header('Content-Type: Application/json'); 
    if( $this->input->method() != 'post' ) exit('No direct script access allowed');
    if( $this->input->is_ajax_request() ) exit('No direct script access allowed');
    if(isset($_POST)){
      $lang = $this->input->post('lang', TRUE);
      if( $lang == 'fr' ) {
        $this->config->set_item('language', 'french');
        $this->lang->load('frenchApi_lang','french');
      }
      else if( $lang == 'sp' ) {
        $this->config->set_item('language', 'spanish');
        $this->lang->load('spanishApi_lang','spanish');
      }
      else{
        $this->config->set_item('language', 'english');
        $this->lang->load('englishApi_lang','english');
      }

      foreach($_POST as $key => $value){ 
        if(trim($value) == "") { $this->errors[] = ucwords(str_replace('_', ' ', $key)); }        
      }
      if(!empty($this->errors)) { 
        echo json_encode(array('response' => 'failed', 'message' => 'Empty field(s) - '.implode(', ', $this->errors))); 
      }
    }
  }

  public function index() { exit('No direct script access allowed'); }

  public function app_params()
  {
    if(empty($this->errors)){ 
      $app_ver = $this->input->post('app_ver', TRUE);
      $params = $this->api->get_application_parameters();
      
      if(version_compare($app_ver, $params->app_version) < 0) {
        if($params->forcefully_update == 1) {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('new_update_found_please_update_to_continue'), 'forcefully_update' => 1 ];
        } else { $this->response = ['response' => 'success', 'message' => $this->lang->line('new_update_found_please_update_to_continue'), 'forcefully_update' => 0 ]; }
      } else {  $this->response = ['response' => 'failed', 'message' => '', 'forcefully_update' => 0 ]; }
      echo json_encode($this->response);
    }
  }

  public function login()
  {
    if(empty($this->errors)){ 
      $device_id = $this->input->post('device_id', TRUE);
      $email = $this->input->post('email', TRUE);
      $password = $this->input->post('password', TRUE);
      $device_type = $this->input->post('device_type', TRUE); $device_type = (int) $device_type; // 1 : android, 2: ios
      $reg_id = $this->input->post('reg_id', TRUE);

      if( (trim($email) != "NULL") AND ($user = $this->api->is_emailId_exists(trim($email))) ) {
        if( $cd_id = $this->api->validate_login(trim($email), trim($password)) ) {
          if( $this->api->is_driver_active(trim($cd_id)) ){ 
            if(!$this->api->is_deviceId_exists($device_id)) { 
              // insert into device table
              $device_insert_data = array(
                "device_id" => trim($device_id),
                "cd_id" => (int)($cd_id),
                "reg_id" => trim($reg_id),
                "device_type" => ($device_type == 2 ) ? 0: 1,
              );
              $this->api->register_new_device($device_insert_data);
            } 
            else {
              $device_update_data = array(
                "cd_id" => (int)($cd_id),
                "reg_id" => trim($reg_id),
                "device_type" => ($device_type == 2 ) ? 0: 1,
              );  
              // update device 
              $this->api->update_device_detail($device_id, $device_update_data);
            }
            // udpate last login 
            $this->api->update_last_login($cd_id);

            if($user['cust_id'] == 0) {
              $relay_point_details = $this->relay->get_relay_point_details($cd_id);
              $relay = array("relay_id" => $relay_point_details['relay_id']);
              $user_relay = array_merge($user, $relay);
              $this->response = ['response' => 'success','message'=> $this->lang->line('driver_login_success'), "user" => $user_relay];
            } else {
              $this->response = ['response' => 'success','message'=> $this->lang->line('driver_login_success'), "user" => $user];
            }

            //$this->response = ['response' => 'success','message'=> $this->lang->line('driver_login_success'), "user" => $user];
          } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_login_failed')]; }
      } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_email_not_exists')]; }
      echo json_encode($this->response);
    }
  }

  public function forget_password()
  {
    if(empty($this->errors)) {  
      $email_id = $this->input->post('email_id', TRUE);
      if( $driver = $this->api->is_emailId_exists($email_id)) {
        $subject = $this->lang->line('driver_forget_pass_title');
        $message ="<p class='lead'>".$this->lang->line('driver_forget_pass_msg1')."</p>";
        $message .="</td></tr><tr><td align='center'><br />";
        $message .="<a class='callout'>".$this->lang->line('driver_forget_pass_msg2')."<strong>".$driver['password']."</strong></a>";
        $username = trim($driver['first_name']) . trim($driver['last_name']);

        // Send activation email to given email id
        if( $this->api_sms->send_email_text($username, $email_id, $subject, trim($message))){
          $sms_msg = $this->lang->line('driver_forget_pass_msg2') . $driver['password'];
          if(substr($driver['mobile1'], 0, 1) == 0) { $mobile1 = ltrim($driver['mobile1'], 0); } else { $mobile1 = $driver['mobile1']; }
          $this->api->sendSMS(trim($driver['country_code']).trim($mobile1), $sms_msg);
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_credential_success') ];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_credential_failed')]; }
        }  else{  $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_not_exists')]; }
        echo json_encode($this->response);
      }
  }

  public function change_password()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $password = $this->input->post('password', TRUE);
      $new_password = $this->input->post('new_password', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        if($this->api->is_driver_password_match($cd_id, $password)){
          $update_data = array(
            "password" => trim($new_password),
          );
          if( $this->api->update_password($cd_id, $update_data) ){
            $this->response = ['response' => 'success','message'=>$this->lang->line('driver_pass_update_success')];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_pass_update_failed')]; }
          $this->api->update_last_login($cd_id);// update user last login
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_old_pass_match')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function get_driver_profile()
  {
    if(empty($this->errors)){ 
      $cd_id = $this->input->post('cd_id', TRUE);
      $driver_profile_details = $this->api->get_profile_details($cd_id);
      if(empty($driver_profile_details)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_details_failed'), 'driver_profile_details' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_details_success'), 'driver_profile_details' => $driver_profile_details]; 
      }
      echo json_encode($this->response);
    }
  }

  public function update_driver_profile()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $first_name = $this->input->post('first_name', TRUE);
      $last_name = $this->input->post('last_name', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $mobile2 = $this->input->post('mobile2', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        $update_data = array(
          "first_name" => trim($first_name),
          "last_name" => trim($last_name),
          "mobile1" => trim($mobile1),
          "mobile2" => trim($mobile2),
          "mod_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_profile_details($cd_id, $update_data) ){
          $driver = $this->api->get_profile_details(trim($cd_id));
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_driver_update_success'), "driver" => $driver];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_driver_update_failed')]; }
        $this->api->update_last_login($cd_id);// update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function update_driver_avatar()
  {
    if(empty($this->errors)) {
      $cd_id = $this->input->post('cd_id', TRUE);
      if($this->api->is_driver_active($cd_id)){
        if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('profile_image')) { 
            $uploads    = $this->upload->data();
            $avatar_url =  $config['upload_path'].$uploads["file_name"];
            $this->api->delete_old_profile_image($cd_id);
          }
          if( $this->api->update_profile_image($avatar_url, $cd_id)){
            $driver = $this->api->get_profile_details(trim($cd_id));
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('driver_avatar_success'), "driver" => $driver];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_avatar_failed')]; }
        } 
        // update user last login
        $this->api->update_last_login($cd_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function driver_online_status()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $online_status = (int)$this->input->post('online_status', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        $update_data = array(
          "online_status" => trim($online_status),
          "last_online_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_online_status($cd_id, $update_data) ){
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_online_success'), 'status' => trim($online_status)];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_online_failed')]; }
        $this->api->update_last_login($cd_id);// update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function driver_location_update()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      
      if($this->api->is_driver_active($cd_id)){
        $update_data = array(
          "latitude" => trim($latitude),
          "longitude" => trim($longitude),
          "loc_update_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_driver_location($cd_id, $update_data) ){
          $this->response = ['response' => 'success', 'message'=>$this->lang->line('driver_location_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_location_failed')]; }
        $this->api->update_last_login($cd_id);// update user last login
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive')]; }
      echo json_encode($this->response);
    }
  }

  public function driver_order_list()
  {
    if(empty($this->errors)){ 
      $cd_id = $this->input->post('cd_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $order_status = $this->input->post('order_status', TRUE);
      $driver_order = $this->api->get_order_list($cd_id, $last_id, $order_status);
      
      for ($i=0; $i < sizeof($driver_order); $i++) { 
        $packages = $this->api->get_order_packages($driver_order[$i]['order_id']);
        for ($j=0; $j < sizeof($packages); $j++){
          if(!is_null($this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'])) {
          $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
          } else {
            $packages[$j]['dangrous_goods_name'] = 'NULL';
          }
        }
        $driver_order[$i] += ['packages' => $packages];
      }
      
      if(empty($driver_order)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed'), 'driver_order' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'driver_order' => $driver_order]; 
      }
      echo json_encode($this->response);
    }
  }

  public function driver_new_order_count()
  {
    if(empty($this->errors)){ 
      $cd_id = $this->input->post('cd_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $order_status = $this->input->post('order_status', TRUE);
      $driver_new_order_count = $this->api->get_new_order_count($cd_id, $last_id, $order_status);
      if(empty($driver_new_order_count)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed')]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'driver_new_order_count' => $driver_new_order_count]; 
      }
      echo json_encode($this->response);
    }
  }

  public function driver_order_status_update()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $order_id = $this->input->post('order_id', TRUE);
      $order_status = $this->input->post('order_status', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE);
      $deliverer_id = $this->input->post('deliverer_id', TRUE);
      $customer_fcm_ids = array();
      $customer_apn_ids = array();
      $order_details = $this->api->get_order_detail($order_id);
      if( $this->api->update_order_status($cd_id, $order_id, $order_status) ){

        //Get customer and deliverer contact and personal details
        $customer_details = $this->api->get_user_details((int)$order_details['cust_id']);
        $deliverer_details = $this->api->get_user_details((int)$order_details['deliverer_id']);

        //Get Users device Details
        $device_details_customer = $this->api->get_user_device_details($cust_id);
        //Get Customer Device Reg ID's
        $arr_customer_fcm_ids = array();
        $arr_customer_apn_ids = array();
        if(!empty($device_details_customer)) {
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_customer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_customer_apn_ids, $value['reg_id']);
            }
          }
          //$customer_fcm_ids = $arr_customer_fcm_ids;
          $customer_apn_ids = $arr_customer_apn_ids;
        }
        
        //Get Deliverer Device ID's
        $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
        
        $arr_deleverer_fcm_ids = array();
        $arr_deleverer_apn_ids = array();
        if(!empty($device_details_deleverer)) {
          //var_dump($device_details_deleverer); die();
          foreach ($device_details_deleverer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_deleverer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_deleverer_apn_ids, $value['reg_id']);
            }
          }
          //$deleverer_fcm_ids = $arr_deleverer_fcm_ids;
          $deleverer_apn_ids = $arr_deleverer_apn_ids;
        }
        
        
        //Get PN API Keys and PEM files
        $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
        $api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
        
        //Send Notifications as per order status
        if ( trim($order_status) == 'accept' ) {
          $this->api->insert_order_status($cd_id, $order_id, $order_status);
          if($order_id <= 99999) { $driver_code = 'CD'.$cd_id.sprintf("%06s", $order_id); } else { $driver_code = 'CD'.$cd_id.$order_id; }
          $this->api->update_driver_code($order_id, $driver_code);

          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('driver_accept_order_msg') . ' ' . $this->lang->line('driver_delivery_code') . $driver_code;

          $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);
          
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          if($customer_details['firstname']!='NULL'){
            $cust_full_name = trim($customer_details['firstname'])." ".trim($customer_details['lastname']);
          } else {
            $email_cut = explode("@", $customer_details['email1']);
            $cust_full_name = strtoupper($email_cut[0]);
          }
          if($deliverer_details['firstname']!='NULL'){
            $deliverer_full_name = trim($deliverer_details['firstname'])." ".trim($deliverer_details['lastname']);
          } else {
            $email_cut = explode("@", $deliverer_details['email1']);
            $deliverer_full_name = strtoupper($email_cut[0]);
          }
          
          //Email confirmation
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear') . trim($deliverer_full_name) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_accept_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_full_name);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          //Push Notification FCM
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          if(!empty($arr_deleverer_fcm_ids)) { $this->api->sendFCM($msg, $arr_deleverer_fcm_ids, $api_key_deliverer); }
          //Push Notification APN
          $msg_apn_deliverer =  array('title' => $subject, 'text' => $sms_msg);
          if(isset($deleverer_apn_ids) && is_array($deleverer_apn_ids)) { 
            $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
          }
          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($cust_full_name), trim($deliverer_full_name), 'NULL', 'NULL', 'NULL');
        } 
        if ( trim($order_status) == 'reject' ){
          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('driver_reject_order_msg');
          if(!empty($device_details_deleverer)) { 
            $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
            if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
            $this->api->sendSMS($country_code.trim($mobile1), $sms_msg); 
          }
          //Email confirmation
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_reject_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          //Push Notification FCM
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          if(!empty($arr_deleverer_fcm_ids)) { $this->api->sendFCM($msg, $arr_deleverer_fcm_ids, $api_key_deliverer); }
          $msg_apn_deliverer =  array('title' => $subject, 'text' => $sms_msg);
          if(isset($deleverer_apn_ids) && is_array($deleverer_apn_ids)) { 
            $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
          }
        }
        if ( trim($order_status) == 'in_progress' ){
          $this->api->insert_order_status($cd_id, $order_id, $order_status);
          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('driver_inprogress_order_msg');
          
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg);

          //Email confirmation deliverer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_inprogress_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          //Email confirmation Customer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($customer_details['firstname']) . " " . trim($customer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_inprogress_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
          $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject, trim($message));
          //Push Notification FCM
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          if(!empty($arr_customer_fcm_ids)) { $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key_deliverer); }
          if(!empty($arr_deleverer_fcm_ids)) { $this->api->sendFCM($msg, $arr_deleverer_fcm_ids, $api_key_deliverer); }
          //Push Notification APN
          $msg_apn_customer =  array('title' => $subject, 'text' => $sms_msg);
          if(isset($customer_apn_ids) && is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          if(isset($deleverer_apn_ids) && is_array($deleverer_apn_ids)) { 
            $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $deleverer_apn_ids);
          }
          
          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
          //check order of dedicated users
          if(trim($order_details['is_bank_payment']) == 0) {
            //Payment Module-------------------------------------------------------------------------------------
              $today = date('Y-m-d h:i:s');
              //deduct pickup_payment details from gonagoo account master and history
              $order_details = $this->api->get_order_detail($order_id);
              if(trim($order_details['pickup_payment']) > 0 && trim($order_details['payment_mode']) != 'cod') {
                //update payment details in gonagoo account master and history
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                  $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($order_details['pickup_payment']);
                  $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$deliverer_id,
                    "type" => 0,
                    "transaction_type" => 'pickup_payment',
                    "amount" => trim($order_details['pickup_payment']),
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'NULL',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => 'NULL',
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

                //Add pickup_payment details in deliverer account master and history
                  $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));
                  $account_balance = trim($customer_account_master_details['account_balance']) + trim($order_details['pickup_payment']);
                  $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $deliverer_id, $account_balance);
                  $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));

                  $update_data_account_history = array(
                    "account_id" => (int)$customer_account_master_details['account_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$deliverer_id,
                    "datetime" => $today,
                    "type" => 1,
                    "transaction_type" => 'pickup_payment',
                    "amount" => trim($order_details['pickup_payment']),
                    "account_balance" => trim($customer_account_master_details['account_balance']),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($order_details['currency_sign']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_account_history($update_data_account_history);

                //Deduct pickup amount from scrow and history
                  $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));
                  $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) - trim($order_details['pickup_payment']);
                  $this->api->update_payment_in_deliverer_scrow((int)$deliverer_scrow_master_details['scrow_id'], $deliverer_id, $scrow_balance);
                  $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));

                  $update_data_scrow_history = array(
                    "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                    "order_id" => (int)$order_id,
                    "deliverer_id" => (int)$deliverer_id,
                    "datetime" => $today,
                    "type" => 0,
                    "transaction_type" => 'pickup_payment',
                    "amount" => trim($order_details['pickup_payment']),
                    "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                    "currency_code" => trim($order_details['currency_sign']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_scrow_history($update_data_scrow_history);
              }

              //post payment in workroom
              $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('pickup_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
            //Payment Module End --------------------------------------------------------------------------------
          }
        } 
        if ( trim($order_status) == 'delivered' ) {
          $this->api->insert_order_status($cd_id, $order_id, $order_status);
          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('driver_delivered_order_msg');

          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg);

          //----------------Email confirmation deliverer----------
            $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
            $message ="<p class='lead'>".$this->lang->line('dear'). trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']) . "</p>";
            $message ="<p class='lead'>". $sms_msg . "</p>";
            $message .="</td></tr><tr><td align='center'><br />";
            $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
            $email1 = trim($deliverer_details['email1']);
            $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
            //Email confirmation Customer
            $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
            $message ="<p class='lead'>".$this->lang->line('dear'). trim($customer_details['firstname']) . " " . trim($customer_details['lastname']) . "</p>";
            $message ="<p class='lead'>". $sms_msg . "</p>";
            $message .="</td></tr><tr><td align='center'><br />";
            $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
            $email1 = trim($customer_details['email1']);
            $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
            $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject, trim($message));
            $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject, trim($message));
            //Push Notification FCM
            $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
            if(!empty($arr_customer_fcm_ids)) { $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key_deliverer); }
            if(!empty($arr_deleverer_fcm_ids)) { $this->api->sendFCM($msg, $arr_deleverer_fcm_ids, $api_key_deliverer); }
            //Push Notification APN
            $msg_apn_customer =  array('title' => $subject, 'text' => $sms_msg);
            if(isset($customer_apn_ids) && is_array($customer_apn_ids)) { 
              $customer_apn_ids = implode(',', $customer_apn_ids);
              $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
            }
            if(isset($deleverer_apn_ids) && is_array($deleverer_apn_ids)) { 
              $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
              $this->notification->sendPushIOS($msg_apn_customer, $deleverer_apn_ids);
            }
          //Email-------------------------------------------------

          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');

          $order_details = $this->api->get_order_detail($order_id);
          if(trim($order_details['is_bank_payment']) == 0) {
            //Payment Module----------------------------------------
              $today = date('Y-m-d h:i:s');
              if(trim($order_details['deliver_payment']) > 0) {
                if(trim($order_details['payment_mode']) != 'cod') {
                  //deduct deliver_payment details from gonagoo account master and history
                    $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                    $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($order_details['deliver_payment']);
                    $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                    $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                    $update_data_gonagoo_history = array(
                      "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                      "order_id" => (int)$order_id,
                      "user_id" => (int)$deliverer_id,
                      "type" => 0,
                      "transaction_type" => 'deliver_payment',
                      "amount" => trim($order_details['deliver_payment']),
                      "datetime" => $today,
                      "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                      "transfer_account_type" => 'NULL',
                      "transfer_account_number" => 'NULL',
                      "bank_name" => 'NULL',
                      "account_holder_name" => 'NULL',
                      "iban" => 'NULL',
                      "email_address" => 'NULL',
                      "mobile_number" => 'NULL',
                      "transaction_id" => 'NULL',
                      "currency_code" => trim($order_details['currency_sign']),
                      "country_id" => trim($order_details['from_country_id']),
                      "cat_id" => trim($order_details['category_id']),
                    );
                    $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

                  //Add deliver_payment details in deliverer account master and history
                  $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));
                  $account_balance = trim($customer_account_master_details['account_balance']) + trim($order_details['deliver_payment']);
                  $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $deliverer_id, $account_balance);
                  $customer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));

                  $update_data_account_history = array(
                    "account_id" => (int)$customer_account_master_details['account_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$deliverer_id,
                    "datetime" => $today,
                    "type" => 1,
                    "transaction_type" => 'deliver_payment',
                    "amount" => trim($order_details['deliver_payment']),
                    "account_balance" => trim($customer_account_master_details['account_balance']),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($order_details['currency_sign']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_account_history($update_data_account_history);
              
                  //Deduct pickup amount from scrow and history
                  $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));
                  $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) - trim($order_details['deliver_payment']);
                  $this->api->update_payment_in_deliverer_scrow($deliverer_scrow_master_details['scrow_id'], $deliverer_id, $scrow_balance);
                  $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));

                  $update_data_scrow_history = array(
                    "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                    "order_id" => (int)$order_id,
                    "deliverer_id" => (int)$deliverer_id,
                    "datetime" => $today,
                    "type" => 0,
                    "transaction_type" => 'deliver_payment',
                    "amount" => trim($order_details['deliver_payment']),
                    "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                    "currency_code" => trim($order_details['currency_sign']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_scrow_history($update_data_scrow_history);

                  //post payment in workroom
                  $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('pickup_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
                }
              }
            //Payment Module End------------------------------------
                
            //Generate Invoice and Email to Customer----------------
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $invoice_lang = '';
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference("$order_id");
              $invoice->setDate(date('M dS ,Y',time()));
              
              $customer_country = $this->api->get_country_details(trim($customer_details['country_id']));
              $customer_state = $this->api->get_state_details(trim($customer_details['state_id']));
              $customer_city = $this->api->get_city_details(trim($customer_details['city_id']));
              $deliverer_country = $this->api->get_country_details(trim($deliverer_details['country_id']));
              $deliverer_state = $this->api->get_state_details(trim($deliverer_details['state_id']));
              $deliverer_city = $this->api->get_city_details(trim($deliverer_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($deliverer_details['country_id']));

              $invoice->setFrom(array(trim($customer_details['firstname']) . " " . trim($customer_details['lastname']),trim($customer_city['city_name']),trim($customer_state['state_name']),trim($customer_country['country_name']),trim($customer_details['email1'])));

              $invoice->setTo(array(trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']),trim($deliverer_city['city_name']),trim($deliverer_state['state_name']),trim($deliverer_country['country_name']),trim($deliverer_details['email1'])));
              
              /* Adding Items in table */
              $order_for = $this->lang->line('courier_delivery');
              $rate = round($order_details['advance_payment'] + $order_details['pickup_payment'] + $order_details['deliver_payment'],2);
              $total = $rate;
              $payment_datetime = substr($order_details['payment_datetime'], 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
              
              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('payment_paid'));

              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              //$invoice->addTitle("Other Details");
              /* Add Paragraph */
              //$invoice->addParagraph("Any Other Details");
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch')."\r\n".$deliverer_details['firstname'].' '.$deliverer_details['lastname']."\r\n".$this->lang->line('telephone').$deliverer_details['mobile1']."\r\n".$this->lang->line('email').$deliverer_details['email1']."");
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($order_id.'.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

              //Update File path
              $pdf_name = $order_id.'.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/order-invoices/".$pdf_name);
              
              //Update Order Invoice and post to workroom
              $this->api->update_order_invoice_url($order_id, "order-invoices/".$pdf_name);
            /*------------------------------------------------------*/
            
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Booking Invoice');
              $emailAddress = $customer_details['email1'];
              $fileToAttach = "resources/order-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */
          
            //Push Notification FCM
            $subject = $this->lang->line('invoice_email_subject');
            $msg =  array('title' => $subject, 'type' => 'raise_invoice', 'notice_date' => date("Y-m-d H:i:s"), 'attachment_url' => "order-invoices/".$pdf_name, "pdf");
            $this->api->sendFCM($msg, $customer_fcm_ids, $api_key_deliverer);
            $msg_apn_customer =  array('title' => $subject, 'text' => $this->lang->line('invoice_email_subject'));
            if(isset($customer_apn_ids) && is_array($customer_apn_ids)) { 
              $customer_apn_ids = implode(',', $customer_apn_ids);
              $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
            }

            $this->api->invoice_post_to_workroom($order_id, $cust_id, $deliverer_id, 'sp', 'raise_invoice', "order-invoices/".$pdf_name, trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'pdf');
          }
        }
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_status_update_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_status_update_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function driver_delivery_code_confirm()
  {
    if(empty($this->errors)){ 
      $order_id = $this->input->post('order_id', TRUE);
      $delivery_code = $this->input->post('delivery_code', TRUE);
      if( $this->api->verify_delivery_code($order_id, $delivery_code) ) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('drivery_code_success')]; 
      } else {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('drivery_code_failed')]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function get_delivery_code()
  {
    if(empty($this->errors)){ 
      $order_id = $this->input->post('order_id', TRUE);
      if( $delivery_code = $this->api->get_order_delivery_code($order_id, $delivery_code) ) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('drivery_code_found'), 'delivery_code' => $delivery_code['delivery_code']]; 
      } else {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('drivery_code_not_found')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function driver_chat_history()
  {
    if(empty($this->errors)){ 
      $cd_id = $this->input->post('cd_id', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $order_id = $this->input->post('order_id', TRUE);
      $driver_chat_list = $this->api->get_driver_chat_list($cd_id, $cust_id, $last_id, $order_id);
      if(empty($driver_chat_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('no_record_found'), 'driver_chat_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_chat_list_success'), 'driver_chat_list' => $driver_chat_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function driver_chat_post()
  {
    //echo json_encode($_POST); die();
    $cd_id = $this->input->post('cd_id', TRUE);
    $cust_id = $this->input->post('cust_id', TRUE);
    $order_id = $this->input->post('order_id', TRUE);
    $text_msg = $this->input->post('text_msg', TRUE);
    $file_type = $this->input->post('file_type', TRUE);
    $cd_name = $this->input->post('cd_name', TRUE);
    $cust_name = $this->input->post('cust_name', TRUE);
    
    if( !empty($_FILES["attachement"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachement')) {   
        $uploads    = $this->upload->data();  
        $attachement =  $config['upload_path'].$uploads["file_name"];  
      } 
    } else { $attachement = "NULL"; }

    $insert_data = array(
      "sender_id" => (int)$cd_id,
      "receiver_id" => (int)$cust_id,
      "cust_id" => (int)$cust_id,
      "cd_id" => (int)$cd_id,
      "order_id" => (int)$order_id,
      "text_msg" => $text_msg,
      "attachment_url" => $attachement,
      "cre_datetime" => date('Y-m-d H:i:s'),
      "file_type" => $file_type,
      "cd_name" => $cd_name,
      "cust_name" => $cust_name,
    );

    if( $chat_id = $this->api->post_driver_chat($insert_data)) {  
      $chat_details = $this->api->get_chat_details($chat_id);

      //Get Users device Details
      $device_details_customer = $this->api->get_user_device_details($cust_id);
      //Get Customer Device Reg ID's
      $arr_customer_fcm_ids = array();
      $arr_customer_apn_ids = array();
      foreach ($device_details_customer as $value) {
        if($value['device_type'] == 1) {
          array_push($arr_customer_fcm_ids, $value['reg_id']);
        } else {
          array_push($arr_customer_apn_ids, $value['reg_id']);
        }
      }
      $customer_fcm_ids = $arr_customer_fcm_ids;
      $customer_apn_ids = $arr_customer_apn_ids;

      //Get PN API Keys and PEM files
      $api_key_customer = $this->config->item('delivererAppGoogleKey');
      $api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

      $msg =  array('title' => 'chat', 'type' => 'chat', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $text_msg, 'attachment' => $attachement, 'cd_name' => $cd_name, 'cust_name' => $cust_name, 'cd_id' => $cd_id, 'cust_id' => $cust_id, 'order_id' => $order_id, 'file_type' => $file_type);
      $this->api->sendFCM($msg, $customer_fcm_ids, $api_key_customer);
      //Push Notification APN
      $msg_apn_customer =  array('title' => 'chat', 'text' => $text_msg);
      if(is_array($customer_apn_ids)) { 
        $customer_apn_ids = implode(',', $customer_apn_ids);
        $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
      }
      $this->response = ['response' => 'success','message'=> $this->lang->line('chat_posted_success'), "chat_details" => $chat_details];
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('chat_posted_failed')]; }
    echo json_encode($this->response);
  }

  public function get_customer_details()
  {
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $detail = $this->api->get_customer_details($cust_id);
      if(empty($detail)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('customer_details_failed'), "profile_details" => array()]; 
      }
      else {
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('customer_details_success'), "profile_details" => $detail];
      }
      echo json_encode($this->response);
    }
  }
  
  public function register_new_support_request()
  { 
    //echo json_encode($_POST); die();
    $type = $this->input->post('type', TRUE);
    $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int) $cat_id;
    $description = $this->input->post('description', TRUE);
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
    $today = date('Y-m-d h:i:s');
    
    $deliverer_details = $this->api->get_customer_details($cust_id);

    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachment')) {   
        $uploads    = $this->upload->data();  
        $file_url =  $config['upload_path'].$uploads["file_name"];  
      } else {  $file_url = "NULL"; }
    } else {  $file_url = "NULL"; }

    $add_data = array(
      'type' => trim($type), 
      'description' => trim($description), 
      'file_url' => trim($file_url), 
      'cust_id' => (int) $cust_id, 
      'cre_datetime' => $today, 
      'response' => 'NULL', 
      'res_datetime' => 'NULL', 
      'status' => 'open', 
      'updated_by' => 0, 
      'order_id' => (int)$order_id, 
      'cat_id' => $cat_id, 
    );
    
    //echo json_encode($add_data); die();
    if( $id = $this->api->register_new_support($add_data)) {
      $ticket_id = 'G-'.time().$id;
      $this->api->update_support_ticket($id, $ticket_id);
  
      $subject = $this->lang->line('order_incident');
      $message ="<p class='lead'>".$this->lang->line('order_id')."</p><strong>".$order_id."</strong>";
      $message .="</td></tr><tr><td align='center'><br />";
      $message .="<a class='callout'>".$this->lang->line('incident_details')."<strong>".$description."</strong></a>";
      $username = 'Team Gonagoo!';

      $this->api_sms->send_email_text($username, 'contact@gonagoo.com', $subject, trim($message));
      
      $parts = explode("@", $deliverer_details['email1']);
      $deliverer_email = $parts[0];
      $this->api_sms->send_email_text($deliverer_email, $deliverer_details['email1'], $subject, trim($message));
      $this->response = ['response' => 'success', 'message'=> $this->lang->line('added_successfully')];  
    } else { $this->response = ['response' => 'failed','message'=> $this->lang->line('unable_to_add')]; }
    echo json_encode($this->response);
  }

  public function driver_confirm_payment()
  {
    $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
    $transaction_id = $this->input->post('cd_id', TRUE);
    $today = date('Y-m-d h:i:s');
    $order_details = $this->api->get_order_detail($order_id);
    $total_amount_paid = $order_details['order_price'];
    $transfer_account_number = "NULL";
    $bank_name = "NULL";
    $account_holder_name = "NULL";
    $iban = "NULL";
    $email_address = "NULL";
    $mobile_number = "NULL";    

    $update_data_order = array(
      "payment_method" => 'cod',
      "paid_amount" => trim($total_amount_paid),
      "transaction_id" => trim($transaction_id),
      "payment_datetime" => $today,
      "complete_paid" => "1",
      "pending_service_charges" => 0,
    );

    if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {
      $booking_details = $this->api->laundry_booking_details((int)$order_details['ref_no']);
      
      if($order_details['is_laundry'] == 1 && $order_details['laundry_is_drop'] == 0 && $booking_details['payment_mode'] != 'bank') {
        $booking_id = (int)$order_details['ref_no'];
        $post_fields ="booking_id=".$booking_id;
        //echo json_encode($post_fields); die();
        $ch = curl_init(base_url('api-laundry/laundry-booking-mark-complete-paid')); // url to send sms
        curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
        curl_setopt($ch, CURLOPT_POST, 1); // method to call url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
        curl_setopt($ch, CURLOPT_POSTFIELDS,$post_fields); 
        $courier_booking = curl_exec($ch); // execute url and save response
        curl_close($ch); // close url connection        
      } else {
        if($order_details['is_laundry'] == 1 && $booking_details['complete_paid'] == 0 && $booking_details['payment_mode'] != 'bank') { 
          $booking_id = (int)$order_details['ref_no'];
          $post_fields ="booking_id=".$booking_id;
          //echo json_encode($post_fields); die();
          $ch = curl_init(base_url('api-laundry/laundry-booking-mark-complete-paid')); // url to send sms
          curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
          curl_setopt($ch, CURLOPT_POST, 1); // method to call url
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
          curl_setopt($ch, CURLOPT_POSTFIELDS,$post_fields); 
          $courier_booking = curl_exec($ch); // execute url and save response
          curl_close($ch); // close url connection 
        } 
      }

      /*************************************** Payment Section ****************************************/
        /*
            //Add core price to gonagoo master
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($order_details['currency_sign']),
              );
              $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            }
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$order_details['cust_id'],
              "type" => 1,
              "transaction_type" => 'standard_price',
              "amount" => trim($order_details['standard_price']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'cod',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //Update urgent fee in gonagoo account master
            if(trim($order_details['urgent_fee']) > 0) {
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['cust_id'],
                "type" => 1,
                "transaction_type" => 'urgent_fee',
                "amount" => trim($order_details['urgent_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'cod',
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
            //Update insurance fee in gonagoo account master
            if(trim($order_details['insurance_fee']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['cust_id'],
                "type" => 1,
                "transaction_type" => 'insurance_fee',
                "amount" => trim($order_details['insurance_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'cod',
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
            //Update handling fee in gonagoo account master
            if(trim($order_details['handling_fee']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['cust_id'],
                "type" => 1,
                "transaction_type" => 'handling_fee',
                "amount" => trim($order_details['handling_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'cod',
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
            //Update vehicle fee in gonagoo account master
            if(trim($order_details['vehicle_fee']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['cust_id'],
                "type" => 1,
                "transaction_type" => 'vehicle_fee',
                "amount" => trim($order_details['vehicle_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'cod',
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
            //Update custome clearance fee in gonagoo account master
            if(trim($order_details['custom_clearance_fee']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['cust_id'],
                "type" => 1,
                "transaction_type" => 'custom_clearance_fee',
                "amount" => trim($order_details['custom_clearance_fee']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'cod',
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
            //Update loading Unloading in gonagoo account master
            if(trim($order_details['loading_unloading_charges']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['cust_id'],
                "type" => 1,
                "transaction_type" => 'loading_unloading_charges',
                "amount" => trim($order_details['loading_unloading_charges']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'cod',
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
        

        $cust_id = (int)$order_details['cust_id'];
        if($order_details['is_laundry'] == 0 ) {
          //Add payment details in customer account master and history
          if($this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        }


        //Deduct payment details from customer account master and history
        if($customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
        } else {
          $insert_data_customer_master = array(
            "user_id" => $cust_id,
            "account_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($order_details['currency_sign']),
          );
          $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
        }
        $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
        $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$order_id,
          "user_id" => (int)$cust_id,
          "datetime" => $today,
          "type" => 0,
          "transaction_type" => 'payment',
          "amount" => trim($total_amount_paid),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "withdraw_request_id" => 0,
          "currency_code" => trim($order_details['currency_sign']),
          "cat_id" => trim($order_details['category_id']),
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);
        */


        //Add payment details in Provider account master and history
        /*if($customer_account_master_details = $this->api->customer_account_master_details($order_details['deliverer_id'], trim($order_details['currency_sign']))) {
        } else {
          $insert_data_customer_master = array(
            "user_id" => $order_details['deliverer_id'],
            "account_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($order_details['currency_sign']),
          );
          $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
          $customer_account_master_details = $this->api->customer_account_master_details($order_details['deliverer_id'], trim($order_details['currency_sign']));
        }
        $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
        $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $order_details['deliverer_id'], $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($order_details['deliverer_id'], trim($order_details['currency_sign']));
        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$order_id,
          "user_id" => (int)$order_details['deliverer_id'],
          "datetime" => $today,
          "type" => 1,
          "transaction_type" => 'delivered',
          "amount" => trim($total_amount_paid),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "withdraw_request_id" => 0,
          "currency_code" => trim($order_details['currency_sign']),
          "cat_id" => trim($order_details['category_id']),
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);*/

      /*************************************** Payment Section ****************************************/
      //success responce
      $this->response = ['response' => 'success', 'message'=> $this->lang->line('payment_success')];
    } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('payment_failed')]; }
    echo json_encode($this->response);
  }

  public function driver_order_pickup()
  {
    //echo json_encode($_POST); die();
    $cd_id = $this->input->post('cd_id', TRUE);
    $order_id = $this->input->post('order_id', TRUE);
    $order_status = $this->input->post('order_status', TRUE);
    $cust_id = $this->input->post('cust_id', TRUE);
    $deliverer_id = $this->input->post('deliverer_id', TRUE);
    $pickup_code = $this->input->post('pickup_code', TRUE);
    $order_details = $this->commonapi->get_order_detail($order_id);
    if($order_details['pickup_code'] == trim($pickup_code)) {
      $customer_fcm_ids = array();
      $customer_apn_ids = array();
      if ( trim($order_status) == 'in_progress' ){
        //var_dump($order_status); die();
        if( $this->user->update_order_status($cd_id, $order_id, $order_status) ){
          $this->commonapi->insert_order_status($cd_id, $order_id, $order_status);
          //Get customer and deliverer contact and personal details
          $customer_details = $this->commonapi->get_user_details($order_details['cust_id']);
          $deliverer_details = $this->commonapi->get_user_details($deliverer_id);
          //Get Users device Details
          $device_details_customer = $this->commonapi->get_user_device_details($cust_id);
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          if(!empty($device_details_customer)) {
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {
                array_push($arr_customer_fcm_ids, $value['reg_id']);
              } else {
                array_push($arr_customer_apn_ids, $value['reg_id']);
              }
            }
            //$customer_fcm_ids = $arr_customer_fcm_ids;
            $customer_apn_ids = $arr_customer_apn_ids;
          }
          
          //Get Deliverer Device ID's
          $device_details_deleverer = $this->commonapi->get_user_device_details($deliverer_id);
          $arr_deleverer_fcm_ids = array();
          $arr_deleverer_apn_ids = array();
          if(!empty($device_details_deleverer)) {
            //var_dump($device_details_deleverer); die();
            foreach ($device_details_deleverer as $value) {
              if($value['device_type'] == 1) {
                array_push($arr_deleverer_fcm_ids, $value['reg_id']);
              } else {
                array_push($arr_deleverer_apn_ids, $value['reg_id']);
              }
            }
            //$deleverer_fcm_ids = $arr_deleverer_fcm_ids;
            $deleverer_apn_ids = $arr_deleverer_apn_ids;
          }
          
          //Get PN API Keys and PEM files
          $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
          $api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

          //SMS confirmation
          $sms_msg = $this->lang->line('driver_order_msg') . $order_id . $this->lang->line('driver_inprogress_order_msg');
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->commonapi->sendSMS($this->commonapi->get_country_code_by_id(trim($customer_details['country_id'])).trim($mobile1), $sms_msg);
          
          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->commonapi->sendSMS($this->commonapi->get_country_code_by_id(trim($deliverer_details['country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg);

          //Email confirmation deliverer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_inprogress_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          //Email confirmation Customer
          $subject = $this->lang->line('driver_order_update_email_subject') . $order_id;
          $message ="<p class='lead'>".$this->lang->line('dear'). trim($customer_details['firstname']) . " " . trim($customer_details['lastname']) . "</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_order_msg'). $order_id . $this->lang->line('driver_inprogress_order_msg') . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
          $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject, trim($message));
          $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject, trim($message));
          //Push Notification FCM
          $msg =  array('title' => $subject, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg);
          if(!empty($arr_customer_fcm_ids)) { $this->commonapi->sendFCM($msg, $arr_customer_fcm_ids, $api_key_deliverer); }
          //Push Notification APN
          $msg_apn_customer =  array('title' => $subject, 'text' => $sms_msg);
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          
          //Update to workroom
          $this->commonapi->post_to_workroom($order_id, $cust_id, $deliverer_id, $sms_msg, 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
          //check order of dedicated users
          if(trim($order_details['is_bank_payment']) == 0) {
            //Payment Module-------------------------------------------------------------------------------------
            $today = date('Y-m-d h:i:s');
            if(trim($order_details['pickup_payment']) > 0) {
              //deduct pickup_payment details in gonagoo account master and history
              $gonagoo_master_details = $this->commonapi->gonagoo_master_details(trim($order_details['currency_sign']));
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($order_details['pickup_payment']);
              $this->commonapi->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->commonapi->gonagoo_master_details($order_details['currency_sign']);

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$deliverer_id,
                "type" => 0,
                "transaction_type" => 'pickup_payment',
                "amount" => trim($order_details['pickup_payment']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->commonapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

              //Add pickup_payment details in deliverer account master and history
              $customer_account_master_details = $this->commonapi->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($order_details['pickup_payment']);
              $this->commonapi->update_payment_in_customer_master($customer_account_master_details['account_id'], $deliverer_id, $account_balance);
              $customer_account_master_details = $this->commonapi->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));

              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$deliverer_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'pickup_payment',
                "amount" => trim($order_details['pickup_payment']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->commonapi->insert_payment_in_account_history($update_data_account_history);
            
              //Deduct pickup amount from scrow and history
              $deliverer_scrow_master_details = $this->commonapi->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));
              $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) - trim($order_details['pickup_payment']);
              $this->commonapi->update_payment_in_deliverer_scrow((int)$deliverer_scrow_master_details['scrow_id'], $deliverer_id, $scrow_balance);
              $deliverer_scrow_master_details = $this->commonapi->deliverer_scrow_master_details($deliverer_id, trim($order_details['currency_sign']));

              $update_data_scrow_history = array(
                "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                "order_id" => (int)$order_id,
                "deliverer_id" => (int)$deliverer_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'pickup_payment',
                "amount" => trim($order_details['pickup_payment']),
                "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->commonapi->insert_payment_in_scrow_history($update_data_scrow_history);
            }
            //post payment in workroom
            $this->commonapi->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('pickup_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
            //Payment Module End --------------------------------------------------------------------------------
          }
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success')];
        } else {  $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      } else {  $this->response = ['response' => 'failed', 'message'=> $this->lang->line('update_failed')]; }
    } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('Pickup code is invalid!')]; }
    echo json_encode($this->response);
  }

  //Ticket Booking WSs-----------------------------
    public function driver_trips_all_filter()
    {
      //echo json_encode($_POST); die();
      $bus_id = $this->input->post('bus_id', TRUE); $bus_id = (int)$bus_id;
      $last_id = $this->input->post('last_id', TRUE); $last_id = (int)$last_id;
      $trip_source = $this->input->post('trip_source', TRUE); 
      $trip_destination = $this->input->post('trip_destination', TRUE);
      $journey_date = $this->input->post('journey_date', TRUE); 
      $status = $this->input->post('status', TRUE);
      if( $trip_list = $this->commonapi_bus->get_driver_trip_master_list_filter($bus_id, $status, $trip_source, $trip_destination ,$journey_date, $last_id, 1)) {
        for($i=0; $i < sizeof($trip_list); $i++) {
          $seat_type = "";
          $available = "";
          $seat_price = "";
          $seat_details =$this->commonapi_bus->get_operator_bus_seat_details($trip_list[$i]['bus_id']);
          $trip_master_details = $this->commonapi_bus->get_trip_master_details($trip_list[$i]['trip_id']);
          if( $trip_list[$i]['journey_date'] > $trip_master_details[0]['special_rate_start_date'] && $trip_list[$i]['journey_date'] < $trip_master_details[0]['special_rate_end_date']){
            $seat_type_price = explode(',', $trip_master_details[0]['special_price']);
          } else {
            $seat_type_price = explode(',', $trip_master_details[0]['seat_type_price']);  
          }
          for($j=0; $j < sizeof($seat_details); $j++){
            //$trip_list[$i] += [ "total_".$seat_details[$j]['type_of_seat'] => $seat_details[$j]['total_seats'] , $seat_details[$j]['type_of_seat']."_price" => $seat_type_price[$j] ];
            if($seat_type==""){ $seat_type .= $seat_details[$j]['type_of_seat'];}else{ $seat_type .= ",".$seat_details[$j]['type_of_seat'];}
            if($available==""){ $available .= $seat_details[$j]['total_seats'];}else{ $available .= ",".$seat_details[$j]['total_seats'];}
            if($seat_price==""){ $seat_price .= $seat_type_price[$j];}else{ $seat_price .= ",".$seat_type_price[$j];}
          }
          $trip_list[$i] += [ "type_of_seats" => $seat_type , "prices_of_seats" => $seat_price , "available_seats" => $available ];
      
          //$trip_list[$i] += ['seat_type'=>$seat_type]; 
          //trip distance-----------------------------------------------
            $pickup_lat_long_string = $this->commonapi_bus->get_loc_lat_long_by_id($trip_list[$i]['source_point_id']);
            $pickup_lat_long_array = explode(',',$pickup_lat_long_string);
            $drop_lat_long_string = $this->commonapi_bus->get_loc_lat_long_by_id($trip_list[$i]['destination_point_id']);
            $drop_lat_long_array = explode(',',$drop_lat_long_string);
          
            if(sizeof($pickup_lat_long_array) == 2 && sizeof($drop_lat_long_array) == 2) {
              $trip_list[$i] += [ "trip_distance" => round($this->commonapi_bus->GetDrivingDistance($pickup_lat_long_array[0],$pickup_lat_long_array[1],$drop_lat_long_array[0],$drop_lat_long_array[1]),2).'KM'];
            } else {
              $trip_list[$i] += [ "trip_distance" => "N/A" ];
            }
          //trip distance-----------------------------------------------
          //trip duration-----------------------------------------------
            if($trip_master_details = $this->commonapi_bus->get_trip_master_details($trip_list[$i]['trip_id'])){
              $trip_list[$i] += [ "trip_duration" => $trip_master_details[0]['trip_duration']]; 
            } else {
             $trip_list[$i] += [ "trip_duration" => "N/A" ]; 
            }
          //trip duration-----------------------------------------------
          //bus details-------------------------------------------------
            if($bus_details = $this->commonapi_bus->get_operator_bus_details($trip_list[$i]['bus_id'])){
             $trip_list[$i] += [ "bus_amenities" =>str_replace('_',' ',str_replace(',',', ',$bus_details['bus_amenities'])), "vehicle" => $bus_details['bus_no'].' - '.$bus_details['bus_ac']. ', ' .$bus_details['bus_seat_type'], "bus_image_url" => $bus_details['bus_image_url'], "bus_icon_url" => $bus_details['bus_icon_url']]; 
            }
          //bus details------------------------------------------------- 
          //Operator Details--------------------------------------------
            $operator_details = $this->commonapi_bus->get_bus_operator_profile($trip_list[$i]['cust_id']);
            $trip_list[$i] += array( 'opr_company_name' => $operator_details['company_name'], 'opr_firstname' => $operator_details['firstname'], 'opr_lastname' => $operator_details['lastname'], 'opr_email_id' => $operator_details['email_id'], 'opr_contact_no' => $operator_details['contact_no'], 'opr_avatar_url' => $operator_details['avatar_url'], 'opr_cover_url' => $operator_details['cover_url'] );
          //------------------------------------------------------------
          //cancelled trips---------------------------------------------
            $p_name = "";
            $p_contact = "";
            $p_gender = "";
            $total_no_seat = 0;

            if($seats = $this->commonapi_bus->get_ticket_seat_details_unique_id($trip_list[$i]['unique_id'])) { 
              foreach ($seats as $seat) {
                if($p_name == ""){ $p_name .= $seat['firstname']." ".$seat['lastname']; }
                else{ $p_name .= ",".$seat['firstname']." ".$seat['lastname']; }

                if($p_contact == ""){ $p_contact .= $this->commonapi_bus->get_country_code_by_id($seat['country_id']).$seat['mobile']; }
                else{ $p_contact .= ",". $this->commonapi_bus->get_country_code_by_id($seat['country_id']).$seat['mobile']; }                  

                if($p_gender == ""){ $p_gender .= $seat['gender']; }
                else{ $p_gender .= ",".$seat['gender']; }
              }
              $trip_list[$i] += [ "passengers_name" => $p_name , "passengers_contact" => $p_contact , "passengers_gender" => $p_gender , "total_no_of_passengers" => sizeof($seats) ];
            } 
          //cancelled trips---------------------------------------------                                
        }
        $this->response = ['response' => 'success','message'=> $this->lang->line('trip list found'), "driver_trip" => $trip_list];
      } else {
        $empty = array();
        $this->response = ['response' => 'failed','message'=> $this->lang->line('trip list not found'), "driver_trip" => $empty];
      }
      echo json_encode($this->response);
    }
    public function driver_trip_status_update()
    {
      $trip_id = $this->input->post('trip_id',true);
      $status = $this->input->post('status',true);
      $today = date('Y-m-d h:i:s');
      if($tlist = $this->commonapi_bus->get_trip_booking_list($trip_id)){
        if($status == 'start') { $data = array( 'trip_status' => $status, 'start_datetime' => $today );
        } else { $data = array( 'trip_status' => $status, 'end_datetime' => $today ); } 
        if($this->commonapi_bus->update_booking_details_by_unique_id($data , $tlist[0]['unique_id'])){
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('Status updated.')];
        } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('Error while upating trip status.')]; }
      } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('trip does not exist')]; }
      echo json_encode($this->response);
    }
  //-----------------------------------------------

}

/* End of file Driver_api.php */
/* Location: ./application/controllers/Driver_api.php */