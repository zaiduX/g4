<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider_offer_manage extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Provider_offer_manage_model', 'offer');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$user = $this->admin->logged_in_user_details($user['type_id']);
			$permissions = $this->admin->get_auth_permissions($id);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	public function index() {
		//echo json_encode($_POST); die();
    $offers = $this->offer->get_service_provider_offers(0);
    $this->load->view('admin/services/provider_offers_list_view', compact('offers'));
		$this->load->view('admin/footer_view');
	}
	public function activate_offer() {
		$offer_id = $this->input->post('offer_id', TRUE);
		$update_data = array(
 			"admin_approval" => 1,
 			"update_datetime" => date("Y-m-d H:i:s"),
 		);
		if($this->offer->update_offer_details($offer_id, $update_data)) { echo 'success';
		} else { echo 'failed';	}
	}
	public function de_activate_offer() {
		$offer_id = $this->input->post('offer_id', TRUE);
		$update_data = array(
 			"admin_approval" => 2,
 			"update_datetime" => date("Y-m-d H:i:s"),
 		);
		if($this->offer->update_offer_details($offer_id, $update_data)) { echo 'success';
		} else { echo 'failed';	}
	}
	public function active_offers() {
		//echo json_encode($_POST); die();
    $offers = $this->offer->get_service_provider_offers(1);
    $this->load->view('admin/services/provider_active_offers_list_view', compact('offers'));
		$this->load->view('admin/footer_view');
	}
	public function rejected_offers() {
		//echo json_encode($_POST); die();
    $offers = $this->offer->get_service_provider_offers(2);
    $this->load->view('admin/services/provider_rejected_offers_list_view', compact('offers'));
		$this->load->view('admin/footer_view');
	}
	public function offer_details() {
    //echo json_encode($_POST); die();
    $offer_id = $this->input->post('offer_id'); $offer_id = (int)$offer_id;
    $offer_details = $this->offer->get_service_provider_offer_details($offer_id);
    $offer_reviews = $this->offer->get_offer_reviews($offer_id);
    //echo json_encode($offer_reviews); die();
    //echo json_encode($this->db->last_query()); die();
    $operator = $this->offer->get_service_provider_profile($offer_details['cust_id']);
    $this->load->view('admin/services/service_provider_offer_details_view', compact('offer_details','operator','offer_reviews'));
    $this->load->view('admin/footer_view');
  }
}

/* End of file Provider_offer_manage.php */
/* Location: ./application/controllers/Provider_offer_manage.php */