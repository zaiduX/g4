<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');		
			$this->load->model('notification_model', 'notification');		
			$this->load->model('users_model', 'users');
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));

		} else{ redirect('admin');}
	}
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}
	public function notification_list()
	{			
			$notification_list = $this->notification->get_notifications();
			// echo $this->db->last_query(); die();
			$this->load->view('admin/notification_list_view',compact('notification_list'));
			$this->load->view('admin/footer_view');
	}

	public function notification_detail()
	{
		$notify_id = $this->uri->segment(3);
		$notification_detail = $this->notification->get_notification_detail((int)$notify_id);
		$this->load->view('admin/notification_detail_view',compact('notification_detail'));
		$this->load->view('admin/footer_view');
	}

	public function send_notification()
	{
		$countries = $this->notification->get_customer_countries();
		$this->load->view('admin/send_notification_view',compact("countries"));
		$this->load->view('admin/footer_view');	
	}

	public function get_states()
	{
		$country_id = $this->input->post('country_id', TRUE);
		$states = $this->notification->get_customer_states((int)$country_id);
		echo json_encode($states);
	}

	public function get_cities()
	{
		$state_id = $this->input->post('state_id', TRUE);
		$cities = $this->notification->get_customer_cities((int)$state_id);
		echo json_encode($cities);
	}

	public function notification_send()
	{
		$consumer_type = $this->input->post("consumer_type"); $consumer_type = (int) $consumer_type;

		$country_id = $this->input->post("country_id"); $country_id = (int) $country_id;
		$state_id = $this->input->post("state_id"); 		$state_id = (int) $state_id;
		$city_id = $this->input->post("city_id");				$city_id = (int) $city_id;

		$notify_title = $this->input->post("title");
		$notify_text = $this->input->post("message");
		$notify_text = nl2br($notify_text);

		$consumer_filter = "All";
		$filter_by = "None";
		$filter_value = "None";

		$today = date('Y-m-d H:i:s');

		if($consumer_type == 1 ) { $consumer_filter = "Individuals"; }
		else if($consumer_type == 2 ) { $consumer_filter = "Business"; }
		else { $consumer_filter = "All"; }

		if($country_id > 0 ) { $filter_by = "Location";	}

		if( !empty($_FILES["attachement"]["tmp_name"]) ) {
			$config['upload_path']    = 'resources/attachements/';                 
  		$config['allowed_types']  = '*';       
  		$config['max_size']       = '0';                          
	 		$config['max_width']      = '0';                          
  		$config['max_height']     = '0';                          
  		$config['encrypt_name']   = true; 
  		$this->load->library('upload', $config);
  		$this->upload->initialize($config);

  		if($this->upload->do_upload('attachement')) {   
    		$uploads    = $this->upload->data();  
    		$attachement_url =  $config['upload_path'].$uploads["file_name"];    		
    	} else { $attachement_url = "NULL";	}
		} else { $attachement_url = "NULL";	}
	
		$insert_data = array(
			"notify_title" => trim($notify_title),
			"notify_text" => $notify_text,
			"attachement_url" => $attachement_url,
			"consumer_filter" => $consumer_filter,
			"filter_by" => $filter_by,
			"country_id" => $country_id,
			"state_id" => $state_id,
			"city_id" => $city_id,
			"consumer_id" => 0,
			"cre_datetime" => $today
		);

		if($this->notification->register_notification($insert_data)){
			$filter = array("consumer" => $consumer_filter, "country_id" => $country_id,"state_id" => $state_id,"city_id" => $city_id,);
			$message = array("title" => trim($notify_title),"text" => trim($notify_text),"attachement_url" => $attachement_url,"datetime" => $today	);

			//  FCM
			$regids = $this->notification->get_android_fcmids($filter);
			if(is_array($regids)) { $this->notification->sendFCM($message,$regids); }

			// APN
			$tokens = $this->notification->get_ios_tokens($filter);
			if(is_array($tokens)) { $tokens = implode(',', $tokens); $this->notification->sendPushIOS($message,$tokens); }

			$this->session->set_flashdata('success','Notification successfully sent!');
		} else { 	$this->session->set_flashdata('error','Unable to send the notification! Try again...');	}

		redirect('admin/send-notification');
	}

	public function send_personal_notification()
	{
		$user_id = $this->input->post("user_id"); $user_id = (int) $user_id;

		$notify_title = $this->input->post("title");
		$notify_text = $this->input->post("message");
		$notify_text = nl2br($notify_text);

		$user = $this->users->get_users_detail($user_id);


		$consumer_filter = "Personal";
		$filter_by = "None";
		$filter_value = "None";

		$today = date('Y-m-d H:i:s');

		if($user["user_type"] == 1 ) { $consumer_filter = "Individuals"; }
		else { $consumer_filter = "Business"; }

		if($user['country_id'] > 0 ) { $filter_by = "Location";	}

		if( !empty($_FILES["attachement"]["tmp_name"]) ) {
			$config['upload_path']    = 'resources/attachements/';                 
  		$config['allowed_types']  = '*';       
  		$config['max_size']       = '0';                          
	 		$config['max_width']      = '0';                          
  		$config['max_height']     = '0';                          
  		$config['encrypt_name']   = true; 
  		$this->load->library('upload', $config);
  		$this->upload->initialize($config);

  		if($this->upload->do_upload('attachement')) {   
    		$uploads    = $this->upload->data();  
    		$attachement_url =  $config['upload_path'].$uploads["file_name"];    		
    	} else { $attachement_url = "NULL";	}
		} else { $attachement_url = "NULL";	}
	
		$insert_data = array(
			"notify_title" => trim($notify_title),
			"notify_text" => $notify_text,
			"attachement_url" => $attachement_url,
			"consumer_filter" => $consumer_filter,
			"filter_by" => $filter_by,
			"country_id" => $user['country_id'],
			"state_id" => $user['state_id'],
			"city_id" => $user['city_id'],
			"consumer_id" => $user_id,
			"cre_datetime" => $today
		);

		if($this->notification->register_notification($insert_data)){
			$message = array("title" => trim($notify_title),"text" => trim($notify_text),"attachement_url" => $attachement_url,"datetime" => $today	);
			
			//  FCM
			$regids = $this->notification->get_users_android_fcmids($user_id);
			if(is_array($regids)) { $this->notification->sendFCM($message,$regids); }

			// APN
			$tokens = $this->notification->get_users_ios_tokens($user_id);

			if(is_array($tokens)) { $tokens = implode(',', $tokens); $this->notification->sendPushIOS($message,$tokens); }


			$this->session->set_flashdata('success','Notification successfully sent!');
		} else { 	$this->session->set_flashdata('error','Unable to send the notification! Try again...');	}

		redirect('admin/view-users/'.$user_id);
	}

      public function testing()
      {
		
      }



}

/* End of file Notification.php */
/* Location: ./application/controllers/Notification.php */