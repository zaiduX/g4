<?php
/**
 * Start @ Shaz
 */
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Api_laundry extends CI_Controller {
  private $errors = array();
  private $cust = array();
  public function __construct()
  {
    parent::__construct();
    $this->load->model('api_model_laundry', 'api');   
    $this->load->model('Notification_model_bus', 'notification');  
    $this->load->model('Web_user_model_bus', 'user');
    $this->load->model('api_model', 'api_courier');
    $this->load->model('api_model_sms', 'api_sms');
    $this->load->model('Users_model', 'users');
    $this->load->model('transport_model', 'transport');


    header('Content-Type: Application/json'); 
    if( $this->input->method() != 'post' ) exit('No direct script access allowed');
    if( $this->input->is_ajax_request() ) exit('No direct script access allowed');

    $cust_id = $this->input->post('cust_id', TRUE);    
    $this->api->update_last_login($cust_id);
    if(!empty($cust_id)){  $this->cust = $this->api->get_consumer_datails($cust_id, true); }

    if(isset($_POST)){

      $lang = $this->input->post('lang', TRUE);
      if( $lang == 'fr' ) {
        $this->config->set_item('language', 'french');
        $this->lang->load('frenchApi_lang','french');
      }
      else if( $lang == 'sp' ) {
        $this->config->set_item('language', 'spanish');
        $this->lang->load('spanishApi_lang','spanish');
      }
      else{
        $this->config->set_item('language', 'english');
        $this->lang->load('englishApi_lang','english');
        $this->lang->load('englishFront_lang','english');
      }

      foreach($_POST as $key => $value){ 
        if(trim($value) == "") { $this->errors[] = ucwords(str_replace('_', ' ', $key)); }        
      }
      if(!empty($this->errors)) { 
        echo json_encode(array('response' => 'failed', 'message' => 'Empty field(s) - '.implode(', ', $this->errors))); 
      }
    }
  }
  public function index() { exit('No direct script access allowed.'); }

  //start Laundry Provider Profile-------------------------
    public function get_laundry_operator_profile()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->input->post('cust_id', TRUE);
      $today = date("Y-m-d H:i:s");
      $detail = array();
      if($this->api->is_user_active_or_exists($cust_id)) {
        if($profile = $this->api->get_laundry_provider_profile($cust_id)) {
          $detail['personal'] = $profile;

          if($provider_photos = $this->api->get_laundry_provider_business_photos($cust_id)) { $detail['provider_photos'] = (string)sizeof($provider_photos); }

          if($provider_documents = $this->api->get_laundry_provider_documents($cust_id) ) { $detail['provider_documents'] = (string)sizeof($provider_documents); }

          if($special_laundry_charges = $this->api->get_laundry_special_charges_list($cust_id) ) { $detail['special_laundry_charges'] = (string)sizeof($special_laundry_charges); }
          
          if($laundry_charges = $this->api->get_laundry_charges_list($cust_id) ) { $detail['laundry_charges'] = (string)sizeof($laundry_charges); }

          if($cancellation_charges = $this->api->get_laundry_provider_cancellation_details($cust_id) ) { $detail['cancellation_charges'] = (string)sizeof($cancellation_charges); }

          $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'),"profile_details" => $detail];

          //$this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "profile" => $profile];
        } else {
          if($deliverer = $this->api->get_deliverer_profile($cust_id)) {
            $insert_data = array(
              "firstname" => trim($deliverer['firstname']),
              "lastname" => trim($deliverer['lastname']),
              "email_id" => trim($deliverer['email_id']),
              "contact_no" => trim($deliverer['contact_no']),
              "address" => trim($deliverer['address']),
              "zipcode" => trim($deliverer['zipcode']),
              "promocode" => trim($deliverer['promocode']),
              "carrier_type" => trim(strtolower($deliverer['carrier_type'])),
              "country_id" => (int)($deliverer['country_id']),
              "state_id" => (int)($deliverer['state_id']),
              "city_id" => (int)($deliverer['city_id']),
              "cust_id" => (int)($deliverer['cust_id']),
              "cre_datetime" => trim($today),
              "company_name" => trim($deliverer['company_name']),
              "introduction" => trim($deliverer['introduction']),
              "latitude" => trim($deliverer['latitude']),
              "longitude" => trim($deliverer['longitude']),
              "gender" => trim($deliverer['gender'])
              //"service_list" => trim($service_list),
            );
            $profile_id = $this->api->register_laundry_provider_profile($insert_data);
            $profile = $this->api->get_laundry_provider_profile($cust_id);

            $detail['personal'] = $profile;

            if($provider_photos = $this->api->get_laundry_provider_business_photos($cust_id)) { $detail['provider_photos'] = (string)sizeof($provider_photos); }

            if($provider_documents = $this->api->get_laundry_provider_documents($cust_id) ) { $detail['provider_documents'] = (string)sizeof($provider_documents); }

            if($laundry_charges = $this->api->get_laundry_charges_list($cust_id) ) { $detail['laundry_charges'] = (string)sizeof($laundry_charges); }

            if($special_laundry_charges = $this->api->get_laundry_special_charges_list($cust_id) ) { $detail['special_laundry_charges'] = (string)sizeof($special_laundry_charges); }

            if($cancellation_charges = $this->api->get_laundry_provider_cancellation_details($cust_id) ) { $detail['cancellation_charges'] = (string)sizeof($cancellation_charges); }

            $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "profile_details" => $detail];

            //$this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "profile" => $profile];

          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user also not found in deliverer profile'), "profile" => array()]; }
        }
        // update user last login
        $this->api->update_last_login($cust_id);
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
      echo json_encode($this->response);
    }
    public function register_laundry_provider()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          $firstname = $this->input->post('firstname', TRUE);
          $lastname = $this->input->post('lastname', TRUE);
          $email_id = $this->input->post('email_id', TRUE);
          $contact_no = $this->input->post('contact_no', TRUE);
          $carrier_type = $this->input->post('carrier_type', TRUE);
          $address = $this->input->post('address', TRUE);
          $country_id = $this->input->post('country_id', TRUE);
          $state_id = $this->input->post('state_id', TRUE);
          $city_id = $this->input->post('city_id', TRUE);
          $zipcode = $this->input->post('zipcode', TRUE);
          $promocode = $this->input->post('promocode', TRUE);
          $company_name = $this->input->post('company_name', TRUE);
          $introduction = $this->input->post('introduction', TRUE);
          $latitude = $this->input->post('latitude', TRUE);
          $longitude = $this->input->post('longitude', TRUE);
          $gender = $this->input->post('gender', TRUE);
          //$service_list = $this->input->post('service_list', TRUE);
          $today = date("Y-m-d H:i:s");

          if($this->api->get_laundry_provider_profile($cust_id)) {
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('Profile already exists.')];
            echo json_encode($this->response);
            die();
          }
          if($this->api->is_laundry_provider_email_exists($email_id)) {  
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_exists')];
          } else {
            $insert_data = array(
              "firstname" => trim($firstname),
              "lastname" => trim($lastname),
              "email_id" => trim($email_id),
              "contact_no" => trim($contact_no),
              "address" => trim($address),
              "zipcode" => trim($zipcode),
              "promocode" => trim($promocode),
              "carrier_type" => trim(strtolower($carrier_type)),
              "country_id" => (int)($country_id),
              "state_id" => (int)($state_id),
              "city_id" => (int)($city_id),
              "cust_id" => (int)($cust_id),
              "cre_datetime" => trim($today),
              "company_name" => trim($company_name),
              "introduction" => trim($introduction),
              "latitude" => trim($latitude),
              "longitude" => trim($longitude),
              "gender" => trim($gender)
              //"service_list" => trim($service_list),
            );
            if( $this->api->register_laundry_provider_profile($insert_data)) {
              // make customer as a deliverer, update flag
              $this->api->make_consumer_deliverer($cust_id);
              $profile = $this->api->get_laundry_provider_profile($cust_id);
              $this->response = ['response' => 'success','message'=> $this->lang->line('register_success'), "profile" => $profile];
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('register_failed'), "profile" => array()]; } 
          } 
        } else {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; 
        } 
        echo json_encode($this->response);
      }
    }
    public function update_laundry_provider()
    {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE);
        if($this->api->is_user_active_or_exists($cust_id)) {
          $firstname = $this->input->post('firstname', TRUE);
          $lastname = $this->input->post('lastname', TRUE);
          $email_id = $this->input->post('email_id', TRUE);
          $contact_no = $this->input->post('contact_no', TRUE);
          $carrier_type = $this->input->post('carrier_type', TRUE);
          $address = $this->input->post('address', TRUE);
          $country_id = $this->input->post('country_id', TRUE);
          $state_id = $this->input->post('state_id', TRUE);
          $city_id = $this->input->post('city_id', TRUE);
          $zipcode = $this->input->post('zipcode', TRUE);
          $promocode = $this->input->post('promocode', TRUE);
          $company_name = $this->input->post('company_name', TRUE);
          $introduction = $this->input->post('introduction', TRUE);
          $latitude = $this->input->post('latitude', TRUE);
          $longitude = $this->input->post('longitude', TRUE);
          $gender = $this->input->post('gender', TRUE);
          $service_list = $this->input->post('service_list', TRUE);
          $today = date("Y-m-d H:i:s");

          $update_data = array(
            "firstname" => trim($firstname),
            "lastname" => trim($lastname),
            "email_id" => trim($email_id),
            "contact_no" => trim($contact_no),
            "address" => trim($address),
            "zipcode" => trim($zipcode),
            "promocode" => trim($promocode),
            "carrier_type" => trim(strtolower($carrier_type)),
            "country_id" => (int)($country_id),
            "state_id" => (int)($state_id),
            "city_id" => (int)($city_id),
            "company_name" => trim($company_name),
            "introduction" => trim($introduction),
            "latitude" => trim($latitude),
            "longitude" => trim($longitude),
            "gender" => trim($gender),
            "service_list" => trim($service_list),
          );

          if($this->api->update_provider($update_data, $cust_id)){
            $profile = $this->api->get_laundry_provider_profile($cust_id);
            $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed'), "profile" => array()]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function update_laundry_provider_avatar()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE);
        if($this->api->is_user_active_or_exists($cust_id)) {
          $profile = $this->api->get_laundry_provider_profile($cust_id);
          if(!empty($_FILES["profile_image"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/profile-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('profile_image')) {   
              $uploads    = $this->upload->data();  
              $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
              if($profile['avatar_url'] != "NULL") {
                unlink(trim($profile['avatar_url']));
              }
            }      
            if($this->api->update_laundry_provider_avatar($avatar_url, $cust_id)) {
              $profile = $this->api->get_laundry_provider_profile($cust_id);
              $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed'), "profile" => array()]; }
          } 
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function update_laundry_provider_cover()
    {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE);
        if($this->api->is_user_active_or_exists($cust_id)) {
          $profile = $this->api->get_laundry_provider_profile($cust_id);
          if( !empty($_FILES["cover_image"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/cover-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('cover_image')) {   
              $uploads    = $this->upload->data();  
              $cover_url =  $config['upload_path'].$uploads["file_name"];  
              if($profile['cover_url'] != "NULL") {
                unlink(trim($profile['cover_url']));
              }
            }
            if( $this->api->update_laundry_provider_cover($cover_url, $cust_id)) {
              $profile = $this->api->get_laundry_provider_profile($cust_id);
              $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
            } else {
              $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')];
            }
          } else {
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; 
          }
          // update user last login
          $this->api->update_last_login($cust_id);
        } else {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"];
        }
        echo json_encode($this->response);
      }
    }
  //end Laundry Provider Profile---------------------------

  //start Laundry Provider Business Photos-----------------
    public function get_laundry_provider_business_photos()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)) {
        if($photos = $this->api->get_laundry_provider_business_photos($cust_id)){
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "user_business_photos" => $photos];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "user_business_photos" => array()]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
      echo json_encode($this->response);
    }
    public function delete_laundry_provider_business_photo()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)) {
        $photo_id = $this->input->post('photo_id', TRUE);
        if($this->api->delete_laundry_provider_business_photo($photo_id)) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
      echo json_encode($this->response);
    }
    public function upload_laundry_provider_business_photo()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)) {
        if(!empty($_FILES["business_photo"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/business-photos/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('business_photo')) {   
            $uploads    = $this->upload->data();  
            $photo_url =  $config['upload_path'].$uploads["file_name"];  
          }      
          if($photo_id = $this->api->add_laundry_provider_business_photo($photo_url, $cust_id)) {          
            $this->response=['response'=>'success','message'=> $this->lang->line('add_success'), "photo_id" => $photo_id, "photo_url" => trim($photo_url)];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
      echo json_encode($this->response);
    }
  //end Laundry Provider Business Photos-------------------

  //start Laundry Provider Documents-----------------------
    public function get_laundry_provider_document()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        if( $docs = $this->api->get_laundry_provider_documents($cust_id) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_documenets" => $docs];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "user_documenets" => array()]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function add_laundry_provider_document()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)) {
        $doc_type = $this->input->post('doc_type', TRUE);
        $deliverer_id = (int) $this->user->get_deliverer_id($cust_id);
        if( !empty($_FILES["attachment"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/attachements/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('attachment')) {   
            $uploads    = $this->upload->data();  
            $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
          } else {
            $attachement_url = "NULL";
          }
          $register_data = array(
            "attachement_url" => trim($attachement_url), 
            "cust_id" => $cust_id, 
            "deliverer_id" => $deliverer_id, 
            "is_verified" => "0", 
            "doc_type" => $doc_type, 
            "is_rejected" => "0",
            "upload_datetime" => date('Y-m-d H:i:s'),
            "verify_datetime" => "NULL",
          );
          if( $this->api->register_operator_document($register_data) ) {
            $this->response = ['response' => 'success','message'=> $this->lang->line('add_success')];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function delete_laundry_provider_document()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)) {
        $doc_id = $this->input->post('doc_id', TRUE);  $doc_id = (int) $doc_id;
        $documents = $this->api->get_old_operator_documents($doc_id);
        if( $this->api->delete_operator_document($doc_id) ) {
          unlink(trim($documents[0]['attachement_url']));
          $this->response = ['response' => 'success','message'=> $this->lang->line('remove_success')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
  //END Laundry Provider Documents-------------------------

  //Start Manage Laundry charges---------------------------
    public function laundry_charges_list()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE);
        $last_id = $this->input->post('last_id', TRUE);
        $cust_id = (int) $cust_id;
        if( $list = $this->api->get_laundry_charges_list($cust_id, $last_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $list];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function laundry_charges_category_list()
    {
      if(empty($this->errors)) { 
        if( $list = $this->api->get_laundry_category_list() ) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $list];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function laundry_charges_sub_category_list()
    {
      if( $list = $this->api->get_laundry_sub_category_list(1, 1)) {
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $list];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
      echo json_encode($this->response);
    }
    public function laundry_charges_add_details()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
        $sub_cat_id = $this->input->post('sub_cat_id', TRUE); $sub_cat_id = (int)$sub_cat_id;
        $country_id = $this->input->post('country_id', TRUE); $country_id = (int)$country_id;
        $charge_amount = $this->input->post('charge_amount', TRUE);
        $today = date('Y-m-d h:i:s');
        $currency_details = $this->api->get_country_currencies($country_id);
        $currency_sign = $currency_details[0]['currency_sign'];

        $insert_data = array(
          "cat_id" => trim($cat_id),
          "sub_cat_id" => trim($sub_cat_id),
          "cust_id" => trim($cust_id),
          "charge_amount" => trim($charge_amount),
          "country_id" => trim($country_id),
          "currency_sign" => trim($currency_sign),
          "cre_datetime" => trim($today),
        );
        //echo json_encode($insert_data); die();
        if(!$this->api->check_laundry_charge_details($insert_data)) {
          if($this->api->add_laundry_charge_details($insert_data)) {
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('added_successfully')];
          } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('unable_to_add')]; } 
        } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('Rate already exists.')]; }  
        echo json_encode($this->response);
      }
    }
    public function update_laundry_charge_amount()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $charge_id = $this->input->post('charge_id', TRUE); $charge_id = (int)$charge_id;
        $charge_amount = $this->input->post('charge_amount', TRUE);
        if($this->api->update_laundry_charge_amount_details($charge_amount, $charge_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success')];
        } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('update_failed')]; } 
        echo json_encode($this->response);
      }
    }
    public function laundry_charges_delete()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $charge_id = $this->input->post('charge_id', TRUE); $charge_id = (int)$charge_id;
        if($this->api->delete_laundry_charge_details($charge_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('delete_success')];
        } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('delete_failed')]; } 
        echo json_encode($this->response);
      }
    }
  //End Manage Laundry charges-----------------------------

  //Start cancellation Laundry charges---------------------
    public function laundry_provider_cancellation_charges_list()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          $cancellation_details = $this->api->get_laundry_provider_cancellation_details($cust_id);
          $today = date('Y-m-d h:i:s');
          if(empty($cancellation_details)) {
            $cancellation_details = $this->api->get_master_cancellation_details();
            for($i=0; $i<sizeof($cancellation_details); $i++) {
              $insert_data = array(
                "cust_id" => (int)$cust_id,
                "charge_per" => $cancellation_details[$i]['charge_per'],
                "country_id" => $cancellation_details[$i]['country_id'],
                "charge_status" => $cancellation_details[$i]['charge_status'],
                "cre_datetime" => $today,
              );
              $this->api->register_laundry_provider_cancellation_details($insert_data);
            }
            $cancellation_details = $this->api->get_laundry_provider_cancellation_details($cust_id);
          }
          if($cancellation_details) {
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "cancellation_list" => $cancellation_details];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "cancellation_list" => array()]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function laundry_cancellation_charges_add()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          $country_id = $this->input->post('country_id', TRUE);
          $charge_per = $this->input->post('charge_per', TRUE);
          $today = date('Y-m-d H:i:s');

          $insert_data = array(
            "cust_id" => (int)$cust_id,
            "charge_per" => trim($charge_per),
            "country_id" => (int)trim($country_id),
            "charge_status" => 1,
            "cre_datetime" => $today,
          );

          if(!$this->api->check_country_cancellation_charges($insert_data)) {
            if($this->api->register_laundry_provider_cancellation_details($insert_data) ) {
              $this->response = ['response' => 'success', 'message'=> $this->lang->line('add_success')];
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('configuration_exists')]; }   
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
        echo json_encode($this->response);
      } 
    }
    public function update_laundry_cancellation_percentage()
    {
      if(empty($this->errors)) { 
        $charge_id = $this->input->post('charge_id', TRUE);
        $charge_per = $this->input->post('charge_per', TRUE);
        $update_data = array(
          "charge_per" => trim($charge_per),
        );

        if($this->api->update_cancellation_percentage_details($update_data, $charge_id) ) {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('update_success')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function laundry_cancellation_charges_delete()
    {
      if(empty($this->errors)){ 
          $charge_id = $this->input->post('charge_id', TRUE); $charge_id = (int) $charge_id;
          if( $this->api->delete_cencellation_charges_details($charge_id)) {
              $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
        echo json_encode($this->response);
      }
    }
  //End cancellation Laundry charges-----------------------

  //Start Favorite Providers For Laundry Buyers------------
    public function get_favourite_provider_list()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $from_country_id = $this->input->post('from_country_id', TRUE);
        $from_state_id = $this->input->post('from_state_id', TRUE);
        $from_city_id = $this->input->post('from_city_id', TRUE);
        $rating = $this->input->post('rating', TRUE);

        if($this->api->is_user_active_or_exists($cust_id)){
          if( $list = $this->api->get_providers_list($cust_id, $last_id, $from_country_id, $from_state_id, $from_city_id, $rating) ) {
            for ($i=0; $i < sizeof($list); $i++) { 
              $did = $list[$i]['cust_id'];
              $is_favorite = $this->api->verify_favorite($did, $cust_id);
              if($is_favorite) $list[$i] += array("is_favorite" => "1");
              else $list[$i] += array("is_favorite" => "0");
            }
            $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "list" => array_reverse($list)];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }

        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
        echo json_encode($this->response);  
      }
    }
    public function register_new_favourite_provider()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)) {
        $deliverer_id = $this->input->post('deliverer_id', TRUE);  $deliverer_id = (int) $deliverer_id;
        if( $this->api->register_new_favourite_provider($cust_id, $deliverer_id) ) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('register_success')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('register_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function remove_favourite_provider()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)) {
        $deliverer_id = $this->input->post('deliverer_id', TRUE);  $deliverer_id = (int) $deliverer_id;
        if( $this->api->remove_favourite_provider($cust_id, $deliverer_id) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('remove_success')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
  //End Favorite Providers For Laundry Buyers--------------

  //Start Laundry Customer Bookings------------------------
    public function customer_accepted_laundry_bookings()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $bookings = $this->api->laundry_booking_list($cust_id, 'accepted', 'customer', $last_id, 1);
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        //echo json_encode($bookings); die();
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => array_reverse($bookings)];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
    public function customer_in_progress_laundry_bookings()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $bookings = $this->api->laundry_booking_list($cust_id, 'in_progress', 'customer', $last_id, 1);
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        //echo json_encode($bookings); die();
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => array_reverse($bookings)];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
    public function customer_completed_laundry_bookings()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $bookings = $this->api->laundry_booking_list($cust_id, 'completed', 'customer', $last_id, 1);
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        //echo json_encode($bookings); die();
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => array_reverse($bookings)];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
    public function customer_cancelled_laundry_bookings()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $bookings = $this->api->laundry_booking_list($cust_id, 'cancelled', 'customer', $last_id, 1);
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        //echo json_encode($bookings); die();
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => array_reverse($bookings)];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
    
    public function laundry_bookings_list() // Customer/Provider all type of bookings
    {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $list_type = $this->input->post('list_type', TRUE); $list_type = trim($list_type); //accepted/in_progress/completed/cencelled
        $user_type = $this->input->post('user_type', TRUE); $user_type = trim($user_type); //provider/customer
        //echo json_encode($_POST); die();

        $bookings = $this->api->laundry_booking_list($cust_id, $list_type, $user_type, $last_id, 1);
        //echo json_encode($bookings); die();
        
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $operator_details = $this->api->get_laundry_provider_profile($bookings[$i]['provider_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          $bookings[$i] += ['opr_avatar_url' => $operator_details['avatar_url']];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $bookings];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
  //End Laundry Customer Bookings--------------------------
  
  //Start Laundry provider Bookings------------------------
    public function provider_accepted_laundry_bookings()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $bookings = $this->api->laundry_booking_list($cust_id, 'accepted', 'provider', $last_id, 1);
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        //echo json_encode($bookings); die();
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => array_reverse($bookings)];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
    public function provider_in_progress_laundry_bookings()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $bookings = $this->api->laundry_booking_list($cust_id, 'in_progress', 'provider', $last_id, 1);
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        //echo json_encode($bookings); die();
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => array_reverse($bookings)];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
    public function provider_completed_laundry_bookings()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $bookings = $this->api->laundry_booking_list($cust_id, 'completed', 'provider', $last_id, 1);
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        //echo json_encode($bookings); die();
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => array_reverse($bookings)];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
    public function provider_cancelled_laundry_bookings()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
        $bookings = $this->api->laundry_booking_list($cust_id, 'cancelled', 'provider', $last_id, 1);
        for ($i=0; $i < sizeof($bookings); $i++) { 
          $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
          $bookings[$i] += ['bookings_details' => $bookings_details];
          if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
            $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
            $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
          }
          if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
            $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
            $bookings[$i] += ['drop_order_details' => $drop_order_details];
          }
        }
        //echo json_encode($bookings); die();
        if($bookings) { 
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => array_reverse($bookings)];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);  
      }
    }
  //End Laundry provider Bookings--------------------------

  //-------------------------------------------------------
    public function courier_payment_confirm($courier_update_data)
    {
      $order_id = (int)trim($courier_update_data['order_id']);
      $payment_method = trim($courier_update_data['payment_method']);
      $paid_amount = trim($courier_update_data['paid_amount']);
      $transaction_id = trim($courier_update_data['transaction_id']);
      $today = trim($courier_update_data['today']);

      if($order_details = $this->api_courier->get_order_detail($order_id)) {
        $cust_id = (int)$order_details['cust_id'];
        $transfer_account_number = 'NULL';
        $bank_name = 'NULL';
        $account_holder_name = 'NULL';
        $iban = 'NULL';
        $email_address = $order_details['cust_name'];
        $mobile_number = 'NULL';
        $total_amount_paid = $paid_amount;

        $update_data_order = array(
          "payment_method" => $payment_method,
          "payment_mode" => "payment",
          "paid_amount" => $paid_amount,
          "transaction_id" => $transaction_id,
          "payment_datetime" => $today,
          "cod_payment_type" => "NULL",
          "complete_paid" => "1",
          "balance_amount" => 0,
        );

        if($this->api->update_courier_order_details($update_data_order, $order_id)) {
          $order_details = $this->api_courier->get_order_detail($order_id);
          //echo json_encode($order_details['currency_sign']); die();
          /************************ Courier Payment Section *****************************/
            //Add core price to gonagoo master
              if($gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api_courier->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
              }
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
              $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($order_details['standard_price']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            
            //Update urgent fee in gonagoo account master
              if(trim($order_details['urgent_fee']) > 0) {
                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'urgent_fee',
                  "amount" => trim($order_details['urgent_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update insurance fee in gonagoo account master
              if(trim($order_details['insurance_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'insurance_fee',
                  "amount" => trim($order_details['insurance_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update handling fee in gonagoo account master
              if(trim($order_details['handling_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'handling_fee',
                  "amount" => trim($order_details['handling_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update vehicle fee in gonagoo account master
              if(trim($order_details['vehicle_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'vehicle_fee',
                  "amount" => trim($order_details['vehicle_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update custome clearance fee in gonagoo account master
              if(trim($order_details['custom_clearance_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'custom_clearance_fee',
                  "amount" => trim($order_details['custom_clearance_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update custome clearance fee in gonagoo account master
              if(trim($order_details['loading_unloading_charges']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'loading_unloading_charges',
                  "amount" => trim($order_details['loading_unloading_charges']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            /*//Add payment details in customer account master and history
              if($customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
              } else {
                $insert_data_customer_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api_courier->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
              $this->api_courier->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($total_amount_paid),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api_courier->insert_payment_in_account_history($update_data_account_history);
            */

            //Deduct payment details from customer account master and history
              if($customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
              } else {
                $insert_data_customer_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $this->api_courier->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
              $this->api_courier->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($total_amount_paid),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api_courier->insert_payment_in_account_history($update_data_account_history);
          /************************ Courier Payment Section *****************************/
        }
      }
    }
  //-------------------------------------------------------

  //Start Laundry Workroom Web Services--------------------
    public function get_laundry_workroom()
    {
      $booking_id = $this->input->post('booking_id',TRUE);
      $last_id = $this->input->post('last_id',TRUE);

      $booking_details = $this->api->laundry_booking_details($booking_id);
      if($workroom = $this->api->get_laundry_workroom_chat($booking_id, $last_id)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line("chats found"), 'booking_details' => $booking_details, 'chats' =>  $workroom];
      } else {
        $this->response = ['response' => 'failed', 'message'=> $this->lang->line("chats not found"), 'booking_details' => $booking_details, 'chats' => array()];
      }
      echo json_encode($this->response);
    }
    public function add_chat_laundry_workroom()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      $booking_id = $this->input->post('booking_id', TRUE);
      $text_msg = $this->input->post('message_text', TRUE);
      $user_type = $this->input->post('user_type', TRUE);
      $file_type = $this->input->post('file_type', TRUE);
      $today = date("Y-m-d H:i:s");
      $booking_details = $this->api->laundry_booking_details($booking_id); 
      $cust_id = $booking_details['cust_id'];
      $provider_id = $booking_details['provider_id'];
      ($user_type == 'sr') ? $sender_id = $cust_id : $sender_id = $booking_details['provider_id'];

      $user_details = $this->api->get_user_details((int)$booking_details['cust_id']);
      if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
        $parts = explode("@", trim($user_details['email_id']));
        $username = $parts[0];
        $cust_name = $username;
      } else { $cust_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }

      $provider_details = $this->api->get_user_details((int)$booking_details['provider_id']);
      if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
        $parts = explode("@", trim($provider_details['email_id']));
        $username = $parts[0];
        $provider_name = $username;
      } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

      if(!empty($_FILES["attachment"]["tmp_name"])){
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('attachment')) {   
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"];  
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      $insert_data = array(
        "order_id" => $booking_id,
        "cust_id" => $cust_id,
        "deliverer_id" => $booking_details['provider_id'],
        "text_msg" => $text_msg,
        "attachment_url" => $attachment_url,
        "cre_datetime" => $today,
        "sender_id" => $sender_id,
        "type" => "chat",
        "file_type" => $file_type,
        "cust_name" => $cust_name,
        "deliverer_name" => $provider_name,
        "thumbnail" => "NULL",
        "ratings" => "NULL",
        "cat_id" => 9,
      );
      if($this->api->add_laundry_chat_to_workroom($insert_data)) {
        if($workroom = $this->api->get_laundry_workroom_chat($booking_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line("chats send"), 'booking_details' => $booking_details, 'Chats' =>  $workroom];
          if($sender_id == $cust_id) {  $device_details = $this->api->get_user_device_details($cust_id); } 
          else {  $device_details = $this->api->get_user_device_details($deliverer_id); }
          //Get Customer Device Reg ID's
          $arr_fcm_ids = $arr_apn_ids = array();
          foreach ($device_details as $value) {
            if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_apn_ids, $value['reg_id']); }
          }
          // Get PN API Keys and PEM files
          $api_key_customer = $this->config->item('delivererAppGoogleKey');
          $msg =  array(
            'title' => $this->lang->line('new_message'),
            'type' => 'chat',
            'notice_date' => $today,
            'desc' => $text_msg,
            'attachment' => $attachment_url,
            'cust_name' => $cust_name,
            'operator_name' => $provider_name,
            'ticket_id' => $booking_id,
            'cust_id' => $cust_id,
            'operator_id' => $provider_id,
            'file_type' => $file_type,
            'thumbnail' => "NULL",
            "ratings" => "NULL",
          );
          $this->api->sendFCM($msg, $arr_fcm_ids, $api_key_customer);
          // Push Notification APN
          $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
          if(is_array($arr_apn_ids)) { 
            $arr_apn_ids = implode(',', $arr_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_apn_ids);
          }
        }
      } else {
        $this->response = ['response' => 'failed', 'message'=> $this->lang->line("chats not send"), 'booking_details' => $booking_details , 'Chats' =>  $workroom];
      }
      echo json_encode($this->response);
    }
    public function add_review_comments_laundry_workroom()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->input->post('cust_id', TRUE); ; $cust_id = (int) $cust_id;
      $booking_id = $this->input->post('booking_id', TRUE); ; $booking_id = (int) $booking_id;
      $rating = $this->input->post('rating', TRUE); $rating = (int) $rating;
      $text_msg = $this->input->post('review_text', TRUE);
      $booking = $this->api->laundry_booking_details($booking_id);
      $cust_id = $booking['cust_id'];
      $provider_id = $booking['provider_id'];
      $sender_id = $cust_id;
      $cust_name = $booking['cust_name'];
      $operator_name = $booking['operator_name'];
      $today = date("Y-m-d H:i:s");
      
      $update_data = array(
        "review_comment" => $text_msg,
        "rating" => $rating,
        "review_datetime" => $today,
      ); 

      if($this->api->update_laundry_review($update_data, $booking_id)) {
        if($old_laundry_ratings = $this->api->get_laundry_review_details($provider_id)) {
          $new_no_of_ratings = $old_laundry_ratings['no_of_ratings']+1;
          $new_total_ratings = $old_laundry_ratings['total_ratings']+$rating;
          $new_ratings = round($new_total_ratings/$new_no_of_ratings,1);
          $laundry_rating_data = array(
            "ratings" => $new_ratings,
            "total_ratings" => $new_total_ratings,
            "no_of_ratings" => $new_no_of_ratings,
          );
          $this->api->update_laundry_ratings($laundry_rating_data, $provider_id);
        } else {
          $insert_laundry_ratings =array(
            'operator_id' => $provider_id,
            'ratings' => $rating,
            'update_date' => $today,
            'no_of_ratings' => 1,
            'total_ratings' => $rating
          );
          $old_laundry_ratings = $this->api->insert_laundry_review_details($insert_laundry_ratings); 
        }

        $old_deliverer_ratings = $this->api->get_consumer_datails($provider_id); 
        $new_operator_total_ratings = $old_deliverer_ratings['total_ratings'] + $rating;
        $new_operator_no_of_ratings = $old_deliverer_ratings['no_of_ratings'] + 1;
        $new_operator_ratings = round($new_operator_total_ratings/$new_operator_no_of_ratings,1);
        $operator_rating_data =array(
          'ratings' => $new_operator_ratings,
          'no_of_ratings' => $new_operator_no_of_ratings,
          'total_ratings' => $new_operator_total_ratings
        );
        $this->api->update_deliverer_rating($operator_rating_data, $provider_id);
        
        $review_data = array(
          "ticket_id" => $booking_id,
          "cust_id" => $cust_id,
          "cust_name" => $cust_name,
          "cust_avatar" => $old_deliverer_ratings['avatar_url'],
          "review_comment" => $text_msg,
          "review_rating" => $rating,
          "operator_id" => $provider_id,
          "operator_name" => $operator_name,
          "review_datetime" => $today,
        );
        $this->api->post_to_review_laundry($review_data);

        $insert_data = array(
          "order_id" => $booking_id,
          "cust_id" => $cust_id,
          "deliverer_id" => $provider_id,
          "text_msg" => $text_msg,
          "attachment_url" => "NULL",
          "cre_datetime" => $today,
          "sender_id" => $cust_id,
          "type" => "review_rating",
          "file_type" => "NULL",
          "cust_name" => $cust_name,
          "deliverer_name" => $operator_name,
          "thumbnail" => "NULL",
          "ratings" => $rating,
          "cat_id" => 9,
        );
        if($this->api->add_laundry_chat_to_workroom($insert_data)) {
          if($workroom = $this->api->get_laundry_workroom_chat($booking_id)) {
            $device_details = $this->api->get_user_device_details($provider_id);
            $arr_fcm_ids = $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            //Get PN API Keys
            $api_key_customer = $this->config->item('delivererAppGoogleKey');
            $msg =  array(
              'title' => $this->lang->line('new_message'), 
              'type' => 'review_rating', 
              'notice_date' => $today, 
              'desc' => $text_msg,
              'attachment' => "NULL",
              'cust_name' => $cust_name, 
              'deliverer_name' => $operator_name, 
              'order_id' => $booking_id, 
              'cust_id' => $cust_id, 
              'deliverer_id' => $provider_id, 
              'file_type' => "NULL", 
              'thumbnail' => "NULL",
              "ratings" => $rating
            );
            $this->api->sendFCM($msg, $arr_fcm_ids, $api_key_customer);
            //Push Notification APN
            $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_apn_ids);
            }
            $this->response = ['response' => 'success', 'message'=> $this->lang->line("review comment added successfully"), 'booking_details' => $booking , 'Chats' =>  $workroom];
          }
        } else {
          $this->response = ['response' => 'failed', 'message'=> $this->lang->line("review comment added failed"), 'booking_details' => $booking , 'Chats' =>  $workroom];
        }
      } else {
        $this->response = ['response' => 'failed', 'message'=> $this->lang->line("review comment added failed"), 'booking_details' => $booking , 'Chats' =>  $workroom];
      }
      echo json_encode($this->response);
    }
    public function make_workroom_notification_read()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id');   
        $order_id = $this->input->post('booking_id');   
        $ids = $this->api->get_total_unread_order_workroom_notifications($order_id, $cust_id);        
        foreach ($ids as $id => $v) {  $this->api->make_workroom_notification_read($v['ow_id'], $cust_id);  }
        $this->response = array(  'response' => 'success' );
        echo json_encode($this->response);
      }
    }
  //End Laundry Workrooom Web Services---------------------

  //Start Customer Cancel Booking--------------------------
    public function laundry_booking_cancel()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $cust_id = $this->input->post("cust_id"); $cust_id = (int)$cust_id;
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $provider_details = $this->api->get_user_details($booking_details['provider_id']);
      $user_details = $this->api->get_user_details($booking_details['cust_id']);
      $today = date('Y-m-d H:i:s');
      
      //get cancellation charges-----------------------------------------------
        $cancellation_charge_per = $this->api->get_gonagoo_laundry_commission_details($user_details['country_id'])['cancellation_charge'];
        $cancellation_charges = round((trim($booking_details['total_price'])) * ( $cancellation_charge_per / 100 ),2);
        $refund_amount = trim($booking_details['total_price']) - $cancellation_charges;
      //-----------------------------------------------------------------------

      $data = array(
        'booking_status' => 'cancelled',
        'status_updated_by' => trim($cust_id), 
        'status_updated_by_user_type' => 'customer', 
        'status_update_datetime' => trim($today),
        'cancelled_by' => 'customer', 
        'cancelled_date' => trim($today),
        'refund_amount' => trim($refund_amount),
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        //check courier boonking and cancel it.
        if($booking_details['is_pickup'] == 1){
          $url = base_url('api/cancel-courier-order');
          $param = "order_id=".$booking_details['pickup_order_id'];
          $ch = curl_init($url); // url to send sms
          curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
          curl_setopt($ch, CURLOPT_POST, 1); // method to call url
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
          curl_setopt($ch, CURLOPT_POSTFIELDS, $param); 
          $cancel_courier = curl_exec($ch); // execute url and save response
          curl_close($ch); // close url connection
          //echo $cancel_courier; die();
        }
        
        $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
        $message = $this->lang->line('Your laundry booking has been cancelled.').' '.$this->lang->line('Booking ID: ').$booking_id;
        $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);

        //Send Push Notifications
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_customer = $this->api->get_user_device_details($cust_id);
        if(!is_null($device_details_customer)) {
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => $this->lang->line('Laundry booking status'), 'type' => 'booking-in-progress', 'notice_date' => $today, 'desc' => $message);
          $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
          //APN
          $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking status'), 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }
        
        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($user_details['email_id']));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($user_details['email_id']), $this->lang->line('Laundry booking status'), trim($message));

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email_id']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['cust_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);

        //check laundry booking payment and process cancellation charges.
        if($booking_details['complete_paid'] == 1 && $booking_details['payment_mode']  != 'cod') {
          //Deduct refund amount from Provider account---------------------------
            if(!$provider_account_master_details = $this->api->customer_account_master_details(trim($booking_details['provider_id']), trim($booking_details['currency_sign']))) {
              $insert_data_master = array(
                "user_id" => trim($booking_details['provider_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_master);
              $provider_account_master_details = $this->api->customer_account_master_details(trim($booking_details['provider_id']), trim($booking_details['currency_sign']));
            }
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($refund_amount);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], trim($booking_details['provider_id']), $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details(trim($booking_details['provider_id']), trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)trim($booking_details['provider_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($refund_amount),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => trim($booking_details['cat_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Deduct Complete amount from Provider account-------------------------

          //Add Complete booking amount to customer account----------------------
            if(!$customer_account_master_details = $this->api->customer_account_master_details(trim($booking_details['cust_id']), trim($booking_details['currency_sign']))) {
              $insert_data_master = array(
                "user_id" => trim($booking_details['cust_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($booking_details['cust_id']), trim($booking_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($refund_amount);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($booking_details['cust_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($booking_details['cust_id']), trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)trim($booking_details['cust_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($refund_amount),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => trim($booking_details['cat_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Deduct Complete amount from Provider account-------------------------
        }
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('Booking cancelled successfully.')];
      } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }
  //End Customer Cancel Booking----------------------------

  //Start Customer Create Booking--------------------------
    public function laundry_provider_list()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->input->post('cust_id', TRUE);
      $c_id = $this->input->post('cat_id', TRUE);
      $sb_id = $this->input->post('sub_cat_id', TRUE);
      $ct = $this->input->post('count', TRUE);
      
      $is_pickup = $this->input->post('is_pickup', TRUE);
      $is_drop = $this->input->post('is_drop', TRUE);
      $deliverer_id = $this->input->post('deliverer_id');
      $pickuptime = $this->input->post('pickuptime');
      $pickupdate = $this->input->post('pickupdate');
      $from_address = $this->input->post('from_address');
      $customer_lat_long = $_POST['customer_lat_long'];
      $lat_long_array = explode(',', $customer_lat_long); //die();
      $cust_lat = $lat_long_array[0];
      $cust_long = $lat_long_array[1];
      
      $cat_id = explode(",",$c_id);
      $sub_cat_id = explode(",",$sb_id);
      $count = explode(",",$ct);
      $width = array();
      $length = array();
      $customer = $this->api->get_consumer_datails($cust_id);
      $provider = $this->api->get_laundry_provider_profile_list();
      //echo json_encode($provider); die();
        
      $details = array();
      for($i = 0 ; $i < sizeof($sub_cat_id) ; $i++){
        $sub_categori_details = $this->api->get_laundry_sub_category($sub_cat_id[$i]);
        $details[$i] = array(
          'cust_id' => $cust_id,
          'cat_id' => $sub_categori_details['cat_id'],
          'sub_cat_id' => $sub_categori_details['sub_cat_id'],
          'height' => $sub_categori_details['height'],
          'weight' => $sub_categori_details['weight'],
          'width' =>  $sub_categori_details['width'],
          'length' => $sub_categori_details['length'],
          'count' => $count[$i],
        );
        array_push($width , $sub_categori_details['width']);
        array_push($length , $sub_categori_details['length']);
      }

      //echo json_encode($details); die();
      
      $providers = array();       
      $courier_payment = array();
      $totals = array();
      $rates = array();       
      $cr_sign = "";
      $provider_id = 0;
      foreach ($provider as $pro) { static $cnt=0; $cnt++;
        $total = 0;
        $flag = 0;
        $cr_sign = "";
        $total_weight = 0;
        $max_height = 0;
        //echo json_encode($details); die();
        foreach ($details as $det){
          if($rate = $this->api->get_laundry_charges_list_apend($pro['cust_id'], $det['sub_cat_id'] , $customer['country_id'])){
            $total = $total + ($rate['charge_amount'] * $det['count']);
            $cr_sign= $rate['currency_sign'];
            $cat = $this->api->get_laundry_category($det['cat_id']);
            $sub_cat = $this->api->get_laundry_sub_category($det['sub_cat_id']);

            $total_weight = $total_weight + ($det['weight']*$det['count']);
            $max_height = $max_height + ($det['height']*$det['count']);
            $sub_to = ($rate['charge_amount'] * $det['count']);
            $cloth_details[] = array(
              'cat_name' => $cat['cat_name'],
              'sub_cat_name' => $sub_cat['sub_cat_name'],
              'charge' => $rate['charge_amount'],
              'count' => $det['count'],
              'sub_total' =>  (string)$sub_to,
              'currency_sign' => $cr_sign,
            );
           
          } else { $flag++; }
        }
        //echo json_encode($cloth_details); die();
        if($flag == 0){
          $driving_distance = $this->api->GetDrivingDistance($pro['latitude'],$pro['longitude'],$cust_lat,$cust_long);
          $provider_id = $pro["cust_id"];
          $provider_details = $this->api->get_laundry_provider_profile((int)$provider_id);
          $total_pricing = array( 
            'total'=> (string)$total,
            'currency_sign'=> $cr_sign,
          );

          $provider_details['distance_to_shop_in_km'] = $driving_distance;
          $provider_details['cloth_wise_details'] = $cloth_details;
          $provider_details['total_laundry_charge'] = $total_pricing;
          unset($cloth_details);

          //***********Courier************************
            if($is_pickup > 0 || $is_drop > 0){
              $avg = array(
                'weight' => round(($total_weight/1000),2),
                'height' => $max_height,
                'length' => max($length),
                'width'  => max($width),
              );
              $total_weight = round(($total_weight/1000),2);
              $from_add = $this->api->get_address_from_book($from_address);
              $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$pro['latitude'],$pro['longitude']);
              
              $transport_vehichle_type = 0;
              if($avg['weight'] > 15){ $transport_vehichle_type = 28;
              } else { $transport_vehichle_type = 27; }

              
              $data = array(
                "category_id" => 7,
                "total_weight" =>$total_weight, 
                "unit_id" => 1,
                "from_country_id" => $from_add['country_id'], 
                "from_state_id" => $from_add['state_id'], 
                "from_city_id" => $from_add['city_id'], 
                "to_country_id" => $pro['country_id'], 
                "to_state_id" => $pro['state_id'], 
                "to_city_id" => $pro['city_id'], 
                "service_area_type" =>'local',
                "transport_type" => 'earth',
                "width" => $avg['width'],
                "height" => $avg['height'],
                "length" => $avg['length'],
                "total_quantity" => 1,
              );
              if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
                // $p_date = explode('/',$pickupdate);
                // $n_date =  $p_date[1]."/".$p_date[0]."/".$p_date[2];
                $p_date = explode('-',$pickupdate);
                $n_date =  $p_date[2]."/".$p_date[1]."/".$p_date[0];
                $my_date = $n_date.' '.$pickuptime.":00";
                $my_date = str_replace('/', '-', $my_date);
                $m_date = new DateTime($my_date);
                $m_date->modify("+".$max_duration['max_duration']." hours");
                $delivery_datetime = $m_date->format("m-d-Y H:i:s");
                $delivery_datetime = str_replace('-', '/', $delivery_datetime); 

                $data =array(  
                  "cust_id" => $pro['cust_id'],
                  "category_id" => 7,
                  "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
                  "delivery_datetime" => $delivery_datetime,
                  "total_quantity" => 1,
                  "width" => $avg['width'],
                  "height" => $avg['height'],
                  "length" => $avg['length'],
                  "total_weight" => $total_weight,
                  "unit_id" =>1,
                  "from_country_id" => $from_add['country_id'],
                  "from_state_id" => $from_add['state_id'],
                  "from_city_id" => $from_add['city_id'],
                  "from_latitude" => $from_add['latitude'],
                  "from_longitude" => $from_add['longitude'],
                  "to_country_id" => $pro['country_id'],
                  "to_state_id" => $pro['state_id'], 
                  "to_city_id" => $pro['city_id'],
                  "to_latitude" => $pro['latitude'],
                  "to_longitude" => $pro['longitude'],
                  "service_area_type" => "local",
                  "transport_type" => "earth",
                  "handling_by" => 0,
                  "dedicated_vehicle" => 0,
                  "package_value" => 0,
                  "custom_clearance_by" =>"NULL",
                  "old_new_goods" =>"NULL",
                  "custom_package_value" =>0,
                  "loading_time" =>0,
                  "transport_vehichle_type" => $transport_vehichle_type,  
                  "laundry" => 1, 
                );
                $order_price = $this->api_courier->calculate_order_price($data);
                if($is_pickup > 0){
                    $data = array(
                      'total_price'=>(string) $order_price['total_price'] ,
                      'standard_price'=> (string)$order_price['standard_price'] ,
                      'urgent_fee'=> (string)$order_price['urgent_fee'] ,
                      'ins_fee'=> (string)$order_price['ins_fee'] ,
                      'handling_fee'=> (string)$order_price['handling_fee'] ,
                      'dedicated_vehicle_fee'=> (string)$order_price['dedicated_vehicle_fee'] ,
                      'currency_id'=> (string)$order_price['currency_id'] ,
                      'currency_sign'=> (string)$order_price['currency_sign'] ,
                      'currency_title'=> (string)$order_price['currency_title'] ,
                      'distance_in_km'=> (string)$order_price['distance_in_km'] ,
                      'advance_percent'=> (string)$order_price['advance_percent'] ,
                      'commission_percent'=> (string)$order_price['commission_percent'] ,
                      'old_goods_custom_commission_percent'=> (string)$order_price['old_goods_custom_commission_percent'] ,
                      'old_goods_custom_commission_fee'=> (string)$order_price['old_goods_custom_commission_fee'] ,
                      'new_goods_custom_commission_percent'=> (string)$order_price['new_goods_custom_commission_percent'] ,
                      'new_goods_custom_commission_fee'=> (string)$order_price['new_goods_custom_commission_fee'] ,
                      'max_duration'=> (string)$order_price['max_duration'] ,
                    );
                    $provider_details['pickup_charge'] = $data;
                }
                if($is_drop > 0){
                  $data = array(
                      'total_price'=>(string) $order_price['total_price'] ,
                      'standard_price'=> (string)$order_price['standard_price'] ,
                      'urgent_fee'=> (string)$order_price['urgent_fee'] ,
                      'ins_fee'=> (string)$order_price['ins_fee'] ,
                      'handling_fee'=> (string)$order_price['handling_fee'] ,
                      'dedicated_vehicle_fee'=> (string)$order_price['dedicated_vehicle_fee'] ,
                      'currency_id'=> (string)$order_price['currency_id'] ,
                      'currency_sign'=> (string)$order_price['currency_sign'] ,
                      'currency_title'=> (string)$order_price['currency_title'] ,
                      'distance_in_km'=> (string)$order_price['distance_in_km'] ,
                      'advance_percent'=> (string)$order_price['advance_percent'] ,
                      'commission_percent'=> (string)$order_price['commission_percent'] ,
                      'old_goods_custom_commission_percent'=> (string)$order_price['old_goods_custom_commission_percent'] ,
                      'old_goods_custom_commission_fee'=> (string)$order_price['old_goods_custom_commission_fee'] ,
                      'new_goods_custom_commission_percent'=> (string)$order_price['new_goods_custom_commission_percent'] ,
                      'new_goods_custom_commission_fee'=> (string)$order_price['new_goods_custom_commission_fee'] ,
                      'max_duration'=> (string)$order_price['max_duration'] ,
                    );
                  $provider_details['drop_charge'] = $data;
                }
              }    
            } //else{ $provider_details['pickup_charge'] = 'NULL'; }
          //***********Courier************************
          
          array_push($providers , $provider_details);
          //$rating = (int)$this->api->get_laundry_review_details((int)$provider_id)['ratings'];
        }
      }
      if(!empty($providers)){
        $this->response  = ['response' => 'success' , 'message' => 'list_found' , 'providers' => $providers];
      }else{
        $this->response  = ['response' => 'failed' , 'message' => 'list_not_found' , 'providers' => array()];
      }
      echo json_encode($this->response); die();
    }
    public function laundry_booking()
    {
      //echo json_encode($_POST); die();
      $today = date('Y-m-d H:i:s');
      $cust_id = $this->input->post('cust_id');
      $pickuptime = $this->input->post('pickuptime');
      $pickupdate = $this->input->post('pickupdate');
      $payment_mode = $this->input->post('payment_mode');
      $booking_price = $this->input->post('booking_price');
      $currency_sign = $this->input->post('currency_sign');
      $pickup_courier_price = $this->input->post('pickup_courier_price');
      $drop_courier_price = $this->input->post('drop_courier_price');
      $item_count = $this->input->post('item_count');
      $is_pickup = $this->input->post('is_pickup');
      $is_drop = $this->input->post('is_drop');
      $from_address = $this->input->post('from_address');
      $total_price = $booking_price + $pickup_courier_price + $drop_courier_price;
      $cod_payment_type = $this->input->post('cod_payment_type');
      $expected_return_date = $this->input->post('expected_return_date');
      $description = $this->input->post('description');
      
      $c_id = $this->input->post('cat_id', TRUE);
      $sb_id = $this->input->post('sub_cat_id', TRUE);
      $ct = $this->input->post('count', TRUE);
      
      $cat_id = explode(",",$c_id);
      $sub_cat_id = explode(",",$sb_id);
      $count = explode(",",$ct);

      $width = array();
      $length = array();
      
      $booking_details = array();
      $max_height = 0;
      $total_weight = 0;

      for($i=0; $i<sizeof($sub_cat_id); $i++) {
        $sub_categori_details = $this->api->get_laundry_sub_category($sub_cat_id[$i]);

        $max_ht = 0;
        $max_wet = 0;
        $max_wet =  $sub_categori_details['weight']*$count[$i];
        $max_ht = $sub_categori_details['height']*$count[$i];
        $total_weight = $total_weight + ($sub_categori_details['weight']*$count[$i]);
        $max_height = $max_height + ($sub_categori_details['height']*$count[$i]);  
        $data['cust_id'] = $cust_id;
        $data['cat_id'] = $cat_id[$i];
        $data['sub_cat_id'] = $sub_cat_id[$i];
        $data['height'] = $max_ht;    
        $data['weight'] = $max_wet;    
        $data['width'] = $sub_categori_details['width'];    
        $data['length'] =$sub_categori_details['length'];
        $data['count'] = $count[$i];
        array_push($booking_details,$data);

        array_push($width , $sub_categori_details['width']);
        array_push($length , $sub_categori_details['length']);
      }
      //echo json_encode($booking_details); die();

      $provider = $this->api->get_laundry_provider_profile($_POST['selected_provider']);
      $user = $this->api->get_user_details($cust_id);

      $country_currency = $this->api->get_country_currency_detail($provider["country_id"]);
      $laundry_advance_payment = $this->api->get_laundry_advance_payment($provider["country_id"]);
      $gonagoo_commission_amount = ($booking_price/100)*$laundry_advance_payment['gonagoo_commission'];

      $data = array(
        'cust_id' => $user['cust_id'], 
        'provider_id' => $provider['cust_id'], 
        'cre_datetime' => $today,
        'booking_price' => $booking_price, 
        'total_price' => $total_price,
        'currency_id' => $country_currency['currency_id'], 
        'currency_sign' => $currency_sign, 
        'country_id' => $user['country_id'], 
        'paid_amount' => 0, 
        'pickup_courier_price' => $pickup_courier_price, 
        'drop_courier_price' => $drop_courier_price, 
        'payment_by' => "NULL", 
        'payment_datetime' => "NULL", 
        'balance_amount' => $total_price, 
        'pickup_payment' => ($_POST['cod_payment_type']=='at_pickup')?trim($total_price):0, 
        'deliver_payment' => ($_POST['cod_payment_type']=='at_deliver')?trim($total_price):0,  
        'complete_paid' => 0, 
        'booking_status' => "accepted", 
        'status_updated_by' => $user['cust_id'], 
        'status_updated_by_user_type' => "customer", 
        'status_update_datetime' => $today, 
        'item_count' => $item_count, 
        'cust_name' => $user['firstname']." ".$user['lastname'], 
        'cust_contact' => $user['mobile1'], 
        'cust_email' => $user['email1'], 
        'operator_name' => $provider['firstname']." ".$provider['lastname'], 
        'operator_company_name' => $provider['company_name'], 
        'operator_contact_name' => $provider['contact_no'], 
        'is_pickup' => $is_pickup, 
        'is_drop' => $is_drop, 
        'gonagoo_commission_per' => $laundry_advance_payment['gonagoo_commission'], 
        'gonagoo_commission_amount' => $gonagoo_commission_amount, 
        'cat_id' => 9,
        'payment_mode' => ($_POST['payment_mode']=='cod' || $_POST['payment_mode']=='bank' )?trim($_POST['payment_mode']):'NULL',
        'cod_payment_type' => trim($cod_payment_type),
        'expected_return_date' => trim($expected_return_date),
        'description' => trim($description),
        'drop_address_id' => trim($from_address),
      );
      //echo json_encode($data); die();
      if($booking_id = $this->api->create_laundry_booking($data)){
        $laundry_booking_details = $this->api->laundry_booking_details($booking_id);
        $this->response  = ['response' => 'success' , 'laundry_booking_id' => (string)$booking_id];
        foreach ($booking_details as $bk) {
          $rate = $this->api->get_laundry_charges_list_apend($provider['cust_id'],$bk['sub_cat_id'], $provider["country_id"]);
          $total_charge = $rate['charge_amount']*$bk['count'];
          $data2 =  array(
            'booking_id' => $booking_id, 
            'cat_id' => $bk['cat_id'], 
            'sub_cat_id' => $bk['sub_cat_id'] ,
            'quantity' => $bk['count'], 
            'single_unit_price' => $rate['charge_amount'], 
            'total_price' => $total_charge, 
            'currency_sign' => $rate['currency_sign'], 
            'cre_datetime' => $today, 
            'height' => $bk['height'], 
            'width' => $bk['width'], 
            'length' => $bk['length'], 
            'weight' => $bk['weight'],
          );
          if($booking_details_id = $this->api->create_laundry_booking_details($data2)){
            $this->response  += ['response' => 'success' , 'booking_details_id' => (string)$booking_details_id];
          } else {
            $this->response  += ['response' => 'failed' , 'booking_details_id' => "0"];
          }
        }//foreach

        //*********Send Booking SMS to customer*********************
          $country_code = $this->api->get_country_code_by_id($user["country_id"]);
          $user_message = $this->lang->line('Your booking is confirmed. Laundry Booking ID - ').$booking_id;
          $this->api->sendSMS((int)$country_code.trim($user['mobile1']), $user_message);
        //*********Send Booking SMS to customer*********************
        //*********Send Booking SMS to provider*********************
          $country_code = $this->api->get_country_code_by_id($provider["country_id"]);
          $provider_message = $this->lang->line('You have recieved Laundry order. Booking ID - ').$booking_id;
          $this->api->sendSMS((int)$country_code.trim($provider['contact_no']), $provider_message);
        //*********Send Booking SMS to provider*********************

        //*********Send Email to customer***************************
          if( trim($user['firstname']) == 'NULL' || trim($user['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($user['email1']));
            $username = $parts[0];
            $customer_name = $username;
          }else{
            $customer_name = trim($user['firstname'])." ".trim($user['lastname']);
          }
          $this->api_sms->send_email_text($customer_name, trim($user['email1']),$this->lang->line('laundry booking confirmation'), trim($user_message));
        //*********Send Email to customer***************************
        //*********Send Email to provider***************************
          if(trim($provider['firstname']) == 'NULL' || trim($provider['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($provider['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          }else{
            $provider_name = trim($provider['firstname'])." ".trim($provider['lastname']);
          }
          $this->api_sms->send_email_text($provider_name, trim($provider['email_id']),$this->lang->line('laundry booking confirmation'), trim($provider_message));
        //*********Send Email to provider***************************

        //*********Send push notification to customer***************
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('laundry booking confirmation'), 'type' => 'laundry-booking-confirmation', 'notice_date' => $today, 'desc' => $user_message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

            //Push Notification APN
            $msg_apn_customer =  array('title' => $this->lang->line('laundry booking confirmation'), 'text' => $user_message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        //*********Send push notification to customer***************
        //*********Send push notification to provider***************
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details($provider['cust_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('laundry booking confirmation'), 'type' => 'laundry-booking-confirmation', 'notice_date' => $today, 'desc' => $provider_message);
            $this->api->sendFCM($msg, $arr_provider_fcm_ids, $api_key);

            //Push Notification APN
            $msg_apn_provider =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $provider_message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
        //*********Send push notification to provider***************


        //*********Post courier_workroom & global_workroom**********
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $cust_id,
            'deliverer_id' => $provider['cust_id'],
            'text_msg' => $user_message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'booking_status',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);

          $global_workroom_update = array(
            'service_id' => $booking_id,
            'cust_id' => $cust_id,
            'deliverer_id' => $provider['cust_id'],
            'sender_id' => $cust_id,
            'cat_id' => 9
          );
          $this->api->create_global_workroom($global_workroom_update);
        //*********Post courier_workroom & global_workroom**********  
        
        //*********Add provider to laundry fav provider*************
          //check is_favorite
          $check = $this->api->check_is_favourite_providers_id($cust_id, $provider['cust_id']);
          if(!$check) {
            $fav_list = array(
              'cust_id' => $cust_id,
              'deliverer_id' => $provider['cust_id'],
              'cre_datetime' => $today
            );
            $this->api->add_laundry_favourite_providers($fav_list);
          }
        //*********Add provider to laundry fav provider*************  
        
        //******courier booking******************************************
          if($is_pickup==1) {
            $from_add = $this->api->get_address_from_book($from_address);
            $avg = array(
              'weight' => round(($total_weight/1000),2),
              'height' => $max_height,
              'length' => max($length),
              'width'  => max($width),
            );

            $volume = $avg['width'] * $avg['height'] * $avg['length'];
            $dimension_id = 0 ;
            if($dimensions = $this->api->get_dimension_id_for_laundry($volume)){
              $dimension_id = $dimensions['dimension_id']; 
            }
            $total_weight =  $avg['weight'];
            $transport_vehichle_type = 0;
            if($total_weight > 15){ $transport_vehichle_type = 28;
            } else { $transport_vehichle_type = 27; }
            $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$provider['latitude'],$provider['longitude']);
            $data = array(
              "category_id" => 7,
              "total_weight" =>$total_weight, 
              "unit_id" => 1,
              "from_country_id" => $from_add['country_id'], 
              "from_state_id" => $from_add['state_id'], 
              "from_city_id" => $from_add['city_id'], 
              "to_country_id" => $provider['country_id'], 
              "to_state_id" => $provider['state_id'], 
              "to_city_id" => $provider['city_id'], 
              "service_area_type" =>'local',
              "transport_type" => 'earth',
              "width" => $avg['width'],
              "height" => $avg['height'],
              "length" => $avg['length'],
              "total_quantity" => 1,
            );
            if($max_duration = $this->api_courier->get_max_duration($data, $distance_in_km)){
              $p_date = explode('-',$pickupdate);
              $n_date =  $p_date[2]."/".$p_date[1]."/".$p_date[0];
              $my_date = $n_date.' '.$pickuptime.":00";
              $my_date = str_replace('/', '-', $my_date);
              $m_date = new DateTime($my_date);
              $ex_date = $m_date;
              $ex_date->modify("+24 hours");
              $ex_datetime = $ex_date->format("m-d-Y H:i:s");
              $expiry_date = str_replace('-', '/', $ex_datetime);
              $m_date->modify("+".$max_duration['max_duration']." hours");
              $delivery_datetime = $m_date->format("m-d-Y H:i:s");
              $delivery_datetime = str_replace('-', '/', $delivery_datetime);
            }

            $data =array(  
              "cust_id" => $provider['cust_id'],
              "category_id" => 7,
              "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
              "total_quantity" => 1,
              "delivery_datetime" => $delivery_datetime,
              "width" => $avg['width'],
              "height" => $avg['height'],
              "length" => $avg['length'],
              "total_weight" => $total_weight,
              "unit_id" =>1,
              "from_country_id" => $from_add['country_id'],
              "from_state_id" => $from_add['state_id'],
              "from_city_id" => $from_add['city_id'],
              "from_latitude" => $from_add['latitude'],
              "from_longitude" => $from_add['longitude'],
              "to_country_id" => $provider['country_id'],
              "to_state_id" => $provider['state_id'], 
              "to_city_id" => $provider['city_id'],
              "to_latitude" => $provider['latitude'],
              "to_longitude" => $provider['longitude'],
              "service_area_type" => "local",
              "transport_type" => "earth",
              "handling_by" => 0,
              "dedicated_vehicle" => 0,
              "package_value" => 0,
              "custom_clearance_by" =>"NULL",
              "old_new_goods" =>"NULL",
              "custom_package_value" =>0,
              "loading_time" =>0,
              "transport_vehichle_type" => $transport_vehichle_type,  
              "laundry" => 1, 
            );
            $order_price = $this->api_courier->calculate_order_price($data);
            if(isset($cod_payment_type)) {
              if(($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')){
                $cod_payment_type = 'at_pickup';
                $pickup_payment = $order_price['total_price'];
                $deliver_payment = 0;
              } else {
                $cod_payment_type = 'at_deliver';
                $pickup_payment = 0;
                $deliver_payment = $order_price['total_price'];
              }
            } else { 
              $cod_payment_type = 'NULL';
              $pickup_payment = 0;
              $deliver_payment = $order_price['total_price']; 
            }

            if($payment_mode == 'bank') { $pending_service_charges = 0; $cod_payment_type = 'at_deliver';
            } else { $pending_service_charges = trim($laundry_booking_details['booking_price'])+trim($laundry_booking_details['drop_courier_price']); }

            $my_string ="cust_id=".$provider['cust_id']."&from_address=".$from_add['street_name']."&from_address_name=".$from_add['firstname']." ".$from_add['lastname']."&from_address_contact=".$from_add['mobile_no']."&from_latitude=".$from_add['latitude']."&from_longitude=".$from_add['longitude']."&from_country_id=".$from_add['country_id']."&from_state_id=".$from_add['state_id']."&from_city_id=".$from_add['city_id']."&from_addr_type=0&from_relay_id=0&to_address=".$provider['address']."&to_address_name=".$provider['firstname']." ".$provider['lastname']."&to_address_contact=".$provider['contact_no']."&to_country_id=".$provider['country_id']."&to_state_id=".$provider['state_id']."&to_city_id=".$provider['city_id']."&to_latitude=".$provider['latitude']."&to_longitude=".$provider['longitude']."&from_address_info=".$from_add['street_name']."&to_address_info=".$provider['address']."&to_addr_type=0&to_relay_id=0&pickup_datetime=".$pickupdate." ".$pickuptime.":00&delivery_datetime=".$delivery_datetime."&expiry_date=".$expiry_date."&order_type=Shipping_Only&service_area_type=local&order_contents=[0]&order_description=laundry&deliver_instructions=laundry&pickup_instructions=laundry&transport_type=earth&vehical_type_id=".$transport_vehichle_type."&dedicated_vehicle=0&with_driver=0&insurance=0&package_value=0&handling_by=0&total_quantity=[1]&width=[".$avg['width']."]&height=[".$avg['height']."]&length=[".$avg['length']."]&total_weight=[".(string)$total_weight."]&unit_id=[1]&advance_payment=0&payment_method=cod&pickup_payment=".$pickup_payment."&deliver_payment=".$deliver_payment."&payment_mode=cod&visible_by=0&from_address_email=".$from_add['email']."&to_address_email=".$provider['email_id']."&ref_no=".$booking_id."&dimension_id=[".$dimension_id."]&category_id=7&custom_clearance_by=NULL&old_new_goods=NULL&custom_package_value=0&need_tailgate=0&loading_time=1&dangerous_goods=[0]&is_laundry=1&cod_payment_type=".$cod_payment_type."&pending_service_charges=".trim($pending_service_charges)."&service_type=Laundry&laundry=1";
            //echo json_encode($my_string); die();
            $ch = curl_init(base_url()."api/book-courier-order"); // url to send sms
            curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
            curl_setopt($ch, CURLOPT_POST, 1); // method to call url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
            curl_setopt($ch, CURLOPT_POSTFIELDS,$my_string); 
            $courier_booking = curl_exec($ch); // execute url and save response
            curl_close($ch); // close url connections
            //echo $courier_booking; die();
            if(strpos($courier_booking, 'success') !== false) {
              $or = substr($courier_booking, strpos($courier_booking, '_id":') + 5);
              $ord = substr(strrev($or), strpos(strrev($or), '}') + 1);
              $order_id = strrev($ord);

              $update = array('pickup_order_id' => $order_id);
              $this->api->laundry_booking_update($update,$booking_id);

              $this->response  += ['response' => 'success' , 'courier_booking' => (string)$order_id,'message' => $this->lang->line('laundry_booking_and_courier_create_successfully') ];
            } else { $this->response  += ['response' => 'failed' , 'courier_booking' => "0" , 'message' => $this->lang->line('courier_booking_error') ]; }
          } else { $this->response  += ['message' => $this->lang->line('laundry_booking_create_successfully')]; }
        //******courier booking******************************************
      } else { $this->response  = ['response' => 'failed' , 'laundry_booking_id' => "0" , 'message' => $this->lang->line('laundry_booking_error')]; }
      echo json_encode($this->response);
    }
  //End Customer Create Booking----------------------------

  //Start Customer laundry payment-------------------------
    public function payment_by_customer_cod()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $cod_payment_type = $this->input->post("cod_payment_type");
      $details = $this->api->laundry_booking_details($booking_id);
      //echo json_encode($details); die();

      $data = array(
        'payment_mode' => 'cod', 
        'cod_payment_type' => ($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')?'at_pickup':'at_deliver', 
        'pickup_payment' => ($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')?$details['total_price']:0, 
        'deliver_payment' => ($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_deliver')?$details['total_price']:0, 
      );
      //echo json_encode($data); die();
      $ss =  $this->api->laundry_booking_update($data, $booking_id);
      //echo json_encode($ss); die();
      //Update COD type in courier
      if( $ss ) {
        if($details['is_pickup'] == 1) {
          $data = array(
            'payment_mode' => 'cod', 
            'cod_payment_type' => ($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')?'at_pickup':'at_deliver', 
          );
          $this->api->update_courier_order_details($data, $details['pickup_order_id']);          
        }
        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }
    public function payment_by_customer_mtn()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id', TRUE);
      $phone_no = $this->input->post('phone_no', TRUE);
      $today = date('Y-m-d H:i:s'); 
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $total_price = trim($booking_details['total_price']);
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$total_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      //$contents = curl_exec($ch);
      //$mtn_pay_res = json_decode($contents, TRUE);
      //var_dump($mtn_pay_res); die();
      
      $payment_method = 'mtn';
      $transaction_id = '1234567890';
      //$transaction_id = trim($mtn_pay_res['TransactionID']);
      
      $payment_data = array();
      // if($mtn_pay_res['StatusCode'] === "01"){
      if(1){
        $data = array(
          'paid_amount' => $booking_details['total_price'],
          'payment_method' => trim($payment_method), 
          'transaction_id' => trim($transaction_id), 
          'payment_mode' => 'online', 
          'payment_by' => 'mobile', 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1, 
          'drop_payment_complete' => ($booking_details['is_drop'] == 1)?1:0, 
        );
        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Payment completed for laundry booking ID').': '.'. '.$booking_id.' '.$this->lang->line('Stripe Payment ID:').' '.$transaction_id;
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email_id']));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($user_details['email_id']), $this->lang->line('Laundry booking status'), trim($message));

          $provider_details = $this->api->get_user_details($booking_details['provider_id']);
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['provider_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          /*************************************** Payment Section ****************************************/
            //Add Payment to customer account-----------------------------------------
              if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //deduct Payment from customer account------------------------------------
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            /* Stripe payment invoice---------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

              $user_country = $this->api->get_country_details(trim($user_details['country_id']));
              $user_state = $this->api->get_state_details(trim($user_details['state_id']));
              $user_city = $this->api->get_city_details(trim($user_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($booking_details['total_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Booking Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_id.'_laundry_booking.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_id);
              //Update to workroom
              $workroom_update = array(
                'order_id' => $booking_id,
                'cust_id' => $booking_details['cust_id'],
                'deliverer_id' => $booking_details['provider_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  "laundry-invoices/".$pdf_name,
                'cre_datetime' => $today,
                'sender_id' => $booking_details['provider_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $customer_name,
                'deliverer_name' => $provider_name,
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 9
              );
              $this->api->update_to_workroom($workroom_update);
            /* -------------------------------------------------------------------- */
            
            /* -----------------------Generate Ticket pdf------------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* -----------------------Generate Ticket pdf-------------------------- */
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $user_details['email1'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */

            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
              $emailAddress = trim($user_details['email1']);
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */

            //Deduct Gonagoo Commission from provider account-------------------------
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "type" => 1,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($booking_details['currency_sign']),
                "country_id" => trim($provider_details['country_id']),
                "cat_id" => 9,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------
            
            /* Ownward Laundry commission invoice---------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_details['booking_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
            /* -------------------------------------------------------------------- */

            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $provider_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */
          /*************************************** Payment Section ****************************************/

          //Update Courier Payment----------------------------------------------------
            if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
              $courier_update_data = array(
                "payment_method" => 'mtn',
                "payment_mode" => 'payment',
                "paid_amount" => $booking_details['pickup_courier_price'],
                "transaction_id" => trim($transaction_id),
                "today" => trim($today),
                "cod_payment_type" => 'NULL',
                "order_id" => $booking_details['pickup_order_id'],
              );
              $this->courier_payment_confirm($courier_update_data);
            }
          //Update Courier Payment----------------------------------------------------

          $this->response  = ['response' => 'success' , 'message' => $this->lang->line('laundry_payment_success')];
        } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('laundry_payment_error')]; }
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('laundry_payment_error')]; }
      echo json_encode($this->response);
    }
    public function payment_by_customer_stripe()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $booking_id = $this->input->post('booking_id', TRUE); $booking_id = (int) $booking_id;
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $payment_method = "stripe";
      $today = date('Y-m-d H:i:s');
      $booking_details = $this->api->laundry_booking_details($booking_id);
      //echo json_encode($booking_details); die();

      $data = array(
        'paid_amount' => $booking_details['total_price'],
        'payment_method' => trim($payment_method), 
        'transaction_id' => trim($transaction_id), 
        'payment_mode' => 'online', 
        'payment_by' => 'mobile', 
        'payment_datetime' => trim($today),
        'balance_amount' => '0', 
        'complete_paid' => 1, 
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        $user_details = $this->api->get_user_details($booking_details['cust_id']);
        $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
        $message = $this->lang->line('Payment completed for laundry booking ID').': '.'. '.$booking_id.' '.$this->lang->line('Stripe Payment ID:').' '.$transaction_id;
        $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
        //Send Push Notifications
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
        if(!is_null($device_details_customer)) {
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
          $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
          //APN
          $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }
        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($user_details['email_id']));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($user_details['email_id']), $this->lang->line('Laundry booking status'), trim($message));

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email_id']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);

        /*************************************** Payment Section ****************************************/
          //Add Payment to customer account-----------------------------------------
            if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $booking_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
            }

            $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['cust_id'],
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($booking_details['total_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //deduct Payment from customer account------------------------------------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['cust_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($booking_details['total_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //Add Payment to Provider account-----------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $booking_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            }

            $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'advance',
              "amount" => trim($booking_details['total_price']),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          /* Stripe payment invoice---------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($booking_details['total_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Booking Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_id.'_laundry_booking.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_id);
            //Update to workroom
            $workroom_update = array(
              'order_id' => $booking_id,
              'cust_id' => $booking_details['cust_id'],
              'deliverer_id' => $booking_details['provider_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "laundry-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $booking_details['provider_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $customer_name,
              'deliverer_name' => $provider_name,
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 9
            );
            $this->api->update_to_workroom($workroom_update);
          /* -------------------------------------------------------------------- */
          
          /* -----------------------Generate Ticket pdf------------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* -----------------------Generate Ticket pdf-------------------------- */
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $user_details['email1'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //Deduct Gonagoo Commission from provider account-------------------------
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "type" => 1,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($booking_details['currency_sign']),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => 9,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------
          
          /* Ownward Laundry commission invoice---------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_details['booking_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
          /* -------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $provider_details['email1'];
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        /*************************************** Payment Section ****************************************/

        //Update Courier Payment----------------------------------------------------
          if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
            $courier_update_data = array(
              "payment_method" => 'stripe',
              "payment_mode" => 'payment',
              "paid_amount" => $booking_details['pickup_courier_price'],
              "transaction_id" => trim($transaction_id),
              "today" => trim($today),
              "cod_payment_type" => 'NULL',
              "order_id" => $booking_details['pickup_order_id'],
            );
            $this->courier_payment_confirm($courier_update_data);
          }
        //Update Courier Payment----------------------------------------------------

        //------------------------------------------------------------------------

        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('laundry_payment_success')];
      } else { $this->response  = ['response' => 'fail' , 'message' => $this->lang->line('laundry_payment_error')]; }
      echo json_encode($this->response);
    }
    public function payment_by_customer_orange()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id'); 
      $cust_id = $this->input->post('cust_id'); 
      $session_token = $this->input->post('notif_token');  
      $pay_token = $this->input->post('pay_token'); 
      $order_payment_id = 'gonagoo_'.time().'_'.$booking_id; 
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $amount = $booking_details['total_price'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "booking_id" => $order_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      //$transaction_id = $payment_details['txnid'];
      $transaction_id = 786;
      //echo json_encode($payment_details); die();

      // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
      if(1) {
        $total_amount_paid = $booking_details['total_price'];
        $transfer_account_number = 'NULL';
        $bank_name = 'NULL';
        $account_holder_name = 'NULL';
        $iban = 'NULL';
        $email_address = 'NULL';
        $mobile_number = 'NULL';    

        $data = array(
          'paid_amount' => $booking_details['total_price'],
          'payment_method' => trim($payment_method), 
          'transaction_id' => trim($transaction_id), 
          'payment_mode' => 'online', 
          'payment_by' => 'mobile', 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1, 
        );
        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Payment completed for laundry booking ID').': '.'. '.$booking_id.' '.$this->lang->line('Stripe Payment ID:').' '.$transaction_id;
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email1']));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Laundry booking status'), trim($message));

          $provider_details = $this->api->get_user_details($booking_details['provider_id']);
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email1']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['provider_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          /*************************************** Payment Section ****************************************/
            //Add Payment to customer account-----------------------------------------
              if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //deduct Payment from customer account------------------------------------
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            /* Orange payment invoice------------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

              $user_country = $this->api->get_country_details(trim($user_details['country_id']));
              $user_state = $this->api->get_state_details(trim($user_details['state_id']));
              $user_city = $this->api->get_city_details(trim($user_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($booking_details['total_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Booking Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_id.'_laundry_booking.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_id);
              //Update to workroom
              $workroom_update = array(
                'order_id' => $booking_id,
                'cust_id' => $booking_details['cust_id'],
                'deliverer_id' => $booking_details['provider_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  "laundry-invoices/".$pdf_name,
                'cre_datetime' => $today,
                'sender_id' => $booking_details['provider_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $customer_name,
                'deliverer_name' => $provider_name,
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 9
              );
              $this->api->update_to_workroom($workroom_update);
            /* -------------------------------------------------------------------- */
            
            /* -----------------------Generate Ticket pdf------------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* -----------------------Generate Ticket pdf-------------------------- */
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $user_details['email1'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */
            
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
              $emailAddress = $user_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */

            //Deduct Gonagoo Commission from provider account-------------------------
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "type" => 1,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($booking_details['currency_sign']),
                "country_id" => trim($provider_details['country_id']),
                "cat_id" => 9,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------
            
            /* Ownward Laundry commission invoice------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_details['booking_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
            /* ----------------------------------------------------------------------- */
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $provider_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */
          /*************************************** Payment Section ****************************************/

          //Update Courier Payment----------------------------------------------------
            if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
              $courier_update_data = array(
                "payment_method" => 'stripe',
                "payment_mode" => 'payment',
                "paid_amount" => $booking_details['pickup_courier_price'],
                "transaction_id" => trim($transaction_id),
                "today" => trim($today),
                "cod_payment_type" => 'NULL',
                "order_id" => $booking_details['pickup_order_id'],
              );
              $this->courier_payment_confirm($courier_update_data);
            }
          //Update Courier Payment----------------------------------------------------

          $this->response  = ['response' => 'success' , 'message' => $this->lang->line('laundry_payment_success')];
        }else{ $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('laundry_payment_error')]; }
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('laundry_payment_error')]; }
      echo json_encode($this->response);
    }
  //End Customer laundry payment---------------------------

  //Start provider Create Booking--------------------------
    public function get_walkin_customers()
    {
      $cust_id = $this->input->post('cust_id');
      if( $walkin_cust = $this->api->get_walkin_customer_list($cust_id)) {
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "walking_customers" => $walkin_cust];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "walking_customers" => array()]; }
      echo json_encode($this->response);
    }
    public function laundry_sub_category_list_provider()
    {
      $cust_id = $this->input->post('cust_id');
      $sub_cat = $this->api->get_laundry_sub_category_list(1, 1);
      $sub_categories = array();
      foreach ($sub_cat as $cat) {
        $charg = array(
          'cat_id' => $cat['cat_id'],
          'sub_cat_id' => $cat['sub_cat_id'],
          'cust_id' => $cust_id,  
          'country_id' => $this->cust['country_id'], 
        );
        if($charges = $this->api->get_laundry_cherge($charg)){
          $cat['charges'] = $charges;
          array_push($sub_categories , $cat);
        }
      }
      if(!empty($sub_categories)){
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $sub_categories];
      }else{
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()];
      }
      echo json_encode($this->response);
    }
    public function laundry_pickup_cart_chareges_list_provider()
    {
      //echo json_encode($_POST); die();
      $provider_id = $this->input->post('provider_id', TRUE);
      $c_id = $this->input->post('cat_id', TRUE);
      $sb_id = $this->input->post('sub_cat_id', TRUE);
      $ct = $this->input->post('count', TRUE);
      
      $is_pickup = $this->input->post('is_pickup', TRUE);
      $is_drop = $this->input->post('is_drop', TRUE);
      $deliverer_id = $this->input->post('deliverer_id');
      $pickuptime = $this->input->post('pickuptime');
      $pickupdate = $this->input->post('pickupdate');
      $from_address = $this->input->post('from_address');
      
      $cat_id = explode(",",$c_id);
      $sub_cat_id = explode(",",$sb_id);
      $count = explode(",",$ct);
      $width = array();
      $length = array();
      $customer = $this->api->get_consumer_datails($provider_id);
      //echo json_encode($customer); die();
        
      $details = array();
      for($i = 0 ; $i < sizeof($sub_cat_id) ; $i++){
        $sub_categori_details = $this->api->get_laundry_sub_category($sub_cat_id[$i]);
        $details[$i] = array(
          'cust_id' => $provider_id,
          'cat_id' => $sub_categori_details['cat_id'],
          'sub_cat_id' => $sub_categori_details['sub_cat_id'],
          'height' => $sub_categori_details['height'],
          'weight' => $sub_categori_details['weight'],
          'width' =>  $sub_categori_details['width'],
          'length' => $sub_categori_details['length'],
          'count' => $count[$i],
        );
        array_push($width , $sub_categori_details['width']);
        array_push($length , $sub_categori_details['length']);
      }

      //echo json_encode($details); die();
      
      $providers = array();       
      $courier_payment = array();
      $totals = array();
      $rates = array();       
      $cr_sign = "";
      if($pro = $this->api->get_laundry_provider_profile($provider_id)) { static $cnt=0; $cnt++;
        $total = 1;
        $flag = 0;
        $cr_sign = "";
        $total_weight = 0;
        $max_height = 0;
        //echo json_encode($details); die();
        foreach ($details as $det){
          if($rate = $this->api->get_laundry_charges_list_apend($pro['cust_id'], $det['sub_cat_id'] , $customer['country_id'])){
            $total = $total + ($rate['charge_amount'] * $det['count']);
            $cr_sign= $rate['currency_sign'];
            $cat = $this->api->get_laundry_category($det['cat_id']);
            $sub_cat = $this->api->get_laundry_sub_category($det['sub_cat_id']);

            $total_weight = $total_weight + ($det['weight']*$det['count']);
            $max_height = $max_height + ($det['height']*$det['count']);
            $sub_to = ($rate['charge_amount'] * $det['count']);
            $cloth_details[] = array(
              'cat_name' => $cat['cat_name'],
              'sub_cat_name' => $sub_cat['sub_cat_name'],
              'charge' => $rate['charge_amount'],
              'count' => $det['count'],
              'sub_total' =>  (string)$sub_to,
              'currency_sign' => $cr_sign,
            );
           
          } else { $flag++; }
        }
        //echo json_encode($cloth_details); die();
        if($flag == 0){
          $provider_id = $pro["cust_id"];
          $provider_details = $this->api->get_laundry_provider_profile((int)$provider_id);
          $total_pricing = array( 
            'total'=> (string)$total,
            'currency_sign'=> $cr_sign,
          );
          $provider_details['cloth_wise_details'] = $cloth_details;
          $provider_details['total_laundry_charge'] = $total_pricing;
          unset($cloth_details);

          //***********Courier************************
            if($is_pickup > 0 || $is_drop > 0){
              $avg = array(
                'weight' => round(($total_weight/1000),2),
                'height' => $max_height,
                'length' => max($length),
                'width'  => max($width),
              );
              $total_weight = round(($total_weight/1000),2);
              $from_add = $this->api->get_address_from_book($from_address);
              $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$pro['latitude'],$pro['longitude']);
              
              $transport_vehichle_type = 0;
              if($avg['weight'] > 15){ $transport_vehichle_type = 28;
              } else { $transport_vehichle_type = 27; }

              
              $data = array(
                "category_id" => 7,
                "total_weight" =>$total_weight, 
                "unit_id" => 1,
                "from_country_id" => $from_add['country_id'], 
                "from_state_id" => $from_add['state_id'], 
                "from_city_id" => $from_add['city_id'], 
                "to_country_id" => $pro['country_id'], 
                "to_state_id" => $pro['state_id'], 
                "to_city_id" => $pro['city_id'], 
                "service_area_type" =>'local',
                "transport_type" => 'earth',
                "width" => $avg['width'],
                "height" => $avg['height'],
                "length" => $avg['length'],
                "total_quantity" => 1,
              );
              if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
                //echo json_encode($max_duration); die();
                // $p_date = explode('/',$pickupdate);
                // $n_date =  $p_date[1]."/".$p_date[0]."/".$p_date[2];
                $p_date = explode('-',$pickupdate);
                $n_date =  $p_date[2]."/".$p_date[1]."/".$p_date[0];
                $my_date = $n_date.' '.$pickuptime.":00";
                $my_date = str_replace('/', '-', $my_date);
                $m_date = new DateTime($my_date);
                $m_date->modify("+".$max_duration['max_duration']." hours");
                $delivery_datetime = $m_date->format("m-d-Y H:i:s");
                $delivery_datetime = str_replace('-', '/', $delivery_datetime); 

                $data =array(  
                  "cust_id" => $pro['cust_id'],
                  "category_id" => 7,
                  "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
                  "delivery_datetime" => $delivery_datetime,
                  "total_quantity" => 1,
                  "width" => $avg['width'],
                  "height" => $avg['height'],
                  "length" => $avg['length'],
                  "total_weight" => $total_weight,
                  "unit_id" =>1,
                  "from_country_id" => $from_add['country_id'],
                  "from_state_id" => $from_add['state_id'],
                  "from_city_id" => $from_add['city_id'],
                  "from_latitude" => $from_add['latitude'],
                  "from_longitude" => $from_add['longitude'],
                  "to_country_id" => $pro['country_id'],
                  "to_state_id" => $pro['state_id'], 
                  "to_city_id" => $pro['city_id'],
                  "to_latitude" => $pro['latitude'],
                  "to_longitude" => $pro['longitude'],
                  "service_area_type" => "local",
                  "transport_type" => "earth",
                  "handling_by" => 0,
                  "dedicated_vehicle" => 0,
                  "package_value" => 0,
                  "custom_clearance_by" =>"NULL",
                  "old_new_goods" =>"NULL",
                  "custom_package_value" =>0,
                  "loading_time" =>0,
                  "transport_vehichle_type" => $transport_vehichle_type, 
                );
                $order_price = $this->api_courier->calculate_order_price($data);
                //echo json_encode($order_price); die();
                if($is_pickup > 0){
                    $data = array(
                      'total_price'=>(string) $order_price['total_price'] ,
                      'standard_price'=> (string)$order_price['standard_price'] ,
                      'urgent_fee'=> (string)$order_price['urgent_fee'] ,
                      'ins_fee'=> (string)$order_price['ins_fee'] ,
                      'handling_fee'=> (string)$order_price['handling_fee'] ,
                      'dedicated_vehicle_fee'=> (string)$order_price['dedicated_vehicle_fee'] ,
                      'currency_id'=> (string)$order_price['currency_id'] ,
                      'currency_sign'=> (string)$order_price['currency_sign'] ,
                      'currency_title'=> (string)$order_price['currency_title'] ,
                      'distance_in_km'=> (string)$order_price['distance_in_km'] ,
                      'advance_percent'=> (string)$order_price['advance_percent'] ,
                      'commission_percent'=> (string)$order_price['commission_percent'] ,
                      'old_goods_custom_commission_percent'=> (string)$order_price['old_goods_custom_commission_percent'] ,
                      'old_goods_custom_commission_fee'=> (string)$order_price['old_goods_custom_commission_fee'] ,
                      'new_goods_custom_commission_percent'=> (string)$order_price['new_goods_custom_commission_percent'] ,
                      'new_goods_custom_commission_fee'=> (string)$order_price['new_goods_custom_commission_fee'] ,
                      'max_duration'=> ((string)$max_duration['max_duration']!='')?(string)$max_duration['max_duration']:'0',
                    );
                    $provider_details['pickup_charge'] = $data;
                }
                if($is_drop > 0){
                  $data = array(
                      'total_price'=>(string) $order_price['total_price'] ,
                      'standard_price'=> (string)$order_price['standard_price'] ,
                      'urgent_fee'=> (string)$order_price['urgent_fee'] ,
                      'ins_fee'=> (string)$order_price['ins_fee'] ,
                      'handling_fee'=> (string)$order_price['handling_fee'] ,
                      'dedicated_vehicle_fee'=> (string)$order_price['dedicated_vehicle_fee'] ,
                      'currency_id'=> (string)$order_price['currency_id'] ,
                      'currency_sign'=> (string)$order_price['currency_sign'] ,
                      'currency_title'=> (string)$order_price['currency_title'] ,
                      'distance_in_km'=> (string)$order_price['distance_in_km'] ,
                      'advance_percent'=> (string)$order_price['advance_percent'] ,
                      'commission_percent'=> (string)$order_price['commission_percent'] ,
                      'old_goods_custom_commission_percent'=> (string)$order_price['old_goods_custom_commission_percent'] ,
                      'old_goods_custom_commission_fee'=> (string)$order_price['old_goods_custom_commission_fee'] ,
                      'new_goods_custom_commission_percent'=> (string)$order_price['new_goods_custom_commission_percent'] ,
                      'new_goods_custom_commission_fee'=> (string)$order_price['new_goods_custom_commission_fee'] ,
                      'max_duration'=> ((string)$max_duration['max_duration']!='')?(string)$max_duration['max_duration']:'0',
                    );
                  $provider_details['drop_charge'] = $data;
                }
              }    
            } //else{ $provider_details['pickup_charge'] = 'NULL'; }
          //***********Courier************************
          
          array_push($providers , $provider_details);
          //$rating = (int)$this->api->get_laundry_review_details((int)$provider_id)['ratings'];
        }
      }
      if(!empty($providers)){
        $this->response  = ['response' => 'success' , 'message' => 'list_found' , 'providers' => $providers];
      }else{
        $this->response  = ['response' => 'fail' , 'message' => 'list_not_found' , 'providers' => "NULL"];
      }
      echo json_encode($this->response); die();
    }
    public function provider_laundry_booking()
    {
      //echo json_encode($_POST); die();
      $today = date('Y-m-d H:i:s');
      $cust_id = $this->input->post('cust_id');
      $pickuptime = $this->input->post('pickuptime');
      $pickupdate = $this->input->post('pickupdate');
      $payment_mode = $this->input->post('payment_mode');
      $cod_payment_type = $this->input->post('cod_payment_type');
      $expected_return_date = $this->input->post('expected_return_date');
      $description = $this->input->post('description');
      $walking_id = $this->input->post('walking_id');
      $pickup_courier_price = ($_POST['is_pickup']>0)?$_POST["pickup_courier_price"]:0;
      $drop_courier_price = ($_POST['is_drop']>0)?$_POST["pickup_courier_price"]:0;
      $total_price = $_POST['booking_price'] + $pickup_courier_price + $drop_courier_price;
      
      $provider = $this->api->get_laundry_provider_profile($cust_id);
      //$user = $this->api->get_user_details($this->cust_id);
      $country_currency = $this->api->get_country_currency_detail($provider["country_id"]);
      $laundry_advance_payment = $this->api->get_laundry_advance_payment($provider["country_id"]);
      $gonagoo_commission_amount = ($_POST['booking_price']/100)*$laundry_advance_payment['gonagoo_commission'];
      
      $c_id = $this->input->post('cat_id', TRUE);
      $sb_id = $this->input->post('sub_cat_id', TRUE);
      $ct = $this->input->post('count', TRUE);
      
      $cat_id = explode(",",$c_id);
      $sub_cat_id = explode(",",$sb_id);
      $count = explode(",",$ct);

      $width = array();
      $length = array();
      
      $booking_details = array();
      $max_height = 0;
      $total_weight = 0;

      for($i=0; $i<sizeof($sub_cat_id); $i++) {
        $sub_categori_details = $this->api->get_laundry_sub_category($sub_cat_id[$i]);

        $max_ht = 0;
        $max_wet = 0;
        $max_wet =  $sub_categori_details['weight']*$count[$i];
        $max_ht = $sub_categori_details['height']*$count[$i];
        $total_weight = $total_weight + ($sub_categori_details['weight']*$count[$i]);
        $max_height = $max_height + ($sub_categori_details['height']*$count[$i]);  
        $data['cust_id'] = $cust_id;
        $data['cat_id'] = $cat_id[$i];
        $data['sub_cat_id'] = $sub_cat_id[$i];
        $data['height'] = $max_ht;    
        $data['weight'] = $max_wet;    
        $data['width'] = $sub_categori_details['width'];    
        $data['length'] =$sub_categori_details['length'];
        $data['count'] = $count[$i];
        array_push($booking_details,$data);

        array_push($width , $sub_categori_details['width']);
        array_push($length , $sub_categori_details['length']);
      }
      //echo json_encode($booking_details); die();
      

      //echo json_encode($booking_details); die();
      $country_id = $_POST['customer_country'];
      $phone_code = $this->api->get_country_code_by_id($country_id);


      $walking_id = $this->input->post('walking_id');
      if(!isset($walking_id) || $walking_id == 'NULL') {
        $data = array(
          'firstname' => $_POST['first_name'],
          'provider_id' => $cust_id,
          'lastname' => $_POST['last_name'],
          'phone' => $_POST['mobile'],
          'email' => $_POST['email'],
          'latitude' => $_POST['walkin_lat'],
          'longitude' => $_POST['walkin_long'],
          'addr_line2' => $_POST['addr_line2'],
          'addr_line1' => $_POST['addr_line1'],
          'street_name' => $_POST['street_name'],
          'country_id' => $country_id,
          'phone_code' => $phone_code,
          'cat_id' => 9,
          'cre_datetime' => date('Y-m-d H:i:s'),
          'provider_id' => $cust_id,
        );
        $walkin_cust_id = $this->api->register_walkin_customer_profile($data);
      } else { $walkin_cust_id = explode('~', $walking_id)[0]; }
      $walkin_cust = $this->api->get_walkin_customer_profile($walkin_cust_id);

      $data = array(
        'cust_id' => $walkin_cust['cust_id'], 
        'provider_id' => $provider['cust_id'], 
        'walkin_cust_id' => $walkin_cust_id, 
        'cre_datetime' => $today, 
        'booking_price' => $_POST['booking_price'], 
        'total_price' => trim($total_price),
        'currency_id' => $country_currency['currency_id'], 
        'currency_sign' => $_POST['currency_sign'], 
        'pickup_payment' => ($_POST['cod_payment_type']=='at_pickup')?trim($total_price):0, 
        'deliver_payment' => ($_POST['cod_payment_type']=='at_deliver')?trim($total_price):0, 
        'country_id' => $walkin_cust['country_id'], 
        'paid_amount' => 0, 
        'pickup_courier_price' => $pickup_courier_price,  
        'drop_courier_price' => $drop_courier_price,  
        'payment_mode' => ($_POST['payment_mode']=='cod')?'cod':'NULL',  
        'payment_by' => "NULL", 
        'payment_datetime' => 'NULL', 
        'balance_amount' => trim($total_price), 
        'complete_paid' => 0, 
        'booking_status' => "accepted",
        'status_updated_by' => $walkin_cust['cust_id'], 
        'status_updated_by_user_type' => "customer", 
        'status_update_datetime' => $today, 
        'item_count' => $_POST['item_count'], 
        'cust_name' => $walkin_cust['firstname']." ".$walkin_cust['lastname'], 
        'cust_contact' => $walkin_cust['phone'], 
        'cust_email' => $walkin_cust['email'], 
        'operator_name' => $provider['firstname']." ".$provider['lastname'], 
        'operator_company_name' => $provider['company_name'], 
        'operator_contact_name' => $provider['contact_no'], 
        'is_pickup' => $_POST['is_pickup'], 
        'is_drop' => $_POST['is_drop'], 
        'gonagoo_commission_per' => $laundry_advance_payment['gonagoo_commission'], 
        'gonagoo_commission_amount' => $gonagoo_commission_amount, 
        'cat_id' => 9,
        'expected_return_date' => trim($expected_return_date), 
        'description' => trim($description), 
        'cod_payment_type' => trim($cod_payment_type),
      );
      //echo json_encode($data); die();
      if($booking_id = $this->api->create_laundry_booking($data)){
        $this->response  = ['response' => 'success' , 'laundry_booking_id' => (string)$booking_id];
        $walkin_data = array(
          'service_booking_id' => (int)$booking_id, 
        );
        $this->api->update_walkin_customer($walkin_data, $walkin_cust_id);

        //Update Booking items Detail-------------------------------
          foreach ($booking_details as $bk){
            $rate = $this->api->get_laundry_charges_list_apend($provider['cust_id'], $bk['sub_cat_id'], $provider["country_id"]);
            $total_charge = $rate['charge_amount']*$bk['count'];
            $data2 =  array(
              'booking_id' => $booking_id, 
              'cat_id' => $bk['cat_id'], 
              'sub_cat_id' => $bk['sub_cat_id'] ,
              'quantity' => $bk['count'], 
              'single_unit_price' => $rate['charge_amount'], 
              'total_price' => $total_charge, 
              'currency_sign' => $rate['currency_sign'], 
              'cre_datetime' => date('Y-m-d H:i:s'), 
            );
            if($booking_details_id = $this->api->create_laundry_booking_details($data2)){
              $this->response  += ['response' => 'success' , 'booking_details_id' => (string)$booking_details_id];
            } else {
              $this->response  += ['response' => 'failed' , 'booking_details_id' => "0"];
            }
          }
        //Update Booking items Detail-------------------------------

        //*********Send Booking SMS to customer*********************
          $country_code = $this->api->get_country_code_by_id($walkin_cust["country_id"]);
          $user_message = $this->lang->line('Your booking is confirmed. Laundry Booking ID - ').$booking_id;
          $this->api->sendSMS((int)$country_code.trim($walkin_cust['phone']), $user_message);
        //*********Send Booking SMS to customer*********************

        //*********Send Booking SMS to provider*********************
          $country_code = $this->api->get_country_code_by_id($provider["country_id"]);
          $provider_message = $this->lang->line('You have recieved Laundry order. Booking ID - ').$booking_id;
          $this->api->sendSMS((int)$country_code.trim($provider['contact_no']), $provider_message);
        //*********Send Booking SMS to provider*********************

        //*********Send Email to customer***************************
          if( trim($walkin_cust['firstname']) == 'NULL' || trim($walkin_cust['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($walkin_cust['email']));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($walkin_cust['firstname'])." ".trim($walkin_cust['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($walkin_cust['email']),$this->lang->line('laundry booking confirmation'), trim($user_message));
        //*********Send Email to customer***************************

        //*********Send Email to provider***************************
          if( trim($provider['firstname']) == 'NULL' || trim($provider['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($provider['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider['firstname'])." ".trim($provider['lastname']); }
          $this->api_sms->send_email_text($provider_name, trim($provider['email_id']),$this->lang->line('laundry booking confirmation'), trim($provider_message));
        //*********Send Email to provider***************************

        //*********Send push notification to provider***************
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details($provider['cust_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('laundry booking confirmation'), 'type' => 'laundry-booking-confirmation', 'notice_date' => $today, 'desc' => $provider_message);
            $this->api->sendFCM($msg, $arr_provider_fcm_ids, $api_key);

            //Push Notification APN
            $msg_apn_provider =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $provider_message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
        //*********Send push notification to provider***************
      
        //*********Post courier_workroom & global_workroom**********
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $walkin_cust['cust_id'],
            'deliverer_id' => $provider['cust_id'],
            'text_msg' => $user_message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $walkin_cust['cust_id'],
            'type' => 'booking_status',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);

          $global_workroom_update = array(
            'service_id' => $booking_id,
            'cust_id' => $walkin_cust['cust_id'],
            'deliverer_id' => $provider['cust_id'],
            'sender_id' => $walkin_cust['cust_id'],
            'cat_id' => 9
          );
          $this->api->create_global_workroom($global_workroom_update);
        //*********Post courier_workroom & global_workroom**********  
        
        //echo json_encode($data); die();
        //*********courier booking**********************************
          if($_POST['is_pickup']=="1"){
            //echo 'in courier'; die();
            $from_add = $this->api->get_address_from_book($_POST['from_address']);
            $avg = array(
              'weight' => round(($total_weight/1000),2),
              'height' => $max_height,
              'length' => max($length),
              'width'  => max($width),
            );

            $volume = $avg['width'] * $avg['height'] * $avg['length'];
            $dimension_id = 0 ;
            if($dimensions = $this->api->get_dimension_id_for_laundry($volume)){
              $dimension_id = $dimensions['dimension_id']; 
            }
            $total_weight =  $avg['weight'];
            $transport_vehichle_type = 0;
            if($total_weight > 15){ $transport_vehichle_type = 28;
            } else { $transport_vehichle_type = 27; }
            $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$provider['latitude'],$provider['longitude']);

            
            $data = array(
              "category_id" => 7,
              "total_weight" =>$total_weight, 
              "unit_id" => 1,
              "from_country_id" => $from_add['country_id'], 
              "from_state_id" => $from_add['state_id'], 
              "from_city_id" => $from_add['city_id'], 
              "to_country_id" => $provider['country_id'], 
              "to_state_id" => $provider['state_id'], 
              "to_city_id" => $provider['city_id'], 
              "service_area_type" =>'local',
              "transport_type" => 'earth',
              "width" => $avg['width'],
              "height" => $avg['height'],
              "length" => $avg['length'],
              "total_quantity" => 1,
            );
            if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
              $p_date = explode('-',$pickupdate);
              $n_date =  $p_date[2]."/".$p_date[1]."/".$p_date[0];
              $my_date = $n_date.' '.$pickuptime.":00";
              $my_date = str_replace('/', '-', $my_date);
              $m_date = new DateTime($my_date);
              $ex_date = $m_date;
              $ex_date->modify("+24 hours");
              $ex_datetime = $ex_date->format("m-d-Y H:i:s");
              $expiry_date = str_replace('-', '/', $ex_datetime);
              $m_date->modify("+".$max_duration['max_duration']." hours");
              $delivery_datetime = $m_date->format("m-d-Y H:i:s");
              $delivery_datetime = str_replace('-', '/', $delivery_datetime);  
            }

            $data =array(  
              "cust_id" => $provider['cust_id'],
              "category_id" => 7,
              "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
              "total_quantity" => 1,
              "delivery_datetime" => $delivery_datetime,
              "width" => $avg['width'],
              "height" => $avg['height'],
              "length" => $avg['length'],
              "total_weight" => $total_weight,
              "unit_id" =>1,
              "from_country_id" => $from_add['country_id'],
              "from_state_id" => $from_add['state_id'],
              "from_city_id" => $from_add['city_id'],
              "from_latitude" => $from_add['latitude'],
              "from_longitude" => $from_add['longitude'],
              "to_country_id" => $provider['country_id'],
              "to_state_id" => $provider['state_id'], 
              "to_city_id" => $provider['city_id'],
              "to_latitude" => $provider['latitude'],
              "to_longitude" => $provider['longitude'],
              "service_area_type" => "local",
              "transport_type" => "earth",
              "handling_by" => 0,
              "dedicated_vehicle" => 0,
              "package_value" => 0,
              "custom_clearance_by" =>"NULL",
              "old_new_goods" =>"NULL",
              "custom_package_value" =>0,
              "loading_time" =>0,
              "transport_vehichle_type" => $transport_vehichle_type, 
            );
            $order_price = $this->api_courier->calculate_order_price($data);
            //echo json_encode($order_price); die();
            if ($description == '') { $description = $this->lang->line('Laundry Parcel'); }
            if(isset($cod_payment_type)) {
              if(($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')){
                $cod_payment_type = 'at_pickup';
                $pickup_payment = $order_price['total_price'];
                $deliver_payment = 0;
              } else {
                $cod_payment_type = 'at_deliver';
                $pickup_payment = 0;
                $deliver_payment = $order_price['total_price'];
              }
            } else { 
              $cod_payment_type = 'NULL';
              $pickup_payment = 0;
              $deliver_payment = $order_price['total_price']; 
            }
            
            
             $p_date = explode('-',$pickupdate);
             $pick_date =  $p_date[2]."/".$p_date[1]."/".$p_date[0];

            $delivery_datetime = explode('/',$delivery_datetime);
            $tm = explode(' ',$delivery_datetime[2]);
            $delivery_datetime = $tm[0]."-".$delivery_datetime[0]."-".$delivery_datetime[1].' '.$tm[1];

            $expiry_date = explode('/',$expiry_date);
            $tm = explode(' ',$expiry_date[2]);
            $expiry_date = $tm[0]."-".$expiry_date[0]."-".$expiry_date[1].' '.$tm[1];
            
            $my_string ="cust_id=".$provider['cust_id']."&from_address=".$from_add['street_name']."&from_address_name=".$from_add['firstname']." ".$from_add['lastname']."&from_address_contact=".$from_add['mobile_no']."&from_latitude=".$from_add['latitude']."&from_longitude=".$from_add['longitude']."&from_country_id=".$from_add['country_id']."&from_state_id=".$from_add['state_id']."&from_city_id=".$from_add['city_id']."&from_addr_type=0&from_relay_id=0&to_address=".$provider['address']."&to_address_name=".$provider['firstname']." ".$provider['lastname']."&to_address_contact=".$provider['contact_no']."&to_country_id=".$provider['country_id']."&to_state_id=".$provider['state_id']."&to_city_id=".$provider['city_id']."&to_latitude=".$provider['latitude']."&to_longitude=".$provider['longitude']."&from_address_info=".$from_add['street_name']."&to_address_info=".$provider['address']."&to_addr_type=0&to_relay_id=0&pickup_datetime=".$pick_date." ".$pickuptime.":00&delivery_datetime=".$delivery_datetime."&expiry_date=".$expiry_date."&order_type=Shipping_Only&service_area_type=local&order_contents=[0]&order_description=".trim($description)."&deliver_instructions=".$from_add['deliver_instructions']."&pickup_instructions=".$from_add['pickup_instructions']."&transport_type=earth&vehical_type_id=".$transport_vehichle_type."&dedicated_vehicle=0&with_driver=0&insurance=0&package_value=0&handling_by=0&total_quantity=[1]&width=[".$avg['width']."]&height=[".$avg['height']."]&length=[".$avg['length']."]&total_weight=[".(string)$total_weight."]&unit_id=[1]&advance_payment=0&payment_method=NULL&pickup_payment=".$pickup_payment."&deliver_payment=".$deliver_payment."&payment_mode=cod&visible_by=0&from_address_email=".$from_add['email']."&to_address_email=".$provider['email_id']."&ref_no=".$booking_id."&dimension_id=[".$dimension_id."]&category_id=7&custom_clearance_by=NULL&old_new_goods=NULL&custom_package_value=0&need_tailgate=0&loading_time=0&dangerous_goods=[0]&is_laundry=1&cod_payment_type=".$cod_payment_type;
            //echo json_encode($my_string); die();
            $ch = curl_init(base_url()."api/book-courier-order"); // url to send sms
            curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
            curl_setopt($ch, CURLOPT_POST, 1); // method to call url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
            curl_setopt($ch, CURLOPT_POSTFIELDS,$my_string); 
            $courier_booking = curl_exec($ch); // execute url and save response
            curl_close($ch); // close url connection
            //echo $courier_booking; die();
            if(strpos($courier_booking, 'success') !== false) {
              $or = substr($courier_booking, strpos($courier_booking, '_id":') + 5);
              $ord = substr(strrev($or), strpos(strrev($or), '}') + 1);
              $order_id = strrev($ord);

              $update = array('pickup_order_id' => $order_id);
              $this->api->laundry_booking_update($update,$booking_id);

              $this->response  += ['response' => 'success' , 'courier_booking' => (string)$order_id,'message' => $this->lang->line('laundry_booking_and_courier_create_successfully') ];
            } else { $this->response  += ['response' => 'failed' , 'courier_booking' => "0" , 'message' => $this->lang->line('courier_booking_error') ]; }
          } else { $this->response  += ['message' => $this->lang->line('laundry_booking_create_successfully')]; }
        //******courier booking******************************************
      } else { $this->response  = ['response' => 'failed' , 'laundry_booking_id' => "0" , 'message' => $this->lang->line('laundry_booking_error')]; }
      echo json_encode($this->response);
    }
  //End provider Create Booking----------------------------
  
  //Start provider laundry payment-------------------------
    public function laundry_provider_booking_mark_complete_paid()
    {
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $cust_id = (int)$booking_details['provider_id'];
      $today = date('Y-m-d H:i:s');

      $data = array(
        'paid_amount' => $booking_details['total_price'],
        'payment_method' => 'NULL', 
        'payment_mode' => 'cod', 
        'payment_by' => 'mobile ', 
        'payment_datetime' => trim($today),
        'balance_amount' => '0', 
        'complete_paid' => 1, 
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id;
        if($booking_details['walkin_cust_id'] > 0) {
          $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['phone'];
          $email_id = $user_details['email'];
          $state_id = 0;
          $city_id = 0;
        } else {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['mobile1'];
          $email_id = $user_details['email1'];
          $state_id = $user_details['state_id'];
          $city_id = $user_details['city_id'];

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        }

        //SMS to customer
        $country_code = $this->api->get_country_code_by_id($country_id);
        $this->api->sendSMS((int)$country_code.trim($mobile), $message);
        
        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($email_id));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email1']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);

        /*************************************** Payment Section ****************************************/
          /* COD payment invoice------------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

            $user_country = $this->api->get_country_details(trim($country_id));
            $user_state = $this->api->get_state_details(trim($state_id));
            $user_city = $this->api->get_city_details(trim($city_id));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($email_id)));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($booking_details['total_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Booking Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_id.'_laundry_booking.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_id);
            //Update to workroom
            $workroom_update = array(
              'order_id' => $booking_id,
              'cust_id' => $booking_details['cust_id'],
              'deliverer_id' => $booking_details['provider_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "laundry-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $booking_details['provider_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $customer_name,
              'deliverer_name' => $provider_name,
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 9
            );
            $this->api->update_to_workroom($workroom_update);
          /* -------------------------------------------------------------------- */
          
            /* --------------------------Generate Ticket pdf----------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* --------------------------Generate Ticket pdf----------------------- */ 
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $booking_details['cust_email'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
            $emailAddress = trim($email_id);
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //Deduct Gonagoo Commission from provider account-------------------------
            if($customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_customer_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($booking_details['currency_sign']));
            }

            $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($booking_details['currency_sign']),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => 9,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------
          
          /* Ownward Laundry commission invoice---------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_details['booking_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
          /* -------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $provider_details['email1'];
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        /*************************************** Payment Section ****************************************/
       $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }
    public function payment_by_provider_mtn()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id', TRUE);
      $phone_no = $this->input->post('phone_no', TRUE);
      $today = date('Y-m-d H:i:s'); 
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $total_price = trim($booking_details['total_price']);
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$total_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      //$contents = curl_exec($ch);
      //$mtn_pay_res = json_decode($contents, TRUE);
      //var_dump($mtn_pay_res); die();
      
      $payment_method = 'mtn';
      $transaction_id = '1234567890';
      //$transaction_id = trim($mtn_pay_res['TransactionID']);
      
      $payment_data = array();
      // if($mtn_pay_res['StatusCode'] === "01"){
      if(1){
        $data = array(
          'paid_amount' => $booking_details['total_price'],
          'payment_method' => trim($payment_method), 
          'transaction_id' => trim($transaction_id), 
          'payment_mode' => 'online', 
          'payment_by' => 'mobile', 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1, 
        );
        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          if($booking_details['walkin_cust_id'] > 0) {
            $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
            $country_id = $user_details['country_id'];
            $mobile = $user_details['phone'];
            $email_id = $user_details['email'];
            $state_id = 0;
            $city_id = 0;
          } else {
            $user_details = $this->api->get_user_details($booking_details['cust_id']);
            $country_id = $user_details['country_id'];
            $mobile = $user_details['mobile1'];
            $email_id = $user_details['email1'];
            $state_id = $user_details['state_id'];
            $city_id = $user_details['city_id'];
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
          }
          
          //SMS to customer
          $country_code = $this->api->get_country_code_by_id($country_id);
          $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id;
          $this->api->sendSMS((int)$country_code.trim($mobile), $message);
          
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($email_id));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

          $provider_details = $this->api->get_user_details($booking_details['provider_id']);
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email1']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['provider_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          /*************************************** Payment Section ****************************************/
            if($booking_details['walkin_cust_id'] == 0) {
              //Add Payment to customer account---------------------------------------
                if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
                } else {
                  $insert_data_provider_master = array(
                    "user_id" => $booking_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($booking_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                  $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                }

                $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$booking_id,
                  "user_id" => (int)$booking_details['cust_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($booking_details['total_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($booking_details['currency_sign']),
                  "cat_id" => 9
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------

              //deduct Payment from customer account----------------------------------
                $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$booking_id,
                  "user_id" => (int)$booking_details['cust_id'],
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'payment',
                  "amount" => trim($booking_details['total_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($booking_details['currency_sign']),
                  "cat_id" => 9
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------
            }

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            /* Stripe payment invoice------------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

              $user_country = $this->api->get_country_details(trim($country_id));
              $user_state = $this->api->get_state_details(trim($state_id));
              $user_city = $this->api->get_city_details(trim($city_id));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($email_id)));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($booking_details['total_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Booking Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_id.'_laundry_booking.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_id);
              //Update to workroom
              $workroom_update = array(
                'order_id' => $booking_id,
                'cust_id' => $booking_details['cust_id'],
                'deliverer_id' => $booking_details['provider_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  "laundry-invoices/".$pdf_name,
                'cre_datetime' => $today,
                'sender_id' => $booking_details['provider_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $customer_name,
                'deliverer_name' => $provider_name,
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 9
              );
              $this->api->update_to_workroom($workroom_update);
            /* ----------------------------------------------------------------------- */
            
            
            /* --------------------------Generate Ticket pdf----------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* --------------------------Generate Ticket pdf----------------------- */ 
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $booking_details['cust_email'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
              $emailAddress = trim($email_id);
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */

            //Deduct Gonagoo Commission from provider account-------------------------
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "type" => 1,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($booking_details['currency_sign']),
                "country_id" => trim($provider_details['country_id']),
                "cat_id" => 9,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------
            
            /* Ownward Laundry commission invoice------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_details['booking_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
            /* ----------------------------------------------------------------------- */
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $provider_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */

            //Update Courier Payment----------------------------------------------------
              if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
                $courier_update_data = array(
                  "payment_method" => 'mtn',
                  "payment_mode" => 'payment',
                  "paid_amount" => $booking_details['pickup_courier_price'],
                  "transaction_id" => trim($transaction_id),
                  "today" => trim($today),
                  "cod_payment_type" => 'NULL',
                  "order_id" => $booking_details['pickup_order_id'],
                );
                $this->courier_payment_confirm($courier_update_data);
              }
            //Update Courier Payment----------------------------------------------------
          /*************************************** Payment Section ****************************************/
          $this->response  = ['response' => 'success' , 'message' => $this->lang->line('laundry_payment_success')];
        } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('laundry_payment_error')]; }
      } else {  $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('laundry_payment_error')]; }
      echo json_encode($this->response);
    }
    public function payment_by_provider_stripe()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id', TRUE); $booking_id = (int) $booking_id;
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $today = date('Y-m-d H:i:s');
      $payment_method = "stripe";
      $booking_details = $this->api->laundry_booking_details($booking_id);
      //echo json_encode($booking_details); die();

      $data = array(
        'paid_amount' => $booking_details['total_price'],
        'payment_method' => trim($payment_method), 
        'transaction_id' => trim($transaction_id), 
        'payment_mode' => 'online', 
        'payment_by' => 'mobile', 
        'payment_datetime' => trim($today),
        'balance_amount' => '0', 
        'complete_paid' => 1, 
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        if($booking_details['walkin_cust_id'] > 0) {
          $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['phone'];
          $email_id = $user_details['email'];
          $state_id = 0;
          $city_id = 0;
        } else {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['mobile1'];
          $email_id = $user_details['email1'];
          $state_id = $user_details['state_id'];
          $city_id = $user_details['city_id'];

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        }

        //SMS to customer
        $country_code = $this->api->get_country_code_by_id($country_id);
        $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id;
        $this->api->sendSMS((int)$country_code.trim($mobile), $message);
        
        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($email_id));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email1']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);

        /*************************************** Payment Section ****************************************/
          if($booking_details['walkin_cust_id'] == 0) {
            //Add Payment to customer account---------------------------------------
              if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //----------------------------------------------------------------------

            //deduct Payment from customer account----------------------------------
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //----------------------------------------------------------------------
          }

          //Add Payment to Provider account-----------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $booking_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            }

            $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'advance',
              "amount" => trim($booking_details['total_price']),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          /* Stripe payment invoice------------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

            $user_country = $this->api->get_country_details(trim($country_id));
            $user_state = $this->api->get_state_details(trim($state_id));
            $user_city = $this->api->get_city_details(trim($city_id));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($email_id)));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($booking_details['total_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Booking Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_id.'_laundry_booking.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_id);
            //Update to workroom
            $workroom_update = array(
              'order_id' => $booking_id,
              'cust_id' => $booking_details['cust_id'],
              'deliverer_id' => $booking_details['provider_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "laundry-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $booking_details['provider_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $customer_name,
              'deliverer_name' => $provider_name,
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 9
            );
            $this->api->update_to_workroom($workroom_update);
          /* ----------------------------------------------------------------------- */
          
          /* --------------------------Generate Ticket pdf----------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* --------------------------Generate Ticket pdf----------------------- */ 
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $booking_details['cust_email'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
            $emailAddress = trim($email_id);
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //Deduct Gonagoo Commission from provider account-------------------------
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "type" => 1,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($booking_details['currency_sign']),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => 9,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------
          
          /* Ownward Laundry commission invoice------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_details['booking_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
          /* ----------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $provider_details['email1'];
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */

          //Update Courier Payment----------------------------------------------------
          if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
            $courier_update_data = array(
              "payment_method" => 'stripe',
              "payment_mode" => 'payment',
              "paid_amount" => $booking_details['pickup_courier_price'],
              "transaction_id" => trim($transaction_id),
              "today" => trim($today),
              "cod_payment_type" => 'NULL',
              "order_id" => $booking_details['pickup_order_id'],
            );
            $this->courier_payment_confirm($courier_update_data);
          }
        //Update Courier Payment----------------------------------------------------
        /*************************************** Payment Section ****************************************/

        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('laundry_payment_success')];
      } else { $this->response  = ['response' => 'fail' , 'message' => $this->lang->line('laundry_payment_error')]; }
      echo json_encode($this->response);
    }
    public function payment_by_provider_orange()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id'); 
      $cust_id = $this->input->post('cust_id'); 
      $session_token = $this->input->post('notif_token');  
      $pay_token = $this->input->post('pay_token'); 
      $order_payment_id = 'gonagoo_'.time().'_'.$booking_id; 
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $amount = $booking_details['total_price'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "booking_id" => $order_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      //$transaction_id = $payment_details['txnid'];
      $transaction_id = 345673;
      //echo json_encode($payment_details); die();

      // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
      if(1) {
        $data = array(
          'paid_amount' => $booking_details['total_price'],
          'payment_method' => trim($payment_method), 
          'transaction_id' => trim($transaction_id), 
          'payment_mode' => 'online', 
          'payment_by' => 'mobile', 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1, 
        );
        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          if($booking_details['walkin_cust_id'] > 0) {
            $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
            $country_id = $user_details['country_id'];
            $mobile = $user_details['phone'];
            $email_id = $user_details['email'];
            $state_id = 0;
            $city_id = 0;
          } else {
            $user_details = $this->api->get_user_details($booking_details['cust_id']);
            $country_id = $user_details['country_id'];
            $mobile = $user_details['mobile1'];
            $email_id = $user_details['email1'];
            $state_id = $user_details['state_id'];
            $city_id = $user_details['city_id'];
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
          }
          
          //SMS to customer
          $country_code = $this->api->get_country_code_by_id($country_id);
          $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id;
          $this->api->sendSMS((int)$country_code.trim($mobile), $message);
          
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($email_id));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

          $provider_details = $this->api->get_user_details($booking_details['provider_id']);
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email1']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['provider_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          /*************************************** Payment Section ****************************************/
            if($booking_details['walkin_cust_id'] == 0) {
              //Add Payment to customer account---------------------------------------
                if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
                } else {
                  $insert_data_provider_master = array(
                    "user_id" => $booking_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($booking_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                  $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                }

                $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$booking_id,
                  "user_id" => (int)$booking_details['cust_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($booking_details['total_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($booking_details['currency_sign']),
                  "cat_id" => 9
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------

              //deduct Payment from customer account----------------------------------
                $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$booking_id,
                  "user_id" => (int)$booking_details['cust_id'],
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'payment',
                  "amount" => trim($booking_details['total_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($booking_details['currency_sign']),
                  "cat_id" => 9
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------
            }

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            /* Stripe payment invoice------------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

              $user_country = $this->api->get_country_details(trim($country_id));
              $user_state = $this->api->get_state_details(trim($state_id));
              $user_city = $this->api->get_city_details(trim($city_id));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($email_id)));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($booking_details['total_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Booking Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_id.'_laundry_booking.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_id);
              //Update to workroom
              $workroom_update = array(
                'order_id' => $booking_id,
                'cust_id' => $booking_details['cust_id'],
                'deliverer_id' => $booking_details['provider_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  "laundry-invoices/".$pdf_name,
                'cre_datetime' => $today,
                'sender_id' => $booking_details['provider_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $customer_name,
                'deliverer_name' => $provider_name,
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 9
              );
              $this->api->update_to_workroom($workroom_update);
            /* ----------------------------------------------------------------------- */
            
            /* --------------------------Generate Ticket pdf----------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* --------------------------Generate Ticket pdf----------------------- */ 
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $booking_details['cust_email'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */
            
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
              $emailAddress = trim($email_id);
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */

            //Deduct Gonagoo Commission from provider account-------------------------
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "type" => 1,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($booking_details['currency_sign']),
                "country_id" => trim($provider_details['country_id']),
                "cat_id" => 9,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------
            
            /* Ownward Laundry commission invoice------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_details['booking_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
            /* ----------------------------------------------------------------------- */
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $provider_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */

            //Update Courier Payment----------------------------------------------------
              if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
                $courier_update_data = array(
                  "payment_method" => 'orange',
                  "payment_mode" => 'payment',
                  "paid_amount" => $booking_details['pickup_courier_price'],
                  "transaction_id" => trim($transaction_id),
                  "today" => trim($today),
                  "cod_payment_type" => 'NULL',
                  "order_id" => $booking_details['pickup_order_id'],
                );
                $this->courier_payment_confirm($courier_update_data);
              }
            //Update Courier Payment----------------------------------------------------
          /*************************************** Payment Section ****************************************/

          $this->response  = ['response' => 'success' , 'message' => $this->lang->line('laundry_payment_success')];
        }else{ $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('laundry_payment_error')]; }
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('laundry_payment_error')]; }
      echo json_encode($this->response);
    }
  //End provider laundry payment---------------------------
  
  //Start Laundry mark accepted/in-progress/completed------
    public function laundry_booking_mark_in_progress()
    {
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $cust_id = $this->input->post("cust_id"); $cust_id = (int)$cust_id;
      $today = date('Y-m-d H:i:s');

      $data = array(
        'booking_status' => 'in_progress',
        'status_updated_by' => trim($cust_id), 
        'status_updated_by_user_type' => 'provider', 
        'status_update_datetime' => trim($today),
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        $message = $this->lang->line('Your laundry booking is in-progress.');
        $booking_details = $this->api->laundry_booking_details($booking_id);
        if($booking_details['walkin_cust_id'] > 0) {
          $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['phone'];
          $email_id = $user_details['email'];
        } else {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['mobile1'];
          $email_id = $user_details['email1'];

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking status'), 'type' => 'booking-in-progress', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking status'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        }

        $country_code = $this->api->get_country_code_by_id($country_id);
        $this->api->sendSMS((int)$country_code.trim($mobile), $message);

        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($email_id));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email_id']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('Booking status updated successfully.')];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }

    public function laundry_booking_mark_completed()
    {
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $cust_id = $this->input->post("cust_id"); $cust_id = (int)$cust_id;
      $today = date('Y-m-d H:i:s');
      $message = $this->lang->line('Your laundry booking is completed.');

      $data = array(
        'booking_status' => 'completed',
        'status_updated_by' => trim($cust_id), 
        'status_updated_by_user_type' => 'provider', 
        'status_update_datetime' => trim($today),
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        $booking_details = $this->api->laundry_booking_details($booking_id);
        if($booking_details['walkin_cust_id'] > 0) {
          $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['phone'];
          $email_id = $user_details['email'];
        } else {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['mobile1'];
          $email_id = $user_details['email1'];
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking status'), 'type' => 'booking-in-progress', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking status'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        }

        $country_code = $this->api->get_country_code_by_id($country_id);
        $this->api->sendSMS((int)$country_code.trim($mobile), $message);
        
        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($email_id));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email_id']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('Booking status updated successfully.')];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }
  //End Laundry mark accepted/in-progress/completed--------
  
  //Start Laundry mark is Drop-----------------------------
    public function laundry_mark_is_drop()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->input->post("cust_id"); $cust_id = (int)$cust_id;
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $addr_id = $this->input->post("addr_id"); $addr_id = (int)$addr_id;
      $update_data = array(
        'is_drop' => 1,
        'drop_address_id' => (int)($addr_id), 
      );
      if($this->api->laundry_booking_update($update_data, $booking_id)) {
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('Booking status updated successfully.')];
      } else {
        $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('update_failed')];
      }
      echo json_encode($this->response);
    }
    public function create_drop_booking()
    {
      //echo json_encode($_POST); die();
      $pickuptime = $this->input->post('pickuptime');
      $pickupdate = $this->input->post('pickupdate');
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $provider = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
      $from_add = $this->api->get_address_from_book($booking_details['drop_address_id']);

      //Create drop booking by getting courier price
      $avg = $this->api->get_cloth_details_volum_by_booking_id($booking_id);  
      $volume = $avg['width'] * $avg['height'] * $avg['length'];
      $dimension_id = 0 ;
      if($dimensions = $this->api->get_dimension_id_for_laundry($volume)){
        $dimension_id = $dimensions['dimension_id']; 
      }
      
      $total_weight =  round(($avg['weight']/1000),2);
      $transport_vehichle_type = 0;
      if($total_weight > 15){
        $transport_vehichle_type = 28;
      }else{
        $transport_vehichle_type = 27;
      }

      $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$provider['latitude'],$provider['longitude']);
      //echo json_encode($distance_in_km); die();
      
      $data = array(
        "category_id" => 7,
        "total_weight" =>$total_weight, 
        "unit_id" => 1,
        "from_country_id" => $from_add['country_id'], 
        "from_state_id" => $from_add['state_id'], 
        "from_city_id" => $from_add['city_id'], 
        "to_country_id" => $provider['country_id'], 
        "to_state_id" => $provider['state_id'], 
        "to_city_id" => $provider['city_id'], 
        "service_area_type" =>'local',
        "transport_type" => 'earth',
        "width" => $avg['width'],
        "height" => $avg['height'],
        "length" => $avg['length'],
        "total_quantity" => 1,
      );
      if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
        $p_date = explode('-',$pickupdate);
        $n_date =  $p_date[2]."/".$p_date[1]."/".$p_date[0];
        $my_date = $n_date.' '.$pickuptime.":00";
        $my_date = str_replace('/', '-', $my_date);
        $m_date = new DateTime($my_date);
        $ex_date = $m_date;
        $ex_date->modify("+24 hours");
        $ex_datetime = $ex_date->format("m-d-Y H:i:s");
        $expiry_date = str_replace('-', '/', $ex_datetime);
        $m_date->modify("+".$max_duration['max_duration']." hours");
        $delivery_datetime = $m_date->format("m-d-Y H:i:s");
        $delivery_datetime = str_replace('-', '/', $delivery_datetime);  
      }

      $data =array(  
        "cust_id" => $provider['cust_id'],
        "category_id" => 7,
        "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
        "total_quantity" => 1,
        "delivery_datetime" => $delivery_datetime,
        "width" => $avg['width'],
        "height" => $avg['height'],
        "length" => $avg['length'],
        "total_weight" => $total_weight,
        "unit_id" =>1,
        "from_country_id" => $from_add['country_id'],
        "from_state_id" => $from_add['state_id'],
        "from_city_id" => $from_add['city_id'],
        "from_latitude" => $from_add['latitude'],
        "from_longitude" => $from_add['longitude'],
        "to_country_id" => $provider['country_id'],
        "to_state_id" => $provider['state_id'], 
        "to_city_id" => $provider['city_id'],
        "to_latitude" => $provider['latitude'],
        "to_longitude" => $provider['longitude'],
        "service_area_type" => "local",
        "transport_type" => "earth",
        "handling_by" => 0,
        "dedicated_vehicle" => 0,
        "package_value" => 0,
        "custom_clearance_by" =>"NULL",
        "old_new_goods" =>"NULL",
        "custom_package_value" =>0,
        "loading_time" =>0,
        "transport_vehichle_type" => $transport_vehichle_type, 
        "laundry" => 1, 
      );
      //echo json_encode($data); die();
      $order_price = $this->api_courier->calculate_order_price($data);
      //echo json_encode($order_price); die();
      if ($booking_details['description'] == '' || $booking_details['description'] == 'NULL' ) { $description = $this->lang->line('Laundry Parcel'); }
      if ($booking_details['drop_payment_complete'] == 0) {
        $cod_payment_type = 'at_deliver';
        $pickup_payment = 0;
        $deliver_payment = $order_price['total_price'];
      } else {
        $cod_payment_type = 'at_pickup';
        $pickup_payment = $order_price['total_price'];
        $deliver_payment = 0;
      }
      
      $pick_date = $pickupdate;
      $delivery_datetime = explode('/',$delivery_datetime);
      $tm = explode(' ',$delivery_datetime[2]);
      $delivery_datetime = $tm[0]."-".$delivery_datetime[0]."-".$delivery_datetime[1].' '.$tm[1];
      
      //echo json_encode($delivery_datetime); die();

      $expiry_date = explode('/',$expiry_date);
      $tm = explode(' ',$expiry_date[2]);
      $expiry_date = $tm[0]."-".$expiry_date[0]."-".$expiry_date[1].' '.$tm[1];

      if($booking_details['payment_mode'] == 'bank') {
        $pending_service_charges = 0;
        $cod_payment_type = 'at_pickup';
        $pickup_payment = $order_price['total_price'];
        $deliver_payment = 0;
      } else {
        $pending_service_charges = trim($booking_details['booking_price'])+trim($booking_details['drop_courier_price']);
      }
      
      $my_string ="cust_id=".$provider['cust_id']."&from_address=".$from_add['street_name']."&from_address_name=".$from_add['firstname']." ".$from_add['lastname']."&from_address_contact=".$from_add['mobile_no']."&from_latitude=".$from_add['latitude']."&from_longitude=".$from_add['longitude']."&from_country_id=".$from_add['country_id']."&from_state_id=".$from_add['state_id']."&from_city_id=".$from_add['city_id']."&from_addr_type=0&from_relay_id=0&to_address=".$provider['address']."&to_address_name=".$provider['firstname']." ".$provider['lastname']."&to_address_contact=".$provider['contact_no']."&to_country_id=".$provider['country_id']."&to_state_id=".$provider['state_id']."&to_city_id=".$provider['city_id']."&to_latitude=".$provider['latitude']."&to_longitude=".$provider['longitude']."&from_address_info=".$from_add['street_name']."&to_address_info=".$provider['address']."&to_addr_type=0&to_relay_id=0&pickup_datetime=".$pick_date." ".$pickuptime.":00&delivery_datetime=".$delivery_datetime."&expiry_date=".$expiry_date."&order_type=Shipping_Only&service_area_type=local&order_contents=[0]&order_description=".trim($description)."&deliver_instructions=".$from_add['deliver_instructions']."&pickup_instructions=".$from_add['pickup_instructions']."&transport_type=earth&vehical_type_id=".$transport_vehichle_type."&dedicated_vehicle=0&with_driver=0&insurance=0&package_value=0&handling_by=0&total_quantity=[1]&width=[".$avg['width']."]&height=[".$avg['height']."]&length=[".$avg['length']."]&total_weight=[".(string)$total_weight."]&unit_id=[1]&advance_payment=0&payment_method=NULL&pickup_payment=".$pickup_payment."&deliver_payment=".$deliver_payment."&payment_mode=cod&visible_by=0&from_address_email=".$from_add['email']."&to_address_email=".$provider['email_id']."&ref_no=".$booking_id."&dimension_id=[".$dimension_id."]&category_id=7&custom_clearance_by=NULL&old_new_goods=NULL&custom_package_value=0&need_tailgate=0&loading_time=0&dangerous_goods=[0]&is_laundry=1&cod_payment_type=".$cod_payment_type."&laundry_is_drop=1&pending_service_charges=".$pending_service_charges."&service_type=Laundry&laundry=1";
      //echo json_encode($my_string); die();
      $ch = curl_init(base_url()."api/book-courier-order"); // url to send sms
      curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
      curl_setopt($ch, CURLOPT_POST, 1); // method to call url
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
      curl_setopt($ch, CURLOPT_POSTFIELDS,$my_string); 
      $courier_booking = curl_exec($ch); // execute url and save response
      curl_close($ch); // close url connection
      //echo json_encode($courier_booking); die();
      if(strpos($courier_booking, 'success') !== false) {
        $or = substr($courier_booking, strpos($courier_booking, '_id":') + 5);
        $ord = substr(strrev($or), strpos(strrev($or), '}') + 1);
        $order_id = strrev($ord);

        $this->response  = ['response' => 'success' , 'courier_booking' => (string)$order_id,'message' => $this->lang->line('Drop booking created successfully.')];

        //Update courier Id in Laundry
        $order_details = $this->api->get_courier_details_by_ref_id($provider['cust_id'], $booking_id);
        $order_id_update = array(
          'drop_order_id' => (int)trim($order_details['order_id'])
        );
        $this->api->laundry_booking_update($order_id_update, $booking_id);
      }else { $this->response  = ['response' => 'failed' , 'courier_booking' => "0" , 'message' => $this->lang->line('Drop booking created error.') ]; }

     echo json_encode($this->response); die();
    }
  //End Laundry mark is Drop-------------------------------

  //Pending Payments and Invoices--------------------------
    public function laundry_pending_payments_invoices()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->input->post("cust_id"); $cust_id = (int)$cust_id;
      $last_id = $this->input->post("last_id"); $last_id = (int)$last_id;
      $user_type = $this->input->post("user_type"); $user_type = (int)$user_type; //customer/provider
      $filter = $this->input->post("filter");
      if($filter == 1) {
        $start_date = $this->input->post("start_date");
        $start_date = $start_date.' 00:00:00';
        $end_date = $this->input->post("end_date");
        $end_date = $end_date.' 23:59:59';
        $country_id = $this->input->post("country_id");
        $currency_sign = $this->input->post("currency_sign");
        $filter = 1;
      } else {
        $start_date = null;
        $end_date = null;
        $country_id = null;
        $currency_sign = null;
        $filter = 0;
      }

      if($bookings = $this->api->pending_invoice_list($cust_id, 'completed', $user_type, 0, $last_id, 1, $start_date, $end_date, $country_id, $currency_sign)){
        //echo $this->db->last_query();
        //echo json_encode($bookings); die();
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('get_list_success'), 'list' => $bookings];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('get_list_failed'), 'list' => array()]; }
      echo json_encode($this->response);
    }
  //Pending Payments and Invoices--------------------------

  //Start Claim Provider-----------------------------------
    public function provider_laundry_claims()
    {
      $cust_id = $this->input->post("cust_id"); $cust_id = (int)$cust_id;
      $last_id = $this->input->post("last_id"); $last_id = (int)$last_id;
      //echo json_encode($_POST); die();
      $laundry_claim_delay = $this->api->get_laundry_provider_profile($cust_id)['laundry_claim_delay'];
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'open',
        'device_type' => 1,
        'last_id' => $last_id,
        'service_cat_id' => 9,
        'service_id' => 0,
        'claim_id' => 0,
      );
      //echo json_encode($data);
      if($claim_list = $this->api->get_claim_list($data)) {
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('get_list_success'), 'list' => $claim_list];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('get_list_failed'), 'list' => array()]; }
      echo json_encode($this->response);
    }
    public function provider_claim_response()
    {
      //echo json_encode($_POST); die();
      $claim_id = $this->input->post('claim_id', TRUE);
      $claim_solution_desc = $this->input->post('claim_solution_desc', TRUE);
      $claim_status = $this->input->post('claim_status', TRUE);
      $today = date('Y-m-d H:i:s');

      $data = array(
        'claim_solution_desc' => $claim_solution_desc,
        'claim_status' => $claim_status,
        'claim_reponse_datetime' => $today,
        'claim_id' => (int)$claim_id,
      );
      
      if($this->api->update_service_claim_details($data, (int)$claim_id)){ 
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('Claim status updated successfully.')];
      } else { 
        $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('Unable to update status. Try again!')];
      } 
      echo json_encode($this->response);
    }
    public function provider_laundry_claims_closed()
    {
      $cust_id = $this->input->post("cust_id"); $cust_id = (int)$cust_id;
      $last_id = $this->input->post("last_id"); $last_id = (int)$last_id;
      //echo json_encode($_POST); die();
      $laundry_claim_delay = $this->api->get_laundry_provider_profile($cust_id)['laundry_claim_delay'];
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'closed',
        'device_type' => 1,
        'last_id' => $last_id,
        'service_cat_id' => 9,
        'service_id' => 0,
        'claim_id' => 0,
      );
      //echo json_encode($data);
      if($claim_list = $this->api->get_claim_list($data)) {
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('get_list_success'), 'list' => $claim_list];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('get_list_failed'), 'list' => array()]; }
      echo json_encode($this->response);
    }
    public function update_laundry_claim_delay()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      $laundry_claim_delay = $this->input->post('laundry_claim_delay', TRUE);
      $data = array(
        'laundry_claim_delay' => $laundry_claim_delay,
      );
      if($this->api->update_provider($data, (int)$cust_id)){ 
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('Claim delay hours updated successfully.')];
      } else { 
        $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('Unable to update claim delay hours.')];
      } 
      echo json_encode($this->response);
    }
  //End Claim Provider-------------------------------------

  //Start Claim Customer-----------------------------------
    public function mark_claim_process()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->input->post("cust_id", TRUE); $cust_id = (int)$cust_id;
      $service_id = $this->input->post('service_id', TRUE); $service_id = (int)$service_id;
      $claim_type = $this->input->post('claim_type', TRUE);
      $claim_desc = $this->input->post('claim_desc', TRUE);
      $cat_ids = $this->input->post('cat_ids', TRUE);
      $sub_cat_ids = $this->input->post('sub_cat_ids', TRUE);
      $item_counts = $this->input->post('item_counts', TRUE);
      $today = date('Y-m-d H:i:s');
      $booking_details = $this->api->laundry_booking_details($service_id);
           
      if( !empty($_FILES["image_url"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/claim-docs/'; 
        $config['allowed_types']  = 'gif|jpg|png'; 
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('image_url')) {   
          $uploads    = $this->upload->data();  
          $image_url =  $config['upload_path'].$uploads["file_name"];  
        } else {  $image_url = "NULL"; }
      } else {  $image_url = "NULL"; }

      if( !empty($_FILES["doc_url"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/claim-docs/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('doc_url')) {   
          $uploads = $this->upload->data();  
          $doc_url =  $config['upload_path'].$uploads["file_name"];  
        } else {  $doc_url = "NULL"; }
      } else {  $doc_url = "NULL"; }

      $data = array(
        'service_cat_id' => 9,
        'service_id' => trim($service_id), 
        'cust_id' => trim($cust_id), 
        'cust_name' => $booking_details['cust_name'], 
        'provider_id' => $booking_details['provider_id'], 
        'provider_name' => $booking_details['operator_name'], 
        'claim_type' => $claim_type, 
        'cat_ids' => $cat_ids, 
        'sub_cat_ids' => $sub_cat_ids, 
        'item_counts' => $item_counts, 
        'claim_desc' => $claim_desc,
        'image_url' => $image_url, 
        'doc_url' => $doc_url, 
        'cre_datetime' => $today,
        'claim_status' => 'open', 
        'status_update_datetime' => $today, 
        'claim_solution_desc' => '', 
        'reopen_count' => 0,
      );
      //echo json_encode($data); die();

      if($claim_id = $this->api->register_claim($data)) {
        $data = array(
          'claim_id' => (int)$claim_id,
        );
        $this->api->laundry_booking_update($data, $service_id);
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('Claim reported successfully.')];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('Unable to report the claim, Try again!')]; }
      echo json_encode($this->response);
    }
    public function customer_laundry_claims()
    {
      $cust_id = $this->input->post("cust_id", TRUE); $cust_id = (int)$cust_id;
      $last_id = $this->input->post("last_id", TRUE); $last_id = (int)$last_id;
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'customer',
        'claim_status' => 'open',
        'device_type' => 1,
        'last_id' => $last_id,
        'service_cat_id' => 9,
        'service_id' => 0,
        'claim_id' => 0,
      );
      if($claim_list = $this->api->get_claim_list($data)) {
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('get_list_success'), 'list' => $claim_list];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('get_list_failed'), 'list' => array()]; }
      echo json_encode($this->response);
    }
    public function customer_claim_details()
    {
      //echo json_encode($_POST); die();
      $claim_id = $this->input->post('claim_id', TRUE); $claim_id = (int)$claim_id; 
      $service_id = $this->input->post('service_id', TRUE); $service_id = (int)$service_id;
      $booking_details = $this->api->laundry_booking_details($service_id);
      $booking_details_list = $this->api->laundry_booking_details_list($service_id);
      $data = array(
        'claim_id' => $claim_id,
        'service_id' => 0,
        'user_type' => 'NULL',
        'claim_status' => 'NULL',
        'device_type' => 0,
        'last_id' => 0,
        'cust_id' => 0,
        'service_cat_id' => 0,
      );
      $claim_details = $this->api->get_claim_list($data);
      //echo json_encode($list); die();

      if($claim_details) {
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('get_list_success'), 'booking_details' => $booking_details, 'booking_details_list' => $booking_details_list, 'claim_details' => $claim_details];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('get_list_failed'), 'booking_details' => array(), 'booking_details_list' => array(), 'claim_details' => array()]; }
      echo json_encode($this->response);
    }
    public function customer_laundry_claims_closed()
    {
      $cust_id = $this->input->post("cust_id", TRUE); $cust_id = (int)$cust_id;
      $last_id = $this->input->post("last_id", TRUE); $last_id = (int)$last_id;
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'customer',
        'claim_status' => 'closed',
        'device_type' => 1,
        'last_id' => $last_id,
        'service_cat_id' => 9,
        'service_id' => 0,
        'claim_id' => 0,
      );
      if($claim_list = $this->api->get_claim_list($data)) {
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('get_list_success'), 'list' => $claim_list];
      } else { $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('get_list_failed'), 'list' => array()]; }
      echo json_encode($this->response);
    }
    public function customer_reopen_claim()
    {
      //echo json_encode($_POST); die();
      $claim_id = $this->input->post('claim_id', TRUE); $claim_id = (int)$claim_id;
      $claim_desc = $this->input->post('claim_desc', TRUE);

      $data = array(
        'claim_id' => (int)$claim_id,
      );
      $reopen_count = $this->api->get_claim_list($data)['reopen_count'];

      $data = array(
        'claim_desc' => $claim_desc,
        'claim_status' => 'open',
        'reopen_count' => ($reopen_count+1),
      );
      //echo json_encode($data); die();
      if($this->api->update_service_claim_details($data, $claim_id)){ 
        $this->response  = ['response' => 'success' , 'message' => $this->lang->line('Claim re-opened successfully.')];
      } else { 
        $this->response  = ['response' => 'failed' , 'message' => $this->lang->line('Unable to re-open claim. Try again!')];
      } 
      echo json_encode($this->response);
    }
  //End Claim Customer-------------------------------------

  //Start Laundry Service Cleanup--------------------------
    public function laundry_booking_cleanup()
    {
      if($this->api->laundry_order_cleanup2()){
        $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
      echo json_encode($this->response);
    }
  //End Laundr yService Cleanup----------------------------

  //Start Promo Code---------------------------------------
    public function promocode()
    {
      //echo json_encode($_POST); die(); 
      if(isset($_POST["promocode"])){
        $booking_id = $this->input->post('booking_id');
        $cust_id = $this->input->post('cust_id');
        $details =  $this->api->laundry_booking_details($booking_id);
        $promo_code_name = $this->input->post("promocode");
        if($details['promo_code'] == 'NULL'){
          if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
            $used_codes = $this->api->get_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version']);
            if(sizeof($used_codes) < $promocode['no_of_limit']){
              if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$cust_id)){
                    $discount_amount = ($details['booking_price']/100)*$promocode['discount_percent'];
                    $booking_price = $details['booking_price']-$discount_amount;
                    $total_price = $details['pickup_courier_price']+$details['drop_courier_price']+$booking_price;
                    $update = array(
                      'booking_price' => $booking_price,
                      'balance_amount' => $booking_price,
                      'total_price' => $total_price,
                      'promo_code' => $promocode['promo_code'],
                      'promo_code_id' => $promocode['promo_code_id'],
                      'promo_code_version' => $promocode['promo_code_version'],
                      'discount_amount' => $discount_amount,
                      'discount_percent' => $promocode['discount_percent'],
                      'actual_price' => $details['booking_price'],
                      'promo_code_datetime' => date('Y-m-d h:i:s')
                    );
                    $this->api->laundry_booking_update($update , $details['booking_id']);

                    $used_promo_update = array(
                      'promo_code_id' => $promocode['promo_code_id'],
                      'promo_code_version' => $promocode['promo_code_version'],
                      'cust_id' => $details['cust_id'],
                      'discount_percent' => $promocode['discount_percent'],
                      'discount_amount' => $discount_amount,
                      'discount_currency' => $details['currency_sign'],
                      'cre_datetime' => date('Y-m-d h:i:s'),
                      'service_id' => '9',
                      'booking_id' => $details['booking_id'],
                    );
                    $this->api->register_user_used_prmocode($used_promo_update);
                    $details =  $this->api->laundry_booking_details($booking_id);
                    $this->response = ['response' => 'success', 'message' => $this->lang->line('Promocode Applied successfully') , 'booking_details' => $details];
                  //code end here
                }else{
                  $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used this promocode')];
                }
              }else{
                $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promocode has been Expired')];
              }
            }else{
              $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promo code is used maximum number of limit')];
            }
          }else{
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')];
          }
        }else{
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used promocode for this order')];
        }
        echo json_encode($this->response);
      }
    }
    public function promocode_check()
    {
      $promo_code_name =  $this->input->post('promocode');
      $booking_id =  $this->input->post('booking_id');
      $cust_id =  $this->input->post('cust_id');
      $details =  $this->api->laundry_booking_details($booking_id);
      if($promo_code = $this->api->get_promocode_by_name($promo_code_name)){
        $service_id = explode(',',$promo_code['service_id']);
        $used_codes = $this->api->get_used_promo_code($promo_code['promo_code_id'],$promo_code['promo_code_version']);
        if(sizeof($used_codes) < $promo_code['no_of_limit']){
          if($details['promo_code'] == 'NULL'){
            if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
              if(in_array("9",$service_id)){
                if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                  if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$cust_id)){
                    $discount_amount = round(($details['booking_price']/100)*$promocode['discount_percent'] , 2); 
                    if($details['currency_sign'] == 'XAF'){
                      $discount_amount = round($discount_amount , 0);
                    }
                    $booking_price =  round($details['booking_price']-$discount_amount ,2);
                    if($details['currency_sign'] == 'XAF'){
                      $booking_price = round($booking_price , 0);
                    }

                    $this->response = ['response' => 'success', 'message' => $this->lang->line('Promocode Available') , 'Discount' => $discount_amount." ".$details['currency_sign'] , 'booking_price' => $booking_price." ".$details['currency_sign'] , 'booking_details' => $details];
                  }else{
                    $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used this promocode')];
                  }
                }else{
                  $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promocode has been Expired')];
                }
              }else{
                $this->response = ['response' => 'failed', 'message' => $this->lang->line('This promocode is not valid for this category')];
              }
            }else{
              $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')];
            }
          }else{
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used promocode for this order')];
          }
        }else{
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promo code is used maximum number of limit')];
        }  
      }else{
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')];
      }
      echo json_encode($this->response);
    }
  //End Promo Code-----------------------------------------
  //Start Special Laundry charges---------------------------
    public function laundry_special_charges_list()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE);
        $last_id = $this->input->post('last_id', TRUE);
        $cust_id = (int) $cust_id;
        if( $list = $this->api->get_laundry_special_charges_list($cust_id, $last_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $list];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function add_laundry_special_charges()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $mon = $this->input->post('mon');
        $tue = $this->input->post('tue');
        $wed = $this->input->post('wed');
        $thu = $this->input->post('thu');
        $fri = $this->input->post('fri');
        $sat = $this->input->post('sat');
        $sun = $this->input->post('sun');
        $start_date = $this->input->post('start_date'); $start_date = date("m/d/Y", strtotime($start_date));
        $end_date = $this->input->post('end_date'); $end_date = date("m/d/Y", strtotime($end_date));
        $cust_id = $this->input->post('cust_id');  
        if($mon == ""){$mon = "0";}    
        if($tue == ""){$tue = "0";}
        if($wed == ""){$wed = "0";}
        if($thu == ""){$thu = "0";}
        if($fri == ""){$fri = "0";}
        if($sat == ""){$sat = "0";}
        if($sun == ""){$sun = "0";}
        $data =array(
          'cust_id' => trim($cust_id), 
          'mon' => trim($mon), 
          'tue' => trim($tue), 
          'wed' => trim($wed), 
          'thu' => trim($thu), 
          'fri' => trim($fri), 
          'sat' => trim($sat), 
          'sun' => trim($sun), 
          'start_date' => trim($start_date), 
          'end_date' => trim($end_date), 
          'end_date' => trim($end_date), 
          'cre_date' => date('Y-m-d'), 
        );
        if($this->api->add_laundry_special_charges($data)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('added_successfully')];
        } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('unable_to_add')]; } 
        echo json_encode($this->response);
      }
    }
    public function update_laundry_special_charges()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $charge_id = $this->input->post('charge_id', TRUE); $charge_id = (int)$charge_id;
        $mon = $this->input->post('mon');
        $tue = $this->input->post('tue');
        $wed = $this->input->post('wed');
        $thu = $this->input->post('thu');
        $fri = $this->input->post('fri');
        $sat = $this->input->post('sat');
        $sun = $this->input->post('sun');
        $start_date = $this->input->post('start_date'); $start_date = date("m/d/Y", strtotime($start_date));
        $end_date = $this->input->post('end_date');  $end_date = date("m/d/Y", strtotime($end_date));
        if($mon == ""){$mon = "0";}    
        if($tue == ""){$tue = "0";}
        if($wed == ""){$wed = "0";}
        if($thu == ""){$thu = "0";}
        if($fri == ""){$fri = "0";}
        if($sat == ""){$sat = "0";}
        if($sun == ""){$sun = "0";}
        $data =array(
          'mon' => trim($mon), 
          'tue' => trim($tue), 
          'wed' => trim($wed), 
          'thu' => trim($thu), 
          'fri' => trim($fri), 
          'sat' => trim($sat), 
          'sun' => trim($sun), 
          'start_date' => trim($start_date), 
          'end_date' => trim($end_date), 
          'end_date' => trim($end_date), 
          'update_date' => date('Y-m-d'), 
        );
        if($this->api->update_laundry_special_charges($data, $charge_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success')];
        } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('update_failed')]; } 
        echo json_encode($this->response);
      }
    }
    public function delete_laundry_special_charges()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $charge_id = $this->input->post('charge_id', TRUE); $charge_id = (int)$charge_id;
        if($this->api->delete_laundry_special_charges($charge_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('delete_success')];
        } else { $this->response = ['response' => 'failed', 'message'=> $this->lang->line('delete_failed')]; } 
        echo json_encode($this->response);
      }
    }
  //End Special Laundry charges-----------------------------

}
/* End of file Api_laundry.php */
/* Location: ./application/controllers/Api_laundry.php */