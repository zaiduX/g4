<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Under_construction extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->config->set_item('lang', 'en');
		$this->lang->load('englishFront_lang','english');
		$this->load->model('country_currency_model', 'api');

	}
	
	public function index()
	{
		$countries = $this->api->get_countries();
		$this->load->view('under_construction_view', compact('countries'));
	}

	public function keep_me_inform()
	{
		$name = $this->input->post("senderName");
		$email = $this->input->post("senderEmail");
		$phone = $this->input->post("SenderPhone");
		$company = $this->input->post("senderCompany");
		$comment = $this->input->post("senderComment");
		$type = $this->input->post("optradio");
		$country = $this->input->post("sendercountry");
		$city = $this->input->post("senderCity");

		$insert_data = array(
			"u_name" => trim($name),
			"u_email" => trim($email),
			"u_phone" => trim($phone),
			"u_company" => empty($company) ? "NULL" : trim($company),
			"u_comment" => empty($comment) ? "NULL" : nl2br(trim($comment)),
			"u_datetime" => date("Y-m-d H:i:s"),
			"type" => trim($type),
			"country" => trim($country),
			"city" => trim($city),
		);
		$message = $messageBody = '';
		$subject = 'Keep me informed!';
		$message .="<p class='lead'> Name : ".$name."</p>";
		$message .="<p class='lead'> Email Address : ".$email."</p>";
		$message .="<p class='lead'> Phone Number : ".$phone."</p>";
		$message .="<p class='lead'> Company Name : ".$company."</p>";
		$message .="<p class='lead'> Comments : ".$comment."</p>";
		$message .="<p class='lead'> Type : ".$type."</p>";
		$message .="<p class='lead'> Country : ".$country."</p>";
		$message .="<p class='lead'> City : ".$city."</p>";
	    $message .="</td></tr><tr><td><br />";
		$messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
	    $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
	    $messageBody .="</head><body bgcolor='#FFFFFF'>";
	    $messageBody .="<table class='body-wrap'><tr><td></td><td class='container'><div class='content'>";
	    $messageBody .="<table><tr><td>";
	    $messageBody .= $message;
	    $messageBody .="</td></tr>";	    
	    $messageBody .="</table></div></td><td></td></tr></table></body></html>";

	    $email_from = $this->config->item('from_email');
	    $email_subject = $subject;
	    $user_email = $this->config->item('contact_gonagoo');
	    $headers = "MIME-Version: 1.0" . "\r\n";
	    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	    $headers .= 'From: '.$email_from.'' . "\r\n";
	    mail($user_email, $email_subject, $messageBody, $headers); 

		if( $this->db->insert('keep_me_inform', $insert_data)){
			$this->session->set_flashdata("success", "We will soon inform you, when we are ON!");	
		}	else{	$this->session->set_flashdata('error',"Some thing goes wrong, please try again...." );	}
		
		redirect('under-construction');
	}

}

/* End of file Under_construction.php */
/* Location: ./application/config/Under_construction.php */