<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_custom extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('advance_payment_model', 'payment');
      $this->load->model('standard_dimension_model', 'standard_dimension');   
      $this->load->model('Web_user_model', 'user');   
      $this->load->model('country_custom_model', 'custom');
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $percent = $this->custom->get_custom_percentage();
    $this->load->view('admin/custom_percentage_list_view', compact('percent'));      
    $this->load->view('admin/footer_view');   
  }

  public function add()
  {
    $categories = $this->standard_dimension->get_categories();
    $countries = $this->payment->get_countries();
    $this->load->view('admin/custom_percentage_add_view',compact('countries','categories'));
    $this->load->view('admin/footer_view');
  }
  
  public function register()
  { 
    $country_id = $this->input->post('country_id', TRUE);
    
    $EICCPNG = $this->input->post('EICCPNG', TRUE);
    $EICCPOG = $this->input->post('EICCPOG', TRUE);  
    $SICCPNG = $this->input->post('SICCPNG', TRUE);
    $SICCPOG = $this->input->post('SICCPOG', TRUE);
    $AICCPNG = $this->input->post('AICCPNG', TRUE);
    $AICCPOG = $this->input->post('AICCPOG', TRUE);

    $EIGCPNG = $this->input->post('EIGCPNG', TRUE);
    $EIGCPOG = $this->input->post('EIGCPOG', TRUE);
    $SIGCPNG = $this->input->post('SIGCPNG', TRUE);
    $SIGCPOG = $this->input->post('SIGCPOG', TRUE);
    $AIGCPNG = $this->input->post('AIGCPNG', TRUE);
    $AIGCPOG = $this->input->post('AIGCPOG', TRUE);
   
    $category_ids = $this->input->post('category', TRUE);
    $insert_flag = false;
           
    foreach ($category_ids as $id) {

      $insert_data = array(
        "category_id" => trim($id), 
        "country_id" => trim($country_id),
        "EICCPNG" => number_format($EICCPNG,2,'.',''),
        "EICCPOG" => number_format($EICCPOG,2,'.',''),
        "SICCPNG" => number_format($SICCPNG,2,'.',''),
        "SICCPOG" => number_format($SICCPOG,2,'.',''),
        "AICCPNG" => number_format($AICCPNG,2,'.',''),
        "AICCPOG" => number_format($AICCPOG,2,'.',''),

        "EIGCPNG" => number_format($EIGCPNG,2,'.',''),
        "EIGCPOG" => number_format($EIGCPOG,2,'.',''),
        "SIGCPNG" => number_format($SIGCPNG,2,'.',''),
        "SIGCPOG" => number_format($SIGCPOG,2,'.',''),
        "AIGCPNG" => number_format($AIGCPNG,2,'.',''),
        "AIGCPOG" => number_format($AIGCPOG,2,'.',''),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
        "status" =>  1, 
      );
      //echo json_encode($insert_data); die();
      if(! $this->custom->check_custom_percentage($insert_data) ) {
        if($this->custom->register_custom_percentage($insert_data)){ $insert_flag = true;  } 
      } 
    }

    if($insert_flag){ $this->session->set_flashdata('success','Custom Commission Setup Successfully Added!'); }
    else{ $this->session->set_flashdata('error','Unable to add or Setup already exists for selected country and category!');  }

    redirect('admin/custom-percentage/add');
  }

  public function edit()
  { 
    if( $id = $this->uri->segment(4) ) {      
      if( $percent = $this->custom->get_custom_percentage_detail($id)){
        $countries = $this->payment->get_countries();
        $this->load->view('admin/custom_percentage_edit_veiw', compact('percent','countries'));
        $this->load->view('admin/footer_view');
      } else { redirect('admin/custom-percentage'); }
    } else { redirect('admin/custom-percentage'); }
  }

  public function update()
  { 
    $custom_id = $this->input->post('custom_id', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $category_id = $this->input->post('category_id', TRUE);

    $EICCPNG = $this->input->post('EICCPNG', TRUE);
    $EICCPOG = $this->input->post('EICCPOG', TRUE);  
    $SICCPNG = $this->input->post('SICCPNG', TRUE);
    $SICCPOG = $this->input->post('SICCPOG', TRUE);
    $AICCPNG = $this->input->post('AICCPNG', TRUE);
    $AICCPOG = $this->input->post('AICCPOG', TRUE);

    $EIGCPNG = $this->input->post('EIGCPNG', TRUE);
    $EIGCPOG = $this->input->post('EIGCPOG', TRUE);
    $SIGCPNG = $this->input->post('SIGCPNG', TRUE);
    $SIGCPOG = $this->input->post('SIGCPOG', TRUE);
    $AIGCPNG = $this->input->post('AIGCPNG', TRUE);
    $AIGCPOG = $this->input->post('AIGCPOG', TRUE);

    $update_data = array(          
      "EICCPNG" => number_format($EICCPNG,2,'.',''),
      "EICCPOG" => number_format($EICCPOG,2,'.',''),          
      "SICCPNG" => number_format($SICCPNG,2,'.',''),
      "SICCPOG" => number_format($SICCPOG,2,'.',''),
      "AICCPNG" => number_format($AICCPNG,2,'.',''),
      "AICCPOG" => number_format($AICCPOG,2,'.',''),

      "EIGCPNG" => number_format($EIGCPNG,2,'.',''),
      "EIGCPOG" => number_format($EIGCPOG,2,'.',''),
      "SIGCPNG" => number_format($SIGCPNG,2,'.',''),
      "SIGCPOG" => number_format($SIGCPOG,2,'.',''),
      "AIGCPNG" => number_format($AIGCPNG,2,'.',''),
      "AIGCPOG" => number_format($AIGCPOG,2,'.',''),
    );
    
    $percent = $this->custom->get_custom_percentage_detail($custom_id);
    if($percent['country_id'] == $country_id && $percent['custom_id'] == $custom_id && $percent['category_id'] == $category_id ) {
      if($this->custom->update_custom_percentage($update_data, $custom_id)){
        $this->session->set_flashdata('success','Custom Commission Setup successfully update!');
      } else{ $this->session->set_flashdata('error','Unable to update setup! Try again...');  }
    } else{ $this->session->set_flashdata('error','Failed! Some unwanted scripting injection found! Please refresh the page and try again...');  }

    redirect('admin/custom-percentage/edit/'.$custom_id);
  }

  public function delete()
  {
    $id = $this->input->post('id', TRUE);
    if($this->custom->delete_custom_percentage($id)){ echo 'success'; } else { echo "failed";  } 
  }

}

/* End of file country_custom.php */
/* Location: ./application/controllers/country_custom.php */