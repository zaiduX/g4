<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Payment_bus extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->cust_id = $this->session->userdata("cust_id"); $this->cust_id = (int) $this->cust_id;
    $this->load->model('api_model_bus', 'api');   
    $this->load->model('api_model_sms', 'api_sms');   
    // $this->load->model('Users_model', 'users');
    if( $this->input->method() != 'post' ) exit('No direct script access allowed');
    if( $this->input->is_ajax_request() ) exit('No direct script access allowed');

    $lang = $this->session->userdata('language');
    $this->config->set_item('language', strtolower($lang));

    if(trim($lang) == 'french'){
      $this->lang->load('frenchApi_lang','french');
      $this->lang->load('frenchFront_lang','french');
    } 
    else if(trim($lang) == 'spanish'){  
      $this->lang->load('spanishApi_lang','spanish');
      $this->lang->load('spanishFront_lang','spanish');
    }
    else { 
      $this->lang->load('englishApi_lang','english');
      $this->lang->load('englishFront_lang','english');
    }
  }
  
  public function index() { exit('No direct script access allowed.'); }

  public function payment_by_mtn()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->cust_id;
    $user_details = $this->api->get_user_details($cust_id);
    $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
    $payment_method = 'mtn';
    $phone_no = $this->input->post('phone_no', TRUE);
    if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
    $today = date('Y-m-d h:i:s');
    $ticket_details = $this->api->get_trip_booking_details($ticket_id);
    $ticket_price = $this->input->post('ticket_price', TRUE); $ticket_price = (int) $ticket_price;
    $currency_sign = $ticket_details['currency_sign'];

    $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
    $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
    $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
    $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
    //Send Push Notifications
    //Get PN API Keys
    $api_key = $this->config->item('delivererAppGoogleKey');
    $device_details_customer = $this->api->get_user_device_details($cust_id);
    if(!is_null($device_details_customer)) {
      $arr_customer_fcm_ids = array();
      $arr_customer_apn_ids = array();
      foreach ($device_details_customer as $value) {
        if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
        else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
      }
      $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
      $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

      //Push Notification APN
      $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
      if(is_array($arr_customer_apn_ids)) { 
        $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
        $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
      }
    }
    
    //MTN Payment Gateway
    /*$url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$ticket_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    $mtn_pay_res = json_decode($contents, TRUE);*/
    //var_dump($mtn_pay_res); die();

    $transaction_id = 123;
    //echo json_encode($ticket_details); die();

    $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
    $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
    //echo json_encode($country_id); die();
    $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
    //echo json_encode($gonagoo_commission_percentage); die();
    $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

    $total_amount_paid = $ticket_price;
    $transfer_account_number = $phone_no;
    $bank_name = "NULL";
    $account_holder_name = "NULL";
    $iban = "NULL";
    $email_address = "NULL";
    $mobile_number = $phone_no;    
    
    $payment_data = array();
    //if($mtn_pay_res['StatusCode'] === "01"){
    if(1) {
      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($ticket_details['ticket_price']),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => "web",
        "complete_paid" => 1,
      );
      if($this->api->update_booking_details($update_data, $ticket_id)){
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        //Update to workroom
        $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
        $workroom_update = array(
          'order_id' => $ticket_details['ticket_id'],
          'cust_id' => $ticket_details['cust_id'],
          'deliverer_id' => $ticket_details['operator_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $ticket_details['operator_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $ticket_details['cust_name'],
          'deliverer_name' => $ticket_details['operator_name'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        /*************************************** Payment Section ****************************************/
          //Add Ownward Trip Payment to customer account + History
          if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Deduct Ownward Trip Payment from customer account + History
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add Ownward Trip Payment to trip operator account + History
          if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => trim($ticket_details['operator_id']),
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          /* Ownward ticket invoice---------------------------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

            //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            $workroom_invoice_url = "ticket-invoices/".$pdf_name;
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  $workroom_invoice_url,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* -------------------------------------------------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ----------------------Generate Ticket pdf ------------------------- */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]==" " || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ----------------------Generate Ticket pdf ------------------------- */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

          //Deduct Ownward Trip Commission From Operator Account
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($ownward_trip_commission),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add Ownward Trip Commission To Gonagoo Account
          if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "type" => 1,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($ownward_trip_commission),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($ticket_details['currency_sign']),
            "country_id" => trim($ownward_trip_operator_detail['country_id']),
            "cat_id" => trim($ticket_details['cat_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          
          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */

          //Return Trip Payment Calculation--------------------------------------
            if($ticket_details['return_trip_id'] > 0) {
              $return_ticket_details = $this->api->get_trip_booking_details($ticket_details['return_trip_id']);

              $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
              $message = $this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id'];
              $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

              $return_update_data = array(
                "payment_method" => trim($payment_method),
                "paid_amount" => trim($return_ticket_details['ticket_price']),
                "balance_amount" => 0,
                "transaction_id" => trim($transaction_id),
                "payment_datetime" => $today,
                "payment_mode" => "online",
                "payment_by" => "web",
                "complete_paid" => 1,
              );
              $this->api->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);
              //Update Gonagoo commission for ticket in seat details table
              $seat_update_data = array(
                "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
              );
              $this->api->update_booking_seat_details($seat_update_data, $return_ticket_details['ticket_id']);
              //Update to Workroom
              $workroom_update = array(
                'order_id' => $return_ticket_details['ticket_id'],
                'cust_id' => $return_ticket_details['cust_id'],
                'deliverer_id' => $return_ticket_details['operator_id'],
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $return_ticket_details['operator_id'],
                'type' => 'payment',
                'file_type' => 'text',
                'cust_name' => $return_ticket_details['cust_name'],
                'deliverer_name' => $return_ticket_details['operator_name'],
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 281
              );
              $this->api->add_bus_chat_to_workroom($workroom_update);

              //Add Return Trip Payment to customer account + History
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$return_ticket_details['ticket_id'],
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($return_ticket_details['ticket_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              //Deduct return Trip Payment from customer account + History
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_ticket_details['ticket_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$return_ticket_details['ticket_id'],
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($return_ticket_details['ticket_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($return_ticket_details['currency_sign']),
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              //Add Return Trip Payment to trip operator account + History
              if($this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
              } else {
                $insert_data_customer_master = array(
                  "user_id" => trim($return_ticket_details['operator_id']),
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)trim($return_ticket_details['ticket_id']),
                "user_id" => (int)trim($return_ticket_details['operator_id']),
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($return_ticket_details['ticket_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              /* Return ticket invoice---------------------------------------------- */
                $return_trip_operator_detail = $this->api->get_user_details($return_ticket_details['operator_id']);

                $country_code = $this->api->get_country_code_by_id($return_trip_operator_detail['country_id']);
                $message = $this->lang->line('Payment recieved against ticket ID - ').$return_ticket_details['ticket_id'];
                $this->api->sendSMS((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);
                
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($user_details['email1']));
                  $username = $parts[0];
                  $customer_name = $username;
                } else {
                  $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $user_country = $this->api->get_country_details(trim($user_details['country_id']));
                $user_state = $this->api->get_state_details(trim($user_details['state_id']));
                $user_city = $this->api->get_city_details(trim($user_details['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

                //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
                $rate = round(trim($return_ticket_details['ticket_price']),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                $workroom_invoice_url = "ticket-invoices/".$pdf_name;
                //Payment Invoice Update to workroom
                $workroom_update = array(
                  'order_id' => $return_ticket_details['ticket_id'],
                  'cust_id' => $return_ticket_details['cust_id'],
                  'deliverer_id' => $return_ticket_details['operator_id'],
                  'text_msg' => $this->lang->line('Invoice'),
                  'attachment_url' =>  $workroom_invoice_url,
                  'cre_datetime' => $today,
                  'sender_id' => $return_ticket_details['operator_id'],
                  'type' => 'raise_invoice',
                  'file_type' => 'pdf',
                  'cust_name' => $return_ticket_details['cust_name'],
                  'deliverer_name' => $return_ticket_details['operator_name'],
                  'thumbnail' => 'NULL',
                  'ratings' => 'NULL',
                  'cat_id' => 281
                );
                $this->api->add_bus_chat_to_workroom($workroom_update);
              /* -------------------------------------------------------------------- */
              
              /* -----------------Email Invoice to customer-------------------------- */
                $emailBody = $this->lang->line('Please download your ticket payment invoice.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
                $emailAddress = $user_details['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to customer-------------------- */
              
              /* -------------------------Return Ticket pdf ------------------------- */
                $return_operator_details = $this->api->get_bus_operator_profile($return_ticket_details['operator_id']);
                if($return_operator_details["avatar_url"]==" " || $return_operator_details["avatar_url"] == "NULL"){
                  $operator_avtr = base_url("resources/no-image.jpg");
                }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
                require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                  QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
                   $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
                   $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
                  if(rename($img_src, $img_dest));
                $html ='
                <page format="100x100" orientation="L" style="font: arial;">';
                $html .= '
                <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                    <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                    <div style="margin-top:27px">
                      <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
                      <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
                    </div>
                  </div>
                  <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                      <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                      <br />
                      <h6>'.$this->lang->line("slider_heading1").'</h6>
                  </div>                
                </div>
                <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                  <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                    '.$this->lang->line("Ownward_Trip").'<br/>
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
                  <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                  <h6 style="margin-top:-10px;">
                  <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
                  <h6 style="margin-top:-8px;"> 
                  <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                  $ticket_seat_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);

                  $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                  foreach ($ticket_seat_details as $seat)
                  {
                    $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                    if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                      
                     $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                  }
                $html .='</tbody></table></div></page>';            
                $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
                require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
                $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
                $html2pdf->pdf->SetDisplayMode('default');
                $html2pdf->writeHTML($html);
                $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
                $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
                rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
                //whatsapp api send ticket
                $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
                $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
                //echo json_encode($result); die();
              /* -------------------------Return Ticket pdf ------------------------- */
              
              /* ---------------------Email Ticket to customer----------------------- */
                $emailBody = $this->lang->line('Please download your e-ticket.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                $emailAddress = $user_details['email1'];
                $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                $fileName = $my_ticket_pdf;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* ---------------------Email Ticket to customer----------------------- */

              //Deduct Return Trip Commission From Operator Account
              $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

              $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)trim($return_ticket_details['ticket_id']),
                "user_id" => (int)trim($return_ticket_details['operator_id']),
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'ticket_commission',
                "amount" => trim($return_trip_commission),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              //Add Return Trip Commission To Gonagoo Account
              if($this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)trim($return_ticket_details['ticket_id']),
                "user_id" => (int)trim($return_ticket_details['operator_id']),
                "type" => 1,
                "transaction_type" => 'ticket_commission',
                "amount" => trim($return_trip_commission),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "country_id" => trim($return_trip_operator_detail['country_id']),
                "cat_id" => trim($return_ticket_details['cat_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

              /* Return ticket commission invoice------------------------------------ */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
                $rate = round(trim($return_trip_commission),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Commission Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "commission_invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
              /* -------------------------------------------------------------------- */
              /* -----------------------Email Invoice to operator-------------------- */
                $emailBody = $this->lang->line('Commission Invoice');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Commission Invoice');
                $emailAddress = $return_trip_operator_detail['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to operator-------------------- */
            }
          //Return Trip Payment Calculation--------------------------------------
        /*************************************** Payment Section ****************************************/
        $this->session->set_flashdata('success', 'Payment successfully done!');
      } else { $this->session->set_flashdata('error', 'Payment failed! Try again...'); }
    } else {  $this->session->set_flashdata('error', 'Payment failed! Try again...'); }
    $location = base_url('user-panel-bus/buyer-upcoming-trips');
    echo '<script>top.window.location = "'.$location.'"</script>';
  }
  public function payment_by_mtn_single()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->cust_id;
    $user_details = $this->api->get_user_details($cust_id);
    $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
    $payment_method = 'mtn';
    $phone_no = $this->input->post('phone_no', TRUE);
    if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
    $today = date('Y-m-d h:i:s');
    $ticket_details = $this->api->get_trip_booking_details($ticket_id);
    $ticket_price = $this->input->post('ticket_price', TRUE); $ticket_price = (int) $ticket_price;
    $currency_sign = $ticket_details['currency_sign'];

    $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
    $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
    $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
    $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
    //Send Push Notifications
    //Get PN API Keys
    $api_key = $this->config->item('delivererAppGoogleKey');
    $device_details_customer = $this->api->get_user_device_details($cust_id);
    if(!is_null($device_details_customer)) {
      $arr_customer_fcm_ids = array();
      $arr_customer_apn_ids = array();
      foreach ($device_details_customer as $value) {
        if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
        else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
      }
      $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
      $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

      //Push Notification APN
      $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
      if(is_array($arr_customer_apn_ids)) { 
        $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
        $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
      }
    }
    
    //MTN Payment Gateway
    /*$url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$ticket_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    $mtn_pay_res = json_decode($contents, TRUE);*/
    //var_dump($mtn_pay_res); die();

    $transaction_id = 123;
    //echo json_encode($ticket_details); die();

    $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
    $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
    //echo json_encode($country_id); die();
    $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
    //echo json_encode($gonagoo_commission_percentage); die();
    $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

    $total_amount_paid = $ticket_price;
    $transfer_account_number = $phone_no;
    $bank_name = "NULL";
    $account_holder_name = "NULL";
    $iban = "NULL";
    $email_address = "NULL";
    $mobile_number = $phone_no;    
    
    $payment_data = array();
    //if($mtn_pay_res['StatusCode'] === "01"){
    if(1) {
      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($ticket_details['ticket_price']),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => "web",
        "complete_paid" => 1,
      );
      if($this->api->update_booking_details($update_data, $ticket_id)){
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        //Update to workroom
        $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
        $workroom_update = array(
          'order_id' => $ticket_details['ticket_id'],
          'cust_id' => $ticket_details['cust_id'],
          'deliverer_id' => $ticket_details['operator_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $ticket_details['operator_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $ticket_details['cust_name'],
          'deliverer_name' => $ticket_details['operator_name'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        /*************************************** Payment Section ****************************************/
          //Add Ownward Trip Payment to customer account + History
          if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Deduct Ownward Trip Payment from customer account + History
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add Ownward Trip Payment to trip operator account + History
          if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => trim($ticket_details['operator_id']),
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          /* Ownward ticket invoice---------------------------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

            //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            $workroom_invoice_url = "ticket-invoices/".$pdf_name;
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => 'NULL',
              'attachment_url' =>  $workroom_invoice_url,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* -------------------------------------------------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ----------------------Generate Ticket pdf ------------------------- */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]==" " || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ----------------------Generate Ticket pdf ------------------------- */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

          //Deduct Ownward Trip Commission From Operator Account
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($ownward_trip_commission),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add Ownward Trip Commission To Gonagoo Account
          if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "type" => 1,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($ownward_trip_commission),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($ticket_details['currency_sign']),
            "country_id" => trim($ownward_trip_operator_detail['country_id']),
            "cat_id" => trim($ticket_details['cat_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          
          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
          
        /*************************************** Payment Section ****************************************/
        $this->session->set_flashdata('success', 'Payment successfully done!');
      } else { $this->session->set_flashdata('error', 'Payment failed! Try again...'); }
    } else {  $this->session->set_flashdata('error', 'Payment failed! Try again...'); }
    $location = base_url('user-panel-bus/buyer-upcoming-trips');
    echo '<script>top.window.location = "'.$location.'"</script>';
  }
  public function payment_by_mtn_from_mobile()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
    $phone_no = $this->input->post('phone_no', TRUE);
    $device_type = $this->input->post('device_type', TRUE); //android/ios
    $ticket_price = $this->input->post('ticket_price', TRUE); $ticket_price = (int) $ticket_price;
    $payment_method = 'mtn';
    if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
    $today = date('Y-m-d h:i:s');
    $user_details = $this->api->get_user_details($cust_id);
    $ticket_details = $this->api->get_trip_booking_details($ticket_id);
    $currency_sign = $ticket_details['currency_sign'];
    //Send SMS
    $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
    $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
    $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);

    //Send Push Notifications
    $api_key = $this->config->item('delivererAppGoogleKey');
    $device_details_customer = $this->api->get_user_device_details($cust_id);
    if(!is_null($device_details_customer)) {
      $arr_customer_fcm_ids = array();
      $arr_customer_apn_ids = array();
      foreach ($device_details_customer as $value) {
        if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
        else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
      }
      $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
      $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
      //Push Notification APN
      $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
      if(is_array($arr_customer_apn_ids)) { 
        $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
        $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
      }
    }
    
    //MTN Payment Gateway
    /*$url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$ticket_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    $mtn_pay_res = json_decode($contents, TRUE);*/
    //var_dump($mtn_pay_res); die();

    $transaction_id = 123;
    //echo json_encode($ticket_details); die();

    $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
    $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
    //echo json_encode($country_id); die();
    $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
    //echo json_encode($gonagoo_commission_percentage); die();
    $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

    $total_amount_paid = $ticket_price;
    $transfer_account_number = $phone_no;
    $bank_name = "NULL";
    $account_holder_name = "NULL";
    $iban = "NULL";
    $email_address = "NULL";
    $mobile_number = $phone_no;    
    
    $payment_data = array();
    //if($mtn_pay_res['StatusCode'] === "01"){
    if(1) {
      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($ticket_details['ticket_price']),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => $device_type,
        "complete_paid" => 1,
      );
      if($this->api->update_booking_details($update_data, $ticket_id)){
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        //Update to workroom
        $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
        $workroom_update = array(
          'order_id' => $ticket_details['ticket_id'],
          'cust_id' => $ticket_details['cust_id'],
          'deliverer_id' => $ticket_details['operator_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $ticket_details['cust_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $ticket_details['cust_name'],
          'deliverer_name' => $ticket_details['operator_name'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        /*************************************** Payment Section ****************************************/
          //Add Ownward Trip Payment to customer account + History----------------
            if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //----------------------------------------------------------------------
          //Deduct Ownward Trip Payment from customer account + History-----------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //----------------------------------------------------------------------
          //Add Ownward Trip Payment to trip operator account + History-----------
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //----------------------------------------------------------------------
          /* Ownward ticket invoice----------------------------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            $workroom_invoice_url = "ticket-invoices/".$pdf_name;
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  $workroom_invoice_url,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* --------------------------------------------------------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]==" " || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result = $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */

          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = trim($user_details['email1']);
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //Deduct Ownward Trip Commission From Operator Account------------------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //----------------------------------------------------------------------
          //Add Ownward Trip Commission To Gonagoo Account------------------------
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //----------------------------------------------------------------------
          /* Ownward ticket commission invoice------------------------------------ */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* --------------------------------------------------------------------- */
          
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */

          //Return Trip Payment Calculation--------------------------------------
            if($ticket_details['return_trip_id'] > 0) {
              $return_ticket_details = $this->api->get_trip_booking_details($ticket_details['return_trip_id']);

              $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
              $message = $this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id'];
              $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);

              $return_update_data = array(
                "payment_method" => trim($payment_method),
                "paid_amount" => trim($return_ticket_details['ticket_price']),
                "balance_amount" => 0,
                "transaction_id" => trim($transaction_id),
                "payment_datetime" => $today,
                "payment_mode" => "online",
                "payment_by" => $device_type,
                "complete_paid" => 1,
              );
              $this->api->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);
              //Update Gonagoo commission for ticket in seat details table
              $seat_update_data = array(
                "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
              );
              $this->api->update_booking_seat_details($seat_update_data, $return_ticket_details['ticket_id']);
              //Update to Workroom
              $workroom_update = array(
                'order_id' => $return_ticket_details['ticket_id'],
                'cust_id' => $return_ticket_details['cust_id'],
                'deliverer_id' => $return_ticket_details['operator_id'],
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $return_ticket_details['cust_id'],
                'type' => 'payment',
                'file_type' => 'text',
                'cust_name' => $return_ticket_details['cust_name'],
                'deliverer_name' => $return_ticket_details['operator_name'],
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 281
              );
              $this->api->add_bus_chat_to_workroom($workroom_update);

              //Add Return Trip Payment to customer account + History-----------------
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$return_ticket_details['ticket_id'],
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);

                //Deduct return Trip Payment from customer account + History
                $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$return_ticket_details['ticket_id'],
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'payment',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------
              //Add Return Trip Payment to trip operator account + History------------
                if($this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_customer_master = array(
                    "user_id" => trim($return_ticket_details['operator_id']),
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                }
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------
              $return_trip_operator_detail = $this->api->get_user_details($return_ticket_details['operator_id']);
              
              /* Return ticket invoice------------------------------------------------ */
                $country_code = $this->api->get_country_code_by_id($return_trip_operator_detail['country_id']);
                $message = $this->lang->line('Payment recieved against ticket ID - ').$return_ticket_details['ticket_id'];
                $this->api->sendSMS((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);
                
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($user_details['email1']));
                  $username = $parts[0];
                  $customer_name = $username;
                } else {
                  $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $user_country = $this->api->get_country_details(trim($user_details['country_id']));
                $user_state = $this->api->get_state_details(trim($user_details['state_id']));
                $user_city = $this->api->get_city_details(trim($user_details['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

                //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
                $rate = round(trim($return_ticket_details['ticket_price']),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                $workroom_invoice_url = "ticket-invoices/".$pdf_name;
                //Payment Invoice Update to workroom
                $workroom_update = array(
                  'order_id' => $return_ticket_details['ticket_id'],
                  'cust_id' => $return_ticket_details['cust_id'],
                  'deliverer_id' => $return_ticket_details['operator_id'],
                  'text_msg' => $this->lang->line('Invoice'),
                  'attachment_url' =>  $workroom_invoice_url,
                  'cre_datetime' => $today,
                  'sender_id' => $return_ticket_details['operator_id'],
                  'type' => 'raise_invoice',
                  'file_type' => 'pdf',
                  'cust_name' => $return_ticket_details['cust_name'],
                  'deliverer_name' => $return_ticket_details['operator_name'],
                  'thumbnail' => 'NULL',
                  'ratings' => 'NULL',
                  'cat_id' => 281
                );
                $this->api->add_bus_chat_to_workroom($workroom_update);
              /* --------------------------------------------------------------------- */
              
              /* -------------------------Return Ticket pdf ------------------------- */
                $return_operator_details = $this->api->get_bus_operator_profile($return_ticket_details['operator_id']);
                if($return_operator_details["avatar_url"]==" " || $return_operator_details["avatar_url"] == "NULL"){
                  $operator_avtr = base_url("resources/no-image.jpg");
                }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
                require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                  QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
                   $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
                   $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
                  if(rename($img_src, $img_dest));
                $html ='
                <page format="100x100" orientation="L" style="font: arial;">';
                $html .= '
                <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                    <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                    <div style="margin-top:27px">
                      <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
                      <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
                    </div>
                  </div>
                  <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                      <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                      <br />
                      <h6>'.$this->lang->line("slider_heading1").'</h6>
                  </div>                
                </div>
                <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                  <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                    '.$this->lang->line("Ownward_Trip").'<br/>
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
                  <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                  <h6 style="margin-top:-10px;">
                  <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
                  <h6 style="margin-top:-8px;"> 
                  <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                  $ticket_seat_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);

                  $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                  foreach ($ticket_seat_details as $seat)
                  {
                    $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                    if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                      
                     $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                  }
                $html .='</tbody></table></div></page>';            
                $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
                require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
                $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
                $html2pdf->pdf->SetDisplayMode('default');
                $html2pdf->writeHTML($html);
                $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
                $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
                rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
                //whatsapp api send ticket
                $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
                $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
                //echo json_encode($result); die();
              /* -------------------------Return Ticket pdf ------------------------- */

              /* ---------------------Email Ticket to customer----------------------- */
                $emailBody = $this->lang->line('Please download your e-ticket.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                $emailAddress = trim($user_details['email1']);
                $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                $fileName = $my_ticket_pdf;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* ---------------------Email Ticket to customer----------------------- */
              
              /* -----------------Email Invoice to customer-------------------------- */
                $emailBody = $this->lang->line('Please download your ticket payment invoice.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
                $emailAddress = $user_details['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to customer-------------------- */

              //Deduct Return Trip Commission From Operator Account-------------------
                $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

                $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------

              //Add Return Trip Commission To Gonagoo Account-------------------------
                if($this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_gonagoo_master = array(
                    "gonagoo_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                }

                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
                $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "type" => 1,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "country_id" => trim($return_trip_operator_detail['country_id']),
                  "cat_id" => trim($return_ticket_details['cat_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              //----------------------------------------------------------------------

              /* Return ticket commission invoice------------------------------------- */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
                $rate = round(trim($return_trip_commission),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Commission Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "commission_invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
              /* --------------------------------------------------------------------- */
              /* -----------------------Email Invoice to operator-------------------- */
                $emailBody = $this->lang->line('Commission Invoice');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Commission Invoice');
                $emailAddress = $return_trip_operator_detail['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to operator-------------------- */
            }
          //Return Trip Payment Calculation--------------------------------------
        /*************************************** Payment Section ****************************************/
        $this->response = ['response' => 'success'];
      } else { $this->response = ['response' => 'failed']; }
    } else { $this->response = ['response' => 'failed']; }
    echo json_encode($this->response);
  }
  public function single_payment_by_mtn_from_mobile()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
    $phone_no = $this->input->post('phone_no', TRUE);
    $device_type = $this->input->post('device_type', TRUE); //android/ios
    $ticket_price = $this->input->post('ticket_price', TRUE); $ticket_price = (int) $ticket_price;
    $payment_method = 'mtn';
    if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
    $today = date('Y-m-d h:i:s');
    $user_details = $this->api->get_user_details($cust_id);
    $ticket_details = $this->api->get_trip_booking_details($ticket_id);
    $currency_sign = $ticket_details['currency_sign'];
    //Send SMS
    $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
    $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
    $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);

    //Send Push Notifications
    $api_key = $this->config->item('delivererAppGoogleKey');
    $device_details_customer = $this->api->get_user_device_details($cust_id);
    if(!is_null($device_details_customer)) {
      $arr_customer_fcm_ids = array();
      $arr_customer_apn_ids = array();
      foreach ($device_details_customer as $value) {
        if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
        else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
      }
      $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
      $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
      //Push Notification APN
      $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
      if(is_array($arr_customer_apn_ids)) { 
        $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
        $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
      }
    }
    
    //MTN Payment Gateway
    /*$url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$ticket_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    $mtn_pay_res = json_decode($contents, TRUE);*/
    //var_dump($mtn_pay_res); die();

    $transaction_id = 123;
    //echo json_encode($ticket_details); die();

    $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
    $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
    //echo json_encode($country_id); die();
    $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
    //echo json_encode($gonagoo_commission_percentage); die();
    $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

    $total_amount_paid = $ticket_price;
    $transfer_account_number = $phone_no;
    $bank_name = "NULL";
    $account_holder_name = "NULL";
    $iban = "NULL";
    $email_address = "NULL";
    $mobile_number = $phone_no;    
    
    $payment_data = array();
    //if($mtn_pay_res['StatusCode'] === "01"){
    if(1) {
      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($ticket_details['ticket_price']),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => $device_type,
        "complete_paid" => 1,
      );
      if($this->api->update_booking_details($update_data, $ticket_id)){
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        //Update to workroom
        $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
        $workroom_update = array(
          'order_id' => $ticket_details['ticket_id'],
          'cust_id' => $ticket_details['cust_id'],
          'deliverer_id' => $ticket_details['operator_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $ticket_details['cust_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $ticket_details['cust_name'],
          'deliverer_name' => $ticket_details['operator_name'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        /*************************************** Payment Section ****************************************/
          //Add Ownward Trip Payment to customer account + History----------------
            if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //----------------------------------------------------------------------
          //Deduct Ownward Trip Payment from customer account + History-----------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //----------------------------------------------------------------------
          //Add Ownward Trip Payment to trip operator account + History-----------
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //----------------------------------------------------------------------
          /* Ownward ticket invoice----------------------------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            $workroom_invoice_url = "ticket-invoices/".$pdf_name;
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  $workroom_invoice_url,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* --------------------------------------------------------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]==" " || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result = $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */

          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = trim($user_details['email1']);
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //Deduct Ownward Trip Commission From Operator Account------------------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //----------------------------------------------------------------------
          //Add Ownward Trip Commission To Gonagoo Account------------------------
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //----------------------------------------------------------------------
          /* Ownward ticket commission invoice------------------------------------ */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* --------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        /*************************************** Payment Section ****************************************/
        $this->response = ['response' => 'success'];
      } else { $this->response = ['response' => 'failed']; }
    } else { $this->response = ['response' => 'failed']; }
    echo json_encode($this->response);
  }



  public function payment_by_wecashup()
  {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $merchant_uid = 'kZudktr3nxX0AMM633dXyHEesV13';            // Replace with your merchant_uid
    $merchant_public_key = 'DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl'; // Replace with your merchant_public_key !!!
    $merchant_secret = 'XKjy7SzenXCvF9N0';                       // Replace with your merchant_private_key !!!
    $transaction_uid = '';// create an empty transaction_uid
    $transaction_token  = '';// create an empty transaction_token
    $transaction_provider_name  = ''; // create an empty transaction_provider_name
    $transaction_confirmation_code  = ''; // create an empty confirmation code
    $today = date('Y-m-d h:i:s');

    if(isset($_POST['transaction_uid'])){ $transaction_uid = $this->input->post('transaction_uid', TRUE);   } 
    if(isset($_POST['transaction_token'])){  $transaction_token  = $this->input->post('transaction_token', TRUE);  } 
    if(isset($_POST['transaction_provider_name'])){  $transaction_provider_name  = $this->input->post('transaction_provider_name', TRUE);   }
    if(isset($_POST['transaction_confirmation_code'])){  $transaction_confirmation_code  = $this->input->post('transaction_confirmation_code', TRUE); } 
    $url = 'https://www.wecashup.com/api/v1.0/merchants/'.$merchant_uid.'/transactions/'.$transaction_uid.'/?merchant_public_key='.$merchant_public_key;

    $fields_string = '';
    $fields = array(
      'merchant_secret' => urlencode($merchant_secret),
      'transaction_token' => urlencode($transaction_token),
      'transaction_uid' => urlencode($transaction_uid),
      'transaction_confirmation_code' => urlencode($transaction_confirmation_code),
      'transaction_provider_name' => urlencode($transaction_provider_name),
      '_method' => urlencode('PATCH')
    );
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);

    $data = json_decode($server_output, true);
    $payment_data = array();
    if($data['response_status'] =="success"){

      $payment_data = array(
        "cust_id" => (int)$data['response_content']['transaction']['transaction_sender_reference'],
        "order_id" => (int)$data['response_content']['transaction']['transaction_receiver_reference'],
        "payment_method" => trim($data['response_content']['transaction']['transaction_provider_name']),
        "transaction_id" => trim($data['response_content']['transaction']['transaction_uid']),
        "total_amount_paid" => trim($data['response_content']['transaction']['transaction_receiver_total_amount']),
        "transfer_account_number" => trim($data['response_content']['transaction']['transaction_sender_uid']),
        "currency_sign" => trim($data['response_content']['transaction']['transaction_receiver_currency']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => "NULL",
        "payment_response" => json_encode($data),
        "payment_by" => "web",
      );
      $this->update_order_payment_for_wecashup($payment_data);
      
      $this->session->set_flashdata('success', 'Payment successfully done!');
    } 
    else {  $this->session->set_flashdata('error', 'Payment failed! Try again...'); }

    $location = base_url('user-panel/my-bookings');
    echo '<script>top.window.location = "'.$location.'"</script>';
  }
  public function update_order_payment_for_wecashup($data=null)
  {
    if(!is_null($data)){
      $cust_id = (int) $data['cust_id'];
      $order_id = (int) $data['order_id'];
      $payment_method = trim($data['payment_method']);
      $transaction_id = trim($data['transaction_id']);
      $today = date('Y-m-d h:i:s');
      $order_details = $this->api->get_order_detail($order_id);
      $total_amount_paid = trim($data['total_amount_paid']);
      $transfer_account_number = trim($data['transfer_account_number']);
      $bank_name = trim($data['bank_name']);
      $account_holder_name = trim($data['account_holder_name']);
      $iban = trim($data['iban']);
      $email_address = trim($data['email_address']);
      $mobile_number = trim($data['mobile_number']);
      $currency_sign = trim($data['currency_sign']);
      $payment_response = trim($data['payment_response']);
      $payment_by = trim($data['payment_by']);

      $update_data_order = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "transaction_id" => trim($transaction_id),
        "payment_response" => trim($payment_response),
        "payment_by" => trim($payment_by),
        "payment_datetime" => $today,
        "complete_paid" => "1",
        "payment_mode" => "payment",
        "cod_payment_type" => "NULL",
      );

      if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {
        /*************************************** Payment Section ****************************************/
        //Add core price to gonagoo master
        if($this->api->gonagoo_master_details(trim($data['currency_sign']))) {
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
        } 
        else {
        
          $insert_data_gonagoo_master = array(
            "gonagoo_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($data['currency_sign']),
          );
          $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
        }

        $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
        $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
        $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

        $update_data_gonagoo_history = array(
          "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
          "order_id" => (int)$order_id,
          "user_id" => (int)$cust_id,
          "type" => 1,
          "transaction_type" => 'standard_price',
          "amount" => trim($order_details['standard_price']),
          "datetime" => $today,
          "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
          "transfer_account_type" => trim($payment_method),
          "transfer_account_number" => trim($transfer_account_number),
          "bank_name" => trim($bank_name),
          "account_holder_name" => trim($account_holder_name),
          "iban" => trim($iban),
          "email_address" => trim($email_address),
          "mobile_number" => trim($mobile_number),
          "transaction_id" => trim($transaction_id),
          "currency_code" => trim($data['currency_sign']),
          "country_id" => trim($order_details['from_country_id']),
          "cat_id" => trim($order_details['category_id']),
        );

        $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          
          //Update urgent fee in gonagoo account master
          if(trim($order_details['urgent_fee']) > 0) {
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'urgent_fee',
              "amount" => trim($order_details['urgent_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
          "country_id" => trim($order_details['from_country_id']),
          "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update insurance fee in gonagoo account master
          if(trim($order_details['insurance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'insurance_fee',
              "amount" => trim($order_details['insurance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update handling fee in gonagoo account master
          if(trim($order_details['handling_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'handling_fee',
              "amount" => trim($order_details['handling_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
          "country_id" => trim($order_details['from_country_id']),
          "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update vehicle fee in gonagoo account master
          if(trim($order_details['vehicle_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'vehicle_fee',
              "amount" => trim($order_details['vehicle_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
          "country_id" => trim($order_details['from_country_id']),
          "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

            //Update custome clearance fee in gonagoo account master
            if(trim($order_details['custom_clearance_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
                $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
              
                $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 1,
                    "transaction_type" => 'custom_clearance_fee',
                    "amount" => trim($order_details['custom_clearance_fee']),
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => trim($payment_method),
                    "transfer_account_number" => trim($transfer_account_number),
                    "bank_name" => trim($bank_name),
                    "account_holder_name" => trim($account_holder_name),
                    "iban" => trim($iban),
                    "email_address" => trim($email_address),
                    "mobile_number" => trim($mobile_number),
                    "transaction_id" => trim($transaction_id),
                    "currency_code" => trim($order_details['currency_sign']),
          "country_id" => trim($order_details['from_country_id']),
          "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }
            //Update loading_unloading_charges in gonagoo account master
            if(trim($order_details['loading_unloading_charges']) > 0) {
              $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
            
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'loading_unloading_charges',
                "amount" => trim($order_details['loading_unloading_charges']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
          "country_id" => trim($order_details['from_country_id']),
          "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            }

          //Add payment details in customer account master and history
          if($this->api->customer_account_master_details($cust_id, trim($data['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($data['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
          }

          $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
   
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($data['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Deduct payment details from customer account master and history
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($data['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          /*************************************** Payment Section ****************************************/ 
      }         
    }
  }
  public function android_payment_by_wecashup()
  {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $merchant_uid = 'kZudktr3nxX0AMM633dXyHEesV13';            // Replace with your merchant_uid
    $merchant_public_key = 'DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl'; // Replace with your merchant_public_key !!!
    $merchant_secret = 'XKjy7SzenXCvF9N0';                       // Replace with your merchant_private_key !!!
    $transaction_uid = '';// create an empty transaction_uid
    $transaction_token  = '';// create an empty transaction_token
    $transaction_provider_name  = ''; // create an empty transaction_provider_name
    $transaction_confirmation_code  = ''; // create an empty confirmation code
    $today = date('Y-m-d h:i:s');

    if(isset($_POST['transaction_uid'])){ $transaction_uid = $this->input->post('transaction_uid', TRUE);   } 
    if(isset($_POST['transaction_token'])){  $transaction_token  = $this->input->post('transaction_token', TRUE);  } 
    if(isset($_POST['transaction_provider_name'])){  $transaction_provider_name  = $this->input->post('transaction_provider_name', TRUE);   }
    if(isset($_POST['transaction_confirmation_code'])){  $transaction_confirmation_code  = $this->input->post('transaction_confirmation_code', TRUE); } 
    $url = 'https://www.wecashup.com/api/v1.0/merchants/'.$merchant_uid.'/transactions/'.$transaction_uid.'/?merchant_public_key='.$merchant_public_key;

    $fields_string = '';
    $fields = array(
      'merchant_secret' => urlencode($merchant_secret),
      'transaction_token' => urlencode($transaction_token),
      'transaction_uid' => urlencode($transaction_uid),
      'transaction_confirmation_code' => urlencode($transaction_confirmation_code),
      'transaction_provider_name' => urlencode($transaction_provider_name),
      '_method' => urlencode('PATCH')
    );
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);

    $data = json_decode($server_output, true);
    $payment_data = array();
    if($data['response_status'] =="success"){

      $payment_data = array(
        "cust_id" => (int)$data['response_content']['transaction']['transaction_sender_reference'],
        "order_id" => (int)$data['response_content']['transaction']['transaction_receiver_reference'],
        "payment_method" => trim($data['response_content']['transaction']['transaction_provider_name']),
        "transaction_id" => trim($data['response_content']['transaction']['transaction_uid']),
        "total_amount_paid" => trim($data['response_content']['transaction']['transaction_receiver_total_amount']),
        "transfer_account_number" => trim($data['response_content']['transaction']['transaction_sender_uid']),
        "currency_sign" => trim($data['response_content']['transaction']['transaction_receiver_currency']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => "NULL",
        "payment_response" => json_encode($data),
        "payment_by" => "android",
        "complete_paid" => 1,
      );
      $this->update_order_payment_for_wecashup($payment_data);
            
      $location = base_url('payment/success');
    } 
    else {  $location = base_url('payment/failed');  }

    echo '<script>top.window.location = "'.$location.'"</script>';
  }
  public function ios_payment_by_wecashup()
  {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $merchant_uid = 'kZudktr3nxX0AMM633dXyHEesV13';            // Replace with your merchant_uid
    $merchant_public_key = 'DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl'; // Replace with your merchant_public_key !!!
    $merchant_secret = 'XKjy7SzenXCvF9N0';                       // Replace with your merchant_private_key !!!
    $transaction_uid = '';// create an empty transaction_uid
    $transaction_token  = '';// create an empty transaction_token
    $transaction_provider_name  = ''; // create an empty transaction_provider_name
    $transaction_confirmation_code  = ''; // create an empty confirmation code
    $today = date('Y-m-d h:i:s');

    if(isset($_POST['transaction_uid'])){ $transaction_uid = $this->input->post('transaction_uid', TRUE);   } 
    if(isset($_POST['transaction_token'])){  $transaction_token  = $this->input->post('transaction_token', TRUE);  } 
    if(isset($_POST['transaction_provider_name'])){  $transaction_provider_name  = $this->input->post('transaction_provider_name', TRUE);   }
    if(isset($_POST['transaction_confirmation_code'])){  $transaction_confirmation_code  = $this->input->post('transaction_confirmation_code', TRUE); } 
    $url = 'https://www.wecashup.com/api/v1.0/merchants/'.$merchant_uid.'/transactions/'.$transaction_uid.'/?merchant_public_key='.$merchant_public_key;

    $fields_string = '';
    $fields = array(
      'merchant_secret' => urlencode($merchant_secret),
      'transaction_token' => urlencode($transaction_token),
      'transaction_uid' => urlencode($transaction_uid),
      'transaction_confirmation_code' => urlencode($transaction_confirmation_code),
      'transaction_provider_name' => urlencode($transaction_provider_name),
      '_method' => urlencode('PATCH')
    );
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);

    $data = json_decode($server_output, true);
    $payment_data = array();
    if($data['response_status'] =="success"){

      $payment_data = array(
        "cust_id" => (int)$data['response_content']['transaction']['transaction_sender_reference'],
        "order_id" => (int)$data['response_content']['transaction']['transaction_receiver_reference'],
        "payment_method" => trim($data['response_content']['transaction']['transaction_provider_name']),
        "transaction_id" => trim($data['response_content']['transaction']['transaction_uid']),
        "total_amount_paid" => trim($data['response_content']['transaction']['transaction_receiver_total_amount']),
        "transfer_account_number" => trim($data['response_content']['transaction']['transaction_sender_uid']),
        "currency_sign" => trim($data['response_content']['transaction']['transaction_receiver_currency']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => "NULL",
        "payment_response" => json_encode($data),
        "payment_by" => "ios",
        "complete_paid" => 1,
      );
      $this->update_order_payment_for_wecashup($payment_data);
            
      $location = base_url('payment/success');
    } 
    else {  $location = base_url('payment/failed');  }

    echo '<script>top.window.location = "'.$location.'"</script>';
  }
  public function success() { echo 'success'; }
  public function failed() {  echo 'failed'; }

}

/* End of file Payment_bus.php */
/* Location: ./application/controllers/Payment_bus.php */