<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Api_troubleshoot extends CI_Controller {
  private $errors = array();
  private $cust = array();
  public function __construct() {
    parent::__construct();
    $this->load->model('Api_model_services', 'api');   
    // $this->load->model('Notification_model_bus', 'notification'); 
    // $this->load->model('Web_user_model_bus', 'user');
    // $this->load->model('api_model', 'api_courier');
    $this->load->model('api_model_sms', 'api_sms');
    // $this->load->model('Users_model', 'users');
    // $this->load->model('transport_model', 'transport');

    header('Content-Type: Application/json'); 
    if( $this->input->method() != 'post' ) exit('No direct script access allowed');
    if( $this->input->is_ajax_request() ) exit('No direct script access allowed');

    $cust_id = $this->input->post('cust_id', TRUE);    
    // $this->api->update_last_login($cust_id);
    if(!empty($cust_id)){  $this->cust = $this->api->get_consumer_datails($cust_id, true); }

    if(isset($_POST)){

      $lang = $this->input->post('lang', TRUE);
      if( $lang == 'fr' ) {
        $this->config->set_item('language', 'french');
        $this->lang->load('frenchApi_lang','french');
      }
      else if( $lang == 'sp' ) {
        $this->config->set_item('language', 'spanish');
        $this->lang->load('spanishApi_lang','spanish');
      }
      else{
        $this->config->set_item('language', 'english');
        $this->lang->load('englishApi_lang','english');
        $this->lang->load('englishFront_lang','english');
      }

      foreach($_POST as $key => $value){ 
        if(trim($value) == "") { $this->errors[] = ucwords(str_replace('_', ' ', $key)); }        
      }
      if(!empty($this->errors)) { 
        echo json_encode(array('response' => 'failed', 'message' => 'Empty field(s) - '.implode(', ', $this->errors))); 
      }
    }
  }
  public function index() { exit('No direct script access allowed.'); }

  public function register_troubleshoot_job()
  {
    // echo json_encode($_POST);die();
    $cust_id =$this->input->post('cust_id', TRUE);
    $job_title = $this->input->post('job_title', TRUE);
    $cat_id = $this->input->post('category', TRUE);
    $sub_cat_id = $this->input->post('sub_category', TRUE);
    $job_desc = $this->input->post('description', TRUE);
    $address = $this->input->post('toMapID', TRUE);

    $immdediate = $this->input->post('immdediate', TRUE);
    $complete_date = $this->input->post('start_date', TRUE);

    $customer_details = $this->api->get_user_details($cust_id);
    $currency_details = $this->api->get_country_currency_detail($customer_details['country_id']);
    $country_name = $this->input->post('country', TRUE);
    $country_id = $this->api->get_country_id_by_name($country_name);
    $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']);

    $size = $this->input->post('size', TRUE);
    $questions=array();
    $answers=array(); 
    for ($i=1; $i <=$size ; $i++) {
      array_push($questions ,$this->input->post('ques_'.$i, TRUE));
      array_push($answers ,$this->input->post('ans_'.$i, TRUE));
    }
   
    $today = date('Y-m-d h:i:s');
    $insert_data = array(
      "job_title" => trim($job_title),
      "cust_id" => trim($cust_id),
      "cat_id" => trim($cat_id),
      "sub_cat_id" => trim($sub_cat_id),
      "job_desc" => trim($job_desc),

      "address" => trim($address),
   
      "country_id" => trim($country_id),
      "cre_datetime" => trim($today),
      "cust_name" => $customer_name,
      "cust_contact" => $customer_details['mobile1'],
      "cust_email" => $customer_details['email1'],
      "job_type"=>1,
      "immediate" => $immdediate,
      "location_type"=>"Onsite",
      "visibility"=>"public",
      "currency_id"=>$currency_details['currency_id'],
      "currency_code"=>$currency_details['currency_sign'],
    );
    $insert_data['complete_date']=($immdediate)?date("Y-m-d" , strtotime($complete_date)):"";
    // echo json_encode($insert_data);die();
    if($job_id = $this->api->register_job_details($insert_data)){

       $k=0;
        foreach ($answers as $ans) {
          $data=array(
            'question'=>$questions[$k],
            'answer'=>$answers[$k],
            'job_id'=>$job_id
          );
          if($answers[$k]){
            $this->db->insert("tbl_question_answer" , $data);
          // echo json_encode($data);
          } 
          $k++;
        }
        $job_details = $this->api->get_customer_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
      
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your  Troubleshoot job is posted').": ".$job_id;
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Troubleshoot post'), 'type' => 'troubleshoot post', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Troubleshoot post'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //Notifications to customers----------------

        //Update to workstream master
        $update_data = array(
          'job_id' => (int)$job_id,
          'cust_id' => $cust_id,
          'provider_id' => 0,
          'cre_datetime' => $today,
          'status' => 1,
        );
        if($id = $this->api->check_workstream_master($update_data)){
          $workstream_id = $id;
        }else{
          $workstream_id = $this->api->update_to_workstream_master($update_data);
        }
        $update_data = array(
          'workstream_id' => (int)$workstream_id,
          'job_id' => (int)$job_id,
          'cust_id' => $cust_id,
          'provider_id' => 0,
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $cust_id,
          'type' => 'job_started',
          'file_type' => 'text',
          'proposal_id' => 0,
        );
        $this->api->update_to_workstream_details($update_data);
       $result=array("response"=>"success" , "message"=> $this->lang->line('Job troubleshoot is succcessfully posted'));
    }else{
       $result=array("response"=>"failed" , "message"=> $this->lang->line('Failed to posted the job troubleshoot'));

    }
    echo json_encode($result);
  }

  public function get_troubleshoot_by_id()
  {
    $job_id=$this->input->post("job_id");
    $job_details = $this->api->get_job_details($job_id);
    $job_details['ques_ans'] = $this->api->get_troubleshoot_ques_ans($job_id);
    if($job_details){
      $result=array("response"=>"success" , "message"=> $this->lang->line('Details found!')  , "list"=>$job_details); 
    }else{
      $result=array("response"=>"failed" , "message"=> $this->lang->line('details not found')); 
    }
    echo json_encode($result);
  }
}
?>