<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
  private $errors = array();
  private $cust = array();

  public function __construct()
  {
    parent::__construct();
    $this->load->model('api_model', 'api');   
    $this->load->model('Notification_model', 'notification');  
    $this->load->model('Web_user_model', 'user');    
    $this->load->model('Users_model', 'users');
    $this->load->model('transport_model', 'transport');
    $this->load->model('api_model_sms', 'api_sms'); 

    header('Content-Type: Application/json'); 
    if( $this->input->method() != 'post' ) exit('No direct script access allowed');
    /*if( $this->input->is_ajax_request() ) exit('No direct script access allowed');*/

    $cust_id = $this->input->post('cust_id', TRUE);    
    if(!empty($cust_id)){  $this->cust = $this->api->get_consumer_datails($cust_id, true); }

    if(isset($_POST)){

      $lang = $this->input->post('lang', TRUE);
      if( $lang == 'fr' ) {
        $this->config->set_item('language', 'french');
        $this->lang->load('frenchApi_lang','french');
      }
      else if( $lang == 'sp' ) {
        $this->config->set_item('language', 'spanish');
        $this->lang->load('spanishApi_lang','spanish');
      }
      else{
        $this->config->set_item('language', 'english');
        $this->lang->load('englishApi_lang','english');
        $this->lang->load('englishFront_lang','english');
      }

      foreach($_POST as $key => $value){ 
        if(trim($value) == "") { $this->errors[] = ucwords(str_replace('_', ' ', $key)); }        
      }
      if(!empty($this->errors)) { 
        echo json_encode(array('response' => 'failed', 'message' => 'Empty field(s) - '.implode(', ', $this->errors))); 
      }
    }
  }

  public function index() { exit('No direct script access allowed.'); }

  public function app_params()
  {
    if(empty($this->errors)){ 
      $app_ver = $this->input->post('version', TRUE);
      $app_type = $this->input->post('app_type', TRUE); // ANDROID/IOS
      $params = $this->api->get_application_parameters();
      //echo "DB Ver=".$params->app_version_ios;
      //echo "==App Ver=".$app_ver;
      //echo "==".version_compare($app_ver, $params->app_version_ios); die();
      
      if($app_type == 'ANDROID') {
        if(version_compare($app_ver, $params->app_version) < 0) {
          if($params->forcefully_update == 1) {
            $this->response = ['response' => 'success', 'message' => $this->lang->line('new_update_found_please_update_to_continue'), 'forcefully_update' => 1 ];
          } else { $this->response = ['response' => 'success', 'message' => $this->lang->line('new_update_found_please_update_to_continue'), 'forcefully_update' => 0 ]; }
        } else { $this->response = ['response' => 'failed', 'message' => '', 'forcefully_update' => 0 ]; }
      } else {
        if(version_compare($app_ver, $params->app_version_ios) < 0) {
          if($params->forcefully_update_ios == 1) {
            $this->response = ['response' => 'success', 'message' => $this->lang->line('new_update_found_please_update_to_continue'), 'forcefully_update' => 1 ];
          } else { $this->response = ['response' => 'success', 'message' => $this->lang->line('new_update_found_please_update_to_continue'), 'forcefully_update' => 0 ]; }
        } else { $this->response = ['response' => 'failed', 'message' => '', 'forcefully_update' => 0 ]; }
      }
      echo json_encode($this->response);
    }
  }

  public function registration()
  {
    if(empty($this->errors)){ 
      $device_id = $this->input->post('device_id', TRUE);
      $firstname = $this->input->post('first_name', TRUE);
      $lastname = $this->input->post('last_name', TRUE);
      $gender = $this->input->post('gender', TRUE);   // m : male , f : female
      $country_id = $this->input->post('country_id', TRUE); $country_id = (int) $country_id;
      $state_id = $this->input->post('state_id', TRUE); $state_id = (int) $state_id;
      $city_id = $this->input->post('city_id', TRUE); $city_id = (int) $city_id;
      $email_id = $this->input->post('email_id', TRUE);
      $password = $this->input->post('password', TRUE);
      $mobile_no = $this->input->post('mobile_no', TRUE); if(substr($mobile_no, 0, 1) == 0) { $mobile_no = ltrim($mobile_no, 0); }
      $user_type = $this->input->post('user_type', TRUE); $user_type = (int) $user_type;    // 1: individuals , 2: Business
      $device_type = $this->input->post('device_type', TRUE); $device_type = (int) $device_type; // 1 : android, 2: ios
      $company_name = $this->input->post('company_name');
      $acc_type = $this->input->post('acc_type');  // seller, buyer, both
      $notification_regid = $this->input->post('notification_regid', TRUE);
      $social_registration = $this->input->post('social_registration', TRUE); $social_registration = (int) $social_registration; // 1 : Social, 2: Manual      
      $social_type = $this->input->post('social_type', TRUE);  // fb: Facebook, li : Linkedin
      $social_type = trim($social_type);
      
      if(strtolower($social_type) == "li" ) {
        $email_id = trim($password).'@linkedin.com';     
        $social_type = "LI"; $social_flag = 1; 
        $password = 'li__'.$password.'@';
      }
      else if ( strtolower($social_type) == "fb" ) {
        $email_id = trim($password).'@facebook.com'; 
        $social_type = "FB"; $social_flag = 1; 
        $password = 'fb__'.$password.'@';
      }
      else { $social_type = "NULL"; $social_flag = 0;}

      if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('profile_image')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];  
        }         
      } else { $avatar_url ="NULL"; }

      if( !empty($_FILES["cover_image"]["tmp_name"]) ) {
        $config2['upload_path']    = 'resources/cover-images/';                 
        $config2['allowed_types']  = '*';       
        $config2['max_size']       = '0';                          
        $config2['max_width']      = '0';                          
        $config2['max_height']     = '0';                          
        $config2['encrypt_name']   = true; 
        $this->load->library('upload', $config2);
        $this->upload->initialize($config2);

        if($this->upload->do_upload('profile_image')) {   
          $uploads2    = $this->upload->data();  
          $cover_url =  $config2['upload_path'].$uploads2["file_name"];  
        }         
      } else { $cover_url ="NULL"; }

      $activation_hash = sha1(mt_rand(10000,99999).time().trim($email_id));
      $OTP = mt_rand(100000,999999);

      if($acc_type == 'seller') { $is_deliverer = 0; }
      else if($acc_type == 'buyer') { $is_deliverer = 0; }
      else  { $is_deliverer = 0; }

      $user_insert_data = array(
        "usertype" =>  ($user_type == 2 ) ? 0: 1,
        "firstname" => trim($firstname),
        "lastname" => trim($lastname),
        "email_id" => trim($email_id),
        "password" => trim($password),
        "mobile_no" => trim($mobile_no),
        "gender" => trim($gender),
        "country_id" => (int) $country_id,
        "state_id" => (int) $state_id,
        "city_id" => (int) $city_id,        
        "avatar_url" => trim($avatar_url),
        "cover_url" => trim($cover_url),
        "social_login" => $social_flag,
        "social_type" => strtoupper($social_type),
        "email_verified" => ($social_flag == 1 ) ? 1: 0,
        "activation_hash" => trim($activation_hash),
        "otp" => trim($OTP),
        "acc_type" => $acc_type,
        "is_deliverer" => (int)$is_deliverer,
        "company_name" => trim($company_name),
        "driver_count" => 0,
      );

      if(!$this->api->is_emailId_exists(trim($email_id))){ 
        if( $cust_id = $this->api->register_consumer($user_insert_data) ){
          // nulled skilled registration 
          $cs_id = $this->api->register_nulled_skilled($cust_id);
          // nulled account master registration 
          //$account_id = $this->api->register_nulled_account_master($cust_id);
          //$this->api->update_account_id($account_id, $cust_id);
          // check for device ID existance
          if(!$this->api->is_deviceId_exists($device_id, $cust_id)){ 
            // insert into device table
            $device_insert_data = array(
              "device_id" => trim($device_id),
              "cust_id" => (int)($cust_id),
              "reg_id" => trim($notification_regid),
              "device_type" => ($device_type == 2 ) ? 0: 1,
            );
            $this->api->register_new_device($device_insert_data);
          } else{
            // update into device table 
            $device_update_data = array(
              "cust_id" => (int)($cust_id),
              "reg_id" => trim($notification_regid),
              "device_type" => ($device_type == 2 ) ? 0: 1,
            );      
            $this->api->update_device_detail($device_id, $device_update_data, $cust_id);
          }
          // Get user detail
          $user = $this->api->get_consumer_datails($cust_id);
          $user['cs_id'] = (int) $cs_id;
          // send SMS
          $country_code = $this->api->get_country_code_by_id($country_id);
          if(substr($mobile_no, 0, 1) == 0) { $mobile_no = ltrim($mobile_no, 0); }
          $this->api->sendSMS($country_code.$mobile_no, $this->lang->line('register_sms_message'). ' ' . $OTP);
          if($user_type == 2) {
            $subject = $this->lang->line('email_verification_title');
            $message = $this->lang->line('verify_email_text1').$email_id.$this->lang->line('verify_email_text2');
            $message .="</td></tr><tr><td align='center'><br />";
            $message .="<a href='".$this->config->item('base_url')."verify/email/". strtolower($email_id)."/".$activation_hash."' class='callout'>".$this->lang->line('verify_email_btn')."</a>";
             $username = trim($user['firstname']) .' '.trim($user['lastname']);

            // Send activation email to given email id
            $this->api_sms->send_email_text($username, $email_id, $subject, trim($message));
            $this->response = ['response' => 'success', 'message' =>  $this->lang->line('verify_register'), "user" => $user ];
          } else { $this->response = ['response' => 'success','message'=> $this->lang->line('register_success_otp'), "user" => $user]; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('register_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_exists').'...']; }
      echo json_encode($this->response);
    }
  }

  public function login()
  {
    if(empty($this->errors)){ 
      $device_id = $this->input->post('device_id', TRUE);
      $email_id = $this->input->post('email_id', TRUE);
      $password = $this->input->post('password', TRUE);
      $login_type = $this->input->post('login_type',TRUE); //  fb: facebook, li: linkedin
      $device_type = $this->input->post('device_type', TRUE); $device_type = (int) $device_type; // 1 : android, 2: ios
      $notification_regid = $this->input->post('notification_regid', TRUE);
      $today_date = date('Y-m-d H:i:s');
      
      if(strtolower($login_type) == "li" ) { $email_id = trim($password).'@linkedin.com';   $password = 'li__'.$password.'@';  }
      else if ( strtolower($login_type) == "fb" ) { $email_id = trim($password).'@facebook.com';  $password = 'fb__'.$password.'@'; }
      else { $password = $password; }   
       
      if( (trim($email_id) != "NULL") AND ($user = $this->api->is_emailId_exists(trim($email_id))) ){ 
        if( $cust_id = $this->api->validate_login(trim($email_id), trim($password)) ){
          if($this->api->is_user_active_or_exists($cust_id)){
            // check for device ID existance
            if(!$this->api->is_deviceId_exists($device_id, $cust_id)){ 
              // insert into device table
              $device_insert_data = array(
                "device_id" => trim($device_id),
                "cust_id" => (int)($cust_id),
                "reg_id" => trim($notification_regid),
                "device_type" => ($device_type == 2 ) ? 0: 1,
              );
              $this->api->register_new_device($device_insert_data);
            } else{
              // update into device table 
              $device_update_data = array(
                "cust_id" => (int)($cust_id),
                "reg_id" => trim($notification_regid),
                "device_type" => ($device_type == 2 ) ? 0: 1,
              );      
              $this->api->update_device_detail($device_id, $device_update_data, $cust_id);
            } 

            // udpate last login 
            $this->api->update_last_login($cust_id);
            $ip =   getenv('HTTP_CLIENT_IP')?:
                    getenv('HTTP_X_FORWARDED_FOR')?:
                    getenv('HTTP_X_FORWARDED')?:
                    getenv('HTTP_FORWARDED_FOR')?:
                    getenv('HTTP_FORWARDED')?:
                    getenv('REMOTE_ADDR');
            
            $user_agent = ($device_type == 1) ? 'Android': 'iOS';
            
            $user_login_data = array(
              "cust_id" =>  (int) $cust_id,
              "cre_datetime" => trim($today_date),
              "mod_datetime" => trim($today_date),
              "hour_spends" => 0,
              "period_status" => 1,
              "user_agent" => trim($user_agent),
              "ip_address" => trim($device_id),
            );
            $this->api->register_user_login_history($user_login_data);
            
            $this->response = ['response' => 'success','message'=> $this->lang->line('login_success'), "user" => $user];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('invalid_email').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_not_exists')]; }
      echo json_encode($this->response);
    }
  }

  public function basic_info_update()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $gender = $this->input->post('gender', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      $city_id = $this->input->post('city_id', TRUE);
    
      $update_data = array(
        'mobile1' => $mobile1,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'gender' => $gender,
        'country_id' => $country_id,
        'state_id' => $state_id,
        'city_id' => $city_id,
      );

      if( $this->api->update_user_basic_info($cust_id, $update_data)){
        $this->response = ['response' => 'success', 'message' => $this->lang->line('update_success')]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function update_avatar()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){

      if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('profile_image')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];  

          $old_profile_image = $this->api->get_old_profile_image($cust_id);
          
          if( $this->api->update_profile_image($avatar_url, $cust_id)){
            if( $old_profile_image != "NULL") { unlink($old_profile_image); }
            $user = $this->api->get_consumer_datails($cust_id);
            $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "user" => $user];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found').'...']; }
      // update user last login
      $this->api->update_last_login($cust_id);
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function update_cover()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){
      if( !empty($_FILES["cover_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/cover-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('cover_image')) {   
          $uploads    = $this->upload->data();  
          $cover_url =  $config['upload_path'].$uploads["file_name"];  
          $old_cover_image = $this->api->get_old_cover_image($cust_id);
          if( $this->api->update_cover_image($cover_url, $cust_id)){
            if( $old_cover_image  != "NULL" ){ unlink($old_cover_image); }
            $user = $this->api->get_consumer_datails($cust_id);
            $this->response = ['response' => 'success','message'=>$this->lang->line('update_success'), "user" => $user];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found').'...']; }
      // update user last login
      $this->api->update_last_login($cust_id);
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function update_overview()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    $profession = $this->input->post('profession', TRUE);
    $overview = $this->input->post('overview', TRUE);

    if($user = $this->api->is_user_active_or_exists($cust_id)){
      $update_data = array(
        "profession" => trim($profession),
        "overview" => trim($overview),
      );

      if( $this->api->update_overview($cust_id,$update_data) ){
        $user = $this->api->get_consumer_datails($cust_id);
        $this->response = ['response' => 'success','message'=>$this->lang->line('update_success'), "user" => $user];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
      // update user last login
      $this->api->update_last_login($cust_id);
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function forget_password()
  {
    $email_id = $this->input->post('email_id', TRUE);
    if( $user = $this->api->is_emailId_exists($email_id)){
      $subject = $this->lang->line('forgot_password_title');
      $message ="<p class='lead'>You recently requested to forgot password for your Gonagoo account!</p>";
      $message .="</td></tr><tr><td align='center'><br />";
      $message .="<a class='callout'>Your password is: <strong>".$user['password']."</strong></a>";
      $username = trim($user['firstname']) . trim($user['lastname']);
      // Send activation email to given email id
      if( $this->api_sms->send_email_text($username, $email_id, $subject, trim($message))){
        if($user['mobile1']) { 
          $country_code = $this->api->get_country_code_by_id($user['country_id']);
          if(substr($user['mobile1'], 0, 1) == 0) { $phone_no = ltrim($user['mobile1'], 0); } else { $phone_no = $user['mobile1']; }
          $this->api->sendSMS($country_code.$phone_no, $this->lang->line('forgot_password_sms_message').' '.$user['password']); 
        }
        $this->response = ['response' => 'success', 'message' => $this->lang->line('email_sending_success') ];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_sending_failed').'...']; }
    }  else{  $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_not_exists')]; }
    echo json_encode($this->response);
  }

  public function get_portfolio_details()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        if( $portfolios = $this->api->get_customers_portfolio($cust_id)){         
          $this->response = ['response' => 'success','message'=> $this->lang->line('get_value_success'), "customers_portfolios" => $portfolios];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_value_failed').'...']; }
        $this->api->update_last_login($cust_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; } 
      echo json_encode($this->response);
    }
  }

  public function register_portfolio()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    $title = $this->input->post('title', TRUE);
    $cat_id = $this->input->post('cat_id', TRUE);
    $subcat_ids = $this->input->post('subcat_ids', TRUE);
    $tags = $this->input->post('tags', TRUE);
    $description = $this->input->post('description', TRUE);

    if( !empty($_FILES["attachement"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

        if($this->upload->do_upload('attachement')) {   
          $uploads    = $this->upload->data();  
          $attachement =  $config['upload_path'].$uploads["file_name"];  
        } else { $attachement = "NULL"; }
      } else { $attachement = "NULL"; }

      $insert_data = array(
        "cust_id" => (int)$cust_id,
        "title" => $title,
        "cat_id" => (int)$cat_id,
        "subcat_ids" => $subcat_ids,
        "description" => $description,
        "tags" => $tags,
        "attachement_url" => $attachement,
        "cre_datetime" => date('Y-m-d H:i:s'),
      );

      if( $portfolio_id = $this->api->register_portfolio($insert_data)){            
        $this->response = ['response' => 'success','message'=> $this->lang->line('portfolio_add_successfully')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('portfolio_add_successfully').'...']; }
      echo json_encode($this->response);
  }

  public function update_portfolio()
  {
    $portfolio_id = $this->input->post('portfolio_id', TRUE);
    $title = $this->input->post('title', TRUE);
    $cat_id = $this->input->post('cat_id', TRUE);
    $subcat_ids = $this->input->post('subcat_ids', TRUE);
    $tags = $this->input->post('tags', TRUE);
    $description = $this->input->post('description', TRUE);

    if( !empty($_FILES["attachement"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachement')) {   
        $uploads    = $this->upload->data();  
        $attachement =  $config['upload_path'].$uploads["file_name"];  

        $update_data = array(
          "title" => $title,
          "cat_id" => (int)$cat_id,
          "subcat_ids" => $subcat_ids,
          "description" => $description,
          "tags" => $tags,
          "attachement_url" => $attachement,
        );
      }
    } else { 
      $update_data = array(
        "title" => $title,
        "cat_id" => (int)$cat_id,
        "subcat_ids" => $subcat_ids,
        "description" => $description,
        "tags" => $tags,
      );
    }

    if( $this->api->update_portfolio($portfolio_id, $update_data)){  
      $detail = $this->api->get_portfolio_detail($portfolio_id);      
      $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "detail" => $detail];
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
    echo json_encode($this->response);
  }

  public function delete_portfolio()
  {
    $portfolio_id = $this->input->post('portfolio_id', TRUE);
    if($this->api->delete_portfolio($portfolio_id)){
      $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed').'...']; }
    echo json_encode($this->response);
  }

  public function skill_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $users_skill_list = $this->users->get_customer_skills($cust_id);
      if(empty($users_skill_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('skill_list_failed'), 'users_skill_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('skill_list_success'), 'users_skill_list' => $users_skill_list]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function skill_update()
  {

    if(empty($this->errors)){ 
      $cs_id = $this->input->post('cs_id', TRUE);
      $junior_skills = $this->input->post('junior_skills', TRUE);
      $confirmed_skills = $this->input->post('confirmed_skills', TRUE);
      $senior_skills = $this->input->post('senior_skills', TRUE);
      $expert_skills = $this->input->post('expert_skills', TRUE);
      $ext1 = $this->input->post('ext1', TRUE);
      $ext2 = $this->input->post('ext2', TRUE);


      $update_data = array(
        'cs_id' => (int) $cs_id,
        'junior_skills' => $junior_skills,
        'confirmed_skills' => $confirmed_skills,
        'senior_skills' => $senior_skills,
        'expert_skills' => $expert_skills,
        'ext1' => $ext1,
        'ext2' => $ext2
      );
      if( $this->api->update_user_skill($update_data)){
        $this->response = ['response' => 'success', 'message' => $this->lang->line('skill_update_success')]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('skill_update_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function skill_list_master()
  {
    if(empty($this->errors)){
      $skill_list = $this->api->get_skill_master();

      if(empty($skill_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('skill_list_master_failed'), 'skill_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('skill_list_master_success'), 'skill_list' => $skill_list]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function category_type_master()
  {
    if(empty($this->errors)){
      $type_list = $this->api->get_category_type();

      if(empty($type_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('category_list_failed'), 'category_type_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('category_list_success'), 'category_type_list' => $type_list]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function category_list_master()
  {
    if(empty($this->errors)){
      $category_list = $this->api->get_category();

      if(empty($category_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('category_list_failed'), 'category_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('category_list_success'), 'category_list' => $category_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function language_list_master()
  {
    if(empty($this->errors)){
      $language_list = $this->api->get_language_master();

      if(empty($language_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('language_list_failed'), 'language_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('language_list_success'), 'language_list' => $language_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function language_update()
  {

    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $lang_known = $this->input->post('lang_known', TRUE);
      $update_data = array(
        'cust_id' => (int) $cust_id,
        'lang_known' => $lang_known
      );

      if( $this->api->update_user_language($update_data)){
        $this->response = ['response' => 'success', 'message' => $this->lang->line('language_updated_success')]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('language_updated_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function education_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $users_education_list = $this->api->get_customer_education($cust_id);
      if(empty($users_education_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('education_list_failed'), 'users_education_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('education_list_success'), 'users_education_list' => $users_education_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function education_add()
  {
    if(empty($this->errors)) {
      $cust_id = $this->input->post('cust_id', TRUE);
      $activities = $this->input->post('activities', TRUE);
      $description = $this->input->post('description', TRUE);
      $start_date = $this->input->post('start_date', TRUE);
      $end_date = $this->input->post('end_date', TRUE);
      $specialization = $this->input->post('specialization', TRUE);
      $qualification = $this->input->post('qualification', TRUE);
      $qualify_year = $this->input->post('qualify_year', TRUE);
      $institute_name = $this->input->post('institute_name', TRUE);
      $institute_address = $this->input->post('institute_address', TRUE);
      $certificate_title = $this->input->post('certificate_title', TRUE);
      $grade = $this->input->post('grade', TRUE);
      $remark = $this->input->post('remark', TRUE);
      $cre_datetime = date('Y-m-d H:i:s');
      $mod_datetime = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust_id,
        'activities' => $activities,
        'description' => $description,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'specialization' => $specialization,
        'qualification' => $qualification, 
        'qualify_year' => $qualify_year, 
        'institute_name' => $institute_name, 
        'institute_address' => $institute_address, 
        'certificate_title' => $certificate_title, 
        'grade' => $grade, 
        'remark' => $remark, 
        'cre_datetime' => $cre_datetime, 
        'mod_datetime' => $mod_datetime
      );
      if( $id = $this->api->add_user_education($add_data)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('education_add_success'), 'edu_id' => $id]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('education_add_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function education_update()
  {

    if(empty($this->errors)){ 
      $edu_id = $this->input->post('edu_id', TRUE);
      $activities = $this->input->post('activities', TRUE);
      $description = $this->input->post('description', TRUE);
      $start_date = $this->input->post('start_date', TRUE);
      $end_date = $this->input->post('end_date', TRUE);
      $specialization = $this->input->post('specialization', TRUE);
      $qualification = $this->input->post('qualification', TRUE);
      $qualify_year = $this->input->post('qualify_year', TRUE);
      $institute_name = $this->input->post('institute_name', TRUE);
      $institute_address = $this->input->post('institute_address', TRUE);
      $certificate_title = $this->input->post('certificate_title', TRUE);
      $grade = $this->input->post('grade', TRUE);
      $remark = $this->input->post('remark', TRUE);
      $mod_datetime = date('Y-m-d H:i:s');
      

      $update_data = array(
        'edu_id' => (int) $edu_id,
        'activities' => $activities,
        'description' => $description,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'specialization' => $specialization,
        'qualification' => $qualification,
        'qualify_year' => $qualify_year,
        'institute_name' => $institute_name,
        'institute_address' => $institute_address,
        'certificate_title' => $certificate_title,
        'grade' => $grade,
        'remark' => $remark,
        'mod_datetime' => $mod_datetime
      );

      if( $this->api->update_user_education($update_data)){
        $this->response = ['response' => 'success', 'message' => $this->lang->line('education_update_success')]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('education_update_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function education_delete()
  {

    if(empty($this->errors)){ 
      $edu_id = $this->input->post('edu_id', TRUE);

      if( $this->api->delete_user_education($edu_id)){
        $this->response = ['response' => 'success', 'message' => $this->lang->line('education_delete_success')]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('education_delete_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function experience_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $users_experience_list = $this->api->get_customer_experience($cust_id);
      if(empty($users_experience_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('experience_list_failed'), 'users_experience_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('experience_list_success'), 'users_experience_list' => $users_experience_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function experience_add()
  {
    if(empty($this->errors)) {
      $cust_id = $this->input->post('cust_id', TRUE);
      $org_name = $this->input->post('org_name', TRUE);
      $job_title = $this->input->post('job_title', TRUE);
      $job_location = $this->input->post('job_location', TRUE);
      $job_desc = $this->input->post('job_desc', TRUE);
      $start_date = $this->input->post('start_date', TRUE);
      $end_date = $this->input->post('end_date', TRUE);
      $remark = $this->input->post('remark', TRUE);
      $grade = $this->input->post('grade', TRUE);
      $other = $this->input->post('other', TRUE);
      $cre_datetime = date('Y-m-d H:i:s');
      $mod_datetime = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust_id,
        'org_name' => $org_name,
        'job_title' => $job_title,
        'job_location' => $job_location,
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => $end_date, 
        'remark' => $remark, 
        'grade' => $grade, 
        'other' => $other, 
        'cre_datetime' => $cre_datetime, 
        'mod_datetime' => $mod_datetime
      );
      if( $id = $this->api->add_user_experience($add_data)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('experience_add_success'), 'edu_id' => $id]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('experience_add_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function experience_update()
  {

    if(empty($this->errors)){ 
      $exp_id = $this->input->post('exp_id', TRUE);
      $org_name = $this->input->post('org_name', TRUE);
      $job_title = $this->input->post('job_title', TRUE);
      $job_location = $this->input->post('job_location', TRUE);
      $job_desc = $this->input->post('job_desc', TRUE);
      $start_date = $this->input->post('start_date', TRUE);
      $end_date = $this->input->post('end_date', TRUE);
      $remark = $this->input->post('remark', TRUE);
      $grade = $this->input->post('grade', TRUE);
      $other = $this->input->post('other', TRUE);
      $mod_datetime = date('Y-m-d H:i:s');

      $update_data = array(
        'exp_id' => (int) $exp_id,
        'org_name' => $org_name,
        'job_title' => $job_title,
        'job_location' => $job_location,
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'remark' => $remark,
        'grade' => $grade,
        'other' => $other,
        'mod_datetime' => $mod_datetime
      );

      if( $this->api->update_user_experience($update_data)){
        $this->response = ['response' => 'success', 'message' => $this->lang->line('experience_update_success')]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('experience_update_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function experience_delete()
  {
    if(empty($this->errors)){ 
      $exp_id = $this->input->post('exp_id', TRUE);

      if( $this->api->delete_user_experience($exp_id)){
        $this->response = ['response' => 'success', 'message' => $this->lang->line('experience_delete_success')]; 
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('experience_delete_failed')]; 
      }
      echo json_encode($this->response);
    }
  }

  public function permitted_cat_list()
  {
    if(empty($this->errors)) {
      $permitted_cat_list = $this->api->get_permitted_cat();
      if(empty($permitted_cat_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('permitted_cat_failed'), 'permitted_cat_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('permitted_cat_success'), 'permitted_cat_list' => $permitted_cat_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function driver_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $users_driver_list = $this->api->get_customer_drivers($cust_id);
      if(empty($users_driver_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_list_failed'), 'users_driver_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_list_success'), 'users_driver_list' => $users_driver_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function driver_add()
  {

    if(empty($this->errors)) {  
      $cust_id = $this->input->post('cust_id', TRUE);
      $first_name = $this->input->post('first_name', TRUE);
      $last_name = $this->input->post('last_name', TRUE);
      $email = $this->input->post('email', TRUE); 
      $country_id = $this->input->post('country_id', TRUE); 
      $country_code = $this->input->post('country_code', TRUE); 
      $password = $this->input->post('password', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $mobile2 = $this->input->post('mobile2', TRUE);
      $cat_ids = $this->input->post('cat_ids', TRUE);
      $available = $this->input->post('available', TRUE);
      $country_name = $this->api->get_country_name_by_id((int)$country_id);
      $today = date('Y-m-d H:i:s');

      if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';             
        $config['allowed_types']  = '*';          
        $config['max_size']       = '0'; 
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('profile_image')) {   
          $uploads    = $this->upload->data();  
          $avatar_url = $config['upload_path'].$uploads["file_name"];  
        }         
      } else { $avatar_url ="NULL"; }

      $driver_insert_data = array(
        "cust_id" => trim($cust_id),
        "first_name" => trim($first_name),
        "last_name" => trim($last_name),
        "email" => trim($email),
        "password" => trim($password),
        "mobile1" => trim($mobile1),
        "mobile2" => trim($mobile2),
        "cat_ids" => trim($cat_ids),
        "available" => trim($available),
        "avatar" => trim($avatar_url),
        "latitude" => '0.00',
        "longitude" => '0.00',
        "loc_update_datetime" => $today,
        "cre_datetime" => $today,
        "mod_datetime" => $today,
        "country_id" => (int)$country_id,
        "country_code" => (int)$country_code,
        "country_name" => $country_name,
      );

      if(!$this->api->is_driver_exists(trim($email))){ 
        if( $cd_id = $this->api->register_driver($driver_insert_data) ){
          $cust = $this->cust;
          $cust_id = (int) $cust['cust_id'];      
          $driver_count = ((int)$cust['driver_count'] + 1);      
          $this->api->update_driver_count($driver_count, $cust_id);
          
          $subject = $this->lang->line('create_driver_title');
          $message ="<p class='lead'>".$this->lang->line('create_driver_email_body1')."</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_email')." : ".trim($email)."</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_password')." : ".trim($password)."</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          // Send email to given email id
          $this->api_sms->send_email_text(trim($first_name), trim($email), $subject, trim($message));

          $sms_msg = $this->lang->line('driver_sms_msg1') . trim($email) . ' ' . $this->lang->line('driver_sms_msg2') . trim($password);
          if(substr($mobile1, 0, 1) == 0) { $mobile1 = ltrim($mobile1, 0); }
          $this->api->sendSMS((int)$country_code.trim($mobile1), $sms_msg);
          
          $link = "http://demo.gonagoo.com/android/GonagooDriver_demo_v1.1.2.apk";
          $sms_app = $this->lang->line('download_driver_app') . $link;
          $this->api->sendSMS((int)$country_code.trim($mobile1), $sms_app);
          
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_add_success'), "cd_id" => $cd_id ];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_add_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_email_exists')]; }
      echo json_encode($this->response);
    }
  }

  public function active_driver()
  {
    if(empty($this->errors)){
      $cd_id = $this->input->post('cd_id', TRUE);
      if( $this->api->activate_driver( (int) $cd_id) ) {

        $cust = $this->cust;
        $cust_id = (int) $cust['cust_id'];
        $driver_count = (int)$cust['driver_count']; $driver_count += 1;  
        $this->api->update_driver_count($driver_count, $cust_id);

        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_active_success')];   
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_active_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function inactive_driver()
  {
    if(empty($this->errors)){
      $cd_id = $this->input->post('cd_id', TRUE);
      if( $this->api->inactivate_driver( (int) $cd_id) ) {
        $cust = $this->cust;
        $cust_id = (int) $cust['cust_id'];
        $driver_count = (int)$cust['driver_count'];
        if($driver_count > 0 ) {  $driver_count -= 1;  $this->api->update_driver_count($driver_count, $cust_id); }

        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_inactive_success')];   
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_inactive_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function inactive_driver_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $users_inactive_driver_list = $this->api->get_customer_drivers_inactive($cust_id);
      if(empty($users_inactive_driver_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_list_failed'), 'users_inactive_driver_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_list_success'), 'users_inactive_driver_list' => $users_inactive_driver_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function driver_update()
  {
    if(empty($this->errors)) {  
      $cd_id = $this->input->post('cd_id', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE);
      $first_name = $this->input->post('first_name', TRUE);
      $last_name = $this->input->post('last_name', TRUE);
      $email = $this->input->post('email', TRUE); 
      $country_id = $this->input->post('country_id', TRUE); 
      $country_code = $this->input->post('country_code', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $mobile2 = $this->input->post('mobile2', TRUE);
      $cat_ids = $this->input->post('cat_ids', TRUE);
      $available = $this->input->post('available', TRUE);
      $country_name = $this->api->get_country_name_by_id((int)$country_id);
      $today = date('Y-m-d H:i:s');

      $driver_update_data = array(
        "cust_id" => trim($cust_id),
        "first_name" => trim($first_name),
        "last_name" => trim($last_name),
        "email" => trim($email),
        "country_id" => (int)$country_id,
        "country_code" => (int)$country_code,
        "country_name" => trim($country_name),
        "mobile1" => trim($mobile1),
        "mobile2" => trim($mobile2),
        "cat_ids" => trim($cat_ids),
        "available" => trim($available),
        "mod_datetime" => $today
      );

      if(!$this->api->is_driver_exists_excluding(trim($email), $cd_id)) { 
        if( $this->api->update_driver($driver_update_data, $cd_id) ){
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_update_success')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_update_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_email_exists')]; }
      echo json_encode($this->response);
    }
  }

  public function driver_update_avatar()
  {
    if(empty($this->errors)) {
      $cd_id = $this->input->post('cd_id', TRUE);
      if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('profile_image')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];  
          $this->api->delete_old_avatar_driver($cd_id);
        }      

        if( $this->api->update_avatar_driver($avatar_url, $cd_id)) {
          $driver_details = $this->api->get_driver_datails($cd_id);
          $this->response = ['response' => 'success','message'=> $this->lang->line('driver_update_avatar_success'), "driver_details" => $driver_details];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_update_avatar_failed')]; }
      }
      echo json_encode($this->response);
    }
  }

  public function service_area_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $service_area_list = $this->api->get_customer_service_area($cust_id);
      if(empty($service_area_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('service_area_list_failed'), 'service_area_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('service_area_list_success'), 'service_area_list' => $service_area_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function service_area_add()
  {
    if(empty($this->errors)) {  
      $cust_id = $this->input->post('cust_id', TRUE);
      $type = $this->input->post('type', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE); 
      $city_id = $this->input->post('city_id', TRUE);
      $area_name = $this->input->post('area_name', TRUE);
      $zip = $this->input->post('zip', TRUE);
      $today = date('Y-m-d H:i:s');

      $service_insert_data = array(
        "cust_id" => trim($cust_id),
        "type" => trim($type),
        "country_id" => trim($country_id),
        "state_id" => trim($state_id),
        "city_id" => trim($city_id),
        "area_name" => trim($area_name),
        "zip" => trim($zip),
        "cre_datetime" => $today
      );

      if( $sa_id = $this->api->register_service_area($service_insert_data) ) {          
        $this->response = ['response' => 'success', 'message' => $this->lang->line('service_area_add_success'), "sa_id" => $sa_id ];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('service_area_add_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function service_area_delete()
  {
    if(empty($this->errors)) {
      $sa_id = $this->input->post('sa_id', TRUE);
      if( $this->api->delete_service_area($sa_id)) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('driver_delete_avatar_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_delete_avatar_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_carrier_type()
  {
    $carrier_type = $this->api->get_carrier_type();
    if(empty($carrier_type)) {
      $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'carrier_type' => array()]; 
    } else {
      $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'carrier_type' => $carrier_type]; 
    }
    echo json_encode($this->response);
  }

  public function register_deliverer()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        $firstname = $this->input->post('firstname', TRUE);
        $lastname = $this->input->post('lastname', TRUE);
        $email_id = $this->input->post('email_id', TRUE);
        $contact_no = $this->input->post('contact_no', TRUE);
        $carrier_type = $this->input->post('carrier_type', TRUE);
        $address = $this->input->post('address', TRUE);
        $country_id = $this->input->post('country_id', TRUE);
        $state_id = $this->input->post('state_id', TRUE);
        $city_id = $this->input->post('city_id', TRUE);
        $zipcode = $this->input->post('zipcode', TRUE);
        $promocode = $this->input->post('promocode', TRUE);
        $company_name = $this->input->post('company_name', TRUE);
        $introduction = $this->input->post('introduction', TRUE);
        $latitude = $this->input->post('latitude', TRUE);
        $longitude = $this->input->post('longitude', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $service_list = $this->input->post('service_list', TRUE);
        $today = date("Y-m-d H:i:s");

        $insert_data = array(
          "firstname" => trim($firstname),
          "lastname" => trim($lastname),
          "email_id" => trim($email_id),
          "contact_no" => trim($contact_no),
          "address" => trim($address),
          "zipcode" => trim($zipcode),
          "promocode" => trim($promocode),
          "carrier_type" => trim(strtolower($carrier_type)),
          "country_id" => (int)($country_id),
          "state_id" => (int)($state_id),
          "city_id" => (int)($city_id),
          "cust_id" => (int)($cust_id),
          "cre_datetime" => trim($today),
          "company_name" => trim($company_name),
          "introduction" => trim($introduction),
          "latitude" => trim($latitude),
          "longitude" => trim($longitude),
          "gender" => trim($gender),
          "service_list" => trim($service_list),
        );

        if(!$this->api->is_deliverer_email_exists($email_id)){                  
          if( $this->api->register_deliverer($insert_data)){
            // make customer as a deliverer, update flag
            $this->api->make_consumer_deliverer($cust_id);
            // nulled scrow master registration 
            //$scrow_id = $this->api->register_nulled_scrow_master($cust_id);
            //$this->api->update_scrow_id($scrow_id, $cust_id);
            $profile = $this->api->get_deliverer_profile($cust_id);
            $this->response = ['response' => 'success','message'=> $this->lang->line('register_success'), "deliverer_profile" => $profile];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('register_failed').'...']; }
          $this->api->update_last_login($cust_id);
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_exists').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; } 
      echo json_encode($this->response);
    }
  }

  public function update_deliverer()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        $firstname = $this->input->post('firstname', TRUE);
        $lastname = $this->input->post('lastname', TRUE);
        $email_id = $this->input->post('email_id', TRUE);
        $contact_no = $this->input->post('contact_no', TRUE);
        $carrier_type = $this->input->post('carrier_type', TRUE);
        $address = $this->input->post('address', TRUE);
        $country_id = $this->input->post('country_id', TRUE);
        $state_id = $this->input->post('state_id', TRUE);
        $city_id = $this->input->post('city_id', TRUE);
        $zipcode = $this->input->post('zipcode', TRUE);
        $promocode = $this->input->post('promocode', TRUE);
        $company_name = $this->input->post('company_name', TRUE);
        $introduction = $this->input->post('introduction', TRUE);
        $latitude = $this->input->post('latitude', TRUE);
        $longitude = $this->input->post('longitude', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $service_list = $this->input->post('service_list', TRUE);
        $today = date("Y-m-d H:i:s");

        $update_data = array(
          "firstname" => trim($firstname),
          "lastname" => trim($lastname),
          "email_id" => trim($email_id),
          "contact_no" => trim($contact_no),
          "address" => trim($address),
          "zipcode" => trim($zipcode),
          "promocode" => trim($promocode),
          "carrier_type" => trim(strtolower($carrier_type)),
          "country_id" => (int)($country_id),
          "state_id" => (int)($state_id),
          "city_id" => (int)($city_id),
          "company_name" => trim($company_name),
          "introduction" => trim($introduction),
          "latitude" => trim($latitude),
          "longitude" => trim($longitude),
          "gender" => trim($gender),
          "service_list" => trim($service_list),
        );

        if(!$this->api->is_deliverer_email_exists($email_id, $cust_id)){
          if($this->api->update_deliverer($update_data, $cust_id)){
            $profile = $this->api->get_deliverer_profile($cust_id);
            $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('email_exists').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function update_bank_info()
  { 
    $bank_short_code = $this->input->post('bank_short_code', TRUE);
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){

        $shipping_mode = $this->input->post('shipping_mode', TRUE);
        $iban = $this->input->post('iban', TRUE);
        $bank_name = $this->input->post('bank_name', TRUE);
        $bank_address = $this->input->post('bank_address', TRUE);
        $bank_title = $this->input->post('bank_title', TRUE);
        $mobile_money_acc1 = $this->input->post('money_acc1', TRUE);
        $mobile_money_acc2 = $this->input->post('money_acc2', TRUE);
        // $contact_code = $this->input->post('contact_code', TRUE);
        // $followup_code = $this->input->post('followup_code', TRUE);

        $update_data = array(
          "shipping_mode" => trim($shipping_mode),
          "iban" => trim($iban),
          "bank_name" => trim($bank_name),
          "bank_address" => trim($bank_address),
          "bank_title" => trim($bank_title),
          "bank_short_code" => trim($bank_short_code),
          "mobile_money_acc1" => trim($mobile_money_acc1),
          "mobile_money_acc2" => trim($mobile_money_acc2),
          "commercial_contact_code" => "NULL",
          "commercial_followup_code" => "NULL",
        );

        if($this->api->update_business_info($update_data, $cust_id)) {
          $profile = $this->api->get_deliverer_profile($cust_id);
            $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function update_business_avatar()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        $profile = $this->api->get_business_datails($cust_id);

        if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('profile_image')) {   
            $uploads    = $this->upload->data();  
            $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
            if( $profile['avatar_url'] != "NULL" ){ unlink($profile['avatar_url']); }
          }      

          if( $this->api->update_business_avatar($avatar_url, $cust_id)){
            $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        } 
        // update user last login
        $this->api->update_last_login($cust_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function update_business_cover()
  {
    //echo json_encode($_FILES); die();
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE);
      //echo json_encode($_POST); die();
      if($this->api->is_user_active_or_exists($cust_id)){
        $profile = $this->api->get_business_datails($cust_id);
        if( !empty($_FILES["cover_image"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/cover-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('cover_image')) {   
            $uploads    = $this->upload->data();  
            $cover_url =  $config['upload_path'].$uploads["file_name"];  
          if( $profile['cover_url'] != "NULL" ){  unlink($profile['cover_url']);  }
            //$this->api->delete_old_business_cover($cust_id);
          }      

          if( $this->api->update_business_cover($cover_url, $cust_id)){
            $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        // update user last login
        $this->api->update_last_login($cust_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function upload_business_photo()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){
      if( !empty($_FILES["business_photo"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/business-photos/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('business_photo')) {   
          $uploads    = $this->upload->data();  
          $photo_url =  $config['upload_path'].$uploads["file_name"];  
        }      

        if( $photo_id = $this->api->add_business_photo($photo_url, $cust_id)){          
          $this->response=['response'=>'success','message'=> $this->lang->line('add_success'),"photo_id"=>$photo_id,"photo_url"=>trim($photo_url)];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found').'...']; }
      // update user last login
      $this->api->update_last_login($cust_id);
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function delete_business_photo()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){
      $photo_id = $this->input->post('photo_id', TRUE);
      if($this->api->delete_business_photo($photo_id)){
        $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed').'...']; }
      
      // update user last login
      $this->api->update_last_login($cust_id);
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function get_business_photos()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){
      if($photos = $this->api->get_business_photos($cust_id)){
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_business_photos" => $photos];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed').'...']; }
      
      // update user last login
      $this->api->update_last_login($cust_id);
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  } 

  public function add_transport_details()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        $transport_mode = $this->input->post('transport_mode', TRUE);
        $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
        $registration_no = $this->input->post('registration_no', TRUE);
        $max_volume = $this->input->post('max_volume', TRUE);
        $max_weight = $this->input->post('max_weight', TRUE);
        $mark = $this->input->post('mark', TRUE);
        $model = $this->input->post('model', TRUE);
        $symbolic_name = $this->input->post('symbolic_name', TRUE);
        $unit_id = $this->input->post('unit_id', TRUE);

        $insert_data = array(
          "cust_id" => (int)$cust_id,
          "transport_mode" => trim($transport_mode),
          "vehical_type_id" => (int)$vehical_type_id,
          "registration_no" => trim($registration_no),
          "max_volume" => trim($max_volume),
          "max_weight" => trim($max_weight),
          "mark" => trim($mark),
          "model" => trim($model),
          "symbolic_name" => trim($symbolic_name),
          "unit_id" => (int)$unit_id,
        );

        if($transport_id = $this->api->add_transport_details($insert_data)) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('add_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    } 
  }

  public function update_transport_details()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        $transport_id = $this->input->post('transport_id', TRUE); $transport_id = (int) $transport_id;
        $transport_mode = $this->input->post('transport_mode', TRUE);
        $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
        $registration_no = $this->input->post('registration_no', TRUE);
        $max_volume = $this->input->post('max_volume', TRUE);
        $max_weight = $this->input->post('max_weight', TRUE);
        $mark = $this->input->post('mark', TRUE);
        $model = $this->input->post('model', TRUE);
        $symbolic_name = $this->input->post('symbolic_name', TRUE);
        $unit_id = $this->input->post('unit_id', TRUE);

        $update_data = array(
          "transport_mode" => trim($transport_mode),
          "vehical_type_id" => (int)$vehical_type_id,
          "registration_no" => trim($registration_no),
          "max_volume" => trim($max_volume),
          "max_weight" => trim($max_weight),
          "mark" => trim($mark),
          "model" => trim($model),
          "symbolic_name" => trim($symbolic_name),
          "unit_id" => (int)$unit_id,
        );

        if($transport_id = $this->api->update_transport_details($update_data, $cust_id, $transport_id)) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function delete_transport_details()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        $transport_id = $this->input->post('transport_id', TRUE); $transport_id = (int) $transport_id;
        if( $this->api->delete_transport_details($transport_id)) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function consumer_transport_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        $cust_id = (int) $cust_id;
        if( $list = $this->api->get_consumers_transport_list($cust_id)) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_transport_list" => $list];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function transport_vehical_master()
  {
    if( $list = $this->api->get_transport_master()) {
      $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'),"vehical_master" => $list];
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed').'...']; }
    echo json_encode($this->response);
  }

  public function unit_master()
  {
    if( $list = $this->api->get_unit_master(true)) {
      $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'),"unit_master" => $list];
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed').'...']; }
    echo json_encode($this->response);
  }

  public function get_profile_details()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){
      $cust_id = (int) $cust_id;
      
      $detail['personal'] = $this->api->get_consumer_datails($cust_id);
      if($skills = $this->api->get_customer_skills($cust_id) ) { $detail['skills'] = $skills; }
      if($experience = $this->api->get_customer_experience($cust_id) ) {  $detail['experience'] = $experience; }
      if($education = $this->api->get_customer_education($cust_id) ) {  $detail['education'] = $education; }
      if($service_area = $this->api->get_customer_service_area($cust_id) ) {  $detail['service_area'] = $service_area; }
      if($drivers = $this->api->get_customer_drivers($cust_id) ) {  $detail['drivers'] = $drivers; }
      if($portfolio = $this->api->get_customers_portfolio($cust_id) ) {  $detail['portfolio'] = $portfolio; }
      if($deliverer_profile = $this->api->get_deliverer_profile($cust_id) ) {  $detail['deliverer_profile'] = $deliverer_profile; }
      if($business_photos = $this->api->get_business_photos($cust_id) ) {  $detail['business_photos'] = $business_photos; }
      if($documents = $this->api->get_documents($cust_id) ) {  $detail['documents'] = $documents; }
      if($payment_details = $this->api->get_payment_details($cust_id) ) {  $detail['payment_details'] = $payment_details; }
      if($user_transport_list = $this->api->get_consumers_transport_list($cust_id) ) {  $detail['user_transport_list'] = $user_transport_list; }
      if($delivere_document_list = $this->api->get_deliverer_documents($cust_id) ) {  $detail['delivere_document_list'] = $delivere_document_list; }

      $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'),"profile_details" => $detail];     
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function get_customers_addressbook()
  {   //var_dump($_POST); die(); 
      if(empty($this->errors)){
        $country_id = $this->input->post('country_id', TRUE);
        $state_id = $this->input->post('state_id', TRUE);
        $city_id = $this->input->post('city_id', TRUE);
        
        $filter = array('country_id' => $country_id, 'state_id' => $state_id, 'city_id' => $city_id);
        $cust_id = $this->input->post('cust_id', TRUE);
        if($this->api->is_user_active_or_exists($cust_id)) {
            $cust_id = (int) $cust_id;
            //var_dump($cust_id); die(); 
            if(! $book = $this->api->get_consumers_addressbook($cust_id, $filter)) { 
              //var_dump($book); die(); 
              $this->response = ['response' => 'failed', 'message'=> $this->lang->line('get_list_failed')]; 
            } else {
              //var_dump($book); die();
              //echo json_encode($this->db->last_query()); die();
              for ($i=0; $i < sizeof($book); $i++) { 
                $country_name = $this->api->get_country_name_by_id($book[$i]['country_id']);
                $state_name = $this->api->get_state_name_by_id($book[$i]['state_id']);
                $city_name = $this->api->get_city_name_by_id($book[$i]['city_id']);
                $book[$i] += ["country_name" => $country_name, "state_name" => $state_name, "city_name" => $city_name];
              }
              //var_dump($book); die();
              $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_addressbook" => $book]; 
            }
        } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
        echo json_encode($this->response);
      }
  }

  public function add_address_in_addressbook()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){
      $cust_id = (int) $cust_id;
      
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $mobile_no = $this->input->post('mobile_no', TRUE);
      $street_name = $this->input->post('street_name', TRUE);
      $address_line1 = $this->input->post('address_line1', TRUE);
      $address_line2 = $this->input->post('address_line2', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      $city_id = $this->input->post('city_id', TRUE);
      $zipcode = $this->input->post('zipcode', TRUE);
      $deliver_instruction = $this->input->post('deliver_instruction', TRUE);
      $pickup_instruction = $this->input->post('pickup_instruction', TRUE);
      $addr_type = $this->input->post('addr_type', TRUE);
      $comapny_name = $this->input->post('comapny_name', TRUE);
      $email = $this->input->post('email', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      $today = date("Y-m-d H:i:s");

      $insert_data = array(
        "cust_id" => $cust_id,
        "firstname" => $firstname,
        "lastname" => $lastname,
        "mobile_no" => $mobile_no,
        "street_name" => $street_name,
        "addr_line1" => $address_line1,
        "addr_line2" => $address_line2,
        "country_id" => $country_id,
        "state_id" => $state_id,
        "city_id" => $city_id,
        "zipcode" => $zipcode,
        "deliver_instructions" => $deliver_instruction,
        "pickup_instructions" => $pickup_instruction,
        "addr_type" => $addr_type,
        "comapny_name" => $comapny_name,
        "email" => $email,
        "latitude" => $latitude,
        "longitude" => $longitude,
        "image_url" => "NULL",        
        "cre_datetime" => $today
      );

      if( $addr_id = $this->api->register_new_address($insert_data) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('register_success'), 'addr_id' => $addr_id];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('register_failed').'...']; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function update_address_in_addressbook()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){
      $cust_id = (int) $cust_id;
      $addr_id = $this->input->post('addr_id', TRUE); $addr_id = (int) $addr_id;
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $mobile_no = $this->input->post('mobile_no', TRUE);
      $street_name = $this->input->post('street_name', TRUE);
      $address_line1 = $this->input->post('address_line1', TRUE);
      $address_line2 = $this->input->post('address_line2', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      $city_id = $this->input->post('city_id', TRUE);
      $zipcode = $this->input->post('zipcode', TRUE);
      $deliver_instruction = $this->input->post('deliver_instruction', TRUE);
      $pickup_instruction = $this->input->post('pickup_instruction', TRUE);
      $addr_type = $this->input->post('addr_type', TRUE);
      $comapny_name = $this->input->post('comapny_name', TRUE);
      $email = $this->input->post('email', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);

      $update_data = array(
        "firstname" => $firstname,
        "lastname" => $lastname,
        "mobile_no" => $mobile_no,
        "street_name" => $street_name,
        "addr_line1" => $address_line1,
        "addr_line2" => $address_line2,
        "country_id" => $country_id,
        "state_id" => $state_id,
        "city_id" => $city_id,
        "zipcode" => $zipcode,
        "deliver_instructions" => $deliver_instruction,
        "pickup_instructions" => $pickup_instruction,
        "addr_type" => $addr_type,
        "comapny_name" => $comapny_name,
        "email" => $email,
        "latitude" => $latitude,
        "longitude" => $longitude,
      );

      if( $this->api->update_existing_address($update_data, $addr_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function upload_address_photo()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    if($this->api->is_user_active_or_exists($cust_id)){
      $cust_id = (int) $cust_id;
      $addr_id = $this->input->post('addr_id', TRUE);   $addr_id = (int) $addr_id;
      if( !empty($_FILES["address_photo"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/address-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('address_photo')) {   
          $uploads    = $this->upload->data();  
          $image_url =  $config['upload_path'].$uploads["file_name"];  
          $this->api->delete_old_address_image($addr_id);
        }
        if( $this->api->update_address_image($image_url, $addr_id)){
          $this->response = ['response' => 'success','message'=>$this->lang->line('update_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('upload_image').'...']; }

    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function delete_address_from_addressbook()
  {
    $cust_id = $this->input->post('cust_id', TRUE);
    $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      $addr_id = $this->input->post('addr_id', TRUE);   $addr_id = (int) $addr_id;
      if( $this->api->delete_address_from_addressbook($addr_id)){
        $this->response = ['response' => 'success','message'=>$this->lang->line('update_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);
  }

  public function get_advance_payment()
  {
    $service_name = $this->input->post('service_name', TRUE);  $service_name = strtolower(trim($service_name));
    $country_id = $this->input->post('country_id', TRUE);  $country_id = (int) $country_id;
    if( $percent = $this->api->get_advance_payment($service_name, $country_id)){
      $this->response = ['response' => 'success','message'=>$this->lang->line('get_value_success'), "percentage" => $percent];
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_value_failed').'...']; }
    echo json_encode($this->response);  
  }

  public function get_standard_dimensions()
  {
    if(! $list = $this->api->get_standard_dimension_masters()) { $list = "NULL"; }
    $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'),"standard_dimensions" => $list];
    echo json_encode($this->response);  
  }
  
  public function calculate_order_price()
  {   
    $cust_id = $this->input->post('cust_id', TRUE);   $cust_id = (int) $cust_id;
    $laundry = $this->input->post('laundry');
    
    if($this->api->is_user_active_or_exists($cust_id)){
      
      $category_id = $this->input->post('category_id');
      $delivery_datetime = $this->input->post('delivery_datetime');
      $pickup_datetime = $this->input->post('pickup_datetime');
      $transport_type = $this->input->post('transport_type');
      $service_area_type = $this->input->post('service_area_type');
       
      $quantity = json_decode($this->input->post('total_quantity'),TRUE);      
      $total_quantity = 0;
      if(!empty($quantity)){ foreach ($quantity as $q) {  $total_quantity += $q; } }
      $width = json_decode($this->input->post('width'),TRUE);      
      $total_width = 0; 
      if(!empty($width)){ foreach ($width as $w) {  $total_width += $w; } }
      $height = json_decode($this->input->post('height'),TRUE);
      $total_height = 0; 
      if(!empty($height)){ foreach ($height as $h) {  $total_height += $h; } }
      $length = json_decode($this->input->post('length'),TRUE);
      $total_length = 0; 
      if(!empty($length)){ foreach ($length as $l) {  $total_length += $l; } }    
      
      $weight = json_decode($this->input->post('total_weight'),TRUE);
      $total_weight = 0;
      $unit_id = json_decode($this->input->post('unit_id'),TRUE);
      if(!empty($unit_id)){       
        $wu = array_map(function($k,$v){ return array($k,$v);},$unit_id,$weight);      
        for($i=0; $i < count($wu); $i++) { 
          $unit = $this->transport->get_unit($wu[$i][0]);
          if(strtolower($unit['shortname']) == 'kg') {  $total_weight += $wu[$i][1];  } 
          else{ $total_weight += ($w * $unit['conversion_rate_in_kg']); }
        } 
      }    
      
      $insurance_check = $this->input->post('insurance_check');
      $package_value = $this->input->post('package_value');
      $handling_by = $this->input->post('handling_by');
      $dedicated_vehicle = $this->input->post('dedicated_vehicle');
      $distance_in_km = 0;

      $from_country_id = $this->input->post('from_country_id');
      $from_state_id = $this->input->post('from_state_id');
      $from_city_id = $this->input->post('from_city_id');
      $from_latitude = $this->input->post('from_latitude');
      $from_longitude = $this->input->post('from_longitude');

      $to_country_id = $this->input->post('to_country_id');   
      $to_state_id = $this->input->post('to_state_id');   
      $to_city_id = $this->input->post('to_city_id');   
      $to_latitude = $this->input->post('to_latitude');   
      $to_longitude = $this->input->post('to_longitude');   
   
      //  Newly added keys
      $old_new_goods = $this->input->post('old_new_goods', TRUE); 
      if(empty($old_new_goods)){ $old_new_goods = "NULL"; }
      $custom_clearance_by = $this->input->post('custom_clearance_by', TRUE);
      if(empty($custom_clearance_by)){ $custom_clearance_by = "NULL"; }
      $custom_package_value = $this->input->post('custom_package_value', TRUE);
      if(empty($custom_package_value)){ $custom_package_value = 0; }
      $loading_time = $this->input->post('loading_time');

      $calculate_price =  array (
        "cust_id" => $cust_id,
        "category_id" => $category_id,
        
        "delivery_datetime" => date_format(date_create($delivery_datetime),"m/d/Y H:i"),
        "pickup_datetime" => date_format(date_create($pickup_datetime),"m/d/Y H:i"),
        
        "total_quantity" => $total_quantity,
        
        "width" => $total_width,
        "height" => $total_height,
        "length" => $total_length,
        
        "total_weight" => $total_weight,
        "unit_id" => 1,
        
        "from_country_id" => $from_country_id,
        "from_state_id" => $from_state_id,
        "from_city_id" => $from_city_id,
        "from_latitude" => $from_latitude,
        "from_longitude" => $from_longitude,

        "to_country_id" => $to_country_id,
        "to_state_id" => $to_state_id,
        "to_city_id" => $to_city_id,
        "to_latitude" => $to_latitude,
        "to_longitude" => $to_longitude,

        "service_area_type" => $service_area_type,
        "transport_type" => $transport_type,
        "handling_by" => $handling_by,
        "dedicated_vehicle" => $dedicated_vehicle,
        "package_value" => $package_value,

        "custom_clearance_by" => $custom_clearance_by,
        "old_new_goods" => $old_new_goods,
        "custom_package_value" => $custom_package_value,

        "loading_time" => $loading_time,
        "laundry" => (isset($laundry) && $laundry == 1)?1:0,
      );
      // echo json_encode($calculate_price); die();
      if($result = $this->api->calculate_order_price($calculate_price)){
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_value_success'), "result" => $result ]; 
      } else { $this->response = ['response' => 'success','message'=> $this->lang->line('get_value_failed')]; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }
  
 
  public function register_new_courier_orders()
  { 
    //echo json_encode($_POST); die();
    $from_relay_id_type = $this->input->post('from_relay_id_type', TRUE); 
    if(!isset($from_relay_id_type)) $from_relay_id_type = 'NULL';
    $to_relay_id_type = $this->input->post('to_relay_id_type', TRUE);
    if(!isset($to_relay_id_type)) $to_relay_id_type = 'NULL';

    $payment_mode = $this->input->post('payment_mode', TRUE);   // cod / payment / bank
    if($payment_mode == 'payment') $payment_mode = 'NULL';
    $cod_payment_type = $this->input->post('cod_payment_type', TRUE);
    if(!isset($cod_payment_type)) $cod_payment_type = 'NULL';
    
    $to_address_email = $this->input->post('to_address_email', TRUE);
    $from_address_email = $this->input->post('from_address_email', TRUE);
    $visible_by = $this->input->post('visible_by', TRUE);
    $is_laundry = $this->input->post('is_laundry', TRUE);
    $laundry_is_drop = $this->input->post('laundry_is_drop', TRUE);
    if(!isset($laundry_is_drop)) $laundry_is_drop = 0;
    $pending_service_charges = $this->input->post('pending_service_charges', TRUE);
    if(!isset($pending_service_charges)) $pending_service_charges = 0;
    $service_type = $this->input->post('service_type', TRUE);
    if(!isset($service_type)) $service_type = 'NULL';
    $laundry = $this->input->post('laundry', TRUE); 
    $laundry = (isset($laundry) && $laundry == 1)?1:0;
    
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE);   $cust_id = (int) $cust_id;  
      if($this->api->is_user_active_or_exists($cust_id)){
        $from_address = $this->input->post('from_address', TRUE); 
        $from_address_name = $this->input->post('from_address_name', TRUE); 
        $from_address_contact = $this->input->post('from_address_contact', TRUE); 
        $from_latitude = $this->input->post('from_latitude', TRUE); 
        $from_longitude = $this->input->post('from_longitude', TRUE); 
        $from_country_id = $this->input->post('from_country_id', TRUE); 
        $from_state_id = $this->input->post('from_state_id', TRUE); 
        $from_city_id = $this->input->post('from_city_id', TRUE); 
        $from_addr_type = $this->input->post('from_addr_type', TRUE); 
        $from_relay_id = $this->input->post('from_relay_id', TRUE); 
        $to_address = $this->input->post('to_address', TRUE); 
        $to_address_name = $this->input->post('to_address_name', TRUE); 
        $to_address_contact = $this->input->post('to_address_contact', TRUE);  
        $to_country_id = $this->input->post('to_country_id', TRUE); 
        $to_state_id = $this->input->post('to_state_id', TRUE); 
        $to_city_id = $this->input->post('to_city_id', TRUE); 
        $to_latitude = $this->input->post('to_latitude', TRUE); 
        $to_longitude = $this->input->post('to_longitude', TRUE); 
        $to_addr_type = $this->input->post('to_addr_type', TRUE); 
        $to_relay_id = $this->input->post('to_relay_id', TRUE); 
        $pickup_datetime = $this->input->post('pickup_datetime', TRUE); 
        $delivery_datetime = $this->input->post('delivery_datetime', TRUE); 
        $expiry_date = $this->input->post('expiry_date', TRUE); 
        $order_type = $this->input->post('order_type', TRUE);   // Shipping_Only, Purchase_and_Deliver
        $content = json_decode($this->input->post('order_contents', TRUE),TRUE);
        if(empty($content)) $content = "NULL";
        $order_description = $this->input->post('order_description', TRUE);
        if(empty($order_description)) $order_description = "NULL";
        $deliver_instructions = $this->input->post('deliver_instructions', TRUE); 
        if(empty($deliver_instructions)) $deliver_instructions = "NULL";
        $pickup_instructions = $this->input->post('pickup_instructions', TRUE); 
        if(empty($pickup_instructions)) $pickup_instructions = "NULL";
        $service_area_type = $this->input->post('service_area_type', TRUE);   // local, national, international
        $transport_type = $this->input->post('transport_type', TRUE);         // earth, air, sea
        $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
        $dedicated_vehicle = $this->input->post('dedicated_vehicle', TRUE); 
        $with_driver = $this->input->post('with_driver', TRUE); 
        $insurance = $this->input->post('insurance', TRUE); 
        $package_value = $this->input->post('package_value', TRUE); 
        $handling_by = $this->input->post('handling_by', TRUE); 
        $from_address_info = $this->input->post('from_address_info', TRUE);
        $to_address_info = $this->input->post('to_address_info', TRUE);
        // $total_quantity = $this->input->post('total_quantity', TRUE);     
        // $dimension_id = $this->input->post('dimension_id', TRUE);     
        // $width = $this->input->post('width', TRUE);     
        // $height = $this->input->post('height', TRUE);     
        // $length = $this->input->post('length', TRUE);
        // $total_weight = $this->input->post('total_weight', TRUE);     
        // $unit_id = $this->input->post('unit_id', TRUE);     
        $dimension_id = json_decode($this->input->post('dimension_id'),TRUE);        
        $dangerous_goods = json_decode($this->input->post('dangerous_goods'),TRUE);        
        $quantity = json_decode($this->input->post('total_quantity'),TRUE);      
        $total_quantity = 0;
        if(!empty($quantity)){ foreach ($quantity as $q) {  $total_quantity += $q; } }
        $width = json_decode($this->input->post('width'),TRUE);      
        $total_width = 0; 
        if(!empty($width)){ foreach ($width as $w) {  $total_width += $w; } }
        $height = json_decode($this->input->post('height'),TRUE);
        $total_height = 0; 
        if(!empty($height)){ foreach ($height as $h) {  $total_height += $h; } }
        $length = json_decode($this->input->post('length'),TRUE);
        $total_length = 0; 
        if(!empty($length)){ foreach ($length as $l) {  $total_length += $l; } }    
        $weight = json_decode($this->input->post('total_weight'),TRUE);
        $total_weight = 0;
        $unit_id = json_decode($this->input->post('unit_id'),TRUE);
        if(!empty($unit_id)){       
          $wu = array_map(function($k,$v){ return array($k,$v);},$unit_id,$weight);      
          for($i=0; $i < count($wu); $i++) { 
            $unit = $this->transport->get_unit($wu[$i][0]);
            if(strtolower($unit['shortname']) == 'kg') {  $total_weight += $wu[$i][1];  } 
            else{ $total_weight += ($w * $unit['conversion_rate_in_kg']); }
          } 
        }

        $count = COUNT($quantity);
        $package = array();
        if($content != 'NULL') {
          for( $i=0; $i < $count; $i++){
            $package[] = array(
              "quantity" => $quantity[$i], 
              "dimension_id" => $dimension_id[$i], 
              "width" => $width[$i], 
              "height" => $height[$i], 
              "length" => $length[$i], 
              "weight" => $weight[$i], 
              "unit_id" => $unit_id[$i], 
              "content" => $content[$i], 
              "dangerous_goods_id" => $dangerous_goods[$i]
            );
          }
        }
        // echo json_encode($package); die();
        $advance_payment = $this->input->post('advance_payment', TRUE);     
        $pickup_payment = $this->input->post('pickup_payment', TRUE);     
        $deliver_payment = $this->input->post('deliver_payment', TRUE);
        $ref_no = $this->input->post('ref_no', TRUE); 

        //For COD, Bank and Payment
        if($payment_mode == 'bank') {
          $dvncPay = 0;
          $pckupPay = 0;
          $dlvrPay = $advance_payment + $pickup_payment + $deliver_payment;
          $is_bank_payment = 1;
          $balance_amount = $dlvrPay;
        } else {
          $is_bank_payment = 0;
          if($payment_mode == 'cod' && $cod_payment_type == 'at_pickup') {
            $dvncPay = 0;
            $pckupPay = $advance_payment + $pickup_payment + $deliver_payment;
            $dlvrPay = 0;
          } else if($payment_mode == 'cod' && $cod_payment_type == 'at_deliver') {
            $dvncPay = 0;
            $pckupPay = 0;
            $dlvrPay = $advance_payment + $pickup_payment + $deliver_payment;
          } else {
            $dvncPay = $advance_payment;
            $pckupPay = $pickup_payment; 
            $dlvrPay = $deliver_payment;
          }
          $balance_amount = $advance_payment + $pickup_payment + $deliver_payment;
        }

        //  Newly added keys
        $need_tailgate = $this->input->post('need_tailgate', TRUE);
        $loading_time = $this->input->post('loading_time', TRUE);
        $category_id = $this->input->post('category_id', TRUE);
        $old_new_goods = $this->input->post('old_new_goods', TRUE);
        if(empty($old_new_goods)){ $old_new_goods = "NULL"; }
        $custom_clearance_by = $this->input->post('custom_clearance_by', TRUE);
        if(empty($custom_clearance_by)){ $custom_clearance_by = "NULL"; }
        $custom_package_value = $this->input->post('custom_package_value', TRUE);
        if(empty($custom_package_value)){ $custom_package_value = 0; }
        $cust = $this->api->get_consumer_datails($cust_id, true);  
        if($cust['firstname'] != "NULL"){ $cust_name = $cust['firstname']. ' '.$cust['lastname'] ; }
        else{ $cust_name = "NULL"; }
        
        $calculate_price =  array (
          "cust_id" => $cust_id,
          "category_id" => $category_id,
          "delivery_datetime" => $delivery_datetime,
          "pickup_datetime" => $pickup_datetime,
          "total_quantity" => $total_quantity,
          "width" => $total_width,
          "height" => $total_height,
          "length" => $total_length,
          "total_weight" => $total_weight,
          "unit_id" => 1,
          "from_country_id" => $from_country_id,
          "from_state_id" => $from_state_id,
          "from_city_id" => $from_city_id,
          "from_latitude" => $from_latitude,
          "from_longitude" => $from_longitude,
          "to_country_id" => $to_country_id,
          "to_state_id" => $to_state_id,
          "to_city_id" => $to_city_id,
          "to_latitude" => $to_latitude,
          "to_longitude" => $to_longitude,
          "service_area_type" => $service_area_type,
          "transport_type" => $transport_type,
          "handling_by" => $handling_by,
          "dedicated_vehicle" => $dedicated_vehicle,
          "package_value" => $package_value,
          "custom_clearance_by" => $custom_clearance_by,
          "old_new_goods" => $old_new_goods,
          "custom_package_value" => $custom_package_value,
          "loading_time" => $loading_time,
          "laundry" => $laundry,
        );
        // echo json_encode($calculate_price); die();
        $prices = $this->api->calculate_order_price($calculate_price);
        // echo json_encode($prices); die();
        $order_price = $prices['total_price']; 
        $standard_price = $prices['standard_price']; 
        $handling_fee = $prices['handling_fee']; 
        $insurance_fee = $prices['ins_fee']; 
        $urgent_fee = $prices['urgent_fee']; 
        $vehicle_fee = $prices['dedicated_vehicle_fee']; 
        $advance_percent = $prices['advance_percent']; 
        $distance_in_km = $prices['distance_in_km']; 
        $commission_percent = $prices['commission_percent']; 
        $currency_id = $prices['currency_id']; 
        $currency_sign = $prices['currency_sign']; 
        $currency_title = $prices['currency_title'];  

        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $order_price = (int)$order_price;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $standard_price = (int)$standard_price;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $handling_fee = (int)$handling_fee;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $insurance_fee = (int)$insurance_fee;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $urgent_fee = (int)$urgent_fee;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $vehicle_fee = (int)$vehicle_fee;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $dvncPay = (int)$dvncPay;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $pckupPay = (int)$pckupPay;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $dlvrPay = (int)$dlvrPay;
        if($currency_sign == 'XAF' || $currency_sign == 'XOF') $balance_amount = (int)$balance_amount;
        // New keys
        $old_goods_custom_commission_percent = $prices['old_goods_custom_commission_percent'];  
        $old_goods_custom_commission_fee = $prices['old_goods_custom_commission_fee'];  
        $new_goods_custom_commission_percent = $prices['new_goods_custom_commission_percent'];  
        $new_goods_custom_commission_fee = $prices['new_goods_custom_commission_fee'];
        if($order_type == "international" && ($old_new_goods == 1)){  
          $custom_clearance_percent = $new_goods_custom_commission_percent;
          $custom_clearance_fee = $new_goods_custom_commission_fee;
        } 
        else if($order_type == "international" && ($old_new_goods == 0)){
          $custom_clearance_percent = $old_goods_custom_commission_percent;
          $custom_clearance_fee = $old_goods_custom_commission_fee;
        } else{ $custom_clearance_percent = 0;  $custom_clearance_fee = 0;  }
        if( !empty($_FILES["order_picture"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/booking-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('order_picture')) {   
            $uploads    = $this->upload->data();  
            $order_picture_url =  $config['upload_path'].$uploads["file_name"];  
          } else { $order_picture_url = "NULL"; }
        } else { $order_picture_url = "NULL"; }

        //Gonagoo Commission
        $user_details = $this->api->get_user_details((int)$cust_id);
        //echo json_encode($user_details);die();
        if($user_details['is_dedicated'] == 1) {
          if($user_details['commission_type'] == 3) {
            $data = array(
              "cust_id" => (int)$cust_id,
              "rate_type" => 1,
              "category_id" => $category_id,
              "country_id" => $from_country_id,
            );
            if($user_gonagoo_commission_details = $this->api->get_user_gonagoo_commission_details($data)) {
              if($transport_type == 'earth' && $service_area_type == 'local') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['ELGCP'];
              } else if ($transport_type == 'earth' && $service_area_type == 'national') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['ENGCP'];
              } else if ($transport_type == 'earth' && $service_area_type == 'international') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['EIGCP'];
              } else if ($transport_type == 'sea' && $service_area_type == 'local') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['SLGCP'];
              } else if ($transport_type == 'sea' && $service_area_type == 'national') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['SNGCP'];
              } else if ($transport_type == 'sea' && $service_area_type == 'international') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['SIGCP'];
              } else if ($transport_type == 'air' && $service_area_type == 'local') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['ALGCP'];
              } else if ($transport_type == 'air' && $service_area_type == 'national') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['ANGCP'];
              } else if ($transport_type == 'air' && $service_area_type == 'international') {
                $commission_percent = 0;
                $commission_amount = $user_gonagoo_commission_details['AIGCP'];
              } else {
                $commission_percent = $commission_percent;
                $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
              }
            } else {
              $commission_percent = $commission_percent;
              $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
            }
          } else if($user_details['commission_type'] == 2) {
            $data = array(
              "cust_id" => (int)$cust_id,
              "rate_type" => 0,
              "category_id" => $category_id,
              "country_id" => $from_country_id,
            );
            if($user_gonagoo_commission_details = $this->api->get_user_gonagoo_commission_details($data)) {
              if($transport_type == 'earth' && $service_area_type == 'local') {
                $commission_percent = $user_gonagoo_commission_details['ELGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ELGCP'] / 100 ),2);
              } else if ($transport_type == 'earth' && $service_area_type == 'national') {
                $commission_percent = $user_gonagoo_commission_details['ENGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ENGCP'] / 100 ),2);
              } else if ($transport_type == 'earth' && $service_area_type == 'international') {
                $commission_percent = $user_gonagoo_commission_details['EIGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['EIGCP'] / 100 ),2);
              } else if ($transport_type == 'sea' && $service_area_type == 'local') {
                $commission_percent = $user_gonagoo_commission_details['SLGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SLGCP'] / 100 ),2);
              } else if ($transport_type == 'sea' && $service_area_type == 'national') {
                $commission_percent = $user_gonagoo_commission_details['SNGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SNGCP'] / 100 ),2);
              } else if ($transport_type == 'sea' && $service_area_type == 'international') {
                $commission_percent = $user_gonagoo_commission_details['SIGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SIGCP'] / 100 ),2);
              } else if ($transport_type == 'air' && $service_area_type == 'local') {
                $commission_percent = $user_gonagoo_commission_details['ALGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ALGCP'] / 100 ),2);
              } else if ($transport_type == 'air' && $service_area_type == 'national') {
                $commission_percent = $user_gonagoo_commission_details['ANGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ANGCP'] / 100 ),2);
              } else if ($transport_type == 'air' && $service_area_type == 'international') {
                $commission_percent = $user_gonagoo_commission_details['AIGCP'];
                $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['AIGCP'] / 100 ),2);
              } else {
                $commission_percent = $commission_percent;
                $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
              }
            } else {
              $commission_percent = $commission_percent;
              $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
            }
          } else {
            $commission_percent = $commission_percent;
            $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
          }
        } else {
          $commission_percent = $commission_percent;
          $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
        }

        if($currency_sign == 'XAF' || 'XOF') $commission_amount = (int)$commission_amount;

        $insert_data = array(
          "cust_id" => $cust_id,
          "from_address" => $from_address,
          "from_address_name" => $from_address_name,
          "from_address_contact" => $from_address_contact,
          "from_latitude" => $from_latitude,
          "from_longitude" => $from_longitude,
          "from_country_id" => $from_country_id,
          "from_state_id" => $from_state_id,
          "from_city_id" => $from_city_id,
          "from_addr_type" => (int)$from_addr_type,
          "from_relay_id" => (int)$from_relay_id,         
          "to_address" => $to_address,
          "to_address_name" => $to_address_name,
          "to_address_contact" => $to_address_contact,
          "to_latitude" => $to_latitude,
          "to_longitude" => $to_longitude,
          "to_country_id" => $to_country_id,
          "to_state_id" => $to_state_id,
          "to_city_id" => $to_city_id,                       
          "distance_in_km" => $distance_in_km,
          "pickup_datetime" => $pickup_datetime,
          "delivery_datetime" => $delivery_datetime,
          "vehical_type_id" => (int)$vehical_type_id,
          "order_type" => $order_type,      
          "order_picture_url" => $order_picture_url,
          "order_description" => nl2br($order_description),
          "order_price" => trim($order_price),
          "order_status" => "open",
          "advance_payment" => $dvncPay,
          "pickup_payment" => $pckupPay,
          "deliver_payment" => $dlvrPay,
          "cre_datetime" => date('Y-m-d H:i:s'),
          "cust_id" => (int) $cust_id,
          "currency_id" => $currency_id,
          "currency_sign" => $currency_sign,
          "currency_title" => $currency_title,
          "cust_name" => $cust_name,
          "insurance" => (int)$insurance,
          "package_value" => $package_value,
          "insurance_fee" => $insurance_fee,
          "handling_by" => (int)$handling_by,
          "dedicated_vehicle" => (int)$dedicated_vehicle,
          "with_driver" => (int)$with_driver,
          "ref_no" => $ref_no,
          "service_area_type" => $service_area_type,
          "expiry_date" => $expiry_date,
          "deliver_instructions" => $deliver_instructions,
          "pickup_instructions" => $pickup_instructions,
          "to_addr_type" => (int)$to_addr_type,
          "to_relay_id" => (int)$to_relay_id,
          "commission_percent" => $commission_percent,
          "commission_amount" => $commission_amount,
          "standard_price" => trim($standard_price),
          "urgent_fee" => trim($urgent_fee),
          "handling_fee" => trim($handling_fee),
          "vehicle_fee" => trim($vehicle_fee),      
          "order_contents" => "NULL",
          "total_quantity" => (int) $total_quantity,
          "width" => "NULL",
          "height" => "NULL",
          "length" => "NULL",
          "volume" => "NULL",
          "total_volume" => "NULL",
          "total_weight" => $total_weight,
          "unit_id" => 1,
          "deliverer_id" => 0,
          "driver_id" => 0,
          "dimension_id" => 0,
          "status_update_datetime" => "NULL",
          "accept_datetime" => "NULL",
          "assigned_datetime" => "NULL",
          "driver_status_update" => "NULL",
          "delivery_code" => "NULL",
          "delivered_datetime" => "NULL",
          "review_comment" => "NULL",
          "rating" => "NULL",
          "review_datetime" => "NULL",
          "status_updated_by" => "NULL",
          "delivery_notes" => "NULL",
          "invoice_url" => "NULL",
          "paid_amount" => "0",
          "balance_amount" => $balance_amount,
          "payment_method" => "NULL",
          "transaction_id" => "NULL",
          "payment_datetime" => "NULL",
          "cd_name" => "NULL",
          "assigned_accept_datetime" => "NULL",
          "deliverer_company_name" => "NULL",
          "deliverer_contact_name" => "NULL",
          "deliverer_name" => "NULL",
          "driver_code" => "NULL",
          "from_relay_status" => "NULL",
          "from_relay_status_datetime" => "NULL",
          "to_relay_status" => "NULL",
          "to_relay_status_datetime" => "NULL",
          "complete_paid" => "0",
          "deliverer_invoice_url" => "NULL",
          "src_relay_commission_invoice_url" => "NULL",
          "dest_relay_commission_invoice_url" => "NULL",
          "transport_type" => (!empty($transport_type)?strtolower($transport_type):"NULL"),
          "custom_clearance_by" => $custom_clearance_by,
          "custom_package_value" => $custom_package_value,
          "old_new_goods" => $old_new_goods,
          "custom_clearance_fee" => $custom_clearance_fee,
          "custom_clearance_percent" => $custom_clearance_percent,
          "need_tailgate" => $need_tailgate,
          "category_id" => $category_id,
          "package_count" => count($package),
          "from_address_info" => trim($from_address_info),
          "to_address_info" => trim($to_address_info),
          "payment_mode" => trim($payment_mode),
          "cod_payment_type" => trim($cod_payment_type),
          "pickup_code" => "NULL",
          "from_address_email" => trim($from_address_email),
          "to_address_email" => trim($to_address_email),
          "visible_by" => (int)trim($visible_by),
          "is_bank_payment" => (int)$is_bank_payment,
          "is_laundry" => (int)$is_laundry,
          "laundry_is_drop" => (int)$laundry_is_drop,
          "pending_service_charges" => $pending_service_charges,
          "service_type" => $service_type,
        );
        // echo json_encode($insert_data); die();
        if( $order_id = $this->api->register_new_courier_orders($insert_data) ) {
          foreach ($package as $p) {        
            $package_array = array(
              "cust_id" => $cust_id,
              "order_id" => $order_id,
              "quantity" => $p['quantity'],
              "dimension_id" => $p['dimension_id'],
              "width" => $p['width'],
              "height" => $p['height'],
              "length" => $p['length'],
              "total_weight" => $p['weight'],
              "unit_id" => $p['unit_id'],
              "contents" => $p['content'],
              "dangerous_goods_id" => $p['dangerous_goods_id'],
            );
            $this->transport->insert_package($package_array);
          }

          $this->api->insert_order_status($cust_id, $order_id, 'open');
            
          if($from_relay_id_type == 'new' && (int)$from_relay_id > 0) {
            $relay_array = array(
              "cust_id" => $cust_id,
              "relay_id" => (int)$from_relay_id,
            );
            $this->api->register_user_new_recent_relay($relay_array);
          }
          if($to_relay_id_type == 'new' && (int)$to_relay_id > 0) {
            $relay_array = array(
              "cust_id" => $cust_id,
              "relay_id" => (int)$to_relay_id,
            );
            $this->api->register_user_new_recent_relay($relay_array);
          }    

          //promocode section--------------------------------------------
            if(isset($_POST['promo_code'])){
              if($_POST['promo_code'] != "NULL"){
              $details = $this->api->get_order_detail($order_id);
              $promo_code_name = $this->input->post("promo_code");
              $promocode = $this->api->get_promocode_by_name($promo_code_name); 
              //code start here
                $discount_amount = round(($details['order_price']/100)*$promocode['discount_percent'] , 2); 
                if($details['currency_sign'] == 'XAF'){
                  $discount_amount = round($discount_amount , 0);
                }
                $order_price =  round($details['order_price']-$discount_amount ,2);
                if($details['currency_sign'] == 'XAF'){
                  $order_price = round($order_price , 0);
                }
                $advance_payment = round(($details['advance_payment']/100)*$promocode['discount_percent'] ,2);
                if($details['currency_sign'] == 'XAF'){
                  $advance_payment = round($advance_payment , 0);
                }
                $pickup_payment = round( ($details['pickup_payment']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $pickup_payment = round($pickup_payment , 0);
                }
                $deliver_payment = round( ($details['deliver_payment']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $deliver_payment = round($deliver_payment , 0);
                }
                $insurance_fee = round( ($details['insurance_fee']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $insurance_fee = round($insurance_fee , 0);
                }
                $commission_amount = round( ($details['commission_amount']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $commission_amount = round($commission_amount , 0);
                }
    
                $standard_price = round( ($details['standard_price']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $standard_price = round($standard_price , 0);
                }
                $urgent_fee = round( ($details['urgent_fee']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $urgent_fee = round($urgent_fee , 0);
                }
    
                $handling_fee = round( ($details['handling_fee']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $handling_fee = round($handling_fee , 0);
                }
                $vehicle_fee = round( ($details['vehicle_fee']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $vehicle_fee = round($vehicle_fee , 0);
                }
    
                $custom_clearance_fee = round( ($details['custom_clearance_fee']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $custom_clearance_fee = round($custom_clearance_fee , 0);
                }
    
                $loading_unloading_charges = round( ($details['loading_unloading_charges']/100)*$promocode['discount_percent'],2);
                if($details['currency_sign'] == 'XAF'){
                  $loading_unloading_charges = round($loading_unloading_charges , 0);
                }
                $courier_order = array(
                  'promo_code' => $promocode['promo_code'],
                  'promo_code_id' => $promocode['promo_code_id'],
                  'promo_code_version' => $promocode['promo_code_version'],
                  'discount_amount' => $discount_amount,
                  'discount_percent' => $promocode['discount_percent'],
                  'actual_order_price' => $details['order_price'],
                  'order_price' => $order_price ,
                  //'advance_payment' => ($details['advance_payment']-$advance_payment),
                  //'pickup_payment' => ($details['pickup_payment']-$pickup_payment),
                  //'deliver_payment' => ($details['deliver_payment']-$deliver_payment),
                  'balance_amount' => $order_price,
                  'insurance_fee' => ($details['insurance_fee']-$insurance_fee),
                  'commission_amount' => ($details['commission_amount']-$commission_amount),
                  'standard_price' => ($details['standard_price']-$standard_price),
                  'urgent_fee' => ($details['urgent_fee']-$urgent_fee),
                  'handling_fee' =>  ($details['handling_fee']-$handling_fee),
                  'vehicle_fee' => ($details['vehicle_fee']-$vehicle_fee),
                  'custom_clearance_fee'=>($details['custom_clearance_fee']-$custom_clearance_fee),
                  'loading_unloading_charges'=>($details['loading_unloading_charges']-$loading_unloading_charges),
                  'promo_code_datetime'=> date('Y-m-d h:i:s'),
                );
                $this->api->update_review($courier_order,$details['order_id']);
                $used_promo_update = array(
                  'promo_code_id' => $promocode['promo_code_id'],
                  'promo_code_version' => $promocode['promo_code_version'],
                  'cust_id' => $cust_id,
                  'customer_email' => "",
                  'discount_percent' => $promocode['discount_percent'],
                  'discount_amount' => $discount_amount,
                  'discount_currency' => $details['currency_sign'],
                  'cre_datetime' => date('Y-m-d h:i:s'),
                  'service_id' => '7',
                  'booking_id' => $details['order_id'],
                );
                $this->api->register_user_used_prmocode($used_promo_update);
              //code end here
            }   
            }
            $details = $this->api->get_order_detail($order_id);
          //promocode section--------------------------------------------
          
          //Create Pickup Code
          if($order_id <= 99999) { $pickup_code = 'PC'.$cust_id.sprintf("%06s", $order_id); } else { $pickup_code = 'PC'.$cust_id.$order_id; }
          $insert_data = array(
              "pickup_code" => $pickup_code,
          );
          $this->api->update_courier_orders($insert_data, $order_id);
          
          $this->response = ['response' => 'success','message'=> $this->lang->line('order_success'), 'order_id' => $order_id , 'Booking_details' => $details];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_failed').'...']; }

      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }
   
  public function mark_as_cod()
  {
    $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
    $cod_payment_type = $this->input->post('cod_payment_type', TRUE);
    $order_details = $this->api->get_order_detail($order_id);
    if($cod_payment_type == 'at_pickup') {
      $update_data_order = array(
        "payment_mode" => 'cod',
        "cod_payment_type" => trim($cod_payment_type),
        "advance_payment" => 0,
        "pickup_payment" => $order_details['order_price'],
        "deliver_payment" => 0,
      );
    }

    if($cod_payment_type == 'at_deliver') {
      $update_data_order = array(
        "payment_mode" => 'cod',
        "cod_payment_type" => trim($cod_payment_type),
        "advance_payment" => 0,
        "pickup_payment" => 0,
        "deliver_payment" => $order_details['order_price'],
      );
    }
    //echo json_encode($update_data_order); die();
    if($this->api->update_payment_type_of_order($update_data_order, $order_id)) {
      $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
    } else { 
      $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; 
    }
    echo json_encode($this->response);  
  }

  public function get_favourite_deliverer_list()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      if( $list = $this->api->get_favourite_deliverers($cust_id) ) {
        //echo json_encode($list); die();
        for ($i=0; $i < sizeof($list) ; $i++) { 
          $details = $this->api->get_deliverer_details($list[$i]['deliverer_id']);
          $list[$i] += $details;
          $list[$i] += ["is_favorite" => "1"];
        }
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "list" => $list];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed').'...']; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function register_new_favourite_deliverer()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      $deliverer_id = $this->input->post('deliverer_id', TRUE);  $deliverer_id = (int) $deliverer_id;

      if( $this->api->register_new_favourite_deliverer($cust_id, $deliverer_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('register_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('register_failed').'...']; }

    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function remove_favourite_deliverer()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      $deliverer_id = $this->input->post('deliverer_id', TRUE);  $deliverer_id = (int) $deliverer_id;

      if( $this->api->remove_favourite_deliverer($cust_id, $deliverer_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('remove_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed').'...']; }

    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function get_documents()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      if( $docs = $this->api->get_documents($cust_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_documenets" => $docs];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function add_document()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      $title = $this->input->post('title', TRUE);
      $description = $this->input->post('description', TRUE);
      
      if( !empty($_FILES["attachement"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('attachement')) {   
          $uploads    = $this->upload->data();  
          $attachement_url =  $config['upload_path'].$uploads["file_name"];  
        }      

        $register_data = array(
          "cust_id" => $cust_id,
          "doc_title" => trim($title),
          "doc_desc" => nl2br($description),
          "attachement_url" => trim($attachement_url),
          "cre_datetime" => date("Y-m-d H:i:s")
        );
        if( $this->api->register_document($register_data) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('add_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found')]; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function update_document()
  { 
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id; 
    if($this->api->is_user_active_or_exists($cust_id)){
      $doc_id = $this->input->post('doc_id', TRUE); $doc_id = (int) $doc_id;
      $title = $this->input->post('title', TRUE);
      $description = $this->input->post('description', TRUE);
      
      if( !empty($_FILES["attachement"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('attachement')) {   
          $uploads    = $this->upload->data();  
          $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
          $update_data = array( "doc_title" => trim($title), "doc_desc" => nl2br($description), "attachement_url" => trim($attachement_url)); 
        } else{ $attachement_url = "NULL"; }
      } else { $attachement_url = "NULL"; $update_data = array( "doc_title" => trim($title), "doc_desc" => nl2br($description) ); }
      $old_doc_url = $this->api->get_old_document_url($cust_id, $doc_id);
      if( $this->api->update_document($doc_id, $update_data) ) { 
        if($attachement_url != "NULL"){ unlink(trim($old_doc_url)); }
        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function delete_document()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      $doc_id = $this->input->post('doc_id', TRUE);  $doc_id = (int) $doc_id;
      $old_doc_url = $this->api->get_old_document_url($cust_id, $doc_id);
      if( $this->api->delete_document($cust_id, $doc_id) ) {
         unlink(trim($old_doc_url));
        $this->response = ['response' => 'success','message'=> $this->lang->line('remove_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed').'...']; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function get_paymnet_details()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      if( $details = $this->api->get_payment_details($cust_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_payment_details" => $details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function add_paymnet_detail()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      $payment_method = $this->input->post('payment_method', TRUE);
      $brand_name = $this->input->post('brand_name', TRUE);
      $card_number = $this->input->post('card_number', TRUE);
      $bank_name = $this->input->post('bank_name', TRUE);
      $bank_address = $this->input->post('bank_address', TRUE);
      $expirty_date = $this->input->post('expirty_date', TRUE);
      $account_title = $this->input->post('account_title', TRUE);
      $bank_short_code = $this->input->post('bank_short_code', TRUE);
    
      $register_data = array(
        "cust_id" => $cust_id,
        "payment_method" => trim($payment_method),
        "brand_name" => trim($brand_name),
        "card_number" => trim($card_number),
        "bank_name" => trim($bank_name),
        "account_title" => trim($account_title),
        "bank_short_code" => trim($bank_short_code),
        "bank_address" => trim($bank_address),
        "expirty_date" => trim($expirty_date),
        "pay_status" => 1,
        "cre_datetime" => date("Y-m-d H:i:s")
      );
      if( $this->api->register_payment_details($register_data) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('add_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function update_paymnet_detail()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      $pay_id = $this->input->post('pay_id', TRUE); $pay_id = (int) $pay_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $brand_name = $this->input->post('brand_name', TRUE);
      $card_number = $this->input->post('card_number', TRUE);
      $bank_name = $this->input->post('bank_name', TRUE);
      $bank_address = $this->input->post('bank_address', TRUE);
      $expirty_date = $this->input->post('expirty_date', TRUE);
      $account_title = $this->input->post('account_title', TRUE);
      $bank_short_code = $this->input->post('bank_short_code', TRUE);
    
      $update_data = array(
        "payment_method" => trim($payment_method),
        "brand_name" => trim($brand_name),
        "card_number" => trim($card_number),
        "bank_name" => trim($bank_name),
        "account_title" => trim($account_title),
        "bank_short_code" => trim($bank_short_code),
        "bank_address" => trim($bank_address),
        "expirty_date" => trim($expirty_date),
      );
      if( $this->api->update_payment_details($pay_id, $update_data) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function delete_paymnet_detail()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      $pay_id = $this->input->post('pay_id', TRUE);  $pay_id = (int) $pay_id;
      if( $this->api->delete_payment_detail($cust_id, $pay_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('remove_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed').'...']; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }
   
  public function deliverer_booking_list_old()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $order_status = $this->input->post('order_status', TRUE);
      $booking_list = $this->api->get_booking_list($cust_id, $last_id, $order_status);

      /*
      for ($i=0; $i < sizeof($booking_list); $i++) { 
        $details = $this->api->order_status_list($booking_list[$i]['order_id']);
        $booking_list += $details;
      }
      */
      $width = $height = $length = $unit_id = $total_weight = $dangerous_goods_id = $quantity = $dimension_id = $contents = array();
      for ($i=0; $i < sizeof($booking_list); $i++) { 
        $details = $this->api->order_status_list($booking_list[$i]['order_id']);
        $packages = $this->api->get_order_packages($booking_list[$i]['order_id']);
        for ($j=0; $j < sizeof($packages); $j++){
          $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
        }
        $booking_list[$i] += ["status_details" => $details, "packages" => $packages];
      }

      if(empty($booking_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'booking_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'booking_list' => $booking_list]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function deliverer_booking_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $filter_type = $this->input->post('filter_type');
      $user_type = $this->input->post('user_type');
      $order_status = $this->input->post('order_status');
    }

    /*  changes on 30 DEC 2017  */
    //Filters
      $filter_type = $this->input->post('filter_type');
      $user_type = $this->input->post('user_type');
      $from_country_id = $this->input->post('country_id_src');
      $from_state_id = $this->input->post('state_id_src');
      $from_city_id = $this->input->post('city_id_src');
      $to_country_id = $this->input->post('country_id_dest');
      $to_state_id = $this->input->post('state_id_dest');
      $to_city_id = $this->input->post('city_id_dest');
      $from_address = $this->input->post('from_address');
      $to_address = $this->input->post('to_address');   
      $delivery_start = $this->input->post('delivery_start');
      $delivery_end = $this->input->post('delivery_end');
      $pickup_start = $this->input->post('pickup_start');
      $pickup_end = $this->input->post('pickup_end');
      $price = $this->input->post('price');
      $dimension_id = $this->input->post('dimension_id');
      $order_type = $this->input->post('order_type');
      //  New params in filter
      $max_weight = $this->input->post('max_weight');
      $unit_id = $this->input->post('c_unit_id');
      $creation_start = $this->input->post('creation_start');
      $creation_end = $this->input->post('creation_end');   
      $expiry_start = $this->input->post('expiry_start');
      $expiry_end = $this->input->post('expiry_end');
      $distance_start = $this->input->post('distance_start');
      $distance_end = $this->input->post('distance_end');
      $transport_type = $this->input->post('transport_type');
      $vehicle_id = $this->input->post('vehicle');
      $cat_id = $this->input->post('cat_id');
      $unit = $this->api->get_unit_master(true);
      $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
      $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
      $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

      if( $filter_type == 'advance') {
        if(trim($creation_start) != "NULL" && trim($creation_end) != "NULL") {
          $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
          $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
        } else { $creation_start_date = $creation_end_date = "NULL"; }

        if(trim($pickup_start) != "NULL" && trim($pickup_end) != "NULL") {
          $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
          $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
        } else { $pickup_start_date = $pickup_end_date = "NULL"; }

        if(trim($delivery_start) != "NULL" && trim($delivery_end) != "NULL") {
          $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
          $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
        } else { $delivery_start_date = $delivery_end_date = "NULL"; }

        if(trim($expiry_start) != "NULL" && trim($expiry_end) != "NULL") {
          $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
          $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
        } else { $expiry_start_date = $expiry_end_date = "NULL"; }

        $filter_array = array(
          "user_id" => $cust_id,
          "user_type" => $user_type,
          "order_status" => $order_status,
          "from_country_id" => ($from_country_id!=0)? $from_country_id: "0",
          "from_state_id" => ($from_state_id!=0)? $from_state_id: "0",
          "from_city_id" => ($from_city_id!=0)? $from_city_id: "0",
          "to_country_id" => ($to_country_id!=0)? $to_country_id: "0",
          "to_state_id" => ($to_state_id!=0)? $to_state_id: "0",
          "to_city_id" => ($to_city_id!=0)? $to_city_id: "0",
          "from_address" => ($from_address!="NULL")? $from_address: "NULL",
          "to_address" => ($to_address!="NULL")? $to_address: "NULL",
          "delivery_start_date" => ($delivery_start_date!="NULL")? $delivery_start_date: "NULL",
          "delivery_end_date" => ($delivery_end_date!="NULL")? $delivery_end_date: "NULL",
          "pickup_start_date" => ($pickup_start_date!="NULL")? $pickup_start_date: "NULL",
          "pickup_end_date" => ($pickup_end_date!="NULL")? $pickup_end_date: "NULL",
          "price" => ($price!=0)?$price:"0",
          "dimension_id" => ($dimension_id!=0)?$dimension_id:"0",
          "order_type" => ($order_type!="NULL")? $order_type: "NULL",
          // new filters
          "creation_start_date" => ($creation_start_date!="NULL")? $creation_start_date: "NULL",
          "creation_end_date" => ($creation_end_date!="NULL")? $creation_end_date: "NULL",
          "expiry_start_date" => ($expiry_start_date!="NULL")? $expiry_start_date: "NULL",
          "expiry_end_date" => ($expiry_end_date!="NULL")? $expiry_end_date: "NULL",
          "distance_start" => ($distance_start!=0)? $distance_start: "0",
          "distance_end" => ($distance_end!=0)? $distance_end: "0",
          "transport_type" => ($transport_type!="NULL")? $transport_type: "NULL",
          "vehicle_id" => ($vehicle_id!=0)? $vehicle_id: "0",
          "max_weight" => ($max_weight!=0)?$max_weight:"0",
          "unit_id" => ($unit_id != 0)?$unit_id:"0",
          "cat_id" => ($cat_id != 0)?$cat_id:"0",
        );

        $booking_list = $this->user->filtered_list($filter_array); 
        //echo $this->db->last_query(); die();
      } else {
          $booking_list = $this->api->get_booking_list($cust_id, $last_id, $order_status);
      }

      $width = $height = $length = $unit_id = $total_weight = $dangerous_goods_id = $quantity = $dimension_id = $contents = array();
      if(!empty($booking_list)){
        for ($i=0; $i < sizeof($booking_list); $i++) { 
          $details = $this->api->order_status_list($booking_list[$i]['order_id']);
          $packages = $this->api->get_order_packages($booking_list[$i]['order_id']);
          for ($j=0; $j < sizeof($packages); $j++){
            $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
          }
          $booking_list[$i] += ["status_details" => $details, "packages" => $packages];
        }
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'booking_list' => $booking_list]; 
      } else {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'booking_list' => array()];          
      }
      echo json_encode($this->response);
    }

  public function search_deliverer()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
      //$dimension_id = $this->input->post('dimension_id', TRUE);
      $from_country_id = $this->input->post('from_country_id', TRUE);
      $from_state_id = $this->input->post('from_state_id', TRUE);
      $from_city_id = $this->input->post('from_city_id', TRUE);
      $rating = $this->input->post('rating', TRUE);

      if($this->api->is_user_active_or_exists($cust_id)){
        if( $list = $this->api->get_deliverer_for_booking($cust_id, $last_id, $from_country_id, $from_state_id, $from_city_id, $rating) ) {
          for ($i=0; $i < sizeof($list); $i++) { 
            $did = $list[$i]['cust_id'];
            $is_favorite = $this->api->verify_favorite($did, $cust_id);
            if($is_favorite) $list[$i] += array("is_favorite" => "1");
            else $list[$i] += array("is_favorite" => "0");
          }
          //var_dump($list); die();
          $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "list" => array_reverse($list)];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed').'...']; }

      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
  }

  public function booking_delivery_request()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $deliverer_id = $this->input->post('deliverer_id', TRUE); $deliverer_id = (int) $deliverer_id;
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      $order_details = $this->api->get_order_detail($order_id);

      if($this->api->is_user_active_or_exists($deliverer_id)){
        $shipping_mode = $this->api->get_deliverer_shipping_mode($deliverer_id);
        $request_status = $this->api->get_deliverer_request_status($cust_id, $deliverer_id, $order_id);

        if($shipping_mode['shipping_mode'] == '0') {
          //var_dump($shipping_mode); die();
          $register_data = array(
            "cust_id" => $cust_id,
            "deliverer_id" => $deliverer_id,
            "order_id" => $order_id,
            "cre_datetime" => date("Y-m-d H:i:s")
          );
          //var_dump($request_status); die();
          if($request_status == 0 ) {
            if( $id = $this->api->booking_delivery_request($register_data) ) {
              //Get Deliverer Device ID's
              $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
              $arr_deleverer_fcm_ids = array();
              $arr_deleverer_apn_ids = array();
              foreach ($device_details_deleverer as $value) {
                if($value['device_type'] == 1) {
                  array_push($arr_deleverer_fcm_ids, $value['reg_id']);
                } else {
                  array_push($arr_deleverer_apn_ids, $value['reg_id']);
                }
              }
              $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
              $deleverer_apn_ids = $arr_deleverer_apn_ids;
              //Get PN API Keys and PEM files
              $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
              //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

              $message = $this->lang->line('order_delivery_request_msg');
              $msg =  array('title' => $this->lang->line('order_delivery_request'), 'type' => 'delivery-request', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $message);

              $this->api->sendFCM($msg, $deleverer_fcm_ids, $api_key_deliverer);
              //Push Notification APN
              $msg_apn_deliverer =  array('title' => $this->lang->line('order_delivery_request'), 'text' => $message);
              if(is_array($deleverer_apn_ids)) { 
                $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
                $this->notification->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
              }
              
              //send SMS to deliverer
              $deliverer_details = $this->api->get_user_details($deliverer_id);
              $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);

              if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
              $this->api->sendSMS($country_code.trim($mobile1), $message);
              $this->response = ['response' => 'success', 'message'=> $this->lang->line('order_request_sent_success'), 'request' => 'sent'];
            } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_request_sent_failed').'...', 'request' => 'notsent']; }
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_request_exists'), "request" => "exists"]; }
        } else {
          if($order_id <= 99999) { $delivery_code = 'DC'.$cust_id.sprintf("%06s", $order_id); } else { $delivery_code = 'DC'.$cust_id.$order_id; }
          //Get customer and deliverer contact and personal details
          $customer_details = $this->api->get_user_details($cust_id);
          $deliverer_details = $this->api->get_user_details($deliverer_id);
          $deliverer_name = $deliverer_details['firstname'].' '.$deliverer_details['lastname'];
          $deliverer_contact = $deliverer_details['mobile1'];
          $deliverer_profile_details = $this->api->get_deliverer_profile($deliverer_id);
          $deliverer_company = $deliverer_profile_details['company_name'];
          if( $this->api->update_order_status($deliverer_id, $order_id, 'accept', $delivery_code, $deliverer_name, $deliverer_company, $deliverer_contact) ) {
            $this->api->insert_order_status($deliverer_id, $order_id, 'accept');
              $this->api->remove_order_request($order_id, $deliverer_id);

            //Get Users device Details
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            //Get Customer Device Reg ID's
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {
                array_push($arr_customer_fcm_ids, $value['reg_id']);
              } else {
                array_push($arr_customer_apn_ids, $value['reg_id']);
              }
            }
            $customer_fcm_ids = $arr_customer_fcm_ids;
            $customer_apn_ids = $arr_customer_apn_ids;
            //Get Deliverer Device ID's
            $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
            $arr_deleverer_fcm_ids = array();
            $arr_deleverer_apn_ids = array();
            foreach ($device_details_deleverer as $value) {
              if($value['device_type'] == 1) {
                array_push($arr_deleverer_fcm_ids, $value['reg_id']);
              } else {
                array_push($arr_deleverer_apn_ids, $value['reg_id']);
              }
            }
            $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
            $deleverer_apn_ids = $arr_deleverer_apn_ids;
            //Get PN API Keys and PEM files
            $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
            //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
            
            if($deliverer_details['firstname'] == 'NULL') {
              if($deliverer_details['company_name'] == 'NULL') {
                $email_cut = explode('@', $deliverer_details['email1']);  
                $dname = $email_cut[0];
              } else {
                $dname = $deliverer_details['company_name'];
              }
            } else {
                $dname = $deliverer_details['firstname'] . ' ' . $deliverer_details['lastname'];
            }

            if($customer_details['firstname'] == 'NULL') {
              if($customer_details['company_name'] == 'NULL') {
                $email_cut = explode('@', $customer_details['email1']);  
                $cname = $email_cut[0];
              } else {
                $cname = $customer_details['company_name'];
              }
            } else {
              $cname = $customer_details['firstname'] . ' ' . $customer_details['lastname'];
            }

            //Send Notifications as per order status
            $sms_msg_customer = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . $delivery_code;

            $sms_msg_from_contact = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . trim($order_details['pickup_code']);
          
            $sms_msg_to_contact = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . $delivery_code;

            $sms_msg_deliverer = $this->lang->line('order_accept_deliverer_msg1') . $cname . $this->lang->line('order_accept_deliverer_msg2') . $order_id;
            
            $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
            if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
            $this->api->sendSMS($country_code.trim($mobile1), $sms_msg_customer);

            $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
            if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
            $this->api->sendSMS($country_code.trim($mobile1), $sms_msg_deliverer);

            if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
            $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg_from_contact);

            if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
            $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg_to_contact);

            //Email Order Placed confirmation deliverer
            $subject_deliverer = $this->lang->line('order_accept_deliverer_email_subject');
            $message ="<p class='lead'>". $sms_msg_deliverer . "</p>";
            $message .="</td></tr><tr><td align='center'><br />";
            $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
            $email1 = trim($deliverer_details['email1']);
            $this->api_sms->send_email_text($username, $email1, $subject_deliverer, trim($message));

            //Email Order Placed confirmation Customer
            $subject_customer = $this->lang->line('order_accept_email_subject');
            $message ="<p class='lead'>". $sms_msg_customer . "</p>";
            $message .="</td></tr><tr><td align='center'><br />";
            $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
            $email1 = trim($customer_details['email1']);
            $this->api_sms->send_email_text($username, $email1, $subject_customer, trim($message));

            //Email Order accept confirmation From contact
            $message ="<p class='lead'>". $sms_msg_from_contact . "</p>";
            $message .="</td></tr><tr><td align='center'><br />";
            $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject_customer, trim($message));

            //Email Order accept confirmation To contact
            $message ="<p class='lead'>". $sms_msg_to_contact . "</p>";
            $message .="</td></tr><tr><td align='center'><br />";
            $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject_customer, trim($message));

            //Push Notification FCM
            $msg_pn_customer =  array('title' => $subject_customer, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_customer);
            $msg_pn_deliverer =  array('title' => $subject_deliverer, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_deliverer);
            $this->api->sendFCM($msg_pn_customer, $customer_fcm_ids, $api_key_deliverer);
            $this->api->sendFCM($msg_pn_deliverer, $deleverer_fcm_ids, $api_key_deliverer);
            //Push Notification APN
            $msg_apn_customer =  array('title' => $subject_customer, 'text' => $sms_msg_customer);
            if(is_array($customer_apn_ids)) { 
              $customer_apn_ids = implode(',', $customer_apn_ids);
              $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
            }
            $msg_apn_deliverer =  array('title' => $subject_deliverer, 'text' => $sms_msg_deliverer);
            if(is_array($deleverer_apn_ids)) { 
              $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
              $this->notification->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
            }
    
            //Update to workroom
            $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('order_accepted_workroom'), 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');

            //Update to Global Workroom
            $global_workroom_update = array(
              'service_id' => $order_id,
              'cust_id' => $cust_id,
              'deliverer_id' => $deliverer_id,
              'sender_id' => $deliverer_id,
              'cat_id' => $order_details['category_id']
            );
            $this->api->create_global_workroom($global_workroom_update);

            $this->response = ['response' => 'success', 'message' => $this->lang->line('order_accepted_workroom'), 'request' => 'accept'];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_accept_failed'), 'request' => 'error']; }
        }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...', "user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function deliverer_order_request_list_old()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $order_request_list = $this->api->deliverer_order_request_list($cust_id, $last_id);
      
      if(empty($order_request_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'order_request_list' => array()]; 
      } else {
        for ($i=0; $i < sizeof($order_request_list); $i++) { 
          $packages = $this->api->get_order_packages($order_request_list[$i]['order_id']);
        for ($j=0; $j < sizeof($packages); $j++){
          $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
        }
          $order_request_list[$i] += ['packages' => $packages];
        }
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'order_request_list' => $order_request_list]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function deliverer_order_request_list()
  {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE);
        $last_id = $this->input->post('last_id', TRUE);
        $filter_type = $this->input->post('filter_type');
        $user_type = $this->input->post('user_type');
      }
      /*  changes on 01 JAN 2018  */
      //Filters
      $filter_type = $this->input->post('filter_type');
      $user_type = $this->input->post('user_type');
      $from_country_id = $this->input->post('country_id_src');
      $from_state_id = $this->input->post('state_id_src');
      $from_city_id = $this->input->post('city_id_src');
      $to_country_id = $this->input->post('country_id_dest');
      $to_state_id = $this->input->post('state_id_dest');
      $to_city_id = $this->input->post('city_id_dest');
      $from_address = $this->input->post('from_address');
      $to_address = $this->input->post('to_address');   
      $delivery_start = $this->input->post('delivery_start');
      $delivery_end = $this->input->post('delivery_end');
      $pickup_start = $this->input->post('pickup_start');
      $pickup_end = $this->input->post('pickup_end');
      $price = $this->input->post('price');
      $dimension_id = $this->input->post('dimension_id');
      $order_type = $this->input->post('order_type');
      //  New params in filter
      $max_weight = $this->input->post('max_weight');
      $unit_id = $this->input->post('c_unit_id');
      $creation_start = $this->input->post('creation_start');
      $creation_end = $this->input->post('creation_end');   
      $expiry_start = $this->input->post('expiry_start');
      $expiry_end = $this->input->post('expiry_end');
      $distance_start = $this->input->post('distance_start');
      $distance_end = $this->input->post('distance_end');
      $transport_type = $this->input->post('transport_type');
      $vehicle_id = $this->input->post('vehicle');
      $cat_id = $this->input->post('cat_id');
  
      $filter_array = array(
        "user_id" => $cust_id,
        "user_type" => $user_type,
        "from_country_id" => ($from_country_id!=0)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=0)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=0)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=0)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=0)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=0)? $to_city_id: "0",
        "from_address" => ($from_address!="NULL")? $from_address: "NULL",
        "to_address" => ($to_address!="NULL")? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start!="NULL")? $delivery_start: "NULL",
        "delivery_end_date" => ($delivery_end!="NULL")? $delivery_end: "NULL",
        "pickup_start_date" => ($pickup_start!="NULL")? $pickup_start: "NULL",
        "pickup_end_date" => ($pickup_end!="NULL")? $pickup_end: "NULL",
        "price" => ($price!=0)?$price:"0",
        "dimension_id" => ($dimension_id!=0)?$dimension_id:"0",
        "order_type" => ($order_type!="NULL")? $order_type: "NULL",
        "creation_start_date" => ($creation_start!="NULL")? $creation_start: "NULL",
        "creation_end_date" => ($creation_end!="NULL")? $creation_end: "NULL",
        "expiry_start_date" => ($expiry_start!="NULL")? $expiry_start: "NULL",
        "expiry_end_date" => ($expiry_end!="NULL")? $expiry_end: "NULL",
        "distance_start" => ($distance_start!=0)? $distance_start: "0",
        "distance_end" => ($distance_end!=0)? $distance_end: "0",
        "transport_type" => ($transport_type!="NULL")? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=0)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=0)?$max_weight:"0",
        "unit_id" => ($unit_id != 0)?$unit_id:"0",
        "cat_id" => ($cat_id != 0)?$cat_id:"0",
        "cust_id" => $cust_id, 
        "filter_type" => $filter_type,
        "last_id" => $last_id
      );
    
      //$order_request_list = $this->api->deliverer_order_request_list($cust_id, $last_id);
      $order_request_list = $this->api->deliverer_order_request_list($filter_array);
      //var_dump($this->db->last_query()); die();
      if(empty($order_request_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'order_request_list' => array()]; 
      } else {
        for ($i=0; $i < sizeof($order_request_list); $i++) { 
          $packages = $this->api->get_order_packages($order_request_list[$i]['order_id']);
          for ($j=0; $j < sizeof($packages); $j++){
            $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
          }
          $order_request_list[$i] += ['packages' => $packages];
        }
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'order_request_list' => $order_request_list ]; 
      }
      echo json_encode($this->response);
  }

  public function deliverer_order_list_old()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $order_status = $this->input->post('order_status', TRUE);

      $deliverer_order = $this->api->deliverer_order_list($cust_id, $last_id, $order_status);
      for ($i=0; $i < sizeof($deliverer_order); $i++) { 
        $workroom_count = $this->api->get_order_workroom_count($deliverer_order[$i]['order_id']);
        $deliverer_order[$i] += ["workroom_count" => "".$workroom_count.""];
      }
      for ($i=0; $i < sizeof($deliverer_order); $i++) { 
        $details = $this->api->order_status_list($deliverer_order[$i]['order_id']);
        $packages = $this->api->get_order_packages($deliverer_order[$i]['order_id']);
        for ($j=0; $j < sizeof($packages); $j++){
          $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
        }
        $deliverer_order[$i] += ["status_details" => $details,'packages' => $packages];
      }
      
      if(empty($deliverer_order)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'deliverer_order' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'deliverer_order' => $deliverer_order]; 
      }
      echo json_encode($this->response);
    }
  }
  
  public function deliverer_order_list()
  {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE);
        $last_id = $this->input->post('last_id', TRUE);
        $filter_type = $this->input->post('filter_type');
        $user_type = $this->input->post('user_type');
        $order_status = $this->input->post('order_status');
      }

      $filter_type = $this->input->post('filter_type');
      $user_type = $this->input->post('user_type');
      $from_country_id = $this->input->post('country_id_src');
      $from_state_id = $this->input->post('state_id_src');
      $from_city_id = $this->input->post('city_id_src');
      $to_country_id = $this->input->post('country_id_dest');
      $to_state_id = $this->input->post('state_id_dest');
      $to_city_id = $this->input->post('city_id_dest');
      $from_address = $this->input->post('from_address');
      $to_address = $this->input->post('to_address');   
      $delivery_start = $this->input->post('delivery_start');
      $delivery_end = $this->input->post('delivery_end');
      $pickup_start = $this->input->post('pickup_start');
      $pickup_end = $this->input->post('pickup_end');
      $price = $this->input->post('price');
      $dimension_id = $this->input->post('dimension_id');
      $order_type = $this->input->post('order_type');
      //  New params in filter
      $max_weight = $this->input->post('max_weight');
      $unit_id = $this->input->post('c_unit_id');
      $creation_start = $this->input->post('creation_start');
      $creation_end = $this->input->post('creation_end');   
      $expiry_start = $this->input->post('expiry_start');
      $expiry_end = $this->input->post('expiry_end');
      $distance_start = $this->input->post('distance_start');
      $distance_end = $this->input->post('distance_end');
      $transport_type = $this->input->post('transport_type');
      $vehicle_id = $this->input->post('vehicle');
      $cat_id = $this->input->post('cat_id');
      $unit = $this->api->get_unit_master(true);
      $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
      $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
      $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');
  
      if( $filter_type == 'advance') {
        if(trim($creation_start) != "NULL" && trim($creation_end) != "NULL") {
          $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
          $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
        } else { $creation_start_date = $creation_end_date = "NULL"; }
  
        if(trim($pickup_start) != "NULL" && trim($pickup_end) != "NULL") {
          $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
          $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
        } else { $pickup_start_date = $pickup_end_date = "NULL"; }
  
        if(trim($delivery_start) != "NULL" && trim($delivery_end) != "NULL") {
          $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
          $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
        } else { $delivery_start_date = $delivery_end_date = "NULL"; }
  
        if(trim($expiry_start) != "NULL" && trim($expiry_end) != "NULL") {
          $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
          $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
        } else { $expiry_start_date = $expiry_end_date = "NULL"; }
  
        $filter_array = array(
          "user_id" => $cust_id,
          "user_type" => $user_type,
          "order_status" => $order_status,
          "from_country_id" => ($from_country_id!=0)? $from_country_id: "0",
          "from_state_id" => ($from_state_id!=0)? $from_state_id: "0",
          "from_city_id" => ($from_city_id!=0)? $from_city_id: "0",
          "to_country_id" => ($to_country_id!=0)? $to_country_id: "0",
          "to_state_id" => ($to_state_id!=0)? $to_state_id: "0",
          "to_city_id" => ($to_city_id!=0)? $to_city_id: "0",
          "from_address" => ($from_address!="NULL")? $from_address: "NULL",
          "to_address" => ($to_address!="NULL")? $to_address: "NULL",
          "delivery_start_date" => ($delivery_start_date!="NULL")? $delivery_start_date: "NULL",
          "delivery_end_date" => ($delivery_end_date!="NULL")? $delivery_end_date: "NULL",
          "pickup_start_date" => ($pickup_start_date!="NULL")? $pickup_start_date: "NULL",
          "pickup_end_date" => ($pickup_end_date!="NULL")? $pickup_end_date: "NULL",
          "price" => ($price!=0)?$price:"0",
          "dimension_id" => ($dimension_id!=0)?$dimension_id:"0",
          "order_type" => ($order_type!="NULL")? $order_type: "NULL",
          // new filters
          "creation_start_date" => ($creation_start_date!="NULL")? $creation_start_date: "NULL",
          "creation_end_date" => ($creation_end_date!="NULL")? $creation_end_date: "NULL",
          "expiry_start_date" => ($expiry_start_date!="NULL")? $expiry_start_date: "NULL",
          "expiry_end_date" => ($expiry_end_date!="NULL")? $expiry_end_date: "NULL",
          "distance_start" => ($distance_start!=0)? $distance_start: "0",
          "distance_end" => ($distance_end!=0)? $distance_end: "0",
          "transport_type" => ($transport_type!="NULL")? $transport_type: "NULL",
          "vehicle_id" => ($vehicle_id!=0)? $vehicle_id: "0",
          "max_weight" => ($max_weight!=0)?$max_weight:"0",
          "unit_id" => ($unit_id != 0)?$unit_id:"0",
          "cat_id" => ($cat_id != 0)?$cat_id:"0",
        );
        $deliverer_order = $this->user->filtered_list($filter_array); 
        //echo $this->db->last_query(); die();
      } else {
        $deliverer_order = $this->api->deliverer_order_list($cust_id, $last_id, $order_status);
      }
      if(!empty($deliverer_order)) {
        for ($i=0; $i < sizeof($deliverer_order); $i++) { 
          $workroom_count = $this->api->get_order_workroom_count($deliverer_order[$i]['order_id']);
          $deliverer_order[$i] += ["workroom_count" => "".$workroom_count.""];
        }
        for ($i=0; $i < sizeof($deliverer_order); $i++) { 
          $details = $this->api->order_status_list($deliverer_order[$i]['order_id']);
          $packages = $this->api->get_order_packages($deliverer_order[$i]['order_id']);
          for ($j=0; $j < sizeof($packages); $j++){
            $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
          }
          $deliverer_order[$i] += ["status_details" => $details,'packages' => $packages];
        }
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'deliverer_order' => $deliverer_order]; 
      } else {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'deliverer_order' => array()]; 
      }
      echo json_encode($this->response); 
  }
 
  public function deliverer_order_accept()
  {
    if(empty($this->errors)) {  
      $deliverer_id = $this->input->post('deliverer_id', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE);
      $order_id = $this->input->post('order_id', TRUE);
      $order_status = $this->input->post('order_status', TRUE);
      $today = date('Y-m-d h:i:s');
      
      $order_details = $this->api->get_order_detail($order_id);
      if($order_details['order_status'] == 'open') {
      
      if($order_status == 'accept') { 
        if($order_id <= 99999) { $delivery_code = 'DC'.$cust_id.sprintf("%06s", $order_id); } else { $delivery_code = 'DC'.$cust_id.$order_id; }

        //Get customer and deliverer contact and personal details
        $customer_details = $this->api->get_user_details($cust_id);
        $deliverer_details = $this->api->get_user_details($deliverer_id);
        $deliverer_name = $deliverer_details['firstname'].' '.$deliverer_details['lastname'];
        $deliverer_contact = $deliverer_details['mobile1'];
        $deliverer_profile_details = $this->api->get_deliverer_profile($deliverer_id);
        $deliverer_company = $deliverer_profile_details['company_name'];
        if($this->api->update_order_status($deliverer_id, $order_id, 'accept', $delivery_code, $deliverer_name, $deliverer_company, $deliverer_contact)) { 
          $this->api->insert_order_status($deliverer_id, $order_id, 'accept');
          $this->api->remove_order_request($order_id, $deliverer_id);
          //Get Users device Details
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          //Get Customer Device Reg ID's
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_customer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_customer_apn_ids, $value['reg_id']);
            }
          }
          $customer_fcm_ids = $arr_customer_fcm_ids;
          $customer_apn_ids = $arr_customer_apn_ids;
          //Get Deliverer Device ID's
          $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
          $arr_deleverer_fcm_ids = array();
          $arr_deleverer_apn_ids = array();
          foreach ($device_details_deleverer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_deleverer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_deleverer_apn_ids, $value['reg_id']);
            }
          }
          $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
          $deleverer_apn_ids = $arr_deleverer_apn_ids;
          //Get PN API Keys and PEM files
          $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
          //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
          //Send Notifications as per order status

          if($deliverer_details['firstname'] == 'NULL') {
            if($deliverer_details['company_name'] == 'NULL') {
              $email_cut = explode('@', $deliverer_details['email1']);  
              $dname = $email_cut[0];
            } else {
              $dname = $deliverer_details['company_name'];
            }
          } else {
              $dname = $deliverer_details['firstname'] . ' ' . $deliverer_details['lastname'];
          }

          if($customer_details['firstname'] == 'NULL') {
            if($customer_details['company_name'] == 'NULL') {
              $email_cut = explode('@', $customer_details['email1']);  
              $cname = $email_cut[0];
            } else {
              $cname = $customer_details['company_name'];
            }
          } else {
              $cname = $customer_details['firstname'] . ' ' . $customer_details['lastname'];
          }

          $sms_msg_customer = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . $delivery_code;

          $sms_msg_from_contact = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . trim($order_details['pickup_code']);
          
          $sms_msg_to_contact = $this->lang->line('order_accept_msg1') . $order_id . $this->lang->line('order_accept_msg2') . $dname . $this->lang->line('order_accept_msg3') . $delivery_code;

          $sms_msg_deliverer = $this->lang->line('order_accept_deliverer_msg1') . $cname . $this->lang->line('order_accept_deliverer_msg2') . $order_id;
          
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg_customer);

          $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
          if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg_deliverer);

          if(substr(trim($order_details['from_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['from_address_contact']), 0); } else { $mobile1 = trim($order_details['from_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['from_country_id'])).trim($mobile1), $sms_msg_from_contact);

          if(substr(trim($order_details['to_address_contact']), 0, 1) == 0) { $mobile1 = ltrim(trim($order_details['to_address_contact']), 0); } else { $mobile1 = trim($order_details['to_address_contact']); }
          $this->api->sendSMS($this->api->get_country_code_by_id(trim($order_details['to_country_id'])).trim($mobile1), $sms_msg_to_contact);

          //Email Order Placed confirmation deliverer
          $subject_deliverer = $this->lang->line('order_accept_deliverer_email_subject');
          $message ="<p class='lead'>". $sms_msg_deliverer . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($deliverer_details['firstname']) . trim($deliverer_details['lastname']);
          $email1 = trim($deliverer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject_deliverer, trim($message));
          //Email Order Placed confirmation Customer
          $subject_customer = $this->lang->line('order_accept_email_subject');
          $message ="<p class='lead'>". $sms_msg_customer . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
          $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject_customer, trim($message));
          //Email Order accept confirmation From contact
          $message ="<p class='lead'>". $sms_msg_from_contact . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $this->api_sms->send_email_text(trim($order_details['from_address_name']), trim($order_details['from_address_email']), $subject_customer, trim($message));

          //Email Order accept confirmation To contact
          $message ="<p class='lead'>". $sms_msg_to_contact . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $this->api_sms->send_email_text(trim($order_details['to_address_name']), trim($order_details['to_address_email']), $subject_customer, trim($message));
          //Push Notification FCM
          $msg_pn_customer =  array('title' => $subject_customer, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_customer);
          $this->api->sendFCM($msg_pn_customer, $customer_fcm_ids, $api_key_deliverer);
          //APN
          $msg_apn_customer =  array('title' => $subject_customer, 'text' => $sms_msg_customer);
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          
          //Update to workroom
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $this->lang->line('order_accepted_workroom'), 'sp', 'order_status', 'NULL', trim($customer_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');

          //Update to Global Workroom
          $global_workroom_update = array(
            'service_id' => $order_id,
            'cust_id' => $cust_id,
            'deliverer_id' => $deliverer_id,
            'sender_id' => $deliverer_id,
            'cat_id' => $order_details['category_id']
          );
          $this->api->create_global_workroom($global_workroom_update);

          $this->response = ['response' => 'success', 'message' => $this->lang->line('order_accepted_workroom'), 'request' => 'accept'];
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_accept_failed'), 'request' => 'error'];
        }
      } else { 
        if( $this->api->insert_order_status($deliverer_id, $order_id, 'reject') ) {
          $reject_history = array(
            "order_id" => (int)$order_id,
            "cust_id" => (int)$cust_id,
            "deliverer_id" => (int)$deliverer_id,
            "rejected_datetime" => $today,
            "from_country_id" => (int) $order_details['from_country_id'],
            "cat_id" => (int) $order_details['category_id'],
          );
          $this->api->register_rejected_order_history($reject_history);
          $this->api->remove_order_request($order_id, $deliverer_id);
          //Get customer and deliverer contact and personal details
          $customer_details = $this->api->get_user_details($cust_id);
          $deliverer_details = $this->api->get_user_details($deliverer_id);
          //Get Users device Details
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          //Get Customer Device Reg ID's
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_customer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_customer_apn_ids, $value['reg_id']);
            }
          }
          $customer_fcm_ids = $arr_customer_fcm_ids;
          $customer_apn_ids = $arr_customer_apn_ids;
          //Get Deliverer Device ID's
          $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
          $arr_deleverer_fcm_ids = array();
          $arr_deleverer_apn_ids = array();
          foreach ($device_details_deleverer as $value) {
            if($value['device_type'] == 1) {
              array_push($arr_deleverer_fcm_ids, $value['reg_id']);
            } else {
              array_push($arr_deleverer_apn_ids, $value['reg_id']);
            }
          }
          $deleverer_fcm_ids = $arr_deleverer_fcm_ids;
          $deleverer_apn_ids = $arr_deleverer_apn_ids;
          //Get PN API Keys and PEM files
          $api_key_deliverer = $this->config->item('delivererAppGoogleKey');
          //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
          //Send Notifications as per order status
          
          if($deliverer_details['firstname'] == 'NULL') {
            if($deliverer_details['company_name'] == 'NULL') {
              $email_cut = explode('@', $deliverer_details['email1']);  
              $dname = $email_cut[0];
            } else {
              $dname = $deliverer_details['company_name'];
            }
          } else {
              $dname = $deliverer_details['firstname'] . ' ' . $deliverer_details['lastname'];
          }

          $sms_msg_customer = $this->lang->line('order_reject_msg1') . $order_id . $this->lang->line('order_reject_msg2') . $dname;
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          if(substr(trim($customer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($customer_details['mobile1']), 0); } else { $mobile1 = trim($customer_details['mobile1']); }
          $this->api->sendSMS($country_code.trim($mobile1), $sms_msg_customer);
          
          //Email Order Reject confirmation Customer
          $subject_customer = $this->lang->line('order_reject_email_subject');
          $message ="<p class='lead'>". $sms_msg_customer . "</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          $username = trim($customer_details['firstname']) . trim($customer_details['lastname']);
          $email1 = trim($customer_details['email1']);
          $this->api_sms->send_email_text($username, $email1, $subject_customer, trim($message));

          //Push Notification FCM
          $msg_pn_customer =  array('title' => $subject_customer, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_customer);
          $this->api->sendFCM($msg_pn_customer, $customer_fcm_ids, $api_key_deliverer);
          //Push Notification APN
          $msg_apn_customer =  array('title' => $subject_customer, 'text' => $sms_msg_customer);
          if(is_array($customer_apn_ids)) { 
            $customer_apn_ids = implode(',', $customer_apn_ids);
            $this->notification->sendPushIOS($msg_apn_customer, $customer_apn_ids);
          }
          
          //Update to workroom
          $this->response = ['response' => 'success', 'message' => $this->lang->line('order_rejected_deliverer'), 'request' => 'reject'];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_reject_failed'), 'request' => 'error']; }
      }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_already_accepted'), 'request' => 'exists']; }
      echo json_encode($this->response);
    }
  }

  public function deliverer_assign_driver()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $cd_id = $this->input->post('cd_id', TRUE); $cd_id = (int) $cd_id;
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      $cd_name = $this->input->post('cd_name', TRUE);
      $today = date('Y-m-d H:i:s');
      $order_details = $this->api->get_order_detail($order_id);
      //Get Driver contact and personal details
      $driver_details = $this->api->get_driver_details($cd_id);

      if( $this->api->update_order_status_deliverer($cd_id, $order_id, $cd_name) ) {
        //$this->api->insert_order_status($cust_id, $order_id, 'assign');

        $assign_status = $this->api->get_assign_status($order_id);
        if($assign_status == 0) { 
          $this->api->insert_order_status($cust_id, $order_id, 'assign');
        }
        
        $deliverer_details = $this->api->get_user_details($cust_id);
        //Get Driver device Details
        $device_details_driver = $this->api->get_driver_device_details($cd_id);
        //Get Driver Device Reg ID's
        $arr_driver_fcm_ids = array();
        $arr_driver_apn_ids = array();
        foreach ($device_details_driver as $value) {
          if($value['device_type'] == 1) {
            array_push($arr_driver_fcm_ids, $value['reg_id']);
          } else {
            array_push($arr_driver_apn_ids, $value['reg_id']);
          }
        }
        $driver_fcm_ids = $arr_driver_fcm_ids;
        $driver_apn_ids = $arr_driver_apn_ids;
        //Get PN API Keys and PEM files
        $api_key_driver = $this->config->item('driverAppGoogleKey');
        //$api_key_driver_pem = $this->config->item('driverAppPemFile');
        //Send Notifications as per order status

        if($deliverer_details['firstname'] == 'NULL') {
          if($deliverer_details['company_name'] == 'NULL') {
            $email_cut = explode('@', $deliverer_details['email1']);  
            $dname = $email_cut[0];
          } else {
            $dname = $deliverer_details['company_name'];
          }
        } else {
          $dname = $deliverer_details['firstname'] . ' ' . $deliverer_details['lastname'];
        }
        $cname = 'User';
        if($order_details['cust_name'] == 'NULL' || $order_details['cust_name'] == 'NULL NULL') {
          $canme = $this->api->get_customer_email_name_by_order_id($order_details['cust_id']);
        } else {
          $cname = $order_details['cust_name'];
        }

        $sms_msg_driver = $this->lang->line('order_assign_msg1') 
                        . $dname 
                        . $this->lang->line('order_assign_msg2') 
                        . $order_id . ' ' 
                        . $this->lang->line('customer_name') . $cname . ' ' 
                        . $this->lang->line('from:') . $order_details['from_address'] . ' ' 
                        . $this->lang->line('to:') . $order_details['to_address'] . ' ' 
                        . $this->lang->line('pickup_at') . $order_details['pickup_datetime'] . ' ' 
                        . $this->lang->line('deliver_at') . $order_details['delivery_datetime'];

        if(substr(trim($driver_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($driver_details['mobile1']), 0); } else { $mobile1 = trim($driver_details['mobile1']); }
        $this->api->sendSMS(trim($driver_details['country_code']).trim($mobile1), $sms_msg_driver);
        //Email Order assign to driver
        $subject_driver = $this->lang->line('order_assign_driver_email_subject');
        $message ="<p class='lead'>". $sms_msg_driver . "</p>";
        $message .="</td></tr><tr><td align='center'><br />";
        $username = trim($driver_details['first_name']) . trim($driver_details['last_name']);
        $email1 = trim($driver_details['email']);
        $this->api_sms->send_email_text($username, $email1, $subject_driver, trim($message));
        //Push Notification FCM
        $msg_pn_driver =  array('title' => $subject_driver, 'type' => 'order-status', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $sms_msg_driver);
        $this->api->sendFCM($msg_pn_driver, $driver_fcm_ids, $api_key_driver);
        //Push Notification APN
        $msg_apn_driver =  array('title' => $subject_driver, 'text' => $sms_msg_driver);
        if(is_array($driver_apn_ids)) { 
          $driver_apn_ids = implode(',', $driver_apn_ids);
          $this->notification->sendPushIOS($msg_apn_driver, $driver_apn_ids);
        }
        $order_details = $this->api->get_order_detail($order_id);
        //check order of dedicated users
        if(trim($order_details['is_bank_payment']) == 0) {
        //Payment Module-------------------------------------------------------------------------------------
          $advance_payment_count = $this->api->check_advance_transfer_in_deliverer_account_master($order_id);
          if($advance_payment_count == 0) {
            //Payment Module---------------------------------------------------------------
              $today = date('Y-m-d h:i:s');
              $order_details = $this->api->get_order_detail($order_id);
              if(trim($order_details['advance_payment']) > 0) {
                //update payment details in gonagoo account master and history
                if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                } else {
                  $insert_data_gonagoo_master = array(
                    "gonagoo_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($order_details['currency_sign']),
                  );
                  $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                }
                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($order_details['advance_payment']);
                $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details($order_details['currency_sign']);

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$order_details['deliverer_id'],
                  "type" => 0,
                  "transaction_type" => 'advance_payment',
                  "amount" => (trim($order_details['advance_payment'])-trim($order_details['insurance_fee'])-trim($order_details['custom_clearance_fee'])),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => 'NULL',
                  "transfer_account_number" => 'NULL',
                  "bank_name" => 'NULL',
                  "account_holder_name" => 'NULL',
                  "iban" => 'NULL',
                  "email_address" => 'NULL',
                  "mobile_number" => 'NULL',
                  "transaction_id" => 'NULL',
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

                //Add advance payment details in deliverer account master and history
                if($this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']))) {
                  $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
                } else {
                  $insert_data_customer_master = array(
                    "user_id" => (int)$order_details['deliverer_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($order_details['currency_sign']),
                  );
                  $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                  $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
                }
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($order_details['advance_payment']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], (int)$order_details['deliverer_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$order_details['deliverer_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'advance_payment',
                  "amount" => (trim($order_details['advance_payment'])-trim($order_details['insurance_fee'])-trim($order_details['custom_clearance_fee'])),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              }
              $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));

              //Deduct deliverer commission from deliverer account master and history
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($order_details['commission_amount']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], (int)$order_details['deliverer_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['deliverer_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'commission_amount',
                "amount" => trim($order_details['commission_amount']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              //add deiverer commission to gonagoo account master and history
              if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
              }
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['commission_amount']);
              $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$order_details['deliverer_id'],
                "type" => 1,
                "transaction_type" => 'commission_amount',
                "amount" => trim($order_details['commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

              //Add pickup amount in deliverer scrow account and history
              if($this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']))) {
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
              } else {
                $insert_data_customer_scrow = array(
                  "deliverer_id" => (int)$order_details['deliverer_id'],
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api->insert_gonagoo_customer_scrow_record($insert_data_customer_scrow);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));
              }
              
              if( trim($order_details['pickup_payment']) > 0 ) {
                $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) + trim($order_details['pickup_payment']);
                $this->api->update_payment_in_deliverer_scrow((int)$deliverer_scrow_master_details['scrow_id'], (int)$order_details['deliverer_id'], $scrow_balance);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));

                $update_data_scrow_history = array(
                  "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                  "order_id" => (int)$order_id,
                  "deliverer_id" => (int)$order_details['deliverer_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'pickup_payment',
                  "amount" => trim($order_details['pickup_payment']),
                  "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_scrow_history($update_data_scrow_history);
              }

              if( trim($order_details['deliver_payment']) > 0 ) {
                //Add delivery amount in deliverer scrow account and history
                $scrow_balance = trim($deliverer_scrow_master_details['scrow_balance']) + trim($order_details['deliver_payment']);
                $this->api->update_payment_in_deliverer_scrow((int)$deliverer_scrow_master_details['scrow_id'], (int)$order_details['deliverer_id'], $scrow_balance);
                $deliverer_scrow_master_details = $this->api->deliverer_scrow_master_details((int)$order_details['deliverer_id'], trim($order_details['currency_sign']));

                $update_data_scrow_history = array(
                  "scrow_id" => (int)$deliverer_scrow_master_details['scrow_id'],
                  "order_id" => (int)$order_id,
                  "deliverer_id" => (int)$order_details['deliverer_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'deliver_payment',
                  "amount" => trim($order_details['deliver_payment']),
                  "scrow_balance" => trim($deliverer_scrow_master_details['scrow_balance']),
                  "currency_code" => trim($order_details['currency_sign']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_scrow_history($update_data_scrow_history);
              }
              
              //send commission invoice to deliverer
                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference("$order_id");
                $invoice->setDate(date('M dS ,Y',time()));
                
                $deliverer_country = $this->api->get_country_details(trim($deliverer_details['country_id']));
                $deliverer_state = $this->api->get_state_details(trim($deliverer_details['state_id']));
                $deliverer_city = $this->api->get_city_details(trim($deliverer_details['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($deliverer_details['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setFrom(array(trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']),trim($deliverer_city['city_name']),trim($deliverer_state['state_name']),trim($deliverer_country['country_name']),trim($deliverer_details['email1'])));

                $invoice->setTo(array($gonagoo_address['company_name'], $gonagoo_city['city_name'], $gonagoo_address['no_street_name'], $gonagoo_country['country_name'], $gonagoo_address['email']));
                
                /* Adding Items in table */
                $order_for = $this->lang->line('gonagoo_commission');
                $rate = round(trim($order_details['commission_amount']),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
                
                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('commission_paid'));

                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                //$invoice->addTitle("Other Details");
                /* Add Paragraph */
                //$invoice->addParagraph("Any Other Details");
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch')."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($order_id.'_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

                //Update File path
                $pdf_name = $order_id.'_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/order-invoices/".$pdf_name);
                
                //Update Order Invoice and post to workroom
                $this->api->update_deliverer_invoice_url($order_id, "order-invoices/".$pdf_name);
              /* ----------------------------------------------------- */
              /* -----------------Email Invoice to customer--------------------- */
                $eol = PHP_EOL;
                $messageBody = $eol . $this->lang->line('invoice_email_message') . $eol;
                $file = "resources/order-invoices/".$pdf_name;
                $file_size = filesize($file);
                $handle = fopen($file, "r");
                $content = fread($handle, $file_size);
                fclose($handle);
                $content = chunk_split(base64_encode($content));
                $uid = md5(uniqid(time()));
                $name = basename($file);
                //$eol = PHP_EOL;
                $from_mail = $this->config->item('from_email');
                $replyto = $this->config->item('from_email');
                // Basic headers
                $header = "From: ".$gonagoo_address['company_name']." <".$from_mail.">".$eol;
                $header .= "Reply-To: ".$replyto.$eol;
                $header .= "MIME-Version: 1.0\r\n";
                $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";

                // Put everything else in $message
                $message = "--".$uid.$eol;
                $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
                $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
                $message .= $messageBody.$eol;
                $message .= "--".$uid.$eol;
                $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
                $message .= "Content-Transfer-Encoding: base64".$eol;
                $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
                $message .= $content.$eol;
                $message .= "--".$uid."--";

                mail($deliverer_details['email1'], $this->lang->line('commission_invoice_email_subject'), $message, $header);
              /* -----------------------Email Invoice to customer----------------------- */

              //post payment in workroom
              $this->api->post_to_workroom($order_id, trim($order_details['cust_id']), $cust_id, $this->lang->line('advance_payment_recieved'), 'sr', 'payment', 'NULL', trim($order_details['cust_name']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
            //Payment Module End ----------------------------------------------------------
          }
        }
        //if order is from laundry service mark it as in_progress
        if(trim($order_details['is_laundry']) == 1) {
          $booking_details = $this->api->laundry_booking_details((int)trim($order_details['ref_no']));
          $data = array(
            'booking_status' => 'in_progress',
            'status_updated_by' => trim($booking_details['provider_id']), 
            'status_updated_by_user_type' => 'provider', 
            'status_update_datetime' => trim($today),
          );
          $this->api->laundry_booking_update($data, $booking_id);
        }
        $this->response = ['response' => 'success', 'message' => $this->lang->line('order_assigned_driver_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_assigned_driver_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_delivery_code()
  {
    if(empty($this->errors)){ 
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      if( $details = $this->api->get_delivery_code($order_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "delivery_code" => $details['delivery_code']];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }
  
  public function submit_review()
  {
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      $deliverer_id = $this->input->post('deliverer_id', TRUE); $deliverer_id = (int) $deliverer_id;
      $cust_name = $this->input->post('cust_name', TRUE);
      $deliverer_name = $this->input->post('deliverer_name', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        $review_comment = $this->input->post('review_comment', TRUE);
        $rating = $this->input->post('rating', TRUE); $rating = (int) $rating;

        $update_data = array(
          "review_comment" => $review_comment,
          "rating" => $rating,
          "review_datetime" => date('Y-m-d H:i:s'),
        );

        if( $this->api->update_review($update_data, $order_id) ) {
          $old_ratings = $this->api->get_user_review_details($deliverer_id);
          $no_of_ratings = $old_ratings['no_of_ratings']+1;
          $total_ratings = $old_ratings['total_ratings']+$rating;
          $ratings = round($total_ratings/$no_of_ratings,2);
          $deliverer_review = array(
            "ratings" => $ratings,
            "total_ratings" => $total_ratings,
            "no_of_ratings" => $no_of_ratings,
          );
          $this->api->update_deliverer_rating($deliverer_review, $deliverer_id);
          if($cust_courier_rating = $this->api->get_courier_rating($deliverer_id)) {
            $courier_no_of_ratings = $cust_courier_rating['no_of_ratings']+1;
            $courier_total_ratings = $cust_courier_rating['total_ratings']+$rating;
            $courier_ratings = round($courier_total_ratings/$courier_no_of_ratings,1);
            $key = 'update';
            $courier_deliverer_rating = array(
              "deliverer_id" => $deliverer_id,
              "update_date" => $today,
              "ratings" => $courier_ratings,
              "total_ratings" => $courier_total_ratings,
              "no_of_ratings" => $courier_no_of_ratings,
            );
            $this->api->manage_courier_ratings($courier_deliverer_rating, $key, $cust_courier_rating['rating_id']);
          } else {
            $key = 'insert';
            $courier_deliverer_rating = array(
              "deliverer_id" => $deliverer_id,
              "update_date" => $today,
              "ratings" => $ratings,
              "total_ratings" => $total_ratings,
              "no_of_ratings" => $no_of_ratings,
            );
            $this->api->manage_courier_ratings($courier_deliverer_rating, $key, 0);
          }

          $cust_details = $this->api->get_consumer_datails($cust_id);
          $review_data = array(
            "order_id" => $order_id,
            "cust_id" => $cust_id,
            "deliverer_id" => $deliverer_id,
            "deliverer_name" => $deliverer_name,
            "cust_name" => $cust_details['firstname']. ' ' .$cust_details['lastname'],
            "cust_avatar" => $cust_details['avatar_url'],
            "review_comment" => $review_comment,
            "review_rating" => $rating,
            "review_datetime" => date('Y-m-d H:i:s'),
          );
          $this->api->post_to_review($review_data);
          $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $review_comment, 'sr', 'review_rating', "NULL", $cust_name, $deliverer_name, "NULL", "NULL", $rating);
          $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function get_driver_location()
  {
    if(empty($this->errors)){ 
      $cd_id = $this->input->post('cd_id', TRUE); $cd_id = (int) $cd_id;
      if( $details = $this->api->get_driver_location($cd_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "driver_lat" => $details['latitude'], "driver_long" => $details['longitude'], "location_update_datetime" => $details['loc_update_datetime']];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function get_order_status()
  {
    if(empty($this->errors)){ 
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      if( $details = $this->api->get_order_detail($order_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "order_status" => $details['order_status']];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function workroom_chat_post()
  {
    if(empty($this->errors)){
      $order_id = $this->input->post('order_id', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE);
      $deliverer_id = $this->input->post('deliverer_id', TRUE);
      $text_msg = $this->input->post('text_msg', TRUE);
      $sender_id = $this->input->post('sender_id', TRUE);
      $user_type = $this->input->post('user_type', TRUE);
      $cust_name = $this->input->post('cust_name', TRUE);
      $deliverer_name = $this->input->post('deliverer_name', TRUE);
      $file_type = $this->input->post('file_type', TRUE);
      
      if( !empty($_FILES["attachement"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('attachement')) {   
          $uploads    = $this->upload->data();  
          $attachement =  $config['upload_path'].$uploads["file_name"];  
        } 
      } else { $attachement = "NULL"; }

      if( !empty($_FILES["thumbnail"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('thumbnail')) {   
          $uploads    = $this->upload->data();  
          $thumbnail =  $config['upload_path'].$uploads["file_name"];  
        } 
      } else { $thumbnail = "NULL"; }

      if( $ow_id = $this->api->post_to_workroom($order_id, $cust_id, $deliverer_id, $text_msg, $user_type, 'chat', $attachement, $cust_name, $deliverer_name, $file_type, $thumbnail)){  
        $chat_details = $this->api->get_chat_details($ow_id);
        //var_dump($chat_details); die();
        //Get Users device Details
        //$device_details = $this->api->get_user_device_details($sender_id);
        if($user_type == 'sp') {
          $device_details = $this->api->get_user_device_details($cust_id);
        } else {
          $device_details = $this->api->get_user_device_details($deliverer_id);
        }

        //Get Customer Device Reg ID's
        $arr_fcm_ids = array();
        $arr_apn_ids = array();
        foreach ($device_details as $value) {
          if($value['device_type'] == 1) {
            array_push($arr_fcm_ids, $value['reg_id']);
          } else {
            array_push($arr_apn_ids, $value['reg_id']);
          }
        }
        $fcm_ids = $arr_fcm_ids;
        $apn_ids = $arr_apn_ids;

        //Get PN API Keys and PEM files
        $api_key_customer = $this->config->item('delivererAppGoogleKey');
        //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

        $msg =  array('title' => 'Gonagoo - New message', 'type' => 'chat', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $chat_details['text_msg'], 'attachment' => $chat_details['attachment_url'], 'cust_name' => $cust_name, 'deliverer_name' => $deliverer_name, 'order_id' => $order_id, 'cust_id' => $cust_id, 'deliverer_id' => $deliverer_id, 'file_type' => $file_type, 'thumbnail' => $thumbnail);
        $this->api->sendFCM($msg, $fcm_ids, $api_key_customer);
        //Push Notification APN
        $msg_apn_customer =  array('title' => 'Gonagoo - New message', 'text' => $chat_details['text_msg']);
        if(is_array($apn_ids)) { 
          $apn_ids = implode(',', $apn_ids);
          $this->notification->sendPushIOS($msg_apn_customer, $apn_ids);
        }

        $this->response = ['response' => 'success','message'=> $this->lang->line('chat_posted_success'), "chat_details" => $chat_details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('chat_posted_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_workroom_chat()
  {
    if(empty($this->errors)){ 
      $order_id = $this->input->post('order_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $workroomchat_list = $this->api->get_workroom_chat($order_id, $last_id);
      if(empty($workroomchat_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'workroomchat_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'workroomchat_list' => $workroomchat_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function customer_chat_post()
  {
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE);
      $cd_id = $this->input->post('cd_id', TRUE);
      $order_id = $this->input->post('order_id', TRUE);
      $text_msg = $this->input->post('text_msg', TRUE);
      $file_type = $this->input->post('file_type', TRUE);
      $cd_name = $this->input->post('cd_name', TRUE);
      $cust_name = $this->input->post('cust_name', TRUE);
      
      if( !empty($_FILES["attachement"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('attachement')) {   
          $uploads    = $this->upload->data();  
          $attachement =  $config['upload_path'].$uploads["file_name"];  
        } 
      } else { $attachement = "NULL"; }

      $insert_data = array(
        "sender_id" => (int)$cust_id,
        "receiver_id" => (int)$cd_id,
        "cust_id" => (int)$cust_id,
        "order_id" => (int)$order_id,
        "text_msg" => $text_msg,
        "attachment_url" => $attachement,
        "cre_datetime" => date('Y-m-d H:i:s'),
        "file_type" => $file_type,
        "cd_name" => $cd_name,
        "cust_name" => $cust_name,
      );

      if( $chat_id = $this->api->customer_chat_post($insert_data)){  
        $chat_details = $this->api->get_customer_chat_details($chat_id);

        //Get Users device Details
        $device_details_driver = $this->api->get_driver_device_details($cd_id);
        //Get Customer Device Reg ID's
        $arr_driver_fcm_ids = array();
        $arr_driver_apn_ids = array();
        foreach ($device_details_driver as $value) {
          if($value['device_type'] == 1) {
            array_push($arr_driver_fcm_ids, $value['reg_id']);
          } else {
            array_push($arr_driver_apn_ids, $value['reg_id']);
          }
        }
        $driver_fcm_ids = $arr_driver_fcm_ids;
        $driver_apn_ids = $arr_driver_apn_ids;

        //Get PN API Keys and PEM files
        $api_key_driver = $this->config->item('driverAppGoogleKey');
        //$api_key_deliverer_pem = $this->config->item('driverAppPemFile');

        $msg =  array('title' => 'Gonagoo - New message.', 'type' => 'chat', 'notice_date' => date("Y-m-d H:i:s"), 'desc' => $text_msg, 'attachment' => $attachement, 'cd_name' => $cd_name, 'cd_id' => $cd_id, 'cust_name' => $cust_name, 'cust_id' => $cust_id, 'order_id' => $order_id, 'file_type' => $file_type);
        $this->api->sendFCM($msg, $driver_fcm_ids, $api_key_driver);
        //Push Notification APN
        $msg_apn_driver =  array('title' => 'Gonagoo - New message.', 'text' => $text_msg);
        if(is_array($driver_apn_ids)) { 
          $driver_apn_ids = implode(',', $driver_apn_ids);
          $this->notification->sendPushIOS($msg_apn_driver, $driver_apn_ids);
        }

        $this->response = ['response' => 'success','message'=> $this->lang->line('chat_posted_success'), "chat_details" => $chat_details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('chat_posted_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function customer_chat_history()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $cd_id = $this->input->post('cd_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $order_id = $this->input->post('order_id', TRUE);
      $customer_chat_list = $this->api->customer_chat_history($cd_id, $cust_id, $last_id, $order_id);
      if(empty($customer_chat_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'customer_chat_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'customer_chat_list' => $customer_chat_list]; 
      }
      echo json_encode($this->response);
    }
  }

  public function courier_order_payment_update()
  {
    if(empty($this->errors)){
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('transaction_id', TRUE);
      $update_data = array(
        "payment_method" => trim($payment_method),
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => date('Y-m-d H:i:s'),
      );
      if( $this->api->courier_order_payment_update($order_id, $update_data) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function update_courier_order()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    if($this->api->is_user_active_or_exists($cust_id)) { 
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      if($detail = $this->api->get_order_detail($order_id)){
        if($detail['order_status'] == 'open') { 
          $from_address = $this->input->post('from_address', TRUE); 
          $from_address_name = $this->input->post('from_address_name', TRUE); 
          $from_address_contact = $this->input->post('from_address_contact', TRUE); 
          $to_address = $this->input->post('to_address', TRUE); 
          $to_address_name = $this->input->post('to_address_name', TRUE); 
          $to_address_contact = $this->input->post('to_address_contact', TRUE); 
          $from_country_id = $this->input->post('from_country_id', TRUE); 
          $from_state_id = $this->input->post('from_state_id', TRUE); 
          $from_city_id = $this->input->post('from_city_id', TRUE); 
          $dimension_id = $this->input->post('dimension_id', TRUE); 
          $from_latitude = $this->input->post('from_latitude', TRUE); 
          $from_longitude = $this->input->post('from_longitude', TRUE); 
          $to_latitude = $this->input->post('to_latitude', TRUE); 
          $to_longitude = $this->input->post('to_longitude', TRUE); 
          $pickup_datetime = $this->input->post('pickup_datetime', TRUE); 
          $delivery_datetime = $this->input->post('delivery_datetime', TRUE); 
          $vehicle_id = $this->input->post('vehicle_id', TRUE); 
          $order_type = $this->input->post('order_type', TRUE);     
          $order_contents = $this->input->post('order_contents', TRUE);     
          $total_quantity = $this->input->post('total_quantity', TRUE);     
          $width = $this->input->post('width', TRUE);     
          $height = $this->input->post('height', TRUE);     
          $length = $this->input->post('length', TRUE);     
          $volume = $this->input->post('volume', TRUE);               
          $total_volume = $this->input->post('total_volume', TRUE);               
          $total_weight = $this->input->post('total_weight', TRUE);     
          $unit_id = $this->input->post('unit_id', TRUE);     
          $order_description = $this->input->post('order_description', TRUE);     
          $order_price = $this->input->post('order_price', TRUE);     
          $currency_id = $this->input->post('currency_id', TRUE);     
          $currency_sign = $this->input->post('currency_sign', TRUE);     
          $currency_title = $this->input->post('currency_title', TRUE);     
          $advance_payment = $this->input->post('advance_payment', TRUE);     
          $pickup_payment = $this->input->post('pickup_payment', TRUE);     
          $deliver_payment = $this->input->post('deliver_payment', TRUE);   
          $no_of_days = $this->input->post('no_of_days', TRUE); $no_of_days = (int) $no_of_days;
          $cust_name = $this->input->post('cust_name', TRUE); 
          $insurance = $this->input->post('insurance', TRUE); 
          $package_value = $this->input->post('package_value', TRUE); 
          $insurance_fee = $this->input->post('insurance_fee', TRUE); 
          $handling_by = $this->input->post('handling_by', TRUE); 
          $dedicated_vehicle = $this->input->post('dedicated_vehicle', TRUE); 
          $with_driver = $this->input->post('with_driver', TRUE); 
          $ref_no = $this->input->post('ref_no', TRUE);
          $expiry_date = $this->input->post('expiry_date', TRUE); 
          $deliver_instructions = $this->input->post('deliver_instructions', TRUE); 
          if(empty($deliver_instructions)) $deliver_instructions = "NULL";
          $pickup_instructions = $this->input->post('pickup_instructions', TRUE); 
          if(empty($pickup_instructions)) $pickup_instructions = "NULL";
          $service_area_type = $this->input->post('service_area_type', TRUE);
          $to_addr_type = $this->input->post('to_addr_type', TRUE); 
          $from_addr_type = $this->input->post('from_addr_type', TRUE); 
          $to_relay_id = $this->input->post('to_relay_id', TRUE); 
          $from_relay_id = $this->input->post('from_relay_id', TRUE);  

          $distance_in_km = $this->api->GetDrivingDistance($from_latitude,$from_longitude, $to_latitude,$to_longitude);
          $cal_params = array(
            "width" => $width,
            "height" => $height,
            "length" => $length,
            "total_weight" => $total_weight,
            "unit_id" => $unit_id,
            "no_of_days" => $no_of_days,
            "country_id" => (int) $from_country_id,
            "distance_in_km" => (string) $distance_in_km,
          );

          if( $result = $this->api->calculate_order_price($cal_params)){  
            $order_price = $result['service_rate']; 
            $currency_id = $result['currency_id']; 
            $currency_sign = $result['currency_sign']; 
            $currency_title = $result['currency_title']; 
          } else { 
            $order_price = $order_price; 
            $currency_id = $currency_id; 
            $currency_sign = $currency_sign; 
            $currency_title = $currency_title; 
          }

          $old_picture_url = $detail['order_picture_url'];
          
          if( !empty($_FILES["order_picture"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/booking-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('order_picture')) {   
              $uploads = $this->upload->data();  
              $order_picture_url =  $config['upload_path'].$uploads["file_name"];  
              unlink(trim($old_picture_url));
            } else { $order_picture_url = $old_picture_url; }
          } else { $order_picture_url = $old_picture_url; }

          $update_data = array(
            "cust_id" => $cust_id,
            "from_address" => $from_address,
            "from_address_name" => $from_address_name,
            "from_address_contact" => $from_address_contact,
            "to_address" => $to_address,
            "to_address_name" => $to_address_name,
            "to_address_contact" => $to_address_contact,
            "from_latitude" => $from_latitude,
            "from_longitude" => $from_longitude,
            "to_latitude" => $to_latitude,
            "to_longitude" => $to_longitude,                
            "from_country_id" => $from_country_id,
            "from_state_id" => $from_state_id,
            "from_city_id" => $from_city_id,
            "dimension_id" => $dimension_id,
            "distance_in_km" => $distance_in_km,
            "pickup_datetime" => $pickup_datetime,
            "delivery_datetime" => $delivery_datetime,
            "vehicle_id" => $vehicle_id,
            "order_type" => $order_type,
            "order_contents" => $order_contents,
            "total_quantity" => $total_quantity,
            "width" => $width,
            "height" => $height,
            "length" => $length,
            "volume" => $volume,
            "total_volume" => $total_volume,
            "total_weight" => $total_weight,
            "order_picture_url" => $order_picture_url,
            "order_description" => nl2br($order_description),
            "order_price" => trim($order_price),
            "order_status" => "open",
            "advance_payment" => $advance_payment,
            "pickup_payment" => $pickup_payment,
            "deliver_payment" => $deliver_payment,
            "cre_datetime" => date('Y-m-d H:i:s'),
            "cust_id" => (int) $cust_id,
            "unit_id" => (int) $unit_id,
            "deliverer_id" => 0,
            "accept_datetime" => "NULL",
            "driver_status_update" => "NULL",
            "driver_id" => 0,
            "assigned_datetime" => "NULL",
            "delivery_code" => "NULL",
            "delivered_datetime" => "NULL",
            "review_comment" => "NULL",
            "rating" => "NULL",
            "review_datetime" => "NULL",
            "status_updated_by" => "NULL",
            "delivery_notes" => "NULL",
            "currency_id" => $currency_id,
            "currency_sign" => $currency_sign,
            "currency_title" => $currency_title,
            "invoice_url" => "NULL",
            "cust_name" => $cust_name,
            "insurance" => (int)$insurance,
            "package_value" => $package_value,
            "insurance_fee" => $insurance_fee,
            "handling_by" => (int)$handling_by,
            "dedicated_vehicle" => (int)$dedicated_vehicle,
            "with_driver" => (int)$with_driver,
            "ref_no" => $ref_no,
            "service_area_type" => $service_area_type,
            "expiry_date" => $expiry_date,
            "deliver_instructions" => $deliver_instructions,
            "pickup_instructions" => $pickup_instructions,
            "to_addr_type" => (int)$to_addr_type,
            "from_addr_type" => (int)$from_addr_type,
            "to_relay_id" => (int)$to_relay_id,
            "from_relay_id" => (int)$from_relay_id,
          );

          if( $this->api->update_courier_orders($update_data, $order_id) ) {
            $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }

        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('already_assigned')]; }

      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('no_order_found').'...']; }

    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }

    echo json_encode($this->response);      
  }

  public function cancel_courier_order_old()
  {
    if(empty($this->errors)){
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $detail = $this->api->get_order_detail($order_id);
      if($detail['order_status'] == 'open' || $detail['driver_status_update'] == 'NULL' || $detail['order_status'] == 'assign') {
        echo $status = $detail['order_status'];
        if( $this->api->cancel_courier_order($order_id) ) {
          if($status == 'open' || $status == 'NULL') {
            //Payment Module - transfer complete payment to customer main account.---------------------------------------------------
            $today = date('Y-m-d h:i:s');
            //deduct refund amount from gonagoo account
            $gonagoo_master_details = $this->api->gonagoo_master_details();
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($detail['order_price']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details();

            $update_data_gonagoo_history = array(
              "gonagoo_id" => 1,
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 0,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($detail['order_price']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

            //Add refund amount in customer account
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id);
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($detail['order_price']);
            $this->api->update_payment_in_customer_master($cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id);

            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($detail['order_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);

            //post payment in workroom
            $this->api->post_to_workroom($order_id, trim($detail['deliverer_id']), $cust_id, $this->lang->line('order_cancel_refund'), 'sr', 'payment', 'NULL', trim($detail['cust_name']), trim($detail['deliverer_name']), 'NULL', 'NULL', 'NULL');
            //Payment Module End ----------------------------------------------------------
          } else {
            //transfer complete payment to customer main account, deduct gongagoo commmission from customer account, add commission amount in gonagoo.
            $today = date('Y-m-d h:i:s');
            $detail = $this->api->get_order_detail($order_id);
            //deduct refund amount from gonagoo account
            $gonagoo_master_details = $this->api->gonagoo_master_details();
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($detail['order_price']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details();

            $update_data_gonagoo_history = array(
              "gonagoo_id" => 1,
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 0,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($detail['order_price']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

            //Add refund amount in customer account
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id);
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($detail['order_price']);
            $this->api->update_payment_in_customer_master($cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id);

            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'cancel_refund',
              "amount" => trim($detail['order_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);

            //Deduct commission from customer account master and history
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($detail['commission_amount']);
            $this->api->update_payment_in_customer_master($cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id);

            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'commission_amount',
              "amount" => trim($detail['commission_amount']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "cat_id" => trim($detail['category_id']),
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);

            //add deiverer commission to gonagoo account master and history
            $gonagoo_master_details = $this->api->gonagoo_master_details();
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($detail['commission_amount']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details();

            $update_data_gonagoo_history = array(
              "gonagoo_id" => 1,
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'commission_amount',
              "amount" => trim($detail['commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            
            //send commission invoice to deliverer
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4','',$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference("$order_id");
              $invoice->setDate(date('M dS ,Y',time()));
              
              $customer_details = $this->api->get_customer_details($cust_id);
              $customer_country = $this->api->get_country_details(trim($customer_details['country_id']));
              $customer_state = $this->api->get_state_details(trim($customer_details['state_id']));
              $customer_city = $this->api->get_city_details(trim($customer_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($customer_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setFrom(array(trim($customer_details['firstname']) . " " . trim($customer_details['lastname']),trim($customer_city['city_name']),trim($customer_state['state_name']),trim($customer_country['country_name']),trim($customer_details['email1'])));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              /* Adding Items in table */
              $order_for = $this->lang->line('gonagoo_commission');
              $rate = round(trim($detail['commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('commission_paid'));

              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              //$invoice->addTitle("Other Details");
              /* Add Paragraph */
              //$invoice->addParagraph("Any Other Details");
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($order_id.'_refund_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

              //Update File path
              $pdf_name = $order_id.'_refund_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/order-invoices/".$pdf_name);
              
              //Update Order Invoice and post to workroom
              $this->api->update_deliverer_invoice_url($order_id, "order-invoices/".$pdf_name);
            /* ----------------------------------------------------- */
            /* -----------------Email Invoice to customer--------------------- */
              $eol = PHP_EOL;
              $messageBody = $eol . $this->lang->line('invoice_email_message') . $eol;
              $file = "resources/order-invoices/".$pdf_name;
              $file_size = filesize($file);
              $handle = fopen($file, "r");
              $content = fread($handle, $file_size);
              fclose($handle);
              $content = chunk_split(base64_encode($content));
              $uid = md5(uniqid(time()));
              $name = basename($file);
              //$eol = PHP_EOL;
              $from_mail = $this->config->item('from_email');
              $replyto = $this->config->item('from_email');
              // Basic headers
              $header = "From: ".$gonagoo_address['company_name']." <".$from_mail.">".$eol;
              $header .= "Reply-To: ".$replyto.$eol;
              $header .= "MIME-Version: 1.0\r\n";
              $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";

              // Put everything else in $message
              $message = "--".$uid.$eol;
              $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
              $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
              $message .= $messageBody.$eol;
              $message .= "--".$uid.$eol;
              $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
              $message .= "Content-Transfer-Encoding: base64".$eol;
              $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
              $message .= $content.$eol;
              $message .= "--".$uid."--";

              mail($customer_details['email1'], $this->lang->line('commission_invoice_email_subject'), $message, $header);
            /* -----------------------Email Invoice to customer----------------------- */

            //post payment in workroom
            $this->api->post_to_workroom($order_id, trim($customer_details['cust_id']), $cust_id, $this->lang->line('advance_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($detail['deliverer_name']), 'NULL', 'NULL', 'NULL');
            //Payment Module End ----------------------------------------------------------
          }
          $this->api->insert_order_status_customer($cust_id, $order_id, 'cancel');
          $this->api->remove_order_request($order_id, $cust_id);
          $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
        } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_accepted_already')]; }
      echo json_encode($this->response);
    }
  }

  public function cancel_courier_order()
  {
    //echo json_encode($_POST); die();
    if(empty($this->errors)){
      $order_id = $this->input->post('order_id', TRUE);
      $detail = $this->api->get_order_detail((int)$order_id);
      $cust_id = $detail['cust_id']; $cust_id = (int)$cust_id;
      if($detail['order_status'] == 'open' || $detail['driver_status_update'] == 'NULL' || $detail['order_status'] == 'assign') {
        $status = $detail['order_status'];
        if( $this->api->cancel_courier_order((int)$order_id) ) {
          if($detail['is_bank_payment'] == 0) {
            if( ($status == 'open' || $status == 'NULL') && $detail['is_laundry'] == 0 ) {
              //Payment Module - transfer complete payment to customer main account----------
                $today = date('Y-m-d h:i:s');
                //deduct refund amount from gonagoo account
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));
                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($detail['order_price']);
                $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 0,
                  "transaction_type" => 'cancel_refund',
                  "amount" => trim($detail['order_price']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => 'NULL',
                  "transfer_account_number" => 'NULL',
                  "bank_name" => 'NULL',
                  "account_holder_name" => 'NULL',
                  "iban" => 'NULL',
                  "email_address" => 'NULL',
                  "mobile_number" => 'NULL',
                  "transaction_id" => 'NULL',
                  "currency_code" => trim($detail['currency_sign']),
                  "country_id" => trim($detail['from_country_id']),
                  "cat_id" => trim($detail['category_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

                //Add refund amount in customer account
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($detail['order_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));

                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'cancel_refund',
                  "amount" => trim($detail['order_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($detail['currency_sign']),
                  "cat_id" => trim($detail['category_id']),
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);

                //post payment in workroom
                $this->api->post_to_workroom($order_id, trim($detail['deliverer_id']), $cust_id, $this->lang->line('order_cancel_refund'), 'sr', 'payment', 'NULL', trim($detail['cust_name']), trim($detail['deliverer_name']), 'NULL', 'NULL', 'NULL');
              //Payment Module End ----------------------------------------------------------
            } else {
              if($detail['is_laundry'] == 1 ) { 
                //transfer complete payment to customer main account, deduct gongagoo commmission from customer account, add commission amount in gonagoo.
                  $today = date('Y-m-d h:i:s');
                  $detail = $this->api->get_order_detail($order_id);
                  //deduct refund amount from gonagoo account
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));
                  $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($detail['order_price']);
                  $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 0,
                    "transaction_type" => 'cancel_refund',
                    "amount" => trim($detail['order_price']),
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'NULL',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => 'NULL',
                    "currency_code" => trim($detail['currency_sign']),
                    "country_id" => trim($detail['from_country_id']),
                    "cat_id" => trim($detail['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

                  //Add refund amount in customer account
                  $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));
                  $account_balance = trim($customer_account_master_details['account_balance']) + trim($detail['order_price']);
                  $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
                  $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));

                  $update_data_account_history = array(
                    "account_id" => (int)$customer_account_master_details['account_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "datetime" => $today,
                    "type" => 1,
                    "transaction_type" => 'cancel_refund',
                    "amount" => trim($detail['order_price']),
                    "account_balance" => trim($customer_account_master_details['account_balance']),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($detail['currency_sign']),
                    "cat_id" => trim($detail['category_id']),
                  );
                  $this->api->insert_payment_in_account_history($update_data_account_history);

                  //Deduct commission from customer account master and history
                  $account_balance = trim($customer_account_master_details['account_balance']) - trim($detail['commission_amount']);
                  $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
                  $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($detail['currency_sign']));

                  $update_data_account_history = array(
                    "account_id" => (int)$customer_account_master_details['account_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "datetime" => $today,
                    "type" => 0,
                    "transaction_type" => 'commission_amount',
                    "amount" => trim($detail['commission_amount']),
                    "account_balance" => trim($customer_account_master_details['account_balance']),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($detail['currency_sign']),
                    "cat_id" => trim($detail['category_id']),
                  );
                  $this->api->insert_payment_in_account_history($update_data_account_history);

                  //add commission to gonagoo account master and history
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));
                  $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($detail['commission_amount']);
                  $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($detail['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 0,
                    "transaction_type" => 'commission_amount',
                    "amount" => trim($detail['commission_amount']),
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'NULL',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => 'NULL',
                    "currency_code" => trim($detail['currency_sign']),
                    "country_id" => trim($detail['from_country_id']),
                    "cat_id" => trim($detail['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                
                  //send commission invoice to deliverer------------------------------------
                    $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                    require_once($phpinvoice);
                    //Language configuration for invoice
                    $lang = $this->input->post('lang', TRUE);
                    if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                    else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                    else { $invoice_lang = "englishApi_lang"; }
                    //Invoice Configuration
                    $invoice = new phpinvoice('A4',trim($detail['currency_sign']),$invoice_lang);
                    $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                    $invoice->setColor("#000");
                    $invoice->setType("");
                    $invoice->setReference("$order_id");
                    $invoice->setDate(date('M dS ,Y',time()));
                    
                    $customer_details = $this->api->get_user_details($cust_id);
                    $customer_country = $this->api->get_country_details(trim($customer_details['country_id']));
                    $customer_state = $this->api->get_state_details(trim($customer_details['state_id']));
                    $customer_city = $this->api->get_city_details(trim($customer_details['city_id']));

                    $gonagoo_address = $this->api->get_gonagoo_address(trim($customer_details['country_id']));
                    $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                    $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                    $invoice->setFrom(array(trim($customer_details['firstname']) . " " . trim($customer_details['lastname']),trim($customer_city['city_name']),trim($customer_state['state_name']),trim($customer_country['country_name']),trim($customer_details['email1'])));

                    $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                    
                    /* Adding Items in table */
                    $order_for = $this->lang->line('gonagoo_commission');
                    $rate = round(trim($detail['commission_amount']),2);
                    $total = $rate;
                    $payment_datetime = substr($today, 0, 10);
                    //set items
                    $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
                    /* Add totals */
                    $invoice->addTotal($this->lang->line('sub_total'),$total);
                    $invoice->addTotal($this->lang->line('taxes'),'0');
                    $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                    /* Set badge */ 
                    $invoice->addBadge($this->lang->line('commission_paid'));
                    /* Add title */
                    $invoice->addTitle($this->lang->line('tnc'));
                    /* Add Paragraph */
                    $invoice->addParagraph($gonagoo_address['terms']);
                    /* Add title */
                    $invoice->addTitle($this->lang->line('payment_dtls'));
                    /* Add Paragraph */
                    $invoice->addParagraph($gonagoo_address['payment_details']);
                    /* Add title */
                    //$invoice->addTitle("Other Details");
                    /* Add Paragraph */
                    //$invoice->addParagraph("Any Other Details");
                    /* Add title */
                    $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                    /* Add Paragraph */
                    $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                    /* Set footer note */
                    $invoice->setFooternote($gonagoo_address['company_name']);
                    /* Render */
                    $invoice->render($order_id.'_refund_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

                    //Update File path
                    $pdf_name = $order_id.'_refund_commission.pdf';
                    //Move file to upload folder
                    rename($pdf_name, "resources/order-invoices/".$pdf_name);
                    
                    //Update Order Invoice and post to workroom
                    $this->api->update_deliverer_invoice_url($order_id, "order-invoices/".$pdf_name);
                  /* ----------------------------------------------------------------------- */
                  /* -----------------Email Invoice to customer----------------------------- */
                    $eol = PHP_EOL;
                    $messageBody = $eol . $this->lang->line('invoice_email_message') . $eol;
                    $file = "resources/order-invoices/".$pdf_name;
                    $file_size = filesize($file);
                    $handle = fopen($file, "r");
                    $content = fread($handle, $file_size);
                    fclose($handle);
                    $content = chunk_split(base64_encode($content));
                    $uid = md5(uniqid(time()));
                    $name = basename($file);
                    //$eol = PHP_EOL;
                    $from_mail = $this->config->item('from_email');
                    $replyto = $this->config->item('from_email');
                    // Basic headers
                    $header = "From: ".$gonagoo_address['company_name']." <".$from_mail.">".$eol;
                    $header .= "Reply-To: ".$replyto.$eol;
                    $header .= "MIME-Version: 1.0\r\n";
                    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";

                    // Put everything else in $message
                    $message = "--".$uid.$eol;
                    $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
                    $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
                    $message .= $messageBody.$eol;
                    $message .= "--".$uid.$eol;
                    $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
                    $message .= "Content-Transfer-Encoding: base64".$eol;
                    $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
                    $message .= $content.$eol;
                    $message .= "--".$uid."--";

                    mail($customer_details['email1'], $this->lang->line('commission_invoice_email_subject'), $message, $header);
                  /* -----------------------Email Invoice to customer----------------------- */

                  //post payment in workroom
                  $this->api->post_to_workroom($order_id, trim($customer_details['cust_id']), $cust_id, $this->lang->line('advance_payment_recieved'), 'sr', 'payment', 'NULL', trim($customer_details['firstname']), trim($detail['deliverer_name']), 'NULL', 'NULL', 'NULL');
                //Payment Module End ----------------------------------------------------------
              }
            }
          }
          $this->api->insert_order_status_customer($cust_id, (int)$order_id, 'cancel');
          $this->response = ['response' => 'success','message'=> $this->lang->line('cancel_success')];
        } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('cancel_failed')]; }
      } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('order_accepted_already')]; }
      echo json_encode($this->response);
    }
  }

  public function get_reviews()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if( $details = $this->api->get_reviews($cust_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "review_list" => $details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function get_insurance_fees()
  {
    if(empty($this->errors)){ 
      $country_id = $this->input->post('country_id', TRUE); $country_id = (int) $country_id;
      $category_id = $this->input->post('category_id', TRUE); $category_id = (int) $category_id;
      $package_value = $this->input->post('package_value', TRUE); $package_value = (int) $package_value;
      if( $details = $this->api->get_insurance_fees($country_id, $package_value, $category_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "result" => $details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function order_status_list()
  {
    if(empty($this->errors)){ 
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      if( $details = $this->api->order_status_list($order_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "result" => $details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function get_deliverer_documents()
  {
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        if( $docs = $this->api->get_deliverer_documents($cust_id) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_documenets" => $docs];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function add_deliverer_document()
  {
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $deliverer_id = $this->input->post('deliverer_id', TRUE); $deliverer_id = (int) $deliverer_id;
      $doc_type = $this->input->post('doc_type', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        
        if( !empty($_FILES["attachement"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/attachements/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('attachement')) {   
            $uploads    = $this->upload->data();  
            $attachement_url =  $config['upload_path'].$uploads["file_name"];  
          }
          $register_data = array(
            "cust_id" => $cust_id,
            "deliverer_id" => $deliverer_id,
            "attachement_url" => trim($attachement_url),
            "is_verified" => 0,
            "is_rejected" => 0,
            "doc_type" => trim($doc_type),
            "upload_datetime" => date('Y-m-d H:i:s'),
            "verify_datetime" => "NULL",
          );
          if( $id = $this->api->register_deliverer_document($register_data) ) {
            $this->response = ['response' => 'success','message'=> $this->lang->line('add_success'), 'doc_id' => $id ];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function update_deliverer_document()
  { 
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id; 
      if($this->api->is_user_active_or_exists($cust_id)){ 
        $doc_id = $this->input->post('doc_id', TRUE); $doc_id = (int) $doc_id;
        
        if( !empty($_FILES["attachement"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/attachements/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('attachement')) {   
            $uploads    = $this->upload->data();  
            $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
          } else{ $attachement_url = "NULL"; }
        } else { $attachement_url = "NULL"; }

        $update_data = array( "attachement_url" => trim($attachement_url), "is_verified" => "0"); 
        $old_doc_url = $this->api->get_deliverer_old_document_url($doc_id);

        if( $this->api->update_deliverer_document($doc_id, $update_data) ) { 
          if($attachement_url != "NULL"){ unlink(trim($old_doc_url)); }
          $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
        } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
  }

  public function delete_deliverer_document()
  {
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        $doc_id = $this->input->post('doc_id', TRUE);  $doc_id = (int) $doc_id;
        $old_doc_url = $this->api->get_deliverer_old_document_url($doc_id);
        if( $this->api->delete_deliverer_document($doc_id) ) {
           unlink(trim($old_doc_url));
          $this->response = ['response' => 'success','message'=> $this->lang->line('remove_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
  }

  public function open_booking_list_old()
  {
    if(empty($this->errors)){ 
      $last_id = $this->input->post('last_id', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE);
      $booking_list = $this->api->get_open_booking_list($last_id, $cust_id);
      
      for ($i=0; $i < sizeof($booking_list); $i++) { 
        $details = $this->api->order_status_list($booking_list[$i]['order_id']);
        $packages = $this->api->get_order_packages($booking_list[$i]['order_id']);
        for ($j=0; $j < sizeof($packages); $j++){
          $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
        }
        $booking_list[$i] += ["status_details" => $details,'packages' => $packages];
      }

      if(empty($booking_list) || is_null($booking_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'open_booking_list' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'open_booking_list' => $booking_list]; 
      }
      echo json_encode($this->response);
    }
  }
    
  public function open_booking_list()
  {
    //echo json_encode($_POST); die();
    if(empty($this->errors)){ 
      $last_id = $this->input->post('last_id', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE);
      $filter_type = $this->input->post('filter_type');
      $user_type = $this->input->post('user_type'); // deliverer
      $order_status = $this->input->post('order_status');
    }
    $filter_type = $this->input->post('filter_type');
    $user_type = $this->input->post('user_type');
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');
    
    //  New params in filter
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');
    $cat_id = $this->input->post('cat_id');
    
    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( $filter_type == 'advance') {
      if(trim($creation_start) != "NULL" && trim($creation_end) != "NULL") {
      $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
      $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
      } else { $creation_start_date = $creation_end_date = "NULL"; }
      
      if(trim($pickup_start) != "NULL" && trim($pickup_end) != "NULL") {
      $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
      $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
      } else { $pickup_start_date = $pickup_end_date = "NULL"; }
      
      if(trim($delivery_start) != "NULL" && trim($delivery_end) != "NULL") {
      $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
      $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
      } else { $delivery_start_date = $delivery_end_date = "NULL"; }
      
      if(trim($expiry_start) != "NULL" && trim($expiry_end) != "NULL") {
      $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
      $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
      } else { $expiry_start_date = $expiry_end_date = "NULL"; }
      
      $filter_array = array(
        "user_id" => $cust_id,
        "user_type" => $user_type,
        "order_status" => $order_status,
        "from_country_id" => ($from_country_id!=0)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=0)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=0)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=0)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=0)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=0)? $to_city_id: "0",
        "from_address" => ($from_address!="NULL")? $from_address: "NULL",
        "to_address" => ($to_address!="NULL")? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!="NULL")? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!="NULL")? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!="NULL")? $pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!="NULL")? $pickup_end_date: "NULL",
        "price" => ($price!=0)?$price:"0",
        "dimension_id" => ($dimension_id!=0)?$dimension_id:"0",
        "order_type" => ($order_type!="NULL")? $order_type: "NULL",
        // new filters
        "creation_start_date" => ($creation_start_date!="NULL")? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!="NULL")? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!="NULL")? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!="NULL")? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=0)? $distance_start: "0",
        "distance_end" => ($distance_end!=0)? $distance_end: "0",
        "transport_type" => ($transport_type!="NULL")? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=0)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=0)?$max_weight:"0",
        "unit_id" => ($unit_id != 0)?$unit_id:"0",
        "cat_id" => ($cat_id != 0)?$cat_id:"0",
      );

      $booking_list = $this->user->filtered_list_market_place($filter_array); 
      //echo $this->db->last_query(); die();
    } else {
      $booking_list = $this->api->get_open_booking_list($last_id, $cust_id);
    }

    if(!empty($booking_list)){
      for ($i=0; $i < sizeof($booking_list); $i++) { 
        if($booking_list[$i]['visible_by'] == "0" || !empty($this->api->get_favourite_deliverers_as_customer($booking_list[$i]['cust_id'], $cust_id))) {
          $details = $this->api->order_status_list($booking_list[$i]['order_id']);
          $packages = $this->api->get_order_packages($booking_list[$i]['order_id']);
          for ($j=0; $j < sizeof($packages); $j++){
            $packages[$j]['dangrous_goods_name'] = $this->api->get_dangerous_goods($packages[$j]['dangerous_goods_id'])['name'];
          }
          $booking_list[$i] += ["status_details" => $details,'packages' => $packages];
        } else {
          array_splice($booking_list, $i, 1);
        }
      }
    }
    if(empty($booking_list) || is_null($booking_list)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), 'open_booking_list' => array()]; 
    } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'open_booking_list' => $booking_list]; 
    }
    echo json_encode($this->response);
  }

  public function get_relay_point_list()
  {
    if(empty($this->errors)){ 
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      $city_id = $this->input->post('city_id', TRUE);
      $filter = array('country_id' => $country_id, 'state_id' => $state_id, 'city_id' => $city_id);
        if( $details = $this->api->get_relay_point_list($filter) ) {
        //echo $this->db->last_query(); die();
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "result" => $details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function verify_otp()
  {
    $cust_id = $this->input->post('cust_id');
    $OTP = $this->input->post('otp');
    $cust = $this->api->get_consumer_datails($cust_id,true);
    if(!$cust['mobile_verified']) {
      if( $cust['cust_otp'] == $OTP ) {       
        if($this->api->update_verified_email_or_otp($cust_id, 'otp')){
          $this->response = ['response' => 'success','message'=> $this->lang->line('otp_success')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('verification_failed')]; }   
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('otp_not_matched')]; }   
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('otp_already_verified')]; }
    echo json_encode($this->response);
  }

  public function resend_otp()
  {
    $cust_id = $this->input->post('cust_id');
    $cust = $this->api->get_consumer_datails($cust_id,true);
    if(is_array($cust) AND $cust['cust_status'] == 1 ) {
      $user = $this->api->get_user_details(trim($cust_id));
      $country_code = $this->api->get_country_code_by_id($user['country_id']);
      if(substr($cust['mobile1'], 0, 1) == 0) { $phone_no = ltrim($cust['mobile1'], 0); } else { $phone_no = $cust['mobile1']; }
      if($this->api->sendSMS($country_code.$phone_no, $this->lang->line('forgot_otp_sms_message').' '.$cust['cust_otp'])){
        $this->response = ['response' => 'success','message'=> $this->lang->line('resend_otp_success')];          
      } else{  $this->response = ['response' => 'failed', 'message' => $this->lang->line('sms_sending_failed')]; }   
    } else{  $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')];  }   
    echo json_encode($this->response);
  }

  // Notification counts and list
  public function notification_counts()
  {
    $cust_id = $this->input->post('cust_id'); 
    if( $this->api->is_user_active_or_exists($cust_id)) { 
      $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust_id);
      $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust_id);
      $total_chatroom_notifications = $this->api->get_total_unread_chatroom_notifications_count($cust_id);  
      $this->response = array(
        'response' => 'success',
        'message'=> $this->lang->line('get_list_success'),
        'admin_count'=> $total_unread_notifications,
        'workroom_count' => $total_workroom_notifications,
        'chatroom_count' => $total_chatroom_notifications,
      );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);
  }
  
  public function get_notifications()
  {
    $cust_id = $this->input->post('cust_id');   
    if( $this->api->is_user_active_or_exists($cust_id)) {
      $cust = $this->api->get_consumer_datails($cust_id,true);
      if($cust['user_type'] == 1) { $type = "Individuals"; } else { $type = "Business"; }     
      $last_id = $this->input->post('last_id');
      $notifications = $this->api->get_notifications($cust_id,$type, 50, $last_id);
      $this->response = array(
        'response' => 'success',
        'message'=> $this->lang->line('get_list_success'),
        'admin_notifications'=> $notifications,
      );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);  
  }
  
  public function unread_notifications()
  {
    $cust_id = $this->input->post('cust_id');   
    if( $this->api->is_user_active_or_exists($cust_id)) {
      $notifications = $this->api->get_total_unread_notifications($cust_id,1000);
      $this->response = array(
        'response' => 'success',
        'message'=> $this->lang->line('get_list_success'),
        'admin_notifications'=> $notifications,
      );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);
  }

  public function unread_workroom_notifications()
  {
    $cust_id = $this->input->post('cust_id');   
    if( $this->api->is_user_active_or_exists($cust_id)) {
      $notifications = $this->api->get_total_unread_workroom_notifications($cust_id,1000);
      $this->response = array(
        'response' => 'success',
        'message'=> $this->lang->line('get_list_success'),
        'workroom_notifications'=> $notifications,
      );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);
  }

  public function unread_chatroom_notifications()
  {
    $cust_id = $this->input->post('cust_id');   
    if( $this->api->is_user_active_or_exists($cust_id)) {
      $notifications = $this->api->get_total_unread_chatroom_notifications($cust_id,1000);
      $this->response = array(
        'response' => 'success',
        'message'=> $this->lang->line('get_list_success'),
        'chatroom_notifications'=> $notifications,
      );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);
  }

  public function make_notification_read()
  {
    $cust_id = $this->input->post('cust_id');   
    if( $this->api->is_user_active_or_exists($cust_id)) {
      $notification = $this->api->get_total_unread_notifications($cust_id);
      foreach ($notification as $n) { $this->api->make_notification_read($n['id'], $cust_id); }
      $this->response = array( 'response' => 'success' );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);
  }

  public function make_workroom_notification_read()
  {
    $cust_id = $this->input->post('cust_id');   
    if( $this->api->is_user_active_or_exists($cust_id)) {
      $order_id = $this->input->post('order_id');   
      $ids = $this->api->get_total_unread_order_workroom_notifications($order_id, $cust_id);        
      foreach ($ids as $id => $v) {  $this->api->make_workroom_notification_read($v['ow_id'], $cust_id);  }
      $this->response = array(  'response' => 'success' );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);
  }

  public function make_chatroom_notification_read()
  {
    $cust_id = $this->input->post('cust_id');   
    if( $this->api->is_user_active_or_exists($cust_id)) {
      $order_id = $this->input->post('order_id');   
      $ids = $this->api->get_total_unread_order_chatroom_notifications($order_id, $cust_id);        
      foreach ($ids as $id => $v) {  $this->api->make_chatrooom_notification_read($v['chat_id'], $cust_id); }
      $this->response = array(  'response' => 'success' );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);
  }

  public function workroom_order_list()
  {
    $cust_id = $this->input->post('cust_id');   
    if( $this->api->is_user_active_or_exists($cust_id)) {
      $data = $this->api->get_workorder_list($cust_id);     
      $this->response = array(  'response' => 'success', 'order_list' => $data );
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
    echo json_encode($this->response);
  }

  public function update_order_payment_by_customer($data=null)
  {
    if(!is_null($data)){
      $cust_id = (int) $data['cust_id'];
      $order_id = (int) $data['order_id'];
      $payment_method = trim($data['payment_method']);
      $transaction_id = trim($data['transaction_id']);
      $today = date('Y-m-d h:i:s');
      $order_details = $this->api->get_order_detail($order_id);
      $total_amount_paid = trim($data['total_amount_paid']);
      $transfer_account_number = trim($data['transfer_account_number']);
      $bank_name = trim($data['bank_name']);
      $account_holder_name = trim($data['account_holder_name']);
      $iban = trim($data['iban']);
      $email_address = trim($data['email_address']);
      $mobile_number = trim($data['mobile_number']);
      $currency_sign = trim($data['currency_sign']);

      $update_data_order = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "complete_paid" => "1",
      );

      if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {
        /*************************************** Payment Section ****************************************/
          //Add core price to gonagoo master
          if($this->api->gonagoo_master_details(trim($data['currency_sign']))) {
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($data['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'standard_price',
            "amount" => trim($order_details['standard_price']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($data['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          
          //Update urgent fee in gonagoo account master
          if(trim($order_details['urgent_fee']) > 0) {
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'urgent_fee',
              "amount" => trim($order_details['urgent_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update insurance fee in gonagoo account master
          if(trim($order_details['insurance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'insurance_fee',
              "amount" => trim($order_details['insurance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update handling fee in gonagoo account master
          if(trim($order_details['handling_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'handling_fee',
              "amount" => trim($order_details['handling_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update vehicle fee in gonagoo account master
          if(trim($order_details['vehicle_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($data['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'vehicle_fee',
              "amount" => trim($order_details['vehicle_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($data['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Add payment details in customer account master and history
          if($this->api->customer_account_master_details($cust_id, trim($data['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($data['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
          }

          $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));
   
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($data['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Deduct payment details from customer account master and history
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($data['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($data['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        /*************************************** Payment Section ****************************************/

        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function update_order_payment_by_customer__old()
  {
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      $advance_payment = $this->input->post('advance_payment', TRUE);
      $pickup_payment = $this->input->post('pickup_payment', TRUE);
      $deliver_payment = $this->input->post('deliver_payment', TRUE);
      $balance_amount = $this->input->post('balance_amount', TRUE); //555
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('transaction_id', TRUE);
      $today = date('Y-m-d h:i:s');
      $order_price = $this->input->post('order_price', TRUE);
      $urgent_fee = $this->input->post('urgent_fee', TRUE);
      $insurance_fee = $this->input->post('insurance_fee', TRUE);
      $handling_fee = $this->input->post('handling_fee', TRUE);
      $vehicle_fee = $this->input->post('vehicle_fee', TRUE);
      $total_amount_paid = $this->input->post('total_amount_paid', TRUE);
      $transfer_account_number = $this->input->post('transfer_account_number', TRUE);
      $bank_name = $this->input->post('bank_name', TRUE);
      $account_holder_name = $this->input->post('account_holder_name', TRUE);
      $iban = $this->input->post('iban', TRUE);
      $email_address = $this->input->post('email_address', TRUE);
      $mobile_number = $this->input->post('mobile_number', TRUE);
      $order_details = $this->api->get_order_detail($order_id);

      $update_data_order = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "complete_paid" => "1",
      );

      if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {
        //Add core price to gonagoo master
        $gonagoo_master_details = $this->api->gonagoo_master_details();
        $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_price);
        $this->api->update_payment_in_gonagoo_master($gonagoo_balance);
        $gonagoo_master_details = $this->api->gonagoo_master_details();
        
        $update_data_gonagoo_history = array(
          "gonagoo_id" => 1,
          "order_id" => (int)$order_id,
          "user_id" => (int)$cust_id,
          "type" => 1,
          "transaction_type" => 'order_price',
          "amount" => trim($order_price),
          "datetime" => $today,
          "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
          "transfer_account_type" => trim($payment_method),
          "transfer_account_number" => trim($transfer_account_number),
          "bank_name" => trim($bank_name),
          "account_holder_name" => trim($account_holder_name),
          "iban" => trim($iban),
          "email_address" => trim($email_address),
          "mobile_number" => trim($mobile_number),
          "transaction_id" => trim($transaction_id),
        );
        $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

        //Update urgent fee in gonagoo account master
        if(trim($urgent_fee) > 0) {
          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($urgent_fee);
          $this->api->update_payment_in_gonagoo_master($gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details();

          $update_data_gonagoo_history = array(
            "gonagoo_id" => 1,
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'urgent_fee',
            "amount" => trim($urgent_fee),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

        //Update insurance fee in gonagoo account master
        if(trim($insurance_fee) > 0) {
          $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($insurance_fee);
          $this->api->update_payment_in_gonagoo_master($gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details();
          
          $update_data_gonagoo_history = array(
            "gonagoo_id" => 1,
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'insurance_fee',
            "amount" => trim($insurance_fee),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

        //Update handling fee in gonagoo account master
        if(trim($handling_fee) > 0) {
          $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($handling_fee);
          $this->api->update_payment_in_gonagoo_master($gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details();
          
          $update_data_gonagoo_history = array(
            "gonagoo_id" => 1,
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'handling_fee',
            "amount" => trim($handling_fee),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

        //Update vehicle fee in gonagoo account master
        if(trim($vehicle_fee) > 0) {
          $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($vehicle_fee);
          $this->api->update_payment_in_gonagoo_master($gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details();
          
          $update_data_gonagoo_history = array(
            "gonagoo_id" => 1,
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'vehicle_fee',
            "amount" => trim($vehicle_fee),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        }

        //Add payment details in customer account master and history
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id);
        $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
        $this->api->update_payment_in_customer_master($cust_id, $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id);

        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$order_id,
          "user_id" => (int)$cust_id,
          "datetime" => $today,
          "type" => 1,
          "transaction_type" => 'add',
          "amount" => trim($total_amount_paid),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "cat_id" => trim($order_details['category_id']),
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);

        //Deduct payment details from customer account master and history
        $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
        $this->api->update_payment_in_customer_master($cust_id, $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id);

        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$order_id,
          "user_id" => (int)$cust_id,
          "datetime" => $today,
          "type" => 0,
          "transaction_type" => 'payment',
          "amount" => trim($total_amount_paid),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "cat_id" => trim($order_details['category_id']),
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);

        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')];
      }
      echo json_encode($this->response);
    }
  }

  public function user_account_withdraw_request()
  { 
    if(empty($this->errors)){
      $user_id = $this->input->post('user_id', TRUE); $user_id = (int) $user_id;
      $amount = $this->input->post('amount', TRUE);
      $transfer_account_type = $this->input->post('transfer_account_type', TRUE);
      $transfer_account_number = $this->input->post('transfer_account_number', TRUE);
      $user_type = $this->input->post('user_type', TRUE); $user_type = (int) $user_type;
      $currency_code = $this->input->post('currency_code', TRUE);

      $register_data = array(
        "user_id" => $user_id,
        "amount" => trim($amount),
        "req_datetime" => date('Y-m-d h:i:s'),
        "user_type" => trim($user_type),
        "response" => "request",
        "response_datetime" => "NULL",
        "reject_remark" => "NULL",
        "transfer_account_type" => $transfer_account_type,
        "transfer_account_number" => $transfer_account_number,
        "transaction_id" => "NULL",
        "amount_transferred" => "0",
        "currency_code" => trim($currency_code),
      );
      
      if($user_type == 0) {
        if($this->api->check_user_account_balance($user_id, $currency_code)) {
          $requested_amount = $this->api->get_customer_account_withdraw_request_sum($user_type, $user_id, 'request');
      
          $user_balance_amount = $this->api->check_user_account_balance($user_id, $currency_code);
 
          $user_balance_amount -= $requested_amount; 
        } else { $user_balance_amount = 0; }

        if($user_balance_amount >= $amount) {
          if( $this->api->user_account_withdraw_request($register_data) ) {
              $this->response = ['response' => 'success','message'=> $this->lang->line('request_success')];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('request_failed')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('request_failed')]; }  
      } else if($user_type == 1) { 
        if($this->api->check_relay_account_balance($user_id, $currency_code)) {
          $requested_amount = $this->api->get_customer_account_withdraw_request_sum($user_type, $user_id, 'request');
          $user_balance_amount = $this->api->check_relay_account_balance($user_id, $currency_code);
          $user_balance_amount -= $requested_amount;
        } else { $user_balance_amount = 0; }
        
        if($user_balance_amount >= $amount) {
          if( $this->api->user_account_withdraw_request($register_data) ) {
              $this->response = ['response' => 'success','message'=> $this->lang->line('request_success')];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('request_failed')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('request_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('request_failed')]; }
      echo json_encode($this->response);  
    }
  }
  
  public function get_customer_order_using_code()
  {
    if(empty($this->errors)){ 
      $driver_code = $this->input->post('driver_code', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE);
      $code = substr($driver_code, 0, 2);
      $order_details = array();
      $details = array();
      if($code == 'CD') {
        $order_details = $this->api->get_order_details_driver($driver_code, $cust_id);

        if(!empty($order_details)) {

          $details = $this->api->order_status_list($order_details['order_id']);
          $order_details += ["status_details" => $details];
          $this->response = ['response' => 'success', 'message' => $this->lang->line('driver_order_list_success'), 'order_details' => $order_details]; 
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('driver_order_list_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('not_a_driver_code')]; }
      echo json_encode($this->response);
    }
  }

  public function get_customer_account_balance()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $account_master_details = $this->api->customer_account_master_list($cust_id);
      if(!empty($account_master_details)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'account_master_details' => $account_master_details]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_customer_account_history()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $account_history_details = $this->api->get_customer_account_history($cust_id, $last_id);
      if(!empty($account_history_details)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'account_history_details' => $account_history_details]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_customer_scrow_balance()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $scrow_details = $this->api->deliverer_scrow_master_list($cust_id);
      if(!empty($scrow_details)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'scrow_details' => $scrow_details]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_customer_scrow_history()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $scrow_history_details = $this->api->get_customer_scrow_history($cust_id, $last_id);
      if(!empty($scrow_history_details)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'scrow_history_details' => $scrow_history_details]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function get_customer_account_withdraw_request_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      $account_withdraw_request_list = $this->api->get_customer_account_withdraw_request_list($cust_id, $last_id);
      if(!empty($account_withdraw_request_list)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'account_withdraw_request_list' => $account_withdraw_request_list]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function wecashup_payment_gateway()
  { 
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $merchant_uid = 'kZudktr3nxX0AMM633dXyHEesV13';            // Replace with your merchant_uid
    $merchant_public_key = 'DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl'; // Replace with your merchant_public_key !!!
    $merchant_secret = 'XKjy7SzenXCvF9N0';                       // Replace with your merchant_private_key !!!
    $transaction_uid = '';// create an empty transaction_uid
    $transaction_token  = '';// create an empty transaction_token
    $transaction_provider_name  = ''; // create an empty transaction_provider_name
    $transaction_confirmation_code  = ''; // create an empty confirmation code
    $today = date('Y-m-d h:i:s');

    if(isset($_POST['transaction_uid'])){
      $transaction_uid = $this->input->post('transaction_uid', TRUE); // Get the transaction_uid posted by the payment box
    } 
    if(isset($_POST['transaction_token'])){
      $transaction_token  = $this->input->post('transaction_token', TRUE); // Get the transaction_token posted by the payment box
    } 
    if(isset($_POST['transaction_provider_name'])){
      $transaction_provider_name  = $this->input->post('transaction_provider_name', TRUE); // Get the transaction_provider_name posted by the payment box
    }
    if(isset($_POST['transaction_confirmation_code'])){
      $transaction_confirmation_code  = $this->input->post('transaction_confirmation_code', TRUE); // Get the transaction_confirmation_code posted by the payment box
    } 
    $url = 'https://www.wecashup.com/api/v1.0/merchants/'.$merchant_uid.'/transactions/'.$transaction_uid.'/?merchant_public_key='.$merchant_public_key;

    //Steps 7 : You must complete this script at this to save the current transaction in your database.
    /* Provide a table with at least 5 columns in your database capturing the following
    /  transaction_uid | transaction_confirmation_code| transaction_token| transaction_provider_name | transaction_status */

    //Step 8 : Sending data to the WeCashUp Server
    $fields_string = '';
    $fields = array(
      'merchant_secret' => urlencode($merchant_secret),
      'transaction_token' => urlencode($transaction_token),
      'transaction_uid' => urlencode($transaction_uid),
      'transaction_confirmation_code' => urlencode($transaction_confirmation_code),
      'transaction_provider_name' => urlencode($transaction_provider_name),
      '_method' => urlencode('PATCH')
    );
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    //Step 9  : Retrieving the WeCashUp Response
    $server_output = curl_exec ($ch);

    curl_close ($ch);

    $data = json_decode($server_output, true);
    $payment_data = array();
    if($data['response_status'] =="success"){
      $payment_data = array(
        "cust_id" => (int)$data['response_content']['transaction']['transaction_sender_reference'],
        "order_id" => (int)$data['response_content']['transaction']['transaction_receiver_reference'],
        "payment_method" => trim($data['response_content']['transaction']['transaction_provider_name']),
        "transaction_id" => trim($data['response_content']['transaction']['transaction_uid']),
        "total_amount_paid" => trim($data['response_content']['transaction']['transaction_receiver_total_amount']),
        "transfer_account_number" => trim($data['response_content']['transaction']['transaction_sender_uid']),
        "currency_sign" => trim($data['response_content']['transaction']['transaction_receiver_currency']),
        "bank_name" => "NULL",
        "account_holder_name" => "NULL",
        "iban" => "NULL",
        "email_address" => "NULL",
        "mobile_number" => "NULL",
      );
      $this->update_order_payment_by_customer($payment_data);
      var_dump($payment_data);
      //echo "<script> window.top.location.href = 'wecashup_payment_success.php'; </script>";
      //$this->response = ['response' => 'success', 'message' => $this->lang->line('payment_success'), 'payment_data' => $data];
    } else {
      echo "<script> window.top.location.href = 'wecashup_payment_failed.php'; </script>";      
      //$this->response = ['response' => 'failed', 'message' => $this->lang->line('payment_success'), 'payment_data' => $data['response_code']];
    }
    //echo json_encode($this->response);
  }

  public function customer_paid_this_month()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $paid_this_month_by_customer = $this->api->customer_paid_this_month($cust_id);
      if(!empty($paid_this_month_by_customer)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'paid_this_month_by_customer' => $paid_this_month_by_customer]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function customer_earned_this_month()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $earned_this_month_by_customer = $this->api->customer_earned_this_month($cust_id);
      if(!empty($earned_this_month_by_customer)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'earned_this_month_by_customer' => $earned_this_month_by_customer]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function customer_paid_to_date()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $paid_to_date_by_customer = $this->api->customer_paid_to_date($cust_id);
      if(!empty($paid_to_date_by_customer)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'paid_to_date_by_customer' => $paid_to_date_by_customer]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function customer_earned_to_date()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $earned_to_date_by_customer = $this->api->customer_earned_to_date($cust_id);
      if(!empty($earned_to_date_by_customer)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'earned_to_date_by_customer' => $earned_to_date_by_customer]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function customer_account_details()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE);
      $customer_account_balance = $this->api->customer_account_master_list($cust_id);
      for ($i=0; $i < sizeof($customer_account_balance); $i++) { 
        $withdraw_amount = $this->api->get_customer_account_withdraw_request_sum_currencywise($cust_id, 'request', $customer_account_balance[$i]['currency_code']);
        $final_amount = $customer_account_balance[$i]['account_balance'] - $withdraw_amount['amount'];
        $customer_account_balance[$i] += ['withdraw_amount' => (string)$final_amount];
      }

      $customer_scrow_balance = $this->api->deliverer_scrow_master_list($cust_id);
      $customer_paid_this_month = $this->api->customer_paid_this_month($cust_id);
      $customer_earned_this_month = $this->api->customer_earned_this_month($cust_id);
      $customer_paid_to_date = $this->api->customer_paid_to_date($cust_id);
      $customer_earned_to_date = $this->api->customer_earned_to_date($cust_id);
      $customer_payment_methods = $this->api->customer_payment_methods($cust_id);
      
      if(!empty($customer_account_balance) || !empty($customer_scrow_balance) || !empty($customer_paid_this_month) || !empty($customer_earned_this_month) || !empty($customer_paid_to_date) || !empty($customer_earned_to_date) || !empty($customer_payment_methods)) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), 'customer_account_balance' => $customer_account_balance, 'customer_scrow_balance' => $customer_scrow_balance, 'customer_paid_this_month' => $customer_paid_this_month, 'customer_earned_this_month' => $customer_earned_this_month, 'customer_paid_to_date' => $customer_paid_to_date, 'customer_earned_to_date' => $customer_earned_to_date, 'customer_payment_methods' => $customer_payment_methods]; 
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);
    }
  }   

  public function update_order_payment_by_customer_stripe()
  {
    if(empty($this->errors)){
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('transaction_id', TRUE);
      $today = date('Y-m-d h:i:s');
      $order_details = $this->api->get_order_detail($order_id);
      $total_amount_paid = $this->input->post('total_amount_paid', TRUE);
      $transfer_account_number = $this->input->post('transfer_account_number', TRUE);
      $bank_name = $this->input->post('bank_name', TRUE);
      $account_holder_name = $this->input->post('account_holder_name', TRUE);
      $iban = $this->input->post('iban', TRUE);
      $email_address = $this->input->post('email_address', TRUE);
      $mobile_number = $this->input->post('mobile_number', TRUE);    

      $update_data_order = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "complete_paid" => "1",
        "payment_mode" => "payment",
        "cod_payment_type" => "NULL",
      );

      if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {
        /*************************************** Payment Section ****************************************/
          //Add core price to gonagoo master
          if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'standard_price',
            "amount" => trim($order_details['standard_price']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          
          //Update urgent fee in gonagoo account master
          if(trim($order_details['urgent_fee']) > 0) {
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'urgent_fee',
              "amount" => trim($order_details['urgent_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update insurance fee in gonagoo account master
          if(trim($order_details['insurance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'insurance_fee',
              "amount" => trim($order_details['insurance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update handling fee in gonagoo account master
          if(trim($order_details['handling_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'handling_fee',
              "amount" => trim($order_details['handling_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Update vehicle fee in gonagoo account master
          if(trim($order_details['vehicle_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'vehicle_fee',
              "amount" => trim($order_details['vehicle_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }

          //Add payment details in customer account master and history
          if($this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          }

          $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
   
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Deduct payment details from customer account master and history
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        /*************************************** Payment Section ****************************************/
        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }
  }

  public function update_order_payment_by_customer_orange()
  {
    //echo json_encode($_POST); die();
    $cust_id = (int) $this->input->post('cust_id', TRUE);
    $order_id = $this->input->post('order_id', TRUE);
    $order_details = $this->api->get_order_detail($order_id);
    $order_payment_id = $this->input->post('order_payment_id', TRUE);

    $amount = $this->input->post('amount', TRUE);
    $pay_token = $this->input->post('pay_token', TRUE);
    $payment_method = 'orange';
    $today = date('Y-m-d h:i:s');
    //get access token
    $ch = curl_init("https://api.orange.com/oauth/v2/token?");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
    $orange_token = curl_exec($ch);
    curl_close($ch);
    $token_details = json_decode($orange_token, TRUE);
    $token = 'Bearer '.$token_details['access_token'];

    // get payment link
    $json_post_data = json_encode(
                        array(
                          "order_id" => $order_payment_id, 
                          "amount" => $amount, 
                          "pay_token" => $pay_token
                        )
                      );
    //echo $json_post_data; die();
    $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: ".$token,
        "Accept: application/json"
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
    $payment_res = curl_exec($ch);
    curl_close($ch);
    $payment_details = json_decode($payment_res, TRUE);
    $transaction_id = $payment_details['txnid'];
    //echo json_encode($payment_details); die();

    //if($payment_details['status'] == 'SUCCESS' && $order_details['complete_paid'] == 0) {
    if(1) {
      $total_amount_paid = $order_details['order_price'];
      $transfer_account_number = "NULL";
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = $this->input->post('stripeEmail', TRUE);
      $mobile_number = "NULL";    

      $update_data_order = array(
        "payment_method" => trim($payment_method),
        "balance_amount" => 0,
        "paid_amount" => trim($total_amount_paid),
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "complete_paid" => "1",
        "payment_mode" => "payment",
        "payment_by" => "mobile",
        "cod_payment_type" => "NULL",
      );
      if($this->api->update_payment_details_in_order($update_data_order, $order_id)) {

        /*************************************** Payment Section ****************************************/
        //Add core price to gonagoo master-----------------
          if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "type" => 1,
            "transaction_type" => 'standard_price',
            "amount" => trim($order_details['standard_price']),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($order_details['currency_sign']),
            "country_id" => trim($order_details['from_country_id']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        //-------------------------------------------------
        
        //Update urgent fee in gonagoo account master------
          if(trim($order_details['urgent_fee']) > 0) {
            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'urgent_fee',
              "amount" => trim($order_details['urgent_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------
        //Update insurance fee in gonagoo account master---
          if(trim($order_details['insurance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'insurance_fee',
              "amount" => trim($order_details['insurance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------
        //Update handling fee in gonagoo account master----
          if(trim($order_details['handling_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'handling_fee',
              "amount" => trim($order_details['handling_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------
        //Update vehicle fee in gonagoo account master-----
          if(trim($order_details['vehicle_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'vehicle_fee',
              "amount" => trim($order_details['vehicle_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------

        //Update custome clearance fee in gonagoo account master
          if(trim($order_details['custom_clearance_fee']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'custom_clearance_fee',
              "amount" => trim($order_details['custom_clearance_fee']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------

        //Update custome clearance fee in gonagoo account master
          if(trim($order_details['loading_unloading_charges']) > 0) {
            $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
          
            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$order_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'loading_unloading_charges',
              "amount" => trim($order_details['loading_unloading_charges']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($order_details['currency_sign']),
              "country_id" => trim($order_details['from_country_id']),
              "cat_id" => trim($order_details['category_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          }
        //-------------------------------------------------

        //Add payment details in customer account master and history
          if($this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($order_details['currency_sign']),
            );
            $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
          }

          $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        //-------------------------------------------------

        //Deduct payment details from customer account master and history
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$order_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($total_amount_paid),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($order_details['currency_sign']),
            "cat_id" => trim($order_details['category_id']),
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        //-------------------------------------------------
        /*************************************** Payment Section ****************************************/
        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
      } 
    } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
    echo json_encode($this->response);
  }

  public function get_user_order_invoices()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
      if( $invoice_details = $this->api->get_user_order_invoices($cust_id, $last_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_order_invoices" => $invoice_details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function get_deliverer_comission_invoices()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
      if( $invoice_details = $this->api->get_deliverer_comission_invoices($cust_id, $last_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "deliverer_comission_invoices" => $invoice_details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }
  
  public function update_password()
  {   
    $cust_id = $this->input->post('cust_id');
    $old_pass = $this->input->post('old_password');
    $new_pass = $this->input->post('new_password');
    if($this->api->validate_old_password($cust_id, $old_pass)){
      if($this->api->update_new_password(['cust_id' => $cust_id, 'password' => $new_pass])){
        $this->response = array('response' =>'success','message' => 'Password successfully changed!');          
      } else { $this->response = array('response' => 'error','message' => 'Failed to change password!');   }
    } else { $this->response = array('response' => 'error','message' => 'Old password did not match!');  }

    echo json_encode($this->response);
  }

  public function get_support_request_list()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
      if( $request_list = $this->api->get_support_list($cust_id, $last_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "request_list" => $request_list];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }

  public function register_new_support_request()
  {
    $type = $this->input->post('type', TRUE);
    $cat_id = $this->input->post('cat_id', TRUE);  $cat_id = (int) $cat_id;
    $description = $this->input->post('description', TRUE);
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
    $order_id = $this->input->post('order_id', TRUE); $order_id = (int) $order_id;
    $today = date('Y-m-d h:i:s');

    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('attachment')) {   
        $uploads    = $this->upload->data();  
        $file_url =  $config['upload_path'].$uploads["file_name"];  
      } else {  $file_url = "NULL"; }
    } else {  $file_url = "NULL"; }

    $add_data = array(
      'type' => trim($type), 
      'description' => trim($description), 
      'file_url' => trim($file_url), 
      'cust_id' => (int)$cust_id, 
      'cre_datetime' => $today, 
      'response' => 'NULL', 
      'res_datetime' => 'NULL', 
      'status' => 'open', 
      'updated_by' => 0, 
      'order_id' => $order_id, 
      'cat_id' => $cat_id, 
    );
    //echo json_encode($add_dat)a; die();
    if( $id = $this->api->register_new_support($add_data)) {
      $ticket_id = 'G-'.time().$id;
      $this->api->update_support_ticket($id, $ticket_id);
      $this->response = ['response' => 'success','message'=> $this->lang->line('added_successfully')];  
    } else { $this->response = ['response' => 'failed','message'=> $this->lang->line('unable_to_add')];  }
    echo json_encode($this->response);
  }

  public function country_wise_dangerous_goods()
  {
    $from_country_id = $this->input->post('from_country_id',TRUE);
    $to_country_id = $this->input->post('to_country_id',TRUE);
    //echo json_encode($from_country_id ); die();   
    if($from_country_id != $to_country_id){  
      if( $list = $this->transport->get_countries_dangerous_goods($from_country_id, $to_country_id)){
        //echo json_encode($list); die();
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "dangerous_goods" => $list];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
    } else{ 
      if( $list = $this->transport->get_countries_dangerous_goods($from_country_id)) { 
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "dangerous_goods" => $list];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
    }
    echo json_encode($this->response);
  }

  public function register_state_city()
  { 
    if(empty($this->errors)) {
      $country_id = $this->input->post('country_id',TRUE);
      $state_id = $this->input->post('state_id',TRUE);
      $state_name = $this->input->post('state_name',TRUE);
      $city_name = $this->input->post('city_name',TRUE);
      //var_dump($_POST); die();
      if($country_id > 0 && $state_id >= 0 && $state_name != 'NULL' && $city_name != 'NULL') {
        if($state_id == 0) {
          //add state and get state id
          $s_id = $this->api->register_state($country_id, $state_name);
          //add city under state id
          $c_id = $this->api->register_city($s_id, $city_name);
          $this->response = ['response' => 'success','message'=> $this->lang->line('add_success'), "country_id" => $country_id, "state_id" => $s_id, "state_name" => $state_name, "city_id" => $c_id, "city_name" => $city_name];
        } else {
          //add city and get city id
          $c_id = $this->api->register_city($state_id, $city_name);
          $this->response = ['response' => 'success','message'=> $this->lang->line('add_success'), "country_id" => $country_id, "state_id" => $state_id, "state_name" => $state_name, "city_id" => $c_id, "city_name" => $city_name];
        }
      } else {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('invalid_data')];
      }
      echo json_encode($this->response);
    }
  }

  public function get_recent_relay_list()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
    if($this->api->is_user_active_or_exists($cust_id)){
      if( $list = $this->api->get_recent_relay_list($cust_id) ) {
        for ($i=0; $i < sizeof($list); $i++) { 
          $country_name = $this->api->get_country_name_by_id($list[$i]['country_id']);
          $state_name = $this->api->get_state_name_by_id($list[$i]['state_id']);
          $city_name = $this->api->get_city_name_by_id($list[$i]['city_id']);
          $list[$i] += ["country_name" => $country_name, "state_name" => $state_name, "city_name" => $city_name];
        }
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "result" => $list];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed').'...']; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }

  public function get_nearby_relay_point_list()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
    $frm_relay_latitude = $this->input->post('lat', TRUE); $frm_relay_latitude = trim($frm_relay_latitude);
    $frm_relay_longitude = $this->input->post('long', TRUE); $frm_relay_longitude = trim($frm_relay_longitude);
    if($this->api->is_user_active_or_exists($cust_id)){
      if( $list = $this->api->get_relay_point_by_lat_long($frm_relay_latitude, $frm_relay_longitude, $cust_id) ) {
        for ($i=0; $i < sizeof($list); $i++) { 
          $country_name = $this->api->get_country_name_by_id($list[$i]['country_id']);
          $state_name = $this->api->get_state_name_by_id($list[$i]['state_id']);
          $city_name = $this->api->get_city_name_by_id($list[$i]['city_id']);
          $list[$i] += ["country_name" => $country_name, "state_name" => $state_name, "city_name" => $city_name,"relay_status" => "new"];
        }
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "result" => $list];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed').'...']; }
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
    echo json_encode($this->response);  
  }
  
  public function gonagoo_support_address()
  {
    if(empty($this->errors)){ 
      $country_id = $this->input->post('country_id', TRUE);
      $address_details = $this->api->get_addresses($country_id);
      if(sizeof($address_details) > 1 && $address_details[0]['country_id'] == 75) {
        $address_details = array_reverse($address_details);
      }
      if(empty($address_details)) {
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('Address not found!'), 'address_details' => array()]; 
      } else {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('Address found!'), 'address_details' => $address_details]; 
      }
      echo json_encode($this->response);
    }
  }

  public function courier_order_cleanup()
  {
    if($this->api->courier_order_cleanup()){
      $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
    } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
    echo json_encode($this->response);
  }

  public function user_pending_invoices_history()
  {
    if(empty($this->errors)){ 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $last_id = $this->input->post('last_id', TRUE); $last_id = (int) $last_id;
      if( $invoice_details = $this->api->get_user_pending_invoices_history($cust_id, $last_id) ) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_order_invoices" => $invoice_details];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      echo json_encode($this->response);  
    }
  }
  
  public function get_unit_detail()
  {
    $unit_id = $this->input->post('unt');
    echo json_encode($this->api->get_unit_detail($unit_id));
  }

    // Promo Code----------------------
    public function promocode()
    {
      if(isset($_POST["promocode"])){
        $order_id = $this->input->post('order_id');
        $cust_id = $this->input->post('cust_id');
        $details =  $this->api->get_order_detail($order_id);
        //echo json_encode($details['currency_sign']); die();
        $promo_code_name = $this->input->post("promocode");
        if(!$details['promo_code']){
          if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
            $used_codes = $this->api->get_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version']);
            if(sizeof($used_codes) < $promocode['no_of_limit']){
              if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$cust_id)){
                  //code start here
                    $discount_amount = round(($details['order_price']/100)*$promocode['discount_percent'] , 2); 
                    if($details['currency_sign'] == 'XAF'){
                      $discount_amount = round($discount_amount , 0);
                    }
                    $order_price =  round($details['order_price']-$discount_amount ,2);
                    if($details['currency_sign'] == 'XAF'){
                      $order_price = round($order_price , 0);
                    }
                    $advance_payment = round(($details['advance_payment']/100)*$promocode['discount_percent'] ,2);
                    if($details['currency_sign'] == 'XAF'){
                      $advance_payment = round($advance_payment , 0);
                    }
                    $pickup_payment = round( ($details['pickup_payment']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $pickup_payment = round($pickup_payment , 0);
                    }
                    $deliver_payment = round( ($details['deliver_payment']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $deliver_payment = round($deliver_payment , 0);
                    }
                    $insurance_fee = round( ($details['insurance_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $insurance_fee = round($insurance_fee , 0);
                    }
                    $commission_amount = round( ($details['commission_amount']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $commission_amount = round($commission_amount , 0);
                    }

                    $standard_price = round( ($details['standard_price']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $standard_price = round($standard_price , 0);
                    }
                    $urgent_fee = round( ($details['urgent_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $urgent_fee = round($urgent_fee , 0);
                    }

                    $handling_fee = round( ($details['handling_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $handling_fee = round($handling_fee , 0);
                    }
                    $vehicle_fee = round( ($details['vehicle_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $vehicle_fee = round($vehicle_fee , 0);
                    }

                    $custom_clearance_fee = round( ($details['custom_clearance_fee']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $custom_clearance_fee = round($custom_clearance_fee , 0);
                    }

                    $loading_unloading_charges = round( ($details['loading_unloading_charges']/100)*$promocode['discount_percent'],2);
                    if($details['currency_sign'] == 'XAF'){
                      $loading_unloading_charges = round($loading_unloading_charges , 0);
                    }

                   
                    $courier_order = array(
                      'promo_code' => $promocode['promo_code'],
                      'promo_code_id' => $promocode['promo_code_id'],
                      'promo_code_version' => $promocode['promo_code_version'],
                      'discount_amount' => $discount_amount,
                      'discount_percent' => $promocode['discount_percent'],
                      'actual_order_price' => $details['order_price'],
                      'order_price' => $order_price ,
                      'advance_payment' => ($details['advance_payment']-$advance_payment),
                      'pickup_payment' => ($details['pickup_payment']-$pickup_payment),
                      'deliver_payment' => ($details['deliver_payment']-$deliver_payment),
                      'balance_amount' => $order_price,
                      'insurance_fee' => ($details['insurance_fee']-$insurance_fee),
                      'commission_amount' => ($details['commission_amount']-$commission_amount),
                      'standard_price' => ($details['standard_price']-$standard_price),
                      'urgent_fee' => ($details['urgent_fee']-$urgent_fee),
                      'handling_fee' =>  ($details['handling_fee']-$handling_fee),
                      'vehicle_fee' => ($details['vehicle_fee']-$vehicle_fee),
                      'custom_clearance_fee'=>($details['custom_clearance_fee']-$custom_clearance_fee),
                      'loading_unloading_charges'=>($details['loading_unloading_charges']-$loading_unloading_charges),
                      'promo_code_datetime'=> date('Y-m-d h:i:s'),
                    );

                    $this->api->update_review($courier_order,$details['order_id']);
                    $used_promo_update = array(
                      'promo_code_id' => $promocode['promo_code_id'],
                      'promo_code_version' => $promocode['promo_code_version'],
                      'cust_id' => $cust_id,
                      'customer_email' => "",
                      'discount_percent' => $promocode['discount_percent'],
                      'discount_amount' => $discount_amount,
                      'discount_currency' => $details['currency_sign'],
                      'cre_datetime' => date('Y-m-d h:i:s'),
                      'service_id' => '7',
                      'booking_id' => $details['order_id'],
                    );
                    //echo json_encode($details['currency_sign']); die();
                    //$this->api->register_user_used_prmocode($used_promo_update);
                    $details = $this->api->get_order_detail($order_id);
                    $this->response = ['response' => 'success', 'message' => $this->lang->line('Promocode Applied successfully') , 'order_details' => $details];
                  //code end here
                }else{
                  $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used this promocode')];
                }
              }else{
                $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promocode has been Expired')];
              }
            }else{
              $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promo code is used maximum number of limit')];
            }  
          }else{
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')];
          }
        }else{
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used promocode for this order')];
        }
        echo json_encode($this->response);
      }
    }

    public function promocode_check()
    {
      $promo_code_name =  $this->input->post('promocode');
      $order_id =  $this->input->post('order_id');
      $cust_id =  $this->input->post('cust_id');
      $details =  $this->api->get_order_detail($order_id);
      
      if($promo_code = $this->api->get_promocode_by_name($promo_code_name)) {
        $service_id = explode(',',$promo_code['service_id']);
        $used_codes = $this->api->get_used_promo_code($promo_code['promo_code_id'],$promo_code['promo_code_version']);
        if(sizeof($used_codes) < $promo_code['no_of_limit']){
          if(!$details['promo_code']){
            if(in_array("6", $service_id) || in_array("7", $service_id) || in_array("280", $service_id)){
              if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
                //echo json_encode($promocode); die();
                if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                  if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$cust_id)){
                    $discount_amount = round(($details['order_price']/100)*$promocode['discount_percent'] , 2); 
                    if($details['currency_sign'] == 'XAF'){
                      $discount_amount = round($discount_amount , 0);
                    }
                    $order_price =  round($details['order_price']-$discount_amount ,2);
                    if($details['currency_sign'] == 'XAF'){
                      $order_price = round($order_price , 0);
                    }

                    $this->response = ['response' => 'success', 'message' => $this->lang->line('Promocode Available') , 'Discount' => $discount_amount." ".$details['currency_sign'] , 'order_price' => $order_price." ".$details['currency_sign'] , 'order_details' => $details];

                  }else{
                    $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used this promocode')];
                  }
                }else{
                  $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promocode has been Expired')];
                }
              }else{
              $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')];
              }
            }else{
                $this->response = ['response' => 'failed', 'message' => $this->lang->line('This promocode is not valid for this category')];
            }
          }else{
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used promocode for this order')];
          }
        }else{
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promo code is used maximum number of limit')];
        }  
      }else{
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')];
      }
      echo json_encode($this->response);
    }
    
    public function promocode_with_booking()
    {
      $promo_code_name =  $this->input->post('promocode');
      $cust_id =  $this->input->post('cust_id');
      if($promocode = $this->api->get_promocode_by_name($promo_code_name)) {
        $service_id = explode(',',$promocode['service_id']);
        $used_codes = $this->api->get_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version']);
        if(sizeof($used_codes) < $promocode['no_of_limit']){
          if(in_array("6", $service_id) || in_array("7", $service_id) || in_array("280", $service_id)){
              //echo json_encode($promocode); die();
              if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$cust_id)){
                  $this->response = ['response' => 'success', 'discount_percent' => $promocode['discount_percent']];
                }else{
                  $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used this promocode')];
                }
              }else{
                $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promocode has been Expired')];
              }  
          }else{
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('This promocode is not valid for this category')];
          }
        }else{
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promo code is used maximum number of limit')];
        } 
      } else {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')]; 
      }
      echo json_encode($this->response);
    }
  // Promo Code----------------------
}
/* End of file Api.php */
/* Location: ./application/controllers/Api.php */