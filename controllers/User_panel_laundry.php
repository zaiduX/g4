<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class User_panel_laundry extends CI_Controller {
    
  private $cust_id = 0; 
  private $cust = array();
    
  public function __construct()
  {
    parent::__construct();
    
    $this->cust_id = $this->session->userdata("cust_id"); $this->cust_id = (int) $this->cust_id;
    
    $this->load->model('api_model_laundry', 'api');
    $this->load->model('Notification_model_bus', 'notification');
    $this->load->model('Web_user_model_laundry', 'user');
    $this->load->model('Web_user_model', 'web_user');
    $this->load->model('Api_model', 'api_courier');
    $this->load->model('Api_model_bus', 'api_bus');
    $this->load->model('Api_model_sms', 'api_sms');
  
    $this->cust = $this->api->get_consumer_datails($this->cust_id, true); $cust = $this->cust;
    
    if( ( $this->session->userdata("is_logged_in") == "NULL" ) OR ( $this->session->userdata("is_logged_in") !== 1 ) ) { redirect('log-in'); }
    
    if($cust['mobile_verified'] == 0) { redirect('verification/otp'); }

    $lang = $this->session->userdata('language');
    $this->config->set_item('language', strtolower($lang));

    if(trim($lang) == 'french'){
      $this->lang->load('frenchApi_lang','french');
      $this->lang->load('frenchFront_lang','french');
    } else if(trim($lang) == 'spanish'){  
      $this->lang->load('spanishApi_lang','spanish');
      $this->lang->load('spanishFront_lang','spanish');
    } else { 
      $this->lang->load('englishApi_lang','english');
      $this->lang->load('englishFront_lang','english');
    }
    
    $this->api->update_last_login($cust['cust_id']);
    
    //echo json_encode($cust); die();
    if (!$this->input->is_ajax_request()){ 
      // var_dump($_POST); 
      $order_id = $this->input->post('booking_id');
      $type = $this->input->post('type');

      // if($order_id && !$ow_id) { 
      if($order_id && ($type =="workroom")) { 
        $ids = $this->api->get_total_unread_order_workroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) {  $this->api->make_workroom_notification_read($v['ow_id'], $cust['cust_id']);  }
      }

      if($order_id && ($type =="chatroom")) { 
        $ids = $this->api->get_total_unread_order_chatroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) { $this->api->make_chatrooom_notification_read($v['chat_id'], $cust['cust_id']);  }
      }

      if($cust['user_type'] == 1 ){ $user_type ="Individuals"; } else { $user_type ="Business"; }
      $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust['cust_id'],$user_type );

      $notification = $this->api->get_total_unread_notifications($cust['cust_id'],$user_type, 5);
      $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust['cust_id']);      
      $nt_workroom = $this->api->get_total_unread_workroom_notifications($cust['cust_id'], 5); 
      //echo json_encode($nt_workroom); die(); 
      //echo $this->db->last_query(); die();
      $total_chatroom_notifications = $this->api->get_total_unread_chatroom_notifications_count($cust['cust_id']);      
      $nt_chatroom = $this->api->get_total_unread_chatroom_notifications($cust['cust_id'], 5);  

      $compact = ["total_unread_notifications","notification","total_workroom_notifications","nt_workroom","total_chatroom_notifications","nt_chatroom"];
    
      $this->load->view('user_panel/header_laundry',compact($compact)); 
      $this->load->view('user_panel/menu_laundry', compact("cust"));
    }
  }

  //Start Dashboard----------------------------------------
    public function dashboard_laundry()
    {   
      $cust = $this->cust;
      $this->load->view('user_panel/dashboard_laundry', compact('cust'));   
      if($cust['acc_type'] == 'buyer') {
        $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
        $current_balance = ($current_balance != NULL ) ? $current_balance['account_balance'] : 0;
        //total spend in service
        $total_spend = $this->user->get_total_spend($cust['cust_id'], 'XAF');    
        $total_spend = ($total_spend !=NULL)?$total_spend['amount']:0;
        $total_spend_service = $this->user->get_total_spend_service($cust['cust_id'], 'XAF', 9); 
        $total_spend_service = $total_spend_service['amount'];
        $total_spend_service = ($total_spend_service !=NULL && $total_spend_service !=null) ? $total_spend_service: 0; 
        //Bookings
        $total_bookings = $this->api->laundry_booking_list($cust['cust_id'], 'all', 'customer', 0, 0);
        $accepted = $this->api->laundry_booking_list($cust['cust_id'], 'accepted', 'customer', 0, 0);
        $in_progress = $this->api->laundry_booking_list($cust['cust_id'], 'in_progress', 'customer', 0, 0);
        $completed = $this->api->laundry_booking_list($cust['cust_id'], 'completed', 'customer', 0, 0);
        $cancelled = $trip_list = $this->api->laundry_booking_list($cust['cust_id'], 'cancelled', 'customer', 0, 0);
        $compact = compact('cust','current_balance','total_spend','total_bookings','accepted','in_progress','completed','cancelled', 'total_spend_service');
        $this->load->view('user_panel/buyer_dashboard_laundry',$compact);
      } else {
        $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
        $current_balance = ($current_balance !=NULL && $current_balance !=null)?$current_balance['account_balance']:0;
        $total_earned = $this->user->get_total_earned($cust['cust_id'], 'XAF'); 
        $total_earned = $total_earned['amount'];
        $total_earned = ($total_earned !=NULL && $total_earned !=null) ? $total_earned: 0; 
        $total_spend = $this->user->get_total_spend($cust['cust_id'], 'XAF');
        $total_spend = ($total_spend !=NULL && $total_spend !=null)?$total_spend['amount']:0;
        //total earned in service
        $total_earned_service = $this->user->get_total_earned_service($cust['cust_id'], 'XAF', 9); 
        $total_earned_service = $total_earned_service['amount'];
        $total_earned_service = ($total_earned_service !=NULL && $total_earned_service !=null) ? $total_earned_service: 0; 
        //total spend in service
        $total_spend_service = $this->user->get_total_spend_service($cust['cust_id'], 'XAF', 9); 
        $total_spend_service = $total_spend_service['amount'];
        $total_spend_service = ($total_spend_service !=NULL && $total_spend_service !=null) ? $total_spend_service: 0; 
        //Ratings
        $laundry_rating = $this->api->get_laundry_review_details($cust['cust_id']);
        $deliverer_ratings = $this->api->get_provider_review_details($cust['cust_id']);
        //Orders
        $total_orders = $this->api->laundry_booking_list($cust['cust_id'], 'all', 'provider', 0, 0);
        $accepted = $this->api->laundry_booking_list($cust['cust_id'], 'accepted', 'provider', 0, 0);
        $in_progress = $this->api->laundry_booking_list($cust['cust_id'], 'in_progress', 'provider', 0, 0);
        $completed = $this->api->laundry_booking_list($cust['cust_id'], 'completed', 'provider', 0, 0);
        $cancelled = $trip_list = $this->api->laundry_booking_list($cust['cust_id'], 'cancelled', 'provider', 0, 0);
        
        $compact = compact('total_orders','accepted','in_progress','completed','cancelled','deliverer_ratings','laundry_rating','cust','current_balance','total_earned','total_spend', 'total_spend_service', 'total_earned_service');
        $this->load->view('user_panel/both_dashboard_laundry', $compact);
      }
      $this->load->view('user_panel/footer');
    }
    public function dashboard_laundry_users()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust['cust_id'];
      //Get Customers List
      $customers_list = $this->api->laundry_booking_and_customer_list($cust_id, 'all', 'provider', 1, 0, 0);
      //echo json_encode($customers_list); die();

      $cust_ids =array();
      //collect customer id's expect walking customer
      foreach ($customers_list as $customer) {
        if($customer['cust_id'] != 67) array_push($cust_ids, $customer['cust_id']);
      }
      $cust_ids = implode(',',$cust_ids);
      //echo json_encode($cust_ids); die();

      //Get Recent customer list
      $recentlyConnectedUsers = $this->api->get_user_login_history('recently', $cust_ids, 0, 0);
      $FrequentlyConnectedUsers = $this->api->get_user_login_history('frequently', $cust_ids, 0, 0);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($FrequentlyConnectedUsers); die();
      
      //status-wise orders
      $all_order_list = $this->api->laundry_booking_list($cust_id, 'all', 'provider', 0, 0);
      $accepted_order_list = $this->api->laundry_booking_list($cust_id, 'accepted', 'provider', 0, 0);
      $in_progress_order_list = $this->api->laundry_booking_list($cust_id, 'in_progress', 'provider', 0, 0);
      $completed_order_list = $this->api->laundry_booking_list($cust_id, 'completed', 'provider', 0, 0);
      $cancelled_order_list = $this->api->laundry_booking_list($cust_id, 'cancelled', 'provider', 0, 0);
      //echo json_encode($all_order_list); die();

      //day-wise orders
      $today_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'today', 'provider', 0, 0);
      $week_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'week', 'provider', 0, 0);
      $month_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'month', 'provider', 0, 0);
      $till_date_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'till_date', 'provider', 0, 0);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($today_order_list); die();

      //Claim day-wise
      $today_claim_list = $this->api->laundry_claim_day_wise($cust_id, 'today', 'provider', 0, 0);
      $week_claim_list = $this->api->laundry_claim_day_wise($cust_id, 'week', 'provider', 0, 0);
      $month_claim_list = $this->api->laundry_claim_day_wise($cust_id, 'month', 'provider', 0, 0);
      $till_date_claim_list = $this->api->laundry_claim_day_wise($cust_id, 'till_date', 'provider', 0, 0);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($today_order_list); die();

      //Claim status-wise
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'open',
        'service_cat_id' => 9,
      );
      $open_claim_list = $this->api->get_claim_status_wise($data);

      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'in_progress',
        'service_cat_id' => 9,
      );
      $in_progress_claim_list = $this->api->get_claim_status_wise($data);

      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'standby',
        'service_cat_id' => 9,
      );
      $standby_claim_list = $this->api->get_claim_status_wise($data);

      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'closed',
        'service_cat_id' => 9,
      );
      $closed_claim_list = $this->api->get_claim_status_wise($data);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($today_order_list); die();

     /* echo date('Y-m-d h:i:s'); die();
      $from_date = 0;
      $to_date = 0;
      $till_date_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'till_date', 'provider', 0, 0, $from_date, $to_date);*/


      // echo json_encode($_SESSION); die();
      $currency_sign = $this->api->get_country_currencies((int)$_SESSION['admin_country_id'])[0]['currency_sign'];
      $currency_title = $this->api->get_country_currencies((int)$_SESSION['admin_country_id'])[0]['currency_title'];


      $today_start = date('Y-m-d 00:00:00');
      $today_end = date('Y-m-d 23:59:59');
      $data = array(
        'cust_id' => $cust_id,
        'from_date' => $today_start,
        'to_date' => $today_end,
        'currency_code' => $currency_sign,
        'cat_id' => 9,
      );
      if($this->api->laundry_sales_day_wise_count($data)) {
        $todays_transaction = (int)$this->api->laundry_sales_day_wise_count($data)[0]['total_sales'];
      } else { $todays_transaction = 0; }
      //echo json_encode($this->db->last_query()); die();

      for ($i=1; $i <= 14; $i++) { 
        $day = date('Y-m-d', strtotime('-'.$i.' days'));
        $today_start = date($day.' 00:00:00');
        $today_end = date($day.' 23:59:59');
        $data = array(
          'cust_id' => $cust_id,
          'from_date' => $today_start,
          'to_date' => $today_end,
          'currency_code' => $currency_sign,
          'cat_id' => 9,
        );

        if($this->api->laundry_sales_day_wise_count($data)) {
          ${"todays_transaction_" . $i} = (int)$this->api->laundry_sales_day_wise_count($data)[0]['total_sales'];
        } else { ${"todays_transaction_" . $i} = 0; }
        //echo ${"todays_transaction_" . $i} . '<br />';
      }
      //echo json_encode($todays_transaction); die();
      
      $this->load->view('user_panel/laundry_provider_customer_list_view', compact('customers_list', 'cust_id', 'recentlyConnectedUsers', 'FrequentlyConnectedUsers', 'all_order_list','accepted_order_list','in_progress_order_list','completed_order_list','cancelled_order_list','today_order_list','week_order_list','month_order_list','till_date_order_list','today_claim_list','week_claim_list','month_claim_list','till_date_claim_list','open_claim_list','in_progress_claim_list','standby_claim_list','closed_claim_list','currency_sign','currency_title','todays_transaction','todays_transaction_1','todays_transaction_2','todays_transaction_3','todays_transaction_4','todays_transaction_5','todays_transaction_6','todays_transaction_7','todays_transaction_8','todays_transaction_9','todays_transaction_10','todays_transaction_11','todays_transaction_12','todays_transaction_13','todays_transaction_14'));
      $this->load->view('user_panel/footer');
    }
    public function today_laundry_orders()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $today_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'today', 'provider', 0, 0);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/today_laundry_orders_list_view', compact('today_order_list', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function current_week_laundry_orders()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $current_week_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'week', 'provider', 0, 0);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/current_week_laundry_orders_list_view', compact('current_week_order_list', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function current_month_laundry_orders()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $current_month_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'month', 'provider', 0, 0);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/current_month_laundry_orders_list_view', compact('current_month_order_list', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function till_date_laundry_orders()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;
      $from_date = $f_date = $this->input->post("from_date");
      $to_date = $t_date = $this->input->post("to_date");

      if(!isset($from_date)) { 
        $from_date = 'NULL'; 
        $f_date = 'NULL';
        $t_date = 'NULL'; 
      } else { 
        $from_date = explode('/', $from_date)[2] . '-' . explode('/', $from_date)[0] . '-' . explode('/', $from_date)[1] . ' 00:00:00'; 
      }

      if(!isset($to_date)) { 
        $to_date = 'NULL';
        $f_date = 'NULL';
        $t_date = 'NULL'; 
      } else { 
        $to_date = explode('/', $to_date)[2] . '-' . explode('/', $to_date)[0] . '-' . explode('/', $to_date)[1] . ' 23:59:59'; 
      }

      $till_date_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'till_date', 'provider', 0, 0, $from_date, $to_date);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/till_date_laundry_orders_list_view', compact('till_date_order_list', 'cust_id', 'f_date', 't_date'));
      $this->load->view('user_panel/footer');
    }
    public function today_laundry_sales()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'NULL'; }

      $currencies = $this->api->get_all_currencies();

      $today_transaction_list = $this->api->laundry_sales_day_wise($cust_id, 'today', 0, 0, 'NULL', 'NULL', $currency_sign);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/today_laundry_transaction_list_view', compact('today_transaction_list', 'cust_id', 'currencies', 'currency_sign'));
      $this->load->view('user_panel/footer');
    }
    public function current_week_laundry_sales()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'NULL'; }

      $currencies = $this->api->get_all_currencies();

      $week_transaction_list = $this->api->laundry_sales_day_wise($cust_id, 'week', 0, 0, 'NULL', 'NULL', $currency_sign);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/current_week_laundry_transaction_list_view', compact('week_transaction_list', 'cust_id', 'currencies', 'currency_sign'));
      $this->load->view('user_panel/footer');
    }
    public function current_month_laundry_sales()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'NULL'; }

      $currencies = $this->api->get_all_currencies();

      $month_transaction_list = $this->api->laundry_sales_day_wise($cust_id, 'month', 0, 0, 'NULL', 'NULL', $currency_sign);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/current_month_laundry_transaction_list_view', compact('month_transaction_list', 'cust_id', 'currencies', 'currency_sign'));
      $this->load->view('user_panel/footer');
    }
    public function till_date_laundry_sales()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'NULL'; }


      $from_date = $f_date = $this->input->post("from_date");
      $to_date = $t_date = $this->input->post("to_date");

      if(!isset($from_date) || $from_date == '') { 
        $from_date = 'NULL'; 
        $f_date = 'NULL';
        $t_date = 'NULL';
      } else { 
        $from_date = explode('/', $from_date)[2] . '-' . explode('/', $from_date)[0] . '-' . explode('/', $from_date)[1] . ' 00:00:00'; 
      }

      if(!isset($to_date) || $to_date == '') { 
        $to_date = 'NULL';
        $f_date = 'NULL';
        $t_date = 'NULL';
      } else { 
        $to_date = explode('/', $to_date)[2] . '-' . explode('/', $to_date)[0] . '-' . explode('/', $to_date)[1] . ' 23:59:59'; 
      }

      $currencies = $this->api->get_all_currencies();

      $till_date_transaction_list = $this->api->laundry_sales_day_wise($cust_id, 'month', 0, 0, $from_date, $to_date, $currency_sign);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/till_date_laundry_transaction_list_view', compact('till_date_transaction_list', 'cust_id', 'currencies', 'currency_sign', 'f_date', 't_date'));
      $this->load->view('user_panel/footer');
    }
    public function best_customers()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $customers_list = $this->api->laundry_booking_and_customer_list($cust_id, 'all', 'provider', 1, 0, 0);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/laundry_provider_best_customer_list_view', compact('customers_list', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_customer_consumption_history()
    {   
      //echo json_encode($_POST); die();
      $c_id = $this->input->post("c_id");
      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'null'; }

      $cust = $this->cust;
      $cust_id = (int)$cust['cust_id'];

      $currencies = $this->api->get_all_currencies();

      $booking_list = $this->api->laundry_booking_and_customer_provider_list($c_id, 'all', 'customer', 'provider' , $cust_id, 0, 0, 0, $currency_sign );
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($booking_list); die();

      $this->load->view('user_panel/laundry_customer_consumption_list_view', compact('booking_list', 'cust_id', 'currencies', 'c_id', 'currency_sign'));
      $this->load->view('user_panel/footer');
    }
  //End Dashboard------------------------------------------

  //Start User Profile-------------------------------------
    public function user_profile()
    { 
      $cust = $this->cust;
      $total_edu = $this->user->get_total_educations($cust['cust_id']);
      $education = $this->user->get_customer_educations($cust['cust_id'],1);

      $total_exp = $this->user->get_total_experiences($cust['cust_id'],false);
      $total_other_exp = $this->user->get_total_experiences($cust['cust_id'],true);

      $experience = $this->user->get_customer_experiences($cust['cust_id'],false, 1);
      $other_experience = $this->user->get_customer_experiences($cust['cust_id'],true, 1);

      $skills = $this->user->get_customer_skills($cust['cust_id'], 1);

      $total_portfolios = $this->user->get_total_portfolios($cust['cust_id']);
      $portfolio = $this->user->get_customer_portfolio($cust['cust_id'], 1);

      $total_docs = $this->user->get_total_documents($cust['cust_id']);
      $docs = $this->user->get_customer_documents($cust['cust_id'], 1);

      $total_pay_methods = $this->user->get_total_payment_methods($cust['cust_id']);
      $pay_methods = $this->user->get_customer_payment_methods($cust['cust_id'], 1);

      $per_counter = 0; 
      $total_counters = 15;

      if($cust['email_verified'] == 1) { $per_counter += 1; }
      if($cust['mobile_verified'] == 1) { $per_counter += 1; }
      if($cust['firstname'] != "NULL") { $per_counter += 1; }
      if($cust['lastname'] != "NULL") { $per_counter += 1; }
      if($cust['gender'] != "NULL") { $per_counter += 1; }
      if($cust['overview'] != "NULL") { $per_counter += 1; }
      if($cust['lang_known'] != "NULL") { $per_counter += 1; }
      if($cust['country_id'] > 0) { $per_counter += 1; }
      if($cust['state_id'] > 0) { $per_counter += 1; }
      if($cust['city_id'] > 0) { $per_counter += 1; }
      if($cust['avatar_url'] != "NULL") { $per_counter += 1; }
      if($total_edu > 0) { $per_counter += 1; }
      if($total_exp > 0) { $per_counter += 1; }
      if($experience) { $per_counter += 1; }
      if($skills) { $per_counter += 1; }

      $percentage = (int) (((int) $per_counter / (int) $total_counters ) *100 );

      $compact = compact('total_edu','education','total_exp','total_other_exp','experience','other_experience','skills','portfolio','total_portfolios','total_docs','docs','total_pay_methods','pay_methods', 'percentage');
      // echo json_encode($skills); die();
      $this->load->view('user_panel/laundry_user_profile_view', $compact);
      $this->load->view('user_panel/footer');
    }
    public function edit_basic_profile_view()
    {
      $countries = $this->user->get_countries();
      $states = $this->user->get_state_by_country_id($this->cust['country_id']);
      if($this->cust['state_id'] > 0 ){ $cities = $this->user->get_city_by_state_id($this->cust['state_id']); } else{ $cities = array(); }
      $this->load->view('user_panel/laundry_edit_basic_profile_view', compact('countries','states','cities'));
      $this->load->view('user_panel/footer');
    }
    public function get_state_by_country_id()
    {
      $country_id = $this->input->post('country_id');
      echo json_encode($this->user->get_state_by_country_id($country_id));
    }
    public function get_city_by_state_id()
    {
      $state_id = $this->input->post('state_id');
      echo json_encode($this->user->get_city_by_state_id($state_id));
    }
    public function update_basic_profile()
    {
      $mobile_no = $this->input->post('mobile_no');
      $firstname = $this->input->post('firstname');
      $lastname = $this->input->post('lastname');
      $country_id = $this->input->post('country_id');
      $state_id = $this->input->post('state_id');
      $city_id = $this->input->post('city_id');
      $gender = $this->input->post('gender');
      $session_mobile_no = $this->session->userdata('mobile_no');

      $cust = $this->cust; 
      $old_avatar_url = $cust['avatar_url'];
      $old_cover_url = $cust['cover_url'];
      
      if($state_id == NULL) { $state_id = $cust['state_id']; }
      if($city_id == NULL) { $city_id = $cust['city_id']; }

      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('avatar')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];  

          if( $old_avatar_url != "NULL") { unlink($old_avatar_url); }           
        } else { $avatar_url = $old_avatar_url; }
      } else { $avatar_url = $old_avatar_url; }

      if( !empty($_FILES["cover"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/cover-images/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('cover')) {   
          $uploads    = $this->upload->data();  
          $cover_url =  $config['upload_path'].$uploads["file_name"];  

          if( $old_cover_url != "NULL") { unlink($old_cover_url); }           
        } else { $cover_url = $old_cover_url; }
      } else { $cover_url = $old_cover_url; }


      if($mobile_no === $session_mobile_no ) {
        $update_data = array(
          "firstname" => $firstname,
          "lastname" => $lastname,
          "mobile1" => $mobile_no,
          "city_id" => $city_id,
          "state_id" => $state_id,
          "country_id" => $country_id,
          "gender" => $gender,
          "avatar_url" => $avatar_url,
          "cover_url" => $cover_url,
        );    
      } else {
        $OTP = mt_rand(100000,999999);
        $update_data = array(
          "firstname" => $firstname,
          "lastname" => $lastname,
          "mobile1" => $mobile_no,
          "city_id" => $city_id,
          "state_id" => $state_id,
          "country_id" => $country_id,
          "avatar_url" => $avatar_url,
          "cover_url" => $cover_url,
          "gender" => $gender,
          "cust_otp" => $OTP,
          "mobile_verified" => 0,
        );
      }

      if($this->user->update_basic_profile($update_data)){
        if($mobile_no == $session_mobile_no ) { 
          $this->session->set_flashdata('success',$this->lang->line('updated_successfully')); 
          redirect('user-panel-laundry/basic-profile/'. $this->cust_id);
        }
        else{
          $this->session->set_userdata('mobile_no', $mobile_no);
          $this->api->sendSMS($this->api->get_country_code_by_id((int)$country_id).$mobile_no, $this->lang->line('lbl_verify_otp'). ': ' . $OTP);
          redirect('verification/otp');
        }
      } else{
        $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found'));  
        redirect('user-panel-laundry/basic-profile/'. $this->session->userdata('cust_id'));
      }
    }
  //End User Profile---------------------------------------

  //Start User Payment Methods-----------------------------
    public function payment_method_list_view()
    {
      $payments = $this->api->get_payment_details($this->cust['cust_id']);
      if($payments) {
        $this->load->view('user_panel/laundry_payment_method_list_view', compact('payments'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function add_payment_method_view()
    {
      $this->load->view('user_panel/laundry_add_payment_method_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_payment_method()
    {
      $cust = $this->cust;
      $pay_method = $this->input->post("pay_method");
      $brand_name = $this->input->post("brand_name");
      $card_no = $this->input->post("card_no");
      $month = $this->input->post("month");
      $year = $this->input->post("year");
      $bank_name = $this->input->post("bank_name");
      $bank_address = $this->input->post("bank_address");
      $account_title = $this->input->post("account_title");
      $bank_short_code = $this->input->post("bank_short_code");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'payment_method' => trim($pay_method), 
        'brand_name' => trim($brand_name),
        'bank_name' => trim($bank_name),
        'account_title' => trim($account_title),
        'account_title' => trim($account_title),
        'bank_short_code' => trim($bank_short_code),
        'pay_status' => 1,
        'card_number' => trim($card_no),
        'expirty_date' => trim($month). '/'.trim($year),
        'cre_datetime' => $today,
      );
          
      if( $id = $this->api->register_payment_details($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-laundry/payment-method/add');
    } 
    public function edit_payment_method_view()
    {
      $id = $this->uri->segment(3);
      if( $pay = $this->user->get_payment_method_detail($id)){
        $this->load->view('user_panel/laundry_edit_payment_method_view',compact('pay'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function edit_payment_method()
    {
      $pay_id = $this->input->post("payment_id");
      $pay_method = $this->input->post("pay_method");
      $brand_name = $this->input->post("brand_name");
      $card_no = $this->input->post("card_no");
      $month = $this->input->post("month");
      $year = $this->input->post("year");
      $bank_name = $this->input->post("bank_name");
      $bank_address = $this->input->post("bank_address");
      $account_title = $this->input->post("account_title");
      $bank_short_code = $this->input->post("bank_short_code");

      $edit_data = array(
        'payment_method' => trim($pay_method), 
        'brand_name' => trim($brand_name),
        'bank_name' => trim($bank_name),
        'bank_address' => trim($bank_address),
        'account_title' => trim($account_title),
        'bank_short_code' => trim($bank_short_code),
        'card_number' => trim($card_no),
        'expirty_date' => trim($month). '/'.trim($year),
      );
          
      if( $id = $this->api->update_payment_details($pay_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-laundry/payment-method/'. $pay_id);
    }
    public function delete_payment_method()
    {
      $id = $this->input->post("id");     
      if( $this->api->delete_payment_detail($this->cust['cust_id'], $id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Payment Methods-------------------------------
  
  //Start User Educations----------------------------------
    public function education_list_view()
    {
      $educations = $this->user->get_customer_educations($this->cust['cust_id'],0);
      $this->load->view('user_panel/laundry_eduction_list_view', compact('educations'));    
      $this->load->view('user_panel/footer');
    }
    public function add_education_view()
    {
      $this->load->view('user_panel/laundry_add_education_view');   
      $this->load->view('user_panel/footer');
    }
    public function add_education()
    {
      $cust = $this->cust;
      $institute_name = $this->input->post("institute_name");
      $qualification = $this->input->post("qualification");
      $title = $this->input->post("title");
      $yr_qualification = $this->input->post("yr_qualification");
      $grade = $this->input->post("grade");
      $yr_attend = $this->input->post("yr_attend");
      $yr_attend_to = $this->input->post("yr_attend_to");
      $additional_info = $this->input->post("additional_info");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'institute_name' => $institute_name, 
        'qualification' => $qualification, 
        'description' => $additional_info,
        'start_date' => $yr_attend,
        'end_date' => $yr_attend_to,
        'grade' => $grade, 
        'qualify_year' => $yr_qualification, 
        'certificate_title' => $title, 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today, 
        'mod_datetime' => $today
      );

      if( $id = $this->api->add_user_education($add_data)) {
        $this->session->set_flashdata('success',$this->lang->line('added_successfully')); 
      } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }
      redirect('user-panel-laundry/education/add');
    }
    public function edit_education_view()
    {
      $edu_id = $this->uri->segment(3);
      if( $education = $this->user->get_customer_education_by_id($edu_id)){
        $this->load->view('user_panel/laundry_edit_education_view',compact('education'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function edit_education()
    {
      $edu_id = $this->input->post("education_id");
      $institute_name = $this->input->post("institute_name");
      $qualification = $this->input->post("qualification");
      $title = $this->input->post("title");
      $yr_qualification = $this->input->post("yr_qualification");
      $grade = $this->input->post("grade");
      $yr_attend = $this->input->post("yr_attend");
      $yr_attend_to = $this->input->post("yr_attend_to");
      $additional_info = $this->input->post("additional_info");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'edu_id' => (int) $edu_id,
        'institute_name' => $institute_name, 
        'qualification' => $qualification, 
        'description' => $additional_info,
        'start_date' => $yr_attend,
        'end_date' => $yr_attend_to,
        'grade' => $grade, 
        'qualify_year' => $yr_qualification, 
        'certificate_title' => $title, 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_education($edit_data)) {
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully')); 
      } else { $this->session->set_flashdata('error',$this->lang->line('update_failed'));  }

      redirect('user-panel-laundry/education/'.$edu_id);
    }
    public function delete_education()
    {
      $edu_id = $this->input->post("id");
      if( $id = $this->api->delete_user_education($edu_id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Educations------------------------------------
  
  //Start User Experience----------------------------------
    public function experience_list_view()
    {
      $experiences = $this->user->get_customer_experiences($this->cust['cust_id'], false, 0);
      $this->load->view('user_panel/laundry_experience_list_view', compact('experiences'));   
      $this->load->view('user_panel/footer');
    }
    public function add_experience_view()
    {
      $this->load->view('user_panel/laundry_add_experience_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_experience()
    {
      $cust = $this->cust;
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');
      
      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => date('Y-m-d',strtotime($start_date)),
        'end_date' => ($currently_working == "on" ) ? "NULL": date('Y-m-d',strtotime($end_date)),
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 0, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today,
        'mod_datetime' => $today
      );
      //echo json_encode($add_data); die();
      if( $id = $this->api->add_user_experience($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-laundry/experience/add');
    }
    public function edit_experience_view()
    {
      $exp_id = $this->uri->segment(3);
      if( $experience = $this->user->get_customer_experience_by_id($exp_id)){
        $this->load->view('user_panel/laundry_edit_experience_view',compact('experience'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function edit_experience()
    {
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'exp_id' => (int) $exp_id,
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_experience($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      redirect('user-panel-laundry/experience/'.$exp_id);
    }
    public function delete_experience()
    {
      $exp_id = $this->input->post("id");
      if( $id = $this->api->delete_user_experience($exp_id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Experience------------------------------------

  //Start User Other Experience----------------------------
    public function other_experience_list_view()
    {
      $experiences = $this->user->get_customer_experiences($this->cust['cust_id'],true, 0);
      if($experiences) {
        $this->load->view('user_panel/laundry_other_experience_list_view', compact('experiences'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function add_other_experience_view()
    {
      $this->load->view('user_panel/laundry_add_other_experience_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_other_experience()
    {
      $cust = $this->cust;
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 1, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today,
        'mod_datetime' => $today
      );

      if( $id = $this->api->add_user_experience($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-laundry/other-experience/add');
    }
    public function edit_other_experience_view()
    {
      $exp_id = $this->uri->segment(3);
      if( $experience = $this->user->get_customer_experience_by_id($exp_id)){
        $this->load->view('user_panel/laundry_edit_other_experience_view',compact('experience'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function edit_other_experience()
    {
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'exp_id' => (int) $exp_id,
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 1, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_experience($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      redirect('user-panel-laundry/other-experience/'.$exp_id);
    }
    public function delete_other_experience()
    {
      $exp_id = $this->input->post("id");
      if( $id = $this->api->delete_user_experience($exp_id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Other Experience------------------------------

  //Start User Skills--------------------------------------
    public function get_skills_by_category_id()
    {
      $cat_id = $this->input->post("category_id");
      echo json_encode($this->user->get_skill_by_cat_id($cat_id));
    }
    public function edit_skill_view()
    { 
      $skills = $this->user->get_customer_skills($this->cust['cust_id']);

      if($skills) {
        $junior_skills = explode(',', $skills['junior_skills']);
        $confirmed_skills = explode(',', $skills['confirmed_skills']);
        $senior_skills = explode(',', $skills['senior_skills']);
        $expert_skills = explode(',', $skills['expert_skills']);
      } 
      $category = $this->api->get_category();
      
      if( $this->cust['cust_id'] > 0 ){
        $this->load->view('user_panel/laundry_edit_skill_view',compact('category','junior_skills', 'confirmed_skills', 'senior_skills','expert_skills'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function edit_skill()
    { 
      $cust = $this->cust;
      $cat_id = $this->input->post("category_id");
      $skill_level = $this->input->post("skill_level");
      $skills = $this->input->post("skills");   
      $today = date('Y-m-d H:i:s');
      $cust_skills = $this->user->get_customer_skills($this->cust['cust_id']);

      if($skills != NULL AND !empty($skills)){ $skills = implode(',', $skills); } else{ $skills = "NULL"; }

      $edit_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'cs_id' => (int) $cust_skills['cs_id'],
        'cat_id' => $cat_id, 
        'skill_level' => $skill_level, 
        'junior_skills' => ($skill_level == "junior") ? $skills : $cust_skills['junior_skills'], 
        'confirmed_skills' => ($skill_level == "confirmed") ? $skills : $cust_skills['confirmed_skills'],
        'senior_skills' => ($skill_level == "senior") ? $skills : $cust_skills['senior_skills'],
        'expert_skills' => ($skill_level == "expert") ? $skills : $cust_skills['expert_skills'],
        'ext1' => "NULL", 
        'ext2' => "NULL", 
      );

      if( $this->api->update_user_skill($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-laundry/skill');
    } 
  //End User Skills----------------------------------------

  //Start User Portfolio-----------------------------------
    public function portfolio_list_view()
    { 
      $portfolios = $this->user->get_customer_portfolio($this->cust['cust_id']);
      if($portfolios) {
        $this->load->view('user_panel/laundry_portfolio_list_view', compact('portfolios'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function get_category_by_cat_type_id()
    { 
      $cat_type_id = $this->input->post("cat_type_id");
      echo json_encode($this->user->get_category_by_cat_type_id($cat_type_id));
    }
    public function get_subcategories_by_category_id($value='')
    { 
      $category_id = $this->input->post("category_id");
      echo json_encode($this->user->get_subcategory_by_category_id($category_id));
    }
    public function add_portfolio_view()
    { 
      $category_types = $this->user->get_category_types();
      $this->load->view('user_panel/laundry_add_portfolio_view', compact('category_types'));    
      $this->load->view('user_panel/footer');
    }
    public function add_portfolio()
    { 
      $cust = $this->cust;
      $title = $this->input->post("title");
      $category_id = $this->input->post("category_id");
      $subcat_ids = $this->input->post("subcat_ids");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      
      if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = "NULL"; }

      if( !empty($_FILES["portfolio"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('portfolio')) {   
          $uploads    = $this->upload->data();  
          $portfolio_url =  $config['upload_path'].$uploads["file_name"];  

          $add_data = array(
            'cust_id' => (int) $cust['cust_id'],
            'title' => $title, 
            'cat_id' => $category_id, 
            'subcat_ids' => $subcat_ids,
            'attachement_url' => $portfolio_url,
            'description' => $description,
            'tags' => "NULL",
            'cre_datetime' => $today,
          );
          
          if( $id = $this->api->register_portfolio($add_data)) {
            $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
          } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }
        }  else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));   }
      } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }

      redirect('user-panel-laundry/portfolio/add');
    }
    public function edit_portfolio_view()
    { 
      $id = $this->uri->segment(3);
      if( $portfolio = $this->api->get_portfolio_detail($id)){
        $category_types = $this->user->get_category_types();
        $this->load->view('user_panel/laundry_edit_portfolio_view',compact('portfolio','category_types'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function edit_portfolio()
    { 
      $portfolio_id = $this->input->post("portfolio_id");
      $title = $this->input->post("title");
      $category_id = $this->input->post("category_id");
      $subcat_ids = $this->input->post("subcat_ids");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      
      $portfolio = $this->api->get_portfolio_detail($portfolio_id);

      if($category_id == NULL OR empty($category_id)) { 
        $category_id = $portfolio['cat_id']; 
        if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = "NULL"; }
      }else{ if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = $portfolio['subcat_ids']; } }

      $old_attachement_url = $portfolio["attachement_url"];

      if( !empty($_FILES["portfolio"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('portfolio')) {   
          $uploads    = $this->upload->data();  
          $portfolio_url =  $config['upload_path'].$uploads["file_name"];  
          
          if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        } else { $portfolio_url = $old_attachement_url; }  
      } else { $portfolio_url = $old_attachement_url; }  
      
      $edit_data = array(
        'title' => $title, 
        'cat_id' => $category_id, 
        'subcat_ids' => $subcat_ids,
        'attachement_url' => $portfolio_url,
        'description' => nl2br(trim($description)),
      );
      
      if(  $this->api->update_portfolio($portfolio_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-laundry/portfolios/'.$portfolio_id);
    }
    public function delete_portfolio()
    {
      $id = $this->input->post("id");
      $portfolio = $this->api->get_portfolio_detail($id);
      $old_attachement_url = $portfolio["attachement_url"];

      if( $id = $this->api->delete_portfolio($id)) { 
        if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
          echo "success"; 
      } else { echo "failed"; }
    }
  //End User Portfolio-------------------------------------

  //Start User Language------------------------------------
    public function get_languages_master()
    {
      $filter = $this->input->post('q');
      echo json_encode($this->user->get_language_master($filter));
    }
    public function update_languages()
    {
      $udate_data = array(
        "cust_id" => $this->session->userdata('cust_id'),
        "lang_known" => $this->input->post('langs'),
      );
      if( $this->api->update_user_language($udate_data) ){ echo 'success'; }
      else { echo 'failed'; }
    }
  //End User Language--------------------------------------

  //Start User Overview------------------------------------
    public function update_overview()
    {
      $cust_id = $this->session->userdata('cust_id');
      $udate_data = [ "overview" => $this->input->post('overview')];
      if( $this->api->update_overview($cust_id, $udate_data) ){ echo 'success'; }
      else { echo 'failed'; }
    }
  //End User Overvi---ew-----------------------------------

  //Start User Document------------------------------------
    public function document_list_view()
    {
      $documents = $this->api->get_documents($this->cust['cust_id']);
      if($documents) {
        $this->load->view('user_panel/laundry_document_list_view', compact('documents'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function add_document_view()
    {
      $this->load->view('user_panel/laundry_add_document_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_document()
    {
      $cust = $this->cust;

      $title = $this->input->post("title");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      
      if( !empty($_FILES["document"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('document')) {   
          $uploads    = $this->upload->data();  
          $document_url =  $config['upload_path'].$uploads["file_name"];  

          $add_data = array(
            'cust_id' => (int) $cust['cust_id'],
            'doc_title' => $title, 
            'attachement_url' => $document_url,
            'doc_desc' => $description,
            'cre_datetime' => $today,
          );
          
          if( $id = $this->api->register_document($add_data)) {
            $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
          } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
        }  else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));  }
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-laundry/document/add');
    }
    public function edit_document_view()
    {
      $id = $this->uri->segment(3);
      if( $document = $this->user->get_document_detail($id)){
        $this->load->view('user_panel/laundry_edit_document_view',compact('document'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-laundry/user-profile'); }
    }
    public function edit_document()
    {   
      $document_id = $this->input->post("document_id");
      $title = $this->input->post("title");
      $description = $this->input->post("description");
      $document = $this->user->get_document_detail($document_id);
      $old_attachement_url = $document["attachement_url"];

      if( !empty($_FILES["document"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('document')) {   
          $uploads    = $this->upload->data();  
          $document_url =  $config['upload_path'].$uploads["file_name"];  
          
          if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        } else { $document_url = $old_attachement_url; }  
      } else { $document_url = $old_attachement_url; }  
      
      $edit_data = array(
        'doc_title' => $title, 
        'attachement_url' => $document_url,
        'doc_desc' => trim(nl2br($description)),
      );
      
      if(  $this->api->update_document($document_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-laundry/document/'.$document_id);
    }
    public function delete_document()
    {
      $id = $this->input->post("id");
      $document = $this->user->get_document_detail($id);
      $old_attachement_url = $document["attachement_url"];
      
      if( $this->api->delete_document($this->cust['cust_id'], $id)) { 
        if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        echo "success"; 
      } else { echo "failed"; }
    }
  //End User Document--------------------------------------

  //Start User Change Password-----------------------------
    public function change_password() {
      $this->load->view('user_panel/laundry_change_password_view');   
      $this->load->view('user_panel/footer');
    }
    public function update_password()
    {   
      $old_pass = $this->input->post('old_password');
      $new_pass = $this->input->post('new_password');
      if($this->api->validate_old_password($this->cust_id, $old_pass)){
        if($this->api->update_new_password(['cust_id' => $this->cust_id, 'password' => $new_pass])){
          $this->session->set_flashdata('success', $this->lang->line('password_changed'));          
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      } else { $this->session->set_flashdata('error', $this->lang->line('old_password_not_matched'));  }    
      redirect('user-panel-laundry/change-password');
    }
  //End User Change Password-------------------------------

  //Start Address Book-------------------------------------
    public function address_book_list()
    {
      $address_details = $this->api->get_consumers_addressbook($this->cust_id);
      $this->load->view('user_panel/laundry_address_book_view', compact('address_details'));
      $this->load->view('user_panel/footer');
    }
    public function add_address_book_view()
    {
      $countries = $this->user->get_countries();
      $this->load->view('user_panel/laundry_add_address_book_view',compact('countries'));   
      $this->load->view('user_panel/footer');
    }
    public function add_address_to_book()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $firstname = $this->input->post("firstname");
      $lastname = $this->input->post("lastname");
      $mobile_no = $this->input->post("mobile");
      $zipcode = $this->input->post("zipcode");
      $street = $this->input->post("street");
      $country = $this->input->post("country");
      $state = $this->input->post("state");
      $city = $this->input->post("city");
      $deliver_instruction = $this->input->post("deliver_instruction");
      $pickup_instruction = $this->input->post("pickup_instruction");
      $address1 = $this->input->post("address1");
      $address2 = $this->input->post("toMapID");
      $latitude = $this->input->post("latitude");
      $longitude = $this->input->post("longitude");
      $address_type = $this->input->post("address_type");
      $company = $this->input->post("company");
      $email_id = $this->input->post("email_id");
      $today = date('Y-m-d H:i:s');

      $country_id = $this->user->getCountryIdByName($country);
      $state_id = $this->user->getStateIdByName($state,$country_id);

      if($state_id == 0) {
        //add state and get state id
        $state_id = $this->api->register_state($country_id, $state);
        //add city under state id
        $city_id = $this->api->register_city($state_id, $city);
      } else {
        $city_id = $this->user->getCityIdByName($city,$state_id);
        if($city_id == 0) {
          //add city and get city id
          $city_id = $this->api->register_city($state_id, $city);
        }
      }

      if(!empty($_FILES["address_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/address-images/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('address_image')) {   
          $uploads    = $this->upload->data();  
          $image_url =  $config['upload_path'].$uploads["file_name"];  
        } else {  $image_url = "NULL"; }
      } else {  $image_url = "NULL"; }

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'firstname' => trim($firstname), 
        'lastname' => trim($lastname), 
        'mobile_no' => trim($mobile_no), 
        'street_name' => trim($street), 
        'addr_line1' => trim($address1), 
        'addr_line2' => (trim($address2) != "") ? trim($address2) : "NULL", 
        'city_id' => (int)($city_id), 
        'state_id' => (int)($state_id), 
        'country_id' => (int)($country_id), 
        'zipcode' => $zipcode,
        'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
        'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
        'image_url' => $image_url,
        'latitude' => trim($latitude), 
        'longitude' => trim($longitude), 
        'addr_type' => trim($address_type), 
        'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
        'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
        'cre_datetime' => $today,
      );
      //echo json_encode($add_data); die();
      if( $id = $this->api->register_new_address($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-laundry/address-book/add');
    }
    public function edit_address_book_view()
    {
      $id = $this->uri->segment(3);
      $address = $this->api->get_address_from_book($id);     
      $countries = $this->user->get_countries();
      $this->load->view('user_panel/laundry_edit_address_book_view',compact('countries','address'));    
      $this->load->view('user_panel/footer');
    }
    public function edit_address()
    {
      $cust = $this->cust;
      $id = $this->input->post("address_id");
      $firstname = $this->input->post("firstname");
      $lastname = $this->input->post("lastname");
      $mobile_no = $this->input->post("mobile");
      $zipcode = $this->input->post("zipcode");
      $street = $this->input->post("street");
      $country = $this->input->post("country");
      $state = $this->input->post("state");
      $city = $this->input->post("city");
      $deliver_instruction = $this->input->post("deliver_instruction");
      $pickup_instruction = $this->input->post("pickup_instruction");
      $address1 = $this->input->post("address1");
      $address2 = $this->input->post("toMapID");
      $latitude = $this->input->post("latitude");
      $longitude = $this->input->post("longitude");
      $address_type = $this->input->post("address_type");
      $company = $this->input->post("company");
      $email_id = $this->input->post("email_id");

      $country_id = $this->user->getCountryIdByName($country);
      $state_id = $this->user->getStateIdByName($state,$country_id);

      if($state_id == 0) {
        //add state and get state id
        $state_id = $this->api->register_state($country_id, $state);
        //add city under state id
        $city_id = $this->api->register_city($state_id, $city);
      } else {
        $city_id = $this->user->getCityIdByName($city,$state_id);
        if($city_id == 0) {
          //add city and get city id
          $city_id = $this->api->register_city($state_id, $city);
        }
      }
      $address = $this->user->get_address_from_book($id);
      $today = date('Y-m-d H:i:s');
      
      if( !empty($_FILES["address_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/address-images/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('address_image')) {   
          $uploads    = $this->upload->data();  
          $image_url =  $config['upload_path'].$uploads["file_name"];  
          if($address['image_url'] != "NULL" AND !empty($address['image_url'])) { unlink($address['image_url']); }
        } else {  $image_url = $address['image_url']; }
      } else {  $image_url = $address['image_url']; }

      $edit_data = array(
        'firstname' => trim($firstname), 
        'lastname' => trim($lastname), 
        'mobile_no' => trim($mobile_no), 
        'street_name' => trim($street), 
        'addr_line1' => trim($address1), 
        'addr_line2' => trim($address2), 
        'city_id' => (int)($city_id), 
        'state_id' => (int)($state_id), 
        'country_id' => (int)($country_id), 
        'zipcode' => $zipcode,
        'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
        'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
        'image_url' => $image_url,
        'latitude' => trim($latitude), 
        'longitude' => trim($longitude), 
        'addr_type' => trim($address_type), 
        'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
        'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
      );

      if( $this->api->update_existing_address($edit_data, $id)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-laundry/address-book/'.$id);
    }
    public function delete_address()
    {
      $address_id = $this->input->post('id');
      if( $this->api->delete_address_from_addressbook($address_id)) { echo "success"; 
      } else { echo "failed"; }
    }
  //End Address Book---------------------------------------

  //Start Laundry operator Profile-------------------------
    public function provider_profile()
    {
      $today = date("Y-m-d H:i:s");
      if($operator = $this->api->get_laundry_provider_profile($this->cust_id)) {
        $documents = $this->api->get_laundry_provider_documents($this->cust_id);
        $this->load->view('user_panel/laundry_provider_profile_view', compact('operator','documents'));
      } else {
        if($details = $this->api->get_deliverer_profile($this->cust_id)) {
          //echo json_encode($details); die();
          $insert_data = array(
            "firstname" => trim($details['firstname']),
            "lastname" => trim($details['lastname']),
            "email_id" => trim($details['email_id']),
            "contact_no" => trim($details['contact_no']),
            "address" => trim($details['address']),
            "zipcode" => trim($details['zipcode']),
            "promocode" => trim($details['promocode']),
            "carrier_type" => trim(strtolower($details['carrier_type'])),
            "country_id" => (int)($details['country_id']),
            "state_id" => (int)($details['state_id']),
            "city_id" => (int)($details['city_id']),
            "cust_id" => (int)($details['cust_id']),
            "cre_datetime" => trim($today),
            "company_name" => trim($details['company_name']),
            "introduction" => trim($details['introduction']),
            "latitude" => trim($details['latitude']),
            "longitude" => trim($details['longitude']),
            "gender" => trim($details['gender'])
            //"service_list" => trim($service_list),
          );
          $this->api->register_laundry_provider_profile($insert_data);
          $documents = $this->api->get_laundry_provider_documents($this->cust_id);
          $operator = $this->api->get_laundry_provider_profile($this->cust_id);
          $this->load->view('user_panel/laundry_provider_profile_view', compact('operator','documents'));
        } else {
          $cust = $this->cust;
          $countries = $this->user->get_countries($this->cust_id);
          $cust_id = $this->cust_id;
          $this->load->view('user_panel/create_laundry_provider_profile_view', compact('cust_id','countries','cust'));
        }
      }
      $this->load->view('user_panel/footer');
    }
    public function create_laundry_provider_profile()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $email_id = $this->input->post('email_id', TRUE);
      $contact_no = $this->input->post('contact_no', TRUE);
      $carrier_type = $this->input->post('carrier_type', TRUE);
      $address = $this->input->post('address', TRUE);
      $street_name = $this->input->post('street_name', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      if($state_id<=0) {
        $state_id = $this->input->post('state_id_hidden', TRUE);
      }
      $city_id = $this->input->post('city_id', TRUE);
      if($city_id<=0) {
        $city_id = $this->input->post('city_id_hidden', TRUE);
      }
      $zipcode = $this->input->post('zipcode', TRUE);
      $promocode = $this->input->post('promocode', TRUE);
      $company_name = $this->input->post('company_name', TRUE);
      $introduction = $this->input->post('introduction', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      $gender = $this->input->post('gender', TRUE);
      //$service_list = $this->input->post('service_list', TRUE);
      //$service_list = implode(',',$service_list);
      $today = date("Y-m-d H:i:s");
      
      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('avatar')) {   
            $uploads    = $this->upload->data();  
            $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
          } else { $avatar_url = "NULL"; }
        } else { $avatar_url = "NULL"; }

        if( !empty($_FILES["cover"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/cover-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('cover')) {   
            $uploads    = $this->upload->data();  
            $cover_url =  $config['upload_path'].$uploads["file_name"]; 
          } else{ $cover_url = "NULL"; }
        } else{ $cover_url = "NULL"; }

      $insert_data = array(
        "firstname" => trim($firstname),
        "lastname" => trim($lastname),
        "email_id" => trim($email_id),
        "contact_no" => trim($contact_no),
        "address" => trim($address),
        "zipcode" => trim($zipcode),
        "promocode" => trim($promocode),
        "carrier_type" => trim($carrier_type),
        "country_id" => (int)($country_id),
        "state_id" => (int)($state_id),
        "city_id" => (int)($city_id),
        "company_name" => trim($company_name),
        "introduction" => trim($introduction),
        "latitude" => trim($latitude),
        "longitude" => trim($longitude),
        "gender" => trim($gender),
        "avatar_url" => trim($avatar_url),
        "cover_url" => trim($cover_url),
        // "service_list" => $service_list,
        "cust_id" => (int)$cust_id,
        "shipping_mode" => 0,
        "cre_datetime" => $today,
        "street_name" => trim($street_name),
      );
      //echo json_encode($insert_data); die();
      if($this->api->register_laundry_provider_profile($insert_data)) {
        $this->api->make_consumer_deliverer($cust_id);
        $this->session->set_flashdata('success', $this->lang->line('profile_created_successfully!'));
      } else { $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found!')); }
      redirect('user-panel-laundry/provider-profile');
    }
    public function edit_provider_profile()
    {
      $operator = $this->api->get_laundry_provider_profile($this->cust_id);
      $countries = $this->user->get_countries($this->cust_id);
      $carrier_type = $this->user->carrier_type_list();
      $this->load->view('user_panel/edit_provider_profile_view', compact('operator','countries','carrier_type'));
      $this->load->view('user_panel/footer');
    }
    public function update_provider_profile()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)){ 
        $cust_id = $this->cust_id;
        $firstname = $this->input->post('firstname', TRUE);
        $lastname = $this->input->post('lastname', TRUE);
        $email_id = $this->input->post('email_id', TRUE);
        $contact_no = $this->input->post('contact_no', TRUE);
        $carrier_type = $this->input->post('carrier_type', TRUE);
        $address = $this->input->post('address', TRUE);
        $street_name = $this->input->post('street_name', TRUE);
        $country_id = $this->input->post('country_id', TRUE);
        $state_id = $this->input->post('state_id', TRUE);
        if($state_id<=0) {
          $state_id = $this->input->post('state_id_hidden', TRUE);
        }
        $city_id = $this->input->post('city_id', TRUE);
        if($city_id<=0) {
          $city_id = $this->input->post('city_id_hidden', TRUE);
        }
        $zipcode = $this->input->post('zipcode', TRUE);
        $promocode = $this->input->post('promocode', TRUE);
        $company_name = $this->input->post('company_name', TRUE);
        $introduction = $this->input->post('introduction', TRUE);
        $latitude = $this->input->post('latitude', TRUE);
        $longitude = $this->input->post('longitude', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $today = date("Y-m-d H:i:s");

        $profile = $this->api->get_operator_business_datails($cust_id);
        if( !empty($_FILES["avatar"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('avatar')) {   
            $uploads    = $this->upload->data();  
            $avatar_url =  $config['upload_path'].$uploads["file_name"];  
            if( $profile['avatar_url'] != "NULL" ){ unlink($profile['avatar_url']); }
          }
          $avatar_update = $this->api->update_laundry_provider_avatar($avatar_url, $cust_id);
        } else $avatar_update = 0;

        if( !empty($_FILES["cover"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/cover-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('cover')) {   
            $uploads    = $this->upload->data();  
            $cover_url =  $config['upload_path'].$uploads["file_name"]; 
            if( $profile['cover_url'] != "NULL" ){  unlink($profile['cover_url']);  }
          }
          $cover_update = $this->api->update_laundry_provider_cover($cover_url, $cust_id);
        } else $cover_update = 0;

        $update_data = array(
          "firstname" => trim($firstname),
          "lastname" => trim($lastname),
          "email_id" => trim($email_id),
          "contact_no" => trim($contact_no),
          "address" => trim($address),
          "zipcode" => trim($zipcode),
          "promocode" => trim($promocode),
          "carrier_type" => trim($carrier_type),
          "country_id" => (int)($country_id),
          "state_id" => (int)($state_id),
          "city_id" => (int)($city_id),
          "company_name" => trim($company_name),
          "introduction" => trim($introduction),
          "latitude" => trim($latitude),
          "longitude" => trim($longitude),
          "gender" => trim($gender),
          "street_name" => trim($street_name),
        );
        //var_dump($cover_update); die();
        //if(!$this->api->is_deliverer_email_exists($email_id, $cust_id)){
          if($this->api->update_provider($update_data, $cust_id) || $avatar_update || $cover_update ){
            $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
          } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        //} else{ $this->session->set_flashdata('error','Email already exists.'); }
        redirect('user-panel-laundry/edit-provider-profile');
      }
    }
  //End Laundry operator Profile---------------------------

  //Start Laundry operator Documents-----------------------
    public function edit_provider_document()
    {
      $document = $this->api->get_laundry_provider_documents($this->cust_id);
      $this->load->view('user_panel/edit_provider_document_view', compact('document'));
      $this->load->view('user_panel/footer');
    }
    public function add_provider_document()
    { 
      //var_dump($_POST); die();
      $cust_id = $this->cust_id;
      $doc_type = $this->input->post('doc_type', TRUE);
      $deliverer_id = (int) $this->user->get_deliverer_id($this->cust_id);
      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('attachment')) {   
          $uploads    = $this->upload->data();  
          $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachement_url = "NULL"; }
      } else { $attachement_url = "NULL"; }

      $insert_data = array(
        "attachement_url" => trim($attachement_url), 
        "cust_id" => $cust_id, 
        "deliverer_id" => $deliverer_id, 
        "is_verified" => "0", 
        "doc_type" => $doc_type, 
        "is_rejected" => "0",
        "upload_datetime" => date('Y-m-d H:i:s'),
        "verify_datetime" => "NULL",
      );  
      //var_dump($insert_data); die();
      if( $id = $this->api->register_operator_document($insert_data) ) {  
        $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
      } else {  $this->session->set_flashdata('error',$this->lang->line('upload_failed')); }
      redirect('user-panel-laundry/edit-provider-document');
    }
    public function provider_document_delete()
    {
      $doc_id = $this->input->post('id', TRUE);
      //if($this->api->delete_operator_document($doc_id)){ echo 'success'; } else { echo "failed"; }
      $documents = $this->api->get_old_operator_documents($doc_id);
      if($this->api->delete_operator_document($doc_id)) { 
        if($documents[0]['attachement_url'] != "NULL") {
          unlink($documents[0]['attachement_url']);
        } echo "success"; 
      } else { echo "failed"; } 
    }
  //End Laundry operator Documents-------------------------

  //Start bus operator business photos---------------------
    public function business_photos()
    {
      $photos = $this->api->get_laundry_provider_business_photos($this->cust_id);
      $this->load->view('user_panel/laundry_provider_business_photos_view', compact('photos'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_provider_business_photos_upload()
    {
      $cust_id = $this->cust_id;
      if( !empty($_FILES["business_photo"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/business-photos/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('business_photo')) {   
          $uploads    = $this->upload->data();  
          $photo_url =  $config['upload_path'].$uploads["file_name"];  
        }      
        if( $photo_id = $this->api->add_laundry_provider_business_photo($photo_url, $cust_id)) {          
          $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_update')); }
      } 
      redirect('user-panel-laundry/business-photos');
    }
    public function laundry_provider_business_photos_delete()
    {
      $photo_id = $this->input->post('id', TRUE);
      if($this->api->delete_laundry_provider_business_photo($photo_id)){ echo 'success'; } else { echo "failed"; } 
    }
  //End bus operator business photos-----------------------
  
  //Start Manage Laundry Charges---------------------------
    public function laundry_charges_list()
    {
      //echo json_encode($_POST); die();
      $charges = $this->api->get_laundry_charges_list($this->cust_id);
      $category = $this->api->get_laundry_category_list();
      $countries = $this->api->get_countries();
      $this->load->view('user_panel/laundry_charges_list_view',compact('charges','category','countries'));
      $this->load->view('user_panel/footer');
    }
    public function get_laundry_category()
    {
      echo json_encode($this->api->get_laundry_sub_category_list($cat_id));
    }
    public function get_laundry_sub_category()
    {
      $cat_id = $this->input->post('cat_id');
      echo json_encode($this->api->get_laundry_sub_category_list($cat_id));
    }
    public function laundry_charges_add_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
      $sub_cat_id = $this->input->post('sub_cat_id', TRUE); $sub_cat_id = (int)$sub_cat_id;
      $country_id = $this->input->post('country_id', TRUE); $country_id = (int)$country_id;
      $currency_sign = $this->input->post('currency_sign', TRUE);
      $charge_amount = $this->input->post('charge_amount', TRUE);
      $today = date('Y-m-d h:i:s');

      $insert_data = array(
        "cat_id" => trim($cat_id),
        "sub_cat_id" => trim($sub_cat_id),
        "cust_id" => trim($cust_id),
        "charge_amount" => trim($charge_amount),
        "country_id" => trim($country_id),
        "currency_sign" => trim($currency_sign),
        "cre_datetime" => trim($today),
      );
      //echo json_encode($insert_data); die();
      if(!$this->api->check_laundry_charge_details($insert_data)) {
        if($this->api->add_laundry_charge_details($insert_data)) {
          $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
        } else {  $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  } 
      } else { $this->session->set_flashdata('error',$this->lang->line('Rate already exists.'));  }  
      redirect('user-panel-laundry/laundry-charges-list');
    }
    public function laundry_charges_delete()
    {
      $charge_id = $this->input->post('charge_id', TRUE);
      if($this->api->delete_laundry_charge_details((int)$charge_id)){ echo 'success'; } else { echo 'failed';  } 
    }
    public function update_laundry_charge_amount()
    {
      $charge_id = $this->input->post('charge_id', TRUE);
      $new_amount = $this->input->post('new_amount', TRUE);
      if($this->api->update_laundry_charge_amount_details($new_amount, (int)$charge_id)){ echo json_encode('success'); } else { echo json_encode('failed');  } 
    }
  //End Manage Laundry Charges-----------------------------

  //Start laundry cancellation and charges-----------------
    public function laundry_provider_cancellation_charges_list()
    {
      $cancellation_details = $this->api->get_laundry_provider_cancellation_details($this->cust_id);
      $today = date('Y-m-d h:i:s');
      if(empty($cancellation_details)) {
        $cancellation_details = $this->api->get_master_cancellation_details();
        for($i=0; $i<sizeof($cancellation_details); $i++) {
          $insert_data = array(
            "cust_id" => (int)$this->cust_id,
            "charge_per" => $cancellation_details[$i]['cancellation_charge'],
            "country_id" => $cancellation_details[$i]['country_id'],
            "charge_status" => $cancellation_details[$i]['status'],
            "cre_datetime" => $today,
          );
          $this->api->register_laundry_provider_cancellation_details($insert_data);
        }
        $cancellation_details = $this->api->get_laundry_provider_cancellation_details($this->cust_id);
      }
      $countries = $this->api->get_countries();
      $this->load->view('user_panel/laundry_provider_cancellation_rescheduling_list_view',compact('cancellation_details','countries'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_cancellation_charges_add()
    {
      $cust_id = (int)$this->cust_id;
      $country_id = $this->input->post('country_id', TRUE);
      $charge_per = $this->input->post('charge_per', TRUE);
      $today = date('Y-m-d H:i:s');

      $insert_data = array(
        "cust_id" => (int)$this->cust_id,
        "charge_per" => trim($charge_per),
        "country_id" => (int)trim($country_id),
        "charge_status" => 1,
        "cre_datetime" => $today,
      );

      if(!$this->api->check_country_cancellation_charges($insert_data)) {
        if($this->api->register_laundry_provider_cancellation_details($insert_data) ) {
          $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); }
      } else { $this->session->set_flashdata('error',$this->lang->line('configuration_exists')); }  
      redirect('user-panel-laundry/laundry-provider-cancellation-charges-list');
    }
    public function laundry_cancellation_charges_delete()
    {
      $charge_id = $this->input->post('charge_id', TRUE);
      if($this->api->delete_cencellation_charges_details((int)$charge_id)){ echo 'success'; } else { echo 'failed';  } 
    }
    public function update_laundry_cancellation_percentage()
    {
      //echo json_encode($_POST); die();
      $charge_id = $this->input->post('charge_id', TRUE);
      $new_percentage = $this->input->post('new_percentage', TRUE);
      $update_data = array(
        "charge_per" => trim($new_percentage),
      );
      if($this->api->update_cancellation_percentage_details($update_data, (int)$charge_id)){ echo json_encode('success'); } else { echo json_encode('failed');  } 
    }
  //End laundry cancellation and charges-------------------

  //Start Favorite Deliverers For Laundry Providers--------
    public function favourite_deliverers()
    {
      $deliverers = $this->api->get_favourite_deliverers($this->cust_id);
      $this->load->view('user_panel/laundry_provider_favourite_deliverers_view',compact('deliverers'));    
      $this->load->view('user_panel/footer');
    }
    public function remove_deliverer_from_favourite()
    {
      $deliverer_id = $this->input->post('id');
      if($this->api->remove_favourite_deliverer($this->cust_id, $deliverer_id)){ echo 'success'; } else{ echo 'failed'; }
    }
    public function search_deliverer_details_view()
    {
      $cust_id = $this->cust_id;
      $id = $this->input->post('deliverer_id');
      $requested = $this->input->post('requested_deliverer');
      $profile = $this->api->get_courir_deliverer_profile($id);
      $customer = $this->api->get_consumer_datails($id);
      $reviews = $this->api->get_reviews($id);
      $accepted = $this->web_user->get_statuswise_order_counts($id,'accept');
      $in_progress = $this->web_user->get_statuswise_order_counts($id,'in_progress');
      $delivered = $this->web_user->get_statuswise_order_counts($id,'delivered');
      $verify_insurence = $this->web_user->verify_deliverer_document($id,'insurance');
      $verify_certificates = $this->web_user->verify_deliverer_document($id,'business');
      $is_delivered = $this->web_user->is_deliverer_delivered($id);
      // echo json_encode($is_delivered); die();
      $compact = compact('id', 'requested', 'profile', 'customer', 'reviews', 'accepted', 'in_progress', 'delivered', 'cust_id', 'verify_insurence', 'verify_certificates', 'is_delivered');
      $this->load->view('user_panel/laundry_provider_search_deliverer_details_view',$compact);
      $this->load->view('user_panel/footer');
    }
    public function search_for_deliverers()
    { 
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $customer_details = $this->api->get_user_details((int)$cust_id);
      
      $country_id = $this->input->post('country_id_src'); $country_id = (!empty($country_id))? (int) $country_id : 0;
      $state_id = $this->input->post('state_id_src'); $state_id = (!empty($state_id))? (int) $state_id : 0;
      $city_id = $this->input->post('city_id_src'); $city_id = (!empty($city_id))? (int) $city_id : 0;
      $rating = $this->input->post('rating_filter'); $rating = (!empty($rating))? (int) $rating : "0";
      $favorite = $this->input->post('favorite'); $favorite = ($favorite == 'on')? 1 : 1;

      $countries = $this->web_user->get_countries();
      $country_name = $this->web_user->get_country_name_by_id($country_id);
      $state_name = $this->web_user->get_state_name_by_id($state_id);
      $city_name = $this->web_user->get_city_name_by_id($city_id);
      
      $country_id1 = ($country_id>0)? (int) $country_id : $customer_details['country_id'];
      
      $filter = array(
        "country_id" => $country_id1,
        "state_id" => $state_id,
        "city_id" => $city_id,
        "rating" => $rating,
        "favorite" => $favorite,
      );  

      $deliverers = $this->web_user->search_for_deliverers($cust_id, $filter);
      $favourite_deliverer_ids = $this->web_user->get_favourite_deliverers_ids($this->cust_id);
       //echo json_encode($deliverers); die();
      $this->load->view('user_panel/laundry_provider_search_for_deliverer_list_view',compact("deliverers","favourite_deliverer_ids","countries","filter","country_id","state_id","city_id","rating","country_name","state_name","city_name","favorite"));
      $this->load->view('user_panel/footer');
    }
    public function make_deliverer_favourite()
    {
      $deliverer_id = $this->input->post('id');
      if($this->api->register_new_favourite_deliverer($this->cust_id, $deliverer_id)){ echo 'success'; } else{ echo 'failed'; }
    }
  //End Favorite Deliverers For Laundry Providers----------

  //Start Favorite Providers For Laundry Buyers------------
    public function favourite_provider()
    {
      $deliverers = $this->api->get_favourite_providers($this->cust_id);
      $this->load->view('user_panel/favourite_providers_view',compact('deliverers'));    
      $this->load->view('user_panel/footer');
    }
    public function search_for_provider()
    { 
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $customer_details = $this->api->get_user_details((int)$cust_id);
      
      $country_id = $this->input->post('country_id_src'); $country_id = (!empty($country_id))? (int) $country_id : 0;
      $state_id = $this->input->post('state_id_src'); $state_id = (!empty($state_id))? (int) $state_id : 0;
      $city_id = $this->input->post('city_id_src'); $city_id = (!empty($city_id))? (int) $city_id : 0;
      $rating = $this->input->post('rating_filter'); $rating = (!empty($rating))? (int) $rating : "0";
      $favorite = $this->input->post('favorite'); $favorite = ($favorite == 'on')? 1 : 1;

      $countries = $this->web_user->get_countries();
      $country_name = $this->web_user->get_country_name_by_id($country_id);
      $state_name = $this->web_user->get_state_name_by_id($state_id);
      $city_name = $this->web_user->get_city_name_by_id($city_id);
      
      $country_id1 = ($country_id>0)? (int) $country_id : $customer_details['country_id'];
      
      $filter = array(
        "country_id" => $country_id1,
        "state_id" => $state_id,
        "city_id" => $city_id,
        "rating" => $rating,
        "favorite" => $favorite,
      );  

      $deliverers = $this->api->search_for_providers($cust_id, $filter);
      $favourite_deliverer_ids = $this->api->get_favourite_providers_ids($this->cust_id);
      //echo json_encode($favourite_deliverer_ids); die();
      $this->load->view('user_panel/search_for_providers_list_view',compact("deliverers","favourite_deliverer_ids","countries","filter","country_id","state_id","city_id","rating","country_name","state_name","city_name","favorite"));
      $this->load->view('user_panel/footer');
    }
    public function make_provider_favourite()
    {
      $deliverer_id = $this->input->post('id');
      if($this->api->register_new_favourite_provider($this->cust_id, $deliverer_id)){ echo 'success'; } else{ echo 'failed'; }
    }
    public function remove_provider_from_favourite()
    {
      $deliverer_id = $this->input->post('id');
      if($this->api->remove_favourite_provider($this->cust_id, $deliverer_id)){ echo 'success'; } else{ echo 'failed'; }
    }
  //End Favorite Providers For Laundry Buyers--------------

  //Start Suggession and Support---------------------------
    public function get_support_list()
    {
      $support_list = $this->user->get_support_list($this->cust_id);
      $this->load->view('user_panel/laundry_support_list_view', compact('support_list'));
      $this->load->view('user_panel/footer');
    }
    public function get_support()
    {
      $this->load->view('user_panel/laundry_get_support_view');
      $this->load->view('user_panel/footer');
    }
    public function add_support_details()
    {
      $type = $this->input->post('type', TRUE);
      $description = $this->input->post('description', TRUE);
      $cust_id = $this->cust_id;
      $today = date('Y-m-d h:i:s');

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('attachment')) {   
          $uploads    = $this->upload->data();  
          $file_url =  $config['upload_path'].$uploads["file_name"];  
        } else {  $file_url = "NULL"; }
      } else {  $file_url = "NULL"; }

      $add_data = array(
        'type' => trim($type), 
        'description' => trim($description), 
        'file_url' => trim($file_url), 
        'cust_id' => (int) $cust_id, 
        'cre_datetime' => $today, 
        'response' => 'NULL', 
        'res_datetime' => 'NULL', 
        'status' => 'open', 
        'updated_by' => 0, 
      );
      
      //echo json_encode($add_data); die();
      if( $id = $this->user->register_new_support($add_data)) {
        $ticket_id = 'G-'.time().$id;
        $this->user->update_support_ticket($id, $ticket_id);
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-laundry/get-support');
    }
    public function view_support_reply_details()
    {
      $id = $this->uri->segment(3);
      $details = $this->user->get_support_details($id);
      $this->load->view('user_panel/laundry_view_support_reply_details', compact('details'));   
      $this->load->view('user_panel/footer');
    }
  //End Suggession and Support-----------------------------

  //Start Notification-------------------------------------
    public function user_notifications()
    {
      $cust = $this->cust;
      if($cust['user_type']) { $type = "Individuals"; } else { $type = "Business"; }
      $notifications = $this->api->get_notifications($cust['cust_id'], $type);        
      //echo $this->db->last_query();  die();
      $this->load->view('user_panel/laundry_notifications_view',compact('notifications'));
      $this->load->view('user_panel/footer'); 
    }
    public function notification_detail()
    {
      $id = $this->input->post("id");
      $notice = $this->api->get_notification($id);

      $this->load->view('user_panel/laundry_notifications_detail_view', compact('notice'));
      $this->load->view('user_panel/footer'); 
    }
    public function notifications_reads()
    {
      $cust = $this->cust;
      if($cust['user_type'] == 1 ){ $type ="Individuals"; } else { $type ="Business"; }
      $notification = $this->api->get_total_unread_notifications($cust['cust_id'], $type);
      foreach ($notification as $n) {
        $this->api->make_notification_read($n['id'], $cust['cust_id']);
      }
      echo 'success';
    }
  //End Notification---------------------------------------

  //Start Laundry Account/E-Wallet/Withdrawals-------------
    public function laundry_wallet()
    {
      $amount = $this->user->get_customer_account_withdraw_request_sum($this->cust_id, 'request');
      $balance = $this->user->check_user_account_balance($this->cust_id);
      $this->load->view('user_panel/laundry_account_history_list_view', compact('balance','amount'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_account_history()
    {
      $history = $this->api->get_laundry_account_history($this->cust_id);
      $this->load->view('user_panel/laundry_transaction_history_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_invoices_history()
    {
      $details = $this->api->get_laundry_invoices($this->cust_id);
      //echo json_encode($details); die();
      $this->load->view('user_panel/laundry_invoice_history_list_view', compact('details'));
      $this->load->view('user_panel/footer');
    }
    public function my_scrow()
    {
      $balance = $this->api->deliverer_scrow_master_list($this->cust_id);
      $this->load->view('user_panel/laundry_scrow_history_list_view', compact('balance'));
      $this->load->view('user_panel/footer');
    }
    public function user_scrow_history()
    {
      $history = $this->user->get_customer_scrow_history($this->cust_id);
      $this->load->view('user_panel/laundry_scrow_account_history_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_withdrawals_request()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'request');
      $this->load->view('user_panel/laundry_withdrawal_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_withdrawals_accepted()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'accept');
      $this->load->view('user_panel/laundry_withdrawal_accepted_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_withdrawals_rejected()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'reject');
      $this->load->view('user_panel/laundry_withdrawal_rejected_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_withdrawal_request_details()
    {
      $req_id = $this->input->post('req_id'); 
      $details = $this->user->withdrawal_request_details((int)$req_id);
      $this->load->view('user_panel/laundry_withdrawal_request_detail_view', compact('details'));
      $this->load->view('user_panel/footer');
    }
    public function create_laundry_withdraw_request()
    {
      //echo json_encode($_POST); die();
      $account_id = $this->input->post('account_id', TRUE);
      $currency_code = $this->input->post('currency_code', TRUE);
      $withdraw_amount = $this->input->post('amount', TRUE);
      $amount = $this->api->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
      $accounts = $this->api->get_payment_details($this->cust_id);
      $balance = $this->api->check_user_account_balance_by_id((int)$account_id);
      $final_balance = $balance['account_balance'] - $amount['amount'];
      $currency_code = $balance['currency_code'];

      $this->load->view('user_panel/laundry_withdraw_request_create_view',compact('final_balance','accounts','amount','currency_code'));
      $this->load->view('user_panel/footer');
    }
    public function withdraw_request_sent_laundry()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $password = $this->input->post('password', TRUE);
      $user_details = $this->api->get_user_details($this->cust_id);
      if($user_details['password'] == trim($password)) {
      $amount = $this->input->post('amount', TRUE);
      $currency_code= $this->input->post('currency_code', TRUE);
      $transfer_account_type = $this->input->post('transfer_account_type', TRUE);
      $balance = $this->api->check_user_account_balance($this->cust_id, $currency_code);
      $requested_amount = $this->user->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
      $withdrawable_amount = $balance - $requested_amount['amount'];

      if($amount <= $withdrawable_amount) {
        $accounts = $this->api->get_payment_details($this->cust_id);
        $transfer_account_number = $accounts["$transfer_account_type"];
        $today = date('Y-m-d h:i:s');
        $data = array(
          "user_id" => (int)$cust_id,
          "amount" => trim($amount),
          "req_datetime" => $today,
          "user_type" => 0,
          "response" => 'request',
          "response_datetime" => 'NULL',
          "reject_remark" => 'NULL',
          "transfer_account_type" => $transfer_account_type,
          "transfer_account_number" => trim($transfer_account_number),
          "currency_code" => trim($currency_code),
        );
        if( $sa_id = $this->api->user_account_withdraw_request($data) ) {
          $this->session->set_flashdata('success', $this->lang->line('withdraw_request_sent'));
        } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_send_request')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('entered_wrong_amount')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('unauthorized_access')); }
      redirect('user-panel-laundry/laundry-wallet');
    }
  //End Laundry Account/E-Wallet/Withdrawals---------------

  //Start laundry provider create booking------------------
    public function provider_create_laundry_bookings()
    {
      $provider = $this->api->get_laundry_provider_profile($this->cust_id);
      if($provider == null) {
        redirect(base_url('user-panel-laundry/provider-profile'),'refresh');
      } else {
        if(!isset($_SESSION['cat_id']) || $_SESSION['cat_id'] == 9) {
          $this->api->delete_cloth_coun_temp($this->cust_id);
        }
        // if(isset($_SESSION['__ci_last_regenerate'])) {
        //   $this->api->update_user_id_by_session_id($_SESSION['__ci_last_regenerate'], $this->cust_id);
        // }
        $address = $this->api->get_consumers_addressbook($this->cust_id);
        $walking_customers = $this->api->get_walkin_customer_list($this->cust_id);
        $user_details = $this->api->get_user_details($this->cust_id);
        $cust_id = $this->cust_id;
        $countries = $this->user->get_countries();
        $this->load->view('user_panel/provider_laundry_booking',compact('countries','address','user_details','cust_id','walking_customers'));     
        $this->load->view('user_panel/footer');
      }
    }
    public function provider_sub_category_append()
    {
      $cat_id = $this->input->post('cat_id');
      $cust_id = $this->cust_id;
      $sub_cat = $this->api->get_laundry_sub_category_list($cat_id);
      $counter = 1;
      $cnt = 0;
      //echo json_encode($sub_cat);
      foreach ($sub_cat as $cat) {
        $charg = array(
          'cat_id' => $cat['cat_id'],
          'sub_cat_id' => $cat['sub_cat_id'],
          'cust_id' => $cust_id,  
          'country_id' => $this->cust['country_id'], 
        );
        if($charges = $this->api->get_laundry_cherge($charg)) {
          $sub_cat_details = $this->api->get_sub_category_details((int)$cat['sub_cat_id']);
          $sub_cat_img = ($sub_cat_details['icon_url'] != 'NULL')?base_url($sub_cat_details['icon_url']):base_url('resources/no-image.jpg');
          $cnt = 1;
          $user_cart = $this->api->get_cloth_details_by_sub_cat_id_user_id($this->cust_id, (int)$cat['sub_cat_id']);

          if($user_cart) {
            echo '<a>'.
              '<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4" style="margin-bottom:-15px; padding-left: 0px; padding-right: 0px;">'.
                '<div class="hpanel plan-box" id="type_cat'.$cat['cat_id'].$counter.'">'.
                  '<div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px;">'.
                    '<img src="'.$sub_cat_img.'" style="max-height: 50px; width: auto;">'.
                    '<h5 class="text-success">'.$cat['sub_cat_name'].'</h5>'.
                    '<button id="min'.$cat['cat_id'].$counter.'" class="btn btn-warning btn-sm">-</button>&nbsp;&nbsp;<lable id="lable'.$cat['cat_id'].$counter.'" count="'.$user_cart['count'].'" cat_id="'.$cat['cat_id'].'" sub_cat_id="'.$cat['sub_cat_id'].'" cust_id="'.$this->cust_id.'">'.$user_cart['count'].'</lable>&nbsp;&nbsp;<button class="btn btn-success btn-sm" id="plus'.$cat['cat_id'].$counter.'">+</button>
                    </div>
                  </div>
                </div>
              </a>'; 
          } else {
            echo '<a>'.
                  '<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4" style="margin-bottom:-15px; padding-left: 0px; padding-right: 0px;">'.
                    '<div class="hpanel plan-box" id="type_cat'.$cat['cat_id'].$counter.'">'.
                      '<div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px;">'.
                        '<img src="'.$sub_cat_img.'" style="width: 50px; height: auto;">'.
                        '<h5 class="text-success">'.$cat['sub_cat_name'].'</h5>'.
                        '<button id="min'.$cat['cat_id'].$counter.'" class="btn btn-warning btn-sm">-</button>&nbsp;&nbsp;<lable id="lable'.$cat['cat_id'].$counter.'" count="0" cat_id="'.$cat['cat_id'].'" sub_cat_id="'.$cat['sub_cat_id'].'" cust_id="'.$this->cust_id.'">0</lable>&nbsp;&nbsp;<button class="btn btn-success btn-sm" id="plus'.$cat['cat_id'].$counter.'">+</button>
                        </div>
                      </div>
                    </div>
                  </a>'; 
          }

          // div_men
          // div_woman
          // div_child
          // div_other


          if($cnt == 1 && $cat['cat_id'] == 1) { echo '<script>$(".div_men").addClass("hidden"); $(".not_found_men").text("")</script>'; }
          if($cnt == 1 && $cat['cat_id'] == 2) { echo '<script>$(".div_woman").addClass("hidden"); $(".not_found_woman").text("")</script>'; }
          if($cnt == 1 && $cat['cat_id'] == 3) { echo '<script>$(".div_child").addClass("hidden"); $(".not_found_child").text("")</script>'; }
          if($cnt == 1 && $cat['cat_id'] == 4) { echo '<script>$(".div_other").addClass("hidden"); $(".not_found_other").text("")</script>'; }

          echo '<script>
                  $("#plus'.$cat['cat_id'].$counter.'").on("click", function(e) {
                    e.preventDefault();
                    var lable = $("#lable'.$cat['cat_id'].$counter.'");
                    var price_lable = $("#price_lable'.$cat['cat_id'].$counter.'");
                    var price = price_lable.attr("count");
                    var currency_sign = price_lable.attr("currency_sign");
                    var count = lable.attr("count");
                    var cust_id = lable.attr("cust_id");
                    var cat_id = lable.attr("cat_id");
                    var sub_cat_id = lable.attr("sub_cat_id");
                    var neww = parseInt(count)+1;
                    lable.attr("count", neww );
                    $("#lable'.$cat['cat_id'].$counter.'").text(neww);
                    $("#type_cat'.$cat['cat_id'].$counter.'").addClass("active");

                    if(cat_id == 1) {
                      var men_cnt = parseInt($("#type_category_man_count").text());
                      if(men_cnt >= 0) {
                        var men_total_cnt = men_cnt + 1;
                        $("#type_category_man_count").text(""+men_total_cnt+"");
                      } else {
                        $("#type_category_man_count").text(""+men_cnt+"");
                      }
                    }
                    if(cat_id == 2) {
                      var women_cnt = parseInt($("#type_category_woman_count").text());
                      var women_total_cnt = women_cnt + 1;
                      $("#type_category_woman_count").text(""+women_total_cnt+"");
                    }
                    if(cat_id == 3) {
                      var child_cnt = parseInt($("#type_category_child_count").text());
                      var child_total_cnt = child_cnt + 1;
                      $("#type_category_child_count").text(""+child_total_cnt+"");
                    }
                    if(cat_id == 4) {
                      var other_cnt = parseInt($("#type_category_other_count").text());
                      var other_total_cnt = other_cnt + 1;
                      $("#type_category_other_count").text(""+other_total_cnt+"");
                    }

                    $.ajax({
                      type: "POST",
                      url: "'.base_url('user-panel-laundry/add-temp-cloth-count').'", 
                      data: {cat_id:cat_id,sub_cat_id:sub_cat_id,cust_id:cust_id},
                      success: function(res){
                        console.log(res);
                      }
                    });
                  });

                  $("#min'.$cat['cat_id'].$counter.'").on("click", function(e) {
                    e.preventDefault();
                    var lable = $("#lable'.$cat['cat_id'].$counter.'");
                    var count = lable.attr("count");
                    var cust_id = lable.attr("cust_id");
                    var cat_id = lable.attr("cat_id");
                    var sub_cat_id = lable.attr("sub_cat_id");
                    if(parseInt(count)==0) {  var neww = 0;
                    } else { var neww = parseInt(count)-1;  }

                    
                    if(cat_id == 1) {
                      var men_cnt = parseInt($("#type_category_man_count").text());
                      if(men_cnt >= 1 && count > 0) {
                        var men_total_cnt = men_cnt - 1;
                        $("#type_category_man_count").text(""+men_total_cnt+"");
                      } else { $("#type_category_man_count").text(""+men_cnt+""); }
                    }
                    if(cat_id == 2) {
                      var women_cnt = parseInt($("#type_category_woman_count").text());
                      if(women_cnt >= 1 && count > 0) {
                        var women_total_cnt = women_cnt - 1;
                        $("#type_category_woman_count").text(""+women_total_cnt+"");
                      } else { $("#type_category_woman_count").text(""+women_cnt+""); }
                    }
                    if(cat_id == 3) {
                      var child_cnt = parseInt($("#type_category_child_count").text());
                      if(child_cnt >= 1 && count > 0) {
                        var child_total_cnt = child_cnt - 1;
                        $("#type_category_child_count").text(""+child_total_cnt+"");
                      } else { $("#type_category_child_count").text(""+child_cnt+""); }
                    }
                    if(cat_id == 4) {
                      var other_cnt = parseInt($("#type_category_other_count").text());
                      if(other_cnt >= 1 && count > 0) {
                        var other_total_cnt = other_cnt - 1;
                        $("#type_category_other_count").text(""+other_total_cnt+"");
                      } else { $("#type_category_other_count").text(""+other_cnt+""); }
                    }

                    if(neww==0) {
                      $("#type_cat'.$cat['cat_id'].$counter.'").removeClass("active");}
                      lable.attr("count", neww );
                      $("#lable'.$cat['cat_id'].$counter.'").text(neww);
    
                      $.ajax({
                        type: "POST",
                        url: "'.base_url('user-panel-laundry/subs-temp-cloth-count').'", 
                        data: {cat_id:cat_id,sub_cat_id:sub_cat_id,cust_id:cust_id},
                        success: function(res){
                          console.log(res);
                        }
                      });
                    });
                </script>';
          $counter++;
        } else { 
          if($cnt == 0 && $cat['cat_id'] == 1) { echo '<script>$(".div_men").removeClass("hidden"); $(".not_found_men").text("'.$this->lang->line("Category Not Found!").'")</script>'; }
          if($cnt == 0 && $cat['cat_id'] == 2) { echo '<script>$(".div_woman").removeClass("hidden"); $(".not_found_woman").text("'.$this->lang->line("Category Not Found!").'")</script>'; }
          if($cnt == 0 && $cat['cat_id'] == 3) { echo '<script>$(".div_child").removeClass("hidden"); $(".not_found_child").text("'.$this->lang->line("Category Not Found!").'")</script>'; }
          if($cnt == 0 && $cat['cat_id'] == 4) { echo '<script>$(".div_other").removeClass("hidden"); $(".not_found_other").text("'.$this->lang->line("Category Not Found!").'")</script>'; }
        }
      }
    }
    public function get_cloth_details_provider()
    {
      $b = 'not_found';
      $a= '';
      $pro_id = $this->input->post('provider_id');
      $details = $this->api->get_cloth_details($this->cust_id);
      $provider = $this->api->get_laundry_provider_profile($pro_id);
      if($details){
        $total = 0;
        $total_quant = 0;
        foreach($details as $item){
          $cat = $this->api->get_laundry_category($item['cat_id']);
          $sub_cat = $this->api->get_laundry_sub_category($item['sub_cat_id']);
          $total_quant = $total_quant+$item["count"];

          $rate = $this->api->get_laundry_charges_list_apend($provider['cust_id'], $item['sub_cat_id'] , $provider['country_id']);
          $total = $total + ($rate['charge_amount']*$item['count']);
          $a .= '<div class="row">
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <lable>'.ucwords($cat["cat_name"]).'-'.ucwords($sub_cat["sub_cat_name"]).'</lable>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">  
                    <lable>'.$item["count"].'</lable>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">  
                    <lable>'.$rate['charge_amount'].' '.$rate['currency_sign'].'</lable>
                  </div><br/>
                </div>';
        }

        $sp_charge = 0;
        $today =Date('Y-m-d');
        $day = trim(strtolower(date('D', strtotime(date('Y-m-d')))));
        if($special_charges = $this->api->get_laundry_special_charges_list($pro_id)){
          foreach ($special_charges as $charge) {
            if(strtotime($charge['start_date']) <= strtotime($today) &&  strtotime($charge['end_date']) > strtotime($today)){
              $percentage = $charge[''.$day.''];
              if($percentage > 0){
                $sp_charge++;
                $old_total = $total;
                $total = round(($total/100)*$percentage,2);
                $total = round($old_total-$total,2);
              }
            }
          }
        }

        if($sp_charge != 0){

          $a .= '<hr style="margin-top:10px;margin-bottom:10px;" /><div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("total").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <s style="color: gray"><lable>'.$old_total." ".$rate["currency_sign"].'</lable></s>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />
               <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("Special Laundry rate").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <lable style="color:red">'.$total." ".$rate["currency_sign"].'</lable>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />';

        }else{

            $a .= '<hr style="margin-top:10px;margin-bottom:10px;" /><div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("total").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <lable style="color:#3498db">'.$total." ".$rate["currency_sign"].'</lable>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />';
        }
        $a .= '<script>
                $("#booking_price").val("'.$total.'");
                $("#item_count").val("'.$total_quant.'");
                $("#currency_sign").val("'.$rate["currency_sign"].'");
              </script>';
          echo $a;
      } else { echo $b; }
    }
    public function update_provider_cart_price()
    {
      $pro_id = $this->input->post('provider_id');
      $to_address_check = $this->input->post('to_address_check');
      $from_address_check = $this->input->post('from_address_check');
      $details = $this->api->get_cloth_details($this->cust_id);
      $customer = $this->api->get_consumer_datails($this->cust_id);
      $provider = $this->api->get_laundry_provider_profile($pro_id);
      $avg = $this->api->get_cloth_details_volum($this->cust_id);
      $total_weight =  round(($avg['weight']/1000),2);
      //echo json_encode($total_weight); die();
      $transport_vehichle_type = 0;
      if($total_weight > 15){$transport_vehichle_type = 28;
      }else{ $transport_vehichle_type = 27; }
      //echo json_encode($transport_vehichle_type); die();

      if($this->input->post('from_address')!=NULL){
        $pickuptime = $this->input->post('pickuptime');
        $pickupdate = $this->input->post('pickupdate');
        $from_address = $this->input->post('from_address');
        $from_add = $this->api->get_address_from_book($from_address);
        $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$provider['latitude'],$provider['longitude']);
        //echo $distance_in_km; die();
        
        $data = array(
          "category_id" => 7,
          "total_weight" =>$total_weight, 
          "unit_id" => 1,
          "from_country_id" => $from_add['country_id'], 
          "from_state_id" => $from_add['state_id'], 
          "from_city_id" => $from_add['city_id'], 
          "to_country_id" => $provider['country_id'], 
          "to_state_id" => $provider['state_id'], 
          "to_city_id" => $provider['city_id'], 
          "service_area_type" =>'local',
          "transport_type" => 'earth',
          "width" => $avg['width'],
          "height" => $avg['height'],
          "length" => $avg['length'],
          "total_quantity" => 1,
        );
        if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
          $p_date = explode('/',$pickupdate);
          $n_date =  $p_date[1]."/".$p_date[0]."/".$p_date[2];
          $my_date = $n_date.' '.$pickuptime.":00";
          $my_date = str_replace('/', '-', $my_date);
          $m_date = new DateTime($my_date);
          $m_date->modify("+".$max_duration['max_duration']." hours");
          $delivery_datetime = $m_date->format("m-d-Y H:i:s");
          $delivery_datetime = str_replace('-', '/', $delivery_datetime); 

          $data =array(  
            "cust_id" => $provider['cust_id'],
            "category_id" => 7,
            "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
            "delivery_datetime" => $delivery_datetime,
            "total_quantity" => 1,
            "width" => $avg['width'],
            "height" => $avg['height'],
            "length" => $avg['length'],
            "total_weight" => $total_weight,
            "unit_id" =>1,
            "from_country_id" => $from_add['country_id'],
            "from_state_id" => $from_add['state_id'],
            "from_city_id" => $from_add['city_id'],
            "from_latitude" => $from_add['latitude'],
            "from_longitude" => $from_add['longitude'],
            "to_country_id" => $provider['country_id'],
            "to_state_id" => $provider['state_id'], 
            "to_city_id" => $provider['city_id'],
            "to_latitude" => $provider['latitude'],
            "to_longitude" => $provider['longitude'],
            "service_area_type" => "local",
            "transport_type" => "earth",
            "handling_by" => 0,
            "dedicated_vehicle" => 0,
            "package_value" => 0,
            "custom_clearance_by" =>"NULL",
            "old_new_goods" =>"NULL",
            "custom_package_value" =>0,
            "loading_time" =>0,
            "transport_vehichle_type" => $transport_vehichle_type, 
          );
          $order_price = $this->api_courier->calculate_order_price($data);
          //echo json_encode($order_price); die();
        }    
      }

      if($details){
        $total_quant = 0;
        $a = '';
        $total = 0;
        foreach($details as $item) {
          $cat = $this->api->get_laundry_category($item['cat_id']);
          $sub_cat = $this->api->get_laundry_sub_category($item['sub_cat_id']);
          $total_quant = $total_quant+$item["count"];

          $rate = $this->api->get_laundry_charges_list_apend($provider['cust_id'], $item['sub_cat_id'] , $provider['country_id']);
          $total = $total + ($rate['charge_amount']*$item['count']);
          
          $a  .= '<div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                      <lable>'.ucwords($cat["cat_name"]).'-'.ucwords($sub_cat["sub_cat_name"]).'</lable>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">  
                      <lable>'.$item["count"].'</lable>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">  
                      <lable>'.$rate["charge_amount"]." ".$rate["currency_sign"].'</lable>
                    </div>
                </div>';
        }


        $sp_charge = 0;
        $today =Date('Y-m-d');
        $day = trim(strtolower(date('D', strtotime(date('Y-m-d')))));
        if($special_charges = $this->api->get_laundry_special_charges_list($provider['cust_id'])){
          foreach ($special_charges as $charge) {
            if(strtotime($charge['start_date']) <= strtotime($today) &&  strtotime($charge['end_date']) > strtotime($today)){
              $percentage = $charge[''.$day.''];
              if($percentage > 0){
                $sp_charge++;
                $old_total = $total;
                $total = round(($total/100)*$percentage,2);
                $total = round($old_total-$total,2);
              }
            }
          }
        }

          $a .= '<script>
                    $("#booking_price").val("'.$total.'");
                    $("#item_count").val("'.$total_quant.'");
                    $("#currency_sign").val("'.$rate["currency_sign"].'");
                  </script>';
        if($sp_charge != 0){

          $a .= '<hr style="margin-top:10px;margin-bottom:10px;" /><div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("Laundry").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <s style="color: gray"><lable>'.$old_total." ".$rate["currency_sign"].'</lable></s>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />
               <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("Special Laundry rate").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <lable style="color:red">'.$total." ".$rate["currency_sign"].'</lable>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />';

        }else{

            $a .= '<hr style="margin-top:10px;margin-bottom:10px;" /><div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("Laundry").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <lable style="color:#3498db">'.$total." ".$rate["currency_sign"].'</lable>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />';
        }

        if($from_address_check) {
          $a .= '<script>
                  $("#pickup_courier_price").val("'.round($order_price["total_price"] ,2).'");
                  $("#is_pickup").val(1);               
                </script>
                <div class="row">
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <lable style="color:#3498db">'.$this->lang->line("pickup_").'</lable>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                    <lable style="color:#3498db">-</lable>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <lable style="color:#3498db">'.round($order_price["total_price"] ,2)." ".$order_price["currency_sign"].'</lable>
                  </div>
                </div><hr style="margin-top:10px;margin-bottom:10px;" />';
        }
        if($to_address_check) {
          $a .= ' <script>
                  $("#pickup_courier_price").val("'.round($order_price["total_price"] ,2).'"); 
                  $("#is_drop").val(1);  
                </script>
                <div class="row">
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <lable style="color:#3498db">'.$this->lang->line("Drop").'</lable>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                    <lable style="color:#3498db">-</lable>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <lable style="color:#3498db">'.round($order_price["total_price"] ,2)." ".$order_price["currency_sign"].'</lable>
                  </div>
                </div><hr style="margin-top:10px;margin-bottom:10px;" />';
        }
        if($from_address_check || $to_address_check){
          if($from_address_check && $to_address_check){
            $gr_total = $total + (round($order_price["total_price"],2)*2);
          } else {
            if($from_address_check || $to_address_check) {
              $gr_total = $total +  (round($order_price["total_price"],2));
            } else {
              $gr_total = $total +  (round($order_price["total_price"],2));
            }
          }

          // echo $gr_total; die();
          $a .= '<script>
                  $("#total_price").val("'.$gr_total.'");
                  $("#balance_amount").val("'.$gr_total.'");
                </script>
                <div class="row">
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <lable style="color:#3498db"><strong>'.$this->lang->line("Grand Total").'</strong></lable>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                    <lable style="color:#3498db">-</lable>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <lable style="color:#3498db"><strong>'.$gr_total." ".$order_price["currency_sign"].'</strong></lable>
                  </div>
                </div><hr style="margin-top:10px;margin-bottom:10px;" />';
          echo $a;
        } else {
          $a .= '<script>
                  $("#total_price").val("'.$total.'");
                  $("#balance_amount").val("'.$total.'");
                </script>
                <div class="row">
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <lable style="color:#3498db"><strong>'.$this->lang->line("Grand Total").'</strong></lable>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                    <lable style="color:#3498db">-</lable>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <lable style="color:#3498db"><strong>'.$total." ".$rate["currency_sign"].'</strong></lable>
                  </div>
                </div><hr style="margin-top:10px;margin-bottom:10px;" />';
          echo $a;
        }
      }
    }
    public function set_customers_info()
    {
      $addr_id = $this->input->post('from_address');
      $address = $this->api->get_address_from_book($addr_id);
      $countries = $this->user->get_countries();

      $a = '<div class="row form-group">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <label>'.$this->lang->line('first_name').'</label>
                <input type="text" class="form-control" id="f_name" name="f_name" value="'.$address['firstname'].'" autocomplete="none" autocomplete="off" />
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <label>'.$this->lang->line('last_name').'</label>
                <input type="text" class="form-control" id="l_name" name="l_name" value="'.$address['lastname'].'" autocomplete="none" autocomplete="off" />
              </div>
            </div>
            <div class="row form-group">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <label>'.$this->lang->line('mobile_number').'</label>
                <input type="text" class="form-control" id="customer_mobile" name="customer_mobile" value="'.$address['mobile_no'].'" autocomplete="none" autocomplete="off" />
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <label>'.$this->lang->line('email').'</label>
                <input type="text" class="form-control" id="customer_email" name="customer_email" value="'.$address['email'].'" autocomplete="none" autocomplete="off" />
                <input type="hidden" name="customer_street_name" id="customer_street_name" value="'.$address['street_name'].'">
                <input type="hidden" name="customer_addr_line1" id="customer_addr_line1" value="'.$address['addr_line1'].'">
                <input type="hidden" name="customer_addr_line2" id="customer_addr_line2" value="'.$address['addr_line2'].'">
                <input type="hidden" name="customer_latitude" id="customer_latitude" value="'.$address['latitude'].'">
                <input type="hidden" name="customer_longitude" id="customer_longitude" value="'.$address['longitude'].'">
              </div>
            </div>
            <div class="row form-group">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <label>'.$this->lang->line('country').'</label>
                <select class="form-control" name="customer_country" id="customer_country">
                  <option value="0">'.$this->lang->line('select').'</option>';
                  $country_name = $this->api->get_country_name_by_id($address['country_id']);    
                  $a .=  '<option value="'.$address['country_id'].'" selected>'.$country_name.'</option>';
                  foreach ($countries as $country){ 
                    $a .=  '<option value="'.$country['country_name'].'">'.$country['country_name'].'</option>';
                  } 
                  $a .= '</select>
              </div>
            </div>';
        echo json_encode($a);
    }
    public function provider_laundry_booking()
    {
      //echo json_encode($_POST); die();
      $today = date('Y-m-d H:i:s');
      $pickuptime = $this->input->post('pickuptime');
      $pickupdate = $this->input->post('pickupdate');
      //$payment_mode = $this->input->post('payment_mode');
      $payment_mode = "NULL";
      $cod_payment_type = $this->input->post('cod_payment_type');
      $expected_return_date = $this->input->post('expected_return_date');
      $description = $this->input->post('description');
      $walking_id = $this->input->post('walking_id');
      $pickup_courier_price = ($_POST['is_pickup']>0)?$_POST["pickup_courier_price"]:0;
      $drop_courier_price = ($_POST['is_drop']>0)?$_POST["pickup_courier_price"]:0;
      $total_price = $_POST['booking_price'] + $pickup_courier_price + $drop_courier_price;
      $drop_address_id = $this->input->post('from_address');
      
      $provider = $this->api->get_laundry_provider_profile($this->cust_id);

      //$provider = $this->api->get_user_details($this->cust_id);
      $country_currency = $this->api->get_country_currency_detail($provider["country_id"]);
      $laundry_advance_payment = $this->api->get_laundry_advance_payment($provider["country_id"]);
      $gonagoo_commission_amount = ($_POST['booking_price']/100)*$laundry_advance_payment['gonagoo_commission'];
      $booking_details = $this->api->get_cloth_details($this->cust_id);
      //echo json_encode($booking_details); die();
        
      $country_id = $_POST['customer_country'];
      $phone_code = $this->api->get_country_code_by_id($country_id);

      if(!isset($walking_id) || $walking_id == '') {
        $data = array(
          'firstname' => $_POST['f_name'],
          'lastname' => $_POST['l_name'],
          'phone' => $_POST['customer_mobile'],
          'email' => $_POST['customer_email'],
          'longitude' => $_POST['customer_longitude'],
          'latitude' => $_POST['customer_latitude'],
          'addr_line2' => $_POST['customer_addr_line2'],
          'addr_line1' => $_POST['customer_addr_line1'],
          'street_name' => $_POST['customer_street_name'],
          'country_id' => $country_id,
          'phone_code' => $phone_code,
          'cat_id' => 9,
          'cre_datetime' => date('Y-m-d H:i:s'),
          'provider_id' => $this->cust_id,
        );
        if(!$walking_cust_details = $this->api->check_walkin_customer_profile($data)) {
          $walkin_cust_id = $this->api->register_walkin_customer_profile($data);
        } else { $walkin_cust_id = $walking_cust_details[0]['walkin_id']; }
      } else { $walkin_cust_id = explode('~', $walking_id)[0]; }

      $walkin_cust = $this->api->get_walkin_customer_profile($walkin_cust_id);

      $data = array(
        'cust_id' => $walkin_cust['cust_id'], 
        'provider_id' => $this->cust_id, 
        'cre_datetime' => $today, 
        'booking_price' => $_POST['booking_price'], 
        'total_price' => trim($total_price),
        'currency_id' => $country_currency['currency_id'], 
        'currency_sign' => $_POST['currency_sign'], 
        //'pickup_payment' => ($_POST['pickup_payment']==1)?trim($total_price):0, 
        //'deliver_payment' => ($_POST['deliverer_payment']==1)?trim($total_price):0,
        'pickup_payment' => 0,
        'deliver_payment' => 0, 
        'country_id' => $walkin_cust['country_id'], 
        'paid_amount' => 0, 
        'pickup_courier_price' => $pickup_courier_price,  
        'drop_courier_price' => $drop_courier_price,  
        //'payment_mode' => ($_POST['payment_mode']=='cod')?'cod':'NULL',  
        'payment_mode' => "NULL",  
        'payment_by' => "NULL", 
        'payment_datetime' => 'NULL', 
        'balance_amount' => trim($total_price), 
        'complete_paid' => 0, 
        'booking_status' => "accepted", 
        'status_updated_by' => $walkin_cust['cust_id'], 
        'status_updated_by_user_type' => "customer", 
        'status_update_datetime' => $today, 
        'item_count' => $_POST['item_count'], 
        'cust_name' => $walkin_cust['firstname']." ".$walkin_cust['lastname'], 
        'cust_contact' => $walkin_cust['phone'], 
        'cust_email' => $walkin_cust['email'], 
        'operator_name' => $provider['firstname']." ".$provider['lastname'], 
        'operator_company_name' => $provider['company_name'], 
        'operator_contact_name' => $provider['contact_no'], 
        'is_pickup' => $_POST['is_pickup'], 
        'is_drop' => $_POST['is_drop'], 
        'gonagoo_commission_per' => $laundry_advance_payment['gonagoo_commission'], 
        'gonagoo_commission_amount' => $gonagoo_commission_amount, 
        'cat_id' => 9,
        'expected_return_date' => trim($expected_return_date), 
        'description' => trim($description), 
        'walkin_cust_id' => $walkin_cust_id,
        'cod_payment_type' => trim($cod_payment_type),
        'drop_address_id' => trim($drop_address_id),
      );
      //echo json_encode($data); die();
      if($booking_id = $this->api->create_laundry_booking($data)){
        //Delivery days
        $date1 = new DateTime($today);
        $date2 = new DateTime($expected_return_date);
        $interval = $date1->diff($date2);
        $days = $interval->days;

        //customer Name
        if( trim($walkin_cust['firstname']) == 'NULL' || trim($walkin_cust['lastname'] == 'NULL') ) {
          $parts = explode("@", trim($walkin_cust['email']));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($walkin_cust['firstname'])." ".trim($walkin_cust['lastname']); }

        //Provider Name
        if( trim($provider['firstname']) == 'NULL' || trim($provider['lastname'] == 'NULL') ) {
          $parts = explode("@", trim($provider['email_id']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider['firstname'])." ".trim($provider['lastname']); }

        $user_message = $this->lang->line('Hello').' '.$customer_name.', '.$this->lang->line('We have registered your laundry order as of').' '.date('d M Y', strtotime($today)).'. '.$this->lang->line('Order number').': '.$booking_id.'. '.$this->lang->line('Estimated production time').' '.$days.' '.$this->lang->line('days').'. '.$this->lang->line('Your team').'. '.$provider['company_name'];

        $provider_message = $this->lang->line('You have recieved Laundry order. Booking ID - ').$booking_id;

        $walkin_data = array(
          'service_booking_id' => (int)$booking_id, 
        );
        $this->api->update_walkin_customer($walkin_data, $walkin_cust_id);

        //Update Booking items Detail-------------------------------
          foreach ($booking_details as $bk){
            $rate = $this->api->get_laundry_charges_list_apend($this->cust_id, $bk['sub_cat_id'], $provider["country_id"]);
            $total_charge = $rate['charge_amount']*$bk['count'];
            $data2 =  array(
              'booking_id' => $booking_id, 
              'cat_id' => $bk['cat_id'], 
              'sub_cat_id' => $bk['sub_cat_id'] ,
              'quantity' => $bk['count'], 
              'single_unit_price' => $rate['charge_amount'], 
              'total_price' => $total_charge, 
              'currency_sign' => $rate['currency_sign'], 
              'cre_datetime' => date('Y-m-d H:i:s'), 
              'height' => $bk['height'], 
              'width' => $bk['width'], 
              'length' => $bk['length'], 
              'weight' => $bk['weight'], 
            );
            $booking_details_id = $this->api->create_laundry_booking_details($data2);
          }
        //Update Booking items Detail-------------------------------

        //*********Send Booking SMS to customer*********************
          $country_code = $this->api->get_country_code_by_id($walkin_cust["country_id"]);
          $this->api->sendSMS((int)$country_code.trim($walkin_cust['phone']), $user_message);
        //*********Send Booking SMS to customer*********************

        //*********Send Booking SMS to provider*********************
          $country_code = $this->api->get_country_code_by_id($provider["country_id"]);
          $this->api->sendSMS((int)$country_code.trim($provider['contact_no']), $provider_message);
        //*********Send Booking SMS to provider*********************

        //*********Send Email to customer***************************
          $this->api_sms->send_email_text($customer_name, trim($walkin_cust['email']),$this->lang->line('laundry booking confirmation'), trim($user_message));
        //*********Send Email to customer***************************

        //*********Send Email to provider***************************
          $this->api_sms->send_email_text($provider_name, trim($provider['email_id']),$this->lang->line('laundry booking confirmation'), trim($provider_message));
        //*********Send Email to provider***************************

        //*********Send push notification to provider***************
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details($this->cust_id);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('laundry booking confirmation'), 'type' => 'laundry-booking-confirmation', 'notice_date' => $today, 'desc' => $provider_message);
            $this->api->sendFCM($msg, $arr_provider_fcm_ids, $api_key);

            //Push Notification APN
            $msg_apn_provider =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $provider_message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
        //*********Send push notification to provider***************
      
        //*********Post courier_workroom & global_workroom**********
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $walkin_cust['cust_id'],
            'deliverer_id' => $this->cust_id,
            'text_msg' => $user_message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $walkin_cust['cust_id'],
            'type' => 'booking_status',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);

          $global_workroom_update = array(
            'service_id' => $booking_id,
            'cust_id' => $walkin_cust['cust_id'],
            'deliverer_id' => $this->cust_id,
            'sender_id' => $walkin_cust['cust_id'],
            'cat_id' => 9
          );
          $this->api->create_global_workroom($global_workroom_update);
        //*********Post courier_workroom & global_workroom**********  
        
        //echo json_encode($data); die();
        //*********courier booking**********************************
          if($_POST['is_pickup']=="1"){
            //echo 'in courier'; die();
            $from_add = $this->api->get_address_from_book($_POST['from_address']);
            $avg = $this->api->get_cloth_details_volum($this->cust_id);
            $volume = $avg['width'] * $avg['height'] * $avg['length'];
            $dimension_id = 0 ;
            if($dimensions = $this->api->get_dimension_id_for_laundry($volume)){
              $dimension_id = $dimensions['dimension_id']; 
            }
            $total_weight =  round(($avg['weight']/1000),2);
            $transport_vehichle_type = 0;
            if($total_weight > 15){
              $transport_vehichle_type = 28;
            }else{
              $transport_vehichle_type = 27;
            }

            $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$provider['latitude'],$provider['longitude']);
            
            $data = array(
              "category_id" => 7,
              "total_weight" =>$total_weight, 
              "unit_id" => 1,
              "from_country_id" => $from_add['country_id'], 
              "from_state_id" => $from_add['state_id'], 
              "from_city_id" => $from_add['city_id'], 
              "to_country_id" => $provider['country_id'], 
              "to_state_id" => $provider['state_id'], 
              "to_city_id" => $provider['city_id'], 
              "service_area_type" =>'local',
              "transport_type" => 'earth',
              "width" => $avg['width'],
              "height" => $avg['height'],
              "length" => $avg['length'],
              "total_quantity" => 1,
            );
            if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
              $p_date = explode('/',$pickupdate);
              $n_date =  $p_date[1]."/".$p_date[0]."/".$p_date[2];
              $my_date = $n_date.' '.$pickuptime.":00";
              $my_date = str_replace('/', '-', $my_date);
              $m_date = new DateTime($my_date);
              $ex_date = $m_date;
              $ex_date->modify("+24 hours");
              $ex_datetime = $ex_date->format("m-d-Y H:i:s");
              $expiry_date = str_replace('-', '/', $ex_datetime);
              $m_date->modify("+".$max_duration['max_duration']." hours");
              $delivery_datetime = $m_date->format("m-d-Y H:i:s");
              $delivery_datetime = str_replace('-', '/', $delivery_datetime);  
            }

            $data =array(  
              "cust_id" => $this->cust_id,
              "category_id" => 7,
              "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
              "total_quantity" => 1,
              "delivery_datetime" => $delivery_datetime,
              "width" => $avg['width'],
              "height" => $avg['height'],
              "length" => $avg['length'],
              "total_weight" => $total_weight,
              "unit_id" =>1,
              "from_country_id" => $from_add['country_id'],
              "from_state_id" => $from_add['state_id'],
              "from_city_id" => $from_add['city_id'],
              "from_latitude" => $from_add['latitude'],
              "from_longitude" => $from_add['longitude'],
              "to_country_id" => $provider['country_id'],
              "to_state_id" => $provider['state_id'], 
              "to_city_id" => $provider['city_id'],
              "to_latitude" => $provider['latitude'],
              "to_longitude" => $provider['longitude'],
              "service_area_type" => "local",
              "transport_type" => "earth",
              "handling_by" => 0,
              "dedicated_vehicle" => 0,
              "package_value" => 0,
              "custom_clearance_by" =>"NULL",
              "old_new_goods" =>"NULL",
              "custom_package_value" =>0,
              "loading_time" =>0,
              "transport_vehichle_type" => $transport_vehichle_type, 
            );
            $order_price = $this->api_courier->calculate_order_price($data);
            //echo json_encode($order_price); die();
            if ($description == '') { $description = $this->lang->line('Laundry Parcel'); }
            if(isset($cod_payment_type)) {
              if(($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')){
                $cod_payment_type = 'at_pickup';
                $pickup_payment = $order_price['total_price'];
                $deliver_payment = 0;
              } else {
                $cod_payment_type = 'at_deliver';
                $pickup_payment = 0;
                $deliver_payment = $order_price['total_price'];
              }
            } else { 
              $cod_payment_type = 'NULL';
              $pickup_payment = 0;
              $deliver_payment = $order_price['total_price']; 
            }
            
            $pick_date = explode('/',$pickupdate);
            $pick_date =  $pick_date[2]."-".$pick_date[0]."-".$pick_date[1];
            
            $delivery_datetime = explode('/',$delivery_datetime);
            $tm = explode(' ',$delivery_datetime[2]);
            $delivery_datetime = $tm[0]."-".$delivery_datetime[0]."-".$delivery_datetime[1].' '.$tm[1];

            $expiry_date = explode('/',$expiry_date);
            $tm = explode(' ',$expiry_date[2]);
            $expiry_date = $tm[0]."-".$expiry_date[0]."-".$expiry_date[1].' '.$tm[1];

            $pending_service_charges = trim($_POST['booking_price']) + trim($drop_courier_price);
            
            $my_string ="cust_id=".$this->cust_id."&from_address=".$from_add['street_name']."&from_address_name=".$from_add['firstname']." ".$from_add['lastname']."&from_address_contact=".$from_add['mobile_no']."&from_latitude=".$from_add['latitude']."&from_longitude=".$from_add['longitude']."&from_country_id=".$from_add['country_id']."&from_state_id=".$from_add['state_id']."&from_city_id=".$from_add['city_id']."&from_addr_type=0&from_relay_id=0&to_address=".$provider['address']."&to_address_name=".$provider['firstname']." ".$provider['lastname']."&to_address_contact=".$provider['contact_no']."&to_country_id=".$provider['country_id']."&to_state_id=".$provider['state_id']."&to_city_id=".$provider['city_id']."&to_latitude=".$provider['latitude']."&to_longitude=".$provider['longitude']."&from_address_info=".$from_add['street_name']."&to_address_info=".$provider['address']."&to_addr_type=0&to_relay_id=0&pickup_datetime=".$pick_date." ".$pickuptime.":00&delivery_datetime=".$delivery_datetime."&expiry_date=".$expiry_date."&order_type=Shipping_Only&service_area_type=local&order_contents=[0]&order_description=".trim($description)."&deliver_instructions=".$from_add['deliver_instructions']."&pickup_instructions=".$from_add['pickup_instructions']."&transport_type=earth&vehical_type_id=".$transport_vehichle_type."&dedicated_vehicle=0&with_driver=0&insurance=0&package_value=0&handling_by=0&total_quantity=[1]&width=[".$avg['width']."]&height=[".$avg['height']."]&length=[".$avg['length']."]&total_weight=[".(string)$total_weight."]&unit_id=[1]&advance_payment=0&payment_method=NULL&pickup_payment=".$pickup_payment."&deliver_payment=".$deliver_payment."&payment_mode=cod&visible_by=0&from_address_email=".$from_add['email']."&to_address_email=".$provider['email_id']."&ref_no=".$booking_id."&dimension_id=[".$dimension_id."]&category_id=7&custom_clearance_by=NULL&old_new_goods=NULL&custom_package_value=0&need_tailgate=0&loading_time=0&dangerous_goods=[0]&is_laundry=1&cod_payment_type=".$cod_payment_type."&pending_service_charges=".trim($pending_service_charges)."&service_type=Laundry";
            //echo json_encode($my_string); die();
            $ch = curl_init(base_url()."api/book-courier-order"); // url to send sms
            curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
            curl_setopt($ch, CURLOPT_POST, 1); // method to call url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
            curl_setopt($ch, CURLOPT_POSTFIELDS,$my_string); 
            $courier_booking = curl_exec($ch); // execute url and save response
            curl_close($ch); // close url connection
            //echo json_encode($courier_booking); die();

            //Update courier Id in Laundry
            $order_details = $this->api->get_courier_details_by_ref_id($this->cust_id, $booking_id);
            $order_id_update = array(
              'pickup_order_id' => (int)trim($order_details['order_id'])
            );
            $this->api->laundry_booking_update($order_id_update, $booking_id);
          }
        //*********courier booking**********************************

        //Flush customer cart after booking complete
        $this->api->delete_cloth_coun_temp($this->cust_id);
      }

      if($payment_mode == 'cod' || $payment_mode == 'bank') {
        redirect(base_url('user-panel-laundry/accepted-laundry-bookings'),'refresh');
      } else {
        redirect(base_url('user-panel-laundry/laundry-booking-payment-by-provider/'.$booking_id),'refresh');
      }
    }
  //End laundry provider create booking--------------------

  //Start Laundry Payment By Provider----------------------
    public function laundry_booking_payment_by_provider()
    {
      //echo json_encode($_POST); die();
      if( !is_null($this->input->post('booking_id')) ) { $booking_id = (int) $this->input->post('booking_id'); }
      else if(!is_null($this->uri->segment(3))) { $booking_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-laundry/provider-accepted-laundry-bookings'); }
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $details = $this->api->laundry_booking_details($booking_id);
      $this->load->view('user_panel/laundry_provider_payment_view', compact('details','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_booking_mark_complete_paid()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $cust_id = (int)$booking_details['provider_id'];
      $today = date('Y-m-d H:i:s');

      $data = array(
        'paid_amount' => $booking_details['total_price'],
        'payment_method' => 'NULL', 
        'payment_mode' => (isset($_POST['page_name']) && trim($_POST['page_name']) == 'provider_laundry_pending_invoices')?'bank':'cod', 
        'payment_by' => 'web', 
        'payment_datetime' => trim($today),
        'balance_amount' => '0', 
        'complete_paid' => 1,  
        'drop_payment_complete' => ($booking_details['is_drop'] == 1)?1:0,  
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        if($booking_details['walkin_cust_id'] > 0) {
          $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['phone'];
          $email_id = $user_details['email'];
          $state_id = 0;
          $city_id = 0;
        } else {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['mobile1'];
          $email_id = $user_details['email1'];
          $state_id = $user_details['state_id'];
          $city_id = $user_details['city_id'];

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        }

        //SMS to customer
        $country_code = $this->api->get_country_code_by_id($country_id);
        $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id;
        $this->api->sendSMS((int)$country_code.trim($mobile), $message);
        
        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($email_id));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email1']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);

        /*************************************** Payment Section ****************************************/
          /* COD payment invoice------------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

            $user_country = $this->api->get_country_details(trim($country_id));
            $user_state = $this->api->get_state_details(trim($state_id));
            $user_city = $this->api->get_city_details(trim($city_id));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($email_id)));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($booking_details['total_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Booking Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_id.'_laundry_booking.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_id);
            //Update to workroom
            $workroom_update = array(
              'order_id' => $booking_id,
              'cust_id' => $booking_details['cust_id'],
              'deliverer_id' => $booking_details['provider_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "laundry-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $booking_details['provider_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $customer_name,
              'deliverer_name' => $provider_name,
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 9
            );
            $this->api->update_to_workroom($workroom_update);
          /* -------------------------------------------------------------------- */
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
            $emailAddress = trim($email_id);
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //Deduct Gonagoo Commission from provider account-------------------------
            if($customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_customer_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($booking_details['currency_sign']));
            }

            $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($booking_details['currency_sign']),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => 9,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------
          
          /* Ownward Laundry commission invoice---------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_details['booking_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
          /* -------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $provider_details['email1'];
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        /*************************************** Payment Section ****************************************/

        $this->session->set_flashdata('success', $this->lang->line('Booking status updated successfully.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      if(isset($_POST['page_name']) && trim($_POST['page_name']) == 'provider_laundry_pending_invoices') {
        redirect('user-panel-laundry/provider-laundry-pending-invoices');
      } else {
        redirect('user-panel-laundry/accepted-laundry-bookings');
      }
    }
    public function provider_confirm_payment_stripe()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id', TRUE); $booking_id = (int) $booking_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $today = date('Y-m-d H:i:s');
      $booking_details = $this->api->laundry_booking_details($booking_id);
      //echo json_encode($booking_details); die();
      $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id;
      $data = array(
        'paid_amount' => $booking_details['total_price'],
        'payment_method' => trim($payment_method), 
        'transaction_id' => trim($transaction_id), 
        'payment_mode' => 'online', 
        'payment_by' => 'web', 
        'payment_datetime' => trim($today),
        'balance_amount' => '0', 
        'complete_paid' => 1, 
        'drop_payment_complete' => ($booking_details['is_drop'] == 1)?1:0, 
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        if($booking_details['walkin_cust_id'] > 0) {
          $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['phone'];
          $email_id = $user_details['email'];
          $state_id = 0;
          $city_id = 0;
        } else {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['mobile1'];
          $email_id = $user_details['email1'];
          $state_id = $user_details['state_id'];
          $city_id = $user_details['city_id'];

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        }

        //SMS to customer
        $country_code = $this->api->get_country_code_by_id($country_id);
        $this->api->sendSMS((int)$country_code.trim($mobile), $message);
        $this->api_sms->send_sms_whatsapp((int)$country_code.trim($mobile), $message);
        
        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($email_id));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email1']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);

        /*************************************** Payment Section ****************************************/
          if($booking_details['walkin_cust_id'] == 0) {
            //Add Payment to customer account---------------------------------------
              if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //----------------------------------------------------------------------

            //deduct Payment from customer account----------------------------------
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //----------------------------------------------------------------------
          }

          //Add Payment to Provider account-----------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $booking_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            }

            $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'advance',
              "amount" => trim($booking_details['total_price']),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          /* Stripe payment invoice------------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

            $user_country = $this->api->get_country_details(trim($country_id));
            $user_state = $this->api->get_state_details(trim($state_id));
            $user_city = $this->api->get_city_details(trim($city_id));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($email_id)));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($booking_details['total_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Booking Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_id.'_laundry_booking.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_id);
            //Update to workroom
            $workroom_update = array(
              'order_id' => $booking_id,
              'cust_id' => $booking_details['cust_id'],
              'deliverer_id' => $booking_details['provider_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "laundry-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $booking_details['provider_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $customer_name,
              'deliverer_name' => $provider_name,
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 9
            );
            $this->api->update_to_workroom($workroom_update);
          /* ----------------------------------------------------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
            $emailAddress = $booking_details['cust_email'];
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* --------------------------Generate Ticket pdf----------------------- */ 
            $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
          
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
            QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
            $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
            $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
            if(rename($img_src, $img_dest));
          
            $html ='<page format="100x100" orientation="L" style="font: arial;">';
            $html .='
            <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                <h6>'.$this->lang->line('slider_heading1').'</h6>
              </div>
            </div>';
            $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line('Booking Details').'<br />
                <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                <br />
                <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
              <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
              <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
              <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                  <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
              <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
          
            $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
            $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
            foreach($booking_det as $dtls){
              $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
            }
            $html .= '</tbody></table></div></page>';
            $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
            $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
            rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
            //whatsapp api send ticket
            $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
            $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
            $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
            $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
          /* --------------------------Generate Ticket pdf----------------------- */
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = $booking_details['cust_email'];
            $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
            $fileName = $my_laundry_booking_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

          //Deduct Gonagoo Commission from provider account-------------------------
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "type" => 1,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($booking_details['currency_sign']),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => 9,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------
          
          /* Ownward Laundry commission invoice------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_details['booking_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
          /* ----------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $provider_details['email1'];
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */

          //Update Courier Payment----------------------------------------------------
          if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
            $courier_update_data = array(
              "payment_method" => 'stripe',
              "payment_mode" => 'payment',
              "paid_amount" => $booking_details['pickup_courier_price'],
              "transaction_id" => trim($transaction_id),
              "today" => trim($today),
              "cod_payment_type" => 'NULL',
              "order_id" => $booking_details['pickup_order_id'],
            );
            $this->courier_payment_confirm($courier_update_data);
          }
        //Update Courier Payment----------------------------------------------------
        /*************************************** Payment Section ****************************************/

        $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-laundry/accepted-laundry-bookings');
    }
    public function provider_confirm_payment_mtn()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id', TRUE);
      $phone_no = $this->input->post('phone_no', TRUE);
      $today = date('Y-m-d H:i:s'); 
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $total_price = trim($booking_details['total_price']);
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$total_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      //$contents = curl_exec($ch);
      //$mtn_pay_res = json_decode($contents, TRUE);
      //var_dump($mtn_pay_res); die();
      
      $payment_method = 'mtn';
      $transaction_id = '1234567890';
      //$transaction_id = trim($mtn_pay_res['TransactionID']);
      
      $payment_data = array();
      // if($mtn_pay_res['StatusCode'] === "01"){
      if(1){
        $data = array(
          'paid_amount' => $booking_details['total_price'],
          'payment_method' => trim($payment_method), 
          'transaction_id' => trim($transaction_id), 
          'payment_mode' => 'online', 
          'payment_by' => 'web', 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1, 
          'drop_payment_complete' => ($booking_details['is_drop'] == 1)?1:0, 
        );
        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id;
          if($booking_details['walkin_cust_id'] > 0) {
            $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
            $country_id = $user_details['country_id'];
            $mobile = $user_details['phone'];
            $email_id = $user_details['email'];
            $state_id = 0;
            $city_id = 0;
          } else {
            $user_details = $this->api->get_user_details($booking_details['cust_id']);
            $country_id = $user_details['country_id'];
            $mobile = $user_details['mobile1'];
            $email_id = $user_details['email1'];
            $state_id = $user_details['state_id'];
            $city_id = $user_details['city_id'];
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
                $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
          }
          
          //SMS to customer
          $country_code = $this->api->get_country_code_by_id($country_id);
          $this->api->sendSMS((int)$country_code.trim($mobile), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($mobile), $message);
          
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($email_id));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

          $provider_details = $this->api->get_user_details($booking_details['provider_id']);
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email1']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['provider_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          /*************************************** Payment Section ****************************************/
            if($booking_details['walkin_cust_id'] == 0) {
              //Add Payment to customer account---------------------------------------
                if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
                } else {
                  $insert_data_provider_master = array(
                    "user_id" => $booking_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($booking_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                  $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                }

                $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$booking_id,
                  "user_id" => (int)$booking_details['cust_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($booking_details['total_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($booking_details['currency_sign']),
                  "cat_id" => 9
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------

              //deduct Payment from customer account----------------------------------
                $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$booking_id,
                  "user_id" => (int)$booking_details['cust_id'],
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'payment',
                  "amount" => trim($booking_details['total_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($booking_details['currency_sign']),
                  "cat_id" => 9
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------
            }

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            /* Stripe payment invoice------------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

              $user_country = $this->api->get_country_details(trim($country_id));
              $user_state = $this->api->get_state_details(trim($state_id));
              $user_city = $this->api->get_city_details(trim($city_id));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($email_id)));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($booking_details['total_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Booking Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_id.'_laundry_booking.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_id);
              //Update to workroom
              $workroom_update = array(
                'order_id' => $booking_id,
                'cust_id' => $booking_details['cust_id'],
                'deliverer_id' => $booking_details['provider_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  "laundry-invoices/".$pdf_name,
                'cre_datetime' => $today,
                'sender_id' => $booking_details['provider_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $customer_name,
                'deliverer_name' => $provider_name,
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 9
              );
              $this->api->update_to_workroom($workroom_update);
            /* ----------------------------------------------------------------------- */
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
              $emailAddress = trim($email_id);
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */
            
            /* --------------------------Generate Ticket pdf----------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* --------------------------Generate Ticket pdf----------------------- */ 
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $booking_details['cust_email'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */

            //Deduct Gonagoo Commission from provider account-------------------------
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "type" => 1,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($booking_details['currency_sign']),
                "country_id" => trim($provider_details['country_id']),
                "cat_id" => 9,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------
            
            /* Ownward Laundry commission invoice------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_details['booking_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
            /* ----------------------------------------------------------------------- */
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $provider_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */

            //Update Courier Payment----------------------------------------------------
              if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
                $courier_update_data = array(
                  "payment_method" => 'mtn',
                  "payment_mode" => 'payment',
                  "paid_amount" => $booking_details['pickup_courier_price'],
                  "transaction_id" => trim($transaction_id),
                  "today" => trim($today),
                  "cod_payment_type" => 'NULL',
                  "order_id" => $booking_details['pickup_order_id'],
                );
                $this->courier_payment_confirm($courier_update_data);
              }
            //Update Courier Payment----------------------------------------------------
          /*************************************** Payment Section ****************************************/

          $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      } else {  $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-laundry/accepted-laundry-bookings','refresh');
    }
    public function provider_confirm_payment_orange()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id', TRUE); $booking_id = (int) $booking_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $order_price = $this->input->post('order_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);
      $booking_details = $this->api->laundry_booking_details($booking_id);

      $order_payment_id = 'gonagoo_'.time().'_'.$booking_id;

      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      //echo json_encode($token_details); die();
      $token = 'Bearer '.$token_details['access_token'];

      $lang = $this->session->userdata('language');
      if($lang=='french') $lang = 'fr';
      else $lang = 'en';

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "merchant_key" => "a8f8c61e", 
                            "currency" => "OUV", 
                            "order_id" => $order_payment_id, 
                            "amount" => $booking_details['total_price'],  
                            "return_url" => base_url('user-panel-laundry/payment-by-provider-orange-process'), 
                            "cancel_url" => base_url('user-panel-laundry/payment-by-provider-orange-process'), 
                            "notif_url" => base_url(), 
                            "lang" => $lang, 
                            "reference" => $this->lang->line('Gonagoo - Service Payment') 
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: ".$token,
        "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      //echo json_encode($url_details); die();
 
      if(isset($url_details) && $url_details['message'] == 'OK' && $url_details['status'] == 201) {
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('booking_id', $booking_id);
        $this->session->set_userdata('order_payment_id', $order_payment_id);
        $this->session->set_userdata('amount', $booking_details['total_price']);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-laundry/accepted-laundry-bookings');
      }
    }
    public function payment_by_provider_orange_process()
    {
      //echo json_encode($_SESSION); die();
      $session_token = $_SESSION['notif_token'];
      $booking_id = $_SESSION['booking_id'];
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $order_payment_id = $_SESSION['order_payment_id'];
      $amount = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "order_id" => $order_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      //echo json_encode($payment_res); die();
      $payment_details = json_decode($payment_res, TRUE);
      $transaction_id = $payment_details['txnid'];

      // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
      if(1) {
        $data = array(
          'paid_amount' => $booking_details['total_price'],
          'payment_method' => trim($payment_method), 
          'transaction_id' => trim($transaction_id), 
          'payment_mode' => 'online', 
          'payment_by' => 'web', 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1, 
          'drop_payment_complete' => ($booking_details['is_drop'] == 1)?1:0, 
        );
        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          if($booking_details['walkin_cust_id'] > 0) {
            $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
            $country_id = $user_details['country_id'];
            $mobile = $user_details['phone'];
            $email_id = $user_details['email'];
            $state_id = 0;
            $city_id = 0;
          } else {
            $user_details = $this->api->get_user_details($booking_details['cust_id']);
            $country_id = $user_details['country_id'];
            $mobile = $user_details['mobile1'];
            $email_id = $user_details['email1'];
            $state_id = $user_details['state_id'];
            $city_id = $user_details['city_id'];
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
          }
          
          //SMS to customer
          $country_code = $this->api->get_country_code_by_id($country_id);
          $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id;
          $this->api->sendSMS((int)$country_code.trim($mobile), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($mobile), $message);
          
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($email_id));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

          $provider_details = $this->api->get_user_details($booking_details['provider_id']);
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email1']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['provider_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          /*************************************** Payment Section ****************************************/
            if($booking_details['walkin_cust_id'] == 0) {
              //Add Payment to customer account---------------------------------------
                if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
                } else {
                  $insert_data_provider_master = array(
                    "user_id" => $booking_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($booking_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                  $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                }

                $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$booking_id,
                  "user_id" => (int)$booking_details['cust_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($booking_details['total_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($booking_details['currency_sign']),
                  "cat_id" => 9
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------

              //deduct Payment from customer account----------------------------------
                $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$booking_id,
                  "user_id" => (int)$booking_details['cust_id'],
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'payment',
                  "amount" => trim($booking_details['total_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($booking_details['currency_sign']),
                  "cat_id" => 9
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //----------------------------------------------------------------------
            }

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            /* Stripe payment invoice------------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

              $user_country = $this->api->get_country_details(trim($country_id));
              $user_state = $this->api->get_state_details(trim($state_id));
              $user_city = $this->api->get_city_details(trim($city_id));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($email_id)));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($booking_details['total_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Booking Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_id.'_laundry_booking.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_id);
              //Update to workroom
              $workroom_update = array(
                'order_id' => $booking_id,
                'cust_id' => $booking_details['cust_id'],
                'deliverer_id' => $booking_details['provider_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  "laundry-invoices/".$pdf_name,
                'cre_datetime' => $today,
                'sender_id' => $booking_details['provider_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $customer_name,
                'deliverer_name' => $provider_name,
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 9
              );
              $this->api->update_to_workroom($workroom_update);
            /* ----------------------------------------------------------------------- */
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
              $emailAddress = trim($email_id);
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */
            
            /* --------------------------Generate Ticket pdf----------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* --------------------------Generate Ticket pdf----------------------- */ 
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $booking_details['cust_email'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */

            //Deduct Gonagoo Commission from provider account-------------------------
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "type" => 1,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($booking_details['currency_sign']),
                "country_id" => trim($provider_details['country_id']),
                "cat_id" => 9,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------
            
            /* Ownward Laundry commission invoice------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_details['booking_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
            /* ----------------------------------------------------------------------- */
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $provider_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */

            //Update Courier Payment----------------------------------------------------
              if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
                $courier_update_data = array(
                  "payment_method" => 'orange',
                  "payment_mode" => 'payment',
                  "paid_amount" => $booking_details['pickup_courier_price'],
                  "transaction_id" => trim($transaction_id),
                  "today" => trim($today),
                  "cod_payment_type" => 'NULL',
                  "order_id" => $booking_details['pickup_order_id'],
                );
                $this->courier_payment_confirm($courier_update_data);
              }
            //Update Courier Payment----------------------------------------------------
          /*************************************** Payment Section ****************************************/

          $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      redirect('user-panel-laundry/accepted-laundry-bookings', 'refresh');
    }
  //Start Laundry Payment By Provider----------------------

  //Start Laundry Providers Orders-------------------------
    public function accepted_laundry_bookings()
    {
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $bookings = $this->api->laundry_booking_list($cust_id, 'accepted', 'provider');
      for ($i=0; $i < sizeof($bookings); $i++) { 
        $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
        $bookings[$i] += ['bookings_details' => $bookings_details];
        if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
          $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
          $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
        }
        if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
          $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
          $bookings[$i] += ['drop_order_details' => $drop_order_details];
        }
      }
      //echo json_encode($bookings); die();
      $this->load->view('user_panel/laundry_provider_accepted_bookings_list_view', compact('bookings','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function mark_is_drop_view()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $page_name = $this->input->post("page_name");
      $address = $this->api->get_consumers_addressbook($this->cust_id);
      $this->load->view('user_panel/mark_is_drop_view',compact('booking_id','address','page_name')); 
      $this->load->view('user_panel/footer');
    }
    public function mark_is_drop_process()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $page_name = $this->input->post("page_name");
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $addr_id = $this->input->post("addr_id"); $addr_id = (int)$addr_id;
      if(isset($addr_id) && $addr_id > 0) {
        $update_data = array(
          'is_drop' => 1,
          'drop_address_id' => (int)($addr_id), 
        );
        if($this->api->laundry_booking_update($update_data, $booking_id)) {
          $this->session->set_flashdata('success', $this->lang->line('Booking status updated successfully.'));
        } else {
          $this->session->set_flashdata('error', $this->lang->line('update_failed'));
        }
      } else {
        $firstname = $this->input->post("firstname");
        $lastname = $this->input->post("lastname");
        $mobile_no = $this->input->post("mobile");
        $zipcode = $this->input->post("zipcode");
        $street = $this->input->post("street");
        $country = $this->input->post("country");
        $state = $this->input->post("state");
        $city = $this->input->post("city");
        $deliver_instruction = $this->input->post("deliver_instruction");
        $pickup_instruction = $this->input->post("pickup_instruction");
        $address1 = $this->input->post("address1");
        $address2 = $this->input->post("toMapID");
        $latitude = $this->input->post("latitude");
        $longitude = $this->input->post("longitude");
        $address_type = $this->input->post("address_type");
        $company = $this->input->post("company");
        $email_id = $this->input->post("email_id");
        $today = date('Y-m-d H:i:s');

        $country_id = $this->user->getCountryIdByName($country);
        $state_id = $this->user->getStateIdByName($state,$country_id);

        if($state_id == 0) {
          //add state and get state id
          $state_id = $this->api->register_state($country_id, $state);
          //add city under state id
          $city_id = $this->api->register_city($state_id, $city);
        } else {
          $city_id = $this->user->getCityIdByName($city,$state_id);
          if($city_id == 0) {
            //add city and get city id
            $city_id = $this->api->register_city($state_id, $city);
          }
        }

        if( !empty($_FILES["address_image"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/address-images/';                 
          $config['allowed_types']  = 'gif|jpg|png';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('address_image')) {   
            $uploads    = $this->upload->data();  
            $image_url =  $config['upload_path'].$uploads["file_name"];  
          } else {  $image_url = "NULL"; }
        } else {  $image_url = "NULL"; }

        $add_data = array(
          'cust_id' => (int) $cust['cust_id'],
          'firstname' => trim($firstname), 
          'lastname' => trim($lastname), 
          'mobile_no' => trim($mobile_no), 
          'street_name' => trim($street), 
          'addr_line1' => trim($address1), 
          'addr_line2' => (trim($address2) != "") ? trim($address2) : "NULL", 
          'city_id' => (int)($city_id), 
          'state_id' => (int)($state_id), 
          'country_id' => (int)($country_id), 
          'zipcode' => $zipcode,
          'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
          'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
          'image_url' => $image_url,
          'latitude' => trim($latitude), 
          'longitude' => trim($longitude), 
          'addr_type' => trim($address_type), 
          'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
          'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
          'cre_datetime' => $today,
        );
        //echo json_encode($add_data); die();
        if( $id = $this->api->register_new_address($add_data)) { 
          //Update drop_address_id and is_drop in laundry
          $update_data = array(
            'is_drop' => 1,
            'drop_address_id' => (int)($id), 
          );
          $this->api->laundry_booking_update($update_data, $booking_id);
          $this->session->set_flashdata('success', $this->lang->line('Booking status updated successfully.'));
        } else {
          $this->session->set_flashdata('error', $this->lang->line('update_failed'));
        }
      }

      if($page_name == 'in_progress_laundry_bookings') {
        redirect('user-panel-laundry/in-progress-laundry-bookings');
      } else if($page_name == 'completed_laundry_bookings') {
        redirect('user-panel-laundry/completed-laundry-bookings');
      } else {
        redirect('user-panel-laundry/accepted-laundry-bookings');
      }
    }
    public function laundry_booking_mark_in_progress()
    {
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $today = date('Y-m-d H:i:s');
        
      $data = array(
        'booking_status' => 'in_progress',
        'status_updated_by' => trim($cust_id), 
        'status_updated_by_user_type' => 'provider', 
        'status_update_datetime' => trim($today),
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        $booking_details = $this->api->laundry_booking_details($booking_id);
        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        $provider_compony_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
        //$message = $this->lang->line('Your laundry booking is in-progress.');
        
        if($booking_details['walkin_cust_id'] > 0) {
          $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['phone'];
          $email_id = $user_details['email'];
          $message = $this->lang->line('Hello')." ".$user_details['firstname'].", ".$this->lang->line('Your laundry order')." ".$booking_id." ".$this->lang->line('is in progress').". ".$this->lang->line('Your team')." ".$provider_compony_details['company_name'].". ".$this->lang->line('Powered by Gonagoo');
            
        } else {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['mobile1'];
          $email_id = $user_details['email1'];
          $message = $this->lang->line('Hello')." ".$user_details['firstname'].", ".$this->lang->line('Your laundry order')." ".$booking_id." ".$this->lang->line('is in progress').". ".$this->lang->line('Your team')." ".$provider_compony_details['company_name'].". ".$this->lang->line('Powered by Gonagoo');
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking status'), 'type' => 'booking-in-progress', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking status'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        }

        $country_code = $this->api->get_country_code_by_id($country_id);
        $this->api->sendSMS((int)$country_code.trim($mobile), $message);

        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($email_id));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));

        
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email_id']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);
        $this->session->set_flashdata('success', $this->lang->line('Booking status updated successfully.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-laundry/accepted-laundry-bookings');
    }
    public function in_progress_laundry_bookings()
    {
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $bookings = $this->api->laundry_booking_list($cust_id, 'in_progress', 'provider');
      for ($i=0; $i < sizeof($bookings); $i++) { 
        $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
        $bookings[$i] += ['bookings_details' => $bookings_details];
        if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
          $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
          $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
        }
        if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
          $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
          $bookings[$i] += ['drop_order_details' => $drop_order_details];
        }
      }
      //echo json_encode($bookings); die();
      $this->load->view('user_panel/laundry_provider_inprogress_bookings_list_view', compact('bookings','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function create_drop_booking()
    {
      //echo json_encode($_POST); die();
      $page_name = $this->input->post("page_name");
      $pickuptime = $this->input->post('pickuptime');
      $pickupdate = $this->input->post('pickupdate');
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $provider = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
      $from_add = $this->api->get_address_from_book($booking_details['drop_address_id']);

      //Create drop booking by getting courier price
      $avg = $this->api->get_cloth_details_volum_by_booking_id($booking_id);  
      $volume = $avg['width'] * $avg['height'] * $avg['length'];
      $dimension_id = 0 ;
      if($dimensions = $this->api->get_dimension_id_for_laundry($volume)){
        $dimension_id = $dimensions['dimension_id']; 
      }
      
      $total_weight =  round(($avg['weight']/1000),2);
      $transport_vehichle_type = 0;
      if($total_weight > 15){
        $transport_vehichle_type = 28;
      }else{
        $transport_vehichle_type = 27;
      }

      $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$provider['latitude'],$provider['longitude']);
      //echo json_encode($distance_in_km); die();
      
      $data = array(
        "category_id" => 7,
        "total_weight" =>$total_weight, 
        "unit_id" => 1,
        "from_country_id" => $from_add['country_id'], 
        "from_state_id" => $from_add['state_id'], 
        "from_city_id" => $from_add['city_id'], 
        "to_country_id" => $provider['country_id'], 
        "to_state_id" => $provider['state_id'], 
        "to_city_id" => $provider['city_id'], 
        "service_area_type" =>'local',
        "transport_type" => 'earth',
        "width" => $avg['width'],
        "height" => $avg['height'],
        "length" => $avg['length'],
        "total_quantity" => 1,
      );
      if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
        $p_date = explode('/',$pickupdate);
        $n_date =  $p_date[1]."/".$p_date[0]."/".$p_date[2];
        $my_date = $n_date.' '.$pickuptime.":00";
        $my_date = str_replace('/', '-', $my_date);
        $m_date = new DateTime($my_date);
        $ex_date = $m_date;
        $ex_date->modify("+24 hours");
        $ex_datetime = $ex_date->format("m-d-Y H:i:s");
        $expiry_date = str_replace('-', '/', $ex_datetime);
        $m_date->modify("+".$max_duration['max_duration']." hours");
        $delivery_datetime = $m_date->format("m-d-Y H:i:s");
        $delivery_datetime = str_replace('-', '/', $delivery_datetime);  
      }

      $data =array(  
        "cust_id" => $provider['cust_id'],
        "category_id" => 7,
        "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
        "total_quantity" => 1,
        "delivery_datetime" => $delivery_datetime,
        "width" => $avg['width'],
        "height" => $avg['height'],
        "length" => $avg['length'],
        "total_weight" => $total_weight,
        "unit_id" =>1,
        "from_country_id" => $from_add['country_id'],
        "from_state_id" => $from_add['state_id'],
        "from_city_id" => $from_add['city_id'],
        "from_latitude" => $from_add['latitude'],
        "from_longitude" => $from_add['longitude'],
        "to_country_id" => $provider['country_id'],
        "to_state_id" => $provider['state_id'], 
        "to_city_id" => $provider['city_id'],
        "to_latitude" => $provider['latitude'],
        "to_longitude" => $provider['longitude'],
        "service_area_type" => "local",
        "transport_type" => "earth",
        "handling_by" => 0,
        "dedicated_vehicle" => 0,
        "package_value" => 0,
        "custom_clearance_by" =>"NULL",
        "old_new_goods" =>"NULL",
        "custom_package_value" =>0,
        "loading_time" =>0,
        "transport_vehichle_type" => $transport_vehichle_type, 
        "laundry" => 1, 
      );
      //echo json_encode($data); die();
      $order_price = $this->api_courier->calculate_order_price($data);
      //echo json_encode($order_price); die();
      if ($booking_details['description'] == '') { $description = $this->lang->line('Laundry Parcel'); } else { $description = $booking_details['description']; } 
      if ($booking_details['drop_payment_complete'] == 0) {
        $cod_payment_type = 'at_deliver';
        $pickup_payment = 0;
        $deliver_payment = $order_price['total_price'];
      } else {
        $cod_payment_type = 'at_pickup';
        $pickup_payment = $order_price['total_price'];
        $deliver_payment = 0;
      }
      
      
      //echo json_encode($description); die();
      $pick_date = explode('/',$pickupdate);
      $pick_date =  $pick_date[2]."-".$pick_date[0]."-".$pick_date[1];
      
      $delivery_datetime = explode('/',$delivery_datetime);
      $tm = explode(' ',$delivery_datetime[2]);
      $delivery_datetime = $tm[0]."-".$delivery_datetime[0]."-".$delivery_datetime[1].' '.$tm[1];

      $expiry_date = explode('/',$expiry_date);
      $tm = explode(' ',$expiry_date[2]);
      $expiry_date = $tm[0]."-".$expiry_date[0]."-".$expiry_date[1].' '.$tm[1];

      if($booking_details['payment_mode'] == 'bank') {
        $pending_service_charges = 0;
        $cod_payment_type = 'at_pickup';
        $pickup_payment = $order_price['total_price'];
        $deliver_payment = 0;
      } else {
        $pending_service_charges = trim($booking_details['booking_price'])+trim($booking_details['drop_courier_price']);
      }
      
      $my_string ="cust_id=".$provider['cust_id']."&from_address=".$from_add['street_name']."&from_address_name=".$from_add['firstname']." ".$from_add['lastname']."&from_address_contact=".$from_add['mobile_no']."&from_latitude=".$from_add['latitude']."&from_longitude=".$from_add['longitude']."&from_country_id=".$from_add['country_id']."&from_state_id=".$from_add['state_id']."&from_city_id=".$from_add['city_id']."&from_addr_type=0&from_relay_id=0&to_address=".$provider['address']."&to_address_name=".$provider['firstname']." ".$provider['lastname']."&to_address_contact=".$provider['contact_no']."&to_country_id=".$provider['country_id']."&to_state_id=".$provider['state_id']."&to_city_id=".$provider['city_id']."&to_latitude=".$provider['latitude']."&to_longitude=".$provider['longitude']."&from_address_info=".$from_add['street_name']."&to_address_info=".$provider['address']."&to_addr_type=0&to_relay_id=0&pickup_datetime=".$pick_date." ".$pickuptime.":00&delivery_datetime=".$delivery_datetime."&expiry_date=".$expiry_date."&order_type=Shipping_Only&service_area_type=local&order_contents=[0]&order_description=".trim($description)."&deliver_instructions=".$from_add['deliver_instructions']."&pickup_instructions=".$from_add['pickup_instructions']."&transport_type=earth&vehical_type_id=".$transport_vehichle_type."&dedicated_vehicle=0&with_driver=0&insurance=0&package_value=0&handling_by=0&total_quantity=[1]&width=[".$avg['width']."]&height=[".$avg['height']."]&length=[".$avg['length']."]&total_weight=[".(string)$total_weight."]&unit_id=[1]&advance_payment=0&payment_method=NULL&pickup_payment=".$pickup_payment."&deliver_payment=".$deliver_payment."&payment_mode=cod&visible_by=0&from_address_email=".$from_add['email']."&to_address_email=".$provider['email_id']."&ref_no=".$booking_id."&dimension_id=[".$dimension_id."]&category_id=7&custom_clearance_by=NULL&old_new_goods=NULL&custom_package_value=0&need_tailgate=0&loading_time=0&dangerous_goods=[0]&is_laundry=1&cod_payment_type=".$cod_payment_type."&laundry_is_drop=1&pending_service_charges=".$pending_service_charges."&service_type=Laundry&laundry=1";
      //echo json_encode($my_string); die();
      $ch = curl_init(base_url()."api/book-courier-order"); // url to send sms
      curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
      curl_setopt($ch, CURLOPT_POST, 1); // method to call url
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
      curl_setopt($ch, CURLOPT_POSTFIELDS,$my_string); 
      $courier_booking = curl_exec($ch); // execute url and save response
      curl_close($ch); // close url connection
      //echo json_encode($courier_booking); die();

      //Update courier Id in Laundry
      $order_details = $this->api->get_courier_details_by_ref_id($provider['cust_id'], $booking_id);
      $order_id_update = array(
        'drop_order_id' => (int)trim($order_details['order_id'])
      );
      $this->api->laundry_booking_update($order_id_update, $booking_id);

      $this->session->set_flashdata('success', $this->lang->line('Drop booking created successfully.'));
      
      if($page_name == 'in_progress_laundry_bookings') {
        redirect('user-panel-laundry/in-progress-laundry-bookings');
      } else if($page_name == 'completed_laundry_bookings') {
        redirect('user-panel-laundry/completed-laundry-bookings');
      } else {
        redirect('user-panel-laundry/accepted-laundry-bookings');
      }
    }
    public function laundry_booking_mark_completed()
    {
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $today = date('Y-m-d H:i:s');

      $laundry_claim_delay = $this->api->get_laundry_provider_profile($cust_id)['laundry_claim_delay'];

      $data = array(
        'booking_status' => 'completed',
        'status_updated_by' => trim($cust_id), 
        'status_updated_by_user_type' => 'provider', 
        'status_update_datetime' => trim($today),
        'claim_delay_hours' => (int)trim($laundry_claim_delay),
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        $booking_details = $this->api->laundry_booking_details($booking_id);
        $user_details = $this->api->get_user_details($booking_details['cust_id']);

        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($email_id));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }

        $provider_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email_id']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        $message = $this->lang->line('Dear Customer').', '.$this->lang->line('Your laundry order').' #'.$booking_id.' '.$this->lang->line('is processed and is ready to be delivered or picked up in the shop.').$this->lang->line('Your team').'. '.$provider_details['company_name'];
        
        if($booking_details['walkin_cust_id'] > 0) {
          $user_details = $this->api->get_walkin_customer_profile($booking_details['walkin_cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['phone'];
          $email_id = $user_details['email'];
        } else {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_id = $user_details['country_id'];
          $mobile = $user_details['mobile1'];
          $email_id = $user_details['email1'];
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking status'), 'type' => 'booking-in-progress', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking status'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        }

        $country_code = $this->api->get_country_code_by_id($country_id);
        $this->api->sendSMS((int)$country_code.trim($mobile), $message);
        
        //Email Customer name, customer Email, Subject, Message
        $this->api_sms->send_email_text($customer_name, trim($email_id), $this->lang->line('Laundry booking status'), trim($message));
        
        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);
        $this->session->set_flashdata('success', $this->lang->line('Booking status updated successfully.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-laundry/in-progress-laundry-bookings');
    }
    public function completed_laundry_bookings()
    {
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $bookings = $this->api->laundry_booking_list($cust_id, 'completed', 'provider');
      for ($i=0; $i < sizeof($bookings); $i++) { 
        $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
        $bookings[$i] += ['bookings_details' => $bookings_details];
        if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
          $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
          $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
        }
        if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
          $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
          $bookings[$i] += ['drop_order_details' => $drop_order_details];
        }
      }
      //echo json_encode($bookings); die();
      $this->load->view('user_panel/laundry_provider_completed_bookings_list_view', compact('bookings','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function cancelled_laundry_bookings()
    {
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $bookings = $this->api->laundry_booking_list($cust_id, 'cancelled', 'provider');
      for ($i=0; $i < sizeof($bookings); $i++) { 
        $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
        $bookings[$i] += ['bookings_details' => $bookings_details];
        if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
          $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
          $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
        }
        if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
          $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
          $bookings[$i] += ['drop_order_details' => $drop_order_details];
        }
      }
      //echo json_encode($bookings); die();
      $this->load->view('user_panel/laundry_provider_cancelled_bookings_list_view', compact('bookings','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function provider_laundry_pending_invoices()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $filter = $this->input->post("filter");
      if($filter == 1) {
        $start_date = $date_start = $this->input->post("date_start");
        $date_start = explode('/', $date_start)[2].'-'.explode('/', $date_start)[0].'-'.explode('/', $date_start)[1].' 00:00:00';
        $end_date = $date_end = $this->input->post("date_end");
        $date_end = explode('/', $date_end)[2].'-'.explode('/', $date_end)[0].'-'.explode('/', $date_end)[1].' 23:59:59';
        $country_id = $this->input->post("country_id");
        $currency_sign = $this->input->post("currency_sign");
        $filter = 1;
      } else {
        $date_start = null;
        $date_end = null;
        $country_id = null;
        $currency_sign = null;
        $filter = 0;
      }

      $bookings = $this->api->pending_invoice_list($cust_id, 'completed', 'provider', 1, 0, 0, $date_start, $date_end, $country_id, $currency_sign); 
      //echo $this->db->last_query();
      //echo json_encode($bookings); die();

      $currency_list = $this->api->get_all_currencies();
      $countries = $this->api->get_countries();
      $this->load->view('user_panel/provider_laundry_pending_invoices_list_view', compact('bookings','cust_id','currency_list','countries','start_date', 'end_date', 'country_id', 'currency_sign', 'filter'));
      $this->load->view('user_panel/footer');
    }
  //End Laundry Providers Orders---------------------------

  //Start Create Booking Laundry Customer------------------
    public function open_booking_laundry()
    {
      $this->load->view('user_panel/new');
      $this->load->view('user_panel/footer');
    }
    public function booking_laundry_ui()
    {
      //echo json_encode($_SESSION); die();
      if(!isset($_SESSION['cat_id']) || $_SESSION['cat_id'] == 9) {
        $this->api->delete_cloth_coun_temp($this->cust_id);
      }
      if(isset($_SESSION['session_id'])) {
        $this->api->update_user_id_by_session_id($_SESSION['session_id'], $this->cust_id);
      }
      if(!isset($_SESSION['selected_provider_id']) || $_SESSION['selected_provider_id'] == '') $selected_provider_id = 0; 
      else $selected_provider_id = (int)$_SESSION['selected_provider_id']; 

      $this->session->unset_userdata('session_id');
      $this->session->unset_userdata('selected_provider_id');

      $address = $this->api->get_consumers_addressbook($this->cust_id);
      $user_details = $this->api->get_user_details($this->cust_id);
      $cust_id = $this->cust_id;

      $this->load->view('user_panel/laundry_booking',compact('address','user_details','cust_id','selected_provider_id'));     
      $this->load->view('user_panel/footer');
    }
    public function add_temp_cloth_count()
    {
      //echo json_encode($_POST);
      $sub = $this->api->get_laundry_sub_category($_POST['sub_cat_id']);
      $insert = array(
        'cust_id' => $_POST['cust_id'],
        'cat_id' => $_POST['cat_id'],
        'sub_cat_id' => $_POST['sub_cat_id'],
        'weight' => $sub['weight'],
        'height' => $sub['height'],
        'width' => $sub['width'],
        'length' => $sub['length'],
        'count' => 1,
        );
      if($res = $this->api->check_cloth_coun_temp($insert)) {
        //echo json_encode($res); die();
        $insert2 = array(
        'cust_id' => $_POST['cust_id'],
        'cat_id' => $_POST['cat_id'],
        'sub_cat_id' => $_POST['sub_cat_id'],
        'weight' => $res['weight']+$sub['weight'],
        'height' => $res['height']+$sub['height'],
        'count' => $res['count']+1,
        );
        $this->api->update_cloth_coun_temp($insert2);
      } else { $this->api->add_cloth_coun_temp($insert); }
    }
    public function subs_temp_cloth_count()
    {
      //echo json_encode($_POST);
      $sub = $this->api->get_laundry_sub_category($_POST['sub_cat_id']);
      $insert = array(
        'cust_id' => $_POST['cust_id'],
        'cat_id' => $_POST['cat_id'],
        'sub_cat_id' => $_POST['sub_cat_id'],
        'count' => 1,
      );
      if($res = $this->api->check_cloth_coun_temp($insert)){
        $insert2 = array(
          'cust_id' => $_POST['cust_id'],
          'cat_id' => $_POST['cat_id'],
          'sub_cat_id' => $_POST['sub_cat_id'],
          'weight' => $res['weight']-$sub['weight'],
          'height' => $res['height']-$sub['height'],
          'count' => $res['count']-1,
        );
        if($res['count']-1 == 0) { $this->api->update_cloth_coun_temp($insert2,1);
        } else { $this->api->update_cloth_coun_temp($insert2); }
      }
    } 
    public function get_cloth_details()
    {
      $b = 'not_found';
      $a= '';
      $details = $this->api->get_cloth_details($this->cust_id);
      if($details){
        $total = 0;
        foreach($details as $item) {
          $cat = $this->api->get_laundry_category($item['cat_id']);
          $sub_cat = $this->api->get_laundry_sub_category($item['sub_cat_id']);
          $total = $total+$item["count"];
          $a .= '<div class="row">
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <lable>'.ucwords($cat["cat_name"]).'-'.ucwords($sub_cat["sub_cat_name"]).'</lable>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">  
                    <lable>'.$item["count"].'</lable>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">  
                    <lable>-</lable>
                  </div><br/>
                </div>';
        }
        $a .= '<hr style="margin-top:10px;margin-bottom:10px;"><div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line('total').'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <lable style="color:#3498db">-</lable>
                </div>
              </div><hr style="margin-top:10px;margin-bottom:10px;">';
          echo $a;
      } else { echo $b; }
    }
    public function get_laundry_provider_append()
    {
      $customer_lat_long = $_POST['customer_lat_long'];
      if($customer_lat_long) {
        $lat_long_array = explode(',', $customer_lat_long);
        $cust_lat = $lat_long_array[0];
        $cust_long = $lat_long_array[1];        
      } else {
        $cust_lat = '48.8582535';
        $cust_long = '2.2924873';
      }

      $details = $this->api->get_cloth_details($this->cust_id);
      $customer = $this->api->get_consumer_datails($this->cust_id);
      $provider = $this->api->get_laundry_provider_profile_list();

      //echo json_encode($provider); die();
      $provider_id = 0;
      $a = '<table id="provider_table" class="table">
              <thead><tr class="hidden"><th class="hidden">ID</th><th></th></tr></thead>
              <tbody>';
      foreach ($provider as $pro) { static $cnt=0; $cnt++;
        $total = 0;
        $flag = 0;
        $cr_sign = "";
        foreach ($details as $det) {
          if($rate = $this->api->get_laundry_charges_list_apend($pro['cust_id'], $det['sub_cat_id'] , $customer['country_id'])){
            $total = $total + ($rate['charge_amount'] * $det['count']);
            $cr_sign= $rate['currency_sign'];
          } else { $flag++; }
        }

        $sp_charge = 0;
        $next_day_charge = 0;        
        $today =Date('Y-m-d');
        $day = trim(strtolower(date('D', strtotime($today))));
        if($special_charges = $this->api->get_laundry_special_charges_list($pro['cust_id'])){
          foreach ($special_charges as $charge) {
            if(strtotime($charge['start_date']) <= strtotime($today) &&  strtotime($charge['end_date']) > strtotime($today)){
              $percentage = $charge[''.$day.''];
              if($percentage > 0){
                $sp_charge++;
                $special_total = ($total/100)*$percentage;
                $special_total = round($total-$special_total,2);
              }
            }

            $nextdate = date('Y-m-d', strtotime($today . " +1 days"));
            $next_day = trim(strtolower(date('D', strtotime($nextdate))));
            if(strtotime($charge['start_date']) <= strtotime($next_day) &&  strtotime($charge['end_date']) > strtotime($next_day)){
              $next_percentage = $charge[''.$next_day.''];
              if($next_percentage > 0){
                $next_day_charge++;
                $next_special_total = ($total/100)*$next_percentage;
                $next_special_total = round($total-$next_special_total,2);  
              }
            }
          }
        }
        if($flag == 0){
          $provider_id = $pro["cust_id"]; 
          $provider_details = $this->api->get_laundry_provider_profile((int)$provider_id);

          $driving_distance = $this->api->GetDrivingDistance($provider_details['latitude'],$provider_details['longitude'],$cust_lat,$cust_long);

          $rating = (int)$this->api->get_laundry_review_details((int)$provider_id)['ratings'];
          $a .= '<tr>
            <td class="hidden" style="line-height: 15px;">'.$pro["deliverer_id"].'</td>
            <td style="line-height: 15px; padding: 0px">
              <div class="hpanel hoverme" style="line-height: 15px; background-color:white; border-radius: 10px; box-shadow: all; margin-bottom:10px;">
                <a>
                  <div class="panel-body" style="line-height: 15px; padding-bottom:0px; padding-top:5px; border-top: 0px !important; border-radius: 10px;">
                    <div class="row" style="line-height: 15px;">
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;"> 
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-center" style="padding-left: 0px; padding-right: 0px;">
                          <img src="'.base_url($pro['avatar_url']).'" class="img-responsive thumbnail" style="border: 1px solid #3498db; margin-bottom:3px; max-height: 50px;">';
                          for($i=0; $i< $rating; $i++) { $a .= '<font color="#FFD700"><i class="fa fa-star fa-1x"></i></font>'; }
                          for($i=0; $i<(5 - $rating); $i++) { $a .= '<font color="#FFD700"><i class="fa fa-star-o fa-1x"></i></font>'; }      
                    $a .= '
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                          <h5 style="margin-top: 0px;"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp;&nbsp;<font color="#3498db">'.$pro["company_name"].'</font><font color="gray"> [ '.$pro["firstname"].' '.$pro["lastname"].' ]</font></h5>
                          <h6><i class="fa fa-address-book-o" aria-hidden="true"></i>&nbsp;&nbsp;<font color="#3498db">'.$pro["street_name"].', '.$pro["address"].'</font></h6>
                          <h6><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;<font color="#3498db">'.$pro["contact_no"].'</font></h6>
                          <h6><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;<font color="#3498db">'.$pro["email_id"].'</font></h6>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-right" style="padding-left: 0px;">';
                      if($sp_charge != 0){
                        $a .='<h5 style="margin-top: 0px;"><s style="color: gray"><strong>&nbsp;'.$total." ".$cr_sign.'&nbsp;</s></strong></h5>
                            <h4 style="margin-top: 0px;"><font color="red"><strong>'.$special_total." ".$cr_sign.'</font></strong></h4>';
                      }else{

                          $a .='<h4 style="margin-top: 0px;"><strong><font color="red">'.$total." ".$cr_sign.'</font></strong></h4>';
                      }
                  $a .='</div>
                        <input type="hidden" name="lat_long_'.$pro["cust_id"].'" value="'.$pro["latitude"].','.$pro["longitude"].'" id="lat_long_'.$pro["cust_id"].'" />';
                      
                  if($next_day_charge != 0){
                      $a .= '<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-right:0px">
                                <label><img src="'.base_url('resources/distance.png').'" class="img-responsive" style="vertical-align:bottom; display: inline" /> '.round($driving_distance,2).' KM</label>
                              </div>
                              <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5" style="padding-right:0px;">
                                <h6 style="margin-top: 13px; color:#3498db"><i class="fa fa-gift" aria-hidden="true"></i><strong>&nbsp;'.$this->lang->line('tomorrow price').'<font color="red">&nbsp;'.$next_special_total." ".$cr_sign.'&nbsp;</font></strong></h6>
                              </div>
                              <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 text-right">
                                <div class="funkyradio">
                                  <div class="funkyradio-success">
                                    <input type="radio" name="selected_provider" id="radio'.$pro["cust_id"].'" class="selected_provider hidden" value="'.$pro["cust_id"].'" style="margin-top:0px;" />
                                    <label for="radio'.$pro["cust_id"].'" style="margin-top:0px; padding-right: 1.25em; background-color: #1e5394; color: white; border-radius: 18px;">'.$this->lang->line("Select This Provider").'</label>
                                  </div>
                                </div>
                              </div>
                            </div>';
                  }else{
                      $a .='<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-5">
                                <label><img src="'.base_url('resources/distance.png').'" class="img-responsive" style="vertical-align:bottom; display: inline" /> '.round($driving_distance,2).' KM</label>
                              </div>
                              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-7 text-right">
                                <div class="funkyradio">
                                  <div class="funkyradio-success">
                                    <input type="radio" name="selected_provider" id="radio'.$pro["cust_id"].'" class="selected_provider hidden" value="'.$pro["cust_id"].'" style="margin-top:0px;" />
                                    <label for="radio'.$pro["cust_id"].'" style="margin-top:0px; padding-right: 1.25em; background-color: #1e5394; color: white; border-radius: 18px;">'.$this->lang->line("Select This Provider").'</label>
                                  </div>
                                </div>
                              </div>
                            </div>';
                  }
                      
               $a .= '</div>
                    </div>
                  </div>
                </a>
              </div>
            </td>
          </tr>';
        }
      }
      $a .= '</tbody></table>';
      $a .= '<script>
              $("input[type=radio][name=selected_provider]").change(function() {
                //alert($(this).val());
                var cust_id = $(this).val();
                //$("#provider_id").val();
                var from_address = $("#from_address").val();
                var pickupdate = $("#pickupdate").val();
                var pickuptime = $("#pickuptime").val();

                if($("#to_address_check").is(":checked")) {
                  var to_address_check = 1;
                } else { var to_address_check = 0; }

                if($("#from_address_check").is(":checked")) {
                  var from_address_check = 1;
                } else { var from_address_check = 0; }

                $.ajax({
                  type: "POST",
                  url: "'.base_url('user-panel-laundry/update-my-cart-price').'", 
                  data: {pro_id:cust_id,from_address:from_address,pickupdate:pickupdate,pickuptime:pickuptime,to_address_check:to_address_check,from_address_check:from_address_check },
                  success: function(res){
                    $(".selected_item").empty();
                    $(".selected_provider_information").empty();
                    var ex = res.split("@zaidu@");
                    console.log(ex[1]);
                    $(".selected_item").append(ex[0]);
                    $(".selected_provider_information").append(ex[1]);
                    $("#tab3next").click();
                  }
                });

                //console.log(cust_id);
                var lat_long_pro = "#lat_long_"+cust_id;
                var pro_lat_long = $(lat_long_pro).val();
                var cust_lat_long = $(".customer_lat_long").val();
                var url = "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin="+cust_lat_long+"&destination="+pro_lat_long;
                $("#map_frame").attr("src", url);

              });
            </script>
            <script type="text/javascript">
              $("#provider_table").dataTable( {
                "pageLength": 4,
                "bLengthChange": false,
                "ordering": false,
              } );
              $(document).ready(function() {
                var dataTable = $("#provider_table").dataTable();
                $("#ordersearchbox").keyup(function(){
                  dataTable.fnFilter(this.value);
                });    
              });
            </script>';
      echo $a;
    }
    public function update_my_cart_price()
    {
      $pro_id = $this->input->post('pro_id');
      $to_address_check = $this->input->post('to_address_check');
      $from_address_check = $this->input->post('from_address_check');
      $details = $this->api->get_cloth_details($this->cust_id);
      $customer = $this->api->get_consumer_datails($this->cust_id);
      $provider = $this->api->get_laundry_provider_profile($pro_id);
      $avg = $this->api->get_cloth_details_volum($this->cust_id);
      $total_weight =  round(($avg['weight']/1000),2);
      //echo json_encode($total_weight); die();

      $transport_vehichle_type = 0;
      if($total_weight > 15){ $transport_vehichle_type = 28;
      } else { $transport_vehichle_type = 27; }
      //echo json_encode($transport_vehichle_type); die();

      if($this->input->post('from_address')!=NULL){
        $pickuptime = $this->input->post('pickuptime');
        $pickupdate = $this->input->post('pickupdate');
        $from_address = $this->input->post('from_address');
        
        $from_add = $this->api->get_address_from_book($from_address);

        $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$provider['latitude'],$provider['longitude']);
        //echo $distance_in_km; die();
        
        $data = array(
          "category_id" => 7,
          "total_weight" =>$total_weight, 
          "unit_id" => 1,
          "from_country_id" => $from_add['country_id'], 
          "from_state_id" => $from_add['state_id'], 
          "from_city_id" => $from_add['city_id'], 
          "to_country_id" => $provider['country_id'], 
          "to_state_id" => $provider['state_id'], 
          "to_city_id" => $provider['city_id'], 
          "service_area_type" =>'local',
          "transport_type" => 'earth',
          "width" => $avg['width'],
          "height" => $avg['height'],
          "length" => $avg['length'],
          "total_quantity" => 1,
        );
        if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
          $p_date = explode('/',$pickupdate);
          $n_date =  $p_date[1]."/".$p_date[0]."/".$p_date[2];
          $my_date = $n_date.' '.$pickuptime.":00";
          $my_date = str_replace('/', '-', $my_date);
          $m_date = new DateTime($my_date);
          $m_date->modify("+".$max_duration['max_duration']." hours");
          $delivery_datetime = $m_date->format("m-d-Y H:i:s");
          $delivery_datetime = str_replace('-', '/', $delivery_datetime); 

          $data =array(  
            "cust_id" => $provider['cust_id'],
            "category_id" => 7,
            "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
            "delivery_datetime" => $delivery_datetime,
            "total_quantity" => 1,
            "width" => $avg['width'],
            "height" => $avg['height'],
            "length" => $avg['length'],
            "total_weight" => $total_weight,
            "unit_id" =>1,
            "from_country_id" => $from_add['country_id'],
            "from_state_id" => $from_add['state_id'],
            "from_city_id" => $from_add['city_id'],
            "from_latitude" => $from_add['latitude'],
            "from_longitude" => $from_add['longitude'],
            "to_country_id" => $provider['country_id'],
            "to_state_id" => $provider['state_id'], 
            "to_city_id" => $provider['city_id'],
            "to_latitude" => $provider['latitude'],
            "to_longitude" => $provider['longitude'],
            "service_area_type" => "local",
            "transport_type" => "earth",
            "handling_by" => 0,
            "dedicated_vehicle" => 0,
            "package_value" => 0,
            "custom_clearance_by" =>"NULL",
            "old_new_goods" =>"NULL",
            "custom_package_value" =>0,
            "loading_time" =>0,
            "transport_vehichle_type" => $transport_vehichle_type, 
            "laundry" =>1,
          );
          $order_price = $this->api_courier->calculate_order_price($data);
          //echo json_encode($order_price); die();
        }    
      }

      if($details){
        $total_quant = 0;
        $a = '';
        $total = 0;
        foreach($details as $item){
          $cat = $this->api->get_laundry_category($item['cat_id']);
          $sub_cat = $this->api->get_laundry_sub_category($item['sub_cat_id']);
          $total_quant = $total_quant+$item["count"];

          $rate = $this->api->get_laundry_charges_list_apend($provider['cust_id'], $item['sub_cat_id'] , $customer['country_id']);
          $total = $total + ($rate['charge_amount']*$item['count']);

          $a .= '<div class="row">
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <lable>'.ucwords($cat["cat_name"]).'-'.ucwords($sub_cat["sub_cat_name"]).'</lable>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">  
                    <lable>'.$item["count"].'</lable>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">  
                    <lable>'.$rate["charge_amount"]." ".$rate["currency_sign"].'</lable>
                  </div>
                </div>';   
        }

        $sp_charge = 0;
        $today =Date('Y-m-d');
        $day = trim(strtolower(date('D', strtotime(date('Y-m-d')))));
        if($special_charges = $this->api->get_laundry_special_charges_list($provider['cust_id'])){
          foreach ($special_charges as $charge) {
            if(strtotime($charge['start_date']) <= strtotime($today) &&  strtotime($charge['end_date']) > strtotime($today)){
              $percentage = $charge[''.$day.''];
              if($percentage > 0){
                $sp_charge++;
                $old_total = $total;
                $total = round(($total/100)*$percentage,2);
                $total = round($old_total-$total,2);
              }
            }
          }
        }

        $a .= '<script>
                  $("#booking_price").val("'.$total.'");
                  $("#item_count").val("'.$total_quant.'");
                  $("#currency_sign").val("'.$rate["currency_sign"].'");
                </script>';

        if($sp_charge != 0){

          $a .= '<hr style="margin-top:10px;margin-bottom:10px;" /><div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("Laundry").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <s style="color: gray"><lable>'.$old_total." ".$rate["currency_sign"].'</lable></s>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />
               <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("Special Laundry rate").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <lable style="color:red">'.$total." ".$rate["currency_sign"].'</lable>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />';

        }else{

            $a .= '<hr style="margin-top:10px;margin-bottom:10px;" /><div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                  <lable style="color:#3498db">'.$this->lang->line("Laundry").'</lable>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                  <lable style="color:#3498db">'.$total_quant.'</lable>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <lable style="color:#3498db">'.$total." ".$rate["currency_sign"].'</lable>
                </div>
               </div><hr style="margin-top:10px;margin-bottom:10px;" />';
        }

        if($from_address_check){
          $a .=      '<script>
                        $("#pickup_courier_price").val("'.round($order_price["total_price"] ,2).'");
                        $("#is_pickup").val(1);               
                      </script>
                      <div class="row">
                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                          <lable style="color:#3498db">'.$this->lang->line("pickup_").'</lable>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                          <lable style="color:#3498db">-</lable>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                          <lable style="color:#3498db">'.round($order_price["total_price"] ,2)." ".$order_price["currency_sign"].'</lable>
                        </div>
                      </div><hr style="margin-top:10px;margin-bottom:10px;" />';
        }
        if($to_address_check) {
          $a .=      ' <script>
                        $("#pickup_courier_price").val("'.round($order_price["total_price"] ,2).'"); 
                        $("#is_drop").val(1);  
                      </script>
                      <div class="row">
                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                          <lable style="color:#3498db">'.$this->lang->line("Drop").'</lable>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                          <lable style="color:#3498db">-</lable>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                          <lable style="color:#3498db">'.round($order_price["total_price"] ,2)." ".$order_price["currency_sign"].'</lable>
                        </div>
                      </div><hr style="margin-top:10px;margin-bottom:10px;" />';
        }
        if($from_address_check || $to_address_check){
          if($from_address_check && $to_address_check){
            $gr_total = $total + (round($order_price["total_price"],2)*2);
          } else {
            if($from_address_check || $to_address_check) {
              $gr_total = $total +  (round($order_price["total_price"],2));
            } else {
              $gr_total = $total +  (round($order_price["total_price"],2));
            }
          }

            

            $a .=  '<script>
                      $("#total_price").val("'.$gr_total.'");
                      $("#balance_amount").val("'.$gr_total.'");
                    </script>
                    <div class="row">
                      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                        <lable style="color:#3498db"><strong>'.$this->lang->line("Grand Total").'</strong></lable>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                        <lable style="color:#3498db">-</lable>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                        <lable style="color:#3498db"><strong>'.$gr_total." ".$order_price["currency_sign"].'</strong></lable>
                      </div>
                    </div><hr style="margin-top:10px;margin-bottom:10px;" />@zaidu@';

            $a   .= '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                        <lable style="color:#3498db">'.$this->lang->line("company").':</lable>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                      '.$provider['company_name'].'
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                      <lable style="color:#3498db">'.$this->lang->line("provider").':</lable>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                      '.$provider['firstname']." ".$provider['lastname'].'
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                      <lable style="color:#3498db">'.$this->lang->line("contact").':</lable>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                      '.$provider['contact_no'].'
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                      <lable style="color:#3498db">'.$this->lang->line("email").':</lable>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                      '.$provider['email_id'].'
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                      <lable style="color:#3498db">'.$this->lang->line("address").':</lable>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                      '.$provider['street_name'].', '.$provider['address'].'
                    </div>';
            echo $a;
        } else {
            $a   .=  '<script>
                        $("#total_price").val("'.$total.'");
                        $("#balance_amount").val("'.$total.'");
                      </script>
                    <div class="row">
                      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                        <lable style="color:#3498db"><strong>'.$this->lang->line("Grand Total").'</strong></lable>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                        <lable style="color:#3498db">-</lable>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                        <lable style="color:#3498db"><strong>'.$total." ".$rate["currency_sign"].'</strong></lable>
                      </div>
                    </div><hr style="margin-top:10px;margin-bottom:10px;" />@zaidu@';


            $a   .= '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                        <lable style="color:#3498db">'.$this->lang->line("company").':</lable>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                        '.$provider['company_name'].'
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                        <lable style="color:#3498db">'.$this->lang->line("provider").':</lable>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                        '.$provider['firstname']." ".$provider['lastname'].'
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                        <lable style="color:#3498db">'.$this->lang->line("contact").':</lable>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                        '.$provider['contact_no'].'
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                        <lable style="color:#3498db">'.$this->lang->line("email").':</lable>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                        '.$provider['email_id'].'
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-bottom:7px;">
                        <lable style="color:#3498db">'.$this->lang->line("address").':</lable>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-bottom:7px;">
                        '.$provider['street_name'].', '.$provider['address'].'
                      </div>';
            echo $a;
        }
      }
    }
    public function laundry_booking()
    {
      //echo json_encode($_POST); die();
      $today = date('Y-m-d H:i:s');
      $pickuptime = $this->input->post('pickuptime');
      $pickupdate = $this->input->post('pickupdate');
      $payment_mode = $this->input->post('payment_mode');
      $cod_payment_type = $this->input->post('cod_payment_type');
      $expected_return_date = $this->input->post('expected_return_date');
      $description = $this->input->post('description');
      $provider = $this->api->get_laundry_provider_profile($_POST['selected_provider']);
      $user = $this->api->get_user_details($this->cust_id);
      $country_currency = $this->api->get_country_currency_detail($user['country_id']);
      $laundry_advance_payment = $this->api->get_laundry_advance_payment($user['country_id']);
      $gonagoo_commission_amount = ($_POST['booking_price']/100)*$laundry_advance_payment['gonagoo_commission'];
      $booking_details = $this->api->get_cloth_details($this->cust_id);
      $drop_address_id = $this->input->post('from_address');
      //echo json_encode($gonagoo_commission_amount); die();
      $data = array(
        'cust_id' => $user['cust_id'], 
        'provider_id' => $provider['cust_id'], 
        'cre_datetime' => date('Y-m-d H:i:s'), 
        'booking_price' => $_POST['booking_price'], 
        'total_price' => trim($_POST['total_price']),
        'currency_id' => $country_currency['currency_id'], 
        'currency_sign' => $_POST['currency_sign'], 
        'pickup_payment' => ($_POST['pickup_payment']==1)?trim($_POST['total_price']):0, 
        'deliver_payment' => ($_POST['deliverer_payment']==1)?trim($_POST['total_price']):0, 
        'country_id' => $user['country_id'], 
        'paid_amount' => 0, 
        'pickup_courier_price' => ($_POST['is_pickup']>0)?$_POST["pickup_courier_price"]:0, 
        'drop_courier_price' => ($_POST['is_drop']>0)?$_POST["pickup_courier_price"]:0, 
        //'payment_mode' => ($_POST['payment_mode']=='cod' || $_POST['payment_mode']=='bank' )?trim($_POST['payment_mode']):'NULL', 
        'payment_mode' => "NULL", 
        'payment_by' => "NULL", 
        'payment_datetime' => 'NULL', 
        'balance_amount' => trim($_POST['total_price']), 
        'complete_paid' => 0, 
        'booking_status' => "accepted", 
        'status_updated_by' => $user['cust_id'], 
        'status_updated_by_user_type' => "customer", 
        'status_update_datetime' => $today, 
        'item_count' => $_POST['item_count'], 
        'cust_name' => $user['firstname']." ".$user['lastname'], 
        'cust_contact' => $user['mobile1'], 
        'cust_email' => $user['email1'], 
        'operator_name' => $provider['firstname']." ".$provider['lastname'], 
        'operator_company_name' => $provider['company_name'], 
        'operator_contact_name' => $provider['contact_no'], 
        'is_pickup' => $_POST['is_pickup'], 
        'is_drop' => $_POST['is_drop'], 
        'gonagoo_commission_per' => $laundry_advance_payment['gonagoo_commission'], 
        'gonagoo_commission_amount' => $gonagoo_commission_amount, 
        'cat_id' => 9,
        'expected_return_date' => explode('/',$expected_return_date)[2]."-".explode('/',$expected_return_date)[0]."-".explode('/',$expected_return_date)[1], 
        'description' => trim($description), 
        'cod_payment_type' => trim($cod_payment_type),
        'drop_address_id' => trim($drop_address_id), 
      );
      //echo json_encode($data); die();
      if($booking_id = $this->api->create_laundry_booking($data)){
        //Delivery days
        $date1 = new DateTime($today);
        $date2 = new DateTime($expected_return_date);
        $interval = $date1->diff($date2);
        $days = $interval->days;

        //customer Name
        if( trim($user['firstname']) == 'NULL' || trim($user['lastname'] == 'NULL') ) {
          $parts = explode("@", trim($user['email1']));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user['firstname'])." ".trim($user['lastname']); }

        //Provider Name
        if( trim($provider['firstname']) == 'NULL' || trim($provider['lastname'] == 'NULL') ) {
          $parts = explode("@", trim($provider['email_id']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider['firstname'])." ".trim($provider['lastname']); }

        $user_message = $this->lang->line('Hello').' '.$customer_name.', '.$this->lang->line('We have registered your laundry order as of').' '.date('d M Y', strtotime($today)).'. '.$this->lang->line('Order number').': '.$booking_id.'. '.$this->lang->line('Estimated production time').' '.$days.' '.$this->lang->line('days').'. '.$this->lang->line('Your team').'. '.$provider['company_name'];
        $provider_message = $this->lang->line('You have recieved Laundry order. Booking ID - ').$booking_id;

        $laundry_booking_details = $this->api->laundry_booking_details($booking_id);
        //Update Booking items Detail-------------------------------
          foreach ($booking_details as $bk){
            $rate = $this->api->get_laundry_charges_list_apend($provider['cust_id'], $bk['sub_cat_id'], $user['country_id']);
            $total_charge = $rate['charge_amount']*$bk['count'];
            $data2 =  array(
              'booking_id' => $booking_id, 
              'cat_id' => $bk['cat_id'], 
              'sub_cat_id' => $bk['sub_cat_id'] ,
              'quantity' => $bk['count'], 
              'single_unit_price' => $rate['charge_amount'], 
              'total_price' => $total_charge, 
              'currency_sign' => $rate['currency_sign'], 
              'cre_datetime' => date('Y-m-d H:i:s'),
              'height' => $bk['height'], 
              'width' => $bk['width'], 
              'length' => $bk['length'], 
              'weight' => $bk['weight'],  
            );
            $booking_details_id = $this->api->create_laundry_booking_details($data2);
          }
        //Update Booking items Detail-------------------------------

        //*********Send Booking SMS to customer*********************
          $country_code = $this->api->get_country_code_by_id($user["country_id"]);
          $this->api->sendSMS((int)$country_code.trim($user['mobile1']), $user_message);
        //*********Send Booking SMS to customer*********************

        //*********Send Booking SMS to provider*********************
          $country_code = $this->api->get_country_code_by_id($provider["country_id"]);
          $this->api->sendSMS((int)$country_code.trim($provider['contact_no']), $provider_message);
        //*********Send Booking SMS to provider*********************

        //*********Send Email to customer***************************
          $this->api_sms->send_email_text($customer_name, trim($user['email1']),$this->lang->line('laundry booking confirmation'), trim($user_message));
        //*********Send Email to customer***************************

        //*********Send Email to provider***************************
          $this->api_sms->send_email_text($provider_name, trim($provider['email_id']),$this->lang->line('laundry booking confirmation'), trim($provider_message));
        //*********Send Email to provider***************************

        //*********Send push notification to customer***************
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($this->cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('laundry booking confirmation'), 'type' => 'laundry-booking-confirmation', 'notice_date' => $today, 'desc' => $user_message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

            //Push Notification APN
            $msg_apn_customer =  array('title' => $this->lang->line('laundry booking confirmation'), 'text' => $user_message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
        //*********Send push notification to customer***************

        //*********Send push notification to provider***************
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details($provider['cust_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('laundry booking confirmation'), 'type' => 'laundry-booking-confirmation', 'notice_date' => $today, 'desc' => $provider_message);
            $this->api->sendFCM($msg, $arr_provider_fcm_ids, $api_key);

            //Push Notification APN
            $msg_apn_provider =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $provider_message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
        //*********Send push notification to provider***************
      
        //*********Post courier_workroom & global_workroom**********
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $this->cust_id,
            'deliverer_id' => $provider['cust_id'],
            'text_msg' => $user_message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $this->cust_id,
            'type' => 'booking_status',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);

          $global_workroom_update = array(
            'service_id' => $booking_id,
            'cust_id' => $this->cust_id,
            'deliverer_id' => $provider['cust_id'],
            'sender_id' => $this->cust_id,
            'cat_id' => 9
          );
          $this->api->create_global_workroom($global_workroom_update);
        //*********Post courier_workroom & global_workroom**********  
        
        //*********Add provider to laundry fav provider*************
          //check is_favorite
          $check = $this->api->check_is_favourite_providers_id($this->cust_id, $provider['cust_id']);
          if(!$check) {
            $fav_list = array(
              'cust_id' => $this->cust_id,
              'deliverer_id' => $provider['cust_id'],
              'cre_datetime' => $today
            );
            $this->api->add_laundry_favourite_providers($fav_list);
          }
        //*********Add provider to laundry fav provider*************  
        
        //*********courier booking**********************************
          if($_POST['is_pickup']=="1") {
            //echo 'in courier'; die();
            $from_add = $this->api->get_address_from_book($_POST['from_address']);
            $avg = $this->api->get_cloth_details_volum($this->cust_id);
            $volume = $avg['width'] * $avg['height'] * $avg['length'];
            $dimension_id = 0 ;
            if($dimensions = $this->api->get_dimension_id_for_laundry($volume)){
              $dimension_id = $dimensions['dimension_id']; 
            }
            $total_weight =  round(($avg['weight']/1000),2);
            $transport_vehichle_type = 0;
            if($total_weight > 15){
              $transport_vehichle_type = 28;
            }else{
              $transport_vehichle_type = 27;
            }

            //echo $from_add['latitude'].'+'.$from_add['longitude'].'+'.$provider['latitude'].'+'.$provider['longitude']; 
            $distance_in_km = $this->api->GetDrivingDistance($from_add['latitude'],$from_add['longitude'],$provider['latitude'],$provider['longitude']);
            //die();

            $data = array(
              "category_id" => 7,
              "total_weight" =>$total_weight, 
              "unit_id" => 1,
              "from_country_id" => $from_add['country_id'], 
              "from_state_id" => $from_add['state_id'], 
              "from_city_id" => $from_add['city_id'], 
              "to_country_id" => $provider['country_id'], 
              "to_state_id" => $provider['state_id'], 
              "to_city_id" => $provider['city_id'], 
              "service_area_type" =>'local',
              "transport_type" => 'earth',
              "width" => $avg['width'],
              "height" => $avg['height'],
              "length" => $avg['length'],
              "total_quantity" => 1,
            );
            if($max_duration = $this->api_courier->get_max_duration($data,$distance_in_km)){
              $p_date = explode('/',$pickupdate);
              $n_date =  $p_date[1]."/".$p_date[0]."/".$p_date[2];
              $my_date = $n_date.' '.$pickuptime.":00";
              $my_date = str_replace('/', '-', $my_date);
              $m_date = new DateTime($my_date);
              $ex_date = $m_date;
              $ex_date->modify("+24 hours");
              $ex_datetime = $ex_date->format("m-d-Y H:i:s");
              $expiry_date = str_replace('-', '/', $ex_datetime);
              $m_date->modify("+".$max_duration['max_duration']." hours");
              $delivery_datetime = $m_date->format("m-d-Y H:i:s");
              $delivery_datetime = str_replace('-', '/', $delivery_datetime);  
            }

            $data =array(  
              "cust_id" => $provider['cust_id'],
              "category_id" => 7,
              "pickup_datetime" => $pickupdate.' '.$pickuptime.":00",
              "total_quantity" => 1,
              "delivery_datetime" => $delivery_datetime,
              "width" => $avg['width'],
              "height" => $avg['height'],
              "length" => $avg['length'],
              "total_weight" => $total_weight,
              "unit_id" =>1,
              "from_country_id" => $from_add['country_id'],
              "from_state_id" => $from_add['state_id'],
              "from_city_id" => $from_add['city_id'],
              "from_latitude" => $from_add['latitude'],
              "from_longitude" => $from_add['longitude'],
              "to_country_id" => $provider['country_id'],
              "to_state_id" => $provider['state_id'], 
              "to_city_id" => $provider['city_id'],
              "to_latitude" => $provider['latitude'],
              "to_longitude" => $provider['longitude'],
              "service_area_type" => "local",
              "transport_type" => "earth",
              "handling_by" => 0,
              "dedicated_vehicle" => 0,
              "package_value" => 0,
              "custom_clearance_by" =>"NULL",
              "old_new_goods" =>"NULL",
              "custom_package_value" =>0,
              "loading_time" =>0,
              "transport_vehichle_type" => $transport_vehichle_type, 
              "laundry" => 1, 
            );
            $order_price = $this->api_courier->calculate_order_price($data);
            //echo json_encode($order_price); die();
            if ($description == '') { $description = $this->lang->line('Laundry Parcel'); }
            if(isset($cod_payment_type)) {
              if(($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')){
                $cod_payment_type = 'at_pickup';
                $pickup_payment = $order_price['total_price'];
                $deliver_payment = 0;
              } else {
                $cod_payment_type = 'at_deliver';
                $pickup_payment = 0;
                $deliver_payment = $order_price['total_price'];
              }
            } else { 
              $cod_payment_type = 'NULL';
              $pickup_payment = 0;
              $deliver_payment = $order_price['total_price']; 
            }
            
            $pick_date = explode('/',$pickupdate);
            $pick_date =  $pick_date[2]."-".$pick_date[0]."-".$pick_date[1];
            
            $delivery_datetime = explode('/',$delivery_datetime);
            $tm = explode(' ',$delivery_datetime[2]);
            $delivery_datetime = $tm[0]."-".$delivery_datetime[0]."-".$delivery_datetime[1].' '.$tm[1];

            $expiry_date = explode('/',$expiry_date);
            $tm = explode(' ',$expiry_date[2]);
            $expiry_date = $tm[0]."-".$expiry_date[0]."-".$expiry_date[1].' '.$tm[1];
            
            if($payment_mode == 'bank') { $pending_service_charges = 0; $cod_payment_type = 'at_deliver';
            } else { $pending_service_charges = trim($laundry_booking_details['booking_price'])+trim($laundry_booking_details['drop_courier_price']); }

            $my_string ="cust_id=".$provider['cust_id']."&from_address=".$from_add['street_name']."&from_address_name=".$from_add['firstname']." ".$from_add['lastname']."&from_address_contact=".$from_add['mobile_no']."&from_latitude=".$from_add['latitude']."&from_longitude=".$from_add['longitude']."&from_country_id=".$from_add['country_id']."&from_state_id=".$from_add['state_id']."&from_city_id=".$from_add['city_id']."&from_addr_type=0&from_relay_id=0&to_address=".$provider['address']."&to_address_name=".$provider['firstname']." ".$provider['lastname']."&to_address_contact=".$provider['contact_no']."&to_country_id=".$provider['country_id']."&to_state_id=".$provider['state_id']."&to_city_id=".$provider['city_id']."&to_latitude=".$provider['latitude']."&to_longitude=".$provider['longitude']."&from_address_info=".$from_add['street_name']."&to_address_info=".$provider['address']."&to_addr_type=0&to_relay_id=0&pickup_datetime=".$pick_date." ".$pickuptime.":00&delivery_datetime=".$delivery_datetime."&expiry_date=".$expiry_date."&order_type=Shipping_Only&service_area_type=local&order_contents=[0]&order_description=".trim($description)."&deliver_instructions=".$from_add['deliver_instructions']."&pickup_instructions=".$from_add['pickup_instructions']."&transport_type=earth&vehical_type_id=".$transport_vehichle_type."&dedicated_vehicle=0&with_driver=0&insurance=0&package_value=0&handling_by=0&total_quantity=[1]&width=[".$avg['width']."]&height=[".$avg['height']."]&length=[".$avg['length']."]&total_weight=[".(string)$total_weight."]&unit_id=[1]&advance_payment=0&payment_method=NULL&pickup_payment=".$pickup_payment."&deliver_payment=".$deliver_payment."&payment_mode=cod&visible_by=0&from_address_email=".$from_add['email']."&to_address_email=".$provider['email_id']."&ref_no=".$booking_id."&dimension_id=[".$dimension_id."]&category_id=7&custom_clearance_by=NULL&old_new_goods=NULL&custom_package_value=0&need_tailgate=0&loading_time=0&dangerous_goods=[0]&is_laundry=1&cod_payment_type=".$cod_payment_type."&pending_service_charges=".trim($pending_service_charges)."&service_type=Laundry&laundry=1";
            //echo json_encode($my_string); die();
            $ch = curl_init(base_url()."api/book-courier-order"); // url to send sms
            curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
            curl_setopt($ch, CURLOPT_POST, 1); // method to call url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
            curl_setopt($ch, CURLOPT_POSTFIELDS,$my_string); 
            $courier_booking = curl_exec($ch); // execute url and save response
            curl_close($ch); // close url connection
            //echo json_encode($courier_booking); die();

            //Update courier Id in Laundry
            $order_details = $this->api->get_courier_details_by_ref_id($provider['cust_id'], $booking_id);
            $order_id_update = array(
              'pickup_order_id' => (int)trim($order_details['order_id'])
            );
            $this->api->laundry_booking_update($order_id_update, $booking_id);
          }
        //*********courier booking**********************************

        //Flush customer cart after booking complete
        $this->api->delete_cloth_coun_temp($this->cust_id);
      }

      if($payment_mode == 'cod' || $payment_mode == 'bank') {
        redirect(base_url('user-panel-laundry/customer-accepted-laundry-bookings'),'refresh');
      } else {
        redirect(base_url('user-panel-laundry/laundry-booking-payment-by-cutomer/'.$booking_id),'refresh');
      }
    }
    public function get_laundry_sub_category_append()
    {
      $cat_id = $this->input->post('cat_id');
      $sub_cat = $this->api->get_laundry_sub_category_list($cat_id);
      $counter = 1;
      foreach ($sub_cat as $cat){
        $charg = array(
          'cat_id' => $cat['cat_id'],
          'sub_cat_id' => $cat['sub_cat_id'],
          'country_id' => $this->cust['country_id'], 
        );
        $charges = $this->api->get_laundry_cherge($charg);
        $sub_cat_details = $this->api->get_sub_category_details((int)$cat['sub_cat_id']);
        $sub_cat_img = ($sub_cat_details['icon_url'] != 'NULL')?base_url($sub_cat_details['icon_url']):base_url('resources/no-image.jpg');

        $user_cart = $this->api->get_cloth_details_by_sub_cat_id_user_id($this->cust_id, (int)$cat['sub_cat_id']);

        if($user_cart) {
          echo '<a>'.
            '<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4" style="margin-bottom:-15px;">'.
              '<div class="hpanel plan-box" id="type_cat'.$cat['cat_id'].$counter.'">'.
                '<div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px; padding-left: 0px; padding-right: 0px;">'.
                  '<img src="'.$sub_cat_img.'" style="max-height: 50px; width: auto;">'.
                  '<h5 class="text-success">'.$cat['sub_cat_name'].'</h5>'.
                  '<button id="min'.$cat['cat_id'].$counter.'" class="btn btn-warning btn-sm">-</button>&nbsp;&nbsp;<lable id="lable'.$cat['cat_id'].$counter.'" count="'.$user_cart['count'].'" cat_id="'.$cat['cat_id'].'" sub_cat_id="'.$cat['sub_cat_id'].'" cust_id="'.$this->cust_id.'">'.$user_cart['count'].'</lable>&nbsp;&nbsp;<button class="btn btn-success btn-sm" id="plus'.$cat['cat_id'].$counter.'">+</button>
                  </div>
                </div>
              </div>
            </a>'; 
        } else {
          echo '<a>'.
                '<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4" style="margin-bottom:-15px;">'.
                  '<div class="hpanel plan-box" id="type_cat'.$cat['cat_id'].$counter.'">'.
                    '<div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px; padding-left: 0px; padding-right: 0px;">'.
                      '<img src="'.$sub_cat_img.'" style="width: 50px; height: auto;">'.
                      '<h5 class="text-success">'.$cat['sub_cat_name'].'</h5>'.
                      '<button id="min'.$cat['cat_id'].$counter.'" class="btn btn-warning btn-sm">-</button>&nbsp;&nbsp;<lable id="lable'.$cat['cat_id'].$counter.'" count="0" cat_id="'.$cat['cat_id'].'" sub_cat_id="'.$cat['sub_cat_id'].'" cust_id="'.$this->cust_id.'">0</lable>&nbsp;&nbsp;<button class="btn btn-success btn-sm" id="plus'.$cat['cat_id'].$counter.'">+</button>
                      </div>
                    </div>
                  </div>
                </a>'; 
        }

        echo '<script>
                $("#plus'.$cat['cat_id'].$counter.'").on("click", function(e) {
                  e.preventDefault();
                  var lable = $("#lable'.$cat['cat_id'].$counter.'");
                  var price_lable = $("#price_lable'.$cat['cat_id'].$counter.'");
                  var price = price_lable.attr("count");
                  var currency_sign = price_lable.attr("currency_sign");
                  var count = lable.attr("count");
                  var cust_id = lable.attr("cust_id");
                  var cat_id = lable.attr("cat_id");
                  var sub_cat_id = lable.attr("sub_cat_id");
                  var neww = parseInt(count)+1;
                  lable.attr("count", neww );
                  $("#lable'.$cat['cat_id'].$counter.'").text(neww);
                  $("#type_cat'.$cat['cat_id'].$counter.'").addClass("active");


                  if(cat_id == 1) {
                    var men_cnt = parseInt($("#type_category_man_count").text());
                    if(men_cnt >= 0) {
                      var men_total_cnt = men_cnt + 1;
                      $("#type_category_man_count").text(""+men_total_cnt+"");
                    } else {
                      $("#type_category_man_count").text(""+men_cnt+"");
                    }
                  }
                  if(cat_id == 2) {
                    var women_cnt = parseInt($("#type_category_woman_count").text());
                    var women_total_cnt = women_cnt + 1;
                    $("#type_category_woman_count").text(""+women_total_cnt+"");
                  }
                  if(cat_id == 3) {
                    var child_cnt = parseInt($("#type_category_child_count").text());
                    var child_total_cnt = child_cnt + 1;
                    $("#type_category_child_count").text(""+child_total_cnt+"");
                  }
                  if(cat_id == 4) {
                    var other_cnt = parseInt($("#type_category_other_count").text());
                    var other_total_cnt = other_cnt + 1;
                    $("#type_category_other_count").text(""+other_total_cnt+"");
                  }

                  $.ajax({
                    type: "POST",
                    url: "'.base_url('user-panel-laundry/add-temp-cloth-count').'", 
                    data: {cat_id:cat_id,sub_cat_id:sub_cat_id,cust_id:cust_id},
                    success: function(res){
                      console.log(res);
                    }
                  });

                });

                $("#min'.$cat['cat_id'].$counter.'").on("click", function(e) {
                  e.preventDefault();
                  var lable = $("#lable'.$cat['cat_id'].$counter.'");
                  var count = lable.attr("count");
                  var cust_id = lable.attr("cust_id");
                  var cat_id = lable.attr("cat_id");
                  var sub_cat_id = lable.attr("sub_cat_id");
                  if(parseInt(count)==0)
                  { 
                    var neww = 0;
                  }else{
                    var neww = parseInt(count)-1; 
                  }

                  if(cat_id == 1) {
                    var men_cnt = parseInt($("#type_category_man_count").text());
                    if(men_cnt >= 1 && count > 0) {
                      var men_total_cnt = men_cnt - 1;
                      $("#type_category_man_count").text(""+men_total_cnt+"");
                    } else {
                      $("#type_category_man_count").text(""+men_cnt+"");
                    }
                  }
                  if(cat_id == 2) {
                    var women_cnt = parseInt($("#type_category_woman_count").text());
                    if(women_cnt >= 1 && count > 0) {
                      var women_total_cnt = women_cnt - 1;
                      $("#type_category_woman_count").text(""+women_total_cnt+"");
                    } else {
                      $("#type_category_woman_count").text(""+women_cnt+"");
                    }
                  }
                  if(cat_id == 3) {
                    var child_cnt = parseInt($("#type_category_child_count").text());
                    if(child_cnt >= 1 && count > 0) {
                      var child_total_cnt = child_cnt - 1;
                      $("#type_category_child_count").text(""+child_total_cnt+"");
                    } else {
                      $("#type_category_child_count").text(""+child_cnt+"");
                    }
                  }
                  if(cat_id == 4) {
                    var other_cnt = parseInt($("#type_category_other_count").text());
                    if(other_cnt >= 1 && count > 0) {
                      var other_total_cnt = other_cnt - 1;
                      $("#type_category_other_count").text(""+other_total_cnt+"");
                    } else {
                      $("#type_category_other_count").text(""+other_cnt+"");
                    }
                  }

                  if(neww==0)
                  {$("#type_cat'.$cat['cat_id'].$counter.'").removeClass("active");}
                  lable.attr("count", neww );
                  $("#lable'.$cat['cat_id'].$counter.'").text(neww);
  
                  $.ajax({
                    type: "POST",
                    url: "'.base_url('user-panel-laundry/subs-temp-cloth-count').'", 
                    data: {cat_id:cat_id,sub_cat_id:sub_cat_id,cust_id:cust_id},
                    success: function(res){
                      console.log(res);
                    }
                  });
                });
              </script>';
        $counter++;
      }
    }
  //End Create Booking Laundry Customer--------------------

  //Start Laundry Payment By Customer----------------------
    public function laundry_booking_payment_by_cutomer()
    {
      if( !is_null($this->input->post('booking_id')) ) { $booking_id = (int) $this->input->post('booking_id'); }
      else if(!is_null($this->uri->segment(3))) { $booking_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-laundry/customer-accepted-laundry-bookings'); }

      //echo json_encode($booking_id); die();
      $details = $this->api->laundry_booking_details($booking_id);
      $cust_id = $details['cust_id']; $cust_id = (int)$cust_id;
      $payment_mode = $this->user->get_customer_payment_methods(trim($details['cust_id']));
      $user_details = $this->api->get_user_details(trim($details['cust_id']));
      $this->load->view('user_panel/laundry_customer_payment_view', compact('details', 'cust_id', 'payment_mode', 'user_details'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_mark_cod_by_customer()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $cod_payment_type = $this->input->post("cod_payment_type");
      $details = $this->api->laundry_booking_details($booking_id);
      //echo json_encode($details); die();

      $data = array(
        'payment_mode' => 'cod', 
        'cod_payment_type' => ($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')?'at_pickup':'at_deliver', 
        'pickup_payment' => ($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')?$details['total_price']:0, 
        'deliver_payment' => ($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_deliver')?$details['total_price']:0, 
      );
      //echo json_encode($data); die();
      
      //Update COD type in courier
      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        if($details['is_pickup'] == 1) {
          $data = array(
            'payment_mode' => 'cod', 
            'cod_payment_type' => ($_POST['cod_payment_type']!="0" && $_POST['cod_payment_type']=='at_pickup')?'at_pickup':'at_deliver', 
          );
          $this->api->update_courier_order_details($data, $details['pickup_order_id']);          
        }
        $this->session->set_flashdata('success', $this->lang->line('Booking status updated successfully.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-laundry/customer-accepted-laundry-bookings');
    }
    public function confirm_payment_stripe()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $booking_id = $this->input->post('booking_id', TRUE); $booking_id = (int) $booking_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $today = date('Y-m-d H:i:s');
      $booking_details = $this->api->laundry_booking_details($booking_id);
      //echo json_encode($booking_details); die();

      $data = array(
        'paid_amount' => $booking_details['total_price'],
        'payment_method' => trim($payment_method), 
        'transaction_id' => trim($transaction_id), 
        'payment_mode' => 'online', 
        'payment_by' => 'web', 
        'payment_datetime' => trim($today),
        'balance_amount' => '0', 
        'complete_paid' => 1, 
        'drop_payment_complete' => ($booking_details['is_drop'] == 1)?1:0, 
      );

      if( $this->api->laundry_booking_update($data, $booking_id) ) {
        $user_details = $this->api->get_user_details($booking_details['cust_id']);
        $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
        $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id.'. '.$this->lang->line('Stripe Payment ID:').' '.$transaction_id;
        $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
        $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
        //Send Push Notifications
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
        if(!is_null($device_details_customer)) {
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
          $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
          //APN
          $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }
        //Email Customer name, customer Email, Subject, Message
        if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($user_details['email1']));
          $username = $parts[0];
          $customer_name = $username;
        } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
        $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Laundry booking status'), trim($message));

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $parts = explode("@", trim($provider_details['email1']));
          $username = $parts[0];
          $provider_name = $username;
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Update to workroom
        $workroom_update = array(
          'order_id' => $booking_id,
          'cust_id' => $booking_details['cust_id'],
          'deliverer_id' => $booking_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $booking_details['provider_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $provider_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 9
        );
        $this->api->update_to_workroom($workroom_update);

        /*************************************** Payment Section ****************************************/
          //Add Payment to customer account-----------------------------------------
            if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $booking_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
            }

            $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['cust_id'],
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($booking_details['total_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //deduct Payment from customer account------------------------------------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['cust_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($booking_details['total_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //Add Payment to Provider account-----------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $booking_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            }

            $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'advance',
              "amount" => trim($booking_details['total_price']),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          /* Stripe payment invoice---------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($booking_details['total_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Booking Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_id.'_laundry_booking.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_id);
            //Update to workroom
            $workroom_update = array(
              'order_id' => $booking_id,
              'cust_id' => $booking_details['cust_id'],
              'deliverer_id' => $booking_details['provider_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "laundry-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $booking_details['provider_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $customer_name,
              'deliverer_name' => $provider_name,
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 9
            );
            $this->api->update_to_workroom($workroom_update);
          /* -------------------------------------------------------------------- */
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
            $emailAddress = trim($user_details['email1']);
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* --------------------------Generate Ticket pdf----------------------- */ 
            $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
          
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
            QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
            $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
            $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
            if(rename($img_src, $img_dest));
          
            $html ='<page format="100x100" orientation="L" style="font: arial;">';
            $html .='
            <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                <h6>'.$this->lang->line('slider_heading1').'</h6>
              </div>
            </div>';
            $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line('Booking Details').'<br />
                <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                <br />
                <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
              <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
              <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
              <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                  <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
              <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
          
            $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
            $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
            foreach($booking_det as $dtls){
              $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
            }
            $html .= '</tbody></table></div></page>';
            $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
            $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
            rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
            //whatsapp api send ticket
            $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
            $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
            $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
            $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
          /* --------------------------Generate Ticket pdf----------------------- */
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = $booking_details['cust_email'];
            $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
            $fileName = $my_laundry_booking_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

          //Deduct Gonagoo Commission from provider account-------------------------
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($booking_details['currency_sign']),
              "cat_id" => 9
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($booking_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$booking_id,
              "user_id" => (int)$booking_details['provider_id'],
              "type" => 1,
              "transaction_type" => 'laundry_commission',
              "amount" => trim($booking_details['gonagoo_commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($booking_details['currency_sign']),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => 9,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------
          
          /* Ownward Laundry commission invoice---------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($booking_details['booking_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "laundry-invoices/".$pdf_name,
            );
            $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
          /* -------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $provider_details['email1'];
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        /*************************************** Payment Section ****************************************/

        //Update Courier Payment----------------------------------------------------
          if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
            $courier_update_data = array(
              "payment_method" => 'stripe',
              "payment_mode" => 'payment',
              "paid_amount" => $booking_details['pickup_courier_price'],
              "transaction_id" => trim($transaction_id),
              "today" => trim($today),
              "cod_payment_type" => 'NULL',
              "order_id" => $booking_details['pickup_order_id'],
            );
            $this->courier_payment_confirm($courier_update_data);
          }
        //Update Courier Payment----------------------------------------------------

        //------------------------------------------------------------------------

        $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-laundry/customer-accepted-laundry-bookings');
    }
    public function courier_payment_confirm($courier_update_data)
    {
      $order_id = (int)trim($courier_update_data['order_id']);
      $payment_method = trim($courier_update_data['payment_method']);
      $paid_amount = trim($courier_update_data['paid_amount']);
      $transaction_id = trim($courier_update_data['transaction_id']);
      $today = trim($courier_update_data['today']);

      if($order_details = $this->api_courier->get_order_detail($order_id)) {
        $cust_id = (int)$order_details['cust_id'];
        $transfer_account_number = 'NULL';
        $bank_name = 'NULL';
        $account_holder_name = 'NULL';
        $iban = 'NULL';
        $email_address = $order_details['cust_name'];
        $mobile_number = 'NULL';
        $total_amount_paid = $paid_amount;

        $update_data_order = array(
          "payment_method" => $payment_method,
          "payment_mode" => "payment",
          "paid_amount" => $paid_amount,
          "transaction_id" => $transaction_id,
          "payment_datetime" => $today,
          "cod_payment_type" => "NULL",
          "complete_paid" => "1",
          "balance_amount" => 0,
          "pending_service_charges" => 0,
        );

        if($this->api->update_courier_order_details($update_data_order, $order_id)) {
          $order_details = $this->api_courier->get_order_detail($order_id);
          //echo json_encode($order_details['currency_sign']); die();
          /************************ Courier Payment Section *****************************/
            //Add core price to gonagoo master
              if($gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api_courier->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
              }
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
              $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($order_details['standard_price']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            
            //Update urgent fee in gonagoo account master
              if(trim($order_details['urgent_fee']) > 0) {
                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'urgent_fee',
                  "amount" => trim($order_details['urgent_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update insurance fee in gonagoo account master
              if(trim($order_details['insurance_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'insurance_fee',
                  "amount" => trim($order_details['insurance_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update handling fee in gonagoo account master
              if(trim($order_details['handling_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'handling_fee',
                  "amount" => trim($order_details['handling_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update vehicle fee in gonagoo account master
              if(trim($order_details['vehicle_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'vehicle_fee',
                  "amount" => trim($order_details['vehicle_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update custome clearance fee in gonagoo account master
              if(trim($order_details['custom_clearance_fee']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'custom_clearance_fee',
                  "amount" => trim($order_details['custom_clearance_fee']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            //Update custome clearance fee in gonagoo account master
              if(trim($order_details['loading_unloading_charges']) > 0) {
                $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
                $this->api_courier->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api_courier->gonagoo_master_details(trim($order_details['currency_sign']));
                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'loading_unloading_charges',
                  "amount" => trim($order_details['loading_unloading_charges']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api_courier->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              }

            /*//Add payment details in customer account master and history
              if($customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
              } else {
                $insert_data_customer_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api_courier->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($total_amount_paid);
              $this->api_courier->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($total_amount_paid),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api_courier->insert_payment_in_account_history($update_data_account_history);
            */

            //Deduct payment details from customer account master and history
              if($customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
              } else {
                $insert_data_customer_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $this->api_courier->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($total_amount_paid);
              $this->api_courier->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api_courier->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($total_amount_paid),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api_courier->insert_payment_in_account_history($update_data_account_history);
          /************************ Courier Payment Section *****************************/
        }
      }
    }
    public function payment_by_customer_mtn()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post('booking_id', TRUE);
      $phone_no = $this->input->post('phone_no', TRUE);
      $today = date('Y-m-d H:i:s'); 
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $total_price = trim($booking_details['total_price']);
      //echo json_encode($total_price); die();
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$total_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      $contents = curl_exec($ch);
      $mtn_pay_res = json_decode($contents, TRUE);
      //var_dump($mtn_pay_res); die();
      
      $payment_method = 'mtn';
      //$transaction_id = '1234567890';
      $transaction_id = trim($mtn_pay_res['TransactionID']);
      
      $payment_data = array();
      if($mtn_pay_res['StatusCode'] === "01"){
      //if(1){
        $data = array(
          'paid_amount' => $booking_details['total_price'],
          'payment_method' => trim($payment_method), 
          'transaction_id' => trim($transaction_id), 
          'payment_mode' => 'online', 
          'payment_by' => 'web', 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1, 
          'drop_payment_complete' => ($booking_details['is_drop'] == 1)?1:0, 
        );
        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id.'. '.$this->lang->line('Stripe Payment ID:').' '.$transaction_id;
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email1']));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Laundry booking status'), trim($message));

          $provider_details = $this->api->get_user_details($booking_details['provider_id']);
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email1']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['provider_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          /*************************************** Payment Section ****************************************/
            //Add Payment to customer account-----------------------------------------
              if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //deduct Payment from customer account------------------------------------
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            /* Stripe payment invoice---------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

              $user_country = $this->api->get_country_details(trim($user_details['country_id']));
              $user_state = $this->api->get_state_details(trim($user_details['state_id']));
              $user_city = $this->api->get_city_details(trim($user_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($booking_details['total_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Booking Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_id.'_laundry_booking.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_id);
              //Update to workroom
              $workroom_update = array(
                'order_id' => $booking_id,
                'cust_id' => $booking_details['cust_id'],
                'deliverer_id' => $booking_details['provider_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  "laundry-invoices/".$pdf_name,
                'cre_datetime' => $today,
                'sender_id' => $booking_details['provider_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $customer_name,
                'deliverer_name' => $provider_name,
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 9
              );
              $this->api->update_to_workroom($workroom_update);
            /* -------------------------------------------------------------------- */

            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
              $emailAddress = trim($user_details['email1']);
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */
            
            /* --------------------------Generate Ticket pdf----------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* --------------------------Generate Ticket pdf----------------------- */ 
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = trim($user_details['email1']);
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */

            //Deduct Gonagoo Commission from provider account-------------------------
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "type" => 1,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($booking_details['currency_sign']),
                "country_id" => trim($provider_details['country_id']),
                "cat_id" => 9,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------
            
            /* Ownward Laundry commission invoice---------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_details['booking_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
            /* -------------------------------------------------------------------- */
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $provider_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */
          /*************************************** Payment Section ****************************************/

          //Update Courier Payment----------------------------------------------------
            if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
              $courier_update_data = array(
                "payment_method" => 'mtn',
                "payment_mode" => 'payment',
                "paid_amount" => $booking_details['pickup_courier_price'],
                "transaction_id" => trim($transaction_id),
                "today" => trim($today),
                "cod_payment_type" => 'NULL',
                "order_id" => $booking_details['pickup_order_id'],
              );
              $this->courier_payment_confirm($courier_update_data);
            }
          //Update Courier Payment----------------------------------------------------

          $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      } else {  $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-laundry/customer-accepted-laundry-bookings','refresh');
    }
    public function payment_by_customer_orange()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $booking_id = $this->input->post('booking_id', TRUE); $booking_id = (int) $booking_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $order_price = $this->input->post('order_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);
      $booking_details = $this->api->laundry_booking_details($booking_id);

      $order_payment_id = 'gonagoo_'.time().'_'.$booking_id;

      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      //echo json_encode($token_details); die();
      $token = 'Bearer '.$token_details['access_token'];

      $lang = $this->session->userdata('language');
      if($lang=='french') $lang = 'fr';
      else $lang = 'en';

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "merchant_key" => "a8f8c61e", 
                            "currency" => "OUV", 
                            "order_id" => $order_payment_id, 
                            "amount" => $booking_details['total_price'],  
                            "return_url" => base_url('user-panel-laundry/payment-by-customer-orange-process'), 
                            "cancel_url" => base_url('user-panel-laundry/payment-by-customer-orange-process'), 
                            "notif_url" => base_url(), 
                            "lang" => $lang, 
                            "reference" => $this->lang->line('Gonagoo - Service Payment') 
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: ".$token,
        "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      
      //echo json_encode($url_details); die();

      //echo $url_details['notif_token']; die();
      
      if(isset($url_details) && $url_details['message'] == 'OK' && $url_details['status'] == 201) {
        //echo 'true'; die();
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('booking_id', $booking_id);
        $this->session->set_userdata('order_payment_id', $order_payment_id);
        $this->session->set_userdata('amount', $booking_details['total_price']);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        //echo 'false'; die();
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-laundry/customer-accepted-laundry-bookings');
      }
    }
    public function payment_by_customer_orange_process()
    {
      //echo json_encode($_REQUEST); die();
      $cust_id = (int) $this->cust_id;
      $session_token = $_SESSION['notif_token'];
      $booking_id = $_SESSION['booking_id'];
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $order_payment_id = $_SESSION['order_payment_id'];
      $amount = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "booking_id" => $order_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      //$transaction_id = $payment_details['txnid'];
      $transaction_id = '786';
      //echo json_encode($payment_details); die();

      // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
      if(1) {
        $total_amount_paid = $booking_details['total_price'];
        $transfer_account_number = 'NULL';
        $bank_name = 'NULL';
        $account_holder_name = 'NULL';
        $iban = 'NULL';
        $email_address = 'NULL';
        $mobile_number = 'NULL';    

        $data = array(
          'paid_amount' => $booking_details['total_price'],
          'payment_method' => trim($payment_method), 
          'transaction_id' => trim($transaction_id), 
          'payment_mode' => 'online', 
          'payment_by' => 'web', 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1, 
          'drop_payment_complete' => ($booking_details['is_drop'] == 1)?1:0, 
        );
        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          $user_details = $this->api->get_user_details($booking_details['cust_id']);
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Payment completed for laundry booking ID').': '.$booking_id.'. '.$this->lang->line('Stripe Payment ID:').' '.$transaction_id;
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($booking_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email_id']));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Laundry booking status'), trim($message));

          $provider_details = $this->api->get_user_details($booking_details['provider_id']);
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['provider_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          /*************************************** Payment Section ****************************************/
            //Add Payment to customer account-----------------------------------------
              if($customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($customer_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //deduct Payment from customer account------------------------------------
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $booking_details['cust_id'], $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($booking_details['cust_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['cust_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $booking_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($booking_details['total_price']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'advance',
                "amount" => trim($booking_details['total_price']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            /* Orange payment invoice------------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

              $user_country = $this->api->get_country_details(trim($user_details['country_id']));
              $user_state = $this->api->get_state_details(trim($user_details['state_id']));
              $user_city = $this->api->get_city_details(trim($user_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Laundry Booking ID: ').$booking_id. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($booking_details['total_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Booking Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_id.'_laundry_booking.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_id.'_laundry_booking.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_id);
              //Update to workroom
              $workroom_update = array(
                'order_id' => $booking_id,
                'cust_id' => $booking_details['cust_id'],
                'deliverer_id' => $booking_details['provider_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  "laundry-invoices/".$pdf_name,
                'cre_datetime' => $today,
                'sender_id' => $booking_details['provider_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $customer_name,
                'deliverer_name' => $provider_name,
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 9
              );
              $this->api->update_to_workroom($workroom_update);
            /* ----------------------------------------------------------------------- */
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
              $emailAddress = $user_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */
            
            /* --------------------------Generate Ticket pdf----------------------- */ 
              $operator_details = $this->api->get_laundry_provider_profile($booking_details['provider_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($booking_details['booking_id'], $booking_details['booking_id'].".png", "L", 2, 2); 
              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$booking_details['booking_id'].'.png';
              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$booking_details['booking_id'].'.png';
              if(rename($img_src, $img_dest));
            
              $html ='<page format="100x100" orientation="L" style="font: arial;">';
              $html .='
              <div style="margin-left:16px; margin-top:20px; width:340px; height:80px; border:dashed 1px #225595; padding-top: 10px; display:flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details['company_name'].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details['firstname']. ' ' .$operator_details['lastname'].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url('resources/images/dashboard-logo.jpg').'" style="width: 100px; height: auto;" /><br />
                  <h6>'.$this->lang->line('slider_heading1').'</h6>
                </div>
              </div>';
              $html .= '<div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line('Booking Details').'<br />
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line('Booking ID').': #'.$booking_details['booking_id'].'</strong></h5> 
                  <br />
                  <img src="'.base_url('resources/laundry-qrcode/').$booking_details["booking_id"].'.png" style="float: right; width: 84px; height: 84px; margin-top: -28px ; margin-left:4px;" />
                <h6 style="margin-top:-23px;"><strong>'.$this->lang->line('Customer').':</strong>'.$booking_details['cust_name'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Booking Date').':</strong>'.$booking_details['cre_datetime'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Total items').':</strong> '.$booking_details['item_count'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <strong> '.$this->lang->line('Price').':</strong>'.$booking_details['total_price'].' '.$booking_details['currency_sign'].'</h6>
                <h6 style="margin-top:-10px;"><strong>'.$this->lang->line('Items Details').':</strong></h6>';
            
              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Item Name").'</th><th style="width:90px;">'. $this->lang->line("quantity").'</th><th style="width:90px;">'. $this->lang->line("total").'</th></tr></thead><tbody>';
              $booking_det = $this->api->laundry_booking_details_list($booking_details['booking_id']);
              foreach($booking_det as $dtls){
                $html .= '<tr><td>'.$dtls["sub_cat_name"].'</td><td>'.$dtls["quantity"].'</td><td>'.$dtls["total_price"].'</td></tr>';
              }
              $html .= '</tbody></table></div></page>';
              $booking_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/laundry-pdf/'.$booking_details['booking_id'].'_booking.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$booking_details['booking_id'].'_laundry_booking.pdf','F');
              $my_laundry_booking_pdf = $booking_details['booking_id'].'_laundry_booking.pdf';
              rename($my_laundry_booking_pdf, "resources/laundry-pdf/".$my_laundry_booking_pdf);  
              //whatsapp api send ticket
              $laundry_booking_location = base_url().'resources/laundry-pdf/'.$my_laundry_booking_pdf;
              $this->api_sms->send_pdf_whatsapp($country_code.trim($booking_details['cust_contact']) ,$laundry_booking_location ,$my_laundry_booking_pdf);
              $cust_sms = $this->lang->line('Dear')." ".$booking_details['cust_name']." ".$this->lang->line('Your Payment Is Completed Successfully For Laundry Booking ID:')." ".$booking_details['booking_id']; 
              $this->api_sms->send_sms_whatsapp($country_code.trim($booking_details['cust_contact']),$cust_sms);
            /* --------------------------Generate Ticket pdf----------------------- */
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $user_details['email1'];
              $fileToAttach = "resources/laundry-pdf/$my_laundry_booking_pdf";
              $fileName = $my_laundry_booking_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */
        
            //Deduct Gonagoo Commission from provider account-------------------------
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $booking_details['provider_id'], $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($booking_details['provider_id'], trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => 9
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //------------------------------------------------------------------------

            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($booking_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($booking_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)$booking_details['provider_id'],
                "type" => 1,
                "transaction_type" => 'laundry_commission',
                "amount" => trim($booking_details['gonagoo_commission_amount']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($booking_details['currency_sign']),
                "country_id" => trim($provider_details['country_id']),
                "cat_id" => 9,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------
            
            /* Ownward Laundry commission invoice------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($booking_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($booking_details['booking_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Booking ID: ').$booking_details['booking_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($booking_details['gonagoo_commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($booking_details['booking_id'].'_ownward_laundry_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $booking_details['booking_id'].'_ownward_laundry_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/laundry-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "laundry-invoices/".$pdf_name,
              );
              $this->api->laundry_booking_update($update_data, $booking_details['booking_id']);
            /* ----------------------------------------------------------------------- */
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $provider_details['email1'];
              $fileToAttach = "resources/laundry-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */
          /*************************************** Payment Section ****************************************/

          //Update Courier Payment----------------------------------------------------
            if($booking_details['is_pickup'] > 0 && $booking_details['pickup_order_id'] > 0) {
              $courier_update_data = array(
                "payment_method" => 'stripe',
                "payment_mode" => 'payment',
                "paid_amount" => $booking_details['pickup_courier_price'],
                "transaction_id" => trim($transaction_id),
                "today" => trim($today),
                "cod_payment_type" => 'NULL',
                "order_id" => $booking_details['pickup_order_id'],
              );
              $this->courier_payment_confirm($courier_update_data);
            }
          //Update Courier Payment----------------------------------------------------

          $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }

      redirect('user-panel-laundry/customer-accepted-laundry-bookings');
      /*$this->load->view('user_panel/confirm_payment_view',compact('response'));
      $this->load->view('user_panel/footer');*/
    }
  //End Laundry Payment By Customer------------------------

  //Start Laundry Customer Bookings------------------------
    public function customer_accepted_laundry_bookings()
    {
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $bookings = $this->api->laundry_booking_list($cust_id, 'accepted', 'customer');
      for ($i=0; $i < sizeof($bookings); $i++) { 
        $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
        $bookings[$i] += ['bookings_details' => $bookings_details];
        if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
          $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
          $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
        }
        if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
          $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
          $bookings[$i] += ['drop_order_details' => $drop_order_details];
        }
      }
      //echo json_encode($bookings); die();
      $this->load->view('user_panel/laundry_customer_accepted_bookings_list_view', compact('bookings','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function customer_mark_is_drop_view()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $page_name = $this->input->post("page_name");
      $address = $this->api->get_consumers_addressbook($this->cust_id);
      $this->load->view('user_panel/customer_mark_is_drop_view',compact('booking_id','address','page_name')); 
      $this->load->view('user_panel/footer');
    }
    public function customer_mark_is_drop_process()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $page_name = $this->input->post("page_name");
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $addr_id = $this->input->post("addr_id"); $addr_id = (int)$addr_id;
      if(isset($addr_id) && $addr_id > 0) {
        $update_data = array(
          'is_drop' => 1,
          'drop_address_id' => (int)($addr_id), 
        );
        if($this->api->laundry_booking_update($update_data, $booking_id)) {
          $this->session->set_flashdata('success', $this->lang->line('Booking status updated successfully.'));
        } else {
          $this->session->set_flashdata('error', $this->lang->line('update_failed'));
        }
      } else {
        $firstname = $this->input->post("firstname");
        $lastname = $this->input->post("lastname");
        $mobile_no = $this->input->post("mobile");
        $zipcode = $this->input->post("zipcode");
        $street = $this->input->post("street");
        $country = $this->input->post("country");
        $state = $this->input->post("state");
        $city = $this->input->post("city");
        $deliver_instruction = $this->input->post("deliver_instruction");
        $pickup_instruction = $this->input->post("pickup_instruction");
        $address1 = $this->input->post("address1");
        $address2 = $this->input->post("toMapID");
        $latitude = $this->input->post("latitude");
        $longitude = $this->input->post("longitude");
        $address_type = $this->input->post("address_type");
        $company = $this->input->post("company");
        $email_id = $this->input->post("email_id");
        $today = date('Y-m-d H:i:s');

        $country_id = $this->user->getCountryIdByName($country);
        $state_id = $this->user->getStateIdByName($state,$country_id);

        if($state_id == 0) {
          //add state and get state id
          $state_id = $this->api->register_state($country_id, $state);
          //add city under state id
          $city_id = $this->api->register_city($state_id, $city);
        } else {
          $city_id = $this->user->getCityIdByName($city,$state_id);
          if($city_id == 0) {
            //add city and get city id
            $city_id = $this->api->register_city($state_id, $city);
          }
        }

        if( !empty($_FILES["address_image"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/address-images/';                 
          $config['allowed_types']  = 'gif|jpg|png';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('address_image')) {   
            $uploads    = $this->upload->data();  
            $image_url =  $config['upload_path'].$uploads["file_name"];  
          } else {  $image_url = "NULL"; }
        } else {  $image_url = "NULL"; }

        $add_data = array(
          'cust_id' => (int) $cust['cust_id'],
          'firstname' => trim($firstname), 
          'lastname' => trim($lastname), 
          'mobile_no' => trim($mobile_no), 
          'street_name' => trim($street), 
          'addr_line1' => trim($address1), 
          'addr_line2' => (trim($address2) != "") ? trim($address2) : "NULL", 
          'city_id' => (int)($city_id), 
          'state_id' => (int)($state_id), 
          'country_id' => (int)($country_id), 
          'zipcode' => $zipcode,
          'deliver_instructions' => (trim($deliver_instruction) !="") ? trim($deliver_instruction) : "NULL", 
          'pickup_instructions' => (trim($pickup_instruction) != "")? trim($pickup_instruction) :"NULL", 
          'image_url' => $image_url,
          'latitude' => trim($latitude), 
          'longitude' => trim($longitude), 
          'addr_type' => trim($address_type), 
          'comapny_name' => (trim($company) !="")? trim($company): "NULL", 
          'email' => (trim($email_id)!="")?trim($email_id):"NULL", 
          'cre_datetime' => $today,
        );
        //echo json_encode($add_data); die();
        if( $id = $this->api->register_new_address($add_data)) { 
          //Update drop_address_id and is_drop in laundry
          $update_data = array(
            'is_drop' => 1,
            'drop_address_id' => (int)($id), 
          );
          $this->api->laundry_booking_update($update_data, $booking_id);
          $this->session->set_flashdata('success', $this->lang->line('Booking status updated successfully.'));
        } else {
          $this->session->set_flashdata('error', $this->lang->line('update_failed'));
        }
      }

      if($page_name == 'customer_in_progress_laundry_bookings') {
        redirect('user-panel-laundry/customer-in-progress-laundry-bookings');
      } else if($page_name == 'customer_completed_laundry_bookings') {
        redirect('user-panel-laundry/customer-completed-laundry-bookings');
      } else {
        redirect('user-panel-laundry/customer-accepted-laundry-bookings');
      }
    }
    public function customer_in_progress_laundry_bookings()
    {
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $bookings = $this->api->laundry_booking_list($cust_id, 'in_progress', 'customer');
      for ($i=0; $i < sizeof($bookings); $i++) { 
        $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
        $bookings[$i] += ['bookings_details' => $bookings_details];
        if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
          $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
          $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
        }
        if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
          $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
          $bookings[$i] += ['drop_order_details' => $drop_order_details];
        }
      }
      //echo json_encode($bookings); die();
      $this->load->view('user_panel/laundry_customer_inprogress_bookings_list_view', compact('bookings','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function customer_completed_laundry_bookings()
    {
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $bookings = $this->api->laundry_booking_list($cust_id, 'completed', 'customer');
      for ($i=0; $i < sizeof($bookings); $i++) { 
        $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
        $bookings[$i] += ['bookings_details' => $bookings_details];
        if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
          $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
          $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
        }
        if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
          $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
          $bookings[$i] += ['drop_order_details' => $drop_order_details];
        }
      }
      //echo json_encode($bookings); die();
      $this->load->view('user_panel/laundry_customer_completed_bookings_list_view', compact('bookings','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function customer_cancelled_laundry_bookings()
    {
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $bookings = $this->api->laundry_booking_list($cust_id, 'cancelled', 'customer');
      for ($i=0; $i < sizeof($bookings); $i++) { 
        $bookings_details = $this->api->laundry_booking_details_list($bookings[$i]['booking_id']);
        $bookings[$i] += ['bookings_details' => $bookings_details];
        if($bookings[$i]['is_pickup'] == 1 && $bookings[$i]['pickup_order_id'] > 0) {
          $pickup_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['pickup_order_id']);
          $bookings[$i] += ['pickup_order_details' => $pickup_order_details];
        }
        if($bookings[$i]['is_drop'] == 1 && $bookings[$i]['drop_order_id'] > 0) {
          $drop_order_details = $this->api->laundry_pickup_drop_order_details($bookings[$i]['drop_order_id']);
          $bookings[$i] += ['drop_order_details' => $drop_order_details];
        }
      }
      //echo json_encode($bookings); die();
      $this->load->view('user_panel/laundry_customer_cancelled_bookings_list_view', compact('bookings','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function customer_laundry_pending_payments()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $filter = $this->input->post("filter");
      if($filter == 1) {
        $start_date = $date_start = $this->input->post("date_start");
        $date_start = explode('/', $date_start)[2].'-'.explode('/', $date_start)[0].'-'.explode('/', $date_start)[1].' 00:00:00';
        $end_date = $date_end = $this->input->post("date_end");
        $date_end = explode('/', $date_end)[2].'-'.explode('/', $date_end)[0].'-'.explode('/', $date_end)[1].' 23:59:59';
        $country_id = $this->input->post("country_id");
        $currency_sign = $this->input->post("currency_sign");
        $filter = 1;
      } else {
        $date_start = null;
        $date_end = null;
        $country_id = null;
        $currency_sign = null;
        $filter = 0;
      }

      $bookings = $this->api->pending_invoice_list($cust_id, 'completed', 'customer', 1, 0, 0, $date_start, $date_end, $country_id, $currency_sign);
      //echo $this->db->last_query();
      //echo json_encode($bookings); die();
      
      $currency_list = $this->api->get_all_currencies();
      $countries = $this->api->get_countries();
      $this->load->view('user_panel/customer_laundry_pending_payments_list_view', compact('bookings','cust_id','currency_list','countries','start_date', 'end_date', 'country_id', 'currency_sign','filter'));
      $this->load->view('user_panel/footer');
    }
  //End Laundry Customer Bookings--------------------------

  //Start Customer Cancel Booking--------------------------
    public function laundry_booking_cancel()
    {
      //echo json_encode($_POST); die();
      $booking_id = $this->input->post("booking_id"); $booking_id = (int)$booking_id;
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $booking_details = $this->api->laundry_booking_details($booking_id);

      if($booking_details['is_pickup'] == 0 || 
        ($booking_details['is_pickup'] == 1 && 
          $this->api->get_courier_details_by_ref_id($booking_details['provider_id'], $booking_details['booking_id'])['order_status'] == 'open')) {

        $provider_details = $this->api->get_user_details($booking_details['provider_id']);
        $user_details = $this->api->get_user_details($booking_details['cust_id']);
        $today = date('Y-m-d H:i:s');
        
        //get cancellation charges-----------------------------------------------
          $cancellation_charge_per = $this->api->get_gonagoo_laundry_commission_details($user_details['country_id'])['cancellation_charge'];
          $cancellation_charges = round((trim($booking_details['total_price'])) * ( $cancellation_charge_per / 100 ),2);
          $refund_amount = trim($booking_details['total_price']) - $cancellation_charges;
        //-----------------------------------------------------------------------

        $data = array(
          'booking_status' => 'cancelled',
          'status_updated_by' => trim($cust_id), 
          'status_updated_by_user_type' => 'customer', 
          'status_update_datetime' => trim($today),
          'cancelled_by' => 'customer', 
          'cancelled_date' => trim($today),
          'refund_amount' => trim($refund_amount),
        );

        if( $this->api->laundry_booking_update($data, $booking_id) ) {
          
          //check courier boonking and cancel it.
          if($booking_details['is_pickup'] == 1 && 
          $this->api->get_courier_details_by_ref_id($booking_details['provider_id'], $booking_details['booking_id'])['order_status'] == 'open'){
            $url = base_url('api/cancel-courier-order');
            $param = "order_id=".$booking_details['pickup_order_id'];
            $ch = curl_init($url); // url to send sms
            curl_setopt($ch, CURLOPT_HEADER, 0); // header to call url
            curl_setopt($ch, CURLOPT_POST, 1); // method to call url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return back to same page
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param); 
            $cancel_courier = curl_exec($ch); // execute url and save response
            curl_close($ch); // close url connection
            //echo $cancel_courier; die();
          }
          
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Your laundry booking has been cancelled.').' '.$this->lang->line('Booking ID: ').$booking_id;
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Laundry booking status'), 'type' => 'booking-in-progress', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Laundry booking status'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          
          //Email Customer name, customer Email, Subject, Message
          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email_id']));
            $username = $parts[0];
            $customer_name = $username;
          } else { $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }
          $this->api_sms->send_email_text($customer_name, trim($user_details['email_id']), $this->lang->line('Laundry booking status'), trim($message));

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($provider_details['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
          //Update to workroom
          $workroom_update = array(
            'order_id' => $booking_id,
            'cust_id' => $booking_details['cust_id'],
            'deliverer_id' => $booking_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $booking_details['cust_id'],
            'type' => 'order_status',
            'file_type' => 'text',
            'cust_name' => $customer_name,
            'deliverer_name' => $provider_name,
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 9
          );
          $this->api->update_to_workroom($workroom_update);

          //check laundry booking payment and process cancellation charges.
          if($booking_details['complete_paid'] == 1 && $booking_details['payment_mode']  != 'cod') {
            //Deduct Complete amount from Provider account-------------------------
              if(!$provider_account_master_details = $this->api->customer_account_master_details(trim($booking_details['provider_id']), trim($booking_details['currency_sign']))) {
                $insert_data_master = array(
                  "user_id" => trim($booking_details['provider_id']),
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_master);
                $provider_account_master_details = $this->api->customer_account_master_details(trim($booking_details['provider_id']), trim($booking_details['currency_sign']));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) - trim($refund_amount);

              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], trim($booking_details['provider_id']), $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details(trim($booking_details['provider_id']), trim($booking_details['currency_sign']));
              
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)trim($booking_details['provider_id']),
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'cancel_refund',
                "amount" => trim($refund_amount),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => trim($booking_details['cat_id']),
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //Deduct Complete amount from Provider account-------------------------

            //Add Complete booking amount to customer account----------------------
              if(!$customer_account_master_details = $this->api->customer_account_master_details(trim($booking_details['cust_id']), trim($booking_details['currency_sign']))) {
                $insert_data_master = array(
                  "user_id" => trim($booking_details['cust_id']),
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($booking_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_master);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($booking_details['cust_id']), trim($booking_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($refund_amount);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($booking_details['cust_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($booking_details['cust_id']), trim($booking_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$booking_id,
                "user_id" => (int)trim($booking_details['cust_id']),
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'cancel_refund',
                "amount" => trim($refund_amount),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($booking_details['currency_sign']),
                "cat_id" => trim($booking_details['cat_id']),
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //Deduct Complete amount from Provider account-------------------------
          }
          $this->session->set_flashdata('success', $this->lang->line('Booking cancelled successfully.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      
      } else { $this->session->set_flashdata('error', $this->lang->line('Order is in progress, contact service provider.')); }
      redirect('user-panel-laundry/customer-accepted-laundry-bookings');
    }
  //End Customer Cancel Booking----------------------------

  //Start Laundry Customer Driver Chat---------------------
    public function chatroom()
    {
      $order_id = $this->input->post('order_id');
      if(!$order_id) { $order_id = $this->session->userdata('workroom_order_id'); }
      $order = $this->api->get_order_detail($order_id);
      if($order){ 
        $driver_id = $order['driver_id'];
        $keys = ['driver_id' => $driver_id, 'cust_id' => $this->cust['cust_id'], 'order_id' => $order_id];
        $chats = $this->api->get_order_chat($order_id);
        $this->load->view('user_panel/laundry_chatroom_view', compact('chats','keys'));     
        $this->load->view('user_panel/footer'); 
      } else { redirect('user-panel-laundry/dashboard-laundry'); }
    } 
    public function add_order_chat()
    {
      $cust = $this->cust;
      $order_id = $this->input->post('order_id', TRUE);
      $text_msg = $this->input->post('message_text', TRUE);
      $order = $this->api->get_order_detail($order_id);
      $cust_id = $order['cust_id'];
      $driver_id = $order['driver_id'];
      $sender_id = $cust['cust_id'];
      $cust_name = $order['cust_name'];
      $cd_name = $order['cd_name'];
      $today = date("Y-m-d H:i:s");
      $order = $this->api->get_order_detail($order_id);

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('attachment')) {   
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"];  
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      $insert_data = array(
        "sender_id" => $sender_id,
        "receiver_id" => $driver_id,
        "cd_id" => 0,
        "cust_id" => $cust_id,
        "text_msg" => trim($text_msg),
        "attachment_url" => $attachment_url,
        "cre_datetime" => $today,
        "file_type" => "text",
        "order_id" => $order_id,
        "cd_name" => $cd_name,
        "cust_name" => $cust_name,
      );

      if( $chat_id = $this->api->add_chat_to_chatroom($insert_data)){
        $device_details = $this->api->get_driver_device_details($driver_id); 
        //Get Customer Device Reg ID's
        $arr_fcm_ids = $arr_apn_ids = array();
        foreach ($device_details as $value) {
          if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_apn_ids, $value['reg_id']); }
        }
        $api_key_customer = $this->config->item('delivererAppGoogleKey');
        $msg =  array(
          'title' => $this->lang->line('new_message'), 
          'type' => 'driver_chat', 
          'notice_date' => $today, 
          'desc' => $text_msg,
          'attachment' => $attachment_url,
          'cust_name' => $cust_name, 
          'order_id' => $order_id, 
          'cust_id' => $cust_id, 
          'file_type' => "text", 
        );
        //Push Notification FCM
        $this->api->sendFCM($msg, $arr_fcm_ids, $api_key_customer);
        //Push Notification APN
        $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
        if(is_array($arr_apn_ids)) { 
          $arr_apn_ids = implode(',', $arr_apn_ids);
          $this->notification->sendPushIOS($msg_apn_customer, $arr_apn_ids);
        }
      }
      $array = array('workroom_order_id' => $order_id );      
      $this->session->set_userdata( $array );
      redirect('user-panel-laundry/laundry-chat-room');
    }
  //End Laundry Customer Driver Chat-----------------------

  //Laundry Workroom---------------------------------------
    public function laundry_work_room()
    {
      $booking_id = $this->input->post('booking_id');
      if(!$booking_id) { $booking_id = $this->session->userdata('workroom_booking_id'); }
      $booking = $this->api->laundry_booking_details($booking_id); 
      $keys = ['provider_id' => $booking['provider_id'], 'cust_id' => $this->cust['cust_id'], 'booking_id' => $booking['booking_id']];
      $workroom = $this->api->get_laundry_workroom_chat($booking_id);
      $this->load->view('user_panel/laundry_workroom_view',compact('booking','workroom','keys'));
      $this->load->view('user_panel/footer');   
    }
    public function add_laundry_workroom_chat()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $booking_id = $this->input->post('booking_id', TRUE);
      $text_msg = $this->input->post('message_text', TRUE);
      $booking = $this->api->laundry_booking_details($booking_id); 
      $cust_id = $booking['cust_id'];
      $provider_id = $booking['provider_id'];
      $sender_id = $cust['cust_id'];
      $cust_name = ($booking['cust_name']!="NULL")?$booking['cust_name']:explode('@', $cust['email1'])[0];
      $provider = $this->api->get_user_details($booking['provider_id']); 
      $operator_name = ($booking['operator_name']!="NULL")?$booking['operator_name']:$provider['firstname'].$provider['lastname'];
      $today = date("Y-m-d H:i:s");
      //echo json_encode($booking); die();

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['max_width']      = '0'; 
        $config['max_height']     = '0'; 
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
        if($this->upload->do_upload('attachment')) { 
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      $insert_data = array(
        "order_id" => $booking_id,
        "cust_id" => $cust_id,
        "deliverer_id" => $provider_id,
        "text_msg" => $text_msg,
        "attachment_url" => $attachment_url,
        "cre_datetime" => $today,
        "sender_id" => $sender_id,
        "type" => "chat",
        "file_type" => "NULL",
        "cust_name" => $cust_name,
        "deliverer_name" => $operator_name,
        "thumbnail" => "NULL",
        "ratings" => "NULL",
        "cat_id" => 9,
      );
      //echo json_encode($insert_data); die();

      if( $ow_id = $this->api->add_laundry_chat_to_workroom($insert_data)){
        if($sender_id == $cust_id) {  $device_details = $this->api->get_user_device_details($cust_id); } 
        else {  $device_details = $this->api->get_user_device_details($provider_id); }
        $arr_fcm_ids = $arr_apn_ids = array();
        foreach ($device_details as $value) {
          if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_apn_ids, $value['reg_id']); }
        }
        $fcm_ids = $arr_fcm_ids;
        $apn_ids = $arr_apn_ids;
        // Get PN API Keys and PEM files
        // $api_key_customer = $this->config->item('delivererAppGoogleKey');
        // $api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
        $msg =  array(
          'title' => $this->lang->line('new_message'), 
          'type' => 'chat', 
          'notice_date' => $today, 
          'desc' => $text_msg,
          'attachment' => $attachment_url,
          'cust_name' => $cust_name, 
          'operator_name' => $operator_name, 
          'booking_id' => $booking_id, 
          'cust_id' => $cust_id, 
          'operator_id' => $provider_id, 
          'file_type' => "NULL", 
          'thumbnail' => "NULL",
          "ratings" => "NULL",
        );
        
        // $this->api->sendFCM($msg, $fcm_ids, $api_key_customer);
        // //Push Notification APN
        // $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
        // if(is_array($apn_ids)) { 
        //     $apn_ids = implode(',', $apn_ids);
        //     $this->notification->sendPushIOS($msg_apn_customer, $apn_ids);
        // }
      }

      $array = array('workroom_booking_id' => $booking_id );      
      $this->session->set_userdata( $array );
      redirect('user-panel-laundry/laundry-work-room');
    }
    public function add_laundry_workroom_rating()
    {
      $cust = $this->cust;
      $booking_id = $this->input->post('booking_id', TRUE);
      $rating = $this->input->post('rating', TRUE); $rating = (int) $rating;
      $text_msg = $this->input->post('review_text', TRUE);
      $booking = $this->api->laundry_booking_details($booking_id);
      $cust_id = $booking['cust_id'];
      $operator_id = $booking['provider_id'];
      $sender_id = $cust['cust_id'];
      $cust_name = ($booking['cust_name']!="NULL")?$booking['cust_name']:explode('@', $cust['email1'])[0];
      $provider = $this->api->get_user_details($booking['provider_id']); 
      $operator_name = ($booking['operator_name']!="NULL")?$booking['operator_name']:$provider['firstname'].$provider['lastname'];
      $today = date("Y-m-d H:i:s");

      $update_data = array(
        "review_comment" => $text_msg,
        "rating" => $rating,
        "review_datetime" => $today,
      );

      if( $this->api->update_laundry_review($update_data, $booking_id) ) {
        if(!$old_ratings = $this->api->get_bus_review_details($operator_id)) {
          $key = "insert";
          $operator_review = array(
            "ratings" => $rating,
            "total_ratings" => $rating,
            "no_of_ratings" => 1,
            "operator_id" => $operator_id,
            "update_date"=> $today
          );
          $this->api->update_laundry_ratings($operator_review, $operator_id, $key);
        } else {
          $no_of_ratings = $old_ratings['no_of_ratings']+1;
          $total_ratings = $old_ratings['total_ratings']+$rating;
          $ratings = round($total_ratings/$no_of_ratings,1);
          $operator_review = array(
            "ratings" => $ratings,
            "total_ratings" => $total_ratings,
            "no_of_ratings" => $no_of_ratings,
            "update_date"=> $today
          );
          $this->api->update_laundry_ratings($operator_review, $operator_id);  
        }
        if($deliver_old_rating = $this->api->get_laundry_review_details($operator_id)){
          $no_of_ratings = $old_ratings['no_of_ratings']+$deliver_old_rating['no_of_ratings'];
          $total_ratings = $old_ratings['total_ratings']+$deliver_old_rating['total_ratings'];
          $ratings = round($total_ratings/$no_of_ratings,1);
          $update = array(
            "ratings" => $ratings,
            "total_ratings" => $total_ratings,
            "no_of_ratings" => $no_of_ratings
          );
          $this->api->update_deliverer_rating($update ,$operator_id);
        } else {
          $no_of_ratings = $old_ratings['no_of_ratings']+1;
          $total_ratings = $old_ratings['total_ratings']+$rating;
          $ratings = round($total_ratings/$no_of_ratings,1);
          $update = array(
            "ratings" => $ratings,
            "total_ratings" => $total_ratings,
            "no_of_ratings" => $no_of_ratings
          );
          $this->api->update_deliverer_rating($update ,$operator_id);
        }
        
        $cust_details = $this->api->get_consumer_datails($cust_id);
        $review_data = array(
          "ticket_id" => $booking_id,
          "cust_id" => $cust_id,
          "cust_name" => $cust_name,
          "cust_avatar" => $cust_details['avatar_url'],
          "review_comment" => $text_msg,
          "review_rating" => $rating,
          "operator_id" => $operator_id,
          "operator_name" => $operator_name,
          "review_datetime" => $today,
        );
        $this->api->post_to_review_laundry($review_data);

        $insert_data = array(
          "order_id" => $booking_id,
          "cust_id" => $cust_id,
          "deliverer_id" => $operator_id,
          "text_msg" => $text_msg,
          "attachment_url" => "NULL",
          "cre_datetime" => $today,
          "sender_id" => $sender_id,
          "type" => "review_rating",
          "file_type" => "NULL",
          "cust_name" => $cust_name,
          "deliverer_name" => $operator_name,
          "thumbnail" => "NULL",
          "ratings" => $rating,
          "cat_id" => 9,
        );
        //ratings/total_ratings/no_of_ratings/
        if( $ow_id = $this->api->add_laundry_chat_to_workroom($insert_data)){
          // if($sender_id == $cust_id) {  $device_details = $this->api->get_user_device_details($cust_id); } 
          // else {  $device_details = $this->api->get_user_device_details($deliverer_id); }

          // //Get Customer Device Reg ID's
          // $arr_fcm_ids = $arr_apn_ids = array();
          // foreach ($device_details as $value) {
          //   if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
          //   else {  array_push($arr_apn_ids, $value['reg_id']); }
          // }

          // $fcm_ids = $arr_fcm_ids;
          // $apn_ids = $arr_apn_ids;

          // //Get PN API Keys and PEM files
          // $api_key_customer = $this->config->item('delivererAppGoogleKey');
          // //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

          // $msg =  array(
          //   'title' => 'Gonagoo - New message', 
          //   'type' => 'review_rating', 
          //   'notice_date' => $today, 
          //   'desc' => $text_msg,
          //   'attachment' => "NULL",
          //   'cust_name' => $cust_name, 
          //   'deliverer_name' => $deliverer_name, 
          //   'order_id' => $order_id, 
          //   'cust_id' => $cust_id, 
          //   'deliverer_id' => $deliverer_id, 
          //   'file_type' => "NULL", 
          //   'thumbnail' => "NULL",
          //   "ratings" => $rating
          // );
          // $this->api->sendFCM($msg, $fcm_ids, $api_key_customer);
          // //Push Notification APN
          // $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
          // if(is_array($apn_ids)) { 
          //     $apn_ids = implode(',', $apn_ids);
          //     $this->notification->sendPushIOS($msg_apn_customer, $apn_ids);
          // }
        }
      }
      
      $array = array('workroom_booking_id' => $booking_id );      
      $this->session->set_userdata( $array );
      redirect('user-panel-laundry/laundry-work-room');
    }
    public function workroom_reads()
    {
      $cust = $this->cust;
      $notification = $this->api->get_total_unread_workroom_notifications($cust['cust_id']);
      foreach ($notification as $n) {
        $this->api->make_workroom_notification_read($n['ow_id'], $cust['cust_id']);
      } echo 'success';
    }
    public function laundry_workroom()
    {
      $cust_id = $this->cust['cust_id'];
      //$tickets = $this->api->get_workorder_list($cust_id);  
      $orders = $this->api_bus->get_global_workroom_list((int)$cust_id);
      //echo $this->db->last_query();
      for ($i=0; $i < sizeof($orders); $i++) { 
        if($orders[$i]['cat_id'] == 6 || $orders[$i]['cat_id'] == 7) {
          if($service_details = $this->api_courier->get_courier_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
            $orders[$i] += array('service_details' => $service_details);
          } else { $orders[$i] += array('service_details' => array()); }
        } else if($orders[$i]['cat_id'] == 9) {
          if($service_details = $this->api_bus->get_laundry_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
            $service_details_details = $this->api_bus->laundry_booking_details_list($service_details['booking_id']);
            $service_details += ['bookings_details' => $service_details_details];
            $orders[$i] += array('service_details' => $service_details);
          } else { $orders[$i] += array('service_details' => array()); }
        } else {
          if($service_details = $this->api_bus->get_booking_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
            $orders[$i] += array('service_details' => $service_details);
          } else { $orders[$i] += array('service_details' => array()); }
        }
      }

      //echo json_encode($orders);  die();

      $this->load->view('user_panel/laundry_workroom_list_view', compact('orders'));
      $this->load->view('user_panel/footer');
    }
  //Laundry Workroom---------------------------------------

  //Start Claim Provider-----------------------------------
    public function provider_laundry_claims()
    {
      $cust_id = $this->cust_id;
      $laundry_claim_delay = $this->api->get_laundry_provider_profile($cust_id)['laundry_claim_delay'];
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'open',
        'device_type' => 0,
        'last_id' => 0,
        'service_cat_id' => 9,
        'service_id' => 0,
        'claim_id' => 0,
      );
      $claim_list = $this->api->get_claim_list($data);
      //echo json_encode($claim_list); die();

      $this->load->view('user_panel/laundry_provider_claim_list_view', compact('claim_list','cust_id','laundry_claim_delay'));
      $this->load->view('user_panel/footer');
    }
    public function provider_claim_details()
    {
      //echo json_encode($_POST); die();
      $claim_id = $this->input->post('claim_id', TRUE); $claim_id = (int)$claim_id; 
      $service_id = $this->input->post('service_id', TRUE); $service_id = (int)$service_id;
      $booking_details = $this->api->laundry_booking_details($service_id);
      $booking_details_list = $this->api->laundry_booking_details_list($service_id);
      $data = array(
        'claim_id' => $claim_id,
      );
      $claim_details = $this->api->get_claim_list($data);
      //echo json_encode($claim_details); die();

      $this->load->view('user_panel/laundry_provider_claim_details_view', compact('booking_details','booking_details_list','claim_details'));
      $this->load->view('user_panel/footer');
    }
    public function provider_claim_response()
    {
      //echo json_encode($_POST); die();
      $claim_id = $this->input->post('claim_id', TRUE);
      $claim_solution_desc = $this->input->post('claim_solution_desc', TRUE);
      $claim_status = $this->input->post('claim_status', TRUE);
      $today = date('Y-m-d H:i:s');

      $data = array(
        'claim_solution_desc' => $claim_solution_desc,
        'claim_status' => $claim_status,
        'claim_reponse_datetime' => $today,
        'claim_id' => (int)$claim_id,
      );
      
      if($this->api->update_service_claim_details($data, (int)$claim_id)){ 
        $this->session->set_flashdata('success', $this->lang->line('Claim status updated successfully.'));  
        redirect('user-panel-laundry/provider-laundry-claims','refresh');
      } else { 
        $this->session->set_flashdata('error', $this->lang->line('Unable to update status. Try again!'));  
        redirect('user-panel-laundry/provider-laundry-claims','refresh');
      } 
    }
    public function provider_laundry_claims_closed()
    {
      $cust_id = $this->cust_id;
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'closed',
        'device_type' => 0,
        'last_id' => 0,
        'service_cat_id' => 9,
        'service_id' => 0,
        'claim_id' => 0,
      );
      $claim_list = $this->api->get_claim_list($data);
      //echo json_encode($claim_list); die();

      $this->load->view('user_panel/laundry_provider_closed_claim_list_view', compact('claim_list','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function provider_closed_claim_details()
    {
      //echo json_encode($_POST); die();
      $claim_id = $this->input->post('claim_id', TRUE); $claim_id = (int)$claim_id; 
      $service_id = $this->input->post('service_id', TRUE); $service_id = (int)$service_id;
      $booking_details = $this->api->laundry_booking_details($service_id);
      $booking_details_list = $this->api->laundry_booking_details_list($service_id);
      $data = array(
        'claim_id' => $claim_id,
      );
      $claim_details = $this->api->get_claim_list($data);
      
      //echo json_encode($claim_details); die();

      $this->load->view('user_panel/laundry_provider_closed_claim_details_view', compact('booking_details','booking_details_list','claim_details'));
      $this->load->view('user_panel/footer');
    }
    public function update_laundry_claim_delay()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      $laundry_claim_delay = $this->input->post('laundry_claim_delay', TRUE);
      $data = array(
        'laundry_claim_delay' => $laundry_claim_delay,
      );
      if($this->api->update_provider($data, (int)$cust_id)){ 
        $this->session->set_flashdata('success', $this->lang->line('Claim delay hours updated successfully.'));  
        redirect('user-panel-laundry/provider-laundry-claims','refresh');
      } else { 
        $this->session->set_flashdata('error', $this->lang->line('Unable to update claim delay hours.'));  
        redirect('user-panel-laundry/provider-laundry-claims','refresh');
      } 
    }
  //End Claim Provider-------------------------------------

  //Start Claim Customer-----------------------------------
    public function customer_mark_claim()
    {
      $cust_id = $this->cust_id;
      $booking_id = $this->input->post('booking_id', TRUE);
      $booking_details = $this->api->laundry_booking_details($booking_id);
      $booking_details_list = $this->api->laundry_booking_details_list($booking_id);
      $claim_type = $this->api->get_claim_types();
      //echo json_encode($claim_type); die();
      $this->load->view('user_panel/customer_mark_claim_view', compact('booking_details','booking_details_list','cust_id','claim_type'));
      $this->load->view('user_panel/footer');
    }
    public function mark_claim_process()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $service_id = $this->input->post('service_id', TRUE); $service_id = (int)$service_id;
      $claim_type = $this->input->post('claim_type', TRUE);
      $claim_desc = $this->input->post('claim_desc', TRUE);
      $booking_details = $this->api->laundry_booking_details($service_id);
      $booking_details_list = $this->api->laundry_booking_details_list($service_id);
      $today = date('Y-m-d H:i:s');
      //echo json_encode($booking_details_list); die();
      $cat_ids = array();
      $sub_cat_ids = array();
      $item_counts = array();
      foreach ($booking_details_list as $list) {
        array_push($cat_ids,$list['cat_id']);
        array_push($sub_cat_ids,$list['sub_cat_id']);        
      }

      foreach ($booking_details_list as $list) {
        $bd_id = 'claimed_quantity_'.$list['bd_id'];
        array_push($item_counts, $_POST[$bd_id]); 
      }

      $cat_id = implode(',',$cat_ids);
      $sub_cat_id = implode(',',$sub_cat_ids);
      $item_count = implode(',',$item_counts);
      
      if( !empty($_FILES["image_url"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/claim-docs/'; 
        $config['allowed_types']  = 'gif|jpg|png'; 
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('image_url')) {   
          $uploads    = $this->upload->data();  
          $image_url =  $config['upload_path'].$uploads["file_name"];  
        } else {  $image_url = "NULL"; }
      } else {  $image_url = "NULL"; }

      if( !empty($_FILES["doc_url"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/claim-docs/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('doc_url')) {   
          $uploads = $this->upload->data();  
          $doc_url =  $config['upload_path'].$uploads["file_name"];  
        } else {  $doc_url = "NULL"; }
      } else {  $doc_url = "NULL"; }

      $data = array(
        'service_cat_id' => 9,
        'service_id' => trim($service_id), 
        'cust_id' => trim($cust_id), 
        'cust_name' => $booking_details['cust_name'], 
        'provider_id' => $booking_details['provider_id'], 
        'provider_name' => $booking_details['operator_name'], 
        'claim_type' => $claim_type, 
        'cat_ids' => $cat_id, 
        'sub_cat_ids' => $sub_cat_id, 
        'item_counts' => $item_count, 
        'claim_desc' => $claim_desc,
        'image_url' => $image_url, 
        'doc_url' => $doc_url, 
        'cre_datetime' => $today,
        'claim_status' => 'open', 
        'status_update_datetime' => $today, 
        'claim_solution_desc' => '', 
        'reopen_count' => 0,
      );
      //echo json_encode($data); die();

      if($claim_id = $this->api->register_claim($data)) {
        $data = array(
          'claim_id' => (int)$claim_id,
        );
        $this->api->laundry_booking_update($data, $service_id);

        $this->session->set_flashdata('success', $this->lang->line('Claim reported successfully.'));  
        redirect('user-panel-laundry/customer-laundry-claims','refresh');
      } else {
        $this->session->set_flashdata('error', $this->lang->line('Unable to report the claim, Try again!'));  
        redirect('user-panel-laundry/customer-laundry-claims','refresh');
      }
    }
    public function customer_laundry_claims()
    {
      $cust_id = $this->cust_id;
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'customer',
        'claim_status' => 'open',
        'device_type' => 0,
        'last_id' => 0,
        'service_cat_id' => 9,
        'service_id' => 0,
        'claim_id' => 0,
      );
      $claim_list = $this->api->get_claim_list($data);
      //echo json_encode($claim_list); die();

      $this->load->view('user_panel/laundry_customer_claim_list_view', compact('claim_list','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function customer_claim_details()
    {
      //echo json_encode($_POST); die();
      $claim_id = $this->input->post('claim_id', TRUE); $claim_id = (int)$claim_id; 
      $service_id = $this->input->post('service_id', TRUE); $service_id = (int)$service_id;
      $booking_details = $this->api->laundry_booking_details($service_id);
      $booking_details_list = $this->api->laundry_booking_details_list($service_id);
      $data = array(
        'claim_id' => $claim_id,
      );
      $claim_details = $this->api->get_claim_list($data);
      
      //echo json_encode($claim_details); die();

      $this->load->view('user_panel/laundry_customer_claim_details_view', compact('booking_details','booking_details_list','claim_details'));
      $this->load->view('user_panel/footer');
    }
    public function customer_laundry_claims_closed()
    {
      $cust_id = $this->cust_id;
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'customer',
        'claim_status' => 'closed',
        'device_type' => 0,
        'last_id' => 0,
        'service_cat_id' => 9,
        'service_id' => 0,
        'claim_id' => 0,
      );
      $claim_list = $this->api->get_claim_list($data);
      //echo json_encode($claim_list); die();

      $this->load->view('user_panel/laundry_customer_closed_claim_list_view', compact('claim_list','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function customer_reopen_claim()
    {
      //echo json_encode($_POST); die();
      $claim_id = $this->input->post('claim_id', TRUE);
      $claim_desc = $this->input->post('claim_desc', TRUE);

      $data = array(
        'claim_id' => (int)$claim_id,
      );
      $reopen_count = $this->api->get_claim_list($data)['reopen_count'];

      $data = array(
        'claim_desc' => $claim_desc,
        'claim_status' => 'open',
        'reopen_count' => ($reopen_count+1),
      );
      //echo json_encode($data); die();
      if($this->api->update_service_claim_details($data, (int)$claim_id)){ 
        $this->session->set_flashdata('success', $this->lang->line('Claim re-opened successfully.'));  
        redirect('user-panel-laundry/customer-laundry-claims','refresh');
      } else { 
        $this->session->set_flashdata('error', $this->lang->line('Unable to re-open claim. Try again!'));  
        redirect('user-panel-laundry/customer-laundry-claims','refresh');
      } 
    }
  //End Claim Customer-------------------------------------

  //Start Promo Code---------------------------------------
    public function promocode()
    {
      //echo json_encode($_POST); die(); 
      if(isset($_POST["promo_code"])){
        $booking_id = $this->input->post('booking_id');
        $details =  $this->api->laundry_booking_details($booking_id);
        $promo_code_name = $this->input->post("promo_code");
        if($details['promo_code'] == 'NULL'){
          if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
            if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
              if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$this->cust_id)){
                  $discount_amount = ($details['booking_price']/100)*$promocode['discount_percent'];
                  $booking_price = $details['booking_price']-$discount_amount;
                  $total_price = $details['pickup_courier_price']+$details['drop_courier_price']+$booking_price;
                  $update = array(
                    'booking_price' => $booking_price,
                    'balance_amount' => $booking_price,
                    'total_price' => $total_price,
                    'promo_code' => $promocode['promo_code'],
                    'promo_code_id' => $promocode['promo_code_id'],
                    'promo_code_version' => $promocode['promo_code_version'],
                    'discount_amount' => $discount_amount,
                    'discount_percent' => $promocode['discount_percent'],
                    'actual_price' => $details['booking_price'],
                    'promo_code_datetime' => date('Y-m-d h:i:s')
                  );
                  $this->api->laundry_booking_update($update , $details['booking_id']);

                  $used_promo_update = array(
                    'promo_code_id' => $promocode['promo_code_id'],
                    'promo_code_version' => $promocode['promo_code_version'],
                    'cust_id' => $details['cust_id'],
                    'discount_percent' => $promocode['discount_percent'],
                    'discount_amount' => $discount_amount,
                    'discount_currency' => $details['currency_sign'],
                    'cre_datetime' => date('Y-m-d h:i:s'),
                    'service_id' => '9',
                    'booking_id' => $details['booking_id'],
                  );
                  $this->api->register_user_used_prmocode($used_promo_update);
                  $this->session->set_flashdata('success', $this->lang->line('Promocode Applied successfully'));
                //code end here
              }else{
                $this->session->set_flashdata('error_promo', $this->lang->line('You are already used this promocode'));
              }
            }else{
              $this->session->set_flashdata('error_promo', $this->lang->line('This Promocode has been Expired'));
            }
          }else{
            $this->session->set_flashdata('error_promo', $this->lang->line('Invalid Promocode Entered'));
          }
        }else{
            $this->session->set_flashdata('error_promo', $this->lang->line('You are already used promocode for this order'));
        }
      }
      
      if($_POST["page_name"] == "payment_buyer"){
        redirect('user-panel-laundry/laundry-booking-payment-by-cutomer/'.$booking_id);
      }else if($_POST["page_name"] == "payment_seller"){
        redirect('user-panel-laundry/laundry-booking-payment-by-provider/'.$booking_id);
      }
    }
    public function promocode_ajax()
    {
      $promo_code_name =  $this->input->post('promocode');
      $booking_id =  $this->input->post('booking_id');
      $details =  $this->api->laundry_booking_details($booking_id);
      if($promo_code = $this->api->get_promocode_by_name($promo_code_name)){
        $service_id = explode(',',$promo_code['service_id']);
        if($details['promo_code'] == 'NULL'){
          if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
            if(in_array("9",$service_id)){
              if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$this->cust_id)){
                  $discount_amount = round(($details['booking_price']/100)*$promocode['discount_percent'] , 2); 
                  if($details['currency_sign'] == 'XAF'){
                    $discount_amount = round($discount_amount , 0);
                  }
                  $booking_price =  round($details['booking_price']-$discount_amount ,2);
                  if($details['currency_sign'] == 'XAF'){
                    $booking_price = round($booking_price , 0);
                  }

                  echo "success@".$this->lang->line('You will get')." ".$discount_amount." ".$details['currency_sign']." ".$this->lang->line('Discount')."$".$this->lang->line('Use this promocode and pay')." ".$booking_price." ".$details['currency_sign']; 
                }else{
                  echo "error@".$this->lang->line('You are already used this promocode');
                }
              }else{
                echo "error@".$this->lang->line('This Promocode has been Expired');
              }
            }else{
              echo "error@".$this->lang->line('This promocode is not valid for this category');
            }
          }else{
            echo "error@".$this->lang->line('Invalid Promocode Entered');
          }
        }else{
          echo "error@".$this->lang->line('You are already used promocode for this order');
        }
      }else{
        echo "error@".$this->lang->line('Invalid Promocode Entered');
      }
    }
  //End Promo Code----------------------------------------
  //Start Laundry Special charges-------------------------
    public function laundry_special_charges_list()
    {
      //echo json_encode($_POST); die();
      $charges = $this->api->get_laundry_special_charges_list($this->cust_id);
      $this->load->view('user_panel/laundry_special_charges_list_view',compact('charges'));
      $this->load->view('user_panel/footer');
    }
    
    public function laundry_special_charges_add()
    {
      $this->load->view('user_panel/laundry_special_charges_list_add',compact('charges'));
      $this->load->view('user_panel/footer');
    }

    public function laundry_special_charges_register()
    {
      //echo json_encode($_POST); die();
      $mon = $this->input->post('mon');
      $tue = $this->input->post('tue');
      $wed = $this->input->post('wed');
      $thu = $this->input->post('thu');
      $fri = $this->input->post('fri');
      $sat = $this->input->post('sat');
      $sun = $this->input->post('sun');
      $start_date = $this->input->post('start_date');
      $end_date = $this->input->post('end_date');
      if($mon == ""){$mon = "0";}    
      if($tue == ""){$tue = "0";}
      if($wed == ""){$wed = "0";}
      if($thu == ""){$thu = "0";}
      if($fri == ""){$fri = "0";}
      if($sat == ""){$sat = "0";}
      if($sun == ""){$sun = "0";}
      $data =array(
        'cust_id' => trim($this->cust_id), 
        'mon' => trim($mon), 
        'tue' => trim($tue), 
        'wed' => trim($wed), 
        'thu' => trim($thu), 
        'fri' => trim($fri), 
        'sat' => trim($sat), 
        'sun' => trim($sun), 
        'start_date' => trim($start_date), 
        'end_date' => trim($end_date), 
        'end_date' => trim($end_date), 
        'cre_date' => date('Y-m-d'), 
      );
      if($this->api->add_laundry_special_charges($data)){
        $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Error! while adding laundry special charges!')); }
      redirect('user-panel-laundry/laundry-special-charges-add');
    }
    
    public function laundry_special_charges_edit()
    {
      if( !is_null($this->input->post('charge_id')) ) { $charge_id = (int) $this->input->post('charge_id'); }
      else if(!is_null($this->uri->segment(3))) { $charge_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-laundry/laundry-special-charges-list'); }
      $charges = $this->api->get_laundry_special_charges($charge_id);
      //echo json_encode($charges); die();
      $this->load->view('user_panel/laundry_special_charges_list_edit', compact('charges'));
      $this->load->view('user_panel/footer');
    }
    
    public function laundry_special_charges_update()
    {
      //echo json_encode($_POST); die();
      $charge_id = $this->input->post('charge_id');
      $mon = $this->input->post('mon');
      $tue = $this->input->post('tue');
      $wed = $this->input->post('wed');
      $thu = $this->input->post('thu');
      $fri = $this->input->post('fri');
      $sat = $this->input->post('sat');
      $sun = $this->input->post('sun');
      $start_date = $this->input->post('start_date');
      $end_date = $this->input->post('end_date');
      if($mon == ""){$mon = "0";}    
      if($tue == ""){$tue = "0";}
      if($wed == ""){$wed = "0";}
      if($thu == ""){$thu = "0";}
      if($fri == ""){$fri = "0";}
      if($sat == ""){$sat = "0";}
      if($sun == ""){$sun = "0";}

      $data =array(
        'mon' => trim($mon), 
        'tue' => trim($tue), 
        'wed' => trim($wed), 
        'thu' => trim($thu), 
        'fri' => trim($fri), 
        'sat' => trim($sat), 
        'sun' => trim($sun), 
        'start_date' => trim($start_date), 
        'end_date' => trim($end_date), 
        'end_date' => trim($end_date), 
        'update_date' => date('Y-m-d'), 
      );
      if($this->api->update_laundry_special_charges($data, $charge_id)){
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Error! while updating details!')); }
      redirect('user-panel-laundry/laundry-special-charges-edit/'.$charge_id);
    }
    public function laundry_special_charges_delete()
    {
      $charge_id = $this->input->post('id', TRUE);
      if($this->api->delete_laundry_special_charges((int)$charge_id)){ echo 'success'; } else { echo 'failed';  } 
    }
  //end Laundry Special charges---------------------------



}
/* End of file User_panel_laundry.php */
/* Location: ./application/controllers/User_panel_laundry.php */