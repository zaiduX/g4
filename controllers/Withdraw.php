<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdraw extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Withdraw_model', 'withdraw');
        $this->load->model('Notification_model', 'notice');
        $this->load->model('Api_model_services', 'api');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function withdraw_request()
	{
		$auth_type_id = $this->session->userdata('auth_type_id');
    if($auth_type_id > 1) {   $admin_country_id = $this->session->userdata('admin_country_id'); }
    else { $admin_country_id = 0; }
		$withdraw_request_list = $this->withdraw->get_withdraw_request($admin_country_id);
      //echo json_encode($withdraw_request_list); die();
		$this->load->view('admin/withdraw_request_list_view', compact('withdraw_request_list'));			
		$this->load->view('admin/footer_view');		
	}
	
	public function reject_withdraw_request()
	{
		// echo $_POST['id']; die();
		// echo json_encode($_POST); die();
		$req_id = $this->input->post('id', TRUE);
		$job_id = $this->input->post('job_id', TRUE);
		$job_details = $this->api->get_customer_job_details($job_id);
		if($job_details['service_type'] == "1"){
			$data = array('withdraw_status' => "reject");
    	$this->api->update_job_details($job_id ,$data);
		}
		if( $this->withdraw->reject_withdraw_request( (int) $req_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	
	public function accept_withdraw_request()
	{
		// echo json_encode($_POST); die();
		$req_id = $this->input->post('req_id', TRUE);
		$job_id = $this->input->post('job_id', TRUE);
		$milestone_id = $this->input->post('milestone_id', TRUE);
		$request_details = $this->withdraw->get_withdraw_request_details($req_id);
		$this->load->view('admin/withdraw_request_accept_view', compact('request_details'));
		$this->load->view('admin/footer_view');
	}

	public function update_withdraw_transfer_amount()
	{   
		$req_id = $this->input->post('req_id', TRUE);
		$user_id = $this->input->post('user_id', TRUE);
		$user_type = $this->input->post('user_type', TRUE);
		$job_id = $this->input->post('job_id', TRUE);
		$milestone_id = $this->input->post('milestone_id', TRUE);
	  // echo json_encode($milestone_id); die();
		$amount_transferred = $this->input->post('amount_transferred', TRUE); $amount_transferred = (int)$amount_transferred;
		$currency_code = $this->input->post('currency_code', TRUE);
		$transaction_id = $this->input->post('transaction_id', TRUE);
		
		if($transaction_id == 'mtn')
		{
  		$phone_no = $this->input->post('transfer_account_number', TRUE);
  		if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transaction.xhtml?idbouton=2&typebouton=PAIE&_amount=".$amount_transferred."&_tel=".$phone_no."&_email=admin@gonagoo.com&_clP=Managoua2017!";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      $contents = curl_exec($ch);
      $mtn_pay_res = json_decode($contents, TRUE);
      if($mtn_pay_res['StatusCode'] === "01")
      {
        //echo $mtn_pay_res['StatusCode']; die();
        $transaction_id = $mtn_pay_res['TransactionID'];
        $update_data = array();
  			$today = date('Y-m-d h:i:s');
    		$update_data = array(
    			"amount_transferred" => trim($amount_transferred),
    			"transaction_id" => trim($transaction_id),
    			"response" => 'accept',
    			"response_datetime" => $today,
    		);
    		
    		$job_details = $this->api->get_customer_job_details($job_id);
				if($job_details['service_type'] == "1"){
					$data = array('withdraw_status' => "accept");
		    	$this->api->update_job_details($job_id ,$data);
				}elseif($milestone_id > 0){
					$data = array('withdraw_status' => "accept");
		    	$this->api->update_proposal_milestone_request($data , $milestone_id);
				}

    		//Payment Module-------------------------------------------------------------------------------------
    		if($user_type == 0) {
    			//Deduct withdraw amount from user main + history
    			$customer_account_master_details = $this->withdraw->customer_account_master_details_currencywise((int)$user_id, $currency_code);
    			$account_balance = trim($customer_account_master_details['account_balance']) - trim($amount_transferred);
    			$this->withdraw->update_payment_in_customer_master_by_id($customer_account_master_details['account_id'], (int)$user_id, $account_balance);
    			$customer_account_master_details = $this->withdraw->customer_account_master_details_currencywise((int)$user_id, $currency_code);
    
    			$update_data_account_history = array(
						"account_id" => (int)$customer_account_master_details['account_id'],
						"order_id" => 0,
						"user_id" => (int)$user_id,
						"datetime" => $today,
						"type" => 0,
						"transaction_type" => 'cash_withdrawn',
						"amount" => trim($amount_transferred),
						"account_balance" => trim($customer_account_master_details['account_balance']),
						"withdraw_request_id" => trim($req_id),
						"currency_code" => trim($currency_code),
            "order_id" =>$job_id ,
            "cat_id" => 0,
					);
      			$this->withdraw->insert_payment_in_account_history($update_data_account_history);
    			//smp Deduct withdraw amount from user main + history
    			$customer_account_master_details = $this->withdraw->customer_account_master_details_currencywise_smp((int)$user_id, $currency_code);
    			$account_balance = trim($customer_account_master_details['account_balance']) - trim($amount_transferred);
    			$this->withdraw->update_payment_in_customer_master_by_id_smp($customer_account_master_details['account_id'], (int)$user_id, $account_balance);
    			$customer_account_master_details = $this->withdraw->customer_account_master_details_currencywise_smp((int)$user_id, $currency_code);
    
    			$update_data_account_history = array(
						"account_id" => (int)$customer_account_master_details['account_id'],
						"order_id" => 0,
						"user_id" => (int)$user_id,
						"datetime" => $today,
						"type" => 0,
						"transaction_type" => 'cash_withdrawn',
						"amount" => trim($amount_transferred),
						"account_balance" => trim($customer_account_master_details['account_balance']),
						"withdraw_request_id" => trim($req_id),
						"currency_code" => trim($currency_code),
            "order_id" =>$job_id ,
            "cat_id" => 0,
					);
      		$this->withdraw->insert_payment_in_account_history_smp($update_data_account_history);
        
    			if( $this->withdraw->update_withdraw_transfer_amount((int) $req_id, $update_data)){
    				$this->session->set_flashdata('success','Transfer details updated successfully!');
    			}else{
    				$this->session->set_flashdata('error','Unable to update or no change found! Try again...');
    			}
				// } else if ($user_type == 1) {
    		} else if (false) {
    			//Deduct withdraw amount from relay main + history
    			$relay_account_master_details = $this->withdraw->relay_account_master_details_currencywise((int)$user_id, $currency_code);
    			$account_balance = trim($relay_account_master_details['account_balance']) - trim($amount_transferred);
    			$this->withdraw->update_payment_in_relay_master_by_id($relay_account_master_details['account_id'], (int)$user_id, $account_balance);
    			$relay_account_master_details = $this->withdraw->relay_account_master_details((int)$user_id, $currency_code);
    
    			$update_data_account_history = array(
						"account_id" => (int)$relay_account_master_details['account_id'],
						"order_id" => 0,
						"user_id" => (int)$user_id,
						"datetime" => $today,
						"type" => 0,
						"transaction_type" => 'cash_withdrawn',
						"amount" => trim($amount_transferred),
						"account_balance" => trim($relay_account_master_details['account_balance']),
						"withdraw_request_id" => trim($req_id),
						"currency_code" => trim($currency_code),
            "order_id" =>$job_id ,
            "cat_id" => 0,
					);
    			$this->withdraw->insert_payment_in_relay_account_history($update_data_account_history);
    			if( $this->withdraw->update_withdraw_transfer_amount((int) $req_id, $update_data)){
    				$this->session->set_flashdata('success','Transfer details updated successfully!');
					} else {
						$this->session->set_flashdata('error','Unable to update or no change found! Try again...');
					}
	  		}else{
	  			$this->session->set_flashdata('error','Unable to update or no change found! Try again...');
	  		} 
	    } else {
	      $this->session->set_flashdata('error','MTN transaction failed! Try again...');
	    }
		} else {
  		$update_data = array();
  		$today = date('Y-m-d h:i:s');
  		$update_data = array(
  			"amount_transferred" => trim($amount_transferred),
  			"transaction_id" => trim($transaction_id),
  			"response" => 'accept',
  			"response_datetime" => $today,
  		);
  		$job_details = $this->api->get_customer_job_details($job_id);
			if($job_details['service_type'] == "1"){
				$data = array('withdraw_status' => "accept");
	    	$this->api->update_job_details($job_id ,$data);
			}elseif($milestone_id > 0){
				$data = array('withdraw_status' => "accept");
	    	$this->api->update_proposal_milestone_request($data , $milestone_id);
			}
  		//Payment Module-----------------------------------------------------------------------------------
  		if($user_type == 0) {
  		  //Deduct withdraw amount from user main + history
  			$customer_account_master_details = $this->withdraw->customer_account_master_details_currencywise((int)$user_id, $currency_code);
  			$account_balance = trim($customer_account_master_details['account_balance']) - trim($amount_transferred);
  			$this->withdraw->update_payment_in_customer_master_by_id($customer_account_master_details['account_id'], (int)$user_id, $account_balance);
  			$customer_account_master_details = $this->withdraw->customer_account_master_details_currencywise((int)$user_id, $currency_code);
  
  			$update_data_account_history = array(
					"account_id" => (int)$customer_account_master_details['account_id'],
					"order_id" => 0,
					"user_id" => (int)$user_id,
					"datetime" => $today,
					"type" => 0,
					"transaction_type" => 'cash_withdrawn',
					"amount" => trim($amount_transferred),
					"account_balance" => trim($customer_account_master_details['account_balance']),
					"withdraw_request_id" => trim($req_id),
					"currency_code" => trim($currency_code),
          "order_id" =>$job_id ,
          "cat_id" => 0,
				);
  			$this->withdraw->insert_payment_in_account_history($update_data_account_history);
  		  //Smp Deduct withdraw amount from user main + history
  			$customer_account_master_details = $this->withdraw->customer_account_master_details_currencywise_smp((int)$user_id, $currency_code);
  			$account_balance = trim($customer_account_master_details['account_balance']) - trim($amount_transferred);
  			$this->withdraw->update_payment_in_customer_master_by_id_smp($customer_account_master_details['account_id'], (int)$user_id, $account_balance);
  			$customer_account_master_details = $this->withdraw->customer_account_master_details_currencywise_smp((int)$user_id, $currency_code);
  
  			$update_data_account_history = array(
					"account_id" => (int)$customer_account_master_details['account_id'],
					"order_id" => 0,
					"user_id" => (int)$user_id,
					"datetime" => $today,
					"type" => 0,
					"transaction_type" => 'cash_withdrawn',
					"amount" => trim($amount_transferred),
					"account_balance" => trim($customer_account_master_details['account_balance']),
					"withdraw_request_id" => trim($req_id),
					"currency_code" => trim($currency_code),
          "order_id" =>$job_id ,
          "cat_id" => 0,
				);
  			$this->withdraw->insert_payment_in_account_history_smp($update_data_account_history);    
  			if( $this->withdraw->update_withdraw_transfer_amount((int) $req_id, $update_data)){
  				$this->session->set_flashdata('success','Transfer details updated successfully!');
  			}else{
  				$this->session->set_flashdata('error','Unable to update or no change found! Try again...');
  			}
  		// } else if ($user_type == 1) {
  		} else if (false) {
  			//Deduct withdraw amount from relay main + history
  			$relay_account_master_details = $this->withdraw->relay_account_master_details_currencywise((int)$user_id, $currency_code);
  			$account_balance = trim($relay_account_master_details['account_balance']) - trim($amount_transferred);
  			$this->withdraw->update_payment_in_relay_master_by_id($relay_account_master_details['account_id'], (int)$user_id, $account_balance);
  			$relay_account_master_details = $this->withdraw->relay_account_master_details((int)$user_id, $currency_code);
  
  			$update_data_account_history = array(
					"account_id" => (int)$relay_account_master_details['account_id'],
					"order_id" => 0,
					"user_id" => (int)$user_id,
					"datetime" => $today,
					"type" => 0,
					"transaction_type" => 'cash_withdrawn',
					"amount" => trim($amount_transferred),
					"account_balance" => trim($relay_account_master_details['account_balance']),
					"withdraw_request_id" => trim($req_id),
					"currency_code" => trim($currency_code),
          "order_id" =>$job_id ,
          "cat_id" => 0,
				);
  			$this->withdraw->insert_payment_in_relay_account_history($update_data_account_history);
  			if( $this->withdraw->update_withdraw_transfer_amount((int) $req_id, $update_data)){
  				$this->session->set_flashdata('success','Transfer details updated successfully!');
  			}else{
  				$this->session->set_flashdata('error','Unable to update or no change found! Try again...');
  			}
  		}else{
  			$this->session->set_flashdata('error','Unable to update or no change found! Try again...');
  		}
		}
		redirect('admin/withdraw-request/');
	}

}

/* End of file Withdraw.php */
/* Location: ./application/controllers/Withdraw.php */