<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advance_payment_services extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('Advance_payment_services_model', 'services');
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
  
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else { redirect('admin');}
  }
  public function index() {
    $adv_payments = $this->services->get_payments();
    $this->load->view('admin/services/advance_payment_services_list_view', compact('adv_payments'));      
    $this->load->view('admin/footer_view');   
  }
  public function add() {
    $countries = $this->services->get_countries();
    $category_type = $this->services->get_category_type();
    $this->load->view('admin/services/advance_payment_services_add_view', compact('countries','category_type'));
    $this->load->view('admin/footer_view');
  }
  public function get_country_currencies() {
    $country_id = $this->input->post('country_id', TRUE); $country_id = (int) $country_id;
    $currencies = $this->services->get_country_currencies($country_id);
    echo json_encode($currencies);
  }
  public function get_categories_by_type() {
    $type_id = $this->input->post('type_id', TRUE);
    $parent_cats = $this->services->get_categories_by_type($type_id);
    echo json_encode($parent_cats);
  }
  public function register() { 
    //echo json_encode($_POST); die();
    $currency_id = $this->input->post('currency_id', TRUE);
    $currency_name = $this->input->post('currency_name', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $cat_type_id = $this->input->post('cat_type_id', TRUE);
    $cat_id = $this->input->post('cat_id', TRUE);
    $advance_payment = $this->input->post('advance_payment', TRUE);
    $gonagoo_commission = $this->input->post('gonagoo_commission', TRUE);
    $min_hourly_budget = $this->input->post('min_hourly_budget', TRUE);
    $security_days = $this->input->post('security_days', TRUE);
    $max_quantity_to_buy_offer = $this->input->post('max_quantity_to_buy_offer', TRUE);
    
    $insert_data = array(
      "country_id" => (int)trim($country_id), 
      "cat_type_id" => (int)trim($cat_type_id), 
      "cat_id" => (int)trim($cat_id), 
      "advance_payment" => number_format($advance_payment,2,'.',''),
      "gonagoo_commission" => number_format($gonagoo_commission,2,'.',''),
      "cre_datetime" =>  date("Y-m-d H:i:s"), 
      "status" =>  1, 
      "security_days" => (int)trim($security_days), 
      "min_hourly_budget" => (int)trim($min_hourly_budget), 
      "currency_id" => (int)trim($currency_id), 
      "currency_code" => trim($currency_name), 
      "max_quantity_to_buy_offer" => (int)trim($max_quantity_to_buy_offer), 
    );
    //echo json_encode($insert_data); die();
    if(! $this->services->check_payment($insert_data) ) {
      if($this->services->register_payment($insert_data)) { $this->session->set_flashdata('success','Payment setup successfully added!'); } 
      else { $this->session->set_flashdata('error','Unable to add payment configuration!'); }
    } else { $this->session->set_flashdata('error','Configuration already exists for selected country!'); }
    redirect('admin/advance-payment-services/add');
  }
  public function edit() { 
    if( $id = $this->uri->segment(4) ) {      
      if( $payment = $this->services->get_payment_detail($id)){
        $this->load->view('admin/services/advance_payment_services_edit_veiw', compact('payment'));
        $this->load->view('admin/footer_view');
      } else { redirect('admin/advance-payment-services'); }
    } else { redirect('admin/advance-payment-services'); }
  }
  public function update() { 
    //echo json_encode($_POST); die();
    $smp_ap_id = $this->input->post('smp_ap_id', TRUE);
    $advance_payment = $this->input->post('advance_payment', TRUE);
    $gonagoo_commission = $this->input->post('gonagoo_commission', TRUE);
    $min_hourly_budget = $this->input->post('min_hourly_budget', TRUE);
    $security_days = $this->input->post('security_days', TRUE);
    $max_quantity_to_buy_offer = $this->input->post('max_quantity_to_buy_offer', TRUE);

    $update_data = array(       
      "advance_payment" => number_format($advance_payment,2,'.',''),
      "gonagoo_commission" => number_format($gonagoo_commission,2,'.',''),
      "min_hourly_budget" => number_format($min_hourly_budget,2,'.',''),
      "security_days" => $security_days,
      "max_quantity_to_buy_offer" => $max_quantity_to_buy_offer,
    );
    
    if($this->services->update_payment($update_data, $smp_ap_id)){
      $this->session->set_flashdata('success','Payment setup successfully updated!');
    } else { $this->session->set_flashdata('error','Unable to update configuration or same configuration found! Try with different configuration...');  }
    redirect('admin/advance-payment-services/edit/'.$smp_ap_id);
  }
  public function delete() {
    $id = $this->input->post('id', TRUE);
    if($this->services->delete_payment($id)){ echo 'success'; } else { echo "failed";  } 
  }
}

/* End of file Advance_payment_services.php */
/* Location: ./application/controllers/Advance_payment_services.php */