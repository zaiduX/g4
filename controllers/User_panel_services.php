<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class User_panel_services extends CI_Controller {
    
  private $cust_id = 0; 
  private $cust = array();
    
  public function __construct() {
    parent::__construct();
    
    $this->cust_id = $this->session->userdata("cust_id"); $this->cust_id = (int) $this->cust_id;
    
    $this->load->model('Api_model_services', 'api');
    $this->load->model('Notification_model_bus', 'notification');
    $this->load->model('Web_user_model', 'user');
    $this->load->model('Web_user_model', 'web_user');
    $this->load->model('Api_model_sms', 'api_sms');
  
    $this->cust = $this->api->get_consumer_datails($this->cust_id, true); $cust = $this->cust;
    
    if( ( $this->session->userdata("is_logged_in") == "NULL" ) OR ( $this->session->userdata("is_logged_in") !== 1 ) ) { redirect('log-in'); }
    
    if($cust['mobile_verified'] == 0) { redirect('verification/otp'); }

    $lang = $this->session->userdata('language');
    $this->config->set_item('language', strtolower($lang));

    if(trim($lang) == 'french'){
      $this->lang->load('frenchApi_lang','french');
      $this->lang->load('frenchFront_lang','french');
    } else if(trim($lang) == 'spanish'){  
      $this->lang->load('spanishApi_lang','spanish');
      $this->lang->load('spanishFront_lang','spanish');
    } else { 
      $this->lang->load('englishApi_lang','english');
      $this->lang->load('englishFront_lang','english');
    }
    
    $this->api->update_last_login($cust['cust_id']);
    
    //echo json_encode($cust); die();
    if (!$this->input->is_ajax_request()){ 
      // var_dump($_POST); 
      $order_id = $this->input->post('job_id');
      $type = $this->input->post('type');

      // if($order_id && !$ow_id) { 
      if($order_id && ($type =="workroom")) { 
        $ids = $this->api->get_total_unread_order_workroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) {  $this->api->make_workroom_notification_read($v['ow_id'], $cust['cust_id']);  }
      }

      if($order_id && ($type =="chatroom")) { 
        $ids = $this->api->get_total_unread_order_chatroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) { $this->api->make_chatrooom_notification_read($v['chat_id'], $cust['cust_id']);  }
      }

      if($cust['user_type'] == 1 ){ $user_type ="Individuals"; } else { $user_type ="Business"; }
      $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust['cust_id'],$user_type );

      $notification = $this->api->get_total_unread_notifications($cust['cust_id'],$user_type, 5);
      $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust['cust_id']);      
      $nt_workroom = $this->api->get_total_unread_workroom_notifications($cust['cust_id'], 5); 
      //echo json_encode($nt_workroom); die(); 
      //echo $this->db->last_query(); die();

      if(isset($_SESSION['deliverdata'])){
        $this->session->unset_userdata('deliverdata');
      }
      if(isset($_SESSION['questions'])){
        $this->session->unset_userdata('questions');      
      }
      if(isset($_SESSION['answers'])){
        $this->session->unset_userdata('answers');      
      }

      if(isset($_SESSION['post_project'])){
        $this->session->unset_userdata('post_project');      
      }



      $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust['cust_id'],$user_type );
      $notification = $this->api->get_total_unread_notifications($cust['cust_id'],$user_type, 5);
      $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust['cust_id']); 
      $nt_workroom = $this->api->get_total_unread_workroom_notifications($cust['cust_id'], 5); 
      $total_chatroom_notifications = $this->api->get_total_unread_chatroom_notifications_count($cust['cust_id']);      
      $nt_chatroom = $this->api->get_total_unread_chatroom_notifications($cust['cust_id'], 5);


      // $total_chatroom_notifications = $this->api->get_total_unread_chatroom_notifications_count($cust['cust_id']);      
      // $nt_chatroom = $this->api->get_total_unread_chatroom_notifications($cust['cust_id'], 5);  

      $compact = ["total_unread_notifications","notification","total_workroom_notifications","nt_workroom","total_chatroom_notifications","nt_chatroom"];

    
      $this->load->view('services/header_services', compact($compact)); 
      $this->load->view('services/menu_services', compact("cust"));
    }
  }

  //Start Dashboard----------------------------------------
    public function dashboard_services() { 
      /*$cust = $this->cust;
      $this->load->view('user_panel/dashboard_laundry', compact('cust'));   
      if($cust['acc_type'] == 'buyer') {
        $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
        $current_balance = ($current_balance != NULL ) ? $current_balance['account_balance'] : 0;
        //total spend in service
        $total_spend = $this->user->get_total_spend($cust['cust_id'], 'XAF');    
        $total_spend = ($total_spend !=NULL)?$total_spend['amount']:0;
        $total_spend_service = $this->user->get_total_spend_service($cust['cust_id'], 'XAF', 9); 
        $total_spend_service = $total_spend_service['amount'];
        $total_spend_service = ($total_spend_service !=NULL && $total_spend_service !=null) ? $total_spend_service: 0; 
        //Bookings
        $total_bookings = $this->api->laundry_booking_list($cust['cust_id'], 'all', 'customer', 0, 0);
        $accepted = $this->api->laundry_booking_list($cust['cust_id'], 'accepted', 'customer', 0, 0);
        $in_progress = $this->api->laundry_booking_list($cust['cust_id'], 'in_progress', 'customer', 0, 0);
        $completed = $this->api->laundry_booking_list($cust['cust_id'], 'completed', 'customer', 0, 0);
        $cancelled = $trip_list = $this->api->laundry_booking_list($cust['cust_id'], 'cancelled', 'customer', 0, 0);
        $compact = compact('cust','current_balance','total_spend','total_bookings','accepted','in_progress','completed','cancelled', 'total_spend_service');
        $this->load->view('user_panel/buyer_dashboard_laundry',$compact);
      } else {
        $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
        $current_balance = ($current_balance !=NULL && $current_balance !=null)?$current_balance['account_balance']:0;
        $total_earned = $this->user->get_total_earned($cust['cust_id'], 'XAF'); 
        $total_earned = $total_earned['amount'];
        $total_earned = ($total_earned !=NULL && $total_earned !=null) ? $total_earned: 0; 
        $total_spend = $this->user->get_total_spend($cust['cust_id'], 'XAF');
        $total_spend = ($total_spend !=NULL && $total_spend !=null)?$total_spend['amount']:0;
        //total earned in service
        $total_earned_service = $this->user->get_total_earned_service($cust['cust_id'], 'XAF', 9); 
        $total_earned_service = $total_earned_service['amount'];
        $total_earned_service = ($total_earned_service !=NULL && $total_earned_service !=null) ? $total_earned_service: 0; 
        //total spend in service
        $total_spend_service = $this->user->get_total_spend_service($cust['cust_id'], 'XAF', 9); 
        $total_spend_service = $total_spend_service['amount'];
        $total_spend_service = ($total_spend_service !=NULL && $total_spend_service !=null) ? $total_spend_service: 0; 
        //Ratings
        $laundry_rating = $this->api->get_laundry_review_details($cust['cust_id']);
        $deliverer_ratings = $this->api->get_provider_review_details($cust['cust_id']);
        //Orders
        $total_orders = $this->api->laundry_booking_list($cust['cust_id'], 'all', 'provider', 0, 0);
        $accepted = $this->api->laundry_booking_list($cust['cust_id'], 'accepted', 'provider', 0, 0);
        $in_progress = $this->api->laundry_booking_list($cust['cust_id'], 'in_progress', 'provider', 0, 0);
        $completed = $this->api->laundry_booking_list($cust['cust_id'], 'completed', 'provider', 0, 0);
        $cancelled = $trip_list = $this->api->laundry_booking_list($cust['cust_id'], 'cancelled', 'provider', 0, 0);
        
        $compact = compact('total_orders','accepted','in_progress','completed','cancelled','deliverer_ratings','laundry_rating','cust','current_balance','total_earned','total_spend', 'total_spend_service', 'total_earned_service');
        $this->load->view('user_panel/both_dashboard_laundry', $compact);
      }*/
      $this->load->view('services/dashboard');
      $this->load->view('user_panel/footer');
    }
    public function dashboard_laundry_users()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust['cust_id'];
      //Get Customers List
      $customers_list = $this->api->laundry_booking_and_customer_list($cust_id, 'all', 'provider', 1, 0, 0);
      //echo json_encode($customers_list); die();

      $cust_ids =array();
      //collect customer id's expect walking customer
      foreach ($customers_list as $customer) {
        if($customer['cust_id'] != 67) array_push($cust_ids, $customer['cust_id']);
      }
      $cust_ids = implode(',',$cust_ids);
      //echo json_encode($cust_ids); die();

      //Get Recent customer list
      $recentlyConnectedUsers = $this->api->get_user_login_history('recently', $cust_ids, 0, 0);
      $FrequentlyConnectedUsers = $this->api->get_user_login_history('frequently', $cust_ids, 0, 0);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($FrequentlyConnectedUsers); die();
      
      //status-wise orders
      $all_order_list = $this->api->laundry_booking_list($cust_id, 'all', 'provider', 0, 0);
      $accepted_order_list = $this->api->laundry_booking_list($cust_id, 'accepted', 'provider', 0, 0);
      $in_progress_order_list = $this->api->laundry_booking_list($cust_id, 'in_progress', 'provider', 0, 0);
      $completed_order_list = $this->api->laundry_booking_list($cust_id, 'completed', 'provider', 0, 0);
      $cancelled_order_list = $this->api->laundry_booking_list($cust_id, 'cancelled', 'provider', 0, 0);
      //echo json_encode($all_order_list); die();

      //day-wise orders
      $today_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'today', 'provider', 0, 0);
      $week_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'week', 'provider', 0, 0);
      $month_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'month', 'provider', 0, 0);
      $till_date_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'till_date', 'provider', 0, 0);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($today_order_list); die();

      //Claim day-wise
      $today_claim_list = $this->api->laundry_claim_day_wise($cust_id, 'today', 'provider', 0, 0);
      $week_claim_list = $this->api->laundry_claim_day_wise($cust_id, 'week', 'provider', 0, 0);
      $month_claim_list = $this->api->laundry_claim_day_wise($cust_id, 'month', 'provider', 0, 0);
      $till_date_claim_list = $this->api->laundry_claim_day_wise($cust_id, 'till_date', 'provider', 0, 0);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($today_order_list); die();

      //Claim status-wise
      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'open',
        'service_cat_id' => 9,
      );
      $open_claim_list = $this->api->get_claim_status_wise($data);

      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'in_progress',
        'service_cat_id' => 9,
      );
      $in_progress_claim_list = $this->api->get_claim_status_wise($data);

      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'standby',
        'service_cat_id' => 9,
      );
      $standby_claim_list = $this->api->get_claim_status_wise($data);

      $data = array(
        'cust_id' => $cust_id,
        'user_type' => 'provider',
        'claim_status' => 'closed',
        'service_cat_id' => 9,
      );
      $closed_claim_list = $this->api->get_claim_status_wise($data);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($today_order_list); die();

     /* echo date('Y-m-d h:i:s'); die();
      $from_date = 0;
      $to_date = 0;
      $till_date_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'till_date', 'provider', 0, 0, $from_date, $to_date);*/

      $currency_sign = $this->api->get_country_currencies((int)$_SESSION['admin_country_id'])[0]['currency_sign'];
      $currency_title = $this->api->get_country_currencies((int)$_SESSION['admin_country_id'])[0]['currency_title'];


      $today_start = date('Y-m-d 00:00:00');
      $today_end = date('Y-m-d 23:59:59');
      $data = array(
        'cust_id' => $cust_id,
        'from_date' => $today_start,
        'to_date' => $today_end,
        'currency_code' => $currency_sign,
        'cat_id' => 9,
      );
      if($this->api->laundry_sales_day_wise_count($data)) {
        $todays_transaction = (int)$this->api->laundry_sales_day_wise_count($data)[0]['total_sales'];
      } else { $todays_transaction = 0; }
      //echo json_encode($this->db->last_query()); die();

      for ($i=1; $i <= 14; $i++) { 
        $day = date('Y-m-d', strtotime('-'.$i.' days'));
        $today_start = date($day.' 00:00:00');
        $today_end = date($day.' 23:59:59');
        $data = array(
          'cust_id' => $cust_id,
          'from_date' => $today_start,
          'to_date' => $today_end,
          'currency_code' => $currency_sign,
          'cat_id' => 9,
        );

        if($this->api->laundry_sales_day_wise_count($data)) {
          ${"todays_transaction_" . $i} = (int)$this->api->laundry_sales_day_wise_count($data)[0]['total_sales'];
        } else { ${"todays_transaction_" . $i} = 0; }
        //echo ${"todays_transaction_" . $i} . '<br />';
      }
      //echo json_encode($todays_transaction); die();
      
      $this->load->view('user_panel/laundry_provider_customer_list_view', compact('customers_list', 'cust_id', 'recentlyConnectedUsers', 'FrequentlyConnectedUsers', 'all_order_list','accepted_order_list','in_progress_order_list','completed_order_list','cancelled_order_list','today_order_list','week_order_list','month_order_list','till_date_order_list','today_claim_list','week_claim_list','month_claim_list','till_date_claim_list','open_claim_list','in_progress_claim_list','standby_claim_list','closed_claim_list','currency_sign','currency_title','todays_transaction','todays_transaction_1','todays_transaction_2','todays_transaction_3','todays_transaction_4','todays_transaction_5','todays_transaction_6','todays_transaction_7','todays_transaction_8','todays_transaction_9','todays_transaction_10','todays_transaction_11','todays_transaction_12','todays_transaction_13','todays_transaction_14'));
      $this->load->view('user_panel/footer');
    }
    public function today_laundry_orders()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $today_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'today', 'provider', 0, 0);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/today_laundry_orders_list_view', compact('today_order_list', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function current_week_laundry_orders()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $current_week_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'week', 'provider', 0, 0);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/current_week_laundry_orders_list_view', compact('current_week_order_list', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function current_month_laundry_orders()
    {   
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $current_month_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'month', 'provider', 0, 0);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/current_month_laundry_orders_list_view', compact('current_month_order_list', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function till_date_laundry_orders()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;
      $from_date = $f_date = $this->input->post("from_date");
      $to_date = $t_date = $this->input->post("to_date");

      if(!isset($from_date)) { 
        $from_date = 'NULL'; 
        $f_date = 'NULL';
        $t_date = 'NULL'; 
      } else { 
        $from_date = explode('/', $from_date)[2] . '-' . explode('/', $from_date)[0] . '-' . explode('/', $from_date)[1] . ' 00:00:00'; 
      }

      if(!isset($to_date)) { 
        $to_date = 'NULL';
        $f_date = 'NULL';
        $t_date = 'NULL'; 
      } else { 
        $to_date = explode('/', $to_date)[2] . '-' . explode('/', $to_date)[0] . '-' . explode('/', $to_date)[1] . ' 23:59:59'; 
      }

      $till_date_order_list = $this->api->laundry_bookings_day_wise($cust_id, 'till_date', 'provider', 0, 0, $from_date, $to_date);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/till_date_laundry_orders_list_view', compact('till_date_order_list', 'cust_id', 'f_date', 't_date'));
      $this->load->view('user_panel/footer');
    }
    public function today_laundry_sales()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'NULL'; }

      $currencies = $this->api->get_all_currencies();

      $today_transaction_list = $this->api->laundry_sales_day_wise($cust_id, 'today', 0, 0, 'NULL', 'NULL', $currency_sign);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/today_laundry_transaction_list_view', compact('today_transaction_list', 'cust_id', 'currencies', 'currency_sign'));
      $this->load->view('user_panel/footer');
    }
    public function current_week_laundry_sales()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'NULL'; }

      $currencies = $this->api->get_all_currencies();

      $week_transaction_list = $this->api->laundry_sales_day_wise($cust_id, 'week', 0, 0, 'NULL', 'NULL', $currency_sign);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/current_week_laundry_transaction_list_view', compact('week_transaction_list', 'cust_id', 'currencies', 'currency_sign'));
      $this->load->view('user_panel/footer');
    }
    public function current_month_laundry_sales()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'NULL'; }

      $currencies = $this->api->get_all_currencies();

      $month_transaction_list = $this->api->laundry_sales_day_wise($cust_id, 'month', 0, 0, 'NULL', 'NULL', $currency_sign);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/current_month_laundry_transaction_list_view', compact('month_transaction_list', 'cust_id', 'currencies', 'currency_sign'));
      $this->load->view('user_panel/footer');
    }
    public function till_date_laundry_sales()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'NULL'; }


      $from_date = $f_date = $this->input->post("from_date");
      $to_date = $t_date = $this->input->post("to_date");

      if(!isset($from_date) || $from_date == '') { 
        $from_date = 'NULL'; 
        $f_date = 'NULL';
        $t_date = 'NULL';
      } else { 
        $from_date = explode('/', $from_date)[2] . '-' . explode('/', $from_date)[0] . '-' . explode('/', $from_date)[1] . ' 00:00:00'; 
      }

      if(!isset($to_date) || $to_date == '') { 
        $to_date = 'NULL';
        $f_date = 'NULL';
        $t_date = 'NULL';
      } else { 
        $to_date = explode('/', $to_date)[2] . '-' . explode('/', $to_date)[0] . '-' . explode('/', $to_date)[1] . ' 23:59:59'; 
      }
      $currencies = $this->api->get_all_currencies();
      $till_date_transaction_list = $this->api->laundry_sales_day_wise($cust_id, 'month', 0, 0, $from_date, $to_date, $currency_sign);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/till_date_laundry_transaction_list_view', compact('till_date_transaction_list', 'cust_id', 'currencies', 'currency_sign', 'f_date', 't_date'));
      $this->load->view('user_panel/footer');
    }
    public function best_customers()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$cust;

      $customers_list = $this->api->laundry_booking_and_customer_list($cust_id, 'all', 'provider', 1, 0, 0);
      //echo json_encode($customers_list); die();

      $this->load->view('user_panel/laundry_provider_best_customer_list_view', compact('customers_list', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function laundry_customer_consumption_history()
    {
      //echo json_encode($_POST); die();
      $c_id = $this->input->post("c_id");
      $currency_sign = $this->input->post("currency_sign");
      if(!isset($currency_sign)) { $currency_sign = 'null'; }

      $cust = $this->cust;
      $cust_id = (int)$cust['cust_id'];

      $currencies = $this->api->get_all_currencies();

      $booking_list = $this->api->laundry_booking_and_customer_provider_list($c_id, 'all', 'customer', 'provider' , $cust_id, 0, 0, 0, $currency_sign );
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($booking_list); die();

      $this->load->view('user_panel/laundry_customer_consumption_list_view', compact('booking_list', 'cust_id', 'currencies', 'c_id', 'currency_sign'));
      $this->load->view('user_panel/footer');
    }
  //End Dashboard------------------------------------------

  //Start User Profile-------------------------------------
    public function user_profile() { 
      $cust = $this->cust;
      $total_edu = $this->api->get_total_educations($cust['cust_id']);
      $education = $this->api->get_customer_educations($cust['cust_id'], 1);
      $total_exp = $this->api->get_total_experiences($cust['cust_id'], false);
      $total_other_exp = $this->api->get_total_experiences($cust['cust_id'], true);
      $experience = $this->api->get_customer_experiences($cust['cust_id'],false, 1);
      $other_experience = $this->api->get_customer_experiences($cust['cust_id'],true, 1);
      $skills = $this->api->get_customer_skills($cust['cust_id'], 1);
      $total_portfolios = $this->api->get_total_portfolios($cust['cust_id']);
      $portfolio = $this->api->get_customer_portfolio($cust['cust_id'], 1);
      $total_docs = $this->api->get_total_documents($cust['cust_id']);
      $docs = $this->api->get_customer_documents($cust['cust_id'], 1);
      $total_pay_methods = $this->api->get_total_payment_methods($cust['cust_id']);
      $pay_methods = $this->api->get_customer_payment_methods($cust['cust_id'], 1);
      $per_counter = 0;
      $total_counters = 15;

      if($cust['email_verified'] == 1) { $per_counter += 1; }
      if($cust['mobile_verified'] == 1) { $per_counter += 1; }
      if($cust['firstname'] != "NULL") { $per_counter += 1; }
      if($cust['lastname'] != "NULL") { $per_counter += 1; }
      if($cust['gender'] != "NULL") { $per_counter += 1; }
      if($cust['overview'] != "NULL") { $per_counter += 1; }
      if($cust['lang_known'] != "NULL") { $per_counter += 1; }
      if($cust['country_id'] > 0) { $per_counter += 1; }
      if($cust['state_id'] > 0) { $per_counter += 1; }
      if($cust['city_id'] > 0) { $per_counter += 1; }
      if($cust['avatar_url'] != "NULL") { $per_counter += 1; }
      if($total_edu > 0) { $per_counter += 1; }
      if($total_exp > 0) { $per_counter += 1; }
      if($experience) { $per_counter += 1; }
      if($skills) { $per_counter += 1; }

      $percentage = (int) (((int) $per_counter / (int) $total_counters ) *100 );
      $compact = compact('total_edu','education','total_exp','total_other_exp','experience','other_experience','skills','portfolio','total_portfolios','total_docs','docs','total_pay_methods','pay_methods', 'percentage');
      //echo json_encode($compact); die();

      $this->load->view('services/services_user_profile_view', $compact);
      $this->load->view('user_panel/footer');
    }
    public function edit_basic_profile_view()
    {
      $countries = $this->api->get_countries();
      $states = $this->api->get_state_by_country_id($this->cust['country_id']);
      if($this->cust['state_id'] > 0 ){ $cities = $this->api->get_city_by_state_id($this->cust['state_id']); } else{ $cities = array(); }
      $this->load->view('services/services_edit_basic_profile_view', compact('countries', 'states', 'cities'));
      $this->load->view('user_panel/footer');
    }
    public function get_state_by_country_id()
    {
      $country_id = $this->input->post('country_id');
      echo json_encode($this->api->get_state_by_country_id($country_id));
    }
    public function get_city_by_state_id()
    {
      $state_id = $this->input->post('state_id');
      echo json_encode($this->api->get_city_by_state_id($state_id));
    }
    public function update_basic_profile()
    {
      $mobile_no = $this->input->post('mobile_no');
      $firstname = $this->input->post('firstname');
      $lastname = $this->input->post('lastname');
      $country_id = $this->input->post('country_id');
      $state_id = $this->input->post('state_id');
      $city_id = $this->input->post('city_id');
      $gender = $this->input->post('gender');
      $session_mobile_no = $this->session->userdata('mobile_no');

      $cust = $this->cust; 
      $old_avatar_url = $cust['avatar_url'];
      $old_cover_url = $cust['cover_url'];
      
      if($state_id == NULL) { $state_id = $cust['state_id']; }
      if($city_id == NULL) { $city_id = $cust['city_id']; }

      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('avatar')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];
          if( $old_avatar_url != "NULL") { unlink($old_avatar_url); }           
        } else { $avatar_url = $old_avatar_url; }
      } else { $avatar_url = $old_avatar_url; }

      if( !empty($_FILES["cover"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/cover-images/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('cover')) {   
          $uploads    = $this->upload->data();  
          $cover_url =  $config['upload_path'].$uploads["file_name"];
          if( $old_cover_url != "NULL") { unlink($old_cover_url); }           
        } else { $cover_url = $old_cover_url; }
      } else { $cover_url = $old_cover_url; }

      if($mobile_no === $session_mobile_no ) {
        $update_data = array(
          "firstname" => $firstname,
          "lastname" => $lastname,
          "mobile1" => $mobile_no,
          "city_id" => $city_id,
          "state_id" => $state_id,
          "country_id" => $country_id,
          "gender" => $gender,
          "avatar_url" => $avatar_url,
          "cover_url" => $cover_url,
        );    
      } else {
        $OTP = mt_rand(100000,999999);
        $update_data = array(
          "firstname" => $firstname,
          "lastname" => $lastname,
          "mobile1" => $mobile_no,
          "city_id" => $city_id,
          "state_id" => $state_id,
          "country_id" => $country_id,
          "avatar_url" => $avatar_url,
          "cover_url" => $cover_url,
          "gender" => $gender,
          "cust_otp" => $OTP,
          "mobile_verified" => 0,
        );
      }

      if($this->api->update_basic_profile($update_data)) {
        if($mobile_no == $session_mobile_no ) { 
          $this->session->set_flashdata('success',$this->lang->line('updated_successfully')); 
          redirect('user-panel-services/basic-profile/'. $this->cust_id);
        } else {
          $this->session->set_userdata('mobile_no', $mobile_no);
          $this->api->sendSMS($this->api->get_country_code_by_id((int)$country_id).$mobile_no, $this->lang->line('lbl_verify_otp'). ': ' . $OTP);
          redirect('verification/otp');
        }
      } else {
        $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found'));  
        redirect('user-panel-services/basic-profile/'. $this->session->userdata('cust_id'));
      }
    }
  //End User Profile---------------------------------------

  //Start User Payment Methods-----------------------------
    public function payment_method_list_view()
    {
      $payments = $this->api->get_payment_details($this->cust['cust_id']);
      if($payments) {
        $this->load->view('services/services_payment_method_list_view', compact('payments'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function add_payment_method_view()
    {
      $this->load->view('services/services_add_payment_method_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_payment_method()
    {
      $cust = $this->cust;
      $pay_method = $this->input->post("pay_method");
      $brand_name = $this->input->post("brand_name");
      $card_no = $this->input->post("card_no");
      $month = $this->input->post("month");
      $year = $this->input->post("year");
      $bank_name = $this->input->post("bank_name");
      $bank_address = $this->input->post("bank_address");
      $account_title = $this->input->post("account_title");
      $bank_short_code = $this->input->post("bank_short_code");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'payment_method' => trim($pay_method), 
        'brand_name' => trim($brand_name),
        'bank_name' => trim($bank_name),
        'account_title' => trim($account_title),
        'account_title' => trim($account_title),
        'bank_short_code' => trim($bank_short_code),
        'pay_status' => 1,
        'card_number' => trim($card_no),
        'expirty_date' => trim($month). '/'.trim($year),
        'cre_datetime' => $today,
      );
          
      if( $id = $this->api->register_payment_details($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-services/payment-method/add');
    } 
    public function edit_payment_method_view()
    {
      $id = $this->uri->segment(3);
      if( $pay = $this->api->get_payment_method_detail($id)){
        $this->load->view('services/services_edit_payment_method_view',compact('pay'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function edit_payment_method()
    {
      $pay_id = $this->input->post("payment_id");
      $pay_method = $this->input->post("pay_method");
      $brand_name = $this->input->post("brand_name");
      $card_no = $this->input->post("card_no");
      $month = $this->input->post("month");
      $year = $this->input->post("year");
      $bank_name = $this->input->post("bank_name");
      $bank_address = $this->input->post("bank_address");
      $account_title = $this->input->post("account_title");
      $bank_short_code = $this->input->post("bank_short_code");

      $edit_data = array(
        'payment_method' => trim($pay_method), 
        'brand_name' => trim($brand_name),
        'bank_name' => trim($bank_name),
        'bank_address' => trim($bank_address),
        'account_title' => trim($account_title),
        'bank_short_code' => trim($bank_short_code),
        'card_number' => trim($card_no),
        'expirty_date' => trim($month). '/'.trim($year),
      );
          
      if( $id = $this->api->update_payment_details($pay_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-services/payment-method/'. $pay_id);
    }
    public function delete_payment_method()
    {
      $id = $this->input->post("id");     
      if( $this->api->delete_payment_detail($this->cust['cust_id'], $id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Payment Methods-------------------------------

  //Start User Educations----------------------------------
    public function education_list_view()
    {
      $educations = $this->api->get_customer_educations($this->cust['cust_id'], 0);
      $this->load->view('services/services_eduction_list_view', compact('educations'));    
      $this->load->view('user_panel/footer');
    }
    public function add_education_view()
    {
      $this->load->view('services/services_add_education_view');   
      $this->load->view('user_panel/footer');
    }
    public function add_education()
    {
      $cust = $this->cust;
      $institute_name = $this->input->post("institute_name");
      $qualification = $this->input->post("qualification");
      $title = $this->input->post("title");
      $yr_qualification = $this->input->post("yr_qualification");
      $grade = $this->input->post("grade");
      $yr_attend = $this->input->post("yr_attend");
      $yr_attend_to = $this->input->post("yr_attend_to");
      $additional_info = $this->input->post("additional_info");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'institute_name' => $institute_name, 
        'qualification' => $qualification, 
        'description' => $additional_info,
        'start_date' => $yr_attend,
        'end_date' => $yr_attend_to,
        'grade' => $grade, 
        'qualify_year' => $yr_qualification, 
        'certificate_title' => $title, 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today, 
        'mod_datetime' => $today
      );

      if( $id = $this->api->add_user_education($add_data)) {
        $this->session->set_flashdata('success',$this->lang->line('added_successfully')); 
      } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }
      redirect('user-panel-services/education/add');
    }
    public function edit_education_view()
    {
      $edu_id = $this->uri->segment(3);
      if( $education = $this->api->get_customer_education_by_id($edu_id)){
        $this->load->view('services/services_edit_education_view', compact('education'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-user_panel/user-profile'); }
    }
    public function edit_education()
    {
      $edu_id = $this->input->post("education_id");
      $institute_name = $this->input->post("institute_name");
      $qualification = $this->input->post("qualification");
      $title = $this->input->post("title");
      $yr_qualification = $this->input->post("yr_qualification");
      $grade = $this->input->post("grade");
      $yr_attend = $this->input->post("yr_attend");
      $yr_attend_to = $this->input->post("yr_attend_to");
      $additional_info = $this->input->post("additional_info");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'edu_id' => (int) $edu_id,
        'institute_name' => $institute_name, 
        'qualification' => $qualification, 
        'description' => $additional_info,
        'start_date' => $yr_attend,
        'end_date' => $yr_attend_to,
        'grade' => $grade, 
        'qualify_year' => $yr_qualification, 
        'certificate_title' => $title, 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_education($edit_data)) {
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully')); 
      } else { $this->session->set_flashdata('error',$this->lang->line('update_failed'));  }
      redirect('user-panel-services/education/'.$edu_id);
    }
    public function delete_education()
    {
      $edu_id = $this->input->post("id");
      if( $id = $this->api->delete_user_education($edu_id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Educations------------------------------------

  //Start User Experience----------------------------------
    public function experience_list_view()
    {
      $experiences = $this->api->get_customer_experiences($this->cust['cust_id'], false, 0);
      $this->load->view('services/services_experience_list_view', compact('experiences'));   
      $this->load->view('user_panel/footer');
    }
    public function add_experience_view()
    {
      $this->load->view('services/services_add_experience_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_experience()
    {
      $cust = $this->cust;
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');
      
      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => date('Y-m-d',strtotime($start_date)),
        'end_date' => ($currently_working == "on" ) ? "NULL": date('Y-m-d',strtotime($end_date)),
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 0, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today,
        'mod_datetime' => $today
      );
      //echo json_encode($add_data); die();
      if( $id = $this->api->add_user_experience($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-services/experience/add');
    }
    public function edit_experience_view()
    {
      $exp_id = $this->uri->segment(3);
      if( $experience = $this->api->get_customer_experience_by_id($exp_id)){
        $this->load->view('services/services_edit_experience_view',compact('experience'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function edit_experience()
    {
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'exp_id' => (int) $exp_id,
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_experience($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      redirect('user-panel-services/experience/'.$exp_id);
    }
    public function delete_experience()
    {
      $exp_id = $this->input->post("id");
      if( $id = $this->api->delete_user_experience($exp_id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Experience------------------------------------

  //Start User Other Experience----------------------------
    public function other_experience_list_view()
    {
      $experiences = $this->api->get_customer_experiences($this->cust['cust_id'],true, 0);
      if($experiences) {
        $this->load->view('services/services_other_experience_list_view', compact('experiences'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function add_other_experience_view()
    {
      $this->load->view('services/services_add_other_experience_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_other_experience()
    {
      $cust = $this->cust;
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 1, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today,
        'mod_datetime' => $today
      );

      if( $id = $this->api->add_user_experience($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-services/other-experience/add');
    }
    public function edit_other_experience_view()
    {
      $exp_id = $this->uri->segment(3);
      if( $experience = $this->api->get_customer_experience_by_id($exp_id)){
        $this->load->view('services/services_edit_other_experience_view',compact('experience'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function edit_other_experience()
    {
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'exp_id' => (int) $exp_id,
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 1, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_experience($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      redirect('user-panel-services/other-experience/'.$exp_id);
    }
    public function delete_other_experience()
    {
      $exp_id = $this->input->post("id");
      if( $id = $this->api->delete_user_experience($exp_id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Other Experience------------------------------

  //Start User Skills--------------------------------------
    public function edit_skill_view_old()
    {
      $skills = $this->api->get_customer_skills($this->cust['cust_id']);
      if($skills) {
        $junior_skills = explode(',', $skills['junior_skills']);
        $confirmed_skills = explode(',', $skills['confirmed_skills']);
        $senior_skills = explode(',', $skills['senior_skills']);
        $expert_skills = explode(',', $skills['expert_skills']);
      } 
      $category = $this->api->get_category();
      if( $this->cust['cust_id'] > 0 ){
        $this->load->view('services/services_edit_skill_view_old',compact('category','junior_skills', 'confirmed_skills', 'senior_skills','expert_skills'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function edit_skill_old()
    {
      // echo json_encode($_POST); die();
      $cust = $this->cust;
      $cat_id = $this->input->post("category_id");
      $skill_level = $this->input->post("skill_level");
      $skills = $this->input->post("skills");
      $today = date('Y-m-d H:i:s');
      $cust_skills = $this->api->get_customer_skills($this->cust['cust_id']);
      if($skills != NULL AND !empty($skills)){ $skills = implode(',', $skills); } else { $skills = "NULL"; }
      $edit_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'cs_id' => (int) $cust_skills['cs_id'],
        'cat_id' => $cat_id,
        'skill_level' => $skill_level,
        'junior_skills' => ($skill_level == "junior") ? $skills : $cust_skills['junior_skills'],
        'confirmed_skills' => ($skill_level == "confirmed") ? $skills : $cust_skills['confirmed_skills'],
        'senior_skills' => ($skill_level == "senior") ? $skills : $cust_skills['senior_skills'],
        'expert_skills' => ($skill_level == "expert") ? $skills : $cust_skills['expert_skills'],
        'ext1' => "NULL",
        'ext2' => "NULL",
      );
      if( $this->api->update_user_skill($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-services/skill');
    }
    public function edit_skill_view()
    {
      //echo json_encode($skills); die();
      $skills = $this->api->get_customer_skills($this->cust['cust_id']); 
      $old_sub_cat =  $skills['confirmed_skills_name'];
      $sub_category = $this->api->get_category_by_id_all();
      if( $this->cust['cust_id'] > 0 ){
        $this->load->view('services/services_edit_skill_view',compact('sub_category','old_sub_cat'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function edit_skill()
    {
      // echo json_encode($_POST['sub_cat']); die();
      $cust = $this->cust;
      $skills = $this->input->post("skills");
      $skills_name = "";
      $skills_id = "";
      $today = date('Y-m-d H:i:s');
      foreach ($skills as $s ) {
        $cat_s = $this->api->get_category_by_ids($s);
        $skills_name .= $cat_s['cat_name'].",";
        $skills_id .= $s.",";
      }
      $skills_name =  mb_substr($skills_name, 0, -1);
      $skills_id =  mb_substr($skills_id, 0, -1);
      $previous_skills = $this->api->get_customer_skills($cust['cust_id']); 
      $edit_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'cs_id' => (int) $previous_skills['cs_id'],
        'confirmed_skills' => $skills_id,
        'confirmed_skills_name' => $skills_name
      );
      // echo json_encode($edit_data); die();
      if( $this->api->update_user_skills($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-services/skill');
    }
    
    public function get_skills_by_category_id()
    {
      $cat_id = $this->input->post("category_id");
      echo json_encode($this->api->get_skill_by_cat_id($cat_id));
    }
  //End User Skills----------------------------------------

  //Start User Portfolio-----------------------------------
    public function portfolio_list_view()
    {
      $portfolios = $this->api->get_customer_portfolio($this->cust['cust_id']);
      if($portfolios) {
        $this->load->view('services/services_portfolio_list_view', compact('portfolios'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function add_portfolio_view()
    {
      $category_types = $this->api->get_category_types();
      $this->load->view('services/services_add_portfolio_view', compact('category_types'));    
      $this->load->view('user_panel/footer');
    }
    public function add_portfolio()
    {
      $cust = $this->cust;
      $title = $this->input->post("title");
      $category_id = $this->input->post("category_id");
      $subcat_ids = $this->input->post("subcat_ids");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = "NULL"; }
      if( !empty($_FILES["portfolio"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('portfolio')) {   
          $uploads    = $this->upload->data();  
          $portfolio_url =  $config['upload_path'].$uploads["file_name"];  

          $add_data = array(
            'cust_id' => (int) $cust['cust_id'],
            'title' => $title, 
            'cat_id' => $category_id, 
            'subcat_ids' => $subcat_ids,
            'attachement_url' => $portfolio_url,
            'description' => $description,
            'tags' => "NULL",
            'cre_datetime' => $today,
          );
          
          if( $id = $this->api->register_portfolio($add_data)) {
            $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
          } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }
        }  else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));   }
      } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }
      redirect('user-panel-services/portfolio/add');
    }
    public function get_category_by_cat_type_id()
    {
      $cat_type_id = $this->input->post("cat_type_id");
      echo json_encode($this->api->get_category_by_cat_type_id($cat_type_id));
    }
    public function get_subcategories_by_category_id($value='')
    {
      $category_id = $this->input->post("category_id");
      echo json_encode($this->api->get_subcategory_by_category_id($category_id));
    }
    public function edit_portfolio_view()
    {
      $id = $this->uri->segment(3);
      if( $portfolio = $this->api->get_portfolio_detail($id)){
        $category_types = $this->api->get_category_types();
        $this->load->view('services/services_edit_portfolio_view',compact('portfolio','category_types'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function edit_portfolio()
    {
      $portfolio_id = $this->input->post("portfolio_id");
      $title = $this->input->post("title");
      $category_id = $this->input->post("category_id");
      $subcat_ids = $this->input->post("subcat_ids");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      
      $portfolio = $this->api->get_portfolio_detail($portfolio_id);

      if($category_id == NULL OR empty($category_id)) { 
        $category_id = $portfolio['cat_id']; 
        if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = "NULL"; }
      } else { if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = $portfolio['subcat_ids']; } }

      $old_attachement_url = $portfolio["attachement_url"];

      if( !empty($_FILES["portfolio"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('portfolio')) {   
          $uploads    = $this->upload->data();  
          $portfolio_url =  $config['upload_path'].$uploads["file_name"];  
          if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        } else { $portfolio_url = $old_attachement_url; }  
      } else { $portfolio_url = $old_attachement_url; }  
      
      $edit_data = array(
        'title' => $title, 
        'cat_id' => $category_id, 
        'subcat_ids' => $subcat_ids,
        'attachement_url' => $portfolio_url,
        'description' => nl2br(trim($description)),
      );
      
      if(  $this->api->update_portfolio($portfolio_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      redirect('user-panel-services/portfolios/'.$portfolio_id);
    }
    public function delete_portfolio()
    {
      $id = $this->input->post("id");
      $portfolio = $this->api->get_portfolio_detail($id);
      $old_attachement_url = $portfolio["attachement_url"];

      if( $id = $this->api->delete_portfolio($id)) { 
        if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
          echo "success"; 
      } else { echo "failed"; }
    }
  //End User Portfolio-------------------------------------

  //Start User Language------------------------------------
    public function get_languages_master()
    {
      $filter = $this->input->post('q');
      echo json_encode($this->api->get_language_masters($filter));
    }
    public function update_languages()
    {
      $udate_data = array(
        "cust_id" => $this->session->userdata('cust_id'),
        "lang_known" => $this->input->post('langs'),
      );
      if( $this->api->update_user_language($udate_data) ){ echo 'success'; }
      else { echo 'failed'; }
    }
  //End User Language--------------------------------------

  //Start User Overview------------------------------------
    public function update_overview()
    {
      $cust_id = $this->session->userdata('cust_id');
      $udate_data = [ "overview" => $this->input->post('overview')];
      if( $this->api->update_overview($cust_id, $udate_data) ){ echo 'success'; }
      else { echo 'failed'; }
    }
  //End User Overview--------------------------------------

  //Start User Document------------------------------------
    public function document_list_view()
    {
      $documents = $this->api->get_documents($this->cust['cust_id']);
      if($documents) {
        $this->load->view('services/services_document_list_view', compact('documents'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function add_document_view()
    {
      $this->load->view('services/services_add_document_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_document()
    {
      $cust = $this->cust;
      $title = $this->input->post("title");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      
      if( !empty($_FILES["document"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('document')) {   
          $uploads    = $this->upload->data();  
          $document_url =  $config['upload_path'].$uploads["file_name"];  
          $add_data = array(
            'cust_id' => (int) $cust['cust_id'],
            'doc_title' => $title, 
            'attachement_url' => $document_url,
            'doc_desc' => $description,
            'cre_datetime' => $today,
          );
          
          if( $id = $this->api->register_document($add_data)) {
            $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
          } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
        }  else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));  }
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-services/document/add');
    }
    public function edit_document_view()
    {
      $id = $this->uri->segment(3);
      if( $document = $this->api->get_document_detail($id)){
        $this->load->view('services/services_edit_document_view',compact('document'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-services/user-profile'); }
    }
    public function edit_document()
    {
      $document_id = $this->input->post("document_id");
      $title = $this->input->post("title");
      $description = $this->input->post("description");
      $document = $this->user->get_document_detail($document_id);
      $old_attachement_url = $document["attachement_url"];
      if( !empty($_FILES["document"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('document')) {   
          $uploads    = $this->upload->data();  
          $document_url =  $config['upload_path'].$uploads["file_name"];  
          
          if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        } else { $document_url = $old_attachement_url; }  
      } else { $document_url = $old_attachement_url; }  
      
      $edit_data = array(
        'doc_title' => $title, 
        'attachement_url' => $document_url,
        'doc_desc' => trim(nl2br($description)),
      );
      if(  $this->api->update_document($document_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      redirect('user-panel-services/document/'.$document_id);
    }
    public function delete_document()
    {
      $id = $this->input->post("id");
      $document = $this->user->get_document_detail($id);
      $old_attachement_url = $document["attachement_url"];
      
      if( $this->api->delete_document($this->cust['cust_id'], $id)) { 
        if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        echo "success"; 
      } else { echo "failed"; }
    }
  //End User Document--------------------------------------

  //Start User Change Password-----------------------------
    public function change_password()
    {
      $this->load->view('services/services_change_password_view');   
      $this->load->view('user_panel/footer');
    }
    public function update_password()
    {
      $old_pass = $this->input->post('old_password');
      $new_pass = $this->input->post('new_password');
      if($this->api->validate_old_password($this->cust_id, $old_pass)){
        if($this->api->update_new_password(['cust_id' => $this->cust_id, 'password' => $new_pass])){
          $this->session->set_flashdata('success', $this->lang->line('password_changed'));          
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      } else { $this->session->set_flashdata('error', $this->lang->line('old_password_not_matched'));  }    
      redirect('user-panel-services/change-password');
    }
  //End User Change Password-------------------------------

  //Start Service Provider Profile-------------------------
    public function provider_profile()
    {
      $today = date("Y-m-d H:i:s");
      if($operator = $this->api->get_service_provider_profile($this->cust_id)) {
        $documents = $this->api->get_service_provider_documents($this->cust_id);
        $off_days = $this->api->get_service_provider_off_days($this->cust_id);
        //echo json_encode($documents); die();
        $this->load->view('services/service_provider_profile_view', compact('operator','documents','off_days'));
      } else if($details = $this->api->get_deliverer_profile($this->cust_id)) {
        $insert_data = array(
          "company_name" => trim($details['company_name']),
          "firstname" => trim($details['firstname']),
          "lastname" => trim($details['lastname']),
          "address" => trim($details['address']),
          "zipcode" => trim($details['zipcode']),
          "country_id" => (int)($details['country_id']),
          "state_id" => (int)($details['state_id']),
          "city_id" => (int)($details['city_id']),
          "email_id" => trim($details['email_id']),
          "contact_no" => trim($details['contact_no']),
          "avatar_url" => 'NULL',
          "cover_url" => 'NULL',
          "cust_id" => (int)($details['cust_id']),
          "cre_datetime" => $today,
          "introduction" => trim($details['introduction']),
          "shipping_mode" => 0,
          "latitude" => trim($details['latitude']),
          "longitude" => trim($details['longitude']),
          "gender" => trim($details['gender']),
          "service_list" => 'NULL',
          "dedicated_provider" => 0,
          "profile_url_login" => base_url('user-panel-services/provider-profile/'.md5($this->cust_id)),
          "profile_url" => base_url('user-profile/'.md5($this->cust_id)),
          "profile_code" => md5($this->cust_id),
          "working_days" => 'NULL',
          "off_days" => 0,
          "max_open_order" => 5,
          "proposal_credits" => 15,
          "current_subscription_expiry" => trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'))),
          "rate_per_hour" => 'NULL',
          "currency_code" => 'NULL',
          "response_time" => 'NULL',
          "work_location" => 'NULL',
          "street_name" => 'NULL',
        );
        //echo json_encode($insert_data); die();
        $this->api->register_service_provider_profile($insert_data);
        $documents = $this->api->get_service_provider_documents($this->cust_id);
        $operator = $this->api->get_service_provider_profile($this->cust_id);
        $this->load->view('services/service_provider_profile_view', compact('operator','documents'));
      } else {
        $cust = $this->cust;
        $countries = $this->api->get_countries($this->cust_id);
        $cust_id = $this->cust_id;
        $this->load->view('services/create_service_provider_profile_view', compact('cust_id','countries','cust'));
      }
      $this->load->view('user_panel/footer');
    }
    public function create_service_provider_profile()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $gender = $this->input->post('gender', TRUE);
      $company_name = $this->input->post('company_name', TRUE);
      $contact_no = $this->input->post('contact_no', TRUE);
      $email_id = $this->input->post('email_id', TRUE);
      $introduction = $this->input->post('introduction', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      if($state_id<=0) { $state_id = $this->input->post('state_id_hidden', TRUE); }
      $city_id = $this->input->post('city_id', TRUE);
      if($city_id<=0) { $city_id = $this->input->post('city_id_hidden', TRUE); }
      $zipcode = $this->input->post('zipcode', TRUE);
      $address = $this->input->post('address', TRUE);
      $street_name = $this->input->post('street_name', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      $today = date("Y-m-d H:i:s");
      
      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('avatar')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $avatar_url = "NULL"; }
      } else { $avatar_url = "NULL"; }

      if( !empty($_FILES["cover"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/cover-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('cover')) {   
          $uploads    = $this->upload->data();  
          $cover_url =  $config['upload_path'].$uploads["file_name"]; 
        } else{ $cover_url = "NULL"; }
      } else{ $cover_url = "NULL"; }

      $insert_data = array(
        "company_name" => trim($company_name),
        "firstname" => trim($firstname),
        "lastname" => trim($lastname),
        "address" => trim($address),
        "zipcode" => trim($zipcode),
        "country_id" => (int)($country_id),
        "state_id" => (int)($state_id),
        "city_id" => (int)($city_id),
        "email_id" => trim($email_id),
        "contact_no" => trim($contact_no),
        "avatar_url" => trim($avatar_url),
        "cover_url" => trim($cover_url),
        "cust_id" => (int)$cust_id,
        "cre_datetime" => $today,
        "introduction" => trim($introduction),
        "shipping_mode" => 0,
        "latitude" => trim($latitude),
        "longitude" => trim($longitude),
        "gender" => trim($gender),
        "service_list" => 'NULL',
        "dedicated_provider" => 0,
        "profile_url_login" => base_url('user-panel-services/provider-profile-view/'.md5($cust_id)),
        "profile_url" => base_url('user-profile-view/'.md5($cust_id)),
        "profile_code" => md5($cust_id),
        "working_days" => 'NULL',
        "off_days" => 0,
        "max_open_order" => 5,
        "proposal_credits" => 15,
        "current_subscription_expiry" => trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'))),
        "rate_per_hour" => 'NULL',
        "currency_code" => 'NULL',
        "response_time" => 'NULL',
        "work_location" => 'NULL',
        "street_name" => trim($street_name),
      );
      //echo json_encode($insert_data); die();
      if($provider_id = $this->api->register_service_provider_profile($insert_data)) {
        $this->api->make_consumer_deliverer($cust_id);
        $this->session->set_flashdata('success', $this->lang->line('profile_created_successfully!'));
      } else { $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found!')); }
      redirect('user-panel-services/provider-profile');
    }
    public function edit_provider_profile()
    {
      $operator = $this->api->get_service_provider_profile($this->cust_id);
      $countries = $this->api->get_countries($this->cust_id);
      $this->load->view('services/edit_provider_profile_view', compact('operator','countries'));
      $this->load->view('user_panel/footer');
    }
    public function update_provider_profile()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)){ 
        $cust_id = $this->cust_id;
        $firstname = $this->input->post('firstname', TRUE);
        $lastname = $this->input->post('lastname', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $company_name = $this->input->post('company_name', TRUE);
        $contact_no = $this->input->post('contact_no', TRUE);
        $email_id = $this->input->post('email_id', TRUE);
        $introduction = $this->input->post('introduction', TRUE);
        $country_id = $this->input->post('country_id', TRUE);
        $state_id = $this->input->post('state_id', TRUE);
        if($state_id<=0) { $state_id = $this->input->post('state_id_hidden', TRUE); }
        $city_id = $this->input->post('city_id', TRUE);
        if($city_id<=0) { $city_id = $this->input->post('city_id_hidden', TRUE); }
        $zipcode = $this->input->post('zipcode', TRUE);
        $address = $this->input->post('address', TRUE);
        $street_name = $this->input->post('street_name', TRUE);
        $latitude = $this->input->post('latitude', TRUE);
        $longitude = $this->input->post('longitude', TRUE);
        $today = date("Y-m-d H:i:s");

        $profile = $this->api->get_service_provider_profile($cust_id);
        if( !empty($_FILES["avatar"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('avatar')) {   
            $uploads    = $this->upload->data();  
            $avatar_url =  $config['upload_path'].$uploads["file_name"];  
            if( $profile['avatar_url'] != "NULL" ){ unlink($profile['avatar_url']); }
          }
          $update_data = array( "avatar_url" => trim($avatar_url) );
          $this->api->update_provider($update_data, $cust_id);
          $avatar_update = 1;
        } else $avatar_update = 0;

        if( !empty($_FILES["cover"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/cover-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('cover')) {   
            $uploads    = $this->upload->data();  
            $cover_url =  $config['upload_path'].$uploads["file_name"]; 
            if( $profile['cover_url'] != "NULL" ){  unlink($profile['cover_url']);  }
          }
          $update_data = array( "cover_url" => trim($cover_url) );
          $this->api->update_provider($update_data, $cust_id);
          $cover_update = 1;
        } else $cover_update = 0;

        $update_data = array(
          "company_name" => trim($company_name),
          "firstname" => trim($firstname),
          "lastname" => trim($lastname),
          "address" => trim($address),
          "zipcode" => trim($zipcode),
          "country_id" => (int)($country_id),
          "state_id" => (int)($state_id),
          "city_id" => (int)($city_id),
          "email_id" => trim($email_id),
          "contact_no" => trim($contact_no),
          "introduction" => trim($introduction),
          "latitude" => trim($latitude),
          "longitude" => trim($longitude),
          "gender" => trim($gender),
          "street_name" => trim($street_name),
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id) || $avatar_update || $cover_update ){
          $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        redirect('user-panel-services/edit-provider-profile');
      }
    }
    public function provider_profile_view()
    {
      if( !is_null($this->input->post('profile_code')) ) { $profile_code = $this->input->post('profile_code'); }
      else if(!is_null($this->uri->segment(3))) { $profile_code = $this->uri->segment(3); }
      else { redirect(base_url('user-panel-services/dashboard-services'),'refresh'); }
      $operator = $this->api->get_service_provider_profile_code($profile_code);
      $off_days = $this->api->get_service_provider_off_days((int)$operator['cust_id']);
      $user = $this->api->get_user_details((int)$operator['cust_id']);
      $skills = $this->api->get_customer_skills((int)$operator['cust_id']);
      $portfolios = $this->api->get_customers_portfolio((int)$operator['cust_id']);
      $offers = $this->api->get_service_provider_offers((int)$operator['cust_id']);
      $reviews = $this->api->get_service_provier_job_reviews((int)$operator['cust_id']);
      // echo json_encode($skills); die();
      $this->load->view('services/service_provider_profile_customer_view', compact('operator', 'off_days', 'user', 'skills', 'portfolios', 'offers', 'reviews'));
      $this->load->view('user_panel/footer');
    }
  //End Service Provider Profile---------------------------

  //Start Service Provider Documents-----------------------
    public function edit_provider_document()
    {
      $document = $this->api->get_service_provider_documents($this->cust_id);
      $this->load->view('services/edit_provider_document_view', compact('document'));
      $this->load->view('user_panel/footer');
    }
    public function add_provider_document()
    {
      //var_dump($_POST); die();
      $cust_id = $this->cust_id;
      $doc_type = $this->input->post('doc_type', TRUE);
      $provider_id = (int) $this->api->get_provider_id($this->cust_id);
      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('attachment')) {   
          $uploads    = $this->upload->data();  
          $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachement_url = "NULL"; }
      } else { $attachement_url = "NULL"; }

      $insert_data = array(
        "attachement_url" => trim($attachement_url), 
        "cust_id" => $cust_id, 
        "provider_id" => $provider_id,
        "is_verified" => "0", 
        "doc_type" => $doc_type, 
        "is_rejected" => "0",
        "upload_datetime" => date('Y-m-d H:i:s'),
        "verify_datetime" => "NULL",
      );  
      //var_dump($insert_data); die();
      if( $id = $this->api->register_operator_document($insert_data) ) {  
        $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
      } else {  $this->session->set_flashdata('error',$this->lang->line('upload_failed')); }
      redirect('user-panel-services/edit-provider-document');
    }
    public function provider_document_delete()
    {
      $doc_id = $this->input->post('id', TRUE);
      $documents = $this->api->get_old_operator_documents($doc_id);
      if($this->api->delete_operator_document($doc_id)) { 
        if($documents[0]['attachement_url'] != "NULL") {
          unlink($documents[0]['attachement_url']);
        } echo "success"; 
      } else { echo "failed"; } 
    }
  //End Service Provider Documents-------------------------
    
  //Start Service Provider Settings------------------------
    public function edit_provider_profile_settings()
    {
      $operator = $this->api->get_service_provider_profile($this->cust_id);
      $countries = $this->api->get_countries($this->cust_id);
      $currencies = $this->api->get_all_currencies();
      $off_days = $this->api->get_service_provider_off_days($this->cust_id);
      $subscriptions = $this->api->get_subscription_master((int)$_SESSION['admin_country_id']);

      if( !is_null($this->input->post('panel_active')) ) { $panel_active = (int) $this->input->post('panel_active'); }
      else if(!is_null($this->uri->segment(3))) { $panel_active = (int)$this->uri->segment(3); }
      else { $panel_active = 1; }
      if(!isset($panel_active)) $panel_active = 1;
      
      // echo json_encode($off_days); die();
      // echo $this->db->last_query(); die();

      $this->load->view('services/edit_provider_settings_view', compact('operator','countries','off_days','panel_active','currencies','subscriptions'));
      $this->load->view('user_panel/footer');
    }
    public function update_working_days()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
        $working_days = $this->input->post('working_days', TRUE);
        $working_days = implode(', ', $working_days);
        $update_data = array(
          "working_days" => trim($working_days),
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
    public function update_off_days()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
        $from_date = $this->input->post('from_date', TRUE);
        $to_date = $this->input->post('to_date', TRUE);
        $today = date('Y-m-d h:i:s');
        
        $add_data = array(
          "from_date" => trim($from_date),
          "to_date" => trim($to_date),
          "cre_datetime" => trim($today),
          "update_datetime" => trim($today),
          "cust_id" => trim($cust_id),
        );
        //json_encode($cover_update); die();
        if($this->api->register_off_day($add_data)){
          $update_data = array(
            "off_days" => 1,
          );
          $this->api->update_provider($update_data, $cust_id);
          $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
    public function delete_off_days()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $off_id = $this->input->post('off_id', TRUE); $off_id = (int)$off_id;
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
        
        if($this->api->delete_off_day($off_id)){
          $off_days = $this->api->get_service_provider_off_days($cust_id);
          if(sizeof($off_days) <= 0) {
            $update_data = array(
              "off_days" => 0,
            );
            $this->api->update_provider($update_data, $cust_id);
          }
          $this->session->set_flashdata('success', $this->lang->line('Deleted successfully.'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('Error while deleting record!')); }
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
    public function update_max_open_order()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
        $max_open_order = $this->input->post('max_open_order', TRUE);
        $update_data = array(
          "max_open_order" => trim($max_open_order),
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
    public function update_rate_hour()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
        $rate_per_hour = $this->input->post('rate_per_hour', TRUE);
        $daily_hours = $this->input->post('daily_hours', TRUE);
        $rate_per_hour_weekly = $this->input->post('rate_per_hour_weekly', TRUE);
        $weekly_hours = $this->input->post('weekly_hours', TRUE);
        $rate_per_hour_monthly = $this->input->post('rate_per_hour_monthly', TRUE);
        $monthly_hours = $this->input->post('monthly_hours', TRUE);
        $currency_code = $this->input->post('currency_code', TRUE);
        

        $update_data = array(
          "rate_per_hour" => trim($rate_per_hour),
          "daily_hours" => trim($daily_hours),
          "rate_per_hour_weekly" => trim($rate_per_hour_weekly),
          "weekly_hours" => trim($weekly_hours),
          "rate_per_hour_monthly" => trim($rate_per_hour_monthly),
          "monthly_hours" => trim($monthly_hours),
          "currency_code" => trim($currency_code),
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
    public function update_work_location()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
        $work_location = $this->input->post('work_location', TRUE);
        $update_data = array(
          "work_location" => trim($work_location),
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
    public function proposal_credit_payment()
    {
      if(empty($this->errors)) { 
        //echo json_encode($_SESSION); die();
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;        
        $subscription_id = $this->input->post('subscription_id', TRUE); $subscription_id = (int)$subscription_id;
        $sub_details = $this->api->get_subscription_master_details((int)$subscription_id);
        $this->load->view('services/proposal_credit_payment_view', compact('cust_id','panel_active','sub_details'));
        $this->load->view('user_panel/footer');
      }
    }
    public function proposal_credit_payment_stripe()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $subscription_id = $this->input->post('subscription_id', TRUE); $subscription_id = (int)$subscription_id;
        $payment_method = $this->input->post('payment_method', TRUE);
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $transaction_id = $this->input->post('stripeToken', TRUE);
        $stripeEmail = $this->input->post('stripeEmail', TRUE);
        $currency = $this->input->post('currency', TRUE);
        $amount_paid = $this->input->post('amount_paid', TRUE);
        $today = date('Y-m-d H:i:s');
        $sub_details = $this->api->get_subscription_master_details((int)$subscription_id);
        $user_details = $this->api->get_service_provider_profile(trim($cust_id));

        if( strtotime($user_details['current_subscription_expiry']) < strtotime(date('Y-m-d')) ) {
          $expiry = trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days')));
          $credit = $sub_details['proposal_credit'];
        } else { 
          $expiry = trim(date('Y-m-d', strtotime($user_details['current_subscription_expiry']. ' + 30 days'))); 
          $credit = (($user_details['proposal_credits']) + ($sub_details['proposal_credit']));
        }

        $update_data = array(
          "proposal_credits" => $credit,
          "current_subscription_type" => trim($sub_details['subscription_title']),
          "current_subscription_expiry" => $expiry,
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)) {

          //Add Payment to Provider account-----------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            }
            $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($amount_paid),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($currency),
              "cat_id" => 0
            );
            $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
            //Smp add Payment to Provider account-----------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              }
              $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
              $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
            //Smp add Payment to Provider account-----------------------------------

          //------------------------------------------------------------------------

          //deduct Payment from customer account------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            }
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($amount_paid),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($currency),
              "cat_id" => 0
            );
            $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
            //Smp deduct Payment from customer account------------------------------------
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
              $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
            //Smp deduct Payment from customer account------------------------------------

          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($amount_paid);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'profile_subscription',
              "amount" => trim($amount_paid),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($currency),
              "country_id" => trim($user_details['country_id']),
              "cat_id" => 0,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------

          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }

          /* Stripe payment invoice---------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($currency),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($subscription_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($user_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($user_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($user_details['city_id']));
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($user_details['email_id'])));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($user_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
            $invoice->setTo(array($gonagoo_address['company_name'],trim($gonagoo_city['city_name']),'-',trim($gonagoo_country['country_name']),trim($gonagoo_address['email'])));

            /* Adding Items in table */
            $order_for = $this->lang->line('Profile subscription'). ' - ' .$this->lang->line('Payment');
            $rate = round(trim($amount_paid),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'), $total);
            $invoice->addTotal($this->lang->line('taxes'), '0');
            $invoice->addTotal($this->lang->line('invoice_total'), $total, true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Subscription Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($subscription_id.'_profile_subscription.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $subscription_id.'_profile_subscription.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/subscription-invoices/".$pdf_name);
            //Update Order Invoice
            $update_data = array(
              "subscription_invoice_url" => "subscription-invoices/".$pdf_name,
            );
            $this->api->update_provider($update_data, $cust_id);
          /* -------------------------------------------------------------------- */

          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your profile subscription payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Profile Subscription Payment Invoice');
            $emailAddress = trim($user_details['email_id']);
            $fileToAttach = "resources/subscription-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //update to subscription purchase history
          $insert_data_purchase_history = array(
            "subscription_id" => $subscription_id,
            "cust_id" => $cust_id,
            "purchase_date" => $today,
            "price" => $amount_paid,
            "currency_id" => $sub_details['currency_id'],
            "currency_code" => $sub_details['currency_code'],
            "country_id" => $sub_details['country_id'],
            "ah_id" => $ah_id,
          );
          $this->api->insert_freelancer_sub_purchase_history($insert_data_purchase_history);

          //uodate to proposal credit history
          $insert_data_proposal_credit_history = array(
            "cust_id" => $cust_id,
            "type" => 'add',
            "quantity" => $sub_details['proposal_credit'],
            "balance" => ($sub_details['proposal_credit']) + ($user_details['proposal_credits']),
            "source_id" => $subscription_id,
            "source_type" => 'proposal',
          );
          $this->api->insert_freelancer_proposal_credit_history($insert_data_proposal_credit_history);

          //SMS to customer
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Payment completed for profile subscription. Subscription ID').': '.$subscription_id;
          $this->api_sms->sendSMS((int)$country_code.trim($user_details['contact_no']), $message);

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Profile subscription payment'), 'type' => 'subscription-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Profile subscription payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->session->set_flashdata('success', $this->lang->line('Profile subscription completed successfully.'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('Error! unable to complete profile subscription.')); }
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
    public function proposal_credit_payment_mtn()
    {
      //echo json_encode($_POST); die();
      $subscription_id = $this->input->post('subscription_id', TRUE); $subscription_id = (int)$subscription_id;
      $phone_no = $this->input->post('phone_no', TRUE);
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      $payment_method = $this->input->post('payment_method', TRUE);
      $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
      $currency = $this->input->post('currency', TRUE);
      $amount_paid = $this->input->post('amount_paid', TRUE);
      $today = date('Y-m-d H:i:s');
      $sub_details = $this->api->get_subscription_master_details((int)$subscription_id);
      $user_details = $this->api->get_service_provider_profile(trim($cust_id));
      
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$amount_paid."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      //$contents = curl_exec($ch);
      //$mtn_pay_res = json_decode($contents, TRUE);
      //var_dump($mtn_pay_res); die();
      
      $transaction_id = '1234567890';
      //$transaction_id = trim($mtn_pay_res['TransactionID']);
      
      $payment_data = array();
      // if($mtn_pay_res['StatusCode'] === "01"){
      if(1) {

        if( strtotime($user_details['current_subscription_expiry']) < strtotime(date('Y-m-d')) ) {
          $expiry = trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days')));
          $credit = $sub_details['proposal_credit'];
        } else { 
          $expiry = trim(date('Y-m-d', strtotime($user_details['current_subscription_expiry']. ' + 30 days'))); 
          $credit = (($user_details['proposal_credits']) + ($sub_details['proposal_credit']));
        }

        $update_data = array(
          "proposal_credits" => $credit,
          "current_subscription_type" => trim($sub_details['subscription_title']),
          "current_subscription_expiry" => $expiry,
        );
        //json_encode($update_data); die();
        if($this->api->update_provider($update_data, $cust_id)) {

          //Add Payment to Provider account-----------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            }

            $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($amount_paid),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($currency),
              "cat_id" => 0
            );
            $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
            //Smp add Payment to Provider account-----------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              }
              $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
              $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
            //Smp add Payment to Provider account-----------------------------------
          //------------------------------------------------------------------------

          //deduct Payment from customer account------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            }
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($amount_paid),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($currency),
              "cat_id" => 0
            );
            $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
            //Smp deduct Payment from customer account------------------------------------
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
              $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
            //Smp deduct Payment from customer account------------------------------------
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($amount_paid);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'profile_subscription',
              "amount" => trim($amount_paid),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($currency),
              "country_id" => trim($user_details['country_id']),
              "cat_id" => 0,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------

          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }

          /* Stripe payment invoice---------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($currency),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($subscription_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($user_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($user_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($user_details['city_id']));
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($user_details['email_id'])));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($user_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
            $invoice->setTo(array($gonagoo_address['company_name'],trim($gonagoo_city['city_name']),'-',trim($gonagoo_country['country_name']),trim($gonagoo_address['email'])));

            /* Adding Items in table */
            $order_for = $this->lang->line('Profile subscription'). ' - ' .$this->lang->line('Payment');
            $rate = round(trim($amount_paid),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'), $total);
            $invoice->addTotal($this->lang->line('taxes'), '0');
            $invoice->addTotal($this->lang->line('invoice_total'), $total, true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Subscription Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($subscription_id.'_profile_subscription.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $subscription_id.'_profile_subscription.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/subscription-invoices/".$pdf_name);
            //Update Order Invoice
            $update_data = array(
              "subscription_invoice_url" => "subscription-invoices/".$pdf_name,
            );
            $this->api->update_provider($update_data, $cust_id);
          /* -------------------------------------------------------------------- */

          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your profile subscription payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Profile Subscription Payment Invoice');
            $emailAddress = trim($user_details['email_id']);
            $fileToAttach = "resources/subscription-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //update to subscription purchase history
          $insert_data_purchase_history = array(
            "subscription_id" => $subscription_id,
            "cust_id" => $cust_id,
            "purchase_date" => $today,
            "price" => $amount_paid,
            "currency_id" => $sub_details['currency_id'],
            "currency_code" => $sub_details['currency_code'],
            "country_id" => $sub_details['country_id'],
            "ah_id" => $ah_id,
          );
          $this->api->insert_freelancer_sub_purchase_history($insert_data_purchase_history);

          //uodate to proposal credit history
          $insert_data_proposal_credit_history = array(
            "cust_id" => $cust_id,
            "type" => 'add',
            "quantity" => $sub_details['proposal_credit'],
            "balance" => ($sub_details['proposal_credit']) + ($user_details['proposal_credits']),
            "source_id" => $subscription_id,
            "source_type" => 'proposal',
          );
          $this->api->insert_freelancer_proposal_credit_history($insert_data_proposal_credit_history);

          //SMS to customer
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Payment completed for profile subscription. Subscription ID').': '.$subscription_id;
          $this->api_sms->sendSMS((int)$country_code.trim($user_details['contact_no']), $message);

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Profile subscription payment'), 'type' => 'subscription-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Profile subscription payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->session->set_flashdata('success', $this->lang->line('Profile subscription completed successfully.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('Error! unable to complete profile subscription.')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('Error! unable to complete profile subscription.')); }
      redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
    }
    public function proposal_credit_payment_orange()
    {
      //echo json_encode($_POST); die();
      $subscription_id = $this->input->post('subscription_id', TRUE); $subscription_id = (int)$subscription_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
      $currency = $this->input->post('currency', TRUE);
      $amount_paid = $this->input->post('amount_paid', TRUE);
      $subscription_payment_id = 'gonagoo_profile'.time().'_'.$subscription_id;

      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      //echo json_encode($token_details); die();
      $token = 'Bearer '.$token_details['access_token'];

      $lang = $this->session->userdata('language');
      if($lang=='french') $lang = 'fr';
      else $lang = 'en';

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "merchant_key" => "a8f8c61e", 
                            "currency" => "OUV", 
                            "order_id" => $subscription_payment_id, 
                            "amount" => $amount_paid,  
                            "return_url" => base_url('user-panel-services/proposal-credit-payment-orange-process'), 
                            "cancel_url" => base_url('user-panel-services/proposal-credit-payment-orange-process'), 
                            "notif_url" => base_url(), 
                            "lang" => $lang, 
                            "reference" => $this->lang->line('Profile subscription payment') 
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: ".$token,
        "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      //echo json_encode($url_details); die();
 
      if(isset($url_details) && $url_details['message'] == 'OK' && $url_details['status'] == 201) {
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('subscription_id', $subscription_id);
        $this->session->set_userdata('subscription_payment_id', $subscription_payment_id);
        $this->session->set_userdata('amount', $amount_paid);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
    public function proposal_credit_payment_orange_process()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $session_token = $_SESSION['notif_token'];
      $subscription_id = $_SESSION['subscription_id'];
      $sub_details = $this->api->get_subscription_master_details((int)$subscription_id);
      $subscription_payment_id = $_SESSION['subscription_payment_id'];
      $amount = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      $panel_active = 4;
      $currency = $sub_details['currency_code'];
      $amount_paid = $amount;
      $user_details = $this->api->get_service_provider_profile(trim($cust_id));

      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "booking_id" => $subscription_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      //$transaction_id = $payment_details['txnid'];
      $transaction_id = '1234567890';
      //echo json_encode($payment_details); die();

      // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
      if(1) {

        if( strtotime($user_details['current_subscription_expiry']) < strtotime(date('Y-m-d')) ) {
          $expiry = trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days')));
          $credit = $sub_details['proposal_credit'];
        } else { 
          $expiry = trim(date('Y-m-d', strtotime($user_details['current_subscription_expiry']. ' + 30 days'))); 
          $credit = (($user_details['proposal_credits']) + ($sub_details['proposal_credit']));
        }

        $update_data = array(
          "proposal_credits" => $credit,
          "current_subscription_type" => trim($sub_details['subscription_title']),
          "current_subscription_expiry" => $expiry,
        );
        //json_encode($update_data); die();
        if($this->api->update_provider($update_data, $cust_id)) {

          //Add Payment to Provider account-----------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            }

            $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($amount_paid),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($currency),
              "cat_id" => 0
            );
            $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
            //Smp add Payment to Provider account-----------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              }
              $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
              $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
            //Smp add Payment to Provider account-----------------------------------
          //------------------------------------------------------------------------

          //deduct Payment from customer account------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            }
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($amount_paid),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($currency),
              "cat_id" => 0
            );
            $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
            //Smp deduct Payment from customer account------------------------------------
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
              $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
            //Smp deduct Payment from customer account------------------------------------
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($amount_paid);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'profile_subscription',
              "amount" => trim($amount_paid),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($currency),
              "country_id" => trim($user_details['country_id']),
              "cat_id" => 0,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------

          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }

          /* Stripe payment invoice---------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($currency),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($subscription_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($user_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($user_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($user_details['city_id']));
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($user_details['email_id'])));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($user_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
            $invoice->setTo(array($gonagoo_address['company_name'],trim($gonagoo_city['city_name']),'-',trim($gonagoo_country['country_name']),trim($gonagoo_address['email'])));

            /* Adding Items in table */
            $order_for = $this->lang->line('Profile subscription'). ' - ' .$this->lang->line('Payment');
            $rate = round(trim($amount_paid),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'), $total);
            $invoice->addTotal($this->lang->line('taxes'), '0');
            $invoice->addTotal($this->lang->line('invoice_total'), $total, true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Subscription Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($subscription_id.'_profile_subscription.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $subscription_id.'_profile_subscription.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/subscription-invoices/".$pdf_name);
            //Update Order Invoice
            $update_data = array(
              "subscription_invoice_url" => "subscription-invoices/".$pdf_name,
            );
            $this->api->update_provider($update_data, $cust_id);
          /* -------------------------------------------------------------------- */

          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your profile subscription payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Profile Subscription Payment Invoice');
            $emailAddress = trim($user_details['email_id']);
            $fileToAttach = "resources/subscription-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //update to subscription purchase history
          $insert_data_purchase_history = array(
            "subscription_id" => $subscription_id,
            "cust_id" => $cust_id,
            "purchase_date" => $today,
            "price" => $amount_paid,
            "currency_id" => $sub_details['currency_id'],
            "currency_code" => $sub_details['currency_code'],
            "country_id" => $sub_details['country_id'],
            "ah_id" => $ah_id,
          );
          $this->api->insert_freelancer_sub_purchase_history($insert_data_purchase_history);

          //uodate to proposal credit history
          $insert_data_proposal_credit_history = array(
            "cust_id" => $cust_id,
            "type" => 'add',
            "quantity" => $sub_details['proposal_credit'],
            "balance" => ($sub_details['proposal_credit']) + ($user_details['proposal_credits']),
            "source_id" => $subscription_id,
            "source_type" => 'proposal',
          );
          $this->api->insert_freelancer_proposal_credit_history($insert_data_proposal_credit_history);

          //SMS to customer
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Payment completed for profile subscription. Subscription ID').': '.$subscription_id;
          $this->api_sms->sendSMS((int)$country_code.trim($user_details['contact_no']), $message);

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Profile subscription payment'), 'type' => 'subscription-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Profile subscription payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->session->set_flashdata('success', $this->lang->line('Profile subscription completed successfully.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('Error! unable to complete profile subscription.')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('Error! unable to complete profile subscription.')); }
      redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
    }
    public function update_social_media()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $panel_active = $this->input->post('panel_active', TRUE); $panel_active = (int)$panel_active;
        $linkedin_url = $this->input->post('linkedin_url', TRUE);
        if($linkedin_url=='') { $linkedin_url = 'NULL'; }
        $twitter_url = $this->input->post('twitter_url', TRUE);
        if($twitter_url=='') { $twitter_url = 'NULL'; }
        $fb_url = $this->input->post('fb_url', TRUE);
        if($fb_url=='') { $fb_url = 'NULL'; }
        $update_data = array(
          "linkedin_url" => trim($linkedin_url),
          "twitter_url" => trim($twitter_url),
          "fb_url" => trim($fb_url),
        );
        //echo json_encode($update_data); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
        } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        redirect('user-panel-services/edit-provider-profile-settings/'.$panel_active);
      }
    }
  //End Service Provider Settings--------------------------

  //Start Service Provider business photos-----------------
    public function business_photos()
    {
      $operator = $this->api->get_service_provider_profile($this->cust['cust_id']);
      //echo json_encode($operator); die();
      if($operator != null) {
        $photos = $this->api->get_service_provider_business_photos($this->cust_id);
        $this->load->view('services/services_provider_business_photos_view', compact('photos'));
        $this->load->view('user_panel/footer');
      } else { redirect(base_url('user-panel-services/provider-profile'),'refresh'); }
    }
    public function laundry_provider_business_photos_upload()
    {
      $cust_id = $this->cust_id;
      if( !empty($_FILES["business_photo"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/business-photos/'; 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';  
        $config['max_width']      = '0';  
        $config['max_height']     = '0';  
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('business_photo')) {   
          $uploads    = $this->upload->data();  
          $photo_url =  $config['upload_path'].$uploads["file_name"];  
        }      
        if( $photo_id = $this->api->add_service_provider_business_photo($photo_url, $cust_id)) {          
          $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_update')); }
      } 
      redirect('user-panel-services/business-photos');
    }
    public function laundry_provider_business_photos_delete()
    {
      $photo_id = $this->input->post('id', TRUE);
      if($this->api->delete_service_provider_business_photo($photo_id)){ echo 'success'; } else { echo "failed"; } 
    }
  //End Service Provider business photos-------------------

  //Start Service Provider Offers--------------------------
    public function provider_offers_list_view()
    {
      //echo json_encode($_POST); die();
      $operator = $this->api->get_service_provider_profile($this->cust['cust_id']);
      //echo json_encode($operator); die();
      if($operator != null) {
        $countries = $this->api->get_countries();
        $category_types = $this->api->get_category_types();
        $filter = $this->input->post('filter', TRUE);
        if(!isset($filter) && $filter == '') { 
          $filter = 'basic';
          $offers = $this->api->get_service_provider_offers($this->cust['cust_id']);
          $this->load->view('services/service_provider_offers_list_view', compact('offers','operator','filter','countries','category_types'));
        } else {
          $country_id = $this->input->post('country_id', TRUE); $country_id = (int)$country_id;
          if(!isset($country_id) && $filter == '') { $country_id = 0; }
          $state_id = $this->input->post('state_id', TRUE); $state_id = (int)$state_id;
          if(!isset($state_id) && $filter == '') { $state_id = 0; }
          $city_id = $this->input->post('city_id', TRUE); $city_id = (int)$city_id;
          if(!isset($city_id) && $filter == '') { $city_id = 0; }
          $sort_by = $this->input->post('sort_by', TRUE);
          $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
          if(!isset($cat_type_id) && $filter == '') { $cat_type_id = 0; }
          $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
          if(!isset($cat_id) && $filter == '') { $cat_id = 0; }
          $delivered_in = $this->input->post('delivered_in', TRUE); $delivered_in = (int)$delivered_in;
          if(!isset($delivered_in) && $filter == '') { $delivered_in = 0; }
          $start_price = $this->input->post('start_price', TRUE); $start_price = (int)$start_price;
          if(!isset($start_price) && $filter == '') { $start_price = 0; }
          $end_price = $this->input->post('end_price', TRUE); $end_price = (int)$end_price;
          if(!isset($end_price) && $filter == '') { $end_price = 0; }
          $latest = $this->input->post('latest', TRUE);

          $active_status = $this->input->post('active_status', TRUE);
          $running_status = $this->input->post('running_status', TRUE);

          if(isset($country_id) && $country_id > 0) { $states = $this->api->get_country_states($country_id); } else { $states = array(); }
          if(isset($city_id) && $city_id > 0) { $cities = $this->api->get_state_cities($state_id); } else { $cities = array(); }       
          if(isset($cat_type_id) && $cat_type_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }       

          $search_data = array(
            "cust_id" => (int)trim($this->cust['cust_id']),
            "country_id" => trim($country_id),
            "state_id" => trim($state_id),
            "city_id" => trim($city_id),
            "sort_by" => trim($sort_by),
            "cat_type_id" => trim($cat_type_id),
            "cat_id" => trim($cat_id),
            "delivered_in" => trim($delivered_in),
            "start_price" => trim($start_price),
            "end_price" => trim($end_price),
            "latest" => trim($latest),
            "active_status" => trim($active_status),
            "running_status" => trim($running_status),
            "device" => 'web',
            "last_id" => 0,
            "offer_pause_resume" => 'all',
          );
          $offers = $this->api->get_service_provider_offers_filtered($search_data);
          //echo json_encode($this->db->last_query()); die();
          //echo json_encode($offers); die();
          $this->load->view('services/service_provider_offers_list_view', compact('offers','operator','filter','countries','category_types','country_id','state_id','city_id','sort_by','cat_type_id','cat_id','delivered_in','start_price','end_price','latest','active_status','running_status','states','cities','categories'));
        }
        //echo json_encode($this->db->last_query()); die();
        $this->load->view('user_panel/footer');
      } else { redirect(base_url('user-panel-services/provider-profile'),'refresh'); }
    }
    public function get_categories_by_type()
    {
      $type_id = $this->input->post('type_id', TRUE);
      $categories = $this->api->get_categories_by_type($type_id);
      echo json_encode($categories);
    }
    public function provider_pause_offer()
    {
      $offer_id = $this->input->post('id'); $offer_id = (int) $offer_id;
      $update_data = array(
        "offer_pause_resume" => 0,
        "update_datetime" => date('Y-m-d H:i:s')
      );
      if( $this->api->update_offer_details($offer_id, $update_data) ) { echo 'success'; }
      else { echo 'failed'; }
    }
    public function provider_resume_offer()
    {
      $offer_id = $this->input->post('id'); $offer_id = (int) $offer_id;
      $update_data = array(
        "offer_pause_resume" => 1,
        "update_datetime" => date('Y-m-d H:i:s')
      );
      if( $this->api->update_offer_details($offer_id, $update_data) ) { echo 'success'; }
      else { echo 'failed'; }
    }
    public function provider_delete_offer()
    {
      $offer_id = $this->input->post('id'); $offer_id = (int) $offer_id;
      $update_data = array(
        "offer_status" => 0,
        "update_datetime" => date('Y-m-d H:i:s')
      );
      if( $this->api->update_offer_details($offer_id, $update_data) ) { echo 'success'; }
      else { echo 'failed'; }
    }
    public function provider_add_offers()
    {
      $cust = $this->cust;
      $countries = $this->api->get_countries($this->cust_id);
      $currency = $this->api->get_country_currency_detail((int)$cust['country_id'])['currency_sign'];
      $category_types = $this->api->get_category_types();
      $cust_id = $this->cust_id;

      if($cust['country_id'] != 'NULL' && $cust['country_id'] > 0) { $states = $this->api->get_country_states($cust['country_id']); } else { $states = array(); }
      if($cust['state_id'] != 'NULL' && $cust['state_id'] > 0) { $cities = $this->api->get_state_cities($cust['state_id']); } else { $cities = array(); }

      $this->load->view('services/provider_add_offers_view', compact('cust_id','countries','cust','currency','category_types','states','cities'));
      $this->load->view('user_panel/footer');
    }
    public function get_offer_tags()
    {
      $filter = $this->input->post('q');
      echo json_encode($this->api->get_offer_tags_masters($filter));
    }
    public function provider_add_offer_details()
    {
      //echo json_encode($_POST); die();
      //echo json_encode($_FILES); //die();
      $cust = $this->cust;
      $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
      $offer_title = $this->input->post('offer_title');
      $offer_price = $this->input->post('offer_price');
      $delivered_in = $this->input->post('delivered_in'); $delivered_in = (int)$delivered_in;
      $cat_type_id = $this->input->post('cat_type_id'); $cat_type_id = (int)$cat_type_id;
      $cat_id = $this->input->post('cat_id'); $cat_id = (int)$cat_id;
      $tags = $this->input->post('tags');
      $offer_desc = $this->input->post('off_desc');
      $offer_req = $this->input->post('offer_req');
      $location_type = $this->input->post('location_type');
      $today = date('Y-m-d H:i:s');
      if($location_type == 'on_site') {
        $location_type = 1;
        $country_id = $this->input->post('country_id');
        $state_id = $this->input->post('state_id');
        $city_id = $this->input->post('city_id');
      } else {
        $location_type = 0;
        $country_id = (int)$cust['country_id'];
        $state_id = 0;
        $city_id = 0;
      }
      $no_of_add_ons = $this->input->post('no_of_add_ons'); $no_of_add_ons = (int)$no_of_add_ons;
      if($no_of_add_ons <= 1) { 
        $addon_title = $this->input->post("addon_title_1");
        $addon_price = $this->input->post("addon_price_1");
        $work_day = $this->input->post("work_day_1");
        if($addon_title != '' && $addon_price > 0 && $work_day > 0) {
          $add_on_status = 1;
        } else { $add_on_status = 0; }
      } else { $add_on_status = 1; }

      $quick_delivery = $this->input->post('quick_delivery');
      if(isset($quick_delivery) && $quick_delivery == 'yes') {
        $quick_delivery_price = $this->input->post('quick_delivery_price');
        $quick_delivery_day = $this->input->post('quick_delivery_day');
        $quick_delivery = 1;
      } else {
        $quick_delivery_price = 0;
        $quick_delivery_day = 0;
        $quick_delivery = 0;
      }

      $currency_code = $this->api->get_country_currency_detail((int)$cust['country_id'])['currency_sign'];
      $currency_id = $this->api->get_country_currency_detail((int)$cust['country_id'])['currency_id'];
      
      $files = $_FILES;
      $cpt = count($_FILES['image_url']['name']); //die();
      for($i=0; $i <= $cpt; $i++) {
        if(isset($files['image_url']['name'][$i])) {
          $_FILES['image_url']['name']= $files['image_url']['name'][$i];
          $_FILES['image_url']['type']= $files['image_url']['type'][$i];
          $_FILES['image_url']['tmp_name']= $files['image_url']['tmp_name'][$i];
          $_FILES['image_url']['error']= $files['image_url']['error'][$i];
          $_FILES['image_url']['size']= $files['image_url']['size'][$i];

          $config['upload_path']    = 'resources/m4-image/offers/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 

          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url')) {
            $uploads    = $this->upload->data();

            /*$config['image_library'] = 'gd2';
            $config['source_image'] = $uploads['full_path'];
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = TRUE;
            $config['width']         = 600;
            $config['height']       = 450;
            $config['overwrite'] = TRUE;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('image_url');
            $uploads    = $this->upload->data();*/

            ${"image_url_$i"} =  $config['upload_path'].$uploads['file_name'];
          } else { ${"image_url_$i"} = 'NULL'; }
        } else { ${"image_url_$i"} = 'NULL'; }
      }
      $cpt++;
      for ($i=$cpt; $i <= 5; $i++) { 
        ${"image_url_$i"} = 'NULL';
      }
      /*echo '1='.$image_url_1.'<br />'; echo '2='.$image_url_2.'<br />'; echo '3='.$image_url_3.'<br />'; echo '4='.$image_url_4.'<br />'; echo '5='.$image_url_5.'<br />'; echo '6='.$image_url_0.'<br />'; die();*/

      $insert_data = array(
        "cust_id" => $cust_id,
        "cust_country_id" => (int)$cust['country_id'],
        "offer_title" => trim($offer_title),
        "offer_desc" => trim($offer_desc),
        "offer_req" => trim($offer_req),
        "delivered_in" => $delivered_in,
        "default_image_url" => $image_url_0,
        "image_url_1" => $image_url_1,
        "image_url_2" => $image_url_2,
        "image_url_3" => $image_url_3,
        "image_url_4" => $image_url_4,
        "image_url_5" => $image_url_5,
        "add_on_status" => $add_on_status,
        "quick_delivery" => $quick_delivery,
        "quick_delivery_day" => $quick_delivery_day,
        "quick_delivery_price" => $quick_delivery_price,
        "view_count" => 0,
        "sales_count" => 0,
        "favorite_counts" => 0,
        "offer_price" => trim($offer_price),
        "currency_id" => (int)$currency_id,
        "currency_code" => trim($currency_code),
        "cat_type_id" => (int)$cat_type_id,
        "cat_id" => (int)$cat_id,
        "rating_count" => 0,
        "review_count" => 0,
        "tags" => $tags,
        "location_type" => $location_type,
        "onsite_address" => 'NULL',
        "country_id" => $country_id,
        "state_id" => $state_id,
        "city_id" => $city_id,
        "cre_datetime" => $today,
        "update_datetime" => $today,
        "offer_url" => 'NULL',
        "offer_status" => 1,
        "admin_approval" => 0,
        "offer_pause_resume" => 1,
        "offer_code" => 'NULL',
      );
      //echo json_encode($insert_data); die();

      if($offer_id = $this->api->register_provider_offer_details($insert_data)) {
        //check for tags and insert new one
        $tags = explode(',', $tags);
        for ($i=0; $i < sizeof($tags); $i++) { 
          if(!$this->api->check_tag_exists(strtolower($tags[$i]))) {
            $insert_data_tag = array(
              "tag_name" => strtolower($tags[$i]),
              "status" => 1,
              "cre_datetime" => $today,
            );
            $this->api->register_offer_tag($insert_data_tag);
          }
        }

        //Update offer URL
        $update_data = array(
          "offer_url" => base_url('user-panel-services/provider-offer-details/'.md5($offer_id)),
          "offer_code" => md5($offer_id),
        );
        $this->api->update_offer_details($offer_id, $update_data);

        //insert Offer Add-ons
        if($add_on_status == 1) {
          for ($i=1; $i <= $no_of_add_ons; $i++) {
            $insert_add_ons = array(
              "offer_id" => $offer_id,
              "cust_id" => $cust_id,
              "cre_datetime" => $today,
              "addon_title" => $this->input->post("addon_title_$i"),
              "addon_price" => $this->input->post("addon_price_$i"),
              "country_id" => (int)$country_id,
              "currency_id" => (int)$currency_id,
              "currency_code" => trim($currency_code),
              "work_day" => $this->input->post("work_day_$i"),
              "status" => 1,
              "update_datetime" => $today,
            );
            $addon_id = $this->api->register_provider_offer_add_on_details($insert_add_ons);
          }
        }
        $this->session->set_flashdata('success', $this->lang->line('Offer added successfully.'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Unable to add offer details. Try again...'));  }
      redirect('user-panel-services/provider-offers');
    }
    public function provider_offer_details()
    {
      //echo json_encode($_POST); die();\
      if( !is_null($this->input->post('offer_code')) ) { $offer_code = $this->input->post('offer_code'); }
      else if(!is_null($this->uri->segment(3))) { $offer_code = $this->uri->segment(3); }
      else { redirect(base_url('user-panel-services/provider-offers'),'refresh'); }
      //echo $offer_code; die(); 
      $offer_details = $this->api->get_service_provider_offer_details_by_code($offer_code);
      $offer_reviews = $this->api->get_offer_reviews((int)$offer_details['offer_id']);
      //echo json_encode($offer_reviews); die();
      $operator = $this->api->get_service_provider_profile($this->cust['cust_id']);
      $search_data = array(
        "country_id" => $offer_details['country_id'],
        "cat_type_id" => $offer_details['cat_type_id'],
        "cat_id" => $offer_details['cat_id'],
      );
      $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);
      $this->load->view('services/service_provider_offer_details_view', compact('offer_details','operator','advance_payment','offer_reviews'));
      $this->load->view('user_panel/footer');
    }
    public function provider_edit_offer()
    {
      // echo json_encode($_POST); die();
      $offer_id = $this->input->post('offer_id'); $offer_id = (int)$offer_id;
      $offer_details = $this->api->get_service_provider_offer_details($offer_id);
      $add_ons = $this->api->get_offer_add_ons($offer_details['offer_id']);

      $cust = $this->cust;
      $countries = $this->api->get_countries($this->cust_id);
      $currency = $this->api->get_country_currency_detail((int)$cust['country_id'])['currency_sign'];
      $category_types = $this->api->get_category_types();
      $categories = $this->api->get_category_by_cat_type_id((int)$offer_details['cat_type_id']);
      $cust_id = $this->cust_id;

      if($cust['country_id'] != 'NULL' && $cust['country_id'] > 0) { $states = $this->api->get_country_states($cust['country_id']); } else { $states = array(); }
      if($cust['state_id'] != 'NULL' && $cust['state_id'] > 0) { $cities = $this->api->get_state_cities($cust['state_id']); } else { $cities = array(); }

      $this->load->view('services/provider_edit_offers_view', compact('cust_id','countries','cust','currency','category_types','states','cities','offer_details','categories','add_ons'));
      $this->load->view('user_panel/footer');
    }
    public function provider_delete_offer_add_on()
    {
      $addon_id = $this->input->post('addon_id'); $addon_id = (int) $addon_id;
      $offer_id = $this->api->get_offer_add_on_details($addon_id)['offer_id'];
      $update_data = array(
        "status" => 0,
        "update_datetime" => date('Y-m-d H:i:s')
      );
      if( $this->api->update_provider_offer_add_on_details($addon_id, $update_data) ) { 
        $offer_addons = $this->api->get_offer_add_ons((int)$offer_id);
        if(sizeof($offer_addons)==0) {
          $update_data = array(
            "add_on_status" => 0,
            "update_datetime" => date('Y-m-d H:i:s'),
            "is_edited" => 1,
          );
          $this->api->update_offer_details($offer_id, $update_data);
        }
        echo 'success'; 
      } else { echo 'failed'; }
    }
    public function provider_delete_offer_images()
    {
      $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
      $offer_details = $this->api->get_service_provider_offer_details($offer_id);
      $type = $this->input->post('type');
      if($type == 'default_image_url') {
        $update_data = array(
          "default_image_url" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['default_image_url'] != 'NULL') unlink($offer_details['default_image_url']);
      }
      if($type == 'image_url_1') {
        $update_data = array(
          "image_url_1" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_1'] != 'NULL') unlink($offer_details['image_url_1']);
      }
      if($type == 'image_url_2') {
        $update_data = array(
          "image_url_2" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_2'] != 'NULL') unlink($offer_details['image_url_2']);
      }
      if($type == 'image_url_3') {
        $update_data = array(
          "image_url_3" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_3'] != 'NULL') unlink($offer_details['image_url_3']);
      }
      if($type == 'image_url_4') {
        $update_data = array(
          "image_url_4" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_4'] != 'NULL') unlink($offer_details['image_url_4']);
      }
      if($type == 'image_url_5') {
        $update_data = array(
          "image_url_5" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_5'] != 'NULL') unlink($offer_details['image_url_5']);
      }
      if( $this->api->update_offer_details($offer_id, $update_data) ) { echo 'success'; }
      else { echo 'failed'; }
    }
    public function provider_add_offer_images()
    {
      $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
      $offer_details = $this->api->get_service_provider_offer_details($offer_id);
      $type = $this->input->post('type');

      if($type == 'default_image_url') {
        if(!empty($_FILES["default_image_url"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/m4-image/offers/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('default_image_url')) { 
            $uploads    = $this->upload->data();  
            $default_image_url =  $config['upload_path'].$uploads["file_name"];
            if($offer_details['default_image_url'] != "NULL") { unlink(trim($offer_details['default_image_url'])); }
          } else { $default_image_url = $offer_details['default_image_url']; }
        } else { $default_image_url = $offer_details['default_image_url']; }

        $update_data = array(
          "default_image_url" => $default_image_url,
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => 1,
        );
      }
      if($type == 'image_url_1') {
        if(!empty($_FILES["image_url_1"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/m4-image/offers/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url_1')) { 
            $uploads    = $this->upload->data();  
            $image_url_1 =  $config['upload_path'].$uploads["file_name"];
            if($offer_details['image_url_1'] != "NULL") { unlink(trim($offer_details['image_url_1'])); }
          } else { $image_url_1 = $offer_details['image_url_1']; }
        } else { $image_url_1 = $offer_details['image_url_1']; }

        $update_data = array(
          "image_url_1" => $image_url_1,
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => 1,
        );
      }
      if($type == 'image_url_2') {
        if(!empty($_FILES["image_url_2"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/m4-image/offers/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url_2')) { 
            $uploads    = $this->upload->data();  
            $image_url_2 =  $config['upload_path'].$uploads["file_name"];
            if($offer_details['image_url_2'] != "NULL") { unlink(trim($offer_details['image_url_2'])); }
          } else { $image_url_2 = $offer_details['image_url_2']; }
        } else { $image_url_2 = $offer_details['image_url_2']; }

        $update_data = array(
          "image_url_2" => $image_url_2,
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => 1,
        );
      }
      if($type == 'image_url_3') {
        if(!empty($_FILES["image_url_3"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/m4-image/offers/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url_3')) { 
            $uploads    = $this->upload->data();  
            $image_url_3 =  $config['upload_path'].$uploads["file_name"];
            if($offer_details['image_url_3'] != "NULL") { unlink(trim($offer_details['image_url_3'])); }
          } else { $image_url_3 = $offer_details['image_url_3']; }
        } else { $image_url_3 = $offer_details['image_url_3']; }

        $update_data = array(
          "image_url_3" => $image_url_3,
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => 1,
        );
      }
      if($type == 'image_url_4') {
        if(!empty($_FILES["image_url_4"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/m4-image/offers/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url_4')) { 
            $uploads    = $this->upload->data();  
            $image_url_4 =  $config['upload_path'].$uploads["file_name"];
            if($offer_details['image_url_4'] != "NULL") { unlink(trim($offer_details['image_url_4'])); }
          } else { $image_url_4 = $offer_details['image_url_4']; }
        } else { $image_url_4 = $offer_details['image_url_4']; }

        $update_data = array(
          "image_url_4" => $image_url_4,
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => 1,
        );
      }
      if($type == 'image_url_5') {
        if(!empty($_FILES["image_url_5"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/m4-image/offers/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url_5')) { 
            $uploads    = $this->upload->data();  
            $image_url_5 =  $config['upload_path'].$uploads["file_name"];
            if($offer_details['image_url_5'] != "NULL") { unlink(trim($offer_details['image_url_5'])); }
          } else { $image_url_5 = $offer_details['image_url_5']; }
        } else { $image_url_5 = $offer_details['image_url_5']; }

        $update_data = array(
          "image_url_5" => $image_url_5,
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => 1,
        );
      }
      if( $this->api->update_offer_details($offer_id, $update_data) ) { echo json_encode('success'); }
      else { echo json_encode('failed'); }
    }
    public function provider_edit_offer_details()
    {
      // echo json_encode($_POST); die();
      //echo json_encode($_FILES); //die();
      $cust = $this->cust;
      $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
      $offer_id = $this->input->post('offer_id'); $offer_id = (int)$offer_id;
      $offer_title = $this->input->post('offer_title');
      $offer_price = $this->input->post('offer_price');
      $delivered_in = $this->input->post('delivered_in'); $delivered_in = (int)$delivered_in;
      $cat_type_id = $this->input->post('cat_type_id'); $cat_type_id = (int)$cat_type_id;
      $cat_id = $this->input->post('cat_id'); $cat_id = (int)$cat_id;
      $tags = $this->input->post('tags');
      $offer_desc = $this->input->post('off_desc');
      $offer_req = $this->input->post('offer_req');
      $location_type = $this->input->post('location_type');
      $today = date('Y-m-d H:i:s');
      if($location_type == 'on_site') {
        $location_type = 1;
        $country_id = $this->input->post('country_id');
        $state_id = $this->input->post('state_id');
        $city_id = $this->input->post('city_id');
      } else {
        $location_type = 0;
        $country_id = (int)$cust['country_id'];
        $state_id = 0;
        $city_id = 0;
      }

      $quick_delivery = $this->input->post('quick_delivery');
      if(isset($quick_delivery) && $quick_delivery == 'yes') {
        $quick_delivery_price = $this->input->post('quick_delivery_price');
        $quick_delivery_day = $this->input->post('quick_delivery_day');
        $quick_delivery = 1;
      } else {
        $quick_delivery_price = 0;
        $quick_delivery_day = 0;
        $quick_delivery = 0;
      }

      $currency_code = $this->api->get_country_currency_detail((int)$cust['country_id'])['currency_sign'];
      $currency_id = $this->api->get_country_currency_detail((int)$cust['country_id'])['currency_id'];

      $update_data = array(
        "offer_title" => trim($offer_title),
        "offer_desc" => trim($offer_desc),
        "offer_req" => trim($offer_req),
        "delivered_in" => $delivered_in,
        /*"add_on_status" => $add_on_status,*/
        "quick_delivery" => $quick_delivery,
        "quick_delivery_day" => $quick_delivery_day,
        "quick_delivery_price" => $quick_delivery_price,
        "offer_price" => trim($offer_price),
        "cat_type_id" => (int)$cat_type_id,
        "cat_id" => (int)$cat_id,
        "tags" => $tags,
        "location_type" => $location_type,
        "country_id" => $country_id,
        "state_id" => $state_id,
        "city_id" => $city_id,
        "update_datetime" => $today,
        "is_edited" => 1,
      );
      //echo json_encode($update_data); die();

      if($this->api->update_offer_details($offer_id, $update_data)) {
        //check for tags and insert new one
        $tags = explode(',', $tags);
        for ($i=0; $i < sizeof($tags); $i++) { 
          if(!$this->api->check_tag_exists(strtolower($tags[$i]))) {
            $insert_data_tag = array(
              "tag_name" => strtolower($tags[$i]),
              "status" => 1,
              "update_datetime" => $today,
            );
            $this->api->register_offer_tag($insert_data_tag);
          }
        }

        //update existing add-ons changes
        $existing_addons_count = $this->input->post('existing_addons_count'); $existing_addons_count = (int)$existing_addons_count; //0
        if($existing_addons_count > 0) {
          for ($i=0; $i < $existing_addons_count; $i++) { 
            $addon_id = $this->input->post("addon_id_$i");
            $addon_title = $this->input->post("addon_title_$i");
            $addon_price = $this->input->post("addon_price_$i");
            $work_day = $this->input->post("work_day_$i");

            $update_data = array(
              "addon_title" => $addon_title,
              "addon_price" => $addon_price,
              "work_day" => $work_day,
              "update_datetime" => $today,
            );
            $this->api->update_provider_offer_add_on_details($addon_id, $update_data);
          }
        }

        //Insert New Add-ons
        $no_of_add_ons = $this->input->post('no_of_add_ons'); $no_of_add_ons = (int)$no_of_add_ons;  //1
        $new_add_on_counter = $existing_addons_count + 1; //1
        $no_of_new_add_ons = ($no_of_add_ons - $existing_addons_count); //1-0=1
        //die();

        if($no_of_new_add_ons > 1) {
          for ($i=$existing_addons_count; $i <= $no_of_add_ons; $i++) {
            $addon_title = $this->input->post("addon_title_$i");
            $addon_price = $this->input->post("addon_price_$i");
            $work_day = $this->input->post("work_day_$i");

            if($addon_title != '' && $addon_price > 0 && $work_day > 0) {
              $insert_add_ons = array(
                "offer_id" => $offer_id,
                "cust_id" => $cust_id,
                "cre_datetime" => $today,
                "addon_title" => $addon_title,
                "addon_price" => $addon_price,
                "country_id" => (int)$country_id,
                "currency_id" => (int)$currency_id,
                "currency_code" => trim($currency_code),
                "work_day" => $work_day,
                "status" => 1,
                "update_datetime" => $today,
              );
              $addon_id = $this->api->register_provider_offer_add_on_details($insert_add_ons);
            }
          }
          //update add_on_status
          $update_data = array(
            "add_on_status" => 1,
            "update_datetime" => $today,
            "is_edited" => 1,
          );
          $this->api->update_offer_details($offer_id, $update_data);
        } else {
          $no_of_add_ons--;
          $addon_id = $this->input->post("addon_id_$no_of_add_ons");
          $addon_title = $this->input->post("addon_title_$no_of_add_ons");
          $addon_price = $this->input->post("addon_price_$no_of_add_ons");
          $work_day = $this->input->post("work_day_$no_of_add_ons");
          if((!isset($addon_id) || $addon_id <= 0) && ($addon_title != '' && $addon_price > 0 && $work_day > 0)) {
            /*if($no_of_new_add_ons == 1 && ) {*/
              $insert_add_ons = array(
                "offer_id" => $offer_id,
                "cust_id" => $cust_id,
                "cre_datetime" => $today,
                "addon_title" => $addon_title,
                "addon_price" => $addon_price,
                "country_id" => (int)$country_id,
                "currency_id" => (int)$currency_id,
                "currency_code" => trim($currency_code),
                "work_day" => $work_day,
                "status" => 1,
                "update_datetime" => $today,
              );
              $addon_id = $this->api->register_provider_offer_add_on_details($insert_add_ons);
              //update add_on_status
              $update_data = array(
                "add_on_status" => 1,
                "update_datetime" => $today,
                "is_edited" => 1,
              );
              $this->api->update_offer_details($offer_id, $update_data);
            /*}*/
          }
        }

        $this->session->set_flashdata('success', $this->lang->line('Offer details updated successfully.'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Unable to update offer details. Try again...'));  }
      redirect('user-panel-services/provider-offers');
    }
  //End Service Provider Offers----------------------------
  
  //Start Customer Offers Search and Buy-------------------  
    public function available_offers_list_view()
    {
      //echo json_encode($_POST); die();
      $countries = $this->api->get_countries();
      $category_types = $this->api->get_category_types();
      $favorite_offers = $this->api->get_favorite_offers($this->cust['cust_id']);
      $favorite_offer_ids = '';
      foreach ($favorite_offers as $offer) {
        $favorite_offer_ids .= $offer['offer_id'].',';
      }
      //rtrim($favorite_offer_ids,',');
      $favorite_offer_ids = substr(trim($favorite_offer_ids), 0, -1);
      $favorite_offer_ids = explode(',', $favorite_offer_ids);
      //echo json_encode($favorite_offer_ids); die();

      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { 
        $filter = 'basic';
        $country_id = 0;
        $state_id = 0;
        $city_id = 0;
        $sort_by = 'NULL';
        $cat_type_id = 0;
        $cat_id = 0;
        $delivered_in = 0;
        $start_price = 0;
        $end_price = 0;
        $latest = 'NULL';
        $active_status = 'active';
        $running_status = 'resumed';

        $search_data = array(
          "cust_id" => 0,
          "country_id" => trim($country_id),
          "state_id" => trim($state_id),
          "city_id" => trim($city_id),
          "sort_by" => trim($sort_by),
          "cat_type_id" => trim($cat_type_id),
          "cat_id" => trim($cat_id),
          "delivered_in" => trim($delivered_in),
          "start_price" => trim($start_price),
          "end_price" => trim($end_price),
          "latest" => trim($latest),
          "active_status" => trim($active_status),
          "running_status" => trim($running_status),
          "device" => 'web',
          "last_id" => 0,
          "offer_pause_resume" => 'all',
        );
        $offers = $this->api->get_service_provider_offers_filtered($search_data);

        $this->load->view('services/available_offers_list_view', compact('offers','filter','countries','category_types','favorite_offers','favorite_offer_ids'));
      } else {
        $country_id = $this->input->get('country_id', TRUE); $country_id = (int)$country_id;
        if(!isset($country_id) && $filter == '') { $country_id = 0; }
        $state_id = $this->input->get('state_id', TRUE); $state_id = (int)$state_id;
        if(!isset($state_id) && $filter == '') { $state_id = 0; }
        $city_id = $this->input->get('city_id', TRUE); $city_id = (int)$city_id;
        if(!isset($city_id) && $filter == '') { $city_id = 0; }
        $sort_by = $this->input->get('sort_by', TRUE);
        $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
        if(!isset($cat_type_id) && $filter == '') { $cat_type_id = 0; }
        $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
        if(!isset($cat_id) && $filter == '') { $cat_id = 0; }
        $delivered_in = $this->input->get('delivered_in', TRUE); $delivered_in = (int)$delivered_in;
        if(!isset($delivered_in) && $filter == '') { $delivered_in = 0; }
        $start_price = $this->input->get('start_price', TRUE); $start_price = (int)$start_price;
        if(!isset($start_price) && $filter == '') { $start_price = 0; }
        $end_price = $this->input->get('end_price', TRUE); $end_price = (int)$end_price;
        if(!isset($end_price) && $filter == '') { $end_price = 0; }
        $latest = $this->input->get('latest', TRUE);

        $active_status = 'active';
        $running_status = 'resumed';

        if(isset($country_id) && $country_id > 0) { $states = $this->api->get_country_states($country_id); } else { $states = array(); }
        if(isset($city_id) && $city_id > 0) { $cities = $this->api->get_state_cities($state_id); } else { $cities = array(); }       
        if(isset($cat_type_id) && $cat_type_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }       

        $search_data = array(
          "cust_id" => 0,
          "country_id" => trim($country_id),
          "state_id" => trim($state_id),
          "city_id" => trim($city_id),
          "sort_by" => trim($sort_by),
          "cat_type_id" => trim($cat_type_id),
          "cat_id" => trim($cat_id),
          "delivered_in" => trim($delivered_in),
          "start_price" => trim($start_price),
          "end_price" => trim($end_price),
          "latest" => trim($latest),
          "active_status" => trim($active_status),
          "running_status" => trim($running_status),
          "device" => 'web',
          "last_id" => 0,
          "offer_pause_resume" => 'all',
        );
        $offers = $this->api->get_service_provider_offers_filtered($search_data);
        //echo json_encode($this->db->last_query()); die();
        //echo json_encode($offers); die();
        $this->load->view('services/available_offers_list_view', compact('offers','filter','countries','category_types','country_id','state_id','city_id','sort_by','cat_type_id','cat_id','delivered_in','start_price','end_price','latest','active_status','running_status','states','cities','categories','favorite_offers','favorite_offer_ids'));
      }
      //echo json_encode($this->db->last_query()); die();
      $this->load->view('user_panel/footer');
    }
    public function customer_offer_details()
    {
      //echo json_encode($_POST); die();
      if( !is_null($this->input->post('offer_id')) ) { $offer_id = $this->input->post('offer_id'); }
      else if(!is_null($this->uri->segment(3))) { $offer_id = $this->uri->segment(3); }
      else { redirect(base_url('user-panel-services/available-offers-list-view'),'refresh'); }
      //echo $offer_id; die(); 
      $offer_details = $this->api->get_service_provider_offer_details($offer_id);
      $offer_reviews = $this->api->get_offer_reviews((int)$offer_id);
      //echo json_encode($offer_reviews); die();

      $favorite_offers = $this->api->get_favorite_offers($this->cust['cust_id']);
      $favorite_offer_ids = '';
      foreach ($favorite_offers as $offer) {
        $favorite_offer_ids .= $offer['offer_id'].',';
      }
      //rtrim($favorite_offer_ids,',');
      $favorite_offer_ids = substr(trim($favorite_offer_ids), 0, -1);
      $favorite_offer_ids = explode(',', $favorite_offer_ids);
      //echo json_encode($favorite_offer_ids); die();

      $search_data = array(
        "country_id" => $offer_details['country_id'],
        "cat_type_id" => $offer_details['cat_type_id'],
        "cat_id" => $offer_details['cat_id'],
      );
      $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);

      //update offer favorite count
      if($this->cust['cust_id'] != $offer_details['cust_id']) {
        $update_data = array( "view_count" => ($offer_details['view_count'] + 1) );
        $this->api->update_offer_details($offer_id, $update_data);
      }

      $this->load->view('services/offer_details_view', compact('offer_details','advance_payment','offer_reviews','favorite_offer_ids'));
      $this->load->view('user_panel/footer');
    }
    public function buy_offer()
    {
      // echo json_encode($_POST); die();
      $offer_id = $this->input->post('offer_id'); $offer_id = (int)$offer_id;
      $offer_details = $this->api->get_service_provider_offer_details($offer_id);
      $cust_id = $this->cust['cust_id']; $cust_id = (int)$cust_id;
      $customer_details = $this->api->get_user_details($cust_id);
      $selected_add_on_ids = $this->input->post('selected_add_on_ids');
      $quick_delivery_base_price = $this->input->post('quick_delivery_base_price');
      $quick_delivery_selected_price = $this->input->post('quick_delivery_selected_price');
      if($quick_delivery_selected_price > 0) { $quick_delivery_base_price = $offer_details['quick_delivery_price'];
      } else { $quick_delivery_base_price = 0; }
      $offer_base_price = $this->input->post('offer_base_price');
      $quantity = $this->input->post('quantity');
      $provider_details = $this->api->get_service_provider_profile((int)$offer_details['cust_id']);
      $today = date('Y-m-d H:i:s');

      $search_data = array(
        "country_id" => $offer_details['country_id'],
        "cat_type_id" => $offer_details['cat_type_id'],
        "cat_id" => $offer_details['cat_id'],
      );
      // echo json_encode($search_data); die();
      if($advance_payment_details = $this->api->get_country_cat_advance_payment_details($search_data)) {
        //echo json_encode($this->db->last_query()); die();
        //echo json_encode($advance_payment_details); die();

        $add_on_price = 0;
        if($selected_add_on_ids != '') {
          $selected_add_on_ids = explode(',', $selected_add_on_ids);
          for ($i = 0; $i < sizeof($selected_add_on_ids); $i++) {
            $add_on_details = $this->api->get_offer_add_on_details($selected_add_on_ids[$i]);
            $add_on_price += $add_on_details['addon_price'];
          }
          $no_of_addons = sizeof($selected_add_on_ids);
        } else { $no_of_addons = 0; }

        $offer_price = round(($offer_details['offer_price']*$quantity)+($add_on_price*$quantity)+($quick_delivery_base_price*$quantity),2);
        //echo json_encode($advance_payment_details); die();
        $gonagoo_commission_amount = round((($offer_price/100) * $advance_payment_details['gonagoo_commission']),2);

        $insert_data = array(
          "job_title" => $offer_details['offer_title'],
          "cat_id" => (int)$offer_details['cat_type_id'],
          "sub_cat_id" => (int)$offer_details['cat_id'],
          "job_desc" => $offer_details['offer_desc'],
          "attachment_1" => 'NULL',
          "attachment_2" => 'NULL',
          "attachment_3" => 'NULL',
          "attachment_4" => 'NULL',
          "attachment_5" => 'NULL',
          "attachment_6" => 'NULL',
          "attachment_7" => 'NULL',
          "attachment_8" => 'NULL',
          "work_type" => 'NULL',
          "currency_id" => (int)$offer_details['currency_id'],
          "currency_code" => $offer_details['currency_code'],
          "budget" => 0,
          "location_type" => 'NULL',
          "address" => 'NULL',
          "start_datetime" => 'NULL',
          "start_datetime" => 'NULL',
          "hours" => 0,
          "hours_per_duration" => 'NULL',
          "visibility" => 'NULL',
          "project_duration" => 'NULL',
          "question_1" => 'NULL',
          "question_2" => 'NULL',
          "question_3" => 'NULL',
          "question_4" => 'NULL',
          "question_5" => 'NULL',
          "cre_datetime" => $today,
          "update_datetime" => $today,
          "country_id" => (int)$offer_details['country_id'],
          "cust_id" => $cust_id,
          "provider_id" => (int)$offer_details['cust_id'],
          "proposal_accepted_datetime" => 'NULL',
          "open_for_proposal" => 1,
          "proposal_id" => 0,
          "proposal_price" => 0,
          "job_type" => 'NULL',
          "job_status" => 'open',
          "job_status_datetime" => $today,
          "cancellation_reason" => 'NULL',
          "cancellation_desc" => 'NULL',
          "promo_code" => 'NULL',
          "promo_code_id" => 0,
          "promo_code_version" => 0,
          "promo_code_datetime" => 'NULL',
          "discount_amount" => 0,
          "discount_percent" => 0,
          "actual_price" => $offer_price,
          "offer_total_price" => $offer_price,
          "paid_amount" => 0,
          "payment_method" => 'NULL',
          "payment_mode" => 'NULL',
          "payment_by" => 'NULL',
          "transaction_id" => 'NULL',
          "payment_response" => 'NULL',
          "payment_datetime" => 'NULL',
          "balance_amount" => $offer_price,
          "complete_paid" => 0,
          "review_comment" => 'NULL',
          "review_datetime" => 'NULL',
          "rating" => 0,
          "cust_name" => $customer_details['firstname'].' '.$customer_details['lastname'],
          "cust_contact" => $customer_details['mobile1'],
          "cust_email" => $customer_details['email1'],
          "provider_name" => $provider_details['firstname'].' '.$provider_details['lastname'],
          "provider_company_name" => $provider_details['company_name'],
          "provider_contact" => $provider_details['contact_no'],
          "provider_email" => $provider_details['email_id'],
          "gonagoo_commission_per" => $advance_payment_details['gonagoo_commission'],
          "gonagoo_commission_amount" => $gonagoo_commission_amount,
          "commission_invoice_url" => 'NULL',
          "is_disputed" => 0,
          "dispute_id" => 0,
          "status_updated_by" => 'customer',
          "cancelled_date" => 'NULL',
          "refund_amount" => 0,
          "refund_invoice_url" => 'NULL',
          "job_url" => 'NULL',
          "view_count" => 0,
          "favorite_counts" => 0,
          "service_type" => 1,
          "offer_id" => $offer_id,
          "offer_qty" => $quantity,
          "contract_expired_on" => 'NULL',
          "offer_quick_delivery_charges" => ($quick_delivery_base_price * $quantity),
          "addon_total_price" => ($add_on_price * $quantity),
          "no_of_addons" => $no_of_addons,
        );
        //echo json_encode($insert_data); die();

        if($job_id = $this->api->register_job_details($insert_data)) {
          //insert ourchased addon details
          if($selected_add_on_ids != '') {
            for ($i = 0; $i < sizeof($selected_add_on_ids); $i++) {
              $add_on_details = $this->api->get_offer_add_on_details($selected_add_on_ids[$i]);
              $insert_data_addon = array(
                "job_id" => (int)$job_id,
                "cre_datetime" => $today,
                "addon_title" => $add_on_details['addon_title'],
                "country_id" => $add_on_details['country_id'],
                "currency_id" => $add_on_details['currency_id'],
                "currency_code" => $add_on_details['currency_code'],
                "addon_price" => $add_on_details['addon_price'],
                "work_day" => $add_on_details['work_day'],
                "update_datetime" => $today,
                "addon_qty" => $quantity,
                "total_price" => round(($add_on_details['addon_price'] * $quantity),2),
              );
              $this->api->register_purchased_addons($insert_data_addon);
            }
          }

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('You have purchased an offer from')." ".$provider_name.". ".$this->lang->line('Kindly complete the payment').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase confirm.'), 'type' => 'offer-purchse-notice', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase confirm.'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase confirmation'), trim($message));
          //------------------------------------------

          //Notifications to Provider----------------
            $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.". ".$this->lang->line('Login your account to proceed it').". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$offer_details['cust_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase confirm.'), 'type' => 'offer-purchse-notice', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer purchase confirm.'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

            //Email Notification
            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase confirmation'), trim($message));
          //------------------------------------------
          
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Offer purchased on')." ".$today.". ".$this->lang->line('Kindly complete the payment').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";

          //Update to workstream master
          $update_data = array(
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$offer_details['cust_id'],
            'cre_datetime' => $today,
            'status' => 0,
          );
          if($workstream_id = $this->api->update_to_workstream_master($update_data)) {
            //Update to workroom
            $update_data = array(
              'workstream_id' => (int)$workstream_id,
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$offer_details['cust_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => (int)$offer_details['cust_id'],
              'type' => 'job_started',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          }

          $this->session->set_flashdata('success', $this->lang->line('Offer purchased confirmed.'));
          redirect('user-panel-services/customer-offer-payment/'.$job_id);
        } else { 
          $this->session->set_flashdata('error', $this->lang->line("Unable purchase offer. Try again...")); 
          redirect(base_url('user-panel-services/available-offers-list-view'),'refresh');
        }
      } else { 
        $this->session->set_flashdata('error', $this->lang->line("Unable purchase offer. Try again...")); 
        redirect(base_url('user-panel-services/available-offers-list-view'),'refresh');
      }
    }
    public function customer_offer_payment()
    {
      if( !is_null($this->input->post('job_id')) ) { $job_id = (int) $this->input->post('job_id'); }
      else if(!is_null($this->uri->segment(3))) { $job_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-services/available-offers-list-view'); }
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $customer_details = $this->api->get_user_details($cust_id);
      $details = $this->api->get_job_details($job_id);
      //echo json_encode($details); die();
      $this->load->view('services/offer_payment_customer_view', compact('details','cust_id','customer_details'));
      $this->load->view('user_panel/footer');
    }
    public function offer_promocode_ajax()
    {
      $promo_code_name =  $this->input->post('promocode');
      $job_id =  $this->input->post('job_id'); $job_id = (int)$job_id;
      $details =  $this->api->get_job_details($job_id);
      if($promo_code = $this->api->get_promocode_by_name($promo_code_name)) {
        $used_codes = $this->api->get_used_promo_code($promo_code['promo_code_id'],$promo_code['promo_code_version']);
        if(sizeof($used_codes) < $promo_code['no_of_limit']) {
          $service_id = explode(',',$promo_code['service_id']);
          if($details['promo_code'] == 'NULL') {
            if(in_array($details['sub_cat_id'], $service_id)) {
              if(strtotime($promo_code['code_expire_date']) >= strtotime(date('M-d-y'))) {
                if(!$this->api->check_used_promo_code($promo_code['promo_code_id'], $promo_code['promo_code_version'], $this->cust_id)) {
                  $discount_amount = round((($details['offer_total_price']/100)*$promo_code['discount_percent']), 2); 
                  if($details['currency_code'] == 'XAF' || $details['currency_code'] == 'XOF') { $discount_amount = round($discount_amount, 0); }
                  $offer_total_price =  round($details['offer_total_price'] - $discount_amount ,2);
                  if($details['currency_code'] == 'XAF' || $details['currency_code'] == 'XOF') { $offer_total_price = round($offer_total_price , 0); }
                  echo "success@".$this->lang->line('You will get')." ".$discount_amount." ".$details['currency_code']." ".$this->lang->line('Discount')."$".$this->lang->line('Use this promocode and pay')." ".$offer_total_price." ".$details['currency_code']; 
                } else { echo "error@".$this->lang->line('You are already used this promocode'); }
              } else { echo "error@".$this->lang->line('This Promocode has been Expired'); }
            } else { echo "error@".$this->lang->line('This promocode is not valid for this category'); }
          } else { echo "error@".$this->lang->line('You are already used promocode for this order'); }
        } else { echo "error@".$this->lang->line('This Promo code is used maximum number of limit'); } 
      } else { echo "error@".$this->lang->line('Invalid Promocode Entered'); }
    }
    public function offer_promocode()
    {
      //echo json_encode($_POST); die(); 
      if(isset($_POST["promo_code"])) {
        $job_id = $this->input->post('job_id');  $job_id = (int)$job_id;
        $details =  $this->api->get_job_details($job_id);

        $promo_code_name = $this->input->post("promo_code");
        if($details['promo_code'] == 'NULL'){
          if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
            if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))) {
              if(!$this->api->check_used_promo_code($promocode['promo_code_id'], $promocode['promo_code_version'], $this->cust_id)) {
                if($details['currency_code'] == 'XAF' || $details['currency_code'] == 'XOF') {
                  $discount_amount = round((($details['offer_total_price']/100)*$promocode['discount_percent']), 2);
                } else { $discount_amount = round((($details['offer_total_price']/100)*$promocode['discount_percent']), 0); }
                $offer_total_price = $details['offer_total_price'] - $discount_amount;
                $update = array(
                  'offer_total_price' => $offer_total_price,
                  'promo_code' => $promocode['promo_code'],
                  'promo_code_id' => $promocode['promo_code_id'],
                  'promo_code_version' => $promocode['promo_code_version'],
                  'discount_amount' => $discount_amount,
                  'discount_percent' => $promocode['discount_percent'],
                  'promo_code_datetime' => date('Y-m-d h:i:s')
                );
                $this->api->job_details_update($job_id, $update);

                $used_promo_update = array(
                  'promo_code_id' => $promocode['promo_code_id'],
                  'promo_code_version' => $promocode['promo_code_version'],
                  'cust_id' => $this->cust_id,
                  'discount_percent' => $promocode['discount_percent'],
                  'discount_amount' => $discount_amount,
                  'discount_currency' => $details['currency_code'],
                  'cre_datetime' => date('Y-m-d h:i:s'),
                  'service_id' => $details['sub_cat_id'],
                  'booking_id' => $job_id,
                );
                $this->api->register_user_used_prmocode($used_promo_update);
                $this->session->set_flashdata('success', $this->lang->line('Promocode Applied successfully'));
              } else { $this->session->set_flashdata('error_promo', $this->lang->line('You are already used this promocode')); }
            } else { $this->session->set_flashdata('error_promo', $this->lang->line('This Promocode has been Expired')); }
          } else { $this->session->set_flashdata('error_promo', $this->lang->line('Invalid Promocode Entered')); }
        } else { $this->session->set_flashdata('error_promo', $this->lang->line('You are already used promocode for this order')); }
      } else { $this->session->set_flashdata('error_promo', $this->lang->line('Promocode not found.')); }
      redirect('user-panel-services/customer-offer-payment/'.$job_id);
    }
    public function offer_payment_bank()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      
      $data = array(
        'payment_method' => 'bank', 
        'payment_mode' => 'bank', 
        'payment_by' => 'web', 
        'is_bank_payment' => 1,
        'job_status' => 'in_progress',
      );
      if( $this->api->job_details_update($job_id, $data) ) {
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);

        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Offer payment marked as bank payment. ID').': '.$job_id;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer payment marked as Bank'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Offer payment marked as Bank'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer payment marked as Bank'), trim($message));
        //------------------------------------------------
        //workstream entry--------------------------------
          $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'payment',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //workstream entry--------------------------------
        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer payment marked as Bank'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Offer payment marked as Bank'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment marked as Bank'), trim($message));
        //------------------------------------------------
        //Update offer sales count------------------------
          $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
          $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
          $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
        //------------------------------------------------
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
    public function offer_payment_mtn()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $phone_no = $this->input->post('phone_no', TRUE); 
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      //echo json_encode($offer_total_price); die();
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$offer_total_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      $contents = curl_exec($ch);
      $mtn_pay_res = json_decode($contents, TRUE);
      //var_dump($mtn_pay_res); die();
      
      $payment_method = 'mtn';
      $transaction_id = '1234567890';
      //$transaction_id = trim($mtn_pay_res['TransactionID']);
      
      $payment_data = array();
      //if($mtn_pay_res['StatusCode'] === "01"){
      if(1){
        $data = array(
          'paid_amount' => $offer_total_price,
          'payment_method' => trim($payment_method), 
          'payment_mode' => 'online', 
          'payment_by' => 'web', 
          'transaction_id' => trim($transaction_id), 
          'payment_response' => trim($mtn_pay_res), 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1,
          'job_status' => 'in_progress',
        );
        if( $this->api->job_details_update($job_id, $data) ) {
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Customer Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
            $message = $this->lang->line('Payment completed for offer purchase. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase payment'), trim($message));
          //------------------------------------------------
          //Workstream entry--------------------------------
            $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
          //Provider Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
            $message = $this->lang->line('Payment received for offer. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer payment received'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment received'), trim($message));
          //------------------------------------------------

          //Payment Section---------------------------------
            //Add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              //--------smp Add Payment to customer account---------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              //--------smp Add Payment to customer account---------
            //----------------------------------------------
            //Deduct Payment to customer account------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              // Smp Deduct Payment to customer account-----------
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'payment',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              // Smp Deduct Payment to customer account-----------

            //----------------------------------------------
            //Add Payment To Gonagoo Account----------------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------
            //Add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$provider_id,
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$provider_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($offer_total_price),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
            //----------------------------------------------
          //Payment Section---------------------------------
          //Update offer sales count------------------------
            $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
            $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
            $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
          //------------------------------------------------
          $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      } else {  $this->session->set_flashdata('error', $this->lang->line('payment_failed')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
    public function offer_payment_stripe()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      $payment_method = 'stripe';
      
      $data = array(
        'paid_amount' => $offer_total_price,
        'payment_method' => trim($payment_method), 
        'payment_mode' => 'online', 
        'payment_by' => 'web', 
        'transaction_id' => trim($transaction_id), 
        'payment_response' => trim($stripeEmail), 
        'payment_datetime' => trim($today),
        'balance_amount' => '0', 
        'complete_paid' => 1,
        'job_status' => 'in_progress',
      );
      if( $this->api->job_details_update($job_id, $data) ) {
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        $provider_id = $provider_details['cust_id'];

        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Payment completed for offer purchase. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer purchase payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase payment'), trim($message));
        //------------------------------------------------
        //Workstream entry--------------------------------
          $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'payment',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //Workstream entry--------------------------------
        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $message = $this->lang->line('Payment received for offer. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Offer payment received'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment received'), trim($message));
        //------------------------------------------------

        //Payment Section---------------------------------
          //Add Payment to customer account---------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
            } else {
              $data_cust_mstr = array(
                "user_id" => $job_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_cust_mstr);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($offer_total_price),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            //Smp Add Payment to customer account--------------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            //Smp Add Payment to customer account--------------
          //----------------------------------------------
          //Deduct Payment to customer account------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
            $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($offer_total_price),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp Deduct Payment to customer account-----------
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp Deduct Payment to customer account-----------
          //----------------------------------------------
          //Add Payment To Gonagoo Account----------------
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
            } else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }

            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($offer_total_price);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'standard_price',
              "amount" => trim($offer_total_price),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($job_details['currency_code']),
              "country_id" => trim($customer_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
          //----------------------------------------------
          //Add Payment to provider Escrow----------------
            if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_pro_scrw = array(
                "deliverer_id" => (int)$provider_id,
                "scrow_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_scrow_record($data_pro_scrw);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
            }

            $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
            $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

            $data_scrw_hstry = array(
              "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
              "order_id" => (int)$job_id,
              "deliverer_id" => (int)$provider_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'standard_price',
              "amount" => trim($offer_total_price),
              "scrow_balance" => trim($scrow_balance),
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_scrow_history_record($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record_smp($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
              }
              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
              $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------------
          //----------------------------------------------
        //Payment Section---------------------------------
        //Update offer sales count------------------------
          $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
          $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
          $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
        //------------------------------------------------
        $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
    public function offer_payment_orange()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      $payment_id = 'g_jobs_'.time().'_'.$job_id;
      //get access token
      $ch = curl_init("//api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      //echo json_encode($token_details); die();

      $token = 'Bearer '.$token_details['access_token'];

      $lang = $this->session->userdata('language');
      if($lang=='french') $lang = 'fr';
      else $lang = 'en';

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "merchant_key" => "a8f8c61e",
                            "currency" => "OUV",
                            "order_id" => $payment_id,
                            "amount" => $offer_total_price,
                            "return_url" => base_url('user-panel-services/offer-payment-orange-process'),
                            "cancel_url" => base_url('user-panel-services/offer-payment-orange-process'),
                            "notif_url" => base_url(),
                            "lang" => $lang, 
                            "reference" => $this->lang->line('Gonagoo - Offer Payment') 
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("//api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: ".$token,
        "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      //echo json_encode($url_details); die();

      //echo $url_details['notif_token']; die();
      
      if(isset($url_details) && $url_details['message'] == 'OK' && $url_details['status'] == 201) {
        //echo 'true'; die();
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('job_id', $job_id);
        $this->session->set_userdata('payment_id', $payment_id);
        $this->session->set_userdata('amount', $offer_total_price);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        //echo 'false'; die();
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-services/my-jobs');
      }
    }
    public function offer_payment_orange_process()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id; $cust_id = (int) $cust_id;
      $session_token = $_SESSION['notif_token'];
      $job_id = $_SESSION['job_id'];
      $job_details = $this->api->get_job_details($job_id);
      $payment_id = $_SESSION['payment_id'];
      $offer_total_price = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];
      // get payment link
      $json_post_data = json_encode(
                          array(
                            "order_id" => $payment_id, 
                            "amount" => $offer_total_price, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      //$transaction_id = $payment_details['txnid'];
      $transaction_id = '123';
      //echo json_encode($payment_details); die();
      // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
      if(1) {
        $data = array(
          'paid_amount' => $offer_total_price,
          'payment_method' => trim($payment_method), 
          'payment_mode' => 'online', 
          'payment_by' => 'web', 
          'transaction_id' => trim($transaction_id), 
          'payment_response' => trim($payment_res), 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1,
          'job_status' => 'in_progress',
        );
        if( $this->api->job_details_update($job_id, $data) ) {
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Customer Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
            $message = $this->lang->line('Payment completed for offer purchase. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase payment'), trim($message));
          //------------------------------------------------
          //Workstream entry--------------------------------
            $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
          //Provider Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
            $message = $this->lang->line('Payment received for offer. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer payment received'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment received'), trim($message));
          //------------------------------------------------

          //Payment Section---------------------------------
            //Add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              // Smp Add Payment to customer account--------------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              // Smp Add Payment to customer account--------------
            //----------------------------------------------
            //Deduct Payment to customer account------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              // Smp Deduct Payment to customer account-----------
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'payment',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              // Smp Deduct Payment to customer account-----------
            //----------------------------------------------
            //Add Payment To Gonagoo Account----------------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------
            //Add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$provider_id,
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
                }
                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$provider_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($offer_total_price),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
            //----------------------------------------------
          //Payment Section---------------------------------
          //Update offer sales count------------------------
            $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
            $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
            $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
          //------------------------------------------------

          $this->session->unset_userdata('notif_token');
          $this->session->unset_userdata('job_id');
          $this->session->unset_userdata('payment_id');
          $this->session->unset_userdata('amount');
          $this->session->unset_userdata('pay_token');

          $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
  //End Customer Offers Search and Buy---------------------
  
  //Start Customer Jobs------------------------------------
    public function my_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => $cust_id,
        "provider_id" => 0,
        "job_status" => 'open',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $job_list = $this->api->get_jobs_list($search_data);
      //echo $this->db->last_query(); die();
      // echo json_encode($job_list); die();
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();

      for($i=0; $i<sizeof($job_list); $i++) {
        if($job_list[$i]['service_type'] == 0){
          $proposals = $this->api->get_job_proposals_list($job_list[$i]['job_id']);
          $job_list[$i]['proposal_counts'] = sizeof($proposals);
        }
      }
      
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
      //echo json_encode($job_list); die();
      $this->load->view('services/my-jobs-list-view', compact('job_list','filter','category_types','cat_id','cat_type_id','start_date','end_date','categories','cancellation_reasons'));
      $this->load->view('user_panel/footer');
    }
    public function my_inprogress_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => $cust_id,
        "provider_id" => 0,
        "job_status" => 'in_progress',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $job_list = $this->api->get_jobs_list($search_data);
      $dispute_master = $this->api->get_dispute_category_master($search_data);
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();
      for($i=0; $i<sizeof($job_list); $i++) {
        if($job_list[$i]['service_type'] == 0){
          if($this->api->get_milestone_payment_request($job_list[$i]['job_id'])){
            $job_list[$i]['milestone_request'] = "yes";
          }else{
            $job_list[$i]['milestone_request'] = "no";
          }
        }
      }
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
      //echo json_encode($job_list); die();

      $this->load->view('services/my-inprogress-jobs-list-view', compact('job_list','filter','category_types','cat_id','cat_type_id','start_date','end_date','categories','cancellation_reasons','dispute_master'));
      $this->load->view('user_panel/footer');
    }
    public function my_completed_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => $cust_id,
        "provider_id" => 0,
        "job_status" => 'completed',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $job_list = $this->api->get_jobs_list($search_data);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($job_list); die();
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();
      
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
      //echo json_encode($job_list); die();
      $this->load->view('services/my-completed-jobs-list-view', compact('job_list','filter','category_types','cat_id','cat_type_id','start_date','end_date','categories','cancellation_reasons'));
      $this->load->view('user_panel/footer');
    }
    public function my_cancelled_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => $cust_id,
        "provider_id" => 0,
        "job_status" => 'cancelled',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $job_list = $this->api->get_jobs_list($search_data);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($job_list); die();
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();
      
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
      //echo json_encode($job_list); die();
      $this->load->view('services/my-cancelled-jobs-list-view', compact('job_list','filter','category_types','cat_id','cat_type_id','start_date','end_date','categories','cancellation_reasons'));
      $this->load->view('user_panel/footer');
    }
    public function customer_offer_cancel()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $reason = $this->input->post('reason', TRUE);
      $other_reason = $this->input->post('other_reason', TRUE);
      $redirect_url = $this->input->post('redirect_url', TRUE);
      if($other_reason == '') { $other_reason = 'NULL'; }
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "cancellation_status" => 'cancel_request',
        "cancellation_reason" => $reason,
        "cancellation_desc" => $other_reason,
        "status_updated_by" => 'customer',
        "cancelled_date" => $today,
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your offer purchase ID').": ".$job_id." ".$this->lang->line('has been sent for cancellation').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer purchase cancellation request'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase cancellation request'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation request'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.", ".$this->lang->line('is requesting to cancel it').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer purchase cancellation request'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Offer purchase cancellation request'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation request'), trim($message));
        //------------------------------------------
          
        $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Offer purchased on')." ".$job_details['cre_datetime'].", ".$this->lang->line('has been requested for cancellation').". ".$this->lang->line('Cancellation Reason').": ".$job_details['cancellation_reason'].". ";

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'cancel_request',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job cancellation request sent.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to send cancellation request. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function customer_withdraw_cancel_request()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "cancellation_status" => 'NULL',
        "cancellation_reason" => 'NULL',
        "cancellation_desc" => 'NULL',
        "status_updated_by" => 'NULL',
        "cancelled_date" => 'NULL',
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('cancellation request has been withdrawn').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation request withdrawn'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job cancellation request withdrawn'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job cancellation request withdrawn'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Cancellation request has been withdrawn by')." ".$customer_name.", ".$this->lang->line('for the job. ID').". ".$job_id;
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation request withdrawn'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job cancellation request withdrawn'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job cancellation request withdrawn'), trim($message));
        //------------------------------------------
          
        $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job cancellation has been withdrawn').", ".$this->lang->line('for the job. ID').". ".$job_id;

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'cancel_withdraw',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job cancellation request has been withdrawn.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to withdraw job cancellation request. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    // public function customer_job_details()
    // {
    //   //echo json_encode($_POST); die();
    //   $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
    //   $job_id = $this->input->post('job_id'); $job_id = (int)$job_id;
    //   $redirect_url = $this->input->post('redirect_url');
    //   $job_details = $this->api->get_job_details($job_id);
    //   $login_customer_details = $this->api->get_user_details($cust_id);
    //   $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
    //   $job_provider_profile = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
    //   $job_provider_details = $this->api->get_user_details((int)$job_details['provider_id']);
    //   $workroom = $this->api->get_workstream_details($job_id);

    //   $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
    //   $milestone_payment_request = $this->api->get_milestone_payment_request($job_id);

    //   $proposal_milestone = $this->api->get_proposal_milestone_by_milestone_id($milestone_payment_request['milestone_id']);
    //     $milestone_price = $proposal_milestone['milestone_price']; $milestone_price = (int)$milestone_price;


    //   $paid_amount =  (int)$job_details['paid_amount'];
    //   $paid_amount_history =(int) $job_paid_history['paid_amount'];

  
    //   $remain = $paid_amount - $paid_amount_history;
    //   $min = $milestone_price - $remain;
    //   $max = $job_details['budget'] - $job_details['paid_amount'];

    //   $this->load->view('services/job_details_view',compact('job_details', 'workroom', 'login_customer_details', 'job_provider_profile', 'job_provider_details', 'job_customer_details', 'redirect_url', 'cust_id','paid_amount_history','min','max','remain','milestone_price'));
    //   $this->load->view('user_panel/footer');
    // }

    public function customer_job_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id'); $job_id = (int)$job_id;
      $redirect_url = $this->input->post('redirect_url');
      $job_details = $this->api->get_job_details($job_id);
      $login_customer_details = $this->api->get_user_details($cust_id);
      $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
      $job_provider_details = $this->api->get_user_details((int)$job_details['provider_id']);
      $workroom = $this->api->get_workstream_details($job_id);

      $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
      $milestone_payment_request = $this->api->get_milestone_payment_request($job_id);

      $proposal_milestone = $this->api->get_proposal_milestone_by_milestone_id($milestone_payment_request['milestone_id']);
        $milestone_price = $proposal_milestone['milestone_price']; $milestone_price = (int)$milestone_price;


      $paid_amount =  (int)$job_details['paid_amount'];
      $paid_amount_history =(int) $job_paid_history['paid_amount'];

  
      $remain = $paid_amount - $paid_amount_history;
      $min = $milestone_price - $remain;
      $max = $job_details['budget'] - $job_details['paid_amount'];

      $this->load->view('services/job_details_view',compact('job_details', 'workroom', 'login_customer_details', 'job_provider_profile', 'job_provider_details', 'job_customer_details', 'redirect_url', 'cust_id','paid_amount_history','min','max','remain','milestone_price'));
      $this->load->view('user_panel/footer');
    }
    public function customer_accept_job_completed()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "job_status" => 'completed',
        "job_status_datetime" => $today,
        "complete_status" => 'completed',
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been completed').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job completed'), 'type' => 'job-completed', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job completed'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job completed'), trim($message));
        //------------------------------------------------

        //Notifications to Provider-----------------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your job ID')." ".$job_id.", ".$this->lang->line('has been completed').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job completed'), 'type' => 'job-completed', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job completed'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job completed'), trim($message));
        //------------------------------------------------
        
        //Update to workroom------------------------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job marked as completed.');
          if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'job_completed',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          }
        //------------------------------------------------

        //Payment Section---------------------------------
          //Deduct Payment from provider Escrow-----------
            if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
            } else {
              $data_pro_scrw = array(
                "deliverer_id" => (int)$job_details['provider_id'],
                "scrow_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_scrow_record($data_pro_scrw);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
            }

            $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($job_details['offer_total_price']);
            $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));

            $data_scrw_hstry = array(
              "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
              "order_id" => (int)$job_id,
              "deliverer_id" => (int)$job_details['provider_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'standard_price',
              "amount" => trim($job_details['offer_total_price']),
              "scrow_balance" => trim($scrow_balance),
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_scrow_history_record($data_scrw_hstry);
            //Smp deduct Payment from provider Escrow-----------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$job_details['provider_id'],
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record_smp($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($job_details['offer_total_price']);
              $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$job_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'standard_price',
                "amount" => trim($job_details['offer_total_price']),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
            //Smp deduct Payment from provider Escrow-----------
          //----------------------------------------------
          //Deduct Payment from Gonagoo Account-----------
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
            } else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }

            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) - trim($job_details['offer_total_price']);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$job_details['provider_id'],
              "type" => 0,
              "transaction_type" => 'standard_price',
              "amount" => trim($job_details['offer_total_price']),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($job_details['currency_code']),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
          //----------------------------------------------
          //Add Payment to Provider account---------------
            if($pro_acc_mstr = $this->api->customer_account_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
            } else {
              $data_pro_mstr = array(
                "user_id" => (int)$job_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_pro_mstr);
              $pro_acc_mstr = $this->api->customer_account_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
            }
            $account_balance = trim($pro_acc_mstr['account_balance']) + trim($job_details['offer_total_price']);
            $this->api->update_payment_in_customer_master($pro_acc_mstr['account_id'], (int)$job_details['provider_id'], $account_balance);
            $pro_acc_mstr = $this->api->customer_account_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$pro_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => (int)$job_details['provider_id'],
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'standard_price',
              "amount" => trim($job_details['offer_total_price']),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp add Payment to Provider account---------------
              if($pro_acc_mstr = $this->api->customer_account_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
              } else {
                $data_pro_mstr = array(
                  "user_id" => (int)$job_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_pro_mstr);
                $pro_acc_mstr = $this->api->customer_account_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
              }
              $account_balance = trim($pro_acc_mstr['account_balance']) + trim($job_details['offer_total_price']);
              $this->api->update_payment_in_customer_master_smp($pro_acc_mstr['account_id'], (int)$job_details['provider_id'], $account_balance);
              $pro_acc_mstr = $this->api->customer_account_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$pro_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => (int)$job_details['provider_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($job_details['offer_total_price']),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp add Payment to Provider account---------------

          //----------------------------------------------
          //Remove G Commission from Provider account-----
            if($pro_acc_mstr = $this->api->customer_account_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
            } else {
              $data_pro_mstr = array(
                "user_id" => (int)$job_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_pro_mstr);
              $pro_acc_mstr = $this->api->customer_account_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
            }
            $account_balance = trim($pro_acc_mstr['account_balance']) - trim($job_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_customer_master($pro_acc_mstr['account_id'], (int)$job_details['provider_id'], $account_balance);
            $pro_acc_mstr = $this->api->customer_account_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$pro_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => (int)$job_details['provider_id'],
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'job_commission',
              "amount" => trim($job_details['gonagoo_commission_amount']),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp Remove G Commission from Provider account-------
              if($pro_acc_mstr = $this->api->customer_account_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
              } else {
                $data_pro_mstr = array(
                  "user_id" => (int)$job_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_pro_mstr);
                $pro_acc_mstr = $this->api->customer_account_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
              }
              $account_balance = trim($pro_acc_mstr['account_balance']) - trim($job_details['gonagoo_commission_amount']);
              $this->api->update_payment_in_customer_master_smp($pro_acc_mstr['account_id'], (int)$job_details['provider_id'], $account_balance);
              $pro_acc_mstr = $this->api->customer_account_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$pro_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => (int)$job_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'job_commission',
                "amount" => trim($job_details['gonagoo_commission_amount']),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp Remove G Commission from Provider account-------
          //----------------------------------------------
          //Add G Commission to Gonagoo Account-----------
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
            } else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }

            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($job_details['gonagoo_commission_amount']);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$job_details['provider_id'],
              "type" => 1,
              "transaction_type" => 'job_commission',
              "amount" => trim($job_details['gonagoo_commission_amount']),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($job_details['currency_code']),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
          //----------------------------------------------
        //Payment Section---------------------------------

        //Payment invoice for customer--------------------
          $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
          require_once($phpinvoice);
          $lang = $this->input->post('lang', TRUE);
          if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
          else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
          else { $invoice_lang = "englishApi_lang"; }
          //Invoice Configuration
          $invoice = new phpinvoice('A4',trim($job_details['currency_code']), $invoice_lang);
          $invoice->setLogo("resources/fpdf-master/invoice-header.png");
          $invoice->setColor("#000");
          $invoice->setType("");
          $invoice->setReference('#GSJ/'.$job_id);
          $invoice->setDate(date('M dS ,Y',time()));

          $pro_country = $this->api->get_country_details(trim($provider_details['country_id']));
          $pro_state = $this->api->get_state_details(trim($provider_details['state_id']));
          $pro_city = $this->api->get_city_details(trim($provider_details['city_id']));

          $cust_country = $this->api->get_country_details(trim($customer_details['country_id']));
          $cust_state = $this->api->get_state_details(trim($customer_details['state_id']));
          $cust_city = $this->api->get_city_details(trim($customer_details['city_id']));

          $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
          $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
          $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

          $invoice->setTo(array($provider_name,trim($pro_city['city_name']),trim($pro_state['state_name']),trim($pro_country['country_name']),trim($provider_details['email_id'])));

          $invoice->setFrom(array($customer_name,trim($cust_city['city_name']),trim($cust_state['state_name']),trim($cust_country['country_name']),trim($customer_details['email1'])));

          $eol = PHP_EOL;
          /* Adding Items in table */
          $job_for = $this->lang->line('Job ID').': '.$job_id. ' - ' .$this->lang->line('Payment');
          $total = $rate = round(trim($job_details['offer_total_price']),2);
          $payment_datetime = substr($today, 0, 10);
          //set items
          $invoice->addItem("","$payment_datetime       $job_for","","","1",$rate,$total);
          /* Add totals */
          $invoice->addTotal($this->lang->line('sub_total'),$total);
          $invoice->addTotal($this->lang->line('taxes'),'0');
          $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
          /* Set badge */ 
          $invoice->addBadge($this->lang->line('Job Amount Paid'));
          /* Add title */
          $invoice->addTitle($this->lang->line('tnc'));
          /* Add Paragraph */
          $invoice->addParagraph($gonagoo_address['terms']);
          /* Add title */
          $invoice->addTitle($this->lang->line('payment_dtls'));
          /* Add Paragraph* /
          $invoice->addParagraph($gonagoo_address['payment_details']);
          /* Add title */
          $invoice->addTitleFooter($this->lang->line('thank_you_order'));
          /* Add Paragraph */
          $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
          /* Set footer note */
          $invoice->setFooternote($gonagoo_address['company_name']);
          /* Render */
          $invoice->render($job_id.'_job_payment.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
          //Update File path
          $pdf_name = $job_id.'_job_payment.pdf';
          //Move file to upload folder
          rename($pdf_name, "resources/job-invoices/".$pdf_name);

          //Update Order Invoice
          $update_data = array(
            "job_invoice_url" => "job-invoices/".$pdf_name,
          );
          $this->api->update_job_details($job_id, $update_data);
        //------------------------------------------------
        //Update invoice to workroom----------------------
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $this->lang->line('Job payment invoice'),
            'attachment_url' =>  "resources/job-invoices/".$pdf_name,
            'cre_datetime' => $today,
            'sender_id' => (int)$job_details['provider_id'],
            'type' => 'payment_invoice',
            'file_type' => 'pdf',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //------------------------------------------------
        //Email Invoice to customer-----------------------
          $emailBody = $this->lang->line('Please download your payment invoice.');
          $gonagooAddress = $gonagoo_address['company_name'];
          $emailSubject = $this->lang->line('Serivesgoo - Job payment invoice');
          $emailAddress = trim($customer_details['email1']);
          $fileToAttach = "resources/job-invoices/$pdf_name";
          $fileName = $pdf_name;
          $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
        //------------------------------------------------
        //Notifications to customers----------------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('An invoice has been emailed for the job ID').": ".$job_id.". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job payment invoice'), 'type' => 'job-payment-invoice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job payment invoice'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
        //------------------------------------------------

        //G Commission invoice for Provider---------------
          $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
          require_once($phpinvoice);
          $lang = $this->input->post('lang', TRUE);
          if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
          else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
          else { $invoice_lang = "englishApi_lang"; }
          //Invoice Configuration
          $invoice = new phpinvoice('A4',trim($job_details['currency_code']), $invoice_lang);
          $invoice->setLogo("resources/fpdf-master/invoice-header.png");
          $invoice->setColor("#000");
          $invoice->setType("");
          $invoice->setReference('#GSC/'.$job_id);
          $invoice->setDate(date('M dS ,Y',time()));

          $pro_country = $this->api->get_country_details(trim($provider_details['country_id']));
          $pro_state = $this->api->get_state_details(trim($provider_details['state_id']));
          $pro_city = $this->api->get_city_details(trim($provider_details['city_id']));

          $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
          $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
          $gonagoo_state = $this->api->get_state_details(trim($gonagoo_address['state_id']));
          $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

          $invoice->setFrom(array($provider_name,trim($pro_city['city_name']),trim($pro_state['state_name']),trim($pro_country['country_name']),trim($provider_details['email_id'])));

          $invoice->setTo(array($gonagoo_address['company_name'],trim($gonagoo_city['city_name']),trim($gonagoo_state['state_name']),trim($gonagoo_country['country_name']),trim($gonagoo_address['email'])));

          $eol = PHP_EOL;
          /* Adding Items in table */
          $job_for = $this->lang->line('Job ID').': '.$job_id. ' - ' .$this->lang->line('Commission');
          $total = $rate = round(trim($job_details['gonagoo_commission_amount']),2);
          $payment_datetime = substr($today, 0, 10);
          //set items
          $invoice->addItem("","$payment_datetime       $job_for","","","1",$rate,$total);
          /* Add totals */
          $invoice->addTotal($this->lang->line('sub_total'),$total);
          $invoice->addTotal($this->lang->line('taxes'),'0');
          $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
          /* Set badge */ 
          $invoice->addBadge($this->lang->line('Commission Paid'));
          /* Add title */
          $invoice->addTitle($this->lang->line('tnc'));
          /* Add Paragraph */
          $invoice->addParagraph($gonagoo_address['terms']);
          /* Add title */
          $invoice->addTitle($this->lang->line('payment_dtls'));
          /* Add Paragraph* /
          $invoice->addParagraph($gonagoo_address['payment_details']);
          /* Add title */
          $invoice->addTitleFooter($this->lang->line('thank_you_order'));
          /* Add Paragraph */
          $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
          /* Set footer note */
          $invoice->setFooternote($gonagoo_address['company_name']);
          /* Render */
          $invoice->render($job_id.'_job_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
          //Update File path
          $pdf_name = $job_id.'_job_commission.pdf';
          //Move file to upload folder
          rename($pdf_name, "resources/job-invoices/".$pdf_name);

          //Update Order Invoice
          $update_data = array(
            "commission_invoice_url" => "job-invoices/".$pdf_name,
          );
          $this->api->update_job_details($job_id, $update_data);
        //------------------------------------------------
        //Email Invoice to customer-----------------------
          $emailBody = $this->lang->line('Please download commission invoice.');
          $gonagooAddress = $gonagoo_address['company_name'];
          $emailSubject = $this->lang->line('Serivesgoo - Job commission invoice');
          $emailAddress = trim($provider_details['email_id']);
          $fileToAttach = "resources/job-invoices/$pdf_name";
          $fileName = $pdf_name;
          $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
        //------------------------------------------------
        //Notifications to Provider-----------------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('A commission invoice has been emailed you for job ID')." ".$job_id;
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job commission invoice'), 'type' => 'job-commission-invoice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job commission invoice'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
        //------------------------------------------------
        //Provider request for review and ratings---------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Kindly rate and review our service.');
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  "NULL",
            'cre_datetime' => $today,
            'sender_id' => (int)$job_details['provider_id'],
            'type' => 'chat',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //------------------------------------------------

        $this->session->set_flashdata('success', $this->lang->line('Job cancellation request sent.'));
        redirect(base_url('user-panel-services/my-completed-jobs'), 'refresh');
      } else { 
        $this->session->set_flashdata('error', $this->lang->line('Unable to send cancellation request. Try again...')); 
        redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
      }
    }
    public function customer_post_job_review()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $rating_offer = $this->input->post('rating_offer', TRUE); $rating_offer = (int) $rating_offer;
      $review_offer = $this->input->post('review_offer', TRUE);
      $rating_job = $this->input->post('rating_job', TRUE); $rating_job = (int) $rating_job;
      $review_job = $this->input->post('review_job', TRUE);
      $rating_provider = $this->input->post('rating_provider', TRUE); $rating_provider = (int) $rating_provider;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');
      //Update job rating and review
      $update_data = array(
        "review_comment" => trim($review_job),
        "review_datetime" => $today,
        "rating" => $rating_job,
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);

        //Update provider server rating
        if($service_rating = $this->api->get_provider_service_rating($job_details['provider_id'])) {
          $no_of_ratings = $service_rating['no_of_ratings']+1;
          $total_ratings = $service_rating['total_ratings']+$rating_provider;
          $ratings = round($total_ratings/$no_of_ratings,1);
          $update_data = array(
            "ratings" => $ratings,
            "total_ratings" => $total_ratings,
            "no_of_ratings" => $no_of_ratings,
            "update_date"=> $today
          );
          $this->api->update_provider_service_ratings($service_rating['rating_id'], $update_data); 
        } else {
          $insert_data = array(
            "provider_id" => $job_details['provider_id'],
            "ratings" => $rating_provider,
            "total_ratings" => $rating_provider,
            "no_of_ratings" => 1,
            "update_date"=> $today
          );
          $this->api->insert_provider_service_ratings($insert_data); 
        }

        //Update provider rating
        $provider_details = $this->api->get_user_details($job_details['provider_id']);
        $no_of_ratings = $provider_details['no_of_ratings']+1;
        $total_ratings = $provider_details['total_ratings']+$rating_provider;
        $ratings = round($total_ratings/$no_of_ratings,1);
        $update_data = array(
          "ratings" => $ratings,
          "total_ratings" => $total_ratings,
          "no_of_ratings" => $no_of_ratings
        );
        $this->api->update_provider_rating($job_details['provider_id'], $update_data);

        //Update job rating and review in smp_job_review
        $insert_data = array(
          "job_id" => $job_id,
          "cust_id" => $job_details['cust_id'],
          "rated_by" => 'customer',
          "provider_id" => $job_details['provider_id'],
          "review_comment" => trim($review_job),
          "review_rating" => $rating_job,
          "review_datetime" => $today,
          "review_reply" => 'NULL',
          "reply_datetime" => 'NULL',
          "offer_id" => $job_details['offer_id'],
        );
        $this->api->insert_job_review($insert_data);

        //Update offer rating and review
        $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
        $update_data = array(
          "rating_count" => $offer_details['rating_count'] + 1,
          "offer_rating" => $offer_details['offer_rating'] + $rating_offer,
          "review_count" => $offer_details['review_count'] + 1,
        );
        $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);

        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
          
        $message = $this->lang->line('Hello')." ".$provider_name.", ".$review_job;
        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'job_rating',
            'file_type' => 'text',
            'proposal_id' => 0,
            'ratings' => $rating_job,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Rating and reviews submitted successfully.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to submit rating and reviews. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function customer_create_dispute_for_job()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $dispute_title = $this->input->post('dispute_title', TRUE);
      $dispute_cat_id = $this->input->post('dispute_cat_id', TRUE); $dispute_cat_id = (int) $dispute_cat_id;
      $dispute_sub_cat_id = $this->input->post('dispute_sub_cat_id', TRUE); $dispute_sub_cat_id = (int) $dispute_sub_cat_id;
      $dispute_desc = $this->input->post('dispute_desc', TRUE);
      $request_money_back = $this->input->post('request_money_back', TRUE);
      $request_money_back = ($request_money_back == 'on') ? 1 : 0;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');
      $job_details = $this->api->get_job_details($job_id);

      $insert_data = array(
        "complete_status" => 'dispute_raised',
      );
      if($this->api->update_job_details($job_id, $insert_data)) {
        $update_data = array(
          "dispute_cat_id" => $dispute_cat_id,
          "dispute_sub_cat_id" => $dispute_sub_cat_id,
          "dispute_title" => trim($dispute_title),
          "dispute_desc" => trim($dispute_desc),
          "service_id" => $job_id,
          "service_type" => $job_details['service_type'],
          "cre_datetime" => $today,
          "cust_id" => $job_details['cust_id'],
          "provider_id" => $job_details['provider_id'],
          "dispute_status" => 'open',
          "dispute_withdraw_datetime" => 'NULL',
          "dispute_in_progress_datetime" => 'NULL',
          "dispute_closed_datetime" => 'NULL',
          "admin_id" => 0,
          "dispute_created_by" => $cust_id,
          "request_money_back" => $request_money_back,
          "money_back_status" => 0,
          "money_back_datetime" => 'NULL',
        );
        $dispute_id = $this->api->create_job_dispute($update_data);

        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $pro_details = $this->api->get_user_details((int)$job_details['provider_id']);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your offer purchase ID').": ".$job_id." ".$this->lang->line('has been marked as disputed').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job marked disputed'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.", ".$this->lang->line('is marked as disputed and send to Gonagoo admin for process').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job marked disputed'), trim($message));
        //------------------------------------------
        
        //update to workroom
        $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('This job has been marked as disputed and send to Gonagoo admin for process');
        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'dispute_raised',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job dispute request sent to Gonagoo admin.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to request for job dispute. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url),'refresh');
    }
  //End Customer Jobs--------------------------------------

  //Start Job Workroom-------------------------------------
    public function customer_job_offer_workroom()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      if(!$job_id) { $job_id = $this->session->userdata('workroom_job_id'); }
      $login_customer_details = $this->api->get_user_details($cust_id);
      $job_details = $this->api->get_job_details($job_id);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
      $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
      $job_provider_details = $this->api->get_user_details((int)$job_details['provider_id']);
      $keys = ['provider_id' => (int)$job_provider_profile['cust_id'], 'cust_id' => $cust_id, 'job_id' => $job_id];
      $workroom = $this->api->get_workstream_details($job_id);
      //echo json_encode($workroom); die();
      $this->load->view('services/services_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'job_provider_profile', 'keys', 'job_provider_details', 'job_customer_details'));
      $this->load->view('user_panel/footer');
    }
    public function customer_job_offer_workroom_ajax()
    {
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $chat_count = $this->input->post('chat_count', TRUE); $chat_count = (int)$chat_count;
      $job_details = $this->api->get_job_details($job_id);
      $login_customer_details = $this->api->get_user_details($cust_id);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
      $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
      $job_provider_details = $this->api->get_user_details((int)$job_details['provider_id']);
      $workroom_details = $this->api->get_workstream_details($job_id);
      $new_chat_count = sizeof($workroom_details);

      if(sizeof($workroom_details) > $chat_count) {
        $message_template ='';
        $message_template .= "<input type='hidden' value='$new_chat_count' id='chat_count' />";
        foreach ($workroom_details as $v => $workroom) { $tabindex = ($new_chat_count==($v+1))?'tabindex=1':'';
          date_default_timezone_set($this->session->userdata("default_timezone")); 
          $ud = strtotime($workroom['cre_datetime']);
          date_default_timezone_set($this->session->userdata("user_timezone"));

          $chat_side = ($workroom['sender_id'] == $cust_id)?'right':'left';
          $message_template .= "<li class='chat-message $chat_side'>";

          if($workroom['sender_id'] == $cust_id) {
            $url = base_url($login_customer_details['avatar_url']);
            $message_template .= "<img class='message-avatar' style='margin-right: 0px;' src='$url' />";
            $message_template .= "<div class='message' style='margin-left: 0px;'>";
              $cust_name = $login_customer_details['firstname'].' '.$login_customer_details['lastname'];
              $message_template .= "<a class='message-author'>$cust_name</a>";
              $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
              $message_template .= "<div class='row'>";

                if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" || $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice"):
                  $message_template .= "<span class='message-content'>";
                    if($workroom['attachment_url'] != 'NULL'):
                      $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<p>&nbsp;</p>";
                      $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                      $message_template .= "</div>";
                    endif;
                    $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                    $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                      if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ):
                        $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                      endif;
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_started"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "payment"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                      for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                      for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                      $message_template .= "( ".$workroom['ratings']." / 5 )";
                      $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

              $message_template .= "</div>";
            $message_template .= "</div>";
          } else {
            $url = base_url(($workroom['sender_id'] == $job_details['cust_id']) ? $job_customer_details['avatar_url'] : $job_provider_details['avatar_url']);
            $message_template .= "<img class='message-avatar' src='$url' />";
            $message_template .= "<div class='message'>";
              $user_name = ($workroom['sender_id']==$job_details['cust_id']) ? $job_customer_details['firstname'].' '.$job_customer_details['lastname'] : $job_provider_details['firstname'].' '.$job_provider_details['lastname'];
              $message_template .= "<a class='message-author text-left'>$user_name</a>";
              $ud = strtotime($workroom['cre_datetime']);
              $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
              $message_template .= "<div class='row'>";

                if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" || $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                  $message_template .= "<span class='message-content'>";
                    $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                    $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                      if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                        $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                      endif;
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p>$msg</p>";
                    $message_template .= "</div>";
                    if($workroom['attachment_url'] != 'NULL') :
                      $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                        $message_template .= "<p>&nbsp;</p>";
                        $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                      $message_template .= "</div>";
                    endif;
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_started"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "payment"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                      for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                      for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                      $message_template .=  "( ".$workroom['ratings']." / 5 )";
                      $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

              $message_template .= "</div>";
            $message_template .= "</div>";
          }
          $message_template .= "</li>";
        }
      } else { $message_template = false; }
      echo json_encode($message_template); die();
    }
    public function add_services_workroom_chat()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $text_msg = $this->input->post('message_text', TRUE);
      $job_details = $this->api->get_job_details($job_id); 
      $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
      $provider_id = $job_details['provider_id']; $provider_id = (int)$provider_id;
      $sender_id = (int)$cust['cust_id'];
      $today = date("Y-m-d H:i:s");

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['max_width']      = '0'; 
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
        if($this->upload->do_upload('attachment')) { 
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
        $insert_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => $job_id,
          'cust_id' => $cust_id,
          'provider_id' => $provider_id,
          'text_msg' => $text_msg,
          'attachment_url' =>  $attachment_url,
          'cre_datetime' => $today,
          'sender_id' => $sender_id,
          'type' => ($attachment_url=='NULL')?'chat':'attachment',
          'file_type' => ($attachment_url=='NULL')?'text':'file',
          'proposal_id' => 0,
          "ratings" => "NULL",
        );
        //echo json_encode($insert_data); die();
        $this->api->update_to_workstream_details($insert_data);

        //Notifications to Provider----------------
          $api_key = $this->config->item('delivererAppGoogleKey');

          if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$job_details['provider_id']);
          } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

          if(!is_null($device_details)) {
            $arr_fcm_ids = array(); $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
            $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_apn_ids, $api_key);
            }
          }
        //------------------------------------------
      }

      $array = array('workroom_job_id' => $job_id );      
      $this->session->set_userdata( $array );
      redirect('user-panel-services/customer-job-offer-workroom');
    }
    public function add_services_workroom_chat_ajax()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $text_msg = $this->input->post('message_text', TRUE);
      $job_details = $this->api->get_job_details($job_id); 
      $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
      $provider_id = $job_details['provider_id']; $provider_id = (int)$provider_id;
      $sender_id = (int)$cust['cust_id'];
      $today = date("Y-m-d H:i:s");

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['max_width']      = '0'; 
        $config['max_height']     = '0'; 
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
        if($this->upload->do_upload('attachment')) { 
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
        $insert_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => $job_id,
          'cust_id' => $cust_id,
          'provider_id' => $provider_id,
          'text_msg' => $text_msg,
          'attachment_url' =>  $attachment_url,
          'cre_datetime' => $today,
          'sender_id' => $sender_id,
          'type' => ($attachment_url=='NULL')?'chat':'attachment',
          'file_type' => ($attachment_url=='NULL')?'text':'file',
          'proposal_id' => 0,
          "ratings" => "NULL",
        );
        //echo json_encode($insert_data); die();
        $this->api->update_to_workstream_details($insert_data);

        //Notifications to Provider----------------
          $api_key = $this->config->item('delivererAppGoogleKey');

          if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$job_details['provider_id']);
          } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

          if(!is_null($device_details)) {
            $arr_fcm_ids = array(); $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
            $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_apn_ids, $api_key);
            }
          }
        //------------------------------------------
      }
      return json_encode(true);
    }
  //End Job Workroom---------------------------------------

  //Start Favorite Section----------------------------------  
    public function my_favorite_offers()
    {
      //echo json_encode($_POST); die();
      $countries = $this->api->get_countries();
      $category_types = $this->api->get_category_types();
      $favorite_offers = $this->api->get_favorite_offers($this->cust['cust_id']);
      $favorite_offer_ids = '';
      foreach ($favorite_offers as $offer) {
        $favorite_offer_ids .= $offer['offer_id'].',';
      }
      //rtrim($favorite_offer_ids,',');
      $favorite_offer_ids = substr(trim($favorite_offer_ids), 0, -1);
      $favorite_offer_ids = explode(',', $favorite_offer_ids);
      //echo json_encode($favorite_offer_ids); die();

      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { 
        $filter = 'basic';
        $country_id = 0;
        $state_id = 0;
        $city_id = 0;
        $sort_by = 'NULL';
        $cat_type_id = 0;
        $cat_id = 0;
        $delivered_in = 0;
        $start_price = 0;
        $end_price = 0;
        $latest = 'NULL';
        $active_status = 'active';
        $running_status = 'resumed';

        $search_data = array(
          "cust_id" => 0,
          "country_id" => trim($country_id),
          "state_id" => trim($state_id),
          "city_id" => trim($city_id),
          "sort_by" => trim($sort_by),
          "cat_type_id" => trim($cat_type_id),
          "cat_id" => trim($cat_id),
          "delivered_in" => trim($delivered_in),
          "start_price" => trim($start_price),
          "end_price" => trim($end_price),
          "latest" => trim($latest),
          "active_status" => trim($active_status),
          "running_status" => trim($running_status),
          "device" => 'web',
          "last_id" => 0,
          "offer_pause_resume" => 'all',
        );
        $offers = $this->api->get_service_provider_offers_filtered($search_data);

        $this->load->view('services/favorite_offers_list_view', compact('offers','filter','countries','category_types','favorite_offers','favorite_offer_ids'));
      } else {
        $country_id = $this->input->get('country_id', TRUE); $country_id = (int)$country_id;
        if(!isset($country_id) && $filter == '') { $country_id = 0; }
        $state_id = $this->input->get('state_id', TRUE); $state_id = (int)$state_id;
        if(!isset($state_id) && $filter == '') { $state_id = 0; }
        $city_id = $this->input->get('city_id', TRUE); $city_id = (int)$city_id;
        if(!isset($city_id) && $filter == '') { $city_id = 0; }
        $sort_by = $this->input->get('sort_by', TRUE);
        $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
        if(!isset($cat_type_id) && $filter == '') { $cat_type_id = 0; }
        $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
        if(!isset($cat_id) && $filter == '') { $cat_id = 0; }
        $delivered_in = $this->input->get('delivered_in', TRUE); $delivered_in = (int)$delivered_in;
        if(!isset($delivered_in) && $filter == '') { $delivered_in = 0; }
        $start_price = $this->input->get('start_price', TRUE); $start_price = (int)$start_price;
        if(!isset($start_price) && $filter == '') { $start_price = 0; }
        $end_price = $this->input->get('end_price', TRUE); $end_price = (int)$end_price;
        if(!isset($end_price) && $filter == '') { $end_price = 0; }
        $latest = $this->input->get('latest', TRUE);

        $active_status = 'active';
        $running_status = 'resumed';

        if(isset($country_id) && $country_id > 0) { $states = $this->api->get_country_states($country_id); } else { $states = array(); }
        if(isset($city_id) && $city_id > 0) { $cities = $this->api->get_state_cities($state_id); } else { $cities = array(); }       
        if(isset($cat_type_id) && $cat_type_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }       

        $search_data = array(
          "cust_id" => 0,
          "country_id" => trim($country_id),
          "state_id" => trim($state_id),
          "city_id" => trim($city_id),
          "sort_by" => trim($sort_by),
          "cat_type_id" => trim($cat_type_id),
          "cat_id" => trim($cat_id),
          "delivered_in" => trim($delivered_in),
          "start_price" => trim($start_price),
          "end_price" => trim($end_price),
          "latest" => trim($latest),
          "active_status" => trim($active_status),
          "running_status" => trim($running_status),
          "device" => 'web',
          "last_id" => 0,
          "offer_pause_resume" => 'all',
        );
        $offers = $this->api->get_service_provider_offers_filtered($search_data);
        //echo json_encode($this->db->last_query()); die();
        //echo json_encode($offers); die();
        $this->load->view('services/favorite_offers_list_view', compact('offers','filter','countries','category_types','country_id','state_id','city_id','sort_by','cat_type_id','cat_id','delivered_in','start_price','end_price','latest','active_status','running_status','states','cities','categories','favorite_offers','favorite_offer_ids'));
      }
      //echo json_encode($this->db->last_query()); die();
      $this->load->view('user_panel/footer');
    }
    public function offer_add_to_favorite()
    {
      $offer_id = $this->input->post('id'); $offer_id = (int) $offer_id;
      $cust_id = $this->cust['cust_id']; $cust_id = (int) $cust_id;
      $offer_details = $this->api->get_service_provider_offer_details($offer_id);
      $insert_data = array(
        "cust_id" => $cust_id,
        "offer_id" => $offer_id,
        "provider_id" => $offer_details['cust_id'],
        "cre_datetime" => date('Y-m-d H:i:s')
      );
      if( $this->api->offer_add_to_favorite($insert_data) ) { 
        //update offer favorite count
        $update_data = array( "favorite_counts" => ($offer_details['favorite_counts'] + 1) );
        $this->api->update_offer_details($offer_id, $update_data);
        echo 'success'; 
      } else { echo 'failed'; }
    }
    public function offer_remove_from_favorite()
    {
      $offer_id = $this->input->post('id'); $offer_id = (int) $offer_id;
      $cust_id = $this->cust['cust_id']; $cust_id = (int) $cust_id;
      if( $this->api->offer_remove_from_favorite($offer_id, $cust_id) ) { 
        //update offer favorite count
        $offer_details = $this->api->get_service_provider_offer_details($offer_id);
        $update_data = array( "favorite_counts" => ($offer_details['favorite_counts'] - 1) );
        $this->api->update_offer_details($offer_id, $update_data);
        echo 'success'; 
      } else { echo 'failed'; }
    }
    public function my_favorite_job()
    {
      $jobs = $this->api->get_my_favorite_jobs($this->cust_id);
      // $jobs = $this->api->get_job_list($data);
      $category_types = $this->api->get_category_types();
      $fav_jobs = $this->api->get_favorite_jobs($this->cust_id);
      $fav_projects = array();
      foreach ($fav_jobs as $fav_key) {
        array_push($fav_projects, $fav_key['job_id']);
      }
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }

      $this->load->view('services/my_favorite_jobs_list', compact('jobs','fav_projects'));
      $this->load->view('user_panel/footer');
    }
    public function make_job_favourite()
    {
      $job_id = $this->input->post('id');
      $job_details = $this->api->get_customer_job_details($job_id);
      if($this->api->register_new_favourite_jobs($job_id, $job_details['cust_id'], $this->cust_id)){ echo 'success'; } else{ echo 'failed'; }
    }
    public function job_remove_from_favorite()
    {
      $job_id = $this->input->post('id');
      $job_details = $this->api->get_customer_job_details($job_id);
      if($this->api->remove_favourite_job($job_id, $job_details['cust_id'], $this->cust_id)){ echo 'success'; } else{ echo 'failed'; }
    }
    public function my_favorite_freelancers()
    {
      $project_5= null;
      $project_50=null;
      $country_id=null;
      $city_id=null;
      $state_id=null;
      $states=null;
      $cities=null;
      $rate_from=null;
      $rate_to=null;
      $fav_freelancer = array();
      $filter = 'basic';
      $country = $this->api->get_countries();
      $cust_id = $this->cust_id;
      if($freelancers = $this->api->get_favourite_freelancers($this->cust_id))
      {
        for($i=0; $i<sizeof($freelancers); $i++)
        {
          $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
          $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
          // echo json_encode($completed);
          $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
          if($completed['total_job'] != "0"){
           $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
          }else{$percent = 0;}  
          $freelancers[$i]['completed_job'] = (int)$completed['total_job'];
          $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
          $freelancers[$i]['job_percent'] = $percent;
          $freelancers[$i]['portfolio'] = (int)$portfolio;
          $freelancers[$i]['project_5'] = "yes";
          if($project_5 != null ){
            if((int)$project_5 < (int)$freelancers[$i]['completed_job']){
              $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
            }
          }
          if($project_50 != null ){
            if((int)$project_50 < (int)$freelancers[$i]['completed_job']){
              $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
            }
          }
        }

        $favorite_freelancer = $this->api->get_favorite_freelancer($this->cust_id);
        // echo json_encode($freelancers);die();
        $fav_freelancer = array();
        foreach ($favorite_freelancer as $fav_key) {
          array_push($fav_freelancer, $fav_key['provider_id']);
        }
      }
      $this->load->view('services/favorite_freelancers_list_view', compact('freelancers','fav_freelancer'));
      $this->load->view('user_panel/footer');     
    }
    public function make_freelancer_favourite()
    {
      $provider_id = $this->input->post('id');
      if($this->api->register_new_favourite_freelancer($this->cust_id, $provider_id)){ echo 'success'; } else{ echo 'failed'; }
    }
    public function freelancer_remove_from_favorite()
    {
      $provider_id = $this->input->post('id');
      if($this->api->remove_favourite_freelancer($this->cust_id, $provider_id)){ echo 'success'; } else{ echo 'failed'; }
    }
  //End Favorite Section------------------------------------  

  //Start Provider Jobs------------------------------------
    public function provider_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => 0,
        "provider_id" => $cust_id,
        "job_status" => 'open',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $job_list = $this->api->get_jobs_list($search_data);
      $dispute_master = $this->api->get_dispute_category_master($search_data);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($job_list); die();
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();
      
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
      //echo json_encode($job_list); die();
      $this->load->view('services/provider-jobs-list-view', compact('job_list','filter','category_types','cat_id','cat_type_id','start_date','end_date','categories','cancellation_reasons','dispute_master'));
      $this->load->view('user_panel/footer');
    }
    public function provider_inprogress_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => 0,
        "provider_id" => $cust_id,
        "job_status" => 'in_progress',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $job_list = $this->api->get_jobs_list($search_data);
      $dispute_master = $this->api->get_dispute_category_master($search_data);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($job_list); die();
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();
      
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
      //echo json_encode($job_list); die();
      for($i=0; $i<sizeof($job_list); $i++) {
        if($job_list[$i]['service_type'] == 0){
          if($proposal_milestone = $this->api->get_proposal_milestone_by_proposal_id($job_list[$i]['proposal_id'])){
            $job_list[$i]['milestone_available'] = "yes";
          }else{
            $job_list[$i]['milestone_available'] = "no";
          }  
          
          if($this->api->get_milestone_payment_request($job_list[$i]['job_id'])){
            $job_list[$i]['milestone_request'] = "yes";
          }else{
            $job_list[$i]['milestone_request'] = "no";
          }
        }
      }
      // echo json_encode($job_list); die();
      $this->load->view('services/provider-inprogress-jobs-list-view', compact('job_list','filter','category_types','cat_id','cat_type_id','start_date','end_date','categories','cancellation_reasons','dispute_master'));
      $this->load->view('user_panel/footer');
    }
    public function provider_completed_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => 0,
        "provider_id" => $cust_id,
        "job_status" => 'completed',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $job_list = $this->api->get_jobs_list($search_data);
      $dispute_master = $this->api->get_dispute_category_master($search_data);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($job_list); die();
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();
      
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
      //echo json_encode($job_list); die();
      $this->load->view('services/provider-completed-jobs-list-view', compact('job_list','filter','category_types','cat_id','cat_type_id','start_date','end_date','categories','cancellation_reasons','dispute_master'));
      $this->load->view('user_panel/footer');
    }
    public function provider_cancelled_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $filter = $this->input->get('filter', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => 0,
        "provider_id" => $cust_id,
        "job_status" => 'cancelled',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $job_list = $this->api->get_jobs_list($search_data);
      $dispute_master = $this->api->get_dispute_category_master($search_data);
      //echo json_encode($this->db->last_query()); die();
      //echo json_encode($job_list); die();
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();
      
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
      //echo json_encode($job_list); die();
      $this->load->view('services/provider-cancelled-jobs-list-view', compact('job_list','filter','category_types','cat_id','cat_type_id','start_date','end_date','categories','cancellation_reasons','dispute_master'));
      $this->load->view('user_panel/footer');
    }
    public function get_dispute_sub_category()
    {
      $dispute_cat_id = $this->input->post("dispute_cat_id");
      echo json_encode($this->api->get_dispute_sub_category_cat_id($dispute_cat_id));
    }
    public function create_dispute_for_job()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $dispute_title = $this->input->post('dispute_title', TRUE);
      $dispute_cat_id = $this->input->post('dispute_cat_id', TRUE); $dispute_cat_id = (int) $dispute_cat_id;
      $dispute_sub_cat_id = $this->input->post('dispute_sub_cat_id', TRUE); $dispute_sub_cat_id = (int) $dispute_sub_cat_id;
      $dispute_desc = $this->input->post('dispute_desc', TRUE);
      $request_money_back = $this->input->post('request_money_back', TRUE);
      $request_money_back = ($request_money_back == 'on') ? 1 : 0;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');
      $job_details = $this->api->get_job_details($job_id);

      $insert_data = array(
        "cancellation_status" => 'dispute_raised',
      );
      if($this->api->update_job_details($job_id, $insert_data)) {
        $update_data = array(
          "dispute_cat_id" => $dispute_cat_id,
          "dispute_sub_cat_id" => $dispute_sub_cat_id,
          "dispute_title" => trim($dispute_title),
          "dispute_desc" => trim($dispute_desc),
          "service_id" => $job_id,
          "service_type" => $job_details['service_type'],
          "cre_datetime" => $today,
          "cust_id" => $job_details['cust_id'],
          "provider_id" => $job_details['provider_id'],
          "dispute_status" => 'open',
          "dispute_withdraw_datetime" => 'NULL',
          "dispute_in_progress_datetime" => 'NULL',
          "dispute_closed_datetime" => 'NULL',
          "admin_id" => 0,
          "dispute_created_by" => $cust_id,
          "request_money_back" => $request_money_back,
          "money_back_status" => 0,
          "money_back_datetime" => 'NULL',
        );
        $dispute_id = $this->api->create_job_dispute($update_data);

        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $pro_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your offer purchase ID').": ".$job_id." ".$this->lang->line('has been marked as disputed').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job marked disputed'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.", ".$this->lang->line('is marked as disputed and send to Gonagoo admin for process').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job marked disputed'), trim($message));
        //------------------------------------------
        
        //update to workroom
        $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('This job has been marked as disputed and send to Gonagoo admin for process');
        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'dispute_raised',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job dispute request sent to Gonagoo admin.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to request for job dispute. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url),'refresh');
    }
    public function accept_cancellation_request()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "cancellation_status" => 'dispute_closed',
        "job_status" => 'cancelled',
        "job_status_datetime" => $today,
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $pro_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your offer purchase ID').": ".$job_id." ".$this->lang->line('has been cancelled').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer purchase cancellation.'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase cancellation.'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------

        //Notifications to Provider-----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.", ".$this->lang->line('is cancelled').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer purchase cancellation.'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Offer purchase cancellation.'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------
        
        $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Offer purchased on')." ".$job_details['cre_datetime'].", ".$this->lang->line('has been cancelled').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'job_cancelled',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        if($job_details['complete_paid'] == 1) {
          //Payment Section---------------------------------
            //Deduct Payment From Provider Escrow-----------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$job_details['provider_id'],
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
              }
              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($job_details['offer_total_price']);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$job_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'standard_price',
                "amount" => trim($job_details['offer_total_price']),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp deduct Payment From Provider Escrow-----------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$job_details['provider_id'],
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($job_details['offer_total_price']);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$job_details['provider_id'],
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($job_details['offer_total_price']),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp deduct Payment From Provider Escrow-----------
            //----------------------------------------------

            //Deduct Payment From Gonagoo Account-----------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) - trim($job_details['offer_total_price']);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => $job_details['cust_id'],
                "type" => 0,
                "transaction_type" => 'standard_price',
                "amount" => trim($job_details['offer_total_price']),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------

            //Add Payment To Customer Account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($job_details['offer_total_price']);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $job_details['cust_id'], $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $job_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($job_details['offer_total_price']),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              // Smp add Payment To Customer Account---------------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($job_details['offer_total_price']);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $job_details['cust_id'], $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $job_details['cust_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($job_details['offer_total_price']),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              // Smp add Payment To Customer Account---------------

            //----------------------------------------------
          //Payment Section---------------------------------
        }

        $this->session->set_flashdata('success', $this->lang->line('Job has been cancelled.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to cancel the job. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function provider_withdraw_dispute()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');
      $job_details = $this->api->get_job_details($job_id);
      
      if($dispute_details = $this->api->get_job_dispute_details($job_id)) {
        if($dispute_details['dispute_status'] == 'open') {
          //echo json_encode($dispute_details); die();
          $insert_data = array(
            "cancellation_status" => 'cancel_request',
          );
          if($this->api->update_job_details($job_id, $insert_data)) {
            $update_data = array(
              "dispute_status" => 'withdrawn',
              "dispute_withdraw_datetime" => $today,
            );
            $this->api->update_job_dispute($dispute_details['dispute_id'], $update_data);

            $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
            $pro_details = $this->api->get_user_details($cust_id);
            $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
            
            if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
              $customer_name = explode("@", trim($customer_details['email1']))[0];
            } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

            if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
              $provider_name = explode("@", trim($provider_details['email_id']))[0];
            } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

            //Notifications to customers----------------
              $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job').": ".$job_id." ".$this->lang->line('dispute has been withdrawn by provider').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
              //Send Push Notifications
              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
              if(!is_null($device_details_customer)) {
                $arr_customer_fcm_ids = array();
                $arr_customer_apn_ids = array();
                foreach ($device_details_customer as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Job dispute withdrawn'), 'type' => 'job-dispute-withdrawn', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
                //APN
                $msg_apn_customer =  array('title' => $this->lang->line('Job dispute withdrawn'), 'text' => $message);
                if(is_array($arr_customer_apn_ids)) { 
                  $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
                }
              }
              //SMS Notification
              $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
              $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

              //Email Notification
              $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job dispute withdrawn'), trim($message));
            //------------------------------------------

            //Notifications to Provider----------------
              $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job dispute has been withdrawn').". ";
              //Send Push Notifications
              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
              if(!is_null($device_details_provider)) {
                $arr_provider_fcm_ids = array();
                $arr_provider_apn_ids = array();
                foreach ($device_details_provider as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Job dispute withdrawn'), 'type' => 'job-dispute-withdrawn', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                //APN
                $msg_apn_provider =  array('title' => $this->lang->line('Job dispute withdrawn'), 'text' => $message);
                if(is_array($arr_provider_apn_ids)) { 
                  $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                }
              }
              //SMS Notification
              $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
              $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

              //Email Notification
              $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job dispute withdrawn'), trim($message));
            //------------------------------------------
            
            //update to workroom
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Job dispute has been withdrawn');
            if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
              $update_data = array(
                'workstream_id' => (int)$workstream_details['workstream_id'],
                'job_id' => (int)$job_id,
                'cust_id' => (int)$job_details['cust_id'],
                'provider_id' => (int)$job_details['provider_id'],
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $cust_id,
                'type' => 'dispute_withdraw',
                'file_type' => 'text',
                'proposal_id' => 0,
              );
              $this->api->update_to_workstream_details($update_data);
            }
            $this->session->set_flashdata('success', $this->lang->line('Job dispute withdraw successfully.'));
          } else { $this->session->set_flashdata('error', $this->lang->line('Unable to withdraw job dispute. Try again...')); }
        } else { $this->session->set_flashdata('error', $this->lang->line('Unable to withdraw job dispute. Try again...')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('You are not allowed to withdraw dispute, already taken by Gonagoo admin')); }
      redirect(base_url('user-panel-services/'.$redirect_url),'refresh');
    }
    public function provider_mark_job_complete()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "complete_status" => 'request_complete',
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $pro_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been marked completed by')." ".$provider_name.". ".$this->lang->line('Kindly approve the same for job payment processing')." ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job completion request'), 'type' => 'job-completion-request', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job completion request'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job completion request'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your job has been marked as completed and send to')." ".$customer_name.", ".$this->lang->line('for approval').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job completion request'), 'type' => 'job-completion-request', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job completion request'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job completion request'), trim($message));
        //------------------------------------------
          
        $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('The job has been marked completed on')." ".$today.", ".$this->lang->line('Kindly approve the same for job payment processing').". ";

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'mark_completed',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job marked as completed.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to mark completed. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function provider_reply_to_review()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $review_id = $this->input->post('review_id', TRUE); $review_id = (int) $review_id;
      $offer_code = $this->input->post('offer_code', TRUE);
      $review_reply = $this->input->post('review_reply', TRUE);
      $rating_customer = $this->input->post('rating_customer', TRUE); $rating_customer = (int) $rating_customer;
      $today = date('Y-m-d H:i:s');
      //Update job review
      $update_data = array(
        "review_reply" => trim($review_reply),
        "reply_datetime" => $today,
      );
      //echo json_encode($update_data); die();
      if($this->api->update_job_review($review_id, $update_data)) {
        $review_details = $this->api->get_review_details($review_id);

        $job_details = $this->api->get_job_details($review_details['job_id']);
        $message = $this->lang->line('Hello')." ".$job_details['cust_name'].", ".$review_reply;
        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_details['job_id'])) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_details['job_id'],
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'customer_rating',
            'file_type' => 'text',
            'proposal_id' => 0,
            'ratings' => $rating_customer,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        //Update customer rating
        $cust_details = $this->api->get_user_details($job_details['cust_id']);
        $no_of_ratings = $cust_details['cust_no_of_ratings']+1;
        $total_ratings = $cust_details['cust_total_ratings']+$rating_customer;
        $ratings = round($total_ratings/$no_of_ratings,1);
        $update_data = array(
          "cust_ratings" => $ratings,
          "cust_total_ratings" => $total_ratings,
          "cust_no_of_ratings" => $no_of_ratings
        );
        $this->api->update_provider_rating($job_details['cust_id'], $update_data);

        $this->session->set_flashdata('success', $this->lang->line('Job marked as completed.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to mark completed. Try again...')); }
      redirect(base_url('user-panel-services/provider-offer-details/'.$offer_code), 'refresh');
    }
  //Start Provider Jobs------------------------------------  
  
  //zaid starts here

  //Start Customer Jobs post------------------------------- 
    public function post_customer_job()
    {
      // echo json_encode(isset($_POST) && !empty($_POST));die();
      $res=array(
        'job_title'=>"",
        'category'=>"",
        'sub_category'=>"",
        'work_type'=>"",
        'budget'=>"",
        'description'=>"",
        'expiry_date'=>"",
        'billing_period'=>"",
      );
      if(isset($_POST) && !empty($_POST)){
        $res=array(
          'job_title'=>$_POST['job_title'],
          'category'=>$_POST['category'],
          'sub_category'=>$_POST['sub_category'],
          'work_type'=>$_POST['work_type'],
          'budget'=>$_POST['budget'],
          'description'=>$_POST['description'],
          'expiry_date'=>$_POST['expiry_date'],
          'billing_period'=>$_POST['billing_period'],
        );
      }
      $category_types = $this->api->get_category_types();
      $countries = $this->api->get_countries();
      $user_details = $this->api->get_user_details($this->cust_id);
      $currency_details = $this->api->get_country_currency_detail($user_details['country_id']);
      // echo json_encode($currency_details); die();
      $this->load->view('services/customer_post_job',compact('res','currency_details','category_types','countries'));
      $this->load->view('user_panel/footer');
    }
    public function post_customer_job_old()
    {
      $category_types = $this->api->get_category_types();
      $countries = $this->api->get_countries();
      $user_details = $this->api->get_user_details($this->cust_id);
      $currency_details = $this->api->get_country_currency_detail($user_details['country_id']);
      // echo json_encode($currency_details); die();
      $this->load->view('services/customer_post_job',compact('currency_details','category_types','countries'));
      $this->load->view('user_panel/footer');
    }
    public function get_sub_category()
    {
      $cat_type_id = $this->input->post('cat_type_id', TRUE);
      $sub_category = $this->api->get_category_by_id($cat_type_id);
      echo json_encode($sub_category);
    }
    public function get_questions_know_freelancer()
    {
      $cat_type_id = $this->input->post('cat_type_id', TRUE);
      $questions = $this->api->get_questions_know_freelancer($cat_type_id);
      echo json_encode($questions);
    }
    public function register_customer_job()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $job_title = $this->input->post('job_title', TRUE);
      $cat_id = $this->input->post('category', TRUE);
      $sub_cat_id = $this->input->post('sub_category', TRUE);
      $job_desc = $this->input->post('description', TRUE);
      $currency_code = $this->input->post('currency_sign', TRUE);
      $currency_id = $this->input->post('currency_id', TRUE);
      $work_type = $this->input->post('work_type', TRUE);
      $budget = $this->input->post('budget', TRUE);
      $customer_details = $this->api->get_user_details($cust_id);
      $location_type = $this->input->post('work_from', TRUE);
      $expiry_date = $this->input->post('expiry_date', TRUE);
      $billing_period = $this->input->post('billing_period', TRUE);
      if($expiry_date == ""){
        $expiry_date = "NULL";
      }
      if($billing_period == "" && $work_type != "per hour"){
        $billing_period = "NULL";
      }
      if($location_type == "On-site (specific location)"){
        $address = $this->input->post('city_name', TRUE);
        $starting_date = $this->input->post('starting_date', TRUE);
        $starting_time = $this->input->post('starting_time', TRUE);
        $start_datetime = $starting_date." ".$starting_time;
        $hours = $this->input->post('hours', TRUE);
        $hours_per_duration = $this->input->post('hours_per', TRUE);
      }else{
        $address = "";
        $starting_date = "";
        $starting_time = "";
        $start_datetime = "";
        $hours = "";
        $hours_per_duration = "";
      }
      $country_id = $this->input->post('country_id', TRUE);
      if($country_id == ""){
        $country_name = $this->input->post('country_location', TRUE);
        if($country_name == ""){
          $country_id = $customer_details['country_id'];
        }else{
          $country_id = $this->api->get_country_id_by_name($country_name);
        }
      }
      $visibility = $this->input->post('visibility', TRUE);
      $question_1 = $this->input->post('question1', TRUE);
      if($question_1 == "Create your own question"){
        $question_1 = $this->input->post('question1_other', TRUE);
      }
      $question_2 = $this->input->post('question2', TRUE);
      if($question_2 == "Create your own question"){
        $question_2 = $this->input->post('question2_other', TRUE);
      }
      $question_3 = $this->input->post('question3', TRUE);
      if($question_3 == "Create your own question"){
        $question_3 = $this->input->post('question3_other', TRUE);
      }
      $question_4 = $this->input->post('question4', TRUE);
      if($question_4 == "Create your own question"){
        $question_4 = $this->input->post('question4_other', TRUE);
      }
      $question_5 = $this->input->post('question5', TRUE);
      if($question_5 == "Create your own question"){
        $question_5 = $this->input->post('question5_other', TRUE);
      }
      $project_duration = $this->input->post('project_duration', TRUE);
      if($project_duration == "other"){
        $project_duration = $this->input->post('project_duration_other', TRUE);
      }
      $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']);
      $files = $_FILES;
      $cpt = count($_FILES['image_url']['name']); //die();
      for($i=0; $i <= $cpt; $i++)
      {
        if(isset($files['image_url']['name'][$i])) {
          $_FILES['image_url']['name']= $files['image_url']['name'][$i];
          $_FILES['image_url']['type']= $files['image_url']['type'][$i];
          $_FILES['image_url']['tmp_name']= $files['image_url']['tmp_name'][$i];
          $_FILES['image_url']['error']= $files['image_url']['error'][$i];
          $_FILES['image_url']['size']= $files['image_url']['size'][$i];

          $config['upload_path']    = 'resources/m4-image/jobs/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 

          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url')) {
            $uploads    = $this->upload->data();

            ${"image_url_$i"} =  $config['upload_path'].$uploads['file_name'];
          } else { ${"image_url_$i"} = 'NULL'; }
        } else { ${"image_url_$i"} = 'NULL'; }
      }
      $cpt++;
      for ($i=$cpt; $i <= 6; $i++) { 
        ${"image_url_$i"} = 'NULL';
      }
      $today = date('Y-m-d h:i:s');
      $insert_data = array(
        "job_title" => trim($job_title),
        "cust_id" => trim($cust_id),
        "cat_id" => trim($cat_id),
        "sub_cat_id" => trim($sub_cat_id),
        "job_desc" => trim($job_desc),
        "attachment_1" => trim(${"image_url_0"}),
        "attachment_2" => trim(${"image_url_1"}),
        "attachment_3" => trim(${"image_url_2"}),
        "attachment_4" => trim(${"image_url_3"}),
        "attachment_5" => trim(${"image_url_4"}),
        "attachment_6" => trim(${"image_url_5"}),
        "work_type" => trim($work_type),
        "currency_id" => trim($currency_id),
        "currency_code" => trim($currency_code),
        "budget" => trim($budget),
        "location_type" => trim($location_type),
        "address" => trim($address),
        "start_datetime" => trim($start_datetime),
        "hours" => trim($hours),
        "hours_per_duration" => trim($hours_per_duration),
        "visibility" => trim($visibility),
        "project_duration" => trim($project_duration),
        "question_1" => trim($question_1),
        "question_2" => trim($question_2),
        "question_3" => trim($question_3),
        "question_4" => trim($question_4),
        "question_5" => trim($question_5),
        "country_id" => trim($country_id),
        "cre_datetime" => trim($today),
        "contract_expired_on" => $expiry_date,
        "billing_period" => $billing_period,
        "cust_name" => $customer_name,
        "cust_contact" => $customer_details['mobile1'],
        "cust_email" => $customer_details['email1'],
      );
      if($job_id = $this->api->register_job_details($insert_data)){

          $job_details = $this->api->get_customer_job_details($job_id);
          $customer_details = $this->api->get_user_details($cust_id);
        
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job posted').": ".$job_id;
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Job post'), 'type' => 'job post', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Job post'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
          //Notifications to customers----------------

          //Update to workstream master
          $update_data = array(
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => 0,
            'cre_datetime' => $today,
            'status' => 1,
          );
          if($id = $this->api->check_workstream_master($update_data)){
            $workstream_id = $id;
          }else{
            $workstream_id = $this->api->update_to_workstream_master($update_data);
          }
          $update_data = array(
            'workstream_id' => (int)$workstream_id,
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => 0,
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'job_started',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        redirect(base_url('user-panel-services/list-of-freelancers/'.$job_id),'refresh');
      }
    }
    public function list_of_freelancers()
    {
      $job_id = $this->uri->segment(3);
      $job_details = $this->api->get_customer_job_details($job_id);
      $skill = $this->api->get_category_details_by_id($job_details['sub_cat_id']);
      $country = $this->api->get_countries();
      
      $data = array(
        'skill' => $skill['cat_name'] , 
        'fav' => "no",
        'country_id' => "NULL",
        'state_id' => "NULL",
        'city_id' => "NULL",
        'rate_from' => "NULL",
        'rate_to' => "NULL",
        'cust_id' => $this->cust_id
      );
      $freelancers = $this->api->list_of_freelancers($data);
      // echo $this->db->last_query(); die();
      // echo json_encode($freelancers); die();


      for($i=0; $i<sizeof($freelancers); $i++) {
        $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
        $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
        $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
        if($completed['total_job'] != "0"){
         $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
        }else{$percent = 0;}  
        $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
        $freelancers[$i]['job_percent'] = $percent;
        $freelancers[$i]['portfolio'] = (int)$portfolio;
        $freelancers[$i]['project_5'] = "yes";
      }
      //echo json_encode($freelancers); die();
      $favorite_freelancer = $this->api->get_favorite_freelancer($this->cust_id);
      $fav_freelancer = array();
      foreach ($favorite_freelancer as $fav_key) {
        array_push($fav_freelancer, $fav_key['provider_id']);
      }

      $filter = 'basic';
      $is_fav = 'no';
      //echo json_encode($freelancers); die();
      $this->load->view('services/recomended_freelancers_profile', compact('country','fav_freelancer','freelancers','filter','job_details','is_fav'));
      $this->load->view('user_panel/footer');
    }
    public function filter_list_of_freelancers()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id');
      $country_id = $this->input->post('country_id');
      $state_id = $this->input->post('state_id');
      $city_id = $this->input->post('city_id');
      $project_5 = $this->input->post('project_5');
      $project_50 = $this->input->post('project_50');
      $rate_from = $this->input->post('rate_from');
      $rate_to = $this->input->post('rate_to');
      $filter = $this->input->post('filter');
      $country = $this->api->get_countries();
      $states = $this->api->get_state_by_country_id($country_id);
      $cities = $this->api->get_city_by_state_id($state_id);
      // echo json_encode($states); die();
      $job_details = $this->api->get_customer_job_details($job_id);
      $skill = $this->api->get_category_details_by_id($job_details['sub_cat_id']);
      $data = array(
        'skill' => $skill['cat_name'] ,
        'fav' => "no",
        'country_id' => $country_id,
        'state_id' => $state_id,
        'city_id' => $city_id,
        'rate_from' => (int)$rate_from,
        'rate_to' => (int)$rate_to,
        'cust_id' => $this->cust_id
      );
      $freelancers = $this->api->list_of_freelancers($data);
      
      for($i=0; $i<sizeof($freelancers); $i++) {
        $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
        $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'completed');
        $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
        if($completed['total_job'] != "0"){
         $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
        }else{$percent = 0;}  
        $freelancers[$i]['completed_job'] = (int)$completed['total_job'];
        $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
        $freelancers[$i]['job_percent'] = $percent;
        $freelancers[$i]['portfolio'] = (int)$portfolio;
        $freelancers[$i]['project_5'] = "yes";
        if($project_5 != null ){
          if((int)$project_5 < (int)$freelancers[$i]['completed_job']){
            $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
          }
        }
        if($project_50 != null ){
          if((int)$project_50 < (int)$freelancers[$i]['completed_job']){
            $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
          }
        }
      }//for
      //echo json_encode($freelancers); die();
      $favorite_freelancer = $this->api->get_favorite_freelancer($this->cust_id);
      $fav_freelancer = array();
      foreach ($favorite_freelancer as $fav_key) {
        array_push($fav_freelancer, $fav_key['provider_id']);
      }
      $is_fav = 'no';
      $this->load->view('services/recomended_freelancers_profile', compact('country','fav_freelancer','freelancers','filter','job_details','country_id','city_id','state_id','is_fav','states','cities','project_5','project_50','rate_from','rate_to'));
      $this->load->view('user_panel/footer');
    }
    public function list_of_fav_freelancers()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id');
      $job_details = $this->api->get_customer_job_details($job_id);
      $country = $this->api->get_countries();
      // $skill = $this->api->get_category_details_by_id($job_details['sub_cat_id']);
      
      $data = array(
        'fav' => "yes" , 
        'skill' => "NULL",
        'country_id' => "NULL",
        'state_id' => "NULL",
        'city_id' => "NULL",
        'rate_from' => "NULL",
        'rate_to' => "NULL",
        'cust_id' => $this->cust_id
      );
       $freelancers = $this->api->list_of_freelancers($data);
      
       //echo json_encode($freelancers); die();

      
      for($i=0; $i<sizeof($freelancers); $i++) {
        $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
        $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
        $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
        if($completed['total_job'] != "0"){
         $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
        }else{$percent = 0;}  
        $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
        $freelancers[$i]['job_percent'] = $percent;
        $freelancers[$i]['portfolio'] = (int)$portfolio;
        $freelancers[$i]['project_5'] = "yes"; 
      }
      //echo json_encode($freelancers); die();
      $favorite_freelancer = $this->api->get_favorite_freelancer($this->cust_id);
      $fav_freelancer = array();
      foreach ($favorite_freelancer as $fav_key) {
        array_push($fav_freelancer, $fav_key['provider_id']);
      }

      $filter = 'basic';
      $is_fav = 'yes';

      $this->load->view('services/recomended_freelancers_profile', compact('country','fav_freelancer','freelancers','filter','job_details','is_fav'));
      $this->load->view('user_panel/footer');
    }
    public function customer_job_details_view()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id'); $job_id = (int)$job_id;
      $redirect_url = $this->input->post('redirect_url');
      $job_details = $this->api->get_customer_job_details($job_id);
      $customer_details = $this->api->get_user_details($job_details['cust_id']);

      if($job_details['service_type'] == 0){
        $proposals = $this->api->get_job_proposals_list($job_id);
        $job_details['proposal_counts'] = sizeof($proposals);
      }
      //echo json_encode($job_details); die();
      $this->load->view('services/customer_job_details_view',compact('job_details', 'customer_details', 'redirect_url', 'cust_id'));
      $this->load->view('user_panel/footer');
    }
  //End Customer Jobs post---------------------------------

  //Start Job proposals------------------------------------
    public function send_proposal_view()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id');
      $redirect_url = $this->input->post('redirect_url');
      $job_details = $this->api->get_customer_job_details($job_id);
      $country_currency = $this->api->get_country_id_by_currency_id($job_details['currency_id']);
      $payment_percent = $this->api->get_job_advance_payment_percent($country_currency['country_id'] , $job_details['cat_id'] , $job_details['sub_cat_id']);
      // echo json_encode($country_currency['country_id']);die();
      $job_customer_profile = $this->api->get_user_details($job_details['cust_id']);
      $freelancers_profile = $this->api->get_service_provider_profile($this->cust_id);
      // echo json_encode($freelancers_profile);die();
      $datetime1 = new DateTime(date('Y-m-d h:i:s'));
      $datetime2 = new DateTime($job_details['cre_datetime']);
      $difference = $datetime1->diff($datetime2);
      $ending = 30-$difference->d;
      $last_job = $this->api->get_last_job_of_customer($job_customer_profile['cust_id'], 'last');
      $completed_job = $this->api->get_last_job_of_customer($job_customer_profile['cust_id'] , 'completed_count');
      $fav_jobs = $this->api->get_favorite_jobs($this->cust_id);
      $fav_projects = array();
      foreach ($fav_jobs as $fav_key) {
        array_push($fav_projects, $fav_key['job_id']);
      }
      $proposals = $this->api->get_job_proposals_list($job_details['job_id']);
      for($i=0; $i<sizeof($proposals); $i++){
        $provider_details = $this->api->get_user_details($proposals[$i]['provider_id']);
        $proposals[$i]['provider_details'] = $provider_details;
      }
      //echo json_encode($job_details); die();
      $job_details['proposal_counts'] = sizeof($proposals);
      if($job_details['job_type']){

      $this->load->view('services/send_trouble_proposal_view', compact('job_details','fav_projects','job_customer_profile','ending','last_job','completed_job','proposals','fav_projects','freelancers_profile','payment_percent','redirect_url'));
      }else{

      $this->load->view('services/send_job_proposal_view', compact('job_details','fav_projects','job_customer_profile','ending','last_job','completed_job','proposals','fav_projects','freelancers_profile','payment_percent','redirect_url'));
      }
      $this->load->view('user_panel/footer');
    }
    public function send_proposal()
    {
      // echo json_encode($_POST); die();
      $today = date('Y-m-d h:i:s');
      $job_id = $this->input->post('job_id');
      $country_id = $this->input->post('country_id');
      $total_price = $this->input->post('total');
      $deposit_price = $this->input->post('deposit');
      $description = $this->input->post('description', TRUE);
      $answer_1 = $this->input->post('answer_1', TRUE);
      $answer_2 = $this->input->post('answer_2', TRUE);
      $answer_3 = $this->input->post('answer_3', TRUE);
      $answer_4 = $this->input->post('answer_4', TRUE);
      $answer_5 = $this->input->post('answer_5', TRUE);
      $answer_5 = $this->input->post('answer_5', TRUE);
      $notif_me = $this->input->post('notif_me', TRUE);
      if(!isset($answer_1)){$answer_1 = "";}
      if(!isset($answer_2)){$answer_2 = "";}
      if(!isset($answer_3)){$answer_3 = "";}
      if(!isset($answer_4)){$answer_4 = "";}
      if(!isset($answer_5)){$answer_5 = "";}      
      if($notif_me == "on"){$notif_me = 1;}else{$notif_me = 0;}      
      //echo json_encode($notif_me); die();
      $job_details = $this->api->get_customer_job_details($job_id);
      $files = $_FILES;
      $cpt = count($_FILES['image_url']['name']); //die();
      // echo json_encode($cpt);die();
      for($i=0; $i < $cpt; $i++)
      {
        $m=$i+1;
        // echo json_encode($i);
        if(isset($files['image_url']['name'][$i])) {
          $_FILES['image_url']['name']= $files['image_url']['name'][$i];
          $_FILES['image_url']['type']= $files['image_url']['type'][$i];
          $_FILES['image_url']['tmp_name']= $files['image_url']['tmp_name'][$i];
          $_FILES['image_url']['error']= $files['image_url']['error'][$i];
          $_FILES['image_url']['size']= $files['image_url']['size'][$i];

          $config['upload_path']    = 'resources/m4-image/jobs_proposal/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 

          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url')) {
            $uploads    = $this->upload->data();
            ${"image_url_$m"} =  $config['upload_path'].$uploads['file_name'];
          } else { 
            ${"image_url_$m"} = 'NULL'; 
          }
        } else {
         ${"image_url_$m"} = 'NULL';
          }
      }
      $cpt++;
      for ($i=$cpt; $i <= 5; $i++) { 
        ${"image_url_$i"} = 'NULL';
      }
      $proposal_breakdown = array();
      
      $j = 1;
      for($i=1; $i<=35; $i++){
        if($this->input->post('price'.$i, TRUE) != "" && $this->input->post('part'.$i, TRUE) != ""){
          $proposal_breakdown['price'.$j] = $this->input->post('price'.$i, TRUE);        
          $proposal_breakdown['part'.$j] = $this->input->post('part'.$i, TRUE);
          $j = $j+1;        
        }
      }
      
      $update_data = array(
        'job_id' => (int)$job_id,
        'cust_id' => $job_details['cust_id'],
        'provider_id' => $this->cust_id,
        'proposal_details' => $description,
        'attachment_1' => trim(${"image_url_1"}),
        'attachment_2' => trim(${"image_url_2"}) ,
        'attachment_3' => trim(${"image_url_3"}),
        'attachment_4' => trim(${"image_url_4"}),
        'attachment_5' => trim(${"image_url_5"}),
        'answer_1' => $answer_1,
        'answer_2' => $answer_2,
        'answer_3' => $answer_3,
        'answer_4' => $answer_4,
        'answer_5' => $answer_5,
        'total_price' => $total_price,
        'deposit_price' => $deposit_price,
        'currency_id' => $job_details['currency_id'],
        'currency_code' => $job_details['currency_code'],
        'country_id' => $country_id,
        'notify_proposal_closed' => $notif_me,
        'proposal_date' => $today,
        'proposal_status' => "open",
      );
      // echo json_encode($update_data);die();
      if($proposal_id = $this->api->register_job_proposal($update_data)){
      // if(true){
      // echo json_encode(sizeof($proposal_breakdown)/2);die();

        $provider_details = $this->api->get_service_provider_profile($this->cust_id);
        $balance =  $provider_details['proposal_credits']-1;
        $update_data = array(
          'proposal_credits' => $balance,
        );
        if($this->api->update_provider($update_data , $this->cust_id)){
          $provider_details = $this->api->get_service_provider_profile($this->cust_id);
          $update_data = array(
            'cust_id' => $this->cust_id,
            'type' => "deduct",
            'quantity' => 1,
            'balance' => $provider_details['proposal_credits'],
            'source_id' => $job_id,
            'source_type' => "proposal",
          );
          $this->api->insert_freelancer_proposal_credit_history($update_data);
        }


        for($i=1; $i<=(sizeof($proposal_breakdown)/2); $i++){    
          $update_data = array(
            'proposal_id' => (int)$proposal_id,
            'job_id' => $job_details['job_id'],
            'cust_id' => $job_details['cust_id'],
            'provider_id' => $this->cust_id,
            'milestone_details' => $proposal_breakdown['part'.$i],
            'milestone_price' => $proposal_breakdown['price'.$i],
            'currency_id' => $job_details['currency_id'],
            'currency_code' => $job_details['currency_code'],
            'country_id' => $country_id,
          );
          $milestone_id = $this->api->insert_proposal_milestone_details($update_data);
        }
        $customer_details = $this->api->get_user_details($job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile($this->cust_id);
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')){
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('A New Proposal from')." ".$provider_name.", ".$this->lang->line('Your Job ID').": ".$job_id;
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value){
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('A New Proposal from'), 'type' => 'proposal-received', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Proposal Received'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('You have to send proposal successfully');
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details($this->cust_id);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Proposal Send'), 'type' => 'proposal-received', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Proposal Received'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------
        $message = $this->lang->line('A New Proposal from')." ".$provider_name.", ".$this->lang->line('Your Job ID').": ".$job_id;

        //Update to workstream master
        $update_data = array(
          'job_id' => (int)$job_id,
          'cust_id' => $job_details['cust_id'],
          'provider_id' => $this->cust_id,
          'cre_datetime' => $today,
          'status' => 0,
        );
        if($id = $this->api->check_workstream_master($update_data)){
          $workstream_id = $id;
        }else{
          $workstream_id = $this->api->update_to_workstream_master($update_data);
        }
        $update_data = array(
          'workstream_id' => (int)$workstream_id,
          'job_id' => (int)$job_id,
          'cust_id' => $job_details['cust_id'],
          'provider_id' => $this->cust_id,
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $this->cust_id,
          'type' => 'job_proposal',
          'file_type' => 'text',
          'proposal_id' => 1,
        );
        $this->api->update_to_workstream_details($update_data);
        $this->session->set_flashdata('success',$this->lang->line('Proposal send successfully'));
      }else{
        $this->session->set_flashdata('error',$this->lang->line('Error on sending proposal'));
      }       
      redirect(base_url('user-panel-services/pending-job-proposals'),'refresh');
    }
    public function accept_job_proposal_payment_view()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $job_id = $this->input->post('job_id');
      $redirect_url = $this->input->post('redirect_url');
      $proposal_id = $this->input->post('proposal_id');
      $provider_id = $this->input->post('provider_id');
      if($job_details = $this->api->get_customer_job_details($job_id)){
        $proposal_details = $this->api->get_job_proposals_by_proposal_id($proposal_id);
        $customer_details = $this->api->get_user_details($cust_id);
        //echo json_encode($proposal_details); die();
        $this->load->view('services/job_payment_customer_view', compact('job_details','provider_id','redirect_url','cust_id','proposal_details','customer_details'));
        $this->load->view('user_panel/footer');
      }else{
        $this->session->set_flashdata('error',$this->lang->line('Job not found'));
        redirect(base_url('user-panel-services/job-workstream-list/'.$job_id),'refresh');
      }
    }    
    public function job_proposal_decline()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id');
      $proposal_id = $this->input->post('proposal_id');
      $provider_id = $this->input->post('provider_id');
      $redirect_url = $this->input->post('redirect_url');
      $reason = $this->input->post('reason');
      $other_desc = $this->input->post('other_desc');
      $today = date('Y-m-d h:i:s');
      $insert_data = array(
        'proposal_status' => "reject",
        'proposal_status_date' => $today,
        'proposal_status_updated_by' => "customer",
        'reject_reason' => $reason,
        'reject_desc' => $other_desc,
      );
      if($this->api->update_job_proposals($insert_data, $job_id , $provider_id)){
        $job_details = $this->api->get_customer_job_details($job_id);
        $cust_id = $job_details['cust_id'];
        $customer_details = $this->api->get_user_details($job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile((int)$provider_id);
        //echo json_encode($provider_details); die();
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your proposal decline by customer');
          //Send jush-Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$provider_id);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('proposal decline'), 'type' => 'proposal-decline', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('proposal decline'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('proposal decline'), trim($message));
        //------------------------------------------
        
       // $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Offer purchased on')." ".$job_details['cre_datetime'].", ".$this->lang->line('has been cancelled').". ".$this->lang->line('Cancellation Reason').": ".$job_details['cancellation_reason'].". ";

        $update_data = array(
          'job_id' => (int)$job_id,
          'cust_id' => $job_details['cust_id'],
          'provider_id' => $provider_id,
          'cre_datetime' => $today,
          'status' => 0,
        );

        if($id = $this->api->check_workstream_master($update_data)){
          $workstream_id = $id;
        }else{
          $workstream_id = $this->api->update_to_workstream_master($update_data);
        }
        $update_data = array(
          'workstream_id' => (int)$workstream_id,
          'job_id' => (int)$job_id,
          'cust_id' => $job_details['cust_id'],
          'provider_id' => $provider_id,
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $cust_id,
          'type' => 'job_decline',
          'file_type' => 'text',
          'proposal_id' => $proposal_id,
        );
        $this->api->update_to_workstream_details($update_data);

        $this->session->set_flashdata('success',$this->lang->line('proposal decline'));
      }else{
        $this->session->set_flashdata('error',$this->lang->line('error on declining'));
      }
      redirect(base_url('user-panel-services/job-workstream-list/'.$job_id),'refresh');
    }
  //End Job proposals--------------------------------------

  //Start customer job deposite payment--------------------
    public function job_payment_bank()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      
      $data = array(
        'payment_method' => 'bank', 
        'payment_mode' => 'bank', 
        'payment_by' => 'web', 
        'is_bank_payment' => 1,
        'job_status' => 'in_progress',
      );
      if( $this->api->job_details_update($job_id, $data) ) {
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);

        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Offer payment marked as bank payment. ID').': '.$job_id;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer payment marked as Bank'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Offer payment marked as Bank'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer payment marked as Bank'), trim($message));
        //------------------------------------------------
        //workstream entry--------------------------------
          $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'payment',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //workstream entry--------------------------------
        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer payment marked as Bank'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Offer payment marked as Bank'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment marked as Bank'), trim($message));
        //------------------------------------------------
        //Update offer sales count------------------------
          $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
          $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
          $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
        //------------------------------------------------
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
    public function job_payment_mtn()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $phone_no = $this->input->post('phone_no', TRUE); 
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      //echo json_encode($offer_total_price); die();
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$offer_total_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      $contents = curl_exec($ch);
      $mtn_pay_res = json_decode($contents, TRUE);
      //var_dump($mtn_pay_res); die();
      
      $payment_method = 'mtn';
      $transaction_id = '1234567890';
      //$transaction_id = trim($mtn_pay_res['TransactionID']);
      
      $payment_data = array();
      //if($mtn_pay_res['StatusCode'] === "01"){
      if(1){
        $data = array(
          'paid_amount' => $offer_total_price,
          'payment_method' => trim($payment_method), 
          'payment_mode' => 'online', 
          'payment_by' => 'web', 
          'transaction_id' => trim($transaction_id), 
          'payment_response' => trim($mtn_pay_res), 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1,
          'job_status' => 'in_progress',
        );
        if( $this->api->job_details_update($job_id, $data) ) {
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Customer Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
            $message = $this->lang->line('Payment completed for offer purchase. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase payment'), trim($message));
          //------------------------------------------------
          //Workstream entry--------------------------------
            $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
          //Provider Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
            $message = $this->lang->line('Payment received for offer. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer payment received'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment received'), trim($message));
          //------------------------------------------------

          //Payment Section---------------------------------
            //Add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
            //----------------------------------------------
            //Deduct Payment to customer account------------
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
            //----------------------------------------------
            //Add Payment To Gonagoo Account----------------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------
            //Add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$provider_id,
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$provider_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($offer_total_price),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
            //----------------------------------------------
          //Payment Section---------------------------------
          //Update offer sales count------------------------
            $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
            $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
            $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
          //------------------------------------------------
          $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      } else {  $this->session->set_flashdata('error', $this->lang->line('payment_failed')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
    public function job_payment_stripe()
    {
      // echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $proposal_id = $this->input->post('proposal_id', TRUE); $proposal_id = (int)$proposal_id;
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_customer_job_details($job_id);
      $proposal_details = $this->api->get_job_proposals_by_proposal_id($proposal_id);
      $provider_details = $this->api->get_service_provider_profile($provider_id);
      $search_data = array(
        "country_id" => $job_details['country_id'],
        "cat_id" => $job_details['sub_cat_id'],
        "cat_type_id" => $job_details['cat_id'],
      );
      $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);
      //echo json_encode($advance_payment); die();
      $gonagoo_commission_per = $advance_payment['gonagoo_commission'];
      $gonagoo_commission_amount = ($proposal_details['deposit_price']/100)*$gonagoo_commission_per;

      $job_deposit_price = trim($proposal_details['deposit_price']);
      $payment_method = 'stripe';
      
      $data = array(
        'budget' => $proposal_details['total_price'], 
        'provider_id' => $provider_id, 
        'proposal_accepted_datetime' => $today, 
        'open_for_proposal' => 1, 
        'proposal_id' => $proposal_id, 
        'proposal_price' => $proposal_details['total_price'], 
        'job_status' => 'in_progress', 
        'paid_amount' => $proposal_details['deposit_price'],
        'payment_method' => $payment_method, 
        'payment_mode' => 'online', 
        'payment_by' => 'web', 
        'transaction_id' => trim($transaction_id), 
        'payment_response' => trim($stripeEmail), 
        'payment_datetime' => trim($today),
        'balance_amount' =>(int)$proposal_details['total_price']-(int)$proposal_details['deposit_price'], 
        'complete_paid' => 0,
        'provider_name' => $provider_details['firstname']." ".$provider_details['lastname'], 
        'provider_company_name' => $provider_details['company_name'], 
        'provider_contact' => $provider_details['contact_no'], 
        'provider_email' => $provider_details['email_id'], 
        'gonagoo_commission_per' => $gonagoo_commission_per, 
        'gonagoo_commission_amount' => $gonagoo_commission_amount, 
        'status_updated_by' => 'customer', 
      );

      
      if($this->api->job_details_update($job_id, $data)){
        $job_details = $this->api->get_customer_job_details($job_id);

        $data = array( 
          'job_id' => $job_id, 
          'cust_id' => $cust_id, 
          'provider_id' => $provider_id,  
          'currency_id' => $job_details['currency_id'], 
          'currency_code' => $job_details['currency_code'], 
        );
        $job_payment_details_id = $this->api->register_job_payment_paid_history($data);


        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        $provider_id = $provider_details['cust_id'];
        //echo json_encode($provider_id); die();
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        
        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Payment completed for job proposal accepted. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job proposal accept payment'), 'type' => 'proposal-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job proposal accept payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job proposal accept payment'), trim($message));
        //Customer Notifications--------------------------

        // //Workstream entry--------------------------------
        //   //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
        //   $data = array(
        //     'provider_id' => $provider_id,
        //     'cust_id' => $cust_id,
        //     'job_id' => $job_id,
        //   );
        //   $workstream_id = $this->api->check_workstream_master($data);
        //   $update_data = array(
        //     'workstream_id' => (int)$workstream_id,
        //     'job_id' => (int)$job_id,
        //     'cust_id' => $cust_id,
        //     'provider_id' => (int)$job_details['provider_id'],
        //     'text_msg' => $message,
        //     'attachment_url' =>  'NULL',
        //     'cre_datetime' => $today,
        //     'sender_id' => $cust_id,
        //     'type' => 'payment',
        //     'file_type' => 'text',
        //     'proposal_id' => 0,
        //   );
        //   $this->api->update_to_workstream_details($update_data);
        // //Workstream entry--------------------------------
        

        //Invoice Notifications---------------------------
          $provider_name=$provider_details['firstname']." ".$provider_details['lastname'];
          $customer_name=$customer_details['firstname']." ".$customer_details['lastname'];
          $phpinvoice = 'resources/fpdf-master/phpinvoicejob.php';
          require_once($phpinvoice);
          //Language configuration for invoice
          $lang = $this->input->post('lang', TRUE);
          if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
          else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
          else { $invoice_lang = "englishApi_lang"; }
          //Invoice Configuration
          $invoice = new phpinvoice('A4',trim($job_details['currency_code']),$invoice_lang);
          $invoice->setLogo("resources/fpdf-master/invoice-header.png");
          $invoice->setColor("#000");
          $invoice->setType("");
          $invoice->setReference($job_details['job_id']."-0");
          $invoice->setDate(date('M dS ,Y',time()));

          $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
          $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
          $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));

          $user_country = $this->api->get_country_details(trim($customer_details['country_id']));
          $user_state = $this->api->get_state_details(trim($customer_details['state_id']));
          $user_city = $this->api->get_city_details(trim($customer_details['city_id']));

          $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
          $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
          $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

          $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email_id'])));

          $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($customer_details['email1'])));

          $eol = PHP_EOL;
          /* Adding Items in table */
          $order_for = ucfirst($job_details['job_title']);
          $rate = $this->lang->line('paid_deposite_amount');
          $total = round(trim($proposal_details['deposit_price']),2);;
          $payment_datetime = substr($today, 0, 10);
          $milestone="NA";
          //set items
          $invoice->addItem($payment_datetime,$order_for,$milestone,$rate,$total);
          /* Add totals */
          $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
          /* Set badge */ 
          /* Add title */
          $invoice->addTitle($this->lang->line('tnc'));
          /* Add Paragraph */
          $invoice->addParagraph($gonagoo_address['terms']);
          /* Add title */
          $invoice->addTitleFooter($this->lang->line('thank_you_job'));
          /* Add Paragraph */
          $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
          /* Set footer note */
          $invoice->setFooternote($gonagoo_address['company_name']);
          /* Render */
          $invoice->render($job_details['job_id'].'-0_customer_job.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
          //Update File path
          $pdf_name = $job_details['job_id'].'-0_customer_job.pdf';
          //Move file to upload folder
          rename($pdf_name, "resources/troubleshoot-invoices/".$pdf_name);
        
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Laundry Invoice');
            $emailAddress = trim($customer_details['email1']);
            $fileToAttach = "resources/laundry-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            // echo json_encode($update_data);die();
          /* -----------------------Email Invoice to customer-------------------- */

          $data = array(
            'provider_id' => $provider_id,
            'cust_id' => $cust_id,              
            'job_id' => $job_id,
          );
          $workstream_id = $this->api->check_workstream_master($data);
          $update_data = array(
            'workstream_id' => (int)$workstream_id,
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' => "resources/troubleshoot-invoices/".$pdf_name,
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'payment_invoice',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //Invoice Notifications---------------------------

        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $message = $this->lang->line('Proposal accept and Payment received for Job. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            
            foreach ($device_details_provider as $value) {

              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job proposal payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN

            $msg_apn_provider =  array('title' => $this->lang->line('Job proposal payment received'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job proposal payment received'), trim($message));
        //Provider Notifications--------------------------

        //proposal accept update--------------------------
          $data = array(
            'proposal_status' => 'accept',
            'proposal_status_date' => $today,
            'proposal_status_updated_by' => 'customer',
          );
          $this->api->update_job_proposals($data,$job_id,$provider_id);
        //proposal accept update--------------------------

        //notify proposal closed--------------------------
          if($proposals = $this->api->get_job_proposals_list_who_notify($job_id,$provider_id)){
            foreach ($proposals as $prop)
            {
              $provider_details = $this->api->get_service_provider_profile($prop['provider_id']);
              $provider_id = $prop['provider_id'];
              $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
              $message = $this->lang->line('Thank you for sending proposal, customer accepted other proposal now your proposal is closed. Job ID:').': '.$job_id;
              $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_provider = $this->api->get_user_device_details((int)$provider_id);
              if(!is_null($device_details_provider)) {
                $arr_provider_fcm_ids = array();
                $arr_provider_apn_ids = array();
                foreach ($device_details_provider as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Notify proposal closed'), 'type' => 'proposal-closed', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                //APN
                $msg_apn_provider =  array('title' => $this->lang->line('Notify proposal closed'), 'text' => $message);
                if(is_array($arr_provider_apn_ids)) { 
                  $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                }
              }

              $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Notify proposal closed'), trim($message));
            }
            $this->api->close_job_proposals_except_accepted($job_id,$provider_id);
          }
        //notify proposal closed--------------------------
        
        //Payment Section---------------------------------
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];
          //Add Payment to customer account---------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
            } else {
              $data_cust_mstr = array(
                "user_id" => $job_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_cust_mstr);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($cust_acc_mstr['account_balance']) + trim($job_deposit_price);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($job_deposit_price),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($job_deposit_price);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($job_deposit_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp add Payment to customer account---------------

          //Add Payment to customer account---------------
          
          //Deduct Payment to customer account------------
            $account_balance = trim($cust_acc_mstr['account_balance']) - trim($job_deposit_price);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($job_deposit_price),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp deduct Payment to customer account------------
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($job_deposit_price);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($job_deposit_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp deduct Payment to customer account------------

          //Deduct Payment to customer account------------
          
          //Add Payment To Gonagoo Account----------------
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
            } else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }

            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($job_deposit_price);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'job_deposit_amount',
              "amount" => trim($job_deposit_price),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($job_details['currency_code']),
              "country_id" => trim($customer_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
          //Add Payment To Gonagoo Account----------------
          
          //Add Payment to provider Escrow----------------
            if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_pro_scrw = array(
                "deliverer_id" => (int)$provider_id,
                "scrow_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_scrow_record($data_pro_scrw);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
            }

            $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($job_deposit_price);
            $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

            $data_scrw_hstry = array(
              "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
              "order_id" => (int)$job_id,
              "deliverer_id" => (int)$provider_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'job_deposite_amount',
              "amount" => trim($job_deposit_price),
              "scrow_balance" => trim($scrow_balance),
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_scrow_history_record($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record_smp($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($job_deposit_price);
              $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'job_deposite_amount',
                "amount" => trim($job_deposit_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------------
          //Add Payment to provider Escrow----------------
        //Payment Section---------------------------------

        $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
    public function job_payment_orange()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      $payment_id = 'g_jobs_'.time().'_'.$job_id;
      //get access token
      $ch = curl_init("//api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      //echo json_encode($token_details); die();

      $token = 'Bearer '.$token_details['access_token'];

      $lang = $this->session->userdata('language');
      if($lang=='french') $lang = 'fr';
      else $lang = 'en';

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "merchant_key" => "a8f8c61e",
                            "currency" => "OUV",
                            "order_id" => $payment_id,
                            "amount" => $offer_total_price,
                            "return_url" => base_url('user-panel-services/offer-payment-orange-process'),
                            "cancel_url" => base_url('user-panel-services/offer-payment-orange-process'),
                            "notif_url" => base_url(),
                            "lang" => $lang, 
                            "reference" => $this->lang->line('Gonagoo - Offer Payment') 
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("//api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: ".$token,
        "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      //echo json_encode($url_details); die();

      //echo $url_details['notif_token']; die();
      
      if(isset($url_details) && $url_details['message'] == 'OK' && $url_details['status'] == 201) {
        //echo 'true'; die();
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('job_id', $job_id);
        $this->session->set_userdata('payment_id', $payment_id);
        $this->session->set_userdata('amount', $offer_total_price);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        //echo 'false'; die();
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-services/my-jobs');
      }
    }
    public function job_payment_orange_process()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id; $cust_id = (int) $cust_id;
      $session_token = $_SESSION['notif_token'];
      $job_id = $_SESSION['job_id'];
      $job_details = $this->api->get_job_details($job_id);
      $payment_id = $_SESSION['payment_id'];
      $offer_total_price = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];
      // get payment link
      $json_post_data = json_encode(
                          array(
                            "order_id" => $payment_id, 
                            "amount" => $offer_total_price, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      //$transaction_id = $payment_details['txnid'];
      $transaction_id = '123';
      //echo json_encode($payment_details); die();
      // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
      if(1) {
        $data = array(
          'paid_amount' => $offer_total_price,
          'payment_method' => trim($payment_method), 
          'payment_mode' => 'online', 
          'payment_by' => 'web', 
          'transaction_id' => trim($transaction_id), 
          'payment_response' => trim($payment_res), 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1,
          'job_status' => 'in_progress',
        );
        if( $this->api->job_details_update($job_id, $data) ) {
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Customer Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
            $message = $this->lang->line('Payment completed for offer purchase. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase payment'), trim($message));
          //------------------------------------------------
          //Workstream entry--------------------------------
            $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
          //Provider Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
            $message = $this->lang->line('Payment received for offer. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer payment received'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment received'), trim($message));
          //------------------------------------------------

          //Payment Section---------------------------------
            //Add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
            //----------------------------------------------
            //Deduct Payment to customer account------------
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
            //----------------------------------------------
            //Add Payment To Gonagoo Account----------------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------
            //Add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$provider_id,
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$provider_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($offer_total_price),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
            //----------------------------------------------
          //Payment Section---------------------------------
          //Update offer sales count------------------------
            $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
            $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
            $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
          //------------------------------------------------

          $this->session->unset_userdata('notif_token');
          $this->session->unset_userdata('job_id');
          $this->session->unset_userdata('payment_id');
          $this->session->unset_userdata('amount');
          $this->session->unset_userdata('pay_token');

          $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
  //End customer job deposite payment----------------------

  //Start Customer invite Jobs ----------------------------
    public function customer_invite_job()
    {
      $provider_id = $this->input->post('provider_id');
      $job_id = $this->input->post('job_id');
      $cust_id = $this->cust_id;
      $today = date('Y-m-d h:i:s');
      $job_details = $this->api->get_customer_job_details($job_id);

      $insert_data  = array(
        "job_id" => $job_id,
        "cust_id" => $cust_id,
        "provider_id" => $provider_id,
        "invite_datetime" => $today,
        "invite_status" => "open",
      );

      $message = $this->lang->line('Job invitation')." - ".$job_details['job_title'];
      $update_data = array(
        'job_id' => (int)$job_id,
        'cust_id' => $cust_id,
        'provider_id' => $provider_id,
        'cre_datetime' => $today,
        'status' => 1,
      );
      if(!$this->api->check_customer_invite_job($insert_data)){
        if($workstream_id = $this->api->update_to_workstream_master($update_data)) {
          //Update to workroom
          $update_data = array(
            'workstream_id' => (int)$workstream_id,
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => $provider_id,
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'job_invite',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }  
        if($this->api->register_customer_invite_job($insert_data))
        {

          $job_details = $this->api->get_customer_job_details($job_id);
          $provider_details = $this->api->get_user_details($provider_id);
        
          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email1']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('You are invited to this job. Job ID').": ".$job_id;
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details($provider_id);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('invitation received'), 'type' => 'job invitation', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('invitation received'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($provider_name, trim($provider_details['email1']), $this->lang->line('invitation received'), trim($message));
          //Notifications to customers----------------

          echo 'success';
        }else{ 
          echo 'failed';
        }
      }else{echo 'already';}
    }
    public function customer_invite_job_all()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id');
      $job_details = $this->api->get_customer_job_details($job_id);
      $cust_id = $this->cust_id;
      $today = date('Y-m-d h:i:s');

      $skill = $this->api->get_category_details_by_id($job_details['sub_cat_id']);
      $data = array(
        'skill' => $skill['cat_name'] ,
        'fav' => "no" , 
        'country_id' => "NULL",
        'state_id' => "NULL",
        'city_id' => "NULL",
        'rate_from' => "NULL",
        'rate_to' => "NULL" ,
        'cust_id' => $this->cust_id
      );
      $freelancers = $this->api->list_of_freelancers($data);

      foreach ($freelancers as $fc)
      {
        $insert_data  = array(
          "job_id" => $job_id,
          "cust_id" => $cust_id,
          "provider_id" => $fc['cust_id'],
          "invite_datetime" => $today,
          "invite_status" => "open",
        );
        $message = $this->lang->line('Job invitation')." - ".$job_details['job_title'];
        $update_data = array(
          'job_id' => (int)$job_id,
          'cust_id' => $cust_id,
          'provider_id' => $fc['cust_id'],
          'cre_datetime' => $today,
          'status' => 1,
        );
        if(!$this->api->check_customer_invite_job($insert_data)){
          if($workstream_id = $this->api->update_to_workstream_master($update_data)) {
            //Update to workroom
            $update_data = array(
              'workstream_id' => (int)$workstream_id,
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => $fc['cust_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'job_invite',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          }
          $this->api->register_customer_invite_job($insert_data);
        }
      }
      $this->session->set_flashdata('success', $this->lang->line('Invite send Successfully.'));
      redirect('user-panel-services/my-jobs','refresh');
    }
  //End Customer invite Jobs ------------------------------
  
  //Start Customer job workroom----------------------------
    public function job_workstream_list_orignal()
    {
      $job_id = $this->uri->segment(3);
      $insert_data = array(
        'job_id' => $job_id,
        'country_id' => "0",
        'ratings' => "0"
      );
      $freelancers =  $this->api->list_of_freelancers_invited_proposals($insert_data);
      $job_details = $this->api->get_job_details($job_id);
      $favorite_freelancer = $this->api->get_favorite_freelancer($this->cust_id);
      $decline_reason = $this->api->job_decline_reason_master();      
      $fav_freelancer = array();
      $freelancer_country_id = array();
      $freelancer_country = array();
      foreach ($favorite_freelancer as $fav_key) {
        array_push($fav_freelancer, $fav_key['provider_id']);
      }
      for($i=0; $i<sizeof($freelancers); $i++){
        $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
        $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
        $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
        if(!in_array($freelancers[$i]['country_id'], $freelancer_country_id))
        {array_push($freelancer_country_id ,$freelancers[$i]['country_id']);}

        if($completed['total_job'] != "0"){
         $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
        }else{$percent = 0;}
        $insert_data = array(
          'cust_id' => $job_details['cust_id'],
          'provider_id' => $freelancers[$i]['cust_id'],
          'job_id' => $job_id,
        );
        if($this->api->check_job_proposals($insert_data)){
          $data = array(
            'job_id' => $job_id,
            'provider_id' => $freelancers[$i]['cust_id'], 
            'decline' => 'no'
          );
          $proposal = $this->api->get_job_proposals($data);
          $freelancers[$i]['proposal_details'] = $proposal;
          $freelancers[$i]['proposal'] = "yes";
        }else{ $freelancers[$i]['proposal'] = "no"; }
        $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
        $freelancers[$i]['job_percent'] = $percent;
        $freelancers[$i]['portfolio'] = (int)$portfolio;
      }
      foreach ($freelancer_country_id as $id){
        $country = $this->api->get_country_details($id);
        array_push($freelancer_country ,$country);
      }
      $filter = 'basic';
      $is_fav = 'no';

      //echo json_encode($freelancer); die();
      $this->load->view('services/job_freelancers_workroom_list', compact('freelancer_country','fav_freelancer','freelancers','filter','job_details','is_fav','decline_reason'));
      $this->load->view('user_panel/footer');
    }
    public function job_workstream_list()
    {
      //echo json_encode($_POST); die();
      if(isset($_POST['job_id'])){
        $job_id = $this->input->post('job_id');
      }else{$job_id = $this->uri->segment(3);}
      if(isset($_POST['proposal_status'])){
        $proposal_status = $this->input->post('proposal_status');
      }else{$proposal_status = "All";}
      if(isset($_POST['ratings'])){
        $ratings = $this->input->post('ratings');
      }else{$ratings = "0";}
      if(isset($_POST['country_id'])){
        $country_id = $this->input->post('country_id');
      }else{$country_id = "0";}
      if(isset($_POST['filter'])){
        $filter = $this->input->post('filter');
      }else{$filter = "advance";}
      $insert_data = array(
        'job_id' => $job_id,
        'country_id' => $country_id,
        'ratings' => $ratings
      );
      $redirect_url = $this->input->post('redirect_url');
      //echo json_encode($insert_data); die();
      $freelancers =  $this->api->list_of_freelancers_invited_proposals($insert_data);
      //echo json_encode($freelancers);            
      $job_details = $this->api->get_customer_job_details($job_id);
      $favorite_freelancer = $this->api->get_favorite_freelancer($this->cust_id);
      $decline_reason = $this->api->job_decline_reason_master();      
      $fav_freelancer = array();
      $freelancer_country_id = array();
      $freelancer_country = array();
      foreach ($favorite_freelancer as $fav_key) {
        array_push($fav_freelancer, $fav_key['provider_id']);
      }
      for($i=0; $i<sizeof($freelancers); $i++){
        $freelancers[$i]['profile_status'] = "safe";
        $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
        $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
        $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
        if(!in_array($freelancers[$i]['country_id'], $freelancer_country_id))
        {array_push($freelancer_country_id ,$freelancers[$i]['country_id']);}

        if($completed['total_job'] != "0"){
         $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
        }else{$percent = 0;}
        $insert_data = array(
          'cust_id' => $job_details['cust_id'],
          'provider_id' => $freelancers[$i]['cust_id'],
          'job_id' => $job_id,
        );
        if($this->api->check_job_proposals($insert_data)){
          $data = array(
            'job_id' => $job_id,
            'provider_id' => $freelancers[$i]['cust_id'], 
            'decline' => 'no'
          );
          $proposal = $this->api->get_job_proposals($data);
          $freelancers[$i]['proposal_details'] = $proposal;
          $freelancers[$i]['proposal'] = "yes";
        }else{
          $freelancers[$i]['proposal'] = "no";
        }
        if($proposal_status == "decline"){
          $data = array(
            'job_id' => $job_id,
            'provider_id' => $freelancers[$i]['cust_id'], 
            'decline' => 'yes'
          );
          if($this->api->get_job_proposals($data)){
            $freelancers[$i]['profile_status'] = "safe";
          }else{
            $freelancers[$i]['profile_status'] = "danger";
          }
        }
        if($proposal_status == "pending"){
          $data = array(
            'job_id' => $job_id,
            'provider_id' => $freelancers[$i]['cust_id'], 
            'decline' => 'yes'
          );
          if($this->api->get_job_proposals($data)){
            $freelancers[$i]['profile_status'] = "danger";
          }else{
            $freelancers[$i]['profile_status'] = "safe";
          }
        }
        $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
        $freelancers[$i]['job_percent'] = $percent;
        $freelancers[$i]['portfolio'] = (int)$portfolio;
      }
      foreach ($freelancer_country_id as $id){
        $country = $this->api->get_country_details($id);
        array_push($freelancer_country ,$country);
      }
      $filter = 'basic';
      $is_fav = 'no';
      //echo json_encode($job_details); die();
      $this->load->view('services/job_freelancers_workroom_list', compact('proposal_status','ratings','country_id','freelancer_country','fav_freelancer','freelancers','filter','job_details','is_fav','decline_reason','redirect_url'));
      $this->load->view('user_panel/footer');
    }
    public function customer_job_workroom()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      if(!$job_id) { $job_id = $this->session->userdata('workroom_job_id'); }
      $job_details = $this->api->get_customer_job_details($job_id);
      $login_customer_details = $this->api->get_user_details($cust_id);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$provider_id);
      $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
      $job_provider_details = $this->api->get_user_details((int)$provider_id);
      $keys = ['provider_id' => (int)$job_provider_profile['cust_id'], 'cust_id' => $cust_id, 'job_id' => $job_id];
      $workroom = $this->api->get_job_workstream_details($job_id , $cust_id , $provider_id);
      //echo json_encode($workroom); die();
      $this->load->view('services/services_job_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'job_provider_profile', 'keys', 'job_provider_details', 'job_customer_details','redirect_url'));
      $this->load->view('user_panel/footer');
    }
    public function customer_job_workroom_ajax()
    {
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $chat_count = $this->input->post('chat_count', TRUE); $chat_count = (int)$chat_count;
      $job_details = $this->api->get_job_details($job_id);
      $login_customer_details = $this->api->get_user_details($cust_id);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$provider_id);
      $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
      $job_provider_details = $this->api->get_user_details((int)$provider_id);
      $workroom_details = $this->api->get_job_workstream_details($job_id , $cust_id , $provider_id);
      $new_chat_count = sizeof($workroom_details);

      if(sizeof($workroom_details) > $chat_count) {
        $message_template ='';
        $message_template .= "<input type='hidden' value='$new_chat_count' id='chat_count' />";
        foreach ($workroom_details as $v => $workroom) { $tabindex = ($new_chat_count==($v+1))?'tabindex=1':'';
          date_default_timezone_set($this->session->userdata("default_timezone")); 
          $ud = strtotime($workroom['cre_datetime']);
          date_default_timezone_set($this->session->userdata("user_timezone"));

          $chat_side = ($workroom['sender_id'] == $cust_id && $workroom['is_admin'] == 0)?'right':'left';
          $message_template .= "<li class='chat-message $chat_side'>";

          if($workroom['sender_id'] == $cust_id && $workroom['is_admin'] == 0) {
            $url = base_url($login_customer_details['avatar_url']);
            $message_template .= "<img class='message-avatar' style='margin-right: 0px;' src='$url' />";
            $message_template .= "<div class='message' style='margin-left: 0px;'>";
              $cust_name = $login_customer_details['firstname'].' '.$login_customer_details['lastname'];
              $message_template .= "<a class='message-author'>$cust_name</a>";
              $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
              $message_template .= "<div class='row'>";

                if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice"):
                  $message_template .= "<span class='message-content'>";
                    if($workroom['attachment_url'] != 'NULL'):
                      $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<p>&nbsp;</p>";
                      $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                      $message_template .= "</div>";
                    endif;
                    $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                    $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                      if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ):
                        $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                      endif;
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_started"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "payment"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "milestone_accepted"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone accepted')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "milestone_rejected"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone rejected')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_proposal"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Send')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_decline"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('proposal decline')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

                if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                      for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                      for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                      $message_template .= "( ".$workroom['ratings']." / 5 )";
                      $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

              $message_template .= "</div>";
            $message_template .= "</div>";
          } elseif($workroom['is_admin'] == 1){
            
            $admin_profile = $this->api->logged_in_user_details($workroom['admin_id']);
            $url = base_url($admin_profile['avatar_url']);
            $message_template .= "<img class='message-avatar' src='$url' />";
            $message_template .= "<div class='message'>";
              $user_name = $admin_profile['firstname'].' '.$admin_profile['lastname'];
              $message_template .= "<a class='message-author text-left'>$user_name"." [".$this->lang->line('gonagoo_admin')."]</a>";
              $ud = strtotime($workroom['cre_datetime']);
              $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
              $message_template .= "<div class='row'>";

                if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                  $message_template .= "<span class='message-content'>";
                    $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                    $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                      if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                        $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                      endif;
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p>$msg</p>";
                    $message_template .= "</div>";
                    if($workroom['attachment_url'] != 'NULL') :
                      $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                        $message_template .= "<p>&nbsp;</p>";
                        $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                      $message_template .= "</div>";
                    endif;
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_started"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "payment"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "milestone_send"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone received')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_proposal"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                      for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                      for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                      $message_template .=  "( ".$workroom['ratings']." / 5 )";
                      $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

              $message_template .= "</div>";
            $message_template .= "</div>";
          } else {
            $url = base_url(($workroom['sender_id'] == $job_details['cust_id']) ? $job_customer_details['avatar_url'] : $job_provider_details['avatar_url']);
            $message_template .= "<img class='message-avatar' src='$url' />";
            $message_template .= "<div class='message'>";
              $user_name = ($workroom['sender_id']==$job_details['cust_id']) ? $job_customer_details['firstname'].' '.$job_customer_details['lastname'] : $job_provider_details['firstname'].' '.$job_provider_details['lastname'];
              $message_template .= "<a class='message-author text-left'>$user_name</a>";
              $ud = strtotime($workroom['cre_datetime']);
              $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
              $message_template .= "<div class='row'>";

                if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                  $message_template .= "<span class='message-content'>";
                    $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                    $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                      if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                        $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                      endif;
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p>$msg</p>";
                    $message_template .= "</div>";
                    if($workroom['attachment_url'] != 'NULL') :
                      $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                        $message_template .= "<p>&nbsp;</p>";
                        $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                      $message_template .= "</div>";
                    endif;
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_started"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "payment"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "milestone_send"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone received')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_proposal"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                      for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                      for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                      $message_template .=  "( ".$workroom['ratings']." / 5 )";
                      $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

              $message_template .= "</div>";
            $message_template .= "</div>";
          }
          $message_template .= "</li>";
        }
      } else { $message_template = false; }
      echo json_encode($message_template); die();
    }
    public function add_job_workroom_chat()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $text_msg = $this->input->post('message_text', TRUE);
      $job_details = $this->api->get_job_details($job_id); 
      $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
      $sender_id = (int)$cust['cust_id'];
      $today = date("Y-m-d H:i:s");

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['max_width']      = '0'; 
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
        if($this->upload->do_upload('attachment')) { 
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
        $insert_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => $job_id,
          'cust_id' => $cust_id,
          'provider_id' => $provider_id,
          'text_msg' => $text_msg,
          'attachment_url' =>  $attachment_url,
          'cre_datetime' => $today,
          'sender_id' => $sender_id,
          'type' => ($attachment_url=='NULL')?'chat':'attachment',
          'file_type' => ($attachment_url=='NULL')?'text':'file',
          'proposal_id' => 0,
          "ratings" => "NULL",
        );
        //echo json_encode($insert_data); die();
        $this->api->update_to_workstream_details($insert_data);

        //Notifications to Provider----------------
          $api_key = $this->config->item('delivererAppGoogleKey');

          if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
          } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

          if(!is_null($device_details)) {
            $arr_fcm_ids = array(); $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
            $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_apn_ids, $api_key);
            }
          }
        //------------------------------------------
      }

      $array = array('workroom_job_id' => $job_id );      
      $this->session->set_userdata( $array );
      redirect('user-panel-services/customer-job-workroom');
    }
    public function add_job_workroom_chat_ajax()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $text_msg = $this->input->post('message_text', TRUE);
      $job_details = $this->api->get_customer_job_details($job_id); 
      $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
      $sender_id = (int)$cust['cust_id'];
      $today = date("Y-m-d H:i:s");

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['max_width']      = '0'; 
        $config['max_height']     = '0'; 
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
        if($this->upload->do_upload('attachment')) { 
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
        $insert_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => $job_id,
          'cust_id' => $cust_id,
          'provider_id' => $provider_id,
          'text_msg' => $text_msg,
          'attachment_url' =>  $attachment_url,
          'cre_datetime' => $today,
          'sender_id' => $sender_id,
          'type' => ($attachment_url=='NULL')?'chat':'attachment',
          'file_type' => ($attachment_url=='NULL')?'text':'file',
          'proposal_id' => 0,
          "ratings" => "NULL",
        );
        //echo json_encode($insert_data); die();
        $this->api->update_to_workstream_details($insert_data);

        //Notifications to Provider----------------
          $api_key = $this->config->item('delivererAppGoogleKey');

          if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
          } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

          if(!is_null($device_details)) {
            $arr_fcm_ids = array(); $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
            $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_apn_ids, $api_key);
            }
          }
        //------------------------------------------
      }
      return json_encode(true);
    }
  //End Customer job workroom------------------------------
    
  //Start Provider job workroom----------------------------
    public function provider_job_workroom()
    {
      //echo json_encode($_POST); die();
      $provider_id = $this->cust_id; $provider_id = (int) $provider_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      if(!$job_id) { $job_id = $this->session->userdata('workroom_job_id'); }
      $job_details = $this->api->get_customer_job_details($job_id);
      $login_customer_details = $this->api->get_user_details($provider_id);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$cust_id);
      $job_customer_details = $this->api->get_user_details((int)$provider_id);
      $job_provider_details = $this->api->get_user_details((int)$cust_id);
      $keys = ['provider_id' => (int)$provider_id, 'cust_id' => $cust_id, 'job_id' => $job_id];
      $workroom = $this->api->get_job_workstream_details($job_id , $cust_id , $provider_id);
      //echo json_encode($job_customer_details['avatar_url']); die();
      $this->load->view('services/services_job_provider_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'job_provider_profile', 'keys', 'job_provider_details', 'job_customer_details','redirect_url'));
      $this->load->view('user_panel/footer');
    }
    public function provider_job_workroom_ajax()
    {
      //echo json_encode($_POST); die();
      $provider_id = $this->cust_id; $provider_id = (int) $provider_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
      $chat_count = $this->input->post('chat_count', TRUE); $chat_count = (int)$chat_count;
      $job_details = $this->api->get_job_details($job_id);
      $login_customer_details = $this->api->get_user_details($provider_id);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$cust_id);
      $job_customer_details = $this->api->get_user_details((int)$provider_id);
      $job_provider_details = $this->api->get_user_details((int)$cust_id);
      $workroom_details = $this->api->get_job_workstream_details($job_id , $cust_id , $provider_id);
      $new_chat_count = sizeof($workroom_details);
      if(sizeof($workroom_details) > $chat_count) {
        $message_template ='';
        $message_template .= "<input type='hidden' value='$new_chat_count' id='chat_count' />";
        foreach ($workroom_details as $v => $workroom) { $tabindex = ($new_chat_count==($v+1))?'tabindex=1':'';
          date_default_timezone_set($this->session->userdata("default_timezone")); 
          $ud = strtotime($workroom['cre_datetime']);
          date_default_timezone_set($this->session->userdata("user_timezone"));

          $chat_side = ($workroom['sender_id'] == $provider_id && $workroom['is_admin'] == 0)?'right':'left';
          $message_template .= "<li class='chat-message $chat_side'>";

          if($workroom['sender_id'] == $provider_id && $workroom['is_admin'] == 0) {
            
            $url = base_url($login_customer_details['avatar_url']);
            $message_template .= "<img class='message-avatar' style='margin-right: 0px;' src='$url' />";
            $message_template .= "<div class='message' style='margin-left: 0px;'>";
              $cust_name = $login_customer_details['firstname'].' '.$login_customer_details['lastname'];
              $message_template .= "<a class='message-author'>$cust_name</a>";
              $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
              $message_template .= "<div class='row'>";

                if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice"):
                  $message_template .= "<span class='message-content'>";
                    if($workroom['attachment_url'] != 'NULL'):
                      $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<p>&nbsp;</p>";
                      $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                      $message_template .= "</div>";
                    endif;
                    $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                    $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                      if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ):
                        $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                      endif;
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_started"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "payment"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "milestone_send"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone request send')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_proposal"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Send')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                      for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                      for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                      $message_template .= "( ".$workroom['ratings']." / 5 )";
                      $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

              $message_template .= "</div>";
            $message_template .= "</div>";
          } elseif($workroom['is_admin'] == 1){

            $admin_profile = $this->api->logged_in_user_details($workroom['admin_id']);
            $url = base_url($admin_profile['avatar_url']);
            $message_template .= "<img class='message-avatar' src='$url' />";
            $message_template .= "<div class='message'>";
              $user_name = $admin_profile['firstname'].' '.$admin_profile['lastname'];
              $message_template .= "<a class='message-author text-left'>$user_name"." [".$this->lang->line('gonagoo_admin')."]</a>";
              $ud = strtotime($workroom['cre_datetime']);
              $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
              $message_template .= "<div class='row'>";

                if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                  $message_template .= "<span class='message-content'>";
                    $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                    $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                      if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                        $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                      endif;
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p>$msg</p>";
                    $message_template .= "</div>";
                    if($workroom['attachment_url'] != 'NULL') :
                      $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                        $message_template .= "<p>&nbsp;</p>";
                        $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                      $message_template .= "</div>";
                    endif;
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_started"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "payment"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "milestone_accepted"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone accepted')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_proposal"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                      for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                      for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                      $message_template .=  "( ".$workroom['ratings']." / 5 )";
                      $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

              $message_template .= "</div>";
            $message_template .= "</div>";
          }else {
            $url = base_url(($workroom['sender_id'] == $job_details['cust_id']) ? $job_provider_details['avatar_url'] : $job_provider_details['avatar_url']);
            $message_template .= "<img class='message-avatar' src='$url' />";
            $message_template .= "<div class='message'>";
              $user_name = ($workroom['sender_id']==$job_details['cust_id']) ? $job_provider_details['firstname'].' '.$job_provider_details['lastname'] : $job_provider_details['firstname'].' '.$job_provider_details['lastname'];
              $message_template .= "<a class='message-author text-left'>$user_name</a>";
              $ud = strtotime($workroom['cre_datetime']);
              $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
              $message_template .= "<div class='row'>";

                if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                  $message_template .= "<span class='message-content'>";
                    $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                    $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                      if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                        $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                      endif;
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p>$msg</p>";
                    $message_template .= "</div>";
                    if($workroom['attachment_url'] != 'NULL') :
                      $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                        $message_template .= "<p>&nbsp;</p>";
                        $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                      $message_template .= "</div>";
                    endif;
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_started"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "payment"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "milestone_accepted"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone accepted')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_proposal"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                      $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                      $message_template .= "<p style='text-align: left;'>$msg</p>";
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;
                if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                  $message_template .= "<span class='message-content'>";
                    $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                      for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                      for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                      $message_template .=  "( ".$workroom['ratings']." / 5 )";
                      $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                    $message_template .= "</div>";
                  $message_template .= "</span>";
                endif;

              $message_template .= "</div>";
            $message_template .= "</div>";
          }
          $message_template .= "</li>";
        }
      } else { $message_template = false; }
      echo json_encode($message_template); die();
    }
    public function add_provider_job_workroom_chat()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $text_msg = $this->input->post('message_text', TRUE);
      $job_details = $this->api->get_job_details($job_id); 
      $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
      $sender_id = (int)$cust['cust_id'];
      $today = date("Y-m-d H:i:s");

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['max_width']      = '0'; 
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
        if($this->upload->do_upload('attachment')) { 
          $uploads    = $this->upload->data();
          $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
        $insert_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => $job_id,
          'cust_id' => $cust_id,
          'provider_id' => $provider_id,
          'text_msg' => $text_msg,
          'attachment_url' =>  $attachment_url,
          'cre_datetime' => $today,
          'sender_id' => $sender_id,
          'type' => ($attachment_url=='NULL')?'chat':'attachment',
          'file_type' => ($attachment_url=='NULL')?'text':'file',
          'proposal_id' => 0,
          "ratings" => "NULL",
        );
        //echo json_encode($insert_data); die();
        $this->api->update_to_workstream_details($insert_data);

        //Notifications to Provider----------------
          $api_key = $this->config->item('delivererAppGoogleKey');

          if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
          } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

          if(!is_null($device_details)) {
            $arr_fcm_ids = array(); $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
            $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_apn_ids, $api_key);
            }
          }
        //------------------------------------------
      }

      $array = array('workroom_job_id' => $job_id );      
      $this->session->set_userdata( $array );
      redirect('user-panel-services/customer-job-workroom');
    }
    public function add_provider_job_workroom_chat_ajax()
    {
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $text_msg = $this->input->post('message_text', TRUE);
      $job_details = $this->api->get_customer_job_details($job_id); 
      // $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
      $sender_id = (int)$cust['cust_id'];
      $cust_id = $provider_id;
      $provider_id = (int)$cust['cust_id'];
      $today = date("Y-m-d H:i:s");

      if(!empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['max_width']      = '0'; 
        $config['max_height']     = '0'; 
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config); 
        if($this->upload->do_upload('attachment')) { 
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
        $insert_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => $job_id,
          'cust_id' => $cust_id,
          'provider_id' => $provider_id,
          'text_msg' => $text_msg,
          'attachment_url' =>  $attachment_url,
          'cre_datetime' => $today,
          'sender_id' => $sender_id,
          'type' => ($attachment_url=='NULL')?'chat':'attachment',
          'file_type' => ($attachment_url=='NULL')?'text':'file',
          'proposal_id' => 0,
          "ratings" => "NULL",
        );
        //echo json_encode($insert_data); die();
        $this->api->update_to_workstream_details($insert_data);

        //Notifications to Provider----------------
          $api_key = $this->config->item('delivererAppGoogleKey');

          if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
          } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

          if(!is_null($device_details)) {
            $arr_fcm_ids = array(); $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
            $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_apn_ids, $api_key);
            }
          }
        //------------------------------------------
      }
      return json_encode(true);
    }
  //End Provider job workroom------------------------------
    
  //Start provider pending invitation and proposals--------
    public function pending_job_proposals()
    {
      //echo json_encode($_POST); die();
      $provider_id = $this->cust_id; $provider_id = (int)$provider_id;
      $job_list = $this->api->list_of_job_proposals_sends($provider_id);   
      //echo json_encode($job_list); die();
      // echo $this->db->last_query(); die();
      
      for($i=0; $i<sizeof($job_list); $i++) {
        if($job_list[$i]['service_type'] == 0){
          $proposals = $this->api->get_job_proposals_list($job_list[$i]['job_id']);
          $job_list[$i]['proposal_counts'] = sizeof($proposals);
        }
      }
      $redirect_url = "pending-job-proposals";
      $this->load->view('services/pending_job_proposal_view', compact('job_list','provider_id','redirect_url'));
      $this->load->view('user_panel/footer');
    }
    public function job_invitations()
    {
      //echo json_encode($_POST); die();
      $provider_id = $this->cust_id; $provider_id = (int)$provider_id;
      $job_list = $this->api->list_of_job_invitation($provider_id);   
      for($i=0; $i<sizeof($job_list); $i++) {
        if($job_list[$i]['service_type'] == 0){
          $proposals = $this->api->get_job_proposals_list($job_list[$i]['job_id']);
          $job_list[$i]['proposal_counts'] = sizeof($proposals);
        }
      }
      $redirect_url = "job-invitations";
      $this->load->view('services/job_invitation_view', compact('job_list','provider_id','redirect_url'));
      $this->load->view('user_panel/footer');
    }
  //End provider pending invitation and proposals----------
  
  //Start Provider Milestone-------------------------------
    public function job_milestone_payment_request_view()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $job_details = $this->api->get_job_details($job_id);
      $proposal_milestone = $this->api->get_proposal_milestone_by_proposal_id($job_details['proposal_id']);
      //echo json_encode($proposal_milestone);die();
      $this->load->view('services/create_milestone_payment_request_view',compact('job_details','proposal_milestone'));
      $this->load->view('user_panel/footer');
    }
    public function job_milestone_payment_request()
    {
      // echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $milestone_id = $this->input->post('milestone_id', TRUE); 
      $request_description = $this->input->post('description', TRUE); 
      $milestone_price = $this->input->post('milestone_price', TRUE);
      $job_details = $this->api->get_job_details($job_id); 
      $today = date("Y-m-d H:i:s");
      $data = array(
        'milestone_id' => $milestone_id,
        'job_id' => $job_id,
        'request_description' => $request_description,
        'status' => "open",
        'cre_datetime' => $today,
      );
      if($this->api->register_milestone_payment_request($data)){

        $cust_id = $job_details['cust_id'];
        $provider_id = $job_details['provider_id'];
        $customer_details = $this->api->get_user_details($job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        $provider_id = $provider_details['cust_id'];


        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('You have recieved milestone request for job ID').': '.$job_id;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('You have recieved milestone request'), 'type' => 'milestone-request', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('You have recieved milestone request'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('You have recieved milestone request'), trim($message));
        //Customer Notifications--------------------------

        //Workstream entry--------------------------------
          //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
          $message = $this->lang->line('Milestone request send');
          $data = array(
            'provider_id' => $provider_id,
            'cust_id' => $cust_id,
            'job_id' => $job_id,
          );
          $workstream_id = $this->api->check_workstream_master($data);
          $update_data = array(
            'workstream_id' => (int)$workstream_id,
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $provider_id,
            'type' => 'milestone_send',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //Workstream entry--------------------------------
        
        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $message = $this->lang->line('Milestone request send successfully');
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            
            foreach ($device_details_provider as $value) {

              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Milestone request send'), 'type' => 'milestone-send', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN

            $msg_apn_provider =  array('title' => $this->lang->line('Milestone request send'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Milestone request send'), trim($message));
        //Provider Notifications--------------------------

        $this->session->set_flashdata('success', $this->lang->line('Milestone payment request send'));
      }else{
        $this->session->set_flashdata('error', $this->lang->line('Unable to request for milestone payment'));
      }
      redirect(base_url('user-panel-services/provider-inprogress-jobs'),'refresh');
    }
  //End Provider Milestone---------------------------------
 
  //Start Customer Milestone-------------------------------
    public function customer_milestone_details()
    {
     // echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $job_details = $this->api->get_job_details($job_id);
      $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
      $milestone_payment_request = $this->api->get_milestone_payment_request($job_id);
      $proposal_milestone = $this->api->get_proposal_milestone_by_milestone_id($milestone_payment_request['milestone_id']);
      //echo json_encode($proposal_milestone); die();

      $paid_amount = $job_details['paid_amount']; $paid_amount = (int)$paid_amount;
      $paid_amount_history = $job_paid_history['paid_amount']; $paid_amount_history = (int)$paid_amount_history;
      $milestone_price = $proposal_milestone['milestone_price']; $milestone_price = (int)$milestone_price;
      
      $remain = $paid_amount - $paid_amount_history;
      $min = $milestone_price - $remain;
      $max = $job_details['budget'] - $job_details['paid_amount'];

      if($remain >= $milestone_price){
        $direct_payment = "yes";
      }else{
        $direct_payment = "no";
      }
      //echo json_encode($remain); die();
      $this->load->view('services/milestone_payment_request_view',compact('job_details','proposal_milestone','paid_amount','paid_amount_history','milestone_price','remain','direct_payment','min','max','milestone_payment_request'));
      $this->load->view('user_panel/footer');
    }
    public function accept_job_milestone_payment_request()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $milestone_id = $this->input->post('milestone_id', TRUE); $milestone_id = (int) $milestone_id;
      $milestone_details = $this->api->get_proposal_milestone_by_milestone_id($milestone_id);
      $milestone_price = $this->input->post('milestone_price', TRUE);
      $job_details = $this->api->get_job_details($job_id);
      $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
      //echo json_encode($job_details); die();
      $provider_id = $job_details['provider_id'];
      $currency_code = $job_details['currency_code'];
      $today = date('Y-m-d H:i:s');
      //Payment Section---------------------------------
      $customer_details = $this->api->get_user_details($cust_id);
      $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);

      if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
        $customer_name = explode("@", trim($customer_details['email1']))[0];
      } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

      if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
        $provider_name = explode("@", trim($provider_details['email_id']))[0];
      } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
      $provider_id = $provider_details['cust_id'];

      //Add Payment To Gonagoo Account----------------
      if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
      } else {
        $data_g_mstr = array(
          "gonagoo_balance" => 0,
          "update_datetime" => $today,
          "operation_lock" => 1,
          "currency_code" => trim($currency_code),
        );
        $this->api->insert_gonagoo_master_record($data_g_mstr);
        $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
      }
      $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);

      if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($currency_code))) {
      } else {
        $data_pro_scrw = array(
          "deliverer_id" => (int)$provider_id,
          "scrow_balance" => 0,
          "update_datetime" => $today,
          "operation_lock" => 1,
          "currency_code" => trim($currency_code),
        );
        $this->api->insert_scrow_record($data_pro_scrw);
        $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($currency_code));
      }
      $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']);
      
      $search_data = array(
        "country_id" => $job_details['country_id'],
        "cat_type_id" => $job_details['cat_id'],
        "cat_id" => $job_details['sub_cat_id'],
      );
      $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);

      //echo json_encode($advance_payment); die();

      if($gonagoo_balance >= $milestone_price && $scrow_balance >= $milestone_price && is_array($advance_payment)){

        //deduct from gonagoo account  
        if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
        }else {
          $data_g_mstr = array(
            "gonagoo_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($currency_code),
          );
          $this->api->insert_gonagoo_master_record($data_g_mstr);
          $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
        }
        $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);
        $gonagoo_balance = trim($gonagoo_balance) - trim($milestone_price);
        $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);

        $g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code));
        $data_g_hstry = array(
          "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
          "order_id" => (int)$job_id,
          "user_id" => (int)$cust_id,
          "type" => 0,
          "transaction_type" => 'milestone_price',
          "amount" => trim($milestone_price),
          "datetime" => $today,
          "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
          "transfer_account_type" => 'NULL',
          "transfer_account_number" => 'NULL',
          "bank_name" => 'NULL',
          "account_holder_name" => 'NULL',
          "iban" => 'NULL',
          "email_address" => 'NULL',
          "mobile_number" => 'NULL',
          "transaction_id" => 'NULL',
          "currency_code" => trim($currency_code),
          "country_id" => trim($provider_details['country_id']),
          "cat_id" => (int)$job_details['sub_cat_id'],
        );
        $this->api->insert_payment_in_gonagoo_history($data_g_hstry);

        // deduct from scrow balance
        $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($milestone_price);
        $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
        $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

        $data_scrw_hstry = array(
          "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
          "order_id" => (int)$job_id,
          "deliverer_id" => (int)$provider_id,
          "datetime" => $today,
          "type" => 0,
          "transaction_type" => 'milestone_price',
          "amount" => trim($milestone_price),
          "scrow_balance" => trim($scrow_balance),
          "currency_code" => trim($job_details['currency_code']),
          "cat_id" => (int)$job_details['sub_cat_id']
        );
        $this->api->insert_scrow_history_record($data_scrw_hstry);
        //Smp deduct from scrow balance-----------------------------
          if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($currency_code))) {
          } else {
            $data_pro_scrw = array(
              "deliverer_id" => (int)$provider_id,
              "scrow_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($currency_code),
            );
            $this->api->insert_scrow_record_smp($data_pro_scrw);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($currency_code));
          }
          $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($milestone_price);
          $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
          $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

          $data_scrw_hstry = array(
            "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
            "order_id" => (int)$job_id,
            "deliverer_id" => (int)$provider_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'milestone_price',
            "amount" => trim($milestone_price),
            "scrow_balance" => trim($scrow_balance),
            "currency_code" => trim($job_details['currency_code']),
            "cat_id" => (int)$job_details['sub_cat_id']
          );
          $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
        //Smp deduct from scrow balance-----------------------------
        
        //add payment to provider account 
        if($provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']))) {
        } else {
          $data_provider_mstr = array(
            "user_id" => $job_details['provider_id'],
            "account_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($job_details['currency_code']),
          );
          $this->api->insert_gonagoo_customer_record($data_provider_mstr);
          $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
        }
        $account_balance = trim($provider_acc_mstr['account_balance']) + trim($milestone_price);
        $this->api->update_payment_in_customer_master($provider_acc_mstr['account_id'], $provider_id, $account_balance);
        $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
        $data_acc_hstry = array(
          "account_id" => (int)$provider_acc_mstr['account_id'],
          "order_id" => $job_id,
          "user_id" => $provider_id,
          "datetime" => $today,
          "type" => 1,
          "transaction_type" => 'milestone_amount',
          "amount" => trim($milestone_price),
          "account_balance" => trim($account_balance),
          "withdraw_request_id" => 0,
          "currency_code" => trim($job_details['currency_code']),
          "cat_id" => (int)$job_details['sub_cat_id']
        );
        $this->api->insert_payment_in_account_history($data_acc_hstry);
        // Smp add payment to provider account-------------- 
          if($provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']))) {
          } else {
            $data_provider_mstr = array(
              "user_id" => $job_details['provider_id'],
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($job_details['currency_code']),
            );
            $this->api->insert_gonagoo_customer_record_smp($data_provider_mstr);
            $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
          }
          $account_balance = trim($provider_acc_mstr['account_balance']) + trim($milestone_price);
          $this->api->update_payment_in_customer_master_smp($provider_acc_mstr['account_id'], $provider_id, $account_balance);
          $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
          $data_acc_hstry = array(
            "account_id" => (int)$provider_acc_mstr['account_id'],
            "order_id" => $job_id,
            "user_id" => $provider_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'milestone_amount',
            "amount" => trim($milestone_price),
            "account_balance" => trim($account_balance),
            "withdraw_request_id" => 0,
            "currency_code" => trim($job_details['currency_code']),
            "cat_id" => (int)$job_details['sub_cat_id']
          );
          $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
        // Smp add payment to provider account-------------- 


        // commision percent 
        $g_commission_percentage = $advance_payment['gonagoo_commission'];
        $gonagoo_commission = ($milestone_price/100)*$g_commission_percentage;

        // deduct commision amount from providers account  
        if($provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']))) {
        } else {
          $data_provider_mstr = array(
            "user_id" => $job_details['provider_id'],
            "account_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($job_details['currency_code']),
          );
          $this->api->insert_gonagoo_customer_record($data_provider_mstr);
          $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
        }
        $account_balance = trim($provider_acc_mstr['account_balance']) - trim($gonagoo_commission);
        $this->api->update_payment_in_customer_master($provider_acc_mstr['account_id'], $provider_id, $account_balance);
        $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
        $data_acc_hstry = array(
          "account_id" => (int)$provider_acc_mstr['account_id'],
          "order_id" => $job_id,
          "user_id" => $provider_id,
          "datetime" => $today,
          "type" => 0,
          "transaction_type" => 'job_commission',
          "amount" => trim($gonagoo_commission),
          "account_balance" => trim($account_balance),
          "withdraw_request_id" => 0,
          "currency_code" => trim($job_details['currency_code']),
          "cat_id" => (int)$job_details['sub_cat_id']
        );
        $this->api->insert_payment_in_account_history($data_acc_hstry);
          // Smp deduct commision amount from providers account----------------
            if($provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_provider_mstr = array(
                "user_id" => $job_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record_smp($data_provider_mstr);
              $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($provider_acc_mstr['account_balance']) - trim($gonagoo_commission);
            $this->api->update_payment_in_customer_master_smp($provider_acc_mstr['account_id'], $provider_id, $account_balance);
            $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$provider_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $provider_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'job_commission',
              "amount" => trim($gonagoo_commission),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
          // Smp deduct commision amount from providers account----------------  


        //Add gonagoo commision to gonagoo account  
        if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
        }else {
          $data_g_mstr = array(
            "gonagoo_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($currency_code),
          );
          $this->api->insert_gonagoo_master_record($data_g_mstr);
          $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
        }
        $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);
        $gonagoo_balance = trim($gonagoo_balance) + trim($gonagoo_commission);
        $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);

        $g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code));
        $data_g_hstry = array(
          "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
          "order_id" => (int)$job_id,
          "user_id" => (int)$cust_id,
          "type" => 1,
          "transaction_type" => 'job_commission',
          "amount" => trim($gonagoo_commission),
          "datetime" => $today,
          "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
          "transfer_account_type" => 'NULL',
          "transfer_account_number" => 'NULL',
          "bank_name" => 'NULL',
          "account_holder_name" => 'NULL',
          "iban" => 'NULL',
          "email_address" => 'NULL',
          "mobile_number" => 'NULL',
          "transaction_id" => 'NULL',
          "currency_code" => trim($currency_code),
          "country_id" => trim($provider_details['country_id']),
          "cat_id" => (int)$job_details['sub_cat_id'],
        );
        $this->api->insert_payment_in_gonagoo_history($data_g_hstry);


        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('You are accepted milestone and payment released for job milestone. ID').': '.$milestone_id;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('You are accepted milestone request'), 'type' => 'milestone-accept', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('You are accepted milestone request'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('You are accepted milestone request'), trim($message));
        //Customer Notifications--------------------------

        //Invoice Notification----------------------------
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
          $provider_id = $provider_details['cust_id'];
          $provider_name=$provider_details['firstname']." ".$provider_details['lastname'];
          $customer_name=$customer_details['firstname']." ".$customer_details['lastname'];
          $phpinvoice = 'resources/fpdf-master/phpinvoicejob.php';
          require_once($phpinvoice);
          //Language configuration for invoice
          $lang = $this->input->post('lang', TRUE);
          if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
          else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
          else { $invoice_lang = "englishApi_lang"; }
          //Invoice Configuration
          $invoice = new phpinvoice('A4',trim($job_details['currency_code']),$invoice_lang);
          $invoice->setLogo("resources/fpdf-master/invoice-header.png");
          $invoice->setColor("#000");
          $invoice->setType("");
          $invoice->setReference($job_details['job_id']."-".$milestone_id);
          $invoice->setDate(date('M dS ,Y',time()));
          $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
          $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
          $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));
          $user_country = $this->api->get_country_details(trim($customer_details['country_id']));
          $user_state = $this->api->get_state_details(trim($customer_details['state_id']));
          $user_city = $this->api->get_city_details(trim($customer_details['city_id']));
          $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
          $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
          $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
          $invoice->setTo(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($customer_details['email1'])));
          $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email_id'])));
          $eol = PHP_EOL;
          /* Adding Items in table */
          $order_for = ucfirst($milestone_details['milestone_details']);
          $rate = $this->lang->line('paid_deposite_amount');
          $total = round(trim($milestone_price),2);
          $payment_datetime = substr($today, 0, 10);
          $milestone=$milestone_id;
          //set items
          $invoice->addItem($payment_datetime,$order_for,$milestone,$rate,$total);
          /* Add totals */       
          $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
          /* Set badge */ 
          if(true){
            
          $invoice->addBadge($this->lang->line('Job Amount Paid'));
          }
          /* Add title */
          $invoice->addTitle($this->lang->line('tnc'));
          /* Add Paragraph */
          $invoice->addParagraph($gonagoo_address['terms']);
          /* Add title */
          $invoice->addTitleFooter($this->lang->line('thank_you_job'));
          /* Add Paragraph */
          $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
          /* Set footer note */
          $invoice->setFooternote($gonagoo_address['company_name']);
          /* Render */
          $invoice->render($job_details['job_id']."-".$milestone_id.'_customer_job.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
          //Update File path
          $pdf_name = $job_details['job_id']."-".$milestone_id.'_customer_job.pdf';
          //Move file to upload folder
          rename($pdf_name, "resources/troubleshoot-invoices/".$pdf_name);
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Job Invoice');
            $emailAddress = trim($provider_details['email_id']);
            $fileToAttach = "resources/troubleshoot-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          //Workstream entry--------------------------------
          //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $message = $this->lang->line('Accepted milestone request');
            $data = array(
              'provider_id' => $provider_id,
              'cust_id' => $cust_id,
              'job_id' => $job_id,
            );
            $workstream_id = $this->api->check_workstream_master($data);
            $update_data = array(
              'workstream_id' => (int)$workstream_id,
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' => "resources/troubleshoot-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment_invoice',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
        //Invoice Notification----------------------------
        
        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $message = $this->lang->line('Milestone accepted and paymen recieved. ID').': '.$milestone_id;
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            
            foreach ($device_details_provider as $value) {

              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Accepted milestone request'), 'type' => 'milestone-accept', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN

            $msg_apn_provider =  array('title' => $this->lang->line('Accepted milestone request'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Milestone payment received'), trim($message));
        //Provider Notifications--------------------------

        //update paid history
        $paid_amount =  $milestone_price+$job_paid_history['paid_amount'];
        $this->api->update_job_payment_paid_history($job_id,$paid_amount, $today); 

        //update milestone
        $data = array(
          'amount_paid' => 1,
          'payment_datetime' => $today
        );
        $this->api->update_proposal_milestone_details($data,$milestone_id);
        $data = array(
          'status' => 'accept',
          'invoice_url' => $fileToAttach,
          'status_datetime' => $today
        );
        $this->api->update_proposal_milestone_request($data,$milestone_id);
        
        //set heading
        $this->session->set_flashdata('success', $this->lang->line('Successfully accepted milestone payment request'));
      }else{
        $this->session->set_flashdata('error', $this->lang->line('Unable to accept milestone payment request'));
      }
      redirect(base_url('user-panel-services/my-inprogress-jobs'),'refresh');
    }
    public function accept_job_milestone_payment_view()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $job_id = $this->input->post('job_id');
      $milestone_id = $this->input->post('milestone_id');
      $redirect_url = $this->input->post('redirect_url');
      $milestone_price = $this->input->post('milestone_price');
      $total = $this->input->post('total');
      $job_details = $this->api->get_job_details($job_id);
      $proposal_details = $this->api->get_proposal_milestone_by_proposal_id($job_details['proposal_id']);
      $customer_details = $this->api->get_user_details($cust_id);
      $provider_id=$job_details['provider_id'];
      $this->load->view('services/job_milestone_payment_customer_view', compact('milestone_id','milestone_price','job_details','provider_id','redirect_url','cust_id','proposal_details','customer_details','total'));
      $this->load->view('user_panel/footer');
    }
  //End Customer Milestone---------------------------------

  //Start Milestone payment--------------------------------
    public function job_milestone_payment_stripe()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $milestone_id = $this->input->post('milestone_id', TRUE);
      $milestone_price = $this->input->post('milestone_price', TRUE);
      $total_amount = $this->input->post('total', TRUE);
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_customer_job_details($job_id);
      $provider_details = $this->api->get_service_provider_profile($provider_id);
      $milestone_details = $this->api->get_proposal_milestone_by_milestone_id($milestone_id);
      //echo json_encode($job_details); die();
      $search_data = array(
        "country_id" => $job_details['country_id'],
        "cat_id" => $job_details['sub_cat_id'],
        "cat_type_id" => $job_details['cat_id'],
      );
      $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);
      //echo json_encode($advance_payment); die();
      $gonagoo_commission_per = $advance_payment['gonagoo_commission'];
      $gonagoo_commission_amount = ($total_amount/100)*$gonagoo_commission_per;

      $job_deposit_price = trim($total_amount);
      $payment_method = 'stripe';
      
      $paid_amount = $total_amount + $job_details['paid_amount'];
      $balance_amount = $job_details['balance_amount'] - $total_amount;
      $gonagoo_commission_amount = $job_details['gonagoo_commission_amount'] + $gonagoo_commission_amount;
      $data = array(
        'paid_amount' => $paid_amount,
        'payment_method' => $payment_method, 
        'payment_mode' => 'online', 
        'payment_by' => 'web', 
        'transaction_id' => trim($transaction_id), 
        'payment_response' => trim($stripeEmail), 
        'payment_datetime' => trim($today),
        'balance_amount' =>(int)$balance_amount, 
        'complete_paid' => ($balance_amount == "0")?"1":"0", 
        'gonagoo_commission_per' => $gonagoo_commission_per, 
        'gonagoo_commission_amount' => $gonagoo_commission_amount, 
        'status_updated_by' => 'customer', 
      );
      if($this->api->job_details_update($job_id, $data))
      {
        $job_details = $this->api->get_customer_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        $provider_id = $provider_details['cust_id'];
        //echo json_encode($provider_id); die();
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        
        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Payment completed for job Payment amount is').' : '.$total_amount;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job payment successfully'), 'type' => 'job-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job payment successfully'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job payment successfully'), trim($message));
        //Customer Notifications--------------------------

        // //Workstream entry--------------------------------
        //   //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
        //   $data = array(
        //     'provider_id' => $provider_id,
        //     'cust_id' => $cust_id,
        //     'job_id' => $job_id,
        //   );
        //   $workstream_id = $this->api->check_workstream_master($data);
        //   $update_data = array(
        //     'workstream_id' => (int)$workstream_id,
        //     'job_id' => (int)$job_id,
        //     'cust_id' => $cust_id,
        //     'provider_id' => (int)$job_details['provider_id'],
        //     'text_msg' => $message,
        //     'attachment_url' =>  'NULL',
        //     'cre_datetime' => $today,
        //     'sender_id' => $cust_id,
        //     'type' => 'payment',
        //     'file_type' => 'text',
        //     'proposal_id' => 0,
        //   );
        //   $this->api->update_to_workstream_details($update_data);
        // //Workstream entry--------------------------------
        

        //Invoice Notification----------------------------
          $provider_name=$provider_details['firstname']." ".$provider_details['lastname'];
          $customer_name=$customer_details['firstname']." ".$customer_details['lastname'];
          $phpinvoice = 'resources/fpdf-master/phpinvoicejob.php';
          require_once($phpinvoice);
          //Language configuration for invoice
          $lang = $this->input->post('lang', TRUE);
          if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
          else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
          else { $invoice_lang = "englishApi_lang"; }
          //Invoice Configuration
          $invoice = new phpinvoice('A4',trim($job_details['currency_code']),$invoice_lang);
          $invoice->setLogo("resources/fpdf-master/invoice-header.png");
          $invoice->setColor("#000");
          $invoice->setType("");
          $invoice->setReference($job_details['job_id']."-".$milestone_id);
          $invoice->setDate(date('M dS ,Y',time()));
          $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
          $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
          $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));
          $user_country = $this->api->get_country_details(trim($customer_details['country_id']));
          $user_state = $this->api->get_state_details(trim($customer_details['state_id']));
          $user_city = $this->api->get_city_details(trim($customer_details['city_id']));
          $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
          $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
          $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
          $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email_id'])));
          $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($customer_details['email1'])));
          $eol = PHP_EOL;
          /* Adding Items in table */
          $order_for = ucfirst($milestone_details['milestone_details']);
          $rate = $this->lang->line('paid_deposite_amount');
          $total = round(trim($total_amount),2);
          $payment_datetime = substr($today, 0, 10);
          $milestone=$milestone_id;
          //set items
          $invoice->addItem($payment_datetime,$order_for,$milestone,$rate,$total);
          /* Add totals */       
          $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
          /* Set badge */ 
          if($balance_amount==0){
            
          $invoice->addBadge($this->lang->line('Job Amount Paid'));
          }
          /* Add title */
          $invoice->addTitle($this->lang->line('tnc'));
          /* Add Paragraph */
          $invoice->addParagraph($gonagoo_address['terms']);
          /* Add title */
          $invoice->addTitleFooter($this->lang->line('thank_you_job'));
          /* Add Paragraph */
          $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
          /* Set footer note */
          $invoice->setFooternote($gonagoo_address['company_name']);
          /* Render */
          $invoice->render($job_details['job_id']."-".$milestone_id.'_customer_job.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
          //Update File path
          $pdf_name = $job_details['job_id']."-".$milestone_id.'_customer_job.pdf';
          //Move file to upload folder
          rename($pdf_name, "resources/troubleshoot-invoices/".$pdf_name);
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Job Invoice');
            $emailAddress = trim($customer_details['email1']);
            $fileToAttach = "resources/troubleshoot-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          //Workstream entry--------------------------------
          //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $message = $this->lang->line('Accepted milestone request');
            $data = array(
              'provider_id' => $provider_id,
              'cust_id' => $cust_id,
              'job_id' => $job_id,
            );
            $workstream_id = $this->api->check_workstream_master($data);
            $update_data = array(
              'workstream_id' => (int)$workstream_id,
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' => "resources/troubleshoot-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment_invoice',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
        //Invoice Notification----------------------------

        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $message = $this->lang->line('Payment recieved for job Payment amount is').' : '.$total_amount;
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            
            foreach ($device_details_provider as $value) {

              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job payment received'), 'type' => 'job-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN

            $msg_apn_provider =  array('title' => $this->lang->line('Job payment received'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job payment received'), trim($message));
        //Provider Notifications--------------------------
        
        //Payment Section---------------------------------
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];
          //Add Payment to customer account---------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
            } else {
              $data_cust_mstr = array(
                "user_id" => $job_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_cust_mstr);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($cust_acc_mstr['account_balance']) + trim($total_amount);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($total_amount),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($total_amount);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($total_amount),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp add Payment to customer account---------------
          //Add Payment to customer account---------------
          
          //Deduct Payment to customer account------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
            } else {
              $data_cust_mstr = array(
                "user_id" => $job_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_cust_mstr);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($cust_acc_mstr['account_balance']) - trim($total_amount);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($total_amount),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp deduct Payment to customer account------------
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($total_amount);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($total_amount),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp deduct Payment to customer account------------

          //Deduct Payment to customer account------------
          
          //Add Payment To Gonagoo Account----------------
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
            } else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }

            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($total_amount);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'job_payment',
              "amount" => trim($total_amount),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($job_details['currency_code']),
              "country_id" => trim($customer_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
          //Add Payment To Gonagoo Account----------------
          
          //Add Payment to provider Escrow----------------
            if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_pro_scrw = array(
                "deliverer_id" => (int)$provider_id,
                "scrow_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_scrow_record($data_pro_scrw);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
            }

            $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($total_amount);
            $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

            $data_scrw_hstry = array(
              "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
              "order_id" => (int)$job_id,
              "deliverer_id" => (int)$provider_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'job_payment',
              "amount" => trim($total_amount),
              "scrow_balance" => trim($scrow_balance),
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_scrow_history_record($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record_smp($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($total_amount);
              $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'job_payment',
                "amount" => trim($total_amount),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------------
          //Add Payment to provider Escrow----------------
        //Payment Section---------------------------------
        
        //Milestone transfer process Section--------------
          $job_details = $this->api->get_job_details($job_id);
          $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
          //echo json_encode($job_details); die();
          $provider_id = $job_details['provider_id'];
          $currency_code = $job_details['currency_code'];

          //Payment Section---------------------------------
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')){
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
          $provider_id = $provider_details['cust_id'];

          //Add Payment To Gonagoo Account----------------
          if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
          } else {
            $data_g_mstr = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($currency_code),
            );
            $this->api->insert_gonagoo_master_record($data_g_mstr);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
          }
          $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);

          if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($currency_code))) {
          } else {
            $data_pro_scrw = array(
              "deliverer_id" => (int)$provider_id,
              "scrow_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($currency_code),
            );
            $this->api->insert_scrow_record($data_pro_scrw);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($currency_code));
          }
          $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']);
          
          $search_data = array(
            "country_id" => $job_details['country_id'],
            "cat_type_id" => $job_details['cat_id'],
            "cat_id" => $job_details['sub_cat_id'],
          );
          $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);

          //echo json_encode($advance_payment); die();

          if($gonagoo_balance >= $milestone_price && $scrow_balance >= $milestone_price && is_array($advance_payment)){

            //deduct from gonagoo account  
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
            }else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency_code),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }
            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);
            $gonagoo_balance = trim($gonagoo_balance) - trim($milestone_price);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);

            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code));
            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 0,
              "transaction_type" => 'milestone_price',
              "amount" => trim($milestone_price),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($currency_code),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);

            // deduct from scrow balance
            $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($milestone_price);
            $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

            $data_scrw_hstry = array(
              "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
              "order_id" => (int)$job_id,
              "deliverer_id" => (int)$provider_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'milestone_price',
              "amount" => trim($milestone_price),
              "scrow_balance" => trim($scrow_balance),
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_scrow_history_record($data_scrw_hstry);
            //Smp deduct from scrow balance
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($currency_code))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency_code),
                );
                $this->api->insert_scrow_record_smp($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($currency_code));
              }
              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($milestone_price);
              $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'milestone_price',
                "amount" => trim($milestone_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
            //Smp deduct from scrow balance
            
            //add payment to provider account 
            if($provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_provider_mstr = array(
                "user_id" => $job_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_provider_mstr);
              $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($provider_acc_mstr['account_balance']) + trim($milestone_price);
            $this->api->update_payment_in_customer_master($provider_acc_mstr['account_id'], $provider_id, $account_balance);
            $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$provider_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $provider_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'milestone_amount',
              "amount" => trim($milestone_price),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp add payment to provider account 
              if($provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_provider_mstr = array(
                  "user_id" => $job_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_provider_mstr);
                $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($provider_acc_mstr['account_balance']) + trim($milestone_price);
              $this->api->update_payment_in_customer_master_smp($provider_acc_mstr['account_id'], $provider_id, $account_balance);
              $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$provider_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'milestone_amount',
                "amount" => trim($milestone_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp add payment to provider account 

            // commision percent 
            $g_commission_percentage = $advance_payment['gonagoo_commission'];
            $gonagoo_commission = ($milestone_price/100)*$g_commission_percentage;

            // deduct commision amount from providers account  
            if($provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_provider_mstr = array(
                "user_id" => $job_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_provider_mstr);
              $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($provider_acc_mstr['account_balance']) - trim($gonagoo_commission);
            $this->api->update_payment_in_customer_master($provider_acc_mstr['account_id'], $provider_id, $account_balance);
            $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$provider_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $provider_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'job_commission',
              "amount" => trim($gonagoo_commission),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // deduct commision amount from providers account  
              if($provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_provider_mstr = array(
                  "user_id" => $job_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_provider_mstr);
                $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($provider_acc_mstr['account_balance']) - trim($gonagoo_commission);
              $this->api->update_payment_in_customer_master_smp($provider_acc_mstr['account_id'], $provider_id, $account_balance);
              $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$provider_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $provider_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'job_commission',
                "amount" => trim($gonagoo_commission),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // deduct commision amount from providers account  


            //Add gonagoo commision to gonagoo account  
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
            }else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency_code),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }
            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);
            $gonagoo_balance = trim($gonagoo_balance) + trim($gonagoo_commission);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);

            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code));
            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'job_commission',
              "amount" => trim($gonagoo_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($currency_code),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);


            //Customer Notifications--------------------------
              $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
              $message = $this->lang->line('You are accepted milestone and payment released for job milestone. ID').': '.$milestone_id;
              $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_customer = $this->api->get_user_device_details($cust_id);
              if(!is_null($device_details_customer)) {
                $arr_customer_fcm_ids = array();
                $arr_customer_apn_ids = array();
                foreach ($device_details_customer as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('You are accepted milestone request'), 'type' => 'milestone-accept', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
                //APN
                $msg_apn_customer =  array('title' => $this->lang->line('You are accepted milestone request'), 'text' => $message);
                if(is_array($arr_customer_apn_ids)) { 
                  $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
                }
              }

              $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('You are accepted milestone request'), trim($message));
            //Customer Notifications--------------------------

            //Workstream entry--------------------------------
              //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
              $message = $this->lang->line('Accepted milestone request');
              $data = array(
                'provider_id' => $provider_id,
                'cust_id' => $cust_id,
                'job_id' => $job_id,
              );
              $workstream_id = $this->api->check_workstream_master($data);
              $update_data = array(
                'workstream_id' => (int)$workstream_id,
                'job_id' => (int)$job_id,
                'cust_id' => $cust_id,
                'provider_id' => (int)$job_details['provider_id'],
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $cust_id,
                'type' => 'milestone_accepted',
                'file_type' => 'text',
                'proposal_id' => 0,
              );
              $this->api->update_to_workstream_details($update_data);
            //Workstream entry--------------------------------
            
            //Provider Notifications--------------------------
              $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
              $message = $this->lang->line('Milestone accepted and paymen recieved. ID').': '.$milestone_id;
              $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

              if(!is_null($device_details_provider)) {
                $arr_provider_fcm_ids = array();
                $arr_provider_apn_ids = array();
                
                foreach ($device_details_provider as $value) {

                  if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Accepted milestone request'), 'type' => 'milestone-accept', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                //APN

                $msg_apn_provider =  array('title' => $this->lang->line('Accepted milestone request'), 'text' => $message);
                if(is_array($arr_provider_apn_ids)) { 
                  $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                }
              }

              $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Milestone payment received'), trim($message));
            //Provider Notifications--------------------------

            //update paid history
            $paid_amount =  $milestone_price+$job_paid_history['paid_amount'];
            $this->api->update_job_payment_paid_history($job_id,$paid_amount, $today); 

            //update milestone
            $data = array(
              'amount_paid' => 1,
              'payment_datetime' => $today
            );
            $this->api->update_proposal_milestone_details($data,$milestone_id);
            $data = array(
              'status' => 'accept',
              'invoice_url' => $fileToAttach,
              'status_datetime' => $today
            );
            $this->api->update_proposal_milestone_request($data,$milestone_id);
          }
        //Milestone transfer process Section--------------
        $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed & milestone accepted'));
      }
      else
      { $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
      }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
  //End Milestone payment----------------------------------

  //Start provider Customer mark complete------------------
    public function provider_mark_job_completed()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "complete_status" => 'request_complete',
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $pro_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been marked completed by')." ".$provider_name.". ".$this->lang->line('Kindly approve the same for complete processing')." ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job completion request'), 'type' => 'job-completion-request', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job completion request'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job completion request'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your job has been marked as completed and send to')." ".$customer_name.", ".$this->lang->line('for approval').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job completion request'), 'type' => 'job-completion-request', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job completion request'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job completion request'), trim($message));
        //------------------------------------------
          
        $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('The job has been marked completed on')." ".$today.", ".$this->lang->line('Kindly approve the same for complete processing').". ";

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'mark_completed',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job marked as completed.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to mark completed. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function customer_accepted_job_complete()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "job_status" => 'completed',
        "job_status_datetime" => $today,
        "complete_status" => 'completed',
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been completed').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job completed'), 'type' => 'job-completed', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job completed'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job completed'), trim($message));
        //------------------------------------------------

        //Notifications to Provider-----------------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your job ID')." ".$job_id.", ".$this->lang->line('has been completed').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job completed'), 'type' => 'job-completed', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job completed'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job completed'), trim($message));
        //------------------------------------------------
        
        //Update to workroom------------------------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job marked as completed.');
          if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'job_completed',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          }
        //------------------------------------------------
          
        $this->session->set_flashdata('success', $this->lang->line('Job cancellation request sent.'));
        redirect(base_url('user-panel-services/my-completed-jobs'), 'refresh');
      } else { 
        $this->session->set_flashdata('error', $this->lang->line('Unable to send cancellation request. Try again...')); 
        redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
      }
    }
  //End provider Customer mark complete--------------------

  //Start Services Account/E-Wallet/Withdrawals------------
    public function account_login()
    {
      $section = $this->uri->segment(3);
      $customer =  $this->api->get_consumer_datails($this->cust_id, true);
      $this->load->view('services/account_login_view',compact('section','customer'));
      $this->load->view('user_panel/footer');
    }
    public function account_login_confirm()
    {
      $section = $this->input->post('section', TRUE);
      $password = $this->input->post('password', TRUE);
      $cust =  $this->api->get_consumer_datails($this->cust_id, true);
      if($cust['password'] == $password){
       $this->session->set_flashdata('success', $this->lang->line('Login successfully'));
       if($section == 1){
        redirect('user-panel-services/services-wallet');
       }elseif($section == 2){
        redirect('user-panel-services/my-scrow');
       }elseif($section == 3){
        redirect('user-panel-services/services-withdrawals-request');
       }
      }else{
        $this->session->set_flashdata('error', $this->lang->line('Please enter correct password'));
        redirect('user-panel-services/account-login/'.$section);
      }
    }
    public function services_wallet()
    {
      $amount = $this->user->get_customer_account_withdraw_request_sum($this->cust_id, 'request');
      $balance = $this->user->check_user_account_balance_smp($this->cust_id);
      $this->load->view('services/services_account_history_list_view', compact('balance','amount'));
      $this->load->view('user_panel/footer');
    }
    public function services_account_history()
    {
      $history = $this->api->get_services_account_history_smp($this->cust_id);
      $this->load->view('services/services_transaction_history_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }    
    public function services_invoices_history()
    {
      $details = $this->api->get_services_invoices($this->cust_id);
      $this->load->view('services/services_invoice_history_list_view', compact('details'));
      $this->load->view('user_panel/footer');
    }
    public function my_scrow()
    {
      $balance = $this->api->deliverer_scrow_master_list_smp($this->cust_id);
      $this->load->view('services/services_scrow_history_list_view', compact('balance'));
      $this->load->view('user_panel/footer');
    }
    public function user_scrow_history()
    {
      $history = $this->api->get_customer_scrow_history_smp($this->cust_id);
      $this->load->view('services/services_scrow_account_history_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }    
    public function avalaible_milestone_for_withdraw()
    {
      // echo json_encode($_POST); die();
      $account_id = $this->input->post('account_id', TRUE);
      $currency_code = $this->input->post('currency_code', TRUE);
      $job_list = $this->api->get_service_job_list($this->cust_id);
      // echo $this->db->last_query(); die();
      for($i=0; $i<sizeof($job_list); $i++) {
        if($job_list[$i]['service_type'] == 0){
          $completed_milestone = $this->api->get_job_completed_milestone($job_list[$i]['job_id']);
          $job_list[$i]['completed_milestone'] = sizeof($completed_milestone);
        }else{
          $job_list[$i]['completed_milestone'] = "offer";
        }
      }    
      $this->load->view('services/services_job_withdraw_list', compact('job_list','account_id','currency_code'));
      $this->load->view('user_panel/footer');
    }
    public function job_milestone_for_withdraw()
    {
      $account_id = $this->input->post('account_id', TRUE);
      $currency_code = $this->input->post('currency_code', TRUE);
      $job_id = $this->input->post('job_id', TRUE);
      $job_details = $this->api->get_customer_job_details($job_id);
      $current_date = Date('Y-m-d H:i:s');
      $search_data = array(
        "country_id" => $job_details['country_id'],
        "cat_id" => $job_details['sub_cat_id'],
        "cat_type_id" => $job_details['cat_id'],
      );
      $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);
      $milestone = $this->api->get_job_completed_milestone($job_id);
      // echo json_encode($milestone); die();
      // echo json_encode($milestone[0]['security_days_date']); die();
      $this->load->view('services/services_job_milestone_withdraw_list', compact('current_date','milestone','account_id','currency_code','job_details','advance_payment'));
      $this->load->view('user_panel/footer');
    }
    public function services_withdrawals_request()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'request');
      $this->load->view('services/services_withdrawal_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function services_withdrawals_accepted()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'accept');
      $this->load->view('services/services_withdrawal_accepted_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function services_withdrawals_rejected()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'reject');
      $this->load->view('services/services_withdrawal_rejected_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function services_withdrawal_request_details()
    {
      $req_id = $this->input->post('req_id'); 
      $details = $this->user->withdrawal_request_details((int)$req_id);
      $this->load->view('services/services_withdrawal_request_detail_view', compact('details'));
      $this->load->view('user_panel/footer');
    }
    public function create_services_withdraw_request()
    {
      $account_id = $this->input->post('account_id', TRUE);
      $currency_code = $this->input->post('currency_code', TRUE);
      $job_id = $this->input->post('job_id', TRUE);
      $milestone_id = $this->input->post('milestone_id', TRUE);
      $job_details = $this->api->get_customer_job_details($job_id);
      if($milestone_id == "NULL"){
        $withdraw_amount = $job_details['paid_amount']-$job_details['gonagoo_commission_amount'];
      }else{
        $withdraw_amount = $this->input->post('withdraw_amount', TRUE);
      }
      $amount = $this->api->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
      $accounts = $this->api->get_payment_details($this->cust_id);
      $balance = $this->api->check_user_account_balance_by_id((int)$account_id);
      // echo $this->db->last_query(); die();
      // echo json_encode($balance); die();    
      $final_balance = $balance['account_balance'] - $amount['amount'];
      $currency_code = $balance['currency_code'];

      $this->load->view('services/services_withdraw_request_create_view',compact('final_balance','accounts','amount','currency_code','withdraw_amount','job_id','milestone_id'));
      $this->load->view('user_panel/footer');
    }
    public function withdraw_request_sent_services()
    {
      $cust_id = (int)$this->cust_id;
      $job_id = $this->input->post('job_id', TRUE);
      $milestone_id = $this->input->post('milestone_id', TRUE);
      $password = $this->input->post('password', TRUE);
      $user_details = $this->api->get_user_details($this->cust_id);
      if($user_details['password'] == trim($password)) {
      $amount = $this->input->post('amount', TRUE);
      $currency_code= $this->input->post('currency_code', TRUE);
      $job_details = $this->api->get_customer_job_details($job_id);
      $transfer_account_type = $this->input->post('transfer_account_type', TRUE);
      $balance = $this->api->check_user_account_balance($this->cust_id, $currency_code);
      $requested_amount = $this->user->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
      $withdrawable_amount = $balance - $requested_amount['amount'];
      if($amount <= $withdrawable_amount) {
        $accounts = $this->api->get_payment_details($this->cust_id);
        $transfer_account_number = $accounts["$transfer_account_type"];
        $today = date('Y-m-d h:i:s');
        $data = array(
          "user_id" => (int)$cust_id,
          "amount" => trim($amount),
          "req_datetime" => $today,
          "user_type" => 0,
          "response" => 'request',
          "response_datetime" => 'NULL',
          "reject_remark" => 'NULL',
          "transfer_account_type" => $transfer_account_type,
          "transfer_account_number" => trim($transfer_account_number),
          "currency_code" => trim($currency_code),
          "job_id" => $job_id,
          "milestone_id" => $milestone_id
        );
        if( $sa_id = $this->api->user_account_withdraw_request($data))
        {
          if($milestone_id == "NULL"){
            $data = array('withdraw_status' => "requested");
            $this->api->update_job_details($job_id ,$data);
          }elseif($milestone_id > 0){
            $data = array('withdraw_status' => "requested");
            $this->api->update_proposal_milestone_request($data , $milestone_id);
          }  
          $this->session->set_flashdata('success', $this->lang->line('withdraw_request_sent'));
        } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_send_request')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('entered_wrong_amount')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('unauthorized_access')); }
      redirect('user-panel-services/services-wallet');
    }
  //End services Account/E-Wallet/Withdrawals--------------
  
    public function customer_job_cancel()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $reason = $this->input->post('reason', TRUE);
      $other_reason = $this->input->post('other_reason', TRUE);
      $redirect_url = $this->input->post('redirect_url', TRUE);
      if($other_reason == '') { $other_reason = 'NULL'; }
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "cancellation_status" => 'cancel_request',
        "cancellation_reason" => $reason,
        "cancellation_desc" => $other_reason,
        "status_updated_by" => 'customer',
        "cancelled_date" => $today,
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your Job ID').": ".$job_id." ".$this->lang->line('has been sent for cancellation').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation request'), 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job cancellation request'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job cancellation request'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your Job has been accepted by')." ".$customer_name.", ".$this->lang->line('is requesting to cancel it').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation request'), 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job cancellation request'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job cancellation request'), trim($message));
        //------------------------------------------
          
        $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job accepted on')." ".$job_details['proposal_accepted_datetime'].", ".$this->lang->line('has been requested for cancellation').". ".$this->lang->line('Cancellation Reason').": ".$job_details['cancellation_reason'].". ";

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'cancel_request',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job cancellation request sent.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to send cancellation request. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function provider_job_cancel()
    {
      //echo json_encode($_POST); die();
      //$cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $reason = $this->input->post('reason', TRUE);
      $other_reason = $this->input->post('other_reason', TRUE);
      $redirect_url = $this->input->post('redirect_url', TRUE);
      if($other_reason == '') { $other_reason = 'NULL'; }
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "cancellation_status" => 'cancel_request',
        "cancellation_reason" => $reason,
        "cancellation_desc" => $other_reason,
        "status_updated_by" => 'provider',
        "cancelled_date" => $today,
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details($job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        $cust_id = $job_details['cust_id'];
        $provider_id = $job_details['provider_id'];
        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your Job ID').": ".$job_id." ".$this->lang->line('has been sent for cancellation').". ".$this->lang->line('Your Customer')." ".$customer_name.". ";
           //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation request'), 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job cancellation request'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job cancellation request'), trim($message));
        //------------------------------------------

        //Notifications to Customer----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your Job provider')." ".$provider_details['company_name'].", ".$this->lang->line('is requesting to cancel it').". ";

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation request'), 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job cancellation request'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job cancellation request'), trim($message));         
        //------------------------------------------
          
        $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Job accepted on')." ".$job_details['proposal_accepted_datetime'].", ".$this->lang->line('has been requested for cancellation').". ".$this->lang->line('Cancellation Reason').": ".$job_details['cancellation_reason'].". ";

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $provider_id,
            'type' => 'cancel_request',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job cancellation request sent.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to send cancellation request. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function provider_withdraw_cancel_request()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "cancellation_status" => 'NULL',
        "cancellation_reason" => 'NULL',
        "cancellation_desc" => 'NULL',
        "status_updated_by" => 'NULL',
        "cancelled_date" => 'NULL',
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        $cust_id = $job_details['cust_id']; 
        $provider_id = $job_details['provider_id']; 
        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('cancellation request has been withdrawn').". ".$this->lang->line('Your Customer')." ".$customer_name.". ";
          //Send Push Notifications

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation request withdrawn'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job cancellation request withdrawn'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job cancellation request withdrawn'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Cancellation request has been withdrawn by')." ".$customer_name.", ".$this->lang->line('for the job. ID').". ".$job_id;
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation request withdrawn'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job cancellation request withdrawn'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job cancellation request withdrawn'), trim($message));
        //------------------------------------------
          
        $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Job cancellation has been withdrawn').", ".$this->lang->line('for the job. ID').". ".$job_id;

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $provider_id,
            'type' => 'cancel_withdraw',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job cancellation request has been withdrawn.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to withdraw job cancellation request. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function provider_accept_cancellation_request()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "cancellation_status" => 'dispute_closed',
        "job_status" => 'cancelled',
        "job_status_datetime" => $today,
      );
      if($this->api->update_job_details($job_id, $update_data)) {
        $job_details = $this->api->get_job_details($job_id);
        $cust_id = $job_details['cust_id'];
        $provider_id = $job_details['provider_id'];
        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }  
        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been cancelled').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation.'), 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job cancellation.'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------

        //Notifications to Provider-----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your Job has been accepted by')." ".$customer_name.", ".$this->lang->line('is cancelled').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation.'), 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job cancellation.'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------
        $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Job accepted on')." ".$job_details['proposal_accepted_datetime'].", ".$this->lang->line('has been cancelled').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $provider_id,
            'type' => 'job_cancelled',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }
        $job_details = $this->api->get_job_details($job_id);
        $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
        $paid_amount = $job_details['paid_amount']; $paid_amount = (int)$paid_amount;
        $paid_amount_history = $job_paid_history['paid_amount']; $paid_amount_history = (int)$paid_amount_history;
        $scrow_amount = $paid_amount - $paid_amount_history;
        if($scrow_amount > 0) {
          //Payment Section---------------------------------
            //Deduct Payment From Provider Escrow-----------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$job_details['provider_id'],
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($scrow_amount);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$job_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'job_cancel_refund',
                "amount" => trim($scrow_amount),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp deduct Payment From Provider Escrow-----------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$job_details['provider_id'],
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($scrow_amount);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$job_details['provider_id'],
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'job_cancel_refund',
                  "amount" => trim($scrow_amount),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp deduct Payment From Provider Escrow-----------
            //----------------------------------------------

            //Deduct Payment From Gonagoo Account-----------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) - trim($scrow_amount);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => $job_details['cust_id'],
                "type" => 0,
                "transaction_type" => 'job_cancel_refund',
                "amount" => trim($scrow_amount),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------

            //Add Payment To Customer Account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($scrow_amount);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $job_details['cust_id'], $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $job_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'job_cancel_refund',
                "amount" => trim($scrow_amount),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              // Smp add Payment To Customer Account---------------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($scrow_amount);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $job_details['cust_id'], $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $job_details['cust_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'job_cancel_refund',
                  "amount" => trim($scrow_amount),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              // Smp add Payment To Customer Account---------------
            //----------------------------------------------
          //Payment Section---------------------------------
        }

        $this->session->set_flashdata('success', $this->lang->line('Job has been cancelled.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to cancel the job. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function customer_accept_cancellation_request()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');

      $update_data = array(
        "cancellation_status" => 'dispute_closed',
        "job_status" => 'cancelled',
        "job_status_datetime" => $today,
      );
      if($this->api->update_job_details($job_id, $update_data)){
        $job_details = $this->api->get_job_details($job_id);
        $cust_id = $job_details['cust_id'];
        $provider_id = $job_details['provider_id'];
        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }  
        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been cancelled');
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation.'), 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job cancellation.'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------

        //Notifications to Provider-----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your Job has been accepted by')." ".$customer_name.", ".$this->lang->line('is cancelled').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job cancellation.'), 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job cancellation.'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------
        
        $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your Job has been accepted by')." ".$customer_name.", ".$this->lang->line('is cancelled').". ";

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'job_cancelled',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }
        $job_details = $this->api->get_job_details($job_id);
        $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
        $paid_amount = $job_details['paid_amount']; $paid_amount = (int)$paid_amount;
        $paid_amount_history = $job_paid_history['paid_amount']; $paid_amount_history = (int)$paid_amount_history;
        $scrow_amount = $paid_amount - $paid_amount_history;
        if($scrow_amount > 0) {
          //Payment Section---------------------------------
            //Deduct Payment From Provider Escrow-----------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$job_details['provider_id'],
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($scrow_amount);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$job_details['provider_id'],
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'job_cancel_refund',
                "amount" => trim($scrow_amount),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp deduct Payment From Provider Escrow-----------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$job_details['provider_id'],
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($scrow_amount);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$job_details['provider_id'],
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'job_cancel_refund',
                  "amount" => trim($scrow_amount),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp deduct Payment From Provider Escrow-----------
            //----------------------------------------------

            //Deduct Payment From Gonagoo Account-----------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) - trim($scrow_amount);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => $job_details['cust_id'],
                "type" => 0,
                "transaction_type" => 'job_cancel_refund',
                "amount" => trim($scrow_amount),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------

            //Add Payment To Customer Account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($scrow_amount);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $job_details['cust_id'], $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $job_details['cust_id'],
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'job_cancel_refund',
                "amount" => trim($scrow_amount),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              // Smp add Payment To Customer Account---------------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($scrow_amount);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $job_details['cust_id'], $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $job_details['cust_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'job_cancel_refund',
                  "amount" => trim($scrow_amount),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              // Smp add Payment To Customer Account---------------

            //----------------------------------------------
          //Payment Section---------------------------------
        }
        $this->session->set_flashdata('success', $this->lang->line('Job has been cancelled.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to cancel the job. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url), 'refresh');
    }
    public function customer_create_dispute_job()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $dispute_title = $this->input->post('dispute_title', TRUE);
      $dispute_cat_id = $this->input->post('dispute_cat_id', TRUE); $dispute_cat_id = (int) $dispute_cat_id;
      $dispute_sub_cat_id = $this->input->post('dispute_sub_cat_id', TRUE);
      $dispute_sub_cat_id = (int) $dispute_sub_cat_id;
      $dispute_desc = $this->input->post('dispute_desc', TRUE);
      $request_money_back = $this->input->post('request_money_back', TRUE);
      $request_money_back = ($request_money_back == 'on') ? 1 : 0;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');
      $job_details = $this->api->get_job_details($job_id);

      $insert_data = array(
        "cancellation_status" => 'dispute_raised',
      );
      if($this->api->update_job_details($job_id, $insert_data)) {
        $update_data = array(
          "dispute_cat_id" => $dispute_cat_id,
          "dispute_sub_cat_id" => $dispute_sub_cat_id,
          "dispute_title" => trim($dispute_title),
          "dispute_desc" => trim($dispute_desc),
          "service_id" => $job_id,
          "service_type" => $job_details['service_type'],
          "cre_datetime" => $today,
          "cust_id" => $job_details['cust_id'],
          "provider_id" => $job_details['provider_id'],
          "dispute_status" => 'open',
          "dispute_withdraw_datetime" => 'NULL',
          "dispute_in_progress_datetime" => 'NULL',
          "dispute_closed_datetime" => 'NULL',
          "admin_id" => 0,
          "dispute_created_by" => $cust_id,
          "request_money_back" => $request_money_back,
          "money_back_status" => 0,
          "money_back_datetime" => 'NULL',
        );
        $dispute_id = $this->api->create_job_dispute($update_data);

        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been marked as disputed').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          $message_provider = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been marked as disputed').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job marked disputed'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job accepted on')." ".$job_details['proposal_accepted_datetime']." ".$customer_name.", ".$this->lang->line('is marked as disputed and send to Gonagoo admin for process').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job marked disputed'), trim($message));
        //------------------------------------------
        
        //update to workroom
        $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('This job has been marked as disputed and send to Gonagoo admin for process');
        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'dispute_raised',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }
        // Admin chat to customer
        $update_data = array(
          'job_id' => (int)$job_id,
          'cust_id' => $cust_id,
          'provider_id' => 0,
          'admin_id' => 0,
          'is_admin' => 1,
          'cre_datetime' => $today,
          'status' => 1,
        );
        if($id = $this->api->check_workstream_master_admin($update_data)){
          $workstream_id = $id;
        }else{
          $workstream_id = $this->api->update_to_workstream_master($update_data);
        }
        $update_data = array(
          'workstream_id' => (int)$workstream_id,
          'job_id' => (int)$job_id,
          'cust_id' => $cust_id,
          'provider_id' => 0,
          'admin_id' => 0,
          'is_admin' => 1,
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => 0,
          'type' => 'dispute_raised',
          'file_type' => 'text',
          'proposal_id' => 0,
        );
        $this->api->update_to_workstream_details($update_data);

        //Admin chat to provider 
        $update_data = array(
          'job_id' => (int)$job_id,
          'provider_id' => (int)$job_details['provider_id'],
          'cust_id' => 0,
          'admin_id' => 0,
          'is_admin' => 1,
          'cre_datetime' => $today,
          'status' => 1,
        );
        if($id = $this->api->check_workstream_master_admin($update_data)){
          $workstream_id = $id;
        }else{
          $workstream_id = $this->api->update_to_workstream_master($update_data);
        }
        $update_data = array(
          'workstream_id' => (int)$workstream_id,
          'job_id' => (int)$job_id,
          'provider_id' => (int)$job_details['provider_id'],
          'cust_id' => 0,
          'admin_id' => 0,
          'is_admin' => 1,
          'text_msg' => $message_provider,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => 0,
          'type' => 'dispute_raised',
          'file_type' => 'text',
          'proposal_id' => 0,
        );
        $this->api->update_to_workstream_details($update_data);


        $this->session->set_flashdata('success', $this->lang->line('Job dispute request sent to Gonagoo admin.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to request for job dispute. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url),'refresh');
    }
    public function provider_create_dispute_job()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $dispute_title = $this->input->post('dispute_title', TRUE);
      $dispute_cat_id = $this->input->post('dispute_cat_id', TRUE); $dispute_cat_id = (int) $dispute_cat_id;
      $dispute_sub_cat_id = $this->input->post('dispute_sub_cat_id', TRUE); $dispute_sub_cat_id = (int) $dispute_sub_cat_id;
      $dispute_desc = $this->input->post('dispute_desc', TRUE);
      $request_money_back = $this->input->post('request_money_back', TRUE);
      $request_money_back = ($request_money_back == 'on') ? 1 : 0;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $today = date('Y-m-d H:i:s');
      $job_details = $this->api->get_job_details($job_id);

      $insert_data = array(
        "cancellation_status" => 'dispute_raised',
      );
      if($this->api->update_job_details($job_id, $insert_data)) {
        $update_data = array(
          "dispute_cat_id" => $dispute_cat_id,
          "dispute_sub_cat_id" => $dispute_sub_cat_id,
          "dispute_title" => trim($dispute_title),
          "dispute_desc" => trim($dispute_desc),
          "service_id" => $job_id,
          "service_type" => $job_details['service_type'],
          "cre_datetime" => $today,
          "cust_id" => $job_details['cust_id'],
          "provider_id" => $job_details['provider_id'],
          "dispute_status" => 'open',
          "dispute_withdraw_datetime" => 'NULL',
          "dispute_in_progress_datetime" => 'NULL',
          "dispute_closed_datetime" => 'NULL',
          "admin_id" => 0,
          "dispute_created_by" => $cust_id,
          "request_money_back" => $request_money_back,
          "money_back_status" => 0,
          "money_back_datetime" => 'NULL',
        );
        $dispute_id = $this->api->create_job_dispute($update_data);

        $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('has been marked as disputed').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job marked disputed'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job accepted on')." ".$job_details['proposal_accepted_datetime']." ".$customer_name.", ".$this->lang->line('is marked as disputed and send to Gonagoo admin for process').". ";
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job marked disputed'), trim($message));
        //------------------------------------------
        
        //update to workroom
        $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('This job has been marked as disputed and send to Gonagoo admin for process');
        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => (int)$job_details['cust_id'],
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'dispute_raised',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        }

        $this->session->set_flashdata('success', $this->lang->line('Job dispute request sent to Gonagoo admin.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Unable to request for job dispute. Try again...')); }
      redirect(base_url('user-panel-services/'.$redirect_url),'refresh');
    }
    public function post_troubleshoot_job()
    {
      $category_types = $this->api->get_category_types();
      $countries = $this->api->get_countries();
      $user_details = $this->api->get_user_details($this->cust_id);
      $currency_details = $this->api->get_country_currency_detail($user_details['country_id']);
      //echo json_encode($currency_details); die();
      $this->load->view('services/customer_troubleshoot_job',compact('currency_details','category_types','countries'));
      $this->load->view('user_panel/footer');
      // echo json_encode("hii");die();
    }
    public function register_troubleshoot_job()
    {
      // echo json_encode($this->cust_id);die();
      $cust_id = $this->cust_id;
      $job_title = $this->input->post('job_title', TRUE);
      $cat_id = $this->input->post('category', TRUE);
      $sub_cat_id = $this->input->post('sub_category', TRUE);
      $job_desc = $this->input->post('description', TRUE);
      $address = $this->input->post('toMapID', TRUE);

      $immdediate = $this->input->post('immdediate', TRUE);
      $complete_date = $this->input->post('start_date', TRUE);

      $customer_details = $this->api->get_user_details($cust_id);
      $currency_details = $this->api->get_country_currency_detail($customer_details['country_id']);
      $country_name = $this->input->post('country', TRUE);
      $country_id = $this->api->get_country_id_by_name($country_name);
       $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']);

      $size = $this->input->post('size', TRUE);
      $questions=array();
      $answers=array(); 
      for ($i=1; $i <=$size ; $i++) {
        array_push($questions ,$this->input->post('ques_'.$i, TRUE));
        array_push($answers ,$this->input->post('ans_'.$i, TRUE));
      }
     
      $today = date('Y-m-d h:i:s');
      $insert_data = array(
        "job_title" => trim($job_title),
        "cust_id" => trim($cust_id),
        "cat_id" => trim($cat_id),
        "sub_cat_id" => trim($sub_cat_id),
        "job_desc" => trim($job_desc),

        "address" => trim($address),
     
        "country_id" => trim($country_id),
        "cre_datetime" => trim($today),
        "cust_name" => $customer_name,
        "cust_contact" => $customer_details['mobile1'],
        "cust_email" => $customer_details['email1'],
        "job_type"=>1,
        "immediate" => $immdediate,
        "location_type"=>"Onsite",
        "visibility"=>"public",
        "currency_id"=>$currency_details['currency_id'],
        "currency_code"=>$currency_details['currency_sign'],
      );
      $insert_data['complete_date']=($immdediate)?date("Y-m-d" , strtotime($complete_date)):"";
      // echo json_encode($insert_data);die();
      if($job_id = $this->api->register_job_details($insert_data)){

         $k=0;
          foreach ($answers as $ans) {
            $data=array(
              'question'=>$questions[$k],
              'answer'=>$answers[$k],
              'job_id'=>$job_id
            );
            if($answers[$k]){
              $this->db->insert("tbl_question_answer" , $data);
            // echo json_encode($data);
            } 
            $k++;
          }
          $job_details = $this->api->get_customer_job_details($job_id);
          $customer_details = $this->api->get_user_details($cust_id);
        
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your  Troubleshoot job is posted').": ".$job_id;
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Troubleshoot post'), 'type' => 'troubleshoot post', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Troubleshoot post'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
          //Notifications to customers----------------

          //Update to workstream master
          $update_data = array(
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => 0,
            'cre_datetime' => $today,
            'status' => 1,
          );
          if($id = $this->api->check_workstream_master($update_data)){
            $workstream_id = $id;
          }else{
            $workstream_id = $this->api->update_to_workstream_master($update_data);
          }
          $update_data = array(
            'workstream_id' => (int)$workstream_id,
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => 0,
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'job_started',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        redirect(base_url('user-panel-services/list-of-freelancers/'.$job_id),'refresh');
      }
    }
    public function browse_troubleshooter_old()
    {
      $cust_id = $this->cust_id;
      $data =array(
        'cust_id' => $cust_id
      );
      $jobs = $this->api->get_troubleshoot_list($data);
      // echo $this->db->last_query(); die();
      $filter = "basic"; 
      $category_types = $this->api->get_category_types();
      $fav_jobs = $this->api->get_favorite_jobs($this->cust_id);
      $fav_projects = array();
      foreach ($fav_jobs as $fav_key) {
        array_push($fav_projects, $fav_key['job_id']);
      }
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }
      // echo json_encode($jobs); die();
      $this->load->view('services/browse_troubleshoot_list_view', compact('jobs','filter','fav_projects','category_types'));
      $this->load->view('user_panel/footer');
    }    
    public function dashboard_services_provider()
    {
        $cust = $this->cust;
        $countries = $this->user->get_countries();
        $cust_id = $this->cust_id;
        $this->load->view('services/create_service_provider_profile_view', compact('cust_id','countries','cust'));
        $this->load->view('user_panel/footer');
    }
    public function send_proposal_view_per_hour()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id');
      $redirect_url = $this->input->post('redirect_url');
      $job_details = $this->api->get_customer_job_details($job_id);
      $country_currency = $this->api->get_country_id_by_currency_id($job_details['currency_id']);
      $payment_percent = $this->api->get_job_advance_payment_percent($country_currency['country_id'] , $job_details['cat_id'] , $job_details['sub_cat_id']);
      // echo json_encode($country_currency['country_id']);die();
      $job_customer_profile = $this->api->get_user_details($job_details['cust_id']);
      $freelancers_profile = $this->api->get_service_provider_profile($this->cust_id);
      // echo json_encode($freelancers_profile);die();
      $datetime1 = new DateTime(date('Y-m-d h:i:s'));
      $datetime2 = new DateTime($job_details['cre_datetime']);
      $difference = $datetime1->diff($datetime2);
      $ending = 30-$difference->d;
      $last_job = $this->api->get_last_job_of_customer($job_customer_profile['cust_id'], 'last');
      $completed_job = $this->api->get_last_job_of_customer($job_customer_profile['cust_id'] , 'completed_count');
      $fav_jobs = $this->api->get_favorite_jobs($this->cust_id);
      $fav_projects = array();
      foreach ($fav_jobs as $fav_key) {
        array_push($fav_projects, $fav_key['job_id']);
      }
      $proposals = $this->api->get_job_proposals_list($job_details['job_id']);
      for($i=0; $i<sizeof($proposals); $i++){
        $provider_details = $this->api->get_user_details($proposals[$i]['provider_id']);
        $proposals[$i]['provider_details'] = $provider_details;
      }
      //echo json_encode($job_details); die();
      $job_details['proposal_counts'] = sizeof($proposals);
      

      $this->load->view('services/send_job_proposal_view_per_hour', compact('job_details','fav_projects','job_customer_profile','ending','last_job','completed_job','proposals','fav_projects','freelancers_profile','payment_percent','redirect_url'));
      $this->load->view('user_panel/footer');
    }
    public function send_proposal_per_hour()
    {
      // echo json_encode($_POST); die();
      $today = date('Y-m-d h:i:s');
      $description = $this->input->post('description', TRUE);
      $job_id = $this->input->post('job_id');
      $country_id = $this->input->post('country_id');
      
      $job_details = $this->api->get_customer_job_details($job_id);
      $freelancers_profile = $this->api->get_service_provider_profile($this->cust_id);      

      $answer_1 = $this->input->post('answer_1', TRUE);
      $answer_2 = $this->input->post('answer_2', TRUE);
      $answer_3 = $this->input->post('answer_3', TRUE);
      $answer_4 = $this->input->post('answer_4', TRUE);
      $answer_5 = $this->input->post('answer_5', TRUE);
      $answer_5 = $this->input->post('answer_5', TRUE);
      $notif_me = $this->input->post('notif_me', TRUE);
      if(!isset($answer_1)){$answer_1 = "";}
      if(!isset($answer_2)){$answer_2 = "";}
      if(!isset($answer_3)){$answer_3 = "";}
      if(!isset($answer_4)){$answer_4 = "";}
      if(!isset($answer_5)){$answer_5 = "";}      
      if($notif_me == "on"){$notif_me = 1;}else{$notif_me = 0;}      
      //echo json_encode($notif_me); die();
      $job_details = $this->api->get_customer_job_details($job_id);
      $files = $_FILES;
      $cpt = count($_FILES['image_url']['name']); //die();
      // echo json_encode($cpt);die();
      for($i=0; $i < $cpt; $i++)
      {
        $m=$i+1;
        // echo json_encode($i);
        if(isset($files['image_url']['name'][$i])) {
          $_FILES['image_url']['name']= $files['image_url']['name'][$i];
          $_FILES['image_url']['type']= $files['image_url']['type'][$i];
          $_FILES['image_url']['tmp_name']= $files['image_url']['tmp_name'][$i];
          $_FILES['image_url']['error']= $files['image_url']['error'][$i];
          $_FILES['image_url']['size']= $files['image_url']['size'][$i];

          $config['upload_path']    = 'resources/m4-image/jobs_proposal/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 

          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url')) {
            $uploads    = $this->upload->data();
            ${"image_url_$m"} =  $config['upload_path'].$uploads['file_name'];
          } else { 
            ${"image_url_$m"} = 'NULL'; 
          }
        } else {
         ${"image_url_$m"} = 'NULL';
          }
      }
      $cpt++;
      for ($i=$cpt; $i <= 5; $i++) { 
        ${"image_url_$i"} = 'NULL';
      }
      switch ($job_details['work_type']) {
        case "Daily":
          $per_hour_amount = $freelancers_profile['rate_per_hour'];
          $hours = $freelancers_profile['daily_hours'];
          break;
        case "Weekly":
          $per_hour_amount = $freelancers_profile['rate_per_hour_weekly'];
          $hours = $freelancers_profile['weekly_hours'];
          break;
        case "Monthly":
          $per_hour_amount = $freelancers_profile['rate_per_hour_monthly'];
          $hours = $freelancers_profile['monthly_hours'];
          break;
      }
      $update_data = array(
        'job_id' => (int)$job_id,
        'cust_id' => $job_details['cust_id'],
        'provider_id' => $this->cust_id,
        'proposal_details' => $description,
        'attachment_1' => trim(${"image_url_1"}),
        'attachment_2' => trim(${"image_url_2"}) ,
        'attachment_3' => trim(${"image_url_3"}),
        'attachment_4' => trim(${"image_url_4"}),
        'attachment_5' => trim(${"image_url_5"}),
        'answer_1' => $answer_1,
        'answer_2' => $answer_2,
        'answer_3' => $answer_3,
        'answer_4' => $answer_4,
        'answer_5' => $answer_5,
        'total_price' => $per_hour_amount,
        'hours_billing_type' => $hours,
        'currency_id' => $job_details['currency_id'],
        'currency_code' => $job_details['currency_code'],
        'country_id' => $country_id,
        'notify_proposal_closed' => $notif_me,
        'proposal_date' => $today,
        'proposal_status' => "open",
      );
      //echo json_encode($update_data);die();
      if($proposal_id = $this->api->register_job_proposal($update_data)){
        $provider_details = $this->api->get_service_provider_profile($this->cust_id);
        $balance =  $provider_details['proposal_credits']-1;
        $update_data = array(
          'proposal_credits' => $balance,
        );
        if($this->api->update_provider($update_data , $this->cust_id)){
          $provider_details = $this->api->get_service_provider_profile($this->cust_id);
          $update_data = array(
            'cust_id' => $this->cust_id,
            'type' => "deduct",
            'quantity' => 1,
            'balance' => $provider_details['proposal_credits'],
            'source_id' => $job_id,
            'source_type' => "proposal",
          );
          $this->api->insert_freelancer_proposal_credit_history($update_data);
        }

        $customer_details = $this->api->get_user_details($job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile($this->cust_id);
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')){
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('A New Proposal from')." ".$provider_name.", ".$this->lang->line('Your Job ID').": ".$job_id;
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value){
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('A New Proposal from'), 'type' => 'proposal-received', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Proposal Received'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('You have to send proposal successfully');
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details($this->cust_id);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Proposal Send'), 'type' => 'proposal-received', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Proposal Received'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------
        $message = $this->lang->line('A New Proposal from')." ".$provider_name.", ".$this->lang->line('Your Job ID').": ".$job_id;

        //Update to workstream master
        $update_data = array(
          'job_id' => (int)$job_id,
          'cust_id' => $job_details['cust_id'],
          'provider_id' => $this->cust_id,
          'cre_datetime' => $today,
          'status' => 0,
        );
        if($id = $this->api->check_workstream_master($update_data)){
          $workstream_id = $id;
        }else{
          $workstream_id = $this->api->update_to_workstream_master($update_data);
        }
        $update_data = array(
          'workstream_id' => (int)$workstream_id,
          'job_id' => (int)$job_id,
          'cust_id' => $job_details['cust_id'],
          'provider_id' => $this->cust_id,
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $this->cust_id,
          'type' => 'job_proposal',
          'file_type' => 'text',
          'proposal_id' => 1,
        );
        $this->api->update_to_workstream_details($update_data);
        $this->session->set_flashdata('success',$this->lang->line('Proposal send successfully'));
      }else{
        $this->session->set_flashdata('error',$this->lang->line('Error on sending proposal'));
      }       
      redirect(base_url('user-panel-services/pending-job-proposals'),'refresh');
    }
    public function accept_per_hour_job_proposal()
    {
      // echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $proposal_id = $this->input->post('proposal_id', TRUE); $proposal_id = (int)$proposal_id;
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_customer_job_details($job_id);
      $proposal_details = $this->api->get_job_proposals_by_proposal_id($proposal_id);
      $provider_details = $this->api->get_service_provider_profile($provider_id);

      $data = array(
        'budget' => $proposal_details['total_price'], 
        'hours_per_duration' => $proposal_details['hours_billing_type'], 
        'provider_id' => $provider_id, 
        'proposal_accepted_datetime' => $today, 
        'open_for_proposal' => 1, 
        'proposal_id' => $proposal_id, 
        'proposal_price' => $proposal_details['total_price'], 
        'job_status' => 'in_progress', 
        'provider_name' => $provider_details['firstname']." ".$provider_details['lastname'], 
        'provider_company_name' => $provider_details['company_name'], 
        'provider_contact' => $provider_details['contact_no'], 
        'provider_email' => $provider_details['email_id'], 
        'status_updated_by' => 'customer'
      );

      
      if($this->api->job_details_update($job_id, $data)){
        $job_details = $this->api->get_customer_job_details($job_id);

        $data = array( 
          'job_id' => $job_id, 
          'cust_id' => $cust_id, 
          'provider_id' => $provider_id,  
          'currency_id' => $job_details['currency_id'], 
          'currency_code' => $job_details['currency_code'], 
        );
        $job_payment_details_id = $this->api->register_job_payment_paid_history($data);

        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        $provider_id = $provider_details['cust_id'];
        //echo json_encode($provider_id); die();
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        
        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Proposal accepted successfully for Job. ID').': '.$job_id.' .';
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job proposal accepted'), 'type' => 'proposal-accepted', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job proposal accepted'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job proposal accepted'), trim($message));
        //Customer Notifications--------------------------

        //Workstream entry--------------------------------
          //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
          $data = array(
            'provider_id' => $provider_id,
            'cust_id' => $cust_id,
            'job_id' => $job_id,
          );
          $workstream_id = $this->api->check_workstream_master($data);
          $update_data = array(
            'workstream_id' => (int)$workstream_id,
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'payment',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //Workstream entry--------------------------------
        

        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $message = $this->lang->line('Proposal accepted successfully for Job. ID').': '.$job_id.' .';
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            
            foreach ($device_details_provider as $value) {

              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job proposal accepted'), 'type' => 'proposal-accepted', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN

            $msg_apn_provider =  array('title' => $this->lang->line('Job proposal accepted'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job proposal accepted'), trim($message));
        //Provider Notifications--------------------------

        //proposal accept update--------------------------
          $data = array(
            'proposal_status' => 'accept',
            'proposal_status_date' => $today,
            'proposal_status_updated_by' => 'customer',
          );
          $this->api->update_job_proposals($data,$job_id,$provider_id);
        //proposal accept update--------------------------

        //notify proposal closed--------------------------
          if($proposals = $this->api->get_job_proposals_list_who_notify($job_id,$provider_id)){
            foreach ($proposals as $prop)
            {
              $provider_details = $this->api->get_service_provider_profile($prop['provider_id']);
              $provider_id = $prop['provider_id'];
              $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
              $message = $this->lang->line('Thank you for sending proposal, customer accepted other proposal now your proposal is closed. Job ID:').': '.$job_id;
              $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_provider = $this->api->get_user_device_details((int)$provider_id);
              if(!is_null($device_details_provider)) {
                $arr_provider_fcm_ids = array();
                $arr_provider_apn_ids = array();
                foreach ($device_details_provider as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Notify proposal closed'), 'type' => 'proposal-closed', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                //APN
                $msg_apn_provider =  array('title' => $this->lang->line('Notify proposal closed'), 'text' => $message);
                if(is_array($arr_provider_apn_ids)) { 
                  $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                }
              }

              $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Notify proposal closed'), trim($message));
            }
            $this->api->close_job_proposals_except_accepted($job_id,$provider_id);
          }
        //notify proposal closed--------------------------

        $this->session->set_flashdata('success', $this->lang->line('Job proposal accepted successfully.'));
      } else { $this->session->set_flashdata('error', $this->lang->line('Proposal accept failed.')); redirect('user-panel-services/my-jobs','refresh'); }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
    public function provider_submit_hours_for_payment()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $job_details = $this->api->get_job_details($job_id);
      $proposal_milestone = $this->api->get_proposal_milestone_by_proposal_id($job_details['proposal_id']);
      //echo json_encode($proposal_milestone);die();
      $this->load->view('services/submit_hours_view',compact('job_details','proposal_milestone'));
      $this->load->view('user_panel/footer');
    }
    public  function provider_submit_hours()
    {
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $milestone_id = $this->input->post('milestone_id', TRUE); 
      $description = $this->input->post('description', TRUE); 
      $from_date = $this->input->post('from_date', TRUE); 
      $to_date = $this->input->post('to_date', TRUE); 
      $hours = $this->input->post('hours', TRUE);
      $today = date("Y-m-d H:i:s");
      $job_details = $this->api->get_job_details($job_id);
      $milestone_price = $hours*$job_details['budget'];
      //echo json_encode($milestone_price); die();
      $data = array(
        'proposal_id' => $job_details['proposal_id'],
        'job_id' => $job_id,
        'cust_id' => $job_details['cust_id'],
        'provider_id' => $job_details['provider_id'],
        'milestone_details' => $description,
        'milestone_price' => $milestone_price,
        'currency_id' => $job_details['currency_id'],
        'currency_code' => $job_details['currency_code'],
        'country_id' => $job_details['country_id'],
      );
      if($milestone_id = $this->api->insert_proposal_milestone_details($data)){
        $data = array(
          'milestone_id' => $milestone_id,
          'from_date' => $from_date,
          'to_date' => $to_date,
          'work_hours' => $hours,
          'days_type' => $job_details['billing_period'],
          'job_id' => $job_id,
          'request_description' => $description,
          'status' => "open",
          'cre_datetime' => $today,
        );
        if($this->api->register_milestone_payment_request($data)){
          $cust_id = $job_details['cust_id'];
          $provider_id = $job_details['provider_id'];
          $customer_details = $this->api->get_user_details($job_details['cust_id']);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
          $provider_id = $provider_details['cust_id'];


          //Customer Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
            $message = $this->lang->line('You have recieved milestone request for job ID').': '.$job_id;
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('You have recieved milestone request'), 'type' => 'milestone-request', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('You have recieved milestone request'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('You have recieved milestone request'), trim($message));
          //Customer Notifications--------------------------

          //Workstream entry--------------------------------
            //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $message = $this->lang->line('Hours submitted and Milestone request send');
            $data = array(
              'provider_id' => $provider_id,
              'cust_id' => $cust_id,
              'job_id' => $job_id,
            );
            $workstream_id = $this->api->check_workstream_master($data);
            $update_data = array(
              'workstream_id' => (int)$workstream_id,
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $provider_id,
              'type' => 'milestone_send',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
          //Provider Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
            $message = $this->lang->line('Hours submitted and milestone request send successfully');
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              
              foreach ($device_details_provider as $value) {

                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Milestone request send'), 'type' => 'milestone-send', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN

              $msg_apn_provider =  array('title' => $this->lang->line('Milestone request send'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Milestone request send'), trim($message));
          //Provider Notifications--------------------------

          $this->session->set_flashdata('success', $this->lang->line('Work hours submitted successfully'));
        }else{
          $this->session->set_flashdata('error', $this->lang->line('Unable to request for milestone payment'));
        }
      }
      redirect(base_url('user-panel-services/provider-inprogress-jobs'),'refresh');
    }
    public function customer_submitted_hours_details()
    {
      // echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      $job_details = $this->api->get_job_details($job_id);
      $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
      $milestone_payment_request = $this->api->get_milestone_payment_request($job_id);
      $proposal_milestone = $this->api->get_proposal_milestone_by_milestone_id($milestone_payment_request['milestone_id']);
      //echo json_encode($milestone_payment_request); die();

      $paid_amount = $job_details['paid_amount']; $paid_amount = (int)$paid_amount;
      $paid_amount_history = $job_paid_history['paid_amount']; $paid_amount_history = (int)$paid_amount_history;
      $milestone_price = $proposal_milestone['milestone_price']; $milestone_price = (int)$milestone_price;
      
      $remain = $paid_amount - $paid_amount_history;
      $min = $milestone_price - $remain;
      $max = $job_details['budget'] - $job_details['paid_amount'];

      if($remain >= $milestone_price){
        $direct_payment = "yes";
      }else{
        $direct_payment = "no";
      }
      //echo json_encode($remain); die();
      $start_date = $this->input->get('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->get('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }

      $search_data = array(
        "cust_id" => $cust_id,
        "provider_id" => 0,
        "job_status" => 'in_progress',
        "sub_cat_id" => $job_details['cat_id'],
        "cat_id" => $job_details['sub_cat_id'],
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'web',
        "last_id" => 0,
      );
      $dispute_master = $this->api->get_dispute_category_master($search_data);
      $cancellation_reasons = $this->api->get_job_cancellation_reasons();
      //echo json_encode($dispute_master); die();

      $this->load->view('services/milestone_payment_request_view_per_hour',compact('job_details','proposal_milestone','paid_amount','paid_amount_history','milestone_price','remain','direct_payment','min','max','milestone_payment_request','dispute_master','cancellation_reasons'));
      $this->load->view('user_panel/footer');
    }
    public function accept_job_work_hours_milestone_payment_view()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id; $cust_id = (int)$cust_id;
      $job_id = $this->input->post('job_id');
      $milestone_id = $this->input->post('milestone_id');
      $redirect_url = $this->input->post('redirect_url');
      $milestone_price = $this->input->post('milestone_price');
      $job_details = $this->api->get_job_details($job_id);
      $proposal_details = $this->api->get_proposal_milestone_by_proposal_id($job_details['proposal_id']);
      $customer_details = $this->api->get_user_details($cust_id);
      $milestone_payment_request = $this->api->get_milestone_payment_request($job_id);
      $provider_id = $job_details['provider_id'];
      $this->load->view('services/job_per_hour_milestone_payment_customer_view', compact('milestone_payment_request','milestone_id','milestone_price','job_details','provider_id','redirect_url','cust_id','proposal_details','customer_details'));
      $this->load->view('user_panel/footer');
    }
    public function job_per_hour_milestone_payment_stripe()
    {
      // echo json_encode($_POST); die();
      $cust_id = $this->cust_id;  $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $milestone_id = $this->input->post('milestone_id', TRUE);
      $milestone_price = $this->input->post('milestone_price', TRUE);
      $total_amount = $milestone_price;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $today = date('Y-m-d H:i:s');
      $security_days = Date('Y-m-d H:i:s', strtotime("+9 days"));
      $dispute_lock = Date('Y-m-d H:i:s', strtotime("+3 days"));
      //echo json_encode($total_amount); die(); 
      $job_details = $this->api->get_customer_job_details($job_id);
      $provider_details = $this->api->get_service_provider_profile($provider_id);
      $milestone_details = $this->api->get_proposal_milestone_by_milestone_id($milestone_id);
      //echo json_encode($job_details); die();
      $search_data = array(
        "country_id" => $job_details['country_id'],
        "cat_id" => $job_details['sub_cat_id'],
        "cat_type_id" => $job_details['cat_id'],
      );
      $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);
      //echo json_encode($advance_payment); die();
      $gonagoo_commission_per = $advance_payment['gonagoo_commission'];
      $gonagoo_commission_amount = ($total_amount/100)*$gonagoo_commission_per;

      $job_deposit_price = trim($total_amount);
      $payment_method = 'stripe';
      
      $paid_amount = $total_amount + $job_details['paid_amount'];
      //$balance_amount = $job_details['balance_amount'] - $total_amount;
      $gonagoo_commission_amount = $job_details['gonagoo_commission_amount'] + $gonagoo_commission_amount;
      $data = array(
        'paid_amount' => $paid_amount,
        'payment_method' => $payment_method, 
        'payment_mode' => 'online', 
        'payment_by' => 'web', 
        'transaction_id' => trim($transaction_id), 
        'payment_response' => trim($stripeEmail), 
        'payment_datetime' => trim($today),
        //'balance_amount' =>(int)$balance_amount, 
        // 'complete_paid' => ($balance_amount == "0")?"1":"0", 
        'gonagoo_commission_per' => $gonagoo_commission_per, 
        'gonagoo_commission_amount' => $gonagoo_commission_amount, 
        'status_updated_by' => 'customer',
        'dispute_lock' => $dispute_lock
      );
      if($this->api->job_details_update($job_id, $data))
      {
        $job_details = $this->api->get_customer_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        $provider_id = $provider_details['cust_id'];
        //echo json_encode($provider_id); die();
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
        
        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Payment completed for job Payment amount is').' : '.$total_amount;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job payment successfully'), 'type' => 'job-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Job payment successfully'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job payment successfully'), trim($message));
        //Customer Notifications--------------------------
        //Invoice Notification----------------------------
          $provider_name=$provider_details['firstname']." ".$provider_details['lastname'];
          $customer_name=$customer_details['firstname']." ".$customer_details['lastname'];
          $phpinvoice = 'resources/fpdf-master/phpinvoicejob.php';
          require_once($phpinvoice);
          //Language configuration for invoice
          $lang = $this->input->post('lang', TRUE);
          if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
          else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
          else { $invoice_lang = "englishApi_lang"; }
          //Invoice Configuration
          $invoice = new phpinvoice('A4',trim($job_details['currency_code']),$invoice_lang);
          $invoice->setLogo("resources/fpdf-master/invoice-header.png");
          $invoice->setColor("#000");
          $invoice->setType("");
          $invoice->setReference($job_details['job_id']."-".$milestone_id);
          $invoice->setDate(date('M dS ,Y',time()));
          $operator_country = $this->api->get_country_details(trim($provider_details['country_id']));
          $operator_state = $this->api->get_state_details(trim($provider_details['state_id']));
          $operator_city = $this->api->get_city_details(trim($provider_details['city_id']));
          $user_country = $this->api->get_country_details(trim($customer_details['country_id']));
          $user_state = $this->api->get_state_details(trim($customer_details['state_id']));
          $user_city = $this->api->get_city_details(trim($customer_details['city_id']));
          $gonagoo_address = $this->api->get_gonagoo_address(trim($provider_details['country_id']));
          $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
          $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
          $invoice->setTo(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($provider_details['email_id'])));
          $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($customer_details['email1'])));
          $eol = PHP_EOL;
          /* Adding Items in table */
          $order_for = ucfirst($milestone_details['milestone_details']);
          $rate = $this->lang->line('paid_deposite_amount');
          $total = round(trim($total_amount),2);
          $payment_datetime = substr($today, 0, 10);
          $milestone=$milestone_id;
          //set items
          $invoice->addItem($payment_datetime,$order_for,$milestone,$rate,$total);
          /* Add totals */       
          $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
          /* Set badge */ 
          if($job_details['balance_amount']==0){
            
          $invoice->addBadge($this->lang->line('Job Amount Paid'));
          }
          /* Add title */
          $invoice->addTitle($this->lang->line('tnc'));
          /* Add Paragraph */
          $invoice->addParagraph($gonagoo_address['terms']);
          /* Add title */
          $invoice->addTitleFooter($this->lang->line('thank_you_job'));
          /* Add Paragraph */
          $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
          /* Set footer note */
          $invoice->setFooternote($gonagoo_address['company_name']);
          /* Render */
          $invoice->render($job_details['job_id']."-".$milestone_id.'_customer_job.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
          //Update File path
          $pdf_name = $job_details['job_id']."-".$milestone_id.'_customer_job.pdf';
          //Move file to upload folder
          rename($pdf_name, "resources/troubleshoot-invoices/".$pdf_name);
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Job Invoice');
            $emailAddress = trim($customer_details['email1']);
            $fileToAttach = "resources/troubleshoot-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          //Workstream entry--------------------------------
          //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $message = $this->lang->line('Accepted milestone request');
            $data = array(
              'provider_id' => $provider_id,
              'cust_id' => $cust_id,
              'job_id' => $job_id,
            );
            $workstream_id = $this->api->check_workstream_master($data);
            $update_data = array(
              'workstream_id' => (int)$workstream_id,
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' => "resources/troubleshoot-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment_invoice',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
        //Invoice Notification----------------------------
        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $message = $this->lang->line('Payment recieved for job Payment amount is').' : '.$total_amount;
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            
            foreach ($device_details_provider as $value) {

              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Job payment received'), 'type' => 'job-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN

            $msg_apn_provider =  array('title' => $this->lang->line('Job payment received'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job payment received'), trim($message));
        //Provider Notifications--------------------------
        
        //Payment Section---------------------------------
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];
          //Add Payment to customer account---------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
            } else {
              $data_cust_mstr = array(
                "user_id" => $job_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_cust_mstr);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($cust_acc_mstr['account_balance']) + trim($total_amount);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($total_amount),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($total_amount);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($total_amount),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp add Payment to customer account---------------
          //Add Payment to customer account---------------
          //Deduct Payment to customer account------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
            } else {
              $data_cust_mstr = array(
                "user_id" => $job_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_cust_mstr);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($cust_acc_mstr['account_balance']) - trim($total_amount);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($total_amount),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp deduct Payment to customer account------------
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($total_amount);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($total_amount),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp deduct Payment to customer account------------
          //Deduct Payment to customer account------------
          
          //Add Payment To Gonagoo Account----------------
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
            } else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }

            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($total_amount);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'job_payment',
              "amount" => trim($total_amount),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($job_details['currency_code']),
              "country_id" => trim($customer_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
          //Add Payment To Gonagoo Account----------------
          
          //Add Payment to provider Escrow----------------
            if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_pro_scrw = array(
                "deliverer_id" => (int)$provider_id,
                "scrow_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_scrow_record($data_pro_scrw);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
            }

            $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($total_amount);
            $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

            $data_scrw_hstry = array(
              "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
              "order_id" => (int)$job_id,
              "deliverer_id" => (int)$provider_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'job_payment',
              "amount" => trim($total_amount),
              "scrow_balance" => trim($scrow_balance),
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_scrow_history_record($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record_smp($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($total_amount);
              $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'job_payment',
                "amount" => trim($total_amount),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------------
          //Add Payment to provider Escrow----------------

        //Payment Section---------------------------------
        
        //Milestone transfer process Section--------------
          $job_details = $this->api->get_job_details($job_id);
          $job_paid_history = $this->api->get_job_payment_paid_history($job_id);
          //echo json_encode($job_details); die();
          $provider_id = $job_details['provider_id'];
          $currency_code = $job_details['currency_code'];

          //Payment Section---------------------------------
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')){
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }
          $provider_id = $provider_details['cust_id'];

          //Add Payment To Gonagoo Account----------------
          if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
          } else {
            $data_g_mstr = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($currency_code),
            );
            $this->api->insert_gonagoo_master_record($data_g_mstr);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
          }
          $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);

          if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($currency_code))) {
          } else {
            $data_pro_scrw = array(
              "deliverer_id" => (int)$provider_id,
              "scrow_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($currency_code),
            );
            $this->api->insert_scrow_record($data_pro_scrw);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($currency_code));
          }
          $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']);
          
          $search_data = array(
            "country_id" => $job_details['country_id'],
            "cat_type_id" => $job_details['cat_id'],
            "cat_id" => $job_details['sub_cat_id'],
          );
          $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);

          //echo json_encode($advance_payment); die();

          if($gonagoo_balance >= $milestone_price && $scrow_balance >= $milestone_price && is_array($advance_payment)){

            //deduct from gonagoo account  
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
            }else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency_code),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }
            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);
            $gonagoo_balance = trim($gonagoo_balance) - trim($milestone_price);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);

            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code));
            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 0,
              "transaction_type" => 'milestone_price',
              "amount" => trim($milestone_price),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($currency_code),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);

            // deduct from scrow balance
            $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($milestone_price);
            $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

            $data_scrw_hstry = array(
              "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
              "order_id" => (int)$job_id,
              "deliverer_id" => (int)$provider_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'milestone_price',
              "amount" => trim($milestone_price),
              "scrow_balance" => trim($scrow_balance),
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_scrow_history_record($data_scrw_hstry);
            //Smp deduct from scrow balance
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($currency_code))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency_code),
                );
                $this->api->insert_scrow_record_smp($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($currency_code));
              }
              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($milestone_price);
              $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'milestone_price',
                "amount" => trim($milestone_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
            //Smp deduct from scrow balance

            //add payment to provider account 
            if($provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_provider_mstr = array(
                "user_id" => $job_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_provider_mstr);
              $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($provider_acc_mstr['account_balance']) + trim($milestone_price);
            $this->api->update_payment_in_customer_master($provider_acc_mstr['account_id'], $provider_id, $account_balance);
            $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$provider_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $provider_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'milestone_amount',
              "amount" => trim($milestone_price),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id'],
              "security_days_date" => $security_days
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp add payment to provider account 
              if($provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_provider_mstr = array(
                  "user_id" => $job_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_provider_mstr);
                $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($provider_acc_mstr['account_balance']) + trim($milestone_price);
              $this->api->update_payment_in_customer_master_smp($provider_acc_mstr['account_id'], $provider_id, $account_balance);
              $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$provider_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'milestone_amount',
                "amount" => trim($milestone_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id'],
                "security_days_date" => $security_days
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp add payment to provider account 

            // commision percent 
            $g_commission_percentage = $advance_payment['gonagoo_commission'];
            $gonagoo_commission = ($milestone_price/100)*$g_commission_percentage;

            // deduct commision amount from providers account  
            if($provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_provider_mstr = array(
                "user_id" => $job_details['provider_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_provider_mstr);
              $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($provider_acc_mstr['account_balance']) - trim($gonagoo_commission);
            $this->api->update_payment_in_customer_master($provider_acc_mstr['account_id'], $provider_id, $account_balance);
            $provider_acc_mstr = $this->api->customer_account_master_details($provider_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$provider_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $provider_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'job_commission',
              "amount" => trim($gonagoo_commission),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id'],
              "security_days_date" => $security_days
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            // Smp deduct commision amount from providers account  
              if($provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_provider_mstr = array(
                  "user_id" => $job_details['provider_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_provider_mstr);
                $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($provider_acc_mstr['account_balance']) - trim($gonagoo_commission);
              $this->api->update_payment_in_customer_master_smp($provider_acc_mstr['account_id'], $provider_id, $account_balance);
              $provider_acc_mstr = $this->api->customer_account_master_details_smp($provider_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$provider_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $provider_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'job_commission',
                "amount" => trim($gonagoo_commission),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id'],
                "security_days_date" => $security_days
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            // Smp deduct commision amount from providers account  


            //Add gonagoo commision to gonagoo account  
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code))) {
            }else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency_code),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }
            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']);
            $gonagoo_balance = trim($gonagoo_balance) + trim($gonagoo_commission);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);

            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($currency_code));
            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'job_commission',
              "amount" => trim($gonagoo_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($currency_code),
              "country_id" => trim($provider_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);

            //Customer Notifications--------------------------
              $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
              $message = $this->lang->line('You are accepted milestone and payment released for job milestone. ID').': '.$milestone_id;
              $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);
              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_customer = $this->api->get_user_device_details($cust_id);
              if(!is_null($device_details_customer)) {
                $arr_customer_fcm_ids = array();
                $arr_customer_apn_ids = array();
                foreach ($device_details_customer as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('You are accepted milestone request'), 'type' => 'milestone-accept', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
                //APN
                $msg_apn_customer =  array('title' => $this->lang->line('You are accepted milestone request'), 'text' => $message);
                if(is_array($arr_customer_apn_ids)) { 
                  $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
                }
              }

              $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('You are accepted milestone request'), trim($message));
            //Customer Notifications--------------------------

            //Workstream entry--------------------------------
              //$workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
              $message = $this->lang->line('Accepted milestone request');
              $data = array(
                'provider_id' => $provider_id,
                'cust_id' => $cust_id,
                'job_id' => $job_id,
              );
              $workstream_id = $this->api->check_workstream_master($data);
              $update_data = array(
                'workstream_id' => (int)$workstream_id,
                'job_id' => (int)$job_id,
                'cust_id' => $cust_id,
                'provider_id' => (int)$job_details['provider_id'],
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $cust_id,
                'type' => 'milestone_accepted',
                'file_type' => 'text',
                'proposal_id' => 0,
              );
              $this->api->update_to_workstream_details($update_data);
            //Workstream entry--------------------------------
            
            //Provider Notifications--------------------------
              $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
              $message = $this->lang->line('Milestone accepted and paymen recieved. ID').': '.$milestone_id;
              $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);

              if(!is_null($device_details_provider)) {
                $arr_provider_fcm_ids = array();
                $arr_provider_apn_ids = array();
                
                foreach ($device_details_provider as $value) {

                  if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Accepted milestone request'), 'type' => 'milestone-accept', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                //APN

                $msg_apn_provider =  array('title' => $this->lang->line('Accepted milestone request'), 'text' => $message);
                if(is_array($arr_provider_apn_ids)) { 
                  $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                }
              }

              $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Milestone payment received'), trim($message));
            //Provider Notifications--------------------------

            //update paid history
            $paid_amount =  $milestone_price+$job_paid_history['paid_amount'];
            $this->api->update_job_payment_paid_history($job_id,$paid_amount, $today); 

            //update milestone
            $data = array(
              'amount_paid' => 1,
              'payment_datetime' => $today
            );
            $this->api->update_proposal_milestone_details($data,$milestone_id);
            $data = array(
              'status' => 'accept',
              'invoice_url' => $fileToAttach,
              "security_days_date" => $security_days,
              'status_datetime' => $today
            );
            $this->api->update_proposal_milestone_request($data,$milestone_id);
          }
        //Milestone transfer process Section--------------
        $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed & milestone accepted'));
      }
      else
      { $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
      }
      redirect('user-panel-services/my-inprogress-jobs','refresh');
    }
    public function customer_per_hour_job_details()
    {
      //echo json_encode($_POST); die();
      // user-panel-services/customer-per-hour-job-details-view
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id'); $job_id = (int)$job_id;
      $redirect_url = $this->input->post('redirect_url');
      $job_details = $this->api->get_customer_job_details($job_id);
      $customer_details = $this->api->get_user_details($job_details['cust_id']);
      $provider_details = $this->api->get_user_details($job_details['provider_id']);
      $job_payment_paid_history = $this->api->get_job_payment_paid_history($job_id);
      $submitted_hours = $this->api->get_total_submitted_hours($job_id);
      $all_milestone_details = $this->api->get_milestone_payment_request_filter($job_id);
      $accepted_milestone = $this->api->get_milestone_payment_request_filter($job_id , "accept");
      // echo json_encode(); die();
      $this->load->view('services/customer_per_hour_job_details_view',compact('job_details', 'customer_details', 'redirect_url', 'cust_id','provider_details','job_payment_paid_history','submitted_hours','accepted_milestone','all_milestone_details'));
      $this->load->view('user_panel/footer');
    }
    public function project_work_room()
    {
      // echo  json_encode($_POST);die();
      $cust_id = $this->cust_id; $cust_id = (int) $cust_id;
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
      $redirect_url = $this->input->post('redirect_url', TRUE);
      if(!$job_id) { $job_id = $this->session->userdata('workroom_job_id'); }
      $job_details = $this->api->get_customer_job_details($job_id);
      $login_customer_details = $this->api->get_user_details($cust_id);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$provider_id);
      $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
      $job_provider_details = $this->api->get_user_details((int)$provider_id);
      $keys = ['provider_id' => (int)$job_provider_profile['cust_id'], 'cust_id' => $cust_id, 'job_id' => $job_id];
      $workroom = $this->api->get_job_workstream_details($job_id ,$job_details['cust_id'], $provider_id);
      // echo json_encode($workroomx);die();
      foreach ($workroom as $v) {
        $res=$this->db->query("SELECT * FROM `tbl_smp_read_workstream_notification` WHERE cust_id=".$v['cust_id']." AND notification_id=".$v['wsd_id'])->row();
        if(!$res){

         $this->api->make_workstream_notification_read($v['cust_id'] , $v['wsd_id']);  
        }
      }
      // echo json_encode($workroom); die();
      $this->load->view('services/services_job_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'job_provider_profile', 'keys', 'job_provider_details', 'job_customer_details','redirect_url'));
      $this->load->view('user_panel/footer');
    }
    public function projects_workroom()
    {
      $cust_id = $this->cust['cust_id'];
      $job_list = $this->api->get_globle_workstream_details((int)$cust_id);
     $this->load->view('services/projects_workroom', compact('job_list' , 'cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function browse_provider()
    {
      // echo json_encode($_POST);die();
      $project_5= null;
      $project_50=null;
      $country_id=null;
      $city_id=null;
      $state_id=null;
      $states=null;
      $cities=null;
      $rate_from=null;
      $rate_to=null;
      $filter = 'basic';
      $country = $this->api->get_countries();
      $cust_id = $this->cust_id;
      if(isset($_POST['fav'])){
        $data = array(
          'fav' => "yes" , 
          'skill' => "NULL",
          'country_id' => "NULL",
          'state_id' => "NULL",
          'city_id' => "NULL",
          'rate_from' => "NULL",
          'rate_to' => "NULL",
          'cust_id' => $this->cust_id
        );
        $is_fav = 'yes';
      } else if(isset($_POST['filter']) && $_POST['filter']=="advance"){
        
        $country_id = $this->input->post('country_id');
        $state_id = $this->input->post('state_id');
        $city_id = $this->input->post('city_id');
        $project_5 = $this->input->post('project_5');
        $project_50 = $this->input->post('project_50');
        $rate_from = $this->input->post('rate_from');
        $rate_to = $this->input->post('rate_to');
        $states = $this->api->get_state_by_country_id($country_id);
        $cities = $this->api->get_city_by_state_id($state_id);
        $filter = 'advance';
       $data = array(
          'skill' => "NULL", 
          'fav' => "no",
           'country_id' => $country_id,
          'state_id' => $state_id,
          'city_id' =>  $city_id,
          'rate_from' =>$rate_from,
          'rate_to' => $rate_to,
          'cust_id' => $this->cust_id
        );
        $is_fav = 'no';
      } else{
        if(!empty($_GET)){
          $data = array(
          'skill' => $this->input->get("skill"), 
          'fav' => "no",
           'country_id' => "NULL",
          'state_id' => "NULL",
          'city_id' => "NULL",
          'rate_from' => "NULL",
          'rate_to' => "NULL",
          'cust_id' => $this->cust_id
        );
        }else{
       
          $data = array(
            'skill' => "NULL", 
            'fav' => "no",
             'country_id' => "NULL",
            'state_id' => "NULL",
            'city_id' => "NULL",
            'rate_from' => "NULL",
            'rate_to' => "NULL",
            'cust_id' => $this->cust_id
          );
        }
        $is_fav = 'no';
      }      
    
      $freelancers = $this->api->list_of_provider($data);
      // echo $this->db->last_query();die();
      for($i=0; $i<sizeof($freelancers); $i++) {
        $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
        $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
        // echo json_encode($completed);
        $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
        if($completed['total_job'] != "0"){
         $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
        }else{$percent = 0;}  
        $freelancers[$i]['completed_job'] = (int)$completed['total_job'];
        $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
        $freelancers[$i]['job_percent'] = $percent;
        $freelancers[$i]['portfolio'] = (int)$portfolio;
        $freelancers[$i]['project_5'] = "yes";
        if($project_5 != null ){
          if((int)$project_5 < (int)$freelancers[$i]['completed_job']){
            $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
          }
        }
        if($project_50 != null ){
          if((int)$project_50 < (int)$freelancers[$i]['completed_job']){
            $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
          }
        }
      }

      $favorite_freelancer = $this->api->get_favorite_freelancer($this->cust_id);
      // echo json_encode($freelancers);die();
      $fav_freelancer = array();
      foreach ($favorite_freelancer as $fav_key) {
        array_push($fav_freelancer, $fav_key['provider_id']);
      }


      $this->load->view('services/list_of_provider', compact('country','fav_freelancer','freelancers','filter','is_fav','country_id','city_id','state_id','states','cities','project_5','project_50','rate_from','rate_to'));
      $this->load->view('user_panel/footer');     
    }
    public function browse_project()
    {
      //echo json_encode($_POST); die();
      if(!empty($_POST)  && $_POST!=[]){
       $cat_type_id=$_POST['cat_type_id'];
       $cat_id=$_POST['cat_id'];
       $start_date = $this->input->post('start_date');
       $start_date = ($start_date!="")?date("Y-m-d", strtotime($start_date)) . ' 00:00:00':"NULL";
       $end_date = $this->input->post('end_date');
       $end_date = ($end_date!="")?date("Y-m-d", strtotime($end_date)) . ' 23:59:59':"NULL"; 
       $filter = "advance"; 
      }else{
        $cat_type_id=0;
        $cat_id=0;
        $start_date="NULL";
        $end_date="NULL";
        $filter = "basic"; 
      }
      $cust_id = $this->cust_id;
      $data =array(
        'cust_id' => $cust_id,
        'cat_id' => $cat_type_id,
        'sub_cat_id' => $cat_id,
        'start_date' => $start_date,
        'end_date' => $end_date,

      );
      $jobs = $this->api->get_job_list($data);
      $category_types = $this->api->get_category_types();
      $fav_jobs = $this->api->get_favorite_jobs($this->cust_id);
      $fav_projects = array();
      foreach ($fav_jobs as $fav_key) {
        array_push($fav_projects, $fav_key['job_id']);
      }
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }
      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }

      //echo json_encode($jobs); die();
      $this->load->view('services/browse_project_list_view', compact('jobs','filter','fav_projects','category_types','category_types','cat_type_id','cat_id','start_date','end_date' ,'categories'));
      $this->load->view('user_panel/footer');
    }
    public function browse_troubleshooter()
    {
      if(!empty($_GET)  && $_GET!=[]){
        $cat_type_id=0;
        $cat_id=0;
        $strt_dt="NULL";
        $end_dt="NULL";
        $filter = "basic"; 
        $start_date="NULL";
        $end_date="NULL";
        $skill=$this->input->get("skill");
      }else if(!empty($_POST)  && $_POST!=[]){
       $cat_type_id=$_POST['cat_type_id'];
       $cat_id=$_POST['cat_id'];
       $start_date = $this->input->post('start_date');
       $strt_dt = ($start_date!="")?date("Y-m-d", strtotime($start_date)) . ' 00:00:00':"NULL";
       $end_date = $this->input->post('end_date');
       $end_dt = ($end_date!="")?date("Y-m-d", strtotime($end_date)) . ' 23:59:59':"NULL"; 
       $filter = "advance"; 
       $skill="";
      }else{
        $cat_type_id=0;
        $cat_id=0;
        $strt_dt="NULL";
        $end_dt="NULL";
        $filter = "basic"; 
        $start_date="NULL";
        $end_date="NULL";
        $skill="";
      }
      $cust_id = $this->cust_id;
      $data =array(
        'cust_id' => $cust_id,
        'cat_id' => $cat_type_id,
        'sub_cat_id' => $cat_id,
        'start_date' => $strt_dt,
        'end_date' => $end_dt,
        'skill'=>$skill,

      );
      // echo json_encode($data);die();
      $jobs = $this->api->get_troubleshoot_list($data);
      // echo $this->db->last_query(); die();
      
      $category_types = $this->api->get_category_types();
      $fav_jobs = $this->api->get_favorite_jobs($this->cust_id);
      $fav_projects = array();
      foreach ($fav_jobs as $fav_key) {
        array_push($fav_projects, $fav_key['job_id']);
      }
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }
      // echo json_encode($jobs); die();

      if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }


      $this->load->view('services/browse_troubleshoot_list_view', compact('jobs','filter','fav_projects','category_types','cat_type_id','cat_id','start_date','end_date' ,'categories'));
      $this->load->view('user_panel/footer');
    }
    public function user_notifications()
    {
      $cust = $this->cust;
      if($cust['user_type']) { $type = "Individuals"; } else { $type = "Business"; }
      $notifications = $this->api->get_notifications($cust['cust_id'], $type);   
      $this->load->view('services/admin_notifications_view',compact('notifications'));
      $this->load->view('user_panel/footer'); 
    }


  //Start Unused functions--------------------------------
    public function browse_project_old()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $data =array(
        'cust_id' => $cust_id
      );
      $jobs = $this->api->get_job_list($data);
      //echo $this->db->last_query(); die();
      $filter = "basic"; 
      $category_types = $this->api->get_category_types();
      $fav_jobs = $this->api->get_favorite_jobs($this->cust_id);
      $fav_projects = array();
      foreach ($fav_jobs as $fav_key) {
        array_push($fav_projects, $fav_key['job_id']);
      }
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }
      // echo json_encode($jobs); die();
      $this->load->view('services/browse_project_list_view', compact('jobs','filter','fav_projects','category_types'));
      $this->load->view('user_panel/footer');
    }
  //End Unused functions----------------------------------
  
  //Start Admin workroom----------------------------------
  public function customer_admin_workroom()
  {
    // echo json_encode($_POST); die();
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $redirect_url = $this->input->post('redirect_url', TRUE); $job_id = (int)$job_id;
    $job_details = $this->api->get_customer_job_details($job_id);
    $cust_id =  $this->cust_id;
    $login_customer_details = $this->api->get_user_details($cust_id);
    $provider_id = $job_details['provider_id'] ;
    $workroom = $this->api->get_job_workstream_details_admin($job_id , $cust_id , $provider_id , "customer");
    $admin_profile = $this->api->logged_in_user_details(1);
    $job_customer_details = $this->api->get_user_details($cust_id);
    $keys = ['provider_id' => $provider_id, 'cust_id' => $cust_id, 'job_id' => $job_id];
    // echo json_encode($admin_profile); die();
    $this->load->view('services/services_customer_admin_job_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'admin_profile', 'keys', 'job_customer_details','redirect_url'));
    $this->load->view('user_panel/footer');
  }
  public function provider_admin_workroom()
  {
    // echo json_encode($_POST); die();
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $redirect_url = $this->input->post('redirect_url', TRUE); $job_id = (int)$job_id;
    $job_details = $this->api->get_customer_job_details($job_id);
    $cust_id = $job_details['cust_id'] ;
    $login_customer_details = $this->api->get_user_details($cust_id);
    $provider_id = $this->cust_id;
    $workroom = $this->api->get_job_workstream_details_admin($job_id , $cust_id , $provider_id , "provider");
    // echo json_encode($workroom); die();
    $admin_profile = $this->api->logged_in_user_details(1);
    $job_customer_details = $this->api->get_user_details($cust_id);
    $keys = ['provider_id' => $provider_id, 'cust_id' => $cust_id, 'job_id' => $job_id];
    // echo json_encode($admin_profile); die();
    $this->load->view('services/services_provider_admin_job_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'admin_profile', 'keys', 'job_customer_details','redirect_url'));
    $this->load->view('user_panel/footer');
  }

  public function admin_add_workroom_chat_ajax()
  {
    //echo json_encode($_POST); die();
    // $cust = $this->cust;
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $type = $this->input->post('type', TRUE);
    $job_details = $this->api->get_customer_job_details($job_id); 
    $text_msg = $this->input->post('message_text', TRUE);
    $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
    $provider_id = $job_details['provider_id']; $provider_id = (int)$provider_id;
    $today = date("Y-m-d H:i:s");

    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/'; 
      $config['allowed_types']  = '*'; 
      $config['max_size']       = '0'; 
      $config['max_width']      = '0'; 
      $config['max_height']     = '0'; 
      $config['encrypt_name']   = true;
      $this->load->library('upload', $config);
      $this->upload->initialize($config); 
      if($this->upload->do_upload('attachment')){
        $uploads    = $this->upload->data();  
        $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
      } else { $attachment_url = "NULL"; }
    } else { $attachment_url = "NULL"; }

    if($type =="customer"){
      $sender_id = $cust_id;
      if($workstream_details = $this->api->get_workstream_details_by_job_id_admin($job_id ,$cust_id , "customer" )) {
        $insert_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => $job_id,
          'cust_id' => $cust_id,
          // 'provider_id' => $provider_id,
          'admin_id' => 0,
          'is_admin' => 1,
          'text_msg' => $text_msg,
          'attachment_url' =>  $attachment_url,
          'cre_datetime' => $today,
          'sender_id' => $sender_id,
          'type' => ($attachment_url=='NULL')?'chat':'attachment',
          'file_type' => ($attachment_url=='NULL')?'text':'file',
          'proposal_id' => 0,
          "ratings" => "NULL",
        );
        //echo json_encode($insert_data); die();
        $this->api->update_to_workstream_details($insert_data);

        //Notifications to Provider----------------
          $api_key = $this->config->item('delivererAppGoogleKey');

          if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
          } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

          if(!is_null($device_details)) {
            $arr_fcm_ids = array(); $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
            $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_apn_ids, $api_key);
            }
          }
        //------------------------------------------
      }
      return json_encode(true);
    }elseif($type =="provider"){
      $sender_id = $provider_id;
      if($workstream_details = $this->api->get_workstream_details_by_job_id_admin($job_id ,$cust_id , "customer" )) {
        $insert_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => $job_id,
          // 'cust_id' => $cust_id,
          'provider_id' => $provider_id,
          'admin_id' => 0,
          'is_admin' => 1,
          'text_msg' => $text_msg,
          'attachment_url' =>  $attachment_url,
          'cre_datetime' => $today,
          'sender_id' => $sender_id,
          'type' => ($attachment_url=='NULL')?'chat':'attachment',
          'file_type' => ($attachment_url=='NULL')?'text':'file',
          'proposal_id' => 0,
          "ratings" => "NULL",
        );
        //echo json_encode($insert_data); die();
        $this->api->update_to_workstream_details($insert_data);

        //Notifications to Provider----------------
          $api_key = $this->config->item('delivererAppGoogleKey');

          if($job_details['provider_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
          } else { $device_details = $this->api->get_user_device_details((int)$job_details['provider_id']); }

          if(!is_null($device_details)) {
            $arr_fcm_ids = array(); $arr_apn_ids = array();
            foreach ($device_details as $value) {
              if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
            $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
            if(is_array($arr_apn_ids)) { 
              $arr_apn_ids = implode(',', $arr_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_apn_ids, $api_key);
            }
          }
        //------------------------------------------
      }
      return json_encode(true);
    }
  }
  public function admin_customer_job_workroom_ajax()
  {
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $chat_count = $this->input->post('chat_count', TRUE); $job_id = (int)$job_id;
    $redirect_url = $this->input->post('redirect_url', TRUE); $job_id = (int)$job_id;
    $job_details = $this->api->get_customer_job_details($job_id);
    $cust_id =  $this->cust_id;
    $login_customer_details = $this->api->get_user_details($cust_id);
    $provider_id = $job_details['provider_id'] ;
    $workroom_details = $this->api->get_job_workstream_details_admin($job_id , $cust_id , $provider_id , "customer");
    $admin_profile = $this->api->logged_in_user_details(1);
    $job_customer_details = $this->api->get_user_details($cust_id);
    $new_chat_count = sizeof($workroom_details);

    if(sizeof($workroom_details) > $chat_count) {
      $message_template ='';
      $message_template .= "<input type='hidden' value='$new_chat_count' id='chat_count' />";
      foreach ($workroom_details as $v => $workroom) { $tabindex = ($new_chat_count==($v+1))?'tabindex=1':'';
        date_default_timezone_set($this->session->userdata("default_timezone")); 
        $ud = strtotime($workroom['cre_datetime']);
        date_default_timezone_set($this->session->userdata("user_timezone"));

        $chat_side = ($workroom['sender_id'] == $cust_id)?'right':'left';
        $message_template .= "<li class='chat-message $chat_side'>";

        if($workroom['sender_id'] == $cust_id) {
          $url = base_url($login_customer_details['avatar_url']);
          $message_template .= "<img class='message-avatar' style='margin-right: 0px;' src='$url' />";
          $message_template .= "<div class='message' style='margin-left: 0px;'>";
            $cust_name = $login_customer_details['firstname'].' '.$login_customer_details['lastname'];
            $message_template .= "<a class='message-author'>$cust_name</a>";
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice"):
                $message_template .= "<span class='message-content'>";
                  if($workroom['attachment_url'] != 'NULL'):
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<p>&nbsp;</p>";
                    $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ):
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_accepted"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone accepted')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_rejected"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone rejected')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Send')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_decline"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('proposal decline')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .= "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        } else {
          $url = base_url(($workroom['sender_id'] == $job_details['cust_id']) ? $job_customer_details['avatar_url'] : $admin_profile['avatar_url']);
          $message_template .= "<img class='message-avatar' src='$url' />";
          $message_template .= "<div class='message'>";
            $user_name = ($workroom['sender_id']==$job_details['cust_id']) ? $job_customer_details['firstname'].' '.$job_customer_details['lastname'] : $admin_profile['firstname'].' '.$admin_profile['lastname'];
            $message_template .= "<a class='message-author text-left'>$user_name</a>";
            $ud = strtotime($workroom['cre_datetime']);
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                $message_template .= "<span class='message-content'>";
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p>$msg</p>";
                  $message_template .= "</div>";
                  if($workroom['attachment_url'] != 'NULL') :
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<p>&nbsp;</p>";
                      $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_send"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .=  "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        }
        $message_template .= "</li>";
      }
    } else { $message_template = false; }
    echo json_encode($message_template); die();
  }
  public function admin_provider_job_workroom_ajax()
  {
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $chat_count = $this->input->post('chat_count', TRUE); $job_id = (int)$job_id;
    $redirect_url = $this->input->post('redirect_url', TRUE); $job_id = (int)$job_id;
    $job_details = $this->api->get_customer_job_details($job_id);
    $cust_id =  $job_details['cust_id'] ;
    $provider_id = $this->cust_id; 
    $login_customer_details = $this->api->get_user_details($provider_id);
    $workroom_details = $this->api->get_job_workstream_details_admin($job_id , $cust_id , $provider_id , "provider");
    $admin_profile = $this->api->logged_in_user_details(1);
    $job_customer_details = $this->api->get_user_details($cust_id);
    $new_chat_count = sizeof($workroom_details);

    if(sizeof($workroom_details) > $chat_count) {
      $message_template ='';
      $message_template .= "<input type='hidden' value='$new_chat_count' id='chat_count' />";
      foreach ($workroom_details as $v => $workroom) { $tabindex = ($new_chat_count==($v+1))?'tabindex=1':'';
        date_default_timezone_set($this->session->userdata("default_timezone")); 
        $ud = strtotime($workroom['cre_datetime']);
        date_default_timezone_set($this->session->userdata("user_timezone"));

        $chat_side = ($workroom['sender_id'] == $provider_id && $workroom['admin_id'] == 0)?'right':'left';
        $message_template .= "<li class='chat-message $chat_side'>";

        if($workroom['sender_id'] == $provider_id && $workroom['admin_id'] == 0) {
          $url = base_url($login_customer_details['avatar_url']);
          $message_template .= "<img class='message-avatar' style='margin-right: 0px;' src='$url' />";
          $message_template .= "<div class='message' style='margin-left: 0px;'>";
            $cust_name = $login_customer_details['firstname'].' '.$login_customer_details['lastname'];
            $message_template .= "<a class='message-author'>$cust_name</a>";
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice"):
                $message_template .= "<span class='message-content'>";
                  if($workroom['attachment_url'] != 'NULL'):
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<p>&nbsp;</p>";
                    $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ):
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_accepted"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone accepted')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_rejected"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone rejected')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Send')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_decline"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('proposal decline')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .= "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        } else {
          $url = base_url(($workroom['sender_id'] == $job_details['cust_id']) ? $job_customer_details['avatar_url'] : $admin_profile['avatar_url']);
          $message_template .= "<img class='message-avatar' src='$url' />";
          $message_template .= "<div class='message'>";
            $user_name = ($workroom['sender_id']==$job_details['cust_id']) ? $job_customer_details['firstname'].' '.$job_customer_details['lastname'] : $admin_profile['firstname'].' '.$admin_profile['lastname'];
            $message_template .= "<a class='message-author text-left'>$user_name</a>";
            $ud = strtotime($workroom['cre_datetime']);
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                $message_template .= "<span class='message-content'>";
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p>$msg</p>";
                  $message_template .= "</div>";
                  if($workroom['attachment_url'] != 'NULL') :
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<p>&nbsp;</p>";
                      $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_send"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .=  "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        }
        $message_template .= "</li>";
      }
    } else { $message_template = false; }
    echo json_encode($message_template); die();
  }
     
  //End Admin workroom------------------------------------ 
    

}
/* End of file User_panel_services.php */
/* Location: ./application/controllers/User_panel_services.php */