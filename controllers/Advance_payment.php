<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advance_payment extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('advance_payment_model', 'payment');
      $this->load->model('standard_dimension_model', 'standard_dimension');   
      $this->load->model('Web_user_model', 'user');   
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $adv_payments = $this->payment->get_payments();
    $this->load->view('admin/advance_payment_list_view', compact('adv_payments'));      
    $this->load->view('admin/footer_view');   
  }

  public function add()
  {
    $categories = $this->standard_dimension->get_categories();
    $countries = $this->payment->get_countries();
    $this->load->view('admin/advance_payment_add_view',compact('countries','categories'));
    $this->load->view('admin/footer_view');
  }
  
  public function register()
  { //echo json_encode($_POST); die();
    $country_id = $this->input->post('country_id', TRUE);

    $loading_free_hours = $this->input->post('loading_free_hours', TRUE);
    $loading_hours_charge = $this->input->post('loading_hours_charge', TRUE);

    $ELAPP =  $this->input->post('ELAPP', TRUE);
    $ELDDC = $this->input->post('ELDDC', TRUE);
    $ELDDP = $this->input->post('ELDDP', TRUE);
    $ELHFC = $this->input->post('ELHFC', TRUE);
    $ELHFP = $this->input->post('ELHFP', TRUE);
    $ELDVC = $this->input->post('ELDVC', TRUE);
    $ELDVP = $this->input->post('ELDVP', TRUE);
    $ELGCP = $this->input->post('ELGCP', TRUE);
 
    $ENAPP = $this->input->post('ENAPP', TRUE);
    $ENDDC = $this->input->post('ENDDC', TRUE);
    $ENDDP = $this->input->post('ENDDP', TRUE);
    $ENHFC = $this->input->post('ENHFC', TRUE);
    $ENHFP = $this->input->post('ENHFP', TRUE);
    $ENDVC = $this->input->post('ENDVC', TRUE);
    $ENDVP = $this->input->post('ENDVP', TRUE);
    $ENGCP = $this->input->post('ENGCP', TRUE);

    $EIAPP = $this->input->post('EIAPP', TRUE);
    $EIDDC = $this->input->post('EIDDC', TRUE);
    $EIDDP = $this->input->post('EIDDP', TRUE);
    $EIHFC = $this->input->post('EIHFC', TRUE);
    $EIHFP = $this->input->post('EIHFP', TRUE);
    $EIDVC = $this->input->post('EIDVC', TRUE);
    $EIDVP = $this->input->post('EIDVP', TRUE);
    $EIGCP = $this->input->post('EIGCP', TRUE);
    $EICCPNG = $this->input->post('EICCPNG', TRUE);
    $EICCPOG = $this->input->post('EICCPOG', TRUE);

    $SLAPP = $this->input->post('SLAPP', TRUE);
    $SLDDC = $this->input->post('SLDDC', TRUE);
    $SLDDP = $this->input->post('SLDDP', TRUE);
    $SLHFC = $this->input->post('SLHFC', TRUE);
    $SLHFP = $this->input->post('SLHFP', TRUE);
    $SLDVC = $this->input->post('SLDVC', TRUE);
    $SLDVP = $this->input->post('SLDVP', TRUE);
    $SLGCP = $this->input->post('SLGCP', TRUE);

    $SNAPP = $this->input->post('SNAPP', TRUE);
    $SNDDC = $this->input->post('SNDDC', TRUE);
    $SNDDP = $this->input->post('SNDDP', TRUE);
    $SNHFC = $this->input->post('SNHFC', TRUE);
    $SNHFP = $this->input->post('SNHFP', TRUE);
    $SNDVC = $this->input->post('SNDVC', TRUE);
    $SNDVP = $this->input->post('SNDVP', TRUE);
    $SNGCP = $this->input->post('SNGCP', TRUE);

    $SIAPP = $this->input->post('SIAPP', TRUE);
    $SIDDC = $this->input->post('SIDDC', TRUE);
    $SIDDP = $this->input->post('SIDDP', TRUE);
    $SIHFC = $this->input->post('SIHFC', TRUE);
    $SIHFP = $this->input->post('SIHFP', TRUE);
    $SIDVC = $this->input->post('SIDVC', TRUE);
    $SIDVP = $this->input->post('SIDVP', TRUE);
    $SIGCP = $this->input->post('SIGCP', TRUE);
    $SICCPNG = $this->input->post('SICCPNG', TRUE);
    $SICCPOG = $this->input->post('SICCPOG', TRUE);

    $ALAPP = $this->input->post('ALAPP', TRUE);
    $ALDDC = $this->input->post('ALDDC', TRUE);
    $ALDDP = $this->input->post('ALDDP', TRUE);
    $ALHFC = $this->input->post('ALHFC', TRUE);
    $ALHFP = $this->input->post('ALHFP', TRUE);
    $ALDVC = $this->input->post('ALDVC', TRUE);
    $ALDVP = $this->input->post('ALDVP', TRUE);
    $ALGCP = $this->input->post('ALGCP', TRUE);

    $ANAPP = $this->input->post('ANAPP', TRUE);
    $ANDDC = $this->input->post('ANDDC', TRUE);
    $ANDDP = $this->input->post('ANDDP', TRUE);
    $ANHFC = $this->input->post('ANHFC', TRUE);
    $ANHFP = $this->input->post('ANHFP', TRUE);
    $ANDVC = $this->input->post('ANDVC', TRUE);
    $ANDVP = $this->input->post('ANDVP', TRUE);
    $ANGCP = $this->input->post('ANGCP', TRUE);

    $AIAPP = $this->input->post('AIAPP', TRUE);
    $AIDDC = $this->input->post('AIDDC', TRUE);
    $AIDDP = $this->input->post('AIDDP', TRUE);
    $AIHFC = $this->input->post('AIHFC', TRUE);
    $AIHFP = $this->input->post('AIHFP', TRUE);
    $AIDVC = $this->input->post('AIDVC', TRUE);
    $AIDVP = $this->input->post('AIDVP', TRUE);
    $AIGCP = $this->input->post('AIGCP', TRUE);
    $AICCPNG = $this->input->post('AICCPNG', TRUE);
    $AICCPOG = $this->input->post('AICCPOG', TRUE);
    /*
        $adv_pay = $this->input->post('adv_pay', TRUE);
        $ddc = $this->input->post('ddc', TRUE);
        $ddp = $this->input->post('ddp', TRUE);
        $hfc = $this->input->post('hfc', TRUE);
        $hfp = $this->input->post('hfp', TRUE);
        $dvc = $this->input->post('dvc', TRUE);
        $dvp = $this->input->post('dvp', TRUE);
        $gcp = $this->input->post('gcp', TRUE);
    */
    $category_ids = $this->input->post('category', TRUE);
    $insert_flag = false;

    if($country_id > 0 ) {
           
      foreach ($category_ids as $id) {

        $insert_data = array(
          "category_id" => trim($id), 
          "country_id" => trim($country_id), 

          "loading_free_hours" => number_format($loading_free_hours,2,'.',''),
          "loading_hours_charge" => number_format($loading_hours_charge,2,'.',''),

          "ELAPP" => number_format($ELAPP,2,'.',''),
          "ELDDC" => number_format($ELDDC,2,'.',''),
          "ELDDP" => number_format($ELDDP,2,'.',''),
          "ELHFC" => number_format($ELHFC,2,'.',''),
          "ELHFP" => number_format($ELHFP,2,'.',''),
          "ELDVC" => number_format($ELDVC,2,'.',''),
          "ELDVP" => number_format($ELDVP,2,'.',''),
          "ELGCP" => number_format($ELGCP,2,'.',''),
           
          "ENAPP" => number_format($ENAPP,2,'.',''),
          "ENDDC" => number_format($ENDDC,2,'.',''),
          "ENDDP" => number_format($ENDDP,2,'.',''),
          "ENHFC" => number_format($ENHFC,2,'.',''),
          "ENHFP" => number_format($ENHFP,2,'.',''),
          "ENDVC" => number_format($ENDVC,2,'.',''),
          "ENDVP" => number_format($ENDVP,2,'.',''),
          "ENGCP" => number_format($ENGCP,2,'.',''),

          "EIAPP" => number_format($EIAPP,2,'.',''),
          "EIDDC" => number_format($EIDDC,2,'.',''),
          "EIDDP" => number_format($EIDDP,2,'.',''),
          "EIHFC" => number_format($EIHFC,2,'.',''),
          "EIHFP" => number_format($EIHFP,2,'.',''),
          "EIDVC" => number_format($EIDVC,2,'.',''),
          "EIDVP" => number_format($EIDVP,2,'.',''),
          "EIGCP" => number_format($EIGCP,2,'.',''),          
          
          "SLAPP" => number_format($SLAPP,2,'.',''),
          "SLDDC" => number_format($SLDDC,2,'.',''),
          "SLDDP" => number_format($SLDDP,2,'.',''),
          "SLHFC" => number_format($SLHFC,2,'.',''),
          "SLHFP" => number_format($SLHFP,2,'.',''),
          "SLDVC" => number_format($SLDVC,2,'.',''),
          "SLDVP" => number_format($SLDVP,2,'.',''),
          "SLGCP" => number_format($SLGCP,2,'.',''),

          "SNAPP" => number_format($SNAPP,2,'.',''),
          "SNDDC" => number_format($SNDDC,2,'.',''),
          "SNDDP" => number_format($SNDDP,2,'.',''),
          "SNHFC" => number_format($SNHFC,2,'.',''),
          "SNHFP" => number_format($SNHFP,2,'.',''),
          "SNDVC" => number_format($SNDVC,2,'.',''),
          "SNDVP" => number_format($SNDVP,2,'.',''),
          "SNGCP" => number_format($SNGCP,2,'.',''),

          "SIAPP" => number_format($SIAPP,2,'.',''),
          "SIDDC" => number_format($SIDDC,2,'.',''),
          "SIDDP" => number_format($SIDDP,2,'.',''),
          "SIHFC" => number_format($SIHFC,2,'.',''),
          "SIHFP" => number_format($SIHFP,2,'.',''),
          "SIDVC" => number_format($SIDVC,2,'.',''),
          "SIDVP" => number_format($SIDVP,2,'.',''),
          "SIGCP" => number_format($SIGCP,2,'.',''),
          
          "ALAPP" => number_format($ALAPP,2,'.',''),
          "ALDDC" => number_format($ALDDC,2,'.',''),
          "ALDDP" => number_format($ALDDP,2,'.',''),
          "ALHFC" => number_format($ALHFC,2,'.',''),
          "ALHFP" => number_format($ALHFP,2,'.',''),
          "ALDVC" => number_format($ALDVC,2,'.',''),
          "ALDVP" => number_format($ALDVP,2,'.',''),
          "ALGCP" => number_format($ALGCP,2,'.',''),

          "ANAPP" => number_format($ANAPP,2,'.',''),
          "ANDDC" => number_format($ANDDC,2,'.',''),
          "ANDDP" => number_format($ANDDP,2,'.',''),
          "ANHFC" => number_format($ANHFC,2,'.',''),
          "ANHFP" => number_format($ANHFP,2,'.',''),
          "ANDVC" => number_format($ANDVC,2,'.',''),
          "ANDVP" => number_format($ANDVP,2,'.',''),
          "ANGCP" => number_format($ANGCP,2,'.',''),

          "AIAPP" => number_format($AIAPP,2,'.',''),
          "AIDDC" => number_format($AIDDC,2,'.',''),
          "AIDDP" => number_format($AIDDP,2,'.',''),
          "AIHFC" => number_format($AIHFC,2,'.',''),
          "AIHFP" => number_format($AIHFP,2,'.',''),
          "AIDVC" => number_format($AIDVC,2,'.',''),
          "AIDVP" => number_format($AIDVP,2,'.',''),
          "AIGCP" => number_format($AIGCP,2,'.',''),          
          /*
          "advance_percent" =>  number_format(trim($adv_pay),2,'.',''), 
          "DDC" =>  number_format(trim($ddc),2,'.',''), 
          "DDP" =>  number_format(trim($ddp),2,'.',''), 
          "HFC" =>  number_format(trim($hfc),2,'.',''), 
          "HFP" =>  number_format(trim($hfp),2,'.',''), 
          "DVC" =>  number_format(trim($dvc),2,'.',''), 
          "DVP" =>  number_format(trim($dvp),2,'.',''), 
          "GCP" =>  number_format(trim($gcp),2,'.',''), 
          */
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "status" =>  1, 
        );
        //echo json_encode($insert_data); die();
        if(! $this->payment->check_payment($insert_data) ) {
          if($this->payment->register_payment($insert_data)){ $insert_flag = true;  } 
        } 
      }
    } else {  $this->session->set_flashdata('error','Please select country!');  }

    if($insert_flag){ $this->session->set_flashdata('success','Payment setup successfully added!'); }
    else{ $this->session->set_flashdata('error','Unable to add or Setup already exists for selected country and category!');  }

    redirect('admin/advance-payment/add');
  }

  public function edit()
  { 
    if( $id = $this->uri->segment(4) ) {      
      if( $payment = $this->payment->get_payment_detail($id)){
        $countries = $this->payment->get_countries();
        $this->load->view('admin/advance_payment_edit_veiw', compact('payment','countries'));
        $this->load->view('admin/footer_view');
      } else { redirect('admin/advance-payment'); }
    } else { redirect('admin/advance-payment'); }
  }

  public function update()
  { 
    $pay_id = $this->input->post('pay_id', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $category_id = $this->input->post('category_id', TRUE);

    $loading_free_hours = $this->input->post('loading_free_hours', TRUE);
    $loading_hours_charge = $this->input->post('loading_hours_charge', TRUE);

    $ELAPP =  $this->input->post('ELAPP', TRUE);
    $ELDDC = $this->input->post('ELDDC', TRUE);
    $ELDDP = $this->input->post('ELDDP', TRUE);
    $ELHFC = $this->input->post('ELHFC', TRUE);
    $ELHFP = $this->input->post('ELHFP', TRUE);
    $ELDVC = $this->input->post('ELDVC', TRUE);
    $ELDVP = $this->input->post('ELDVP', TRUE);
    $ELGCP = $this->input->post('ELGCP', TRUE);
 
    $ENAPP = $this->input->post('ENAPP', TRUE);
    $ENDDC = $this->input->post('ENDDC', TRUE);
    $ENDDP = $this->input->post('ENDDP', TRUE);
    $ENHFC = $this->input->post('ENHFC', TRUE);
    $ENHFP = $this->input->post('ENHFP', TRUE);
    $ENDVC = $this->input->post('ENDVC', TRUE);
    $ENDVP = $this->input->post('ENDVP', TRUE);
    $ENGCP = $this->input->post('ENGCP', TRUE);

    $EIAPP = $this->input->post('EIAPP', TRUE);
    $EIDDC = $this->input->post('EIDDC', TRUE);
    $EIDDP = $this->input->post('EIDDP', TRUE);
    $EIHFC = $this->input->post('EIHFC', TRUE);
    $EIHFP = $this->input->post('EIHFP', TRUE);
    $EIDVC = $this->input->post('EIDVC', TRUE);
    $EIDVP = $this->input->post('EIDVP', TRUE);
    $EIGCP = $this->input->post('EIGCP', TRUE);

    $SLAPP = $this->input->post('SLAPP', TRUE);
    $SLDDC = $this->input->post('SLDDC', TRUE);
    $SLDDP = $this->input->post('SLDDP', TRUE);
    $SLHFC = $this->input->post('SLHFC', TRUE);
    $SLHFP = $this->input->post('SLHFP', TRUE);
    $SLDVC = $this->input->post('SLDVC', TRUE);
    $SLDVP = $this->input->post('SLDVP', TRUE);
    $SLGCP = $this->input->post('SLGCP', TRUE);

    $SNAPP = $this->input->post('SNAPP', TRUE);
    $SNDDC = $this->input->post('SNDDC', TRUE);
    $SNDDP = $this->input->post('SNDDP', TRUE);
    $SNHFC = $this->input->post('SNHFC', TRUE);
    $SNHFP = $this->input->post('SNHFP', TRUE);
    $SNDVC = $this->input->post('SNDVC', TRUE);
    $SNDVP = $this->input->post('SNDVP', TRUE);
    $SNGCP = $this->input->post('SNGCP', TRUE);

    $SIAPP = $this->input->post('SIAPP', TRUE);
    $SIDDC = $this->input->post('SIDDC', TRUE);
    $SIDDP = $this->input->post('SIDDP', TRUE);
    $SIHFC = $this->input->post('SIHFC', TRUE);
    $SIHFP = $this->input->post('SIHFP', TRUE);
    $SIDVC = $this->input->post('SIDVC', TRUE);
    $SIDVP = $this->input->post('SIDVP', TRUE);
    $SIGCP = $this->input->post('SIGCP', TRUE);

    $ALAPP = $this->input->post('ALAPP', TRUE);
    $ALDDC = $this->input->post('ALDDC', TRUE);
    $ALDDP = $this->input->post('ALDDP', TRUE);
    $ALHFC = $this->input->post('ALHFC', TRUE);
    $ALHFP = $this->input->post('ALHFP', TRUE);
    $ALDVC = $this->input->post('ALDVC', TRUE);
    $ALDVP = $this->input->post('ALDVP', TRUE);
    $ALGCP = $this->input->post('ALGCP', TRUE);

    $ANAPP = $this->input->post('ANAPP', TRUE);
    $ANDDC = $this->input->post('ANDDC', TRUE);
    $ANDDP = $this->input->post('ANDDP', TRUE);
    $ANHFC = $this->input->post('ANHFC', TRUE);
    $ANHFP = $this->input->post('ANHFP', TRUE);
    $ANDVC = $this->input->post('ANDVC', TRUE);
    $ANDVP = $this->input->post('ANDVP', TRUE);
    $ANGCP = $this->input->post('ANGCP', TRUE);

    $AIAPP = $this->input->post('AIAPP', TRUE);
    $AIDDC = $this->input->post('AIDDC', TRUE);
    $AIDDP = $this->input->post('AIDDP', TRUE);
    $AIHFC = $this->input->post('AIHFC', TRUE);
    $AIHFP = $this->input->post('AIHFP', TRUE);
    $AIDVC = $this->input->post('AIDVC', TRUE);
    $AIDVP = $this->input->post('AIDVP', TRUE);
    $AIGCP = $this->input->post('AIGCP', TRUE);

    $update_data = array(       

          "loading_free_hours" => number_format($loading_free_hours,2,'.',''),
          "loading_hours_charge" => number_format($loading_hours_charge,2,'.',''),
   
      "ELAPP" => number_format($ELAPP,2,'.',''),
      "ELDDC" => number_format($ELDDC,2,'.',''),
      "ELDDP" => number_format($ELDDP,2,'.',''),
      "ELHFC" => number_format($ELHFC,2,'.',''),
      "ELHFP" => number_format($ELHFP,2,'.',''),
      "ELDVC" => number_format($ELDVC,2,'.',''),
      "ELDVP" => number_format($ELDVP,2,'.',''),
      "ELGCP" => number_format($ELGCP,2,'.',''),
       
      "ENAPP" => number_format($ENAPP,2,'.',''),
      "ENDDC" => number_format($ENDDC,2,'.',''),
      "ENDDP" => number_format($ENDDP,2,'.',''),
      "ENHFC" => number_format($ENHFC,2,'.',''),
      "ENHFP" => number_format($ENHFP,2,'.',''),
      "ENDVC" => number_format($ENDVC,2,'.',''),
      "ENDVP" => number_format($ENDVP,2,'.',''),
      "ENGCP" => number_format($ENGCP,2,'.',''),

      "EIAPP" => number_format($EIAPP,2,'.',''),
      "EIDDC" => number_format($EIDDC,2,'.',''),
      "EIDDP" => number_format($EIDDP,2,'.',''),
      "EIHFC" => number_format($EIHFC,2,'.',''),
      "EIHFP" => number_format($EIHFP,2,'.',''),
      "EIDVC" => number_format($EIDVC,2,'.',''),
      "EIDVP" => number_format($EIDVP,2,'.',''),
      "EIGCP" => number_format($EIGCP,2,'.',''),
      
      "SLAPP" => number_format($SLAPP,2,'.',''),
      "SLDDC" => number_format($SLDDC,2,'.',''),
      "SLDDP" => number_format($SLDDP,2,'.',''),
      "SLHFC" => number_format($SLHFC,2,'.',''),
      "SLHFP" => number_format($SLHFP,2,'.',''),
      "SLDVC" => number_format($SLDVC,2,'.',''),
      "SLDVP" => number_format($SLDVP,2,'.',''),
      "SLGCP" => number_format($SLGCP,2,'.',''),

      "SNAPP" => number_format($SNAPP,2,'.',''),
      "SNDDC" => number_format($SNDDC,2,'.',''),
      "SNDDP" => number_format($SNDDP,2,'.',''),
      "SNHFC" => number_format($SNHFC,2,'.',''),
      "SNHFP" => number_format($SNHFP,2,'.',''),
      "SNDVC" => number_format($SNDVC,2,'.',''),
      "SNDVP" => number_format($SNDVP,2,'.',''),
      "SNGCP" => number_format($SNGCP,2,'.',''),

      "SIAPP" => number_format($SIAPP,2,'.',''),
      "SIDDC" => number_format($SIDDC,2,'.',''),
      "SIDDP" => number_format($SIDDP,2,'.',''),
      "SIHFC" => number_format($SIHFC,2,'.',''),
      "SIHFP" => number_format($SIHFP,2,'.',''),
      "SIDVC" => number_format($SIDVC,2,'.',''),
      "SIDVP" => number_format($SIDVP,2,'.',''),
      "SIGCP" => number_format($SIGCP,2,'.',''),

      "ALAPP" => number_format($ALAPP,2,'.',''),
      "ALDDC" => number_format($ALDDC,2,'.',''),
      "ALDDP" => number_format($ALDDP,2,'.',''),
      "ALHFC" => number_format($ALHFC,2,'.',''),
      "ALHFP" => number_format($ALHFP,2,'.',''),
      "ALDVC" => number_format($ALDVC,2,'.',''),
      "ALDVP" => number_format($ALDVP,2,'.',''),
      "ALGCP" => number_format($ALGCP,2,'.',''),

      "ANAPP" => number_format($ANAPP,2,'.',''),
      "ANDDC" => number_format($ANDDC,2,'.',''),
      "ANDDP" => number_format($ANDDP,2,'.',''),
      "ANHFC" => number_format($ANHFC,2,'.',''),
      "ANHFP" => number_format($ANHFP,2,'.',''),
      "ANDVC" => number_format($ANDVC,2,'.',''),
      "ANDVP" => number_format($ANDVP,2,'.',''),
      "ANGCP" => number_format($ANGCP,2,'.',''),

      "AIAPP" => number_format($AIAPP,2,'.',''),
      "AIDDC" => number_format($AIDDC,2,'.',''),
      "AIDDP" => number_format($AIDDP,2,'.',''),
      "AIHFC" => number_format($AIHFC,2,'.',''),
      "AIHFP" => number_format($AIHFP,2,'.',''),
      "AIDVC" => number_format($AIDVC,2,'.',''),
      "AIDVP" => number_format($AIDVP,2,'.',''),
      "AIGCP" => number_format($AIGCP,2,'.',''),
    );
    
    $pay = $this->payment->get_payment_detail($pay_id);
    if($pay['country_id'] == $country_id && $pay['pay_id'] == $pay_id && $pay['category_id'] == $category_id ) {
      if($this->payment->update_payment($update_data, $pay_id)){
        $this->session->set_flashdata('success','Payment setup successfully added!');
      } else{ $this->session->set_flashdata('error','Unable to update configuration or same configuration found! Try with different configuration...');  }
    } else{ $this->session->set_flashdata('error','Failed! Some unwanted scripting injection found! Please refresh the page and try again...');  }

    redirect('admin/advance-payment/edit/'.$pay_id);
  }

  public function update__old()
  {   
    $pay_id = $this->input->post('pay_id', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $adv_pay = $this->input->post('adv_pay', TRUE);
    $ddc = $this->input->post('ddc', TRUE);
    $ddp = $this->input->post('ddp', TRUE);
    $hfc = $this->input->post('hfc', TRUE);
    $hfp = $this->input->post('hfp', TRUE);
    $dvc = $this->input->post('dvc', TRUE);
    $dvp = $this->input->post('dvp', TRUE);
    $gcp = $this->input->post('gcp', TRUE);

    //if($country_id > 0 ) {
      if ( (float) $adv_pay < 100 ) {
        $update_data = array(
          "country_id" =>  trim($country_id), 
          "advance_percent" => number_format(trim($adv_pay),2,'.',''), 
          "DDC" =>  number_format(trim($ddc),2,'.',''), 
          "DDP" =>  number_format(trim($ddp),2,'.',''), 
          "HFC" =>  number_format(trim($hfc),2,'.',''), 
          "HFP" =>  number_format(trim($hfp),2,'.',''), 
          "DVC" =>  number_format(trim($dvc),2,'.',''), 
          "DVP" =>  number_format(trim($dvp),2,'.',''), 
          "GCP" =>  number_format(trim($gcp),2,'.',''), 
        );
        
        $pay = $this->payment->get_payment_detail($pay_id);

        if($country_id == $pay['country_id']) {
          if($this->payment->update_payment($update_data, $pay_id)){
            $this->session->set_flashdata('success','Advance payment successfully updated!');
          } else {  $this->session->set_flashdata('error','Unable to update Advance payment! Try again...');  } 
        }
        else{         
          if(! $this->payment->check_payment($update_data) ) {
            if($this->payment->update_payment($update_data, $pay_id)){
              $this->session->set_flashdata('success','Advance payment successfully updated!');
            } else {  $this->session->set_flashdata('error','Unable to update Advance payment! Try again...');  } 
          } else {  $this->session->set_flashdata('error','Advance payment already exists!'); }         
        }

      } else {  $this->session->set_flashdata('error','Please enter percentage less than or equal to 100.!'); }
    //} else {  $this->session->set_flashdata('error','Please select country!');  }

    redirect('admin/advance-payment/edit/'.$pay_id);
  }

  public function delete()
  {
    $id = $this->input->post('id', TRUE);
    if($this->payment->delete_payment($id)){ echo 'success'; } else { echo "failed";  } 
  }

}

/* End of file Advance_payment.php */
/* Location: ./application/controllers/Advance_payment.php */