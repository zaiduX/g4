<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends CI_Controller {

  private $cust = array();

  public function __construct()
  {
    parent::__construct();

    $this->cust_id = $this->session->userdata("cust_id"); $this->cust_id = (int) $this->cust_id;
      
    $this->load->model('api_model', 'api');
    $this->load->model('Notification_model', 'notification');
    $this->load->model('Web_user_model', 'user');
    $this->load->model('Transport_model', 'transport');
  
    $this->cust = $this->api->get_consumer_datails($this->cust_id, true); $cust = $this->cust;
    
    if( ( $this->session->userdata("is_logged_in") == "NULL" ) OR ( $this->session->userdata("is_logged_in") !== 1 ) ) { redirect('log-in'); }

    if($cust['mobile_verified'] == 0) { redirect('verification/otp'); }

    $lang = $this->session->userdata('language');
    $this->config->set_item('language', strtolower($lang));

    if(trim($lang) == 'french'){
      $this->lang->load('frenchApi_lang','french');
      $this->lang->load('frenchFront_lang','french');
    } 
    else if(trim($lang) == 'spanish'){  
      $this->lang->load('spanishApi_lang','spanish');
      $this->lang->load('spanishFront_lang','spanish');
    }
    else { 
      $this->lang->load('englishApi_lang','english');
      $this->lang->load('englishFront_lang','english');
    }
      
    $this->api->update_last_login($cust['cust_id']);
    if (!$this->input->is_ajax_request()){ 
      // var_dump($_POST); 
      $order_id = $this->input->post('order_id');
      $type = $this->input->post('type');

      // if($order_id && !$ow_id) { 
      if($order_id && ($type =="workroom")) { 
        $ids = $this->api->get_total_unread_order_workroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) {  $this->api->make_workroom_notification_read($v['ow_id'], $cust['cust_id']);  }
      }

      if($order_id && ($type =="chatroom")) { 
        $ids = $this->api->get_total_unread_order_chatroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) { $this->api->make_chatrooom_notification_read($v['chat_id'], $cust['cust_id']);  }
      }
      if($cust['user_type'] == 1 ){ $user_type ="Individuals"; } else { $user_type ="Business"; }
      $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust['cust_id'],$user_type );

      //echo $this->db->last_query(); die();
      $notification = $this->api->get_total_unread_notifications($cust['cust_id'],$user_type, 5);
      $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust['cust_id']);      
      $nt_workroom = $this->api->get_total_unread_workroom_notifications($cust['cust_id'], 5);  
      $total_chatroom_notifications = $this->api->get_total_unread_chatroom_notifications_count($cust['cust_id']);      
      $nt_chatroom = $this->api->get_total_unread_chatroom_notifications($cust['cust_id'], 5);  

      $compact = ["total_unread_notifications","notification","total_workroom_notifications","nt_workroom","total_chatroom_notifications","nt_chatroom"];
      $this->load->view('user_panel/header',compact($compact)); 
      $this->load->view('user_panel/menu', compact("cust"));
    }
  }

  public function index() { exit('No direct script access allowed');  }

  public function my_bookings()
  { 
    echo json_encode("Under Development"); die();
    // echo json_encode($_POST); die();
    $countries = $this->user->get_countries();
    $categories = $this->api->get_category();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id);   
    //Filters
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');

    //  New params in filter
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth', '6');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air', '6');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea', '6');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
        $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
        $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
      } else { $creation_start_date = $creation_end_date = null; }
      
      if(trim($pickup_start) != "" && trim($pickup_end) != "") {
        $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
        $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
      } else { $pickup_start_date = $pickup_end_date = null; }

      if(trim($delivery_start) != "" && trim($delivery_end) != "") {
        $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
        $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
      } else { $delivery_start_date = $delivery_end_date = null; }

      if(trim($expiry_start) != "" && trim($expiry_end) != "") {
        $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
        $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
      } else { $expiry_start_date = $expiry_end_date = null; }

      $filter_array = array(
        "user_id" => $this->cust_id,
        "user_type" => "cust",
        "order_status" => 'open',
        "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
        "from_address" => ($from_address!=null)? $from_address: "NULL",
        "to_address" => ($to_address!=null)? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!=null)? $$pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
        "price" => ($price!=null)?$price:"0",
        "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
        "order_type" => ($order_type!=null)? $order_type: "NULL",
        // new filters
        "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=null)? $distance_start: "0",
        "distance_end" => ($distance_end!=null)? $distance_end: "0",
        "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=null)?$max_weight:"0",
        "unit_id" => ($unit_id != null)?$unit_id:"0",
      );
      // echo json_encode($filter_array); die();
      $orders = $this->user->filtered_list($filter_array);
      //echo $this->db->last_query(); die();
      $filter = 'advance';
      $this->load->view('user_panel/transport_my_bookings_list_view', compact('orders','countries','categories','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans'));
    } else {
      $orders = $this->user->get_booking_list($this->cust_id,'open');
      // echo json_encode($orders); die();
      $filter = 'basic';
      $this->load->view('user_panel/transport_my_bookings_list_view', compact('orders','countries','categories','dimensions','filter','unit','earth_trans','air_trans','sea_trans'));
    }   
    $this->load->view('user_panel/footer');
  }

  // Create new booking view
  public function add_bookings()
  { 
    //echo json_encode($_REQUEST); die();
    $category_id = $this->input->post("category_id"); $category_id = (int)$category_id;
    if(isset($category_id) && $category_id == 6 ) {
      $transport_type = 'earth'; 
      $this->session->unset_userdata('cat_id');
      $this->session->unset_userdata('vehicle');
      $this->session->unset_userdata('pickupdate');
      $this->session->unset_userdata('pickuptime');
      $this->session->unset_userdata('c_quantity');
      $this->session->unset_userdata('dimension_id');
      $this->session->unset_userdata('deliverdate');
      $this->session->unset_userdata('delivertime');
      $this->session->unset_userdata('c_weight');
      $this->session->unset_userdata('image_url');
      $this->session->unset_userdata('c_width');
      $this->session->unset_userdata('c_height');
      $this->session->unset_userdata('c_length');
      $this->session->unset_userdata('order_mode');

      $this->session->unset_userdata('from_address');
      $this->session->unset_userdata('frm_latitude');
      $this->session->unset_userdata('frm_longitude');
      $this->session->unset_userdata('frm_formatted_address');
      $this->session->unset_userdata('frm_postal_code');
      $this->session->unset_userdata('frm_country');
      $this->session->unset_userdata('from_country_id');
      $this->session->unset_userdata('frm_state');
      $this->session->unset_userdata('frm_city');

      $this->session->unset_userdata('to_address');
      $this->session->unset_userdata('to_latitude');
      $this->session->unset_userdata('to_longitude');
      $this->session->unset_userdata('to_formatted_address');
      $this->session->unset_userdata('to_postal_code');
      $this->session->unset_userdata('to_country');
      $this->session->unset_userdata('to_country_id');
      $this->session->unset_userdata('to_state');
      $this->session->unset_userdata('to_city');

      $this->session->unset_userdata('transport_type');
      $this->session->unset_userdata('loading_free_hours');
    } else {
      $category_id = (int)$_SESSION['cat_id']; 
      $transport_type = $_SESSION['transport_type'];
    }

    if(!$category_id) { redirect(base_url('user-panel/my-bookings')); }
    $countries = $this->user->get_countries();
    $default_trans = $this->user->get_transport_vehicle_list_by_type(trim($transport_type), $category_id);
    $address = $this->api->get_consumers_addressbook($this->cust['cust_id'],array());
    $relay = $this->user->get_relay_points();
    // $transport = $this->api->get_transport_master(true);
    $dimension = $this->api->get_standard_dimension_masters_cat_id($category_id, trim($transport_type));
    $unit = $this->api->get_unit_master(true);
    $user_details = $this->api->get_user_details($this->cust['cust_id']);
    $this->load->view('user_panel/transport_add_new_booking_view',compact('category_id','address','transport','dimension','unit','relay','default_trans','countries','user_details'));
    $this->load->view('user_panel/footer');
  }

  public function get_countries_dangerous_goods()
  {
    $category_id = $this->input->post('category_id');
    $from_country_id = $this->input->post('from_country_id');
    $to_country_id = $this->input->post('to_country_id'); 
    $result=[];
    // echo json_encode($to_country_id ); die();   
    if($from_country_id != $to_country_id){  
      $result['dangerous_goods'] = $this->transport->get_countries_dangerous_goods($from_country_id, $to_country_id); 
    }
    else{   $result['dangerous_goods'] = $this->transport->get_country_dangerous_goods($from_country_id); }
    if(!$category_id){ $result['loading_unloading_free_hrs']=0;}
    else{
     $constants = $this->api->get_advance_payment((int)$category_id, $from_country_id); 
     //var_dump($constants);
     $result['loading_unloading_free_hrs']=$constants['loading_free_hours'];
    }

   echo json_encode($result);
  }

  // get total order price
  public function get_total_order_price()
  {
    // echo json_encode($_POST); die();
    $category_id = $this->input->post('category_id');
    $delivery_datetime = $this->input->post('delivery_datetime');
    $pickup_datetime = $this->input->post('pickup_datetime');
    $transport_type = $this->input->post('transport_type');
    $service_area_type = $this->input->post('service_area_type');
    $frm_addr_id = $this->input->post('frm_addr_id');
    $to_addr_id = $this->input->post('to_addr_id');
    $frm_addr_type = $this->input->post('frm_addr_type');
    $to_addr_type = $this->input->post('to_addr_type');   
  
    $quantity = $this->input->post('total_quantity');
    $total_quantity = 0;
    if(!empty($quantity)){ foreach ($quantity as $q) {  $total_quantity += $q; } }
    $width = $this->input->post('c_width');
    $total_width = 0; 
    if(!empty($width)){ foreach ($width as $w) {  $total_width += $w; } }
    $height = $this->input->post('c_height');
    $total_height = 0; 
    if(!empty($height)){ foreach ($height as $h) {  $total_height += $h; } }
    $length = $this->input->post('c_length');
    $total_length = 0; 
    if(!empty($length)){ foreach ($length as $l) {  $total_length += $l; } }    
   
    $weight = $this->input->post('total_weight');
    $total_weight = 0;
    $unit_id = $this->input->post('c_unit_id');
    if(!empty($unit_id)){       
      $wu = array_map(function($k,$v){ return array($k,$v);},$unit_id,$weight);      
      for($i=0; $i < count($wu); $i++) { 
        $unit = $this->transport->get_unit($wu[$i][0]);
        if(strtolower($unit['shortname']) == 'kg') {  $total_weight += $wu[$i][1];  } 
        else{ $total_weight += ($w * $unit['conversion_rate_in_kg']); }
      } 
    }    
    
    $insurance_check = $this->input->post('insurance_check');
    $package_value = $this->input->post('package_value');
    $handling_by = $this->input->post('handling_by');
    $dedicated_vehicle = $this->input->post('dedicated_vehicle');
    $distance_in_km = 0;

    //  Newly added keys
    $old_new_goods = $this->input->post('old_new_goods', TRUE); 
    if(empty($old_new_goods)){ $old_new_goods = "NULL"; }
    $custom_clearance_by = $this->input->post('custom_clearance', TRUE);
    if(empty($custom_clearance_by)){ $custom_clearance_by = "NULL"; }
    $custom_package_value = $this->input->post('custom_package_value', TRUE);
    if(empty($custom_package_value)){ $custom_package_value = 0; }
    $loading_time = $this->input->post('loading_time');

    if($frm_addr_type =="address"){ 
      $frm_addr = $this->user->get_address_from_book($frm_addr_id);
      $from_latitude = $frm_addr['latitude'];
      $from_longitude = $frm_addr['longitude'];
      $from_country_id = $frm_addr['country_id'];
      $from_state_id = $frm_addr['state_id'];
      $from_city_id = $frm_addr['city_id'];
    } 
    else if($frm_addr_type =="relay"){  
      $frm_addr = $this->user->get_relay_pint_details($frm_addr_id);
      $from_latitude = $frm_addr['latitude'];
      $from_longitude = $frm_addr['longitude'];     
      $from_country_id = $frm_addr['country_id'];
      $from_state_id = $frm_addr['state_id'];
      $from_city_id = $frm_addr['city_id'];
    } else { $from_latitude = $from_longitude = 0; $from_country_id = 0; $from_state_id = 0; $from_city_id = 0; }

    if($to_addr_type =="address"){  
      $to_addr = $this->user->get_address_from_book($to_addr_id);
      $to_latitude = $to_addr['latitude'];
      $to_longitude = $to_addr['longitude'];
      $to_country_id = $to_addr['country_id'];
      $to_state_id = $to_addr['state_id'];
      $to_city_id = $to_addr['city_id'];
    } 
    else if($to_addr_type =="relay"){ 
      $to_addr = $this->user->get_relay_pint_details($to_addr_id); 
      $to_latitude = $to_addr['latitude'];
      $to_longitude = $to_addr['longitude'];
      $to_country_id = $to_addr['country_id'];
      $to_state_id = $to_addr['state_id'];
      $to_city_id = $to_addr['city_id'];
    } else { $to_latitude = $to_longitude = 0; $to_country_id = 0; $to_state_id = 0; $to_city_id = 0; }

    $calculate_price =  array (
      "cust_id" => $this->cust_id,
      "category_id" => $category_id,
      
      "delivery_datetime" => $delivery_datetime,
      "pickup_datetime" => $pickup_datetime,
      
      "total_quantity" => $total_quantity,
      
      "width" => $total_width,
      "height" => $total_height,
      "length" => $total_length,
      
      "total_weight" => $total_weight,
      "unit_id" => 1,
      
      "from_country_id" => $from_country_id,
      "from_state_id" => $from_state_id,
      "from_city_id" => $from_city_id,
      "from_latitude" => $from_latitude,
      "from_longitude" => $from_longitude,

      "to_country_id" => $to_country_id,
      "to_state_id" => $to_state_id,
      "to_city_id" => $to_city_id,
      "to_latitude" => $to_latitude,
      "to_longitude" => $to_longitude,

      "service_area_type" => $service_area_type,
      "transport_type" => $transport_type,
      "handling_by" => $handling_by,
      "dedicated_vehicle" => $dedicated_vehicle,
      "package_value" => $package_value,

      "custom_clearance_by" => $custom_clearance_by,
      "old_new_goods" => $old_new_goods,
      "custom_package_value" => $custom_package_value,
      "loading_time" => $loading_time,
    );
    // echo json_encode($calculate_price); die();
    echo json_encode($this->api->calculate_order_price($calculate_price));
  }

  // insert new booking
  public function register_booking()
  {
    //echo json_encode($_POST); die();
    if( isset($_SESSION['cat_id'])) {
      $this->session->unset_userdata('cat_id');
      $this->session->unset_userdata('vehicle');
      $this->session->unset_userdata('pickupdate');
      $this->session->unset_userdata('pickuptime');
      $this->session->unset_userdata('c_quantity');
      $this->session->unset_userdata('dimension_id');
      $this->session->unset_userdata('deliverdate');
      $this->session->unset_userdata('delivertime');
      $this->session->unset_userdata('c_weight');
      $this->session->unset_userdata('image_url');
      $this->session->unset_userdata('c_width');
      $this->session->unset_userdata('c_height');
      $this->session->unset_userdata('c_length');
    }
        
    $payment_mode = $this->input->post('payment_mode', TRUE);  // cod / payment
    if($payment_mode == 'payment') $payment_mode = 'NULL';
    $cod_payment_type = $this->input->post('cod_payment_type', TRUE);
    if(!isset($cod_payment_type)) $cod_payment_type = 'NULL';
    $frm_addr_type = $this->input->post('frm_addr_type', TRUE); 
    $to_addr_type = $this->input->post('to_addr_type', TRUE); 
    $transport_type = $this->input->post('transport_type', TRUE); 
    $service_area_type = $this->input->post('order_mode', TRUE); 
    $vehical_type_id = $this->input->post('vehicle', TRUE); 
    $order_type = $this->input->post('shipping_mode', TRUE); 
    $dimension_id = $this->input->post('dimension_id', TRUE); 
    $content = $this->input->post('content', TRUE); 
    $dangerous_goods = $this->input->post('dangerous_goods', TRUE); 
    $need_tailgate = $this->input->post('need_tailgate', TRUE); 

    $pickup_datetime = $this->input->post('pickupdate', TRUE) . ' '.$this->input->post('pickuptime', TRUE);  
    $pickup_datetime = date('Y-m-d H:i:s', strtotime($pickup_datetime));
    $delivery_datetime = $this->input->post('deliverdate', TRUE). ' '.$this->input->post('delivertime', TRUE); 
    $delivery_datetime = date('Y-m-d H:i:s', strtotime($delivery_datetime));
    $expiry_date = $this->input->post('expiry_date', TRUE);  
    $expiry_date = date('Y-m-d H:i:s', strtotime($expiry_date));
    $visible_by = $this->input->post('visible_by', TRUE);
    
    $quantity = $this->input->post('c_quantity');
    $total_quantity = 0;
    if(!empty($quantity)){ foreach ($quantity as $q) {  $total_quantity += $q; } }

    $width = $this->input->post('c_width');
    $total_width = 0; 
    if(!empty($width)){ foreach ($width as $w) {  $total_width += $w; } }

    $height = $this->input->post('c_height');
    $total_height = 0; 
    if(!empty($height)){ foreach ($height as $h) {  $total_height += $h; } }

    $length = $this->input->post('c_length');
    $total_length = 0; 
    if(!empty($length)){ foreach ($length as $l) {  $total_length += $l; } }    

    $weight = $this->input->post('c_weight');
    $total_weight = 0;
    $unit_id = $this->input->post('c_unit_id');
    if(!empty($unit_id)){ 
      $wu = array_map(function($k,$v){ return array($k,$v);},$unit_id,$weight);      
      for($i=0; $i < count($wu); $i++) { 
        $unit = $this->transport->get_unit($wu[$i][0]);
        if(strtolower($unit['shortname']) == 'kg') {  $total_weight += $wu[$i][1];  } 
        else{ $total_weight += ($w * $unit['conversion_rate_in_kg']); }
      } 
    } 

    $count = COUNT($quantity);
    $package = array();

    for( $i=0; $i < $count; $i++){
      $package[] = array(
        "quantity" => $quantity[$i], 
        "dimension_id" => $dimension_id[$i], 
        "width" => $width[$i], 
        "height" => $height[$i], 
        "length" => $length[$i], 
        "weight" => $weight[$i], 
        "unit_id" => $unit_id[$i], 
        "content" => $content[$i], 
        "dangerous_goods_id" => $dangerous_goods[$i]
      );
    }

    $insurance_check = $this->input->post('insurance', TRUE); 
    $package_value = $this->input->post('package_value', TRUE); 
    $handling_by = $this->input->post('handle_by', TRUE); 
    $with_driver = $this->input->post('travel_with', TRUE); 
    $dedicated_vehicle = $this->input->post('dedicated_vehicle', TRUE); 
    $ref_no = $this->input->post('ref_no', TRUE); 
    $deliver_instructions = $this->input->post('deliver_instruction', TRUE);      
    if(empty($deliver_instructions)){ $deliver_instructions = "NULL"; }   
    $pickup_instructions = $this->input->post('pickup_instruction', TRUE);      
    if(empty($pickup_instructions)){ $pickup_instructions = "NULL"; }
    $description = $this->input->post('description', TRUE);     
    if(empty($description)){ $description = "NULL"; }
    $advance_payment = $this->input->post('advance_pay', TRUE);
    $pickup_payment = $this->input->post('pickup_pay', TRUE);
    $deliver_payment = $this->input->post('deliver_pay', TRUE);
    //For COD, Bank and Payment
    if($payment_mode == 'bank') {
      $dvncPay = 0;
      $pckupPay = 0;
      $dlvrPay = $advance_payment + $pickup_payment + $deliver_payment;
      $is_bank_payment = 1;
      $balance_amount = $dlvrPay;
    } else {
      $is_bank_payment = 0;
      if($payment_mode == 'cod' && $cod_payment_type == 'at_pickup') {
        $dvncPay = 0;
        $pckupPay = $advance_payment + $pickup_payment + $deliver_payment;
        $dlvrPay = 0;
      } else if($payment_mode == 'cod' && $cod_payment_type == 'at_deliver') {
        $dvncPay = 0;
        $pckupPay = 0;
        $dlvrPay = $advance_payment + $pickup_payment + $deliver_payment;
      } else {
        $dvncPay = $advance_payment;
        $pckupPay = $pickup_payment; 
        $dlvrPay = $deliver_payment;
      }
      $balance_amount = $advance_payment + $pickup_payment + $deliver_payment;
    }

    //  Newly added keys
    $category_id = $this->input->post('category_id', TRUE);
    $old_new_goods = $this->input->post('old_new_goods', TRUE);
    if(empty($old_new_goods)){ $old_new_goods = "NULL"; }
    $custom_clearance_by = $this->input->post('custom_clearance', TRUE);
    if(empty($custom_clearance_by)){ $custom_clearance_by = "NULL"; }
    $custom_package_value = $this->input->post('custom_package_value', TRUE);
    if(empty($custom_package_value)){ $custom_package_value = 0; }
    $loading_time = $this->input->post('loading_unloading_hours');
    
    $cust = $this->cust; 
    $cust_id = (int) $cust['cust_id'];
    $cust_email = $cust['email1'];
    $parts = explode("@", $cust_email);
    $cust_email = $parts[0];
    $cust_name = ($cust['firstname'] !="NULL") ? ($cust['firstname']. ' ' .$cust['lastname']):$cust_email;
    $cust_name = ($cust_name!="NULL" && $cust['company_name']!="NULL")?($cust_name.' ['.trim($cust['company_name']).']'):( ($cust_name=="NULL")?trim($cust['company_name']):$cust_name);

    //echo $cust_name;  die();

    $distance_in_km = 0;

    if($frm_addr_type =="address"){ 
      $from_addr_type = 0; $from_relay_id = 0;
      $frm_addr_id = $this->input->post('from_address', TRUE); 
      $frm_addr = $this->user->get_address_from_book($frm_addr_id); 
      $from_address_name = trim($frm_addr['firstname']). ' '. trim($frm_addr['lastname']);
      $from_address_contact = trim($frm_addr['mobile_no']);
      $from_full_address = ($frm_addr['addr_line1'] !="NULL")? trim($frm_addr['addr_line1']):"". ' ';
      $from_full_address .= ($frm_addr['addr_line2'] !="NULL")? trim($frm_addr['addr_line2']):"". ' ';
      $from_latitude = $frm_addr['latitude'];
      $from_longitude = $frm_addr['longitude'];
      $from_country_id = $frm_addr['country_id'];
      $from_state_id = $frm_addr['state_id'];
      $from_city_id = $frm_addr['city_id']; 
      $from_address_email = $frm_addr['email'];
      // Format1: <Street>, <Zip code>, <City>, <State>, <Country>
      $frm_addr_type1 = $frm_addr['street_name']; 
    } 
    else if($frm_addr_type =="relay"){  
      $frm_addr_id = $this->input->post('from_relay', TRUE);
      $from_addr_type = 1; $from_relay_id = $frm_addr_id;
      $frm_addr = $this->user->get_relay_pint_details($frm_addr_id);
      $from_address_name = trim($frm_addr['firstname']). ' '. trim($frm_addr['lastname']);
      $from_address_contact = trim($frm_addr['mobile_no']);
      $from_full_address = ($frm_addr['addr_line1'] !="NULL")? trim($frm_addr['addr_line1']):"". ' ';
      $from_full_address .= ($frm_addr['addr_line2'] !="NULL")? trim($frm_addr['addr_line2']):"". ' ';
      $from_latitude = $frm_addr['latitude'];
      $from_longitude = $frm_addr['longitude']; 
      $from_country_id = $frm_addr['country_id'];
      $from_state_id = $frm_addr['state_id'];
      $from_city_id = $frm_addr['city_id'];
      // Format1: <Street>, <Zip code>, <City>, <State>, <Country>
      $frm_addr_type1 = $frm_addr['street_name'].', '.$frm_addr['zipcode'].', '.$this->api->get_city_name_by_id($frm_addr['city_id']).', '.$this->api->get_state_name_by_id($frm_addr['state_id']).', '.$this->api->get_country_name_by_id($frm_addr['country_id']); 
    } else { $from_latitude = $from_longitude = 0; }

    if($to_addr_type =="address"){  
      $to_addr_type = 0; $to_relay_id = 0;
      $to_addr_id = $this->input->post('to_address', TRUE);
      $to_addr = $this->user->get_address_from_book($to_addr_id);
      $to_address_name = trim($to_addr['firstname']). ' '. trim($to_addr['lastname']);
      $to_address_contact = trim($to_addr['mobile_no']);
      $to_full_address = ($to_addr['addr_line1'] !="NULL")? trim($to_addr['addr_line1']):"". ' ';
      $to_full_address .= ($to_addr['addr_line1'] !="NULL")? trim($to_addr['addr_line2']):"". ' ';
      $to_latitude = $to_addr['latitude'];
      $to_longitude = $to_addr['longitude'];
      $to_country_id = $to_addr['country_id'];
      $to_state_id = $to_addr['state_id'];
      $to_city_id = $to_addr['city_id'];
      $to_address_email = $to_addr['email'];
      // Format1: <Street>, <Zip code>, <City>, <State>, <Country>
      $to_addr_type1 = $to_addr['street_name']; 
    } 
    else if($to_addr_type =="relay"){ 
      $to_addr_id = $this->input->post('to_relay', TRUE);
      $to_addr_type = 1; $to_relay_id = $to_addr_id;
      $to_addr = $this->user->get_relay_pint_details($to_addr_id); 
      $to_address_name = trim($to_addr['firstname']). ' '. trim($to_addr['lastname']);
      $to_address_contact = trim($to_addr['mobile_no']);
      $to_full_address = ($to_addr['addr_line1'] !="NULL")? trim($to_addr['addr_line1']):"". ' ';
      $to_full_address .= ($to_addr['addr_line2'] !="NULL")? trim($to_addr['addr_line2']):"". ' ';
      $to_latitude = $to_addr['latitude'];
      $to_longitude = $to_addr['longitude'];
      $to_country_id = $to_addr['country_id'];
      $to_state_id = $to_addr['state_id'];
      $to_city_id = $to_addr['city_id'];
      // Format1: <Street>, <Zip code>, <City>, <State>, <Country>
      $to_addr_type1 = $to_addr['street_name'].', '.$to_addr['zipcode'].', '.$this->api->get_city_name_by_id($to_addr['city_id']).', '.$this->api->get_state_name_by_id($to_addr['state_id']).', '.$this->api->get_country_name_by_id($to_addr['country_id']); 
    } 
    else { $to_latitude = $to_longitude = 0; }

    $calculate_price =  array (
      "cust_id" => $this->cust_id,
      "category_id" => $category_id,
      "delivery_datetime" => $delivery_datetime,
      "pickup_datetime" => $pickup_datetime,
      "total_quantity" => $total_quantity,
      "width" => $total_width,
      "height" => $total_height,
      "length" => $total_length,
      "total_weight" => $total_weight,
      "unit_id" => 1,
      "from_country_id" => $from_country_id,
      "from_state_id" => $from_state_id,
      "from_city_id" => $from_city_id,
      "from_latitude" => $from_latitude,
      "from_longitude" => $from_longitude,
      "to_country_id" => $to_country_id,
      "to_state_id" => $to_state_id,
      "to_city_id" => $to_city_id,
      "to_latitude" => $to_latitude,
      "to_longitude" => $to_longitude,
      "service_area_type" => $service_area_type,
      "transport_type" => $transport_type,
      "handling_by" => $handling_by,
      "dedicated_vehicle" => $dedicated_vehicle,
      "package_value" => $package_value,
      "custom_clearance_by" => $custom_clearance_by,
      "old_new_goods" => $old_new_goods,
      "custom_package_value" => $custom_package_value,
      "loading_time" => $loading_time,
    );

    //echo json_encode($calculate_price); die();

    $prices = $this->api->calculate_order_price($calculate_price);
    //echo json_encode($prices); die();
    $order_price = $prices['total_price']; 
    $standard_price = $prices['standard_price']; 
    $handling_fee = $prices['handling_fee']; 
    $insurance_fee = $prices['ins_fee']; 
    $urgent_fee = $prices['urgent_fee']; 
    $vehicle_fee = $prices['dedicated_vehicle_fee']; 
    $advance_percent = $prices['advance_percent']; 
    $distance_in_km = $prices['distance_in_km']; 
    $commission_percent = $prices['commission_percent']; 

    $currency_id = $prices['currency_id']; 
    $currency_sign = $prices['currency_sign']; 
    $currency_title = $prices['currency_title'];

    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $order_price = (int)$order_price;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $standard_price = (int)$standard_price;
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $handling_fee = (int)$handling_fee; 
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $insurance_fee = (int)$insurance_fee; 
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $urgent_fee = (int)$urgent_fee; 
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $vehicle_fee = (int)$vehicle_fee; 
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $dvncPay = (int)$dvncPay; 
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $pckupPay = (int)$pckupPay; 
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $dlvrPay = (int)$dlvrPay; 
    if($currency_sign == 'XAF' || $currency_sign == 'XOF') $balance_amount = (int)$balance_amount;

    // New keys
    $old_goods_custom_commission_percent = $prices['old_goods_custom_commission_percent'];  
    $old_goods_custom_commission_fee = $prices['old_goods_custom_commission_fee'];  
    $new_goods_custom_commission_percent = $prices['new_goods_custom_commission_percent'];  
    $new_goods_custom_commission_fee = $prices['new_goods_custom_commission_fee'];  
    $loading_unloading_charges = $prices['loading_unloading_charge'];

    //echo json_encode($service_area_type);
    //echo json_encode($old_new_goods); die();
    if($service_area_type == "international" && ($old_new_goods == 'New')){  
      $custom_clearance_percent = $new_goods_custom_commission_percent;
      $custom_clearance_fee = $new_goods_custom_commission_fee;
    } 
    else if($service_area_type == "international" && ($old_new_goods == 'Old')){
      $custom_clearance_percent = $old_goods_custom_commission_percent;
      $custom_clearance_fee = $old_goods_custom_commission_fee;
    } else{ $custom_clearance_percent = 0;  $custom_clearance_fee = 0;  }

    //echo json_encode($prices); die();

    if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/booking-images/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('profile_image')) {   
        $uploads    = $this->upload->data();  
        $order_picture_url =  $config['upload_path'].$uploads["file_name"];  
      } else { $order_picture_url = "NULL"; }
    } else { $order_picture_url = "NULL"; }

    //Gonagoo Commission
    $user_details = $this->api->get_user_details($this->cust_id);
    //echo json_encode($user_details); die();
    if($user_details['is_dedicated'] == 1) {
      if($user_details['commission_type'] == 3) {
        $data = array(
          "cust_id" => $this->cust_id,
          "rate_type" => 1,
          "category_id" => $category_id,
          "country_id" => $from_country_id,
        );
        if($user_gonagoo_commission_details = $this->api->get_user_gonagoo_commission_details($data)) {
          if($transport_type == 'earth' && $service_area_type == 'local') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['ELGCP'];
          } else if ($transport_type == 'earth' && $service_area_type == 'national') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['ENGCP'];
          } else if ($transport_type == 'earth' && $service_area_type == 'international') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['EIGCP'];
          } else if ($transport_type == 'sea' && $service_area_type == 'local') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['SLGCP'];
          } else if ($transport_type == 'sea' && $service_area_type == 'national') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['SNGCP'];
          } else if ($transport_type == 'sea' && $service_area_type == 'international') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['SIGCP'];
          } else if ($transport_type == 'air' && $service_area_type == 'local') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['ALGCP'];
          } else if ($transport_type == 'air' && $service_area_type == 'national') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['ANGCP'];
          } else if ($transport_type == 'air' && $service_area_type == 'international') {
            $commission_percent = 0;
            $commission_amount = $user_gonagoo_commission_details['AIGCP'];
          } else {
            $commission_percent = $commission_percent;
            $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
          }
        } else {
          $commission_percent = $commission_percent;
          $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
        }
      } else if($user_details['commission_type'] == 2) {
        $data = array(
          "cust_id" => $this->cust_id,
          "rate_type" => 0,
          "category_id" => $category_id,
          "country_id" => $from_country_id,
        );
        if($user_gonagoo_commission_details = $this->api->get_user_gonagoo_commission_details($data)) {
          if($transport_type == 'earth' && $service_area_type == 'local') {
            $commission_percent = $user_gonagoo_commission_details['ELGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ELGCP'] / 100 ),2);
          } else if ($transport_type == 'earth' && $service_area_type == 'national') {
            $commission_percent = $user_gonagoo_commission_details['ENGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ENGCP'] / 100 ),2);
          } else if ($transport_type == 'earth' && $service_area_type == 'international') {
            $commission_percent = $user_gonagoo_commission_details['EIGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['EIGCP'] / 100 ),2);
          } else if ($transport_type == 'sea' && $service_area_type == 'local') {
            $commission_percent = $user_gonagoo_commission_details['SLGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SLGCP'] / 100 ),2);
          } else if ($transport_type == 'sea' && $service_area_type == 'national') {
            $commission_percent = $user_gonagoo_commission_details['SNGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SNGCP'] / 100 ),2);
          } else if ($transport_type == 'sea' && $service_area_type == 'international') {
            $commission_percent = $user_gonagoo_commission_details['SIGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['SIGCP'] / 100 ),2);
          } else if ($transport_type == 'air' && $service_area_type == 'local') {
            $commission_percent = $user_gonagoo_commission_details['ALGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ALGCP'] / 100 ),2);
          } else if ($transport_type == 'air' && $service_area_type == 'national') {
            $commission_percent = $user_gonagoo_commission_details['ANGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['ANGCP'] / 100 ),2);
          } else if ($transport_type == 'air' && $service_area_type == 'international') {
            $commission_percent = $user_gonagoo_commission_details['AIGCP'];
            $commission_amount = round(($order_price) * ( $user_gonagoo_commission_details['AIGCP'] / 100 ),2);
          } else {
            $commission_percent = $commission_percent;
            $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
          }
        } else {
          $commission_percent = $commission_percent;
          $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
        }
      } else {
        $commission_percent = $commission_percent;
        $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
      }
    } else {
      $commission_percent = $commission_percent;
      $commission_amount = round(($order_price) * ( $commission_percent / 100 ),2);
    }

    if($currency_sign == 'XAF' || 'XOF') $commission_amount = (int)$commission_amount;

    //echo $commission_amount; die();

    $insert_data = array(
      "cust_id" => $cust_id,
      "from_address" => $frm_addr_type1,
      "from_address_name" => $from_address_name,
      "from_address_contact" => $from_address_contact,
      "from_latitude" => $from_latitude,
      "from_longitude" => $from_longitude,
      "from_country_id" => $from_country_id,
      "from_state_id" => $from_state_id,
      "from_city_id" => $from_city_id,
      "from_addr_type" => (int)$from_addr_type,
      "from_relay_id" => (int)$from_relay_id,         
      "to_address" => $to_addr_type1,
      "to_address_name" => $to_address_name,
      "to_address_contact" => $to_address_contact,
      "to_latitude" => $to_latitude,
      "to_longitude" => $to_longitude,
      "to_country_id" => $to_country_id,
      "to_state_id" => $to_state_id,
      "to_city_id" => $to_city_id,                       
      "distance_in_km" => $distance_in_km,
      "pickup_datetime" => $pickup_datetime,
      "delivery_datetime" => $delivery_datetime,
      "vehical_type_id" => (int)$vehical_type_id,
      "order_type" => $order_type,      
      "order_picture_url" => $order_picture_url,
      "order_description" => nl2br($description),
      "order_price" => trim($order_price),
      "order_status" => "open",
      "advance_payment" => $dvncPay,
      "pickup_payment" => $pckupPay,
      "deliver_payment" => $dlvrPay,
      "cre_datetime" => date('Y-m-d H:i:s'),
      "cust_id" => (int) $cust_id,
      "currency_id" => $currency_id,
      "currency_sign" => $currency_sign,
      "currency_title" => $currency_title,
      "cust_name" => $cust_name,
      "insurance" => (int)$insurance_check,
      "package_value" => $package_value,
      "insurance_fee" => $insurance_fee,
      "handling_by" => (int)$handling_by,
      "dedicated_vehicle" => (int)$dedicated_vehicle,
      "with_driver" => (int)$with_driver,
      "ref_no" => $ref_no,
      "service_area_type" => $service_area_type,
      "expiry_date" => $expiry_date,
      "deliver_instructions" => $deliver_instructions,
      "pickup_instructions" => $pickup_instructions,
      "to_addr_type" => (int)$to_addr_type,
      "to_relay_id" => (int)$to_relay_id,
      "commission_percent" => $commission_percent,
      "commission_amount" => round((trim($standard_price)+trim($urgent_fee)+trim($handling_fee)+trim($vehicle_fee)) * ( $commission_percent / 100 ),2),
      "standard_price" => trim($standard_price),
      "urgent_fee" => trim($urgent_fee),
      "handling_fee" => trim($handling_fee),
      "vehicle_fee" => trim($vehicle_fee),      
      "order_contents" => "NULL",
      "total_quantity" => (int) $total_quantity,
      "width" => "NULL",
      "height" => "NULL",
      "length" => "NULL",
      "volume" => "NULL",
      "total_volume" => "NULL",
      "total_weight" => (int) $total_weight,
      "unit_id" => 1,
      "deliverer_id" => 0,
      "driver_id" => 0,
      "dimension_id" => 0,
      "status_update_datetime" => "NULL",
      "accept_datetime" => "NULL",
      "assigned_datetime" => "NULL",
      "driver_status_update" => "NULL",
      "delivery_code" => "NULL",
      "delivered_datetime" => "NULL",
      "review_comment" => "NULL",
      "rating" => "NULL",
      "review_datetime" => "NULL",
      "status_updated_by" => "NULL",
      "delivery_notes" => "NULL",
      "invoice_url" => "NULL",
      "paid_amount" => "0",
      "balance_amount" => $balance_amount,
      "payment_method" => "NULL",
      "transaction_id" => "NULL",
      "payment_datetime" => "NULL",
      "cd_name" => "NULL",
      "assigned_accept_datetime" => "NULL",
      "deliverer_company_name" => "NULL",
      "deliverer_contact_name" => "NULL",
      "deliverer_name" => "NULL",
      "driver_code" => "NULL",
      "from_relay_status" => "NULL",
      "from_relay_status_datetime" => "NULL",
      "to_relay_status" => "NULL",
      "to_relay_status_datetime" => "NULL",
      "complete_paid" => "0",
      "deliverer_invoice_url" => "NULL",
      "src_relay_commission_invoice_url" => "NULL",
      "dest_relay_commission_invoice_url" => "NULL",
      "transport_type" => (!empty($transport_type)?strtolower($transport_type):"NULL"),
      "custom_clearance_by" => $custom_clearance_by,
      "custom_package_value" => $custom_package_value,
      "old_new_goods" => $old_new_goods,
      "custom_clearance_fee" => $custom_clearance_fee,
      "custom_clearance_percent" => $custom_clearance_percent,
      "need_tailgate" => $need_tailgate,
      "category_id" => $category_id,
      "package_count" => count($package),
      "loading_unloading_charges" => $loading_unloading_charges,
      "from_address_info" => trim($from_full_address),
      "to_address_info" => trim($to_full_address),
      "payment_mode" => trim($payment_mode),
      "cod_payment_type" => trim($cod_payment_type),
      "pickup_code" => "NULL",
      "from_address_email" => trim($from_address_email),
      "to_address_email" => trim($to_address_email),
      "visible_by" => (int)trim($visible_by),
      "is_bank_payment" => (int)$is_bank_payment,
    );

    //echo json_encode($insert_data); die();

    if( $order_id = $this->api->register_new_courier_orders($insert_data) ) {
      
      foreach ($package as $p) {        
        $package_array = array(
          "cust_id" => $cust_id,
          "order_id" => $order_id,
          "quantity" => $p['quantity'],
          "dimension_id" => $p['dimension_id'],
          "width" => $p['width'],
          "height" => $p['height'],
          "length" => $p['length'],
          "total_weight" => $p['weight'],
          "unit_id" => $p['unit_id'],
          "contents" => $p['content'],
          "dangerous_goods_id" => $p['dangerous_goods_id'],
        );
        $this->transport->insert_package($package_array);
      }

      $this->api->insert_order_status($cust_id, $order_id, 'open');

      //promocode section--------------------------------------------
        if($_POST['total_price_with_promocode'] != "0"){
          $details = $this->api->get_order_detail($order_id);
          $promo_code_name = $this->input->post("hidden_promo_code");
          $promocode = $this->api->get_promocode_by_name($promo_code_name); 
          //code start here
            $discount_amount = round(($details['order_price']/100)*$promocode['discount_percent'] , 2); 
            if($details['currency_sign'] == 'XAF'){
              $discount_amount = round($discount_amount , 0);
            }
            $order_price =  round($details['order_price']-$discount_amount ,2);
            if($details['currency_sign'] == 'XAF'){
              $order_price = round($order_price , 0);
            }
            $advance_payment = round(($details['advance_payment']/100)*$promocode['discount_percent'] ,2);
            if($details['currency_sign'] == 'XAF'){
              $advance_payment = round($advance_payment , 0);
            }
            $pickup_payment = round( ($details['pickup_payment']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $pickup_payment = round($pickup_payment , 0);
            }
            $deliver_payment = round( ($details['deliver_payment']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $deliver_payment = round($deliver_payment , 0);
            }
            $insurance_fee = round( ($details['insurance_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $insurance_fee = round($insurance_fee , 0);
            }
            $commission_amount = round( ($details['commission_amount']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $commission_amount = round($commission_amount , 0);
            }

            $standard_price = round( ($details['standard_price']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $standard_price = round($standard_price , 0);
            }
            $urgent_fee = round( ($details['urgent_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $urgent_fee = round($urgent_fee , 0);
            }

            $handling_fee = round( ($details['handling_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $handling_fee = round($handling_fee , 0);
            }
            $vehicle_fee = round( ($details['vehicle_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $vehicle_fee = round($vehicle_fee , 0);
            }

            $custom_clearance_fee = round( ($details['custom_clearance_fee']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $custom_clearance_fee = round($custom_clearance_fee , 0);
            }

            $loading_unloading_charges = round( ($details['loading_unloading_charges']/100)*$promocode['discount_percent'],2);
            if($details['currency_sign'] == 'XAF'){
              $loading_unloading_charges = round($loading_unloading_charges , 0);
            }
            $courier_order = array(
              'promo_code' => $promocode['promo_code'],
              'promo_code_id' => $promocode['promo_code_id'],
              'promo_code_version' => $promocode['promo_code_version'],
              'discount_amount' => $discount_amount,
              'discount_percent' => $promocode['discount_percent'],
              'actual_order_price' => $details['order_price'],
              'order_price' => $order_price ,
              //'advance_payment' => ($details['advance_payment']-$advance_payment),
              //'pickup_payment' => ($details['pickup_payment']-$pickup_payment),
              //'deliver_payment' => ($details['deliver_payment']-$deliver_payment),
              'balance_amount' => $order_price,
              'insurance_fee' => ($details['insurance_fee']-$insurance_fee),
              'commission_amount' => ($details['commission_amount']-$commission_amount),
              'standard_price' => ($details['standard_price']-$standard_price),
              'urgent_fee' => ($details['urgent_fee']-$urgent_fee),
              'handling_fee' =>  ($details['handling_fee']-$handling_fee),
              'vehicle_fee' => ($details['vehicle_fee']-$vehicle_fee),
              'custom_clearance_fee'=>($details['custom_clearance_fee']-$custom_clearance_fee),
              'loading_unloading_charges'=>($details['loading_unloading_charges']-$loading_unloading_charges),
              'promo_code_datetime'=> date('Y-m-d h:i:s'),
            );
            $this->api->update_review($courier_order,$details['order_id']);
            $used_promo_update = array(
              'promo_code_id' => $promocode['promo_code_id'],
              'promo_code_version' => $promocode['promo_code_version'],
              'cust_id' => $this->cust_id,
              'customer_email' => "",
              'discount_percent' => $promocode['discount_percent'],
              'discount_amount' => $discount_amount,
              'discount_currency' => $details['currency_sign'],
              'cre_datetime' => date('Y-m-d h:i:s'),
              'service_id' => '7',
              'booking_id' => $details['order_id'],
            );
            $this->api->register_user_used_prmocode($used_promo_update);
            $this->session->set_flashdata('success_promo', $this->lang->line('Promocode Applied successfully'));
          //code end here
        }
      //promocode section--------------------------------------------

      //Create Pickup Code
      if($order_id <= 99999) { $pickup_code = 'PC'.$cust_id.sprintf("%06s", $order_id); } else { $pickup_code = 'PC'.$cust_id.$order_id; }
      $insert_data = array(
          "pickup_code" => $pickup_code,
      );
      $this->api->update_courier_orders($insert_data, $order_id);

      if(trim($payment_mode) == 'bank') {
        $this->session->set_flashdata('success', $this->lang->line('order_created_successfully'));
        redirect('user-panel/my-bookings');
      }
      else if(trim($payment_mode) == 'cod') {
        $this->session->set_flashdata('success', $this->lang->line('order_created_successfully'));
        redirect('user-panel/my-bookings');
      } else {
        $this->session->set_flashdata('success', ['msg' => $this->lang->line('order_created_successfully'), 'courier_order_id' => $order_id]);  
        redirect('user-panel/courier-payment');
      }
    } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); redirect('transport/add-bookings');   }  
  }
  
  public function get_unit_detail()
  {
    $country_name = $this->input->post('country_name');
    echo json_encode($this->api->get_unit_detail($country_name));
  }

  public function get_address_by_id()
  {
    $addr_id = $this->input->post('addr_id');
    $address_type = $this->input->post('address_type');
    $cust = $this->cust; 
    $cust_id = (int) $cust['cust_id'];
    echo json_encode($this->api->get_address_by_id($addr_id, $address_type, $cust_id));
  }
  

}

/* End of file Transport.php */
/* Location: ./application/controllers/Transport.php */