<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_orders extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin');
      $this->load->model('Admin_orders_model', 'orders');
      $this->load->model('Notification_model', 'notice');
      $this->load->model('Transport_model', 'transport');
      $this->load->model('Web_user_model', 'user');
      $this->load->model('Api_model_laundry', 'laundry');
      $this->load->model('Api_model_bus', 'bus');
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }
  
  public function index()
  {
    if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
  }
  
  public function get_all_orders()
  {   
    $all_orders = $this->orders->get_orders();
    $this->load->view('admin/users_order_list_view', compact('all_orders')); 
    $this->load->view('admin/footer_view'); 
  }

  public function view_order_details()
  { 
    $order_id = (int) $this->uri->segment('3');
    $order_details = $this->orders->get_order_details($order_id);
    $package_details = $this->orders->get_order_package_details($order_id);
    $cust_details = $this->orders->get_customer_details($order_details['cust_id']);
    $order_country = $this->orders->get_country_details($order_details['from_country_id']);
    $order_state = $this->orders->get_state_details($order_details['from_state_id']);
    $order_city = $this->orders->get_city_details($order_details['from_city_id']);
    $vehicle_details = $this->orders->get_vehicle_details($order_details['vehical_type_id']);
    $unit_details = $this->orders->get_unit_details($order_details['unit_id']);
    $requested_deliverers = $this->orders->get_requested_deliverers_details($order_details['order_id']);
    $rejected_deliverers = $this->orders->get_rejected_deliverers_details($order_details['order_id']);

    if($order_details['deliverer_id'] != "NULL" || $order_details['deliverer_id'] != 0) {
      $deliverer_details = $this->orders->get_customer_details($order_details['deliverer_id']);
    }
    if($order_details['driver_id'] != "NULL" || $order_details['driver_id'] != 0) {
      $driver_details = $this->orders->get_driver_details($order_details['driver_id']);
    }
    
    $this->load->view('admin/admin_order_details_view' , compact('order_details','cust_details','deliverer_details','driver_details','order_country','order_state','order_city','vehicle_details','unit_details','requested_deliverers','rejected_deliverers','package_details')); //admin_laundry_details_view//admin_order_details_view
    $this->load->view('admin/footer_view');
  }

  public function view_laundry_details()
  { 
    //echo json_encode($booking_details_cart); die();
    $booking_id = (int) $this->uri->segment('3');
    $booking_details = $this->admin->laundry_booking_details($booking_id);
    $booking_details_cart = $this->admin->laundry_booking_details_cart($booking_id);
    $laundry_provider = $this->admin->get_laundry_provider_profile($booking_details['provider_id']);
    $laundry_cust = $this->orders->get_customer_details($booking_details['cust_id']);
    //echo json_encode($laundry_cust); die();
    $this->load->view('admin/admin_laundry_details_view', compact('booking_details','booking_details_cart','laundry_cust','laundry_provider'));
    $this->load->view('admin/footer_view');
  }

  public function view_bus_details()
  { 
    //echo json_encode($booking_details_cart); die();
    $ticket_id = (int) $this->uri->segment('3');
    $booking_details = $this->admin->get_trip_booking_details($ticket_id);
    $booking_details_seat_details = $this->admin->get_ticket_seat_details($ticket_id);
    $bus_operator_profile = $this->admin->get_bus_operator_profile($booking_details['operator_id']);
    $customer = $this->orders->get_customer_details($booking_details['cust_id']);
    //echo json_encode($ticket_id); die();   
    $this->load->view('admin/admin_bus_details_view', compact('booking_details','booking_details_seat_details','bus_operator_profile','customer'));
    $this->load->view('admin/footer_view');
  }

  public function verify_it()
  {
    $doc_id = $this->input->post('id', TRUE);
    if( $this->orders->verify_document( (int) $doc_id) ) {  echo 'success'; } else {  echo 'failed';  }
  }

  public function reject_it()
  {
    $doc_id = $this->input->post('id', TRUE);
    if( $this->orders->reject_document( (int) $doc_id) ) {  echo 'success'; } else {  echo 'failed';  }
  }
  
  public function remove_open_order_view()
  {
    $order_id = $this->uri->segment(3);
    $details = $this->orders->get_order_details($order_id);
    if($details['order_status'] == 'open') {
      $this->load->view('admin/remove_open_order_view.php', compact('order_id')); 
      $this->load->view('admin/footer_view');       
    }
    else{ $this->session->set_flashdata('error', 'Order not in marketplace!'); redirect(base_url('admin/get-all-orders'));  }   
  }

  public function make_open_order_remove()
  {
    $reason = $this->input->post('reason');
    $service_type = $this->input->post('service_type');
    $order_id = $this->input->post('order_id');
    $details = $this->orders->get_order_details($order_id);
    if($details['order_status'] == 'open') {
      if($this->orders->inactivate_order($order_id)){
        $update = [
          "order_id" => $order_id,
          "admin_id" => $this->session->userdata('userid'),
          "reason" => $reason,
          "service_type" => $service_type,
          "cre_datetime" => date('Y-m-d H:i:s'),
        ];

        $this->orders->insert_order_inactive_reason($update);
        $this->session->set_flashdata('success', 'Order successfully removed from marketplace!');
      }
    }
    else{ $this->session->set_flashdata('error', 'Order not in marketplace!');  }   
    redirect(base_url('admin/get-all-orders'));
  }

  public function get_all_laundry_orders()
  {   
    $all_orders = $this->orders->get_laundry_orders();
    //echo json_encode($all_orders); die();
    $this->load->view('admin/users_laundry_order_list_view', compact('all_orders')); 
    $this->load->view('admin/footer_view'); 
  }
  public function view_laundry_order_details()
  { 
    $order_id = (int) $this->uri->segment('3');
    $booking_details = $this->laundry->laundry_booking_details($order_id);
    $booking_details_cart = $this->laundry->laundry_booking_details_list($order_id);
    $laundry_provider = $this->admin->get_laundry_provider_profile($booking_details['provider_id']);
    $laundry_cust = $this->orders->get_customer_details($booking_details['cust_id']);
    //echo json_encode($laundry_cust); die();

    $this->load->view('admin/admin_laundry_order_details_view', compact('booking_details','booking_details_cart','laundry_cust','laundry_provider'));
    $this->load->view('admin/footer_view');
  }

  public function get_all_ticket_orders()
  {   
    $all_orders = $this->bus->get_all_trip_master_list();
    //echo json_encode($all_orders); die();
    $this->load->view('admin/users_bus_order_list_view', compact('all_orders')); 
    $this->load->view('admin/footer_view'); 
  }
  public function view_ticket_order_details()
  { 
    $ticket_id = (int) $this->uri->segment('3');
    $booking_details = $this->bus->get_trip_booking_details($ticket_id);
    $booking_passengar = $this->bus->get_ticket_seat_details($ticket_id);
    $laundry_provider = $this->admin->get_laundry_provider_profile($booking_details['operator_id']);
    $laundry_cust = $this->orders->get_customer_details($booking_details['cust_id']);
    //echo json_encode($booking_passengar); die();

    $this->load->view('admin/admin_ticket_order_details_view', compact('booking_details','booking_passengar','laundry_cust','laundry_provider'));
    $this->load->view('admin/footer_view');
  }
}

/* End of file Admin_orders.php */
/* Location: ./application/controllers/Admin_orders.php */