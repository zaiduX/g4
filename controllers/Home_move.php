<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_move extends CI_Controller {

	private $cust = array();

	public function __construct()
	{
		parent::__construct();

		$this->cust_id = $this->session->userdata("cust_id"); $this->cust_id = (int) $this->cust_id;
      
    $this->load->model('api_model', 'api');
    $this->load->model('Notification_model', 'notification');
    $this->load->model('Web_user_model', 'user');
  
    $this->cust = $this->api->get_consumer_datails($this->cust_id, true); $cust = $this->cust;
    
    if( ( $this->session->userdata("is_logged_in") == "NULL" ) OR ( $this->session->userdata("is_logged_in") !== 1 ) ) { redirect('log-in'); }

    if($cust['mobile_verified'] == 0) { redirect('verification/otp'); }

    $lang = $this->session->userdata('language');
    $this->config->set_item('language', strtolower($lang));

    if(trim($lang) == 'french'){
      $this->lang->load('frenchApi_lang','french');
      $this->lang->load('frenchFront_lang','french');
    } 
    else if(trim($lang) == 'spanish'){  
      $this->lang->load('spanishApi_lang','spanish');
      $this->lang->load('spanishFront_lang','spanish');
    }
    else { 
      $this->lang->load('englishApi_lang','english');
      $this->lang->load('englishFront_lang','english');
    }
      
    $this->api->update_last_login($cust['cust_id']);
   	if (!$this->input->is_ajax_request()){ 
      // var_dump($_POST); 
      $order_id = $this->input->post('order_id');
      $type = $this->input->post('type');

      // if($order_id && !$ow_id) { 
      if($order_id && ($type =="workroom")) { 
        $ids = $this->api->get_total_unread_order_workroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) {  $this->api->make_workroom_notification_read($v['ow_id'], $cust['cust_id']);  }
      }

      if($order_id && ($type =="chatroom")) { 
        $ids = $this->api->get_total_unread_order_chatroom_notifications($order_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) { $this->api->make_chatrooom_notification_read($v['chat_id'], $cust['cust_id']);  }
      }
      if($cust['user_type'] == 1 ){ $user_type ="Individuals"; } else { $user_type ="Business"; }
      $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust['cust_id'],$user_type );

      //echo $this->db->last_query(); die();
      $notification = $this->api->get_total_unread_notifications($cust['cust_id'],$user_type, 5);
      $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust['cust_id']);      
      $nt_workroom = $this->api->get_total_unread_workroom_notifications($cust['cust_id'], 5);  
      $total_chatroom_notifications = $this->api->get_total_unread_chatroom_notifications_count($cust['cust_id']);      
      $nt_chatroom = $this->api->get_total_unread_chatroom_notifications($cust['cust_id'], 5);  

      $compact = ["total_unread_notifications","notification","total_workroom_notifications","nt_workroom","total_chatroom_notifications","nt_chatroom"];
      $this->load->view('user_panel/header',compact($compact)); 
      $this->load->view('user_panel/menu', compact("cust"));
    }
	}

	public function index()
	{
		exit('No direct script access allowed');
	}

	public function my_bookings()
  { 
    // echo json_encode($_POST); die();
    $countries = $this->user->get_countries();
    $categories = $this->api->get_category();
    $dimensions = $this->api->get_standard_dimension_masters($this->cust_id);   
    //Filters
    $from_country_id = $this->input->post('country_id_src');
    $from_state_id = $this->input->post('state_id_src');
    $from_city_id = $this->input->post('city_id_src');
    $to_country_id = $this->input->post('country_id_dest');
    $to_state_id = $this->input->post('state_id_dest');
    $to_city_id = $this->input->post('city_id_dest');
    $from_address = $this->input->post('from_address');
    $to_address = $this->input->post('to_address');   
    $delivery_start = $this->input->post('delivery_start');
    $delivery_end = $this->input->post('delivery_end');
    $pickup_start = $this->input->post('pickup_start');
    $pickup_end = $this->input->post('pickup_end');
    $price = $this->input->post('price');
    $dimension_id = $this->input->post('dimension_id');
    $order_type = $this->input->post('order_type');

    //  New params in filter
    $max_weight = $this->input->post('max_weight');
    $unit_id = $this->input->post('c_unit_id');
    $creation_start = $this->input->post('creation_start');
    $creation_end = $this->input->post('creation_end');   
    $expiry_start = $this->input->post('expiry_start');
    $expiry_end = $this->input->post('expiry_end');
    $distance_start = $this->input->post('distance_start');
    $distance_end = $this->input->post('distance_end');
    $transport_type = $this->input->post('transport_type');
    $vehicle_id = $this->input->post('vehicle');

    $unit = $this->api->get_unit_master(true);
    $earth_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $air_trans = $this->user->get_transport_vehicle_list_by_type('air');
    $sea_trans = $this->user->get_transport_vehicle_list_by_type('sea');

    if( ($from_country_id > 0) || !empty($from_state_id) || !empty($from_city_id) || ($to_country_id > 0) || !empty($to_state_id) || !empty($to_city_id) || !empty($from_address) || !empty($to_address) || !empty($delivery_start) || !empty($delivery_end) || !empty($pickup_start) || !empty($pickup_end) || !empty($price) || !empty($dimension_id) || !empty($order_type) || !empty($max_weight) || !empty($unit_id) || !empty($creation_start) || !empty($creation_end) || !empty($expiry_start) || !empty($expiry_end) || !empty($distance_start) || !empty($distance_end) || !empty($vehicle_id) || !empty($transport_type)) {

      if(trim($creation_start) != "" && trim($creation_end) != "") {
        $creation_start_date = date('Y-m-d H:i:s', strtotime($creation_start));
        $creation_end_date = date('Y-m-d H:i:s', strtotime($creation_end));
      } else { $creation_start_date = $creation_end_date = null; }
      
      if(trim($pickup_start) != "" && trim($pickup_end) != "") {
        $pickup_start_date = date('Y-m-d H:i:s', strtotime($pickup_start));
        $pickup_end_date = date('Y-m-d H:i:s', strtotime($pickup_end));
      } else { $pickup_start_date = $pickup_end_date = null; }

      if(trim($delivery_start) != "" && trim($delivery_end) != "") {
        $delivery_start_date = date('Y-m-d H:i:s', strtotime($delivery_start));
        $delivery_end_date = date('Y-m-d H:i:s', strtotime($delivery_end));
      } else { $delivery_start_date = $delivery_end_date = null; }

      if(trim($expiry_start) != "" && trim($expiry_end) != "") {
        $expiry_start_date = date('Y-m-d H:i:s', strtotime($expiry_start));
        $expiry_end_date = date('Y-m-d H:i:s', strtotime($expiry_end));
      } else { $expiry_start_date = $expiry_end_date = null; }

      $filter_array = array(
        "user_id" => $this->cust_id,
        "user_type" => "cust",
        "order_status" => 'open',
        "from_country_id" => ($from_country_id!=null)? $from_country_id: "0",
        "from_state_id" => ($from_state_id!=null)? $from_state_id: "0",
        "from_city_id" => ($from_city_id!=null)? $from_city_id: "0",
        "to_country_id" => ($to_country_id!=null)? $to_country_id: "0",
        "to_state_id" => ($to_state_id!=null)? $to_state_id: "0",
        "to_city_id" => ($to_city_id!=null)? $to_city_id: "0",
        "from_address" => ($from_address!=null)? $from_address: "NULL",
        "to_address" => ($to_address!=null)? $to_address: "NULL",
        "delivery_start_date" => ($delivery_start_date!=null)? $delivery_start_date: "NULL",
        "delivery_end_date" => ($delivery_end_date!=null)? $delivery_end_date: "NULL",
        "pickup_start_date" => ($pickup_start_date!=null)? $$pickup_start_date: "NULL",
        "pickup_end_date" => ($pickup_end_date!=null)? $pickup_end_date: "NULL",
        "price" => ($price!=null)?$price:"0",
        "dimension_id" => ($dimension_id!=null)?$dimension_id:"0",
        "order_type" => ($order_type!=null)? $order_type: "NULL",
        // new filters
        "creation_start_date" => ($creation_start_date!=null)? $creation_start_date: "NULL",
        "creation_end_date" => ($creation_end_date!=null)? $creation_end_date: "NULL",
        "expiry_start_date" => ($expiry_start_date!=null)? $expiry_start_date: "NULL",
        "expiry_end_date" => ($expiry_end_date!=null)? $expiry_end_date: "NULL",
        "distance_start" => ($distance_start!=null)? $distance_start: "0",
        "distance_end" => ($distance_end!=null)? $distance_end: "0",
        "transport_type" => ($transport_type!=null)? $transport_type: "NULL",
        "vehicle_id" => ($vehicle_id!=null)? $vehicle_id: "0",
        "max_weight" => ($max_weight!=null)?$max_weight:"0",
        "unit_id" => ($unit_id != null)?$unit_id:"0",
      );
      // echo json_encode($filter_array); die();
      $orders = $this->user->filtered_list($filter_array);
      //echo $this->db->last_query(); die();
      $filter = 'advance';
      $this->load->view('user_panel/home_move_my_bookings_list_view', compact('orders','countries','categories','dimensions','filter','from_country_id','from_state_id','from_city_id','to_country_id','to_state_id','to_city_id','from_address','to_address','delivery_start','delivery_end','pickup_start','pickup_end','price','dimension_id','order_type','unit','max_weight','unit_id','creation_start','creation_end','expiry_start','expiry_end','distance_start','distance_end','vehicle_id','transport_type','earth_trans','air_trans','sea_trans'));
    } else {
      $orders = $this->user->get_booking_list($this->cust_id,'open');
      // echo json_encode($orders); die();
      $filter = 'basic';
      $this->load->view('user_panel/home_move_my_bookings_list_view', compact('orders','countries','categories','dimensions','filter','unit','earth_trans','air_trans','sea_trans'));
    }   
    $this->load->view('user_panel/footer');
  }

  // Create new booking view
  public function add_bookings()
  {
exit('Under Development'); die();
    $category_id = $this->input->post('category_id');
    $countries = $this->user->get_countries();
    $default_trans = $this->user->get_transport_vehicle_list_by_type('earth');
    $address = $this->api->get_consumers_addressbook($this->cust['cust_id'],array());
    $relay = $this->user->get_relay_points();
    // $transport = $this->api->get_transport_master(true);
    $dimension = $this->api->get_standard_dimension_masters();
    $unit = $this->api->get_unit_master(true);
    $this->load->view('user_panel/home_move_add_new_booking_view',compact('address','transport','dimension','unit','relay','default_trans','countries','category_id'));
    $this->load->view('user_panel/footer');
  }

}

/* End of file Home_move.php */
/* Location: ./application/controllers/Home_move.php */