<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_permissions extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Group_permissions_model', 'group_per');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function group_perm()
	{
		$group_perm = $this->group_per->get_group_permission();
		$this->load->view('admin/group_permission_list_view', compact('group_perm'));			
		$this->load->view('admin/footer_view');		
	}

	public function add_group_permission()
	{
		$this->load->view('admin/group_permission_add_view');
		$this->load->view('admin/footer_view');
	}

	public function register_group_permission()
	{
		$auth_type = $this->input->post('auth_type', TRUE);
		//category_type permission
		if($this->input->post('category_type_view', TRUE) == '') { $category_type_view = 0; } else { $category_type_view = $this->input->post('category_type_view', TRUE); }
		if($this->input->post('category_type_add', TRUE) == '') { $category_type_add = 0; } else { $category_type_add = $this->input->post('category_type_add', TRUE); }
		if($this->input->post('category_type_edit', TRUE) == '') { $category_type_edit = 0; } else { $category_type_edit = $this->input->post('category_type_edit', TRUE); }
		if($this->input->post('category_type_active', TRUE) == '') { $category_type_active = 0; } else { $category_type_active = $this->input->post('category_type_active', TRUE); }
		$category_type = $category_type_view.','.$category_type_add.','.$category_type_edit.','.$category_type_active;

		//category permission
		if($this->input->post('category_view', TRUE) == '') { $category_view = 0; } else { $category_view = $this->input->post('category_view', TRUE); }
		if($this->input->post('category_add', TRUE) == '') { $category_add = 0; } else { $category_add = $this->input->post('category_add', TRUE); }
		if($this->input->post('category_edit', TRUE) == '') { $category_edit = 0; } else { $category_edit = $this->input->post('category_edit', TRUE); }
		if($this->input->post('category_active', TRUE) == '') { $category_active = 0; } else { $category_active = $this->input->post('category_active', TRUE); }
		$categories = $category_view.','.$category_add.','.$category_edit.','.$category_active;

		//currency master permission
		if($this->input->post('currency_master_view', TRUE) == '') { $currency_master_view = 0; } else { $currency_master_view = $this->input->post('currency_master_view', TRUE); }
		if($this->input->post('currency_master_add', TRUE) == '') { $currency_master_add = 0; } else { $currency_master_add = $this->input->post('currency_master_add', TRUE); }
		if($this->input->post('currency_master_edit', TRUE) == '') { $currency_master_edit = 0; } else { $currency_master_edit = $this->input->post('currency_master_edit', TRUE); }
		if($this->input->post('currency_master_active', TRUE) == '') { $currency_master_active = 0; } else { $currency_master_active = $this->input->post('currency_master_active', TRUE); }
		if($this->input->post('currency_master_delete', TRUE) == '') { $currency_master_delete = 0; } else { $currency_master_delete = $this->input->post('currency_master_delete', TRUE); }
		$currency_master = $currency_master_view.','.$currency_master_add.','.$currency_master_edit.','.$currency_master_active.','.$currency_master_delete;

		//country currency master permission
		if($this->input->post('country_currency_view', TRUE) == '') { $country_currency_view = 0; } else { $country_currency_view = $this->input->post('country_currency_view', TRUE); }
		if($this->input->post('country_currency_add', TRUE) == '') { $country_currency_add = 0; } else { $country_currency_add = $this->input->post('country_currency_add', TRUE); }
		if($this->input->post('country_currency_edit', TRUE) == '') { $country_currency_edit = 0; } else { $country_currency_edit = $this->input->post('country_currency_edit', TRUE); }
		if($this->input->post('country_currency_active', TRUE) == '') { $country_currency_active = 0; } else { $country_currency_active = $this->input->post('country_currency_active', TRUE); }
		$country_currency = $country_currency_view.','.$country_currency_add.','.$country_currency_edit.','.$country_currency_active;

		//Authority type permission
		if($this->input->post('authority_type_view', TRUE) == '') { $authority_type_view = 0; } else { $authority_type_view = $this->input->post('authority_type_view', TRUE); }
		if($this->input->post('authority_type_add', TRUE) == '') { $authority_type_add = 0; } else { $authority_type_add = $this->input->post('authority_type_add', TRUE); }
		if($this->input->post('authority_type_edit', TRUE) == '') { $authority_type_edit = 0; } else { $authority_type_edit = $this->input->post('authority_type_edit', TRUE); }
		if($this->input->post('authority_type_active', TRUE) == '') { $authority_type_active = 0; } else { $authority_type_active = $this->input->post('authority_type_active', TRUE); }
		$authority_type = $authority_type_view.','.$authority_type_add.','.$authority_type_edit.','.$authority_type_active;

		//Authority permission
		if($this->input->post('manage_authority_view', TRUE) == '') { $manage_authority_view = 0; } else { $manage_authority_view = $this->input->post('manage_authority_view', TRUE); }
		if($this->input->post('manage_authority_add', TRUE) == '') { $manage_authority_add = 0; } else { $manage_authority_add = $this->input->post('manage_authority_add', TRUE); }
		if($this->input->post('manage_authority_edit', TRUE) == '') { $manage_authority_edit = 0; } else { $manage_authority_edit = $this->input->post('manage_authority_edit', TRUE); }
		if($this->input->post('manage_authority_active', TRUE) == '') { $manage_authority_active = 0; } else { $manage_authority_active = $this->input->post('manage_authority_active', TRUE); }
		$manage_authority = $manage_authority_view.','.$manage_authority_add.','.$manage_authority_edit.','.$manage_authority_active;

		//Users permission
		if($this->input->post('manage_users_view', TRUE) == '') { $manage_users_view = 0; } else { $manage_users_view = $this->input->post('manage_users_view', TRUE); }
		if($this->input->post('manage_users_add', TRUE) == '') { $manage_users_add = 0; } else { $manage_users_add = $this->input->post('manage_users_add', TRUE); }
		if($this->input->post('manage_users_edit', TRUE) == '') { $manage_users_edit = 0; } else { $manage_users_edit = $this->input->post('manage_users_edit', TRUE); }
		if($this->input->post('manage_users_active', TRUE) == '') { $manage_users_active = 0; } else { $manage_users_active = $this->input->post('manage_users_active', TRUE); }
		$manage_users = $manage_users_view.','.$manage_users_add.','.$manage_users_edit.','.$manage_users_active;

		//Notification
		if($this->input->post('manage_notifications_view', TRUE) == '') { $manage_notifications_view = 0; } else { $manage_notifications_view = $this->input->post('manage_notifications_view', TRUE); }
		if($this->input->post('manage_notifications_add', TRUE) == '') { $manage_notifications_add = 0; } else { $manage_notifications_add = $this->input->post('manage_notifications_add', TRUE); }
		$manage_notifications = $manage_notifications_view.','.$manage_notifications_add;

		//Promo Code
		if($this->input->post('manage_promo_view', TRUE) == '') { $manage_promo_view = 0; } else { $manage_promo_view = $this->input->post('manage_promo_view', TRUE); }
		if($this->input->post('manage_promo_add', TRUE) == '') { $manage_promo_add = 0; } else { $manage_promo_add = $this->input->post('manage_promo_add', TRUE); }
		if($this->input->post('manage_promo_edit', TRUE) == '') { $manage_promo_edit = 0; } else { $manage_promo_edit = $this->input->post('manage_promo_edit', TRUE); }
		if($this->input->post('manage_promo_active', TRUE) == '') { $manage_promo_active = 0; } else { $manage_promo_active = $this->input->post('manage_promo_active', TRUE); }
		$manage_promo = $manage_promo_view.','.$manage_promo_add.','.$manage_promo_edit.','.$manage_promo_active;

		//Promo Code Service
		if($this->input->post('manage_promo_service_view', TRUE) == '') { $manage_promo_service_view = 0; } else { $manage_promo_service_view = $this->input->post('manage_promo_service_view', TRUE); }
		$manage_promo_service = $manage_promo_service_view;

		//Standard Dimension 
		if($this->input->post('manage_dimension_view', TRUE) == '') { $manage_dimension_view = 0; } else { $manage_dimension_view = $this->input->post('manage_dimension_view', TRUE); }
		if($this->input->post('manage_dimension_add', TRUE) == '') { $manage_dimension_add = 0; } else { $manage_dimension_add = $this->input->post('manage_dimension_add', TRUE); }
		if($this->input->post('manage_dimension_edit', TRUE) == '') { $manage_dimension_edit = 0; } else { $manage_dimension_edit = $this->input->post('manage_dimension_edit', TRUE); }
		if($this->input->post('manage_dimension_delete', TRUE) == '') { $manage_dimension_delete = 0; } else { $manage_dimension_delete = $this->input->post('manage_dimension_delete', TRUE); }
		$manage_dimension = $manage_dimension_view.','.$manage_dimension_add.','.$manage_dimension_edit.','.$manage_dimension_delete;

		//Vehicles
		if($this->input->post('manage_vehicals_view', TRUE) == '') { $manage_vehicals_view = 0; } else { $manage_vehicals_view = $this->input->post('manage_vehicals_view', TRUE); }
		if($this->input->post('manage_vehicals_add', TRUE) == '') { $manage_vehicals_add = 0; } else { $manage_vehicals_add = $this->input->post('manage_vehicals_add', TRUE); }
		if($this->input->post('manage_vehicals_edit', TRUE) == '') { $manage_vehicals_edit = 0; } else { $manage_vehicals_edit = $this->input->post('manage_vehicals_edit', TRUE); }
		if($this->input->post('manage_vehicals_delete', TRUE) == '') { $manage_vehicals_delete = 0; } else { $manage_vehicals_delete = $this->input->post('manage_vehicals_delete', TRUE); }
		$manage_vehicals = $manage_vehicals_view.','.$manage_vehicals_add.','.$manage_vehicals_edit.','.$manage_vehicals_delete;

		//Standard Rates
		if($this->input->post('manage_rates_view', TRUE) == '') { $manage_rates_view = 0; } else { $manage_rates_view = $this->input->post('manage_rates_view', TRUE); }
		if($this->input->post('manage_rates_add', TRUE) == '') { $manage_rates_add = 0; } else { $manage_rates_add = $this->input->post('manage_rates_add', TRUE); }
		if($this->input->post('manage_rates_edit', TRUE) == '') { $manage_rates_edit = 0; } else { $manage_rates_edit = $this->input->post('manage_rates_edit', TRUE); }
		if($this->input->post('manage_rates_delete', TRUE) == '') { $manage_rates_delete = 0; } else { $manage_rates_delete = $this->input->post('manage_rates_delete', TRUE); }
		$manage_rates = $manage_rates_view.','.$manage_rates_add.','.$manage_rates_edit.','.$manage_rates_delete;

		//Driver Category
		if($this->input->post('manage_driver_category_view', TRUE) == '') { $manage_driver_category_view = 0; } else { $manage_driver_category_view = $this->input->post('manage_driver_category_view', TRUE); }
		if($this->input->post('manage_driver_category_add', TRUE) == '') { $manage_driver_category_add = 0; } else { $manage_driver_category_add = $this->input->post('manage_driver_category_add', TRUE); }
		if($this->input->post('manage_driver_category_edit', TRUE) == '') { $manage_driver_category_edit = 0; } else { $manage_driver_category_edit = $this->input->post('manage_driver_category_edit', TRUE); }
		if($this->input->post('manage_driver_category_delete', TRUE) == '') { $manage_driver_category_delete = 0; } else { $manage_driver_category_delete = $this->input->post('manage_driver_category_delete', TRUE); }
		$manage_driver_category = $manage_driver_category_view.','.$manage_driver_category_add.','.$manage_driver_category_edit.','.$manage_driver_category_delete;

		//Advance Payment
		if($this->input->post('manage_advance_payment_view', TRUE) == '') { $manage_advance_payment_view = 0; } else { $manage_advance_payment_view = $this->input->post('manage_advance_payment_view', TRUE); }
		if($this->input->post('manage_advance_payment_add', TRUE) == '') { $manage_advance_payment_add = 0; } else { $manage_advance_payment_add = $this->input->post('manage_advance_payment_add', TRUE); }
		if($this->input->post('manage_advance_payment_edit', TRUE) == '') { $manage_advance_payment_edit = 0; } else { $manage_advance_payment_edit = $this->input->post('manage_advance_payment_edit', TRUE); }
		if($this->input->post('manage_advance_payment_delete', TRUE) == '') { $manage_advance_payment_delete = 0; } else { $manage_advance_payment_delete = $this->input->post('manage_advance_payment_delete', TRUE); }
		$manage_advance_payment = $manage_advance_payment_view.','.$manage_advance_payment_add.','.$manage_advance_payment_edit.','.$manage_advance_payment_delete;

		//Skill permission 
		if($this->input->post('manage_skills_master_view', TRUE) == '') { $manage_skills_master_view = 0; } else { $manage_skills_master_view = $this->input->post('manage_skills_master_view', TRUE); }
		if($this->input->post('manage_skills_master_add', TRUE) == '') { $manage_skills_master_add = 0; } else { $manage_skills_master_add = $this->input->post('manage_skills_master_add', TRUE); }
		if($this->input->post('manage_skills_master_edit', TRUE) == '') { $manage_skills_master_edit = 0; } else { $manage_skills_master_edit = $this->input->post('manage_skills_master_edit', TRUE); }
		if($this->input->post('manage_skills_master_active', TRUE) == '') { $manage_skills_master_active = 0; } else { $manage_skills_master_active = $this->input->post('manage_skills_master_active', TRUE); }
		$manage_skills_master = $manage_skills_master_view.','.$manage_skills_master_add.','.$manage_skills_master_edit.','.$manage_skills_master_active;

		//Insurance permission 
		if($this->input->post('manage_insurance_master_view', TRUE) == '') { $manage_insurance_master_view = 0; } else { $manage_insurance_master_view = $this->input->post('manage_insurance_master_view', TRUE); }
		if($this->input->post('manage_insurance_master_add', TRUE) == '') { $manage_insurance_master_add = 0; } else { $manage_insurance_master_add = $this->input->post('manage_insurance_master_add', TRUE); }
		if($this->input->post('manage_insurance_master_edit', TRUE) == '') { $manage_insurance_master_edit = 0; } else { $manage_insurance_master_edit = $this->input->post('manage_insurance_master_edit', TRUE); }
		if($this->input->post('manage_insurance_master_active', TRUE) == '') { $manage_insurance_master_active = 0; } else { $manage_insurance_master_active = $this->input->post('manage_insurance_master_active', TRUE); }
		$manage_insurance_master = $manage_insurance_master_view.','.$manage_insurance_master_add.','.$manage_insurance_master_edit.','.$manage_insurance_master_active;

		//Relay Point permission 
		if($this->input->post('manage_relay_point_view', TRUE) == '') { $manage_relay_point_view = 0; } else { $manage_relay_point_view = $this->input->post('manage_relay_point_view', TRUE); }
		if($this->input->post('manage_relay_point_add', TRUE) == '') { $manage_relay_point_add = 0; } else { $manage_relay_point_add = $this->input->post('manage_relay_point_add', TRUE); }
		if($this->input->post('manage_relay_point_edit', TRUE) == '') { $manage_relay_point_edit = 0; } else { $manage_relay_point_edit = $this->input->post('manage_relay_point_edit', TRUE); }
		if($this->input->post('manage_relay_point_active', TRUE) == '') { $manage_relay_point_active = 0; } else { $manage_relay_point_active = $this->input->post('manage_relay_point_active', TRUE); }
		$manage_relay_point = $manage_relay_point_view.','.$manage_relay_point_add.','.$manage_relay_point_edit.','.$manage_relay_point_active;

		//Withdraw request permission 
		if($this->input->post('manage_withdraw_request_view', TRUE) == '') { $manage_withdraw_request_view = 0; } else { $manage_withdraw_request_view = $this->input->post('manage_withdraw_request_view', TRUE); }
		if($this->input->post('manage_withdraw_request_active', TRUE) == '') { $manage_withdraw_request_active = 0; } else { $manage_withdraw_request_active = $this->input->post('manage_withdraw_request_active', TRUE); }
		$manage_withdraw_request = $manage_withdraw_request_view.','.$manage_withdraw_request_active;
		
		//Commission refund request permission 
		if($this->input->post('manage_commission_refund_request_view', TRUE) == '') { $manage_commision_refund_view = 0; } else { $manage_commision_refund_view = $this->input->post('manage_commission_refund_request_view', TRUE); }
		if($this->input->post('manage_commission_refund_request_active', TRUE) == '') { $manage_commision_refund_active = 0; } else { $manage_commision_refund_active = $this->input->post('manage_commission_refund_request_active', TRUE); }
		$manage_commission_refund = $manage_commision_refund_view.','.$manage_commision_refund_active;

		//Document Verification permission 
		if($this->input->post('manage_verify_doc_view', TRUE) == '') { $manage_verify_doc_view = 0; } else { $manage_verify_doc_view = $this->input->post('manage_verify_doc_view', TRUE); }
		$manage_verify_doc = $manage_verify_doc_view;

		//All orders permission 
		if($this->input->post('manage_all_orders_view', TRUE) == '') { $manage_all_orders_view = 0; } else { $manage_all_orders_view = $this->input->post('manage_all_orders_view', TRUE); }
		$manage_all_orders = $manage_all_orders_view;

		//Group Permissions
		if($this->input->post('permission_type_view', TRUE) == '') { $permission_type_view = 0; } else { $permission_type_view = $this->input->post('permission_type_view', TRUE); }
		if($this->input->post('permission_type_add', TRUE) == '') { $permission_type_add = 0; } else { $permission_type_add = $this->input->post('permission_type_add', TRUE); }
		if($this->input->post('permission_type_edit', TRUE) == '') { $permission_type_edit = 0; } else { $permission_type_edit = $this->input->post('permission_type_edit', TRUE); }
		if($this->input->post('permission_type_active', TRUE) == '') { $permission_type_active = 0; } else { $permission_type_active = $this->input->post('permission_type_active', TRUE); }
		$manage_group_permission = $permission_type_view.','.$permission_type_add.','.$permission_type_edit.','.$permission_type_active;

		//carrier Type permission
		if($this->input->post('carrier_type_view', TRUE) == '') { $carrier_type_view = 0; } else { $carrier_type_view = $this->input->post('carrier_type_view', TRUE); }
		if($this->input->post('carrier_type_add', TRUE) == '') { $carrier_type_add = 0; } else { $carrier_type_add = $this->input->post('carrier_type_add', TRUE); }
		if($this->input->post('carrier_type_edit', TRUE) == '') { $carrier_type_edit = 0; } else { $carrier_type_edit = $this->input->post('carrier_type_edit', TRUE); }
		if($this->input->post('carrier_type_delete', TRUE) == '') { $carrier_type_delete = 0; } else { $carrier_type_delete = $this->input->post('carrier_type_delete', TRUE); }
		$carrier_type = $carrier_type_view.','.$carrier_type_add.','.$carrier_type_edit.','.$carrier_type_delete;

		//Dangerous Goods
		if($this->input->post('dangerous_goods_view', TRUE) == '') { $dangerous_goods_view = 0; } else { $dangerous_goods_view = $this->input->post('dangerous_goods_view', TRUE); }
		if($this->input->post('dangerous_goods_add', TRUE) == '') { $dangerous_goods_add = 0; } else { $dangerous_goods_add = $this->input->post('dangerous_goods_add', TRUE); }
		if($this->input->post('dangerous_goods_edit', TRUE) == '') { $dangerous_goods_edit = 0; } else { $dangerous_goods_edit = $this->input->post('dangerous_goods_edit', TRUE); }
		if($this->input->post('dangerous_goods_active', TRUE) == '') { $dangerous_goods_active = 0; } else { $dangerous_goods_active = $this->input->post('dangerous_goods_active', TRUE); }
		$dangerous_goods = $dangerous_goods_view.','.$dangerous_goods_add.','.$dangerous_goods_edit.','.$dangerous_goods_active;

		//Country Dangerous Goods
		if($this->input->post('country_dangerous_goods_view', TRUE) == '') { $country_dangerous_goods_view = 0; } else { $country_dangerous_goods_view = $this->input->post('country_dangerous_goods_view', TRUE); }
		if($this->input->post('country_dangerous_goods_add', TRUE) == '') { $country_dangerous_goods_add = 0; } else { $country_dangerous_goods_add = $this->input->post('country_dangerous_goods_add', TRUE); }
		if($this->input->post('country_dangerous_goods_edit', TRUE) == '') { $country_dangerous_goods_edit = 0; } else { $country_dangerous_goods_edit = $this->input->post('country_dangerous_goods_edit', TRUE); }
		$country_dangerous_goods = $country_dangerous_goods_view.','.$country_dangerous_goods_add.','.$country_dangerous_goods_edit;

		//User Follow-ups
		if($this->input->post('user_followups_view', TRUE) == '') { $user_followups_view = 0; } else { $user_followups_view = $this->input->post('user_followups_view', TRUE); }
		if($this->input->post('user_followups_add', TRUE) == '') { $user_followups_add = 0; } else { $user_followups_add = $this->input->post('user_followups_add', TRUE); }
		if($this->input->post('user_followups_edit', TRUE) == '') { $user_followups_edit = 0; } else { $user_followups_edit = $this->input->post('user_followups_edit', TRUE); }
		$user_followups = $user_followups_view.','.$user_followups_add.','.$user_followups_edit;

		//Gonagoo Address permission
		if($this->input->post('gonagoo_address_view', TRUE) == '') { $gonagoo_address_view = 0; } else { $gonagoo_address_view = $this->input->post('gonagoo_address_view', TRUE); }
		if($this->input->post('gonagoo_address_add', TRUE) == '') { $gonagoo_address_add = 0; } else { $gonagoo_address_add = $this->input->post('gonagoo_address_add', TRUE); }
		if($this->input->post('gonagoo_address_edit', TRUE) == '') { $gonagoo_address_edit = 0; } else { $gonagoo_address_edit = $this->input->post('gonagoo_address_edit', TRUE); }
		if($this->input->post('gonagoo_address_active', TRUE) == '') { $gonagoo_address_active = 0; } else { $gonagoo_address_active = $this->input->post('gonagoo_address_active', TRUE); }
		if($this->input->post('gonagoo_address_delete', TRUE) == '') { $gonagoo_address_delete = 0; } else { $gonagoo_address_delete = $this->input->post('gonagoo_address_delete', TRUE); }
		$gonagoo_address = $gonagoo_address_view.','.$gonagoo_address_add.','.$gonagoo_address_edit.','.$gonagoo_address_active.','.$gonagoo_address_delete;

		//Gonagoo Laundry Category
		if($this->input->post('laundry_category_view', TRUE) == '') { $laundry_category_view = 0; } else { $laundry_category_view = $this->input->post('laundry_category_view', TRUE); }
		if($this->input->post('laundry_category_add', TRUE) == '') { $laundry_category_add = 0; } else { $laundry_category_add = $this->input->post('laundry_category_add', TRUE); }
		if($this->input->post('laundry_category_edit', TRUE) == '') { $laundry_category_edit = 0; } else { $laundry_category_edit = $this->input->post('laundry_category_edit', TRUE); }
		if($this->input->post('laundry_category_active', TRUE) == '') { $laundry_category_active = 0; } else { $laundry_category_active = $this->input->post('laundry_category_active', TRUE); }
		if($this->input->post('laundry_category_delete', TRUE) == '') { $laundry_category_delete = 0; } else { $laundry_category_delete = $this->input->post('laundry_category_delete', TRUE); }
		$laundry_category = $laundry_category_view.','.$laundry_category_add.','.$laundry_category_edit.','.$laundry_category_active.','.$laundry_category_delete;

		//Gonagoo Laundry Advance Payment and Gonagoo Commission
		if($this->input->post('laundry_payment_view', TRUE) == '') { $laundry_payment_view = 0; } else { $laundry_payment_view = $this->input->post('laundry_payment_view', TRUE); }
		if($this->input->post('laundry_payment_add', TRUE) == '') { $laundry_payment_add = 0; } else { $laundry_payment_add = $this->input->post('laundry_payment_add', TRUE); }
		if($this->input->post('laundry_payment_edit', TRUE) == '') { $laundry_payment_edit = 0; } else { $laundry_payment_edit = $this->input->post('laundry_payment_edit', TRUE); }
		if($this->input->post('laundry_payment_active', TRUE) == '') { $laundry_payment_active = 0; } else { $laundry_payment_active = $this->input->post('laundry_payment_active', TRUE); }
		if($this->input->post('laundry_payment_delete', TRUE) == '') { $laundry_payment_delete = 0; } else { $laundry_payment_delete = $this->input->post('laundry_payment_delete', TRUE); }
		$laundry_payment = $laundry_payment_view.','.$laundry_payment_add.','.$laundry_payment_edit.','.$laundry_payment_active.','.$laundry_payment_delete;

		//Module 4 - Advance Payment
		if($this->input->post('m4_adv_payment_view', TRUE) == '') { $m4_adv_payment_view = 0; } else { $m4_adv_payment_view = $this->input->post('m4_adv_payment_view', TRUE); }
		if($this->input->post('m4_adv_payment_add', TRUE) == '') { $m4_adv_payment_add = 0; } else { $m4_adv_payment_add = $this->input->post('m4_adv_payment_add', TRUE); }
		if($this->input->post('m4_adv_payment_edit', TRUE) == '') { $m4_adv_payment_edit = 0; } else { $m4_adv_payment_edit = $this->input->post('m4_adv_payment_edit', TRUE); }
		if($this->input->post('m4_adv_payment_delete', TRUE) == '') { $m4_adv_payment_delete = 0; } else { $m4_adv_payment_delete = $this->input->post('m4_adv_payment_delete', TRUE); }
		$m4_adv_payment = $m4_adv_payment_view.','.$m4_adv_payment_add.','.$m4_adv_payment_edit.','.$m4_adv_payment_delete;

		//Module 4 - Provider Questions
		if($this->input->post('m4_know_provider_view', TRUE) == '') { $m4_know_provider_view = 0; } else { $m4_know_provider_view = $this->input->post('m4_know_provider_view', TRUE); }
		if($this->input->post('m4_know_provider_add', TRUE) == '') { $m4_know_provider_add = 0; } else { $m4_know_provider_add = $this->input->post('m4_know_provider_add', TRUE); }
		if($this->input->post('m4_know_provider_edit', TRUE) == '') { $m4_know_provider_edit = 0; } else { $m4_know_provider_edit = $this->input->post('m4_know_provider_edit', TRUE); }
		if($this->input->post('m4_know_provider_delete', TRUE) == '') { $m4_know_provider_delete = 0; } else { $m4_know_provider_delete = $this->input->post('m4_know_provider_delete', TRUE); }
		$m4_know_provider = $m4_know_provider_view.','.$m4_know_provider_add.','.$m4_know_provider_edit.','.$m4_know_provider_delete;

		//Module 4 - Job cancellation Reasons
		if($this->input->post('m4_job_cancel_reason_view', TRUE) == '') { $m4_job_cancel_reason_view = 0; } else { $m4_job_cancel_reason_view = $this->input->post('m4_job_cancel_reason_view', TRUE); }
		if($this->input->post('m4_job_cancel_reason_add', TRUE) == '') { $m4_job_cancel_reason_add = 0; } else { $m4_job_cancel_reason_add = $this->input->post('m4_job_cancel_reason_add', TRUE); }
		if($this->input->post('m4_job_cancel_reason_edit', TRUE) == '') { $m4_job_cancel_reason_edit = 0; } else { $m4_job_cancel_reason_edit = $this->input->post('m4_job_cancel_reason_edit', TRUE); }
		if($this->input->post('m4_job_cancel_reason_delete', TRUE) == '') { $m4_job_cancel_reason_delete = 0; } else { $m4_job_cancel_reason_delete = $this->input->post('m4_job_cancel_reason_delete', TRUE); }
		$m4_job_cancel_reason = $m4_job_cancel_reason_view.','.$m4_job_cancel_reason_add.','.$m4_job_cancel_reason_edit.','.$m4_job_cancel_reason_delete;

		//Module 4 - Profile Subscription Master
		if($this->input->post('m4_profile_sub_view', TRUE) == '') { $m4_profile_sub_view = 0; } else { $m4_profile_sub_view = $this->input->post('m4_profile_sub_view', TRUE); }
		if($this->input->post('m4_profile_sub_add', TRUE) == '') { $m4_profile_sub_add = 0; } else { $m4_profile_sub_add = $this->input->post('m4_profile_sub_add', TRUE); }
		if($this->input->post('m4_profile_sub_edit', TRUE) == '') { $m4_profile_sub_edit = 0; } else { $m4_profile_sub_edit = $this->input->post('m4_profile_sub_edit', TRUE); }
		if($this->input->post('m4_profile_sub_delete', TRUE) == '') { $m4_profile_sub_delete = 0; } else { $m4_profile_sub_delete = $this->input->post('m4_profile_sub_delete', TRUE); }
		$m4_profile_sub = $m4_profile_sub_view.','.$m4_profile_sub_add.','.$m4_profile_sub_edit.','.$m4_profile_sub_delete;

		//Module 4 - Dispute Category Master
		if($this->input->post('m4_dispute_cat_view', TRUE) == '') { $m4_dispute_cat_view = 0; } else { $m4_dispute_cat_view = $this->input->post('m4_dispute_cat_view', TRUE); }
		if($this->input->post('m4_dispute_cat_add', TRUE) == '') { $m4_dispute_cat_add = 0; } else { $m4_dispute_cat_add = $this->input->post('m4_dispute_cat_add', TRUE); }
		if($this->input->post('m4_dispute_cat_edit', TRUE) == '') { $m4_dispute_cat_edit = 0; } else { $m4_dispute_cat_edit = $this->input->post('m4_dispute_cat_edit', TRUE); }
		if($this->input->post('m4_dispute_cat_active', TRUE) == '') { $m4_dispute_cat_active = 0; } else { $m4_dispute_cat_active = $this->input->post('m4_dispute_cat_active', TRUE); }
		$m4_dispute_cat = $m4_dispute_cat_view.','.$m4_dispute_cat_add.','.$m4_dispute_cat_edit.','.$m4_dispute_cat_active;

		//Module 4 - Dispute Sub Category Master
		if($this->input->post('m4_dispute_sub_cat_view', TRUE) == '') { $m4_dispute_sub_cat_view = 0; } else { $m4_dispute_sub_cat_view = $this->input->post('m4_dispute_sub_cat_view', TRUE); }
		if($this->input->post('m4_dispute_sub_cat_add', TRUE) == '') { $m4_dispute_sub_cat_add = 0; } else { $m4_dispute_sub_cat_add = $this->input->post('m4_dispute_sub_cat_add', TRUE); }
		if($this->input->post('m4_dispute_sub_cat_edit', TRUE) == '') { $m4_dispute_sub_cat_edit = 0; } else { $m4_dispute_sub_cat_edit = $this->input->post('m4_dispute_sub_cat_edit', TRUE); }
		if($this->input->post('m4_dispute_sub_cat_active', TRUE) == '') { $m4_dispute_sub_cat_active = 0; } else { $m4_dispute_sub_cat_active = $this->input->post('m4_dispute_sub_cat_active', TRUE); }
		$m4_dispute_sub_cat = $m4_dispute_sub_cat_view.','.$m4_dispute_sub_cat_add.','.$m4_dispute_sub_cat_edit.','.$m4_dispute_sub_cat_active;

		//Module 4 - Contract Expiry Notification Delay
		if($this->input->post('m4_contract_expiry_delay_view', TRUE) == '') { $m4_contract_expiry_delay_view = 0; } else { $m4_contract_expiry_delay_view = $this->input->post('m4_contract_expiry_delay_view', TRUE); }
		if($this->input->post('m4_contract_expiry_delay_add', TRUE) == '') { $m4_contract_expiry_delay_add = 0; } else { $m4_contract_expiry_delay_add = $this->input->post('m4_contract_expiry_delay_add', TRUE); }
		if($this->input->post('m4_contract_expiry_delay_edit', TRUE) == '') { $m4_contract_expiry_delay_edit = 0; } else { $m4_contract_expiry_delay_edit = $this->input->post('m4_contract_expiry_delay_edit', TRUE); }
		if($this->input->post('m4_contract_expiry_delay_delete', TRUE) == '') { $m4_contract_expiry_delay_delete = 0; } else { $m4_contract_expiry_delay_delete = $this->input->post('m4_contract_expiry_delay_delete', TRUE); }
		$m4_contract_expiry_delay = $m4_contract_expiry_delay_view.','.$m4_contract_expiry_delay_add.','.$m4_contract_expiry_delay_edit.','.$m4_contract_expiry_delay_delete;

		$update_data = array(
			'auth_type' => trim($auth_type),
			'cre_datetime' => date('Y-m-d h:i:s'),
			'category_type' => $category_type,
			'categories' => $categories,
			'currency_master' => $currency_master,
			'country_currency' => $country_currency,
			'authority_type' => $authority_type,
			'manage_authority' => $manage_authority,
			'manage_users' => $manage_users,
			'notifications' => $manage_notifications,
			'promo_code' => $manage_promo,
			'promo_code_service' => $manage_promo_service,
			'dimension' => $manage_dimension,
			'vehicals' => $manage_vehicals,
			'rates' => $manage_rates,
			'driver_category' => $manage_driver_category,
			'advance_payment' => $manage_advance_payment,
			'skills_master' => $manage_skills_master,
			'insurance_master' => $manage_insurance_master,
			'relay_point' => $manage_relay_point,
			'verify_doc' => $manage_verify_doc,
			'all_orders' => $manage_all_orders,
			'withdraw_request' => $manage_withdraw_request,
			'manage_group_permission' => $manage_group_permission,
			'carrier_type' => $carrier_type,
			'dangerous_goods' => $dangerous_goods,
			'country_dangerous_goods' => $country_dangerous_goods,
			'user_followups' => $user_followups,
			'gonagoo_address' => $gonagoo_address,
			'laundry_category' => $laundry_category,
			'laundry_payment' => $laundry_payment,
			'ticket_comm_refund' => $manage_commission_refund,
			'm4_adv_payment' => $m4_adv_payment,
			'm4_know_provider' => $m4_know_provider,
			'm4_job_cancel_reason' => $m4_job_cancel_reason,
			'm4_profile_sub' => $m4_profile_sub,
			'm4_dispute_cat' => $m4_dispute_cat,
			'm4_dispute_sub_cat' => $m4_dispute_sub_cat,
			'm4_contract_expiry_delay' => $m4_contract_expiry_delay,
		);

		if($this->group_per->register_group_permission($update_data)){
			$this->session->set_flashdata('success','Group successfully added!');
		} else { 	$this->session->set_flashdata('error','Unable to add new group! Try again...');	} 
		redirect('admin/add-group-permission');
	}

	public function edit_group_permission()
	{
		$auth_type_id = (int) $this->uri->segment('3');
		$group_per = $this->group_per->get_group_permission_detail($auth_type_id);
		$this->load->view('admin/group_permission_edit_view', compact('group_per'));
		$this->load->view('admin/footer_view');
	}

	public function active_group_permission()
	{
		$auth_type_id = (int) $this->input->post('id', TRUE);
		if( $this->group_per->activate_group_permission($auth_type_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

	public function inactive_group_permission()
	{
		$auth_type_id = (int) $this->input->post('id', TRUE);
		if( $this->group_per->inactivate_group_permission($auth_type_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

	public function update_group_permission()
	{
		$auth_type_id = $this->input->post('auth_type_id', TRUE);
		$auth_type = $this->input->post('auth_type', TRUE);
		//category_type permission
		if($this->input->post('category_type_view', TRUE) == '') { $category_type_view = 0; } else { $category_type_view = $this->input->post('category_type_view', TRUE); }
		if($this->input->post('category_type_add', TRUE) == '') { $category_type_add = 0; } else { $category_type_add = $this->input->post('category_type_add', TRUE); }
		if($this->input->post('category_type_edit', TRUE) == '') { $category_type_edit = 0; } else { $category_type_edit = $this->input->post('category_type_edit', TRUE); }
		if($this->input->post('category_type_active', TRUE) == '') { $category_type_active = 0; } else { $category_type_active = $this->input->post('category_type_active', TRUE); }
		$category_type = $category_type_view.','.$category_type_add.','.$category_type_edit.','.$category_type_active;

		//category permission
		if($this->input->post('category_view', TRUE) == '') { $category_view = 0; } else { $category_view = $this->input->post('category_view', TRUE); }
		if($this->input->post('category_add', TRUE) == '') { $category_add = 0; } else { $category_add = $this->input->post('category_add', TRUE); }
		if($this->input->post('category_edit', TRUE) == '') { $category_edit = 0; } else { $category_edit = $this->input->post('category_edit', TRUE); }
		if($this->input->post('category_active', TRUE) == '') { $category_active = 0; } else { $category_active = $this->input->post('category_active', TRUE); }
		$categories = $category_view.','.$category_add.','.$category_edit.','.$category_active;

		//currency master permission
		if($this->input->post('currency_master_view', TRUE) == '') { $currency_master_view = 0; } else { $currency_master_view = $this->input->post('currency_master_view', TRUE); }
		if($this->input->post('currency_master_add', TRUE) == '') { $currency_master_add = 0; } else { $currency_master_add = $this->input->post('currency_master_add', TRUE); }
		if($this->input->post('currency_master_edit', TRUE) == '') { $currency_master_edit = 0; } else { $currency_master_edit = $this->input->post('currency_master_edit', TRUE); }
		if($this->input->post('currency_master_active', TRUE) == '') { $currency_master_active = 0; } else { $currency_master_active = $this->input->post('currency_master_active', TRUE); }
		if($this->input->post('currency_master_delete', TRUE) == '') { $currency_master_delete = 0; } else { $currency_master_delete = $this->input->post('currency_master_delete', TRUE); }
		$currency_master = $currency_master_view.','.$currency_master_add.','.$currency_master_edit.','.$currency_master_active.','.$currency_master_delete;

		//country currency master permission
		if($this->input->post('country_currency_view', TRUE) == '') { $country_currency_view = 0; } else { $country_currency_view = $this->input->post('country_currency_view', TRUE); }
		if($this->input->post('country_currency_add', TRUE) == '') { $country_currency_add = 0; } else { $country_currency_add = $this->input->post('country_currency_add', TRUE); }
		if($this->input->post('country_currency_edit', TRUE) == '') { $country_currency_edit = 0; } else { $country_currency_edit = $this->input->post('country_currency_edit', TRUE); }
		if($this->input->post('country_currency_active', TRUE) == '') { $country_currency_active = 0; } else { $country_currency_active = $this->input->post('country_currency_active', TRUE); }
		$country_currency = $country_currency_view.','.$country_currency_add.','.$country_currency_edit.','.$country_currency_active;

		//Authority
		if($this->input->post('manage_authority_view', TRUE) == '') { $manage_authority_view = 0; } else { $manage_authority_view = $this->input->post('manage_authority_view', TRUE); }
		if($this->input->post('manage_authority_add', TRUE) == '') { $manage_authority_add = 0; } else { $manage_authority_add = $this->input->post('manage_authority_add', TRUE); }
		if($this->input->post('manage_authority_edit', TRUE) == '') { $manage_authority_edit = 0; } else { $manage_authority_edit = $this->input->post('manage_authority_edit', TRUE); }
		if($this->input->post('manage_authority_active', TRUE) == '') { $manage_authority_active = 0; } else { $manage_authority_active = $this->input->post('manage_authority_active', TRUE); }
		$manage_authority = $manage_authority_view.','.$manage_authority_add.','.$manage_authority_edit.','.$manage_authority_active;

		//Users permission
		if($this->input->post('manage_users_view', TRUE) == '') { $manage_users_view = 0; } else { $manage_users_view = $this->input->post('manage_users_view', TRUE); }
		if($this->input->post('manage_users_add', TRUE) == '') { $manage_users_add = 0; } else { $manage_users_add = $this->input->post('manage_users_add', TRUE); }
		if($this->input->post('manage_users_edit', TRUE) == '') { $manage_users_edit = 0; } else { $manage_users_edit = $this->input->post('manage_users_edit', TRUE); }
		if($this->input->post('manage_users_active', TRUE) == '') { $manage_users_active = 0; } else { $manage_users_active = $this->input->post('manage_users_active', TRUE); }
		$manage_users = $manage_users_view.','.$manage_users_add.','.$manage_users_edit.','.$manage_users_active;

		//Notification
		if($this->input->post('manage_notifications_view', TRUE) == '') { $manage_notifications_view = 0; } else { $manage_notifications_view = $this->input->post('manage_notifications_view', TRUE); }
		if($this->input->post('manage_notifications_add', TRUE) == '') { $manage_notifications_add = 0; } else { $manage_notifications_add = $this->input->post('manage_notifications_add', TRUE); }
		$manage_notifications = $manage_notifications_view.','.$manage_notifications_add;

		//Promo Code
		if($this->input->post('manage_promo_view', TRUE) == '') { $manage_promo_view = 0; } else { $manage_promo_view = $this->input->post('manage_promo_view', TRUE); }
		if($this->input->post('manage_promo_add', TRUE) == '') { $manage_promo_add = 0; } else { $manage_promo_add = $this->input->post('manage_promo_add', TRUE); }
		if($this->input->post('manage_promo_edit', TRUE) == '') { $manage_promo_edit = 0; } else { $manage_promo_edit = $this->input->post('manage_promo_edit', TRUE); }
		if($this->input->post('manage_promo_active', TRUE) == '') { $manage_promo_active = 0; } else { $manage_promo_active = $this->input->post('manage_promo_active', TRUE); }
		$manage_promo = $manage_promo_view.','.$manage_promo_add.','.$manage_promo_edit.','.$manage_promo_active;

		//Promo Code Service
		if($this->input->post('manage_promo_service_view', TRUE) == '') { $manage_promo_service_view = 0; } else { $manage_promo_service_view = $this->input->post('manage_promo_service_view', TRUE); }
		$manage_promo_service = $manage_promo_service_view;

		//Standard Dimension 
		if($this->input->post('manage_dimension_view', TRUE) == '') { $manage_dimension_view = 0; } else { $manage_dimension_view = $this->input->post('manage_dimension_view', TRUE); }
		if($this->input->post('manage_dimension_add', TRUE) == '') { $manage_dimension_add = 0; } else { $manage_dimension_add = $this->input->post('manage_dimension_add', TRUE); }
		if($this->input->post('manage_dimension_edit', TRUE) == '') { $manage_dimension_edit = 0; } else { $manage_dimension_edit = $this->input->post('manage_dimension_edit', TRUE); }
		if($this->input->post('manage_dimension_delete', TRUE) == '') { $manage_dimension_delete = 0; } else { $manage_dimension_delete = $this->input->post('manage_dimension_delete', TRUE); }
		$manage_dimension = $manage_dimension_view.','.$manage_dimension_add.','.$manage_dimension_edit.','.$manage_dimension_delete;

		//Vehicles
		if($this->input->post('manage_vehicals_view', TRUE) == '') { $manage_vehicals_view = 0; } else { $manage_vehicals_view = $this->input->post('manage_vehicals_view', TRUE); }
		if($this->input->post('manage_vehicals_add', TRUE) == '') { $manage_vehicals_add = 0; } else { $manage_vehicals_add = $this->input->post('manage_vehicals_add', TRUE); }
		if($this->input->post('manage_vehicals_edit', TRUE) == '') { $manage_vehicals_edit = 0; } else { $manage_vehicals_edit = $this->input->post('manage_vehicals_edit', TRUE); }
		if($this->input->post('manage_vehicals_delete', TRUE) == '') { $manage_vehicals_delete = 0; } else { $manage_vehicals_delete = $this->input->post('manage_vehicals_delete', TRUE); }
		$manage_vehicals = $manage_vehicals_view.','.$manage_vehicals_add.','.$manage_vehicals_edit.','.$manage_vehicals_delete;

		//Standard Rates
		if($this->input->post('manage_rates_view', TRUE) == '') { $manage_rates_view = 0; } else { $manage_rates_view = $this->input->post('manage_rates_view', TRUE); }
		if($this->input->post('manage_rates_add', TRUE) == '') { $manage_rates_add = 0; } else { $manage_rates_add = $this->input->post('manage_rates_add', TRUE); }
		if($this->input->post('manage_rates_edit', TRUE) == '') { $manage_rates_edit = 0; } else { $manage_rates_edit = $this->input->post('manage_rates_edit', TRUE); }
		if($this->input->post('manage_rates_delete', TRUE) == '') { $manage_rates_delete = 0; } else { $manage_rates_delete = $this->input->post('manage_rates_delete', TRUE); }
		$manage_rates = $manage_rates_view.','.$manage_rates_add.','.$manage_rates_edit.','.$manage_rates_delete;

		//Driver Category
		if($this->input->post('manage_driver_category_view', TRUE) == '') { $manage_driver_category_view = 0; } else { $manage_driver_category_view = $this->input->post('manage_driver_category_view', TRUE); }
		if($this->input->post('manage_driver_category_add', TRUE) == '') { $manage_driver_category_add = 0; } else { $manage_driver_category_add = $this->input->post('manage_driver_category_add', TRUE); }
		if($this->input->post('manage_driver_category_edit', TRUE) == '') { $manage_driver_category_edit = 0; } else { $manage_driver_category_edit = $this->input->post('manage_driver_category_edit', TRUE); }
		if($this->input->post('manage_driver_category_delete', TRUE) == '') { $manage_driver_category_delete = 0; } else { $manage_driver_category_delete = $this->input->post('manage_driver_category_delete', TRUE); }
		$manage_driver_category = $manage_driver_category_view.','.$manage_driver_category_add.','.$manage_driver_category_edit.','.$manage_driver_category_delete;

		//Advance Payment
		if($this->input->post('manage_advance_payment_view', TRUE) == '') { $manage_advance_payment_view = 0; } else { $manage_advance_payment_view = $this->input->post('manage_advance_payment_view', TRUE); }
		if($this->input->post('manage_advance_payment_add', TRUE) == '') { $manage_advance_payment_add = 0; } else { $manage_advance_payment_add = $this->input->post('manage_advance_payment_add', TRUE); }
		if($this->input->post('manage_advance_payment_edit', TRUE) == '') { $manage_advance_payment_edit = 0; } else { $manage_advance_payment_edit = $this->input->post('manage_advance_payment_edit', TRUE); }
		if($this->input->post('manage_advance_payment_delete', TRUE) == '') { $manage_advance_payment_delete = 0; } else { $manage_advance_payment_delete = $this->input->post('manage_advance_payment_delete', TRUE); }
		$manage_advance_payment = $manage_advance_payment_view.','.$manage_advance_payment_add.','.$manage_advance_payment_edit.','.$manage_advance_payment_delete;

		//Skill permission 
		if($this->input->post('manage_skills_master_view', TRUE) == '') { $manage_skills_master_view = 0; } else { $manage_skills_master_view = $this->input->post('manage_skills_master_view', TRUE); }
		if($this->input->post('manage_skills_master_add', TRUE) == '') { $manage_skills_master_add = 0; } else { $manage_skills_master_add = $this->input->post('manage_skills_master_add', TRUE); }
		if($this->input->post('manage_skills_master_edit', TRUE) == '') { $manage_skills_master_edit = 0; } else { $manage_skills_master_edit = $this->input->post('manage_skills_master_edit', TRUE); }
		if($this->input->post('manage_skills_master_active', TRUE) == '') { $manage_skills_master_active = 0; } else { $manage_skills_master_active = $this->input->post('manage_skills_master_active', TRUE); }
		$manage_skills_master = $manage_skills_master_view.','.$manage_skills_master_add.','.$manage_skills_master_edit.','.$manage_skills_master_active;

		//Insurance permission 
		if($this->input->post('manage_insurance_master_view', TRUE) == '') { $manage_insurance_master_view = 0; } else { $manage_insurance_master_view = $this->input->post('manage_insurance_master_view', TRUE); }
		if($this->input->post('manage_insurance_master_add', TRUE) == '') { $manage_insurance_master_add = 0; } else { $manage_insurance_master_add = $this->input->post('manage_insurance_master_add', TRUE); }
		if($this->input->post('manage_insurance_master_edit', TRUE) == '') { $manage_insurance_master_edit = 0; } else { $manage_insurance_master_edit = $this->input->post('manage_insurance_master_edit', TRUE); }
		if($this->input->post('manage_insurance_master_active', TRUE) == '') { $manage_insurance_master_active = 0; } else { $manage_insurance_master_active = $this->input->post('manage_insurance_master_active', TRUE); }
		$manage_insurance_master = $manage_insurance_master_view.','.$manage_insurance_master_add.','.$manage_insurance_master_edit.','.$manage_insurance_master_active;

		//Relay Point permission 
		if($this->input->post('manage_relay_point_view', TRUE) == '') { $manage_relay_point_view = 0; } else { $manage_relay_point_view = $this->input->post('manage_relay_point_view', TRUE); }
		if($this->input->post('manage_relay_point_add', TRUE) == '') { $manage_relay_point_add = 0; } else { $manage_relay_point_add = $this->input->post('manage_relay_point_add', TRUE); }
		if($this->input->post('manage_relay_point_edit', TRUE) == '') { $manage_relay_point_edit = 0; } else { $manage_relay_point_edit = $this->input->post('manage_relay_point_edit', TRUE); }
		if($this->input->post('manage_relay_point_active', TRUE) == '') { $manage_relay_point_active = 0; } else { $manage_relay_point_active = $this->input->post('manage_relay_point_active', TRUE); }
		$manage_relay_point = $manage_relay_point_view.','.$manage_relay_point_add.','.$manage_relay_point_edit.','.$manage_relay_point_active;

		//Withdraw request permission 
		if($this->input->post('manage_withdraw_request_view', TRUE) == '') { $manage_withdraw_request_view = 0; } else { $manage_withdraw_request_view = $this->input->post('manage_withdraw_request_view', TRUE); }
		if($this->input->post('manage_withdraw_request_active', TRUE) == '') { $manage_withdraw_request_active = 0; } else { $manage_withdraw_request_active = $this->input->post('manage_withdraw_request_active', TRUE); }
		$manage_withdraw_request = $manage_withdraw_request_view.','.$manage_withdraw_request_active;

		//Commission refund request permission 
		if($this->input->post('manage_commission_refund_request_view', TRUE) == '') { $manage_commission_refund_view = 0; } else { $manage_commission_refund_view = $this->input->post('manage_commission_refund_request_view', TRUE); }
		if($this->input->post('manage_commission_refund_request_active', TRUE) == '') { $manage_commission_refund_active = 0; } else { $manage_commission_refund_active = $this->input->post('manage_commission_refund_request_active', TRUE); }
		$manage_commission_refund = $manage_commission_refund_view.','.$manage_commission_refund_active;

		//Document Verification permission 
		if($this->input->post('manage_verify_doc_view', TRUE) == '') { $manage_verify_doc_view = 0; } else { $manage_verify_doc_view = $this->input->post('manage_verify_doc_view', TRUE); }
		$manage_verify_doc = $manage_verify_doc_view;

		//User Orders permission 
		if($this->input->post('manage_all_orders_view', TRUE) == '') { $manage_all_orders_view = 0; } else { $manage_all_orders_view = $this->input->post('manage_all_orders_view', TRUE); }
		if($this->input->post('manage_all_orders_active', TRUE) == '') { $manage_all_orders_active = 0; } else { $manage_all_orders_active = $this->input->post('manage_all_orders_active', TRUE); }
		$manage_all_orders = $manage_all_orders_view.','.$manage_all_orders_active;

		//Group Permissions
		if($this->input->post('permission_type_view', TRUE) == '') { $permission_type_view = 0; } else { $permission_type_view = $this->input->post('permission_type_view', TRUE); }
		if($this->input->post('permission_type_add', TRUE) == '') { $permission_type_add = 0; } else { $permission_type_add = $this->input->post('permission_type_add', TRUE); }
		if($this->input->post('permission_type_edit', TRUE) == '') { $permission_type_edit = 0; } else { $permission_type_edit = $this->input->post('permission_type_edit', TRUE); }
		if($this->input->post('permission_type_active', TRUE) == '') { $permission_type_active = 0; } else { $permission_type_active = $this->input->post('permission_type_active', TRUE); }
		$manage_group_permission = $permission_type_view.','.$permission_type_add.','.$permission_type_edit.','.$permission_type_active;


		//Customer support 
		if($this->input->post('customer_support_view', TRUE) == '') { $customer_support_view = 0; } else { $customer_support_view = $this->input->post('customer_support_view', TRUE); }
		if($this->input->post('customer_support_edit', TRUE) == '') { $customer_support_edit = 0; } else { $customer_support_edit = $this->input->post('customer_support_edit', TRUE); }
		$customer_support = $customer_support_view.','.$customer_support_edit;

		//carrier Type permission
		if($this->input->post('carrier_type_view', TRUE) == '') { $carrier_type_view = 0; } else { $carrier_type_view = $this->input->post('carrier_type_view', TRUE); }
		if($this->input->post('carrier_type_add', TRUE) == '') { $carrier_type_add = 0; } else { $carrier_type_add = $this->input->post('carrier_type_add', TRUE); }
		if($this->input->post('carrier_type_edit', TRUE) == '') { $carrier_type_edit = 0; } else { $carrier_type_edit = $this->input->post('carrier_type_edit', TRUE); }
		if($this->input->post('carrier_type_delete', TRUE) == '') { $carrier_type_delete = 0; } else { $carrier_type_delete = $this->input->post('carrier_type_delete', TRUE); }
		$carrier_type = $carrier_type_view.','.$carrier_type_add.','.$carrier_type_edit.','.$carrier_type_delete;

		//Dangerous Goods
		if($this->input->post('dangerous_goods_view', TRUE) == '') { $dangerous_goods_view = 0; } else { $dangerous_goods_view = $this->input->post('dangerous_goods_view', TRUE); }
		if($this->input->post('dangerous_goods_add', TRUE) == '') { $dangerous_goods_add = 0; } else { $dangerous_goods_add = $this->input->post('dangerous_goods_add', TRUE); }
		if($this->input->post('dangerous_goods_edit', TRUE) == '') { $dangerous_goods_edit = 0; } else { $dangerous_goods_edit = $this->input->post('dangerous_goods_edit', TRUE); }
		if($this->input->post('dangerous_goods_active', TRUE) == '') { $dangerous_goods_active = 0; } else { $dangerous_goods_active = $this->input->post('dangerous_goods_active', TRUE); }
		$dangerous_goods = $dangerous_goods_view.','.$dangerous_goods_add.','.$dangerous_goods_edit.','.$dangerous_goods_active;

		//Country Dangerous Goods
		if($this->input->post('country_dangerous_goods_view', TRUE) == '') { $country_dangerous_goods_view = 0; } else { $country_dangerous_goods_view = $this->input->post('country_dangerous_goods_view', TRUE); }
		if($this->input->post('country_dangerous_goods_add', TRUE) == '') { $country_dangerous_goods_add = 0; } else { $country_dangerous_goods_add = $this->input->post('country_dangerous_goods_add', TRUE); }
		if($this->input->post('country_dangerous_goods_edit', TRUE) == '') { $country_dangerous_goods_edit = 0; } else { $country_dangerous_goods_edit = $this->input->post('country_dangerous_goods_edit', TRUE); }
		$country_dangerous_goods = $country_dangerous_goods_view.','.$country_dangerous_goods_add.','.$country_dangerous_goods_edit;

		//User Follow-ups
		if($this->input->post('user_followups_view', TRUE) == '') { $user_followups_view = 0; } else { $user_followups_view = $this->input->post('user_followups_view', TRUE); }
		if($this->input->post('user_followups_add', TRUE) == '') { $user_followups_add = 0; } else { $user_followups_add = $this->input->post('user_followups_add', TRUE); }
		if($this->input->post('user_followups_edit', TRUE) == '') { $user_followups_edit = 0; } else { $user_followups_edit = $this->input->post('user_followups_edit', TRUE); }
		$user_followups = $user_followups_view.','.$user_followups_add.','.$user_followups_edit;

		//Gonagoo Address permission
		if($this->input->post('gonagoo_address_view', TRUE) == '') { $gonagoo_address_view = 0; } else { $gonagoo_address_view = $this->input->post('gonagoo_address_view', TRUE); }
		if($this->input->post('gonagoo_address_add', TRUE) == '') { $gonagoo_address_add = 0; } else { $gonagoo_address_add = $this->input->post('gonagoo_address_add', TRUE); }
		if($this->input->post('gonagoo_address_edit', TRUE) == '') { $gonagoo_address_edit = 0; } else { $gonagoo_address_edit = $this->input->post('gonagoo_address_edit', TRUE); }
		if($this->input->post('gonagoo_address_active', TRUE) == '') { $gonagoo_address_active = 0; } else { $gonagoo_address_active = $this->input->post('gonagoo_address_active', TRUE); }
		if($this->input->post('gonagoo_address_delete', TRUE) == '') { $gonagoo_address_delete = 0; } else { $gonagoo_address_delete = $this->input->post('gonagoo_address_delete', TRUE); }
		$gonagoo_address = $gonagoo_address_view.','.$gonagoo_address_add.','.$gonagoo_address_edit.','.$gonagoo_address_active.','.$gonagoo_address_delete;

		//Gonagoo Laundry Category
		if($this->input->post('laundry_category_view', TRUE) == '') { $laundry_category_view = 0; } else { $laundry_category_view = $this->input->post('laundry_category_view', TRUE); }
		if($this->input->post('laundry_category_add', TRUE) == '') { $laundry_category_add = 0; } else { $laundry_category_add = $this->input->post('laundry_category_add', TRUE); }
		if($this->input->post('laundry_category_edit', TRUE) == '') { $laundry_category_edit = 0; } else { $laundry_category_edit = $this->input->post('laundry_category_edit', TRUE); }
		if($this->input->post('laundry_category_active', TRUE) == '') { $laundry_category_active = 0; } else { $laundry_category_active = $this->input->post('laundry_category_active', TRUE); }
		if($this->input->post('laundry_category_delete', TRUE) == '') { $laundry_category_delete = 0; } else { $laundry_category_delete = $this->input->post('laundry_category_delete', TRUE); }
		$laundry_category = $laundry_category_view.','.$laundry_category_add.','.$laundry_category_edit.','.$laundry_category_active.','.$laundry_category_delete;

		//Gonagoo Laundry Advance Payment and Gonagoo Commission
		if($this->input->post('laundry_payment_view', TRUE) == '') { $laundry_payment_view = 0; } else { $laundry_payment_view = $this->input->post('laundry_payment_view', TRUE); }
		if($this->input->post('laundry_payment_add', TRUE) == '') { $laundry_payment_add = 0; } else { $laundry_payment_add = $this->input->post('laundry_payment_add', TRUE); }
		if($this->input->post('laundry_payment_edit', TRUE) == '') { $laundry_payment_edit = 0; } else { $laundry_payment_edit = $this->input->post('laundry_payment_edit', TRUE); }
		if($this->input->post('laundry_payment_active', TRUE) == '') { $laundry_payment_active = 0; } else { $laundry_payment_active = $this->input->post('laundry_payment_active', TRUE); }
		if($this->input->post('laundry_payment_delete', TRUE) == '') { $laundry_payment_delete = 0; } else { $laundry_payment_delete = $this->input->post('laundry_payment_delete', TRUE); }
		$laundry_payment = $laundry_payment_view.','.$laundry_payment_add.','.$laundry_payment_edit.','.$laundry_payment_active.','.$laundry_payment_delete;

		//Module 4 - Advance Payment
		if($this->input->post('m4_adv_payment_view', TRUE) == '') { $m4_adv_payment_view = 0; } else { $m4_adv_payment_view = $this->input->post('m4_adv_payment_view', TRUE); }
		if($this->input->post('m4_adv_payment_add', TRUE) == '') { $m4_adv_payment_add = 0; } else { $m4_adv_payment_add = $this->input->post('m4_adv_payment_add', TRUE); }
		if($this->input->post('m4_adv_payment_edit', TRUE) == '') { $m4_adv_payment_edit = 0; } else { $m4_adv_payment_edit = $this->input->post('m4_adv_payment_edit', TRUE); }
		if($this->input->post('m4_adv_payment_delete', TRUE) == '') { $m4_adv_payment_delete = 0; } else { $m4_adv_payment_delete = $this->input->post('m4_adv_payment_delete', TRUE); }
		$m4_adv_payment = $m4_adv_payment_view.','.$m4_adv_payment_add.','.$m4_adv_payment_edit.','.$m4_adv_payment_delete;

		//Module 4 - Provider Questions
		if($this->input->post('m4_know_provider_view', TRUE) == '') { $m4_know_provider_view = 0; } else { $m4_know_provider_view = $this->input->post('m4_know_provider_view', TRUE); }
		if($this->input->post('m4_know_provider_add', TRUE) == '') { $m4_know_provider_add = 0; } else { $m4_know_provider_add = $this->input->post('m4_know_provider_add', TRUE); }
		if($this->input->post('m4_know_provider_edit', TRUE) == '') { $m4_know_provider_edit = 0; } else { $m4_know_provider_edit = $this->input->post('m4_know_provider_edit', TRUE); }
		if($this->input->post('m4_know_provider_delete', TRUE) == '') { $m4_know_provider_delete = 0; } else { $m4_know_provider_delete = $this->input->post('m4_know_provider_delete', TRUE); }
		$m4_know_provider = $m4_know_provider_view.','.$m4_know_provider_add.','.$m4_know_provider_edit.','.$m4_know_provider_delete;

		//Module 4 - Job cancellation Reasons
		if($this->input->post('m4_job_cancel_reason_view', TRUE) == '') { $m4_job_cancel_reason_view = 0; } else { $m4_job_cancel_reason_view = $this->input->post('m4_job_cancel_reason_view', TRUE); }
		if($this->input->post('m4_job_cancel_reason_add', TRUE) == '') { $m4_job_cancel_reason_add = 0; } else { $m4_job_cancel_reason_add = $this->input->post('m4_job_cancel_reason_add', TRUE); }
		if($this->input->post('m4_job_cancel_reason_edit', TRUE) == '') { $m4_job_cancel_reason_edit = 0; } else { $m4_job_cancel_reason_edit = $this->input->post('m4_job_cancel_reason_edit', TRUE); }
		if($this->input->post('m4_job_cancel_reason_delete', TRUE) == '') { $m4_job_cancel_reason_delete = 0; } else { $m4_job_cancel_reason_delete = $this->input->post('m4_job_cancel_reason_delete', TRUE); }
		$m4_job_cancel_reason = $m4_job_cancel_reason_view.','.$m4_job_cancel_reason_add.','.$m4_job_cancel_reason_edit.','.$m4_job_cancel_reason_delete;

		//Module 4 - Profile Subscription Master
		if($this->input->post('m4_profile_sub_view', TRUE) == '') { $m4_profile_sub_view = 0; } else { $m4_profile_sub_view = $this->input->post('m4_profile_sub_view', TRUE); }
		if($this->input->post('m4_profile_sub_add', TRUE) == '') { $m4_profile_sub_add = 0; } else { $m4_profile_sub_add = $this->input->post('m4_profile_sub_add', TRUE); }
		if($this->input->post('m4_profile_sub_edit', TRUE) == '') { $m4_profile_sub_edit = 0; } else { $m4_profile_sub_edit = $this->input->post('m4_profile_sub_edit', TRUE); }
		if($this->input->post('m4_profile_sub_delete', TRUE) == '') { $m4_profile_sub_delete = 0; } else { $m4_profile_sub_delete = $this->input->post('m4_profile_sub_delete', TRUE); }
		$m4_profile_sub = $m4_profile_sub_view.','.$m4_profile_sub_add.','.$m4_profile_sub_edit.','.$m4_profile_sub_delete;

		//Module 4 - Dispute Category Master
		if($this->input->post('m4_dispute_cat_view', TRUE) == '') { $m4_dispute_cat_view = 0; } else { $m4_dispute_cat_view = $this->input->post('m4_dispute_cat_view', TRUE); }
		if($this->input->post('m4_dispute_cat_add', TRUE) == '') { $m4_dispute_cat_add = 0; } else { $m4_dispute_cat_add = $this->input->post('m4_dispute_cat_add', TRUE); }
		if($this->input->post('m4_dispute_cat_edit', TRUE) == '') { $m4_dispute_cat_edit = 0; } else { $m4_dispute_cat_edit = $this->input->post('m4_dispute_cat_edit', TRUE); }
		if($this->input->post('m4_dispute_cat_active', TRUE) == '') { $m4_dispute_cat_active = 0; } else { $m4_dispute_cat_active = $this->input->post('m4_dispute_cat_active', TRUE); }
		$m4_dispute_cat = $m4_dispute_cat_view.','.$m4_dispute_cat_add.','.$m4_dispute_cat_edit.','.$m4_dispute_cat_active;

		//Module 4 - Dispute Sub Category Master
		if($this->input->post('m4_dispute_sub_cat_view', TRUE) == '') { $m4_dispute_sub_cat_view = 0; } else { $m4_dispute_sub_cat_view = $this->input->post('m4_dispute_sub_cat_view', TRUE); }
		if($this->input->post('m4_dispute_sub_cat_add', TRUE) == '') { $m4_dispute_sub_cat_add = 0; } else { $m4_dispute_sub_cat_add = $this->input->post('m4_dispute_sub_cat_add', TRUE); }
		if($this->input->post('m4_dispute_sub_cat_edit', TRUE) == '') { $m4_dispute_sub_cat_edit = 0; } else { $m4_dispute_sub_cat_edit = $this->input->post('m4_dispute_sub_cat_edit', TRUE); }
		if($this->input->post('m4_dispute_sub_cat_active', TRUE) == '') { $m4_dispute_sub_cat_active = 0; } else { $m4_dispute_sub_cat_active = $this->input->post('m4_dispute_sub_cat_active', TRUE); }
		$m4_dispute_sub_cat = $m4_dispute_sub_cat_view.','.$m4_dispute_sub_cat_add.','.$m4_dispute_sub_cat_edit.','.$m4_dispute_sub_cat_active;

		//Module 4 - Contract Expiry Notification Delay
		if($this->input->post('m4_contract_expiry_delay_view', TRUE) == '') { $m4_contract_expiry_delay_view = 0; } else { $m4_contract_expiry_delay_view = $this->input->post('m4_contract_expiry_delay_view', TRUE); }
		if($this->input->post('m4_contract_expiry_delay_add', TRUE) == '') { $m4_contract_expiry_delay_add = 0; } else { $m4_contract_expiry_delay_add = $this->input->post('m4_contract_expiry_delay_add', TRUE); }
		if($this->input->post('m4_contract_expiry_delay_edit', TRUE) == '') { $m4_contract_expiry_delay_edit = 0; } else { $m4_contract_expiry_delay_edit = $this->input->post('m4_contract_expiry_delay_edit', TRUE); }
		if($this->input->post('m4_contract_expiry_delay_delete', TRUE) == '') { $m4_contract_expiry_delay_delete = 0; } else { $m4_contract_expiry_delay_delete = $this->input->post('m4_contract_expiry_delay_delete', TRUE); }
		$m4_contract_expiry_delay = $m4_contract_expiry_delay_view.','.$m4_contract_expiry_delay_add.','.$m4_contract_expiry_delay_edit.','.$m4_contract_expiry_delay_delete;

		$update_data = array(
			'auth_type' => trim($auth_type),
			'cre_datetime' => date('Y-m-d h:i:s'),
			'category_type' => $category_type,
			'categories' => $categories,
			'currency_master' => $currency_master,
			'country_currency' => $country_currency,
			'manage_authority' => $manage_authority,
			'manage_users' => $manage_users,
			'notifications' => $manage_notifications,
			'promo_code' => $manage_promo,
			'promo_code_service' => $manage_promo_service,
			'dimension' => $manage_dimension,
			'vehicals' => $manage_vehicals,
			'rates' => $manage_rates,
			'driver_category' => $manage_driver_category,
			'advance_payment' => $manage_advance_payment,
			'skills_master' => $manage_skills_master,
			'insurance_master' => $manage_insurance_master,
			'relay_point' => $manage_relay_point,
			'verify_doc' => $manage_verify_doc,
			'all_orders' => $manage_all_orders,
			'withdraw_request' => $manage_withdraw_request,
			'manage_group_permission' => $manage_group_permission,
			'customer_support' => $customer_support,
			'carrier_type' => $carrier_type,
			'dangerous_goods' => $dangerous_goods,
			'country_dangerous_goods' => $country_dangerous_goods,
			'user_followups' => $user_followups,
			'gonagoo_address' => $gonagoo_address,
			'laundry_category' => $laundry_category,
			'laundry_payment' => $laundry_payment,
			'ticket_comm_refund' => $manage_commission_refund,
			'm4_adv_payment' => $m4_adv_payment,
			'm4_know_provider' => $m4_know_provider,
			'm4_job_cancel_reason' => $m4_job_cancel_reason,
			'm4_profile_sub' => $m4_profile_sub,
			'm4_dispute_cat' => $m4_dispute_cat,
			'm4_dispute_sub_cat' => $m4_dispute_sub_cat,
			'm4_contract_expiry_delay' => $m4_contract_expiry_delay,
		);
		
		if( $this->group_per->update_group_permission($update_data, $auth_type_id)){
			$this->session->set_flashdata('success','Group and permission successfully updated!');
		}	else { 	$this->session->set_flashdata('error','Unable to update group and permission! Try again...');	}
		
		redirect('admin/edit-group-permission/'. $auth_type_id);
	}

	
	public function delete_authority_type()
	{
		$auth_type_id = (int) $this->uri->segment('3');
		if( $this->group_per->delete_authority_type($auth_type_id) ) {
			$this->session->set_flashdata('success','authority type successfully deleted!');
		}	else { 	$this->session->set_flashdata('error','Unable to delete authority type! Try again...');	}
		redirect('authority_type');					
	}

}

/* End of file Group_permissions.php */
/* Location: ./application/controllers/Group_permissions.php */