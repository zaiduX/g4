<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_logged_in')){
			$this->load->model('Admin_model', 'admin');
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($id);
			$this->load->view('admin/side_bar_view', compact('user','permissions'));

		} else{ redirect('admin');}
	}
	public function index()
	{
		if(!$this->session->userdata('is_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}
	public function dashboard()
	{			
			$this->load->view('admin/dashboard_view');
			$this->load->view('admin/footer_view');
	}
	public function forgot_pass($value='')
	{
		$this->load->view('admin/dashboard_view');
	}

	public function profile()
	{	
		$this->load->view('admin/profile_view');
		$this->load->view('admin/footer_view');
	}
}

/* End of file Administration.php */
/* Location: ./application/controllers/Administration.php */