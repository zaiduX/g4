<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_followups_sales extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Users_followups_sales_model', 'users');		
			$this->load->model('web_user_model', 'user');		
			$this->load->model('Notification_model', 'notice');			
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function user_followups_sales_list()
	{		
	    $userid = $this->session->userdata('userid');
	    $users_list = $this->users->get_users($userid);
//echo $this->db->last_query(); die();
	    $this->load->view('admin/users_followups_sales_list_view', compact('users_list'));			
	    $this->load->view('admin/footer_view');	
	}
	
	public function mark_user_followup()
	{
		$cust_id = $this->input->post('cust_id', TRUE);
		$followup_remark = $this->input->post('followup_remark', TRUE);
		$followup_status = $this->input->post('followup_status', TRUE);
		$type = $this->input->post('type', TRUE);

		if($this->users->post_followup_remark($cust_id, $followup_remark, $followup_status, $type)) {
			$this->session->set_flashdata('success','Remark posted successfully!');
		} else { $this->session->set_flashdata('error','Unable to post followup remark! Try again...');	} 

		if($type == 'assign') {
			redirect('admin/user-followups-sales');
		} else { redirect('admin/followup-taken-users'); } 

		
	}

	public function followup_taken_users()
	{		
	    $userid = $this->session->userdata('userid');
	    $users_list = $this->users->get_follow_up_taken_users($userid);
	    $this->load->view('admin/users_followup_taken_list_view', compact('users_list'));			
	    $this->load->view('admin/footer_view');	
	}
	
}

/* End of file Users_followups_sales.php */
/* Location: ./application/controllers/Users_followups_sales.php */