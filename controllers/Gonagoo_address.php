<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gonagoo_address extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin');   
      $this->load->model('gonagoo_address_model', 'address');   
      $this->load->model('standard_dimension_model', 'dimension');
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $addresses = $this->address->get_addresses();
    $this->load->view('admin/gonagoo_address_list_view', compact('addresses'));     
    $this->load->view('admin/footer_view');   
  }

  public function add_address()
  {
    $countries = $this->address->get_countries();
    $this->load->view('admin/gonagoo_address_add_view', compact('countries'));
    $this->load->view('admin/footer_view');
  }

  public function register_address()
  {
    //echo json_encode($_POST); die();
    $company_name = $this->input->post('company_name', TRUE);
    $phone = $this->input->post('phone', TRUE);
    $email = $this->input->post('email', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $state_id = $this->input->post('state_id', TRUE);
    $city_id = $this->input->post('city_id', TRUE);
    $no_street_name = $this->input->post('no_street_name', TRUE);
    $terms = $this->input->post('terms', TRUE);
    $payment_details = $this->input->post('payment_details', TRUE);
    $zip_code = $this->input->post('zip_code', TRUE);
    $support_email = $this->input->post('support_email', TRUE);
    $support_phone = $this->input->post('support_phone', TRUE);
    $sales_email = $this->input->post('sales_email', TRUE);
    $sales_phone = $this->input->post('sales_phone', TRUE);
    $admin_id = $_SESSION['userid'];
    $insert_flag = false;
    
    if(!$this->address->check_country_address_exists($country_id)) {
      $insert_data = array(
        "company_name" => trim($company_name), 
        "phone" => trim($phone), 
        "email" => trim($email), 
        "country_id" => (int)$country_id, 
        "state_id" => (int)$state_id, 
        "city_id" => (int)$city_id, 
        "no_street_name" => trim($no_street_name), 
        "terms" => trim($terms), 
        "payment_details" => trim($payment_details), 
        "zip_code" => trim($zip_code), 
        "support_email" => trim($support_email), 
        "support_phone" => trim($support_phone), 
        "sales_email" => trim($sales_email), 
        "sales_phone" => trim($sales_phone), 
        "cre_datetime" => DATE('Y-m-d h:i:s'), 
        "admin_id" => (int)$admin_id, 
      );
      
      if($this->address->register_address($insert_data)) { $insert_flag = true;  }          
      else{ $insert_flag = false; }
      
      if($insert_flag){ $this->session->set_flashdata('success','Address successfully added!'); }
      else{ $this->session->set_flashdata('error','Unable to add address!');  }

    } else { $this->session->set_flashdata('error','Selected country address already exists!'); } 
    redirect('admin/add-address');
  }

  public function delete()
  {
    $addr_id = $this->input->post('id', TRUE);
    if($this->address->delete_gonagoo_address($addr_id)){ echo 'success'; } else { echo "failed";  } 
  }

  public function edit_address()
  {
    if( !is_null($this->input->post('addr_id')) ) { $addr_id = (int) $this->input->post('addr_id'); }
    else if(!is_null($this->uri->segment(3))) { $addr_id = (int)$this->uri->segment(3); }
    else { redirect('admin/gonagoo-address-admin'); }
    $address_details = $this->address->get_addresses($addr_id);
    $states = $this->address->get_states($address_details['country_id']);
    $cities = $this->address->get_cities($address_details['state_id']);
    $this->load->view('admin/gonagoo_address_edit_view', compact('address_details','states','cities'));
    $this->load->view('admin/footer_view');
  }

  public function update_address()
  { 
    //echo json_encode($_POST); die();
    $addr_id = (int) $this->input->post('addr_id');
    $company_name = $this->input->post('company_name', TRUE);
    $phone = $this->input->post('phone', TRUE);
    $email = $this->input->post('email', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $state_id = $this->input->post('state_id', TRUE);
    $city_id = $this->input->post('city_id', TRUE);
    $no_street_name = $this->input->post('no_street_name', TRUE);
    $terms = $this->input->post('terms', TRUE);
    $payment_details = $this->input->post('payment_details', TRUE);
    $zip_code = $this->input->post('zip_code', TRUE);
    $support_email = $this->input->post('support_email', TRUE);
    $support_phone = $this->input->post('support_phone', TRUE);
    $sales_email = $this->input->post('sales_email', TRUE);
    $sales_phone = $this->input->post('sales_phone', TRUE);
    $admin_id = $_SESSION['userid'];

    $update_data = array(
      "company_name" => trim($company_name), 
      "phone" => trim($phone), 
      "email" => trim($email), 
      "country_id" => (int)$country_id, 
      "state_id" => (int)$state_id, 
      "city_id" => (int)$city_id, 
      "no_street_name" => trim($no_street_name), 
      "terms" => trim($terms), 
      "payment_details" => trim($payment_details), 
      "zip_code" => trim($zip_code), 
      "support_email" => trim($support_email), 
      "support_phone" => trim($support_phone), 
      "sales_email" => trim($sales_email), 
      "sales_phone" => trim($sales_phone), 
      "cre_datetime" => DATE('Y-m-d h:i:s'), 
      "admin_id" => (int)$admin_id, 
    );

    if($this->address->update_gonagoo_address($update_data, $addr_id)){
      $this->session->set_flashdata('success','Address successfully updated!');
    } else {  $this->session->set_flashdata('error','Unable to update address details! Try again...');  }  
    
    redirect('admin/edit-address/'.$addr_id);
  }

}

/* End of file Gonagoo_address.php */
/* Location: ./application/controllers/Gonagoo_address.php */