<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_dangerous_goods extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('advance_payment_model', 'payment');
      $this->load->model('standard_dimension_model', 'standard_dimension');   
      $this->load->model('Web_user_model', 'user');   
      $this->load->model('dangerous_goods_model', 'goods');   
      $this->load->model('country_dangerous_goods_model', 'cg');   
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $goods = $this->cg->get_country_dangerous_goods();
    $this->load->view('admin/country_dangerous_goods_list_view',compact('goods'));      
    $this->load->view('admin/footer_view');   
  }

  public function add()
  {
    $countries = $this->payment->get_countries();
    $goods = $this->goods->get_dangerous_goods();
    $this->load->view('admin/country_dangerous_goods_add_view',compact('goods','countries'));      
    $this->load->view('admin/footer_view');   
  }  

  public function register()
  {
    $country_id = $this->input->post('country_id', TRUE);
    $added_goods = $this->input->post('added_goods', TRUE);
    $goods_ids = implode(',', $added_goods);

    if(!$this->cg->check_country_existance($country_id)){
      $data = array('country_id' => $country_id, 'goods_ids' => $goods_ids);
      if($this->cg->register($data)){
        $this->session->set_flashdata('success','Country-wise Dangerous goods successfully added!');
      } else{  $this->session->set_flashdata('error','Unable to add Country-wise  Dangerous goods! Try again...'); }   
    } else{ $this->session->set_flashdata('error','Country has Dangerous goods!');  }   
    redirect('admin/country-dangerous-goods/add');
  }

  public function edit()
  {
    $id = $this->uri->segment(4);
    if($cg = $this->cg->get_country_dangerous_goods($id)){
      $goods = $this->goods->get_dangerous_goods();
      $this->load->view('admin/country_dangerous_goods_edit_view',compact('goods','cg'));      
      $this->load->view('admin/footer_view');   
    }else { redirect('admin/country-dangerous-goods');}
  }

  public function update()
  {
    $id = $this->input->post('id', TRUE);
    $added_goods = $this->input->post('added_goods', TRUE);
    $goods_ids = implode(',', $added_goods);

    $data = array( 'goods_ids' => $goods_ids);
    if($this->cg->update($data, $id)){
      $this->session->set_flashdata('success','Country-wise Dangerous goods successfully updatd!');
    } else{  $this->session->set_flashdata('error','Unable to update Country-wise  Dangerous goods! Try again...'); }   
    redirect('admin/country-dangerous-goods/edit/'.$id);
  }



}

/* End of file country_dangerous_goods.php */
/* Location: ./application/controllers/country_dangerous_goods.php */