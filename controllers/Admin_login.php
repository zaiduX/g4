<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Admin_model', 'admin');   
  }

  public function index()
  {
    if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
  }

  public function validate_login()
  {
    $email = $this->input->post('user_email',TRUE);
    $password = $this->input->post('user_pass',TRUE);
    
    if( $auth = $this->admin->user_authentication(trim($email), trim($password)) ) {  
      $this->session->set_userdata( ['userid' => $auth['auth_id'],'email' => $email, 'is_admin_logged_in' => TRUE, 'auth_type_id' => $auth['auth_type_id'], 'admin_country_id' => $auth['country_id'] , 'default_timezone' => ini_get('date.timezone')]);      
      echo 'success';
    } 
    else {
      $this->session->set_flashdata('errors',"Invalid Email / Password!" );
      $this->session->set_flashdata('email', $email );
      echo 'invalid';
    }
  }

  public function logout()
  {
    $this->session->unset_userdata('userid');
    $this->session->unset_userdata('email');
    $this->session->unset_userdata('is_admin_logged_in');
    redirect('admin');
  }

}

/* End of file Admin_Login.php */
/* Location: ./application/controllers/Admin_Login.php */