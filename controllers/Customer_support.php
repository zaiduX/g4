<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_support extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Customer_support_model', 'support');
                        $this->load->model('Notification_model', 'notice');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function get_customer_support_list()
	{
		$auth_type_id = $this->session->userdata('auth_type_id');
	    if($auth_type_id > 1) {   $admin_country_id = $this->session->userdata('admin_country_id'); }
	    else { $admin_country_id = 0; }
		$customer_support_list = $this->support->get_support_list($admin_country_id);
		//echo json_encode($customer_support_list); die();
		$this->load->view('admin/customer_support_list_view', compact('customer_support_list'));			
		$this->load->view('admin/footer_view');		
	}
	
	public function reply_support_request()
	{
		if( !is_null($this->input->post('query_id')) ) { $query_id = (int) $this->input->post('query_id'); }
		else if(!is_null($this->uri->segment(3))) { $query_id = (int)$this->uri->segment(3); }
		else { redirect('user-panel/customer-support-list'); }
		$request_details = $this->support->get_support_request_details($query_id);
		$this->load->view('admin/reply_support_request_view', compact('request_details'));
		$this->load->view('admin/footer_view');
	}
	
	public function update_support_request()
	{
		$query_id = $this->input->post('query_id', TRUE); $query_id = (int) $query_id;
		$response = $this->input->post('response', TRUE);
		$status = $this->input->post('status', TRUE);
		$update_data = array();
		$today = date('Y-m-d h:i:s');
		
		$update_data = array(
			"response" => trim($response),
			"res_datetime" => $today,
			"status" => trim($status),
			"updated_by" => 'Gonagoo Admin',
		);

		if( $this->support->update_customer_support_request($query_id, $update_data)){
			$this->session->set_flashdata('success','response details saved successfully!');
		} else { $this->session->set_flashdata('error','Unable to update details! Try again...'); }
			
		redirect('admin/reply-support-request/'.$query_id);
	}

}

/* End of file Customer_support.php */
/* Location: ./application/controllers/Customer_support.php */