<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract_expiry_delay extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Contract_expiry_delay_model', 'expiry');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$user = $this->admin->logged_in_user_details($user['type_id']);
			$permissions = $this->admin->get_auth_permissions($id);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	public function index() {
		$delay_hours = $this->expiry->get_contract_delay_hours();
		$countries = $this->expiry->get_countries();
		$this->load->view('admin/services/contract_delay_hours_list_view', compact('delay_hours','countries'));			
		$this->load->view('admin/footer_view');
	}
	public function register() {
		//echo json_encode($_POST); die();
		$hours = $this->input->post('hours', TRUE);
		$country_id = $this->input->post('country_id', TRUE); $country_id = (int)$country_id;
		$expiry_id = $this->input->post('expiry_id', TRUE); $expiry_id = (int)$expiry_id;
		if($expiry_id == 0) { //Register
	 		$insert_data = array(
	 			"hours" => trim($hours),
	 			"country_id" => trim($country_id),
	 			"cre_datetime" => date("Y-m-d H:i:s"),
	 			"update_datetime" => date("Y-m-d H:i:s"),
	 		);
	 		if($this->expiry->register_delay_hours($insert_data)) {
	 			$this->session->set_flashdata('success', 'Hours successfully added!');
	 		} else { $this->session->set_flashdata('error', 'Unable to add dispute hours! Try again...'); }
		} else { //Update
			$update_data = array(
	 			"hours" => trim($hours),
	 			"update_datetime" => date("Y-m-d H:i:s"),
	 		);
	 		if($this->expiry->update_delay_hours($update_data, $expiry_id)){
				$this->session->set_flashdata('success','Hours successfully updated!');
			} else { 	$this->session->set_flashdata('error','Unable to update hours! Try again...');	} 
		}
 		redirect('admin/contract-expiry-delay');
	}
	public function delete() {
		$expiry_id = $this->input->post('expiry_id', TRUE);
		if($this->expiry->delete_delay_hours($expiry_id)) { echo 'success';
		} else { echo 'failed';	}
	}
}

/* End of file Contract_expiry_delay.php */
/* Location: ./application/controllers/Contract_expiry_delay.php */