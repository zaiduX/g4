<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_dispute extends CI_Controller {
	private $admin_id = 0; 
	private $admin_profile = array();

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Api_model_services', 'api');
			$this->load->model('Job_dispute_model', 'dispute');	
			$this->load->model('Api_model_sms', 'api_sms');	
			$id = $this->session->userdata('userid');
    	$this->admin_id = $this->session->userdata("userid"); $this->admin_id = (int) $this->admin_id;
    	$this->admin_profile = $this->admin->logged_in_user_details($this->admin_id, true); $admin = $this->admin_profile;
			$user = $this->admin->logged_in_user_details($id);
			$user = $this->admin->logged_in_user_details($user['type_id']);
			$permissions = $this->admin->get_auth_permissions($id);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	public function index()
	{
		$job_dispute = $this->dispute->get_job_dispute();
		// echo json_encode($job_dispute); die();
		$this->load->view('admin/services/job_dispute_list_view', compact('job_dispute'));			
		$this->load->view('admin/footer_view');
	}
	public function customer_chat()
  {
  	// echo json_encode($_POST); die();
    // $cust_id = $this->input->post('customer_id', TRUE); $cust_id = (int)$cust_id;
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $job_details = $this->api->get_customer_job_details($job_id);
    $cust_id =  $job_details['cust_id'];
    $login_customer_details = $this->admin->logged_in_user_details($this->admin_id);
    $provider_id = $job_details['provider_id'] ;
    $workroom = $this->api->get_job_workstream_details_admin($job_id , $cust_id , $provider_id , "customer");
    $job_provider_profile = $this->api->get_service_provider_profile((int)$cust_id);
    $job_provider_details = $this->api->get_user_details((int)$cust_id);
    $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
    $keys = ['provider_id' => (int)$job_provider_profile['cust_id'], 'cust_id' => $cust_id, 'job_id' => $job_id];
    // echo json_encode($job_provider_details); die();
    $this->load->view('admin/services/admin_customer_job_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'job_provider_profile', 'keys', 'job_provider_details', 'job_customer_details'));
		$this->load->view('admin/footer_view');
  }
  public function add_job_workroom_chat_ajax()
  {
    //echo json_encode($_POST); die();
    // $cust = $this->cust;
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $type = $this->input->post('type', TRUE);
    $job_details = $this->api->get_customer_job_details($job_id); 
    $text_msg = $this->input->post('message_text', TRUE);
    $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
    $provider_id = $job_details['provider_id']; $provider_id = (int)$provider_id;
    $sender_id = $this->admin_id;
    $today = date("Y-m-d H:i:s");

    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/'; 
      $config['allowed_types']  = '*'; 
      $config['max_size']       = '0'; 
      $config['max_width']      = '0'; 
      $config['max_height']     = '0'; 
      $config['encrypt_name']   = true;
      $this->load->library('upload', $config);
      $this->upload->initialize($config); 
      if($this->upload->do_upload('attachment')){
        $uploads    = $this->upload->data();  
        $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
      } else { $attachment_url = "NULL"; }
    } else { $attachment_url = "NULL"; }

    if($type =="customer"){
    	if($workstream_details = $this->api->get_workstream_details_by_job_id_admin($job_id ,$cust_id , "customer" )) {
	      $insert_data = array(
	        'workstream_id' => (int)$workstream_details['workstream_id'],
	        'job_id' => $job_id,
	        'cust_id' => $cust_id,
	        // 'provider_id' => $provider_id,
	        'admin_id' => $this->admin_id,
	        'is_admin' => 1,
	        'text_msg' => $text_msg,
	        'attachment_url' =>  $attachment_url,
	        'cre_datetime' => $today,
	        'sender_id' => $sender_id,
	        'type' => ($attachment_url=='NULL')?'chat':'attachment',
	        'file_type' => ($attachment_url=='NULL')?'text':'file',
	        'proposal_id' => 0,
	        "ratings" => "NULL",
	      );
	      //echo json_encode($insert_data); die();
	      $this->api->update_to_workstream_details($insert_data);

	      //Notifications to Provider----------------
	        $api_key = $this->config->item('delivererAppGoogleKey');

	        if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
	        } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

	        if(!is_null($device_details)) {
	          $arr_fcm_ids = array(); $arr_apn_ids = array();
	          foreach ($device_details as $value) {
	            if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
	            else {  array_push($arr_apn_ids, $value['reg_id']); }
	          }
	          $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
	          $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
	          //APN
	          $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
	          if(is_array($arr_apn_ids)) { 
	            $arr_apn_ids = implode(',', $arr_apn_ids);
	            $this->api_sms->sendPushIOS($msg_apn_customer, $arr_apn_ids, $api_key);
	          }
	        }
	      //------------------------------------------
	    }
	    return json_encode(true);
    }elseif($type =="provider"){
    	
    	if($workstream_details = $this->api->get_workstream_details_by_job_id_admin($job_id ,$cust_id , "customer" )) {
	      $insert_data = array(
	        'workstream_id' => (int)$workstream_details['workstream_id'],
	        'job_id' => $job_id,
	        // 'cust_id' => $cust_id,
	        'provider_id' => $provider_id,
	        'admin_id' => $this->admin_id,
	        'is_admin' => 1,
	        'text_msg' => $text_msg,
	        'attachment_url' =>  $attachment_url,
	        'cre_datetime' => $today,
	        'sender_id' => $sender_id,
	        'type' => ($attachment_url=='NULL')?'chat':'attachment',
	        'file_type' => ($attachment_url=='NULL')?'text':'file',
	        'proposal_id' => 0,
	        "ratings" => "NULL",
	      );
	      //echo json_encode($insert_data); die();
	      $this->api->update_to_workstream_details($insert_data);

	      //Notifications to Provider----------------
	        $api_key = $this->config->item('delivererAppGoogleKey');

	        if($job_details['provider_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
	        } else { $device_details = $this->api->get_user_device_details((int)$job_details['provider_id']); }

	        if(!is_null($device_details)) {
	          $arr_fcm_ids = array(); $arr_apn_ids = array();
	          foreach ($device_details as $value) {
	            if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
	            else {  array_push($arr_apn_ids, $value['reg_id']); }
	          }
	          $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
	          $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
	          //APN
	          $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
	          if(is_array($arr_apn_ids)) { 
	            $arr_apn_ids = implode(',', $arr_apn_ids);
	            $this->api_sms->sendPushIOS($msg_apn_customer, $arr_apn_ids, $api_key);
	          }
	        }
	      //------------------------------------------
	    }
	    return json_encode(true);
    }
  }
	public function customer_job_workroom_ajax()
  {
    $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $chat_count = $this->input->post('chat_count', TRUE); $chat_count = (int)$chat_count;
    // $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
    $job_details = $this->api->get_job_details($job_id);
    $provider_id  = $job_details['provider_id'];
   	$login_customer_details = $this->admin->logged_in_user_details($this->admin_id);
    $job_provider_profile = $this->api->get_service_provider_profile((int)$provider_id);
    $job_customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
    $job_provider_details = $this->api->get_user_details((int)$provider_id);
    $workroom_details = $this->api->get_job_workstream_details_admin($job_id , $cust_id , $provider_id , "customer");
    $new_chat_count = sizeof($workroom_details);
    if(sizeof($workroom_details) > $chat_count) {
      $message_template ='';
      $message_template .= "<input type='hidden' value='$new_chat_count' id='chat_count' />";
      foreach ($workroom_details as $v => $workroom) { $tabindex = ($new_chat_count==($v+1))?'tabindex=1':'';
        date_default_timezone_set($this->session->userdata("default_timezone")); 
        $ud = strtotime($workroom['cre_datetime']);
        date_default_timezone_set($this->session->userdata("default_timezone"));

        $chat_side = ($workroom['sender_id'] == 0 || $workroom['admin_id'] != 0)?'right':'left';
        $message_template .= "<li class='chat-message $chat_side'>";

        if($workroom['sender_id'] == 0 || $workroom['admin_id'] != 0 ) {
          $url = base_url($login_customer_details['avatar_url']);
          $message_template .= "<img class='message-avatar' style='margin-right: 0px;' src='$url' />";
          $message_template .= "<div class='message' style='margin-left: 0px;'>";
            $cust_name = $login_customer_details['fname'].' '.$login_customer_details['lname'];
            $message_template .= "<a class='message-author'>$cust_name</a>";
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice"):
                $message_template .= "<span class='message-content'>";
                  if($workroom['attachment_url'] != 'NULL'):
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<p>&nbsp;</p>";
                    $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ):
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_accepted"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone accepted')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_rejected"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone rejected')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Send')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_decline"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('proposal decline')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .= "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        } else {
          $url = base_url(($workroom['sender_id'] == $job_details['cust_id']) ? $job_customer_details['avatar_url'] : $job_provider_details['avatar_url']);
          $message_template .= "<img class='message-avatar' src='$url' />";
          $message_template .= "<div class='message'>";
            $user_name = ($workroom['sender_id']==$job_details['cust_id']) ? $job_customer_details['firstname'].' '.$job_customer_details['lastname'] : $job_provider_details['firstname'].' '.$job_provider_details['lastname'];
            $message_template .= "<a class='message-author text-left'>$user_name</a>";
            $ud = strtotime($workroom['cre_datetime']);
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                $message_template .= "<span class='message-content'>";
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p>$msg</p>";
                  $message_template .= "</div>";
                  if($workroom['attachment_url'] != 'NULL') :
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<p>&nbsp;</p>";
                      $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_send"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .=  "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        }
        $message_template .= "</li>";
      }
    } else { $message_template = false; }
    echo json_encode($message_template); die();
  }
	public function freelancer_chat()
	{
  	// echo json_encode($_POST); die();
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $job_details = $this->api->get_customer_job_details($job_id);
  	$cust_id = $job_details['cust_id'];
  	$provider_id = $job_details['provider_id'];
    // echo json_encode($job_details); die();
    $login_customer_details = $this->admin->logged_in_user_details($this->admin_id);
    $workroom = $this->api->get_job_workstream_details_admin($job_id , $cust_id , $provider_id , "provider");
    $job_customer_details = $this->api->get_user_details((int)$provider_id);
    $job_provider_details = $this->api->get_user_details((int)$provider_id);
    $job_provider_profile = $this->api->get_service_provider_profile((int)$provider_id);
    $keys = ['cust_id' => $cust_id, 'job_id' => $job_id , 'provider_id' =>$provider_id];
    $this->load->view('admin/services/admin_provider_job_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'job_provider_profile', 'keys', 'job_customer_details', 'job_provider_details'));
		$this->load->view('admin/footer_view');
  }
  public function provider_job_workroom_ajax()
  {
    $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $chat_count = $this->input->post('chat_count', TRUE); $chat_count = (int)$chat_count;
    // $provider_id = $this->input->post('provider_id', TRUE); $provider_id = (int)$provider_id;
    $job_details = $this->api->get_job_details($job_id);
    $cust_id  = $job_details['cust_id'];
   	$login_customer_details = $this->admin->logged_in_user_details($this->admin_id);
    $job_provider_profile = $this->api->get_service_provider_profile((int)$provider_id);
    $job_customer_details = $this->api->get_user_details((int)$job_details['provider_id']);
    $job_provider_details = $this->api->get_user_details((int)$provider_id);
    $workroom_details = $this->api->get_job_workstream_details_admin($job_id , $cust_id , $provider_id , "provider");
    $new_chat_count = sizeof($workroom_details);
    if(sizeof($workroom_details) > $chat_count) {
      $message_template ='';
      $message_template .= "<input type='hidden' value='$new_chat_count' id='chat_count' />";
      foreach ($workroom_details as $v => $workroom) { $tabindex = ($new_chat_count==($v+1))?'tabindex=1':'';
        date_default_timezone_set($this->session->userdata("default_timezone")); 
        $ud = strtotime($workroom['cre_datetime']);
        date_default_timezone_set($this->session->userdata("default_timezone"));

        $chat_side = ($workroom['sender_id'] == 0 || $workroom['admin_id'] != 0)?'right':'left';
        $message_template .= "<li class='chat-message $chat_side'>";

        if($workroom['sender_id'] == 0 || $workroom['admin_id'] != 0 ) {
          $url = base_url($login_customer_details['avatar_url']);
          $message_template .= "<img class='message-avatar' style='margin-right: 0px;' src='$url' />";
          $message_template .= "<div class='message' style='margin-left: 0px;'>";
            $cust_name = $login_customer_details['fname'].' '.$login_customer_details['lname'];
            $message_template .= "<a class='message-author'>$cust_name</a>";
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice"):
                $message_template .= "<span class='message-content'>";
                  if($workroom['attachment_url'] != 'NULL'):
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<p>&nbsp;</p>";
                    $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ):
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_accepted"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone accepted')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_rejected"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone rejected')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Send')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_decline"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('proposal decline')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .= "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        } else {
          $url = base_url(($workroom['sender_id'] == $job_details['cust_id']) ? $job_customer_details['avatar_url'] : $job_provider_details['avatar_url']);
          $message_template .= "<img class='message-avatar' src='$url' />";
          $message_template .= "<div class='message'>";
            $user_name = ($workroom['sender_id']==$job_details['cust_id']) ? $job_customer_details['firstname'].' '.$job_customer_details['lastname'] : $job_provider_details['firstname'].' '.$job_provider_details['lastname'];
            $message_template .= "<a class='message-author text-left'>$user_name</a>";
            $ud = strtotime($workroom['cre_datetime']);
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                $message_template .= "<span class='message-content'>";
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p>$msg</p>";
                  $message_template .= "</div>";
                  if($workroom['attachment_url'] != 'NULL') :
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<p>&nbsp;</p>";
                      $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_send"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .=  "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        }
        $message_template .= "</li>";
      }
    } else { $message_template = false; }
    echo json_encode($message_template); die();
  }
	public function group_chat()
  {
    // echo json_encode($_POST); die();
    // $cust_id = $this->input->post('customer_id', TRUE); $cust_id = (int)$cust_id;
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $job_details = $this->api->get_customer_job_details($job_id);
    $cust_id =  $job_details['cust_id'];
    $provider_id = $job_details['provider_id'];
    $login_customer_details = $this->admin->logged_in_user_details($this->admin_id);
    
    $workroom = $this->api->get_job_workstream_details_admin_group($job_id);
    $provider = $this->api->get_user_details((int)$provider_id);
    $customer = $this->api->get_user_details((int)$cust_id);
    // echo json_encode($workroom); die();

    $keys = ['provider_id' => $provider_id, 'cust_id' => $cust_id, 'job_id' => $job_id];
    // echo json_encode($job_provider_details); die();
    $this->load->view('admin/services/admin_group_job_workroom_view',compact('job_details', 'workroom', 'login_customer_details', 'provider', 'keys', 'customer'));
    $this->load->view('admin/footer_view');
  }
  public function group_job_workroom_ajax()
  {
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $chat_count = $this->input->post('chat_count', TRUE); $chat_count = (int)$chat_count;
    $job_details = $this->api->get_job_details($job_id);
    $cust_id  = $job_details['cust_id'];
    $provider_id  = $job_details['provider_id'];
    $login_customer_details = $this->admin->logged_in_user_details($this->admin_id);
    $workroom_details = $this->api->get_job_workstream_details_admin_group($job_id);
    $provider = $this->api->get_user_details((int)$provider_id);
    $customer = $this->api->get_user_details((int)$cust_id);

    $new_chat_count = sizeof($workroom_details);
    if(sizeof($workroom_details) > $chat_count) {
      $message_template ='';
      $message_template .= "<input type='hidden' value='$new_chat_count' id='chat_count' />";
      foreach ($workroom_details as $v => $workroom) { $tabindex = ($new_chat_count==($v+1))?'tabindex=1':'';
        date_default_timezone_set($this->session->userdata("default_timezone")); 
        $ud = strtotime($workroom['cre_datetime']);
        date_default_timezone_set($this->session->userdata("default_timezone"));

        $chat_side = ($workroom['sender_id'] == 0 || $workroom['admin_id'] != 0)?'right':'left';
        $message_template .= "<li class='chat-message $chat_side'>";

        if($workroom['sender_id'] == 0 || $workroom['admin_id'] != 0 ) {
          $url = base_url($login_customer_details['avatar_url']);
          $message_template .= "<img class='message-avatar' style='margin-right: 0px;' src='$url' />";
          $message_template .= "<div class='message' style='margin-left: 0px;'>";
            $cust_name = $login_customer_details['fname'].' '.$login_customer_details['lname'];
            $message_template .= "<a class='message-author'>$cust_name</a>";
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice"):
                $message_template .= "<span class='message-content'>";
                  if($workroom['attachment_url'] != 'NULL'):
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<p>&nbsp;</p>";
                    $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ):
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_accepted"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone accepted')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_rejected"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone rejected')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Send')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_decline"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('proposal decline')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .= "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        } else {
          $url = base_url(($workroom['sender_id'] == $job_details['cust_id']) ? $customer['avatar_url'] : $provider['avatar_url']);
          $message_template .= "<img class='message-avatar' src='$url' />";
          $message_template .= "<div class='message'>";
            $user_name = ($workroom['sender_id']==$job_details['cust_id']) ? $customer['firstname'].' '.$customer['lastname'] : $provider['firstname'].' '.$provider['lastname'];
            $message_template .= "<a class='message-author text-left'>$user_name</a>";
            $ud = strtotime($workroom['cre_datetime']);
            $message_template .= "<span class='message-date'>".date('l, M / d / Y  h:i a', $ud)."</span>";
            $message_template .= "<div class='row'>";

              if($workroom['type'] == "chat" || $workroom['type'] == "job_invite" ||  $workroom['type'] == "attachment" || $workroom['type'] == "cancel_request" || $workroom['type'] == "cancel_withdraw" || $workroom['type'] == "job_cancelled" || $workroom['type'] == "dispute_raised" || $workroom['type'] == "dispute_withdraw" || $workroom['type'] == "mark_completed" || $workroom['type'] == "job_completed" || $workroom['type'] == "payment_invoice") :
                $message_template .= "<span class='message-content'>";
                  $div_size = ($workroom['attachment_url'] == 'NULL')?12:9;
                  $message_template .= "<div class='col-xl-$div_size col-lg-$div_size col-md-$div_size col-sm-$div_size col-xs-$div_size text-left'>";
                    if(trim($workroom["text_msg"]) != "NULL" && trim($workroom["text_msg"]) != "" ) :
                      $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('message')."</h5>";
                    endif;
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p>$msg</p>";
                  $message_template .= "</div>";
                  if($workroom['attachment_url'] != 'NULL') :
                    $message_template .= "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left'>";
                      $message_template .= "<p>&nbsp;</p>";
                      $message_template .= "<p><a href='".base_url($workroom['attachment_url'])."' target='_blank'>".$this->lang->line('download')." <i class='fa fa-download'></i></a></p>";
                    $message_template .= "</div>";
                  endif;
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_started"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Job started')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "payment"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Payment completed')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "milestone_send"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Milestone received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_proposal"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Proposal Received')."</h5>";
                    $msg = (trim($workroom["text_msg"]=='NULL'))?'':trim($workroom["text_msg"]);
                    $message_template .= "<p style='text-align: left;'>$msg</p>";
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;
              if($workroom['type'] == "job_rating" || $workroom['type'] == "customer_rating"):
                $message_template .= "<span class='message-content'>";
                  $message_template .= "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left'>";
                    $message_template .= "<h5 style='text-align: left;'>".$this->lang->line('Review and ratings')."</h5>";
                    for($i=0; $i < (int) $workroom['ratings']; $i++) { $message_template .= ' <i class="fa fa-star"></i> '; } 
                    for($i=0; $i < ( 5-(int) $workroom['ratings']); $i++) { $message_template .= ' <i class="fa fa-star-o"></i> '; } 
                    $message_template .=  "( ".$workroom['ratings']." / 5 )";
                    $message_template .= (trim($workroom['text_msg'])!= 'NULL')? "<p>".trim($workroom['text_msg'])."</p>" : '';
                  $message_template .= "</div>";
                $message_template .= "</span>";
              endif;

            $message_template .= "</div>";
          $message_template .= "</div>";
        }
        $message_template .= "</li>";
      }
    } else { $message_template = false; }
    echo json_encode($message_template); die();
  }
  public function add_job_workroom_group_chat_ajax()
  {
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $job_details = $this->api->get_customer_job_details($job_id); 
    $text_msg = $this->input->post('message_text', TRUE);
    $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
    $provider_id = $job_details['provider_id']; $provider_id = (int)$provider_id;
    $sender_id = $this->admin_id;
    $today = date("Y-m-d H:i:s");

    if( !empty($_FILES["attachment"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/attachements/'; 
      $config['allowed_types']  = '*'; 
      $config['max_size']       = '0'; 
      $config['max_width']      = '0'; 
      $config['max_height']     = '0'; 
      $config['encrypt_name']   = true;
      $this->load->library('upload', $config);
      $this->upload->initialize($config); 
      if($this->upload->do_upload('attachment')){
        $uploads    = $this->upload->data();  
        $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
      } else { $attachment_url = "NULL"; }
    } else { $attachment_url = "NULL"; }
      
    if($workstream_details = $this->api->get_workstream_details_by_job_id_admin($job_id ,$cust_id , "customer" )) {
      $insert_data = array(
        'workstream_id' => (int)$workstream_details['workstream_id'],
        'job_id' => $job_id,
        'cust_id' => $cust_id,
        'provider_id' => $provider_id,
        'admin_id' => $this->admin_id,
        'is_admin' => 1,
        'text_msg' => $text_msg,
        'attachment_url' =>  $attachment_url,
        'cre_datetime' => $today,
        'sender_id' => $sender_id,
        'type' => ($attachment_url=='NULL')?'chat':'attachment',
        'file_type' => ($attachment_url=='NULL')?'text':'file',
        'proposal_id' => 0,
        "ratings" => "NULL",
      );
      //echo json_encode($insert_data); die();
      $this->api->update_to_workstream_details($insert_data);
    }
    return json_encode(true);
  }
  public function about_dispute()
  {
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $dispute_id = $this->input->post('dispute_id', TRUE); $dispute_id = (int)$dispute_id;
    $job_details = $this->api->get_job_details($job_id);
    $dispute_details = $this->api->get_job_dispute_details($job_id);
    $this->load->view('admin/services/job_dispute_details_view',compact('job_details','dispute_details'));
    $this->load->view('admin/footer_view');
  }
  public function dispute_job_cancel()
  {
    $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
    $dispute_id = $this->input->post('dispute_id', TRUE); $dispute_id = (int)$dispute_id;
    $today = date('Y-m-d H:i:s');

    $update_data = array(
      "cancellation_status" => 'dispute_closed',
      "job_status" => 'cancelled',
      "job_status_datetime" => $today,
    );

    $update_data_dispute = array(
      "dispute_closed_datetime" => $today,
      "admin_id" => $this->admin_id,
      "dispute_status" => "closed",
    );
    if($this->api->update_job_details($job_id, $update_data) && $this->api->update_job_dispute($dispute_id, $update_data_dispute) ) {
      $job_details = $this->api->get_job_details($job_id);
      $cust_id = $job_details['cust_id'];
      $provider_id = $job_details['provider_id'];
      $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
      $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
      
      if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
        $customer_name = explode("@", trim($customer_details['email1']))[0];
      } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

      if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
        $provider_name = explode("@", trim($provider_details['email_id']))[0];
      } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }  
      //Notifications to customers----------------
        $message = "Hello ".$customer_name.", Your job ID : ".$job_id." has been cancelled by Admin";
        //Send Push Notifications
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
        if(!is_null($device_details_customer)) {
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => "Job cancellation", 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
          $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
          //APN
          $msg_apn_customer =  array('title' => 'Job cancellation.', 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }
        //SMS Notification
        $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
        $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

        //Email Notification
        $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), 'Offer purchase cancellation.', trim($message));
      //------------------------------------------
      //Notifications to Provider-----------------
        $message = "Hello ".$provider_name.", Your job ID : ".$job_id." has been cancelled by Admin";
        //Send Push Notifications
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
        if(!is_null($device_details_provider)) {
          $arr_provider_fcm_ids = array();
          $arr_provider_apn_ids = array();
          foreach ($device_details_provider as $value) {
            if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => 'Job cancellation.', 'type' => 'job-cancel-notice', 'notice_date' => $today, 'desc' => $message);
          $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
          //APN
          $msg_apn_provider =  array('title' => 'Job cancellation.', 'text' => $message);
          if(is_array($arr_provider_apn_ids)) { 
            $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
            $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
          }
        }
        //SMS Notification
        $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
        $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

        //Email Notification
        $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), 'Offer purchase cancellation.', trim($message));
      //------------------------------------------
      $message = "This job has been cancelled by Admin.";
      if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
        $update_data = array(
          'workstream_id' => (int)$workstream_details['workstream_id'],
          'job_id' => (int)$job_id,
          'cust_id' => (int)$job_details['cust_id'],
          'provider_id' => (int)$job_details['provider_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'admin_id' => $this->admin_id,
          'sender_id' => $this->admin_id,
          'is_admin' => 1,
          'type' => 'job_cancelled',
          'file_type' => 'text',
          'proposal_id' => 0,
        );
        $this->api->update_to_workstream_details($update_data);
      }      
      $this->session->set_flashdata('success', "Dispute close job cancelled by admin");
    } else { $this->session->set_flashdata('error', "Error occurred in closing dispute"); }
    redirect(base_url('manage-dispute'), 'refresh');
  }
	
}

/* End of file Manage job dispute.php */
/* Location: ./application/controllers/Manage job dispute.php */