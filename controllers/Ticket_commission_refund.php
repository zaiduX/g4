<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Ticket_commission_refund extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('Ticket_commission_refund_model', 'refund');
      $this->load->model('api_model_bus', 'api_bus');  
      $this->load->model('Api_model_sms', 'api_sms');  
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else { redirect('admin'); }
  }
  public function index()
  {
    if(!$this->session->userdata('is_admin_logged_in')) { $this->load->view('admin/login_view'); } else { redirect('admin/dashboard');}
  }
  public function commission_refund_request()
  {
    $auth_type_id = $this->session->userdata('auth_type_id');
    if($auth_type_id > 1) {   $admin_country_id = $this->session->userdata('admin_country_id'); }
    else { $admin_country_id = 0; }
    $commission_refund_request_list = $this->refund->get_commission_refund_request($admin_country_id);
    //echo json_encode($commission_refund_request_list); die();
    $this->load->view('admin/commission_refund_request_list_view', compact('commission_refund_request_list')); 
    $this->load->view('admin/footer_view');   
  }
  public function reject_refund_request()
  {
    $id = $this->input->post('id', TRUE);
    $req_id = explode('~', $id)[0];
    $seat_id = explode('~', $id)[1];
    if( $this->refund->reject_refund_request( (int) $req_id) ) {  
      //update refund status in booking
      $update_data = array(
          "commission_refund" => 0,
      );
      $this->api_bus->update_bus_booking_seat_details($update_data, (int)$seat_id);

      $seat_details = $this->api_bus->get_seat_details((int)$seat_id);
      $operator_details = $this->api_bus->get_user_details((int)$seat_details['operator_id']);

      $mobile = $this->api_bus->get_country_code_by_id($operator_details['country_id']).$operator_details['mobile1'];
      $subject = 'Gonagoo - Refund Commission request status.';
      $message = 'Your request for commission has been rejected. Please contact Gonagoo support for further details. Request ID: ' . $req_id;
      $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
      $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
      $messageBody .="</head><body bgcolor='#FFFFFF'>";
      $messageBody .= "<h3> Dear " . trim($operator_details['firstname']) . ' ' . trim($operator_details['lastname']). "</h3>"; 
      $messageBody .= "<h4>" . $message . "</h4><br /><br /><br />";
      //Email Signature
      $messageBody .="<table class='head-wrap'>";
      $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
      $messageBody .="<tr><td>Your Gonagoo team!</td></tr>";
      $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />".$this->lang->line('slider_heading1')."</td></tr>";
      $messageBody .="<tr><td>Support : support@gonagoo.com</td></tr>";
      $messageBody .="<tr><td>Website : www.gonagoo.com</td></tr>";
      //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
      $messageBody .="<tr><td>Join us on : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
      $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
      $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
      $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
      //Download the App <icon for iOS App download><icon for Android App download>
      $messageBody .="<tr><td>Download the App : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
      $messageBody .="<a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
      $messageBody .="</table></div></td><td></td></tr></table>";
      //Email Signature End
      $messageBody .="</body></html>";  

      require_once('resources/PHPMailer-master/src/Exception.php');
      require_once('resources/PHPMailer-master/src/PHPMailer.php');
      $email = new PHPMailer();
      $email->SetFrom($this->config->item('from_email'), 'Gonagoo');
      $email->isHTML(true);
      $email->Subject   = $subject;
      $email->Body      = $messageBody;
      $email->AddAddress(trim($operator_details['email1']));
      $email->Send();

      $this->api_bus->sendSMS(trim($mobile), $message);
      echo 'success';
    } else {  echo 'failed';  }
  }
  public function accept_refund_request()
  {
    $id = $this->input->post('id', TRUE);
    $req_id = explode('~', $id)[0];
    $seat_id = explode('~', $id)[1];
    $today = date('Y-m-d h:i:s');
    if( $this->refund->accept_refund_request( (int) $req_id) ) {  
      //get seat details
      $seat_details = $this->api_bus->get_seat_details((int)$seat_id);
      //Trip operator details
      $operator_details = $this->api_bus->get_user_details((int)$seat_details['operator_id']);
      //get refund request details
      $request_details = $this->refund->get_refund_request_details((int)$req_id);
      //calculate commission
      $commission_amount = round(($request_details['seat_price']/100) * $seat_details['comm_percentage'],2);
      
      //Payment Module-----------------------------------------
        //Deduct Ownward Trip Commission To Gonagoo Account
          if($gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($request_details['currency_sign']))) {
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($request_details['currency_sign']),
            );
            $this->api_bus->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($request_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($commission_amount);
          $this->api_bus->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($request_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$seat_details['ticket_id'],
            "user_id" => (int)trim($seat_details['operator_id']),
            "type" => 0,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($commission_amount),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => 'NULL',
            "transfer_account_number" => 'NULL',
            "bank_name" => 'NULL',
            "account_holder_name" => 'NULL',
            "iban" => 'NULL',
            "email_address" => $operator_details['email1'],
            "mobile_number" => $operator_details['mobile1'],
            "transaction_id" => 'NULL',
            "currency_code" => trim($request_details['currency_sign']),
            "country_id" => $operator_details['country_id'],
            "cat_id" => '281',
          );
          $this->api_bus->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
        //Deduct Ownward Trip Commission To Gonagoo Account
        //Add Ownward Trip Commission From Operator Account
          if($customer_account_master_details = $this->api_bus->customer_account_master_details(trim($seat_details['operator_id']), trim($request_details['currency_sign']))) {
          } else {
            $insert_data_customer_master = array(
              "user_id" => trim($seat_details['operator_id']),
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($request_details['currency_sign']),
            );
            $this->api_bus->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($seat_details['operator_id']), trim($request_details['currency_sign']));
          }

          $account_balance = trim($customer_account_master_details['account_balance']) + trim($commission_amount);
          $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($seat_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($seat_details['operator_id']), trim($request_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$seat_details['ticket_id'],
            "user_id" => (int)trim($seat_details['operator_id']),
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($commission_amount),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($request_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api_bus->insert_payment_in_account_history($update_data_account_history);
        //Add Ownward Trip Commission From Operator Account
      //-------------------------------------------------------

      $mobile = $this->api_bus->get_country_code_by_id($operator_details['country_id']).$operator_details['mobile1'];
      $subject = 'Gonagoo - Refund Commission request status.';
      $message = 'Your request for commission has been accepted. Commission amount has been credited to your gonagoo account. Request ID: ' . $req_id;
      $messageBody ="<html><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
      $messageBody .="<link rel='stylesheet' type='text/css' href='". $this->config->item('resource_url') . 'css/email.css'."' />";
      $messageBody .="</head><body bgcolor='#FFFFFF'>";
      $messageBody .= "<h3> Dear " . trim($operator_details['firstname']) . ' ' . trim($operator_details['lastname']). "</h3>"; 
      $messageBody .= "<h4>" . $message . "</h4><br /><br /><br />";
      //Email Signature
      $messageBody .="<table class='head-wrap'>";
      $messageBody .="<tr><td></td><td class='header container'><div class='content'><table>";
      $messageBody .="<tr><td>Your Gonagoo team!</td></tr>";
      $messageBody .="<tr><td><img src='". $this->config->item('resource_url') . 'images/dashboard-logo.png'."' class='img-size' /> <br />".$this->lang->line('slider_heading1')."</td></tr>";
      $messageBody .="<tr><td>Support : support@gonagoo.com</td></tr>";
      $messageBody .="<tr><td>Website : www.gonagoo.com</td></tr>";
      //Join us on <Facebook icon><LinkedIn icon><Twitter icon>
      $messageBody .="<tr><td>Join us on : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/google.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='G+'></a>";
      $messageBody .="<a href='https://twitter.com/Gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/twitter.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='Twitter'></a>";
      $messageBody .="<a href='https://www.linkedin.com/company/gonagoo' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/linkedin.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='LinkedIn'></a>";
      $messageBody .="<a href='https://www.facebook.com/gonagoocm' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/icons/facebook.png'."' class='sc-icons' style='height: 36px; width: 36px;margin-right:10px;' alt='FB'></a></td></tr>";
      //Download the App <icon for iOS App download><icon for Android App download>
      $messageBody .="<tr><td>Download the App : <br /><a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/apple-app-store.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='iOS'></a>";
      $messageBody .="<a href='' target='_blank'><img src='".$this->config->item('resource_url').'web/front_site/images/google-play.png'."' class='sc-icons' style='width: 110px; height:auto;' alt='Android'></a></td></tr>";
      $messageBody .="</table></div></td><td></td></tr></table>";
      //Email Signature End
      $messageBody .="</body></html>";  

      require_once('resources/PHPMailer-master/src/Exception.php');
      require_once('resources/PHPMailer-master/src/PHPMailer.php');
      $email = new PHPMailer();
      $email->SetFrom($this->config->item('from_email'), 'Gonagoo');
      $email->isHTML(true);
      $email->Subject   = $subject;
      $email->Body      = $messageBody;
      $email->AddAddress(trim($operator_details['email1']));
      $email->Send();

      $this->api_bus->sendSMS(trim($mobile), $message);
      echo 'success';
    } else {  echo 'failed';  }
  }
}

/* End of file Ticket_commission_refund.php */
/* Location: ./application/controllers/Ticket_commission_refund.php */