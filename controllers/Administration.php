<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('Currency_model', 'currency');
      $this->load->model('Admin_model', 'admin'); 
      $this->load->model('Admin_orders_model', 'orders');
      $this->load->model('Withdraw_model', 'withdraw'); 
      $this->load->model('Notification_model', 'notice'); 
      $this->load->model('insurance_model', 'insurance');  
      $this->load->model('standard_dimension_model', 'dimension');   
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      if (!$this->input->is_ajax_request()){ $this->load->view('admin/side_bar_view', compact('user','permissions')); }

    } else{ redirect('admin');}
  }
  public function index()
  {
    if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
  }

  public function admin_dashboard()
  { 
    $id = $this->session->userdata('userid');
    $country_id = $this->session->userdata('admin_country_id');
    $categories = $this->dimension->get_categories();
    $countries = $this->insurance->get_countries();
    $gonagoo_master = $this->admin->get_gonagoo_master_details();
    $cat_id = $this->input->post('cat_id', TRUE);
    $c_id = $this->input->post('country_id', TRUE);
    $dateRange = $this->input->post('dateRange', TRUE);
    
    if(isset($cat_id)) { $cat_id = (int)$cat_id; } else { $cat_id = 7; }
    if(isset($c_id)) { $country_id = (int)$c_id; } else { $country_id = $country_id; }
    if(isset($dateRange)) { $dateFrom = trim(substr($dateRange,0,11)); } else { $dateFrom = date('Y-m-1'); }
    if(isset($dateRange)) { $dateTo = trim(substr($dateRange,13,24)); } else { $dateTo = date('Y-m-d'); } 
    
    //echo json_encode($dateFrom. '/' .$dateTo ); die();
    
    $gonagoo_insurance = $this->admin->get_gonagoo_account_details('insurance_fee', 0, 0, 0, 0);
    //echo json_encode($this->db->last_query($gonagoo_insurance)); die();
    $gonagoo_custom = $this->admin->get_gonagoo_account_details('custom_clearance_fee', 0, 0, 0, 0);
    //echo json_encode($this->db->last_query($gonagoo_custom)); die();
    $gonagoo_commission = $this->admin->get_gonagoo_account_details('my_xode', 0, 0, 0 , 0);
    //echo $this->db->last_query($gonagoo_commission); die();
    $gonagoo_refund = $this->admin->get_gonagoo_refund_details();
    $history = $this->admin->get_gonagoo_history(10);
    
    //new dashboard
    //latest Jobs Data
    
    //courier
    $recentlyOpenJobs = $this->admin->get_orders('open', 5);
    $recentlyInprogressJobs = $this->admin->get_orders('in_progress', 5);
    $recentlyDeliveredJobs = $this->admin->get_orders('delivered', 5);
    $recentlyCancelledJobs = $this->admin->get_orders('cancel', 5);

    //bus
    $recentlyCurrentTrips = $this->admin->get_total_trips('current', 0, 'records', 0, 0, 5);
    $recentlyUpcomingTrips = $this->admin->get_total_trips('upcoming', 0, 'records', 0, 0, 5);
    $recentlyPreviousTrips = $this->admin->get_total_trips('previous', 0, 'records', 0, 0, 5);
    $recentlyCancelledTrips = $this->admin->get_cancelled_trips('cancelled', 0, 'records', 0, 0, 5);
    //echo json_encode($recentlyPreviousTrips); die();
    
    //laundry
    $recently_accepted_laundry = $this->admin->laundry_booking_list('accepted', 0 ,'records', 0, 0, 5);
    $recently_in_progress_laundry =$this->admin->laundry_booking_list('in_progress', 0 ,'records', 0, 0, 5);
    $recently_completed_laundry = $this->admin->laundry_booking_list('completed', 0 ,'records', 0, 0, 5);
    $recently_cancelled_laundry = $this->admin->laundry_booking_list('cancelled', 0 ,'records', 0, 0, 5);
    //echo json_encode($completed_laundry); die();

    //Users counters
    $totalUsers = $this->admin->get_users('all', 0, 'count', 0);
    $businessUsers = $this->admin->get_users('business', 0, 'count', 0);
    $individualUsers = $this->admin->get_users('individual', 0, 'count', 0);
    $sellerUsers = $this->admin->get_users('seller', 0, 'count', 0);
    $buyerUsers = $this->admin->get_users('buyer', 0, 'count', 0);
    $bothUsers = $this->admin->get_users('both', 0, 'count', 0);
    $activeUsers = $this->admin->get_users('active', 0, 'count', 0);
    $inactiveUsers = $this->admin->get_users('inactive', 0, 'count', 0);
    $onlineUsers = $this->admin->get_users('online', 0, 'count', 0);
    $offlineUsers = $this->admin->get_users('offline', 0, 'count', 0);
    
    $currency_master = $this->currency->get_currencies();
    
    $totalJobs = $this->admin->get_jobs('all', 0, 'count', 0 , 0);
     //echo json_encode($totalJobs); die();
     //order_status, country_id, record_type, rows, cat_id
    $totalOpenJobs = $this->admin->get_jobs('open', 0, 'count', 0 , 0);
    $totalAcceptedJobs = $this->admin->get_jobs('accepted', 0, 'count', 0, 0);
    $totalInprogressJobs = $this->admin->get_jobs('inprogress', 0, 'count', 0, 0);
    $totalDeliveredJobs = $this->admin->get_jobs('delivered', 0, 'count', 0, 0);
    $totalCancelledJobs = $this->admin->get_jobs('cancelled', 0, 'count', 0, 0);
    $totalExpiredJobs = $this->admin->get_jobs('expired', 0, 'count', 0, 0);
    //$totalRejectedJobs = $this->admin->get_jobs('rejected', 0, 'count', 0);
    //echo json_encode($totalOpenJobs); die();
    
    //bus
    $totaltrips = $this->admin->get_total_trips('all', 0 , 'count');
    // echo json_encode($totaltrips); die();
    $currenttrips = $this->admin->get_total_trips('current', 0, 'count');
    $upcomingtrips = $this->admin->get_total_trips('upcoming', 0, 'count');
    $previoustrips = $this->admin->get_total_trips('previous', 0, 'count');
    $completedtrips = $this->admin->get_total_trips('complete', 0, 'count');
    $cancelledtrips = $this->admin->get_cancelled_trips('cancelled', 0, 'count');
    $previoustrips[0]['jobCount'] = $previoustrips[0]['jobCount'] - $completedtrips[0]['jobCount']; 
    //laundry
    $total_laundry_bookings = $this->admin->laundry_booking_list('all', 0 ,'count');
    $total_accepted_bookings = $this->admin->laundry_booking_list('accepted', 0 ,'count');
    $total_in_progress_bookings = $this->admin->laundry_booking_list('in_progress', 0 ,'count');
    $total_completed_bookings = $this->admin->laundry_booking_list('completed', 0 ,'count');
    $total_cancelled_bookings = $this->admin->laundry_booking_list('cancelled', 0 ,'count');

    $totalJobs[0]['jobCount'] = $totalJobs[0]['jobCount'] + $totaltrips[0]['jobCount'];
    $totalJobs[0]['jobCount'] = $totalJobs[0]['jobCount'] + $cancelledtrips[0]['jobCount'];
    $totalJobs[0]['jobCount'] = $totalJobs[0]['jobCount'] + $total_laundry_bookings[0]['jobCount'];
    //echo json_encode($totalJobs); die();
    $totalOpenJobs[0]['jobCount'] = $totalOpenJobs[0]['jobCount'] + $currenttrips[0]['jobCount'];
    $totalAcceptedJobs[0]['jobCount'] = $totalAcceptedJobs[0]['jobCount'] + $upcomingtrips[0]['jobCount'];
    $totalAcceptedJobs[0]['jobCount'] = $totalAcceptedJobs[0]['jobCount'] + $total_accepted_bookings[0]['jobCount'];
    $totalInprogressJobs[0]['jobCount'] = $totalInprogressJobs[0]['jobCount'] + $previoustrips[0]['jobCount'];
    $totalInprogressJobs[0]['jobCount'] = $totalInprogressJobs[0]['jobCount'] + $total_in_progress_bookings[0]['jobCount'];
    $totalDeliveredJobs[0]['jobCount'] = $totalDeliveredJobs[0]['jobCount'] + $completedtrips[0]['jobCount'];
    $totalDeliveredJobs[0]['jobCount'] = $totalDeliveredJobs[0]['jobCount'] + $total_completed_bookings[0]['jobCount'];
    $totalCancelledJobs[0]['jobCount'] = $totalCancelledJobs[0]['jobCount'] + $cancelledtrips[0]['jobCount'];
    $totalCancelledJobs[0]['jobCount'] = $totalCancelledJobs[0]['jobCount'] + $total_cancelled_bookings[0]['jobCount'];
    //echo json_encode($totalAcceptedJobs); die();

    $this->load->view('admin/admin_dashboard_view', compact('totalExpiredJobs','totalOpenJobs','totalAcceptedJobs','totalInprogressJobs','totalDeliveredJobs','totalCancelledJobs','totalJobs','offlineUsers','onlineUsers','inactiveUsers','activeUsers','totalUsers','businessUsers','individualUsers','sellerUsers','buyerUsers','bothUsers','recentlyCancelledJobs','recentlyDeliveredJobs','recentlyInprogressJobs','recentlyOpenJobs','id','gonagoo_master','gonagoo_insurance','gonagoo_custom','gonagoo_refund','history','gonagoo_commission','categories','countries','dateFrom', 'dateTo', 'country_id', 'cat_id','currency_master','recentlyCurrentTrips','recentlyUpcomingTrips','recentlyPreviousTrips','recentlyCancelledTrips','recently_accepted_laundry','recently_in_progress_laundry','recently_completed_laundry','recently_cancelled_laundry'));
    $this->load->view('admin/footer_view');
  }
  
  public function recently_current_trips()
  {
    $jobs = $this->admin->get_total_trips('current', 0, 'records');
    $this->load->view('admin/dashboard_current_trip_bus_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');   
  }

  public function recently_upcoming_trips()
  {
    $jobs = $this->admin->get_total_trips('upcoming', 0, 'records');
    $this->load->view('admin/dashboard_upcoming_trip_bus_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');   
  }

  public function recently_previous_trips()
  {
    $jobs = $this->admin->get_total_trips('previous', 0, 'records');
    $this->load->view('admin/dashboard_previous_trip_bus_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');   
  }

  public function recently_cancelled_trips()
  {
    $jobs = $this->admin->get_cancelled_trips('cancelled', 0 , 'records');
    $this->load->view('admin/dashboard_cancelled_trip_bus_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');   
  }

  public function recently_open_jobs()
  {
    $jobs = $this->admin->get_orders('open', 0);
	  $this->load->view('admin/dashboard_recently_open_jobs_list_view', compact('jobs'));			
	  $this->load->view('admin/footer_view');		
  }

  public function inprogress_open_jobs()
  {
    $jobs = $this->admin->get_orders('in_progress', 0);
	  $this->load->view('admin/dashboard_recently_inprogress_jobs_list_view', compact('jobs'));			
	  $this->load->view('admin/footer_view');		
  }
  
  public function delivered_open_jobs()
  {
    $jobs = $this->admin->get_orders('delivered', 0);
	  $this->load->view('admin/dashboard_recently_delivered_jobs_list_view', compact('jobs'));			
	  $this->load->view('admin/footer_view');		
  }
  
  public function cancelled_open_jobs()
  {
    $jobs = $this->admin->get_orders('cancel', 0);
	  $this->load->view('admin/dashboard_recently_cancel_jobs_list_view', compact('jobs'));			
  	$this->load->view('admin/footer_view');		
  }

  public function recently_accepted_laundry()
  {
    $jobs = $this->admin->laundry_booking_list('accepted',0, 'records');
    $this->load->view('admin/dashboard_accepted_laundry_booking_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');
  }

  public function recently_in_progress_laundry()
  {
    $jobs = $this->admin->laundry_booking_list('in_progress',0, 'records');
    $this->load->view('admin/dashboard_in_progress_laundry_booking_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');
  }

  public function recently_completed_laundry()
  {
    $jobs = $this->admin->laundry_booking_list('completed',0, 'records');
    $this->load->view('admin/dashboard_completed_laundry_booking_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');
  }

  public function recently_cancelled_laundry()
  {
    $jobs = $this->admin->laundry_booking_list('cancelled',0, 'records');
    $this->load->view('admin/dashboard_cancelled_laundry_booking_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');
  }
  
  public function total_users()
  {
    $totalUsersCountryId = $this->input->post('totalUsersCountryId', TRUE);
	  $users = $this->admin->get_users('all', $totalUsersCountryId, 'records', 0);
  	$currency_master = $this->currency->get_currencies();
  	$this->load->view('admin/dashboard_total_users_list_view', compact('users','currency_master'));			
  	$this->load->view('admin/footer_view');		
  }
  
  public function business_users()
  {
    $businessUsersCountryId = $this->input->post('businessUsersCountryId', TRUE);
  	$users = $this->admin->get_users('business', $businessUsersCountryId, 'records', 0);
  	$this->load->view('admin/dashboard_business_users_list_view', compact('users'));			
  	$this->load->view('admin/footer_view');		
  }
  
  public function individual_users()
  {
    $individualUsersCountryId = $this->input->post('individualUsersCountryId', TRUE);
	  $users = $this->admin->get_users('individual', $individualUsersCountryId, 'records', 0);
	  $this->load->view('admin/dashboard_individual_users_list_view', compact('users'));			
	  $this->load->view('admin/footer_view');		
  }
  
  public function buyer_users()
  {
    $buyerUsersCountryId = $this->input->post('buyerUsersCountryId', TRUE);
  	$users = $this->admin->get_users('buyer', $buyerUsersCountryId, 'records', 0);
  	$this->load->view('admin/dashboard_buyer_users_list_view', compact('users'));			
	  $this->load->view('admin/footer_view');		
  }
  
  public function seller_users()
  {
    $sellerUsersCountryId = $this->input->post('sellerUsersCountryId', TRUE);
	  $users = $this->admin->get_users('seller', $sellerUsersCountryId, 'records', 0);
  	$this->load->view('admin/dashboard_seller_users_list_view', compact('users'));			
  	$this->load->view('admin/footer_view');		
  }
  
  public function both_users()
  {
    $bothUsersCountryId = $this->input->post('bothUsersCountryId', TRUE);
	  $users = $this->admin->get_users('both', $bothUsersCountryId, 'records', 0);
	  $this->load->view('admin/dashboard_both_users_list_view', compact('users'));			
	  $this->load->view('admin/footer_view');		
  }
  
  public function online_users()
  {
    $onlineUsersCountryId = $this->input->post('onlineUsersCountryId', TRUE);
	  $users = $this->admin->get_users('online', $onlineUsersCountryId, 'records', 0);
	  $this->load->view('admin/dashboard_online_users_list_view', compact('users'));			
	  $this->load->view('admin/footer_view');		
  }
  
  public function offline_users()
  {
    $offlineUsersCountryId = $this->input->post('offlineUsersCountryId', TRUE);
	  $users = $this->admin->get_users('offline', $offlineUsersCountryId, 'records', 0);
 	  $this->load->view('admin/dashboard_offline_users_list_view', compact('users'));			
  	$this->load->view('admin/footer_view');		
  }
  
  public function active_users()
  {
    $activeUsersCountryId = $this->input->post('activeUsersCountryId', TRUE);
	  $users = $this->admin->get_users('active', $activeUsersCountryId, 'records', 0);
	  $this->load->view('admin/dashboard_active_users_list_view', compact('users'));			
	  $this->load->view('admin/footer_view');		
  }
  
  public function inactive_users()
  {
    $inactiveUsersCountryId = $this->input->post('inactiveUsersCountryId', TRUE);
  	$users = $this->admin->get_users('inactive', $inactiveUsersCountryId, 'records', 0);
  	$this->load->view('admin/dashboard_inactive_users_list_view', compact('users'));			
  	$this->load->view('admin/footer_view');		
  }
  
  public function total_jobs()
  {
    //echo json_encode($_POST); die();
    $totalJobsStartDate = $this->input->post('totalJobsStartDate', TRUE);
    $totalJobsToDate = $this->input->post('totalJobsToDate', TRUE);
    $totalJobsCountryId = $this->input->post('totalJobsCountryId', TRUE);
    $totalJobsCatId = $this->input->post('totalJobsCatId', TRUE);
    if($totalJobsCatId== 6 || $totalJobsCatId== 7 || $totalJobsCatId== 280 ){
      $jobs = $this->admin->get_jobs('all', $totalJobsCountryId, 'records', 0, $totalJobsCatId, $totalJobsStartDate, $totalJobsToDate);
      $this->load->view('admin/dashboard_total_jobs_list_view', compact('jobs'));
    }
    if($totalJobsCatId== 281){
      $jobs = $this->admin->get_total_trips('all', $totalJobsCountryId, 'records', $totalJobsStartDate, $totalJobsToDate);
      //echo json_encode($jobs); die();
      $this->load->view('admin/dashboard_total_trips_bus_list_view', compact('jobs'));     
    }
    if($totalJobsCatId== 9){
      $jobs = $this->admin->laundry_booking_list('all', $totalJobsCountryId ,'records', $totalJobsStartDate, $totalJobsToDate);
      //echo json_encode($jobs); die();
      $this->load->view('admin/dashboard_total_laundry_booking_list_view', compact('jobs'));     
    }
    $this->load->view('admin/footer_view');   
  }
  
  public function total_open_jobs()
  {
    $totalOpenJobsStartDate = $this->input->post('totalOpenJobsStartDate', TRUE);
    $totalOpenJobsToDate = $this->input->post('totalOpenJobsToDate', TRUE);
    $totalOpenJobsCountryId = $this->input->post('totalOpenJobsCountryId', TRUE);
    $totalOpenJobsCatId = $this->input->post('totalOpenJobsCatId', TRUE);
    if($totalOpenJobsCatId== 6 || $totalOpenJobsCatId== 7 || $totalOpenJobsCatId== 280 ){
      $jobs = $this->admin->get_jobs('open', $totalOpenJobsCountryId, 'records', 0, $totalOpenJobsCatId, $totalOpenJobsStartDate, $totalOpenJobsToDate);
      $this->load->view('admin/dashboard_open_jobs_list_view', compact('jobs'));     
    }
    if($totalOpenJobsCatId== 281){
      $jobs = $this->admin->get_total_trips('current',$totalOpenJobsCountryId , 'records', $totalOpenJobsStartDate, $totalOpenJobsToDate);
      //echo json_encode($jobs); die();
      $this->load->view('admin/dashboard_current_trip_bus_list_view', compact('jobs'));     
    }
    if($totalOpenJobsCatId== 9){
      $jobs = $this->admin->laundry_booking_list('accepted',$totalOpenJobsCountryId , 'records', $totalOpenJobsStartDate, $totalOpenJobsToDate);
      //echo json_encode($jobs); die();
      $this->load->view('admin/dashboard_accepted_laundry_booking_list_view', compact('jobs'));     
    }
    $this->load->view('admin/footer_view');
  }
  
  public function total_accepted_jobs()
  {
    $totalAcceptedJobsStartDate = $this->input->post('totalAcceptedJobsStartDate', TRUE);
    $totalAcceptedJobsToDate = $this->input->post('totalAcceptedJobsToDate', TRUE);
    $totalAcceptedJobsCountryId = $this->input->post('totalAcceptedJobsCountryId', TRUE);
    $totalAcceptedJobsCatId = $this->input->post('totalAcceptedJobsCatId', TRUE);
    if($totalAcceptedJobsCatId == 6 || $totalAcceptedJobsCatId == 7 || $totalAcceptedJobsCatId == 280){
      $jobs = $this->admin->get_jobs('accepted', $totalAcceptedJobsCountryId, 'records', 0, $totalAcceptedJobsCatId, $totalAcceptedJobsStartDate, $totalAcceptedJobsToDate);
      $this->load->view('admin/dashboard_accepted_jobs_list_view', compact('jobs'));      
    }
    if($totalAcceptedJobsCatId == 281){
      $jobs = $this->admin->get_total_trips('upcoming', $totalAcceptedJobsCountryId, 'records', $totalAcceptedJobsStartDate, $totalAcceptedJobsToDate);
      $this->load->view('admin/dashboard_upcoming_trip_bus_list_view', compact('jobs'));      
    }
    if($totalAcceptedJobsCatId == 9){
      $jobs = $this->admin->laundry_booking_list('in_progress', $totalAcceptedJobsCountryId, 'records', $totalAcceptedJobsStartDate, $totalAcceptedJobsToDate);
      //echo json_encode($jobs); die();
      $this->load->view('admin/dashboard_in_progress_laundry_booking_list_view', compact('jobs'));      
    }
    $this->load->view('admin/footer_view');   
  }
  
  public function total_inprogress_jobs()
  {
    $totalInprogressJobsStartDate = $this->input->post('totalInprogressJobsStartDate', TRUE);
    $totalInprogressJobsToDate = $this->input->post('totalInprogressJobsToDate', TRUE);
    $totalInprogressJobsCountryId = $this->input->post('totalInprogressJobsCountryId', TRUE);
    $totalInprogressJobsCatId = $this->input->post('totalInprogressJobsCatId', TRUE);
    if($totalInprogressJobsCatId == 6 || $totalInprogressJobsCatId == 7 || $totalInprogressJobsCatId == 280){
      $jobs = $this->admin->get_jobs('inprogress', $totalInprogressJobsCountryId, 'records', 0, $totalInprogressJobsCatId, $totalInprogressJobsStartDate, $totalInprogressJobsToDate);
      $this->load->view('admin/dashboard_inprogress_jobs_list_view', compact('jobs'));      
    }
    if($totalInprogressJobsCatId == 281){
      $jobs = $this->admin->get_total_trips('previous', $totalInprogressJobsCountryId, 'records', $totalInprogressJobsStartDate, $totalInprogressJobsToDate);
      $this->load->view('admin/dashboard_previous_trip_bus_list_view', compact('jobs'));      
    }
    if($totalInprogressJobsCatId == 9){
      $jobs = $this->admin->laundry_booking_list('completed', $totalInprogressJobsCountryId, 'records', $totalInprogressJobsStartDate, $totalInprogressJobsToDate);
      //echo json_encode($jobs); die();
      $this->load->view('admin/dashboard_completed_laundry_booking_list_view', compact('jobs'));      
    }
    $this->load->view('admin/footer_view');   
  }
  
  public function total_delivered_jobs()
  {
    $totalDeliveredJobsStartDate = $this->input->post('totalDeliveredJobsStartDate', TRUE);
    $totalDeliveredJobsToDate = $this->input->post('totalDeliveredJobsToDate', TRUE);
    $totalDeliveredJobsCountryId = $this->input->post('totalDeliveredJobsCountryId', TRUE);
    $totalDeliveredJobsCatId = $this->input->post('totalDeliveredJobsCatId', TRUE);
    if($totalDeliveredJobsCatId == 6 || $totalDeliveredJobsCatId == 7 || $totalDeliveredJobsCatId == 280){
      $jobs = $this->admin->get_jobs('delivered', $totalDeliveredJobsCountryId, 'records', 0, $totalDeliveredJobsCatId, $totalDeliveredJobsStartDate, $totalDeliveredJobsToDate);
      $this->load->view('admin/dashboard_delivered_jobs_list_view', compact('jobs'));     
    }
    if($totalDeliveredJobsCatId == 281){
      $jobs = $this->admin->get_total_trips('complete', $totalDeliveredJobsCountryId, 'records', $totalDeliveredJobsStartDate, $totalDeliveredJobsToDate);
      $this->load->view('admin/dashboard_complete_trip_bus_list_view', compact('jobs'));
    }
    if($totalDeliveredJobsCatId == 9){
      $jobs = $this->admin->laundry_booking_list('cancelled', $totalDeliveredJobsCountryId, 'records', $totalDeliveredJobsStartDate, $totalDeliveredJobsToDate);
      //echo json_encode($jobs); die();
      $this->load->view('admin/dashboard_cancelled_laundry_booking_list_view', compact('jobs'));
    }
    $this->load->view('admin/footer_view');   
  }
  
  public function total_cancelled_jobs()
  {
    $totalCancelledJobsStartDate = $this->input->post('totalCancelledJobsStartDate', TRUE);
    $totalCancelledJobsToDate = $this->input->post('totalCancelledJobsToDate', TRUE);
    $totalCancelledJobsCountryId = $this->input->post('totalCancelledJobsCountryId', TRUE);
    $totalCancelledJobsCatId = $this->input->post('totalCancelledJobsCatId', TRUE);
    if($totalCancelledJobsCatId == 6 || $totalCancelledJobsCatId == 7 || $totalCancelledJobsCatId == 280){
      $jobs = $this->admin->get_jobs('cancelled', $totalCancelledJobsCountryId, 'records', 0, $totalCancelledJobsCatId, $totalCancelledJobsStartDate, $totalCancelledJobsToDate);
      $this->load->view('admin/dashboard_cancelled_jobs_list_view', compact('jobs'));     
    }
    if($totalCancelledJobsCatId == 281){
      $jobs = $this->admin->get_cancelled_trips('cancelled', $totalCancelledJobsCountryId, 'records', $totalCancelledJobsStartDate, $totalCancelledJobsToDate);
      $this->load->view('admin/dashboard_cancelled_trip_bus_list_view', compact('jobs'));     
    //echo json_encode($jobs); die();
    }
    $this->load->view('admin/footer_view');   
  } 

  public function total_expired_jobs()
  {
    $totalExpiredJobsStartDate = $this->input->post('totalExpiredJobsStartDate', TRUE);
    $totalExpiredJobsToDate = $this->input->post('totalExpiredJobsToDate', TRUE);
    $totalExpiredJobsCountryId = $this->input->post('totalExpiredJobsCountryId', TRUE);
    $totalExpiredJobsCatId = $this->input->post('totalExpiredJobsCatId', TRUE);
    $jobs = $this->admin->get_jobs('expired', $totalExpiredJobsCountryId, 'records', 0, $totalExpiredJobsCatId, $totalExpiredJobsStartDate, $totalExpiredJobsToDate);
    $this->load->view('admin/dashboard_expired_jobs_list_view', compact('jobs'));     
    $this->load->view('admin/footer_view');   
  }
  
  public function get_users_count_by_country_id()
  { 
    $user_type = $this->input->post('user_type', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $record_type = $this->input->post('record_type', TRUE);
    $rows = $this->input->post('rows', TRUE);
    echo json_encode($this->admin->get_users($user_type, $country_id, $record_type, $rows));
  }
  
  public function get_job_count_by_filters()
  { 
    $order_status = $this->input->post('order_status', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $record_type = $this->input->post('record_type', TRUE);
    $rows = $this->input->post('rows', TRUE);
    $cat_id = $this->input->post('cat_id', TRUE);
    $start_date = $this->input->post('start_date', TRUE);
    $to_date = $this->input->post('to_date', TRUE);

    if($cat_id == 0){
      if($order_status == "all"){
        $totaltrips = $this->admin->get_total_trips('all', $country_id , 'count' , $start_date , $to_date);
        $totalJobs = $this->admin->get_jobs('all', $country_id, 'count', $start_date , $to_date);
        $cancelledtrips = $this->admin->get_cancelled_trips('cancelled', $country_id, 'count' , $start_date , $to_date);
        $total_laundry = $this->admin->laundry_booking_list('all', $country_id ,'count',trim($start_date), trim($to_date));

        $totalJobs[0]['jobCount'] = $totalJobs[0]['jobCount'] + $cancelledtrips[0]['jobCount'];
        $totalJobs[0]['jobCount'] = $totalJobs[0]['jobCount'] + $totaltrips[0]['jobCount'];
        $totalJobs[0]['jobCount'] = $totalJobs[0]['jobCount'] + $total_laundry[0]['jobCount'];
        echo json_encode($totalJobs); die();
      }
      elseif($order_status == "open"){
        $totalOpenJobs = $this->admin->get_jobs('open', $country_id, 'count', $start_date , $to_date);
        $currenttrips = $this->admin->get_total_trips('current', $country_id, 'count' , $start_date , $to_date);
        //$totalOpenJobs[0]['jobCount'] = $totalOpenJobs[0]['jobCount'] + $currenttrips[0]['jobCount'];
        echo json_encode($totalOpenJobs); die();
      }
      elseif($order_status == "accepted"){
        $totalAcceptedJobs = $this->admin->get_jobs('accepted', $country_id, 'count', $start_date , $to_date);
        $upcomingtrips = $this->admin->get_total_trips('upcoming', $country_id, 'count', $start_date , $to_date);
        $accepted_laundry = $this->admin->laundry_booking_list('accepted', $country_id ,'count',trim($start_date), trim($to_date));
        $totalAcceptedJobs[0]['jobCount'] = $totalAcceptedJobs[0]['jobCount'] + $upcomingtrips[0]['jobCount'];
        $totalAcceptedJobs[0]['jobCount'] = $accepted_laundry[0]['jobCount'] + $upcomingtrips[0]['jobCount'];
        echo json_encode($totalAcceptedJobs); die();   
      }
      elseif($order_status=="inprogress"){
        $totalInprogressJobs = $this->admin->get_jobs('inprogress', $country_id, 'count', $start_date , $to_date);
        $previoustrips = $this->admin->get_total_trips('previous', $country_id, 'count' , $start_date , $to_date);
        $in_progress_laundry = $this->admin->laundry_booking_list('in_progress', $country_id ,'count',trim($start_date), trim($to_date));
        $totalInprogressJobs[0]['jobCount'] = $totalInprogressJobs[0]['jobCount'] + $previoustrips[0]['jobCount'];
        $totalInprogressJobs[0]['jobCount'] = $in_progress_laundry[0]['jobCount'] + $previoustrips[0]['jobCount'];
        echo json_encode($totalInprogressJobs); die();
      }
      elseif($order_status=="delivered"){
        $totalDeliveredJobs = $this->admin->get_jobs($order_status, $country_id, 'count', $start_date , $to_date);
        $completedtrips = $this->admin->get_total_trips('complete', $country_id, 'count' , $start_date , $to_date);
        $completed_laundry = $this->admin->laundry_booking_list('completed', $country_id ,'count',trim($start_date), trim($to_date));
        $totalDeliveredJobs[0]['jobCount'] = $totalDeliveredJobs[0]['jobCount'] + $completedtrips[0]['jobCount'];
        $totalDeliveredJobs[0]['jobCount'] = $completed_laundry[0]['jobCount'] + $completedtrips[0]['jobCount'];
        echo json_encode($totalDeliveredJobs); die();     
      }
      elseif($order_status=="cancelled"){
        $totalCancelledJobs = $this->admin->get_jobs('cancelled', $country_id, 'count', $start_date , $to_date);
        $cancelledtrips = $this->admin->get_cancelled_trips('cancelled', $country_id, 'count' , $start_date , $to_date);   
        $cancelled_laundry = $this->admin->laundry_booking_list('cancelled', $country_id ,'count',trim($start_date), trim($to_date)); 
        $totalCancelledJobs[0]['jobCount'] = $totalCancelledJobs[0]['jobCount'] + $cancelledtrips[0]['jobCount'];
        $totalCancelledJobs[0]['jobCount'] = $cancelled_laundry[0]['jobCount'] + $cancelledtrips[0]['jobCount'];
        echo json_encode($cancelledtrips); die();
      }
      else{
        $totalCancelledJobs = $this->admin->get_jobs($order_status, $country_id, 'count', $start_date , $to_date);
        echo json_encode($cancelledtrips); die();
      }
    }

    if($cat_id == 6 || $cat_id == 7 || $cat_id == 280){
      $tt = $this->admin->get_jobs($order_status, $country_id, $record_type, $rows, $cat_id, trim($start_date), trim($to_date));
      //$tt[0] += ['name' => 'jobs'];
      echo json_encode($tt); die();
    }

    if($cat_id == 281){
      if($order_status=="all"){
        $tt = $this->admin->get_total_trips("all",$country_id, $record_type,trim($start_date), trim($to_date));
        $cancelledtrips = $this->admin->get_cancelled_trips("cancelled",$country_id, $record_type,trim($start_date), trim($to_date));
        $tt[0]['jobCount'] = $tt[0]['jobCount'] + $cancelledtrips[0]['jobCount'];
        echo json_encode($tt); die();
      }
      if($order_status=="open"){
       $tt = $this->admin->get_total_trips("current" ,$country_id, $record_type,trim($start_date), trim($to_date));
       echo json_encode($tt); die();
      }
      if($order_status=="accepted"){
       $tt = $this->admin->get_total_trips("upcoming" ,$country_id, $record_type,trim($start_date), trim($to_date));
       echo json_encode($tt); die();
      }
      if($order_status=="inprogress"){
       $tt = $this->admin->get_total_trips("previous" ,$country_id, $record_type,trim($start_date), trim($to_date));
       $tt2 = $this->admin->get_total_trips("complete" ,$country_id, $record_type,trim($start_date), trim($to_date));
       $tt[0]['jobCount'] = $tt[0]['jobCount'] - $tt2[0]['jobCount'];
       echo json_encode($tt); die();
      }
      if($order_status=="delivered"){
       $tt = $this->admin->get_total_trips("complete" ,$country_id, $record_type,trim($start_date), trim($to_date));
       echo json_encode($tt); die();
      }
      if($order_status=="cancelled"){
       $tt = $this->admin->get_cancelled_trips("cancelled" ,$country_id, $record_type,trim($start_date), trim($to_date));
       echo json_encode($tt); die();
      }      
    }

    if($cat_id == 9){
      if($order_status=="all"){
        $tt = $this->admin->laundry_booking_list('all', $country_id , $record_type , trim($start_date), trim($to_date));
        echo json_encode($tt); die();
      }
      if($order_status=="open"){
        $tt = $this->admin->laundry_booking_list('accepted', $country_id , $record_type , trim($start_date), trim($to_date));
        echo json_encode($tt); die();
      }
      if($order_status=="accepted"){
       $tt = $this->admin->laundry_booking_list('in_progress', $country_id , $record_type , trim($start_date), trim($to_date));
       echo json_encode($tt); die();
      }
      if($order_status=="inprogress"){
       $tt = $this->admin->laundry_booking_list('completed', $country_id , $record_type , trim($start_date), trim($to_date));
       echo json_encode($tt); die();
      }
      if($order_status=="delivered"){
       $tt = $this->admin->laundry_booking_list('cancelled', $country_id , $record_type , trim($start_date), trim($to_date));
       echo json_encode($tt); die();
      }
    }
  }
  
  public function get_other_account_details_by_filters()
  { 
    $type = $this->input->post('account_type', TRUE);
    $start_date = $this->input->post('start_date', TRUE);
    $to_date = $this->input->post('to_date', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $cat_id = $this->input->post('cat_id', TRUE);
    if($cat_id == 281 && $type == "commission_amount" ){  $type = "ticket_commission"; }
    if($cat_id == 9 && $type == "commission_amount" ){  $type = "laundry_commission"; }
    if($cat_id == 0 &&  $type == 'commission_amount'){
      echo json_encode($this->admin->get_gonagoo_account_details('my_xode', $start_date, $to_date, $country_id, $cat_id)); die();
    }
    echo json_encode($this->admin->get_gonagoo_account_details($type, $start_date, $to_date, $country_id, $cat_id));
  }
  
  public function dashboard()
  { 
    $id = $this->session->userdata('userid');
    $country_id = $this->session->userdata('admin_country_id');
    $categories = $this->dimension->get_categories();
    $countries = $this->insurance->get_countries();
    $gonagoo_master = $this->admin->get_gonagoo_master_details();
    $cat_id = $this->input->post('cat_id', TRUE);
    $c_id = $this->input->post('country_id', TRUE);
    $dateRange = $this->input->post('dateRange', TRUE);
    
    if(isset($cat_id)) { $cat_id = (int)$cat_id; } else { $cat_id = 7; }
    if(isset($c_id)) { $country_id = (int)$c_id; } else { $country_id = $country_id; }
    if(isset($dateRange)) { $dateFrom = trim(substr($dateRange,0,11)); } else { $dateFrom = date('Y-m-1'); }
    if(isset($dateRange)) { $dateTo = trim(substr($dateRange,13,24)); } else { $dateTo = date('Y-m-d'); } 
    
    //echo json_encode($dateFrom. '/' .$dateTo ); die();
    
    $gonagoo_insurance = $this->admin->get_gonagoo_account_details('insurance_fee', $dateFrom, $dateTo, $country_id, $cat_id);
    //echo json_encode($this->db->last_query($gonagoo_insurance)); die();
    $gonagoo_custom = $this->admin->get_gonagoo_account_details('custom_clearance_fee', $dateFrom, $dateTo, $country_id, $cat_id);
    //echo json_encode($this->db->last_query($gonagoo_custom)); die();
    $gonagoo_commission = $this->admin->get_gonagoo_account_details('commission_amount', $dateFrom, $dateTo, $country_id, $cat_id);
    //echo json_encode($this->db->last_query($gonagoo_commission)); die();
    $gonagoo_refund = $this->admin->get_gonagoo_refund_details();
    $history = $this->admin->get_gonagoo_history(10);
    $this->load->view('admin/dashboard_view', compact('id','gonagoo_master','gonagoo_insurance','gonagoo_custom','gonagoo_refund','history','gonagoo_commission','categories','countries','dateFrom', 'dateTo', 'country_id', 'cat_id'));
    $this->load->view('admin/footer_view');
  }
  
  public function user_wise_dashboard()
  { 
    $id = $this->session->userdata('userid');
    $country_id = $this->session->userdata('admin_country_id');
    $categories = $this->dimension->get_categories();
    $countries = $this->insurance->get_countries();
    $currencies = $this->admin->get_currencies();
    
    $cat_id = $this->input->post('cat_id', TRUE);
    $c_id = $this->input->post('country_id', TRUE);
    
    if(isset($cat_id)) { $cat_id = (int)$cat_id; } else { $cat_id = 0; }
    if(isset($c_id)) { $country_id = (int)$c_id; } else { $country_id = 0; }
    
    $expiredBookingProviders = $this->admin->get_user_count_with_bookings('deliverer', $country_id, $cat_id, '5', 'in_progress');
    // echo json_encode($expiredBookingProviders); die();
    $rejectedBookingProviders = $this->admin->get_user_count_with_rejected_bookings('deliverer', $country_id, $cat_id, '5');
    $acceptedBookingProviders = $this->admin->get_user_count_with_bookings('deliverer', $country_id, $cat_id, '5', 'accept');
    $deliveredBookingProviders = $this->admin->get_user_count_with_bookings('deliverer', $country_id, $cat_id, '5', 'delivered');
    
    $createdBookingCustomer = $this->admin->get_user_count_with_bookings('customer', $country_id, $cat_id, '5', 'open');
    $rejectedBookingCustomer = $this->admin->get_user_count_with_rejected_bookings('customer', $country_id, $cat_id, '5');
    $expiredBookingCustomer = $this->admin->get_user_count_with_bookings('customer', $country_id, $cat_id, '5', 'in_progress');
    $acceptedBookingCustomer = $this->admin->get_user_count_with_bookings('customer', $country_id, $cat_id, '5', 'accept');
    $cancelledBookingCustomer = $this->admin->get_user_count_with_bookings('customer', $country_id, $cat_id, '5', 'cancel');
    $deliveredBookingCustomer = $this->admin->get_user_count_with_bookings('customer', $country_id, $cat_id, '5', 'delivered');
    
    $recentlyConnectedUsers = $this->admin->get_user_login_history('recently', $country_id, '5');
    $frequentlyConnectedUsers = $this->admin->get_user_login_history('frequently', $country_id, '5');
    $neverConnectedUsers = $this->admin->get_user_login_history('never', $country_id, '5');
    $bestRatingUsers = $this->admin->get_user_by_ratings('high', $country_id, '5');
    $lowestRatingUsers = $this->admin->get_user_by_ratings('low', $country_id, '5');
    
    $this->load->view('admin/user_wise_dashboard_view', compact('id','categories','countries','currencies', 'country_id', 'cat_id', 'expiredBookingProviders','rejectedBookingProviders','acceptedBookingProviders','deliveredBookingProviders','createdBookingCustomer','rejectedBookingCustomer','expiredBookingCustomer','acceptedBookingCustomer','cancelledBookingCustomer','deliveredBookingCustomer','recentlyConnectedUsers','frequentlyConnectedUsers','neverConnectedUsers','bestRatingUsers','lowestRatingUsers'));
    $this->load->view('admin/footer_view');
  }

  public function user_wise_bus_dashboard()
  { 
    //echo json_encode($_POST); die();
    $id = $this->session->userdata('userid');
    $country_id = $this->session->userdata('admin_country_id');
    $countries = $this->insurance->get_countries();
    $currencies = $this->admin->get_currencies();
    
    $c_id = $this->input->post('country_id', TRUE);
    
    if(isset($c_id)) { $country_id = (int)$c_id; } else { $country_id = 0; }
    //echo json_encode($country_id); die();
    
    $current_trips_providers = $this->admin->get_user_count_with_bookings_bus('deliverer', $country_id,  '5', 'current');
    //echo json_encode($this->db->last_query()); die();
    //echo json_encode($current_trips_providers); die();

    $upcoming_trips_providers = $this->admin->get_user_count_with_bookings_bus('deliverer', $country_id,  '5', 'upcoming');
    
    $previous_trips_providers = $this->admin->get_user_count_with_bookings_bus('deliverer', $country_id,  '5', 'previous');

    $completed_trips_providers = $this->admin->get_user_count_with_bookings_bus('deliverer', $country_id,  '5', 'complete');

    $current_trips_customer = $this->admin->get_user_count_with_bookings_bus('customer', $country_id,  '5', 'current');

    $upcoming_trips_customer = $this->admin->get_user_count_with_bookings_bus('customer', $country_id,  '5', 'upcoming');
    
    $previous_trips_customer = $this->admin->get_user_count_with_bookings_bus('customer', $country_id,  '5', 'previous');

    $completed_trips_customer = $this->admin->get_user_count_with_bookings_bus('customer', $country_id,  '5', 'complete');
    
    $recentlyConnectedUsers = $this->admin->get_user_login_history('recently', $country_id, '5');
    $frequentlyConnectedUsers = $this->admin->get_user_login_history('frequently', $country_id, '5');
    $neverConnectedUsers = $this->admin->get_user_login_history('never', $country_id, '5');
    $bestRatingUsers = $this->admin->get_user_by_ratings('high', $country_id, '5');
    $lowestRatingUsers = $this->admin->get_user_by_ratings('low', $country_id, '5');
    
    $this->load->view('admin/user_wise_bus_dashboard_view', compact('id','countries','currencies', 'country_id', 'current_trips_providers','upcoming_trips_providers','previous_trips_providers','completed_trips_providers' , 'current_trips_customer','upcoming_trips_customer','previous_trips_customer','completed_trips_customer','recentlyConnectedUsers','frequentlyConnectedUsers','neverConnectedUsers','bestRatingUsers','lowestRatingUsers'));
    $this->load->view('admin/footer_view');
  }

  public function user_wise_laundry_dashboard()
  { 
    //echo json_encode($_POST); die();
    $id = $this->session->userdata('userid');
    $country_id = $this->session->userdata('admin_country_id');
    $countries = $this->insurance->get_countries();
    $currencies = $this->admin->get_currencies();
    
    $c_id = $this->input->post('country_id', TRUE);
    
    if(isset($c_id)) { $country_id = (int)$c_id; } else { $country_id = 0; }
    
    $all_orders_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer', $country_id,  '5', 'all');
    
    $accepted_orders_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer', $country_id,  '5', 'accepted');

    $in_progress_orders_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer', $country_id,  '5', 'in_progress');
    
    $completed_orders_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer', $country_id,  '5', 'completed');

    $cancelled_orders_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer', $country_id,  '5', 'cancelled');
    // echo json_encode($cancelled_orders); die();

    $all_orders_customer = $this->admin->get_user_count_with_bookings_laundry('customer', $country_id,  '5', 'all');
    
    $accepted_orders_customer = $this->admin->get_user_count_with_bookings_laundry('customer', $country_id,  '5', 'accepted');

    $in_progress_orders_customer = $this->admin->get_user_count_with_bookings_laundry('customer', $country_id,  '5', 'in_progress');
    
    $completed_orders_customer = $this->admin->get_user_count_with_bookings_laundry('customer', $country_id,  '5', 'completed');

    $cancelled_orders_customer = $this->admin->get_user_count_with_bookings_laundry('customer', $country_id,  '5', 'cancelled');
    // echo json_encode($all_orders_customer); die();
    
    $recentlyConnectedUsers = $this->admin->get_user_login_history('recently', $country_id, '5');
    $frequentlyConnectedUsers = $this->admin->get_user_login_history('frequently', $country_id, '5');
    $neverConnectedUsers = $this->admin->get_user_login_history('never', $country_id, '5');
    $bestRatingUsers = $this->admin->get_user_by_ratings('high', $country_id, '5');
    $lowestRatingUsers = $this->admin->get_user_by_ratings('low', $country_id, '5');
    
    $this->load->view('admin/user_wise_laundry_dashboard_view', compact('id','categories','countries','currencies', 'country_id', 'cat_id', 'all_orders_provider','accepted_orders_provider','in_progress_orders_provider','completed_orders_provider','cancelled_orders_provider','all_orders_customer','accepted_orders_customer','in_progress_orders_customer','completed_orders_customer','cancelled_orders_customer','recentlyConnectedUsers','frequentlyConnectedUsers','neverConnectedUsers','bestRatingUsers','lowestRatingUsers'));
    $this->load->view('admin/footer_view');
  }

  public function get_user_by_currency()
  { 
    $currency_code = $this->input->post('currency_code', TRUE);
    $rows = $this->input->post('rows', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $type = $this->input->post('type', TRUE);
    echo json_encode($this->admin->get_user_by_currency($currency_code, $country_id, $rows, $type));
  }

  //laundry
  public function dashboard_laundry_total_bookings_providers()
  {
    $total_orders_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer',0,0,'all');
    $this->load->view('admin/dashboard_total_bookings_providers_view', compact('total_orders_provider'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_laundry_accepted_bookings_providers()
  {
    $accepted_bookings_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer',0,0,'accepted');
    $this->load->view('admin/dashboard_accepted_bookings_providers_view', compact('accepted_bookings_provider'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_laundry_in_progress_bookings_providers()
  {
    $in_progress_bookings_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer',0,0,'in_progress');
    $this->load->view('admin/dashboard_in_progress_bookings_providers_view', compact('in_progress_bookings_provider'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_laundry_completed_bookings_providers()
  {
    $completed_bookings_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer',0,0,'completed');
    $this->load->view('admin/dashboard_completed_bookings_providers_view', compact('completed_bookings_provider'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_laundry_cancelled_bookings_providers()
  {
    $cancelled_bookings_provider = $this->admin->get_user_count_with_bookings_laundry('deliverer',0,0,'cancelled');
    $this->load->view('admin/dashboard_cancelled_bookings_providers_view', compact('cancelled_bookings_provider'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_laundry_total_bookings_customers()
  {
    $total_orders_customers = $this->admin->get_user_count_with_bookings_laundry('customer',0,0,'all');
    $this->load->view('admin/dashboard_total_bookings_customer_view', compact('total_orders_customers'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_laundry_accepted_bookings_customers()
  {
    $accepted_bookings_customers = $this->admin->get_user_count_with_bookings_laundry('customer',0,0,'accepted');
    $this->load->view('admin/dashboard_accepted_bookings_customer_view', compact('accepted_bookings_customers'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_laundry_in_progress_bookings_customers()
  {
    $in_progress_bookings_customers = $this->admin->get_user_count_with_bookings_laundry('customer',0,0,'in_progress');
    $this->load->view('admin/dashboard_in_progress_bookings_customer_view', compact('in_progress_bookings_customers'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_laundry_completed_bookings_customers()
  {
    $completed_bookings_customers = $this->admin->get_user_count_with_bookings_laundry('customer',0,0,'completed');
    $this->load->view('admin/dashboard_completed_bookings_customer_view', compact('completed_bookings_customers'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_laundry_cancelled_bookings_customers()
  {
    $cancelled_bookings_customers = $this->admin->get_user_count_with_bookings_laundry('customer',0,0,'cancelled');
    $this->load->view('admin/dashboard_cancelled_bookings_customer_view', compact('cancelled_bookings_customers'));
    $this->load->view('admin/footer_view');
  }

  //bus
  public function dashboard_bus_current_trips_operator()
  {
    $current_trips_operator=$this->admin->get_user_count_with_bookings_bus('deliverer',0,0,'current');
    $this->load->view('admin/dashboard_current_trips_operators_view', compact('current_trips_operator'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_bus_upcoming_trips_operator()
  {
    $upcoming_trips_operator=$this->admin->get_user_count_with_bookings_bus('deliverer',0,0,'upcoming');
    $this->load->view('admin/dashboard_upcoming_trips_operators_view', compact('upcoming_trips_operator'));
    $this->load->view('admin/footer_view');
  }  

  public function dashboard_bus_previous_trips_operator()
  {
    $previous_trips_operator=$this->admin->get_user_count_with_bookings_bus('deliverer',0,0,'previous');
    $this->load->view('admin/dashboard_previous_trips_operators_view', compact('previous_trips_operator'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_bus_completed_trips_operator()
  {
    $completed_trips_operator=$this->admin->get_user_count_with_bookings_bus('deliverer',0,0,'complete');
    $this->load->view('admin/dashboard_completed_trips_operators_view', compact('completed_trips_operator'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_bus_current_trips_customer()
  {
    $current_trips_customer=$this->admin->get_user_count_with_bookings_bus('customer',0,0,'current');
    $this->load->view('admin/dashboard_current_trips_customer_view', compact('current_trips_customer'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_bus_upcoming_trips_customer()
  {
    $upcoming_trips_customer=$this->admin->get_user_count_with_bookings_bus('customer',0,0,'upcoming');
    $this->load->view('admin/dashboard_upcoming_trips_customer_view', compact('upcoming_trips_customer'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_bus_previous_trips_customer()
  {
    $previous_trips_customer=$this->admin->get_user_count_with_bookings_bus('customer',0,0,'previous');
    $this->load->view('admin/dashboard_previous_trips_customer_view', compact('previous_trips_customer'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_bus_completed_trips_customer()
  {
    $completed_trips_customer=$this->admin->get_user_count_with_bookings_bus('customer',0,0,'complete');
    $this->load->view('admin/dashboard_completed_trips_customer_view', compact('completed_trips_customer'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_expired_booking_providers()
  { 
    $expiredBookingProviders = $this->admin->get_user_count_with_bookings('deliverer', 0, 0, 0, 'in_progress');
    $this->load->view('admin/dashboard_expired_booking_providers_view', compact('expiredBookingProviders'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_rejected_booking_providers()
  { 
    $rejectedBookingProviders = $this->admin->get_user_count_with_rejected_bookings('deliverer', 0, 0, 0);
    $this->load->view('admin/dashboard_rejected_booking_providers_view', compact('rejectedBookingProviders'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_accepted_booking_providers()
  { 
    $acceptedBookingProviders = $this->admin->get_user_count_with_bookings('deliverer', 0, 0, 0, 'accept');
    $this->load->view('admin/dashboard_accepted_booking_providers_view', compact('acceptedBookingProviders'));
    $this->load->view('admin/footer_view');
  }

  public function dashboard_delivered_booking_providers()
  { 
    $deliveredBookingProviders = $this->admin->get_user_count_with_bookings('deliverer', 0, 0, 0, 'delivered');
    $this->load->view('admin/dashboard_delivered_booking_providers_view', compact('deliveredBookingProviders'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_created_booking_customer()
  { 
    $createdBookingCustomer = $this->admin->get_user_count_with_bookings('customer', 0, 0, 0, 'open');
    $this->load->view('admin/dashboard_created_booking_customers_view', compact('createdBookingCustomer'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_expired_booking_customer()
  { 
    $expiredBookingCustomer = $this->admin->get_user_count_with_bookings('customer', 0, 0, 0, 'in_progress');
    $this->load->view('admin/dashboard_expired_booking_customers_view', compact('expiredBookingCustomer'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_rejected_booking_customer()
  { 
    $rejectedBookingCustomer = $this->admin->get_user_count_with_rejected_bookings('customer', 0, 0, 0);
    $this->load->view('admin/dashboard_rejected_booking_customers_view', compact('rejectedBookingCustomer'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_accepted_booking_customer()
  { 
    $acceptedBookingCustomer = $this->admin->get_user_count_with_bookings('customer', 0, 0, 0, 'accept');
    $this->load->view('admin/dashboard_accepted_booking_customers_view', compact('acceptedBookingCustomer'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_cancelled_booking_customer()
  { 
    $cancelledBookingCustomer = $this->admin->get_user_count_with_bookings('customer', 0, 0, 0, 'cancel');
    $this->load->view('admin/dashboard_cancelled_booking_customers_view', compact('cancelledBookingCustomer'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_delivered_booking_customer()
  { 
    $deliveredBookingCustomer = $this->admin->get_user_count_with_bookings('customer', 0, 0, 0, 'delivered');
    $this->load->view('admin/dashboard_delivered_booking_customers_view', compact('deliveredBookingCustomer'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_recently_connected_users()
  { 
    $recentlyConnectedUsers = $this->admin->get_user_login_history('recently', 0, 0);
    $this->load->view('admin/dashboard_recently_connected_users_view', compact('recentlyConnectedUsers'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_never_connected_users()
  { 
    $neverConnectedUsers = $this->admin->get_user_login_history('never', 0, 0);
    $this->load->view('admin/dashboard_never_connected_users_view', compact('neverConnectedUsers'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_frequently_connected_users()
  { 
    $frequentlyConnectedUsers = $this->admin->get_user_login_history('frequently', 0, 0);
    $this->load->view('admin/dashboard_frequently_connected_users_view', compact('frequentlyConnectedUsers'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_highest_money_spent_users()
  { 
    $currency_code = $this->input->post('currency_cd_high', TRUE);
    $highestMoneySpentUsers = $this->admin->get_user_by_currency($currency_code, 0, 0, 'high');
    $this->load->view('admin/dashboard_highest_money_spent_users_view', compact('highestMoneySpentUsers'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_lowest_money_spent_users()
  { 
    $currency_code = $this->input->post('currency_cd_low', TRUE);
    $lowestMoneySpentUsers = $this->admin->get_user_by_currency($currency_code, 0, 0, 'high');
    $this->load->view('admin/dashboard_lowest_money_spent_users_view', compact('lowestMoneySpentUsers'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_best_rating_users()
  { 
    $bestRatingUsers = $this->admin->get_user_by_ratings('high', 0, 0);
    $this->load->view('admin/dashboard_best_rating_users_view', compact('bestRatingUsers'));
    $this->load->view('admin/footer_view');
  }
  
  public function dashboard_lowest_rating_users()
  { 
    $lowestRatingUsers = $this->admin->get_user_by_ratings('high', 0, 0);
    $this->load->view('admin/dashboard_lowest_rating_users_view', compact('lowestRatingUsers'));
    $this->load->view('admin/footer_view');
  }
  
  public function get_country_phone_code_by_id()
  { 
    $country_code = $this->input->post('country_code', TRUE);
    echo json_encode($this->admin->get_country_country_phonecode_by_id($country_code));
  }
  
  public function gonagoo_account_history()
  { 
    $history = $this->admin->get_gonagoo_history();
    $this->load->view('admin/gonagoo_account_history_view', compact('history'));
    $this->load->view('admin/footer_view');
  }
  
  public function currencywise_account_history()
  { 
    $currency_code = $this->uri->segment(3);
    $history = $this->admin->get_gonagoo_history_by_currency(trim($currency_code));
    $this->load->view('admin/gonagoo_currencywise_account_history_view', compact('history'));
    $this->load->view('admin/footer_view');
  }
  
  public function currencywise_gonagoo_comission_history()
  { 
    $currency_code = $this->uri->segment(3);
    $cat_id = $this->uri->segment(4);
    $transaction_type = 'commission_amount';
    if($cat_id == 281){
      $transaction_type = 'ticket_commission';
    }
    if($cat_id == 9){
      $transaction_type = 'laundry_commission';
    }
    if($cat_id == 0){
      $transaction_type = '0';
    }
    $history = $this->admin->get_history_by_currency(trim($currency_code), $transaction_type );
    //echo json_encode($history); die();
    $this->load->view('admin/gonagoo_currencywise_gonagoo_commission_history_view', compact('history'));
    $this->load->view('admin/footer_view');
  }
  
  public function currencywise_insurance_history()
  { 
    $currency_code = $this->uri->segment(3);
    $history = $this->admin->get_history_by_currency(trim($currency_code), 'insurance_fee');
    $this->load->view('admin/gonagoo_currencywise_insurance_history_view', compact('history'));
    $this->load->view('admin/footer_view');
  }
  
  public function currencywise_custom_history()
  { 
    $currency_code = $this->uri->segment(3);
    $history = $this->admin->get_history_by_currency(trim($currency_code), 'custom_clearance_fee');
    $this->load->view('admin/gonagoo_currencywise_custom_history_view', compact('history'));
    $this->load->view('admin/footer_view');
  }
  
  public function currencywise_refund_history()
  { 
    $currency_code = $this->uri->segment(3);
    $history_request = $this->admin->get_currencywise_refund_history(trim($currency_code), 'request');
    $history_accept = $this->admin->get_currencywise_refund_history(trim($currency_code), 'accept');
    $history_reject = $this->admin->get_currencywise_refund_history(trim($currency_code), 'reject');
    $this->load->view('admin/gonagoo_currencywise_refund_history_view', compact('history_request','history_accept','history_reject'));
    $this->load->view('admin/footer_view');
  }
  
  public function profile()
  {     
    $this->load->view('admin/profile_view');
    $this->load->view('admin/footer_view');
  }

  public function update_profile()
  {
    $fname = $this->input->post('auth_fname', TRUE);
    $lname = $this->input->post('auth_lname', TRUE);
    $email = $this->input->post('auth_email', TRUE);

    $update_data = array();

    if(!empty($_FILES["profile_image"]["tmp_name"])) {
      $config['upload_path']    = 'resources/profile-images/';                 
      $config['allowed_types']  = '*';       
      $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
      $config['max_height']     = '0';                          
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if($this->upload->do_upload('profile_image')) {   
        $uploads    = $this->upload->data();  
        $profile_image_url =  $config['upload_path'].$uploads["file_name"];  

        $update_data = array(
          "auth_fname" => trim($fname),
          "auth_lname" => trim($lname),
          "auth_email" => trim($email),
          "auth_avatar_url" => $profile_image_url,
        );
      }         
    } else { 
      $update_data = array(
        "auth_fname" => trim($fname),
        "auth_lname" => trim($lname),
        "auth_email" => trim($email),
      );
    }

    if( $this->admin->update_profile($update_data)){
      $this->session->set_flashdata('success','Profile details successfully updated!');
    } else {  $this->session->set_flashdata('error','Unable to update profile details! Try again...');  }
    
    redirect('admin/profile');
  }
  
  public function update_profile_password() 
  {   
    $auth_email = $this->session->userdata('email');
    $old_pass = $this->input->post('auth_old_pass', TRUE);

    $auth = $this->admin->user_authentication($auth_email,$old_pass);
    if($auth['auth_id'] > 0 ) {
      $new_pass = $this->input->post('auth_pass', TRUE);
      if( $this->admin->update_profile_password(trim($new_pass))){
        $this->session->set_flashdata('success1','Password successfully updated!');
      } else {  $this->session->set_flashdata('error1','Unable to update password! Try again...');  }
    } else {  $this->session->set_flashdata('error1','Old password did not match! Try again...'); }
    
    redirect('admin/profile');
  }
  
  public function change_app_version()
  {
    $main_app = $this->admin->get_main_app_version();
    $driver_app = $this->admin->get_driver_app_version();
    $this->load->view('admin/app_version_update_view', compact('main_app', 'driver_app'));			
    $this->load->view('admin/footer_view');		
  }

  public function update_main_app_version() 
  {   
    $id = $this->input->post('main_app_id', TRUE);
    $app_version = $this->input->post('main_app_new_version', TRUE);
    $forcefully_update = $this->input->post('force_update_main', TRUE);
    
    $update_data = array(
      "app_version" => trim($app_version),
      "forcefully_update" => (int)$forcefully_update,
      "update_datetime" => date('Y-m-d h:i:s'),
    );

    if( $this->admin->update_main_app_version((int)$id, $update_data)) {
      $this->session->set_flashdata('success1','App version successfully updated!');
    } else {  $this->session->set_flashdata('error1','Unable to update app version! Try again...');  }
    
    redirect('admin/change-app-version');
  }

  public function update_ios_main_app_version() 
  {   
    $id = $this->input->post('ios_main_app_id', TRUE);
    $app_version_ios = $this->input->post('ios_main_app_new_version', TRUE);
    $forcefully_update_ios = $this->input->post('force_update_ios_main', TRUE);
    
    $update_data = array(
      "app_version_ios" => trim($app_version_ios),
      "forcefully_update_ios" => (int)$forcefully_update_ios,
      "update_datetime_ios" => date('Y-m-d h:i:s'),
    );

    if( $this->admin->update_main_app_version((int)$id, $update_data)) {
      $this->session->set_flashdata('success3','App version successfully updated!');
    } else {  $this->session->set_flashdata('error3','Unable to update app version! Try again...');  }
    
    redirect('admin/change-app-version');
  }

  public function update_driver_app_version() 
  {   
    $id = $this->input->post('driver_app_id', TRUE);
    $app_version = $this->input->post('driver_app_new_version', TRUE);
    $forcefully_update = $this->input->post('force_update_driver', TRUE);
    
    $update_data = array(
      "app_version" => trim($app_version),
      "forcefully_update" => (int)$forcefully_update,
      "update_datetime" => date('Y-m-d h:i:s'),
    );

    if( $this->admin->update_driver_app_version((int)$id, $update_data)) {
      $this->session->set_flashdata('success2','App version successfully updated!');
    } else {  $this->session->set_flashdata('error2','Unable to update app version! Try again...');  }

    redirect('admin/change-app-version');
  }

  public function transaction_graph()
  {
    $filter = $_POST['filter_type'];
    $transaction_currency = $_POST['transaction_currency'];
    
      if($filter == 'daily'){
        $date = $_POST['date'];
        $result = $this->admin->get_daily_transaction_graph($date, $transaction_currency);
        echo json_encode($result);
      }elseif ($filter == 'weekly') {
        $weekly_date = $_POST['weekly_date'];
         $dates = explode(' To ', $weekly_date);
        if(empty($dates))
        {
            return [];
          }else{
            $range['to_date'] = trim($dates[0]);
            $range['from_date'] = trim($dates[1]);
        }
         $result = $this->admin->get_weekly_transaction_graph($range,$transaction_currency);
        echo json_encode($result);
      }elseif ($filter == 'monthly') {
        $year = $_POST['year'];
        $result = $this->admin->get_monthly_transaction_graph($transaction_currency,$year);
        echo json_encode($result);
      }else{
        echo json_encode(['type'=>'Undefine']);
      }
  } // transaction_graph

  public function users_graph()
  {
    $type = $_POST['filter_type'];
    if($type == 'daily'){
      $date = $_POST['date'];
      $data = $this->admin->get_daily_users_graph($date);
      echo json_encode($data);      
    }else if ($type == 'weekly') {
      $weekly_date = $_POST['weekly_date'];
      $data = $this->admin->get_weekly_users_graph($weekly_date);
      echo json_encode($data);
    }else if ($type == 'monthly') {
      $year = $_POST['year'];
      $data = $this->admin->get_monthly_users_graph( $year);
      echo json_encode($data);
    }else{
      echo json_encode([]);
    }
  } // users_graph

  //Manage Promo Code-------------------------------
    public function promocode_master()
    {
      $promocode = $this->admin->get_promocode();
      //echo json_encode($promocode); die();
      $this->load->view('admin/promocode_list_view', compact('promocode'));     
      $this->load->view('admin/footer_view');   
    }
    public function activate_promo_code()
    {
      $promo_code_id = (int) $this->input->post('id', TRUE);
      if($this->admin->activate_promo_code($promo_code_id)){echo 'success'; } else { echo 'failed';}
    }
    public function inactivate_promo_code()
    {
      $promo_code_id = (int) $this->input->post('id', TRUE);
      if($this->admin->inactivate_promo_code($promo_code_id)){echo 'success';} else { echo 'failed';}
    }
    public function delete_promo_code()
    {
      $promo_code_id = (int) $this->uri->segment('3');
      if( $this->admin->delete_promo_code($promo_code_id)) {
          $this->session->set_flashdata('success','Promocode Deleted successfully!');
      }   else {  $this->session->set_flashdata('error','Unable to delete Promocode! Try again...'); }
      redirect('admin/promo-code');                 
    }
    public function add_promocode()
    {
      $categories = $this->dimension->get_categories();
      $this->load->view('admin/promo_code_add_view' , compact('categories'));
      $this->load->view('admin/footer_view');
    }
    public function register_promocode()
    {
      //echo json_encode($_POST); die();
      $promo_code = $this->input->post('promo_code', TRUE);
      $discount_percent = $this->input->post('discount_percent', TRUE);
      $promocode_title = $this->input->post('promocode_title', TRUE);
      $description = $this->input->post('description', TRUE);
      $start_date = $this->input->post('start_date', TRUE);
      $expire_date = $this->input->post('expire_date', TRUE);
      $limit = $this->input->post('limit', TRUE);
      $services = $this->input->post('service_id', TRUE);
      $service_id = implode(',',$services);
      //echo json_encode($service_id); die();
      
      $update_data = array();

      if(!empty($_FILES["promo_code_image"]["tmp_name"])) {
        $config['upload_path']    = 'resources/promocode-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('promo_code_image')) {   
          $uploads    = $this->upload->data();  
          $promo_code_image_url =  $config['upload_path'].$uploads["file_name"];  

          $update_data = array(
            "promo_code" => strtoupper(trim($promo_code)),
            "promo_code_version" => '1',
            "discount_percent" => trim($discount_percent),
            "promocode_avtar_url" => trim($promo_code_image_url),
            "promo_title" => trim($promocode_title),
            "promo_desc" => trim($description),
            "code_start_date" => trim($start_date),
            "code_expire_date" => trim($expire_date),
            "cre_datetime" => date('Y-m-d h:i:s'),
            "service_id" => trim($service_id),
            "no_of_limit" => trim($limit),
          );
        }         
      } else { 
        $update_data = array(
          "promo_code" => strtoupper(trim($promo_code)),
          "promo_code_version" => '1',
          "discount_percent" => trim($discount_percent),
          "promo_title" => trim($promocode_title),
          "promo_desc" => trim($description),
          "code_start_date" => trim($start_date),
          "code_expire_date" => trim($expire_date),
          "cre_datetime" => date('Y-m-d h:i:s'),
          "service_id" => trim($service_id),
          "no_of_limit" => trim($limit),
        );
      }
      //echo json_encode($update_data); die();
      if($this->admin->register_promocode($update_data)){$this->session->set_flashdata('success','Promocode successfully added!');
      } else {$this->session->set_flashdata('error','Unable to add new Promocode! Try again...'); } 
      redirect('admin/add-promo');
    }
    public function edit_promocode()
    { 
      //echo json_encode($_POST); die();
      $promo_code_id = (int) $this->uri->segment('3');
      $promo_code = $this->admin->get_promocode_by_id($promo_code_id);
      $categories = $this->dimension->get_categories();
      $this->load->view('admin/promo_code_edit_view', compact('promo_code' , 'categories'));
      $this->load->view('admin/footer_view');
    }
    public function update_promocode()
    {
      //echo json_encode($service_id); die();
      $promo_code_id = $this->input->post('promo_code_id', TRUE);
      $promo_code = $this->input->post('promo_code', TRUE);
      $discount_percent = $this->input->post('discount_percent', TRUE);
      $promocode_title = $this->input->post('promocode_title', TRUE);
      $description = $this->input->post('description', TRUE);
      $start_date = $this->input->post('start_date', TRUE);
      $expire_date = $this->input->post('expire_date', TRUE);
      $services = $this->input->post('service_id', TRUE);
      $limit = $this->input->post('limit', TRUE);
      $service_id = implode(',',array_unique($services));

      $update_data = array();

      if(!empty($_FILES["promo_code_image"]["tmp_name"])) {
        $config['upload_path']    = 'resources/promocode-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('promo_code_image')) {   
          $uploads    = $this->upload->data();  
          $promo_code_image_url =  $config['upload_path'].$uploads["file_name"];  

          $update_data = array(
            "promo_code" => strtoupper(trim($promo_code)),
            "discount_percent" => trim($discount_percent),
            "promocode_avtar_url" => trim($promo_code_image_url),
            "promo_title" => trim($promocode_title),
            "promo_desc" => trim($description),
            "code_start_date" => trim($start_date),
            "code_expire_date" => trim($expire_date),
            "cre_datetime" => date('Y-m-d h:i:s'),
            "service_id" => trim($service_id),
            "no_of_limit" => trim($limit),
          );
        }         
      } else { 
        $update_data = array(
            "promo_code" => strtoupper(trim($promo_code)),
            "discount_percent" => trim($discount_percent),
            "promo_title" => trim($promocode_title),
            "promo_desc" => trim($description),
            "code_start_date" => trim($start_date),
            "code_expire_date" => trim($expire_date),
            "mod_datetime" => date('Y-m-d h:i:s'),
            "service_id" => trim($service_id),
            "no_of_limit" => trim($limit),
          );
      }
      if($this->admin->update_promo_code($promo_code_id , $update_data)){$this->session->set_flashdata('success','Promocode successfully Updated!');
      }else{$this->session->set_flashdata('error','Unable to Update Promocode! Try again...');} 
      $promo_code = $this->admin->get_promocode_by_id($promo_code_id);
      $categories = $this->dimension->get_categories();
      $this->load->view('admin/promo_code_edit_view', compact('promo_code' , 'categories'));
      $this->load->view('admin/footer_view');
    }
    public function check_promo_code_exists()
    {
      $promo_code = $this->input->post('promo_code');
      $data = $this->admin->is_promocode_exists(trim($promo_code));
      if($data) echo 1; 
      else echo 0;
    }
  //Manage Promo Code-------------------------------



}

/* End of file Administration.php */
/* Location: ./application/controllers/Administration.php */