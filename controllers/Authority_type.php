<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authority_type extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('is_admin_logged_in')){
            $this->load->model('admin_model', 'admin'); 
            $this->load->model('authority_type_model', 'auth_type');        
            $id = $this->session->userdata('userid');
            $user = $this->admin->logged_in_user_details($id);
            $permissions = $this->admin->get_auth_permissions($user['type_id']);
            if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));   
        } else{ redirect('admin');}
    }
    
    public function index()
    {
        if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
    }

    public function authority_type()
    {       
        $authority_type = $this->auth_type->get_authority_types();
        $this->load->view('admin/authority_type_list_view', compact('authority_type'));         
        $this->load->view('admin/footer_view');     
    }
    public function edit_authority_type()
    {   
        $auth_type_id = (int) $this->uri->segment('3');
        $auth_type = $this->auth_type->get_authority_type_detail($auth_type_id);

        $this->load->view('admin/authority_type_edit_view', compact('auth_type'));
        $this->load->view('admin/footer_view');
    }
    public function update_authority_type()
    {
        $auth_type_id = $this->input->post('auth_type_id', TRUE);
        $auth_type = $this->input->post('auth_type', TRUE);
        $udpate_data = array(
        'auth_type_id' => (int) $auth_type_id,
        'auth_type' => $auth_type
        );
        if(! $this->auth_type->get_authority_type_by_type($auth_type) ) {
            if( $this->auth_type->update_authority_type($udpate_data)){
                $this->session->set_flashdata('success','authority type successfully updated!');
            }   else {  $this->session->set_flashdata('error','Unable to update authority type! Try again...'); }
        } else {    $this->session->set_flashdata('error','authority type already exists! Try again with different type...');   }
        redirect('admin/edit-authority-type/'. $auth_type_id);
    }
    public function active_authority_type()
    {
        $auth_type_id = (int) $this->input->post('id', TRUE);
        if( $this->auth_type->activate_authority_type($auth_type_id) ) {    echo 'success'; }   else {  echo 'failed';  }
    }
    public function inactive_authority_type()
    {
        $auth_type_id = (int) $this->input->post('id', TRUE);
        if( $this->auth_type->inactivate_authority_type($auth_type_id) ) {  echo 'success'; }   else {  echo 'failed';  }
    }
    public function delete_authority_type()
    {
        $auth_type_id = (int) $this->uri->segment('3');
        if( $this->auth_type->delete_authority_type($auth_type_id) ) {
            $this->session->set_flashdata('success','authority type successfully deleted!');
        }   else {  $this->session->set_flashdata('error','Unable to delete authority type! Try again...'); }
        redirect('authority_type');                 
    }
    public function add_authority_type()
    {
        $this->load->view('admin/authority_type_add_view');
        $this->load->view('admin/footer_view');
    }
    public function register_authority_type()
    {
        $auth_type = $this->input->post('auth_type', TRUE);
        if(! $this->auth_type->get_authority_type_by_type($auth_type) ) {
            if($this->auth_type->register_authority_type($auth_type)){
                $this->session->set_flashdata('success','authority type successfully added!');
            } else {    $this->session->set_flashdata('error','Unable to add new authority type! Try again...');    } 
        } else {    $this->session->set_flashdata('error','authority type already exists! Try again with different type...');   }
        redirect('admin/add-authority-type');
    }

}

/* End of file Authority_type.php */
/* Location: ./application/controllers/Authority_type.php */