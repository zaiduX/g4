<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advance_payment_bus_reservation extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('Advance_payment_bus_reservation_model', 'payment');
      $this->load->model('standard_dimension_model', 'standard_dimension');   
      $this->load->model('Web_user_model', 'user');   
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $adv_payments = $this->payment->get_payments();
    $this->load->view('admin/advance_payment_bus_reservation_list_view', compact('adv_payments'));      
    $this->load->view('admin/footer_view');   
  }

  public function add()
  {
    $countries = $this->payment->get_countries();
    $this->load->view('admin/advance_payment_bus_reservation_add_view',compact('countries','categories'));
    $this->load->view('admin/footer_view');
  }
  
  public function register()
  { //echo json_encode($_POST); die();
    $country_id = $this->input->post('country_id', TRUE);
    $advance_payment = $this->input->post('advance_payment', TRUE);
    $gonagoo_commission = $this->input->post('gonagoo_commission', TRUE);
    $insert_flag = false;

    if($country_id > 0 ) {
      $insert_data = array(
        "country_id" => (int)trim($country_id), 
        "advance_payment" => number_format($advance_payment,2,'.',''),
        "gonagoo_commission" => number_format($gonagoo_commission,2,'.',''),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
        "status" =>  1, 
      );
      //echo json_encode($insert_data); die();
      if(! $this->payment->check_payment($insert_data) ) {
        if($this->payment->register_payment($insert_data)){ $insert_flag = true;  } 
      } 
    } else {  $this->session->set_flashdata('error','Please select country!');  }

    if($insert_flag){ $this->session->set_flashdata('success','Payment setup successfully added!'); }
    else{ $this->session->set_flashdata('error','Unable to add or Setup already exists for selected country!');  }

    redirect('admin/advance-payment-bus-reservation/add');
  }

  public function edit()
  { 
    if( $id = $this->uri->segment(4) ) {      
      if( $payment = $this->payment->get_payment_detail($id)){
        $countries = $this->payment->get_countries();
        $this->load->view('admin/advance_payment_bus_reservation_edit_veiw', compact('payment','countries'));
        $this->load->view('admin/footer_view');
      } else { redirect('admin/advance-payment'); }
    } else { redirect('admin/advance-payment'); }
  }

  public function update()
  { 
    $bap_id = $this->input->post('bap_id', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $advance_payment = $this->input->post('advance_payment', TRUE);
    $gonagoo_commission = $this->input->post('gonagoo_commission', TRUE);

    $update_data = array(       
      "advance_payment" => number_format($advance_payment,2,'.',''),
      "gonagoo_commission" => number_format($gonagoo_commission,2,'.',''),
      "cre_datetime" =>  date("Y-m-d H:i:s"), 
      "status" =>  1,
    );
    
    $pay = $this->payment->get_payment_detail($bap_id);
    if($pay['country_id'] == $country_id && $pay['bap_id'] == $bap_id ) {
      //echo json_encode($update_data); die();
      if($this->payment->update_payment($update_data, $bap_id)){
        $this->session->set_flashdata('success','Payment setup successfully updated!');
      } else{ $this->session->set_flashdata('error','Unable to update configuration or same configuration found! Try with different configuration...');  }
    } else{ $this->session->set_flashdata('error','Failed! Some unwanted scripting injection found! Please refresh the page and try again...');  }
    redirect('admin/advance-payment-bus-reservation/edit/'.$bap_id);
  }

  public function delete()
  {
    $id = $this->input->post('id', TRUE);
    if($this->payment->delete_payment($id)){ echo 'success'; } else { echo "failed";  } 
  }

}

/* End of file Advance_payment_bus_reservation.php */
/* Location: ./application/controllers/Advance_payment_bus_reservation.php */