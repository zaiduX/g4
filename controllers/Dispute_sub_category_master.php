<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dispute_sub_category_master extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Dispute_sub_category_master_model', 'dispute');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$user = $this->admin->logged_in_user_details($user['type_id']);
			$permissions = $this->admin->get_auth_permissions($id);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	public function index() {
		$categories = $this->dispute->get_categories();
		$sub_categories = $this->dispute->get_sub_categories();
		$this->load->view('admin/services/dispute_sub_category_list_view', compact('categories','sub_categories'));			
		$this->load->view('admin/footer_view');
	}
	public function register() {
		//echo json_encode($_POST); die();
		$dispute_sub_cat_title = $this->input->post('dispute_sub_cat_title', TRUE);
		$dispute_cat_id = $this->input->post('dispute_cat_id', TRUE); $dispute_cat_id = (int)$dispute_cat_id;
		$dispute_sub_cat_id = $this->input->post('dispute_sub_cat_id', TRUE); $dispute_sub_cat_id = (int)$dispute_sub_cat_id;
		if($dispute_sub_cat_id == 0) { //Register
	 		$insert_data = array(
	 			"dispute_sub_cat_title" => trim($dispute_sub_cat_title),
	 			"dispute_cat_id" => trim($dispute_cat_id),
	 			"cre_datetime" => date("Y-m-d H:i:s"),
	 			"status" => 1,
	 			"update_datetime" => date("Y-m-d H:i:s"),
	 		);
	 		if($this->dispute->register_sub_cateogry($insert_data)) {
	 			$this->session->set_flashdata('success', 'Dispute  sub category successfully added!');
	 		} else { $this->session->set_flashdata('error', 'Unable to add dispute  sub category! Try again...'); }
		} else { //Update
			$update_data = array(
	 			"dispute_sub_cat_title" => trim($dispute_sub_cat_title),
	 			"update_datetime" => date("Y-m-d H:i:s"),
	 		);
	 		if($this->dispute->update_sub_cateogry($update_data, $dispute_sub_cat_id)){
				$this->session->set_flashdata('success','Dispute sub category successfully updated!');
			} else { 	$this->session->set_flashdata('error','Unable to update dispute sub category! Try again...');	} 
		}
 		redirect('admin/dispute-sub-category-master');
	}
	public function inactivate_dispute_sub_category() {
		$dispute_sub_cat_id = $this->input->post('dispute_sub_cat_id', TRUE);
		$update_data = array(
 			"status" => 0,
 			"update_datetime" => date("Y-m-d H:i:s"),
 		);
		if($this->dispute->update_sub_cateogry($update_data, $dispute_sub_cat_id)) { echo 'success';
		} else { echo 'failed';	}
	}
	public function activate_dispute_sub_category() {
		$dispute_sub_cat_id = $this->input->post('dispute_sub_cat_id', TRUE);
		$update_data = array(
 			"status" => 1,
 			"update_datetime" => date("Y-m-d H:i:s"),
 		);
		if($this->dispute->update_sub_cateogry($update_data, $dispute_sub_cat_id)) { echo 'success';
		} else { echo 'failed';	}
	}
}

/* End of file Dispute_sub_category_master.php */
/* Location: ./application/controllers/Dispute_sub_category_master.php */