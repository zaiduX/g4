<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Api_services extends CI_Controller {
  private $errors = array();
  private $cust = array();
  public function __construct() {
    parent::__construct();
    $this->load->model('Api_model_services', 'api');   
    $this->load->model('Notification_model_bus', 'notification'); 
    $this->load->model('Web_user_model', 'user');
    $this->load->model('api_model', 'api_courier');
    $this->load->model('api_model_sms', 'api_sms');
    $this->load->model('Users_model', 'users');
    $this->load->model('transport_model', 'transport');

    header('Content-Type: Application/json'); 
    if( $this->input->method() != 'post' ) exit('No direct script access allowed');
    if( $this->input->is_ajax_request() ) exit('No direct script access allowed');

    $cust_id = $this->input->post('cust_id', TRUE);    
    $this->api->update_last_login($cust_id);
    if(!empty($cust_id)){  $this->cust = $this->api->get_consumer_datails($cust_id, true); }

    if(isset($_POST)){

      $lang = $this->input->post('lang', TRUE);
      if( $lang == 'fr' ) {
        $this->config->set_item('language', 'french');
        $this->lang->load('frenchApi_lang','french');
      }
      else if( $lang == 'sp' ) {
        $this->config->set_item('language', 'spanish');
        $this->lang->load('spanishApi_lang','spanish');
      }
      else{
        $this->config->set_item('language', 'english');
        $this->lang->load('englishApi_lang','english');
        $this->lang->load('englishFront_lang','english');
      }

      foreach($_POST as $key => $value){ 
        if(trim($value) == "") { $this->errors[] = ucwords(str_replace('_', ' ', $key)); }        
      }
      if(!empty($this->errors)) { 
        echo json_encode(array('response' => 'failed', 'message' => 'Empty field(s) - '.implode(', ', $this->errors))); 
      }
    }
  }
  public function index() { exit('No direct script access allowed.'); }

  //Start customer profile settings------------------------
    public function get_profile_details()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        $cust_id = (int) $cust_id;
        
        $detail['personal'] = $this->api->get_consumer_datails($cust_id);
        if($skills = $this->api->get_customer_skills($cust_id) ) { $detail['skills'] = $skills; }
        if($experience = $this->api->get_customer_experience($cust_id) ) {  $detail['experience'] = $experience; }
        if($education = $this->api->get_customer_education($cust_id) ) {  $detail['education'] = $education; }
        if($portfolio = $this->api->get_customers_portfolio($cust_id) ) {  $detail['portfolio'] = $portfolio; }
        if($business_photos = $this->api->get_business_photos($cust_id) ) {  $detail['business_photos'] = $business_photos; }
        if($documents = $this->api->get_documents($cust_id) ) {  $detail['documents'] = $documents; }
        if($payment_details = $this->api->get_payment_details($cust_id) ) {  $detail['payment_details'] = $payment_details; }

        $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'),"profile_details" => $detail];     
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
    public function basic_info_update()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE);
        $mobile1 = $this->input->post('mobile1', TRUE);
        $firstname = $this->input->post('firstname', TRUE);
        $lastname = $this->input->post('lastname', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $country_id = $this->input->post('country_id', TRUE);
        $state_id = $this->input->post('state_id', TRUE);
        $city_id = $this->input->post('city_id', TRUE);
      
        $update_data = array(
          'mobile1' => $mobile1,
          'firstname' => $firstname,
          'lastname' => $lastname,
          'gender' => $gender,
          'country_id' => $country_id,
          'state_id' => $state_id,
          'city_id' => $city_id,
        );

        if( $this->api->update_user_basic_info($cust_id, $update_data)){
          $this->response = ['response' => 'success', 'message' => $this->lang->line('update_success')]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
    public function update_avatar()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){

        if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('profile_image')) {
            $uploads    = $this->upload->data();  
            $avatar_url =  $config['upload_path'].$uploads["file_name"];  

            $old_profile_image = $this->api->get_old_profile_image($cust_id);
            
            if( $this->api->update_profile_image($avatar_url, $cust_id)){
              if( $old_profile_image != "NULL") { unlink($old_profile_image); }
              $user = $this->api->get_consumer_datails($cust_id);
              $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "user" => $user];
            } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found').'...']; }
        // update user last login
        $this->api->update_last_login($cust_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
    public function update_cover()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      if($this->api->is_user_active_or_exists($cust_id)){
        if( !empty($_FILES["cover_image"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/cover-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('cover_image')) {   
            $uploads    = $this->upload->data();  
            $cover_url =  $config['upload_path'].$uploads["file_name"];  
            $old_cover_image = $this->api->get_old_cover_image($cust_id);
            if( $this->api->update_cover_image($cover_url, $cust_id)){
              if( $old_cover_image  != "NULL" ){ unlink($old_cover_image); }
              $user = $this->api->get_consumer_datails($cust_id);
              $this->response = ['response' => 'success','message'=>$this->lang->line('update_success'), "user" => $user];
            } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found').'...']; }
        // update user last login
        $this->api->update_last_login($cust_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
    public function update_overview()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      $profession = $this->input->post('profession', TRUE);
      $overview = $this->input->post('overview', TRUE);

      if($user = $this->api->is_user_active_or_exists($cust_id)){
        $update_data = array(
          "profession" => trim($profession),
          "overview" => trim($overview),
        );

        if( $this->api->update_overview($cust_id,$update_data) ){
          $user = $this->api->get_consumer_datails($cust_id);
          $this->response = ['response' => 'success','message'=>$this->lang->line('update_success'), "user" => $user];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
        // update user last login
        $this->api->update_last_login($cust_id);
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);
    }
    public function get_portfolio_details()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        if($this->api->is_user_active_or_exists($cust_id)){
          if( $portfolios = $this->api->get_customers_portfolio($cust_id)){         
            $this->response = ['response' => 'success','message'=> $this->lang->line('get_value_success'), "customers_portfolios" => $portfolios];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_value_failed').'...']; }
          $this->api->update_last_login($cust_id);
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; } 
        echo json_encode($this->response);
      }
    }
    public function register_portfolio()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      $title = $this->input->post('title', TRUE);
      $cat_id = $this->input->post('cat_id', TRUE);
      $subcat_ids = $this->input->post('subcat_ids', TRUE);
      $tags = $this->input->post('tags', TRUE);
      $description = $this->input->post('description', TRUE);

      if( !empty($_FILES["attachement"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
          if($this->upload->do_upload('attachement')) {   
            $uploads    = $this->upload->data();  
            $attachement =  $config['upload_path'].$uploads["file_name"];  
          } else { $attachement = "NULL"; }
        } else { $attachement = "NULL"; }
        
        $insert_data = array(
          "cust_id" => (int)$cust_id,
          "title" => $title,
          "cat_id" => (int)$cat_id,
          "subcat_ids" => $subcat_ids,
          "description" => $description,
          "tags" => $tags,
          "attachement_url" => $attachement,
          "cre_datetime" => date('Y-m-d H:i:s'),
        );

        if( $portfolio_id = $this->api->register_portfolio($insert_data)){            
          $this->response = ['response' => 'success','message'=> $this->lang->line('portfolio_add_successfully')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('portfolio_add_successfully').'...']; }
        echo json_encode($this->response);
    }
    public function update_portfolio()
    {
      $portfolio_id = $this->input->post('portfolio_id', TRUE);
      $title = $this->input->post('title', TRUE);
      $cat_id = $this->input->post('cat_id', TRUE);
      $subcat_ids = $this->input->post('subcat_ids', TRUE);
      $tags = $this->input->post('tags', TRUE);
      $description = $this->input->post('description', TRUE);

      if( !empty($_FILES["attachement"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('attachement')) {   
          $uploads    = $this->upload->data();  
          $attachement =  $config['upload_path'].$uploads["file_name"];  

          $update_data = array(
            "title" => $title,
            "cat_id" => (int)$cat_id,
            "subcat_ids" => $subcat_ids,
            "description" => $description,
            "tags" => $tags,
            "attachement_url" => $attachement,
          );
        }
      } else { 
        $update_data = array(
          "title" => $title,
          "cat_id" => (int)$cat_id,
          "subcat_ids" => $subcat_ids,
          "description" => $description,
          "tags" => $tags,
        );
      }

      if( $this->api->update_portfolio($portfolio_id, $update_data)){  
        $detail = $this->api->get_portfolio_detail($portfolio_id);      
        $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "detail" => $detail];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed').'...']; }
      echo json_encode($this->response);
    }
    public function delete_portfolio()
    {
      $portfolio_id = $this->input->post('portfolio_id', TRUE);
      if($this->api->delete_portfolio($portfolio_id)){
        $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed').'...']; }
      echo json_encode($this->response);
    }
    public function get_documents()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        if( $docs = $this->api->get_documents($cust_id) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_documenets" => $docs];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'),"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function add_document()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        $title = $this->input->post('title', TRUE);
        $description = $this->input->post('description', TRUE);
        
        if( !empty($_FILES["attachement"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/attachements/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('attachement')) {   
            $uploads    = $this->upload->data();  
            $attachement_url =  $config['upload_path'].$uploads["file_name"];  
          }      

          $register_data = array(
            "cust_id" => $cust_id,
            "doc_title" => trim($title),
            "doc_desc" => nl2br($description),
            "attachement_url" => trim($attachement_url),
            "cre_datetime" => date("Y-m-d H:i:s")
          );
          if( $this->api->register_document($register_data) ) {
            $this->response = ['response' => 'success','message'=> $this->lang->line('add_success')];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function update_document()
    { 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id; 
      if($this->api->is_user_active_or_exists($cust_id)){
        $doc_id = $this->input->post('doc_id', TRUE); $doc_id = (int) $doc_id;
        $title = $this->input->post('title', TRUE);
        $description = $this->input->post('description', TRUE);
        
        if( !empty($_FILES["attachement"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/attachements/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('attachement')) {   
            $uploads    = $this->upload->data();  
            $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
            $update_data = array( "doc_title" => trim($title), "doc_desc" => nl2br($description), "attachement_url" => trim($attachement_url)); 
          } else{ $attachement_url = "NULL"; }
        } else { $attachement_url = "NULL"; $update_data = array( "doc_title" => trim($title), "doc_desc" => nl2br($description) ); }
        $old_doc_url = $this->api->get_old_document_url($cust_id, $doc_id);
        if( $this->api->update_document($doc_id, $update_data) ) { 
          if($attachement_url != "NULL"){ unlink(trim($old_doc_url)); }
          $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function delete_document()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        $doc_id = $this->input->post('doc_id', TRUE);  $doc_id = (int) $doc_id;
        $old_doc_url = $this->api->get_old_document_url($cust_id, $doc_id);
        if( $this->api->delete_document($cust_id, $doc_id) ) {
           unlink(trim($old_doc_url));
          $this->response = ['response' => 'success','message'=> $this->lang->line('remove_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function skill_list()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE);
        $skills = $this->api->get_customer_skills($cust_id); 
        $old_sub_cat =  $skills['confirmed_skills_name'];
        $sub_category = $this->api->get_category_by_id_all();
        if(empty($skills)) {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('skill_list_failed'), 'users_skill_list' => array()]; 
        } else {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('skill_list_success'), 'exist_skills' => $old_sub_cat , 'skills' => $sub_category]; 
        }
        echo json_encode($this->response);
      }
    }
    public function skill_update()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE);
        $skills = $this->input->post("skills");
        $skills_name = "";
        $skills_id = "";
        $today = date('Y-m-d H:i:s');
        foreach ($skills as $s ) {
          $cat_s = $this->api->get_category_by_ids($s);
          $skills_name .= $cat_s['cat_name'].",";
          $skills_id .= $s.",";
        }
        $skills_name =  mb_substr($skills_name, 0, -1);
        $skills_id =  mb_substr($skills_id, 0, -1);
        $previous_skills = $this->api->get_customer_skills($cust_id); 
        $edit_data = array(
          'cust_id' => (int) $cust_id,
          'cs_id' => (int) $previous_skills['cs_id'],
          'confirmed_skills' => $skills_id,
          'confirmed_skills_name' => $skills_name
        );
        // echo json_encode($edit_data); die();
        if( $this->api->update_user_skills($edit_data)) {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('skill_update_success')]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('skill_update_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
    public function get_paymnet_details()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        if( $details = $this->api->get_payment_details($cust_id) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('get_list_success'), "user_payment_details" => $details];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function add_paymnet_detail()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        $payment_method = $this->input->post('payment_method', TRUE);
        $brand_name = $this->input->post('brand_name', TRUE);
        $card_number = $this->input->post('card_number', TRUE);
        $bank_name = $this->input->post('bank_name', TRUE);
        $bank_address = $this->input->post('bank_address', TRUE);
        $expirty_date = $this->input->post('expirty_date', TRUE);
        $account_title = $this->input->post('account_title', TRUE);
        $bank_short_code = $this->input->post('bank_short_code', TRUE);
      
        $register_data = array(
          "cust_id" => $cust_id,
          "payment_method" => trim($payment_method),
          "brand_name" => trim($brand_name),
          "card_number" => trim($card_number),
          "bank_name" => trim($bank_name),
          "account_title" => trim($account_title),
          "bank_short_code" => trim($bank_short_code),
          "bank_address" => trim($bank_address),
          "expirty_date" => trim($expirty_date),
          "pay_status" => 1,
          "cre_datetime" => date("Y-m-d H:i:s")
        );
        if( $this->api->register_payment_details($register_data) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('add_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function update_paymnet_detail()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        $pay_id = $this->input->post('pay_id', TRUE); $pay_id = (int) $pay_id;
        $payment_method = $this->input->post('payment_method', TRUE);
        $brand_name = $this->input->post('brand_name', TRUE);
        $card_number = $this->input->post('card_number', TRUE);
        $bank_name = $this->input->post('bank_name', TRUE);
        $bank_address = $this->input->post('bank_address', TRUE);
        $expirty_date = $this->input->post('expirty_date', TRUE);
        $account_title = $this->input->post('account_title', TRUE);
        $bank_short_code = $this->input->post('bank_short_code', TRUE);
      
        $update_data = array(
          "payment_method" => trim($payment_method),
          "brand_name" => trim($brand_name),
          "card_number" => trim($card_number),
          "bank_name" => trim($bank_name),
          "account_title" => trim($account_title),
          "bank_short_code" => trim($bank_short_code),
          "bank_address" => trim($bank_address),
          "expirty_date" => trim($expirty_date),
        );
        if( $this->api->update_payment_details($pay_id, $update_data) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('update_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function delete_paymnet_detail()
    {
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      if($this->api->is_user_active_or_exists($cust_id)){
        $pay_id = $this->input->post('pay_id', TRUE);  $pay_id = (int) $pay_id;
        if( $this->api->delete_payment_detail($cust_id, $pay_id) ) {
          $this->response = ['response' => 'success','message'=> $this->lang->line('remove_success')];
        } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed').'...']; }
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive').'...',"user_status" => "0"]; }
      echo json_encode($this->response);  
    }
    public function category_type_master()
    {
      if(empty($this->errors)){
        $type_list = $this->api->get_category_type();

        if(empty($type_list)) {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('category_list_failed'), 'category_type_list' => array()]; 
        } else {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('category_list_success'), 'category_type_list' => $type_list]; 
        }
        echo json_encode($this->response);
      }
    }
    public function category_list_master()
    {
      if(empty($this->errors)){
        $category_list = $this->api->get_category();

        if(empty($category_list)) {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('category_list_failed'), 'category_list' => array()]; 
        } else {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('category_list_success'), 'category_list' => $category_list]; 
        }
        echo json_encode($this->response);
      }
    }
    public function language_list_master()
    {
      if(empty($this->errors)){
        $language_list = $this->api->get_language_master();

        if(empty($language_list)) {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('language_list_failed'), 'language_list' => array()]; 
        } else {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('language_list_success'), 'language_list' => $language_list]; 
        }
        echo json_encode($this->response);
      }
    }
    public function language_update()
    {

      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE);
        $lang_known = $this->input->post('lang_known', TRUE);
        $update_data = array(
          'cust_id' => (int) $cust_id,
          'lang_known' => $lang_known
        );

        if( $this->api->update_user_language($update_data)){
          $this->response = ['response' => 'success', 'message' => $this->lang->line('language_updated_success')]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('language_updated_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
    public function education_list()
    {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE);
        $users_education_list = $this->api->get_customer_education($cust_id);
        if(empty($users_education_list)) {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('education_list_failed')]; 
        } else {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('education_list_success'), 'users_education_list' => $users_education_list]; 
        }
        echo json_encode($this->response);
      }
    }
    public function education_add()
    {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE);
        $activities = $this->input->post('activities', TRUE);
        $description = $this->input->post('description', TRUE);
        $start_date = $this->input->post('start_date', TRUE);
        $end_date = $this->input->post('end_date', TRUE);
        $specialization = $this->input->post('specialization', TRUE);
        $qualification = $this->input->post('qualification', TRUE);
        $qualify_year = $this->input->post('qualify_year', TRUE);
        $institute_name = $this->input->post('institute_name', TRUE);
        $institute_address = $this->input->post('institute_address', TRUE);
        $certificate_title = $this->input->post('certificate_title', TRUE);
        $grade = $this->input->post('grade', TRUE);
        $remark = $this->input->post('remark', TRUE);
        $cre_datetime = date('Y-m-d H:i:s');
        $mod_datetime = date('Y-m-d H:i:s');

        $add_data = array(
          'cust_id' => (int) $cust_id,
          'activities' => $activities,
          'description' => $description,
          'start_date' => $start_date,
          'end_date' => $end_date,
          'specialization' => $specialization,
          'qualification' => $qualification, 
          'qualify_year' => $qualify_year, 
          'institute_name' => $institute_name, 
          'institute_address' => $institute_address, 
          'certificate_title' => $certificate_title, 
          'grade' => $grade, 
          'remark' => $remark, 
          'cre_datetime' => $cre_datetime, 
          'mod_datetime' => $mod_datetime
        );
        if( $id = $this->api->add_user_education($add_data)) {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('education_add_success'), 'edu_id' => $id]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('education_add_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
    public function education_update()
    {

      if(empty($this->errors)){ 
        $edu_id = $this->input->post('edu_id', TRUE);
        $activities = $this->input->post('activities', TRUE);
        $description = $this->input->post('description', TRUE);
        $start_date = $this->input->post('start_date', TRUE);
        $end_date = $this->input->post('end_date', TRUE);
        $specialization = $this->input->post('specialization', TRUE);
        $qualification = $this->input->post('qualification', TRUE);
        $qualify_year = $this->input->post('qualify_year', TRUE);
        $institute_name = $this->input->post('institute_name', TRUE);
        $institute_address = $this->input->post('institute_address', TRUE);
        $certificate_title = $this->input->post('certificate_title', TRUE);
        $grade = $this->input->post('grade', TRUE);
        $remark = $this->input->post('remark', TRUE);
        $mod_datetime = date('Y-m-d H:i:s');
        

        $update_data = array(
          'edu_id' => (int) $edu_id,
          'activities' => $activities,
          'description' => $description,
          'start_date' => $start_date,
          'end_date' => $end_date,
          'specialization' => $specialization,
          'qualification' => $qualification,
          'qualify_year' => $qualify_year,
          'institute_name' => $institute_name,
          'institute_address' => $institute_address,
          'certificate_title' => $certificate_title,
          'grade' => $grade,
          'remark' => $remark,
          'mod_datetime' => $mod_datetime
        );

        if( $this->api->update_user_education($update_data)){
          $this->response = ['response' => 'success', 'message' => $this->lang->line('education_update_success')]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('education_update_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
    public function education_delete()
    {

      if(empty($this->errors)){ 
        $edu_id = $this->input->post('edu_id', TRUE);

        if( $this->api->delete_user_education($edu_id)){
          $this->response = ['response' => 'success', 'message' => $this->lang->line('education_delete_success')]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('education_delete_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
    public function experience_list()
    {
      if(empty($this->errors)){
        $cust_id = $this->input->post('cust_id', TRUE);
        $users_experience_list = $this->api->get_customer_experience($cust_id);
        if(empty($users_experience_list)) {
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('experience_list_failed'), 'users_experience_list' => array()]; 
        } else {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('experience_list_success'), 'users_experience_list' => $users_experience_list]; 
        }
        echo json_encode($this->response);
      }
    }
    public function experience_add()
    {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE);
        $org_name = $this->input->post('org_name', TRUE);
        $job_title = $this->input->post('job_title', TRUE);
        $job_location = $this->input->post('job_location', TRUE);
        $job_desc = $this->input->post('job_desc', TRUE);
        $start_date = $this->input->post('start_date', TRUE);
        $end_date = $this->input->post('end_date', TRUE);
        $remark = $this->input->post('remark', TRUE);
        $grade = $this->input->post('grade', TRUE);
        $other = $this->input->post('other', TRUE);
        $currently_working = $this->input->post('currently_working', TRUE);
        $cre_datetime = date('Y-m-d H:i:s');
        $mod_datetime = date('Y-m-d H:i:s');

        $add_data = array(
          'cust_id' => (int) $cust_id,
          'org_name' => $org_name,
          'job_title' => $job_title,
          'job_location' => $job_location,
          'job_desc' => $job_desc,
          'start_date' => $start_date,
          'end_date' => $end_date,
          'remark' => $remark, 
          'grade' => $grade, 
          'other' => $other,
          'currently_working' => ($currently_working == "1" ) ? 1: 0, 
          'cre_datetime' => $cre_datetime, 
          'mod_datetime' => $mod_datetime
        );
        if( $id = $this->api->add_user_experience($add_data)) {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('experience_add_success'), 'edu_id' => $id]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('experience_add_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
    public function experience_update()
    {
      if(empty($this->errors)){ 
        $exp_id = $this->input->post('exp_id', TRUE);
        $org_name = $this->input->post('org_name', TRUE);
        $job_title = $this->input->post('job_title', TRUE);
        $job_location = $this->input->post('job_location', TRUE);
        $job_desc = $this->input->post('job_desc', TRUE);
        $start_date = $this->input->post('start_date', TRUE);
        $end_date = $this->input->post('end_date', TRUE);
        $remark = $this->input->post('remark', TRUE);
        $grade = $this->input->post('grade', TRUE);
        $other = $this->input->post('other', TRUE);
        $currently_working = $this->input->post('currently_working', TRUE);
        $mod_datetime = date('Y-m-d H:i:s');

        $update_data = array(
          'exp_id' => (int) $exp_id,
          'org_name' => $org_name,
          'job_title' => $job_title,
          'job_location' => $job_location,
          'job_desc' => $job_desc,
          'start_date' => $start_date,
          'end_date' => $end_date,
          'remark' => $remark,
          'grade' => $grade,
          'other' => $other,
          'currently_working' => ($currently_working == "1" ) ? 1: 0,
          'mod_datetime' => $mod_datetime
        );

        if( $this->api->update_user_experience($update_data)){
          $this->response = ['response' => 'success', 'message' => $this->lang->line('experience_update_success')]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('experience_update_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
    public function experience_delete()
    {
      if(empty($this->errors)){ 
        $exp_id = $this->input->post('exp_id', TRUE);

        if( $this->api->delete_user_experience($exp_id)){
          $this->response = ['response' => 'success', 'message' => $this->lang->line('experience_delete_success')]; 
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('experience_delete_failed')]; 
        }
        echo json_encode($this->response);
      }
    }
  //End customer profile settings--------------------------

  //Start Service Provider Profile-------------------------
    public function get_service_provider_profile() {
      //echo json_encode($_POST); die();
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $today = date("Y-m-d H:i:s");
      $detail = array();
      if($this->api->is_user_active_or_exists($cust_id)) {
        if($profile = $this->api->get_service_provider_profile($cust_id)) {
          $detail['personal'] = $profile;
          if($provider_photos = $this->api->get_service_provider_business_photos($cust_id)) { $detail['provider_photos'] = (string)sizeof($provider_photos); }
          if($provider_documents = $this->api->get_service_provider_documents($cust_id) ) { $detail['provider_documents'] = (string)sizeof($provider_documents); }
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "profile_details" => $detail];
        } else {
          if($deliverer = $this->api->get_deliverer_profile($cust_id)) {
            echo json_encode($deliverer); die();
            $insert_data = array(
              "company_name" => trim($deliverer['company_name']),
              "firstname" => trim($deliverer['firstname']),
              "lastname" => trim($deliverer['lastname']),
              "address" => trim($deliverer['address']),
              "zipcode" => trim($deliverer['zipcode']),
              "country_id" => (int)($deliverer['country_id']),
              "state_id" => (int)($deliverer['state_id']),
              "city_id" => (int)($deliverer['city_id']),
              "email_id" => trim($deliverer['email_id']),
              "contact_no" => trim($deliverer['contact_no']),
              "avatar_url" => 'NULL',
              "cover_url" => 'NULL',
              "cust_id" => (int)$cust_id,
              "cre_datetime" => $today,
              "introduction" => trim($deliverer['introduction']),
              "shipping_mode" => 0,
              "latitude" => trim($deliverer['latitude']),
              "longitude" => trim($deliverer['longitude']),
              "gender" => trim($deliverer['gender']),
              "service_list" => 'NULL',
              "dedicated_provider" => 0,
              "profile_url_login" => base_url('user-panel-services/provider-profile-view/'.md5($cust_id)),
              "profile_url" => base_url('user-profile/'.md5($cust_id)),
              "profile_code" => md5($cust_id),
              "working_days" => 'NULL',
              "off_days" => 0,
              "max_open_order" => 5,
              "proposal_credits" => 15,
              "current_subscription_expiry" => trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'))),
              "rate_per_hour" => 'NULL',
              "currency_code" => 'NULL',
              "response_time" => 'NULL',
              "work_location" => 'NULL',
              "street_name" => 'NULL',
            );
            $this->api->register_service_provider_profile($insert_data);
            $profile = $this->api->get_service_provider_profile($cust_id);
            $detail['personal'] = $profile;
            if($provider_photos = $this->api->get_service_provider_business_photos($cust_id)) { $detail['provider_photos'] = (string)sizeof($provider_photos); }
            if($provider_documents = $this->api->get_service_provider_documents($cust_id) ) { $detail['provider_documents'] = (string)sizeof($provider_documents); }
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "profile_details" => $detail];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Profile not found.')]; }
        }
        $this->api->update_last_login($cust_id);
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
      echo json_encode($this->response);
    }
    public function register_service_provider() {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          $firstname = $this->input->post('firstname', TRUE);
          $lastname = $this->input->post('lastname', TRUE);
          $email_id = $this->input->post('email_id', TRUE);
          $contact_no = $this->input->post('contact_no', TRUE);
          $address = $this->input->post('address', TRUE);
          $country_id = $this->input->post('country_id', TRUE);
          $state_id = $this->input->post('state_id', TRUE);
          $city_id = $this->input->post('city_id', TRUE);
          $zipcode = $this->input->post('zipcode', TRUE);
          $company_name = $this->input->post('company_name', TRUE);
          $introduction = $this->input->post('introduction', TRUE);
          $latitude = $this->input->post('latitude', TRUE);
          $longitude = $this->input->post('longitude', TRUE);
          $gender = $this->input->post('gender', TRUE);
          $street_name = $this->input->post('street_name', TRUE);
          $today = date("Y-m-d H:i:s");

          if($this->api->get_service_provider_profile($cust_id)) {
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('Profile already exists.')];
          } else {
            $insert_data = array(
              "company_name" => trim($company_name),
              "firstname" => trim($firstname),
              "lastname" => trim($lastname),
              "address" => trim($address),
              "zipcode" => trim($zipcode),
              "country_id" => (int)($country_id),
              "state_id" => (int)($state_id),
              "city_id" => (int)($city_id),
              "email_id" => trim($email_id),
              "contact_no" => trim($contact_no),
              "avatar_url" => 'NULL',
              "cover_url" => 'NULL',
              "cust_id" => (int)($cust_id),
              "cre_datetime" => $today,
              "introduction" => trim($introduction),
              "shipping_mode" => 0,
              "latitude" => trim($latitude),
              "longitude" => trim($longitude),
              "gender" => trim($gender),
              "service_list" => 'NULL',
              "dedicated_provider" => 0,
              "profile_url_login" => base_url('user-panel-services/provider-profile-view/'.md5($cust_id)),
              "profile_url" => base_url('user-profile/'.md5($cust_id)),
              "profile_code" => md5($cust_id),
              "working_days" => 'NULL',
              "off_days" => 0,
              "max_open_order" => 5,
              "proposal_credits" => 15,
              "current_subscription_expiry" => trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'))),
              "rate_per_hour" => 'NULL',
              "currency_code" => 'NULL',
              "response_time" => 'NULL',
              "work_location" => 'NULL',
              "street_name" => trim($street_name),
            );
            if( $this->api->register_service_provider_profile($insert_data)) {
              // make customer as a deliverer, update flag
              $this->api->make_consumer_deliverer($cust_id);
              $profile = $this->api->get_service_provider_profile($cust_id);
              $this->response = ['response' => 'success', 'message'=> $this->lang->line('register_success'), "profile" => $profile];
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('register_failed'), "profile" => array()]; } 
          } 
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; } 
        echo json_encode($this->response);
      }
    }
    public function update_service_provider() {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          $firstname = $this->input->post('firstname', TRUE);
          $lastname = $this->input->post('lastname', TRUE);
          $email_id = $this->input->post('email_id', TRUE);
          $contact_no = $this->input->post('contact_no', TRUE);
          $address = $this->input->post('address', TRUE);
          $country_id = $this->input->post('country_id', TRUE);
          $state_id = $this->input->post('state_id', TRUE);
          $city_id = $this->input->post('city_id', TRUE);
          $zipcode = $this->input->post('zipcode', TRUE);
          $company_name = $this->input->post('company_name', TRUE);
          $introduction = $this->input->post('introduction', TRUE);
          $latitude = $this->input->post('latitude', TRUE);
          $longitude = $this->input->post('longitude', TRUE);
          $gender = $this->input->post('gender', TRUE);
          $street_name = $this->input->post('street_name', TRUE);
          $today = date("Y-m-d H:i:s");
          $update_data = array(
            "company_name" => trim($company_name),
            "firstname" => trim($firstname),
            "lastname" => trim($lastname),
            "address" => trim($address),
            "zipcode" => trim($zipcode),
            "country_id" => (int)($country_id),
            "state_id" => (int)($state_id),
            "city_id" => (int)($city_id),
            "email_id" => trim($email_id),
            "contact_no" => trim($contact_no),
            // "avatar_url" => 'NULL',
            // "cover_url" => 'NULL',
            "cust_id" => (int)($cust_id),
            "cre_datetime" => $today,
            "introduction" => trim($introduction),
            "shipping_mode" => 0,
            "latitude" => trim($latitude),
            "longitude" => trim($longitude),
            "gender" => trim($gender),
            // "service_list" => 'NULL',
            // "dedicated_provider" => 0,
            "profile_url_login" => base_url('user-panel-services/provider-profile-view/'.md5($cust_id)),
            "profile_url" => base_url('user-profile/'.md5($cust_id)),
            "profile_code" => md5($cust_id),
            // "working_days" => 'NULL',
            // "off_days" => 0,
            // "max_open_order" => 5,
            // "proposal_credits" => 15,
            // "rate_per_hour" => 'NULL',
            // "currency_code" => 'NULL',
            // "response_time" => 'NULL',
            // "work_location" => 'NULL',
            "street_name" => trim($street_name),
          );
          if($this->api->update_provider($update_data, $cust_id)){
            $profile = $this->api->get_service_provider_profile($cust_id);
            // echo json_encode($profile); die();
            $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed'), "profile" => array()]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function update_service_provider_avatar() {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE);
        if($this->api->is_user_active_or_exists($cust_id)) {
          $profile = $this->api->get_service_provider_profile($cust_id);
          if(!empty($_FILES["profile_image"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/profile-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('profile_image')) {   
              $uploads    = $this->upload->data();  
              $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
              if($profile['avatar_url'] != "NULL") {
                unlink(trim($profile['avatar_url']));
              }
            }
            $update_data = array(
              "avatar_url" => $avatar_url,
            );

            if($this->api->update_provider($update_data, $cust_id)) {
              $profile = $this->api->get_service_provider_profile($cust_id);
              $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function update_service_provider_cover() {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE);
        if($this->api->is_user_active_or_exists($cust_id)) {
          $profile = $this->api->get_service_provider_profile($cust_id);
          if( !empty($_FILES["cover_image"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/cover-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('cover_image')) {   
              $uploads    = $this->upload->data();  
              $cover_url =  $config['upload_path'].$uploads["file_name"];  
              if($profile['cover_url'] != "NULL") {
                unlink(trim($profile['cover_url']));
              }
            }
            $update_data = array(
              "cover_url" => $cover_url,
            );
            if( $this->api->update_provider($update_data, $cust_id)) {
              $profile = $this->api->get_service_provider_profile($cust_id);
              $this->response = ['response' => 'success','message'=> $this->lang->line('update_success'), "profile" => $profile];
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function update_working_days() {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $working_days = $this->input->post('working_days', TRUE);
        $update_data = array(
          "working_days" => trim($working_days),
        );
        if($this->api->update_provider($update_data, $cust_id)) {
          $profile = $this->api->get_service_provider_profile($cust_id);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success'), "profile" => $profile];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function get_service_provider_off_days() {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        if($off_days = $this->api->get_service_provider_off_days($cust_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "off_days" => $off_days];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "off_days" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function update_off_days() {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $from_date = $this->input->post('from_date', TRUE);
        $to_date = $this->input->post('to_date', TRUE);
        $today = date('Y-m-d h:i:s');
        
        $add_data = array(
          "from_date" => trim($from_date),
          "to_date" => trim($to_date),
          "cre_datetime" => trim($today),
          "update_datetime" => trim($today),
          "cust_id" => trim($cust_id),
        );
        //json_encode($cover_update); die();
        if($this->api->register_off_day($add_data)){
          $update_data = array(
            "off_days" => 1,
          );
          $this->api->update_provider($update_data, $cust_id);
          $profile = $this->api->get_service_provider_profile($cust_id);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success'), "profile" => $profile];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
    
    public function delete_off_days() {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $off_id = $this->input->post('off_id', TRUE); $off_id = (int)$off_id;
        
        if($this->api->delete_off_day($off_id)){
          $off_days = $this->api->get_service_provider_off_days($cust_id);
          if(sizeof($off_days) <= 0) {
            $update_data = array(
              "off_days" => 0,
            );
            $this->api->update_provider($update_data, $cust_id);
          }
          $profile = $this->api->get_service_provider_profile($cust_id);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('delete_success'), "profile" => $profile];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function update_max_open_order() {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $max_open_order = $this->input->post('max_open_order', TRUE);
        $update_data = array(
          "max_open_order" => trim($max_open_order),
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $profile = $this->api->get_service_provider_profile($cust_id);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success'), "profile" => $profile];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function get_all_currencies_master() {
      if($currencies = $this->api->get_all_currencies()) {
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "currencies" => $currencies];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "currencies" => array()]; }
      echo json_encode($this->response);
    }
    public function update_rate_hour() {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $rate_per_hour = $this->input->post('rate_per_hour', TRUE);
        $currency_code = $this->input->post('currency_code', TRUE);
        $update_data = array(
          "rate_per_hour" => trim($rate_per_hour),
          "currency_code" => trim($currency_code),
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $profile = $this->api->get_service_provider_profile($cust_id);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success'), "profile" => $profile];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function update_work_location() {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $work_location = $this->input->post('work_location', TRUE);
        $update_data = array(
          "work_location" => trim($work_location),
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $profile = $this->api->get_service_provider_profile($cust_id);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success'), "profile" => $profile];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function update_social_media() {
      if(empty($this->errors)){ 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $linkedin_url = $this->input->post('linkedin_url', TRUE);
        $twitter_url = $this->input->post('twitter_url', TRUE);
        $fb_url = $this->input->post('fb_url', TRUE);
        $update_data = array(
          "linkedin_url" => trim($linkedin_url),
          "twitter_url" => trim($twitter_url),
          "fb_url" => trim($fb_url),
        );
        //echo json_encode($update_data); die();
        if($this->api->update_provider($update_data, $cust_id)){
          $profile = $this->api->get_service_provider_profile($cust_id);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('update_success'), "profile" => $profile];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function get_profile_subscription_master() {
      if(empty($this->errors)) { 
        $country_id = $this->input->post('country_id', TRUE); $country_id = (int)$country_id;
        if($subscriptions = $this->api->get_subscription_master($country_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "subscriptions" => $subscriptions];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "subscriptions" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function proposal_credit_payment_stripe() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $subscription_id = $this->input->post('subscription_id', TRUE); $subscription_id = (int)$subscription_id;
        $payment_method = $this->input->post('payment_method', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $transaction_id = $this->input->post('stripeToken', TRUE);
        $stripeEmail = $this->input->post('stripeEmail', TRUE);
        $currency = $this->input->post('currency', TRUE);
        $amount_paid = $this->input->post('amount_paid', TRUE);
        $today = date('Y-m-d H:i:s');
        $sub_details = $this->api->get_subscription_master_details((int)$subscription_id);
        $user_details = $this->api->get_service_provider_profile(trim($cust_id));

        if( strtotime($user_details['current_subscription_expiry']) < strtotime(date('Y-m-d')) ) {
          $expiry = trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days')));
          $credit = $sub_details['proposal_credit'];
        } else { 
          $expiry = trim(date('Y-m-d', strtotime($user_details['current_subscription_expiry']. ' + 30 days'))); 
          $credit = (($user_details['proposal_credits']) + ($sub_details['proposal_credit']));
        }

        $update_data = array(
          "proposal_credits" => $credit,
          "current_subscription_type" => trim($sub_details['subscription_title']),
          "current_subscription_expiry" => $expiry,
        );
        //json_encode($cover_update); die();
        if($this->api->update_provider($update_data, $cust_id)) {

          //Add Payment to Provider account-----------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            }

            $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($amount_paid),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($currency),
              "cat_id" => 0
            );
            $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
            //smp add Payment to Provider account-----------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
              $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
            //smp add Payment to Provider account-----------------------------------
          //------------------------------------------------------------------------

          //deduct Payment from customer account------------------------------------
            if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
            } else {
              $insert_data_provider_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            }
            $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
            $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
            $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
            $update_data_account_history = array(
              "account_id" => (int)$provider_account_master_details['account_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($amount_paid),
              "account_balance" => trim($provider_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($currency),
              "cat_id" => 0
            );
            $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
            //Smp deduct Payment from customer account------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              }
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
              $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
            //Smp deduct Payment from customer account------------------------------
          //------------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account--------------------------
            if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency))) {
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($currency),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($amount_paid);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$subscription_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'profile_subscription',
              "amount" => trim($amount_paid),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($currency),
              "country_id" => trim($user_details['country_id']),
              "cat_id" => 0,
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //------------------------------------------------------------------------

          if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
            $parts = explode("@", trim($user_details['email_id']));
            $username = $parts[0];
            $provider_name = $username;
          } else { $provider_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }

          /* Stripe payment invoice---------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($currency),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($subscription_id);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($user_details['country_id']));
            $operator_state = $this->api->get_state_details(trim($user_details['state_id']));
            $operator_city = $this->api->get_city_details(trim($user_details['city_id']));
            $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($user_details['email_id'])));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($user_details['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
            $invoice->setTo(array($gonagoo_address['company_name'],trim($gonagoo_city['city_name']),'-',trim($gonagoo_country['country_name']),trim($gonagoo_address['email'])));

            /* Adding Items in table */
            $order_for = $this->lang->line('Profile subscription'). ' - ' .$this->lang->line('Payment');
            $rate = round(trim($amount_paid),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'), $total);
            $invoice->addTotal($this->lang->line('taxes'), '0');
            $invoice->addTotal($this->lang->line('invoice_total'), $total, true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Subscription Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($subscription_id.'_profile_subscription.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $subscription_id.'_profile_subscription.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/subscription-invoices/".$pdf_name);
            //Update Order Invoice
            $update_data = array(
              "subscription_invoice_url" => "subscription-invoices/".$pdf_name,
            );
            $this->api->update_provider($update_data, $cust_id);
          /* -------------------------------------------------------------------- */

          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your profile subscription payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Profile Subscription Payment Invoice');
            $emailAddress = trim($user_details['email_id']);
            $fileToAttach = "resources/subscription-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          //update to subscription purchase history
          $insert_data_purchase_history = array(
            "subscription_id" => $subscription_id,
            "cust_id" => $cust_id,
            "purchase_date" => $today,
            "price" => $amount_paid,
            "currency_id" => $sub_details['currency_id'],
            "currency_code" => $sub_details['currency_code'],
            "country_id" => $sub_details['country_id'],
            "ah_id" => $ah_id,
          );
          $this->api->insert_freelancer_sub_purchase_history($insert_data_purchase_history);

          //uodate to proposal credit history
          $insert_data_proposal_credit_history = array(
            "cust_id" => $cust_id,
            "type" => 'add',
            "quantity" => $sub_details['proposal_credit'],
            "balance" => ($sub_details['proposal_credit']) + ($user_details['proposal_credits']),
            "source_id" => $subscription_id,
            "source_type" => 'proposal',
          );
          $this->api->insert_freelancer_proposal_credit_history($insert_data_proposal_credit_history);

          //SMS to customer
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Payment completed for profile subscription. Subscription ID').': '.$subscription_id;
          $this->api_sms->sendSMS((int)$country_code.trim($user_details['contact_no']), $message);

          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Profile subscription payment'), 'type' => 'subscription-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Profile subscription payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          $profile = $this->api->get_service_provider_profile($cust_id);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('Profile subscription completed successfully.'), "profile" => $profile];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Error! unable to complete profile subscription.')]; }
        echo json_encode($this->response);
      }
    }
    public function proposal_credit_payment_mtn() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $subscription_id = $this->input->post('subscription_id', TRUE); $subscription_id = (int)$subscription_id;
        $phone_no = $this->input->post('phone_no', TRUE);
        if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
        $payment_method = $this->input->post('payment_method', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $currency = $this->input->post('currency', TRUE);
        $amount_paid = $this->input->post('amount_paid', TRUE);
        $today = date('Y-m-d H:i:s');
        $sub_details = $this->api->get_subscription_master_details((int)$subscription_id);
        $user_details = $this->api->get_service_provider_profile(trim($cust_id));
        
        $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$amount_paid."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        //$contents = curl_exec($ch);
        //$mtn_pay_res = json_decode($contents, TRUE);
        //var_dump($mtn_pay_res); die();
        
        $transaction_id = '1234567890';
        //$transaction_id = trim($mtn_pay_res['TransactionID']);
        
        $payment_data = array();
        // if($mtn_pay_res['StatusCode'] === "01"){
        if(1) {
          if( strtotime($user_details['current_subscription_expiry']) < strtotime(date('Y-m-d')) ) {
            $expiry = trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days')));
            $credit = $sub_details['proposal_credit'];
          } else { 
            $expiry = trim(date('Y-m-d', strtotime($user_details['current_subscription_expiry']. ' + 30 days'))); 
            $credit = (($user_details['proposal_credits']) + ($sub_details['proposal_credit']));
          }

          $update_data = array(
            "proposal_credits" => $credit,
            "current_subscription_type" => trim($sub_details['subscription_title']),
            "current_subscription_expiry" => $expiry,
          );
          //json_encode($update_data); die();
          if($this->api->update_provider($update_data, $cust_id)) {

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
              //Smp add Payment to Provider account-----------------------------------
                if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
                } else {
                  $insert_data_provider_master = array(
                    "user_id" => $cust_id,
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($currency),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                  $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
                }

                $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
                $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
                $update_data_account_history = array(
                  "account_id" => (int)$provider_account_master_details['account_id'],
                  "order_id" => (int)$subscription_id,
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($amount_paid),
                  "account_balance" => trim($provider_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($currency),
                  "cat_id" => 0
                );
                $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
              //Smp add Payment to Provider account-----------------------------------
            //------------------------------------------------------------------------

            //deduct Payment from customer account------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
              }
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
              //Smp deduct Payment from customer account------------------------------
                if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
                } else {
                  $insert_data_provider_master = array(
                    "user_id" => $cust_id,
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($currency),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                  $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
                }
                $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
                $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
                $update_data_account_history = array(
                  "account_id" => (int)$provider_account_master_details['account_id'],
                  "order_id" => (int)$subscription_id,
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'payment',
                  "amount" => trim($amount_paid),
                  "account_balance" => trim($provider_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($currency),
                  "cat_id" => 0
                );
                $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
              //Smp deduct Payment from customer account------------------------------
            //------------------------------------------------------------------------


            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($amount_paid);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'profile_subscription',
                "amount" => trim($amount_paid),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($currency),
                "country_id" => trim($user_details['country_id']),
                "cat_id" => 0,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------

            if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
              $parts = explode("@", trim($user_details['email_id']));
              $username = $parts[0];
              $provider_name = $username;
            } else { $provider_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }

            /* Stripe payment invoice---------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($currency),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($subscription_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($user_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($user_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($user_details['city_id']));
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($user_details['email_id'])));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($user_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
              $invoice->setTo(array($gonagoo_address['company_name'],trim($gonagoo_city['city_name']),'-',trim($gonagoo_country['country_name']),trim($gonagoo_address['email'])));

              /* Adding Items in table */
              $order_for = $this->lang->line('Profile subscription'). ' - ' .$this->lang->line('Payment');
              $rate = round(trim($amount_paid),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'), $total);
              $invoice->addTotal($this->lang->line('taxes'), '0');
              $invoice->addTotal($this->lang->line('invoice_total'), $total, true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Subscription Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($subscription_id.'_profile_subscription.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $subscription_id.'_profile_subscription.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/subscription-invoices/".$pdf_name);
              //Update Order Invoice
              $update_data = array(
                "subscription_invoice_url" => "subscription-invoices/".$pdf_name,
              );
              $this->api->update_provider($update_data, $cust_id);
            /* -------------------------------------------------------------------- */

            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your profile subscription payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Profile Subscription Payment Invoice');
              $emailAddress = trim($user_details['email_id']);
              $fileToAttach = "resources/subscription-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */

            //update to subscription purchase history
            $insert_data_purchase_history = array(
              "subscription_id" => $subscription_id,
              "cust_id" => $cust_id,
              "purchase_date" => $today,
              "price" => $amount_paid,
              "currency_id" => $sub_details['currency_id'],
              "currency_code" => $sub_details['currency_code'],
              "country_id" => $sub_details['country_id'],
              "ah_id" => $ah_id,
            );
            $this->api->insert_freelancer_sub_purchase_history($insert_data_purchase_history);

            //uodate to proposal credit history
            $insert_data_proposal_credit_history = array(
              "cust_id" => $cust_id,
              "type" => 'add',
              "quantity" => $sub_details['proposal_credit'],
              "balance" => ($sub_details['proposal_credit']) + ($user_details['proposal_credits']),
              "source_id" => $subscription_id,
              "source_type" => 'proposal',
            );
            $this->api->insert_freelancer_proposal_credit_history($insert_data_proposal_credit_history);

            //SMS to customer
            $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
            $message = $this->lang->line('Payment completed for profile subscription. Subscription ID').': '.$subscription_id;
            $this->api_sms->sendSMS((int)$country_code.trim($user_details['contact_no']), $message);

            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Profile subscription payment'), 'type' => 'subscription-payment', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Profile subscription payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            $profile = $this->api->get_service_provider_profile($cust_id);
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('Profile subscription completed successfully.'), "profile" => $profile];
          } else { $this->session->set_flashdata('error', $this->lang->line('Error! unable to complete profile subscription.')); }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Error! unable to complete profile subscription.')]; }
        echo json_encode($this->response);
      }
    }
    public function proposal_credit_payment_orange() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) {
        $subscription_id = $this->input->post('subscription_id', TRUE); $subscription_id = (int)$subscription_id;
        $subscription_payment_id = $this->input->post('subscription_payment_id', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $pay_token = $this->input->post('pay_token', TRUE);
        $amount_paid = $this->input->post('amount_paid', TRUE);
        $payment_method = $this->input->post('payment_method', TRUE);
        $currency = $this->input->post('currency_code', TRUE);
        $today = date('Y-m-d h:i:s');
        $sub_details = $this->api->get_subscription_master_details((int)$subscription_id);
        $user_details = $this->api->get_service_provider_profile(trim($cust_id));
        
        //get access token
        $ch = curl_init("https://api.orange.com/oauth/v2/token?");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
        $orange_token = curl_exec($ch);
        curl_close($ch);
        $token_details = json_decode($orange_token, TRUE);
        $token = 'Bearer '.$token_details['access_token'];

        // get payment link
        $json_post_data = json_encode(
                            array(
                              "booking_id" => $subscription_payment_id, 
                              "amount" => $amount_paid, 
                              "pay_token" => $pay_token
                            )
                          );
        //echo $json_post_data; die();
        $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: ".$token,
            "Accept: application/json"
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
        $payment_res = curl_exec($ch);
        curl_close($ch);
        $payment_details = json_decode($payment_res, TRUE);
        //$transaction_id = $payment_details['txnid'];
        $transaction_id = '1234567890';
        //echo json_encode($payment_details); die();

        // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
        if(1) {
          if( strtotime($user_details['current_subscription_expiry']) < strtotime(date('Y-m-d')) ) {
            $expiry = trim(date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days')));
            $credit = $sub_details['proposal_credit'];
          } else { 
            $expiry = trim(date('Y-m-d', strtotime($user_details['current_subscription_expiry']. ' + 30 days'))); 
            $credit = (($user_details['proposal_credits']) + ($sub_details['proposal_credit']));
          }
          $update_data = array(
            "proposal_credits" => $credit,
            "current_subscription_type" => trim($sub_details['subscription_title']),
            "current_subscription_expiry" => $expiry,
          );
          //json_encode($update_data); die();
          if($this->api->update_provider($update_data, $cust_id)) {

            //Add Payment to Provider account-----------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
              }

              $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
              //Smp add Payment to Provider account-----------------------------------
                if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
                } else {
                  $insert_data_provider_master = array(
                    "user_id" => $cust_id,
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($currency),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                  $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
                }

                $account_balance = trim($provider_account_master_details['account_balance']) + trim($amount_paid);
                $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
                $update_data_account_history = array(
                  "account_id" => (int)$provider_account_master_details['account_id'],
                  "order_id" => (int)$subscription_id,
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($amount_paid),
                  "account_balance" => trim($provider_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($currency),
                  "cat_id" => 0
                );
                $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
              //Smp add Payment to Provider account-----------------------------------
            //------------------------------------------------------------------------

            //deduct Payment from customer account------------------------------------
              if($provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency))) {
              } else {
                $insert_data_provider_master = array(
                  "user_id" => $cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_provider_master);
                $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
              }
              $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
              $this->api->update_payment_in_customer_master($provider_account_master_details['account_id'], $cust_id, $account_balance);
              $provider_account_master_details = $this->api->customer_account_master_details($cust_id, trim($currency));
              $update_data_account_history = array(
                "account_id" => (int)$provider_account_master_details['account_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($amount_paid),
                "account_balance" => trim($provider_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($currency),
                "cat_id" => 0
              );
              $ah_id = $this->api->insert_payment_in_account_history($update_data_account_history);
              //Smp deduct Payment from customer account------------------------------
                if($provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency))) {
                } else {
                  $insert_data_provider_master = array(
                    "user_id" => $cust_id,
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($currency),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($insert_data_provider_master);
                  $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
                }
                $account_balance = trim($provider_account_master_details['account_balance']) - trim($amount_paid);
                $this->api->update_payment_in_customer_master_smp($provider_account_master_details['account_id'], $cust_id, $account_balance);
                $provider_account_master_details = $this->api->customer_account_master_details_smp($cust_id, trim($currency));
                $update_data_account_history = array(
                  "account_id" => (int)$provider_account_master_details['account_id'],
                  "order_id" => (int)$subscription_id,
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'payment',
                  "amount" => trim($amount_paid),
                  "account_balance" => trim($provider_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($currency),
                  "cat_id" => 0
                );
                $ah_id = $this->api->insert_payment_in_account_history_smp($update_data_account_history);
              //Smp deduct Payment from customer account------------------------------
            //------------------------------------------------------------------------


            //Add Ownward Trip Commission To Gonagoo Account--------------------------
              if($gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency))) {
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($currency),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($amount_paid);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($currency));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$subscription_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'profile_subscription',
                "amount" => trim($amount_paid),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($currency),
                "country_id" => trim($user_details['country_id']),
                "cat_id" => 0,
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //------------------------------------------------------------------------

            if(trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL')) {
              $parts = explode("@", trim($user_details['email_id']));
              $username = $parts[0];
              $provider_name = $username;
            } else { $provider_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']); }

            /* Stripe payment invoice---------------------------------------------- */
              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($currency),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($subscription_id);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($user_details['country_id']));
              $operator_state = $this->api->get_state_details(trim($user_details['state_id']));
              $operator_city = $this->api->get_city_details(trim($user_details['city_id']));
              $invoice->setFrom(array($provider_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($user_details['email_id'])));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($user_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));
              $invoice->setTo(array($gonagoo_address['company_name'],trim($gonagoo_city['city_name']),'-',trim($gonagoo_country['country_name']),trim($gonagoo_address['email'])));

              /* Adding Items in table */
              $order_for = $this->lang->line('Profile subscription'). ' - ' .$this->lang->line('Payment');
              $rate = round(trim($amount_paid),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'), $total);
              $invoice->addTotal($this->lang->line('taxes'), '0');
              $invoice->addTotal($this->lang->line('invoice_total'), $total, true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Subscription Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph* /
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($subscription_id.'_profile_subscription.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $subscription_id.'_profile_subscription.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/subscription-invoices/".$pdf_name);
              //Update Order Invoice
              $update_data = array(
                "subscription_invoice_url" => "subscription-invoices/".$pdf_name,
              );
              $this->api->update_provider($update_data, $cust_id);
            /* -------------------------------------------------------------------- */

            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your profile subscription payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Profile Subscription Payment Invoice');
              $emailAddress = trim($user_details['email_id']);
              $fileToAttach = "resources/subscription-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */

            //update to subscription purchase history
            $insert_data_purchase_history = array(
              "subscription_id" => $subscription_id,
              "cust_id" => $cust_id,
              "purchase_date" => $today,
              "price" => $amount_paid,
              "currency_id" => $sub_details['currency_id'],
              "currency_code" => $sub_details['currency_code'],
              "country_id" => $sub_details['country_id'],
              "ah_id" => $ah_id,
            );
            $this->api->insert_freelancer_sub_purchase_history($insert_data_purchase_history);

            //uodate to proposal credit history
            $insert_data_proposal_credit_history = array(
              "cust_id" => $cust_id,
              "type" => 'add',
              "quantity" => $sub_details['proposal_credit'],
              "balance" => ($sub_details['proposal_credit']) + ($user_details['proposal_credits']),
              "source_id" => $subscription_id,
              "source_type" => 'proposal',
            );
            $this->api->insert_freelancer_proposal_credit_history($insert_data_proposal_credit_history);

            //SMS to customer
            $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
            $message = $this->lang->line('Payment completed for profile subscription. Subscription ID').': '.$subscription_id;
            $this->api_sms->sendSMS((int)$country_code.trim($user_details['contact_no']), $message);

            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Profile subscription payment'), 'type' => 'subscription-payment', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Profile subscription payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            $profile = $this->api->get_service_provider_profile($cust_id);
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('Profile subscription completed successfully.'), "profile" => $profile];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Error! unable to complete profile subscription.')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Error! unable to complete profile subscription.')]; }
        echo json_encode($this->response);
      }
    }
  //End Service Provider Profile---------------------------

  //Start Service Provider Documents-----------------------
    public function get_service_provider_document() {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        if($this->api->is_user_active_or_exists($cust_id)){
          if( $docs = $this->api->get_service_provider_documents($cust_id) ) {
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "user_documents" => $docs];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "user_documents" => array()]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function add_service_provider_document() {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          $doc_type = $this->input->post('doc_type', TRUE);
          $provider_id = (int) $this->api->get_provider_id($cust_id);
          if( !empty($_FILES["attachment"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/attachements/';
            $config['allowed_types']  = '*';
            $config['max_size']       = '0';
            $config['max_width']      = '0';
            $config['max_height']     = '0';
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('attachment')) {   
              $uploads    = $this->upload->data();  
              $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
            } else { $attachement_url = "NULL"; }

            $register_data = array(
              "attachement_url" => trim($attachement_url), 
              "cust_id" => $cust_id, 
              "provider_id" => $provider_id, 
              "is_verified" => "0", 
              "doc_type" => $doc_type, 
              "is_rejected" => "0",
              "upload_datetime" => date('Y-m-d H:i:s'),
              "verify_datetime" => "NULL",
            );
            if( $this->api->register_operator_document($register_data) ) {
              $docs = $this->api->get_service_provider_documents($cust_id);
              $this->response = ['response' => 'success', 'message'=> $this->lang->line('add_success')];
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function delete_service_provider_document() {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          $doc_id = $this->input->post('doc_id', TRUE);  $doc_id = (int) $doc_id;
          $documents = $this->api->get_old_operator_documents($doc_id);
          if( $this->api->delete_operator_document($doc_id) ) {
            unlink(trim($documents[0]['attachement_url']));
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('remove_success')];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove_failed')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }  
    }
  //End Service Provider Documents-------------------------

  //Start Service Provider Business Photos-----------------
    public function get_service_provider_business_photos() {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          if($photos = $this->api->get_service_provider_business_photos($cust_id)){
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "user_business_photos" => $photos];
          } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "user_business_photos" => array()]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function upload_service_provider_business_photo() 
    {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          if(!empty($_FILES["business_photo"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/business-photos/';
            $config['allowed_types']  = '*';
            $config['max_size']       = '0';
            $config['max_width']      = '0';
            $config['max_height']     = '0';
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('business_photo')) { 
              $uploads    = $this->upload->data();  
              $photo_url =  $config['upload_path'].$uploads["file_name"];  
            }
            if($photo_id = $this->api->add_service_provider_business_photo($photo_url, $cust_id)) {
              $this->response=['response'=>'success', 'message'=> $this->lang->line('add_success'), "photo_id" => $photo_id, "photo_url" => trim($photo_url)];
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('image_not_found')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
    public function delete_service_provider_business_photo() {
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        if($this->api->is_user_active_or_exists($cust_id)) {
          $photo_id = $this->input->post('photo_id', TRUE); $photo_id = (int)$photo_id;
          if($this->api->delete_service_provider_business_photo($photo_id)) {
            $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive'), "user_status" => "0"]; }
        echo json_encode($this->response);
      }
    }
  //End Service Provider Business Photos-------------------

  //Start Service Provider Manage Offers-------------------
    public function provider_offers_list() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int)$last_id;
        $country_id = $this->input->post('country_id', TRUE); $country_id = (int)$country_id;
        $state_id = $this->input->post('state_id', TRUE); $state_id = (int)$state_id;
        $city_id = $this->input->post('city_id', TRUE); $city_id = (int)$city_id;
        $sort_by = $this->input->post('sort_by', TRUE);
        $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
        $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
        $delivered_in = $this->input->post('delivered_in', TRUE); $delivered_in = (int)$delivered_in;
        $start_price = $this->input->post('start_price', TRUE); $start_price = (int)$start_price;
        $end_price = $this->input->post('end_price', TRUE); $end_price = (int)$end_price;
        $latest = $this->input->post('latest', TRUE);
        $active_status = $this->input->post('active_status', TRUE);
        $running_status = $this->input->post('running_status', TRUE);

        $search_data = array(
          "cust_id" => (int)trim($this->cust['cust_id']),
          "country_id" => trim($country_id),
          "state_id" => trim($state_id),
          "city_id" => trim($city_id),
          "sort_by" => trim($sort_by),
          "cat_type_id" => trim($cat_type_id),
          "cat_id" => trim($cat_id),
          "delivered_in" => trim($delivered_in),
          "start_price" => trim($start_price),
          "end_price" => trim($end_price),
          "latest" => trim($latest),
          "active_status" => trim($active_status),
          "running_status" => trim($running_status),
          "device" => 'mobile',
          "last_id" => $last_id,
          "offer_pause_resume" => 'all',
        );
        
        if( $list = $this->api->get_service_provider_offers_filtered($search_data)) {
          for ($i=0; $i < sizeof($list); $i++) {
            $add_ons = $this->api->get_offer_add_ons($list[$i]['offer_id']);
            if($add_ons) $list[$i] += ['add_ons' => $add_ons];
            else $list[$i] += ['add_ons' => array()];
          }
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $list];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function check_offer_tags() {
      if(empty($this->errors)) {
        $tag_name = $this->input->post('tag_name', TRUE);
        if( $tags = $this->api->get_offer_tags_masters($tag_name) ) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "tag_name" => $tags];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "tag_name" => "NULL"]; }
        echo json_encode($this->response);
      }
    }
    public function get_category_type_with_category() {
      if( $category_types = $this->api->get_category_types() ) {
        for ($i=0; $i < sizeof($category_types); $i++) {
          $categories = $this->api->get_category_by_id($category_types[$i]['cat_type_id']);
          if($categories) $category_types[$i] += ['categories' => $categories];
          else $category_types[$i] += ['categories' => array()];
        }
        $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "category_types" => $category_types];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "category_types" => array()]; }
      echo json_encode($this->response);
    }
    public function provider_add_offer_details() {
      //echo json_encode($_POST); die();
      //echo json_encode($_FILES); //die();
      $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
      $cust_country_id = $this->input->post('cust_country_id'); $cust_country_id = (int)$cust_country_id;
      $offer_title = $this->input->post('offer_title');
      $offer_price = $this->input->post('offer_price');
      $delivered_in = $this->input->post('delivered_in'); $delivered_in = (int)$delivered_in;
      $cat_type_id = $this->input->post('cat_type_id'); $cat_type_id = (int)$cat_type_id;
      $cat_id = $this->input->post('cat_id'); $cat_id = (int)$cat_id;
      $tags = $this->input->post('tags');
      $offer_desc = $this->input->post('off_desc');
      $offer_req = $this->input->post('offer_req');
      $location_type = $this->input->post('location_type'); $location_type = (int)$location_type;
      $country_id = $this->input->post('country_id'); $country_id = (int)$country_id;
      if($country_id == 0) $country_id = $cust_country_id;
      $state_id = $this->input->post('state_id'); $state_id = (int)$state_id;
      $city_id = $this->input->post('city_id'); $city_id = (int)$city_id;
      $add_on_status = $this->input->post('add_on_status'); $add_on_status = (int)$add_on_status;
      $no_of_add_ons = $this->input->post('no_of_add_ons'); $no_of_add_ons = (int)$no_of_add_ons;
      $quick_delivery = $this->input->post('quick_delivery'); $quick_delivery = (int)$quick_delivery;
      $quick_delivery_price = $this->input->post('quick_delivery_price');
      $quick_delivery_day = $this->input->post('quick_delivery_day'); $quick_delivery_day = (int)$quick_delivery_day;
      $addon_title = $this->input->post('addon_title');
      $addon_price = $this->input->post('addon_price');
      $work_day = $this->input->post('work_day');
      $today = date('Y-m-d H:i:s');
      $currency_code = $this->api->get_country_currency_detail((int)$cust_country_id)['currency_sign'];
      $currency_id = $this->api->get_country_currency_detail((int)$cust_country_id)['currency_id'];
      
      if(!empty($_FILES["default_image_url"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('default_image_url')) { 
          $uploads    = $this->upload->data();  
          $default_image_url =  $config['upload_path'].$uploads["file_name"];  
        } else { $default_image_url = 'NULL'; }
      } else { $default_image_url = 'NULL'; }
      if(!empty($_FILES["image_url_1"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_1')) { 
          $uploads    = $this->upload->data();  
          $image_url_1 =  $config['upload_path'].$uploads["file_name"];  
        } else { $image_url_1 = 'NULL'; }
      } else { $image_url_1 = 'NULL'; }
      if(!empty($_FILES["image_url_2"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_2')) { 
          $uploads    = $this->upload->data();  
          $image_url_2 =  $config['upload_path'].$uploads["file_name"];  
        } else { $image_url_2 = 'NULL'; }
      } else { $image_url_2 = 'NULL'; }
      if(!empty($_FILES["image_url_3"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_3')) { 
          $uploads    = $this->upload->data();  
          $image_url_3 =  $config['upload_path'].$uploads["file_name"];  
        } else { $image_url_3 = 'NULL'; }
      } else { $image_url_3 = 'NULL'; }
      if(!empty($_FILES["image_url_4"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_4')) { 
          $uploads    = $this->upload->data();  
          $image_url_4 =  $config['upload_path'].$uploads["file_name"];  
        } else { $image_url_4 = 'NULL'; }
      } else { $image_url_4 = 'NULL'; }
      if(!empty($_FILES["image_url_5"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_5')) { 
          $uploads    = $this->upload->data();  
          $image_url_5 =  $config['upload_path'].$uploads["file_name"];  
        } else { $image_url_5 = 'NULL'; }
      } else { $image_url_5 = 'NULL'; }

      $insert_data = array(
        "cust_id" => $cust_id,
        "cust_country_id" => (int)$cust_country_id,
        "offer_title" => trim($offer_title),
        "offer_desc" => trim($offer_desc),
        "offer_req" => trim($offer_req),
        "delivered_in" => $delivered_in,
        "default_image_url" => $default_image_url,
        "image_url_1" => $image_url_1,
        "image_url_2" => $image_url_2,
        "image_url_3" => $image_url_3,
        "image_url_4" => $image_url_4,
        "image_url_5" => $image_url_5,
        "add_on_status" => $add_on_status,
        "quick_delivery" => $quick_delivery,
        "quick_delivery_day" => $quick_delivery_day,
        "quick_delivery_price" => $quick_delivery_price,
        "view_count" => 0,
        "sales_count" => 0,
        "favorite_counts" => 0,
        "offer_price" => trim($offer_price),
        "currency_id" => (int)$currency_id,
        "currency_code" => trim($currency_code),
        "cat_type_id" => (int)$cat_type_id,
        "cat_id" => (int)$cat_id,
        "rating_count" => 0,
        "review_count" => 0,
        "tags" => $tags,
        "location_type" => $location_type,
        "onsite_address" => 'NULL',
        "country_id" => $country_id,
        "state_id" => $state_id,
        "city_id" => $city_id,
        "cre_datetime" => $today,
        "update_datetime" => $today,
        "offer_url" => 'NULL',
        "offer_status" => 1,
        "admin_approval" => 0,
        "offer_pause_resume" => 1,
        "offer_code" => 'NULL',
      );
      //echo json_encode($insert_data); die();

      if($offer_id = $this->api->register_provider_offer_details($insert_data)) {
        //check for tags and insert new one
        $tags = explode(',', $tags);
        for ($i=0; $i < sizeof($tags); $i++) { 
          if(!$this->api->check_tag_exists(strtolower($tags[$i]))) {
            $insert_data_tag = array(
              "tag_name" => strtolower($tags[$i]),
              "status" => 1,
              "cre_datetime" => $today,
            );
            $this->api->register_offer_tag($insert_data_tag);
          }
        }

        //Update offer URL
        $update_data = array(
          "offer_url" => base_url('user-panel-services/provider-offer-details/'.md5($offer_id)),
          "offer_code" => md5($offer_id),
        );
        $this->api->update_offer_details($offer_id, $update_data);

        //insert Offer Add-ons
        if($add_on_status == 1) {
          $addon_title = explode('~', $addon_title);
          $addon_price = explode('~', $addon_price);
          $work_day = explode('~', $work_day);

          for ($i=0; $i < $no_of_add_ons; $i++) {
            $insert_add_ons = array(
              "offer_id" => $offer_id,
              "cust_id" => $cust_id,
              "cre_datetime" => $today,
              "addon_title" => $addon_title[$i],
              "addon_price" => $addon_price[$i],
              "country_id" => (int)$country_id,
              "currency_id" => (int)$currency_id,
              "currency_code" => trim($currency_code),
              "work_day" => $work_day[$i],
              "status" => 1,
              "update_datetime" => $today,
            );
            $addon_id = $this->api->register_provider_offer_add_on_details($insert_add_ons);
          }
        }
        $this->response=['response'=>'success', 'message'=> $this->lang->line('add_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')];  }
      echo json_encode($this->response);
    }
    public function provider_pause_resume_offer() {
      if(empty($this->errors)) { 
        $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
        $status = $this->input->post('status'); $status = (int) $status;
        $update_data = array(
          "offer_pause_resume" => $status,
          "update_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_offer_details($offer_id, $update_data) ) { $this->response=['response'=>'success', 'message'=> $this->lang->line('update_success')]; }
        else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function provider_delete_offer() {
      if(empty($this->errors)) { 
        $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
        $update_data = array(
          "offer_status" => 0,
          "update_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_offer_details($offer_id, $update_data) ) { $this->response=['response'=>'success', 'message'=> $this->lang->line('delete_success')]; }
        else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function provider_offer_details() {
      if(empty($this->errors)) { 
        $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
        if($offer_details = $this->api->get_service_provider_offer_details((int)$offer_id)) {

          $user_profile = $this->api->get_user_details((int)$offer_details['cust_id']);
          if($user_profile) $offer_details += ['user_profile' => $user_profile];
          else $offer_details += ['user_profile' => array()];

          $provider_profile = $this->api->get_service_provider_profile((int)$offer_details['cust_id']);
          if($provider_profile) $offer_details += ['provider_profile' => $provider_profile];
          else $offer_details += ['provider_profile' => array()];
          
          $offer_addons = $this->api->get_offer_add_ons((int)$offer_id);
          if($offer_addons) $offer_details += ['offer_addons' => $offer_addons];
          else $offer_details += ['offer_addons' => array()];

          $offer_reviews = $this->api->get_offer_reviews((int)$offer_id);
          if($offer_reviews) $offer_details += ['offer_reviews' => $offer_reviews];
          else $offer_details += ['offer_reviews' => array()];

          $this->response = ['response' => 'success', 'message'=> $this->lang->line('Details found!'), "offer_details" => $offer_details];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('details not found'), "offer_details" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function provider_edit_offer_details() {
      //echo json_encode($_POST); die();
      //echo json_encode($_FILES); //die();
      $offer_id = $this->input->post('offer_id'); $offer_id = (int)$offer_id;
      $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
      $cust_country_id = $this->input->post('cust_country_id'); $cust_country_id = (int)$cust_country_id;
      $offer_title = $this->input->post('offer_title');
      $offer_price = $this->input->post('offer_price');
      $delivered_in = $this->input->post('delivered_in'); $delivered_in = (int)$delivered_in;
      $cat_type_id = $this->input->post('cat_type_id'); $cat_type_id = (int)$cat_type_id;
      $cat_id = $this->input->post('cat_id'); $cat_id = (int)$cat_id;
      $tags = $this->input->post('tags');
      $offer_desc = $this->input->post('off_desc');
      $offer_req = $this->input->post('offer_req');
      $location_type = $this->input->post('location_type'); $location_type = (int)$location_type;
      $country_id = $this->input->post('country_id'); $country_id = (int)$country_id;
      if($country_id == 0) $country_id = $cust_country_id;
      $state_id = $this->input->post('state_id'); $state_id = (int)$state_id;
      $city_id = $this->input->post('city_id'); $city_id = (int)$city_id;
      $add_on_status = $this->input->post('add_on_status'); $add_on_status = (int)$add_on_status;
      $no_of_add_ons = $this->input->post('no_of_add_ons'); $no_of_add_ons = (int)$no_of_add_ons;
      $quick_delivery = $this->input->post('quick_delivery'); $quick_delivery = (int)$quick_delivery;
      $quick_delivery_price = $this->input->post('quick_delivery_price');
      $quick_delivery_day = $this->input->post('quick_delivery_day'); $quick_delivery_day = (int)$quick_delivery_day;
      $addon_id = $this->input->post('addon_id');
      $addon_title = $this->input->post('addon_title');
      $addon_price = $this->input->post('addon_price');
      $work_day = $this->input->post('work_day');
      $today = date('Y-m-d H:i:s');
      $offer_details = $this->api->get_service_provider_offer_details($offer_id);
      $currency_code = $this->api->get_country_currency_detail((int)$cust_country_id)['currency_sign'];
      $currency_id = $this->api->get_country_currency_detail((int)$cust_country_id)['currency_id'];

      if(!empty($_FILES["default_image_url"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('default_image_url')) { 
          $uploads    = $this->upload->data();  
          $default_image_url =  $config['upload_path'].$uploads["file_name"];
          if($offer_details['default_image_url'] != "NULL") { unlink(trim($offer_details['default_image_url'])); }
        } else { $default_image_url = $offer_details['default_image_url']; }
      } else { $default_image_url = $offer_details['default_image_url']; }
      if(!empty($_FILES["image_url_1"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_1')) { 
          $uploads    = $this->upload->data();  
          $image_url_1 =  $config['upload_path'].$uploads["file_name"];
          if($offer_details['image_url_1'] != "NULL") { unlink(trim($offer_details['image_url_1'])); }
        } else { $image_url_1 = $offer_details['image_url_1']; }
      } else { $image_url_1 = $offer_details['image_url_1']; }
      if(!empty($_FILES["image_url_2"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_2')) { 
          $uploads    = $this->upload->data();  
          $image_url_2 =  $config['upload_path'].$uploads["file_name"];
          if($offer_details['image_url_2'] != "NULL") { unlink(trim($offer_details['image_url_2'])); }
        } else { $image_url_2 = $offer_details['image_url_2']; }
      } else { $image_url_2 = $offer_details['image_url_2']; }
      if(!empty($_FILES["image_url_3"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_3')) { 
          $uploads    = $this->upload->data();  
          $image_url_3 =  $config['upload_path'].$uploads["file_name"];
          if($offer_details['image_url_3'] != "NULL") { unlink(trim($offer_details['image_url_3'])); }
        } else { $image_url_3 = $offer_details['image_url_3']; }
      } else { $image_url_3 = $offer_details['image_url_3']; }
      if(!empty($_FILES["image_url_4"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_4')) { 
          $uploads    = $this->upload->data();  
          $image_url_4 =  $config['upload_path'].$uploads["file_name"];
          if($offer_details['image_url_4'] != "NULL") { unlink(trim($offer_details['image_url_4'])); }
        } else { $image_url_4 = $offer_details['image_url_4']; }
      } else { $image_url_4 = $offer_details['image_url_4']; }
      if(!empty($_FILES["image_url_5"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/m4-image/offers/';
        $config['allowed_types']  = '*';
        $config['max_size']       = '0';
        $config['max_width']      = '0';
        $config['max_height']     = '0';
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image_url_5')) { 
          $uploads    = $this->upload->data();  
          $image_url_5 =  $config['upload_path'].$uploads["file_name"];
          if($offer_details['image_url_5'] != "NULL") { unlink(trim($offer_details['image_url_5'])); }
        } else { $image_url_5 = $offer_details['image_url_5']; }
      } else { $image_url_5 = $offer_details['image_url_5']; }

      $update_data = array(
        "offer_title" => trim($offer_title),
        "offer_desc" => trim($offer_desc),
        "offer_req" => trim($offer_req),
        "delivered_in" => $delivered_in,
        "default_image_url" => $default_image_url,
        "image_url_1" => $image_url_1,
        "image_url_2" => $image_url_2,
        "image_url_3" => $image_url_3,
        "image_url_4" => $image_url_4,
        "image_url_5" => $image_url_5,
        "add_on_status" => $add_on_status,
        "quick_delivery" => $quick_delivery,
        "quick_delivery_day" => $quick_delivery_day,
        "quick_delivery_price" => $quick_delivery_price,
        "offer_price" => trim($offer_price),
        "cat_type_id" => (int)$cat_type_id,
        "cat_id" => (int)$cat_id,
        "tags" => $tags,
        "location_type" => $location_type,
        "country_id" => $country_id,
        "state_id" => $state_id,
        "city_id" => $city_id,
        "update_datetime" => $today,
        "is_edited" => 1,
      );
      //echo json_encode($update_data); die();

      if($this->api->update_offer_details($offer_id, $update_data)) {
        //check for tags and insert new one
        $tags = explode(',', $tags);
        for ($i=0; $i < sizeof($tags); $i++) { 
          if(!$this->api->check_tag_exists(strtolower($tags[$i]))) {
            $insert_data_tag = array(
              "tag_name" => strtolower($tags[$i]),
              "status" => 1,
              "cre_datetime" => $today,
            );
            $this->api->register_offer_tag($insert_data_tag);
          }
        }

        //insert/update Offer Add-ons
        if((int)$add_on_status == 1 && $addon_id != "NULL" ) {
          $addon_id = explode('~', $addon_id);
          $addon_title = explode('~', $addon_title);
          $addon_price = explode('~', $addon_price);
          $work_day = explode('~', $work_day);
          for ($i=0; $i < $no_of_add_ons; $i++) {
            if($addon_id[$i] == 0) {
              $insert_add_ons = array(
                "offer_id" => $offer_id,
                "cust_id" => $cust_id,
                "cre_datetime" => $today,
                "addon_title" => $addon_title[$i],
                "addon_price" => $addon_price[$i],
                "country_id" => (int)$country_id,
                "currency_id" => (int)$currency_id,
                "currency_code" => trim($currency_code),
                "work_day" => $work_day[$i],
                "status" => 1,
                "update_datetime" => $today,
              );
              $this->api->register_provider_offer_add_on_details($insert_add_ons);
            } else {
              $update_add_ons = array(
                "addon_title" => $addon_title[$i],
                "addon_price" => $addon_price[$i],
                "work_day" => $work_day[$i],
                "update_datetime" => $today,
              );
              $this->api->update_provider_offer_add_on_details((int)$addon_id[$i], $update_add_ons);
            }
          }
        }
        $this->response=['response'=>'success', 'message'=> $this->lang->line('update_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')];  }
      echo json_encode($this->response);
    }
    public function provider_delete_offer_add_on() {
      if(empty($this->errors)) { 
        $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
        $addon_id = $this->input->post('addon_id'); $addon_id = (int) $addon_id;
        $update_add_ons = array(
          "status" => 0,
          "update_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->update_provider_offer_add_on_details((int)$addon_id, $update_add_ons) ) { 
          $offer_addons = $this->api->get_offer_add_ons($offer_id);
          if(sizeof($offer_addons)==0) {
            $update_data = array(
              "add_on_status" => 0,
              "update_datetime" => date('Y-m-d H:i:s'),
              "is_edited" => 1,
            );
            $this->api->update_offer_details($offer_id, $update_data);
          }
          $this->response = ['response'=>'success', 'message'=> $this->lang->line('update_success')]; }
        else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
        echo json_encode($this->response);
      }
    }
  //End Service Provider Manage Offers---------------------

  //Start Customer offer search, buy and payment-----------
    public function available_offers_list_view() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int)$last_id;
        $country_id = $this->input->post('country_id', TRUE); $country_id = (int)$country_id;
        $state_id = $this->input->post('state_id', TRUE); $state_id = (int)$state_id;
        $city_id = $this->input->post('city_id', TRUE); $city_id = (int)$city_id;
        $sort_by = $this->input->post('sort_by', TRUE);
        $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
        $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
        $delivered_in = $this->input->post('delivered_in', TRUE); $delivered_in = (int)$delivered_in;
        $start_price = $this->input->post('start_price', TRUE); $start_price = (int)$start_price;
        $end_price = $this->input->post('end_price', TRUE); $end_price = (int)$end_price;
        $latest = $this->input->post('latest', TRUE);
        $active_status = 'active';
        $running_status = 'resumed';

        $favorite_offers = $this->api->get_favorite_offers($cust_id);
        $favorite_offer_ids = '';
        foreach ($favorite_offers as $offer) {
          $favorite_offer_ids .= $offer['offer_id'].',';
        }
        $favorite_offer_ids = substr(trim($favorite_offer_ids), 0, -1);
        $favorite_offer_ids = explode(',', $favorite_offer_ids);

        $search_data = array(
          "cust_id" => 0,
          "country_id" => trim($country_id),
          "state_id" => trim($state_id),
          "city_id" => trim($city_id),
          "sort_by" => trim($sort_by),
          "cat_type_id" => trim($cat_type_id),
          "cat_id" => trim($cat_id),
          "delivered_in" => trim($delivered_in),
          "start_price" => trim($start_price),
          "end_price" => trim($end_price),
          "latest" => trim($latest),
          "active_status" => trim($active_status),
          "running_status" => trim($running_status),
          "device" => 'mobile',
          "last_id" => $last_id,
          "offer_pause_resume" => 'all',
        );
        
        if( $list = $this->api->get_service_provider_offers_filtered($search_data)) {
          for ($i=0; $i < sizeof($list); $i++) {
            //chekc of favourite offers
            if(in_array($list[$i]['offer_id'], $favorite_offer_ids)) { $list[$i] += ['is_favorite' => 1]; } else { $list[$i] += ['is_favorite' => 0]; }

            //get addons
            $add_ons = $this->api->get_offer_add_ons($list[$i]['offer_id']);
            if($add_ons) $list[$i] += ['add_ons' => $add_ons];
            else $list[$i] += ['add_ons' => array()];
          }
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $list];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function customer_offer_details() {
      if(empty($this->errors)) { 
        $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
        if($offer_details = $this->api->get_service_provider_offer_details((int)$offer_id)) {

          if($user_profile = $this->api->get_user_details((int)$offer_details['cust_id'])) $offer_details += ['user_profile' => $user_profile]; 
          else $offer_details += ['user_profile' => array()];

          if($provider_profile = $this->api->get_service_provider_profile((int)$offer_details['cust_id'])) $offer_details += ['provider_profile' => $provider_profile]; 
          else $offer_details += ['provider_profile' => array()];
          
          if($offer_addons = $this->api->get_offer_add_ons((int)$offer_id)) $offer_details += ['offer_addons' => $offer_addons];
          else $offer_details += ['offer_addons' => array()];

          if($offer_reviews = $this->api->get_offer_reviews((int)$offer_id)) $offer_details += ['offer_reviews' => $offer_reviews];
          else $offer_details += ['offer_reviews' => array()];

          $search_data = array(
            "country_id" => $offer_details['country_id'],
            "cat_type_id" => $offer_details['cat_type_id'],
            "cat_id" => $offer_details['cat_id'],
          );
          if($advance_payment_details = $this->api->get_country_cat_advance_payment_details($search_data)) $offer_details += ['advance_payment_details' => $advance_payment_details];
          else $offer_details += ['advance_payment_details' => array()];

          $this->response = ['response' => 'success', 'message'=> $this->lang->line('Details found!'), "offer_details" => $offer_details];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('details not found'), "offer_details" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function buy_offer() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $offer_id = $this->input->post('offer_id'); $offer_id = (int)$offer_id;
        $offer_details = $this->api->get_service_provider_offer_details($offer_id);
        $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
        $customer_details = $this->api->get_user_details($cust_id);
        $selected_add_on_ids = $this->input->post('selected_add_on_ids');
        
        $quick_delivery = $this->input->post('quick_delivery'); $quick_delivery = (int)$quick_delivery;
        if($quick_delivery == 1) { $quick_delivery_base_price = $offer_details['quick_delivery_price'];
        } else { $quick_delivery_base_price = 0; }

        $offer_base_price = $offer_details['offer_price'];
        $quantity = $this->input->post('quantity'); $quantity = (int)$quantity;
        $provider_details = $this->api->get_service_provider_profile((int)$offer_details['cust_id']);
        $today = date('Y-m-d H:i:s');
        $search_data = array(
          "country_id" => $offer_details['country_id'],
          "cat_type_id" => $offer_details['cat_type_id'],
          "cat_id" => $offer_details['cat_id'],
        );
        if($advance_payment_details = $this->api->get_country_cat_advance_payment_details($search_data)) {
          //echo json_encode($advance_payment_details); die();
          
          $add_on_price = 0;
          if($selected_add_on_ids != 'NULL') {
            $selected_add_on_ids = explode(',', $selected_add_on_ids);
            for ($i = 0; $i < sizeof($selected_add_on_ids); $i++) {
              $add_on_details = $this->api->get_offer_add_on_details($selected_add_on_ids[$i]);
              $add_on_price += $add_on_details['addon_price'];
            }
            $no_of_addons = sizeof($selected_add_on_ids);
          } else { $no_of_addons = 0; }

          $offer_price = round(($offer_details['offer_price']*$quantity)+($add_on_price*$quantity)+($quick_delivery_base_price*$quantity),2);

          $gonagoo_commission_amount = round((($offer_price/100) * $advance_payment_details['gonagoo_commission']),2);

          $insert_data = array(
            "job_title" => $offer_details['offer_title'],
            "cat_id" => (int)$offer_details['cat_type_id'],
            "sub_cat_id" => (int)$offer_details['cat_id'],
            "job_desc" => $offer_details['offer_desc'],
            "attachment_1" => 'NULL',
            "attachment_2" => 'NULL',
            "attachment_3" => 'NULL',
            "attachment_4" => 'NULL',
            "attachment_5" => 'NULL',
            "attachment_6" => 'NULL',
            "attachment_7" => 'NULL',
            "attachment_8" => 'NULL',
            "work_type" => 'NULL',
            "currency_id" => (int)$offer_details['currency_id'],
            "currency_code" => $offer_details['currency_code'],
            "budget" => 0,
            "location_type" => 'NULL',
            "address" => 'NULL',
            "start_datetime" => 'NULL',
            "start_datetime" => 'NULL',
            "hours" => 0,
            "hours_per_duration" => 'NULL',
            "visibility" => 'NULL',
            "project_duration" => 'NULL',
            "question_1" => 'NULL',
            "question_2" => 'NULL',
            "question_3" => 'NULL',
            "question_4" => 'NULL',
            "question_5" => 'NULL',
            "cre_datetime" => $today,
            "update_datetime" => $today,
            "country_id" => (int)$offer_details['country_id'],
            "cust_id" => $cust_id,
            "provider_id" => (int)$offer_details['cust_id'],
            "proposal_accepted_datetime" => 'NULL',
            "open_for_proposal" => 1,
            "proposal_id" => 0,
            "proposal_price" => 0,
            "job_type" => 'NULL',
            "job_status" => 'open',
            "job_status_datetime" => $today,
            "cancellation_reason" => 'NULL',
            "cancellation_desc" => 'NULL',
            "promo_code" => 'NULL',
            "promo_code_id" => 0,
            "promo_code_version" => 0,
            "promo_code_datetime" => 'NULL',
            "discount_amount" => 0,
            "discount_percent" => 0,
            "actual_price" => $offer_price,
            "offer_total_price" => $offer_price,
            "paid_amount" => 0,
            "payment_method" => 'NULL',
            "payment_mode" => 'NULL',
            "payment_by" => 'NULL',
            "transaction_id" => 'NULL',
            "payment_response" => 'NULL',
            "payment_datetime" => 'NULL',
            "balance_amount" => $offer_price,
            "complete_paid" => 0,
            "review_comment" => 'NULL',
            "review_datetime" => 'NULL',
            "rating" => 0,
            "cust_name" => $customer_details['firstname'].' '.$customer_details['lastname'],
            "cust_contact" => $customer_details['mobile1'],
            "cust_email" => $customer_details['email1'],
            "provider_name" => $provider_details['firstname'].' '.$provider_details['lastname'],
            "provider_company_name" => $provider_details['company_name'],
            "provider_contact" => $provider_details['contact_no'],
            "provider_email" => $provider_details['email_id'],
            "gonagoo_commission_per" => $advance_payment_details['gonagoo_commission'],
            "gonagoo_commission_amount" => $gonagoo_commission_amount,
            "commission_invoice_url" => 'NULL',
            "is_disputed" => 0,
            "dispute_id" => 0,
            "status_updated_by" => 'customer',
            "cancelled_date" => 'NULL',
            "refund_amount" => 0,
            "refund_invoice_url" => 'NULL',
            "job_url" => 'NULL',
            "view_count" => 0,
            "favorite_counts" => 0,
            "service_type" => 1,
            "offer_id" => $offer_id,
            "offer_qty" => $quantity,
            "contract_expired_on" => 'NULL',
            "offer_quick_delivery_charges" => ($quick_delivery_base_price * $quantity),
            "addon_total_price" => ($add_on_price * $quantity),
            "no_of_addons" => $no_of_addons,
          );
          //echo json_encode($insert_data); die();

          if($job_id = $this->api->register_job_details($insert_data)) {
            //insert ourchased addon details
            if($selected_add_on_ids != 'NULL') {
              for ($i = 0; $i < sizeof($selected_add_on_ids); $i++) {
                $add_on_details = $this->api->get_offer_add_on_details($selected_add_on_ids[$i]);
                $insert_data_addon = array(
                  "job_id" => (int)$job_id,
                  "cre_datetime" => $today,
                  "addon_title" => $add_on_details['addon_title'],
                  "country_id" => $add_on_details['country_id'],
                  "currency_id" => $add_on_details['currency_id'],
                  "currency_code" => $add_on_details['currency_code'],
                  "addon_price" => $add_on_details['addon_price'],
                  "work_day" => $add_on_details['work_day'],
                  "update_datetime" => $today,
                  "addon_qty" => $quantity,
                  "total_price" => round(($add_on_details['addon_price'] * $quantity),2),
                );
                $this->api->register_purchased_addons($insert_data_addon);
              }
            }

            if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
              $parts = explode("@", trim($customer_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

            if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
              $parts = explode("@", trim($provider_details['email_id']));
              $username = $parts[0];
              $provider_name = $username;
            } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

            //Notifications to customers----------------
              $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('You have purchased an offer from')." ".$provider_name.". ".$this->lang->line('Kindly complete the payment').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
              //Send Push Notifications
              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_customer = $this->api->get_user_device_details($cust_id);
              if(!is_null($device_details_customer)) {
                $arr_customer_fcm_ids = array();
                $arr_customer_apn_ids = array();
                foreach ($device_details_customer as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Offer purchase confirm.'), 'type' => 'offer-purchse-notice', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
                //APN
                $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase confirm.'), 'text' => $message);
                if(is_array($arr_customer_apn_ids)) { 
                  $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
                }
              }
              //SMS Notification
              $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
              $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

              //Email Notification
              $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase confirmation'), trim($message));
            //------------------------------------------

            //Notifications to Provider----------------
              $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.". ".$this->lang->line('Login your account to proceed it').". ";
              //Send Push Notifications
              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_provider = $this->api->get_user_device_details((int)$offer_details['cust_id']);
              if(!is_null($device_details_provider)) {
                $arr_provider_fcm_ids = array();
                $arr_provider_apn_ids = array();
                foreach ($device_details_provider as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Offer purchase confirm.'), 'type' => 'offer-purchse-notice', 'notice_date' => $today, 'desc' => $message);
                $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                //APN
                $msg_apn_provider =  array('title' => $this->lang->line('Offer purchase confirm.'), 'text' => $message);
                if(is_array($arr_provider_apn_ids)) { 
                  $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                  $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                }
              }
              //SMS Notification
              $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
              $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

              //Email Notification
              $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase confirmation'), trim($message));
            //------------------------------------------
            
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Offer purchased on')." ".$today.". ".$this->lang->line('Kindly complete the payment').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";

            //Update to workstream master
            $update_data = array(
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$offer_details['cust_id'],
              'cre_datetime' => $today,
              'status' => 0,
            );
            if($workstream_id = $this->api->update_to_workstream_master($update_data)) {
              //Update to workroom
              $update_data = array(
                'workstream_id' => (int)$workstream_id,
                'job_id' => (int)$job_id,
                'cust_id' => $cust_id,
                'provider_id' => (int)$offer_details['cust_id'],
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $cust_id,
                'type' => 'new_job',
                'file_type' => 'text',
                'proposal_id' => 0,
              );
              $this->api->update_to_workstream_details($update_data);
            }
            $job_details = $this->api->get_customer_job_details($job_id);
            $this->response=['response'=>'success', 'message'=> $this->lang->line('Offer purchased successfully'), 'job_details'=> $job_details];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Unable to purchase offer, try again...'), 'job_details'=> array()];
          }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Unable to purchase offer, try again...'), 'job_details'=> array()]; }
        echo json_encode($this->response);
      }
    }
    public function offer_payment_bank() {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $device_type = $this->input->post('device_type', TRUE); // ios/android
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      
      $data = array(
        'payment_method' => 'bank', 
        'payment_mode' => 'bank', 
        'payment_by' => $device_type, 
        'is_bank_payment' => 1,
        'job_status' => 'in_progress',
      );
      if( $this->api->job_details_update($job_id, $data) ) {
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);

        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Offer payment marked as bank payment. ID').': '.$job_id;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer payment marked as Bank'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Offer payment marked as Bank'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer payment marked as Bank'), trim($message));
        //------------------------------------------------
        //workstream entry--------------------------------
          $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'payment',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //workstream entry--------------------------------
        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer payment marked as Bank'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Offer payment marked as Bank'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment marked as Bank'), trim($message));
        //------------------------------------------------
        //Update offer sales count------------------------
          $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
          $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
          $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
        //------------------------------------------------
        $this->response = ['response'=>'success', 'message'=> $this->lang->line('update_success')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('update_failed')]; }
      echo json_encode($this->response);
    }
    public function offer_payment_mtn() {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $device_type = $this->input->post('device_type', TRUE); // ios/android
      $phone_no = $this->input->post('phone_no', TRUE); 
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      //echo json_encode($offer_total_price); die();
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      
      $url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$offer_total_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      $contents = curl_exec($ch);
      $mtn_pay_res = json_decode($contents, TRUE);
      //var_dump($mtn_pay_res); die();
      
      $payment_method = 'mtn';
      $transaction_id = '1234567890';
      //$transaction_id = trim($mtn_pay_res['TransactionID']);
      
      $payment_data = array();
      //if($mtn_pay_res['StatusCode'] === "01"){
      if(1){
        $data = array(
          'paid_amount' => $offer_total_price,
          'payment_method' => trim($payment_method), 
          'payment_mode' => 'online', 
          'payment_by' => $device_type, 
          'transaction_id' => trim($transaction_id), 
          'payment_response' => trim($mtn_pay_res), 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1,
          'job_status' => 'in_progress',
        );
        if( $this->api->job_details_update($job_id, $data) ) {
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Customer Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
            $message = $this->lang->line('Payment completed for offer purchase. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase payment'), trim($message));
          //------------------------------------------------
          //Workstream entry--------------------------------
            $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
          //Provider Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
            $message = $this->lang->line('Payment received for offer. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer payment received'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment received'), trim($message));
          //------------------------------------------------

          //Payment Section---------------------------------
            //Add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              //Smp add Payment to customer account---------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              //Smp add Payment to customer account---------
            //----------------------------------------------
            //Deduct Payment to customer account------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              //Smp deduct Payment to customer account------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'payment',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              //Smp deduct Payment to customer account------
            //----------------------------------------------
            //Add Payment To Gonagoo Account----------------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------
            //Add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$provider_id,
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$provider_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($offer_total_price),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------------
            //----------------------------------------------
          //Payment Section---------------------------------
          //Update offer sales count------------------------
            $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
            $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
            $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
          //------------------------------------------------
          $this->response = ['response'=>'success', 'message'=> $this->lang->line('Payment successfully completed.')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('payment_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('payment_failed')]; }
      echo json_encode($this->response);
    }
    public function offer_payment_stripe() {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $stripeEmail = $this->input->post('stripeEmail', TRUE);
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $device_type = $this->input->post('device_type', TRUE); // ios/android
      $today = date('Y-m-d H:i:s'); 
      $job_details = $this->api->get_job_details($job_id);
      $offer_total_price = trim($job_details['offer_total_price']);
      $payment_method = 'stripe';
      
      $data = array(
        'paid_amount' => $offer_total_price,
        'payment_method' => trim($payment_method), 
        'payment_mode' => 'online', 
        'payment_by' => $device_type, 
        'transaction_id' => trim($transaction_id), 
        'payment_response' => trim($stripeEmail), 
        'payment_datetime' => trim($today),
        'balance_amount' => '0', 
        'complete_paid' => 1,
        'job_status' => 'in_progress',
      );
      if( $this->api->job_details_update($job_id, $data) ) {
        $customer_details = $this->api->get_user_details($cust_id);
        $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
        $provider_id = $provider_details['cust_id'];

        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Customer Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
          $message = $this->lang->line('Payment completed for offer purchase. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer purchase payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase payment'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase payment'), trim($message));
        //------------------------------------------------
        //Workstream entry--------------------------------
          $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
          $update_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => (int)$job_details['provider_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'payment',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
        //Workstream entry--------------------------------
        //Provider Notifications--------------------------
          $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
          $message = $this->lang->line('Payment received for offer. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Offer payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Offer payment received'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }

          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment received'), trim($message));
        //------------------------------------------------

        //Payment Section---------------------------------
          //Add Payment to customer account---------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
            } else {
              $data_cust_mstr = array(
                "user_id" => $job_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_cust_mstr);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($offer_total_price),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            //Smp add Payment to customer account---------
              if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            //Smp add Payment to customer account-----------
          //----------------------------------------------
          //Deduct Payment to customer account------------
            if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
            } else {
              $data_cust_mstr = array(
                "user_id" => $job_details['cust_id'],
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_customer_record($data_cust_mstr);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            }
            $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
            $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
            $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
            $data_acc_hstry = array(
              "account_id" => (int)$cust_acc_mstr['account_id'],
              "order_id" => $job_id,
              "user_id" => $cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($offer_total_price),
              "account_balance" => trim($account_balance),
              "withdraw_request_id" => 0,
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_payment_in_account_history($data_acc_hstry);
            //Smp deduct Payment to customer account------
              if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
              $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
            //Smp deduct Payment to customer account------
          //----------------------------------------------
          //Add Payment To Gonagoo Account----------------
            if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
            } else {
              $data_g_mstr = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_gonagoo_master_record($data_g_mstr);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
            }

            $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($offer_total_price);
            $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
            $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

            $data_g_hstry = array(
              "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
              "order_id" => (int)$job_id,
              "user_id" => (int)$cust_id,
              "type" => 1,
              "transaction_type" => 'standard_price',
              "amount" => trim($offer_total_price),
              "datetime" => $today,
              "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
              "transfer_account_type" => 'NULL',
              "transfer_account_number" => 'NULL',
              "bank_name" => 'NULL',
              "account_holder_name" => 'NULL',
              "iban" => 'NULL',
              "email_address" => 'NULL',
              "mobile_number" => 'NULL',
              "transaction_id" => 'NULL',
              "currency_code" => trim($job_details['currency_code']),
              "country_id" => trim($customer_details['country_id']),
              "cat_id" => (int)$job_details['sub_cat_id'],
            );
            $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
          //----------------------------------------------
          //Add Payment to provider Escrow----------------
            if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
            } else {
              $data_pro_scrw = array(
                "deliverer_id" => (int)$provider_id,
                "scrow_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($job_details['currency_code']),
              );
              $this->api->insert_scrow_record($data_pro_scrw);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
            }

            $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
            $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
            $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

            $data_scrw_hstry = array(
              "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
              "order_id" => (int)$job_id,
              "deliverer_id" => (int)$provider_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'standard_price',
              "amount" => trim($offer_total_price),
              "scrow_balance" => trim($scrow_balance),
              "currency_code" => trim($job_details['currency_code']),
              "cat_id" => (int)$job_details['sub_cat_id']
            );
            $this->api->insert_scrow_history_record($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record_smp($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
              $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
            //Smp add Payment to provider Escrow----------
          //----------------------------------------------
        //Payment Section---------------------------------
        //Update offer sales count------------------------
          $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
          $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
          $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
        //------------------------------------------------
        $this->response = ['response'=>'success', 'message'=> $this->lang->line('Payment successfully completed.')];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('payment_failed')]; }
      echo json_encode($this->response);
    }
    public function offer_payment_orange() {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $job_details = $this->api->get_job_details($job_id);
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
      $device_type = $this->input->post('device_type', TRUE); // ios/android
      $notif_token = $this->input->post('notif_token', TRUE);
      $pay_token = $this->input->post('pay_token', TRUE);
      $payment_id = $this->input->post('payment_id', TRUE);
      $offer_total_price = $this->input->post('offer_total_price', TRUE);
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];
      // get payment link
      $json_post_data = json_encode(
                          array(
                            "order_id" => $payment_id, 
                            "amount" => $offer_total_price, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      //$transaction_id = $payment_details['txnid'];
      $transaction_id = '123';
      //echo json_encode($payment_details); die();
      // if($payment_details['status'] == 'SUCCESS' && $booking_details['complete_paid'] == 0) {
      if(1) {
        $data = array(
          'paid_amount' => $offer_total_price,
          'payment_method' => trim($payment_method), 
          'payment_mode' => 'online', 
          'payment_by' => $device_type, 
          'transaction_id' => trim($transaction_id), 
          'payment_response' => trim($payment_res), 
          'payment_datetime' => trim($today),
          'balance_amount' => '0', 
          'complete_paid' => 1,
          'job_status' => 'in_progress',
        );
        if( $this->api->job_details_update($job_id, $data) ) {
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          $provider_id = $provider_details['cust_id'];

          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Customer Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($customer_details['country_id']);
            $message = $this->lang->line('Payment completed for offer purchase. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($customer_details['mobile1']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase payment'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase payment'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase payment'), trim($message));
          //------------------------------------------------
          //Workstream entry--------------------------------
            $workstream_details = $this->api->get_workstream_details_by_job_id($job_id);
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'payment',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          //Workstream entry--------------------------------
          //Provider Notifications--------------------------
            $country_code = $this->api->get_country_code_by_id($provider_details['country_id']);
            $message = $this->lang->line('Payment received for offer. ID').': '.$job_id.'. '.$this->lang->line('Payment ID').' '.$transaction_id;
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);
            $this->api_sms->send_sms_whatsapp((int)$country_code.trim($provider_details['contact_no']), $message);

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer payment received'), 'type' => 'booking-payment', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer payment received'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }

            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer payment received'), trim($message));
          //------------------------------------------------

          //Payment Section---------------------------------
            //Add Payment to customer account---------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              //Smp add Payment to customer account---------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              //Smp add Payment to customer account---------
            //----------------------------------------------
            //Deduct Payment to customer account------------
              if($cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']))) {
              } else {
                $data_cust_mstr = array(
                  "user_id" => $job_details['cust_id'],
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              }
              $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
              $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $cust_id, $account_balance);
              $cust_acc_mstr = $this->api->customer_account_master_details($cust_id, trim($job_details['currency_code']));
              $data_acc_hstry = array(
                "account_id" => (int)$cust_acc_mstr['account_id'],
                "order_id" => $job_id,
                "user_id" => $cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'payment',
                "amount" => trim($offer_total_price),
                "account_balance" => trim($account_balance),
                "withdraw_request_id" => 0,
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_payment_in_account_history($data_acc_hstry);
              //Smp deduct Payment to customer account------
                if($cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) - trim($offer_total_price);
                $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $cust_id, $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details_smp($cust_id, trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'payment',
                  "amount" => trim($offer_total_price),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
              //Smp deduct Payment to customer account------
            //----------------------------------------------
            //Add Payment To Gonagoo Account----------------
              if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
              } else {
                $data_g_mstr = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_gonagoo_master_record($data_g_mstr);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
              }

              $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) + trim($offer_total_price);
              $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
              $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

              $data_g_hstry = array(
                "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                "order_id" => (int)$job_id,
                "user_id" => (int)$cust_id,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "datetime" => $today,
                "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => 'NULL',
                "currency_code" => trim($job_details['currency_code']),
                "country_id" => trim($customer_details['country_id']),
                "cat_id" => (int)$job_details['sub_cat_id'],
              );
              $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
            //----------------------------------------------
            //Add Payment to provider Escrow----------------
              if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']))) {
              } else {
                $data_pro_scrw = array(
                  "deliverer_id" => (int)$provider_id,
                  "scrow_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($job_details['currency_code']),
                );
                $this->api->insert_scrow_record($data_pro_scrw);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));
              }

              $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
              $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
              $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$provider_id, trim($job_details['currency_code']));

              $data_scrw_hstry = array(
                "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                "order_id" => (int)$job_id,
                "deliverer_id" => (int)$provider_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'standard_price',
                "amount" => trim($offer_total_price),
                "scrow_balance" => trim($scrow_balance),
                "currency_code" => trim($job_details['currency_code']),
                "cat_id" => (int)$job_details['sub_cat_id']
              );
              $this->api->insert_scrow_history_record($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$provider_id,
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record_smp($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) + trim($offer_total_price);
                $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$provider_id, $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$provider_id, trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$provider_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($offer_total_price),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
              //Smp add Payment to provider Escrow----------
            //----------------------------------------------
          //Payment Section---------------------------------
          //Update offer sales count------------------------
            $offer_details = $this->api->get_service_provider_offer_details((int)$job_details['offer_id']);
            $update_data = array( "sales_count" => ($offer_details['sales_count'] + 1) );
            $this->api->update_offer_details((int)$job_details['offer_id'], $update_data);
          //------------------------------------------------

          $this->session->unset_userdata('notif_token');
          $this->session->unset_userdata('job_id');
          $this->session->unset_userdata('payment_id');
          $this->session->unset_userdata('amount');
          $this->session->unset_userdata('pay_token');

          $this->response = ['response'=>'success', 'message'=> $this->lang->line('Payment successfully completed.')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('payment_failed')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('payment_failed')]; }
      echo json_encode($this->response);
    }
  //End Customer offer search, buy and payment-------------

  //Start Customer Jobs------------------------------------
    public function job_cancellation_reason_master() {
      if($list = $this->api->get_job_cancellation_reasons()) {
        $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), "list" => $list];
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
      echo json_encode($this->response);
    }
    public function my_jobs()
    {
      //echo json_encode($_GET); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
        $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int)$last_id;
        $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
        $start_date = $this->input->post('start_date', TRUE); //Y-m-d 00:00:00
        $end_date = $this->input->post('end_date', TRUE); //Y-m-d 00:00:00
        $status = $this->input->post('status', TRUE); //open/in_progress/completed/cancelled
        $category_types = $this->api->get_category_types();

        $search_data = array(
          "provider_id" => 0,
          "cust_id" => $cust_id,
          "job_status" => $status,
          "sub_cat_id" => $cat_id,
          "cat_id" => $cat_type_id,
          "start_date" => $start_date,
          "end_date" => $end_date,
          "device" => 'mobile',
          "last_id" => $last_id,
        );

        // $cust_id = $this->input->post('cust_id', TRUE);
        // $filter = $this->input->post('filter', TRUE);
        // if(!isset($filter) && $filter == '') { $filter = 'basic'; }
        // $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
        // if(!isset($cat_id)) { $cat_id = 0; }
        // $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
        // if(!isset($cat_type_id)) { $cat_type_id = 0; }
        // $start_date = $this->input->post('start_date', TRUE);
        // if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        //   $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
        // $end_date = $this->input->post('end_date', TRUE);
        // if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        //   $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
        // $category_types = $this->api->get_category_types();

        // $search_data = array(
        //   "cust_id" => $cust_id,
        //   "provider_id" => 0,
        //   "job_status" => 'open',
        //   "sub_cat_id" => $cat_id,
        //   "cat_id" => $cat_type_id,
        //   "start_date" => $strt_dt,
        //   "end_date" => $end_dt,
        //   "device" => 'web',
        //   "last_id" => 0,
        // );


        if($job_list = $this->api->get_jobs_list($search_data)) {
          if($status == "open"){
            $cancellation_reasons = $this->api->get_job_cancellation_reasons();
            for($i=0; $i<sizeof($job_list); $i++) {
              if($job_list[$i]['service_type'] == 0){
                $proposals = $this->api->get_job_proposals_list($job_list[$i]['job_id']);
                $job_list[$i]['proposal_counts'] = sizeof($proposals);
              }
            }    
            if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
          }

          $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), "job_list" => $job_list];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "job_list" => array()]; }
        echo json_encode($this->response);
      }
    }

    public function customer_offer_cancel() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
        $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
        $reason = $this->input->post('reason', TRUE);
        $other_reason = $this->input->post('other_reason', TRUE);
        $today = date('Y-m-d H:i:s');

        $update_data = array(
          "cancellation_status" => 'cancel_request',
          "cancellation_reason" => $reason,
          "cancellation_desc" => $other_reason,
          "status_updated_by" => 'customer',
          "cancelled_date" => $today,
        );
        if($this->api->update_job_details($job_id, $update_data)) {
          $job_details = $this->api->get_job_details($job_id);
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your offer purchase ID').": ".$job_id." ".$this->lang->line('has been sent for cancellation').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase cancellation request'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase cancellation request'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation request'), trim($message));
          //------------------------------------------

          //Notifications to Provider----------------
            $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.", ".$this->lang->line('is requesting to cancel it').". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase cancellation request'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer purchase cancellation request'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

            //Email Notification
            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation request'), trim($message));
          //------------------------------------------
            
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Offer purchased on')." ".$job_details['cre_datetime'].", ".$this->lang->line('has been requested for cancellation').". ".$this->lang->line('Cancellation Reason').": ".$job_details['cancellation_reason'].". ";

          if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'cancel_request',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          }

          $job_details = $this->api->get_job_details($job_id);
          $this->response = ['response' => 'success', 'message' => $this->lang->line('Job cancellation request sent.'), "job_details" => $job_details];
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('Unable to send cancellation request. Try again...'), "job_details" => array()];
        }
        echo json_encode($this->response);
      }
    }
    public function customer_withdraw_cancel_request() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
        $today = date('Y-m-d H:i:s');

        $update_data = array(
          "cancellation_status" => 'NULL',
          "cancellation_reason" => 'NULL',
          "cancellation_desc" => 'NULL',
          "status_updated_by" => 'NULL',
          "cancelled_date" => 'NULL',
        );
        if($this->api->update_job_details($job_id, $update_data)) {
          $job_details = $this->api->get_job_details($job_id);
          $customer_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job ID').": ".$job_id." ".$this->lang->line('cancellation request has been withdrawn').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Job cancellation request withdrawn'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Job cancellation request withdrawn'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job cancellation request withdrawn'), trim($message));
          //------------------------------------------

          //Notifications to Provider----------------
            $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Cancellation request has been withdrawn by')." ".$customer_name.", ".$this->lang->line('for the job. ID').". ".$job_id;
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Job cancellation request withdrawn'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Job cancellation request withdrawn'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

            //Email Notification
            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job cancellation request withdrawn'), trim($message));
          //------------------------------------------
            
          $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job cancellation has been withdrawn').", ".$this->lang->line('for the job. ID').". ".$job_id;

          if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'cancel_withdraw',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          }
          $job_details = $this->api->get_job_details($job_id);
          $this->response = ['response' => 'success', 'message' => $this->lang->line('Job cancellation request has been withdrawn.'), "job_details" => $job_details];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Unable to withdraw job cancellation request. Try again...'), "job_details" => array()]; }
        echo json_encode($this->response);
      }
    }
  //End Customer Jobs--------------------------------------

  //Start Favorite Offers----------------------------------
    public function my_favorite_offers() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int)$last_id;
        $country_id = $this->input->post('country_id', TRUE); $country_id = (int)$country_id;
        $state_id = $this->input->post('state_id', TRUE); $state_id = (int)$state_id;
        $city_id = $this->input->post('city_id', TRUE); $city_id = (int)$city_id;
        $sort_by = $this->input->post('sort_by', TRUE);
        $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
        $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
        $delivered_in = $this->input->post('delivered_in', TRUE); $delivered_in = (int)$delivered_in;
        $start_price = $this->input->post('start_price', TRUE); $start_price = (int)$start_price;
        $end_price = $this->input->post('end_price', TRUE); $end_price = (int)$end_price;
        $latest = $this->input->post('latest', TRUE);
        $active_status = 'active';
        $running_status = 'resumed';

        $favorite_offers = $this->api->get_favorite_offers($cust_id);
        $favorite_offer_ids = '';
        foreach ($favorite_offers as $offer) {
          $favorite_offer_ids .= $offer['offer_id'].',';
        }
        $favorite_offer_ids = substr(trim($favorite_offer_ids), 0, -1);
        $favorite_offer_ids = explode(',', $favorite_offer_ids);

        $search_data = array(
          "cust_id" => 0,
          "country_id" => trim($country_id),
          "state_id" => trim($state_id),
          "city_id" => trim($city_id),
          "sort_by" => trim($sort_by),
          "cat_type_id" => trim($cat_type_id),
          "cat_id" => trim($cat_id),
          "delivered_in" => trim($delivered_in),
          "start_price" => trim($start_price),
          "end_price" => trim($end_price),
          "latest" => trim($latest),
          "active_status" => trim($active_status),
          "running_status" => trim($running_status),
          "device" => 'mobile',
          "last_id" => $last_id,
          "offer_pause_resume" => 'all',
        );
        
        if( $list = $this->api->get_service_provider_offers_filtered($search_data)) {
          //extract favorite offers
          $favrt_list = array();
          foreach ($list as $ls) {
            if($favorite_offer_ids[0]!='') {
              if(in_array($ls['offer_id'], $favorite_offer_ids)) {
                array_push($favrt_list, $ls);
              }
            }
          }
          //get addons
          for ($i=0; $i < sizeof($favrt_list); $i++) {
            $add_ons = $this->api->get_offer_add_ons($favrt_list[$i]['offer_id']);
            if($add_ons) $favrt_list[$i] += ['add_ons' => $add_ons];
            else $favrt_list[$i] += ['add_ons' => array()];
          }
          if (empty($favrt_list)) { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()];
          } else { $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "list" => $favrt_list]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "list" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function offer_add_to_favorite() {
      if(empty($this->errors)) {
        $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
        $cust_id = $this->input->post('cust_id'); $cust_id = (int) $cust_id;
        $offer_details = $this->api->get_service_provider_offer_details($offer_id);
        $insert_data = array(
          "cust_id" => $cust_id,
          "offer_id" => $offer_id,
          "provider_id" => $offer_details['cust_id'],
          "cre_datetime" => date('Y-m-d H:i:s')
        );
        if( $this->api->offer_add_to_favorite($insert_data) ) { 
          //update offer favorite count
          $update_data = array( "favorite_counts" => ($offer_details['favorite_counts'] + 1) );
          $this->api->update_offer_details($offer_id, $update_data);
          $this->response=['response'=>'success', 'message'=> $this->lang->line('add_success')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function offer_remove_from_favorite() {
      if(empty($this->errors)) {
        $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
        $cust_id = $this->input->post('cust_id'); $cust_id = (int) $cust_id;
        if( $this->api->offer_remove_from_favorite($offer_id, $cust_id) ) { 
          //update offer favorite count
          $offer_details = $this->api->get_service_provider_offer_details($offer_id);
          $update_data = array( "favorite_counts" => ($offer_details['favorite_counts'] - 1) );
          $this->api->update_offer_details($offer_id, $update_data);
          $this->response=['response'=>'success', 'message'=> $this->lang->line('delete_success')]; 
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
        echo json_encode($this->response);
      }
    }
  //End Favorite Offers------------------------------------

  //Start Promo Code---------------------------------------
    public function check_promocode() {
      $promo_code_name =  $this->input->post('promocode');
      $job_id =  $this->input->post('job_id');
      $cust_id =  $this->input->post('cust_id');
      $details =  $this->api->get_job_details($job_id);
      if($promo_code = $this->api->get_promocode_by_name(strtoupper($promo_code_name))) {
        $used_codes = $this->api->get_used_promo_code($promo_code['promo_code_id'],$promo_code['promo_code_version']);
        if(sizeof($used_codes) < $promo_code['no_of_limit']) {
          $service_id = explode(',',$promo_code['service_id']);
          if($details['promo_code'] == 'NULL') {
            if(in_array($details['sub_cat_id'], $service_id)) {
              if(strtotime($promo_code['code_expire_date']) >= strtotime(date('M-d-y'))) {
                if(!$this->api->check_used_promo_code($promo_code['promo_code_id'], $promo_code['promo_code_version'], $cust_id)) {
                  $discount_amount = round((($details['offer_total_price']/100)*$promo_code['discount_percent']), 2); 
                  if($details['currency_code'] == 'XAF' || $details['currency_code'] == 'XOF') { $discount_amount = round($discount_amount, 0); }
                  $offer_total_price =  round(($details['offer_total_price'] - $discount_amount) ,2);
                  if($details['currency_code'] == 'XAF' || $details['currency_code'] == 'XOF') { $offer_total_price = round($offer_total_price , 0); }
                  $this->response = ['response' => 'success', 'message' => $this->lang->line('Promocode Available'), 'discount_amount' => $discount_amount, 'discount_percent' => $promo_code['discount_percent'], 'offer_total_price' => $offer_total_price];
                } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used this promocode')]; }
              } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promocode has been Expired')]; }
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('This promocode is not valid for this category')]; }
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used promocode for this order')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promo code is used maximum number of limit')]; }  
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')]; }
      echo json_encode($this->response);
    }
    public function apply_promocode() {
      //echo json_encode($_POST); die(); 
      if(isset($_POST["promocode"])) {
        $job_id = $this->input->post('job_id');  $job_id = (int)$job_id;
        $cust_id = $this->input->post('cust_id');  $cust_id = (int)$cust_id;
        $details =  $this->api->get_job_details($job_id);
        $promo_code_name = $this->input->post("promocode");
        if($details['promo_code'] == 'NULL') {
          if($promocode = $this->api->get_promocode_by_name($promo_code_name)) {
            $used_codes = $this->api->get_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version']);
            if(sizeof($used_codes) < $promocode['no_of_limit']) {
              if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))) {
                if(!$this->api->check_used_promo_code($promocode['promo_code_id'], $promocode['promo_code_version'], $cust_id)) {
                  if($details['currency_code'] == 'XAF' || $details['currency_code'] == 'XOF') {
                    $discount_amount = round((($details['offer_total_price']/100)*$promocode['discount_percent']), 2);
                  } else { $discount_amount = round((($details['offer_total_price']/100)*$promocode['discount_percent']), 0); }
                  $offer_total_price = $details['offer_total_price'] - $discount_amount;
                  $update = array(
                    'offer_total_price' => $offer_total_price,
                    'promo_code' => $promocode['promo_code'],
                    'promo_code_id' => $promocode['promo_code_id'],
                    'promo_code_version' => $promocode['promo_code_version'],
                    'discount_amount' => $discount_amount,
                    'discount_percent' => $promocode['discount_percent'],
                    'promo_code_datetime' => date('Y-m-d h:i:s')
                  );
                  $this->api->job_details_update($job_id, $update);

                  $used_promo_update = array(
                    'promo_code_id' => $promocode['promo_code_id'],
                    'promo_code_version' => $promocode['promo_code_version'],
                    'cust_id' => $cust_id,
                    'discount_percent' => $promocode['discount_percent'],
                    'discount_amount' => $discount_amount,
                    'discount_currency' => $details['currency_code'],
                    'cre_datetime' => date('Y-m-d h:i:s'),
                    'service_id' => $details['sub_cat_id'],
                    'booking_id' => $job_id,
                  );
                  $this->api->register_user_used_prmocode($used_promo_update);
                  $job_details =  $this->api->get_job_details($job_id);
                  $this->response = ['response' => 'success', 'message' => $this->lang->line('Promocode Applied successfully') , 'job_details' => $job_details];
                } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used this promocode')]; }
              } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promocode has been Expired')]; }
            } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('This Promo code is used maximum number of limit')]; }
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invalid Promocode Entered')]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are already used promocode for this order')]; }
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Promocode not found.')]; }
        echo json_encode($this->response);
    }
  //End Promo Code-----------------------------------------
  
  //Start Provider Jobs------------------------------------
    public function provider_jobs() {
      //echo json_encode($_GET); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
        $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
        $last_id = $this->input->post('last_id', TRUE); $last_id = (int)$last_id;
        $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
        $start_date = $this->input->post('start_date', TRUE); //Y-m-d 00:00:00
        $end_date = $this->input->post('end_date', TRUE); //Y-m-d 00:00:00
        $status = $this->input->post('status', TRUE); //open/in_progress/completed/cancelled
        $category_types = $this->api->get_category_types();

        $search_data = array(
          "cust_id" => 0,
          "provider_id" => $cust_id,
          "job_status" => $status,
          "sub_cat_id" => $cat_id,
          "cat_id" => $cat_type_id,
          "start_date" => $start_date,
          "end_date" => $end_date,
          "device" => 'mobile',
          "last_id" => $last_id,
        );
        if($job_list = $this->api->get_jobs_list($search_data)) {
          $this->response = ['response' => 'success', 'message' => $this->lang->line('get_list_success'), "job_list" => $job_list];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "job_list" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function accept_cancellation_request() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
        $today = date('Y-m-d H:i:s');

        $update_data = array(
          "cancellation_status" => 'dispute_closed',
          "job_status" => 'cancelled',
          "job_status_datetime" => $today,
        );
        if($this->api->update_job_details($job_id, $update_data)) {
          $job_details = $this->api->get_job_details($job_id);
          $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
          $pro_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your offer purchase ID').": ".$job_id." ".$this->lang->line('has been cancelled').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase cancellation.'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Offer purchase cancellation.'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
          //------------------------------------------

          //Notifications to Provider-----------------
            $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.", ".$this->lang->line('is cancelled').". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Offer purchase cancellation.'), 'type' => 'offer-purchase-notice', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Offer purchase cancellation.'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

            //Email Notification
            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation.'), trim($message));
          //------------------------------------------
          
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Offer purchased on')." ".$job_details['cre_datetime'].", ".$this->lang->line('has been cancelled').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";

          if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => (int)$job_details['cust_id'],
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'job_cancelled',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          }

          if($job_details['complete_paid'] == 1) {
            //Payment Section---------------------------------
              //Deduct Payment From Provider Escrow-----------
                if($pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
                } else {
                  $data_pro_scrw = array(
                    "deliverer_id" => (int)$job_details['provider_id'],
                    "scrow_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_scrow_record($data_pro_scrw);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));
                }

                $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($job_details['offer_total_price']);
                $this->api->update_scrow_payment((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
                $pro_scrw_mstr_dtls = $this->api->scrow_master_details((int)$job_details['provider_id'], trim($job_details['currency_code']));

                $data_scrw_hstry = array(
                  "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                  "order_id" => (int)$job_id,
                  "deliverer_id" => (int)$job_details['provider_id'],
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($job_details['offer_total_price']),
                  "scrow_balance" => trim($scrow_balance),
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_scrow_history_record($data_scrw_hstry);
                //Smp deduct Payment From Provider Escrow-----------
                  if($pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']))) {
                  } else {
                    $data_pro_scrw = array(
                      "deliverer_id" => (int)$job_details['provider_id'],
                      "scrow_balance" => 0,
                      "update_datetime" => $today,
                      "operation_lock" => 1,
                      "currency_code" => trim($job_details['currency_code']),
                    );
                    $this->api->insert_scrow_record_smp($data_pro_scrw);
                    $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));
                  }

                  $scrow_balance = trim($pro_scrw_mstr_dtls['scrow_balance']) - trim($job_details['offer_total_price']);
                  $this->api->update_scrow_payment_smp((int)$pro_scrw_mstr_dtls['scrow_id'], (int)$job_details['provider_id'], $scrow_balance);
                  $pro_scrw_mstr_dtls = $this->api->scrow_master_details_smp((int)$job_details['provider_id'], trim($job_details['currency_code']));

                  $data_scrw_hstry = array(
                    "scrow_id" => (int)$pro_scrw_mstr_dtls['scrow_id'],
                    "order_id" => (int)$job_id,
                    "deliverer_id" => (int)$job_details['provider_id'],
                    "datetime" => $today,
                    "type" => 0,
                    "transaction_type" => 'standard_price',
                    "amount" => trim($job_details['offer_total_price']),
                    "scrow_balance" => trim($scrow_balance),
                    "currency_code" => trim($job_details['currency_code']),
                    "cat_id" => (int)$job_details['sub_cat_id']
                  );
                  $this->api->insert_scrow_history_record_smp($data_scrw_hstry);
                //Smp deduct Payment From Provider Escrow-----------
              //----------------------------------------------

              //Deduct Payment From Gonagoo Account-----------
                if($g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']))) {
                } else {
                  $data_g_mstr = array(
                    "gonagoo_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_master_record($data_g_mstr);
                  $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));
                }

                $gonagoo_balance = trim($g_mstr_dtls['gonagoo_balance']) - trim($job_details['offer_total_price']);
                $this->api->update_payment_in_gonagoo_master($g_mstr_dtls['gonagoo_id'], $gonagoo_balance);
                $g_mstr_dtls = $this->api->gonagoo_master_details(trim($job_details['currency_code']));

                $data_g_hstry = array(
                  "gonagoo_id" => (int)$g_mstr_dtls['gonagoo_id'],
                  "order_id" => (int)$job_id,
                  "user_id" => $job_details['cust_id'],
                  "type" => 0,
                  "transaction_type" => 'standard_price',
                  "amount" => trim($job_details['offer_total_price']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($g_mstr_dtls['gonagoo_balance']),
                  "transfer_account_type" => 'NULL',
                  "transfer_account_number" => 'NULL',
                  "bank_name" => 'NULL',
                  "account_holder_name" => 'NULL',
                  "iban" => 'NULL',
                  "email_address" => 'NULL',
                  "mobile_number" => 'NULL',
                  "transaction_id" => 'NULL',
                  "currency_code" => trim($job_details['currency_code']),
                  "country_id" => trim($customer_details['country_id']),
                  "cat_id" => (int)$job_details['sub_cat_id'],
                );
                $this->api->insert_payment_in_gonagoo_history($data_g_hstry);
              //----------------------------------------------

              //Add Payment To Customer Account---------------
                if($cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']))) {
                } else {
                  $data_cust_mstr = array(
                    "user_id" => $job_details['cust_id'],
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($job_details['currency_code']),
                  );
                  $this->api->insert_gonagoo_customer_record($data_cust_mstr);
                  $cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']));
                }
                $account_balance = trim($cust_acc_mstr['account_balance']) + trim($job_details['offer_total_price']);
                $this->api->update_payment_in_customer_master($cust_acc_mstr['account_id'], $job_details['cust_id'], $account_balance);
                $cust_acc_mstr = $this->api->customer_account_master_details($job_details['cust_id'], trim($job_details['currency_code']));
                $data_acc_hstry = array(
                  "account_id" => (int)$cust_acc_mstr['account_id'],
                  "order_id" => $job_id,
                  "user_id" => $job_details['cust_id'],
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($job_details['offer_total_price']),
                  "account_balance" => trim($account_balance),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($job_details['currency_code']),
                  "cat_id" => (int)$job_details['sub_cat_id']
                );
                $this->api->insert_payment_in_account_history($data_acc_hstry);
                //Smp add Payment To Customer Account---------
                  if($cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']))) {
                  } else {
                    $data_cust_mstr = array(
                      "user_id" => $job_details['cust_id'],
                      "account_balance" => 0,
                      "update_datetime" => $today,
                      "operation_lock" => 1,
                      "currency_code" => trim($job_details['currency_code']),
                    );
                    $this->api->insert_gonagoo_customer_record_smp($data_cust_mstr);
                    $cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']));
                  }
                  $account_balance = trim($cust_acc_mstr['account_balance']) + trim($job_details['offer_total_price']);
                  $this->api->update_payment_in_customer_master_smp($cust_acc_mstr['account_id'], $job_details['cust_id'], $account_balance);
                  $cust_acc_mstr = $this->api->customer_account_master_details_smp($job_details['cust_id'], trim($job_details['currency_code']));
                  $data_acc_hstry = array(
                    "account_id" => (int)$cust_acc_mstr['account_id'],
                    "order_id" => $job_id,
                    "user_id" => $job_details['cust_id'],
                    "datetime" => $today,
                    "type" => 1,
                    "transaction_type" => 'add',
                    "amount" => trim($job_details['offer_total_price']),
                    "account_balance" => trim($account_balance),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($job_details['currency_code']),
                    "cat_id" => (int)$job_details['sub_cat_id']
                  );
                  $this->api->insert_payment_in_account_history_smp($data_acc_hstry);
                //Smp add Payment To Customer Account---------
              //----------------------------------------------
            //Payment Section---------------------------------
          }

          $job_details = $this->api->get_job_details($job_id);
          $this->response = ['response' => 'success', 'message' => $this->lang->line('Job has been cancelled.'), "job_details" => $job_details];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Unable to cancel the job. Try again...'), "job_details" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function create_dispute_for_job() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
        $dispute_title = $this->input->post('dispute_title', TRUE);
        $dispute_cat_id = $this->input->post('dispute_cat_id', TRUE); $dispute_cat_id = (int) $dispute_cat_id;
        $dispute_sub_cat_id = $this->input->post('dispute_sub_cat_id', TRUE); $dispute_sub_cat_id = (int) $dispute_sub_cat_id;
        $dispute_desc = $this->input->post('dispute_desc', TRUE);
        $request_money_back = $this->input->post('request_money_back', TRUE);  $request_money_back = (int) $request_money_back;
        $today = date('Y-m-d H:i:s');
        $job_details = $this->api->get_job_details($job_id);

        $insert_data = array(
          "cancellation_status" => 'dispute_raised',
        );
        if($this->api->update_job_details($job_id, $insert_data)) {
          $update_data = array(
            "dispute_cat_id" => $dispute_cat_id,
            "dispute_sub_cat_id" => $dispute_sub_cat_id,
            "dispute_title" => trim($dispute_title),
            "dispute_desc" => trim($dispute_desc),
            "service_id" => $job_id,
            "service_type" => $job_details['service_type'],
            "cre_datetime" => $today,
            "cust_id" => $job_details['cust_id'],
            "provider_id" => $job_details['provider_id'],
            "dispute_status" => 'open',
            "dispute_withdraw_datetime" => 'NULL',
            "dispute_in_progress_datetime" => 'NULL',
            "dispute_closed_datetime" => 'NULL',
            "admin_id" => 0,
            "dispute_created_by" => ($cust_id==$job_details['cust_id'])?'customer':'provider',
            "request_money_back" => $request_money_back,
            "money_back_status" => 0,
            "money_back_datetime" => 'NULL',
          );
          $dispute_id = $this->api->create_job_dispute($update_data);

          $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
          $pro_details = $this->api->get_user_details($cust_id);
          $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
          
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
            $provider_name = explode("@", trim($provider_details['email_id']))[0];
          } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your offer purchase ID').": ".$job_id." ".$this->lang->line('has been marked as disputed').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job marked disputed'), trim($message));
          //------------------------------------------

          //Notifications to Provider----------------
            $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Your offer has been purchased by')." ".$customer_name.", ".$this->lang->line('is marked as disputed and send to Gonagoo admin for process').". ";
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
            if(!is_null($device_details_provider)) {
              $arr_provider_fcm_ids = array();
              $arr_provider_apn_ids = array();
              foreach ($device_details_provider as $value) {
                if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Job marked disputed'), 'type' => 'dispute-for-job', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('Job marked disputed'), 'text' => $message);
              if(is_array($arr_provider_apn_ids)) { 
                $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

            //Email Notification
            $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job marked disputed'), trim($message));
          //------------------------------------------
          
          //update to workroom
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('This job has been marked as disputed and send to Gonagoo admin for process');
          if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
            $update_data = array(
              'workstream_id' => (int)$workstream_details['workstream_id'],
              'job_id' => (int)$job_id,
              'cust_id' => (int)$job_details['cust_id'],
              'provider_id' => (int)$job_details['provider_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $cust_id,
              'type' => 'dispute_raised',
              'file_type' => 'text',
              'proposal_id' => 0,
            );
            $this->api->update_to_workstream_details($update_data);
          }

          $job_details = $this->api->get_job_details($job_id);
          $this->response = ['response' => 'success', 'message' => $this->lang->line('Job dispute request sent to Gonagoo admin.'), "job_details" => $job_details];
        } else { 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('Unable to request for job dispute. Try again...'), "job_details" => array()];
        }
        echo json_encode($this->response);
      }
    }
    public function provider_withdraw_dispute() {
      //echo json_encode($_POST); die();
      if(empty($this->errors)) {
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
        $job_id = $this->input->post('job_id', TRUE); $job_id = (int) $job_id;
        $today = date('Y-m-d H:i:s');
        $job_details = $this->api->get_job_details($job_id);
        
        if($dispute_details = $this->api->get_job_dispute_details($job_id)) {
          //echo json_encode($dispute_details); die();
          if($dispute_details['dispute_status'] == 'open') {
            $insert_data = array(
              "cancellation_status" => 'cancel_request',
            );
            if($this->api->update_job_details($job_id, $insert_data)) {
              $update_data = array(
                "dispute_status" => 'withdrawn',
                "dispute_withdraw_datetime" => $today,
              );
              $this->api->update_job_dispute($dispute_details['dispute_id'], $update_data);

              $customer_details = $this->api->get_user_details((int)$job_details['cust_id']);
              $pro_details = $this->api->get_user_details($cust_id);
              $provider_details = $this->api->get_service_provider_profile((int)$job_details['provider_id']);
              
              if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
                $customer_name = explode("@", trim($customer_details['email1']))[0];
              } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

              if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
                $provider_name = explode("@", trim($provider_details['email_id']))[0];
              } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

              //Notifications to customers----------------
                $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job').": ".$job_id." ".$this->lang->line('dispute has been withdrawn by provider').". ".$this->lang->line('Your team')." ".$provider_details['company_name'].". ";
                //Send Push Notifications
                $api_key = $this->config->item('delivererAppGoogleKey');
                $device_details_customer = $this->api->get_user_device_details((int)$job_details['cust_id']);
                if(!is_null($device_details_customer)) {
                  $arr_customer_fcm_ids = array();
                  $arr_customer_apn_ids = array();
                  foreach ($device_details_customer as $value) {
                    if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                    else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
                  }
                  $msg = array('title' => $this->lang->line('Job dispute withdrawn'), 'type' => 'job-dispute-withdrawn', 'notice_date' => $today, 'desc' => $message);
                  $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
                  //APN
                  $msg_apn_customer =  array('title' => $this->lang->line('Job dispute withdrawn'), 'text' => $message);
                  if(is_array($arr_customer_apn_ids)) { 
                    $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                    $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
                  }
                }
                //SMS Notification
                $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
                $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

                //Email Notification
                $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Job dispute withdrawn'), trim($message));
              //------------------------------------------

              //Notifications to Provider----------------
                $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('Job dispute has been withdrawn').". ";
                //Send Push Notifications
                $api_key = $this->config->item('delivererAppGoogleKey');
                $device_details_provider = $this->api->get_user_device_details((int)$job_details['provider_id']);
                if(!is_null($device_details_provider)) {
                  $arr_provider_fcm_ids = array();
                  $arr_provider_apn_ids = array();
                  foreach ($device_details_provider as $value) {
                    if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                    else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                  }
                  $msg = array('title' => $this->lang->line('Job dispute withdrawn'), 'type' => 'job-dispute-withdrawn', 'notice_date' => $today, 'desc' => $message);
                  $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                  //APN
                  $msg_apn_provider =  array('title' => $this->lang->line('Job dispute withdrawn'), 'text' => $message);
                  if(is_array($arr_provider_apn_ids)) { 
                    $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                    $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                  }
                }
                //SMS Notification
                $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
                $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

                //Email Notification
                $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Job dispute withdrawn'), trim($message));
              //------------------------------------------
              
              //update to workroom
              $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Job dispute has been withdrawn');
              if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
                $update_data = array(
                  'workstream_id' => (int)$workstream_details['workstream_id'],
                  'job_id' => (int)$job_id,
                  'cust_id' => (int)$job_details['cust_id'],
                  'provider_id' => (int)$job_details['provider_id'],
                  'text_msg' => $message,
                  'attachment_url' =>  'NULL',
                  'cre_datetime' => $today,
                  'sender_id' => $cust_id,
                  'type' => 'dispute_withdraw',
                  'file_type' => 'text',
                  'proposal_id' => 0,
                );
                $this->api->update_to_workstream_details($update_data);
              }

              $job_details = $this->api->get_job_details($job_id);
              $this->response = ['response' => 'success', 'message' => $this->lang->line('Job dispute withdraw successfully.'), "job_details" => $job_details];
            } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('Unable to withdraw job dispute. Try again...'), "job_details" => array()]; } 
          } else {  $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are not allowed to withdraw dispute, already taken by Gonagoo admin'), "job_details" => array()]; }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('You are not allowed to withdraw dispute, already taken by Gonagoo admin'), "job_details" => array()]; }
        echo json_encode($this->response);
      }
    }
  //End Provider Jobs--------------------------------------

  //Start DB Flush-----------------------------------------
    public function services_job_cleanup() {
      if($this->api->services_job_cleanup()) {
        $this->response = ['response' => 'success','message'=> $this->lang->line('delete_success')];
      } else{ $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')]; }
      echo json_encode($this->response);
    }
  //End DB Flush-------------------------------------------

  //zaid
  //Start Service Customer post job------------------------
    public function get_post_customer_job_details()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
        $category_types = $this->api->get_category_types();
        $countries = $this->api->get_countries();
        $user_details = $this->api->get_user_details($cust_id);
        $currency_details = $this->api->get_country_currency_detail($user_details['country_id']);

        if($category_types &&  $countries && $user_details && $currency_details) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('Get details success'), "category_types" => $category_types, "countries" => $countries, "user_details" => $user_details,"currency_details" => $currency_details ];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Get details failed'), "category_types" => array(), "countries" => array(), "user_details" => array(),"currency_details" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function get_sub_category()
    {
      if(empty($this->errors)) { 
        $cat_type_id = $this->input->post('cat_type_id', TRUE);
        if($sub_category = $this->api->get_category_by_id($cat_type_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "sub_category" => $sub_category];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "sub_category" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function get_sub_category_details()
    {
      if(empty($this->errors)) { 
        $cat_id = $this->input->post('cat_id', TRUE);
        if($sub_category = $this->api->get_category_details_by_id($cat_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "sub_category" => $sub_category];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; }
        echo json_encode($this->response);
      }
    }
    public function get_questions_know_freelancer()
    {
      if(empty($this->errors)) { 
        $cat_type_id = $this->input->post('cat_type_id', TRUE);
        if($questions = $this->api->get_questions_know_freelancer($cat_type_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "questions" => $questions];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "questions" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function register_customer_job()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE);
        $customer_details = $this->api->get_user_details($cust_id);
        $job_title = $this->input->post('job_title', TRUE);
        $cat_id = $this->input->post('category', TRUE);
        $sub_cat_id = $this->input->post('sub_category', TRUE);
        $job_desc = $this->input->post('description', TRUE);
        $currency_code = $this->input->post('currency_sign', TRUE);
        $currency_id = $this->input->post('currency_id', TRUE);
        $work_type = $this->input->post('work_type', TRUE);
        $budget = $this->input->post('budget', TRUE);
        $location_type = $this->input->post('work_from', TRUE);
        $address = $this->input->post('city_name', TRUE);
        $starting_date = $this->input->post('starting_date', TRUE);
        $starting_time = $this->input->post('starting_time', TRUE);
        $hours = $this->input->post('hours', TRUE);
        $hours_per_duration = $this->input->post('hours_per', TRUE);
        $start_datetime = $starting_date." ".$starting_time;
        $country_id = $this->input->post('country_id', TRUE);
        $today = date('Y-m-d h:i:s');
        if($country_id == "NULL"){
          $country_name = $this->input->post('country_location', TRUE);
          if($country_name == "NULL"){
            $country_id = $customer_details['country_id'];
          }else{
            $country_id = $this->api->get_country_id_by_name($country_name);
          }
        }
        $visibility = $this->input->post('visibility', TRUE);
        $question_1 = $this->input->post('question1', TRUE);
        if($question_1 == "Create your own question"){
          $question_1 = $this->input->post('question1_other', TRUE);
        }
        $question_2 = $this->input->post('question2', TRUE);
        if($question_2 == "Create your own question"){
          $question_2 = $this->input->post('question2_other', TRUE);
        }
        $question_3 = $this->input->post('question3', TRUE);
        if($question_3 == "Create your own question"){
          $question_3 = $this->input->post('question3_other', TRUE);
        }
        $question_4 = $this->input->post('question4', TRUE);
        if($question_4 == "Create your own question"){
          $question_4 = $this->input->post('question4_other', TRUE);
        }
        $question_5 = $this->input->post('question5', TRUE);
        if($question_5 == "Create your own question"){
          $question_5 = $this->input->post('question5_other', TRUE);
        }
        $project_duration = $this->input->post('project_duration', TRUE);

        $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']);
      
        if(!empty($_FILES["project_file1"]["tmp_name"]) )
        {
          $config['upload_path']    = 'resources/m4-image/jobs/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('project_file1')) { 
            $uploads    = $this->upload->data();  
            $project_url1 =  $config['upload_path'].$uploads["file_name"];  
          }
        }else{
          $project_url1 = "NULL";
        }

        if(!empty($_FILES["project_file2"]["tmp_name"]) )
        {
          $config['upload_path']    = 'resources/m4-image/jobs/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('project_file2')) { 
            $uploads    = $this->upload->data();  
            $project_url2 =  $config['upload_path'].$uploads["file_name"];  
          }
        }else{
          $project_url2 = "NULL";
        }

        if(!empty($_FILES["project_file3"]["tmp_name"]) )
        {
          $config['upload_path']    = 'resources/m4-image/jobs/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('project_file3')) { 
            $uploads    = $this->upload->data();  
            $project_url3 =  $config['upload_path'].$uploads["file_name"];  
          }
        }else{
          $project_url3 = "NULL";
        }

        if(!empty($_FILES["project_file4"]["tmp_name"]) )
        {
          $config['upload_path']    = 'resources/m4-image/jobs/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('project_file4')) { 
            $uploads    = $this->upload->data();  
            $project_url4 =  $config['upload_path'].$uploads["file_name"];  
          }
        }else{
          $project_url4 = "NULL";
        }

        if(!empty($_FILES["project_file5"]["tmp_name"]) )
        {
          $config['upload_path']    = 'resources/m4-image/jobs/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('project_file5')) { 
            $uploads    = $this->upload->data();  
            $project_url5 =  $config['upload_path'].$uploads["file_name"];  
          }
        }else{
          $project_url5 = "NULL";
        }

        if(!empty($_FILES["project_file6"]["tmp_name"]) )
        {
          $config['upload_path']    = 'resources/m4-image/jobs/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('project_file6')) { 
            $uploads    = $this->upload->data();  
            $project_url6 =  $config['upload_path'].$uploads["file_name"];  
          }
        }else{
          $project_url6 = "NULL";
        }
        $insert_data = array(
          "job_title" => trim($job_title),
          "cust_id" => trim($cust_id),
          "cat_id" => trim($cat_id),
          "sub_cat_id" => trim($sub_cat_id),
          "job_desc" => trim($job_desc),
          "attachment_1" => $project_url1,
          "attachment_2" => $project_url2,
          "attachment_3" => $project_url3,
          "attachment_4" => $project_url4,
          "attachment_5" => $project_url5,
          "attachment_6" => $project_url6,
          "work_type" => trim($work_type),
          "currency_id" => trim($currency_id),
          "currency_code" => trim($currency_code),
          "budget" => trim($budget),
          "location_type" => trim($location_type),
          "address" => trim($address),
          "start_datetime" => trim($start_datetime),
          "hours" => trim($hours),
          "hours_per_duration" => trim($hours_per_duration),
          "visibility" => trim($visibility),
          "project_duration" => trim($project_duration),
          "question_1" => trim($question_1),
          "question_2" => trim($question_2),
          "question_3" => trim($question_3),
          "question_4" => trim($question_4),
          "question_5" => trim($question_5),
          "country_id" => trim($country_id),
          "cre_datetime" => trim($today),
          "cust_name" => $customer_name,
        "cust_contact" => $customer_details['mobile1'],
        "cust_email" => $customer_details['email1'],
        );
        if($job_id = $this->api->register_job_details($insert_data)) {
          //------------------------------------------------
          $job_details = $this->api->get_customer_job_details($job_id);
          $customer_details = $this->api->get_user_details($cust_id);
        
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your job posted').": ".$job_id;
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Job post'), 'type' => 'job post', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Job post'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
          //Notifications to customers----------------

          //Update to workstream master
          $update_data = array(
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => 0,
            'cre_datetime' => $today,
            'status' => 1,
          );
          if($id = $this->api->check_workstream_master($update_data)){
            $workstream_id = $id;
          }else{
            $workstream_id = $this->api->update_to_workstream_master($update_data);
          }
          $update_data = array(
            'workstream_id' => (int)$workstream_id,
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => 0,
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'job_started',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('job post successfully'), "job_details" => $job_details];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('job post failed'), "job_details" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function list_of_freelancers()
    {
      if(empty($this->errors)) { 
        $job_id = $this->input->post('job_id', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE);
        $job_details = $this->api->get_job_details($job_id);
        $skill = $this->api->get_category_details_by_id($job_details['sub_cat_id']);
        $country = $this->api->get_countries();
        $data = array(
          'skill' => $skill['cat_name'] , 
          'fav' => "no",
          'country_id' => "NULL",
          'state_id' => "NULL",
          'city_id' => "NULL",
          'rate_from' => "NULL",
          'rate_to' => "NULL",
          'cust_id' => $cust_id,
        );
        if($freelancers = $this->api->list_of_freelancers($data)) {
          for($i=0; $i<sizeof($freelancers); $i++) {
            $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
            $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
            $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
            if($completed['total_job'] != "0"){
             $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
            }else{$percent = 0;}  
            $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
            $freelancers[$i]['job_percent'] = $percent;
            $freelancers[$i]['portfolio'] = (int)$portfolio;
            $freelancers[$i]['project_5'] = "yes";
          }
          //echo json_encode($freelancers); die();
          $favorite_freelancer = $this->api->get_favorite_freelancer($cust_id);
          $fav_freelancer = array();
          foreach ($favorite_freelancer as $fav_key) {
            array_push($fav_freelancer, $fav_key['provider_id']);
          }


          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "freelancers" => $freelancers , "country" => $country , "fav_freelancer" => $fav_freelancer ,"job_details" => $job_details ];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "freelancers" =>  array(), "country" => array() , "fav_freelancer" => array() ,"job_details" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function filter_list_of_freelancers()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE);
        $job_id = $this->input->post('job_id');
        $country_id = $this->input->post('country_id');
        $state_id = $this->input->post('state_id');
        $city_id = $this->input->post('city_id');
        $project_5 = $this->input->post('project_5');
        $project_50 = $this->input->post('project_50');
        $rate_from = $this->input->post('rate_from');
        $rate_to = $this->input->post('rate_to');
        $country = $this->api->get_countries();
        $states = $this->api->get_state_by_country_id($country_id);
        $cities = $this->api->get_city_by_state_id($state_id);
        // echo json_encode($states); die();
        $job_details = $this->api->get_job_details($job_id);
        $skill = $this->api->get_category_details_by_id($job_details['sub_cat_id']);
        $data = array(
          'skill' => $skill['cat_name'] ,
          'fav' => "no",
          'country_id' => $country_id,
          'state_id' => $state_id,
          'city_id' => $city_id,
          'rate_from' => (int)$rate_from,
          'rate_to' => (int)$rate_to,
          'cust_id' => $cust_id
        );
        if($freelancers = $this->api->list_of_freelancers($data)) {
          for($i=0; $i<sizeof($freelancers); $i++) {
            $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
            $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
            $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
            if($completed['total_job'] != "0"){
             $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
            }else{$percent = 0;}  
            $freelancers[$i]['completed_job'] = (int)$completed['total_job'];
            $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
            $freelancers[$i]['job_percent'] = $percent;
            $freelancers[$i]['portfolio'] = (int)$portfolio;
            $freelancers[$i]['project_5'] = "yes";
            if($project_5 != "NULL"){
              if((int)$project_5 < (int)$freelancers[$i]['completed_job']){
                $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
              }
            }
            if($project_50 != "NULL"){
              if((int)$project_50 < (int)$freelancers[$i]['completed_job']){
                $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
              }
            }
          }//for
          //echo json_encode($freelancers); die();
          $favorite_freelancer = $this->api->get_favorite_freelancer($cust_id);
          $fav_freelancer = array();
          foreach ($favorite_freelancer as $fav_key) {
            array_push($fav_freelancer, $fav_key['provider_id']);
          }

          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "freelancers" => $freelancers , "country" => $country , "fav_freelancer" => $fav_freelancer ,"job_details" => $job_details, "country_id" => $country_id , "city_id" => $city_id ,"state_id" => $state_id , "states" => $states, "cities" => $cities, "project_5" => $project_5, "project_50" => $project_50, "rate_from" => $rate_from, "rate_to" => $rate_to ];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "freelancers" =>  array(), "country" => array() , "fav_freelancer" => array() ,"job_details" => array(), "country_id" => $country_id , "city_id" => $city_id ,"state_id" => $state_id , "states" => array(), "cities" => array(), "project_5" => $project_5, "project_50" => $project_50, "rate_from" => $rate_from, "rate_to" => $rate_to ]; }
        echo json_encode($this->response);
      }
    }
    public function list_of_fav_freelancers()
    {
      if(empty($this->errors)) { 
        $job_id = $this->input->post('job_id');
        $cust_id = $this->input->post('cust_id');
        $job_details = $this->api->get_job_details($job_id);
        $country = $this->api->get_countries();
        // $skill = $this->api->get_category_details_by_id($job_details['sub_cat_id']);
        
        $data = array(
          'fav' => "yes" , 
          'skill' => "NULL",
          'country_id' => "NULL",
          'state_id' => "NULL",
          'city_id' => "NULL",
          'rate_from' => "NULL",
          'rate_to' => "NULL"
        );
        if($freelancers = $this->api->list_of_freelancers($data)) {
          for($i=0; $i<sizeof($freelancers); $i++) {
            $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
            $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
            $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
            if($completed['total_job'] != "0"){
             $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
            }else{$percent = 0;}  
            $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
            $freelancers[$i]['job_percent'] = $percent;
            $freelancers[$i]['portfolio'] = (int)$portfolio;
            $freelancers[$i]['project_5'] = "yes"; 
          }
          //echo json_encode($freelancers); die();
          $favorite_freelancer = $this->api->get_favorite_freelancer($cust_id);
          $fav_freelancer = array();
          foreach ($favorite_freelancer as $fav_key) {
            array_push($fav_freelancer, $fav_key['provider_id']);
          }

          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success') , "country" => $country, "fav_freelancer" => $fav_freelancer, "freelancers" => $freelancers, "job_details" => $job_details];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "country" => array(), "fav_freelancer" => array(), "freelancers" => array(), "job_details" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function make_freelancer_favourite()
    {
      if(empty($this->errors)) { 
        $provider_id = $this->input->post('provider_id', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE);
        if(!$this->api->check_favourite_freelancer($cust_id, $provider_id)) {
          if($this->api->register_new_favourite_freelancer($cust_id, $provider_id)) {
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('make freelancers favorite successfully')];
          } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('make freelancers favorite failed')]; }
        }else{
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('Already added into favorite freelancers')];
        }
        echo json_encode($this->response);
      }
    }
    public function freelancer_remove_from_favorite()
    {
      if(empty($this->errors)) { 
        $provider_id = $this->input->post('provider_id', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE);
        if($this->api->remove_favourite_freelancer($cust_id, $provider_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('remove freelancers favorite successfully')];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('remove freelancers favorite failed')]; }
        echo json_encode($this->response);
      }
    }
    public function invite_freelancers_for_job()
    {
      if(empty($this->errors)) { 
        $provider_id = $this->input->post('provider_id');
        $job_id = $this->input->post('job_id');
        $cust_id = $this->input->post('cust_id');
        $today = date('Y-m-d h:i:s');
        if($job_details = $this->api->get_job_details($job_id)){
          $insert_data  = array(
            "job_id" => $job_id,
            "cust_id" => $cust_id,
            "provider_id" => $provider_id,
            "invite_datetime" => $today,
            "invite_status" => "open",
          );

          $message = $this->lang->line('Job invitation')." - ".$job_details['job_title'];
          $update_data = array(
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => $provider_id,
            'cre_datetime' => $today,
            'status' => 1,
          );
          if(!$this->api->check_customer_invite_job($insert_data)){
            if($workstream_id = $this->api->update_to_workstream_master($update_data)) {
              //Update to workroom
              $update_data = array(
                'workstream_id' => (int)$workstream_id,
                'job_id' => (int)$job_id,
                'cust_id' => $cust_id,
                'provider_id' => $provider_id,
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $cust_id,
                'type' => 'job_invite',
                'file_type' => 'text',
                'proposal_id' => 0,
              );
              $this->api->update_to_workstream_details($update_data);
            }  
            if($this->api->register_customer_invite_job($insert_data))
            {

              $job_details = $this->api->get_job_details($job_id);
              $provider_details = $this->api->get_user_details($provider_id);
            
              if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
                $provider_name = explode("@", trim($provider_details['email1']))[0];
              } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

              //Notifications to customers----------------
                $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('You are invited to this job. Job ID').": ".$job_id;
                //Send Push Notifications
                $api_key = $this->config->item('delivererAppGoogleKey');
                $device_details_provider = $this->api->get_user_device_details($provider_id);
                if(!is_null($device_details_provider)) {
                  $arr_provider_fcm_ids = array();
                  $arr_provider_apn_ids = array();
                  foreach ($device_details_provider as $value) {
                    if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                    else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                  }
                  $msg = array('title' => $this->lang->line('invitation received'), 'type' => 'job invitation', 'notice_date' => $today, 'desc' => $message);
                  $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                  //APN
                  $msg_apn_provider =  array('title' => $this->lang->line('invitation received'), 'text' => $message);
                  if(is_array($arr_provider_apn_ids)) { 
                    $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                    $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                  }
                }
                //SMS Notification
                $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
                $this->api_sms->sendSMS((int)$country_code.trim($provider_details['mobile1']), $message);

                //Email Notification
                $this->api_sms->send_email_text($provider_name, trim($provider_details['email1']), $this->lang->line('invitation received'), trim($message));
              //Notifications to customers----------------


              $this->response = ['response' => 'success', 'message' => $this->lang->line('Invitation send successfully')];
            }else{ 
              $this->response = ['response' => 'failed', 'message' => $this->lang->line('Invitation send failed')]; 
            }
          }else{
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('Already invited for this job')];
          }
        }else{
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('Job not found')];
        }
        echo json_encode($this->response);
      }
    }
    public function invite_all_freelancers_for_job()
    {
      if(empty($this->errors)) { 
        
        $job_id = $this->input->post('job_id');
        $cust_id = $this->input->post('cust_id');
        $job_details = $this->api->get_job_details($job_id);
        $today = date('Y-m-d h:i:s');
        $skill = $this->api->get_category_details_by_id($job_details['sub_cat_id']);
        $data = array(
          'cust_id' => $cust_id,
          'skill' => $skill['cat_name'] ,
          'fav' => "no" , 
          'country_id' => "NULL",
          'state_id' => "NULL",
          'city_id' => "NULL",
          'rate_from' => "NULL",
          'rate_to' => "NULL" 
        );
        if($freelancers = $this->api->list_of_freelancers($data)){
          foreach ($freelancers as $fc)
          {
            $insert_data  = array(
              "job_id" => $job_id,
              "cust_id" => $cust_id,
              "provider_id" => $fc['cust_id'],
              "invite_datetime" => $today,
              "invite_status" => "open",
            );
            $message = $this->lang->line('Job invitation')." - ".$job_details['job_title'];
            $update_data = array(
              'job_id' => (int)$job_id,
              'cust_id' => $cust_id,
              'provider_id' => $fc['cust_id'],
              'cre_datetime' => $today,
              'status' => 1,
            );
            if(!$this->api->check_customer_invite_job($insert_data)){
              if($workstream_id = $this->api->update_to_workstream_master($update_data)) {
                //Update to workroom
                $update_data = array(
                  'workstream_id' => (int)$workstream_id,
                  'job_id' => (int)$job_id,
                  'cust_id' => $cust_id,
                  'provider_id' => $fc['cust_id'],
                  'text_msg' => $message,
                  'attachment_url' =>  'NULL',
                  'cre_datetime' => $today,
                  'sender_id' => $cust_id,
                  'type' => 'job_invite',
                  'file_type' => 'text',
                  'proposal_id' => 0,
                );
                $this->api->update_to_workstream_details($update_data);
              }
              if($this->api->register_customer_invite_job($insert_data))
              {
                $job_details = $this->api->get_job_details($job_id);
                $provider_details = $this->api->get_user_details($fc['cust_id']);
              
                if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
                  $provider_name = explode("@", trim($provider_details['email1']))[0];
                } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

                //Notifications to customers----------------
                  $message = $this->lang->line('Hello')." ".$provider_name.", ".$this->lang->line('You are invited to this job. Job ID').": ".$job_id;
                  //Send Push Notifications
                  $api_key = $this->config->item('delivererAppGoogleKey');
                  $device_details_provider = $this->api->get_user_device_details($fc['cust_id']);
                  if(!is_null($device_details_provider)) {
                    $arr_provider_fcm_ids = array();
                    $arr_provider_apn_ids = array();
                    foreach ($device_details_provider as $value) {
                      if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
                      else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
                    }
                    $msg = array('title' => $this->lang->line('invitation received'), 'type' => 'job invitation', 'notice_date' => $today, 'desc' => $message);
                    $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
                    //APN
                    $msg_apn_provider =  array('title' => $this->lang->line('invitation received'), 'text' => $message);
                    if(is_array($arr_provider_apn_ids)) { 
                      $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
                      $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
                    }
                  }
                  //SMS Notification
                  $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
                  $this->api_sms->sendSMS((int)$country_code.trim($provider_details['mobile1']), $message);

                  //Email Notification
                  $this->api_sms->send_email_text($provider_name, trim($provider_details['email1']), $this->lang->line('invitation received'), trim($message));
                //Notifications to customers----------------


              }
            }
          }
          $this->response = ['response' => 'success', 'message' => $this->lang->line('Invitation send successfully')];
        }else{
          $this->response = ['response' => 'failed', 'message'=> $this->lang->line('Freelancers not found')];
        }
        echo json_encode($this->response);
      }
    }
    public function get_state_by_country_id()
    {
      if(empty($this->errors)) { 
        $country_id = $this->input->post('country_id');
        if($states = $this->api->get_state_by_country_id($country_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "states" => $states];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "states" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function get_city_by_state_id()
    {
      if(empty($this->errors)) { 
        $state_id = $this->input->post('state_id');
        if($cities = $this->api->get_city_by_state_id($state_id)) {
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "cities" => $cities];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'), "cities" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function job_workstream_list_old()
    {
      if(empty($this->errors)) { 
        $job_id = $this->input->post('job_id');
        $cust_id = $this->input->post('cust_id');
        if(isset($_POST['proposal_status'])){
          $proposal_status = $this->input->post('proposal_status');
        }else{$proposal_status = "All";}
        if(isset($_POST['ratings'])){
          $ratings = $this->input->post('ratings');
        }else{$ratings = "0";}
        if(isset($_POST['country_id'])){
          $country_id = $this->input->post('country_id');
        }else{$country_id = "0";}
        $insert_data = array(
          'job_id' => $job_id,
          'country_id' => $country_id,
          'ratings' => $ratings
        );

        if($freelancers=$this->api->list_of_freelancers_invited_proposals($insert_data)){
          $job_details = $this->api->get_job_details($job_id);
          $favorite_freelancer = $this->api->get_favorite_freelancer($cust_id);
          $decline_reason = $this->api->job_decline_reason_master();      
          $fav_freelancer = array();
          $freelancer_country_id = array();
          $freelancer_country = array();
          foreach ($favorite_freelancer as $fav_key) {
            array_push($fav_freelancer, $fav_key['provider_id']);
          }
          for($i=0; $i<sizeof($freelancers); $i++){
            $freelancers[$i]['profile_status'] = "safe";
            $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
            $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
            $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
            if(!in_array($freelancers[$i]['country_id'], $freelancer_country_id))
            {array_push($freelancer_country_id ,$freelancers[$i]['country_id']);}

            if($completed['total_job'] != "0"){
             $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
            }else{$percent = 0;}
            $insert_data = array(
              'cust_id' => $job_details['cust_id'],
              'provider_id' => $freelancers[$i]['cust_id'],
              'job_id' => $job_id,
            );
            if($this->api->check_job_proposals($insert_data)){
              $data = array(
                'job_id' => $job_id,
                'provider_id' => $freelancers[$i]['cust_id'], 
                'decline' => 'no'
              );
              $proposal = $this->api->get_job_proposals($data);
              $freelancers[$i]['proposal_details'] = $proposal;
              $freelancers[$i]['proposal'] = "yes";
            }else{
              $freelancers[$i]['proposal'] = "no";
            }
            if($proposal_status == "decline"){
              $data = array(
                'job_id' => $job_id,
                'provider_id' => $freelancers[$i]['cust_id'], 
                'decline' => 'yes'
              );
              if($this->api->get_job_proposals($data)){
                $freelancers[$i]['profile_status'] = "safe";
              }else{
                $freelancers[$i]['profile_status'] = "danger";
              }
            }
            if($proposal_status == "pending"){
              $data = array(
                'job_id' => $job_id,
                'provider_id' => $freelancers[$i]['cust_id'], 
                'decline' => 'yes'
              );
              if($this->api->get_job_proposals($data)){
                $freelancers[$i]['profile_status'] = "danger";
              }else{
                $freelancers[$i]['profile_status'] = "safe";
              }
            }
            $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
            $freelancers[$i]['job_percent'] = $percent;
            $freelancers[$i]['portfolio'] = (int)$portfolio;
          }
          foreach ($freelancer_country_id as $id){
            $country = $this->api->get_country_details($id);
            array_push($freelancer_country ,$country);
          }
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success') ,"proposal_status" => $proposal_status, "ratings" => $ratings, "country_id" => $country_id, "freelancer_country" => $freelancer_country, "fav_freelancer" => $fav_freelancer, "freelancers" => $freelancers, "job_details" => $job_details, "decline_reason" => $decline_reason];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'),"proposal_status" => $proposal_status, "ratings" => $ratings, "country_id" => $country_id, "freelancer_country" => array(), "fav_freelancer" => array(), "freelancers" => array(), "job_details" => $job_details, "decline_reason" => $decline_reason]; }
        echo json_encode($this->response);
      }
    }
    public function search_jobs()
    {
      if(empty($this->errors)) { 
        $cust_id = $this->input->post('cust_id', TRUE);
        if($jobs = $this->api->get_job_list(array())) {
          $category_types = $this->api->get_category_types();
          $fav_jobs = $this->api->get_favorite_jobs($cust_id);
          $fav_projects = array();
          foreach ($fav_jobs as $fav_key) {
            array_push($fav_projects, $fav_key['job_id']);
          }
          for($i=0; $i<sizeof($jobs); $i++) {
            $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
            $jobs[$i]['proposal_counts'] = sizeof($proposals);
          }
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('getting jobs list successfully') , "jobs" => $jobs, "fav_projects" => $fav_projects, "category_types" => $category_types];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('job list getting failed'), "jobs" => array(), "fav_projects" => array(), "category_types" => array()]; }
        echo json_encode($this->response);
      }
    }
    public function my_favorite_job()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      if($jobs = $this->api->get_my_favorite_jobs($cust_id))
      {
        $fav_jobs = $this->api->get_favorite_jobs($cust_id);
        $fav_projects = array();
        foreach ($fav_jobs as $fav_key) {
          array_push($fav_projects, $fav_key['job_id']);
        }
        for($i=0; $i<sizeof($jobs); $i++) {
          $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
          $jobs[$i]['proposal_counts'] = sizeof($proposals);
        }
        $projects=array(
          'jobs'=>$jobs,
          'fav'=> $fav_projects
        );
        $this->response = array('response' => 'success','message'=> $this->lang->line('get_list_success'),
          'list_project'=> $projects,
        );
      } else { 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('job list getting failed')]; 
      }
        echo json_encode($this->response);
    }
    public function make_job_favourite()
    {
      if(empty($this->errors)) { 
        $job_id = $this->input->post('job_id', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE);
        if($job_details = $this->api->get_job_details($job_id)) {
          if($this->api->register_new_favourite_jobs($job_id, $job_details['cust_id'], $cust_id)){
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('job added to favorite successfully')];
          }else{
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('add_failed')]; 
          }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('job not found')];
        }
        echo json_encode($this->response);
      }
    }
    public function job_remove_from_favorite()
    {
      if(empty($this->errors)) { 
        $job_id = $this->input->post('job_id', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE);
        if($job_details = $this->api->get_job_details($job_id)) {
          if($this->api->remove_favourite_job($job_id, $job_details['cust_id'], $cust_id)){
            $this->response = ['response' => 'success', 'message'=> $this->lang->line('job remove to favorite successfully')];
          }else{
            $this->response = ['response'=>'failed','message' => $this->lang->line('remove_failed')]; 
          }
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('job not found')];
        }
        echo json_encode($this->response);
      }
    }
    public function send_proposal_view_details()
    {
      if(empty($this->errors)) { 
        $job_id = $this->input->post('job_id', TRUE);
        $cust_id = $this->input->post('cust_id', TRUE);
        if($job_details = $this->api->get_job_details($job_id)) {
          $country_currency = $this->api->get_country_id_by_currency_id($job_details['currency_id']);
          $payment_percent = $this->api->get_job_advance_payment_percent($country_currency['country_id'] , $job_details['cat_id'] , $job_details['sub_cat_id']);
          $job_customer_profile = $this->api->get_user_details($job_details['cust_id']);
          $freelancers_profile = $this->api->get_service_provider_profile($cust_id);
          $datetime1 = new DateTime(date('Y-m-d h:i:s'));
          $datetime2 = new DateTime($job_details['cre_datetime']);
          $difference = $datetime1->diff($datetime2);
          $ending = 30-$difference->d;
          $last_job = $this->api->get_last_job_of_customer($job_customer_profile['cust_id'], 'last');
          $completed_job = $this->api->get_last_job_of_customer($job_customer_profile['cust_id'] , 'completed_count');
          $fav_jobs = $this->api->get_favorite_jobs($cust_id);
          $fav_projects = array();
          foreach ($fav_jobs as $fav_key) {
            array_push($fav_projects, $fav_key['job_id']);
          }
          $proposals = $this->api->get_job_proposals_list($job_details['job_id']);
          for($i=0; $i<sizeof($proposals); $i++){
            $provider_details = $this->api->get_user_details($proposals[$i]['provider_id']);
            $proposals[$i]['provider_details'] = $provider_details;
          }
          $job_details['proposal_counts'] = sizeof($proposals);
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success'), "job_details" => $job_details, "fav_projects" => $fav_projects, "job_customer_profile" => $job_customer_profile, "ending" => $ending, "last_job" => $last_job, "completed_job" => $completed_job, "proposals" => $proposals, "freelancers_profile" => $freelancers_profile, "payment_percent" => $payment_percent];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('Job not found')]; }
        echo json_encode($this->response);
      }
    }
  //End Service Customer post job--------------------------

  ///////Maaz/////// New Add from  gonagoo m4 module
    public function job_workstream_list()
    {
      if(empty($this->errors)) { 
        $job_id = $this->input->post('job_id');
        $cust_id = $this->input->post('cust_id');
        if(isset($_POST['proposal_status'])){
          $proposal_status = $this->input->post('proposal_status');
        }else{$proposal_status = "All";}
        if(isset($_POST['ratings'])){
          $ratings = $this->input->post('ratings');
        }else{$ratings = "0";}
        if(isset($_POST['country_id'])){
          $country_id = $this->input->post('country_id');
        }else{$country_id = "0";}
        $insert_data = array(
          'job_id' => $job_id,
          'country_id' => $country_id,
          'ratings' => $ratings
        );

        if($freelancers=$this->api->list_of_freelancers_invited_proposals($insert_data)){
          $job_details = $this->api->get_job_details($job_id);
          $favorite_freelancer = $this->api->get_favorite_freelancer($cust_id);
          $decline_reason = $this->api->job_decline_reason_master();      
          $fav_freelancer = array();
          $freelancer_country_id = array();
          $freelancer_country = array();
          foreach ($favorite_freelancer as $fav_key) {
            array_push($fav_freelancer, $fav_key['provider_id']);
          }
          for($i=0; $i<sizeof($freelancers); $i++){
            $freelancers[$i]['profile_status'] = "safe";
            $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
            $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
            $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
            if(!in_array($freelancers[$i]['country_id'], $freelancer_country_id))
            {array_push($freelancer_country_id ,$freelancers[$i]['country_id']);}

            if($completed['total_job'] != "0"){
             $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
            }else{$percent = 0;}
            $insert_data = array(
              'cust_id' => $job_details['cust_id'],
              'provider_id' => $freelancers[$i]['cust_id'],
              'job_id' => $job_id,
            );
            if($this->api->check_job_proposals($insert_data)){
              $data = array(
                'job_id' => $job_id,
                'provider_id' => $freelancers[$i]['cust_id'], 
                'decline' => 'no'
              );
              $proposal = $this->api->get_job_proposals($data);
              $freelancers[$i]['proposal_details'] = $proposal;
              $freelancers[$i]['proposal'] = "yes";
            }else{
              $freelancers[$i]['proposal'] = "no";
            }
            if($proposal_status == "decline"){
              $data = array(
                'job_id' => $job_id,
                'provider_id' => $freelancers[$i]['cust_id'], 
                'decline' => 'yes'
              );
              if($this->api->get_job_proposals($data)){
                $freelancers[$i]['profile_status'] = "safe";
              }else{
                $freelancers[$i]['profile_status'] = "danger";
              }
            }
            if($proposal_status == "pending"){
              $data = array(
                'job_id' => $job_id,
                'provider_id' => $freelancers[$i]['cust_id'], 
                'decline' => 'yes'
              );
              if($this->api->get_job_proposals($data)){
                $freelancers[$i]['profile_status'] = "danger";
              }else{
                $freelancers[$i]['profile_status'] = "safe";
              }
            }
            $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
            $freelancers[$i]['job_percent'] = $percent;
            $freelancers[$i]['portfolio'] = (int)$portfolio;
          }
          foreach ($freelancer_country_id as $id){
            $country = $this->api->get_country_details($id);
            array_push($freelancer_country ,$country);
          }
          $this->response = ['response' => 'success', 'message'=> $this->lang->line('get_list_success') ,"proposal_status" => $proposal_status, "ratings" => $ratings, "country_id" => $country_id, "freelancer_country" => $freelancer_country, "fav_freelancer" => $fav_freelancer, "freelancers" => $freelancers, "job_details" => $job_details, "decline_reason" => $decline_reason];
        } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed'),"proposal_status" => $proposal_status, "ratings" => $ratings, "country_id" => $country_id, "freelancer_country" => array(), "fav_freelancer" => array(), "freelancers" => array(), "job_details" => array(), "decline_reason" => array()]; }
        echo json_encode($this->response);
      }
    }

    //maaz
    public function register_troubleshoot_job()
    {
      // echo json_encode($_POST);die();
      $cust_id =$this->input->post('cust_id', TRUE);
      $job_title = $this->input->post('job_title', TRUE);
      $cat_id = $this->input->post('category', TRUE);
      $sub_cat_id = $this->input->post('sub_category', TRUE);
      $job_desc = $this->input->post('description', TRUE);
      $address = $this->input->post('toMapID', TRUE);

      $immdediate = $this->input->post('immdediate', TRUE);
      $complete_date = $this->input->post('start_date', TRUE);

      $customer_details = $this->api->get_user_details($cust_id);
      $currency_details = $this->api->get_country_currency_detail($customer_details['country_id']);
      $country_name = $this->input->post('country', TRUE);
      $country_id = $this->api->get_country_id_by_name($country_name);
      $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']);

      $size = $this->input->post('size', TRUE);
      $questions=array();
      $answers=array(); 
      for ($i=1; $i <=$size ; $i++) {
        array_push($questions ,$this->input->post('ques_'.$i, TRUE));
        array_push($answers ,$this->input->post('ans_'.$i, TRUE));
      }     
      $today = date('Y-m-d h:i:s');
      $insert_data = array(
        "job_title" => trim($job_title),
        "cust_id" => trim($cust_id),
        "cat_id" => trim($cat_id),
        "sub_cat_id" => trim($sub_cat_id),
        "job_desc" => trim($job_desc),
        "address" => trim($address),
        "country_id" => trim($country_id),
        "cre_datetime" => trim($today),
        "cust_name" => $customer_name,
        "cust_contact" => $customer_details['mobile1'],
        "cust_email" => $customer_details['email1'],
        "job_type"=>1,
        "immediate" => $immdediate,
        "location_type"=>"Onsite",
        "visibility"=>"public",
        "currency_id"=>$currency_details['currency_id'],
        "currency_code"=>$currency_details['currency_sign'],
      );
      $insert_data['complete_date']=($immdediate)?date("Y-m-d" , strtotime($complete_date)):"";
      // echo json_encode($insert_data);die();
      if($job_id = $this->api->register_job_details($insert_data)){

         $k=0;
          foreach ($answers as $ans) {
            $data=array(
              'question'=>$questions[$k],
              'answer'=>$answers[$k],
              'job_id'=>$job_id
            );
            if($answers[$k]){
              $this->db->insert("tbl_question_answer" , $data);
            // echo json_encode($data);
            } 
            $k++;
          }
          $job_details = $this->api->get_customer_job_details($job_id);
          $customer_details = $this->api->get_user_details($cust_id);
        
          if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
            $customer_name = explode("@", trim($customer_details['email1']))[0];
          } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

          //Notifications to customers----------------
            $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your  Troubleshoot job is posted').": ".$job_id;
            //Send Push Notifications
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($cust_id);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Troubleshoot post'), 'type' => 'troubleshoot post', 'notice_date' => $today, 'desc' => $message);
              $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
              //APN
              $msg_apn_customer =  array('title' => $this->lang->line('Troubleshoot post'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //SMS Notification
            $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
            $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

            //Email Notification
            $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
          //Notifications to customers----------------

          //Update to workstream master
          $update_data = array(
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => 0,
            'cre_datetime' => $today,
            'status' => 1,
          );
          if($id = $this->api->check_workstream_master($update_data)){
            $workstream_id = $id;
          }else{
            $workstream_id = $this->api->update_to_workstream_master($update_data);
          }
          $update_data = array(
            'workstream_id' => (int)$workstream_id,
            'job_id' => (int)$job_id,
            'cust_id' => $cust_id,
            'provider_id' => 0,
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $cust_id,
            'type' => 'job_started',
            'file_type' => 'text',
            'proposal_id' => 0,
          );
          $this->api->update_to_workstream_details($update_data);
         $result=array("response"=>"success" , "message"=> $this->lang->line('Job troubleshoot is succcessfully posted'));
      }else{
         $result=array("response"=>"failed" , "message"=> $this->lang->line('Failed to posted the job troubleshoot'));

      }
      echo json_encode($result);
    }
    public function get_troubleshoot_by_id()
    {
      $job_id=$this->input->post("job_id");
      $job_details = $this->api->get_job_details($job_id);
      $job_details['ques_ans'] = $this->api->get_troubleshoot_ques_ans($job_id);
      if($job_details){
        $result=array("response"=>"success" , "message"=> $this->lang->line('Details found!')  , "list"=>$job_details); 
      }else{
        $result=array("response"=>"failed" , "message"=> $this->lang->line('details not found')); 
      }
      echo json_encode($result);
    }
    public function notification_counts()
    {
      $cust_id = $this->input->post('cust_id'); 
      if( $this->api->is_user_active_or_exists($cust_id)) { 
        $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust_id);
        $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust_id);
        $this->response = array(
          'response' => 'success',
          'message'=> $this->lang->line('get_list_success'),
          'admin_count'=> $total_unread_notifications,
          'workstream_count' => $total_workroom_notifications
        );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
      echo json_encode($this->response);
    }  
    public function get_notifications()
    {
      $cust_id = $this->input->post('cust_id');   
      if( $this->api->is_user_active_or_exists($cust_id)) {
        $cust = $this->api->get_consumer_datails($cust_id,true);
        if($cust['user_type'] == 1) { $type = "Individuals"; } else { $type = "Business"; }     
        $last_id = $this->input->post('last_id');
        $notifications = $this->api->get_notifications($cust_id,$type, 50, $last_id);
        $this->response = array(
          'response' => 'success',
          'message'=> $this->lang->line('get_list_success'),
          'admin_notifications'=> $notifications,
        );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
      echo json_encode($this->response);  
    }  
    public function unread_notifications()
    {
      $cust_id = $this->input->post('cust_id');   
      if( $this->api->is_user_active_or_exists($cust_id)) {
        $notifications = $this->api->get_total_unread_notifications($cust_id,1000);
        $this->response = array(
          'response' => 'success',
          'message'=> $this->lang->line('get_list_success'),
          'admin_notifications'=> $notifications,
        );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
      echo json_encode($this->response);
    }
    public function unread_workroom_notifications()
    {
      $cust_id = $this->input->post('cust_id');   
      if( $this->api->is_user_active_or_exists($cust_id)) {
        $notifications = $this->api->get_total_unread_workroom_notifications($cust_id,1000);
        $this->response = array(
          'response' => 'success',
          'message'=> $this->lang->line('get_list_success'),
          'workroom_notifications'=> $notifications,
        );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
      echo json_encode($this->response);
    }
    public function make_notification_read()
    {
      $cust_id = $this->input->post('cust_id');   
      if( $this->api->is_user_active_or_exists($cust_id)) {
        $notification = $this->api->get_total_unread_notifications($cust_id);
        foreach ($notification as $n) { $this->api->make_notification_read($n['id'], $cust_id); }
        $this->response = array( 'response' => 'success' );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
      echo json_encode($this->response);
    }
    public function make_workroom_notification_read()
    {
      $cust_id = $this->input->post('cust_id');   
      $wsd_id = $this->input->post('wsd_id');   
      if( $this->api->is_user_active_or_exists($cust_id)) {
        $this->api->make_workstream_notification_read($cust_id , $wsd_id);       
        $this->response = array(  'response' => 'success' );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; }
      echo json_encode($this->response);
    }
    public function projects_workroom()
    {
      $cust_id = $this->input->post('cust_id');
      $last_id = $this->input->post('current_page');
      $job_list = $this->api->get_globle_workstream_details((int)$cust_id , $last_id);
      $this->response = array(
        'response' => 'success',
        'message'=> $this->lang->line('get_list_success'),
        'all_workroom'=> $job_list,
      );
      echo json_encode($this->response);
    }
    public function send_proposal()
    {

      $provider_id = $this->input->post('provider_id');
      $today = date('Y-m-d h:i:s');
      $job_id = $this->input->post('job_id');
      $country_id = $this->input->post('country_id');
      $total_price = $this->input->post('total');
      $deposit_price = $this->input->post('deposit');
      $description = $this->input->post('description', TRUE);
      $answer_1 = $this->input->post('answer_1', TRUE);
      $answer_2 = $this->input->post('answer_2', TRUE);
      $answer_3 = $this->input->post('answer_3', TRUE);
      $answer_4 = $this->input->post('answer_4', TRUE);
      $answer_5 = $this->input->post('answer_5', TRUE);
      $answer_5 = $this->input->post('answer_5', TRUE);
      $notif_me = $this->input->post('notif_me', TRUE);

      $job_details = $this->api->get_customer_job_details($job_id);
      $files = $_FILES;

      for($i=0; $i < 5 ; $i++)
      {
        $m=$i+1;

        if(isset($files['image_url'.$m]['name']) && $_FILES['image_url'.$m]['name']!="")
        {
          $_FILES['image_url'.$m]['name']= $files['image_url'.$m]['name'];
          $_FILES['image_url'.$m]['type']= $files['image_url'.$m]['type'];
          $_FILES['image_url'.$m]['tmp_name']= $files['image_url'.$m]['tmp_name'];
          $_FILES['image_url'.$m]['error']= $files['image_url'.$m]['error'];
          $_FILES['image_url'.$m]['size']= $files['image_url'.$m]['size'];

          $config['upload_path']    = 'resources/m4-image/jobs_proposal/';
          $config['allowed_types']  = '*';
          $config['max_size']       = '0';
          $config['max_width']      = '0';
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 

          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('image_url'.$m)) {
            $uploads    = $this->upload->data();
             
            ${"image_url_$m"} =  $config['upload_path'].$uploads['file_name'];
          } else { 
            ${"image_url_$m"} = 'NULL'; 
          }
        } else {
         ${"image_url_$m"} = 'NULL';
          }
      }

      $proposal_breakdown = array();
      
      $j = 1;
      for($i=1; $i<=35; $i++){
        if($this->input->post('price'.$i, TRUE) != "" && $this->input->post('part'.$i, TRUE) != ""){
          $proposal_breakdown['price'.$j] = $this->input->post('price'.$i, TRUE);        
          $proposal_breakdown['part'.$j] = $this->input->post('part'.$i, TRUE);
          $j = $j+1;        
        }
      }
      
      $update_data = array(
        'job_id' => $job_id,
        'cust_id' => $job_details['cust_id'],
        'provider_id' => $provider_id,
        'proposal_details' => $description,
        'attachment_1' => trim(${"image_url_1"}),
        'attachment_2' => trim(${"image_url_2"}) ,
        'attachment_3' => trim(${"image_url_3"}),
        'attachment_4' => trim(${"image_url_4"}),
        'attachment_5' => trim(${"image_url_5"}),
        'answer_1' => $answer_1,
        'answer_2' => $answer_2,
        'answer_3' => $answer_3,
        'answer_4' => $answer_4,
        'answer_5' => $answer_5,
        'total_price' => $total_price,
        'deposit_price' => $deposit_price,
        'currency_id' => $job_details['currency_id'],
        'currency_code' => $job_details['currency_code'],
        'country_id' => $country_id,
        'notify_proposal_closed' => $notif_me,
        'proposal_date' => $today,
        'proposal_status' => "open",
      );
      if($proposal_id = $this->api->register_job_proposal($update_data)){
        for($i=1; $i<=(sizeof($proposal_breakdown)/2); $i++){    
          $update_data = array(
            'proposal_id' => (int)$proposal_id,
            'job_id' => $job_details['job_id'],
            'cust_id' => $job_details['cust_id'],
            'provider_id' =>  $provider_id,
            'milestone_details' => $proposal_breakdown['part'.$i],
            'milestone_price' => $proposal_breakdown['price'.$i],
            'currency_id' => $job_details['currency_id'],
            'currency_code' => $job_details['currency_code'],
            'country_id' => $country_id,
          );
          $milestone_id = $this->api->insert_proposal_milestone_details($update_data);
        }
        $customer_details = $this->api->get_user_details($job_details['cust_id']);
        $provider_details = $this->api->get_service_provider_profile( $provider_id);
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')){
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        if(trim($provider_details['firstname']) == 'NULL' || trim($provider_details['lastname'] == 'NULL')) {
          $provider_name = explode("@", trim($provider_details['email_id']))[0];
        } else { $provider_name = trim($provider_details['firstname']) . " " . trim($provider_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('A New Proposal from')." ".$provider_name.", ".$this->lang->line('Your Job ID').": ".$job_id;
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($job_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value){
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('A New Proposal from'), 'type' => 'proposal-received', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Proposal Received'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------

        //Notifications to Provider----------------
          $message = $this->lang->line('You have to send proposal successfully');
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_provider = $this->api->get_user_device_details( $provider_id);
          if(!is_null($device_details_provider)) {
            $arr_provider_fcm_ids = array();
            $arr_provider_apn_ids = array();
            foreach ($device_details_provider as $value) {
              if($value['device_type'] == 1) {  array_push($arr_provider_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_provider_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Proposal Send'), 'type' => 'proposal-received', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_provider_fcm_ids, $api_key);
            //APN
            $msg_apn_provider =  array('title' => $this->lang->line('Proposal Received'), 'text' => $message);
            if(is_array($arr_provider_apn_ids)) { 
              $arr_provider_apn_ids = implode(',', $arr_provider_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_provider, $arr_provider_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$provider_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($provider_details['contact_no']), $message);

          //Email Notification
          $this->api_sms->send_email_text($provider_name, trim($provider_details['email_id']), $this->lang->line('Offer purchase cancellation.'), trim($message));
        //------------------------------------------
        $message = $this->lang->line('A New Proposal from')." ".$provider_name.", ".$this->lang->line('Your Job ID').": ".$job_id;
        // echo  json_encode($message);die();

        //Update to workstream master
        $update_data = array(
          'job_id' => (int)$job_id,
          'cust_id' => $job_details['cust_id'],
          'provider_id' =>  $provider_id,
          'cre_datetime' => $today,
          'status' => 0,
        );
        if($id = $this->api->check_workstream_master($update_data)){
          $workstream_id = $id;
        }else{
          $workstream_id = $this->api->update_to_workstream_master($update_data);
        }
        $update_data = array(
          'workstream_id' => (int)$workstream_id,
          'job_id' => (int)$job_id,
          'cust_id' => $job_details['cust_id'],
          'provider_id' =>  $provider_id,
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $provider_id,
          'type' => 'job_proposal',
          'file_type' => 'text',
          'proposal_id' => 1,
        );
        $this->api->update_to_workstream_details($update_data);
        $this->response = array(
          'response' => 'success',
          'message'=> $this->lang->line('proposal_successfully_send'),
        );
      }else{
        $this->response = array(
          'response' => 'failed',
          'message'=> $this->lang->line('failed_to_send_propopsal'),
        );     
      }       
      echo json_encode($this->response);
    }
    public function browse_project_old()
    {
      $cust_id =$this->input->post("cust_id");
      $last_id = $this->input->post('last_id');
      $start_date = $this->input->post('start_date');
      $strt_dt = ($start_date!="NULL")?date("Y-m-d", strtotime($start_date)) . ' 00:00:00':"NULL";
      $end_date = $this->input->post('end_date', TRUE);
      $end_dt = ($end_date!="NULL")?date("Y-m-d", strtotime($end_date)) . ' 23:59:59':"NULL"; 


      $data =array(
        'cust_id' => $cust_id,
        'sub_cat_id' => $this->input->post("sub_cat_id"),
        'cat_id' => $this->input->post("cat_id"),
        'start_date' => $strt_dt,
        'end_date' => $end_dt
      );
     if( $this->api->is_user_active_or_exists($cust_id)) {
        $jobs = $this->api->get_job_list($data , $last_id);
        $fav_jobs = $this->api->get_favorite_jobs($cust_id);
        $fav_projects = array();
        foreach ($fav_jobs as $fav_key) {
          array_push($fav_projects, $fav_key['job_id']);
        }
        for($i=0; $i<sizeof($jobs); $i++) {
          $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
          $jobs[$i]['proposal_counts'] = sizeof($proposals);
          $jobs[$i]['is_favorite']=(in_array($jobs[$i]['job_id'], $fav_projects))?"1":"0";
        }
        $projects=array(
          'filter'=>$data,
          'jobs'=>$jobs,
        );
        $this->response = array('response' => 'success','message'=> $this->lang->line('get_list_success'),
          'list_project'=> $projects,
        );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; } 
      echo  json_encode($this->response);
    }
    public function browse_project()
    {
      $cust_id = $this->input->post('cust_id');
      $cat_type_id = $this->input->post('cat_type_id');
      $cat_id = $this->input->post('cat_id');
      $start_date = $this->input->post('start_date');
      $start_date = ($start_date!="NULL")?date("Y-m-d", strtotime($start_date)) . ' 00:00:00':"NULL";
      $end_date = $this->input->post('end_date');
      $end_date = ($end_date!="NULL")?date("Y-m-d", strtotime($end_date)) . ' 23:59:59':"NULL";   
      $data =array(
        'cust_id' => $cust_id,
        'cat_id' => $cat_type_id,
        'sub_cat_id' => $cat_id,
        'start_date' => $start_date,
        'end_date' => $end_date,
      );
      if($jobs = $this->api->get_job_list($data))
      {
        $category_types = $this->api->get_category_types();
        $fav_jobs = $this->api->get_favorite_jobs($cust_id);
        $fav_projects = array();
        foreach ($fav_jobs as $fav_key) {
          array_push($fav_projects, $fav_key['job_id']);
        }
        for($i=0; $i<sizeof($jobs); $i++) {
          $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
          $jobs[$i]['proposal_counts'] = sizeof($proposals);
        }
        $projects=array(
          'filter'=>$data,
          'jobs'=>$jobs,
          'fav'=> $fav_projects
        );
        $this->response = array('response' => 'success','message'=> $this->lang->line('get_list_success'),
          'list_project'=> $projects,
        );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')]; } 
      echo  json_encode($this->response);
    }

    public function browse_provider()
    {
      $country_id = $this->input->post('country_id');
      $state_id = $this->input->post('state_id');
      $city_id = $this->input->post('city_id');
      $project_5 = $this->input->post('project_5');
      $project_50 = $this->input->post('project_50');
      $rate_from = $this->input->post('rate_from');
      $rate_to = $this->input->post('rate_to');
      $cust_id = $this->input->post('cust_id');
      $fav = $this->input->post('fav');
      $filter = $this->input->post('filter');
      if($filter == "advance")
      {
        $data = array(
          'skill' => "NULL", 
          'fav' => ($fav==1)?"yes":"no",
          'country_id' => $country_id,
          'state_id' => $state_id,
          'city_id' =>  $city_id,
          'rate_from' =>$rate_from,
          'rate_to' => $rate_to,
          'cust_id' => $cust_id
        );
      }else{  
        $data = array(
          'skill' => "NULL", 
          'fav' => ($fav==1)?"yes":"no",
           'country_id' => "NULL",
          'state_id' => "NULL",
          'city_id' => "NULL",
          'rate_from' => "NULL",
          'rate_to' => "NULL",
          'cust_id' => $cust_id
        );
      }
      if($freelancers = $this->api->list_of_provider($data))
      {
        // echo json_encode($freelancers);die();
        $favorite_freelancer = $this->api->get_favorite_freelancer($cust_id);
         $fav_freelancer = array();
        foreach ($favorite_freelancer as $fav_key) {
          array_push($fav_freelancer, $fav_key['provider_id']);
        }
        for($i=0; $i<sizeof($freelancers); $i++) {
          $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
          $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
          // echo json_encode($completed);
          $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
          if($completed['total_job'] != "0"){
           $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
          }else{$percent = 0;}  
          $freelancers[$i]['completed_job'] = (int)$completed['total_job'];
          $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
          $freelancers[$i]['job_percent'] = $percent;
          $freelancers[$i]['portfolio'] = (int)$portfolio;
          $freelancers[$i]['is_favorite']=(in_array($freelancers[$i]['cust_id'], $fav_freelancer))?"1":"0";
          if($project_5 >0 ){
            if((int)$project_5 < (int)$freelancers[$i]['completed_job']){
              $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
            }
          }
          if($project_50 >0 ){
            if((int)$project_50 < (int)$freelancers[$i]['completed_job']){
              $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
            }
          }
        }
        // echo  json_encode($freelancers);die();
        $data["project_5"]=$project_5;
        $data["project_50"]=$project_50;
        $data["filters"]=$filter;
        $result=array(
         "freelancers"=>$freelancers,
         "filters"=>$data,
        );
        $this->response = array('response' => 'success','message'=> $this->lang->line('get_list_success'),'list_project'=> $result);
      }else{
        $this->response = array('response' => 'failed','message'=> $this->lang->line('get_list_failed'));
      }
      echo  json_encode($this->response);  
    }
    public function browse_troubleshooter()
    {
      $cust_id =$this->input->post("cust_id");
      $last_id = $this->input->post('current_page');

      $start_date = $this->input->post('start_date');
      $strt_dt = ($start_date!="NULL")?date("Y-m-d", strtotime($start_date)) . ' 00:00:00':"NULL";
      $end_date = $this->input->post('end_date', TRUE);
      $end_dt = ($end_date!="NULL")?date("Y-m-d", strtotime($end_date)) . ' 23:59:59':"NULL"; 
      $data =array(
        'cust_id' => $cust_id,
        'sub_cat_id' => $this->input->post("sub_cat_id"),
        'cat_id' => $this->input->post("cat_id"),
        'start_date' => $strt_dt,
        'end_date' => $end_dt
      );
     if( $this->api->is_user_active_or_exists($cust_id)) {
        $jobs = $this->api->get_troubleshoot_list($data , $last_id);
        // $filter = "basic"; 
        // $category_types = $this->api->get_category_types();
        $fav_jobs = $this->api->get_favorite_jobs($cust_id);
        $fav_projects = array();
        foreach ($fav_jobs as $fav_key) {
          array_push($fav_projects, $fav_key['job_id']);
        }
        for($i=0; $i<sizeof($jobs); $i++) {
          $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
          $jobs[$i]['proposal_counts'] = sizeof($proposals);
          $jobs[$i]['is_favorite']=(in_array($jobs[$i]['job_id'], $fav_projects))?"1":"0";
        }
        $projects=array(
          'filter'=>$data,
          'jobs'=>$jobs,
        );
        $this->response = array(
          'response' => 'success',
          'message'=> $this->lang->line('get_list_success'),
          'list_project'=> $projects,
        );
      } else { $this->response = ['response' => 'failed', 'message' => $this->lang->line('user_inactive')]; } 
      echo  json_encode($this->response);
    }
    public function provider_delete_offer_images()
    {
      $offer_id = $this->input->post('offer_id'); $offer_id = (int) $offer_id;
      $no = $this->input->post('no'); $offer_id = (int) $offer_id;
      $offer_details = $this->api->get_service_provider_offer_details($offer_id);
      if($no == '1') {
        $update_data = array(
          "image_url_1" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_1'] != 'NULL'){
          unlink($offer_details['image_url_1']);
          $message = $this->lang->line('delete_success'); 
          $response = "success";
        }else{
          $response = "failed";
          $message = $this->lang->line('delete_failed');
        }
      }
      if($no == '2') {
        $update_data = array(
          "image_url_2" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_2'] != 'NULL'){
          unlink($offer_details['image_url_2']);
          $message = $this->lang->line('delete_success'); 
          $response = "success";
        }else{
          $response = "failed";
          $message = $this->lang->line('delete_failed');
        }
      }
      if($no == '3') {
        $update_data = array(
          "image_url_3" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_3'] != 'NULL'){
          unlink($offer_details['image_url_3']);
          $message = $this->lang->line('delete_success'); 
          $response = "success";
        }else{
          $response = "failed";
          $message = $this->lang->line('delete_failed');
        }
      }
      if($no == '4') {
        $update_data = array(
          "image_url_4" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_4'] != 'NULL'){
          unlink($offer_details['image_url_4']);
          $message = $this->lang->line('delete_success'); 
          $response = "success";
        }else{
          $response = "failed";
          $message = $this->lang->line('delete_failed');
        }
      }
      if($no == '5') {
        $update_data = array(
          "image_url_5" => 'NULL',
          "update_datetime" => date('Y-m-d H:i:s'),
          "is_edited" => '1',
        );
        if($offer_details['image_url_5'] != 'NULL'){
          unlink($offer_details['image_url_5']);
          $message = $this->lang->line('delete_success'); 
          $response = "success";
        }else{
          $response = "failed";
          $message = $this->lang->line('delete_failed');
        }
      }
      if( $this->api->update_offer_details($offer_id, $update_data)){

        $this->response=['response'=>$response, 'message'=> $message]; 
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('delete_failed')];
      }
      echo  json_encode($this->response);
    }
    public function provider_inprogress_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->input->post('cust_id', TRUE);
      $cust_id = (int) $cust_id;
      $filter = $this->input->post('filter', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      if(!isset($last_id) && $last_id == '') { $last_id = 0; }
      $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->post('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->post('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }  
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => 0,
        "provider_id" => $cust_id,
        "job_status" => 'in_progress',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'mobile',
        "last_id" => $last_id,
      );
      if($job_list = $this->api->get_jobs_list($search_data)){
        $dispute_master = $this->api->get_dispute_category_master($search_data);
        $cancellation_reasons = $this->api->get_job_cancellation_reasons();
        if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
        for($i=0; $i<sizeof($job_list); $i++) {
          if($job_list[$i]['service_type'] == 0){
            if($proposal_milestone = $this->api->get_proposal_milestone_by_proposal_id($job_list[$i]['proposal_id'])){
              $job_list[$i]['milestone_available'] = "yes";
            }else{
              $job_list[$i]['milestone_available'] = "no";
            }  
            if($this->api->get_milestone_payment_request($job_list[$i]['job_id'])){
              $job_list[$i]['milestone_request'] = "yes";
            }else{
              $job_list[$i]['milestone_request'] = "no";
            }
          }
        }
        $this->response=['response'=>'success', 'message'=> $this->lang->line('getting jobs list successfully') , 'job_list' => $job_list , 'filter' =>$filter , 'category_types'=> $category_types , 'cat_id'=> $cat_id , 'cat_type_id'=> $cat_type_id , 'start_date' => $start_date , 'end_date'=> $end_date , 'categories' => $categories , 'cancellation_reasons'=> $cancellation_reasons , 'dispute_master' => $dispute_master]; 
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('job list getting failed')];
      }
      echo json_encode($this->response);
    }
    public function my_inprogress_jobs()
    {
      //echo json_encode($_GET); die();
      $cust_id = $this->input->post('cust_id', TRUE);
      $cust_id = (int) $cust_id;
      $filter = $this->input->post('filter', TRUE);
      $last_id = $this->input->post('last_id', TRUE);
      if(!isset($filter) && $filter == '') { $filter = 'basic'; }
      if(!isset($last_id) && $last_id == '') { $last_id = 0; }
      $cat_id = $this->input->post('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id)) { $cat_id = 0; }
      $cat_type_id = $this->input->post('cat_type_id', TRUE); $cat_type_id = (int) $cat_type_id;
      if(!isset($cat_type_id)) { $cat_type_id = 0; }
      $start_date = $this->input->post('start_date', TRUE);
      if(!isset($start_date) || $start_date == '') { $start_date = $strt_dt = 'NULL'; } else {
        $strt_dt = date("Y-m-d", strtotime($start_date)) . ' 00:00:00'; }
      $end_date = $this->input->post('end_date', TRUE);
      if(!isset($end_date) || $end_date == '') { $end_date = $end_dt = 'NULL'; } else {
        $end_dt = date("Y-m-d", strtotime($end_date)) . ' 23:59:59'; }
      $category_types = $this->api->get_category_types();

      $search_data = array(
        "cust_id" => $cust_id,
        "provider_id" => 0,
        "job_status" => 'in_progress',
        "sub_cat_id" => $cat_id,
        "cat_id" => $cat_type_id,
        "start_date" => $strt_dt,
        "end_date" => $end_dt,
        "device" => 'mobile',
        "last_id" => $last_id,
      );
      if($job_list = $this->api->get_jobs_list($search_data)){
        $dispute_master = $this->api->get_dispute_category_master($search_data);
        $cancellation_reasons = $this->api->get_job_cancellation_reasons();
        for($i=0; $i<sizeof($job_list); $i++) {
          if($job_list[$i]['service_type'] == 0){
            if($this->api->get_milestone_payment_request($job_list[$i]['job_id'])){
              $job_list[$i]['milestone_request'] = "yes";
            }else{
              $job_list[$i]['milestone_request'] = "no";
            }
          }
        }
        if(isset($cat_id) && $cat_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }
        //echo json_encode($job_list); die();
       
       $this->response=['response'=>'success', 'message'=> $this->lang->line('getting jobs list successfully') , 'job_list' => $job_list , 'filter' =>$filter , 'category_types'=> $category_types , 'cat_id'=> $cat_id , 'cat_type_id'=> $cat_type_id , 'start_date' => $start_date , 'end_date'=> $end_date , 'categories' => $categories , 'cancellation_reasons'=> $cancellation_reasons , 'dispute_master' => $dispute_master]; 
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('job list getting failed')];
      }
      echo json_encode($this->response);
    }
    public function job_invitations()
    {
      //echo json_encode($_POST); die();
      $provider_id = $this->input->post('cust_id', TRUE);
      $provider_id = (int) $provider_id;
      if($job_list = $this->api->list_of_job_invitation($provider_id))
      {
        for($i=0; $i<sizeof($job_list); $i++) {
          if($job_list[$i]['service_type'] == 0){
            $proposals = $this->api->get_job_proposals_list($job_list[$i]['job_id']);
            $job_list[$i]['proposal_counts'] = sizeof($proposals);
          }
        }
        $this->response=['response'=>'success', 'message'=> $this->lang->line('getting jobs list successfully') , 'job_list' => $job_list]; 
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('job list getting failed')];
      }
      echo json_encode($this->response);
    }
    public function pending_job_proposals()
    {
      //echo json_encode($_POST); die();
      $provider_id = $this->input->post('cust_id', TRUE);
      $provider_id = (int) $provider_id;
      if($job_list = $this->api->list_of_job_proposals_sends($provider_id))
      {
        for($i=0; $i<sizeof($job_list); $i++) {
          if($job_list[$i]['service_type'] == 0){
            $proposals = $this->api->get_job_proposals_list($job_list[$i]['job_id']);
            $job_list[$i]['proposal_counts'] = sizeof($proposals);
          }
        }
        $this->response=['response'=>'success', 'message'=> $this->lang->line('getting jobs list successfully') , 'job_list' => $job_list]; 
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('job list getting failed')];
      }
      echo json_encode($this->response);
    }

    //Start Services Account/E-Wallet/Withdrawals-------------
      public function services_wallet()
      {
        //echo json_encode($_POST); die();
        $cust_id = $this->input->post('cust_id', TRUE);
        if($balance = $this->user->check_user_account_balance_smp($cust_id))
        {
          if($amount = $this->user->get_customer_account_withdraw_request_sum($cust_id, 'request'))
          {
            $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'balance' => $balance , 'pending_withdrawls' => $amount ];
          }else{
            $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'balance' => $balance];
          } 
        }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('No account found')];
        }
        echo json_encode($this->response);
      }
      public function services_account_history()
      {
        //echo json_encode($_POST); die();
        $cust_id = $this->input->post('cust_id', TRUE);
        if($history = $this->api->get_services_account_history_smp($cust_id))
        {
          for($i=0; $i<sizeof($history); $i++)
          {
            $cat = $this->api->get_category_details_by_id($history[$i]['cat_id']);
            $history[$i]['icon_url'] = $cat['icon_url']; 
          }

          $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'history' => $history ]; 
        }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('No account history found')];
        }
        echo json_encode($this->response);
      }
      public function services_invoices_history()
      { 
        $cust_id = $this->input->post('cust_id', TRUE);
        if($invoices = $this->api->get_services_invoices($cust_id))
        {
          $this->response=['response'=>'success', 'message'=> $this->lang->line('success') ,'invoices' => $invoices ]; 
        }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('No invoice history found')];
        }
        echo json_encode($this->response);
      }
      public function my_scrow()
      {
        $cust_id = $this->input->post('cust_id', TRUE);
        if($balance = $this->api->deliverer_scrow_master_list_smp($cust_id))
        {
          $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'balance' => $balance ]; 
        }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('failed to retrive scrow balance')];
        }
        echo json_encode($this->response);
      }
      public function user_scrow_history()
      {
        $cust_id = $this->input->post('cust_id', TRUE);
        if($history = $this->api->get_customer_scrow_history_smp($cust_id))
        {
          $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'history' => $history ]; 
        }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('No account history found')];
        }
        echo json_encode($this->response);
      }
      public function services_withdrawals_request()
      {
        $cust_id = $this->input->post('cust_id', TRUE);
        if($history = $this->user->get_customer_account_withdraw_request_list($cust_id, 'request'))
        {
          $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'requested_history' => $history ]; 
        }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('No account history found')];
        }
        echo json_encode($this->response);
      }
      public function services_withdrawals_accepted()
      {
        $cust_id = $this->input->post('cust_id', TRUE);
        if($history = $this->user->get_customer_account_withdraw_request_list($cust_id, 'accept'))
        {
          $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'accepted_history' => $history ]; 
        }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('No account history found')];
        }
        echo json_encode($this->response);
      }
      public function services_withdrawals_rejected()
      {
        $cust_id = $this->input->post('cust_id', TRUE);
        if($history = $this->user->get_customer_account_withdraw_request_list($cust_id, 'reject'))
        {
          $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'rejected_history' => $history ]; 
        }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('No account history found')];
        }
        echo json_encode($this->response);
      }
    //Start Services Account/E-Wallet/Withdrawals-------------
    
    public function my_favorite_freelancers()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      $project_5= null;
      $project_50=null;
      $country_id=null;
      $city_id=null;
      $state_id=null;
      $states=null;
      $cities=null;
      $rate_from=null;
      $rate_to=null;
      $fav_freelancer = array();
      $filter = 'basic';
      $country = $this->api->get_countries();
      if($freelancers = $this->api->get_favourite_freelancers($cust_id))
      {
        for($i=0; $i<sizeof($freelancers); $i++)
        {
          $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
          $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
          // echo json_encode($completed);
          $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
          if($completed['total_job'] != "0"){
           $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
          }else{$percent = 0;}  
          $freelancers[$i]['completed_job'] = (int)$completed['total_job'];
          $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
          $freelancers[$i]['job_percent'] = $percent;
          $freelancers[$i]['portfolio'] = (int)$portfolio;
          $freelancers[$i]['project_5'] = "yes";
          if($project_5 != null ){
            if((int)$project_5 < (int)$freelancers[$i]['completed_job']){
              $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
            }
          }
          if($project_50 != null ){
            if((int)$project_50 < (int)$freelancers[$i]['completed_job']){
              $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
            }
          }
        }

        $favorite_freelancer = $this->api->get_favorite_freelancer($cust_id);
        // echo json_encode($freelancers);die();
        $fav_freelancer = array();
        foreach ($favorite_freelancer as $fav_key) {
          array_push($fav_freelancer, $fav_key['provider_id']);
        }

        $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'freelancers' => $freelancers ]; 
      }else{ 
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')];
      }
      echo json_encode($this->response);    
    }

    public function wallet_login_confirm()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      $password = $this->input->post('password', TRUE);
      $cust =  $this->api->get_consumer_datails($cust_id, true);
      if($cust['password'] == $password){
        $this->response=['response'=>'success']; 
      }else{ 
        $this->response = ['response' => 'failed'];
      }
      echo json_encode($this->response);    
    }

    public function avalaible_milestone_for_withdraw()
    {
      $cust_id = $this->input->post('cust_id', TRUE);
      $account_id = $this->input->post('account_id', TRUE);
      $currency_code = $this->input->post('currency_code', TRUE);
      if($job_list = $this->api->get_service_job_list($cust_id))
      { 
        for($i=0; $i<sizeof($job_list); $i++){
          if($job_list[$i]['service_type'] == 0){
            $completed_milestone = $this->api->get_job_completed_milestone($job_list[$i]['job_id']);
            $job_list[$i]['completed_milestone'] = sizeof($completed_milestone);
          }else{
            $job_list[$i]['completed_milestone'] = 0;
          }
        }
        $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'job_list' => $job_list , 'account_id' => $account_id , 'currency_code' => $currency_code]; 
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')];
      }
      echo json_encode($this->response);
    }

    public function job_milestone_for_withdraw()
    {
      $account_id = $this->input->post('account_id', TRUE);
      $currency_code = $this->input->post('currency_code', TRUE);
      $job_id = $this->input->post('job_id', TRUE);
      $current_date = Date('Y-m-d H:i:s');
      if($job_details = $this->api->get_customer_job_details($job_id))
      { 
        $search_data = array(
          "country_id" => $job_details['country_id'],
          "cat_id" => $job_details['sub_cat_id'],
          "cat_type_id" => $job_details['cat_id'],
        );
        if($advance_payment = $this->api->get_country_cat_advance_payment_details($search_data))
        {
          if($milestone = $this->api->get_job_completed_milestone($job_id))
          {
            $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'job_details' => $job_details , 'account_id' => $account_id , 'currency_code' => $currency_code, 'advance_payment' => $advance_payment , 'milestone' => $milestone];
          }else{
            $this->response = ['response' => 'failed', 'message' => $this->lang->line('milestone not found')];
          }
        }else{
          $this->response = ['response' => 'failed', 'message' => $this->lang->line('country advance payment for this category not found')];
        }
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('job not found')];
      }
      echo json_encode($this->response);
    }
    public function job_workroom()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $provider_id = $this->input->post('provider_id', TRUE); $job_id = (int)$job_id;
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int)$cust_id;
      $job_details = $this->api->get_customer_job_details($job_id);
      $job_provider_profile = $this->api->get_service_provider_profile((int)$cust_id);
      $job_customer_details = $this->api->get_user_details((int)$provider_id);
      $workroom = $this->api->get_job_workstream_details($job_id , $cust_id , $provider_id);
      if($job_details && $job_provider_profile && $job_customer_details) {
        $this->response=['response'=>'success', 'message'=> $this->lang->line('success') , 'job_details' => $job_details , 'job_provider_profile' => $job_provider_profile , 'job_customer_details' => $job_customer_details, 'workroom' => $workroom]; 
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('get_list_failed')];
      }
      echo json_encode($this->response);
    }

    public function send_job_workroom_chat()
    {
      //echo json_encode($_POST); die();
      $job_id = $this->input->post('job_id', TRUE); $job_id = (int)$job_id;
      $sender_id = $this->input->post('sender_id', TRUE); $sender_id = (int)$sender_id;
      $text_msg = $this->input->post('message_text', TRUE);
      $today = date("Y-m-d H:i:s");
      if($job_details = $this->api->get_job_details($job_id))
      {
        $cust_id = $job_details['cust_id']; $cust_id = (int)$cust_id;
        $provider_id = $job_details['provider_id']; $provider_id = (int)$provider_id;
        if( !empty($_FILES["attachment"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/attachements/'; 
          $config['allowed_types']  = '*'; 
          $config['max_size']       = '0'; 
          $config['max_width']      = '0'; 
          $config['max_height']     = '0';
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config); 
          if($this->upload->do_upload('attachment')) { 
            $uploads    = $this->upload->data();  
            $attachment_url =  $config['upload_path'].$uploads["file_name"]; 
          } else { $attachment_url = "NULL"; }
        } else { $attachment_url = "NULL"; }

        if($workstream_details = $this->api->get_workstream_details_by_job_id($job_id)) {
          $insert_data = array(
            'workstream_id' => (int)$workstream_details['workstream_id'],
            'job_id' => $job_id,
            'cust_id' => $cust_id,
            'provider_id' => $provider_id,
            'text_msg' => $text_msg,
            'attachment_url' =>  $attachment_url,
            'cre_datetime' => $today,
            'sender_id' => $sender_id,
            'type' => ($attachment_url=='NULL')?'chat':'attachment',
            'file_type' => ($attachment_url=='NULL')?'text':'file',
            'proposal_id' => 0,
            "ratings" => "NULL",
          );
          //echo json_encode($insert_data); die();
          $this->api->update_to_workstream_details($insert_data);

          //Notifications to Provider----------------
            $api_key = $this->config->item('delivererAppGoogleKey');

            if($job_details['cust_id'] == $sender_id) { $device_details = $this->api->get_user_device_details((int)$provider_id);
            } else { $device_details = $this->api->get_user_device_details((int)$job_details['cust_id']); }

            if(!is_null($device_details)) {
              $arr_fcm_ids = array(); $arr_apn_ids = array();
              foreach ($device_details as $value) {
                if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('new_message'), 'type' => 'chat', 'notice_date' => $today, 'desc' => $text_msg);
              $this->api_sms->sendFCM($msg, $arr_fcm_ids, $api_key);
              //APN
              $msg_apn_provider =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
              if(is_array($arr_apn_ids)) { 
                $arr_apn_ids = implode(',', $arr_apn_ids);
                $this->api_sms->sendPushIOS($msg_apn_provider, $arr_apn_ids, $api_key);
              }
            }
          //------------------------------------------
        }
        $workroom = $this->api->get_job_workstream_details($job_id , $cust_id , $provider_id);
        $this->response=['response'=>'success', 'message'=> $this->lang->line('send') ,'workroom' => $workroom]; 
      }else{ 
        $this->response = ['response' => 'failed', 'message' => $this->lang->line('message send fail')];
      }
      echo json_encode($this->response); 
    }
}

/* End of file Api_services.php */
/* Location: ./application/controllers/Api_services.php */