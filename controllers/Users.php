<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    
  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('Users_model', 'users');   
      $this->load->model('web_user_model', 'user'); 
      $this->load->model('Admin_orders_model', 'orders'); 
      $this->load->model('Notification_model', 'notice');
      $this->load->model('Withdraw_model', 'withdraw'); 
      $this->load->model('Driver_Api_model', 'api');  
      $this->load->model('Relay_api_model', 'relayapi');
      $this->load->model('api_model_sms', 'api_sms');  
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }
  
  public function index()
  {
    if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
  }

  public function users_master()
  {   
    $auth_type_id = $this->session->userdata('auth_type_id');
    if($auth_type_id > 1) {   $admin_country_id = $this->session->userdata('admin_country_id'); }
    else { $admin_country_id = 0; }
    $users_list = $this->users->get_users($admin_country_id);
    $this->load->view('admin/users_list_view', compact('users_list'));      
    $this->load->view('admin/footer_view');   
  }

  public function view_users()
  { 
    $cust_id = (int) $this->uri->segment('3');
    $users_detail = $this->users->get_users_detail($cust_id);
    $deliverer_detail = $this->users->get_deliverer_detail($cust_id);
    $users_skill_list = $this->users->get_customer_skills($cust_id);
    $wallet_balance = $this->user->check_user_account_balance($cust_id);
    $scrow_balance = $this->user->deliverer_scrow_master_list($cust_id);
    $withdraw_list = $this->user->get_customer_account_withdraw_list($cust_id);
    /*$user_skills = array();
    foreach ($users_skill_list as $skills) {
      $skill_id = $skills['skill_id'];
      $level_id = $skills['level_id'];
      $skill_dtls = $this->users->get_skill_name($skill_id);
      $level_dtls = $this->users->get_level_name($level_id);
      $skill_name = $skill_dtls['skill_name'];
      $level_name = $level_dtls['level_title'];
      $skills_dtl = array("skill_id" => $skill_id, "skill_name" => $skill_name, "level_id" => $level_id, "level_name" => $level_name);
      array_push($user_skills, $skills_dtl);
      unset($skills_dtl);
    }*/
    $users_country = $this->notice->get_country_detail($users_detail['country_id']);
    $users_state = $this->notice->get_state_detail($users_detail['state_id']);
    $users_city = $this->notice->get_city_detail($users_detail['city_id']);

    $this->load->view('admin/users_profile_view', compact('users_detail','users_skill_list','users_country','users_state','users_city','deliverer_detail','wallet_balance','scrow_balance','withdraw_list'));
    $this->load->view('admin/footer_view');
  }

  public function user_wallet_history()
  {   
    $cust_id = $this->input->post('cust_id', TRUE);
    $history = $this->user->get_customer_account_history($cust_id);
    $this->load->view('admin/user_wallet_history_view', compact('history','cust_id'));      
    $this->load->view('admin/footer_view');   
  }

  public function user_scrow_history()
  {   
    $cust_id = $this->input->post('cust_id', TRUE);
    $history = $this->user->get_customer_scrow_history($cust_id);
    $this->load->view('admin/user_scrow_history_view', compact('history','cust_id'));     
    $this->load->view('admin/footer_view');   
  }

  public function active_users()
  {
    $cust_id = $this->input->post('id', TRUE);
    if( $this->users->activate_users( (int) $cust_id) ) { echo 'success'; } else {  echo 'failed';  }
  }

  public function inactive_users()
  {
    $cust_id = $this->input->post('id', TRUE);
    if( $this->users->inactivate_users( (int) $cust_id) ) { echo 'success'; } else {  echo 'failed';  }
  }

  public function update_shipping_mode()
  {
    $deliverer_id = $this->input->post('deliverer_id', TRUE);
    $shipping_mode = $this->input->post('shipping_mode', TRUE);
    if( $this->users->update_shipping_mode((int)$deliverer_id,(int)$shipping_mode)) { echo 'success'; } else {  echo 'failed';  }
  }

  //inactive users
  public function inact_users()
  {
    $inactive_users_list = $this->users->get_inactive_users();
    $this->load->view('admin/inactive_users_list_view', compact('inactive_users_list'));      
    $this->load->view('admin/footer_view');   
  }

  public function active_users_dedicated()
  {
    $cust_id = $this->input->post('id', TRUE);
    //echo $cust_id; die();
    if( $this->users->activate_users_dedicated( (int) $cust_id) ) { echo 'success'; } else {  echo 'failed';  }
  }

  public function inactivate_users_dedicated()
  {
    $cust_id = $this->input->post('id', TRUE);
    //echo $cust_id; die();
    if( $this->users->inactivate_users_dedicated( (int) $cust_id) ) { echo 'success'; } else {  echo 'failed';  }
  }

  public function activate_payment_type()
  {
    $cust_id = $this->input->post('id', TRUE);
    //echo $cust_id; die();
    if( $this->users->activate_users_payment_type( (int) $cust_id) ) {  echo 'success'; } else {  echo 'failed';  }
  }

  public function inactivate_payment_type()
  {
    $cust_id = $this->input->post('id', TRUE);
    //echo $cust_id; die();
    if( $this->users->inactivate_users_payment_type( (int) $cust_id) ) {  echo 'success'; } else {  echo 'failed';  }
  }

  public function set_gonagoo_commission()
  {
    $commission_type = $this->input->post('commission_type', TRUE);
    $cust_id = $this->input->post('cust_id', TRUE);
    //echo $commission_type . $cust_id; die();
    if( $this->users->set_gonagoo_commission_type( (int) $cust_id, (int) $commission_type ) ) { echo 'success'; } else {  echo 'failed';  }
  }

  public function define_user_commission()
  {
    $cust_id = (int) $this->uri->segment(4);
    //die();
    $commission_percentage = $this->users->get_user_gonagoo_commission_details((int)$cust_id);
    //echo json_encode($commission_percentage); die();
    $this->load->view('admin/define_commission_percentage_list_view', compact('commission_percentage','cust_id'));      
    $this->load->view('admin/footer_view');     
  }

  public function add_define_commission_percentage()
  {
    if( !is_null($this->input->post('cust_id')) ) { $cust_id = (int) $this->input->post('cust_id'); }
    else if(!is_null($this->uri->segment(4))) { $cust_id = (int)$this->uri->segment(4); }
    else { redirect(base_url('admin/users')); }
    $categories = $this->users->get_categories();
    $countries = $this->users->get_countries();
    $this->load->view('admin/user_commission_percentage_add_view',compact('countries','categories','cust_id'));
    $this->load->view('admin/footer_view'); 
  }

  public function register_commission_percentage()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->input->post('cust_id', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $category_ids = $this->input->post('category', TRUE);
    $ELGCP = $this->input->post('ELGCP', TRUE);
    $ENGCP = $this->input->post('ENGCP', TRUE);
    $EIGCP = $this->input->post('EIGCP', TRUE);
    $SLGCP = $this->input->post('SLGCP', TRUE);
    $SNGCP = $this->input->post('SNGCP', TRUE);
    $SIGCP = $this->input->post('SIGCP', TRUE);
    $ALGCP = $this->input->post('ALGCP', TRUE);
    $ANGCP = $this->input->post('ANGCP', TRUE);
    $AIGCP = $this->input->post('AIGCP', TRUE);
    $insert_flag = false;

    if($country_id > 0 ) {
      foreach ($category_ids as $id) {
        $insert_data = array(
          "cust_id" => (int)trim($cust_id), 
          "category_id" => (int)trim($id), 
          "country_id" => trim($country_id), 
          "rate_type" => 0, 
          "ELGCP" => number_format($ELGCP,2,'.',''),
          "ENGCP" => number_format($ENGCP,2,'.',''),
          "EIGCP" => number_format($EIGCP,2,'.',''),          
          "SLGCP" => number_format($SLGCP,2,'.',''),
          "SNGCP" => number_format($SNGCP,2,'.',''),
          "SIGCP" => number_format($SIGCP,2,'.',''),
          "ALGCP" => number_format($ALGCP,2,'.',''),
          "ANGCP" => number_format($ANGCP,2,'.',''),
          "AIGCP" => number_format($AIGCP,2,'.',''),          
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "status" =>  1, 
        );
        //echo json_encode($insert_data); die();
        if(! $this->users->check_payment($insert_data) ) {
          if($this->users->register_payment($insert_data)){ $insert_flag = true;  } 
        } 
      }
    } else {  $this->session->set_flashdata('error','Please select country!');  }
    if($insert_flag){ $this->session->set_flashdata('success','Commission details successfully added!'); }
    else{ $this->session->set_flashdata('error','Unable to add, already exists for selected country and category!');  }
    redirect(base_url('admin/users/add-define-commission-percentage/'.(int)trim($cust_id)));
  }

  public function delete_user_commission()
  {
    $pay_id = $this->input->post('pay_id', TRUE);
    if( $this->users->delete_commission_rate( (int) $pay_id) ) { echo 'success'; } else { echo 'failed'; }
  }

  public function edit_commission_rate()
  {
    if( !is_null($this->input->post('pay_id')) ) { $pay_id = $this->input->post('pay_id'); }
    else if(!is_null($this->uri->segment(4))) { $pay_id = (int)$this->uri->segment(4); }
    else { redirect(base_url('admin/users')); }
    $details = $this->users->get_commission_details((int)$pay_id);
    //echo json_encode($details); die();
    $this->load->view('admin/user_commission_edit_view',compact('details'));
    $this->load->view('admin/footer_view'); 
  }

  public function update_commission_percentage()
  {
    $pay_id = $this->input->post('pay_id');
    $ELGCP = $this->input->post('ELGCP', TRUE);
    $ENGCP = $this->input->post('ENGCP', TRUE);
    $EIGCP = $this->input->post('EIGCP', TRUE);
    $SLGCP = $this->input->post('SLGCP', TRUE);
    $SNGCP = $this->input->post('SNGCP', TRUE);
    $SIGCP = $this->input->post('SIGCP', TRUE);
    $ALGCP = $this->input->post('ALGCP', TRUE);
    $ANGCP = $this->input->post('ANGCP', TRUE);
    $AIGCP = $this->input->post('AIGCP', TRUE);
    
    $update_data = array(
      "ELGCP" => number_format($ELGCP,2,'.',''),
      "ENGCP" => number_format($ENGCP,2,'.',''),
      "EIGCP" => number_format($EIGCP,2,'.',''),
      "SLGCP" => number_format($SLGCP,2,'.',''),
      "SNGCP" => number_format($SNGCP,2,'.',''),
      "SIGCP" => number_format($SIGCP,2,'.',''),
      "ALGCP" => number_format($ALGCP,2,'.',''),
      "ANGCP" => number_format($ANGCP,2,'.',''),
      "AIGCP" => number_format($AIGCP,2,'.',''),
    );
    if($this->users->update_payment_details($update_data, $pay_id)){ $this->session->set_flashdata('success', 'Commission details updated successfully!'); }
    else { $this->session->set_flashdata('error', 'Failed to update commission details!'); }
    redirect(base_url('admin/users/edit-commission-rate/'.(int)trim($pay_id)));
  }

  public function add_define_commission_rate()
  {
    if( !is_null($this->input->post('cust_id')) ) { $cust_id = (int) $this->input->post('cust_id'); }
    else if(!is_null($this->uri->segment(4))) { $cust_id = (int)$this->uri->segment(4); }
    else { redirect(base_url('admin/users')); }
    $categories = $this->users->get_categories();
    $countries = $this->users->get_countries();
    $this->load->view('admin/user_commission_rate_add_view',compact('countries','categories','cust_id'));
    $this->load->view('admin/footer_view'); 
  }

  public function register_commission_rate()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->input->post('cust_id', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $category_ids = $this->input->post('category', TRUE);
    $ELGCP = $this->input->post('ELGCP', TRUE);
    $ENGCP = $this->input->post('ENGCP', TRUE);
    $EIGCP = $this->input->post('EIGCP', TRUE);
    $SLGCP = $this->input->post('SLGCP', TRUE);
    $SNGCP = $this->input->post('SNGCP', TRUE);
    $SIGCP = $this->input->post('SIGCP', TRUE);
    $ALGCP = $this->input->post('ALGCP', TRUE);
    $ANGCP = $this->input->post('ANGCP', TRUE);
    $AIGCP = $this->input->post('AIGCP', TRUE);
    $insert_flag = false;

    if($country_id > 0 ) {
      foreach ($category_ids as $id) {
        $insert_data = array(
          "cust_id" => (int)trim($cust_id), 
          "category_id" => (int)trim($id), 
          "country_id" => trim($country_id), 
          "rate_type" => 1, 
          "ELGCP" => number_format($ELGCP,2,'.',''),
          "ENGCP" => number_format($ENGCP,2,'.',''),
          "EIGCP" => number_format($EIGCP,2,'.',''),
          "SLGCP" => number_format($SLGCP,2,'.',''),
          "SNGCP" => number_format($SNGCP,2,'.',''),
          "SIGCP" => number_format($SIGCP,2,'.',''),
          "ALGCP" => number_format($ALGCP,2,'.',''),
          "ANGCP" => number_format($ANGCP,2,'.',''),
          "AIGCP" => number_format($AIGCP,2,'.',''),        
          "cre_datetime" =>  date("Y-m-d H:i:s"), 
          "status" =>  1, 
        );
        //echo json_encode($insert_data); die();
        if(! $this->users->check_payment($insert_data) ) {
          if($this->users->register_payment($insert_data)){ $insert_flag = true;  } 
        } 
      }
    } else {  $this->session->set_flashdata('error','Please select country!');  }
    if($insert_flag){ $this->session->set_flashdata('success','Commission details successfully added!'); }
    else{ $this->session->set_flashdata('error','Unable to add, already exists for selected country and category!');  }
    redirect(base_url('admin/users/add-define-commission-rate/'.(int)trim($cust_id)));
  }

  public function pending_orders()
  {
    if( !is_null($this->input->post('cust_id')) ) { $cust_id = $this->input->post('cust_id'); }
    else if(!is_null($this->uri->segment(4))) { $cust_id = (int)$this->uri->segment(4); }
    else { redirect(base_url('admin/users')); }

    $order_list = $this->users->get_dedicated_users_pending_order_list($cust_id);
    $currency_list = $this->users->get_dedicated_users_pending_order_currency_list($cust_id);
    $user_details = $this->users->get_users_detail($cust_id);
    $countries = $this->users->get_countries();
    //echo json_encode($order_list); die();
    $this->load->view('admin/users_pending_payment_list_view', compact('order_list','user_details','currency_list','countries'));
    $this->load->view('admin/footer_view');   
  }

  public function pending_orders_filter()
  {
    //echo json_encode($_POST); die();
    $cust_id = $this->input->post('cust_id'); $cust_id = (int)$cust_id;
    $date_range = $this->input->post('date_range');
    $country_id = $this->input->post('country_id');
    if($date_range!='') {
      $date_range_array = explode(',',$date_range);
      $from_date = $date_range_array[0];
      $to_date = $date_range_array[1];
    } else {
      $from_date = '0';
      $to_date = '0';
    }

    $currency_sign = $this->input->post('currency_sign');
    $filter = $this->input->post('filter');

    $filter_data = array(
      "cust_id" => (int)$cust_id,
      "from_date" => $from_date,
      "to_date" => $to_date,
      "currency_sign" => $currency_sign,
      "country_id" => $country_id,
    );
    //echo json_encode($filter_data); die();

    $order_list = $this->users->get_dedicated_users_pending_order_list_filter($filter_data, $cust_id);
    //echo json_encode($order_list); die();
    //echo $this->db->last_query(); die();
    
    $currency_list = $this->users->get_dedicated_users_pending_order_currency_list($cust_id);
    $user_details = $this->users->get_users_detail($cust_id);
    $countries = $this->users->get_countries();

    $this->load->view('admin/users_pending_payment_list_view', compact('order_list','user_details','currency_list','date_range','currency_sign','filter','countries','country_id'));      
    $this->load->view('admin/footer_view');   
  }

  public function view_pending_order_details()
  { 
    if( !is_null($this->input->post('order_id')) ) { $order_id = $this->input->post('order_id'); }
    else if(!is_null($this->uri->segment(4))) { $order_id = (int)$this->uri->segment(4); }
    else { redirect(base_url('admin/users')); }
    $order_details = $this->orders->get_order_details($order_id);
    $package_details = $this->orders->get_order_package_details($order_id);
    $cust_details = $this->orders->get_customer_details($order_details['cust_id']);
    $order_country = $this->orders->get_country_details($order_details['from_country_id']);
    $order_state = $this->orders->get_state_details($order_details['from_state_id']);
    $order_city = $this->orders->get_city_details($order_details['from_city_id']);
    $vehicle_details = $this->orders->get_vehicle_details($order_details['vehical_type_id']);
    $unit_details = $this->orders->get_unit_details($order_details['unit_id']);
    $requested_deliverers = $this->orders->get_requested_deliverers_details($order_details['order_id']);
    $rejected_deliverers = $this->orders->get_rejected_deliverers_details($order_details['order_id']);

    if($order_details['deliverer_id'] != "NULL" || $order_details['deliverer_id'] != 0) {
      $deliverer_details = $this->orders->get_customer_details($order_details['deliverer_id']);
    }
    if($order_details['driver_id'] != "NULL" || $order_details['driver_id'] != 0) {
      $driver_details = $this->orders->get_driver_details($order_details['driver_id']);
    }
    $this->load->view('admin/pending_order_details_view', compact('order_details','cust_details','deliverer_details','driver_details','order_country','order_state','order_city','vehicle_details','unit_details','requested_deliverers','rejected_deliverers','package_details'));
    $this->load->view('admin/footer_view');
  }

  public function confirm_pending_order_details()
  {
    //echo json_encode($_POST);
    $order_id = $this->input->post('order_id'); $order_id = (int)$order_id;
    $transaction_id = $this->input->post('transaction_id');
    $payment_date = $this->input->post('payment_date');
    $payment_time = $this->input->post('payment_time');
    $payment_datetime = $payment_date . ' ' . $payment_time . ':00';
    $order_details = $this->orders->get_order_details($order_id);
    $cust_id = (int)$order_details['cust_id'];
    $deliverer_id = (int)$order_details['deliverer_id'];
    $cust_details = $this->api->get_user_details($cust_id);
    $deliverer_details = $this->api->get_user_details($deliverer_id);
    $today = date('Y-m-d h:i:s');
    //echo json_encode($deliverer_details); die();

    if($order_details['order_status'] == 'delivered'){
      if($order_details['complete_paid'] == 0){
        $update_date = array( 'paid_amount' => $order_details['order_price'], 'balance_amount' => 0, 'complete_paid' => 1, 'payment_method' => 'bank', 'payment_datetime' => $payment_datetime, 'transaction_id' => $transaction_id );
        //echo json_encode($update_date); die();
        if( $this->users->update_order_details($update_date, $order_id) ){
          //invoice configuration--------------------------------------------------------------
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if(trim($lang) == 'fr'){ $this->lang->load('frenchApi_lang','french'); } 
            else if(trim($lang) == 'sp'){ $this->lang->load('spanishApi_lang','spanish');} 
            else { $this->lang->load('englishApi_lang','english'); }
            $invoice_lang = '';
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
          //invoice configuration--------------------------------------------------------------

          //Payment Section--------------------------------------------------------------------
            //Add order payment and commission details in customer account master------------
              if($this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']))) {
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              } else {
                $insert_data_customer_master = array(
                  "user_id" => (int)$cust_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));
              }

              $account_balance = trim($customer_account_master_details['account_balance']) + (trim($order_details['order_price']) + trim($order_details['commission_amount']));
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => (trim($order_details['order_price']) + trim($order_details['commission_amount'])),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //End Add order payment and commission details in customer account master--------

            //Deduct payment details from customer account master and history----------------
              $account_balance = trim($customer_account_master_details['account_balance']) - (trim($order_details['order_price']) + trim($order_details['commission_amount']));
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($order_details['currency_sign']));

              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => (trim($order_details['order_price']) + trim($order_details['commission_amount'])),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //End Deduct payment details from customer account master and history------------

            //Add Order Payment to Gonagoo master--------------------------------------------
              //Add standard price---------------------------------------------------------
                if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                } else {
                  $insert_data_gonagoo_master = array(
                    "gonagoo_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($order_details['currency_sign']),
                  );
                  $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                }

                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['standard_price']);
                $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'standard_price',
                  "amount" => ($order_details['currency_sign']=='XAF' || 'XOF')?(int)$order_details['standard_price']:$order_details['standard_price'],
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => 'bank',
                  "transfer_account_number" => 'NULL',
                  "bank_name" => 'NULL',
                  "account_holder_name" => 'NULL',
                  "iban" => 'NULL',
                  "email_address" => 'NULL',
                  "mobile_number" => 'NULL',
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              //End Add standard price----------------------------------------------------- 

              //Update urgent fee in gonagoo account master--------------------------------
                if(trim($order_details['urgent_fee']) > 0) {
                  $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['urgent_fee']);
                  $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 1,
                    "transaction_type" => 'urgent_fee',
                    "amount" => ($order_details['currency_sign']=='XAF' || 'XOF')?(int)$order_details['urgent_fee']:$order_details['urgent_fee'],
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'bank',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => trim($transaction_id),
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                }
              //End Update urgent fee in gonagoo account master----------------------------

              //Update insurance fee in gonagoo account master-----------------------------
                if(trim($order_details['insurance_fee']) > 0) {
                  $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['insurance_fee']);
                  $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 1,
                    "transaction_type" => 'insurance_fee',
                    "amount" => ($order_details['currency_sign']=='XAF' || 'XOF')?(int)$order_details['insurance_fee']:$order_details['insurance_fee'],
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'bank',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => trim($transaction_id),
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                }
              //End Update insurance fee in gonagoo account master-------------------------

              //Update handling fee in gonagoo account master------------------------------
                if(trim($order_details['handling_fee']) > 0) {
                  $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['handling_fee']);
                  $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 1,
                    "transaction_type" => 'handling_fee',
                    "amount" => ($order_details['currency_sign']=='XAF' || 'XOF')?(int)$order_details['handling_fee']:$order_details['handling_fee'],
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'bank',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => trim($transaction_id),
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                }
              //End Update handling fee in gonagoo account master--------------------------

              //Update vehicle fee in gonagoo account master-------------------------------
                if(trim($order_details['vehicle_fee']) > 0) {
                  $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['vehicle_fee']);
                  $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 1,
                    "transaction_type" => 'vehicle_fee',
                    "amount" => ($order_details['currency_sign']=='XAF' || 'XOF')?(int)$order_details['vehicle_fee']:$order_details['vehicle_fee'],
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'bank',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => trim($transaction_id),
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                }
              //End Update vehicle fee in gonagoo account master---------------------------

              //Update custome clearance fee in gonagoo account master---------------------
                if(trim($order_details['custom_clearance_fee']) > 0) {
                  $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['custom_clearance_fee']);
                  $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 1,
                    "transaction_type" => 'custom_clearance_fee',
                    "amount" => ($order_details['currency_sign']=='XAF' || 'XOF')?(int)$order_details['custom_clearance_fee']:$order_details['custom_clearance_fee'],
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'bank',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => trim($transaction_id),
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                }
              //End Update custome clearance fee in gonagoo account master-----------------

              //Update loading Unloading in gonagoo account master-------------------------
                if(trim($order_details['loading_unloading_charges']) > 0) {
                  $gonagoo_balance =  trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['loading_unloading_charges']);
                  $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$cust_id,
                    "type" => 1,
                    "transaction_type" => 'loading_unloading_charges',
                    "amount" => ($order_details['currency_sign']=='XAF' || 'XOF')?(int)$order_details['loading_unloading_charges']:$order_details['loading_unloading_charges'],
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'bank',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => trim($transaction_id),
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                }
              //End Update loading Unloading in gonagoo account master---------------------

              //Update Gonagoo Commission in gonagoo account master------------------------
                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($order_details['commission_amount']);
                      $this->api->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                      $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

                      $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)$order_id,
                  "user_id" => (int)$cust_id,
                  "type" => 1,
                  "transaction_type" => 'commission_amount',
                  "amount" => trim($order_details['commission_amount']),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => 'bank',
                  "transfer_account_number" => 'NULL',
                  "bank_name" => 'NULL',
                  "account_holder_name" => 'NULL',
                  "iban" => 'NULL',
                  "email_address" => 'NULL',
                  "mobile_number" => 'NULL',
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($order_details['currency_sign']),
                  "country_id" => trim($order_details['from_country_id']),
                  "cat_id" => trim($order_details['category_id']),
                      );
                      $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                  //End Update Gonagoo Commission in gonagoo account master--------------------
            //End Add Order Payment to Gonagoo master----------------------------------------

            //Deduct order price from Gonagoo account----------------------------------------
              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($order_details['order_price']);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$deliverer_id,
                "type" => 0,
                "transaction_type" => 'deliver_payment',
                "amount" => trim($order_details['order_price']),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => 'NULL',
                "transfer_account_number" => 'NULL',
                "bank_name" => 'NULL',
                "account_holder_name" => 'NULL',
                "iban" => 'NULL',
                "email_address" => 'NULL',
                "mobile_number" => 'NULL',
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($order_details['currency_sign']),
                "country_id" => trim($order_details['from_country_id']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //End Deduct order price from Gonagoo account------------------------------------

            //Add payment details in Deliverer account master and history--------------------
              if($this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']))) {
                $deliverer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));
              } else {
                $insert_data_customer_master = array(
                  "user_id" => (int)$deliverer_id,
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($order_details['currency_sign']),
                );
                $gonagoo_id = $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                $deliverer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));
              }

              $account_balance = trim($deliverer_account_master_details['account_balance']) + trim($order_details['order_price']);
              $this->api->update_payment_in_customer_master($deliverer_account_master_details['account_id'], $deliverer_id, $account_balance);
              $deliverer_account_master_details = $this->api->customer_account_master_details($deliverer_id, trim($order_details['currency_sign']));

              $update_data_account_history = array(
                "account_id" => (int)$deliverer_account_master_details['account_id'],
                "order_id" => (int)$order_id,
                "user_id" => (int)$deliverer_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'deliver_payment',
                "amount" => trim($order_details['order_price']),
                "account_balance" => trim($deliverer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($order_details['currency_sign']),
                "cat_id" => trim($order_details['category_id']),
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //End Add payment details in Deliverer account master and history----------------

            //Generate Invoice and Email to Customer-----------------------------------------
              $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference("$order_id");
              $invoice->setDate(date('M dS ,Y',time()));

              $customer_country = $this->api->get_country_details(trim($cust_details['country_id']));
              $customer_state = $this->api->get_state_details(trim($cust_details['state_id']));
              $customer_city = $this->api->get_city_details(trim($cust_details['city_id']));

              $deliverer_country = $this->api->get_country_details(trim($deliverer_details['country_id']));
              $deliverer_state = $this->api->get_state_details(trim($deliverer_details['state_id']));
              $deliverer_city = $this->api->get_city_details(trim($deliverer_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($deliverer_details['country_id']));

              $invoice->setFrom(array(trim($cust_details['firstname']) . " " . trim($cust_details['lastname']),trim($customer_city['city_name']),trim($customer_state['state_name']),trim($customer_country['country_name']),trim($cust_details['email1'])));

              $invoice->setTo(array(trim($deliverer_details['firstname']) . " " . trim($deliverer_details['lastname']),trim($deliverer_city['city_name']),trim($deliverer_state['state_name']),trim($deliverer_country['country_name']),trim($deliverer_details['email1'])));

              /* Adding Items in table */
              $order_for = $this->lang->line('courier_delivery');
              $rate = round($order_details['order_price'],2);
              $total = $rate;
              $payment_datetime = substr($order_details['payment_datetime'], 0, 10);
              //set items
              $invoice->addItem("","$payment_date       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('payment_paid'));

              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              //$invoice->addTitle("Other Details");
              /* Add Paragraph */
              //$invoice->addParagraph("Any Other Details");
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch')."\r\n".$deliverer_details['firstname'].' '.$deliverer_details['lastname']."\r\n".$this->lang->line('telephone').$deliverer_details['mobile1']."\r\n".$this->lang->line('email').$deliverer_details['email1']."");
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($order_id.'.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

              //Update File path
              $pdf_name = $order_id.'.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/order-invoices/".$pdf_name);

              //Update Order Invoice and post to workroom
              $this->api->update_order_invoice_url($order_id, "order-invoices/".$pdf_name);
            //-------------------------------------------------------------------------------

            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Booking Invoice');
              $emailAddress = $cust_details['email1'];
              $fileToAttach = "resources/order-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */

            //Post payment and invoice in Workroom-------------------------------------------
              $this->api->post_to_workroom($order_id, $order_details['cust_id'], $order_details['deliverer_id'], $this->lang->line('Order Payment Received.'), 'sp', 'payment', 'NULL', trim($cust_details['firstname']), trim($deliverer_details['firstname']), 'NULL', 'NULL', 'NULL');
              $invoice_path = "order-invoices/".$pdf_name;
              $this->api->post_to_workroom($order_id, $order_details['cust_id'], $order_details['deliverer_id'], $this->lang->line('Order Invoice'), 'sp', 'raise_invoice', trim($invoice_path), trim($cust_details['firstname']), trim($deliverer_details['firstname']), 'pdf', 'NULL', 'NULL');
            //End Post payment and invoice in Workroom---------------------------------------

            //SMS confirmation---------------------------------------------------------------
              $sms_msg = $this->lang->line('Payment recieved against order ID: ').$order_id;
              //SMS to deliverere
              $country_code = $this->api->get_country_code_by_id($deliverer_details['country_id']);
              if(substr(trim($deliverer_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($deliverer_details['mobile1']), 0); } else { $mobile1 = trim($deliverer_details['mobile1']); }
              $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);
              //SMS to Customer
              $country_code = $this->api->get_country_code_by_id($cust_details['country_id']);
              if(substr(trim($cust_details['mobile1']), 0, 1) == 0) { $mobile1 = ltrim(trim($cust_details['mobile1']), 0); } else { $mobile1 = trim($cust_details['mobile1']); }
              $this->api->sendSMS($country_code.trim($mobile1), $sms_msg);
            //SMS confirmation---------------------------------------------------------------

            //Push Notifications-------------------------------------------------------------
              //Get PN API Keys
              $api_key_deliverer = $this->config->item('delivererAppGoogleKey');

              //Get Customer Device ID's
              $device_details_customer = $this->api->get_user_device_details($cust_id);
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              if(!empty($device_details_customer)) {
                foreach ($device_details_customer as $value) {
                  if($value['device_type'] == 1) { array_push($arr_customer_fcm_ids, $value['reg_id']);
                  } else { array_push($arr_customer_apn_ids, $value['reg_id']); }
                }
                $customer_apn_ids = $arr_customer_apn_ids;
              }
              
              $subject = $this->lang->line('invoice_email_message');
              $msg =  array('title' => $subject, 'type' => 'raise_invoice', 'notice_date' => date("Y-m-d H:i:s"), 'attachment_url' => "order-invoices/".$pdf_name, "pdf");
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key_deliverer);

              $msg_apn_customer =  array('title' => $subject, 'text' => $subject);
              if(isset($customer_apn_ids) && is_array($customer_apn_ids)) { 
                $customer_apn_ids = implode(',', $customer_apn_ids);
                $this->users->sendPushIOS($msg_apn_customer, $customer_apn_ids);
              }

              //Get Deliverer Device ID's
              $device_details_deleverer = $this->api->get_user_device_details($deliverer_id);
              $arr_deleverer_fcm_ids = array();
              $arr_deleverer_apn_ids = array();
              if(!empty($device_details_deleverer)) {
                foreach ($device_details_deleverer as $value) {
                  if($value['device_type'] == 1) { array_push($arr_deleverer_fcm_ids, $value['reg_id']);
                  } else { array_push($arr_deleverer_apn_ids, $value['reg_id']); }
                }
                $deleverer_apn_ids = $arr_deleverer_apn_ids;
              }

              $msg =  array('title' => $subject, 'type' => 'raise_invoice', 'notice_date' => date("Y-m-d H:i:s"), 'attachment_url' => "order-invoices/".$pdf_name, "pdf");
              $this->api->sendFCM($msg, $arr_deleverer_fcm_ids, $api_key_deliverer);

              $msg_apn_deliverer =  array('title' => $subject, 'text' => $subject);
              if(isset($deleverer_apn_ids) && is_array($deleverer_apn_ids)) { 
                $deleverer_apn_ids = implode(',', $deleverer_apn_ids);
                $this->users->sendPushIOS($msg_apn_deliverer, $deleverer_apn_ids);
              }
            //Push Notifications-------------------------------------------------------------

            //send commission invoice to Customer--------------------------------------------
              $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference("$order_id");
              $invoice->setDate(date('M dS ,Y',time()));

              $customer_country = $this->api->get_country_details(trim($cust_details['country_id']));
              $customer_state = $this->api->get_state_details(trim($cust_details['state_id']));
              $customer_city = $this->api->get_city_details(trim($cust_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($cust_details['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setFrom(array(trim($cust_details['firstname']) . " " . trim($cust_details['lastname']),trim($customer_city['city_name']),trim($customer_state['state_name']),trim($customer_country['country_name']),trim($cust_details['email1'])));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));

              /* Adding Items in table */
              $order_for = $this->lang->line('gonagoo_commission');
              $rate = round(trim($order_details['commission_amount']),2);
              $total = $rate;
              $payment_datetime = substr($order_details['payment_datetime'], 0, 10);
              //set items
              $invoice->addItem("","$payment_date       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('commission_paid'));

              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              //$invoice->addTitle("Other Details");
              /* Add Paragraph */
              //$invoice->addParagraph("Any Other Details");
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($order_id.'_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

              //Update File path
              $pdf_name = $order_id.'_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/order-invoices/".$pdf_name);

              //Update Order Invoice and post to workroom
              $this->api->update_deliverer_invoice_url($order_id, "order-invoices/".$pdf_name);
            //-------------------------------------------------------------------------------
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $cust_details['email1'];
              $fileToAttach = "resources/order-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */

            //if from address is relay process relay commission------------------------------
              if($order_details['from_addr_type'] == 1) {
                //deduct commission amount from gonagoo account master and history-------
                  $relay_point_details = $this->relayapi->get_relay_point_details_by_id((int)$order_details['from_relay_id']);
                  $commission_percent = trim($relay_point_details['relay_commission']);
                  $relay_commission_amount = round((trim($order_details['commission_amount'])/100)*trim($commission_percent),2);
                  if($order_details['currency_sign'] == 'XAF' || 'XOF') $relay_commission_amount = (int)$relay_commission_amount;

                  if($this->api->gonagoo_master_details(trim($order_details['currency_sign']))) {
                    $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                  } else {
                    $insert_data_gonagoo_master = array(
                      "gonagoo_balance" => 0,
                      "update_datetime" => $today,
                      "operation_lock" => 1,
                      "currency_code" => trim($order_details['currency_sign']),
                    );
                    $gonagoo_id = $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                    $gonagoo_master_details = $this->api->gonagoo_master_details(trim($order_details['currency_sign']));
                  }

                  //echo $this->db->last_query();
                  //echo json_encode($gonagoo_master_details); die();
                  $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($relay_commission_amount);
                  $this->relayapi->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->relayapi->gonagoo_master_details($order_details['currency_sign']);

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$order_details['from_relay_id'],
                    "type" => 0,
                    "transaction_type" => 'relay_commission',
                    "amount" => trim($relay_commission_amount),
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'NULL',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => 'NULL',
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->relayapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                //End deduct commission amount from gonagoo account master and history---

                //Add commission amount in relay account master and history--------------
                  if($this->relayapi->relay_account_master_details($order_details['from_relay_id'], trim($order_details['currency_sign']))) {
                    $relay_account_master_details = $this->relayapi->relay_account_master_details($order_details['from_relay_id'], trim($order_details['currency_sign']));
                  } else {
                    $insert_data_relay_master = array(
                      "user_id" => (int)$order_details['from_relay_id'],
                      "account_balance" => 0,
                      "update_datetime" => $today,
                      "operation_lock" => 1,
                      "currency_code" => trim($order_details['currency_sign']),
                    );
                    $this->relayapi->insert_relay_master_record($insert_data_relay_master);
                  }

                  $relay_account_master_details = $this->relayapi->relay_account_master_details($order_details['from_relay_id'], trim($order_details['currency_sign']));
                  $account_balance = trim($relay_account_master_details['account_balance']) + trim($relay_commission_amount);
                  $this->relayapi->update_payment_in_relay_master($relay_account_master_details['account_id'], $order_details['from_relay_id'], $account_balance);
                  $relay_account_master_details = $this->relayapi->relay_account_master_details($order_details['from_relay_id'], trim($order_details['currency_sign']));

                  $update_data_account_history = array(
                    "account_id" => (int)$relay_account_master_details['account_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$order_details['from_relay_id'],
                    "datetime" => $today,
                    "type" => 1,
                    "transaction_type" => 'relay_commission',
                    "amount" => trim($relay_commission_amount),
                    "account_balance" => trim($relay_account_master_details['account_balance']),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($order_details['currency_sign']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->relayapi->insert_payment_in_relay_account_history($update_data_account_history);
                //End Add commission amount in relay account master and history----------

                //send relay commission invoice to Gonagoo ------------------------------
                  $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
                  $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                  $invoice->setColor("#000");
                  $invoice->setType("");
                  $invoice->setReference("$order_id");
                  $invoice->setDate(date('M dS ,Y',time()));

                  $relay_country = $this->relayapi->get_country_details(trim($relay_point_details['country_id']));
                  $relay_state = $this->relayapi->get_state_details(trim($relay_point_details['state_id']));
                  $relay_city = $this->relayapi->get_city_details(trim($relay_point_details['city_id']));

                  $gonagoo_address = $this->relayapi->get_gonagoo_address(trim($relay_point_details['country_id']));
                  $gonagoo_country = $this->relayapi->get_country_details(trim($gonagoo_address['country_id']));
                  $gonagoo_city = $this->relayapi->get_city_details(trim($gonagoo_address['city_id']));

                  $invoice->setTo(array(trim($relay_point_details['firstname']) . " " . trim($relay_point_details['lastname']),trim($relay_city['city_name']),trim($relay_state['state_name']),trim($relay_country['country_name']),trim($relay_point_details['email'])));

                  $invoice->setFrom(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));

                  /* Adding Items in table */
                  $order_for = $this->lang->line('gonagoo_commission');
                  $rate = round(trim($relay_commission_amount),2);
                  $total = $rate;
                  $payment_datetime = substr($today, 0, 10);
                  //set items
                  $invoice->addItem("","$payment_date       $order_for","","","1",$rate,$total);

                  /* Add totals */
                  $invoice->addTotal($this->lang->line('sub_total'),$total);
                  $invoice->addTotal($this->lang->line('taxes'),'0');
                  $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                  /* Set badge */ 
                  $invoice->addBadge($this->lang->line('commission_paid'));

                  /* Add title */
                  $invoice->addTitle($this->lang->line('tnc'));
                  /* Add Paragraph */
                  $invoice->addParagraph($gonagoo_address['terms']);
                  /* Add title */
                  $invoice->addTitle($this->lang->line('payment_dtls'));
                  /* Add Paragraph */
                  $invoice->addParagraph($gonagoo_address['payment_details']);
                  /* Add title */
                  //$invoice->addTitle("Other Details");
                  /* Add Paragraph */
                  //$invoice->addParagraph("Any Other Details");
                  /* Add title */
                  $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                  /* Add Paragraph */
                  $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                  /* Set footer note */
                  $invoice->setFooternote($gonagoo_address['company_name']);
                  /* Render */
                  $invoice->render($order_id.'_src_relay_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                  //Update File path
                  $pdf_name = $order_id.'_src_relay_commission.pdf';
                  //Move file to upload folder
                  rename($pdf_name, "resources/order-invoices/".$pdf_name);
                  //Update Order Invoice and post to workroom
                  $this->relayapi->update_dest_relay_commission_invoice_url($order_id, "order-invoices/".$pdf_name);
                //-----------------------------------------------------------------------
                /* -----------------------Email Invoice to operator-------------------- */
                  $emailBody = $this->lang->line('Commission Invoice');
                  $gonagooAddress = $gonagoo_address['company_name'];
                  $emailSubject = $this->lang->line('Commission Invoice');
                  $emailAddress = $gonagoo_address['email'];
                  $fileToAttach = "resources/order-invoices/$pdf_name";
                  $fileName = $pdf_name;
                  $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
                /* -----------------------Email Invoice to operator-------------------- */
              }
            //End if from address is relay process relay commission--------------------------

            //if to address is relay process relay commission--------------------------------
              if($order_details['to_addr_type'] == 1) {
                //deduct commission amount from gonagoo account master and history-------
                  $relay_point_details = $this->relayapi->get_relay_point_details_by_id($order_details['to_relay_id']);
                  $commission_percent = trim($relay_point_details['relay_commission']);
                  $relay_commission_amount = round((trim($order_details['commission_amount'])/100)*trim($commission_percent),2);
                  if($order_details['currency_sign'] == 'XAF' || 'XOF') $relay_commission_amount = (int)$relay_commission_amount;
                  $gonagoo_master_details = $this->relayapi->gonagoo_master_details($order_details['currency_sign']);
                  $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) - trim($relay_commission_amount);
                  $this->relayapi->update_payment_in_gonagoo_master((int)$gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->relayapi->gonagoo_master_details($order_details['currency_sign']);

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$order_details['to_relay_id'],
                    "type" => 0,
                    "transaction_type" => 'relay_commission',
                    "amount" => trim($relay_commission_amount),
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => 'NULL',
                    "transfer_account_number" => 'NULL',
                    "bank_name" => 'NULL',
                    "account_holder_name" => 'NULL',
                    "iban" => 'NULL',
                    "email_address" => 'NULL',
                    "mobile_number" => 'NULL',
                    "transaction_id" => 'NULL',
                    "currency_code" => trim($order_details['currency_sign']),
                    "country_id" => trim($order_details['from_country_id']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->relayapi->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                //End deduct commission amount from gonagoo account master and history---

                //Add commission amount in relay account master and history--------------
                  if($this->relayapi->relay_account_master_details($order_details['to_relay_id'], trim($order_details['currency_sign']))) {
                    $relay_account_master_details = $this->relayapi->relay_account_master_details($order_details['to_relay_id'], trim($order_details['currency_sign']));
                  } else {
                    $insert_data_relay_master = array(
                      "user_id" => (int)$order_details['to_relay_id'],
                      "account_balance" => 0,
                      "update_datetime" => $today,
                      "operation_lock" => 1,
                      "currency_code" => trim($order_details['currency_sign']),
                    );
                    $this->relayapi->insert_relay_master_record($insert_data_relay_master);
                  }

                  $relay_account_master_details = $this->relayapi->relay_account_master_details($order_details['to_relay_id'], trim($order_details['currency_sign']));
                  $account_balance = trim($relay_account_master_details['account_balance']) + trim($relay_commission_amount);
                  $this->relayapi->update_payment_in_relay_master($relay_account_master_details['account_id'], $order_details['to_relay_id'], $account_balance);
                  $relay_account_master_details = $this->relayapi->relay_account_master_details($order_details['to_relay_id'], trim($order_details['currency_sign']));

                  $update_data_account_history = array(
                    "account_id" => (int)$relay_account_master_details['account_id'],
                    "order_id" => (int)$order_id,
                    "user_id" => (int)$order_details['to_relay_id'],
                    "datetime" => $today,
                    "type" => 1,
                    "transaction_type" => 'relay_commission',
                    "amount" => trim($relay_commission_amount),
                    "account_balance" => trim($relay_account_master_details['account_balance']),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($order_details['currency_sign']),
                    "cat_id" => trim($order_details['category_id']),
                  );
                  $this->relayapi->insert_payment_in_relay_account_history($update_data_account_history);
                //End Add commission amount in relay account master and history----------

                //send relay commission invoice to Gonagoo ------------------------------
                  $invoice = new phpinvoice('A4',trim($order_details['currency_sign']),$invoice_lang);
                  $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                  $invoice->setColor("#000");
                  $invoice->setType("");
                  $invoice->setReference("$order_id");
                  $invoice->setDate(date('M dS ,Y',time()));

                  $relay_country = $this->relayapi->get_country_details(trim($relay_point_details['country_id']));
                  $relay_state = $this->relayapi->get_state_details(trim($relay_point_details['state_id']));
                  $relay_city = $this->relayapi->get_city_details(trim($relay_point_details['city_id']));

                  $gonagoo_address = $this->relayapi->get_gonagoo_address(trim($relay_point_details['country_id']));
                  $gonagoo_country = $this->relayapi->get_country_details(trim($gonagoo_address['country_id']));
                  $gonagoo_city = $this->relayapi->get_city_details(trim($gonagoo_address['city_id']));

                  $invoice->setTo(array(trim($relay_point_details['firstname']) . " " . trim($relay_point_details['lastname']),trim($relay_city['city_name']),trim($relay_state['state_name']),trim($relay_country['country_name']),trim($relay_point_details['email'])));

                  $invoice->setFrom(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));

                  /* Adding Items in table */
                  $order_for = $this->lang->line('gonagoo_commission');
                  $rate = round(trim($relay_commission_amount),2);
                  $total = $rate;
                  $payment_datetime = substr($today, 0, 10);
                  //set items
                  $invoice->addItem("","$payment_date       $order_for","","","1",$rate,$total);

                  /* Add totals */
                  $invoice->addTotal($this->lang->line('sub_total'),$total);
                  $invoice->addTotal($this->lang->line('taxes'),'0');
                  $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                  /* Set badge */ 
                  $invoice->addBadge($this->lang->line('commission_paid'));

                  /* Add title */
                  $invoice->addTitle($this->lang->line('tnc'));
                  /* Add Paragraph */
                  $invoice->addParagraph($gonagoo_address['terms']);
                  /* Add title */
                  $invoice->addTitle($this->lang->line('payment_dtls'));
                  /* Add Paragraph */
                  $invoice->addParagraph($gonagoo_address['payment_details']);
                  /* Add title */
                  //$invoice->addTitle("Other Details");
                  /* Add Paragraph */
                  //$invoice->addParagraph("Any Other Details");
                  /* Add title */
                  $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                  /* Add Paragraph */
                  $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                  /* Set footer note */
                  $invoice->setFooternote($gonagoo_address['company_name']);
                  /* Render */
                  $invoice->render($order_id.'_dest_relay_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                  //Update File path
                  $pdf_name = $order_id.'_dest_relay_commission.pdf';
                  //Move file to upload folder
                  rename($pdf_name, "resources/order-invoices/".$pdf_name);
                  //Update Order Invoice and post to workroom
                  $this->relayapi->update_dest_relay_commission_invoice_url($order_id, "order-invoices/".$pdf_name);
                //-----------------------------------------------------------------------
                /* -----------------------Email Invoice to operator-------------------- */
                  $emailBody = $this->lang->line('Commission Invoice');
                  $gonagooAddress = $gonagoo_address['company_name'];
                  $emailSubject = $this->lang->line('Commission Invoice');
                  $emailAddress = $gonagoo_address['email'];
                  $fileToAttach = "resources/order-invoices/$pdf_name";
                  $fileName = $pdf_name;
                  $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
                /* -----------------------Email Invoice to operator-------------------- */
              }
            //End if to address is relay process relay commission----------------------------
          //Payment Section--------------------------------------------------------------------
          $this->session->set_flashdata('success', 'Order payment completed successfully!');
        } else { $this->session->set_flashdata('error', 'Failed to update payment details!'); }
      } else { $this->session->set_flashdata('error', 'Order already paid!'); }
    } else { $this->session->set_flashdata('error', 'Order not completed yet!'); }
    redirect('admin/users/pending-orders/'.$order_details['cust_id']);
  }
}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */