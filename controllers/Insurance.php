<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insurance extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin');   
      $this->load->model('insurance_model', 'insurance');   
      $this->load->model('standard_dimension_model', 'dimension');
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $insurance = $this->insurance->get_insurance();
    $this->load->view('admin/insurance_list_view', compact('insurance'));     
    $this->load->view('admin/footer_view');   
  }

  public function add_insurance()
  {
                $categories = $this->dimension->get_categories();
    $countries = $this->insurance->get_countries();
    $this->load->view('admin/insurance_add_view', compact('countries','categories'));
    $this->load->view('admin/footer_view');
  }

  public function register_insurance()
  {
    //echo json_encode($_POST); die();
    $min_value = $this->input->post('min_value', TRUE);
    $max_value = $this->input->post('max_value', TRUE);
    $ins_fee = $this->input->post('ins_fee', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $category_ids = $this->input->post('category', TRUE);
    $commission = $this->input->post('commission', TRUE);
    $insert_flag = false;
    
    $country_details = $this->insurance->get_country_details($country_id);
    $country_currency_details = $this->insurance->get_country_currency_details($country_id);
    $currency_details = $this->insurance->get_currency_details((int)trim($country_currency_details['currency_id']));

    foreach ($category_ids as $id) {
      $insert_data = array(
        "min_value" => (int)trim($min_value), 
        "max_value" => (int)trim($max_value), 
        "ins_fee" => (int)trim($ins_fee), 
        "commission_percent" => (int)trim($commission), 
        "category_id" => trim($id), 
        "country_id" => (int)trim($country_id), 
        "country_name" => trim($country_details['country_name']), 
        "currency_id" => (int)trim($country_currency_details['currency_id']), 
        "currency_sign" => trim($currency_details['currency_sign']), 
        "currency_title" => trim($currency_details['currency_title']), 
      );
      if(! $this->insurance->check_min_insurance($insert_data) && ! $this->insurance->check_max_insurance($insert_data) ) {
        if($this->insurance->register_insurance($insert_data)){ $insert_flag = true;  }           
        else{ $insert_flag = false; }
      } else { $insert_flag = false; }
    }

    if($insert_flag){ $this->session->set_flashdata('success','Insurance successfully added!'); }
    else{ $this->session->set_flashdata('error','Unable to add insurance, configuration already exists for selected country and category!');  }

    redirect('admin/add-insurance');
  }

  public function delete()
  {
    $ins_id = $this->input->post('id', TRUE);

    if($this->insurance->delete_insurance($ins_id)){ echo 'success'; } else { echo "failed";  } 
  }

  public function edit_insurance()
  {
    if( !is_null($this->input->post('ins_id')) ) { $ins_id = (int) $this->input->post('ins_id'); }
    else if(!is_null($this->uri->segment(3))) { $ins_id = (int)$this->uri->segment(3); }
    else { redirect('admin/insurance-master'); }
    $countries = $this->insurance->get_countries();
    $insurance_details = $this->insurance->get_insurance($ins_id);
    $this->load->view('admin/insurance_edit_view', compact('insurance_details', 'countries'));
    $this->load->view('admin/footer_view');
  }

  public function update_insurance()
  { 
    $ins_id = (int) $this->input->post('ins_id');
    $min_value = $this->input->post('min_value', TRUE);
    $max_value = $this->input->post('max_value', TRUE);
    $ins_fee = $this->input->post('ins_fee', TRUE);
    $commission = $this->input->post('commission', TRUE);

    $update_data = array(
      "min_value" => trim($min_value), 
      "max_value" => trim($max_value), 
      "ins_fee" => trim($ins_fee), 
      "commission_percent" => trim($commission), 
    );

    if(! $this->insurance->check_insurance_excluding($update_data, $ins_id) ) {
        if($this->insurance->update_insurance($update_data, $ins_id)){
          $this->session->set_flashdata('success','Insurance successfully updated!');
        } else {  $this->session->set_flashdata('error','Unable to update insurance details! Try again...');  }  
    } else {  $this->session->set_flashdata('error','No change found!');  }
    
    redirect('admin/edit-insurance/'.$ins_id);
  }

}

/* End of file Insurance.php */
/* Location: ./application/controllers/Insurance.php */