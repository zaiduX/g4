<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laundry_categories extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('laundry_categories_model', 'category');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$user = $this->admin->logged_in_user_details($user['type_id']);
			$permissions = $this->admin->get_auth_permissions($id);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}

	public function index()
	{
		$categories = $this->category->get_sub_categories();
		$this->load->view('admin/laundry_sub_category_list_view', compact('categories'));			
		$this->load->view('admin/footer_view');	
	}

	public function add()
	{
		$category = $this->category->get_categories();
		$this->load->view('admin/laundry_category_add_view',compact('category'));
		$this->load->view('admin/footer_view');
	}
	
	public function register()
	{
		$cat_id = $this->input->post('cat_id', TRUE);
    	$sub_cat_name = $this->input->post('sub_cat_name', TRUE);
    	$height = $this->input->post('height', TRUE);
    	$width = $this->input->post('width', TRUE);
    	$length = $this->input->post('length', TRUE);
    	$weight = $this->input->post('weight', TRUE);

    	if(!empty($_FILES["icon_url"]["tmp_name"]) ) {
			$config['upload_path']    = 'resources/laundry-category-icons/';
			$config['allowed_types']  = '*';
			$config['max_size']       = '0';
			$config['max_width']      = '0';
			$config['max_height']     = '0';
			$config['encrypt_name']   = true; 
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload('icon_url')) {   
				$uploads    = $this->upload->data();  
				$icon_url =  $config['upload_path'].$uploads["file_name"];  
        	} else { $icon_url = 'NULL'; }
        } else { $icon_url = 'NULL'; }

  		$insert_data = array(
  			"cat_id" => trim($cat_id), 
  			"sub_cat_name" =>  trim($sub_cat_name), 
  			"height" =>  trim($height), 
  			"width" =>  trim($width), 
  			"length" =>  trim($length), 
  			"weight" =>  trim($weight), 
  			"icon_url" =>  trim($icon_url), 
  			"cre_datetime" =>  date("Y-m-d H:i:s"), 
  		);
  		//echo json_encode($insert_data); die();
		if(!$this->category->check_sub_category($insert_data) ) {
			if($this->category->register_sub_category($insert_data)){
				$this->session->set_flashdata('success','Laundry sub category successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new sub category! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Sub category already exists!');	}
		redirect('admin/laundry-categories-add');
	}

	public function edit()
	{	

		if( $id = $this->uri->segment(4) ) { 			
			if( $sub_category = $this->category->get_sub_category_detail($id)){
				$category = $this->category->get_categories();
				$this->load->view('admin/laundry_category_edit_veiw', compact('category','sub_category'));
				$this->load->view('admin/footer_view');
			} else { redirect('admin/laundry-categories'); }
		} else { redirect('admin/laundry-categories'); }
	}

	public function update()
	{
		$sub_cat_id = $this->input->post('sub_cat_id', TRUE);
		$cat_id = $this->input->post('cat_id', TRUE);
		$sub_cat_name = $this->input->post('sub_cat_name', TRUE);
		$height = $this->input->post('height', TRUE);
    	$width = $this->input->post('width', TRUE);
    	$length = $this->input->post('length', TRUE);
    	$weight = $this->input->post('weight', TRUE);
    	$details = $this->category->get_sub_category_detail($sub_cat_id);

  		$check_data = array(
  			"cat_id" =>  trim($cat_id), 
  			"sub_cat_id" =>  trim($sub_cat_id), 
  			"sub_cat_name" => trim($sub_cat_name), 
  		);

		if(! $this->category->check_sub_category_excluding($check_data) ) {
			if(!empty($_FILES["icon_url"]["tmp_name"]) ) {
				$config['upload_path']    = 'resources/laundry-category-icons/';
				$config['allowed_types']  = '*';
				$config['max_size']       = '0';
				$config['max_width']      = '0';
				$config['max_height']     = '0';
				$config['encrypt_name']   = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('icon_url')) { 
					$uploads    = $this->upload->data();  
					$icon_url =  $config['upload_path'].$uploads["file_name"]; 
					if($details['icon_url'] != "NULL") {
		                unlink(trim($details['icon_url']));
		            } 
	        	} else { $icon_url = 'NULL'; }
	        } else { $icon_url = 'NULL'; }

			$update_data = array(
	  			"cat_id" =>  trim($cat_id), 
	  			"sub_cat_name" => trim($sub_cat_name), 
	  			"height" =>  trim($height), 
	  			"width" =>  trim($width), 
	  			"length" =>  trim($length), 
	  			"weight" =>  trim($weight), 
	  			"icon_url" =>  trim($icon_url),
	  		);
			if($this->category->update_sub_category($update_data, $sub_cat_id)){
				$this->session->set_flashdata('success','Laundry category successfully updated!');
			} else { 	$this->session->set_flashdata('error','Unable to update laundry category! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Laundry category already exists!');	}

		redirect('admin/laundry-categories/edit/'.$sub_cat_id);
	}

	public function active_sub_category()
	{
		$id = (int) $this->input->post('id', TRUE);
		if( $this->category->activate_sub_category($id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

	public function inactive_sub_category()
	{
		$id = (int) $this->input->post('id', TRUE);
		if( $this->category->inactivate_sub_category($id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

}

/* End of file Laundry_categories.php */
/* Location: ./application/controllers/Laundry_categories.php */