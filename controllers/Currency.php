<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('Admin_model', 'admin');		
			$this->load->model('Currency_model', 'currency');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));		
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function currency_master()
	{		
		$currency_master = $this->currency->get_currencies();
		$this->load->view('admin/currency_master_list_view', compact('currency_master'));			
		$this->load->view('admin/footer_view');		
	}

	public function add_currency_master()
	{
		$this->load->view('admin/currency_master_add_view');
		$this->load->view('admin/footer_view');
	}

	public function edit_currency_master()
	{	
		$currency_id = (int) $this->uri->segment('3');
		$currency_dtls = $this->currency->get_currency_detail($currency_id);
		$this->load->view('admin/currency_master_edit_view', compact('currency_dtls'));
		$this->load->view('admin/footer_view');
	}

	public function update_currency_master()
	{
		$currency_id = $this->input->post('currency_id', TRUE);
		$currency_title = $this->input->post('currency_title', TRUE);
		$currency_sign = $this->input->post('currency_sign', TRUE);
		$currency_details = $this->input->post('currency_details', TRUE); 
		$update_data = array(
			'currency_id' => (int) $currency_id,
			'currency_title' => $currency_title,
			'currency_sign' => $currency_sign,
			'currency_details' => $currency_details
		);
		if(! $this->currency->get_currency_verification($currency_title, $currency_sign) ) {
			if( $this->currency->update_currency_details($update_data)){
				$this->session->set_flashdata('success','Currency successfully updated!');
			}	else { 	$this->session->set_flashdata('error','Unable to update currency! Try again...');	}
		} else { 	$this->session->set_flashdata('error','Currency already exists! Try with different currency...');	}
		redirect('admin/edit-currency-master/'. $currency_id);
	}

	public function register_currency_master()
	{
		$currency_title = $this->input->post('currency_title', TRUE);
		$currency_sign = $this->input->post('currency_sign', TRUE);
		$currency_details = $this->input->post('currency_details', TRUE); 
		$insertdata = array(
			'currency_title' => trim($currency_title),
			'currency_sign' => trim($currency_sign),
			'currency_details' => trim($currency_details)
		);
		if(! $this->currency->get_currency_verification($currency_title, $currency_sign) ) {
			if($this->currency->register_currency($insertdata)) {
				$this->session->set_flashdata('success','Currency type successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new currency! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Currency already exists! Try again with different currency...');	}
		redirect('admin/add-currency-master');
	}

	public function active()
	{
		$id = (int) $this->input->post('id', TRUE);
		if( $this->currency->activate($id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	public function inactive()
	{
		$id = (int) $this->input->post('id', TRUE);
		if( $this->currency->inactivate($id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

}

/* End of file Currency.php */
/* Location: ./application/controllers/Currency.php */