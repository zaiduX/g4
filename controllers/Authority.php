<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authority extends CI_Controller {
  
  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('Authority_model', 'authorities');
      $this->load->model('web_user_model', 'user');     
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }
  
  public function index()
  {
    if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
  }

  public function authority_master()
  {   
    $authorities_details = $this->authorities->get_authorities();
    $this->load->view('admin/authority_list_view', compact('authorities_details'));     
    $this->load->view('admin/footer_view');   
  }

  public function edit_authority()
  { 
    $auth_type_list = $this->authorities->get_authority_type();
    $countries = $this->user->get_countries();
    $auth_id = (int) $this->uri->segment('3');
    $authorities = $this->authorities->get_authority_detail($auth_id);
    $this->load->view('admin/authority_edit_view', compact('authorities', 'auth_type_list','countries'));
    $this->load->view('admin/footer_view');
  }
  
  public function add_authority()
  {
    $auth_type_list = $this->authorities->get_authority_type();
    $countries = $this->user->get_countries();
    $this->load->view('admin/authority_add_view', compact('auth_type_list','countries'));
    $this->load->view('admin/footer_view');
  }

  public function assign_permission()
  { 
    $auth_type_list = $this->authorities->get_authority_type();
    $auth_id = (int) $this->uri->segment('3');
    $authorities = $this->authorities->get_authority_detail($auth_id);
    $user_permissions = $this->admin->get_auth_permissions($auth_id);
    $this->load->view('admin/authority_permission_view', compact('authorities', 'auth_type_list','user_permissions'));
    $this->load->view('admin/footer_view');
  }

  public function update_authority_permissions()
  {
    $auth_id = $this->input->post('auth_id', TRUE);
    //category_type permission
    if($this->input->post('category_type_view', TRUE) == '') { $category_type_view = 0; } else { $category_type_view = $this->input->post('category_type_view', TRUE); }
    if($this->input->post('category_type_add', TRUE) == '') { $category_type_add = 0; } else { $category_type_add = $this->input->post('category_type_add', TRUE); }
    if($this->input->post('category_type_edit', TRUE) == '') { $category_type_edit = 0; } else { $category_type_edit = $this->input->post('category_type_edit', TRUE); }
    if($this->input->post('category_type_active', TRUE) == '') { $category_type_active = 0; } else { $category_type_active = $this->input->post('category_type_active', TRUE); }
    $category_type = $category_type_view.','.$category_type_add.','.$category_type_edit.','.$category_type_active;

    //category permission
    if($this->input->post('category_view', TRUE) == '') { $category_view = 0; } else { $category_view = $this->input->post('category_view', TRUE); }
    if($this->input->post('category_add', TRUE) == '') { $category_add = 0; } else { $category_add = $this->input->post('category_add', TRUE); }
    if($this->input->post('category_edit', TRUE) == '') { $category_edit = 0; } else { $category_edit = $this->input->post('category_edit', TRUE); }
    if($this->input->post('category_active', TRUE) == '') { $category_active = 0; } else { $category_active = $this->input->post('category_active', TRUE); }
    $categories = $category_view.','.$category_add.','.$category_edit.','.$category_active;

    //currency master permission
    if($this->input->post('currency_master_view', TRUE) == '') { $currency_master_view = 0; } else { $currency_master_view = $this->input->post('currency_master_view', TRUE); }
    if($this->input->post('currency_master_add', TRUE) == '') { $currency_master_add = 0; } else { $currency_master_add = $this->input->post('currency_master_add', TRUE); }
    if($this->input->post('currency_master_edit', TRUE) == '') { $currency_master_edit = 0; } else { $currency_master_edit = $this->input->post('currency_master_edit', TRUE); }
    if($this->input->post('currency_master_active', TRUE) == '') { $currency_master_active = 0; } else { $currency_master_active = $this->input->post('currency_master_active', TRUE); }
    if($this->input->post('currency_master_delete', TRUE) == '') { $currency_master_delete = 0; } else { $currency_master_delete = $this->input->post('currency_master_delete', TRUE); }
    $currency_master = $currency_master_view.','.$currency_master_add.','.$currency_master_edit.','.$currency_master_active.','.$currency_master_delete;

    //country currency master permission
    if($this->input->post('country_currency_view', TRUE) == '') { $country_currency_view = 0; } else { $country_currency_view = $this->input->post('country_currency_view', TRUE); }
    if($this->input->post('country_currency_add', TRUE) == '') { $country_currency_add = 0; } else { $country_currency_add = $this->input->post('country_currency_add', TRUE); }
    if($this->input->post('country_currency_edit', TRUE) == '') { $country_currency_edit = 0; } else { $country_currency_edit = $this->input->post('country_currency_edit', TRUE); }
    if($this->input->post('country_currency_active', TRUE) == '') { $country_currency_active = 0; } else { $country_currency_active = $this->input->post('country_currency_active', TRUE); }
    $country_currency = $country_currency_view.','.$country_currency_add.','.$country_currency_edit.','.$country_currency_active;

    //Authority type permission
    if($this->input->post('authority_type_view', TRUE) == '') { $authority_type_view = 0; } else { $authority_type_view = $this->input->post('authority_type_view', TRUE); }
    if($this->input->post('authority_type_add', TRUE) == '') { $authority_type_add = 0; } else { $authority_type_add = $this->input->post('authority_type_add', TRUE); }
    if($this->input->post('authority_type_edit', TRUE) == '') { $authority_type_edit = 0; } else { $authority_type_edit = $this->input->post('authority_type_edit', TRUE); }
    if($this->input->post('authority_type_active', TRUE) == '') { $authority_type_active = 0; } else { $authority_type_active = $this->input->post('authority_type_active', TRUE); }
    $authority_type = $authority_type_view.','.$authority_type_add.','.$authority_type_edit.','.$authority_type_active;

    //Authority permission
    if($this->input->post('manage_authority_view', TRUE) == '') { $manage_authority_view = 0; } else { $manage_authority_view = $this->input->post('manage_authority_view', TRUE); }
    if($this->input->post('manage_authority_add', TRUE) == '') { $manage_authority_add = 0; } else { $manage_authority_add = $this->input->post('manage_authority_add', TRUE); }
    if($this->input->post('manage_authority_edit', TRUE) == '') { $manage_authority_edit = 0; } else { $manage_authority_edit = $this->input->post('manage_authority_edit', TRUE); }
    if($this->input->post('manage_authority_active', TRUE) == '') { $manage_authority_active = 0; } else { $manage_authority_active = $this->input->post('manage_authority_active', TRUE); }
    $manage_authority = $manage_authority_view.','.$manage_authority_add.','.$manage_authority_edit.','.$manage_authority_active;

    //Users permission
    if($this->input->post('manage_users_view', TRUE) == '') { $manage_users_view = 0; } else { $manage_users_view = $this->input->post('manage_users_view', TRUE); }
    if($this->input->post('manage_users_add', TRUE) == '') { $manage_users_add = 0; } else { $manage_users_add = $this->input->post('manage_users_add', TRUE); }
    if($this->input->post('manage_users_edit', TRUE) == '') { $manage_users_edit = 0; } else { $manage_users_edit = $this->input->post('manage_users_edit', TRUE); }
    if($this->input->post('manage_users_active', TRUE) == '') { $manage_users_active = 0; } else { $manage_users_active = $this->input->post('manage_users_active', TRUE); }
    $manage_users = $manage_users_view.','.$manage_users_add.','.$manage_users_edit.','.$manage_users_active;

    //Notification
    if($this->input->post('manage_notifications_view', TRUE) == '') { $manage_notifications_view = 0; } else { $manage_notifications_view = $this->input->post('manage_notifications_view', TRUE); }
    if($this->input->post('manage_notifications_add', TRUE) == '') { $manage_notifications_add = 0; } else { $manage_notifications_add = $this->input->post('manage_notifications_add', TRUE); }
    $manage_notifications = $manage_notifications_view.','.$manage_notifications_add;

    //Promo Code
    if($this->input->post('manage_promo_view', TRUE) == '') { $manage_promo_view = 0; } else { $manage_promo_view = $this->input->post('manage_promo_view', TRUE); }
    if($this->input->post('manage_promo_add', TRUE) == '') { $manage_promo_add = 0; } else { $manage_promo_add = $this->input->post('manage_promo_add', TRUE); }
    if($this->input->post('manage_promo_edit', TRUE) == '') { $manage_promo_edit = 0; } else { $manage_promo_edit = $this->input->post('manage_promo_edit', TRUE); }
    if($this->input->post('manage_promo_active', TRUE) == '') { $manage_promo_active = 0; } else { $manage_promo_active = $this->input->post('manage_promo_active', TRUE); }
    $manage_promo = $manage_promo_view.','.$manage_promo_add.','.$manage_promo_edit.','.$manage_promo_active;

    //Promo Code Service
    if($this->input->post('manage_promo_service_view', TRUE) == '') { $manage_promo_service_view = 0; } else { $manage_promo_service_view = $this->input->post('manage_promo_service_view', TRUE); }
    $manage_promo_service = $manage_promo_service_view;

    //Standard Dimension 
    if($this->input->post('manage_dimension_view', TRUE) == '') { $manage_dimension_view = 0; } else { $manage_dimension_view = $this->input->post('manage_dimension_view', TRUE); }
    if($this->input->post('manage_dimension_add', TRUE) == '') { $manage_dimension_add = 0; } else { $manage_dimension_add = $this->input->post('manage_dimension_add', TRUE); }
    if($this->input->post('manage_dimension_edit', TRUE) == '') { $manage_dimension_edit = 0; } else { $manage_dimension_edit = $this->input->post('manage_dimension_edit', TRUE); }
    if($this->input->post('manage_dimension_delete', TRUE) == '') { $manage_dimension_delete = 0; } else { $manage_dimension_delete = $this->input->post('manage_dimension_delete', TRUE); }
    $manage_dimension = $manage_dimension_view.','.$manage_dimension_add.','.$manage_dimension_edit.','.$manage_dimension_delete;

    //Vehicles
    if($this->input->post('manage_vehicals_view', TRUE) == '') { $manage_vehicals_view = 0; } else { $manage_vehicals_view = $this->input->post('manage_vehicals_view', TRUE); }
    if($this->input->post('manage_vehicals_add', TRUE) == '') { $manage_vehicals_add = 0; } else { $manage_vehicals_add = $this->input->post('manage_vehicals_add', TRUE); }
    if($this->input->post('manage_vehicals_edit', TRUE) == '') { $manage_vehicals_edit = 0; } else { $manage_vehicals_edit = $this->input->post('manage_vehicals_edit', TRUE); }
    if($this->input->post('manage_vehicals_delete', TRUE) == '') { $manage_vehicals_delete = 0; } else { $manage_vehicals_delete = $this->input->post('manage_vehicals_delete', TRUE); }
    $manage_vehicals = $manage_vehicals_view.','.$manage_vehicals_add.','.$manage_vehicals_edit.','.$manage_vehicals_delete;

    //Standard Rates
    if($this->input->post('manage_rates_view', TRUE) == '') { $manage_rates_view = 0; } else { $manage_rates_view = $this->input->post('manage_rates_view', TRUE); }
    if($this->input->post('manage_rates_add', TRUE) == '') { $manage_rates_add = 0; } else { $manage_rates_add = $this->input->post('manage_rates_add', TRUE); }
    if($this->input->post('manage_rates_edit', TRUE) == '') { $manage_rates_edit = 0; } else { $manage_rates_edit = $this->input->post('manage_rates_edit', TRUE); }
    if($this->input->post('manage_rates_delete', TRUE) == '') { $manage_rates_delete = 0; } else { $manage_rates_delete = $this->input->post('manage_rates_delete', TRUE); }
    $manage_rates = $manage_rates_view.','.$manage_rates_add.','.$manage_rates_edit.','.$manage_rates_delete;

    //Driver Category
    if($this->input->post('manage_driver_category_view', TRUE) == '') { $manage_driver_category_view = 0; } else { $manage_driver_category_view = $this->input->post('manage_driver_category_view', TRUE); }
    if($this->input->post('manage_driver_category_add', TRUE) == '') { $manage_driver_category_add = 0; } else { $manage_driver_category_add = $this->input->post('manage_driver_category_add', TRUE); }
    if($this->input->post('manage_driver_category_edit', TRUE) == '') { $manage_driver_category_edit = 0; } else { $manage_driver_category_edit = $this->input->post('manage_driver_category_edit', TRUE); }
    if($this->input->post('manage_driver_category_delete', TRUE) == '') { $manage_driver_category_delete = 0; } else { $manage_driver_category_delete = $this->input->post('manage_driver_category_delete', TRUE); }
    $manage_driver_category = $manage_driver_category_view.','.$manage_driver_category_add.','.$manage_driver_category_edit.','.$manage_driver_category_delete;

    //Advance Payment
    if($this->input->post('manage_advance_payment_view', TRUE) == '') { $manage_advance_payment_view = 0; } else { $manage_advance_payment_view = $this->input->post('manage_advance_payment_view', TRUE); }
    if($this->input->post('manage_advance_payment_add', TRUE) == '') { $manage_advance_payment_add = 0; } else { $manage_advance_payment_add = $this->input->post('manage_advance_payment_add', TRUE); }
    if($this->input->post('manage_advance_payment_edit', TRUE) == '') { $manage_advance_payment_edit = 0; } else { $manage_advance_payment_edit = $this->input->post('manage_advance_payment_edit', TRUE); }
    if($this->input->post('manage_advance_payment_delete', TRUE) == '') { $manage_advance_payment_delete = 0; } else { $manage_advance_payment_delete = $this->input->post('manage_advance_payment_delete', TRUE); }
    $manage_advance_payment = $manage_advance_payment_view.','.$manage_advance_payment_add.','.$manage_advance_payment_edit.','.$manage_advance_payment_delete;

    //Skill permission 
    if($this->input->post('manage_skills_master_view', TRUE) == '') { $manage_skills_master_view = 0; } else { $manage_skills_master_view = $this->input->post('manage_skills_master_view', TRUE); }
    if($this->input->post('manage_skills_master_add', TRUE) == '') { $manage_skills_master_add = 0; } else { $manage_skills_master_add = $this->input->post('manage_skills_master_add', TRUE); }
    if($this->input->post('manage_skills_master_edit', TRUE) == '') { $manage_skills_master_edit = 0; } else { $manage_skills_master_edit = $this->input->post('manage_skills_master_edit', TRUE); }
    if($this->input->post('manage_skills_master_active', TRUE) == '') { $manage_skills_master_active = 0; } else { $manage_skills_master_active = $this->input->post('manage_skills_master_active', TRUE); }
    $manage_skills_master = $manage_skills_master_view.','.$manage_skills_master_add.','.$manage_skills_master_edit.','.$manage_skills_master_active;

    //Insurance permission 
    if($this->input->post('manage_insurance_master_view', TRUE) == '') { $manage_insurance_master_view = 0; } else { $manage_insurance_master_view = $this->input->post('manage_insurance_master_view', TRUE); }
    if($this->input->post('manage_insurance_master_add', TRUE) == '') { $manage_insurance_master_add = 0; } else { $manage_insurance_master_add = $this->input->post('manage_insurance_master_add', TRUE); }
    if($this->input->post('manage_insurance_master_edit', TRUE) == '') { $manage_insurance_master_edit = 0; } else { $manage_insurance_master_edit = $this->input->post('manage_insurance_master_edit', TRUE); }
    if($this->input->post('manage_insurance_master_active', TRUE) == '') { $manage_insurance_master_active = 0; } else { $manage_insurance_master_active = $this->input->post('manage_insurance_master_active', TRUE); }
    $manage_insurance_master = $manage_insurance_master_view.','.$manage_insurance_master_add.','.$manage_insurance_master_edit.','.$manage_insurance_master_active;

    //Relay Point permission 
    if($this->input->post('manage_relay_point_view', TRUE) == '') { $manage_relay_point_view = 0; } else { $manage_relay_point_view = $this->input->post('manage_relay_point_view', TRUE); }
    if($this->input->post('manage_relay_point_add', TRUE) == '') { $manage_relay_point_add = 0; } else { $manage_relay_point_add = $this->input->post('manage_relay_point_add', TRUE); }
    if($this->input->post('manage_relay_point_edit', TRUE) == '') { $manage_relay_point_edit = 0; } else { $manage_relay_point_edit = $this->input->post('manage_relay_point_edit', TRUE); }
    if($this->input->post('manage_relay_point_active', TRUE) == '') { $manage_relay_point_active = 0; } else { $manage_relay_point_active = $this->input->post('manage_relay_point_active', TRUE); }
    $manage_relay_point = $manage_relay_point_view.','.$manage_relay_point_add.','.$manage_relay_point_edit.','.$manage_relay_point_active;

    //Withdraw request permission 
    if($this->input->post('manage_withdraw_request_view', TRUE) == '') { $manage_withdraw_request_view = 0; } else { $manage_withdraw_request_view = $this->input->post('manage_withdraw_request_view', TRUE); }
    if($this->input->post('manage_withdraw_request_active', TRUE) == '') { $manage_withdraw_request_active = 0; } else { $manage_withdraw_request_active = $this->input->post('manage_withdraw_request_active', TRUE); }
    $manage_withdraw_request = $manage_withdraw_request_view.','.$manage_withdraw_request_active;

    //Document Verification permission 
    if($this->input->post('manage_verify_doc_view', TRUE) == '') { $manage_verify_doc_view = 0; } else { $manage_verify_doc_view = $this->input->post('manage_verify_doc_view', TRUE); }
    $manage_verify_doc = $manage_verify_doc_view;

    //Document Verification permission 
    if($this->input->post('manage_all_orders_view', TRUE) == '') { $manage_all_orders_view = 0; } else { $manage_all_orders_view = $this->input->post('manage_all_orders_view', TRUE); }
    $manage_all_orders = $manage_all_orders_view;



    $update_data = array(
    'auth_id' => (int) $auth_id,
    'category_type' => $category_type,
    'categories' => $categories,
    'currency_master' => $currency_master,
    'country_currency' => $country_currency,
    'authority_type' => $authority_type,
    'manage_authority' => $manage_authority,
    'manage_users' => $manage_users,
    'notifications' => $manage_notifications,
    'promo_code' => $manage_promo,
    'promo_code_service' => $manage_promo_service,
    'dimension' => $manage_dimension,
    'vehicals' => $manage_vehicals,
    'rates' => $manage_rates,
    'driver_category' => $manage_driver_category,
    'advance_payment' => $manage_advance_payment,
    'skills_master' => $manage_skills_master,
    'insurance_master' => $manage_insurance_master,
    'relay_point' => $manage_relay_point,
    'verify_doc' => $manage_verify_doc,
    'all_orders' => $manage_all_orders,
    'withdraw_request' => $manage_withdraw_request,
    );
    if( $this->authorities->update_permissions($update_data)){
      $this->session->set_flashdata('success','Permission updated successfully!');
      redirect('admin/authority');
    } else {  $this->session->set_flashdata('error','Unable to update permissions! Try again...'); redirect('admin/assign-permission/'. $auth_id); }
    
  }

  public function update_authority()
  {
    $auth_id = $this->input->post('auth_id', TRUE);
    $fname = $this->input->post('auth_fname', TRUE);
    $lname = $this->input->post('auth_lname', TRUE);
    $email = $this->input->post('auth_email', TRUE);
    $country_id = $this->input->post('country_id', TRUE);
    $auth_type_id = $this->input->post('auth_type_id', TRUE);
    $type = $this->input->post('type', TRUE);
    $update_data = array();
    if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
      $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('profile_image')) {   
          $uploads    = $this->upload->data();  
          $profile_image_url =  $config['upload_path'].$uploads["file_name"];  

          $update_data = array(
            "auth_fname" => trim($fname),
            "auth_lname" => trim($lname),
            "auth_email" => trim($email),
            "auth_type_id" => $auth_type_id,
            "type" => $type,
            "country_id" => $country_id,
            "auth_avatar_url" => $profile_image_url,
          );
        }         
    } else { 
      $update_data = array(
        "auth_fname" => trim($fname),
        "auth_lname" => trim($lname),
        "auth_email" => trim($email),
        "country_id" => $country_id,
        "auth_type_id" => $auth_type_id,
        "type" => $type,
      );
    }

    if( $this->authorities->update_authority((int) $auth_id, $update_data)){
      $this->session->set_flashdata('success','authority details successfully updated!');
    } else {  $this->session->set_flashdata('error','Unable to update authority details! Try again...');  }
    
    redirect('admin/edit-authority/'. $auth_id);
  }
  
  public function update_authority_password() {
    $auth_id = $this->input->post('auth_id', TRUE);
    $auth_pass = $this->input->post('auth_pass', TRUE);

    if( $this->authorities->update_authority_password((int) $auth_id, trim($auth_pass))){
      $this->session->set_flashdata('success1','authority password successfully updated!');
    } else {  $this->session->set_flashdata('error1','Unable to update authority password! Try again...');  }
    
    redirect('admin/edit-authority/'. $auth_id);
  }

  public function active_authority()
  {
    $auth_id = $this->input->post('id', TRUE);
    if( $this->authorities->activate_authority( (int) $auth_id) ) { echo 'success'; } else {  echo 'failed';  }
  }
  
  public function inactive_authority()
  {
    $auth_id = $this->input->post('id', TRUE);
    if( $this->authorities->inactivate_authority( (int) $auth_id) ) { echo 'success'; } else {  echo 'failed';  }
  }
  
  public function delete_authority_type()
  {
    $auth_type_id = (int) $this->uri->segment('3');
    if( $this->authorities->delete_authority_type($auth_type_id) ) {
      $this->session->set_flashdata('success','authority type successfully deleted!');
    } else {  $this->session->set_flashdata('error','Unable to delete authority type! Try again...'); }
    redirect('authority_type');         
  }

  public function register_authority()
  {
    $email = $this->input->post('auth_email', TRUE);
    if(! $this->authorities->check_auth_email_existance(trim($email)) ) { 

      $fname = $this->input->post('auth_fname', TRUE);
      $lname = $this->input->post('auth_lname', TRUE);
      $password = $this->input->post('auth_pass', TRUE);
      $auth_type_id = $this->input->post('auth_type_id', TRUE);
      $type = $this->input->post('type', TRUE);
      $country_id = $this->input->post('country_id', TRUE); 
      
      if( isset($_FILES["profile_image"]["tmp_name"]) ){
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('profile_image')){   
          $uploads    = $this->upload->data();  
          $profile_image_url =  $config['upload_path'].$uploads["file_name"];  
        } else { $profile_image_url = "NULL"; }
            
      } else { $profile_image_url = "NULL"; } 
      
      $insert_data = array(
        "fname" => trim($fname),
        "lname" => trim($lname),
        "email" => trim($email),
        "password" => trim($password),
        "auth_type_id" => $auth_type_id,
        "type" => trim($type),
        "country_id" => (int)$country_id,
        "profile_image_url" => $profile_image_url,
      );

      if($auth_id = $this->authorities->register_authority($insert_data)){
        $this->session->set_flashdata('success','New authority successfully created! Assign permissions!');
        redirect('admin/add-authority');
      } else {  $this->session->set_flashdata('error','Unable to create new authority! Try again...'); redirect('admin/add-authority'); } 
    } else {  $this->session->set_flashdata('error','Email address already taken! Try again with different email address...'); redirect('admin/add-authority'); }   
  }
}

/* End of file Authority.php */
/* Location: ./application/controllers/Authority.php */