<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Point_to_point_rates extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if($this->session->userdata('is_admin_logged_in')){
      $this->load->model('admin_model', 'admin'); 
      $this->load->model('standard_rates_model', 'standard_rates');   
      $this->load->model('Point_to_point_rates_model', 'pp');   
      $this->load->model('Standard_dimension_model', 'dimension');    
      $id = $this->session->userdata('userid');
      $user = $this->admin->logged_in_user_details($id);
      $permissions = $this->admin->get_auth_permissions($user['type_id']);
      
      if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions')); 
    } else{ redirect('admin');}
  }

  public function index()
  {
    $rates = $this->pp->get_pp_rates();
    $this->load->view('admin/point_to_point_list_view', compact('rates'));      
    $this->load->view('admin/footer_view');   
  }

  public function get_states()
  {
    $country_id = $this->input->post('country_id', TRUE);
    $states = $this->pp->get_states((int)$country_id);
    echo json_encode($states);
  }

  public function get_cities()
  {
    $state_id = $this->input->post('state_id', TRUE);
    $cities = $this->pp->get_cities($state_id);
    echo json_encode($cities);
  }

  public function add_volume_based()
  {
    $countries_list = $this->standard_rates->get_countries();
    $categories = $this->dimension->get_categories();
    $dimensions_list = $this->dimension->get_standard_dimension(); 
    $this->load->view('admin/point_to_point_add_volume_based_view', compact('countries_list','dimensions_list','categories'));
    $this->load->view('admin/footer_view'); 
  }

  public function register_volume_based()
  {
    $categories = $this->input->post('category', TRUE);    
    $from_country_id = $this->input->post('from_country_id', TRUE);
    $from_state_id = $this->input->post('from_state_id', TRUE);
    $from_city_id = $this->input->post('from_city_id', TRUE);
    $to_country_id = $this->input->post('to_country_id', TRUE);
    $to_state_id = $this->input->post('to_state_id', TRUE);
    $to_city_id = $this->input->post('to_city_id', TRUE);
    $currency_id = $this->input->post('currency_id', TRUE);

    $min_width = $this->input->post('min_width', TRUE);
    $min_height = $this->input->post('min_height', TRUE);
    $min_length = $this->input->post('min_length', TRUE);
    $min_dimension = $this->input->post('min_dimension', TRUE);

    $max_width = $this->input->post('max_width', TRUE);
    $max_height = $this->input->post('max_height', TRUE);
    $max_length = $this->input->post('max_length', TRUE);
    $max_dimension = $this->input->post('max_dimension', TRUE);

    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);  
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);  
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);

    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);

    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $currency = $this->standard_rates->get_currency_detail((int) $currency_id);
    $insert_flag = false;
    $post_data = array(
      "category_id" =>  $categories, 
      "from_country_id" =>  (int)($from_country_id), 
      "from_state_id" =>  (int)($from_state_id), 
      "from_city_id" =>  (int)($from_city_id), 
      "to_country_id" =>  (int)($to_country_id), 
      "to_state_id" =>  (int)($to_state_id), 
      "to_city_id" =>  (int)($to_city_id), 
      "currency_id" =>  (int)($currency_id), 
      "currency_sign" =>  trim($currency['currency_sign']), 
      "currency_title" =>  trim($currency['currency_title']),       

      "min_width" =>  trim($min_width), 
      "min_height" =>  trim($min_height), 
      "min_length" =>  trim($min_length), 
      "min_volume" =>  ( $min_width * $min_height * $min_length ), 
      "min_dimension_id" => (trim($min_dimension) == "other" ) ? 0 : $min_dimension , 
      
      "max_width" =>  trim($max_width), 
      "max_height" =>  trim($max_height), 
      "max_length" =>  trim($max_length), 
      "max_volume" =>  ( $max_width * $max_height * $max_length ), 
      "max_dimension_id" => (trim($max_dimension) == "other" ) ? 0 : $max_dimension ,
           
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),

      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration),
      "cre_datetime" =>  date("Y-m-d H:i:s"), 
    );

    foreach ($categories as $c) {
      $insert_data = array(
        "category_id" =>  (int)($c), 
        "from_country_id" =>  (int)($from_country_id), 
        "from_state_id" =>  (int)($from_state_id), 
        "from_city_id" =>  (int)($from_city_id), 
        "to_country_id" =>  (int)($to_country_id), 
        "to_state_id" =>  (int)($to_state_id), 
        "to_city_id" =>  (int)($to_city_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),       

        "min_width" =>  trim($min_width), 
        "min_height" =>  trim($min_height), 
        "min_length" =>  trim($min_length), 
        "min_volume" =>  ( $min_width * $min_height * $min_length ), 
        "min_dimension_id" => (trim($min_dimension) == "other" ) ? 0 : $min_dimension , 
        
        "max_width" =>  trim($max_width), 
        "max_height" =>  trim($max_height), 
        "max_length" =>  trim($max_length), 
        "max_volume" =>  ( $max_width * $max_height * $max_length ), 
        "max_dimension_id" => (trim($max_dimension) == "other" ) ? 0 : $max_dimension ,
        
        "min_weight" =>  0, 
        "max_weight" =>  0, 
        "unit_id" =>  0, 
        
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );
      
      if( !$this->pp->check_volume_based_p_to_p_rates($insert_data,"min")  && !$this->pp->check_volume_based_p_to_p_rates($insert_data,"max")) {      
        if($this->pp->register_p_to_p_rates($insert_data)){ $insert_flag = true; } 
        else { $insert_flag = false;   } 
      } 
    }    

    if($insert_flag){ $this->session->set_flashdata('success','Rate successfully added!'); }
    else{ $this->session->set_flashdata('error',array('error_msg' => 'Unable to add rates! Try again...', 'data' => $post_data));  }
    redirect('admin/p-to-p-rates/add/volume-based');
  }

  public function edit()
  {   
    if( $id = $this->uri->segment(4) ) {
      if($rates = $this->pp->get_pp_rates($id)){
        $this->load->view('admin/point_to_point_rates_edit_view', compact('rates'));
        $this->load->view('admin/footer_view');
      } else { redirect('admin/standard-rates'); }
    } else { redirect('admin/standard-rates'); }
  }

  public function update()
  {
    //echo json_encode($_POST); die();
    
    $rate_id = $this->input->post('rate_id', TRUE);   
    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);

    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $update_data = array( 
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),
      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration), 
    );
  
    // echo json_encode($insert_data); die();

    if($this->pp->update_pp_rates($update_data, $rate_id)){
      $this->session->set_flashdata('success','Rates successfully updated!');
    } else { $this->session->set_flashdata('error', 'Unable to update rates! Try again...');  }

    redirect('admin/p-to-p-rates/edit/'.$rate_id);
  }

  public function add_weight_based()
  {
    $categories = $this->dimension->get_categories();
    $countries_list = $this->standard_rates->get_countries();
    $unit_list = $this->standard_rates->get_unit_master(); 
    $this->load->view('admin/point_to_point_add_weight_based_view', compact('countries_list','unit_list','categories'));
    $this->load->view('admin/footer_view'); 
  }

  public function register_weight_based()
  {
    //echo json_encode($_POST); die();
    $categories = $this->input->post('category', TRUE);    
    $from_country_id = $this->input->post('from_country_id', TRUE);
    $from_state_id = $this->input->post('from_state_id', TRUE);
    $from_city_id = $this->input->post('from_city_id', TRUE);
    $to_country_id = $this->input->post('to_country_id', TRUE);
    $to_state_id = $this->input->post('to_state_id', TRUE);
    $to_city_id = $this->input->post('to_city_id', TRUE);
    $currency_id = $this->input->post('currency_id', TRUE);

    $min_weight = $this->input->post('min_weight', TRUE);
    $max_weight = $this->input->post('max_weight', TRUE);
    $unit_id = $this->input->post('unit_id', TRUE);

    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);  
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);  
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);

    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);

    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $currency = $this->standard_rates->get_currency_detail((int) $currency_id);
    $insert_flag = false;

    $post_data = array(
      "category_id" =>  $categories,
      "from_country_id" =>  (int)($from_country_id), 
      "from_state_id" =>  (int)($from_state_id), 
      "from_city_id" =>  (int)($from_city_id), 
      "to_country_id" =>  (int)($to_country_id), 
      "to_state_id" =>  (int)($to_state_id), 
      "to_city_id" =>  (int)($to_city_id), 
      "currency_id" =>  (int)($currency_id), 
      "currency_sign" =>  trim($currency['currency_sign']), 
      "currency_title" =>  trim($currency['currency_title']),       
      
      "min_weight" =>  (int) $min_weight, 
      "max_weight" =>  (int) $max_weight, 
      "unit_id" =>  (int) $unit_id, 
      
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),

      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration),
      "cre_datetime" =>  date("Y-m-d H:i:s"), 
    );

    foreach ($categories as $c) {
      $insert_data = array(
        "category_id" =>  (int)($c),
        "from_country_id" =>  (int)($from_country_id), 
        "from_state_id" =>  (int)($from_state_id), 
        "from_city_id" =>  (int)($from_city_id), 
        "to_country_id" =>  (int)($to_country_id), 
        "to_state_id" =>  (int)($to_state_id), 
        "to_city_id" =>  (int)($to_city_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),       

        "min_width" =>  0, 
        "min_height" =>  0, 
        "min_length" =>  0, 
        "min_volume" =>  0, 
        "min_dimension_id" => 0, 
        
        "max_width" =>  0, 
        "max_height" =>  0, 
        "max_length" =>  0, 
        "max_volume" =>  0, 
        "max_dimension_id" => 0 ,
        
        "min_weight" =>  (int) $min_weight, 
        "max_weight" =>  (int) $max_weight, 
        "unit_id" =>  (int) $unit_id, 
        
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
      );

      if( !$this->pp->check_weight_based_p_to_p_rates($insert_data,"min")  && !$this->pp->check_weight_based_p_to_p_rates($insert_data,"max")) {      
        if($this->pp->register_p_to_p_rates($insert_data)){ $insert_flag = true;  } 
        else { $insert_flag = false;  } 
      } 
    }
    if($insert_flag){ $this->session->set_flashdata('success','Rate successfully added!'); }
    else{ $this->session->set_flashdata('error',array('error_msg' => 'Unable to add rates! Try again...', 'data' => $post_data));  }
    redirect('admin/p-to-p-rates/add/weight-based');
  }

  public function delete()
  {
    $rates_id = $this->input->post('id', TRUE);
    if($this->pp->delete_pp_rates($rates_id)){ echo 'success'; } else { echo "failed";  } 
  }

  public function add_formula_volume_based()
  {
    $countries_list = $this->standard_rates->get_countries();
    $categories = $this->dimension->get_categories();
    $dimensions_list = $this->dimension->get_standard_dimension(); 
    $this->load->view('admin/point_to_point_add_formula_volume_based_view', compact('countries_list','dimensions_list','categories'));
    $this->load->view('admin/footer_view'); 
  }

  public function register_formula_volume_based()
  {
    //echo json_encode($_POST); die();
    $categories = $this->input->post('category', TRUE);    
    $from_country_id = $this->input->post('from_country_id', TRUE);
    $from_state_id = $this->input->post('from_state_id', TRUE);
    $from_city_id = $this->input->post('from_city_id', TRUE);
    $to_country_id = $this->input->post('to_country_id', TRUE);
    $to_state_id = $this->input->post('to_state_id', TRUE);
    $to_city_id = $this->input->post('to_city_id', TRUE);
    $currency_id = $this->input->post('currency_id', TRUE);

    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);  
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);  
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);

    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);

    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $currency = $this->standard_rates->get_currency_detail((int) $currency_id);
    $insert_flag = false;
    $post_data = array(
      "category_id" =>  $categories, 
      "from_country_id" =>  (int)($from_country_id), 
      "from_state_id" =>  (int)($from_state_id), 
      "from_city_id" =>  (int)($from_city_id), 
      "to_country_id" =>  (int)($to_country_id), 
      "to_state_id" =>  (int)($to_state_id), 
      "to_city_id" =>  (int)($to_city_id), 
      "currency_id" =>  (int)($currency_id), 
      "currency_sign" =>  trim($currency['currency_sign']), 
      "currency_title" =>  trim($currency['currency_title']),
           
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),

      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration),
      "cre_datetime" =>  date("Y-m-d H:i:s"), 
    );

    foreach ($categories as $c) {
      $insert_data = array(
        "category_id" =>  (int)($c), 
        "from_country_id" =>  (int)($from_country_id), 
        "from_state_id" =>  (int)($from_state_id), 
        "from_city_id" =>  (int)($from_city_id), 
        "to_country_id" =>  (int)($to_country_id), 
        "to_state_id" =>  (int)($to_state_id), 
        "to_city_id" =>  (int)($to_city_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),       

        "min_width" =>  0, 
        "min_height" =>  0, 
        "min_length" =>  0, 
        "min_volume" =>  0, 
        "min_dimension_id" => 0, 
        
        "max_width" =>  0, 
        "max_height" =>  0, 
        "max_length" =>  0, 
        "max_volume" =>  0, 
        "max_dimension_id" => 0,
        
        "min_weight" =>  0, 
        "max_weight" =>  0, 
        "unit_id" =>  0, 
        
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
        "is_formula_volume_rate" =>  1,
      );
      
      if( !$this->pp->check_formula_volume_based_p_to_p_rates($insert_data) ) {      
        if($this->pp->register_p_to_p_rates($insert_data)){ $insert_flag = true; } 
        else { $insert_flag = false;   } 
      } 
    }    

    if($insert_flag){ $this->session->set_flashdata('success','Rate successfully added!'); }
    else{ $this->session->set_flashdata('error',array('error_msg' => 'Unable to add rates! Rate exists for given category and country.', 'data' => $post_data));  }
    redirect('admin/p-to-p-rates/add/formula-volume-based');
  }

  public function add_formula_weight_based()
  {
    $categories = $this->dimension->get_categories();
    $countries_list = $this->standard_rates->get_countries();
    $unit_list = $this->standard_rates->get_unit_master(); 
    $this->load->view('admin/point_to_point_formula_add_weight_based_view', compact('countries_list','unit_list','categories'));
    $this->load->view('admin/footer_view'); 
  }

  public function get_unit_detail()
  {
    $unit_id = $this->input->post('unt');
    echo json_encode($this->standard_rates->get_unit_detail($unit_id));
  }

  public function register_formula_weight_based()
  {
    //echo json_encode($_POST); die();
    $categories = $this->input->post('category', TRUE);    
    $from_country_id = $this->input->post('from_country_id', TRUE);
    $from_state_id = $this->input->post('from_state_id', TRUE);
    $from_city_id = $this->input->post('from_city_id', TRUE);
    $to_country_id = $this->input->post('to_country_id', TRUE);
    $to_state_id = $this->input->post('to_state_id', TRUE);
    $to_city_id = $this->input->post('to_city_id', TRUE);
    $currency_id = $this->input->post('currency_id', TRUE);

    $unit_id = $this->input->post('unit_id', TRUE);

    $earth_local_rate = $this->input->post('earth_local_rate', TRUE);
    $earth_local_duration = $this->input->post('earth_local_duration', TRUE);  
    $earth_national_rate = $this->input->post('earth_national_rate', TRUE);
    $earth_national_duration = $this->input->post('earth_national_duration', TRUE);  
    $earth_international_rate = $this->input->post('earth_international_rate', TRUE);
    $earth_international_duration = $this->input->post('earth_international_duration', TRUE);

    $air_local_rate = $this->input->post('air_local_rate', TRUE);
    $air_local_duration = $this->input->post('air_local_duration', TRUE);
    $air_national_rate = $this->input->post('air_national_rate', TRUE);
    $air_national_duration = $this->input->post('air_national_duration', TRUE);
    $air_international_rate = $this->input->post('air_international_rate', TRUE);
    $air_international_duration = $this->input->post('air_international_duration', TRUE);

    $sea_local_rate = $this->input->post('sea_local_rate', TRUE);
    $sea_local_duration = $this->input->post('sea_local_duration', TRUE);
    $sea_national_rate = $this->input->post('sea_national_rate', TRUE);
    $sea_national_duration = $this->input->post('sea_national_duration', TRUE);
    $sea_international_rate = $this->input->post('sea_international_rate', TRUE);
    $sea_international_duration = $this->input->post('sea_international_duration', TRUE);

    $currency = $this->standard_rates->get_currency_detail((int) $currency_id);
    $insert_flag = false;

    $post_data = array(
      "category_id" =>  $categories,
      "from_country_id" =>  (int)($from_country_id), 
      "from_state_id" =>  (int)($from_state_id), 
      "from_city_id" =>  (int)($from_city_id), 
      "to_country_id" =>  (int)($to_country_id), 
      "to_state_id" =>  (int)($to_state_id), 
      "to_city_id" =>  (int)($to_city_id), 
      "currency_id" =>  (int)($currency_id), 
      "currency_sign" =>  trim($currency['currency_sign']), 
      "currency_title" =>  trim($currency['currency_title']),       
      
      "unit_id" =>  (int) $unit_id, 
      
      "earth_local_rate" =>  trim($earth_local_rate), 
      "earth_national_rate" =>  trim($earth_national_rate), 
      "earth_international_rate" =>  trim($earth_international_rate), 
      "air_local_rate" =>  trim($air_local_rate), 
      "air_national_rate" =>  trim($air_national_rate), 
      "air_international_rate" =>  trim($air_international_rate), 
      "sea_local_rate" =>  trim($sea_local_rate), 
      "sea_national_rate" =>  trim($sea_national_rate), 
      "sea_international_rate" =>  trim($sea_international_rate),

      "earth_local_duration" =>  trim($earth_local_duration), 
      "earth_national_duration" =>  trim($earth_national_duration), 
      "earth_international_duration" =>  trim($earth_international_duration), 
      "air_local_duration" =>  trim($air_local_duration), 
      "air_national_duration" =>  trim($air_national_duration), 
      "air_international_duration" =>  trim($air_international_duration), 
      "sea_local_duration" =>  trim($sea_local_duration), 
      "sea_national_duration" =>  trim($sea_national_duration), 
      "sea_international_duration" =>  trim($sea_international_duration),
      "cre_datetime" =>  date("Y-m-d H:i:s"), 
    );

    foreach ($categories as $c) {
      $insert_data = array(
        "category_id" =>  (int)($c),
        "from_country_id" =>  (int)($from_country_id), 
        "from_state_id" =>  (int)($from_state_id), 
        "from_city_id" =>  (int)($from_city_id), 
        "to_country_id" =>  (int)($to_country_id), 
        "to_state_id" =>  (int)($to_state_id), 
        "to_city_id" =>  (int)($to_city_id), 
        "currency_id" =>  (int)($currency_id), 
        "currency_sign" =>  trim($currency['currency_sign']), 
        "currency_title" =>  trim($currency['currency_title']),       

        "min_width" =>  0, 
        "min_height" =>  0, 
        "min_length" =>  0, 
        "min_volume" =>  0, 
        "min_dimension_id" => 0, 
        
        "max_width" =>  0, 
        "max_height" =>  0, 
        "max_length" =>  0, 
        "max_volume" =>  0, 
        "max_dimension_id" => 0 ,
        
        "min_weight" =>  0, 
        "max_weight" =>  0, 
        "unit_id" =>  (int) $unit_id, 
        
        "earth_local_rate" =>  trim($earth_local_rate), 
        "earth_national_rate" =>  trim($earth_national_rate), 
        "earth_international_rate" =>  trim($earth_international_rate), 
        "air_local_rate" =>  trim($air_local_rate), 
        "air_national_rate" =>  trim($air_national_rate), 
        "air_international_rate" =>  trim($air_international_rate), 
        "sea_local_rate" =>  trim($sea_local_rate), 
        "sea_national_rate" =>  trim($sea_national_rate), 
        "sea_international_rate" =>  trim($sea_international_rate),

        "earth_local_duration" =>  trim($earth_local_duration), 
        "earth_national_duration" =>  trim($earth_national_duration), 
        "earth_international_duration" =>  trim($earth_international_duration), 
        "air_local_duration" =>  trim($air_local_duration), 
        "air_national_duration" =>  trim($air_national_duration), 
        "air_international_duration" =>  trim($air_international_duration), 
        "sea_local_duration" =>  trim($sea_local_duration), 
        "sea_national_duration" =>  trim($sea_national_duration), 
        "sea_international_duration" =>  trim($sea_international_duration),
        "cre_datetime" =>  date("Y-m-d H:i:s"), 
        "is_formula_weight_rate" =>  1,
      );

      if( !$this->pp->check_formula_weight_based_p_to_p_rates($insert_data) ) {      
        if($this->pp->register_p_to_p_rates($insert_data)){ $insert_flag = true;  } 
        else { $insert_flag = false;  } 
      } 
    }
    if($insert_flag){ $this->session->set_flashdata('success','Rate successfully added!'); }
    else{ $this->session->set_flashdata('error',array('error_msg' => 'Unable to add rates! Rate exists for given category and country.', 'data' => $post_data));  }
    redirect('admin/p-to-p-rates/add/formula-weight-based');
  }

}

/* End of file point_to_point_rates.php */
/* Location: ./application/controllers/point_to_point_rates.php */