<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_currency extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('country_currency_model', 'country_currency');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function index()
	{
		if(!$this->session->userdata('is_admin_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
	}

	public function country_currency()
	{		
		$country_currency = $this->country_currency->get_country_currency();		
		$this->load->view('admin/country_currency_list_view', compact('country_currency'));			
		$this->load->view('admin/footer_view');		
	}
	public function edit_country_currency()
	{	
		$cc_id = (int) $this->uri->segment('3');
		$country_currency = $this->country_currency->get_country_currency_detail($cc_id);

		$country_id = $country_currency['country_id'];
		$currency_id = $country_currency['currency_id'];
		$currencies = $this->country_currency->get_currency_except_by_id($currency_id, $country_id);
		$this->load->view('admin/country_currency_edit_view', compact('country_currency', 'currencies'));
		$this->load->view('admin/footer_view');
	}
	public function update_country_currency()
	{
		$cc_id = $this->input->post('cc_id', TRUE);
		$currency_id = $this->input->post('currency_id', TRUE);

		$update_data = array(
			'cc_id' => (int) $cc_id,
			'currency_id' => $currency_id
		);
		
		if( $this->country_currency->update_country_currency($update_data)){
			$this->session->set_flashdata('success','currency successfully updated!');
		}	else { 	$this->session->set_flashdata('error','Unable to update currency! Try again...');	}
		
		redirect('admin/edit-country-currency/'. $cc_id);
	}
	public function active_country_currency()
	{
		$cc_id = (int) $this->input->post('id', TRUE);
		if( $this->country_currency->activate_country_currency($cc_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	public function inactive_country_currency()
	{
		$cc_id = (int) $this->input->post('id', TRUE);
		if( $this->country_currency->inactivate_country_currency($cc_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	public function delete_country_currency()
	{
		$cc_id = (int) $this->uri->segment('3');
		if( $this->country_currency->delete_country_currency($cc_id) ) {
			$this->session->set_flashdata('success','Currency successfully deleted!');
		}	else { 	$this->session->set_flashdata('error','Unable to delete currency! Try again...');	}
		redirect('country_currency');					
	}
	public function add_country_currency()
	{
		$countries_list = $this->country_currency->get_countries();
		$currencies_list = $this->country_currency->get_currencies();
		$this->load->view('admin/country_currency_add_view', compact('countries_list','currencies_list'));
		$this->load->view('admin/footer_view');
	}
	public function register_country_currency()
	{
		$country_id = $this->input->post('country_id', TRUE);
		$currency_id = $this->input->post('currency_id', TRUE);
		if(! $this->country_currency->check_currency_existance($currency_id, $country_id) ) {
			if($this->country_currency->register_country_currency($currency_id, $country_id)){
				$this->session->set_flashdata('success','Currency successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new currency! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Currency already assigned for the selected country!');	}
		redirect('admin/add-country-currency');
	}

}

/* End of file Country_currency.php */
/* Location: ./application/controllers/Country_currency.php */