<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_cancellation_reason_master extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Job_cancellation_reason_master_model', 'cancellation');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$user = $this->admin->logged_in_user_details($user['type_id']);
			$permissions = $this->admin->get_auth_permissions($id);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	public function index() {
		$reasons = $this->cancellation->get_reasons();
		$this->load->view('admin/services/job_cancellation_reason_list_view', compact('reasons'));			
		$this->load->view('admin/footer_view');
	}
	public function register() {
		$title = $this->input->post('title', TRUE);
		$reason_id = $this->input->post('reason_id', TRUE); $reason_id = (int)$reason_id;
		if($reason_id == 0) { //Register
	 		$insert_data = array(
	 			"title" => trim($title),
	 			"cre_datetime" => date("Y-m-d H:i:s")
	 		);
	 		if($this->cancellation->register_reason($insert_data)) {
	 			$this->session->set_flashdata('success', 'Reason successfully added!');
	 		} else { $this->session->set_flashdata('error', 'Unable to add reason! Try again...'); }
		} else { //Update
			$update_data = array(
	 			"title" => trim($title),
	 			"cre_datetime" => date("Y-m-d H:i:s")
	 		);
	 		if($this->cancellation->update_reason($update_data, $reason_id)){
				$this->session->set_flashdata('success','Reason successfully updated!');
			} else { 	$this->session->set_flashdata('error','Unable to update reason! Try again...');	} 
		}
 		redirect('admin/job-cancellation-reason-master');
	}
	public function delete() {
		$reason_id = $this->input->post('reason_id', TRUE);
		if($this->cancellation->delete_reason($reason_id)) { echo 'success';
		} else { echo 'failed';	}
	}
}

/* End of file Job_cancellation_reason_master.php */
/* Location: ./application/controllers/Job_cancellation_reason_master.php */