<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('category_type_model', 'category_type');		
			$this->load->model('categories_model', 'categories');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}

	public function index()
	{
		$categories = $this->categories->get_categories();
		$this->load->view('admin/category_list_view', compact('categories'));			
		$this->load->view('admin/footer_view');		
	}

	public function active_category()
	{
		$cat_id = (int) $this->input->post('id', TRUE);
		if( $this->categories->activate_category($cat_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	public function inactive_category()
	{
		$cat_id = (int) $this->input->post('id', TRUE);
		if( $this->categories->inactivate_category($cat_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

	public function add_category()
	{
		$active_category_types = $this->category_type->get_active_category_types();
		$this->load->view('admin/category_add_view', compact('active_category_types'));
		$this->load->view('admin/footer_view');
	}
	
	public function register_category()
	{
		$cat_name = $this->input->post('cat_name', TRUE);
 		$parent_cat_id = $this->input->post('parent_cat_id', TRUE);
 		$type_id = $this->input->post('type_id', TRUE);

 		if(!empty($_FILES["category_icon"]["tmp_name"])) {
		  $config['upload_path']    = 'resources/category-icons/';                 
		  $config['allowed_types']  = '*';       
		  $config['max_size']       = '0';                          
		  $config['max_width']      = '0';                          
		  $config['max_height']     = '0';                          
		  $config['encrypt_name']   = true; 
		  $this->load->library('upload', $config);
		  $this->upload->initialize($config);

		  if($this->upload->do_upload('category_icon')) {   
		    $uploads    = $this->upload->data();  
		    $icon_url =  $config['upload_path'].$uploads["file_name"];  
		  } 
		} else { $icon_url = "NULL"; }

 		$insert_data = array("cat_name" => trim($cat_name), "parent_cat_id" => (int) $parent_cat_id, "type_id" => (int) $type_id, "icon_url" => $icon_url );

		if(! $this->categories->check_category($cat_name, $type_id,$parent_cat_id) ) {
			if($this->categories->register_category($insert_data)){
				$this->session->set_flashdata('success','Category successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new category! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Category already exists in chosen category type!');	}
		redirect('admin/add-category');
	}

	public function edit_category()
	{	
		if( !is_null($this->input->post('cat_id')) ) { $cat_id = (int) $this->input->post('cat_id'); }
		else if( !is_null($this->session->flashdata('msg'))){ $cat_id = $this->session->flashdata('msg')['cat_id']; }
		else { redirect('admin/categories');}
		$cat = $this->categories->get_category_detail($cat_id);		
		
		if( $cat['parent_cat_id'] != 0 ) { 
			$parent_cat = $this->categories->get_category_detail($cat_id);
			$cat['parent_cat_name'] = $parent_cat['cat_name'];
		} else { $cat['parent_cat_name'] = "Parent"; }

		$cat_type = $this->category_type->get_category_type_detail($cat['cat_type_id']);
		$cat['cat_type'] = $cat_type['cat_type'];

		$active_category_types = $this->category_type->get_active_category_types();

		$this->load->view('admin/category_edit_view', compact('cat', 'active_category_types'));
		$this->load->view('admin/footer_view');
	}
	public function update_category()
	{  
		$cat_id = $this->input->post('cat_id', TRUE);
		$cat_name = $this->input->post('cat_name', TRUE);
		$type_id = $this->input->post('type_id', TRUE);
		$parent_cat_id = $this->input->post('parent_cat_id', TRUE);
	        $old_icon_url = $this->input->post('old_icon_url', TRUE); 

		if(!empty($_FILES["category_icon"]["tmp_name"])) {
		  $config['upload_path']    = 'resources/category-icons/';                 
		  $config['allowed_types']  = '*';       
		  $config['max_size']       = '0';                          
		  $config['max_width']      = '0';                          
		  $config['max_height']     = '0';                          
		  $config['encrypt_name']   = true; 
		  $this->load->library('upload', $config);
		  $this->upload->initialize($config);

		  if($this->upload->do_upload('category_icon')) {   
		    $uploads    = $this->upload->data();  
		    $icon_url =  $config['upload_path'].$uploads["file_name"];  
		  } 
		} else { $icon_url = $old_icon_url; }

		$cat = $this->categories->get_category_detail($cat_id);
		if( (int)$parent_cat_id == 0 ) { $parent_cat_id = (int) $cat['parent_cat_id']; }

		if( (int)$type_id == 0 ) { 
			$type_id = (int) $cat['cat_type_id']; 
			$update_data = [	'cat_id' => (int) $cat_id,	'cat_name' => $cat_name,	'type_id' => (int) $type_id,	'parent_cat_id' => (int) $parent_cat_id,	"icon_url" => $icon_url ];

			if( $this->categories->update_category($update_data)){
				$msg = array('success' =>'Category type successfully updated!','cat_id' => $cat_id );
			}	
			else { 	$msg = array('error' => 'Unable to update category type! Try again...','cat_id' => $cat_id );	}
		}
		else if( !$this->categories->check_category_update($cat_name, $type_id, $parent_cat_id, $icon_url)) {
			$update_data = [ 'cat_id' => (int) $cat_id,	'cat_name' => $cat_name,	'type_id' => (int) $type_id,	'parent_cat_id' => (int) $parent_cat_id,	'icon_url' => $icon_url ];
			
			if( $this->categories->update_category($update_data)){
				$msg = array('success' =>'Category type successfully updated!','cat_id' => $cat_id );
			}	
			else { $msg = array('error' => 'Unable to update category type! Try again...','cat_id' => $cat_id );	} 
		}
		else { 	$msg = array('error' => 'Category already exists! Try again...', 'cat_id' => $cat_id );	}

		$this->session->set_flashdata('msg',$msg);

		redirect('admin/edit-category');
	}

	public function get_categories_by_type()
	{
		$type_id = $this->input->post('type_id', TRUE);
		$parent_cats = $this->categories->get_categories_by_type($type_id);
		echo json_encode($parent_cats);
	}

}

/* End of file Categories.php */
/* Location: ./application/controllers/Categories.php */