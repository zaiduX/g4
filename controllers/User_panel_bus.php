<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class User_panel_bus extends CI_Controller {
    
  private $cust_id = 0; 
  private $cust = array();  
    
  public function __construct()
  {
    parent::__construct();
    
    $this->cust_id = $this->session->userdata("cust_id"); $this->cust_id = (int) $this->cust_id;
    
    $this->load->model('api_model_sms','api_sms');
    $this->load->model('api_model_bus', 'api');
    $this->load->model('Notification_model_bus', 'notification');
    $this->load->model('Web_user_model_bus', 'user');

    $this->load->model('Transport_model', 'transport');
  
    $this->cust = $this->api->get_consumer_datails($this->cust_id, true); $cust = $this->cust;
    
    if( ( $this->session->userdata("is_logged_in") == "NULL" ) OR ( $this->session->userdata("is_logged_in") !== 1 ) ) { redirect('log-in'); }
    
    if($cust['mobile_verified'] == 0) { redirect('verification/otp'); }

    $lang = $this->session->userdata('language');
    $this->config->set_item('language', strtolower($lang));

    if(trim($lang) == 'french'){
      $this->lang->load('frenchApi_lang','french');
      $this->lang->load('frenchFront_lang','french');
    } else if(trim($lang) == 'spanish'){  
      $this->lang->load('spanishApi_lang','spanish');
      $this->lang->load('spanishFront_lang','spanish');
    } else { 
      $this->lang->load('englishApi_lang','english');
      $this->lang->load('englishFront_lang','english');
    }
    
    $this->api->update_last_login($cust['cust_id']);
    
    //echo json_encode($cust); die();
    if (!$this->input->is_ajax_request()){ 
      // var_dump($_POST); 
      $ticket_id = $this->input->post('ticket_id');
      $type = $this->input->post('type');

      // if($ticket_id && !$ow_id) { 
      if($ticket_id && ($type =="workroom")) { 
        $ids = $this->api->get_total_unread_order_workroom_notifications($ticket_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) {  $this->api->make_workroom_notification_read($v['ow_id'], $cust['cust_id']);  }
      }

      if($ticket_id && ($type =="chatroom")) { 
        $ids = $this->api->get_total_unread_order_chatroom_notifications($ticket_id, $cust['cust_id']);        
        foreach ($ids as $id => $v) { $this->api->make_chatrooom_notification_read($v['chat_id'], $cust['cust_id']);  }
      }
      if($cust['user_type'] == 1 ){ $user_type ="Individuals"; } else { $user_type ="Business"; }
      $total_unread_notifications = $this->api->get_total_unread_notifications_count($cust['cust_id'],$user_type );

      $notification = $this->api->get_total_unread_notifications($cust['cust_id'],$user_type, 5);
      $total_workroom_notifications = $this->api->get_total_unread_workroom_notifications_count($cust['cust_id']);      
      $nt_workroom = $this->api->get_total_unread_workroom_notifications($cust['cust_id'], 5); 
      //echo json_encode($nt_workroom); die(); 
      //echo $this->db->last_query(); die();
      $total_chatroom_notifications = $this->api->get_total_unread_chatroom_notifications_count($cust['cust_id']);      
      $nt_chatroom = $this->api->get_total_unread_chatroom_notifications($cust['cust_id'], 5);  

      $compact = ["total_unread_notifications","notification","total_workroom_notifications","nt_workroom","total_chatroom_notifications","nt_chatroom"];
    
      $this->load->view('user_panel/header_bus',compact($compact)); 
      $this->load->view('user_panel/menu_bus', compact("cust"));
    }
  }

  //Start Dashboard----------------------------------------
    public function landing()
    {   
      $cust = $this->cust;
      $category = $this->api->get_category();  
      $this->load->view('user_panel/landing_view', compact('category'));
      $this->load->view('user_panel/footer');   
    }
    public function dashboard_bus()
    {   
      $cust = $this->cust;
      $category_type = $this->api->get_category_type();
      $category = $this->api->get_category();
      if($cust['acc_type'] == 'buyer'){
        $trip_sources = $this->api->get_trip_master_source_list_buyer();
        $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      }else{
        $trip_sources = $this->api->get_multi_trip_master_source_list($cust['cust_id']);
        $trip_destinations = $this->api->get_multi_trip_master_destination_list($cust['cust_id']);  
      }
      $this->load->view('user_panel/dashboard_bus', compact('cust','trip_sources','trip_destinations'));   

      if($cust['acc_type'] == 'buyer') {
        $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
        $current_balance = ($current_balance != NULL ) ? $current_balance['account_balance'] : 0;

        $total_spend = $this->user->get_total_spend($cust['cust_id'],'XAF');    
        $total_spend = ($total_spend !=NULL)?$total_spend['amount']:0;

        //trips
        $type="cust";
        $total_trips = $this->api->get_total_seller_trips($cust['cust_id'], $type);
        //echo json_encode($total_trips); die();
        //total spend in service
        $total_spend_service = $this->user->get_total_spend_service($cust['cust_id'], 'XAF', 281); 
        $total_spend_service = $total_spend_service['amount'];
        $total_spend_service = ($total_spend_service !=NULL && $total_spend_service !=null) ? $total_spend_service: 0; 
        
        $upcoming = $this->api->get_buyer_trip_master_list($cust['cust_id'], 'upcoming','','','',5);
        $current = $this->api->get_buyer_trip_master_list($cust['cust_id'], 'current','','','',5);
        $previous = $this->api->get_buyer_trip_master_list($cust['cust_id'], 'previous','','','',5);
        $cancel_ticket = $trip_list = $this->api->get_buyer_trip_master_list( $cust['cust_id'] , 'cancelled','','','',5);
        $completed_trips = $this->api->get_total_seller_completed_trips($cust['cust_id'],$type);
        //echo json_encode($completed_trips); die();
        $compact = compact('cust','current_balance','total_spend','total_trips','upcoming','current','previous','cancel_ticket','completed_trips', 'total_spend_service');

        $this->load->view('user_panel/buyer_dashboard_bus',$compact);
      } else if($cust['acc_type'] == 'seller') {  
        $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
        $current_balance = ($current_balance !=NULL && $current_balance !=null)?$current_balance['account_balance']:0;

        $total_earned = $this->user->get_total_earned($cust['cust_id'],'XAF'); 
        $total_earned = $total_earned['amount'];
        $total_earned = ($total_earned !=NULL && $total_earned !=null) ? $total_earned: 0; 

        // buyer
        $total_spend = $this->user->get_total_spend($cust['cust_id'],'XAF');
        //echo json_encode($total_spend);
        //$total_spend = $total_spend['amount']; 
        $total_spend = ($total_spend !=NULL && $total_spend !=null)?$total_spend['amount']:0;

        //total earned in service
        $total_earned_service = $this->user->get_total_earned_service($cust['cust_id'], 'XAF', 281); 
        $total_earned_service = $total_earned_service['amount'];
        $total_earned_service = ($total_earned_service !=NULL && $total_earned_service !=null) ? $total_earned_service: 0; 
        //total spend in service
        $total_spend_service = $this->user->get_total_spend_service($cust['cust_id'], 'XAF', 281); 
        $total_spend_service = $total_spend_service['amount'];
        $total_spend_service = ($total_spend_service !=NULL && $total_spend_service !=null) ? $total_spend_service: 0; 

        $recent_order_reviews = $this->user->get_recent_order_reviews($cust['cust_id']);
        $bus_rating = $this->api->get_bus_review_details($cust['cust_id']);
        $deliverer_ratings = $this->api->get_deliverer_review_details($cust['cust_id']);
        $total_trips = $this->api->get_total_seller_trips($cust['cust_id']);
        $upcoming = $this->api->get_seller_trip_master_list($cust['cust_id'] , 'upcoming' , 'operator',0,0,5);
        $current = $this->api->get_seller_trip_master_list($cust['cust_id'] , 'current' , 'operator',0,0,5);
        $previous = $this->api->get_seller_trip_master_list($cust['cust_id'] , 'previous' , 'operator',0,0,5);
        $cancel_ticket = array();
        if(!$completed_trips = $this->api->get_total_seller_completed_trips($cust['cust_id'])){
          $completed_trips = array(); 
        }
         $compact = compact('completed_trips','cancel_ticket','previous','current','upcoming','total_trips','deliverer_ratings','bus_rating','cust','current_balance','total_earned','total_spend', 'total_spend_service', 'total_earned_service');

        $this->load->view('user_panel/both_dashboard_bus',$compact);
      } else { //Both
        $current_balance = $this->user->get_current_balance($cust['cust_id'],'XAF');
        $current_balance = ($current_balance !=NULL && $current_balance !=null)?$current_balance['account_balance']:0;

        $total_earned = $this->user->get_total_earned($cust['cust_id'],'XAF'); 
        $total_earned = $total_earned['amount'];
        $total_earned = ($total_earned !=NULL && $total_earned !=null) ? $total_earned: 0; 
        // buyer
        $total_spend = $this->user->get_total_spend($cust['cust_id'],'XAF');
        //total earned in service
        $total_earned_service = $this->user->get_total_earned_service($cust['cust_id'], 'XAF', 281); 
        $total_earned_service = $total_earned_service['amount'];
        $total_earned_service = ($total_earned_service !=NULL && $total_earned_service !=null) ? $total_earned_service: 0; 
        //total spend in service
        $total_spend_service = $this->user->get_total_spend_service($cust['cust_id'], 'XAF', 281); 
        $total_spend_service = $total_spend_service['amount'];
        $total_spend_service = ($total_spend_service !=NULL && $total_spend_service !=null) ? $total_spend_service: 0; 
        //echo json_encode($total_spend);
        //$total_spend = $total_spend['amount'];
        $total_spend = ($total_spend !=NULL && $total_spend !=null)?$total_spend['amount']:0;

        $recent_order_reviews = $this->user->get_recent_order_reviews($cust['cust_id']);
        $bus_rating = $this->api->get_bus_review_details($cust['cust_id']);
        $deliverer_ratings = $this->api->get_deliverer_review_details($cust['cust_id']);
        //echo json_encode($bus_rating); die();
        $total_trips = $this->api->get_total_seller_trips($cust['cust_id']);
        $upcoming = $this->api->get_seller_trip_master_list($cust['cust_id'] , 'upcoming' , 'operator',0,0,5);
        $current = $this->api->get_seller_trip_master_list($cust['cust_id'] , 'current' , 'operator',0,0,5);
        $previous = $this->api->get_seller_trip_master_list($cust['cust_id'] , 'previous' , 'operator',0,0,5);
        $cancel_ticket = $this->api->get_cancelled_seller_ticket($cust['cust_id'],0,5);
        $cancel_trips = $this->api->get_cancelled_seller_trips(5);
        if(!$completed_trips = $this->api->get_total_seller_completed_trips($cust['cust_id'])) $completed_trips = array();
        $compact = compact('cancel_trips','cancel_ticket','previous','current','upcoming','total_trips','deliverer_ratings','bus_rating','cust','current_balance','total_earned','total_spend', 'completed_trips', 'total_spend_service', 'total_earned_service');

        $this->load->view('user_panel/both_dashboard_bus',$compact); 
      }
      $this->load->view('user_panel/footer');
    }
  //End Dashboard------------------------------------------

  //Start Bus operator Profile-----------------------------
    public function bus_operator_profile()
    {
      if($operator = $this->api->get_bus_operator_profile($this->cust_id)) {
        $documents = $this->api->get_operator_documents($this->cust_id);
        $this->load->view('user_panel/bus_operator_profile_view', compact('operator','documents'));
      } else {
        if($operator = $this->api->get_deliverer_profile($this->cust_id)) {
          unset($operator['deliverer_id']);
          //echo json_encode($operator); die();
          $this->api->create_operator_profile($operator);
          $documents = $this->api->get_operator_documents($this->cust_id);
          $this->load->view('user_panel/bus_operator_profile_view', compact('operator','documents'));
        } else {
          $cust = $this->cust;
          $countries = $this->user->get_countries($this->cust_id);
          $cust_id = $this->cust_id;
          $this->load->view('user_panel/create_bus_operator_profile_view', compact('cust_id','countries','cust'));
        }
      }
      $this->load->view('user_panel/footer');
    }
    public function create_bus_operator_profile()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $email_id = $this->input->post('email_id', TRUE);
      $contact_no = $this->input->post('contact_no', TRUE);
      $carrier_type = $this->input->post('carrier_type', TRUE);
      $address = $this->input->post('address', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $state_id = $this->input->post('state_id', TRUE);
      if($state_id<=0) {
        $state_id = $this->input->post('state_id_hidden', TRUE);
      }
      $city_id = $this->input->post('city_id', TRUE);
      if($city_id<=0) {
        $city_id = $this->input->post('city_id_hidden', TRUE);
      }
      $zipcode = $this->input->post('zipcode', TRUE);
      $promocode = $this->input->post('promocode', TRUE);
      $company_name = $this->input->post('company_name', TRUE);
      $introduction = $this->input->post('introduction', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      $gender = $this->input->post('gender', TRUE);
      //$service_list = $this->input->post('service_list', TRUE);
      //$service_list = implode(',',$service_list);
      $today = date("Y-m-d H:i:s");
      //$profile = $this->api->get_business_datails($cust_id);
      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('avatar')) {   
            $uploads    = $this->upload->data();  
            $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
          } else { $avatar_url = "NULL"; }
        } else { $avatar_url = "NULL"; }

        if( !empty($_FILES["cover"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/cover-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('cover')) {   
            $uploads    = $this->upload->data();  
            $cover_url =  $config['upload_path'].$uploads["file_name"]; 
          } else{ $cover_url = "NULL"; }
        } else{ $cover_url = "NULL"; }

      $insert_data = array(
        "firstname" => trim($firstname),
        "lastname" => trim($lastname),
        "email_id" => trim($email_id),
        "contact_no" => trim($contact_no),
        "address" => trim($address),
        "zipcode" => trim($zipcode),
        "promocode" => trim($promocode),
        "carrier_type" => trim($carrier_type),
        "country_id" => (int)($country_id),
        "state_id" => (int)($state_id),
        "city_id" => (int)($city_id),
        "company_name" => trim($company_name),
        "introduction" => trim($introduction),
        "latitude" => trim($latitude),
        "longitude" => trim($longitude),
        "gender" => trim($gender),
        "avatar_url" => trim($avatar_url),
        "cover_url" => trim($cover_url),
        // "service_list" => $service_list,
        "cust_id" => (int)$cust_id,
        "shipping_mode" => 0,
        "cre_datetime" => $today,
      );
      //echo json_encode($insert_data); die();
      if($this->api->create_operator_profile($insert_data)) {
        $this->api->make_consumer_deliverer($cust_id);
        $this->session->set_flashdata('success', $this->lang->line('profile_created_successfully!'));
      } else { $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found!')); }
      redirect('user-panel-bus/bus-operator-profile');
    }
    public function edit_operator_profile()
    {
      $operator = $this->api->get_bus_operator_profile($this->cust_id);
      $countries = $this->user->get_countries($this->cust_id);
      $carrier_type = $this->user->carrier_type_list();
      $this->load->view('user_panel/edit_operator_profile_view', compact('operator','countries','carrier_type'));
      $this->load->view('user_panel/footer');
    }
    public function update_operator_profile()
    {
      //echo json_encode($_POST); die();
      if(empty($this->errors)){ 
        $cust_id = $this->cust_id;
        $firstname = $this->input->post('firstname', TRUE);
        $lastname = $this->input->post('lastname', TRUE);
        $email_id = $this->input->post('email_id', TRUE);
        $contact_no = $this->input->post('contact_no', TRUE);
        $carrier_type = $this->input->post('carrier_type', TRUE);
        $address = $this->input->post('address', TRUE);
        $country_id = $this->input->post('country_id', TRUE);
        $state_id = $this->input->post('state_id', TRUE);
        if($state_id<=0) {
          $state_id = $this->input->post('state_id_hidden', TRUE);
        }
        $city_id = $this->input->post('city_id', TRUE);
        if($city_id<=0) {
          $city_id = $this->input->post('city_id_hidden', TRUE);
        }
        $zipcode = $this->input->post('zipcode', TRUE);
        $promocode = $this->input->post('promocode', TRUE);
        $company_name = $this->input->post('company_name', TRUE);
        $introduction = $this->input->post('introduction', TRUE);
        $latitude = $this->input->post('latitude', TRUE);
        $longitude = $this->input->post('longitude', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $today = date("Y-m-d H:i:s");

        $profile = $this->api->get_operator_business_datails($cust_id);
        if( !empty($_FILES["avatar"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/profile-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('avatar')) {   
            $uploads    = $this->upload->data();  
            $avatar_url =  $config['upload_path'].$uploads["file_name"];  
            if( $profile['avatar_url'] != "NULL" ){ unlink($profile['avatar_url']); }
          }
          $avatar_update = $this->api->update_operator_business_avatar($avatar_url, $cust_id);
        } else $avatar_update = 0;

        if( !empty($_FILES["cover"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/cover-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('cover')) {   
            $uploads    = $this->upload->data();  
            $cover_url =  $config['upload_path'].$uploads["file_name"]; 
            if( $profile['cover_url'] != "NULL" ){  unlink($profile['cover_url']);  }
          }
          $cover_update = $this->api->update_operator_business_cover($cover_url, $cust_id);
        } else $cover_update = 0;

        $update_data = array(
          "firstname" => trim($firstname),
          "lastname" => trim($lastname),
          "email_id" => trim($email_id),
          "contact_no" => trim($contact_no),
          "address" => trim($address),
          "zipcode" => trim($zipcode),
          "promocode" => trim($promocode),
          "carrier_type" => trim($carrier_type),
          "country_id" => (int)($country_id),
          "state_id" => (int)($state_id),
          "city_id" => (int)($city_id),
          "company_name" => trim($company_name),
          "introduction" => trim($introduction),
          "latitude" => trim($latitude),
          "longitude" => trim($longitude),
          "gender" => trim($gender),
        );
        //var_dump($cover_update); die();
        //if(!$this->api->is_deliverer_email_exists($email_id, $cust_id)){
          if($this->api->update_operator($update_data, $cust_id) || $avatar_update || $cover_update ){
            $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
          } else{ $this->session->set_flashdata('error', $this->lang->line('update_or_no_change_found')); }
        //} else{ $this->session->set_flashdata('error','Email already exists.'); }
        redirect('user-panel-bus/edit-operator-profile');
      }
    }
    public function edit_operator_bank()
    {
      if( !is_null($this->input->post('cust_id')) ) { $cust_id = (int) $this->input->post('cust_id'); }
      else if(!is_null($this->uri->segment(3))) { $cust_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/bus-operator-profile'); }
      $deliverer = $this->api->get_bus_operator_profile($cust_id);
      $this->load->view('User_panel/edit_operator_bank_view', compact('deliverer'));
      $this->load->view('user_panel/footer');
    }
    public function update_operator_bank()
    {
      $cust_id = (int) $this->input->post('cust_id');
      $shipping_mode = $this->input->post('shipping_mode', TRUE);
      $iban = $this->input->post('iban', TRUE);
      $bank_name = $this->input->post('bank_name', TRUE);
      $bank_address = $this->input->post('bank_address', TRUE);
      $bank_title = $this->input->post('bank_title', TRUE);
      $bank_short_code = $this->input->post('bank_short_code', TRUE);
      $mobile_money_acc1 = $this->input->post('mobile_money_acc1', TRUE);
      $mobile_money_acc2 = $this->input->post('mobile_money_acc2', TRUE);

      $update_data = array(
              "shipping_mode" => (int)$shipping_mode,
              "iban" => trim($iban),
              "bank_name" => trim($bank_name),
              "bank_address" => trim($bank_address),
              "bank_title" => trim($bank_title),
              "bank_short_code" => trim($bank_short_code),
              "mobile_money_acc1" => trim($mobile_money_acc1),
              "mobile_money_acc2" => trim($mobile_money_acc2),
              );

      if($this->api->update_business_info($update_data, $cust_id)) {
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else {  $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found'));  } 
      redirect('user-panel-bus/edit_operator_bank/'.$cust_id);
    }
    public function edit_operator_document()
    {
      $document = $this->api->get_operator_documents($this->cust_id);
      $this->load->view('user_panel/edit_operator_document_view', compact('document'));
      $this->load->view('user_panel/footer');
    }
    public function add_operator_document()
    { 
      //var_dump($_POST); die();
      $cust_id = $this->cust_id;
      $doc_type = $this->input->post('doc_type', TRUE);
      $deliverer_id = (int) $this->user->get_deliverer_id($this->cust_id);
      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('attachment')) {   
          $uploads    = $this->upload->data();  
          $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
        } else {
          $attachement_url = "NULL";
        }
      } else { $attachement_url = "NULL"; }

      $insert_data = array(
        "attachement_url" => trim($attachement_url), 
        "cust_id" => $cust_id, 
        "deliverer_id" => $deliverer_id, 
        "is_verified" => "0", 
        "doc_type" => $doc_type, 
        "is_rejected" => "0",
        "upload_datetime" => date('Y-m-d H:i:s'),
        "verify_datetime" => "NULL",
      );  
      //var_dump($insert_data); die();
      if( $id = $this->api->register_operator_document($insert_data) ) {  
        $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
      } else {  $this->session->set_flashdata('error',$this->lang->line('upload_failed')); }
      redirect('user-panel-bus/edit-operator-document');
    }
    public function update_deliverer_doc()
    { 
      $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;  
      $doc_id = $this->input->post('doc_id', TRUE); $doc_id = (int) $doc_id;

      if( !empty($_FILES["attachement"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('attachement')) {   
            $uploads    = $this->upload->data();  
            $attachement_url =  $config['upload_path'].$uploads["file_name"]; 
          } else{ $attachement_url = "NULL"; }
      } else { $attachement_url = "NULL"; }

      $update_data = array( "attachement_url" => trim($attachement_url), "is_verified" => "0", "is_rejected" => "0"); 
      $old_doc_url = $this->api->get_deliverer_old_document_url($doc_id);

      if( $this->api->update_deliverer_document($doc_id, $update_data) ) { 
        if($attachement_url != "NULL"){ unlink(trim($old_doc_url)); }
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else {  $this->session->set_flashdata('error',$this->lang->line('unable_to_update')); }
      redirect('user-panel/deliverer-profile'); 
    }
    public function operator_document_delete()
    {
      $doc_id = $this->input->post('id', TRUE);
      //if($this->api->delete_operator_document($doc_id)){ echo 'success'; } else { echo "failed"; }
      $documents = $this->api->get_old_operator_documents($doc_id);
      if($this->api->delete_operator_document($doc_id)) { 
        if($documents[0]['attachement_url'] != "NULL") {
          unlink($documents[0]['attachement_url']);
        }
        echo "success"; 
      } else {
        echo "failed";
      } 
    }
  //End Bus operator Profile-------------------------------

  //Start bus operator business photos---------------------
    public function business_photos()
    {
      $photos = $this->api->get_bus_operator_business_photos($this->cust_id);
      $this->load->view('user_panel/bus_operator_business_photos_view', compact('photos'));
      $this->load->view('user_panel/footer');
    }
    public function bus_operator_business_photos_delete()
    {
      $photo_id = $this->input->post('id', TRUE);
      if($this->api->delete_bus_operator_business_photo($photo_id)){ echo 'success'; } else { echo "failed"; } 
    }
    public function bus_operator_business_photos_upload()
    {
      $cust_id = $this->cust_id;
      if( !empty($_FILES["business_photo"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/business-photos/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('business_photo')) {   
          $uploads    = $this->upload->data();  
          $photo_url =  $config['upload_path'].$uploads["file_name"];  
        }      
        if( $photo_id = $this->api->add_bus_operator_business_photo($photo_url, $cust_id)) {          
          $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
        } else {
          $this->session->set_flashdata('error',$this->lang->line('unable_to_update'));
        }
      } 
      // update user last login
      $this->api->update_last_login($cust_id);
      redirect('user-panel-bus/business-photos');
    }
  //End bus operator business photos-----------------------

  //Start Manage Bus---------------------------------------
    public function bus_list()
    {
      $buses = $this->api->get_operator_bus_list($this->cust_id);
      $this->load->view('user_panel/bus_list_view',compact('buses'));
      $this->load->view('user_panel/footer');
    }
    public function bus_add()
    {
      $amenities = $this->api->get_amenities_master();
      $layouts = $this->api->get_layout_master();
      $vehicle_types = $this->api->get_category_vehicles_master();
      $this->load->view('user_panel/bus_add_view', compact('amenities','layouts', 'vehicle_types'));
      $this->load->view('user_panel/footer');
    }
    public function bus_add_details()
    {
      //echo json_encode($_POST); die();
      if(isset($_POST['manage_seat_layout']) && $_POST['manage_seat_layout'] == '1'){
        if( !empty($_FILES["bus_image_url"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/bus-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('bus_image_url')) {   
            $uploads    = $this->upload->data();  
            $image_url =  $config['upload_path'].$uploads["file_name"]; 
          } else { $image_url = "NULL"; }
        } else { $image_url = "NULL"; }

        if( !empty($_FILES["bus_icon_url"]["tmp_name"]) ) {
          $config['upload_path']    = 'resources/bus-images/';                 
          $config['allowed_types']  = '*';       
          $config['max_size']       = '0';                          
          $config['max_width']      = '0';                          
          $config['max_height']     = '0';                          
          $config['encrypt_name']   = true; 
          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('bus_icon_url')) {   
            $uploads    = $this->upload->data();  
            $icon_url =  $config['upload_path'].$uploads["file_name"]; 
          } else{ $icon_url = "NULL"; }
        } else{ $icon_url = "NULL"; }

        $this->load->view('user_panel/manage_vip_seat_layout', compact('$_POST','image_url','icon_url'));
        $this->load->view('user_panel/footer');
      } else {
        //echo json_encode($_POST); die();
        if(isset($_POST['is_vip_seat']) && $_POST['is_vip_seat'] == '1') {
          $bus_amenities = $this->input->post('bus_amenities', TRUE);
          $seat_type = $this->input->post('seat_type', TRUE);
          $seat_type = explode(',',$seat_type);
          $seat_type_count = $this->input->post('seat_type_count', TRUE);
          $seat_type_count = explode(',',$seat_type_count);
          $image_url = $this->input->post('image_url', TRUE);
          $icon_url = $this->input->post('icon_url', TRUE);
          $vip_seats = $this->input->post('vip_seats', TRUE);

          $s_types_a = $this->input->post('s_types_a', TRUE);
          $s_types_b = $this->input->post('s_types_b', TRUE);
          $s_types_c = $this->input->post('s_types_c', TRUE);
          $s_types_d = $this->input->post('s_types_d', TRUE);
          $s_types_e = $this->input->post('s_types_e', TRUE);
          if(!isset($s_types_e) || $s_types_e == '') $s_types_e = 'NULL';
          else $s_types_e = explode(',', $s_types_e)[sizeof(explode(',', $s_types_e))-1];
        } else {
          $bus_amenities = $this->input->post('bus_amenities', TRUE);
          $bus_amenities = implode(',', $bus_amenities);
          $seat_type = $this->input->post('seat_type', TRUE);
          $seat_type_count = $this->input->post('seat_type_count', TRUE);
          $vip_seats = 'NULL';
          $s_types_a = 'NULL';
          $s_types_b = 'NULL';
          $s_types_c = 'NULL';
          $s_types_d = 'NULL';
          $s_types_e = 'NULL';
          if( !empty($_FILES["bus_image_url"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/bus-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('bus_image_url')) {   
              $uploads    = $this->upload->data();  
              $image_url =  $config['upload_path'].$uploads["file_name"]; 
            } else { $image_url = "NULL"; }
          } else { $image_url = "NULL"; }
          if( !empty($_FILES["bus_icon_url"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/bus-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('bus_icon_url')) {   
              $uploads    = $this->upload->data();  
              $icon_url =  $config['upload_path'].$uploads["file_name"]; 
            } else{ $icon_url = "NULL"; }
          } else{ $icon_url = "NULL"; }
        }
        $cust_id = $this->cust_id;
        $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
        $bus_make = $this->input->post('bus_make', TRUE);
        $bus_modal = $this->input->post('bus_modal', TRUE);
        $bus_no = $this->input->post('bus_no', TRUE);
        $bus_seat_type = $this->input->post('bus_seat_type', TRUE);
        $bus_ac = $this->input->post('bus_ac', TRUE);
        $today = date('Y-m-d h:i:s');

        $insert_data = array(
          "bus_make" => trim($bus_make),
          "bus_modal" => trim($bus_modal),
          "bus_image_url" => trim($image_url),
          "bus_icon_url" => trim($icon_url),
          "bus_seat_type" => trim($bus_seat_type),
          "bus_ac" => trim($bus_ac),
          "bus_amenities" => trim($bus_amenities),
          "cre_datetime" => $today,
          "cust_id" => (int)$cust_id,
          "bus_no" => trim($bus_no),
          "bus_status" => 1,
          "total_seats" => (int)array_sum($seat_type_count),
          "vehical_type_id" => trim($vehical_type_id),
          "vip_seat_nos" => trim(rtrim($vip_seats, ',')),
          "s_types_a" => trim($s_types_a),
          "s_types_b" => trim($s_types_b),
          "s_types_c" => trim($s_types_c),
          "s_types_d" => trim($s_types_d),
          "s_types_e" => trim($s_types_e),
        );

        //echo json_encode($insert_data); die();
        if($bus_id = $this->api->add_bus_details($insert_data)){
          for ($i=0; $i < sizeof($seat_type); $i++) { 
            $seat_type_data = array(
              "bus_id" => trim($bus_id),
              "cust_id" => trim($cust_id),
              "type_of_seat" => trim($seat_type[$i]),
              "total_seats" => trim($seat_type_count[$i]),
              "cre_datetime" => $today,
            );
            $this->api->add_bus_seat_type_details($seat_type_data);
          }
          $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
        } else {  $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  } 
        redirect('user-panel-bus/bus-add');
      }
    }
    public function bus_edit()
    {
      if( !is_null($this->input->post('bus_id')) ) { $bus_id = (int) $this->input->post('bus_id'); }
      else if(!is_null($this->uri->segment(3))) { $bus_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel/bus-list'); }

      $bus_details = $this->api->get_operator_bus_details($bus_id);
      $bus_seat_details = $this->api->get_operator_bus_seat_details($bus_id);
      $amenities = $this->api->get_amenities_master();
      $layouts = $this->api->get_layout_master();
      $this->load->view('user_panel/bus_edit_view', compact('bus_details','amenities','layouts','bus_seat_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_edit_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $bus_id = $this->input->post('bus_id', TRUE);
      $bus_make = $this->input->post('bus_make', TRUE);
      $bus_modal = $this->input->post('bus_modal', TRUE);
      $bus_no = $this->input->post('bus_no', TRUE);
      $bus_image_url_old = $this->input->post('bus_image_url_old', TRUE);
      $bus_icon_url_old = $this->input->post('bus_icon_url_old', TRUE);
      $bus_seat_type = $this->input->post('bus_seat_type', TRUE);
      $bus_ac = $this->input->post('bus_ac', TRUE);
      $std_id = $this->input->post('std_id', TRUE);
      $seat_type_count = $this->input->post('seat_type_count', TRUE);
      $bus_amenities = $this->input->post('bus_amenities', TRUE);
      $bus_amenities = implode(',', $bus_amenities);
      $today = date('Y-m-d h:i:s');

      $bus_details = $this->api->get_operator_bus_details($bus_id);

      if( !empty($_FILES["bus_image_url"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/bus-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('bus_image_url')) {   
          $uploads    = $this->upload->data();  
          $image_url =  $config['upload_path'].$uploads["file_name"]; 
          if( $bus_details['bus_image_url'] != "NULL" ) { unlink($bus_details['bus_image_url']); }
        } else { $image_url = $bus_image_url_old; }
      } else { $image_url = $bus_image_url_old; }

      if( !empty($_FILES["bus_icon_url"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/bus-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('bus_icon_url')) {   
          $uploads    = $this->upload->data();  
          $icon_url =  $config['upload_path'].$uploads["file_name"];
          if( $bus_details['bus_icon_url'] != "NULL" ) { unlink($bus_details['bus_icon_url']); }
        } else{ $icon_url = $bus_icon_url_old; }
      } else{ $icon_url = $bus_icon_url_old; }

      $update_data = array(
        "bus_make" => trim($bus_make),
        "bus_modal" => trim($bus_modal),
        "bus_image_url" => trim($image_url),
        "bus_icon_url" => trim($icon_url),
        "bus_seat_type" => trim($bus_seat_type),
        "bus_ac" => trim($bus_ac),
        "bus_amenities" => trim($bus_amenities),
        "bus_no" => trim($bus_no),
        "total_seats" => (int)array_sum($seat_type_count),
      );

      //echo json_encode($update_data); die();
      if($this->api->update_bus_details($update_data, $bus_id)){
        //update seat count
        for ($i=0; $i < sizeof($std_id); $i++) { 
          $seat_type_data = array(
            "total_seats" => trim($seat_type_count[$i]),
          );
          $this->api->update_bus_seat_type_details($seat_type_data, $std_id[$i]);
        }
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else {  $this->session->set_flashdata('error',$this->lang->line('update_failed'));  } 
      redirect(base_url('user-panel-bus/bus-edit/'.$bus_id));
    }
    public function bus_delete()
    {
      $bus_id = $this->input->post('bus_id', TRUE);
      if($this->api->delete_bus_details((int)$bus_id)){ echo 'success'; } else { echo 'failed';  } 
    }
    public function get_vehicle_seat_type()
    {
      $vehical_type_id = $this->input->post('id');
      echo json_encode($this->api->get_seat_type_list($vehical_type_id));
    }
    public function bus_layout_manager()
    {
      if( !is_null($this->input->post('bus_id')) ) { $bus_id = (int) $this->input->post('bus_id'); }
      else if(!is_null($this->uri->segment(3))) { $bus_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel/bus-list'); }

      $bus_details = $this->api->get_operator_bus_details($bus_id);
      $bus_seat_details = $this->api->get_operator_bus_seat_details($bus_id);
      $amenities = $this->api->get_amenities_master();
      $layouts = $this->api->get_layout_master();
      $this->load->view('user_panel/bus_layout_manager_view', compact('bus_details','amenities','layouts','bus_seat_details'));
      $this->load->view('user_panel/footer');
    }
  //End Manage Bus-----------------------------------------

  //Start Bus Drivers--------------------------------------
    public function driver_list()
    {
      $drivers = $this->api->get_customer_bus_drivers($this->cust_id);
      $this->load->view('user_panel/bus_driver_list_view', compact('drivers'));
      $this->load->view('user_panel/footer');
    }
    public function driver_inactive_list()
    {
      $drivers = $this->api->get_customer_drivers_inactive($this->cust_id);
      $this->load->view('user_panel/bus_driver_inactive_list_view', compact('drivers'));
      $this->load->view('user_panel/footer');
    }
    public function view_driver()
    {
      if( !is_null($this->input->post('cd_id')) ) { $cd_id = (int) $this->input->post('cd_id'); }
      else if(!is_null($this->uri->segment(3))) { $cd_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel/driver-list'); }
      $drivers = $this->api->get_driver_datails($cd_id);
      $category = explode(',', $drivers[0]['cat_ids']); 
      $category_name = array();           
      for($i=0; $i < sizeof($category); $i++){
        $cname = $this->user->get_driver_category_name($category[$i]);
        array_push($category_name, $cname['short_name']); 
      }
      
      $upcoming = $this->api->get_driver_trip_master_list_filter($drivers[0]['vehicle_id'],'upcoming');
      $current = $this->api->get_driver_trip_master_list_filter($drivers[0]['vehicle_id'],'current');
      $previous = $this->api->get_driver_trip_master_list_filter($drivers[0]['vehicle_id'],'previous');
    
      //echo json_encode($upcoming); die();
      //get_trip_count
      $this->load->view('user_panel/bus_driver_details_view', compact('drivers','category_name','upcoming', 'current' , 'previous'));
      $this->load->view('user_panel/footer');
    }
    public function add_driver()
    {
      $categories = $this->api->get_permitted_cat();
      $countries = $this->user->get_countries();
      $vehicle_types = $this->api->get_category_vehicles_master();
      $this->load->view('user_panel/bus_driver_add_view', compact('categories','countries','vehicle_types'));
      $this->load->view('user_panel/footer');
    }
    public function get_vehicle_by_type_id()
    {
      $vehical_type_id = $this->input->post('id');
      echo json_encode($this->api->get_vehicle_by_type_id($vehical_type_id, $this->cust_id));
    }
    public function add_driver_details()
    {
     // echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$this->cust_id;
      $first_name = $this->input->post('first_name', TRUE);
      $last_name = $this->input->post('last_name', TRUE);
      $email = $this->input->post('email', TRUE); 
      $country_id = $this->input->post('country_id', TRUE); 
      $country_code = $this->input->post('country_code', TRUE); 
      $password = $this->input->post('password', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $mobile2 = $this->input->post('mobile2', TRUE);
      $cid = $this->input->post('cat_ids', TRUE);
      if(isset($cid) && !empty($cid)) { $cat_ids = implode(',',$this->input->post('cat_ids', TRUE)); } else $cat_ids = 'NULL';
      $avail = $this->input->post('available', TRUE);
      if(isset($avail) && !empty($avail)) { $available = implode(',',$this->input->post('available', TRUE)); } else $available = 'NULL';
      $country_name = $this->api->get_country_name_by_id((int)$country_id);
      $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
      $bus_id = $this->input->post('bus_id', TRUE);
      $today = date('Y-m-d H:i:s');

      if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('profile_image')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];  
        }         
      } else { $avatar_url ="NULL"; }

      $driver_insert_data = array(
        "cust_id" => trim($cust_id),
        "first_name" => trim($first_name),
        "last_name" => trim($last_name),
        "email" => trim($email),
        "country_id" => (int)$country_id,
        "country_code" => (int)$country_code,
        "country_name" => trim($country_name),
        "password" => trim($password),
        "mobile1" => trim($mobile1),
        "mobile2" => trim($mobile2),
        "cat_ids" => trim($cat_ids),
        "available" => trim($available),
        "avatar" => trim($avatar_url),
        "latitude" => 'NULL',
        "longitude" => 'NULL',
        "loc_update_datetime" => $today,
        "cre_datetime" => $today,
        "mod_datetime" => $today,
        "is_bus_driver" => 1,
        "vehical_type_id" => (int)$vehical_type_id,
        "vehicle_id" => (int)$bus_id,
      );

      if(!$this->api->is_driver_exists(trim($email))){ 
        if( $cd_id = $this->api->register_driver($driver_insert_data) ){
          $driver_count = ((int)$cust['driver_count'] + 1);      
          $this->api->update_driver_count($driver_count, $cust['cust_id']);

          $subject = $this->lang->line('create_driver_title');
          $message ="<p class='lead'>".$this->lang->line('create_driver_email_body1')."</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_email')." : ".trim($email)."</p>";
          $message ="<p class='lead'>".$this->lang->line('driver_password')." : ".trim($password)."</p>";
          $message .="</td></tr><tr><td align='center'><br />";
          // Send email to given email id
          $this->api_sms->send_email_text(trim($first_name), trim($email), $subject, trim($message));
          $sms_msg = $this->lang->line('driver_sms_msg1') . trim($email) . ' ' . $this->lang->line('driver_sms_msg2') . trim($password);
          $this->api->sendSMS((int)$country_code.trim($mobile1), $sms_msg);
          $link = "https://tinyurl.com/y932j77c";
          $sms_app = $this->lang->line('download_driver_app') . $link;
          $this->api->sendSMS((int)$country_code.trim($mobile1), $sms_app);
          $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); }
      } else { $this->session->set_flashdata('error',$this->lang->line('email_already_registered')); }
      redirect('user-panel-bus/add-driver');
    }
    public function edit_driver()
    {
      if( !is_null($this->input->post('cd_id')) ) { $cd_id = (int) $this->input->post('cd_id'); }
      else if(!is_null($this->uri->segment(3))) { $cd_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/driver-list'); }
      $categories = $this->api->get_permitted_cat();
      $driver_details = $this->api->get_driver_datails($cd_id);
      $countries = $this->user->get_countries();
      $vehicle_types = $this->api->get_category_vehicles_master();
      $vehicles = $this->api->get_vehicle_by_type_id($driver_details[0]['vehical_type_id']);
      $this->load->view('user_panel/bus_driver_edit_view', compact('categories','driver_details','countries','vehicle_types','vehicles'));
      $this->load->view('user_panel/footer');
    }
    public function update_driver_details()
    {
      //echo json_encode($_POST); die(); 
      $cd_id = (int)$this->input->post('cd_id', TRUE);
      $first_name = $this->input->post('first_name', TRUE);
      $last_name = $this->input->post('last_name', TRUE);
      $email = $this->input->post('email', TRUE); 
      $country_id = $this->input->post('country_id', TRUE); 
      $country_code = $this->input->post('country_code', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $mobile2 = $this->input->post('mobile2', TRUE);
      $cid = $this->input->post('cat_ids', TRUE);
      if(isset($cid) && !empty($cid)) { $cat_ids = implode(',',$this->input->post('cat_ids', TRUE)); } else $cat_ids = 'NULL';
      $avail = $this->input->post('available', TRUE);
      if(isset($avail) && !empty($avail)) { $available = implode(',',$this->input->post('available', TRUE)); } else $available = 'NULL';
      $country_name = $this->api->get_country_name_by_id((int)$country_id);
      $today = date('Y-m-d H:i:s');
      $password = $this->input->post('password', TRUE);
      $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
      $bus_id = $this->input->post('bus_id', TRUE);
      
      if($password != '' ){
        $password = $this->input->post('password', TRUE);
        $update_data = array(
          "first_name" => trim($first_name),
          "last_name" => trim($last_name),
          "email" => trim($email),
          "country_id" => trim($country_id),
          "country_code" => (int)$country_code,
          "country_name" => trim($country_name),
          "password" => trim($password),
          "mobile1" => trim($mobile1),
          "mobile2" => trim($mobile2),
          "cat_ids" => trim($cat_ids),
          "available" => trim($available),
          "mod_datetime" => $today,
          "vehical_type_id" => (int)$vehical_type_id,
          "vehicle_id" => (int)$bus_id,
        );
      } else {
        $update_data = array(
          "first_name" => trim($first_name),
          "last_name" => trim($last_name),
          "email" => trim($email),
          "country_id" => trim($country_id),
          "country_code" => (int)$country_code,
          "country_name" => trim($country_name),
          "mobile1" => trim($mobile1),
          "mobile2" => trim($mobile2),
          "cat_ids" => trim($cat_ids),
          "available" => trim($available),
          "mod_datetime" => $today,
          "vehical_type_id" => (int)$vehical_type_id,
          "vehicle_id" => (int)$bus_id,
        );
      } 
      //var_dump($update_data); die();
      if(!$this->api->is_driver_exists_excluding(trim($email), $cd_id)) { 
        if( $this->api->update_driver($update_data, $cd_id) ){
          $this->session->set_flashdata('success',$this->lang->line('uploaded_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('upload_failed')); }
      } else { $this->session->set_flashdata('error',$this->lang->line('email_already_registered')); }
      redirect($this->config->item('base_url').'user-panel-bus/edit-driver/'.$cd_id);
    }
    public function driver_inactive()
    {
      $cust = $this->cust;
      $cd_id = $this->input->post('id', TRUE);
      if($this->api->inactivate_driver($cd_id)){ 
        $driver_count = (int)$cust['driver_count'];
        if($driver_count > 0 ) {  $driver_count -= 1;  $this->api->update_driver_count($driver_count, $cust['cust_id']); }
        echo 'success'; 
      } else { echo "failed";  } 
    }
    public function driver_active()
    {
      $cust = $this->cust;
      $cd_id = $this->input->post('id', TRUE);
      if($this->api->activate_driver($cd_id)){ 
        $driver_count = (int)$cust['driver_count']; $driver_count += 1;  
        $this->api->update_driver_count($driver_count, $cust['cust_id'] );
        echo 'success'; 
      } 
      else { echo "failed";  } 
    }
    public function driver_avatar_update()
    {
      $cd_id = $this->input->post('cd_id', TRUE);
      if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/'; 
        $config['allowed_types']  = '*'; 
        $config['max_size']       = '0'; 
        $config['max_width']      = '0';
        $config['max_height']     = '0'; 
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('profile_image')) {
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];  
          $this->api->delete_old_avatar_driver($cd_id);
        }      

        if( $this->api->update_avatar_driver($avatar_url, $cd_id)) {
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('update_failed')); } 

        redirect($this->config->item('base_url').'user-panel-bus/edit-driver/'.$cd_id);
      } else { 
        $this->session->set_flashdata('error',$this->lang->line('update_failed')); 
        redirect($this->config->item('base_url').'user-panel-bus/edit-driver/'.$cd_id); 
      }
    }
    public function driver_trip_passengers_view()
    {
      //echo json_encode($_POST); die();
      $unique_id = $this->input->post('unique_id');
      $ticket_seat_details = $this->api->get_ticket_seat_details_unique_id($unique_id);
      $master_booking_details = $this->api->get_ticket_master_booking_details_unique_id($unique_id);
      $this->load->view('user_panel/driver_trip_passengers_view', compact('ticket_seat_details','master_booking_details'));
      $this->load->view('user_panel/footer');      
    }
  //End Bus Drivers----------------------------------------

  //Start Bus cancellation and rescheduling charges--------
    public function bus_operator_cancellation_charges_list()
    {
      $cancellation_details = $this->api->get_bus_operator_cancellation_details($this->cust_id);
      $today = date('Y-m-d h:i:s');
      if(empty($cancellation_details)) {
        $cancellation_details = $this->api->get_master_cancellation_details();
        for($i=0; $i<sizeof($cancellation_details); $i++) {
          $insert_data = array(
            "cust_id" => (int)$this->cust_id,
            "bcr_min_hours" => $cancellation_details[$i]['bcr_min_hours'],
            "bcr_max_hours" => $cancellation_details[$i]['bcr_max_hours'],
            "bcr_cancellation" => $cancellation_details[$i]['bcr_cancellation'],
            "bcr_rescheduling" => $cancellation_details[$i]['bcr_rescheduling'],
            "country_id" => (int)$cancellation_details[$i]['country_id'],
            "bcr_status" => (int)$cancellation_details[$i]['bcr_status'],
            "vehical_type_id" => (int)$cancellation_details[$i]['vehical_type_id'],
            "bcr_cre_datetime" => $today,
          );
          $this->api->register_bus_operator_cancellation_details($insert_data);
        }
        $cancellation_details = $this->api->get_bus_operator_cancellation_details($this->cust_id);
      }
      $this->load->view('user_panel/cancellation_rescheduling_list_view',compact('cancellation_details'));
      $this->load->view('user_panel/footer');
    }
    public function add_bus_operator_cancellation_charges()
    {
      $countries = $this->user->get_countries($this->cust_id);
      $vehicle_types = $this->api->get_category_vehicles_master();
      $this->load->view('user_panel/bus_operator_cancellation_charges_add_view', compact('countries','vehicle_types'));
      $this->load->view('user_panel/footer');
    }
    public function add_bus_operator_cancellation_charges_details()
    {
      $cust_id = (int)$this->cust_id;
      $bcr_min_hours = $this->input->post('bcr_min_hours', TRUE);
      $bcr_max_hours = $this->input->post('bcr_max_hours', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $bcr_cancellation = $this->input->post('bcr_cancellation', TRUE);
      $bcr_rescheduling = $this->input->post('bcr_rescheduling', TRUE);
      $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
      $today = date('Y-m-d H:i:s');

      $insert_data = array(
        "cust_id" => (int)$this->cust_id,
        "bcr_min_hours" => trim($bcr_min_hours),
        "bcr_max_hours" => trim($bcr_max_hours),
        "bcr_cancellation" => trim($bcr_cancellation),
        "bcr_rescheduling" => trim($bcr_rescheduling),
        "country_id" => (int)trim($country_id),
        "bcr_status" => 1,
        "bcr_cre_datetime" => $today,
        "vehical_type_id" => (int)trim($vehical_type_id),
      );

      if(!$this->api->check_payment($insert_data, 'min', $cust_id) && !$this->api->check_payment($insert_data, 'max', $cust_id)) {
        if($this->api->register_bus_operator_cancellation_details($insert_data) ) {
          $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); }
      } else { $this->session->set_flashdata('error',$this->lang->line('configuration_exists')); }  
      redirect('user-panel-bus/add-bus-operator-cancellation-charges');
    }
    public function edit_bus_operator_cancellation_charges()
    {
      if( !is_null($this->input->post('bcr_id')) ) { $bcr_id = (int) $this->input->post('bcr_id'); }
      else if(!is_null($this->uri->segment(3))) { $bcr_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/driver-list'); }
      $charge_details = $this->api->get_cancellation_charges_datails((int)$bcr_id);
      $countries = $this->user->get_countries($this->cust_id);
      $vehicle_types = $this->api->get_category_vehicles_master();
      $this->load->view('user_panel/bus_operator_cancellation_charges_edit_view', compact('countries','charge_details','vehicle_types'));
      $this->load->view('user_panel/footer');
    }
    public function edit_bus_operator_cancellation_charges_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $bcr_id = $this->input->post('bcr_id', TRUE);
      $bcr_min_hours = $this->input->post('bcr_min_hours', TRUE);
      $bcr_max_hours = $this->input->post('bcr_max_hours', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $bcr_cancellation = $this->input->post('bcr_cancellation', TRUE);
      $bcr_rescheduling = $this->input->post('bcr_rescheduling', TRUE);
      $vehical_type_id = $this->input->post('vehical_type_id', TRUE);

      $update_data = array(
        "bcr_min_hours" => trim($bcr_min_hours),
        "bcr_max_hours" => trim($bcr_max_hours),
        "bcr_cancellation" => trim($bcr_cancellation),
        "bcr_rescheduling" => trim($bcr_rescheduling),
        "country_id" => (int)trim($country_id),
        "vehical_type_id" => (int)trim($vehical_type_id),
        "cust_id" => (int)$cust_id,
      );

      if(!$this->api->check_payment($update_data, 'min', $cust_id) && !$this->api->check_payment($update_data, 'max', $cust_id)) {
        if($this->api->update_bus_operator_cancellation_details($update_data, $bcr_id) ) {
          $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('update_failed')); }
      } else { $this->session->set_flashdata('error',$this->lang->line('configuration_exists')); }  
      redirect('user-panel-bus/edit-bus-operator-cancellation-charges/'.$bcr_id);
    }
    public function cencellation_rescheduling_charges_delete()
    {
      $bcr_id = $this->input->post('bcr_id', TRUE);
      if($this->api->delete_cencellation_rescheduling_charges_details((int)$bcr_id)){ echo 'success'; } else { echo 'failed';  } 
    }
  //End Bus cancellation and rescheduling charges----------

  //Start Agent permission---------------------------------
    public function agent_permissions_list()
    {
      $permissions = $this->api->get_operator_agent_group_permissions_list($this->cust_id);
      $this->load->view('user_panel/bus_agent_permission_list_view',compact('permissions'));
      $this->load->view('user_panel/footer');
    }
    public function add_agent_permissions()
    {
      $this->load->view('user_panel/bus_agent_permission_add_view');
      $this->load->view('user_panel/footer');
    }
    public function group_permission_add_details()
    { 
      //echo json_encode($_POST); die();
      $cust = $this->cust;
      $cust_id = (int)$this->cust_id;
      $group_name = $this->input->post('group_name', TRUE);
      // Dashboard
      if($this->input->post('dashboard_view', TRUE) == '') { $dashboard_view = 0; } else { $dashboard_view = $this->input->post('dashboard_view', TRUE); }
      $dashboard = $dashboard_view;

      // Address Book
      if($this->input->post('address_book_view', TRUE) == '') { $address_book_view = 0; } else { $address_book_view = $this->input->post('address_book_view', TRUE); }
      if($this->input->post('address_book_add', TRUE) == '') { $address_book_add = 0; } else { $address_book_add = $this->input->post('address_book_add', TRUE); }
      if($this->input->post('address_book_update', TRUE) == '') { $address_book_update = 0; } else { $address_book_update = $this->input->post('address_book_update', TRUE); }
      if($this->input->post('address_book_delete', TRUE) == '') { $address_book_delete = 0; } else { $address_book_delete = $this->input->post('address_book_delete', TRUE); }
      $address_book = $address_book_view.','.$address_book_add.','.$address_book_update.','.$address_book_delete;

      // Basic Info
      if($this->input->post('basic_info_view', TRUE) == '') { $basic_info_view = 0; } else { $basic_info_view = $this->input->post('basic_info_view', TRUE); }
      if($this->input->post('basic_info_update', TRUE) == '') { $basic_info_update = 0; } else { $basic_info_update = $this->input->post('basic_info_update', TRUE); }
      $basic_info = $basic_info_view.','.$basic_info_update;

      // Address Book
      if($this->input->post('business_photos_view', TRUE) == '') { $business_photos_view = 0; } else { $business_photos_view = $this->input->post('business_photos_view', TRUE); }
      if($this->input->post('business_photos_add', TRUE) == '') { $business_photos_add = 0; } else { $business_photos_add = $this->input->post('business_photos_add', TRUE); }
      if($this->input->post('business_photos_delete', TRUE) == '') { $business_photos_delete = 0; } else { $business_photos_delete = $this->input->post('business_photos_delete', TRUE); }
      $business_photos = $business_photos_view.','.$business_photos_add.','.$business_photos_delete;

      // Manage Bus
      if($this->input->post('manage_bus_view', TRUE) == '') { $manage_bus_view = 0; } else { $manage_bus_view = $this->input->post('manage_bus_view', TRUE); }
      if($this->input->post('manage_bus_add', TRUE) == '') { $manage_bus_add = 0; } else { $manage_bus_add = $this->input->post('manage_bus_add', TRUE); }
      if($this->input->post('manage_bus_update', TRUE) == '') { $manage_bus_update = 0; } else { $manage_bus_update = $this->input->post('manage_bus_update', TRUE); }
      if($this->input->post('manage_bus_delete', TRUE) == '') { $manage_bus_delete = 0; } else { $manage_bus_delete = $this->input->post('manage_bus_delete', TRUE); }
      $manage_bus = $manage_bus_view.','.$manage_bus_add.','.$manage_bus_update.','.$manage_bus_delete;

      // Operating Locations
      if($this->input->post('operating_locations_view', TRUE) == '') { $operating_locations_view = 0; } else { $operating_locations_view = $this->input->post('operating_locations_view', TRUE); }
      if($this->input->post('operating_locations_add', TRUE) == '') { $operating_locations_add = 0; } else { $operating_locations_add = $this->input->post('operating_locations_add', TRUE); }
      if($this->input->post('operating_locations_update', TRUE) == '') { $operating_locations_update = 0; } else { $operating_locations_update = $this->input->post('operating_locations_update', TRUE); }
      if($this->input->post('operating_locations_delete', TRUE) == '') { $operating_locations_delete = 0; } else { $operating_locations_delete = $this->input->post('operating_locations_delete', TRUE); }
      $operating_locations = $operating_locations_view.','.$operating_locations_add.','.$operating_locations_update.','.$operating_locations_delete;

      // Drivers
      if($this->input->post('drivers_view', TRUE) == '') { $drivers_view = 0; } else { $drivers_view = $this->input->post('drivers_view', TRUE); }
      if($this->input->post('drivers_add', TRUE) == '') { $drivers_add = 0; } else { $drivers_add = $this->input->post('drivers_add', TRUE); }
      if($this->input->post('drivers_update', TRUE) == '') { $drivers_update = 0; } else { $drivers_update = $this->input->post('drivers_update', TRUE); }
      if($this->input->post('drivers_active', TRUE) == '') { $drivers_active = 0; } else { $drivers_active = $this->input->post('drivers_active', TRUE); }
      $drivers = $drivers_view.','.$drivers_add.','.$drivers_update.','.$drivers_active;

      // Agent Permissions
      if($this->input->post('agent_permissions_view', TRUE) == '') { $agent_permissions_view = 0; } else { $agent_permissions_view = $this->input->post('agent_permissions_view', TRUE); }
      if($this->input->post('agent_permissions_add', TRUE) == '') { $agent_permissions_add = 0; } else { $agent_permissions_add = $this->input->post('agent_permissions_add', TRUE); }
      if($this->input->post('agent_permissions_update', TRUE) == '') { $agent_permissions_update = 0; } else { $agent_permissions_update = $this->input->post('agent_permissions_update', TRUE); }
      if($this->input->post('agent_permissions_delete', TRUE) == '') { $agent_permissions_delete = 0; } else { $agent_permissions_delete = $this->input->post('agent_permissions_delete', TRUE); }
      $agent_permissions = $agent_permissions_view.','.$agent_permissions_add.','.$agent_permissions_update.','.$agent_permissions_delete;

      // Agent Permissions
      if($this->input->post('agents_view', TRUE) == '') { $agents_view = 0; } else { $agents_view = $this->input->post('agents_view', TRUE); }
      if($this->input->post('agents_add', TRUE) == '') { $agents_add = 0; } else { $agents_add = $this->input->post('agents_add', TRUE); }
      if($this->input->post('agents_update', TRUE) == '') { $agents_update = 0; } else { $agents_update = $this->input->post('agents_update', TRUE); }
      if($this->input->post('agents_delete', TRUE) == '') { $agents_delete = 0; } else { $agents_delete = $this->input->post('agents_delete', TRUE); }
      $agents = $agents_view.','.$agents_add.','.$agents_update.','.$agents_delete;

      // Cancel and reschedule
      if($this->input->post('cancel_reschedule_view', TRUE) == '') { $cancel_reschedule_view = 0; } else { $cancel_reschedule_view = $this->input->post('cancel_reschedule_view', TRUE); }
      if($this->input->post('cancel_reschedule_add', TRUE) == '') { $cancel_reschedule_add = 0; } else { $cancel_reschedule_add = $this->input->post('cancel_reschedule_add', TRUE); }
      if($this->input->post('cancel_reschedule_update', TRUE) == '') { $cancel_reschedule_update = 0; } else { $cancel_reschedule_update = $this->input->post('cancel_reschedule_update', TRUE); }
      if($this->input->post('cancel_reschedule_delete', TRUE) == '') { $cancel_reschedule_delete = 0; } else { $cancel_reschedule_delete = $this->input->post('cancel_reschedule_delete', TRUE); }
      $cancel_reschedule = $cancel_reschedule_view.','.$cancel_reschedule_add.','.$cancel_reschedule_update.','.$cancel_reschedule_delete;

      // Cancel and reschedule
      if($this->input->post('special_rates_view', TRUE) == '') { $special_rates_view = 0; } else { $special_rates_view = $this->input->post('special_rates_view', TRUE); }
      if($this->input->post('special_rates_add', TRUE) == '') { $special_rates_add = 0; } else { $special_rates_add = $this->input->post('special_rates_add', TRUE); }
      if($this->input->post('special_rates_update', TRUE) == '') { $special_rates_update = 0; } else { $special_rates_update = $this->input->post('special_rates_update', TRUE); }
      if($this->input->post('special_rates_delete', TRUE) == '') { $special_rates_delete = 0; } else { $special_rates_delete = $this->input->post('special_rates_delete', TRUE); }
      $special_rates = $special_rates_view.','.$special_rates_add.','.$special_rates_update.','.$special_rates_delete;

      // My Trip
      if($this->input->post('my_trips_view', TRUE) == '') { $my_trips_view = 0; } else { $my_trips_view = $this->input->post('my_trips_view', TRUE); }
      if($this->input->post('my_trips_add', TRUE) == '') { $my_trips_add = 0; } else { $my_trips_add = $this->input->post('my_trips_add', TRUE); }
      if($this->input->post('my_trips_update', TRUE) == '') { $my_trips_update = 0; } else { $my_trips_update = $this->input->post('my_trips_update', TRUE); }
      if($this->input->post('my_trips_delete', TRUE) == '') { $my_trips_delete = 0; } else { $my_trips_delete = $this->input->post('my_trips_delete', TRUE); }
      $my_trips = $my_trips_view.','.$my_trips_add.','.$my_trips_update.','.$my_trips_delete;

      // Manage Trip
      if($this->input->post('manage_trip_view', TRUE) == '') { $manage_trip_view = 0; } else { $manage_trip_view = $this->input->post('manage_trip_view', TRUE); }
      if($this->input->post('manage_trip_add', TRUE) == '') { $manage_trip_add = 0; } else { $manage_trip_add = $this->input->post('manage_trip_add', TRUE); }
      if($this->input->post('manage_trip_update', TRUE) == '') { $manage_trip_update = 0; } else { $manage_trip_update = $this->input->post('manage_trip_update', TRUE); }
      if($this->input->post('manage_trip_delete', TRUE) == '') { $manage_trip_delete = 0; } else { $manage_trip_delete = $this->input->post('manage_trip_delete', TRUE); }
      $manage_trip = $manage_trip_view.','.$manage_trip_add.','.$manage_trip_update.','.$manage_trip_delete;

      // Accounts
      if($this->input->post('my_accounts_view', TRUE) == '') { $my_accounts_view = 0; } else { $my_accounts_view = $this->input->post('my_accounts_view', TRUE); }
      $my_accounts = $my_accounts_view;

      // Notifications
      if($this->input->post('view_notifications_view', TRUE) == '') { $view_notifications_view = 0; } else { $view_notifications_view = $this->input->post('view_notifications_view', TRUE); }
      $view_notifications = $view_notifications_view;

      // Work Room
      if($this->input->post('work_room_view', TRUE) == '') { $work_room_view = 0; } else { $work_room_view = $this->input->post('work_room_view', TRUE); }
      if($this->input->post('work_room_add', TRUE) == '') { $work_room_add = 0; } else { $work_room_add = $this->input->post('work_room_add', TRUE); }
      if($this->input->post('manage_trip_update', TRUE) == '') { $manage_trip_update = 0; } else { $manage_trip_update = $this->input->post('manage_trip_update', TRUE); }
      if($this->input->post('manage_trip_delete', TRUE) == '') { $manage_trip_delete = 0; } else { $manage_trip_delete = $this->input->post('manage_trip_delete', TRUE); }
      $work_room = $work_room_view.','.$work_room_add;

      $today = date('Y-m-d H:i:s');

      $driver_insert_data = array(
        "cust_id" => trim($cust_id),
        "group_name" => trim($group_name),
        "cre_datetime" => $today,
        "dashboard" => trim($dashboard),
        "address_book" => trim($address_book),
        "basic_info" => trim($basic_info),
        "business_photos" => trim($business_photos),
        "manage_bus" => trim($manage_bus),
        "operating_locations" => trim($operating_locations),
        "drivers" => trim($drivers),
        "agent_permissions" => trim($agent_permissions),
        "agents" => trim($agents),
        "cancel_reschedule" => trim($cancel_reschedule),
        "special_rates" => trim($special_rates),
        "my_trips" => trim($my_trips),
        "manage_trip" => trim($manage_trip),
        "my_accounts" => trim($my_accounts),
        "view_notifications" => trim($view_notifications),
        "work_room" => trim($work_room),
      );

      //echo json_encode($driver_insert_data); die();
      if($this->api->register_operator_agent_permission($driver_insert_data)) {
        $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
      } else {
        $this->session->set_flashdata('failed',$this->lang->line('unable_to_add'));
      }
      redirect('user-panel-bus/add-agent-permissions');
    }
    public function bus_operator_agent_active_list()
    {
      $agents = $this->api->get_active_agents_list($this->cust_id);
      $this->load->view('user_panel/bus_operator_agent_list_view',compact('agents'));
      $this->load->view('user_panel/footer');
    }
  //End Agent permission-----------------------------------

  //Start Operator Agents----------------------------------
    public function bus_operator_agent_add()
    {
      $countries = $this->user->get_countries($this->cust_id);
      $this->load->view('user_panel/bus_operator_agent_add_view', compact('countries'));
      $this->load->view('user_panel/footer');
    }
    public function bus_operator_agent_add_details()
    { 
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $email1 = $this->input->post('email1', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $password = $this->input->post('password', TRUE);
      $gender = $this->input->post('gender', TRUE);
      $today = date('Y-m-d H:i:s');

      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('avatar')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $avatar_url = "NULL"; }
      } else { $avatar_url = "NULL"; }

      if( !empty($_FILES["cover"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/cover-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('cover')) {   
          $uploads    = $this->upload->data();  
          $cover_url =  $config['upload_path'].$uploads["file_name"]; 
        } else{ $cover_url = "NULL"; }
      } else{ $cover_url = "NULL"; }

      $insert_data = array(
        "cust_id" => (int)$this->cust_id,
        "group_id" => 0,
        "firstname" => trim($firstname),
        "lastname" => trim($lastname),
        "email1" => trim($email1),
        "password" => trim($password),
        "mobile1" => trim($mobile1),
        "gender" => trim($gender),
        "city_id" => 0,
        "state_id" => 0,
        "country_id" => (int)trim($country_id),
        "avatar_url" => trim($avatar_url),
        "cover_url" => trim($cover_url),
        "email_verified" => 1,
        "mobile_verified" => 1,
        "cre_datetime" => $today,
        "last_login_datetime" => 'NULL',
        "status" => 1,
      );

      //echo json_encode($insert_data); die();

      if(!$this->api->chek_bus_operator_agent_email(trim($email1))) {
        if($this->api->add_bus_operator_agent_details($insert_data) ) {
          $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); }
      } else { $this->session->set_flashdata('error',$this->lang->line('Email already registered!')); }  
      redirect('user-panel-bus/bus-operator-agent-add');
    }
    public function bus_operator_agent_edit()
    {
      if( !is_null($this->input->post('agent_id')) ) { $agent_id = (int) $this->input->post('agent_id'); }
      else if(!is_null($this->uri->segment(3))) { $agent_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/bus-operator-agent-add'); }
      $agent_details = $this->api->get_bus_operator_agent_details($agent_id, $this->cust_id);
      $countries = $this->user->get_countries($this->cust_id);
      $this->load->view('user_panel/bus_operator_agent_edit_view', compact('agent_details','countries'));
      $this->load->view('user_panel/footer');
    }
    public function bus_operator_agent_edit_details()
    { 
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $agent_id = $this->input->post('agent_id', TRUE);
      $mobile1 = $this->input->post('mobile1', TRUE);
      $firstname = $this->input->post('firstname', TRUE);
      $lastname = $this->input->post('lastname', TRUE);
      $country_id = $this->input->post('country_id', TRUE);
      $password = $this->input->post('password', TRUE);
      $gender = $this->input->post('gender', TRUE);
      $old_avatar_url = $this->input->post('old_avatar_url', TRUE);
      $old_cover_url = $this->input->post('old_cover_url', TRUE);
      $today = date('Y-m-d H:i:s');

      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('avatar')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"]; 
        } else { $avatar_url = $old_avatar_url; }
      } else { $avatar_url = $old_avatar_url; }

      if( !empty($_FILES["cover"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/cover-images/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('cover')) {   
          $uploads    = $this->upload->data();  
          $cover_url =  $config['upload_path'].$uploads["file_name"]; 
        } else{ $cover_url = $old_cover_url; }
      } else{ $cover_url = $old_cover_url; }

      $update_data = array(
        "firstname" => trim($firstname),
        "lastname" => trim($lastname),
        "password" => trim($password),
        "mobile1" => trim($mobile1),
        "gender" => trim($gender),
        "country_id" => (int)trim($country_id),
        "avatar_url" => trim($avatar_url),
        "cover_url" => trim($cover_url),
      );

      //echo json_encode($update_data); die();

      if($this->api->edit_bus_operator_agent_details($update_data, $agent_id) ) {
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('update_failed')); }
      redirect('user-panel-bus/bus-operator-agent-edit/'.$agent_id);
    }
    public function bus_operator_agent_inactive_list()
    {
      $agents = $this->api->get_inactive_agents_list($this->cust_id);
      $this->load->view('user_panel/bus_operator_inactive_agent_list_view',compact('agents'));
      $this->load->view('user_panel/footer');
    }
    public function bus_operator_agent_activate()
    {
      $agent_id = $this->input->post('id', TRUE);
      if($this->api->activate_bus_operator_agent($agent_id)){ echo 'success'; 
      } else { echo "failed"; }
    }
    public function bus_operator_agent_inactivate()
    {
      $agent_id = $this->input->post('id', TRUE);
      if($this->api->inactivate_bus_operator_agent($agent_id)){ echo 'success'; 
      } else { echo "failed";  } 
    }
  //End Operator Agents------------------------------------

  //Start Pickup Drop Points-------------------------------
    public function bus_pick_drop_point_list()
    {
      $locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $this->load->view('user_panel/bus_pick_drop_point_list_view',compact('locations'));
      $this->load->view('user_panel/footer');
    }
    public function bus_pick_drop_point_list_view()
    {
      if( !is_null($this->input->post('loc_id')) ) { $loc_id = (int) $this->input->post('loc_id'); }
      else if(!is_null($this->uri->segment(3))) { $loc_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/bus-pick-drop-point-list'); }
      $cust_id = (int)$this->cust_id;
      $loc_data = $this->api->get_pickup_drop_operating_location_details($loc_id);
      $loc_city_name = $loc_data['city_name'];
      $points = $this->api->get_pickup_drop_points_list($cust_id, $loc_id);
      $vehicle_types = $this->api->get_category_vehicles_master();

      $this->load->view('user_panel/bus_pick_drop_point_view',compact('points','loc_id','loc_city_name','vehicle_types'));
      $this->load->view('user_panel/footer');
    }
    public function point_remove_as_pickup()
    {
      $point_id = $this->input->post('id', TRUE);
      if($this->api->remove_as_pickup_point($point_id)){ echo 'success'; 
      } else { echo "failed"; }
    }
    public function point_mark_as_pickup()
    {
      $point_id = $this->input->post('id', TRUE);
      if($this->api->mark_as_pickup_point($point_id)){ echo 'success'; 
      } else { echo "failed"; }
    }
    public function point_remove_as_drop()
    {
      $point_id = $this->input->post('id', TRUE);
      if($this->api->remove_as_drop_point($point_id)){ echo 'success'; 
      } else { echo "failed"; }
    }
    public function point_mark_as_drop()
    {
      $point_id = $this->input->post('id', TRUE);
      if($this->api->mark_as_drop_point($point_id)){ echo 'success'; 
      } else { echo "failed"; }
    }
    public function bus_pick_drop_point_add_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $loc_id = $this->input->post('loc_id', TRUE);
      $loc_city_name = $this->input->post('loc_city_name', TRUE);
      $point_address = $this->input->post('point_address', TRUE);
      $point_landmark = $this->input->post('point_landmark', TRUE);
      $point_widget_address = $this->input->post('street', TRUE);
      $point_country = $this->input->post('country', TRUE);
      $point_state = $this->input->post('state', TRUE);
      $point_city = $this->input->post('city', TRUE);
      $latitude = $this->input->post('latitude', TRUE);
      $longitude = $this->input->post('longitude', TRUE);
      $point_lat_long = $latitude.','.$longitude;
      $is_pickup = $this->input->post('is_pickup', TRUE); ($is_pickup=='on') ? $is_pickup = 1 : $is_pickup = 0;
      $is_drop = $this->input->post('is_drop', TRUE); ($is_drop=='on') ? $is_drop = 1 : $is_drop = 0;
      $point_postal_code = $this->input->post('zipcode', TRUE);
      $today = date('Y-m-d H:i:s');

      $location = $this->api->get_pickup_drop_operating_location_details($loc_id);
      $location_details = $this->api->get_pickup_drop_points_list($cust_id, $loc_id);

      if($location != null) {
        $insert_data = array(
          "loc_id" => (int)$loc_id,
          "loc_city_name" => trim($loc_city_name),
          "loc_state_name" => trim($location['state_name']),
          "loc_country_name" => trim($location['country_name']),
          "loc_zip_code" => trim($location['zip_code']),
          "loc_google_address" => trim($location['google_address']),
          "loc_lat_long" => trim($location['lat_long']),
          "cust_id" => (int)trim($cust_id),
          "point_address" => trim($point_address),
          "point_landmark" => trim($point_landmark),
          "point_widget_address" => trim($point_widget_address),
          "point_country" => trim($point_country),
          "point_state" => trim($point_state),
          "point_city" => trim($point_city),
          "point_postal_code" => trim($point_postal_code),
          "point_lat_long" => trim($point_lat_long),
          "is_pickup" => trim($is_pickup),
          "is_drop" => trim($is_drop),
          "cre_datetime" => $today,
          "status" => 1,
          "vehical_type_id" => trim($location_details[0]['vehical_type_id']),
        );

        //echo json_encode($insert_data); die();
        if($this->api->add_pick_drop_point_details($insert_data) ) {
          $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add')); }
      } else { $this->session->set_flashdata('error',$this->lang->line('Location not exists!')); }
      redirect('user-panel-bus/bus-pick-drop-point-list-view/'.$loc_id);
    }
    public function bus_pick_drop_location_and_point_add()
    {
      $vehicle_types = $this->api->get_category_vehicles_master();
      $this->load->view('user_panel/bus_pick_drop_location_and_point_add_view', compact('vehicle_types'));
      $this->load->view('user_panel/footer');
    }
    public function bus_pick_drop_location_and_point_add_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $city_name = $this->input->post('city_name', TRUE);
      $state_name = $this->input->post('state_location', TRUE);
      $country_name = $this->input->post('country_location', TRUE);
      $zip_code = $this->input->post('zipcode_location', TRUE);
      $google_address = $this->input->post('street_location', TRUE);
        $latitude_location = $this->input->post('latitude_location', TRUE);
        $longitude_location = $this->input->post('longitude_location', TRUE);
      $lat_long = $latitude_location.','.$longitude_location;
      $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
      $today = date('Y-m-d H:i:s');

      //Insert Location Data
      $insert_data_location = array(
        "city_name" => trim($city_name),
        "state_name" => trim($state_name),
        "country_name" => trim($country_name),
        "zip_code" => trim($zip_code),
        "google_address" => trim($google_address),
        "lat_long" => trim($lat_long),              
        "cre_datetime" => $today,
      );
     
      if($this->api->check_bus_location_by_city_state_country($insert_data_location, $cust_id, $vehical_type_id)) {
        $this->session->set_flashdata('error',$this->lang->line('Location exists!'));
      } else {
        //check location master for location exists
        if($location_master = $this->api->check_location_exists_master($insert_data_location)) {
          $loc_id = $location_master['loc_id'];
        } else { $loc_id = $this->api->add_bus_location_details($insert_data_location); }

        if($loc_id) { 
          //Insert Points Data
          for ($i=1; $i < 500; $i++) { 
            if(isset($_POST['point_address_'.$i])) {
              $point_address = $_POST['point_address_'.$i];
              $point_landmark = $_POST['point_landmark_'.$i];
              $point_widget_address = $_POST['point_street_'.$i];
              $point_country = $_POST['point_country_'.$i];
              $point_state = $_POST['point_state_'.$i];
              $point_city = $_POST['point_city_'.$i];
              $point_postal_code = $_POST['point_zipcode_'.$i];
                $point_lat = $_POST['point_latitude_'.$i];
                $point_long = $_POST['point_longitude_'.$i];
              $point_lat_long = $point_lat.','.$point_long;;

              if(isset($_POST['point_is_pickup_'.$i])) {
                ($_POST['point_is_pickup_'.$i]=='on') ? $is_pickup = 1 : $is_pickup = 0;
              } else { $is_pickup = 0; }
              if(isset($_POST['point_is_drop_'.$i])) {
                ($_POST['point_is_drop_'.$i]=='on') ? $is_drop = 1 : $is_drop = 0;
              } else { $is_drop = 0; }

              $insert_data_point = array(
                "loc_id" => (int)trim($loc_id),
                "loc_city_name" => trim($city_name),
                "loc_state_name" => trim($state_name),
                "loc_country_name" => trim($country_name),
                "loc_zip_code" => trim($zip_code),
                "loc_google_address" => trim($google_address),
                "loc_lat_long" => trim($lat_long),              
                "cust_id" => trim($cust_id),  
                "point_address" => $point_address,
                "point_landmark" => $point_landmark,
                "point_widget_address" => $point_widget_address,
                "point_country" => $point_country,
                "point_state" => $point_state,
                "point_city" => $point_city,
                "point_postal_code" => $point_postal_code,
                "point_lat_long" => $point_lat_long,
                "is_pickup" => $is_pickup,
                "is_drop" => $is_drop,            
                "cre_datetime" => $today,
                "status" => 1,
                "vehical_type_id" => trim($vehical_type_id),              
              );
              //echo json_encode($insert_data_point); die();
              $this->api->add_pick_drop_point_details($insert_data_point);
            }
          }
          $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
        } else { $this->session->set_flashdata('error',$this->lang->line('Error! while adding location details!')); }
      }
      redirect('user-panel-bus/bus-pick-drop-location-and-point-add');
    }
    public function bus_location_activate()
    {
      $cust_id = (int)$this->cust_id;
      $loc_id = $this->input->post('id', TRUE);
      if($this->api->activate_bus_location($loc_id, $cust_id)){ echo 'success'; 
      } else { echo "failed"; }
    }
    public function bus_location_inactivate()
    {
      $cust_id = (int)$this->cust_id;
      $loc_id = $this->input->post('id', TRUE);
      if($this->api->inactivate_bus_location($loc_id, $cust_id)){ echo 'success'; 
      } else { echo "failed"; }
    }
    public function bus_location_point_delete()
    {
      $point_id = $this->input->post('id', TRUE);
      if($this->api->pick_drop_point_delete($point_id)){ echo 'success'; 
      } else { echo "failed"; }
    }
  //End Pickup Drop Points---------------------------------

  //Start Address Book-------------------------------------
    public function address_book_list()
    {
      $address_details = $this->api->get_consumers_addressbook($this->cust_id);
      $this->load->view('user_panel/bus_address_book_view', compact('address_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_address_delete()
    {
      $address_id = $this->input->post('address_id');
      if( $this->api->delete_address_from_addressbook($address_id)) { echo "success"; 
      } else { echo "failed"; }
    }
    public function address_book_add()
    {
      $countries = $this->user->get_countries();
      $this->load->view('user_panel/bus_address_book_add_view',compact('countries'));   
      $this->load->view('user_panel/footer');
    }
    public function address_book_add_details()
    {   
      $cust = $this->cust;
      $firstname = $this->input->post("firstname");
      $lastname = $this->input->post("lastname");
      $email_id = $this->input->post("email_id");
      $gender = $this->input->post("gender");
      $country_id = $this->input->post("country_id");
      $mobile = $this->input->post("mobile");
      $dob = $this->input->post("age");
      $from = new DateTime($dob);
      $to   = new DateTime('today');
      $age  = $from->diff($to)->y;
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'firstname' => trim($firstname), 
        'lastname' => trim($lastname), 
        'email_id' => (trim($email_id) != "") ? trim($email_id) : "NULL", 
        'gender' => trim($gender), 
        'country_id' => trim($country_id), 
        'mobile' => trim($mobile), 
        'age' => (int)($age), 
        'dob' => ($dob), 
        'cre_dateteime' => $today,
      );
      
      //echo json_encode($add_data); die();
      if( $id = $this->api->register_new_address($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-bus/address-book-add');
    }
    public function bus_address_book_edit()
    {
      if( !is_null($this->input->post('address_id')) ) { $address_id = (int) $this->input->post('address_id'); }
      else if(!is_null($this->uri->segment(3))) { $address_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/address-book-list'); }

      $address = $this->api->get_address_from_book((int)$address_id);
      $countries = $this->user->get_countries();
      $this->load->view('user_panel/bus_address_book_edit_view',compact('countries','address'));    
      $this->load->view('user_panel/footer');
    }
    public function bus_address_book_edit_details()
    {
      $cust = $this->cust;
      $address_id = $this->input->post("address_id"); $address_id = (int)$address_id;
      $firstname = $this->input->post("firstname");
      $lastname = $this->input->post("lastname");
      $email_id = $this->input->post("email_id");
      $gender = $this->input->post("gender");
      $country_id = $this->input->post("country_id");
      $mobile = $this->input->post("mobile");
      $dob = $this->input->post("age");
      $from = new DateTime($dob);
      $to   = new DateTime('today');
      $age  = $from->diff($to)->y;
      $today = date('Y-m-d H:i:s');
      
      $edit_data = array(
        'firstname' => trim($firstname), 
        'lastname' => trim($lastname), 
        'email_id' => (trim($email_id) != "") ? trim($email_id) : "NULL", 
        'gender' => trim($gender), 
        'country_id' => trim($country_id), 
        'mobile' => trim($mobile), 
        'age' => (int)($age), 
        'dob' => ($dob), 
      );

      if( $this->api->update_existing_address($edit_data, $address_id)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }

      redirect('user-panel-bus/bus-address-book-edit/'.$address_id);
    }
    
  //End Address Book---------------------------------------

  //Start Trip Master--------------------------------------
    public function bus_trip_master_list()
    {
      $trip_master = $this->api->get_operator_trip_master($this->cust_id);
      $this->load->view('user_panel/bus_trip_master_list_view', compact('trip_master'));
      $this->load->view('user_panel/footer');
    }
    public function get_locations_by_vehicle_type_id()
    {
      $vehical_type_id = $this->input->post('id');
      echo json_encode($this->api->get_locations_by_vehicle_type_id($vehical_type_id, $this->cust_id));
    }
    public function bus_trip_master_add()
    {
      $operator = $this->api->get_deliverer_profile($this->cust_id);
      if($operator == null) {
          redirect(base_url('user-panel-bus/bus-operator-profile'),'refresh');
      } else {
        $locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
        $buses = $this->api->get_operator_bus_list($this->cust_id);
        $vehicle_types = $this->api->get_category_vehicles_master();
        $this->load->view('user_panel/bus_trip_master_add_view', compact('locations','buses','vehicle_types'));
        $this->load->view('user_panel/footer');
      }
    }
    public function bus_trip_master_add_details()
    {
      //echo json_encode($_POST);  die();
      $cust_id = (int)$this->cust_id;
      $vehical_type_id = $this->input->post('vehical_type_id', TRUE);
      $currency_details = $this->api->get_country_currency_detail($this->cust['country_id']);
      $currency_id = $currency_details['currency_id'];
      $currency_sign = $currency_details['currency_sign'];
      $bus_id = $this->input->post('bus_id', TRUE); $bus_id = (int)$bus_id;
      $booking_type = $this->input->post('booking_type', TRUE);
      $avail = $this->input->post('available', TRUE);
      if(isset($avail) && !empty($avail)) { $trip_availability = implode(',',$this->input->post('available', TRUE)); } else $trip_availability = 'NULL';
      $trip_depart_time = $this->input->post('trip_depart_time', TRUE);
      $trip_start_date = $this->input->post('trip_start_date', TRUE);
      $trip_end_date = $this->input->post('trip_end_date', TRUE);
      $today = date('Y-m-d H:i:s');
      $src_loc_id_1 = $this->input->post('src_loc_id_1', TRUE);
      $trip_source = $this->api->get_location_name_by_id($src_loc_id_1);
      $dest_loc_id_1 = $this->input->post('dest_loc_id_1', TRUE);
      $trip_destination = $this->api->get_location_name_by_id($dest_loc_id_1);
      $trip_duration = $this->input->post('trip_duration', TRUE);

      $seat_type = $this->input->post('seat_type', TRUE);
      if(isset($seat_type) && !empty($seat_type)) { $seat_type_string = implode(',',$this->input->post('seat_type', TRUE)); } else $seat_type_string = 'NULL';
      $seat_type_price = $this->input->post('seat_type_price', TRUE);
      if(isset($seat_type_price) && !empty($seat_type_price)) { $seat_type_price_string = implode(',',$this->input->post('seat_type_price', TRUE)); } else $seat_type_price_string = 'NULL';

      //Insert Location Data
      $insert_data_trip = array(
        "cust_id" => trim($cust_id),
        "trip_source_id" => trim($src_loc_id_1),
        "trip_source" => trim($trip_source),
        "trip_destination_id" => trim($dest_loc_id_1),
        "trip_destination" => trim($trip_destination),
        "bus_id" => trim($bus_id), 
        "trip_depart_time" => trim($trip_depart_time), 
        "seat_type" => $seat_type_string, 
        "seat_type_price" => $seat_type_price_string, 
        "currency_id" => trim($currency_id), 
        "currency_sign" => trim($currency_sign), 
        "trip_availability" => trim($trip_availability), 
        "trip_start_date" => trim($trip_start_date), 
        "trip_end_date" => trim($trip_end_date), 
        "trip_cre_datetime" => $today, 
        "trip_status" => 1, 
        "bus_id" => trim($bus_id), 
        "trip_depart_time" => trim($trip_depart_time), 
        "vehical_type_id" => trim($vehical_type_id), 
        "booking_type" => trim($booking_type), 
        "trip_duration" => trim($trip_duration), 
      );
      
      //echo json_encode($insert_data_trip); die();

      if($trip_id = $this->api->add_trip_master_details($insert_data_trip)) {
        if(isset($_POST['seat_type']) && isset($_POST['seat_type'])) {
          $seat_type = $_POST['seat_type'];
          for ($i=0; $i < sizeof($seat_type); $i++) {
            //Insert Seat Price
            $insert_data_price = array(
              "currency_id" => (int)trim($currency_id),
              "currency_sign" => trim($currency_sign),
              "seat_type" => trim($seat_type[$i]),
              "seat_type_price" => trim($seat_type_price[$i]),
              "trip_id" => (int)trim($trip_id),
              "cust_id" => (int)trim($cust_id),
            );
            $this->api->add_trip_seat_prices($insert_data_price);
          }
        }
        //die();

        $src_point_id_1 = $this->input->post('src_point_id_1', TRUE);
        if(isset($src_point_id_1) && !empty($src_point_id_1)) { $src_point_ids = implode(',',$this->input->post('src_point_id_1', TRUE)); } else $src_point_ids = 'NULL';
        $src_point_time_1 = $this->input->post('src_point_time_1', TRUE);
        if(isset($src_point_time_1) && !empty($src_point_time_1)) { $src_point_times = implode(',',$this->input->post('src_point_time_1', TRUE)); } else $src_point_times = 'NULL';

        $dest_point_id_1 = $this->input->post('dest_point_id_1', TRUE);
        if(isset($dest_point_id_1) && !empty($dest_point_id_1)) { $dest_point_ids = implode(',',$this->input->post('dest_point_id_1', TRUE)); } else $dest_point_ids = 'NULL';
        $dest_point_id_time_1 = $this->input->post('dest_point_id_time_1', TRUE);
        if(isset($dest_point_id_time_1) && !empty($dest_point_id_time_1)) { $dest_point_times = implode(',',$this->input->post('dest_point_id_time_1', TRUE)); } else $dest_point_times = 'NULL';
        //Insert Master data in detail table
        $insert_data_trip_locations = array(
          "trip_id" => trim($trip_id),
          "trip_source_id" => trim($src_loc_id_1),
          "trip_source" => trim($trip_source),
          "src_point_ids" => trim($src_point_ids),
          "src_point_times" => trim($src_point_times),
          "trip_destination_id" => trim($dest_loc_id_1),
          "trip_destination" => trim($trip_destination),
          "dest_point_ids" => trim($dest_point_ids),
          "dest_point_times" => trim($dest_point_times),
          "trip_availability" => trim($trip_availability), 
          "trip_start_date" => trim($trip_start_date), 
          "trip_end_date" => trim($trip_end_date), 
          "trip_cre_datetime" => $today, 
          "cust_id" => trim($cust_id),
          "is_master" => 1,
          "bus_id" => trim($bus_id), 
          "trip_depart_time" => trim($trip_depart_time),
          "vehical_type_id" => trim($vehical_type_id),  
          "booking_type" => trim($booking_type),  
        );
        $this->api->add_trip_location_details($insert_data_trip_locations);

        //die();
        //Insert Points Data
        for ($i=2; $i < 500; $i++) { 
          if(isset($_POST['src_loc_id_'.$i]) && isset($_POST['dest_loc_id_'.$i])) {
            $trip_source_id = $_POST['src_loc_id_'.$i];
            $trip_source = $this->api->get_location_name_by_id($trip_source_id);
            $trip_destination_id = $_POST['dest_loc_id_'.$i];
            $trip_destination = $this->api->get_location_name_by_id($trip_destination_id); 

            $point_counter = $i + 1;

            $src_point_ids = $this->input->post('src_point_id_'.$point_counter, TRUE);
            if(isset($src_point_ids) && !empty($src_point_ids)) { $src_point_ids = implode(',',$this->input->post('src_point_id_'.$point_counter, TRUE)); } else $src_point_ids = 'NULL';
            $src_point_times = $this->input->post('src_point_time_'.$point_counter, TRUE);
            if(isset($src_point_times) && !empty($src_point_times)) { $src_point_times = implode(',',$this->input->post('src_point_time_'.$point_counter, TRUE)); } else $src_point_times = 'NULL';

            $dest_point_ids = $this->input->post('dest_point_id_'.$point_counter, TRUE);
            if(isset($dest_point_ids) && !empty($dest_point_ids)) { $dest_point_ids = implode(',',$this->input->post('dest_point_id_'.$point_counter, TRUE)); } else $dest_point_ids = 'NULL';
            $dest_point_times = $this->input->post('dest_point_time_'.$point_counter, TRUE);
            if(isset($dest_point_times) && !empty($dest_point_times)) { $dest_point_times = implode(',',$this->input->post('dest_point_time_'.$point_counter, TRUE)); } else $dest_point_times = 'NULL';

            $insert_data_trip_locations = array(
              "trip_id" => trim($trip_id),
              "trip_source_id" => trim($trip_source_id),
              "trip_source" => trim($trip_source),
              "src_point_ids" => trim($src_point_ids),
              "src_point_times" => trim($src_point_times),
              "trip_destination_id" => trim($trip_destination_id),
              "trip_destination" => trim($trip_destination),
              "dest_point_ids" => trim($dest_point_ids),
              "dest_point_times" => trim($dest_point_times),
              "trip_availability" => trim($trip_availability), 
              "trip_start_date" => trim($trip_start_date), 
              "trip_end_date" => trim($trip_end_date), 
              "trip_cre_datetime" => $today, 
              "cust_id" => trim($cust_id),
              "is_master" => 0,
              "bus_id" => trim($bus_id),
              "trip_depart_time" => trim($trip_depart_time),
              "vehical_type_id" => trim($vehical_type_id), 
              "booking_type" => trim($booking_type),  
            );
            $this->api->add_trip_location_details($insert_data_trip_locations);
          }
        }
        $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Error! while adding trip details!')); }
      redirect('user-panel-bus/bus-trip-master-add');
    }
    public function bus_trip_master_edit()
    {
      if( !is_null($this->input->post('trip_id')) ) { $trip_id = (int) $this->input->post('trip_id'); }
      else if(!is_null($this->uri->segment(3))) { $trip_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/bus-trip-master-list'); }
      $trip_details = $this->api->get_trip_master_details($trip_id);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $this->cust_id);
      $buses = $this->api->get_vehicle_by_type_id($vehical_type_id, $this->cust_id);
      //get seat price details
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_id);
      //get multiple source destinatin details with pickup drop timings
      $src_dest_details = $this->api->get_trip_location_master($trip_id);
      $this->load->view('user_panel/bus_trip_master_edit_view', compact('locations','buses','trip_details','seat_type_price','src_dest_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_master_edit_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $trip_id = $this->input->post('trip_id', TRUE); $trip_id = (int)$trip_id;
      $bus_id = $this->input->post('bus_id', TRUE); $bus_id = (int)$bus_id;
      $booking_type = $this->input->post('booking_type', TRUE);

      $price_id = $this->input->post('price_id', TRUE);
      if(isset($price_id) && !empty($price_id)) { $price_id = implode(',',$this->input->post('price_id', TRUE)); } else $price_id = 'NULL';
      
      $seat_type_price = $this->input->post('seat_type_price', TRUE);
      if(isset($seat_type_price) && !empty($seat_type_price)) { $seat_type_price = implode(',',$this->input->post('seat_type_price', TRUE)); } else $seat_type_price = 'NULL';

      $avail = $this->input->post('available', TRUE);
      if(isset($avail) && !empty($avail)) { $trip_availability = implode(',',$this->input->post('available', TRUE)); } else $trip_availability = 'NULL';
      $trip_depart_time = $this->input->post('trip_depart_time', TRUE);
      $trip_start_date = $this->input->post('trip_start_date', TRUE);
      $trip_end_date = $this->input->post('trip_end_date', TRUE); 

      //Update Trip Data
      $update_data_trip = array(
        "bus_id" => trim($bus_id), 
        "trip_depart_time" => trim($trip_depart_time), 
        "trip_availability" => trim($trip_availability), 
        "trip_start_date" => trim($trip_start_date), 
        "trip_end_date" => trim($trip_end_date), 
        "seat_type_price" => trim($seat_type_price), 
        "booking_type" => trim($booking_type), 
      );
      
      if($this->api->update_trip_master_details($update_data_trip, $trip_id)) {
        //Update Seat pricing table
        $seat_type_price = explode(',', $seat_type_price);
        $price_id = explode(',', $price_id);
        for ($i=0; $i < sizeof($price_id); $i++) { 
          $update_seat_price = array(
            "seat_type_price" => trim($seat_type_price[$i]),
          );
          $this->api->update_seat_type_prices($update_seat_price, $price_id[$i]);
        }
        //Update pickup drop location timings
        $sdd_id = $this->input->post('sdd_id', TRUE);
        if(isset($sdd_id) && !empty($sdd_id)) { $sdd_id = implode(',',$this->input->post('sdd_id', TRUE)); } else $sdd_id = 'NULL';
        $sdd_id = explode(',', $sdd_id);

        if(is_array($sdd_id)) {
          for ($i=0; $i < sizeof($sdd_id); $i++) { 
            //source point times
            $src_point_times = $this->input->post('src_point_times_'.$sdd_id[$i], TRUE);
            if(isset($src_point_times) && !empty($src_point_times)) { 
              $src_point_times = implode(',',$this->input->post('src_point_times_'.$sdd_id[$i], TRUE)); 
              $update_times = array(
                "src_point_times" => trim($src_point_times),
              );
              $this->api->update_pickup_drop_point_timings($update_times, $sdd_id[$i]);
            }
            //Destination point times
            $dest_point_times = $this->input->post('dest_point_times_'.$sdd_id[$i], TRUE);
            if(isset($dest_point_times) && !empty($dest_point_times)) { 
              $dest_point_times = implode(',',$this->input->post('dest_point_times_'.$sdd_id[$i], TRUE));
              $update_times = array(
                "dest_point_times" => trim($dest_point_times),
              );
              $this->api->update_pickup_drop_point_timings($update_times, $sdd_id[$i]); 
            }
          }
        }
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Error! while updating trip details!')); }
      redirect('user-panel-bus/bus-trip-master-edit/'.$trip_id);
    }
    public function bus_trip_master_delete()
    {
      $trip_id = $this->input->post('trip_id');
      if( $this->api->delete_trip_master_details($trip_id)) { echo "success"; 
      } else { echo "failed"; }
    }
    //Manage Locations
    public function bus_trip_location_list()
    {
      if( !is_null($this->input->post('trip_id')) ) { $trip_id = (int) $this->input->post('trip_id'); }
      else if(!is_null($this->uri->segment(3))) { $trip_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/bus-trip-master-list'); }
      $trip_master_details = $this->api->get_trip_master_details($trip_id);
      //echo json_encode($vehical_type_id); die();
      $vehical_type_id = $trip_master_details[0]['vehical_type_id'];
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $this->cust_id);
      $locations_list = $this->api->get_trip_location_master($trip_id);
      //echo $trip_id; die();
      $this->load->view('user_panel/bus_locations_list_view', compact('locations_list','locations','trip_id'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_location_add_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $trip_id = $this->input->post('trip_id', TRUE); $trip_id = (int)$trip_id;
      $src_loc_id = $this->input->post('src_loc_id', TRUE); $src_loc_id = (int)$src_loc_id;
      $trip_source = $this->api->get_location_name_by_id($src_loc_id);
      $dest_loc_id = $this->input->post('dest_loc_id', TRUE); $dest_loc_id = (int)$dest_loc_id;
      $trip_destination = $this->api->get_location_name_by_id($dest_loc_id);
      $trip_details = $this->api->get_trip_master_details($trip_id);
      //echo json_encode($trip_details);  die();
      $trip_availability = $trip_details[0]['trip_availability'];
      $trip_start_date = $trip_details[0]['trip_start_date'];
      $trip_end_date = $trip_details[0]['trip_end_date'];
      $bus_id = $trip_details[0]['bus_id'];
      $trip_depart_time = $trip_details[0]['trip_depart_time'];
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $booking_type = $trip_details[0]['booking_type'];
      $today = date('Y-m-d H:i:s');

      $src_point_ids = $this->input->post('src_point_ids', TRUE);
      if(isset($src_point_ids) && !empty($src_point_ids)) { $src_point_ids = implode(',',$this->input->post('src_point_ids', TRUE)); } else $src_point_ids = 'NULL';
      $src_point_times = $this->input->post('src_point_times', TRUE);
      if(isset($src_point_times) && !empty($src_point_times)) { $src_point_times = implode(',',$this->input->post('src_point_times', TRUE)); } else $src_point_times = 'NULL';

      $dest_point_ids = $this->input->post('dest_point_ids', TRUE);
      if(isset($dest_point_ids) && !empty($dest_point_ids)) { $dest_point_ids = implode(',',$this->input->post('dest_point_ids', TRUE)); } else $dest_point_ids = 'NULL';
      $dest_point_times = $this->input->post('dest_point_times', TRUE);
      if(isset($dest_point_times) && !empty($dest_point_times)) { $dest_point_times = implode(',',$this->input->post('dest_point_times', TRUE)); } else $dest_point_times = 'NULL';

      //Insert Location Data
      $insert_data_trip_locations = array(
        "trip_id" => trim($trip_id),
        "trip_source_id" => trim($src_loc_id),
        "trip_source" => trim($trip_source),
        "src_point_ids" => trim($src_point_ids),
        "src_point_times" => trim($src_point_times),
        "trip_destination_id" => trim($dest_loc_id),
        "trip_destination" => trim($trip_destination),
        "dest_point_ids" => trim($dest_point_ids),
        "dest_point_times" => trim($dest_point_times),
        "trip_availability" => trim($trip_availability), 
        "trip_start_date" => trim($trip_start_date), 
        "trip_end_date" => trim($trip_end_date), 
        "trip_cre_datetime" => $today, 
        "cust_id" => trim($cust_id),
        "is_master" => 0,
        "bus_id" => trim($bus_id),
        "trip_depart_time" => trim($trip_depart_time),
        "vehical_type_id" => trim($vehical_type_id), 
        "booking_type" => trim($booking_type),  
      );
      
      //echo json_encode($insert_data_trip_locations); die();
      if($this->api->add_trip_location_details($insert_data_trip_locations)) {
        $this->session->set_flashdata('success',$this->lang->line('added_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Error! while adding location details!')); }
      redirect('user-panel-bus/bus-trip-location-list/'.$trip_id);
    }
    public function bus_trip_location_delete()
    {
      $sdd_id = $this->input->post('sdd_id');
      if( $this->api->delete_trip_location_details($sdd_id)) { echo "success"; 
      } else { echo "failed"; }
    }
    public function get_location_pickup_point()
    {
      $cust_id = (int)$this->cust_id;
      $loc_id = $this->input->post('id');
      echo json_encode($this->api->get_pickup_drop_points_list($cust_id, $loc_id));
    }
    public function get_locations_pickup_points()
    {
      $cust_id = (int)$this->cust_id;
      $loc_id = $this->input->post('id');
      $vehical_type_id = $this->input->post('type_id');
      echo json_encode($this->api->get_locations_pickup_points($cust_id, $loc_id, 0, 0, $vehical_type_id));
    }
    public function get_locations_drop_points()
    {
      $cust_id = (int)$this->cust_id;
      $loc_id = $this->input->post('id');
      $vehical_type_id = $this->input->post('type_id');
      echo json_encode($this->api->get_locations_drop_points($cust_id, $loc_id, 0, 0, $vehical_type_id));
    }
    public function bus_trip_master_disable()
    {
      if( !is_null($this->input->post('trip_id')) ) { $trip_id = (int) $this->input->post('trip_id'); }
      else if(!is_null($this->uri->segment(3))) { $trip_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/bus-trip-master-list'); }
      $trip_details = $this->api->get_trip_master_details($trip_id);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $this->cust_id);
      $buses = $this->api->get_vehicle_by_type_id($vehical_type_id, $this->cust_id);
      //get seat price details
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_id);
      //get multiple source destinatin details with pickup drop timings
      $src_dest_details = $this->api->get_trip_location_master($trip_id);
      //Get trip bookings list
      $booking_details = $this->api->get_trip_upcoming_booking_list($trip_id);
      $query = $this->db->last_query();
      $this->load->view('user_panel/bus_trip_master_disable_view', compact('locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','query'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_master_disable_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $trip_id = $this->input->post('trip_id', TRUE); $trip_id = (int)$trip_id;
      $trip_end_date = $this->input->post('trip_end_date', TRUE); 

      //Update Trip Data
      $update_data_trip = array(
        "trip_end_date" => trim($trip_end_date), 
      );
      
      if($this->api->update_trip_master_details($update_data_trip, $trip_id)) {
        //Cancel bookings if in trip
        $this->api->cancel_trip_by_end_date($trip_end_date, $trip_id);
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Error! while updating trip details!')); }
      redirect(base_url('user-panel-bus/bus-trip-master-disable/'.$trip_id));
    }
    public function bus_trip_master_enable()
    {
      if( !is_null($this->input->post('trip_id')) ) { $trip_id = (int) $this->input->post('trip_id'); }
      else if(!is_null($this->uri->segment(3))) { $trip_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/bus-trip-master-list'); }
      $trip_details = $this->api->get_trip_master_details($trip_id);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $this->cust_id);
      $buses = $this->api->get_vehicle_by_type_id($vehical_type_id, $this->cust_id);
      //get seat price details
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_id);
      //get multiple source destinatin details with pickup drop timings
      $src_dest_details = $this->api->get_trip_location_master($trip_id);
      $this->load->view('user_panel/bus_trip_master_enable_view', compact('locations','buses','trip_details','seat_type_price','src_dest_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_master_enable_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $trip_id = $this->input->post('trip_id', TRUE); $trip_id = (int)$trip_id;
      $trip_start_date = $this->input->post('trip_start_date', TRUE); 
      $trip_end_date = $this->input->post('trip_end_date', TRUE); 

      //Update Trip Data
      $update_data_trip = array(
        "trip_start_date" => trim($trip_start_date), 
        "trip_end_date" => trim($trip_end_date), 
      );
      
      if($this->api->update_trip_master_details($update_data_trip, $trip_id) &&  $this->api->update_trip_source_destination_details($update_data_trip, $trip_id)) {
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Error! while updating trip details!')); }
      redirect(base_url('user-panel-bus/bus-trip-master-enable/'.$trip_id));
    }
  //End Trip Master----------------------------------------
    
  //Start Trip Special Rate--------------------------------
    public function bus_trip_special_rate_list()
    {
      $trip_master = $this->api->get_operator_trip_master($this->cust_id);
      $this->load->view('user_panel/bus_trip_special_rate_list_view', compact('trip_master'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_special_rate_edit()
    {
      if( !is_null($this->input->post('trip_id')) ) { $trip_id = (int) $this->input->post('trip_id'); }
      else if(!is_null($this->uri->segment(3))) { $trip_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/bus-trip-master-list'); }
      $trip_details = $this->api->get_trip_master_details($trip_id);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $buses = $this->api->get_vehicle_by_type_id($vehical_type_id, $this->cust_id);
      //get seat price details
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_id);
      $this->load->view('user_panel/bus_trip_special_rate_edit_view', compact('buses','trip_details','seat_type_price'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_special_rate_edit_details()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $trip_id = $this->input->post('trip_id', TRUE); $trip_id = (int)$trip_id;
      $special_rate_start_date = $this->input->post('special_rate_start_date', TRUE);
      $special_rate_end_date = $this->input->post('special_rate_end_date', TRUE);

      $price_id = $this->input->post('price_id', TRUE);
      if(isset($price_id) && !empty($price_id)) { $price_id = implode(',',$this->input->post('price_id', TRUE)); } else $price_id = 'NULL';
      
      $special_price = $this->input->post('special_price', TRUE);
      if(isset($special_price) && !empty($special_price)) { $special_price = implode(',',$this->input->post('special_price', TRUE)); } else $special_price = 'NULL';

      //Update Trip Data
      $update_data_trip = array(
        "special_rate_start_date" => trim($special_rate_start_date), 
        "special_rate_end_date" => trim($special_rate_end_date), 
        "special_price" => trim($special_price), 
      );
      
      if($this->api->update_trip_master_details($update_data_trip, $trip_id)) {
        //Update Seat pricing table
        $special_price = explode(',', $special_price);
        $price_id = explode(',', $price_id);
        for ($i=0; $i < sizeof($price_id); $i++) { 
          $update_seat_price = array(
            "special_price" => trim($special_price[$i]),
            "special_rate_start_date" => trim($special_rate_start_date),
            "special_rate_end_date" => trim($special_rate_end_date),
          );
          $this->api->update_seat_type_prices($update_seat_price, $price_id[$i]);
        }
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error',$this->lang->line('Error! while updating details!')); }
      redirect('user-panel-bus/bus-trip-special-rate-edit/'.$trip_id);
    }
  //End Trip Special Rate----------------------------------
  
  //Start Ticket Booking by Seller-------------------------
    public function bus_trip_ticket_booking_seller()
    {
      //echo json_encode($_POST); die();
      //echo json_encode($_SESSION); die();
      if(isset($_SESSION['cat_id']) && $_SESSION['cat_id']==281) {
        $this->session->unset_userdata('cat_id');
        $this->session->unset_userdata('trip_source');
        $this->session->unset_userdata('trip_destination');
        $this->session->unset_userdata('journey_date');
        $this->session->unset_userdata('return_date');
        $this->session->unset_userdata('no_of_seat');
        $this->session->unset_userdata('ownward_trip');
        $this->session->unset_userdata('return_trip');
      }
      
      $operator = $this->api->get_bus_operator_profile($this->cust_id);
      if($operator == false) {
          redirect(base_url('user-panel-bus/bus-operator-profile'),'refresh');
      } else {
        $trip_sources = $this->api->get_multi_trip_master_source_list($this->cust_id);
        $trip_destinations = $this->api->get_multi_trip_master_destination_list($this->cust_id);
        $filter = 'basic';
        $this->load->view('user_panel/bus_trip_search_operator_view', compact('trip_sources','trip_destinations','filter'));
        $this->load->view('user_panel/footer');
      }
    }
    public function old_bus_trip_search_result_seller()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $trip_source = $this->input->post('trip_source');
      $trip_destination = $this->input->post('trip_destination');
      $jdate = $journey_date = $this->input->post('journey_date');
      $rdate = $return_date = $this->input->post('return_date');
      $journey_date=date_format($journey_date,"Y-m-d");
      $return_date=date_format($return_date,"Y-m-d");

      //Update Trip Data
      $search_data = array(
        "trip_source" => trim($trip_source), 
        "trip_destination" => trim($trip_destination), 
        "journey_date" => trim($journey_date), 
        "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
        "return_date" => trim($return_date), 
        "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
        "cust_id" => (int)trim($cust_id), 
      );
      //echo json_encode($search_data); die();
      $trip_list = $this->api->get_trip_source_destination_search_list($search_data);
      //echo $this->db->last_query(); 
      //echo json_encode($trip_list); die();
      $filter = 'basic';
      $trip_sources = $this->api->get_multi_trip_master_source_list($this->cust_id);
      $trip_destinations = $this->api->get_multi_trip_master_destination_list($this->cust_id);
      $journey_date = $jdate;
      $return_date = $rdate;
      $this->load->view('user_panel/bus_trip_search_result_operator_view', compact('trip_list','trip_sources','trip_destinations','filter','trip_source','trip_destination','journey_date','return_date'));
      $this->load->view('user_panel/footer');
    }
    public function old__bus_trip_search_result_seller()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $trip_source = $this->input->post('trip_source');
      $trip_destination = $this->input->post('trip_destination');
      $journey_date = $this->input->post('journey_date');
      $return_date = $this->input->post('return_date');
      $depart_from = $this->input->post('depart_from');
      $bus_seat_type = $this->input->post('bus_seat_type');
      $bus_ac = $this->input->post('bus_ac');
      $ticket_to = $this->input->post('ticket_to');
      $ticket_from = $this->input->post('ticket_from');
      $operator = $this->input->post('operator');
      $bus_amen = $this->input->post('bus_amenities');

      $trip_list_return = array();
      $trip_list = array();

      //$pickup_point = $this->input->post('pickup_point');
      //$drop_point = $this->input->post('drop_point');
      $search_data = array(
        "trip_source" => trim($trip_source), 
        "trip_destination" => trim($trip_destination), 
        "journey_date" => trim($journey_date), 
        "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
        "return_date" => trim($return_date),
        "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
        "cust_id" => (int)trim($cust_id), 
      );

      //Return Trip
      if($return_date!=""){
        $search_data_return = array(
        "trip_destination" => trim($trip_source), 
        "trip_source" => trim($trip_destination), 
        // "journey_date" => trim($journey_date), 
        // "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
        "return_date" => trim($return_date),
        "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
        "cust_id" => (int)trim($cust_id), 
        );
        $trip_list_return = $this->api->get_trip_source_destination_search_list_return($search_data_return);
      }

      //echo json_encode($search_data); die();
      $trip_list = $this->api->get_trip_source_destination_search_list($search_data);
      $filter = 'basic';
      $trip_sources = $this->api->get_multi_trip_master_source_list($this->cust_id);
      $trip_destinations = $this->api->get_multi_trip_master_destination_list($this->cust_id);
      function zaid_date_time_validator($date, $format = 'Y-m-d H:i:s')
      { $d = DateTime::createFromFormat($format, $date);return $d && $d->format($format) == $date; }

      for($i=0; $i<sizeof($trip_list); $i++) { 
        $t=0; $seat_ac_amen=0; $pr=0;
        if(zaid_date_time_validator($depart_from , 'H:i')) {
          $depart=$this->api->get_departure_time_filter($trip_list[$i]['trip_id'],$depart_from);
          if($depart==true){$t=0;}if($depart==false){$t++;}
        }
        if($bus_seat_type=="SEATER" || $bus_seat_type=="SLEEPER" || $bus_seat_type=="SEMI-SLEEPER" || $bus_ac=="AC" || $bus_ac=="NON AC" || $bus_amen!="") {
          $b= $this->api->get_seat_ac_amen_filter($trip_list[$i]['bus_id'],$bus_seat_type,$bus_ac,$bus_amen);
          if($b==true){ $seat_ac_amen=0;}if($b==false){$seat_ac_amen++; }
        }
        if($ticket_to!="" || $ticket_from!="") {
          $price = $this->api->get_trip_master_search_price_filter($trip_list[$i]['trip_id'],$ticket_to,$ticket_from);
          if($price==true){ $pr=0; }
          if($price==false){ $pr++; } 
        }
        if($t>0 || $seat_ac_amen>0 || $pr>0) {
          array_push($trip_list[$i],"delete");
        } else { array_push($trip_list[$i],"safe"); }     
      }

      //Return Trip Filter
      for($i=0; $i<sizeof($trip_list_return); $i++) { 
        $t=0; $seat_ac_amen=0; $pr=0;
        if(zaid_date_time_validator($depart_from , 'H:i')) {
          $depart=$this->api->get_departure_time_filter($trip_list_return[$i]['trip_id'],$depart_from);
          if($depart==true){$t=0;}if($depart==false){$t++;}
        }
        if($bus_seat_type=="SEATER" || $bus_seat_type=="SLEEPER" || $bus_seat_type=="SEMI-SLEEPER" || $bus_ac=="AC" || $bus_ac=="NON AC" || $bus_amen!="") {
          $b= $this->api->get_seat_ac_amen_filter($trip_list_return[$i]['bus_id'],$bus_seat_type,$bus_ac,$bus_amen);
          if($b==true){ $seat_ac_amen=0;}if($b==false){$seat_ac_amen++; }
        }
        if($ticket_to!="" || $ticket_from!="") {
          $price = $this->api->get_trip_master_search_price_filter($trip_list_return[$i]['trip_id'],$ticket_to,$ticket_from);
          if($price==true){ $pr=0; }
          if($price==false){ $pr++; } 
        }
        if($t>0 || $seat_ac_amen>0 || $pr>0) {
          array_push($trip_list_return[$i],"delete");
        } else { array_push($trip_list_return[$i],"safe"); }     
      }

      $this->load->view('user_panel/bus_trip_search_result_operator_view', compact('trip_list_return','trip_list','trip_sources','trip_destinations','depart','filter','trip_source','trip_destination','journey_date','return_date','depart_from','bus_seat_type','bus_ac','ticket_to','ticket_from','operator','bus_amen'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_search_result_seller()
    {
      $cust_id = $this->cust_id;
      $cust = $this->cust;
      //echo json_encode($_SESSION); die();

      if(isset($_SESSION['cat_id']) && $_SESSION['cat_id']==281) {
        $trip_source = $_SESSION['trip_source'];
        $trip_destination = $_SESSION['trip_destination'];
        $no_of_seat = $_SESSION['no_of_seat'];
        if(isset($_SESSION['journey_date'])) {
          $date_array = explode('-', $_SESSION['journey_date']);
          $journey_date = $date_array[1].'/'.$date_array[2].'/'.$date_array[0];
        }
        if(isset($_SESSION['return_date']) && $_SESSION['return_date'] != '') {
          $date_array = explode('-', $_SESSION['return_date']);
          $return_date = $date_array[1].'/'.$date_array[2].'/'.$date_array[0];
        } else { $return_date = ''; }

        $this->session->unset_userdata('cat_id');
        $this->session->unset_userdata('trip_source');
        $this->session->unset_userdata('trip_destination');
        $this->session->unset_userdata('journey_date');
        $this->session->unset_userdata('return_date');
        $this->session->unset_userdata('no_of_seat');
      } else {
        $trip_source = $this->input->post('trip_source');
        $trip_destination = $this->input->post('trip_destination');
        $jdate = $journey_date = $this->input->post('journey_date');
        $rdate = $return_date = $this->input->post('return_date');
        $no_of_seat = $this->input->post('no_of_seat');
        $journey_date=date_create($journey_date);
        $journey_date=date_format($journey_date,"Y-m-d");
      }
      //echo json_encode($journey_date); die();
      //Update Trip Data
      if($no_of_seat > 40) {
        $this->session->set_flashdata('error', $this->lang->line('The maximum number of seat is 40'));
        if($_POST['place']=="dashboard"){
          redirect('user-panel-bus/dashboard-bus');
        } else { redirect('user-panel-bus/bus-trip-ticket-booking-seller'); }
      } else {
        $search_data = array(
          "trip_source" => trim($trip_source), 
          "trip_destination" => trim($trip_destination), 
          "journey_date" => trim($journey_date), 
          "vehical_type_id" =>"",
          "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
          "return_date" => trim($return_date),
          "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
          "cust_id" => (int)trim($cust_id), 
          "opr_id" => (int)trim($cust_id), 
        );
        //echo json_encode($search_data); die();
        $trip_list = $this->api->get_trip_source_destination_search_list($search_data);
        //echo $this->db->last_query(); 
        //echo json_encode($trip_list); die();
        if($return_date!="") {
          $return_date=date_create($return_date);
        $return_date=date_format($return_date,"Y-m-d");
          $search_data_return = array(
          "trip_destination" => trim($trip_source), 
          "trip_source" => trim($trip_destination), 
          "vehical_type_id" =>"", 
          // "journey_date" => trim($journey_date), 
          // "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
          "return_date" => trim($return_date),
          "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
          "cust_id" => (int)trim($cust_id), 
          "opr_id" => (int)trim($cust_id), 
          );
          $trip_list_return = $this->api->get_trip_source_destination_search_list_return($search_data_return);
        }
        $filter = 'basic';
        $trip_sources = $this->api->get_multi_trip_master_source_list($this->cust_id);
        $trip_destinations = $this->api->get_multi_trip_master_destination_list($this->cust_id);
     
        //echo json_encode($cancelled_trips); die();
        for($i=0; $i<sizeof($trip_list); $i++) {
          $unique_id = str_replace("/","",$journey_date)."_".$trip_list[$i]['trip_id'];
          if($cancelled_trips = $this->api->get_cancelled_seller_trips())
          {
            foreach ($cancelled_trips as $ct){
            if($ct['unique_id']==$unique_id){array_push($trip_list[$i],"delete");}
            else{array_push($trip_list[$i],"safe");}
            }  
          }else{
            array_push($trip_list[$i],"safe");
          }
        }
        if(isset($trip_list_return)) {
          for($i=0; $i<sizeof($trip_list_return); $i++) {
            $unique_id = str_replace("/","",$return_date)."_".$trip_list_return[$i]['trip_id'];
            if($cancelled_trips = $this->api->get_cancelled_seller_trips()) {
              foreach ($cancelled_trips as $ct) {
                if($ct['unique_id']==$unique_id){ array_push($trip_list_return[$i],"delete"); }
                else { array_push($trip_list_return[$i],"safe"); }
              }  
            } else { array_push($trip_list_return[$i],"safe"); }
          }//if no_of_seat
        }
        //echo json_encode($trip_list); die();
        $journey_date = $jdate;
        $return_date = $rdate;
        $this->load->view('user_panel/bus_trip_search_result_operator_view', compact('trip_list','trip_list_return','filter','trip_source','trip_destination','trip_sources','trip_destinations','journey_date','return_date','no_of_seat', 'cust'));
        $this->load->view('user_panel/footer');
      }
    }
    public function bus_trip_search_result_seller_filter()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $trip_source = $this->input->post('trip_source_filter');
      $trip_destination = $this->input->post('trip_destination_filter');
      $journey_date = $this->input->post('journey_date_filter');
      $return_date = $this->input->post('return_date_filter');
      $depart_from = $this->input->post('depart_from');
      $bus_seat_type = $this->input->post('bus_seat_type');
      $bus_ac = $this->input->post('bus_ac');
      $ticket_to = $this->input->post('ticket_to');
      $ticket_from = $this->input->post('ticket_from');
      $operator = $this->input->post('operator');
      $bus_amen = $this->input->post('bus_amenities');
      $vehical_type_id = $this->input->post('vehical_type_id');
      $no_of_seat = $this->input->post('no_of_seat');
      //$pickup_point = $this->input->post('pickup_point');
      //$drop_point = $this->input->post('drop_point');
      $search_data = array(
        "trip_source" => trim($trip_source), 
        "trip_destination" => trim($trip_destination), 
        "journey_date" => trim($journey_date), 
        "vehical_type_id" => trim($vehical_type_id),
        "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
        "return_date" => trim($return_date),
        "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
        "cust_id" => (int)trim($cust_id),
        "opr_id" => (int)trim($cust_id), 
      );
      //echo json_encode($search_data); die();
      $trip_list = $this->api->get_trip_source_destination_search_list($search_data);
      //Return Trip
      if($return_date!=""){
        $search_data_return = array(
        "trip_destination" => trim($trip_source), 
        "trip_source" => trim($trip_destination), 
        "vehical_type_id" => trim($vehical_type_id), 
        // "journey_date" => trim($journey_date), 
        // "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
        "return_date" => trim($return_date),
        "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
        "cust_id" => (int)trim($cust_id), 
        "opr_id" => (int)trim($operator), 
        );
        $trip_list_return = $this->api->get_trip_source_destination_search_list_return($search_data_return);
      }
      
      $filter = 'basic';
      $trip_sources = $this->api->get_multi_trip_master_source_list($this->cust_id);
      $trip_destinations = $this->api->get_multi_trip_master_destination_list($this->cust_id);
      function zaid_date_time_validator($date, $format = 'Y-m-d H:i:s')
      { $d = DateTime::createFromFormat($format, $date);return $d && $d->format($format) == $date; }
      for($i=0; $i<sizeof($trip_list); $i++) {
        $unique_id = str_replace("/","",$journey_date)."_".$trip_list[$i]['trip_id'];
        $t=0; $seat_ac_amen=0; $pr=0;
        if(zaid_date_time_validator($depart_from , 'H:i')) {
          $depart=$this->api->get_departure_time_filter($trip_list[$i]['trip_id'],$depart_from);
          if($depart==true){$t=0;}if($depart==false){$t++;}
        }
        if($bus_seat_type=="SEATER" || $bus_seat_type=="SLEEPER" || $bus_seat_type=="SEMI-SLEEPER" || $bus_ac=="AC" || $bus_ac=="NON AC" || $bus_amen!="") {
          $b= $this->api->get_seat_ac_amen_filter($trip_list[$i]['bus_id'],$bus_seat_type,$bus_ac,$bus_amen);
          if($b==true){ $seat_ac_amen=0;}if($b==false){$seat_ac_amen++; }
        }
        if($ticket_to!="" || $ticket_from!="") {
          $price = $this->api->get_trip_master_search_price_filter($trip_list[$i]['trip_id'],$ticket_to,$ticket_from);
          if($price==true){ $pr=0; }
          if($price==false){ $pr++; } 
        }
        // if($t>0 || $seat_ac_amen>0 || $pr>0) {
        //   array_push($trip_list[$i],"delete");
        // } else { array_push($trip_list[$i],"safe"); }
        if($cancelled_trips = $this->api->get_cancelled_seller_trips()){
          foreach ($cancelled_trips as $ct){
          if($ct['unique_id']==$unique_id || $t>0 || $seat_ac_amen>0 || $pr>0 ){array_push($trip_list[$i],"delete");}
            else{array_push($trip_list[$i],"safe");}
          }  
        }else
        {
          if($t>0 || $seat_ac_amen>0 || $pr>0) {
           array_push($trip_list[$i],"delete");
          } else { array_push($trip_list[$i],"safe"); }
        }
             
      }

      //Return Trip Filter
      if(isset($trip_list_return)) {
        for($i=0; $i<sizeof($trip_list_return); $i++) {
          $unique_id = str_replace("/","",$return_date)."_".$trip_list_return[$i]['trip_id'];
          $t=0; $seat_ac_amen=0; $pr=0;
          if(zaid_date_time_validator($depart_from , 'H:i')) {
            $depart=$this->api->get_departure_time_filter($trip_list_return[$i]['trip_id'],$depart_from);
            if($depart==true){$t=0;}if($depart==false){$t++;}
          }
          if($bus_seat_type=="SEATER" || $bus_seat_type=="SLEEPER" || $bus_seat_type=="SEMI-SLEEPER" || $bus_ac=="AC" || $bus_ac=="NON AC" || $bus_amen!="") {
            $b= $this->api->get_seat_ac_amen_filter($trip_list_return[$i]['bus_id'],$bus_seat_type,$bus_ac,$bus_amen);
            if($b==true){ $seat_ac_amen=0;}if($b==false){$seat_ac_amen++; }
          }
          if($ticket_to!="" || $ticket_from!="") {
            $price = $this->api->get_trip_master_search_price_filter($trip_list_return[$i]['trip_id'],$ticket_to,$ticket_from);
            if($price==true){ $pr=0; }
            if($price==false){ $pr++; } 
          }
          // if($t>0 || $seat_ac_amen>0 || $pr>0) {
          //   array_push($trip_list_return[$i],"delete");
          // } else { array_push($trip_list_return[$i],"safe"); }
          if($cancelled_trips = $this->api->get_cancelled_seller_trips()){
            foreach ($cancelled_trips as $ct){
              if($ct['unique_id']==$unique_id || $t>0 || $seat_ac_amen>0 || $pr>0 ){array_push($trip_list_return[$i],"delete");}
              else{array_push($trip_list_return[$i],"safe");}
            }
          }else{
            if($t>0 || $seat_ac_amen>0 || $pr>0) {
             array_push($trip_list_return[$i],"delete");
            } else { array_push($trip_list_return[$i],"safe"); }
          }              
        }
      }
      $this->load->view('user_panel/bus_trip_search_result_operator_view', compact('trip_list_return','trip_list','trip_sources','trip_destinations','depart','filter','trip_source','trip_destination','journey_date','return_date','depart_from','bus_seat_type','bus_ac','ticket_to','ticket_from','operator','bus_amen','vehical_type_id','no_of_seat'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_details_seller()
    {
      if(!is_null($this->input->post('trip_id'))) { $trip_id = (int) $this->input->post('trip_id');
      } else if(!is_null($this->uri->segment(3))) { $trip_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/bus-trip-master-list'); }
      $trip_details = $this->api->get_trip_master_details($trip_id);
      $operator_details = $this->api->get_bus_operator_profile($this->cust_id);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $this->cust_id);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_id);
      $src_dest_details = $this->api->get_trip_location_master($trip_id);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_id);
      $this->load->view('user_panel/bus_trip_seller_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_booking_pickup_drop_details()
    {
      //echo json_encode($_POST); die();
      $countries = $this->user->get_countries();
      $address_details = $this->api->get_consumers_addressbook($this->cust_id);
      $this->load->view('user_panel/bus_trip_seat_booking', compact('$_POST','address_details','countries'));
      $this->load->view('user_panel/footer');
    }
    public function get_seat()
    {
      if($_POST['seat_type']!="") {
        $sdd_id = $_POST['sdd_id'];
        $seat_type = $_POST['seat_type'];
        $journey_date = $_POST['journey_date'];
        $trip_details = $this->api->get_source_destination_of_trip_by_sdd_id($sdd_id);
        $booked_seat_count= $this->api->get_trip_booked_seat_count_details($trip_details[0]['trip_id'],$seat_type,$journey_date);
        $trip_master_details = $this->api->get_trip_master_details($trip_details[0]['trip_id']);

        $total_seats= $this->api->get_total_seats($trip_details[0]['bus_id'],$seat_type);
        $seat_price= $this->api->get_seats_price($trip_details[0]['trip_id'],$seat_type);
        $price_currency = explode(',',$seat_price);
        
        if($journey_date >= $trip_master_details[0]['special_rate_start_date'] && $journey_date <= $trip_master_details[0]['special_rate_end_date']) {
          $seat_type_price = explode(',', $trip_master_details[0]['special_price']);
          $s_type = explode(',', $trip_master_details[0]['seat_type']);
          $s_p_array = array();
          for($i=0; $i<sizeof($seat_type_price); $i++) {
             //$s_p_array[$seat_type_price[$i]] = $s_type[$i];
             $s_p_array[$s_type[$i]] = $seat_type_price[$i];
          }
          $result=array_flip($s_p_array);
          $p = array_search($seat_type , $result);
          $a =($total_seats-$booked_seat_count).",".$p.",".$price_currency[1].",".$price_currency[2];
        } else {
          $a = ($total_seats-$booked_seat_count).",".$price_currency[0].",".$price_currency[1].",".$price_currency[2];  
        }
        echo $a;
      } else {
        echo "chooseright";
      }
    }
    public function address_book_add_details_from_booking_page()
    {
      $cust = $this->cust;
      $firstname = $this->input->post("firstname");
      $lastname = $this->input->post("lastname");
      $email_id = $this->input->post("email_id");
      $gender = $this->input->post("gender");
      $country_id = $this->input->post("country_id");
      $mobile = $this->input->post("mobile");
      $dob = $this->input->post("age");
      $from = new DateTime($dob);
      $to   = new DateTime('today');
      $age  = $from->diff($to)->y;
      $today = date('Y-m-d H:i:s');
      //echo $gender;
      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'firstname' => trim($firstname), 
        'lastname' => trim($lastname), 
        'email_id' => (trim($email_id) != "") ? trim($email_id) : "NULL", 
        'gender' => trim($gender), 
        'country_id' => trim($country_id), 
        'mobile' => trim($mobile), 
        'age' => (int)($age), 
        'dob' => ($dob), 
        'cre_dateteime' => $today,
      );
      //echo json_encode($add_data); die();
      if( $id = $this->api->register_new_address($add_data)) {
        echo $id."@".$age; 
      } else { echo "failed";   }
    }
    public function get_bus_address_book()
    {
      $no_of_passengers = array();
      $no_of_passengers = explode(',', rtrim($_POST['no_of_passengers'], ','));
      // echo  json_encode($this->api->get_address_from_book($no_of_passengers[0]));
      echo '<div class="col-md-2 text-center" style="padding-left: 0; padding-right: 0;"><label>'.$this->lang->line("Sr. No").'</label></div><div class="col-md-6"><label>'.$this->lang->line("name").'</label></div><div class="col-md-4"><label>'.$this->lang->line("Age / Gender").'</label></div>'; 
      for ($i=0; $i <sizeof($no_of_passengers) ; $i++) { 
        $a = $this->api->get_address_from_book($no_of_passengers[$i]);
        echo '<div class="col-md-2 text-center" style="padding-left: 0; padding-right: 0;">'.($i+1).'</div><div class="col-md-6">'.$a['firstname'].' '.$a['lastname'].'</div><div class="col-md-4">'.$a['age'].' / '.(($a['gender']=='M')?'Male':"Female").'</div>';
      }
    }
    public function get_bus_trip_pickup_drop_locations()
    {
      $pickup = $this->api->get_locations_pickup_drop_points_for_booking_info_page($_POST['onward_pickup_point']);
      $drop = $this->api->get_locations_pickup_drop_points_for_booking_info_page($_POST['onward_drop_point']);
      echo $pickup."@".$drop;
    }
    public function bus_booking()
    {
      //echo json_encode($_POST); die();
      $no_of_seat = array();
      $no_of_seat = $_POST['selected_passengers'];
      $ownward_selected_seat_nos = $_POST['ownward_selected_seat_nos'];
      $return_selected_seat_nos = $_POST['return_selected_seat_nos'];
      if($ownward_selected_seat_nos == '') $ownward_selected_seat_nos = 'NULL';
      if($return_selected_seat_nos == '') $return_selected_seat_nos = 'NULL';

      $user_details = $this->api->get_user_details($_SESSION['cust_id']);
      $trip = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['ownward_trip_sdd_id']);
      $operator = $this->api->get_bus_operator_profile($user_details['cust_id']);
      $unique_id = str_replace("/","",$_POST['journey_date'])."_".$trip[0]['trip_id'];
      $country_id = $this->api->get_address_from_book($no_of_seat[0])['country_id'];
      //echo json_encode($country_id); die();
      $today =date('Y-m-d H:i:s');
      if(isset($_POST['return_trip_sdd_id']) && $_POST['return_trip_sdd_id'] > 0) {

        $sms_body_24_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];
        $sms_body_2_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];
        $email_body = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];

        $data_master = array(
          'trip_id' => $trip[0]['trip_id'] ,
          'cust_id' => $trip[0]['cust_id'] ,
          'source_point_id' => $trip[0]['trip_source_id'] ,
          'source_point' => $trip[0]['trip_source'] ,
          'pickup_point' => $_POST['onward_pickup_point'] ,
          'destination_point_id' => $trip[0]['trip_destination_id'] ,
          'destination_point' => $trip[0]['trip_destination'] ,
          'drop_point' => $_POST['onward_drop_point'] ,
          'bus_id' => $trip[0]['bus_id'] ,
          'journey_date' => $_POST['journey_date'] ,
          'trip_start_date_time' => $trip[0]['trip_depart_time'] ,
          'bookig_date' =>  date('Y-m-d H:i:s'),
          'seat_type' =>  $_POST['onward_bus_seat_type'],
          'no_of_seats' =>  sizeof($no_of_seat),
          'seat_price' =>  $_POST['seat_price'],
          'ticket_price' =>  ($_POST['seat_price']*sizeof($no_of_seat)),
          'currency_id' =>  $_POST['currency_id'],
          'currency_sign' =>  $_POST['currency_sign'],
          'cust_name' => $user_details['firstname'] ,
          'cust_contact' => $user_details['mobile1'] ,
          'cust_email' =>  $user_details['email1'],
          'operator_name' => $operator['firstname'],
          'operator_company_name' => $operator['company_name'],
          'operator_contact_name' =>  $operator['contact_no'],
          'operator_id' => $trip[0]['cust_id'],
          'balance_amount' => ($_POST['seat_price']*sizeof($no_of_seat)),
          'ticket_status' => "booked",
          'status_updated_by_user_type' => "operator",
          'status_update_datetime' => date('Y-m-d H:i:s'),
          'is_return' => 0,
          'return_trip_id' => 0,
          'unique_id' => $unique_id,
          'booked_by' => 'operator',
          'status_updated_by' => $user_details['cust_id'],
          'cat_id' => '281',
          'country_id' => $country_id,
          'vip_seat_nos' => $ownward_selected_seat_nos,
        );
        //echo json_encode($data_master); die();

        $ticket_id = $this->api->create_bus_ticket_booking_master($data_master);

        //Adding passenger details
        for($i=0; $i < sizeof($no_of_seat) ; $i++) { 
          $a = $this->api->get_address_from_book($no_of_seat[$i]);
          $data = array(
            'ticket_id' => $ticket_id,
            'trip_id' => $trip[0]['trip_id'],
            'cust_id' => $trip[0]['cust_id'] ,
            'operator_id' => $trip[0]['cust_id'],
            'seat_type' =>  $_POST['onward_bus_seat_type'],
            'cre_datetime' => date('Y-m-d H:i:s') ,
            'firstname' => $a['firstname'],
            'lastname' => $a['lastname'],
            'mobile' =>$a['mobile'],
            'email_id' => $a['email_id'] ,
            'gender' => $a['gender'],
            'country_id' => $a['country_id'],
            'age' => $a ['age'],
            'journey_date' => $_POST['journey_date'],
            'unique_id' => $unique_id,
            'ticket_status' => 'booked',
          );
          $res = $this->api->create_bus_ticket_booking_seat_details($data);

          //Check if trip date time is greater than current datetime + 2hrs 
          $datetime1 = new DateTime();
          //echo json_encode($datetime1); die();
          $datetime2 = new DateTime($_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00');
          $interval = $datetime1->diff($datetime2);
          //echo json_encode($interval); die();
          $years = $interval->y;
          $months = $interval->m;
          $days = $interval->d;
          $hours = $interval->h;
          $minutes = $interval->i;
          $seconds = $interval->s;
          $diff_type = $interval->invert;
          if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
          } else { 
            //Crone Job Alerts
            $crone_data_24_sms = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_24_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 24,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_24_sms);
            $crone_data_2_sms = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_2_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 2,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_2_sms);
            $crone_data = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => 'NULL',
              'email_body' => $email_body,
              'alert_type' => 1,
              'sms_duration' => 0,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data);
          }

          //Send Booking SMS
          $country_code = $this->api->get_country_code_by_id($a['country_id']);
          //$message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id;
          $seat_nos = ($ownward_selected_seat_nos == 'NULL')?'N/A':$ownward_selected_seat_nos;
          $bus_details = $this->api->get_operator_bus_details($trip[0]['bus_id']);
          $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id.'.  '.$this->lang->line('from').': '.$trip[0]['trip_source'].' '.$_POST['onward_pickup_point'].'.  '.$this->lang->line('To').': '.$trip[0]['trip_destination'].' '.$_POST['onward_drop_point'].'.  '.$this->lang->line('date').': '.$_POST['journey_date'].' '.$trip[0]['trip_depart_time'].'.  '.$this->lang->line('Vehicle').': '.$bus_details['bus_modal'].'. '.$this->lang->line('Seats').': '.$seat_nos . ' - ' . $operator['company_name'];
          
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
          //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
          if( trim($a['firstname']) == 'NULL' || trim($a['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($a['email_id']));
            $username = $parts[0];
            $customer_name = $username;
          } else {
            $customer_name = trim($a['firstname']) . " " . trim($a['lastname']);
          }
          $this->api_sms->send_email_text($customer_name, trim($a['email_id']), $this->lang->line('Ticket booking confirmation'), trim($message));
        }//for

        //Update to workroom
        $workroom_update = array(
          'order_id' => $ticket_id,
          'cust_id' => $trip[0]['cust_id'],
          'deliverer_id' => $trip[0]['cust_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $trip[0]['cust_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $customer_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);

        //Update to Global Workroom
        $global_workroom_update = array(
          'service_id' => $ticket_id,
          'cat_id' => 281,
          'cust_id' => 67,
          'deliverer_id' => $trip[0]['cust_id'],
          'sender_id' => $trip[0]['cust_id'],
        );
        $this->api->create_global_workroom($global_workroom_update);

        $return_trip = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['return_trip_sdd_id']);
        $unique_id = str_replace("/","",$_POST['return_date'])."_".$return_trip[0]['trip_id'];

        $sms_body_24_hr = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['return_date'].' '.$return_trip[0]['trip_depart_time'];
        $sms_body_2_hr = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['return_date'].' '.$return_trip[0]['trip_depart_time'];
        $email_body = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['return_date'].' '.$return_trip[0]['trip_depart_time'];

        $return_data_master = array(
          'trip_id' => $return_trip[0]['trip_id'],
          'cust_id' => $return_trip[0]['cust_id'],
          'source_point_id' => $return_trip[0]['trip_source_id'],
          'source_point' => $return_trip[0]['trip_source'],
          'pickup_point' => $_POST['onward_pickup_point'],
          'destination_point_id' => $return_trip[0]['trip_destination_id'],
          'destination_point' => $return_trip[0]['trip_destination'],
          'drop_point' => $_POST['onward_drop_point'],
          'bus_id' => $return_trip[0]['bus_id'],
          'journey_date' => $_POST['return_date'],
          'trip_start_date_time' => $return_trip[0]['trip_depart_time'],
          'bookig_date' =>  date('Y-m-d H:i:s'),
          'seat_type' =>  $_POST['return_bus_seat_type'],
          'no_of_seats' =>  sizeof($no_of_seat),
          'seat_price' =>  $_POST['return_seat_price'],
          'ticket_price' =>  ($_POST['return_seat_price']*sizeof($no_of_seat)),
          'currency_id' =>  $_POST['return_currency_id'],
          'currency_sign' =>  $_POST['return_currency_sign'],
          'cust_name' => $user_details['firstname'],
          'cust_contact' => $user_details['mobile1'],
          'cust_email' =>  $user_details['email1'],
          'operator_name' => $operator['firstname'],
          'operator_company_name' => $operator['company_name'],
          'operator_contact_name' =>  $operator['contact_no'],
          'operator_id' => $return_trip[0]['cust_id'],
          'balance_amount' =>  ($_POST['return_seat_price']*sizeof($no_of_seat)),
          'ticket_status' => "booked",
          'status_updated_by_user_type' => "operator",
          'status_update_datetime' => date('Y-m-d H:i:s'),
          'is_return' => 1,
          'return_trip_id' => $ticket_id,
          'unique_id' => $unique_id,
          'booked_by' => 'operator',
          'status_updated_by' => $return_trip[0]['cust_id'],
          'cat_id' => '281',
          'country_id' => $country_id,
          'vip_seat_nos' => $return_selected_seat_nos,
        );

        $return_ticket_id = $this->api->create_bus_ticket_booking_master($return_data_master);

        //Update return trip ID
        $dt = array(
          'return_trip_id' => $return_ticket_id
        );
        $uptd = $this->api->update_booking_details($dt , $ticket_id);

        //Adding return passenger details
        for($i=0; $i < sizeof($no_of_seat) ; $i++) { 
          $a = $this->api->get_address_from_book($no_of_seat[$i]);
          $data = array(
            'ticket_id' => $return_ticket_id,
            'trip_id' => $return_trip[0]['trip_id'],
            'cust_id' => $return_trip[0]['cust_id'] ,
            'operator_id' => $return_trip[0]['cust_id'],
            'seat_type' =>  $_POST['return_bus_seat_type'],
            'cre_datetime' => date('Y-m-d H:i:s') ,
            'firstname' => $a['firstname'],
            'lastname' => $a['lastname'],
            'mobile' =>$a['mobile'],
            'email_id' => $a['email_id'] ,
            'gender' => $a['gender'],
            'country_id' => $a['country_id'],
            'age' => $a ['age'] ,
            'journey_date' => $_POST['return_date'],
            'unique_id' => $unique_id,
            'ticket_status' => 'booked',
          );
          $res = $this->api->create_bus_ticket_booking_seat_details($data);
          //Check if trip date time is greater than current datetime + 2hrs 
          $datetime1 = new DateTime();
          //echo json_encode($datetime1); die();
          $datetime2 = new DateTime($_POST['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00');
          $interval = $datetime1->diff($datetime2);
          //echo json_encode($interval); die();
          $years = $interval->y;
          $months = $interval->m;
          $days = $interval->d;
          $hours = $interval->h;
          $minutes = $interval->i;
          $seconds = $interval->s;
          $diff_type = $interval->invert;
          if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
          } else { 
            //Crone Job Alerts
            $crone_data_24_sms = array(
              'journey_date' => $_POST['return_date'],
              'journey_time' => $return_trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id, 
              'ticket_id' => $return_ticket_id,
              'trip_id' => $return_trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_24_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 24,
              'journey_datetime' => $_POST['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_24_sms);
            $crone_data_2_sms = array(
              'journey_date' => $_POST['return_date'],
              'journey_time' => $return_trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id, 
              'ticket_id' => $return_ticket_id,
              'trip_id' => $return_trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_2_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 2,
              'journey_datetime' => $_POST['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_2_sms);
            $crone_data = array(
              'journey_date' => $_POST['return_date'],
              'journey_time' => $return_trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id, 
              'ticket_id' => $return_ticket_id,
              'trip_id' => $return_trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => 'NULL',
              'email_body' => $email_body,
              'alert_type' => 1,
              'sms_duration' => 0,
              'journey_datetime' => $_POST['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data);
          }

          //Send Booking SMS
          $country_code = $this->api->get_country_code_by_id($a['country_id']);
          //$message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$return_ticket_id;
          $seat_nos = ($return_selected_seat_nos == 'NULL')?'N/A':$return_selected_seat_nos;
          $return_bus_details = $this->api->get_operator_bus_details($return_trip[0]['bus_id']);
          $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$return_ticket_id.'.  '.$this->lang->line('from').': '.$return_trip[0]['trip_source'].' '.$_POST['return_pickup_point'].'.  '.$this->lang->line('To').': '.$return_trip[0]['trip_destination'].' '.$_POST['return_drop_point'].'.  '.$this->lang->line('date').': '.$_POST['return_date'].' '.$return_trip[0]['trip_depart_time'].'.  '.$this->lang->line('Vehicle').': '.$return_bus_details['bus_modal'].'. '.$this->lang->line('Seats').': '.$seat_nos . ' - ' . $operator['company_name'];
          
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
          
          //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
          if( trim($a['firstname']) == 'NULL' || trim($a['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($a['email_id']));
            $username = $parts[0];
            $customer_name = $username;
          } else {
            $customer_name = trim($a['firstname']) . " " . trim($a['lastname']);
          }
          $this->api_sms->send_email_text($customer_name, trim($a['email_id']), $this->lang->line('Ticket booking confirmation'), trim($message));
        }//for

        //Update to workroom
        $workroom_update = array(
          'order_id' => $return_ticket_id,
          'cust_id' => $return_trip[0]['cust_id'],
          'deliverer_id' => $return_trip[0]['cust_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $trip[0]['cust_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $customer_name,
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
      }
      
      if(!isset($data_master)) {
        $sms_body_24_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];
        $sms_body_2_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];
        $email_body = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];

        $data_master = array(
          'trip_id' => $trip[0]['trip_id'],
          'cust_id' => $trip[0]['cust_id'],
          'source_point_id' => $trip[0]['trip_source_id'],
          'source_point' => $trip[0]['trip_source'],
          'pickup_point' => $_POST['onward_pickup_point'],
          'destination_point_id' => $trip[0]['trip_destination_id'],
          'destination_point' => $trip[0]['trip_destination'],
          'drop_point' => $_POST['onward_drop_point'],
          'bus_id' => $trip[0]['bus_id'],
          'journey_date' => $_POST['journey_date'],
          'trip_start_date_time' => $trip[0]['trip_depart_time'],
          'bookig_date' =>  date('Y-m-d H:i:s'),
          'seat_type' =>  $_POST['bus_seat_type'],
          'no_of_seats' =>  sizeof($no_of_seat),
          'seat_price' =>  $_POST['seat_price'],
          'ticket_price' =>  ($_POST['seat_price']*sizeof($no_of_seat)),
          'currency_id' =>  $_POST['currency_id'],
          'currency_sign' =>  $_POST['currency_sign'],
          'cust_name' => $user_details['firstname'],
          'cust_contact' => $user_details['mobile1'] ,
          'cust_email' =>  $user_details['email1'],
          'operator_name' => $operator['firstname'],
          'operator_company_name' => $operator['company_name'],
          'operator_contact_name' =>  $operator['contact_no'],
          'operator_id' => $trip[0]['cust_id'],
          'balance_amount' => ($_POST['seat_price']*sizeof($no_of_seat)),
          'ticket_status' => "booked",
          'status_updated_by_user_type' => "operator",
          'status_update_datetime' => date('Y-m-d H:i:s'),
          'is_return' => 0,
          'return_trip_id' => 0,
          'unique_id' => $unique_id,
          'booked_by' => 'operator',
          'status_updated_by' => $user_details['cust_id'],
          'cat_id' => '281',
          'country_id' => $country_id,
          'vip_seat_nos' => $ownward_selected_seat_nos,
        );
        $ticket_id = $this->api->create_bus_ticket_booking_master($data_master);
        for($i=0; $i < sizeof($no_of_seat) ; $i++) { 
          $a = $this->api->get_address_from_book($no_of_seat[$i]);
          $data = array(
            'ticket_id' => $ticket_id,
            'trip_id' => $trip[0]['trip_id'],
            'cust_id' => $trip[0]['cust_id'],
            'operator_id' => $trip[0]['cust_id'],
            'seat_type' =>  $_POST['bus_seat_type'],
            'cre_datetime' => date('Y-m-d H:i:s') ,
            'firstname' => $a['firstname'],
            'lastname' => $a['lastname'],
            'mobile' =>$a['mobile'],
            'email_id' => $a['email_id'] ,
            'gender' => $a['gender'],
            'country_id' => $a['country_id'],
            'age' => $a ['age'],
            'journey_date' => $_POST['journey_date'],
            'unique_id' => $unique_id,
            'ticket_status' => 'booked',
          );
          $res = $this->api->create_bus_ticket_booking_seat_details($data);

          //Check if trip date time is greater than current datetime + 2hrs 
          $datetime1 = new DateTime();
          //echo json_encode($datetime1); die();
          $datetime2 = new DateTime($_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00');
          $interval = $datetime1->diff($datetime2);
          //echo json_encode($interval); die();
          $years = $interval->y;
          $months = $interval->m;
          $days = $interval->d;
          $hours = $interval->h;
          $minutes = $interval->i;
          $seconds = $interval->s;
          $diff_type = $interval->invert;
          if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
          } else { 
            //Crone Job Alerts
            $crone_data_24_sms = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_24_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 24,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_24_sms);
            $crone_data_2_sms = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_2_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 2,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_2_sms);
            $crone_data = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => 'NULL',
              'email_body' => $email_body,
              'alert_type' => 1,
              'sms_duration' => 0,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data);
          }

          //Send Booking SMS
          $country_code = $this->api->get_country_code_by_id($a['country_id']);
          //$message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id;
          $seat_nos = ($ownward_selected_seat_nos == 'NULL')?'N/A':$ownward_selected_seat_nos;
          $bus_details = $this->api->get_operator_bus_details($trip[0]['bus_id']);
          $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id.'.  '.$this->lang->line('from').': '.$trip[0]['trip_source'].' '.$_POST['onward_pickup_point'].'.  '.$this->lang->line('To').': '.$trip[0]['trip_destination'].' '.$_POST['onward_drop_point'].'.  '.$this->lang->line('date').': '.$_POST['journey_date'].' '.$trip[0]['trip_depart_time'].'.  '.$this->lang->line('Vehicle').': '.$bus_details['bus_modal'].'. '.$this->lang->line('Seats').': '.$seat_nos . ' - ' . $operator['company_name'];
          
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
          
          //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
          if( trim($a['firstname']) == 'NULL' || trim($a['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($a['email_id']));
            $username = $parts[0];
            $customer_name = $username;
          } else {
            $customer_name = trim($a['firstname']) . " " . trim($a['lastname']);
          }
          $this->api_sms->send_email_text($customer_name, trim($a['email_id']), $this->lang->line('Ticket booking confirmation'), trim($message));
        }//for

        //Update to workroom
        $workroom_update = array(
          'order_id' => $ticket_id,
          'cust_id' => $trip[0]['cust_id'],
          'deliverer_id' => $trip[0]['cust_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $trip[0]['cust_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $user_details['firstname'],
          'deliverer_name' => $operator['firstname'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        //Update to Global Workroom
        $global_workroom_update = array(
          'service_id' => $ticket_id,
          'cat_id' => 281,
          'cust_id' => 67,
          'deliverer_id' => $trip[0]['cust_id'],
          'sender_id' => $trip[0]['cust_id'],
        );
        $this->api->create_global_workroom($global_workroom_update);
      } //if 
      redirect(base_url('user-panel-bus/bus-ticket-payment-seller').'/'.$ticket_id,'refresh');
    }
    public function bus_ticket_payment_seller()
    {
      //echo json_encode($_POST); die();
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/seller-upcoming-trips'); }

      //echo json_encode($ticket_id); die();
      
      $details =  $this->api->get_trip_booking_details($ticket_id);

      //if ownward trip has return trip - get return trip details.
      $return_details = array();
      if($details['return_trip_id'] > 0) {
        $return_details =  $this->api->get_trip_booking_details($details['return_trip_id']);
      }

      //echo json_encode($details); die();
      $this->load->view('user_panel/bus_seller_ticket_payment_view',compact('details','return_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_single_ticket_payment_seller()
    {
      //echo json_encode($_POST); die();
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/seller-upcoming-trips'); }

      //echo json_encode($ticket_id); die();
      $details =  $this->api->get_trip_booking_details($ticket_id);

      //echo json_encode($details); die();
      $this->load->view('user_panel/bus_seller_single_ticket_payment_view',compact('details'));
      $this->load->view('user_panel/footer');
    }
    public function seller_ticket_mark_as_cod()
    {
      //echo json_encode($_POST); die();
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $details = $this->api->get_trip_booking_details($ticket_id);
      $update_data = array(
        "payment_mode" => 'cod',
      );
      //echo json_encode($update_data); die();
      if($this->api->update_booking_details($update_data, $ticket_id)) {
        //if ownward trip has return trip - get return trip details.
        $return_details = array();
        if($details['return_trip_id'] > 0) {
          $return_details =  $this->api->get_trip_booking_details($details['return_trip_id']);
          $update_data = array(
            "payment_mode" => 'cod',
          );
          $this->api->update_booking_details($update_data, $return_details['ticket_id']);
        }
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-bus/seller-upcoming-trips');
    }
    public function bus_seller_ticket_payment()
    {
      if($msg = $this->session->flashdata('success')) {  $ticket_id = $msg['ticket_id']; } 
      else if( $this->input->post('ticket_id') > 0 ) { $ticket_id = $this->input->post('ticket_id'); } 
      else{ redirect('user-panel-bus/buyer-upcoming-trips'); }
      
      $details =  $this->api->get_trip_booking_details($ticket_id);

      //if ownward trip has return trip - get return trip details.
      $return_details = array();
      if($details['return_trip_id'] > 0) {
        $return_details =  $this->api->get_trip_booking_details($details['return_trip_id']);
      }

      //echo json_encode($details); die();
      $this->load->view('user_panel/bus_ticket_payment_view',compact('details','return_details'));
      $this->load->view('user_panel/footer');
    }
    public function ticket_confirm_payment_seller()
    {
      echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $today = date('Y-m-d h:i:s');

      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $total_amount_paid = $ticket_details['ticket_price'];
      $transfer_account_number = "NULL";
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = $this->input->post('stripeEmail', TRUE);
      $mobile_number = "NULL";    

      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => "web",
        "complete_paid" => 1,
      );

     // echo json_encode($update_data); die();

      if($this->api->update_booking_details($update_data, $ticket_id)) {

        /*************************************** Payment Section ****************************************/
        //Add payment details in customer account master and history
        if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
        } else {
          $insert_data_customer_master = array(
            "user_id" => $cust_id,
            "account_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($ticket_details['currency_sign']),
          );
          $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
        }
        $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
        $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$ticket_id,
          "user_id" => (int)$cust_id,
          "datetime" => $today,
          "type" => 1,
          "transaction_type" => 'add',
          "amount" => trim($ticket_details['ticket_price']),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "withdraw_request_id" => 0,
          "currency_code" => trim($ticket_details['currency_sign']),
          "cat_id" => 281
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);

        //Deduct payment details from customer account master and history
        $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
        $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$ticket_id,
          "user_id" => (int)$cust_id,
          "datetime" => $today,
          "type" => 0,
          "transaction_type" => 'payment',
          "amount" => trim($ticket_details['ticket_price']),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "withdraw_request_id" => 0,
          "currency_code" => trim($ticket_details['currency_sign']),
          "cat_id" => 281
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);

        //Add payment details in Operator account master and history
        if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
        } else {
          $insert_data_customer_master = array(
            "user_id" => trim($ticket_details['operator_id']),
            "account_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($ticket_details['currency_sign']),
          );
          $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
        }
        $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
        $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$ticket_id,
          "user_id" => (int)trim($ticket_details['operator_id']),
          "datetime" => $today,
          "type" => 1,
          "transaction_type" => 'add',
          "amount" => trim($ticket_details['ticket_price']),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "withdraw_request_id" => 0,
          "currency_code" => trim($ticket_details['currency_sign']),
          "cat_id" => 281
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);

        //Deduct Comission from OPerator account master and history
        $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
        $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
        $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
        $update_data_account_history = array(
          "account_id" => (int)$customer_account_master_details['account_id'],
          "order_id" => (int)$ticket_id,
          "user_id" => (int)$cust_id,
          "datetime" => $today,
          "type" => 0,
          "transaction_type" => 'payment',
          "amount" => trim($ticket_details['ticket_price']),
          "account_balance" => trim($customer_account_master_details['account_balance']),
          "withdraw_request_id" => 0,
          "currency_code" => trim($ticket_details['currency_sign']),
          "cat_id" => 281
        );
        $this->api->insert_payment_in_account_history($update_data_account_history);

        if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
        } else {
          $insert_data_gonagoo_master = array(
            "gonagoo_balance" => 0,
            "update_datetime" => $today,
            "operation_lock" => 1,
            "currency_code" => trim($ticket_details['currency_sign']),
          );
          $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
        }

        $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ticket_details['ticket_price']);
        $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
        $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

        $update_data_gonagoo_history = array(
          "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
          "order_id" => (int)$ticket_id,
          "user_id" => (int)$cust_id,
          "type" => 1,
          "transaction_type" => 'standard_price',
          "amount" => trim($ticket_details['ticket_price']),
          "datetime" => $today,
          "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
          "transfer_account_type" => trim($payment_method),
          "transfer_account_number" => trim($transfer_account_number),
          "bank_name" => trim($bank_name),
          "account_holder_name" => trim($account_holder_name),
          "iban" => trim($iban),
          "email_address" => trim($email_address),
          "mobile_number" => trim($mobile_number),
          "transaction_id" => trim($transaction_id),
          "currency_code" => trim($ticket_details['currency_sign']),
          "country_id" => trim($user_details['country_id']),
          "cat_id" => trim($ticket_details['cat_id']),
        );
        $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

        /*************************************** Payment Section ****************************************/
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      redirect('user-panel-bus/buyer-upcoming-trips');
    }
    public function get_trip_master_source_list_seller()
    {
      $source_string = $this->input->post('source_string');
      $sources = $this->api->get_trip_master_source_list($source_string);
      echo json_encode($sources);
    }
    public function seller_ticket_confirm_payment_oldd()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $ticket_price = $this->input->post('ticket_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);
      $today = date('Y-m-d h:i:s');
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $seat_details = $this->api->get_ticket_seat_details($ticket_id);
      //echo json_encode($seat_details); die();

      $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
      $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
      //echo json_encode($country_id); die();

      $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
      //echo json_encode($gonagoo_commission_percentage); die();
      $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

      //echo json_encode($ownward_trip_commission); die();
      
      $total_amount_paid = $ticket_details['ticket_price'];
      $transfer_account_number = "NULL";
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = $this->input->post('stripeEmail', TRUE);
      $mobile_number = "NULL";    

      $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
      $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
      $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
      $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

      //Send Push Notifications
      //Get PN API Keys
      $api_key = $this->config->item('delivererAppGoogleKey');
      $device_details_customer = $this->api->get_user_device_details($cust_id);
      if(!is_null($device_details_customer)) {
        $arr_customer_fcm_ids = array();
        $arr_customer_apn_ids = array();
        foreach ($device_details_customer as $value) {
          if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
        }
        $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
        $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

        //Push Notification APN
        $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
        if(is_array($arr_customer_apn_ids)) { 
          $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
          $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
        }
      }

      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => "web",
        "complete_paid" => 1,
      );
      //echo json_encode($update_data); die();

      if($this->api->update_booking_details($update_data, $ticket_id)) {
        //Update to workroom
        $workroom_update = array(
          'order_id' => $ticket_id,
          'cust_id' => $ticket_details['cust_id'],
          'deliverer_id' => $ticket_details['operator_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $ticket_details['operator_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $ticket_details['cust_name'],
          'deliverer_name' => $ticket_details['operator_name'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        /********************* Payment Section ************************************/

          //---------------------Insert counter payment record----------------------
            $data_counter_sale_payment = array(
              "cat_id" => $ticket_details['cat_id'],
              "service_id" => $ticket_details['ticket_id'],
              "cust_id" => $ticket_details['operator_id'],
              "cre_datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "currency_code" => trim($ticket_details['currency_sign']),
              "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
              "cat_name" => 'ticket_booking',
              "customer_email" => trim($seat_details[0]['email_id']),
              "customer_mobile" => trim($seat_details[0]['mobile']),
              "customer_country_id" => trim($seat_details[0]['country_id']),
            );
            $this->api->insert_counter_sale_payment($data_counter_sale_payment);
          //---------------------Insert counter payment record----------------------


          //Add Ownward Trip Payment to trip operator account + History
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              //echo json_encode($customer_account_master_details); die();
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }//else

            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            //echo json_encode($account_balance); die();

            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            
            //echo json_encode($customer_account_master_details); die();
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Add Ownward Trip Payment to trip operator account + History

          /* -----------------------COD ticket invoice--------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $user_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);
            if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details[0]['email_id']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details[0]['country_id']));
            // $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            // $user_city = $this->api->get_city_details(trim($user_details['city_id']));
            $user_state = "---";
            $user_city = "---";

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($user_details[0]['email_id'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_Ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_Ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            //Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "ticket-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* -----------------------COD ticket invoice--------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */
        
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = trim($user_details[0]['email_id']);
              $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
              $fileName = $my_ticket_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */

          //Deduct Ownward Trip Commission From Operator Account
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Deduct Ownward Trip Commission From Operator Account
          
          //Add Ownward Trip Commission To Gonagoo Account
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //Add Ownward Trip Commission To Gonagoo Account
          
          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
          
          //----------------------Return Trip Payment Calculation-----------------
            if($ticket_details['return_trip_id'] > 0) {
              $return_ticket_details = $this->api->get_trip_booking_details($ticket_details['return_trip_id']);
              $return_trip_operator_detail = $this->api->get_user_details($return_ticket_details['operator_id']);

              $country_code = $this->api->get_country_code_by_id($return_trip_operator_detail['country_id']);
              $message = $this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id'];
              $this->api->sendSMS((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);

              $return_update_data = array(
                "payment_method" => trim($payment_method),
                "paid_amount" => trim($return_ticket_details['ticket_price']),
                "balance_amount" => 0,
                "transaction_id" => trim($transaction_id),
                "payment_datetime" => $today,
                "payment_mode" => "online",
                "payment_by" => "web",
                "complete_paid" => 1,
              );
              $this->api->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);


              //--------------Insert counter payment record----------------
                $data_counter_sale_payment = array(
                  "cat_id" => $return_ticket_details['cat_id'],
                  "service_id" => $return_ticket_details['ticket_id'],
                  "cust_id" => $return_ticket_details['operator_id'],
                  "cre_datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'payment',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
                  "cat_name" => 'ticket_booking',
                  "customer_email" => trim($seat_details[0]['email_id']),
                  "customer_mobile" => trim($seat_details[0]['mobile']),
                  "customer_country_id" => trim($seat_details[0]['country_id']),
                );
                $this->api->insert_counter_sale_payment($data_counter_sale_payment);
              //--------------Insert counter payment record-----------------

              //-----------------Update to workroom--------------------------
                $workroom_update = array(
                  'order_id' => $return_ticket_details['ticket_id'],
                  'cust_id' => $return_ticket_details['cust_id'],
                  'deliverer_id' => $return_ticket_details['operator_id'],
                  'text_msg' => $message,
                  'attachment_url' =>  "NULL",
                  'cre_datetime' => $today,
                  'sender_id' => $return_ticket_details['operator_id'],
                  'type' => 'payment',
                  'file_type' => 'text',
                  'cust_name' => $return_ticket_details['cust_name'],
                  'deliverer_name' => $return_ticket_details['operator_name'],
                  'thumbnail' => 'NULL',
                  'ratings' => 'NULL',
                  'cat_id' => 281
                );
                $this->api->add_bus_chat_to_workroom($workroom_update);
              //-------------------Update to workroom------------------------

              //------Add Return Trip Payment to trip operator account + History---
                if($this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_customer_master = array(
                    "user_id" => trim($return_ticket_details['operator_id']),
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                }
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //------Add Return Trip Payment to trip operator account + History---

              /* -------------------------Return ticket invoice------------------------ */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $user_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);

                if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($user_details[0]['email_id']));
                  $username = $parts[0];
                  $customer_name = $username;
                } else {
                  $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $user_country = $this->api->get_country_details(trim($user_details[0]['country_id']));
                // $user_state = $this->api->get_state_details(trim($user_details['state_id']));
                // $user_city = $this->api->get_city_details(trim($user_details['city_id']));
                $user_state = "---";
                $user_city = "---";

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($user_details[0]['email_id'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
                $rate = round(trim($return_ticket_details['ticket_price']),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph* /
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_Return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_Return_trip.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                //Update to workroom
                $workroom_update = array(
                  'order_id' => $return_ticket_details['ticket_id'],
                  'cust_id' => $return_ticket_details['cust_id'],
                  'deliverer_id' => $return_ticket_details['operator_id'],
                  'text_msg' => $this->lang->line('Invoice'),
                  'attachment_url' =>  "ticket-invoices/".$pdf_name,
                  'cre_datetime' => $today,
                  'sender_id' => $return_ticket_details['operator_id'],
                  'type' => 'raise_invoice',
                  'file_type' => 'pdf',
                  'cust_name' => $return_ticket_details['cust_name'],
                  'deliverer_name' => $return_ticket_details['operator_name'],
                  'thumbnail' => 'NULL',
                  'ratings' => 'NULL',
                  'cat_id' => 281
                );
                $this->api->add_bus_chat_to_workroom($workroom_update);
              /* -------------------------Return ticket invoice----------------------- */
              
                /* -----------------Email Invoice to customer-------------------------- */
                  $emailBody = $this->lang->line('Please download your ticket payment invoice.');
                  $gonagooAddress = $gonagoo_address['company_name'];
                  $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
                  $emailAddress = trim($user_details[0]['email_id']);
                  $fileToAttach = "resources/ticket-invoices/$pdf_name";
                  $fileName = $pdf_name;
                  $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
                /* -----------------------Email Invoice to customer-------------------- */
              
              /* -------------------------Return Ticket pdf ------------------------- */
              $return_operator_details = $this->api->get_bus_operator_profile($return_ticket_details['operator_id']);
              if($return_operator_details["avatar_url"]=="" || $return_operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
                 $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
                 $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
                if(rename($img_src, $img_dest));
              $html ='
              <page format="100x100" orientation="L" style="font: arial;">';
              $html .= '
              <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                    <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
                    <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                    <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                    <br />
                    <h6>'.$this->lang->line("slider_heading1").'</h6>
                </div>                
              </div>
              <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line("Ownward_Trip").'<br/>
                <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
                <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                <h6 style="margin-top:-10px;">
                <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
                <h6 style="margin-top:-8px;"> 
                <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                $ticket_seat_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);

                $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                foreach ($ticket_seat_details as $seat)
                {
                  $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                  if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                    
                   $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                }
              $html .='</tbody></table></div></page>';            
              $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
              $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
              rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
              //whatsapp api send ticket
              $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
              $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
              //echo json_encode($result); die();
            /* -------------------------Return Ticket pdf ------------------------- */
            
              /* ---------------------Email Ticket to customer----------------------- */
                  $emailBody = $this->lang->line('Please download your e-ticket.');
                  $gonagooAddress = $gonagoo_address['company_name'];
                  $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                  $emailAddress = trim($user_details[0]['email_id']);
                  $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                  $fileName = $my_ticket_pdf;
                  $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
                /* ---------------------Email Ticket to customer----------------------- */

              //Deduct Return Trip Commission From Operator Account
                $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

                $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //Deduct Return Trip Commission From Operator Account

              //Add Return Trip Commission To Gonagoo Account
                if($this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_gonagoo_master = array(
                    "gonagoo_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                }

                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
                $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "type" => 1,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "country_id" => trim($return_trip_operator_detail['country_id']),
                  "cat_id" => trim($return_ticket_details['cat_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              //Add Return Trip Commission To Gonagoo Account

              /* Return ticket commission invoice----------------------------------- */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
                $rate = round(trim($return_trip_commission),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Commission Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "commission_invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
              /* -------------------------------------------------------------------- */
              
                /* -----------------------Email Invoice to operator-------------------- */
                  $emailBody = $this->lang->line('Commission Invoice');
                  $gonagooAddress = $gonagoo_address['company_name'];
                  $emailSubject = $this->lang->line('Commission Invoice');
                  $emailAddress = $return_trip_operator_detail['email1'];
                  $fileToAttach = "resources/ticket-invoices/$pdf_name";
                  $fileName = $pdf_name;
                  $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
                /* -----------------------Email Invoice to operator-------------------- */
            }
          //----------------------Return Trip Payment Calculation-----------------
          
        /********************* Payment Section ************************************/
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      redirect('user-panel-bus/seller-upcoming-trips');
    }
    public function seller_ticket_confirm_payment()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $ticket_price = $this->input->post('ticket_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);
      $today = date('Y-m-d h:i:s');
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $seat_details = $this->api->get_ticket_seat_details($ticket_id);
      //echo json_encode($seat_details); die();

      $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
      $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
      //echo json_encode($country_id); die();

      $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
      //echo json_encode($gonagoo_commission_percentage); die();
      $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

      //echo json_encode($ownward_trip_commission); die();
      
      $total_amount_paid = $ticket_details['ticket_price'];
      $transfer_account_number = "NULL";
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = $this->input->post('stripeEmail', TRUE);
      $mobile_number = "NULL";    

      $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
      $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
      $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
      $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

      //Send Push Notifications
      //Get PN API Keys
      $api_key = $this->config->item('delivererAppGoogleKey');
      $device_details_customer = $this->api->get_user_device_details($cust_id);
      if(!is_null($device_details_customer)) {
        $arr_customer_fcm_ids = array();
        $arr_customer_apn_ids = array();
        foreach ($device_details_customer as $value) {
          if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
        }
        $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
        $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

        //Push Notification APN
        $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
        if(is_array($arr_customer_apn_ids)) { 
          $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
          $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
        }
      }

      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => "web",
        "complete_paid" => 1,
      );
      //echo json_encode($update_data); die();

      if($this->api->update_booking_details($update_data, $ticket_id)) {
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);

        //Update to workroom
        $workroom_update = array(
          'order_id' => $ticket_id,
          'cust_id' => $ticket_details['cust_id'],
          'deliverer_id' => $ticket_details['operator_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $ticket_details['operator_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $ticket_details['cust_name'],
          'deliverer_name' => $ticket_details['operator_name'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        /********************* Payment Section ************************************/

          //---------------------Insert counter payment record----------------------
            $data_counter_sale_payment = array(
              "cat_id" => $ticket_details['cat_id'],
              "service_id" => $ticket_details['ticket_id'],
              "cust_id" => $ticket_details['operator_id'],
              "cre_datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "currency_code" => trim($ticket_details['currency_sign']),
              "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
              "cat_name" => 'ticket_booking',
              "customer_email" => trim($seat_details[0]['email_id']),
              "customer_mobile" => trim($seat_details[0]['mobile']),
              "customer_country_id" => trim($seat_details[0]['country_id']),
            );
            $this->api->insert_counter_sale_payment($data_counter_sale_payment);
          //---------------------Insert counter payment record----------------------


          //Add Ownward Trip Payment to trip operator account + History
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              //echo json_encode($customer_account_master_details); die();
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }//else

            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            //echo json_encode($account_balance); die();

            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            
            //echo json_encode($customer_account_master_details); die();
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Add Ownward Trip Payment to trip operator account + History

          /* -----------------------COD ticket invoice--------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $user_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);
            if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details[0]['email_id']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details[0]['country_id']));
            // $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            // $user_city = $this->api->get_city_details(trim($user_details['city_id']));
            $user_state = "---";
            $user_city = "---";

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($user_details[0]['email_id'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_Ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_Ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            //Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "ticket-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* -----------------------COD ticket invoice--------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */
            
          //Deduct Ownward Trip Commission From Operator Account
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Deduct Ownward Trip Commission From Operator Account
          
          //Add Ownward Trip Commission To Gonagoo Account
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //Add Ownward Trip Commission To Gonagoo Account
          
          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */

          //----------------------Return Trip Payment Calculation-----------------
            if($ticket_details['return_trip_id'] > 0) {
              $return_ticket_details = $this->api->get_trip_booking_details($ticket_details['return_trip_id']);
              $return_trip_operator_detail = $this->api->get_user_details($return_ticket_details['operator_id']);

              $country_code = $this->api->get_country_code_by_id($return_trip_operator_detail['country_id']);
              $message = $this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id'];
              $this->api->sendSMS((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);

              $return_update_data = array(
                "payment_method" => trim($payment_method),
                "paid_amount" => trim($return_ticket_details['ticket_price']),
                "balance_amount" => 0,
                "transaction_id" => trim($transaction_id),
                "payment_datetime" => $today,
                "payment_mode" => "online",
                "payment_by" => "web",
                "complete_paid" => 1,
              );
              $this->api->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);

              //Update Gonagoo commission for ticket in seat details table
              $seat_update_data = array(
                "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
              );
              $this->api->update_booking_seat_details($seat_update_data, $return_ticket_details['ticket_id']);

              //--------------Insert counter payment record----------------
                $data_counter_sale_payment = array(
                  "cat_id" => $return_ticket_details['cat_id'],
                  "service_id" => $return_ticket_details['ticket_id'],
                  "cust_id" => $return_ticket_details['operator_id'],
                  "cre_datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'payment',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
                  "cat_name" => 'ticket_booking',
                  "customer_email" => trim($seat_details[0]['email_id']),
                  "customer_mobile" => trim($seat_details[0]['mobile']),
                  "customer_country_id" => trim($seat_details[0]['country_id']),
                );
                $this->api->insert_counter_sale_payment($data_counter_sale_payment);
              //--------------Insert counter payment record-----------------

              //-----------------Update to workroom--------------------------
                $workroom_update = array(
                  'order_id' => $return_ticket_details['ticket_id'],
                  'cust_id' => $return_ticket_details['cust_id'],
                  'deliverer_id' => $return_ticket_details['operator_id'],
                  'text_msg' => $message,
                  'attachment_url' =>  "NULL",
                  'cre_datetime' => $today,
                  'sender_id' => $return_ticket_details['operator_id'],
                  'type' => 'payment',
                  'file_type' => 'text',
                  'cust_name' => $return_ticket_details['cust_name'],
                  'deliverer_name' => $return_ticket_details['operator_name'],
                  'thumbnail' => 'NULL',
                  'ratings' => 'NULL',
                  'cat_id' => 281
                );
                $this->api->add_bus_chat_to_workroom($workroom_update);
              //-------------------Update to workroom------------------------

              //------Add Return Trip Payment to trip operator account + History---
                if($this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_customer_master = array(
                    "user_id" => trim($return_ticket_details['operator_id']),
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                }
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //------Add Return Trip Payment to trip operator account + History---

              /* -------------------------Return ticket invoice------------------------ */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $user_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);

                if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($user_details[0]['email_id']));
                  $username = $parts[0];
                  $customer_name = $username;
                } else {
                  $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $user_country = $this->api->get_country_details(trim($user_details[0]['country_id']));
                // $user_state = $this->api->get_state_details(trim($user_details['state_id']));
                // $user_city = $this->api->get_city_details(trim($user_details['city_id']));
                $user_state = "---";
                $user_city = "---";

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($user_details[0]['email_id'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
                $rate = round(trim($return_ticket_details['ticket_price']),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph* /
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_Return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_Return_trip.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                //Update to workroom
                $workroom_update = array(
                  'order_id' => $return_ticket_details['ticket_id'],
                  'cust_id' => $return_ticket_details['cust_id'],
                  'deliverer_id' => $return_ticket_details['operator_id'],
                  'text_msg' => $this->lang->line('Invoice'),
                  'attachment_url' =>  "ticket-invoices/".$pdf_name,
                  'cre_datetime' => $today,
                  'sender_id' => $return_ticket_details['operator_id'],
                  'type' => 'raise_invoice',
                  'file_type' => 'pdf',
                  'cust_name' => $return_ticket_details['cust_name'],
                  'deliverer_name' => $return_ticket_details['operator_name'],
                  'thumbnail' => 'NULL',
                  'ratings' => 'NULL',
                  'cat_id' => 281
                );
                $this->api->add_bus_chat_to_workroom($workroom_update);
              /* -------------------------Return ticket invoice----------------------- */
              
              /* -----------------Email Invoice to customer-------------------------- */
                $emailBody = $this->lang->line('Please download your ticket payment invoice.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
                $emailAddress = trim($user_details[0]['email_id']);
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to customer-------------------- */
              
              /* -------------------------Return Ticket pdf ------------------------- */
                $return_operator_details = $this->api->get_bus_operator_profile($return_ticket_details['operator_id']);
                if($return_operator_details["avatar_url"]=="" || $return_operator_details["avatar_url"] == "NULL"){
                  $operator_avtr = base_url("resources/no-image.jpg");
                }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
                require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                  QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
                   $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
                   $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
                  if(rename($img_src, $img_dest));
                $html ='
                <page format="100x100" orientation="L" style="font: arial;">';
                $html .= '
                <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                    <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                    <div style="margin-top:27px">
                      <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
                      <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
                    </div>
                  </div>
                  <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                      <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                      <br />
                      <h6>'.$this->lang->line("slider_heading1").'</h6>
                  </div>                
                </div>
                <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                  <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                    '.$this->lang->line("Ownward_Trip").'<br/>
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
                  <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                  <h6 style="margin-top:-10px;">
                  <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
                  <h6 style="margin-top:-8px;"> 
                  <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                  $ticket_seat_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);

                  $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                  foreach ($ticket_seat_details as $seat)
                  {
                    $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                    if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                      
                     $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                  }
                  $html .='</tbody></table></div></page>';            
                  $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
                  require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
                  $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
                  $html2pdf->pdf->SetDisplayMode('default');
                  $html2pdf->writeHTML($html);
                  $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
                  $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
                  rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
                  //whatsapp api send ticket
                  $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
                  $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
                  //echo json_encode($result); die();
              /* -------------------------Return Ticket pdf ------------------------- */
            
              /* ---------------------Email Ticket to customer----------------------- */
                $emailBody = $this->lang->line('Please download your e-ticket.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                $emailAddress = trim($user_details[0]['email_id']);
                $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                $fileName = $my_ticket_pdf;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* ---------------------Email Ticket to customer----------------------- */

              //Deduct Return Trip Commission From Operator Account
                $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

                $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //Deduct Return Trip Commission From Operator Account

              //Add Return Trip Commission To Gonagoo Account
                if($this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_gonagoo_master = array(
                    "gonagoo_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                }

                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
                $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "type" => 1,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "country_id" => trim($return_trip_operator_detail['country_id']),
                  "cat_id" => trim($return_ticket_details['cat_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              //Add Return Trip Commission To Gonagoo Account

              /* Return ticket commission invoice----------------------------------- */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
                $rate = round(trim($return_trip_commission),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Commission Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "commission_invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
              /* -------------------------------------------------------------------- */
              /* -----------------------Email Invoice to operator-------------------- */
                $emailBody = $this->lang->line('Commission Invoice');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Commission Invoice');
                $emailAddress = $return_trip_operator_detail['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to operator-------------------- */
            }
          //----------------------Return Trip Payment Calculation-----------------
          
        /********************* Payment Section ************************************/
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      redirect('user-panel-bus/seller-upcoming-trips');
    }
    public function seller_single_ticket_confirm_payment()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $ticket_price = $this->input->post('ticket_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);
      $today = date('Y-m-d h:i:s');
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $seat_details = $this->api->get_ticket_seat_details($ticket_id);
      //echo json_encode($ticket_details); die();

      $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
      $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
      //echo json_encode($country_id); die();

      $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
      //echo json_encode($gonagoo_commission_percentage); die();
      $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

      //echo json_encode($ownward_trip_commission); die();
      
      $total_amount_paid = $ticket_details['ticket_price'];
      $transfer_account_number = "NULL";
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = $this->input->post('stripeEmail', TRUE);
      $mobile_number = "NULL";    

      $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
      $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
      $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
      $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

      //Send Push Notifications
      //Get PN API Keys
      $api_key = $this->config->item('delivererAppGoogleKey');
      $device_details_customer = $this->api->get_user_device_details($cust_id);
      if(!is_null($device_details_customer)) {
        $arr_customer_fcm_ids = array();
        $arr_customer_apn_ids = array();
        foreach ($device_details_customer as $value) {
          if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
        }
        $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
        $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

        //Push Notification APN
        $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
        if(is_array($arr_customer_apn_ids)) { 
          $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
          $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
        }
      }

      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => "web",
        "complete_paid" => 1,
      );
      //echo json_encode($update_data); die();

      if($this->api->update_booking_details($update_data, $ticket_id)) {
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);

        //Update to workroom
        $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
        $workroom_update = array(
          'order_id' => $ticket_details['ticket_id'],
          'cust_id' => $ticket_details['cust_id'],
          'deliverer_id' => $ticket_details['operator_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $ticket_details['operator_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $ticket_details['cust_name'],
          'deliverer_name' => $ticket_details['operator_name'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        /*********************** Payment Section ********************************/
          //---------------------Insert counter payment record----------------------
            $data_counter_sale_payment = array(
              "cat_id" => $ticket_details['cat_id'],
              "service_id" => $ticket_details['ticket_id'],
              "cust_id" => $ticket_details['operator_id'],
              "cre_datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "currency_code" => trim($ticket_details['currency_sign']),
              "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
              "cat_name" => 'ticket_booking',
              "customer_email" => trim($seat_details[0]['email_id']),
              "customer_mobile" => trim($seat_details[0]['mobile']),
              "customer_country_id" => trim($seat_details[0]['country_id']),
            );
            $this->api->insert_counter_sale_payment($data_counter_sale_payment);
          //---------------------Insert counter payment record----------------------
          
          //Add Ownward Trip Payment to trip operator account + History
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Add Ownward Trip Payment to trip operator account + History

          /* COD ticket invoice-------------------------------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $user_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);
            if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details[0]['email_id']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details[0]['country_id']));
            // $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            // $user_city = $this->api->get_city_details(trim($user_details['city_id']));
            $user_state = "---";
            $user_city = "---";

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($user_details[0]['email_id'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph* /
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_Ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_Ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            $workroom_invoice_url = "ticket-invoices/".$pdf_name;
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  $workroom_invoice_url,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* -------------------------------------------------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */
          
          //Deduct Ownward Trip Commission From Operator Account
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Deduct Ownward Trip Commission From Operator Account
          
          //Add Ownward Trip Commission To Gonagoo Account
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //Add Ownward Trip Commission To Gonagoo Account           
          
          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        
        /*********************** Payment Section ********************************/
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      redirect('user-panel-bus/seller-upcoming-trips');
    }
    public function delete_bus_ticket_seller()
    {
      //echo json_encode($_POST); die();
      $ticket_id = $_POST['ticket_id'];
      $ticket_details = $this->api->get_trip_booking_details((int)$ticket_id);
      $today =date('Y-m-d H:i:s');
      
      if($ticket_details['complete_paid'] == '1') {
        $ticket_id = $_POST['ticket_id'];
        $t=strtotime($ticket_details['journey_date']." ".$ticket_details['trip_start_date_time'].":00")-strtotime(date('Y-m-d H:i:s'));
        $t = $t/3600;
        $cancellation_charge = 0; 
        $vehicle_type = $this->api->get_operator_bus_details($ticket_details['bus_id']);
        $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
        $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
        $cancellation = $this->api->get_cancellation_charges_by_country($ticket_details['operator_id'], $vehicle_type['vehical_type_id'],(int)$country_id);

        foreach($cancellation as $cancel) {
          if($cancel['bcr_min_hours'] < round($t) && $cancel['bcr_max_hours'] > round($t)) {
            $cancellation_charge = $cancel['bcr_cancellation'];
          }  
        }
        $charge = round(($ticket_details['ticket_price']/100) * $cancellation_charge,2);
        $return_amount = $ticket_details['ticket_price'] - $charge;

        if($ticket_details['payment_mode'] == 'online') {
          //Deduct refund amount from operator account
          $operator_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $account_balance = trim($operator_account_master_details['account_balance']) - trim($return_amount);
          $this->api->update_payment_in_customer_master($operator_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance, $today);
          $operator_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$operator_account_master_details['account_id'],
            "order_id" => (int)$ticket_details['ticket_id'],
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'cancel_refund',
            "amount" => trim($return_amount),
            "account_balance" => trim($operator_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
          
          //Add refund amount to customer account
          if($this->api->customer_account_master_details((int)$ticket_details['cust_id'], trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details((int)$ticket_details['cust_id'], trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => trim((int)$ticket_details['cust_id']),
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['cust_id']), trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_amount);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['cust_id']), $account_balance, date('Y-m-d H:i:s'));
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['cust_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_details['ticket_id'],
            "user_id" => (int)trim($ticket_details['cust_id']),
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'cancel_refund',
            "amount" => trim($return_amount),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        }

        $update_seat  = array('ticket_status' => 'cancelled', 'cancelled_by' => 'operator', 'cancelled_date' => $today);
        $seat_details = $this->api->get_std_id_of_seat_details($ticket_details['ticket_id']);
        foreach ($seat_details as $value) {
          $this->api->update_bus_booking_seat_details($update_seat , $value['seat_id']);
        }
        $update_ticket  = array('ticket_status' => 'cancelled', 'cancelled_by' => 'operator', 'cancelled_date' => $today, 'refund_amount' => $return_amount);
        if($this->api->update_booking_details($update_ticket, $ticket_details['ticket_id'])) {
          $user_details = $this->api->get_user_details($ticket_details['cust_id']);
          //Send cancellation SMS
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Your booking has been cancelled. Ticket ID - ').$ticket_id;
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          
          //Email Ticket cancellation confirmation Customer name, customer Email, Subject, Message
          if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($user_details['email1']));
            $username = $parts[0];
            $customer_name = $username;
          } else {
            $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
          }
          $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Ticket booking cancellation'), trim($message));

          //Send Push Notifications
          //Get PN API Keys
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($ticket_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Ticket cencellation'), 'type' => 'ticket-cencellation', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

            //Push Notification APN
            $msg_apn_customer =  array('title' => $this->lang->line('Ticket cencellation'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          //Update to workroom
          $message = $this->lang->line('Your ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line(' has been cancelled.');
          $workroom_update = array(
            'order_id' => $ticket_details['ticket_id'],
            'cust_id' => $ticket_details['cust_id'],
            'deliverer_id' => $ticket_details['operator_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $ticket_details['operator_id'],
            'type' => 'ticket_cancel',
            'file_type' => 'text',
            'cust_name' => $ticket_details['cust_name'],
            'deliverer_name' => $ticket_details['operator_name'],
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 281
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);

          echo "cancel_successfull";
        }
      } else {
        $ticket_id = $_POST['ticket_id'];
        $r = $this->api->delete_bus_ticket_booking_master((int)$ticket_id);
        if($r == "ok") {
          $r1 =  $this->api->delete_bus_ticket_booking_seat((int)$ticket_id);
          //Make return trip as ownward trip
          if($ticket_details['return_trip_id'] > 0) {
            $update_return_booking  = array('is_return' => 0, 'return_trip_id' => 0);
            $this->api->update_booking_details($update_return_booking, $ticket_details['return_trip_id']);
          }
          if($r1 == "ok") {
            $user_details = $this->api->get_user_details($ticket_details['cust_id']);
            //Send cancellation SMS
            $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
            $message = $this->lang->line('Your booking has been cancelled. Ticket ID - ').$ticket_id;
            $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
            
            //Email Ticket cancellation confirmation Customer name, customer Email, Subject, Message
            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }
            $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Ticket booking cancellation'), trim($message));

            //Send Push Notifications
            //Get PN API Keys
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($ticket_details['cust_id']);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Ticket cencellation'), 'type' => 'ticket-cencellation', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

              //Push Notification APN
              $msg_apn_customer =  array('title' => $this->lang->line('Ticket cencellation'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            //Update to workroom
            $message = $this->lang->line('Your ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line(' has been cancelled.');
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'ticket_cancel',
              'file_type' => 'text',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);

            echo "cancel_successfull";
          }
        } else {
          echo "delete_failed";
        }
      }
    }
    public function cancel_bus_trip_seller()
    {
      //echo json_encode($_POST); die();
      $unique_id = $_POST['unique_id'];
      $ticket_details = $this->api->get_ticket_master_booking_details_unique_id($unique_id);
      //echo json_encode($ticket_details); die();
      $seat_details = $this->api->get_ticket_seat_details_unique_id($unique_id);
      $today = date('Y-m-d H:i:s');
      $trip_id = "";
      $cancelled_date = "";
      //echo json_encode($seat_details); die();
      
      foreach ($ticket_details as $ticket) {
        $trip_id = $ticket['trip_id'];
        $unique_id = $ticket['unique_id'];
        $cancelled_date = $ticket['journey_date'];
        if($ticket['payment_mode'] == "cod" || $ticket['payment_mode'] == "NULL") {
          $ticket_update = array(
            'ticket_status' => "cancelled",
            'cancelled_by' => "operator",
            'cancelled_date' => $today,
            'refund_amount' => $ticket['ticket_price'],
          );
          $this->api->update_booking_details($ticket_update, $ticket['ticket_id']);
        } else if($ticket['payment_mode'] == "online" && $ticket['complete_paid'] == 1) {
          if(!$buyer_account =$this->api->customer_account_master_details($ticket['cust_id'], $ticket['currency_sign'])){
            $update = array(
              'user_id' => $ticket['cust_id'],
              'account_balance' => 0,
              'update_datetime' => $today,
              'operation_lock' =>  1,
              'currency_code' => $ticket['currency_sign'],
            );
            $this->api->insert_gonagoo_customer_record($update);
          }
          if(!$seller_account =$this->api->customer_account_master_details($ticket['operator_id'], $ticket['currency_sign'])){
            $update = array(
              'user_id' => $ticket['operator_id'],
              'account_balance' => 0,
              'update_datetime' => $today,
              'operation_lock' =>  1,
              'currency_code' => $ticket['currency_sign'],
            );
            $this->api->insert_gonagoo_customer_record($update);
          }
          $buyer_account =$this->api->customer_account_master_details($ticket['cust_id'], $ticket['currency_sign']);
          $seller_account =$this->api->customer_account_master_details($ticket['operator_id'], $ticket['currency_sign']);
          //echo json_encode($buyer_account); die();
          $seller_balance = trim($seller_account['account_balance']) - trim($ticket['ticket_price']);
          $this->api->update_payment_in_customer_master($seller_account['account_id'],$seller_account['user_id'],$seller_balance);

          $update_data_account_history = array(
            "account_id" => (int)$seller_account['account_id'],
            "order_id" => (int)$ticket['ticket_id'],
            "user_id" => (int)$seller_account['user_id'],
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'refund',
            "amount" => trim($ticket['ticket_price']),
            "account_balance" => trim($seller_balance),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
          
          $buyer_balance = trim($buyer_account['account_balance']) + trim($ticket['ticket_price']);
          $this->api->update_payment_in_customer_master($buyer_account['account_id'] , $buyer_account['user_id'] , $buyer_balance);

          $update_data_account_history = array(
            "account_id" => (int)$buyer_account['account_id'],
            "order_id" => (int)$ticket['ticket_id'],
            "user_id" => (int)$buyer_account['user_id'],
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'refund',
            "amount" => trim($ticket['ticket_price']),
            "account_balance" => trim($buyer_balance),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          $ticket_update = array(
            'ticket_status' => "cancelled",
            'cancelled_by' => "operator",
            'cancelled_date' => $today,
            'refund_amount' => $ticket['ticket_price']
          );
          $this->api->update_booking_details($ticket_update, $ticket['ticket_id']);

          if($ticket['booked_by'] = 'customer') {
            $message =$this->lang->line('Dear').$ticket['cust_name'].", ".$this->lang->line('Your Ticket Is Cancelled By Seller Due To Some Reason , For Ticket ID - ').$ticket['ticket_id'];

            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($ticket['cust_id']);
            if(!is_null($device_details_customer)){
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value){
                if($value['device_type'] == 1){ array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else { array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

              //Push Notification APN
              $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)){ 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }
            //Update to Workroom
            $workroom_update = array(
            'order_id' => $ticket['ticket_id'],
            'cust_id' => $ticket['cust_id'],
            'deliverer_id' => $ticket['operator_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $ticket['operator_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $ticket['cust_name'],
            'deliverer_name' => $ticket['operator_name'],
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 281
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);
          }
        }

        foreach($seat_details as $seat) {
          $update_data = array('cancelled_by' => "operator", 'ticket_status' => "cancelled" , 'cancelled_date' => $today);
          $this->api->update_bus_booking_seat_details($update_data , $seat['seat_id']);
          $country_code = $this->api->get_country_code_by_id($seat['country_id']);
          $message =$this->lang->line('Dear').$seat['firstname']." ".$seat['lastname']." ".$this->lang->line('Your Ticket Is Cancelled By Seller Due To Some Reason , For Ticket ID - ').$seat['ticket_id'];
          $this->api->sendSMS((int)$country_code.trim($seat['mobile']),$message);
        } 
        
        $cancelled_trip = array(
          'trip_id' => $trip_id,
          'cancelled_date' => date('m/d/Y'),
          'cancellation_time' => $today,
          'unique_id' => $unique_id,
        );
        if($this->api->insert_cancelled_trips($cancelled_trip)){
          $this->session->set_flashdata('success', $this->lang->line('Trip Cancelled Completed'));
        } else { $this->session->set_flashdata('error', $this->lang->line('Trip Cancelled Error')); }
      }
      redirect('user-panel-bus/seller-upcoming-trips');       
    }
    public function seller_ticket_orange_payment()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $payment_method = $this->input->post('payment_method', TRUE);
      $ticket_price = $this->input->post('ticket_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);

      $ticket_payment_id = 'gonagoo_'.time().'_'.$ticket_id;

      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
          array(
            "merchant_key" => "a8f8c61e", 
            "currency" => "OUV", 
            "order_id" => $ticket_payment_id, 
            "amount" => $ticket_price,  
            "return_url" => base_url('user-panel-bus/seller-orange-payment-process'), 
            "cancel_url" => base_url('user-panel-bus/seller-orange-payment-process'), 
            "notif_url" => base_url('test.php'), 
            "lang" => "en", 
            "reference" => $this->lang->line('Gonagoo - Service Payment') 
          )
        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      //echo json_encode($url_details); die();
      if(isset($url_details)) {
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('ticket_id', $ticket_id);
        $this->session->set_userdata('ticket_payment_id', $ticket_payment_id);
        $this->session->set_userdata('amount', $ticket_price);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-bus/seller-upcoming-trips');
      }
    }
    public function seller_orange_payment_process()
    {
      //echo json_encode($_SESSION); die();
      $session_token = $_SESSION['notif_token'];
      $ticket_id = $_SESSION['ticket_id'];
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $ticket_payment_id = $_SESSION['ticket_payment_id'];
      $amount = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "order_id" => $ticket_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      $transaction_id = $payment_details['txnid'];
      
      //echo json_encode($payment_details); die();

      //if($payment_details['status'] == 'SUCCESS' && $ticket_details['complete_paid'] == 0) {
      if(1){ 
        $user_details = $this->api->get_user_details($ticket_details['cust_id']);
        $cust_id = (int)$user_details['cust_id'];
        $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
        $country_id = $this->api->get_country_id_by_name($location_details['country_name']);

        $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
        //echo json_encode($gonagoo_commission_percentage); die();
        $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

        $total_amount_paid = $ticket_details['ticket_price'];
        $transfer_account_number = "NULL";
        $bank_name = "NULL";
        $account_holder_name = "NULL";
        $iban = "NULL";
        $email_address = 'NULL';
        $mobile_number = "NULL";  

        $seat_details = $this->api->get_ticket_seat_details($ticket_id); 
        
        $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);

        for ($i=0; $i < sizeof($seat_details); $i++) { 
          $country_code = $this->api->get_country_code_by_id($seat_details[$i]['country_id']);
          $this->api->sendSMS((int)$country_code.trim($seat_details[$i]['mobile']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($seat_details[$i]['mobile']), $message);
        }

        //echo json_encode($seat_details); die();
        //update payment status in booking
        $trip_update = array(
          "payment_method" => 'orange',
          "paid_amount" => $ticket_details['ticket_price'],
          "balance_amount" => 0,
          "transaction_id" => $transaction_id,
          "payment_datetime" => $today,
          "payment_mode" => "online",
          "payment_by" => "web",
          "complete_paid" => 1,
          'boarding_code' => $ticket_details['unique_id'].'_'.$ticket_id,
        );
        $this->api->update_booking_details($trip_update, $ticket_id);

        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        
        /************************** Payment Section ********************************/
          //---------------------Insert counter payment record----------------------
            $data_counter_sale_payment = array(
              "cat_id" => $ticket_details['cat_id'],
              "service_id" => $ticket_details['ticket_id'],
              "cust_id" => $ticket_details['operator_id'],
              "cre_datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "currency_code" => trim($ticket_details['currency_sign']),
              "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
              "cat_name" => 'ticket_booking',
              "customer_email" => trim($seat_details[0]['email_id']),
              "customer_mobile" => trim($seat_details[0]['mobile']),
              "customer_country_id" => trim($seat_details[0]['country_id']),
            );
            $this->api->insert_counter_sale_payment($data_counter_sale_payment);
          //---------------------Insert counter payment record----------------------

          $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
          if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
            $username = $parts[0];
            $operator_name = $username;
          } else {
            $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
          }

          $user_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);
          $customer_name = trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']);
          
          /* -----------------------Ownward ticket invoice---------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($seat_details[0]['country_id']));
            $user_state = '---';
            $user_city = '---';

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($seat_details[0]['email_id'])));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items

            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -----------------------Ownward ticket invoice---------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            //echo json_encode($user_details); die();
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

          //Add Ownward Trip Payment to trip operator account + History---------
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Add Ownward Trip Payment to trip operator account + History---------

          //Deduct Ownward Trip Commission From Operator Account---------------
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Deduct Ownward Trip Commission From Operator Account---------------

          //Add Ownward Trip Commission To Gonagoo Account---------------------
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'orange',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //Add Ownward Trip Commission To Gonagoo Account---------------------

          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* Ownward ticket commission invoice----------------------------------- */
          
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */

          //-----------------------Return Trip Payment Calculation-----------------
            if($ticket_details['return_trip_id'] > 0) {
              $return_ticket_details = $this->api->get_trip_booking_details($ticket_details['return_trip_id']);

             $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($return_ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
              for ($i=0; $i < sizeof($seat_details); $i++) { 
                $country_code = $this->api->get_country_code_by_id($seat_details[$i]['country_id']);
                $this->api->sendSMS((int)$country_code.trim($seat_details[$i]['mobile']), $message);
                $this->api_sms->send_sms_whatsapp((int)$country_code.trim($seat_details[$i]['mobile']), $message);
              }

              //Update order payment details
              $return_update_data = array(
                "payment_method" => trim($payment_method),
                "paid_amount" => trim($return_ticket_details['ticket_price']),
                "balance_amount" => 0,
                "transaction_id" => trim($transaction_id),
                "payment_datetime" => $today,
                "payment_mode" => "online",
                "payment_by" => "web",
                "complete_paid" => 1,
                'boarding_code' => $return_ticket_details['unique_id'].'_'.$return_ticket_details['ticket_id'],
              );
              $this->api->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);

              //Update Gonagoo commission for ticket in seat details table
              $seat_update_data = array(
                "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
              );
              $this->api->update_booking_seat_details($seat_update_data, $return_ticket_details['ticket_id']);

              //---------------------Insert counter payment record---------------------
                $data_counter_sale_payment = array(
                  "cat_id" => $return_ticket_details['cat_id'],
                  "service_id" => $return_ticket_details['ticket_id'],
                  "cust_id" => $return_ticket_details['operator_id'],
                  "cre_datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'payment',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
                  "cat_name" => 'ticket_booking',
                  "customer_email" => trim($seat_details[0]['email_id']),
                  "customer_mobile" => trim($seat_details[0]['mobile']),
                  "customer_country_id" => trim($seat_details[0]['country_id']),
                );
                $this->api->insert_counter_sale_payment($data_counter_sale_payment);
              //---------------------Insert counter payment record---------------------
              
              /* ------------------------Return ticket invoice-------------------- */
                $return_trip_operator_detail = $this->api->get_user_details($return_ticket_details['operator_id']);

                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($seat_details[0]['email_id'])));
                
                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
                $rate = round(trim($return_ticket_details['ticket_price']),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                $workroom_invoice_url = "ticket-invoices/".$pdf_name;
              /* ------------------------Return ticket invoice-------------------- */
              
              /* -----------------Email Invoice to customer-------------------------- */
                $emailBody = $this->lang->line('Please download your ticket payment invoice.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
                $emailAddress = trim($user_details[0]['email_id']);
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to customer-------------------- */
              
              /* -------------------------Return Ticket pdf ------------------------- */
                $return_operator_details = $this->api->get_bus_operator_profile($return_ticket_details['operator_id']);
                if($return_operator_details["avatar_url"]=="" || $return_operator_details["avatar_url"] == "NULL"){
                  $operator_avtr = base_url("resources/no-image.jpg");
                }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
                require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                  QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
                   $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
                   $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
                  if(rename($img_src, $img_dest));
                $html ='
                <page format="100x100" orientation="L" style="font: arial;">';
                $html .= '
                <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                    <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                    <div style="margin-top:27px">
                      <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
                      <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
                    </div>
                  </div>
                  <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                      <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                      <br />
                      <h6>'.$this->lang->line("slider_heading1").'</h6>
                  </div>                
                </div>
                <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                  <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                    '.$this->lang->line("Ownward_Trip").'<br/>
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
                  <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                  <h6 style="margin-top:-10px;">
                  <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
                  <h6 style="margin-top:-8px;"> 
                  <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                  $ticket_seat_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);
    
                  $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                  foreach ($ticket_seat_details as $seat)
                  {
                    $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                    if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                      
                     $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                  }
                $html .='</tbody></table></div></page>';            
                $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
                require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
                $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
                $html2pdf->pdf->SetDisplayMode('default');
                $html2pdf->writeHTML($html);
                $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
                $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
                rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
                //whatsapp api send ticket
                $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
                $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
                //echo json_encode($result); die();
              /* -------------------------Return Ticket pdf ------------------------- */
              
              /* ---------------------Email Ticket to customer----------------------- */
                $emailBody = $this->lang->line('Please download your e-ticket.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                $emailAddress = trim($user_details[0]['email_id']);
                $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                $fileName = $my_ticket_pdf;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* ---------------------Email Ticket to customer----------------------- */

              //Add Return Trip Payment to trip operator account + History
                if($this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_customer_master = array(
                    "user_id" => trim($return_ticket_details['operator_id']),
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                }
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //Add Return Trip Payment to trip operator account + History

              //Deduct Return Trip Commission From Operator Account-----------------
                $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //Deduct Return Trip Commission From Operator Account-----------------
              
              //Add Return Trip Commission To Gonagoo Account-----------------------
                if($this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_gonagoo_master = array(
                    "gonagoo_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                }

                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
                $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "type" => 1,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "country_id" => trim($return_trip_operator_detail['country_id']),
                  "cat_id" => trim($return_ticket_details['cat_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              //Add Return Trip Commission To Gonagoo Account-----------------------

              /* Return ticket commission invoice--------------------------------- */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
                $rate = round(trim($return_trip_commission),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Commission Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "commission_invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
              /* Return ticket commission invoice--------------------------------- */
              /* -----------------------Email Invoice to operator-------------------- */
                $emailBody = $this->lang->line('Commission Invoice');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Commission Invoice');
                $emailAddress = $return_trip_operator_detail['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to operator-------------------- */
            }
          //-----------------------Return Trip Payment Calculation-----------------
        /*************************** Payment Section ********************************/
        //unset all session data
        $this->session->unset_userdata('notif_token');
        $this->session->unset_userdata('ticket_id');
        $this->session->unset_userdata('ticket_payment_id');
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('pay_token');
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else {
        //unset all session data
        $this->session->unset_userdata('notif_token');
        $this->session->unset_userdata('ticket_id');
        $this->session->unset_userdata('ticket_payment_id');
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('pay_token');
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
      }
      redirect('user-panel-bus/seller-upcoming-trips');
    }
    public function seller_single_ticket_orange_payment()
    {
      //echo json_encode($_POST); die();
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $payment_method = $this->input->post('payment_method', TRUE);
      $ticket_price = $this->input->post('ticket_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);

      $ticket_payment_id = 'gonagoo_'.time().'_'.$ticket_id;

      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "merchant_key" => "a8f8c61e", 
                            "currency" => "OUV", 
                            "order_id" => $ticket_payment_id, 
                            "amount" => $ticket_price,  
                            "return_url" => base_url('user-panel-bus/seller-single-orange-payment-process'), 
                            "cancel_url" => base_url('user-panel-bus/seller-single-orange-payment-process'), 
                            "notif_url" => base_url('test.php'), 
                            "lang" => "en", 
                            "reference" => $this->lang->line('Gonagoo - Service Payment') 
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      //echo json_encode($url_details); die();
      if(isset($url_details)) {
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('ticket_id', $ticket_id);
        $this->session->set_userdata('ticket_payment_id', $ticket_payment_id);
        $this->session->set_userdata('amount', $ticket_price);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-bus/seller-upcoming-trips');
      }
    }
    public function seller_single_orange_payment_process()
    {
      //echo json_encode($_SESSION); die();
      $session_token = $_SESSION['notif_token'];
      $ticket_id = $_SESSION['ticket_id'];
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $ticket_payment_id = $_SESSION['ticket_payment_id'];
      $amount = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "order_id" => $ticket_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      $transaction_id = $payment_details['txnid'];
      //echo json_encode($payment_details); die();

      //if($payment_details['status'] == 'SUCCESS' && $ticket_details['complete_paid'] == 0) {
      if(1){  
        $user_details = $this->api->get_user_details($ticket_details['cust_id']);
        $cust_id = (int)$user_details['cust_id'];
        $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
        $country_id = $this->api->get_country_id_by_name($location_details['country_name']);

        $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
        //echo json_encode($gonagoo_commission_percentage); die();
        $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

        $total_amount_paid = $ticket_details['ticket_price'];
        $transfer_account_number = "NULL";
        $bank_name = "NULL";
        $account_holder_name = "NULL";
        $iban = "NULL";
        $email_address = 'NULL';
        $mobile_number = "NULL";  

        $seat_details = $this->api->get_ticket_seat_details($ticket_id); 
        
        $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);

        for ($i=0; $i < sizeof($seat_details); $i++) { 
          $country_code = $this->api->get_country_code_by_id($seat_details[$i]['country_id']);
          $this->api->sendSMS((int)$country_code.trim($seat_details[$i]['mobile']), $message);
          $this->api_sms->send_sms_whatsapp((int)$country_code.trim($seat_details[$i]['mobile']), $message);
        }

        //echo json_encode($seat_details); die();
        //update payment status in booking
        $trip_update = array(
          "payment_method" => 'orange',
          "paid_amount" => $ticket_details['ticket_price'],
          "balance_amount" => 0,
          "transaction_id" => $transaction_id,
          "payment_datetime" => $today,
          "payment_mode" => "online",
          "payment_by" => "web",
          "complete_paid" => 1,
          'boarding_code' => $ticket_details['unique_id'].'_'.$ticket_id,
        );
        $this->api->update_booking_details($trip_update, $ticket_id);

        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        
        /************************ Payment Section ****************************************/
          //Inser counter payment record--------------------------------------
            $data_counter_sale_payment = array(
              "cat_id" => $ticket_details['cat_id'],
              "service_id" => $ticket_details['ticket_id'],
              "cust_id" => $ticket_details['operator_id'],
              "cre_datetime" => $today,
              "type" => 1,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "currency_code" => trim($ticket_details['currency_sign']),
              "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
              "cat_name" => 'ticket_booking',
              "customer_email" => trim($seat_details[0]['email_id']),
              "customer_mobile" => trim($seat_details[0]['mobile']),
              "customer_country_id" => trim($seat_details[0]['country_id']),
            );
            $this->api->insert_counter_sale_payment($data_counter_sale_payment);
          //------------------------------------------------------------------

          $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
          if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
            $username = $parts[0];
            $operator_name = $username;
          } else {
            $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
          }
          $user_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);
          $customer_name = trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']);
          
          /* Ownward ticket invoice-------------------------------------------- */
            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($seat_details[0]['country_id']));
            $user_state = '---';
            $user_city = '---';

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($seat_details[0]['email_id'])));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items

            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* ------------------------------------------------------------------ */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = trim($user_details[0]['email_id']);
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

         //Add Ownward Trip Payment to trip operator account + History---------
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Add Ownward Trip Payment to trip operator account + History---------

          //Deduct Ownward Trip Commission From Operator Account---------------
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //Deduct Ownward Trip Commission From Operator Account---------------

          //Add Ownward Trip Commission To Gonagoo Account---------------------
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'orange',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //Add Ownward Trip Commission To Gonagoo Account---------------------

          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        /*************************************** Payment Section ****************************************/
        //unset all session data
        $this->session->unset_userdata('notif_token');
        $this->session->unset_userdata('ticket_id');
        $this->session->unset_userdata('ticket_payment_id');
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('pay_token');
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else {
        //unset all session data
        $this->session->unset_userdata('notif_token');
        $this->session->unset_userdata('ticket_id');
        $this->session->unset_userdata('ticket_payment_id');
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('pay_token');
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
      }
      redirect('user-panel-bus/seller-upcoming-trips');
    }
    public function seller_payment_by_mtn()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $payment_method = 'mtn';
      $phone_no = $this->input->post('phone_no', TRUE);
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      $today = date('Y-m-d h:i:s');
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $ticket_price = $this->input->post('ticket_price', TRUE); $ticket_price = (int) $ticket_price;
      $currency_sign = $ticket_details['currency_sign'];

      $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
      $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
      $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
      $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
     
      //MTN Payment Gateway
      /*$url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$ticket_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      $contents = curl_exec($ch);
      $mtn_pay_res = json_decode($contents, TRUE);*/
      //var_dump($mtn_pay_res); die();

      $transaction_id = 123;
      //echo json_encode($ticket_details); die();

      $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
      $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
      //echo json_encode($country_id); die();
      $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
      //echo json_encode($gonagoo_commission_percentage); die();
      $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

      $total_amount_paid = $ticket_price;
      $transfer_account_number = $phone_no;
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = "NULL";
      $mobile_number = $phone_no;    
      
      $payment_data = array();
      //if($mtn_pay_res['StatusCode'] === "01"){
      if(1) {
        $update_data = array(
          "payment_method" => trim($payment_method),
          "paid_amount" => trim($ticket_details['ticket_price']),
          "balance_amount" => 0,
          "transaction_id" => trim($transaction_id),
          "payment_datetime" => $today,
          "payment_mode" => "online",
          "payment_by" => "web",
          "complete_paid" => 1,
        );
        if($this->api->update_booking_details($update_data, $ticket_id)){

          //Update Gonagoo commission for ticket in seat details table
          $seat_update_data = array(
            "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
          );
          $this->api->update_booking_seat_details($seat_update_data, $ticket_id);

          //************************Update to workroom*************************************
            $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'payment',
              'file_type' => 'text',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          //************************Update to workroom*************************************
          /************************ Payment Section **********************************/
            //Add Ownward Trip Payment to trip operator account + History
              if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
                $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              } else {
                $insert_data_customer_master = array(
                  "user_id" => trim($ticket_details['operator_id']),
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($ticket_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$ticket_id,
                "user_id" => (int)trim($ticket_details['operator_id']),
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($ticket_details['ticket_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //Add Ownward Trip Payment to trip operator account + History

            /* ------------------------Ownward ticket invoice-------------------------- */
              $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
              if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
                $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
                $username = $parts[0];
                $operator_name = $username;
              } else {
                $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
              }

              if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
                $parts = explode("@", trim($user_details['email1']));
                $username = $parts[0];
                $customer_name = $username;
              } else {
                $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
              }

              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($ticket_details['ticket_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
              $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
              $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

              $user_country = $this->api->get_country_details(trim($user_details['country_id']));
              $user_state = $this->api->get_state_details(trim($user_details['state_id']));
              $user_city = $this->api->get_city_details(trim($user_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

              //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($ticket_details['ticket_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "ticket-invoices/".$pdf_name,
              );
              $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
              $workroom_invoice_url = "ticket-invoices/".$pdf_name;
              //Payment Invoice Update to workroom
              $workroom_update = array(
                'order_id' => $ticket_details['ticket_id'],
                'cust_id' => $ticket_details['cust_id'],
                'deliverer_id' => $ticket_details['operator_id'],
                'text_msg' => $this->lang->line('Invoice'),
                'attachment_url' =>  $workroom_invoice_url,
                'cre_datetime' => $today,
                'sender_id' => $ticket_details['operator_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $ticket_details['cust_name'],
                'deliverer_name' => $ticket_details['operator_name'],
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 281
              );
              $this->api->add_bus_chat_to_workroom($workroom_update);
            /* ------------------------Ownward ticket invoice-------------------------- */
            
            $user_detailss = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);
            
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your ticket payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
              $emailAddress = trim($user_detailss[0]['email_id']);
              $fileToAttach = "resources/ticket-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */
            
            /* ------------------------Generate Ticket pdf ------------------------ */
              $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
                 $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
                 $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
                if(rename($img_src, $img_dest));
              $html ='
              <page format="100x100" orientation="L" style="font: arial;">';
              $html .= '
              <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                    <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                    <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                    <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                    <br />
                    <h6>'.$this->lang->line("slider_heading1").'</h6>
                </div>                
              </div>
              <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line("Ownward_Trip").'<br/>
                <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
                <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                <h6 style="margin-top:-10px;">
                <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
                <h6 style="margin-top:-8px;"> 
                <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

                $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                foreach ($ticket_seat_details as $seat)
                {
                  $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                  if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                    
                   $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                }
              $html .='</tbody></table></div></page>';            
              $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
              $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
              rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
              //whatsapp api send ticket
              $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
              $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
              //echo json_encode($result); die();
            /* ------------------------Generate Ticket pdf ------------------------ */
            
            /* ---------------------Email Ticket to customer----------------------- */
                $emailBody = $this->lang->line('Please download your e-ticket.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                $emailAddress = trim($user_detailss[0]['email_id']);
                $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                $fileName = $my_ticket_pdf;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */

            //Deduct Ownward Trip Commission From Operator Account
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$ticket_id,
                "user_id" => (int)trim($ticket_details['operator_id']),
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'ticket_commission',
                "amount" => trim($ownward_trip_commission),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //Deduct Ownward Trip Commission From Operator Account

            //Add Ownward Trip Commission To Gonagoo Account
              if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($ticket_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$ticket_id,
                "user_id" => (int)trim($ticket_details['operator_id']),
                "type" => 1,
                "transaction_type" => 'ticket_commission',
                "amount" => trim($ownward_trip_commission),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($ticket_details['currency_sign']),
                "country_id" => trim($ownward_trip_operator_detail['country_id']),
                "cat_id" => trim($ticket_details['cat_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //Add Ownward Trip Commission To Gonagoo Account
            
            /* -------------------Ownward ticket commission invoice-------------------- */
              if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
                $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
                $username = $parts[0];
                $operator_name = $username;
              } else {
                $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
              }

              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($ticket_details['ticket_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
              $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
              $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($ownward_trip_commission),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "ticket-invoices/".$pdf_name,
              );
              $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            /* -------------------Ownward ticket commission invoice----------------- */
            
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $ownward_trip_operator_detail['email1'];
              $fileToAttach = "resources/ticket-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */

            //***********************Return Trip Payment Calculation****************** 
              if($ticket_details['return_trip_id'] > 0) {
                $return_ticket_details = $this->api->get_trip_booking_details($ticket_details['return_trip_id']);

                $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
                $message = $this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id'];
                $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
                $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

                $return_update_data = array(
                  "payment_method" => trim($payment_method),
                  "paid_amount" => trim($return_ticket_details['ticket_price']),
                  "balance_amount" => 0,
                  "transaction_id" => trim($transaction_id),
                  "payment_datetime" => $today,
                  "payment_mode" => "online",
                  "payment_by" => "web",
                  "complete_paid" => 1,
                );
                $this->api->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);

                //Update Gonagoo commission for ticket in seat details table
                $seat_update_data = array(
                  "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
                );
                $this->api->update_booking_seat_details($seat_update_data, $return_ticket_details['ticket_id']);

                //-------------------Update to Workroom---------------------------------
                  $workroom_update = array(
                    'order_id' => $return_ticket_details['ticket_id'],
                    'cust_id' => $return_ticket_details['cust_id'],
                    'deliverer_id' => $return_ticket_details['operator_id'],
                    'text_msg' => $message,
                    'attachment_url' =>  'NULL',
                    'cre_datetime' => $today,
                    'sender_id' => $return_ticket_details['operator_id'],
                    'type' => 'payment',
                    'file_type' => 'text',
                    'cust_name' => $return_ticket_details['cust_name'],
                    'deliverer_name' => $return_ticket_details['operator_name'],
                    'thumbnail' => 'NULL',
                    'ratings' => 'NULL',
                    'cat_id' => 281
                  );
                  $this->api->add_bus_chat_to_workroom($workroom_update);
                //-------------------Update to Workroom---------------------------------

                //Add Return Trip Payment to trip operator account + History
                  if($this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
                    $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                  } else {
                    $insert_data_customer_master = array(
                      "user_id" => trim($return_ticket_details['operator_id']),
                      "account_balance" => 0,
                      "update_datetime" => $today,
                      "operation_lock" => 1,
                      "currency_code" => trim($return_ticket_details['currency_sign']),
                    );
                    $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                    $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                  }
                  $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
                  $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                  $update_data_account_history = array(
                    "account_id" => (int)$customer_account_master_details['account_id'],
                    "order_id" => (int)trim($return_ticket_details['ticket_id']),
                    "user_id" => (int)trim($return_ticket_details['operator_id']),
                    "datetime" => $today,
                    "type" => 1,
                    "transaction_type" => 'add',
                    "amount" => trim($return_ticket_details['ticket_price']),
                    "account_balance" => trim($customer_account_master_details['account_balance']),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                    "cat_id" => 281
                  );
                  $this->api->insert_payment_in_account_history($update_data_account_history);
                //Add Return Trip Payment to trip operator account + History

                /* ----------------------Return ticket invoice----------------------- */
                  $return_trip_operator_detail = $this->api->get_user_details($return_ticket_details['operator_id']);

                  $country_code = $this->api->get_country_code_by_id($return_trip_operator_detail['country_id']);
                  $message = $this->lang->line('Payment recieved against ticket ID - ').$return_ticket_details['ticket_id'];
                  $this->api->sendSMS((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);
                  
                  if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                    $parts = explode("@", trim($return_trip_operator_detail['email1']));
                    $username = $parts[0];
                    $operator_name = $username;
                  } else {
                    $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                  }

                  if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
                    $parts = explode("@", trim($user_details['email1']));
                    $username = $parts[0];
                    $customer_name = $username;
                  } else {
                    $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
                  }

                  $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                  require_once($phpinvoice);
                  //Language configuration for invoice
                  $lang = $this->input->post('lang', TRUE);
                  if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                  else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                  else { $invoice_lang = "englishApi_lang"; }
                  //Invoice Configuration
                  $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                  $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                  $invoice->setColor("#000");
                  $invoice->setType("");
                  $invoice->setReference($return_ticket_details['ticket_id']);
                  $invoice->setDate(date('M dS ,Y',time()));

                  $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                  $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                  $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                  $user_country = $this->api->get_country_details(trim($user_details['country_id']));
                  $user_state = $this->api->get_state_details(trim($user_details['state_id']));
                  $user_city = $this->api->get_city_details(trim($user_details['city_id']));

                  $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                  $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                  $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                  $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                  $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

                  //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                  
                  $eol = PHP_EOL;
                  /* Adding Items in table */
                  $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
                  $rate = round(trim($return_ticket_details['ticket_price']),2);
                  $total = $rate;
                  $payment_datetime = substr($today, 0, 10);
                  //set items
                  $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                  /* Add totals */
                  $invoice->addTotal($this->lang->line('sub_total'),$total);
                  $invoice->addTotal($this->lang->line('taxes'),'0');
                  $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                  /* Set badge */ 
                  $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
                  /* Add title */
                  $invoice->addTitle($this->lang->line('tnc'));
                  /* Add Paragraph */
                  $invoice->addParagraph($gonagoo_address['terms']);
                  /* Add title */
                  $invoice->addTitle($this->lang->line('payment_dtls'));
                  /* Add Paragraph */
                  $invoice->addParagraph($gonagoo_address['payment_details']);
                  /* Add title */
                  $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                  /* Add Paragraph */
                  $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                  /* Set footer note */
                  $invoice->setFooternote($gonagoo_address['company_name']);
                  /* Render */
                  $invoice->render($return_ticket_details['ticket_id'].'_return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                  //Update File path
                  $pdf_name = $return_ticket_details['ticket_id'].'_return_trip.pdf';
                  //Move file to upload folder
                  rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                  //Update Order Invoice
                  $update_data = array(
                    "invoice_url" => "ticket-invoices/".$pdf_name,
                  );
                  $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                  $workroom_invoice_url = "ticket-invoices/".$pdf_name;
                  //Payment Invoice Update to workroom
                  $workroom_update = array(
                    'order_id' => $return_ticket_details['ticket_id'],
                    'cust_id' => $return_ticket_details['cust_id'],
                    'deliverer_id' => $return_ticket_details['operator_id'],
                    'text_msg' => $this->lang->line('Invoice'),
                    'attachment_url' =>  $workroom_invoice_url,
                    'cre_datetime' => $today,
                    'sender_id' => $return_ticket_details['operator_id'],
                    'type' => 'raise_invoice',
                    'file_type' => 'pdf',
                    'cust_name' => $return_ticket_details['cust_name'],
                    'deliverer_name' => $return_ticket_details['operator_name'],
                    'thumbnail' => 'NULL',
                    'ratings' => 'NULL',
                    'cat_id' => 281
                  );
                  $this->api->add_bus_chat_to_workroom($workroom_update);
                /* ----------------------Return ticket invoice----------------------- */
                
                /* -----------------Email Invoice to customer-------------------------- */
                  $emailBody = $this->lang->line('Please download your ticket payment invoice.');
                  $gonagooAddress = $gonagoo_address['company_name'];
                  $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
                  $emailAddress = trim($user_detailss[0]['email_id']);
                  $fileToAttach = "resources/ticket-invoices/$pdf_name";
                  $fileName = $pdf_name;
                  $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
                /* -----------------------Email Invoice to customer-------------------- */
                
                  /* -------------------------Return Ticket pdf ------------------------- */
                    $return_operator_details = $this->api->get_bus_operator_profile($return_ticket_details['operator_id']);
                    if($return_operator_details["avatar_url"]=="" || $return_operator_details["avatar_url"] == "NULL"){
                      $operator_avtr = base_url("resources/no-image.jpg");
                    }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
                    require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                      QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
                       $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
                       $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
                      if(rename($img_src, $img_dest));
                    $html ='
                    <page format="100x100" orientation="L" style="font: arial;">';
                    $html .= '
                    <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                      <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                        <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                        <div style="margin-top:27px">
                          <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
                          <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
                        </div>
                      </div>
                      <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                          <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                          <br />
                          <h6>'.$this->lang->line("slider_heading1").'</h6>
                      </div>                
                    </div>
                    <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                      <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                        '.$this->lang->line("Ownward_Trip").'<br/>
                      <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
                      <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                      <h6 style="margin-top:-10px;">
                      <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
                      <h6 style="margin-top:-8px;">
                      <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
                      <h6 style="margin-top:-8px;">
                      <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
                      <h6 style="margin-top:-8px;"> 
                      <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
                      <h6 style="margin-top:-8px;">
                      <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                      $ticket_seat_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);
        
                      $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                      foreach ($ticket_seat_details as $seat)
                      {
                        $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                        if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                          
                         $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                      }
                    $html .='</tbody></table></div></page>';            
                    $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
                    require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
                    $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
                    $html2pdf->pdf->SetDisplayMode('default');
                    $html2pdf->writeHTML($html);
                    $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
                    $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
                    rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
                    //whatsapp api send ticket
                    $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
                    $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
                    //echo json_encode($result); die();
                  /* -------------------------Return Ticket pdf ------------------------- */
                
                /* ---------------------Email Ticket to customer----------------------- */
                  $emailBody = $this->lang->line('Please download your e-ticket.');
                  $gonagooAddress = $gonagoo_address['company_name'];
                  $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                  $emailAddress = trim($user_detailss[0]['email_id']);
                  $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                  $fileName = $my_ticket_pdf;
                  $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
                /* ---------------------Email Ticket to customer----------------------- */

                //Deduct Return Trip Commission From Operator Account
                  $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

                  $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
                  $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                  $update_data_account_history = array(
                    "account_id" => (int)$customer_account_master_details['account_id'],
                    "order_id" => (int)trim($return_ticket_details['ticket_id']),
                    "user_id" => (int)trim($return_ticket_details['operator_id']),
                    "datetime" => $today,
                    "type" => 0,
                    "transaction_type" => 'ticket_commission',
                    "amount" => trim($return_trip_commission),
                    "account_balance" => trim($customer_account_master_details['account_balance']),
                    "withdraw_request_id" => 0,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                    "cat_id" => 281
                  );
                  $this->api->insert_payment_in_account_history($update_data_account_history);
                //Deduct Return Trip Commission From Operator Account

                //Add Return Trip Commission To Gonagoo Account
                  if($this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
                    $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                  } else {
                    $insert_data_gonagoo_master = array(
                      "gonagoo_balance" => 0,
                      "update_datetime" => $today,
                      "operation_lock" => 1,
                      "currency_code" => trim($return_ticket_details['currency_sign']),
                    );
                    $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                    $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                  }

                  $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
                  $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

                  $update_data_gonagoo_history = array(
                    "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                    "order_id" => (int)trim($return_ticket_details['ticket_id']),
                    "user_id" => (int)trim($return_ticket_details['operator_id']),
                    "type" => 1,
                    "transaction_type" => 'ticket_commission',
                    "amount" => trim($return_trip_commission),
                    "datetime" => $today,
                    "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                    "transfer_account_type" => trim($payment_method),
                    "transfer_account_number" => trim($transfer_account_number),
                    "bank_name" => trim($bank_name),
                    "account_holder_name" => trim($account_holder_name),
                    "iban" => trim($iban),
                    "email_address" => trim($email_address),
                    "mobile_number" => trim($mobile_number),
                    "transaction_id" => trim($transaction_id),
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                    "country_id" => trim($return_trip_operator_detail['country_id']),
                    "cat_id" => trim($return_ticket_details['cat_id']),
                  );
                  $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
                //Add Return Trip Commission To Gonagoo Account

                /* ------------------Return ticket commission invoice------------------- */
                  if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                    $parts = explode("@", trim($return_trip_operator_detail['email1']));
                    $username = $parts[0];
                    $operator_name = $username;
                  } else {
                    $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                  }

                  $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                  require_once($phpinvoice);
                  //Language configuration for invoice
                  $lang = $this->input->post('lang', TRUE);
                  if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                  else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                  else { $invoice_lang = "englishApi_lang"; }
                  //Invoice Configuration
                  $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                  $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                  $invoice->setColor("#000");
                  $invoice->setType("");
                  $invoice->setReference($return_ticket_details['ticket_id']);
                  $invoice->setDate(date('M dS ,Y',time()));

                  $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                  $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                  $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                  $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                  $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                  $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                  $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                  
                  $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                  $eol = PHP_EOL;
                  /* Adding Items in table */
                  $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
                  $rate = round(trim($return_trip_commission),2);
                  $total = $rate;
                  $payment_datetime = substr($today, 0, 10);
                  //set items
                  $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                  /* Add totals */
                  $invoice->addTotal($this->lang->line('sub_total'),$total);
                  $invoice->addTotal($this->lang->line('taxes'),'0');
                  $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                  /* Set badge */ 
                  $invoice->addBadge($this->lang->line('Commission Paid'));
                  /* Add title */
                  $invoice->addTitle($this->lang->line('tnc'));
                  /* Add Paragraph */
                  $invoice->addParagraph($gonagoo_address['terms']);
                  /* Add title */
                  $invoice->addTitle($this->lang->line('payment_dtls'));
                  /* Add Paragraph */
                  $invoice->addParagraph($gonagoo_address['payment_details']);
                  /* Add title */
                  $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                  /* Add Paragraph */
                  $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                  /* Set footer note */
                  $invoice->setFooternote($gonagoo_address['company_name']);
                  /* Render */
                  $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                  //Update File path
                  $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
                  //Move file to upload folder
                  rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                  //Update Order Invoice
                  $update_data = array(
                    "commission_invoice_url" => "ticket-invoices/".$pdf_name,
                  );
                  $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                /* ------------------Return ticket commission invoice------------------- */
                
                /* -----------------------Email Invoice to operator-------------------- */
                  $emailBody = $this->lang->line('Commission Invoice');
                  $gonagooAddress = $gonagoo_address['company_name'];
                  $emailSubject = $this->lang->line('Commission Invoice');
                  $emailAddress = $return_trip_operator_detail['email1'];
                  $fileToAttach = "resources/ticket-invoices/$pdf_name";
                  $fileName = $pdf_name;
                  $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
                /* -----------------------Email Invoice to operator-------------------- */
              }
            //***********************Return Trip Payment Calculation******************
          /************************ Payment Section **********************************/
          $this->session->set_flashdata('success', 'Payment successfully done!');
        } else { $this->session->set_flashdata('error', 'Payment failed! Try again...'); }
      } else {  $this->session->set_flashdata('error', 'Payment failed! Try again...'); }
      $location = base_url('user-panel-bus/seller-upcoming-trips');
      echo '<script>top.window.location = "'.$location.'"</script>';
    }
    public function seller_payment_by_mtn_single()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $payment_method = 'mtn';
      $phone_no = $this->input->post('phone_no', TRUE);
      if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
      $today = date('Y-m-d h:i:s');
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $ticket_price = $this->input->post('ticket_price', TRUE); $ticket_price = (int) $ticket_price;
      $currency_sign = $ticket_details['currency_sign'];

      $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
      $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
      $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
      $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
      
      //MTN Payment Gateway
      /*$url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$ticket_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      $contents = curl_exec($ch);
      $mtn_pay_res = json_decode($contents, TRUE);*/
      //var_dump($mtn_pay_res); die();

      $transaction_id = 123;
      //echo json_encode($ticket_details); die();

      $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
      $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
      //echo json_encode($country_id); die();
      $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
      //echo json_encode($gonagoo_commission_percentage); die();
      $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

      $total_amount_paid = $ticket_price;
      $transfer_account_number = $phone_no;
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = "NULL";
      $mobile_number = $phone_no;    
      
      $payment_data = array();
      //if($mtn_pay_res['StatusCode'] === "01"){
      if(1) {
        $update_data = array(
          "payment_method" => trim($payment_method),
          "paid_amount" => trim($ticket_details['ticket_price']),
          "balance_amount" => 0,
          "transaction_id" => trim($transaction_id),
          "payment_datetime" => $today,
          "payment_mode" => "online",
          "payment_by" => "web",
          "complete_paid" => 1,
        );
        if($this->api->update_booking_details($update_data, $ticket_id)){
          //Update Gonagoo commission for ticket in seat details table
          $seat_update_data = array(
            "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
          );
          $this->api->update_booking_seat_details($seat_update_data, $ticket_id);

          //Update to workroom
          $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
          $workroom_update = array(
            'order_id' => $ticket_details['ticket_id'],
            'cust_id' => $ticket_details['cust_id'],
            'deliverer_id' => $ticket_details['operator_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $ticket_details['operator_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $ticket_details['cust_name'],
            'deliverer_name' => $ticket_details['operator_name'],
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 281
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);
          /********************** Payment Section **************************************/
            //Add Ownward Trip Payment to trip operator account + History
              if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
                $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              } else {
                $insert_data_customer_master = array(
                  "user_id" => trim($ticket_details['operator_id']),
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($ticket_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$ticket_id,
                "user_id" => (int)trim($ticket_details['operator_id']),
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($ticket_details['ticket_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //Add Ownward Trip Payment to trip operator account + History

            /* -----------------------Ownward ticket invoice----------------------- */
              $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
              if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
                $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
                $username = $parts[0];
                $operator_name = $username;
              } else {
                $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
              }

              if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
                $parts = explode("@", trim($user_details['email1']));
                $username = $parts[0];
                $customer_name = $username;
              } else {
                $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
              }

              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($ticket_details['ticket_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
              $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
              $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

              $user_country = $this->api->get_country_details(trim($user_details['country_id']));
              $user_state = $this->api->get_state_details(trim($user_details['state_id']));
              $user_city = $this->api->get_city_details(trim($user_details['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

              $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

              //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
              $rate = round(trim($ticket_details['ticket_price']),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "invoice_url" => "ticket-invoices/".$pdf_name,
              );
              $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
              $workroom_invoice_url = "ticket-invoices/".$pdf_name;
              //Payment Invoice Update to workroom
              $workroom_update = array(
                'order_id' => $ticket_details['ticket_id'],
                'cust_id' => $ticket_details['cust_id'],
                'deliverer_id' => $ticket_details['operator_id'],
                'text_msg' => 'NULL',
                'attachment_url' =>  $workroom_invoice_url,
                'cre_datetime' => $today,
                'sender_id' => $ticket_details['operator_id'],
                'type' => 'raise_invoice',
                'file_type' => 'pdf',
                'cust_name' => $ticket_details['cust_name'],
                'deliverer_name' => $ticket_details['operator_name'],
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 281
              );
              $this->api->add_bus_chat_to_workroom($workroom_update);
            /* -----------------------Ownward ticket invoice----------------------- */
            
            $user_detailss = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);
                
            /* -----------------Email Invoice to customer-------------------------- */
              $emailBody = $this->lang->line('Please download your ticket payment invoice.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
              $emailAddress = $user_detailss[0]['email_id'];
              $fileToAttach = "resources/ticket-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to customer-------------------- */
            
            /* ------------------------Generate Ticket pdf ------------------------ */
              $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
              if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
                $operator_avtr = base_url("resources/no-image.jpg");
              }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
              require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
                 $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
                 $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
                if(rename($img_src, $img_dest));
              $html ='
              <page format="100x100" orientation="L" style="font: arial;">';
              $html .= '
              <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                  <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                  <div style="margin-top:27px">
                    <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                    <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                  </div>
                </div>
                <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                    <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                    <br />
                    <h6>'.$this->lang->line("slider_heading1").'</h6>
                </div>                
              </div>
              <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                  '.$this->lang->line("Ownward_Trip").'<br/>
                <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
                <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                <h6 style="margin-top:-10px;">
                <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
                <h6 style="margin-top:-8px;"> 
                <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
                <h6 style="margin-top:-8px;">
                <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

                $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                foreach ($ticket_seat_details as $seat)
                {
                  $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                  if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                    
                   $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                }
              $html .='</tbody></table></div></page>';            
              $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
              require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
              $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
              $html2pdf->pdf->SetDisplayMode('default');
              $html2pdf->writeHTML($html);
              $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
              $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
              rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
              //whatsapp api send ticket
              $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
              $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
              //echo json_encode($result); die();
            /* ------------------------Generate Ticket pdf ------------------------ */
            
            /* ---------------------Email Ticket to customer----------------------- */
              $emailBody = $this->lang->line('Please download your e-ticket.');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
              $emailAddress = $user_detailss[0]['email_id'];
              $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
              $fileName = $my_ticket_pdf;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* ---------------------Email Ticket to customer----------------------- */

            //Deduct Ownward Trip Commission From Operator Account
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$ticket_id,
                "user_id" => (int)trim($ticket_details['operator_id']),
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'ticket_commission',
                "amount" => trim($ownward_trip_commission),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);
            //Deduct Ownward Trip Commission From Operator Account

            //Add Ownward Trip Commission To Gonagoo Account
              if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($ticket_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)$ticket_id,
                "user_id" => (int)trim($ticket_details['operator_id']),
                "type" => 1,
                "transaction_type" => 'ticket_commission',
                "amount" => trim($ownward_trip_commission),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($ticket_details['currency_sign']),
                "country_id" => trim($ownward_trip_operator_detail['country_id']),
                "cat_id" => trim($ticket_details['cat_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
            //Add Ownward Trip Commission To Gonagoo Account
            
            /* ---------------------Ownward ticket commission invoice------------- */
              if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
                $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
                $username = $parts[0];
                $operator_name = $username;
              } else {
                $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
              }

              $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
              require_once($phpinvoice);
              //Language configuration for invoice
              $lang = $this->input->post('lang', TRUE);
              if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
              else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
              else { $invoice_lang = "englishApi_lang"; }
              //Invoice Configuration
              $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
              $invoice->setLogo("resources/fpdf-master/invoice-header.png");
              $invoice->setColor("#000");
              $invoice->setType("");
              $invoice->setReference($ticket_details['ticket_id']);
              $invoice->setDate(date('M dS ,Y',time()));

              $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
              $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
              $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

              $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
              $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
              $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

              $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
              $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

              $eol = PHP_EOL;
              /* Adding Items in table */
              $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
              $rate = round(trim($ownward_trip_commission),2);
              $total = $rate;
              $payment_datetime = substr($today, 0, 10);
              //set items
              $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

              /* Add totals */
              $invoice->addTotal($this->lang->line('sub_total'),$total);
              $invoice->addTotal($this->lang->line('taxes'),'0');
              $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
              /* Set badge */ 
              $invoice->addBadge($this->lang->line('Commission Paid'));
              /* Add title */
              $invoice->addTitle($this->lang->line('tnc'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['terms']);
              /* Add title */
              $invoice->addTitle($this->lang->line('payment_dtls'));
              /* Add Paragraph */
              $invoice->addParagraph($gonagoo_address['payment_details']);
              /* Add title */
              $invoice->addTitleFooter($this->lang->line('thank_you_order'));
              /* Add Paragraph */
              $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
              /* Set footer note */
              $invoice->setFooternote($gonagoo_address['company_name']);
              /* Render */
              $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
              //Update File path
              $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
              //Move file to upload folder
              rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

              //Update Order Invoice
              $update_data = array(
                "commission_invoice_url" => "ticket-invoices/".$pdf_name,
              );
              $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            /* ---------------------Ownward ticket commission invoice------------- */
                
            /* -----------------------Email Invoice to operator-------------------- */
              $emailBody = $this->lang->line('Commission Invoice');
              $gonagooAddress = $gonagoo_address['company_name'];
              $emailSubject = $this->lang->line('Commission Invoice');
              $emailAddress = $ownward_trip_operator_detail['email1'];
              $fileToAttach = "resources/ticket-invoices/$pdf_name";
              $fileName = $pdf_name;
              $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
            /* -----------------------Email Invoice to operator-------------------- */
          /********************** Payment Section **************************************/
          $this->session->set_flashdata('success', 'Payment successfully done!');
        } else { $this->session->set_flashdata('error', 'Payment failed! Try again...'); }
      } else {  $this->session->set_flashdata('error', 'Payment failed! Try again...'); }
      $location = base_url('user-panel-bus/seller-upcoming-trips');
      echo '<script>top.window.location = "'.$location.'"</script>';
    }
  //End Ticket Booking by Seller---------------------------   

  //Seller Trip List (Upcomming/Current/Past/Cancelled)----
    public function seller_upcoming_trips()
    {
      $cust_id = $this->cust_id;
      $trip_list = $this->api->get_seller_trip_master_list($cust_id, 'upcoming' , 'operator');
      //echo $this->db->last_query(); die();
      //echo json_encode($trip_list); die();
      
      $filter = 'basic';
      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_seller_upcoming_trip_list_view', compact('trip_list','filter','trip_sources','trip_destinations'));
      $this->load->view('user_panel/footer');
    }
    public function seller_upcoming_trips_view_passengers()
    {
      //echo json_encode($_POST); die();
      if( !is_null($this->input->post('unique_id')) ) { $unique_id = $this->input->post('unique_id'); }
      else if(!is_null($this->uri->segment(3))) { $unique_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/seller-upcoming-trips'); }
      $cust_id = $this->cust_id;

      $ticket_seat_details = $this->api->get_ticket_seat_details_unique_id($unique_id);
      $master_booking_details = $this->api->get_ticket_master_booking_details_unique_id($unique_id);
      //echo json_encode($master_booking_details); die();
      if(empty($master_booking_details)) {
        redirect(base_url('user-panel-bus/seller-upcoming-trips'),'refresh');
      } else { 
        $operator_details = $this->api->get_bus_operator_profile($master_booking_details[0]['operator_id']);
        $this->load->view('user_panel/bus_seller_upcoming_trip_passengers_view', compact('ticket_seat_details','master_booking_details','operator_details','cust_id'));
        $this->load->view('user_panel/footer');
      }
    }    
    public function seller_upcoming_trips_filter()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $trip_source = $_POST['trip_source'];
      $trip_destination = $_POST['trip_destination'];
      $journey_date = $_POST['journey_date'];
      $trip_list = $this->api->get_seller_trip_master_list_filter($cust_id, 'upcoming' , $_POST['trip_source'], $_POST['trip_destination'] , $_POST['journey_date']);
      //echo $this->db->last_query(); 
      //echo json_encode($trip_list); die();

      $filter = 'basic';
      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_seller_upcoming_trip_list_view', compact('trip_list','filter','trip_sources','trip_destinations','trip_source','trip_destination','journey_date'));
      $this->load->view('user_panel/footer');
    }
    public function bus_upcoming_trip_details_seller()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/seller-upcoming-trips'); }
      $cust_id = $this->cust_id;
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $trip_id = $ticket_details['trip_id'];
      $trip_details = $this->api->get_trip_master_details($ticket_details['trip_id']);
      $operator_details = $this->api->get_bus_operator_profile($trip_details[0]['cust_id']);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $trip_details[0]['cust_id']);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_details[0]['trip_id']);
      $src_dest_details = $this->api->get_trip_location_master($trip_details[0]['trip_id']);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      
      $unique_id= $this->api->get_ticket_seat_details($ticket_id);
      $ticket_seat_details = $this->api->get_ticket_seat_details_unique_id($unique_id[0]['unique_id']);
      $master_booking_details = $this->api->get_ticket_master_booking_details_unique_id($unique_id[0]['unique_id']);
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_details[0]['trip_id']);
      //echo json_encode($booking_details); die();
      $this->load->view('user_panel/bus_upcoming_trip_seller_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details','ticket_seat_details','unique_id','master_booking_details','cust_id'));
      $this->load->view('user_panel/footer');
    }
    public function seller_current_trips()
    {
      $cust_id = $this->cust_id;
      $trip_list = $this->api->get_seller_trip_master_list($cust_id, 'current' , 'operator');
      //echo $this->db->last_query(); 
      //echo json_encode($trip_list); die();
      
      $filter = 'basic';
      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_seller_current_trip_list_view', compact('trip_list','filter','trip_sources','trip_destinations'));
      $this->load->view('user_panel/footer');
    }
    public function seller_current_trips_view_passengers()
    {
      //echo json_encode($_POST); die();
      if( !is_null($this->input->post('unique_id')) ) { $unique_id = $this->input->post('unique_id'); }
      else if(!is_null($this->uri->segment(3))) { $unique_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/seller-current-trips'); }
      //$unique_id= $this->api->get_ticket_seat_details($_POST['ticket_id']);
      $ticket_seat_details = $this->api->get_ticket_seat_details_unique_id((int)$unique_id);
      $master_booking_details = $this->api->get_ticket_master_booking_details_unique_id((int)$unique_id);
      $operator_details = $this->api->get_bus_operator_profile((int)$master_booking_details[0]['operator_id']);
      //echo json_encode($operator_details); die();
      $this->load->view('user_panel/bus_seller_current_trip_passengers_view', compact('ticket_seat_details','master_booking_details','operator_details'));
      $this->load->view('user_panel/footer');
    }
    public function seller_current_trips_filter()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $trip_source = $_POST['trip_source'];
      $trip_destination = $_POST['trip_destination'];
      $journey_date = $_POST['journey_date'];
      $trip_list = $this->api->get_seller_trip_master_list_filter($cust_id, 'upcoming' , $_POST['trip_source'], $_POST['trip_destination'] , $_POST['journey_date']);
      //echo $this->db->last_query(); 
      //echo json_encode($trip_list); die();

      $filter = 'basic';
      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_seller_current_trip_list_view', compact('trip_list','filter','trip_sources','trip_destinations','trip_source','trip_destination','journey_date'));
      $this->load->view('user_panel/footer');
    }
    public function bus_current_trip_details_seller()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/seller-upcoming-trips'); }
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $trip_id = $ticket_details['trip_id'];
      $trip_details = $this->api->get_trip_master_details($ticket_details['trip_id']);
      $operator_details = $this->api->get_bus_operator_profile($trip_details[0]['cust_id']);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $trip_details[0]['cust_id']);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_details[0]['trip_id']);
      $src_dest_details = $this->api->get_trip_location_master($trip_details[0]['trip_id']);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      
      $unique_id= $this->api->get_ticket_seat_details($ticket_id);
      $ticket_seat_details = $this->api->get_ticket_seat_details_unique_id($unique_id[0]['unique_id']);
      $master_booking_details = $this->api->get_ticket_master_booking_details_unique_id($unique_id[0]['unique_id']);
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_details[0]['trip_id']);
      //echo json_encode($booking_details); die();
      $this->load->view('user_panel/bus_current_trip_seller_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details','ticket_seat_details','unique_id','master_booking_details'));
      $this->load->view('user_panel/footer');
    } 
    public function seller_previous_trips()
    {
      $cust_id = $this->cust_id;
      $trip_list = $this->api->get_seller_trip_master_list($cust_id, 'previous' , 'operator');
      //echo $this->db->last_query(); die();
      $filter = 'basic';
      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_seller_previous_trip_list_view', compact('trip_list','filter','trip_sources','trip_destinations'));
      $this->load->view('user_panel/footer');
    }
    public function seller_previous_trips_filter()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $trip_source = $_POST['trip_source'];
      $trip_destination = $_POST['trip_destination'];
      $journey_date = $_POST['journey_date'];
      $trip_list = $this->api->get_seller_trip_master_list_filter($cust_id, 'previous' , $_POST['trip_source'], $_POST['trip_destination'] , $_POST['journey_date']);
      //echo $this->db->last_query(); 
      //echo json_encode($trip_list); die();

      $filter = 'basic';
      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_seller_previous_trip_list_view', compact('trip_list','filter','trip_sources','trip_destinations','trip_source','trip_destination','journey_date'));
      $this->load->view('user_panel/footer');
    }
    public function selle_previous_trips_view_passengers()
    {
      //echo json_encode($_POST); die();
      if( !is_null($this->input->post('ticket_id')) ) { $ticket_id = (int) $this->input->post('ticket_id'); }
      else if(!is_null($this->uri->segment(3))) { $ticket_id = (int) $this->uri->segment(3); }
      else { redirect('user-panel-bus/seller-previous-trips'); }
      
      $unique_id= $this->api->get_ticket_seat_details($ticket_id);
      $ticket_seat_details = $this->api->get_ticket_seat_details_unique_id($unique_id[0]['unique_id']);
      $master_booking_details = $this->api->get_ticket_master_booking_details_unique_id($unique_id[0]['unique_id']);
      $operator_details = $this->api->get_bus_operator_profile($master_booking_details[0]['operator_id']);
      $this->load->view('user_panel/bus_seller_previous_trip_passengers_view', compact('ticket_seat_details','master_booking_details','operator_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_previous_trip_details_seller()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/seller-upcoming-trips'); }
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $trip_id = $ticket_details['trip_id'];
      $trip_details = $this->api->get_trip_master_details($ticket_details['trip_id']);
      $operator_details = $this->api->get_bus_operator_profile($trip_details[0]['cust_id']);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $trip_details[0]['cust_id']);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_details[0]['trip_id']);
      $src_dest_details = $this->api->get_trip_location_master($trip_details[0]['trip_id']);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      
      $unique_id= $this->api->get_ticket_seat_details($ticket_id);
      $ticket_seat_details = $this->api->get_ticket_seat_details_unique_id($unique_id[0]['unique_id']);
      $master_booking_details = $this->api->get_ticket_master_booking_details_unique_id($unique_id[0]['unique_id']);
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_details[0]['trip_id']);
      //echo json_encode($booking_details); die();
      $this->load->view('user_panel/bus_previous_trip_seller_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details','ticket_seat_details','unique_id','master_booking_details'));
      $this->load->view('user_panel/footer');
    }
    public function seller_cancelled_tickets()
    {
      $cust_id = $this->cust_id;
      $master_booking_details = $this->api->get_cancelled_seller_ticket($cust_id);
      //echo json_encode($master_booking_details); die();
      $this->load->view('user_panel/bus_seller_cancelled_ticket', compact('master_booking_details'));
      $this->load->view('user_panel/footer');
    }
    public function seller_cancelled_trips()
    {
      $cancelled_trips = $this->api->get_cancelled_seller_trips();
      //echo json_encode($cancelled_trips); die();
      $this->load->view('user_panel/bus_seller_cancelled_trip', compact('cancelled_trips'));
      $this->load->view('user_panel/footer');
    }
    public function seller_cancelled_trips_view_passengers()
    {
      //echo json_encode($_POST); die();
      if( !is_null($this->input->post('unique_id')) ) { $unique_id = $this->input->post('unique_id'); }
      else if(!is_null($this->uri->segment(3))) { $unique_id = (int)$this->uri->segment(3); }
      else { redirect('user-panel-bus/seller-cancelled-trips'); }
      $c = "yes";
      $ticket_seat_details = $this->api->get_ticket_seat_details_unique_id($unique_id);
      //echo json_encode($ticket_seat_details); die();
      $master_booking_details = $this->api->get_ticket_master_booking_details_unique_id($unique_id, $c);
      $operator_details = $this->api->get_bus_operator_profile($master_booking_details[0]['operator_id']);
      //echo json_encode($master_booking_details); die();
      if(empty($master_booking_details)) {
        redirect(base_url('user-panel-bus/seller-cancelled-trips'),'refresh');
      } else { 
        $this->load->view('user_panel/bus_seller_cancelled_trip_passengers_view', compact('ticket_seat_details','master_booking_details','operator_details'));
        $this->load->view('user_panel/footer');
      }
    }    
  //Seller Trip List (Upcomming/Current/Past/Cancelled)----

  //Start Buyer Trip Search, Booking and Payment-----------
    public function bus_ticket_search()
    {
      if(isset($_SESSION['cat_id']) && $_SESSION['cat_id']==281) {
        $this->session->unset_userdata('cat_id');
        $this->session->unset_userdata('trip_source');
        $this->session->unset_userdata('trip_destination');
        $this->session->unset_userdata('journey_date');
        $this->session->unset_userdata('return_date');
        $this->session->unset_userdata('no_of_seat');
        $this->session->unset_userdata('ownward_trip');
        $this->session->unset_userdata('return_trip');
      }

      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      $filter = 'basic';
      $this->load->view('user_panel/bus_trip_search_buyer_view', compact('trip_sources','trip_destinations','filter'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_search_result_buyer()
    {
      //echo json_encode($_SESSION); die();
      $cust_id = $this->cust_id;

      if(isset($_SESSION['cat_id']) && $_SESSION['cat_id']==281) {
        $trip_source = $_SESSION['trip_source'];
        $trip_destination = $_SESSION['trip_destination'];
        $no_of_seat = $_SESSION['no_of_seat'];
        if(isset($_SESSION['journey_date'])) {
          $date_array = explode('-', $_SESSION['journey_date']);
          $journey_date = $date_array[1].'/'.$date_array[2].'/'.$date_array[0];
        }
        if(isset($_SESSION['return_date']) && $_SESSION['return_date'] != '') {
          $date_array = explode('-', $_SESSION['return_date']);
          $return_date = $date_array[1].'/'.$date_array[2].'/'.$date_array[0];
        } else { $return_date = ''; }

        $this->session->unset_userdata('cat_id');
        $this->session->unset_userdata('trip_source');
        $this->session->unset_userdata('trip_destination');
        $this->session->unset_userdata('journey_date');
        $this->session->unset_userdata('return_date');
        $this->session->unset_userdata('no_of_seat');
        $this->session->unset_userdata('ownward_trip');
        $this->session->unset_userdata('return_trip');
      } else {
        $trip_source = $this->input->post('trip_source');
        $trip_destination = $this->input->post('trip_destination');
        $jdate = $journey_date = $this->input->post('journey_date');
        $rdate = $return_date = $this->input->post('return_date');
        $no_of_seat = $this->input->post('no_of_seat');
        
        $journey_date=date_create($journey_date);
      $journey_date=date_format($journey_date,"Y-m-d");
      }

      //Update Trip Data
      if($no_of_seat > 10) {
        $this->session->set_flashdata('error', $this->lang->line('The maximum number of seat is 40'));
        if($_POST['place']=="dashboard") { redirect('user-panel-bus/dashboard-bus');
        } else { redirect('user-panel-bus/bus-ticket-search'); }
      } else {
        $search_data = array(
          "trip_source" => trim($trip_source), 
          "trip_destination" => trim($trip_destination), 
          "journey_date" => trim($journey_date), 
          "vehical_type_id" =>"",
          "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))),
          "return_date" => trim($return_date),
          "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
          "cust_id" => (int)trim($cust_id), 
          "opr_id" => '', 
          "booking_type" => 'online', 
        );
        //echo json_encode($search_data); die();
        $trip_list = $this->api->get_trip_source_destination_search_list($search_data);
        //echo $this->db->last_query(); 
        //echo json_encode($trip_list); die();
        if($return_date!="") {
          $return_date=date_create($return_date);
        $return_date=date_format($return_date,"Y-m-d");
          $search_data_return = array(
            "trip_destination" => trim($trip_source),
            "trip_source" => trim($trip_destination),
            "vehical_type_id" =>"",
            // "journey_date" => trim($journey_date),
            // "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))),
            "return_date" => trim($return_date),
            "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
            "cust_id" => (int)trim($cust_id),
            "opr_id" => '', 
            "booking_type" => 'online',
          );
          $trip_list_return = $this->api->get_trip_source_destination_search_list_return($search_data_return);
        }
        $filter = 'basic';
        $trip_sources = $this->api->get_trip_master_source_list_buyer();
        $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
        // for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
        for($i=0; $i<sizeof($trip_list); $i++) {
          $unique_id = str_replace("/","",$journey_date)."_".$trip_list[$i]['trip_id'];
          if($cancelled_trips = $this->api->get_cancelled_seller_trips()) {
            foreach ($cancelled_trips as $ct) {
              if($ct['unique_id']==$unique_id) { array_push($trip_list[$i],"delete");
              } else{ array_push($trip_list[$i],"safe");  }
            }  
          } else { array_push($trip_list[$i],"safe"); }
        }
        if(isset($trip_list_return)) {
          // for($i=0; $i<sizeof($trip_list_return); $i++) { array_push($trip_list_return[$i],"safe"); }
          for($i=0; $i<sizeof($trip_list_return); $i++) {
            $unique_id = str_replace("/","",$return_date)."_".$trip_list_return[$i]['trip_id'];
            if($cancelled_trips = $this->api->get_cancelled_seller_trips()) {
              foreach ($cancelled_trips as $ct){
                if($ct['unique_id']==$unique_id){ array_push($trip_list_return[$i],"delete");
                } else { array_push($trip_list_return[$i],"safe"); }
              }  
            } else { array_push($trip_list_return[$i],"safe"); } 
          } 
        }
        //echo json_encode($trip_list); die();
        $journey_date = $jdate;
        $return_date = $rdate;
        $this->load->view('user_panel/bus_trip_search_result_buyer_view', compact('trip_list','trip_list_return','filter','trip_source','trip_destination','trip_sources','trip_destinations','journey_date','return_date','no_of_seat'));
        $this->load->view('user_panel/footer');
      }
    }
    public function bus_trip_search_result_buyer_filter()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $trip_source = $this->input->post('trip_source_filter');
      $trip_destination = $this->input->post('trip_destination_filter');
      $journey_date = $this->input->post('journey_date_filter');
      $return_date = $this->input->post('return_date_filter');
      $depart_from = $this->input->post('depart_from');
      $bus_seat_type = $this->input->post('bus_seat_type');
      $bus_ac = $this->input->post('bus_ac');
      $ticket_to = $this->input->post('ticket_to');
      $ticket_from = $this->input->post('ticket_from');
      $operator = $this->input->post('operator');
      $bus_amen = $this->input->post('bus_amenities');
      $vehical_type_id = $this->input->post('vehical_type_id');
      $no_of_seat = $this->input->post('no_of_seat');
      //$pickup_point = $this->input->post('pickup_point');
      //$drop_point = $this->input->post('drop_point');
      $search_data = array(
        "trip_source" => trim($trip_source), 
        "trip_destination" => trim($trip_destination), 
        "journey_date" => trim($journey_date), 
        "vehical_type_id" => trim($vehical_type_id),
        "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
        "return_date" => trim($return_date),
        "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
        "cust_id" => (int)trim($cust_id), 
        "opr_id" => (int)trim($operator), 
      );
      //echo json_encode($search_data); die();
      $trip_list = $this->api->get_trip_source_destination_search_list($search_data);
      //Return Trip
      if($return_date!=""){
        $search_data_return = array(
        "trip_destination" => trim($trip_source), 
        "trip_source" => trim($trip_destination), 
        "vehical_type_id" => trim($vehical_type_id), 
        // "journey_date" => trim($journey_date), 
        // "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
        "return_date" => trim($return_date),
        "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
        "cust_id" => (int)trim($cust_id), 
        "opr_id" => (int)trim($operator), 
        );
        $trip_list_return = $this->api->get_trip_source_destination_search_list_return($search_data_return);
      }
      $filter = 'basic';
      $trip_sources = $this->api->get_multi_trip_master_source_list($this->cust_id);
      $trip_destinations = $this->api->get_multi_trip_master_destination_list($this->cust_id);
      function zaid_date_time_validator($date, $format = 'Y-m-d H:i:s')
      { $d = DateTime::createFromFormat($format, $date);return $d && $d->format($format) == $date; }
      for($i=0; $i<sizeof($trip_list); $i++) {
        $unique_id = str_replace("/","",$journey_date)."_".$trip_list[$i]['trip_id'];
        $t=0; $seat_ac_amen=0; $pr=0;
        if(zaid_date_time_validator($depart_from , 'H:i')) {
          $depart=$this->api->get_departure_time_filter($trip_list[$i]['trip_id'],$depart_from);
          if($depart==true){$t=0;}if($depart==false){$t++;}
        }
        if($bus_seat_type=="SEATER" || $bus_seat_type=="SLEEPER" || $bus_seat_type=="SEMI-SLEEPER" || $bus_ac=="AC" || $bus_ac=="NON AC" || $bus_amen!="") {
          $b= $this->api->get_seat_ac_amen_filter($trip_list[$i]['bus_id'],$bus_seat_type,$bus_ac,$bus_amen);
          if($b==true){ $seat_ac_amen=0;}if($b==false){$seat_ac_amen++; }
        }
        if($ticket_to!="" || $ticket_from!="") {
          $price = $this->api->get_trip_master_search_price_filter($trip_list[$i]['trip_id'],$ticket_to,$ticket_from);
          if($price==true){ $pr=0; }
          if($price==false){ $pr++; } 
        }
        // if($t>0 || $seat_ac_amen>0 || $pr>0) {
        //   array_push($trip_list[$i],"delete");
        // } else { array_push($trip_list[$i],"safe"); }     
        if($cancelled_trips = $this->api->get_cancelled_seller_trips()){
          foreach ($cancelled_trips as $ct){
          if($ct['unique_id']==$unique_id || $t>0 || $seat_ac_amen>0 || $pr>0 ){array_push($trip_list[$i],"delete");}
          else{array_push($trip_list[$i],"safe");}
          }
        }else{
          if($t>0 || $seat_ac_amen>0 || $pr>0) {
           array_push($trip_list[$i],"delete");
          } else { array_push($trip_list[$i],"safe"); }
        }   
      }
      //Return Trip Filter
      if(isset($trip_list_return)) {
        for($i=0; $i<sizeof($trip_list_return); $i++) {
          $unique_id = str_replace("/","",$return_date)."_".$trip_list_return[$i]['trip_id'];
          $t=0; $seat_ac_amen=0; $pr=0;
          if(zaid_date_time_validator($depart_from , 'H:i')) {
            $depart=$this->api->get_departure_time_filter($trip_list_return[$i]['trip_id'],$depart_from);
            if($depart==true){$t=0;}if($depart==false){$t++;}
          }
          if($bus_seat_type=="SEATER" || $bus_seat_type=="SLEEPER" || $bus_seat_type=="SEMI-SLEEPER" || $bus_ac=="AC" || $bus_ac=="NON AC" || $bus_amen!="") {
            $b= $this->api->get_seat_ac_amen_filter($trip_list_return[$i]['bus_id'],$bus_seat_type,$bus_ac,$bus_amen);
            if($b==true){ $seat_ac_amen=0;}if($b==false){$seat_ac_amen++; }
          }
          if($ticket_to!="" || $ticket_from!="") {
            $price = $this->api->get_trip_master_search_price_filter($trip_list_return[$i]['trip_id'],$ticket_to,$ticket_from);
            if($price==true){ $pr=0; }
            if($price==false){ $pr++; } 
          }
          // if($t>0 || $seat_ac_amen>0 || $pr>0) {
          //   array_push($trip_list_return[$i],"delete");
          // } else { array_push($trip_list_return[$i],"safe"); }     
          if($cancelled_trips = $this->api->get_cancelled_seller_trips()){
            foreach ($cancelled_trips as $ct){
            if($ct['unique_id']==$unique_id || $t>0 || $seat_ac_amen>0 || $pr>0 ){array_push($trip_list_return[$i],"delete");}
            else{array_push($trip_list_return[$i],"safe");}
            }  
          }else{
            if($t>0 || $seat_ac_amen>0 || $pr>0) {
             array_push($trip_list_return[$i],"delete");
            } else { array_push($trip_list_return[$i],"safe"); }
          }
        }
      }
      $this->load->view('user_panel/bus_trip_search_result_buyer_view', compact('trip_list_return','trip_list','trip_sources','trip_destinations','depart','filter','trip_source','trip_destination','journey_date','return_date','depart_from','bus_seat_type','bus_ac','ticket_to','ticket_from','operator','bus_amen','vehical_type_id','no_of_seat'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_details_buyer()
    {
      if(!is_null($this->input->post('trip_id'))) { $trip_id = (int) $this->input->post('trip_id');
      } else if(!is_null($this->uri->segment(3))) { $trip_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/bus-trip-master-list'); }
      $trip_details = $this->api->get_trip_master_details($trip_id);
      $operator_details = $this->api->get_bus_operator_profile($this->cust_id);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $this->cust_id);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_id);
      $src_dest_details = $this->api->get_trip_location_master($trip_id);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_id);
      $this->load->view('user_panel/bus_trip_buyer_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_booking_buyer()
    {
      //echo json_encode($_POST); die();
      if(!isset($_POST['journey_date'])) {
        $_POST = array(
          'journey_date' => $_SESSION['journey_date'],
          'return_date' => $_SESSION['return_date'],
          'ownward_trip' => $_SESSION['ownward_trip'],
          'return_trip' => $_SESSION['return_trip'],
          'no_of_seat' => $_SESSION['no_of_seat'],
          'seat_type' => $_SESSION['seat_type'],
          'pickup_point' => $_SESSION['pickup_point'],
          'drop_point' => $_SESSION['drop_point'],
          'return_seat_type' => $_SESSION['return_seat_type'],
          'return_pickup_point' => $_SESSION['return_pickup_point'],
          'return_drop_point' => $_SESSION['return_drop_point'],
          'is_return' => $_SESSION['is_return'],

          'seat_price' => $_SESSION['seat_price'],
          'distance' => $_SESSION['distance'],
          'duration' => $_SESSION['duration'],
          'ownward_currency_sign' => $_SESSION['ownward_currency_sign'],
          'ownward_currency_id' => $_SESSION['ownward_currency_id'],
          'return_seat_price' => $_SESSION['return_seat_price'],
          'return_trip_destination' => $_SESSION['return_trip_destination'],
          'return_currency_sign' => $_SESSION['return_currency_sign'],

          'ownward_selected_seats_count_'.$_SESSION['ownward_trip'] => $_SESSION['ownward_selected_seats_count_'.$_SESSION['ownward_trip']],
          'ownward_selected_seat_nos_'.$_SESSION['ownward_trip'] => $_SESSION['ownward_selected_seat_nos_'.$_SESSION['ownward_trip']],

          'return_selected_seats_count_'.$_SESSION['return_trip'] => ($_SESSION['return_trip']>0)?$_SESSION['return_selected_seats_count_'.$_SESSION['return_trip']]:0,
          'return_selected_seat_nos_'.$_SESSION['return_trip'] => ($_SESSION['return_trip']>0)?$_SESSION['return_selected_seat_nos_'.$_SESSION['return_trip']]:'NULL',

          'trip_source' => $_SESSION['trip_source'],
          'trip_destination' => $_SESSION['trip_destination'],
          'return_trip_source' => $_SESSION['return_trip_source'],
          'return_trip_destination' => $_SESSION['return_trip_destination'],
        );
       } else {$_POST = $_POST; }
      
      //echo json_encode($_POST); die();

      $countries = $this->user->get_countries();
      $address_details = $this->api->get_consumers_addressbook($this->cust_id);
      $this->load->view('user_panel/bus_trip_seat_booking_buyer', compact('$_POST','address_details','countries'));
      $this->load->view('user_panel/footer');
    }
    public function bus_trip_booking_process_buyer()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $no_of_seat = array();
      $no_of_seat = $_POST['selected_passengers'];
      $ownward_selected_seat_nos = $_POST['ownward_selected_seat_nos'];
      $return_selected_seat_nos = $_POST['return_selected_seat_nos'];
      if($ownward_selected_seat_nos == '') $ownward_selected_seat_nos = 'NULL';
      if($return_selected_seat_nos == '') $return_selected_seat_nos = 'NULL';

      $user_details = $this->api->get_user_details((int)$cust_id);
      $trip = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['ownward_trip_sdd_id']);
      $operator = $this->api->get_bus_operator_profile($trip[0]['cust_id']);
      $country_id = $this->api->get_address_from_book($no_of_seat[0])['country_id'];
      //echo json_encode($country_id); die();
      $today = date('Y-m-d H:i:s');
      $unique_id = str_replace('/','',$_POST['journey_date']).'_'.$trip[0]['trip_id'];

      if(isset($_POST['return_trip_sdd_id']) && $_POST['return_trip_sdd_id'] > 0) {
        $sms_body_24_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];
        $sms_body_2_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];
        $email_body = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];

        $data_master = array(
          'trip_id' => $trip[0]['trip_id'] ,
          'cust_id' => (int)$cust_id,
          'source_point_id' => $trip[0]['trip_source_id'] ,
          'source_point' => $trip[0]['trip_source'] ,
          'pickup_point' => $_POST['onward_pickup_point'] ,
          'destination_point_id' => $trip[0]['trip_destination_id'] ,
          'destination_point' => $trip[0]['trip_destination'] ,
          'drop_point' => $_POST['onward_drop_point'] ,
          'bus_id' => $trip[0]['bus_id'] ,
          'journey_date' => $_POST['journey_date'],
          'trip_start_date_time' => $trip[0]['trip_depart_time'] ,
          'bookig_date' => $today,
          'seat_type' =>  $_POST['onward_bus_seat_type'],
          'no_of_seats' =>  sizeof($no_of_seat),
          'seat_price' =>  $_POST['seat_price'],
          'ticket_price' =>  ($_POST['seat_price']*sizeof($no_of_seat)),
          'currency_id' =>  $_POST['currency_id'],
          'currency_sign' =>  $_POST['currency_sign'],
          'cust_name' => $user_details['firstname'] ,
          'cust_contact' => $user_details['mobile1'] ,
          'cust_email' =>  $user_details['email1'],
          'operator_name' => $operator['firstname'],
          'operator_company_name' => $operator['company_name'],
          'operator_contact_name' =>  $operator['contact_no'],
          'operator_id' => $operator['cust_id'],
          'balance_amount' => ($_POST['seat_price']*sizeof($no_of_seat)),
          'ticket_status' => "booked",
          'status_updated_by' => (int)$cust_id,
          'status_updated_by_user_type' => "customer",
          'status_update_datetime' => $today,
          'is_return' => 0,
          'return_trip_id' => 0,
          'cat_id' => 281,
          'unique_id' => $unique_id,
          'booked_by' => 'customer',
          'country_id' => (int)$country_id,
          'vip_seat_nos' => $ownward_selected_seat_nos,
        );
        //echo json_encode($data_master); die();
        $ticket_id = $this->api->create_bus_ticket_booking_master($data_master);
        //Send Booking SMS
        $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
        //$message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id;
        
        $seat_nos = ($ownward_selected_seat_nos == 'NULL')?'N/A':$ownward_selected_seat_nos;
        $bus_details = $this->api->get_operator_bus_details($trip[0]['bus_id']);
        $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id.'.  '.$this->lang->line('from').': '.$trip[0]['trip_source'].' '.$_POST['onward_pickup_point'].'.  '.$this->lang->line('To').': '.$trip[0]['trip_destination'].' '.$_POST['onward_drop_point'].'.  '.$this->lang->line('date').': '.$_POST['journey_date'].' '.$trip[0]['trip_depart_time'].'.  '.$this->lang->line('Vehicle').': '.$bus_details['bus_modal'].'. '.$this->lang->line('Seats').': '.$seat_nos . ' - ' . $operator['company_name'];
        
        $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
        $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
        
        //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
        if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
          $parts = explode("@", trim($user_details['email1']));
          $username = $parts[0];
          $customer_name = $username;
        } else {
          $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
        }
        $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Ticket booking confirmation'), trim($message));

        //Send Push Notifications
        //Get PN API Keys
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_customer = $this->api->get_user_device_details($cust_id);
        if(!is_null($device_details_customer)) {
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => $this->lang->line('Ticket booking confirmation'), 'type' => 'ticket-booking-confirmation', 'notice_date' => $today, 'desc' => $message);
          $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

          //Push Notification APN
          $msg_apn_customer =  array('title' => $this->lang->line('Ticket booking confirmation'), 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }

        //Adding passenger details
        for($i=0; $i < sizeof($no_of_seat) ; $i++) { 
          $a = $this->api->get_address_from_book($no_of_seat[$i]);
          $data = array(
            'ticket_id' => $ticket_id,
            'trip_id' => $trip[0]['trip_id'],
            'cust_id' => (int)$cust_id,
            'operator_id' => $operator['cust_id'],
            'seat_type' =>  $_POST['onward_bus_seat_type'],
            'cre_datetime' => $today,
            'firstname' => $a['firstname'],
            'lastname' => $a['lastname'],
            'mobile' =>$a['mobile'],
            'email_id' => $a['email_id'] ,
            'gender' => $a['gender'],
            'country_id' => $a['country_id'],
            'age' => $a ['age'],
            'journey_date' => $_POST['journey_date'],
            'unique_id' => $unique_id,
            'ticket_status' => "booked",
          );
          $res = $this->api->create_bus_ticket_booking_seat_details($data);

          //Check if trip date time is greater than current datetime + 2hrs 
          $datetime1 = new DateTime();
          //echo json_encode($datetime1); die();
          $datetime2 = new DateTime($_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00');
          $interval = $datetime1->diff($datetime2);
          //echo json_encode($interval); die();
          $years = $interval->y;
          $months = $interval->m;
          $days = $interval->d;
          $hours = $interval->h;
          $minutes = $interval->i;
          $seconds = $interval->s;
          $diff_type = $interval->invert;
          if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
          } else { 
            //Crone Job Alerts
            $crone_data_24_sms = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_24_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 24,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_24_sms);
            $crone_data_2_sms = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_2_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 2,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_2_sms);
            $crone_data = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => 'NULL',
              'email_body' => $email_body,
              'alert_type' => 1,
              'sms_duration' => 0,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data);
          }
        }//for

        //Update to workroom
        $workroom_update = array(
          'order_id' => $ticket_id,
          'cust_id' => (int)$cust_id,
          'deliverer_id' => $operator['cust_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $cust_id,
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $operator['firstname'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);

        //Update to Global Workroom
        $global_workroom_update = array(
          'service_id' => $ticket_id,
          'cust_id' => (int)$cust_id,
          'deliverer_id' => $operator['cust_id'],
          'sender_id' => $cust_id,
          'cat_id' => 281
        );
        $this->api->create_global_workroom($global_workroom_update);

        //Adding Return Trip Details
        $return_trip = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['return_trip_sdd_id']);
        $operator = $this->api->get_bus_operator_profile($return_trip[0]['cust_id']);

        $return_unique_id = str_replace('/','',$_POST['return_date']).'_'.$return_trip[0]['trip_id'];

        $sms_body_24_hr = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['return_date'].' '.$return_trip[0]['trip_depart_time'];
        $sms_body_2_hr = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['return_date'].' '.$return_trip[0]['trip_depart_time'];
        $email_body = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['return_date'].' '.$return_trip[0]['trip_depart_time'];

        $return_data_master = array(
          'trip_id' => $return_trip[0]['trip_id'] ,
          'cust_id' => (int)$cust_id,
          'source_point_id' => $return_trip[0]['trip_source_id'] ,
          'source_point' => $return_trip[0]['trip_source'] ,
          'pickup_point' => $_POST['onward_pickup_point'] ,
          'destination_point_id' => $return_trip[0]['trip_destination_id'] ,
          'destination_point' => $return_trip[0]['trip_destination'] ,
          'drop_point' => $_POST['onward_drop_point'] ,
          'bus_id' => $return_trip[0]['bus_id'] ,
          'journey_date' => $_POST['return_date'],
          'trip_start_date_time' => $return_trip[0]['trip_depart_time'] ,
          'bookig_date' =>  $today,
          'seat_type' =>  $_POST['return_bus_seat_type'],
          'no_of_seats' =>  sizeof($no_of_seat),
          'seat_price' =>  $_POST['return_seat_price'],
          'ticket_price' =>  ($_POST['return_seat_price']*sizeof($no_of_seat)),
          'currency_id' =>  $_POST['return_currency_id'],
          'currency_sign' =>  $_POST['return_currency_sign'],
          'cust_name' => $user_details['firstname'] ,
          'cust_contact' => $user_details['mobile1'] ,
          'cust_email' =>  $user_details['email1'],
          'operator_name' => $operator['firstname'],
          'operator_company_name' => $operator['company_name'],
          'operator_contact_name' =>  $operator['contact_no'],
          'operator_id' => $operator['cust_id'],
          'balance_amount' =>  ($_POST['return_seat_price']*sizeof($no_of_seat)),
          'ticket_status' => "booked",
          'status_updated_by' => (int)$cust_id,
          'status_updated_by_user_type' => "customer",
          'status_update_datetime' => $today,
          'is_return' => 1,
          'return_trip_id' => $ticket_id,
          'cat_id' => 281,
          'unique_id' => $return_unique_id,
          'booked_by' => 'customer',
          'country_id' => (int)$country_id,
          'vip_seat_nos' => $return_selected_seat_nos,
        );

        $return_ticket_id = $this->api->create_bus_ticket_booking_master($return_data_master);
        //Send SMS
        $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
        //$message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$return_ticket_id;
        
        
        $return_bus_details = $this->api->get_operator_bus_details($return_trip[0]['bus_id']);
        $seat_nos = ($return_selected_seat_nos == 'NULL')?'N/A':$return_selected_seat_nos;
        $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$return_ticket_id.'.  '.$this->lang->line('from').': '.$return_trip[0]['trip_source'].' '.$_POST['return_pickup_point'].'.  '.$this->lang->line('To').': '.$return_trip[0]['trip_destination'].' '.$_POST['return_drop_point'].'.  '.$this->lang->line('date').': '.$_POST['return_date'].' '.$return_trip[0]['trip_depart_time'].'.  '.$this->lang->line('Vehicle').': '.$return_bus_details['bus_modal'].'. '.$this->lang->line('Seats').': '.$seat_nos . ' - ' . $operator['company_name'];
        
        $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
        $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
        //Send Push Notifications
        //Get PN API Keys
        if(!is_null($device_details_customer)) {
          $msg = array('title' => $this->lang->line('Ticket booking confirmation'), 'type' => 'ticket-booking-confirmation', 'notice_date' => $today, 'desc' => $message);
          $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
          
          //Push Notification APN
          $msg_apn_customer =  array('title' => $this->lang->line('Ticket booking confirmation'), 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }

        if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
          $parts = explode("@", trim($user_details['email1']));
          $username = $parts[0];
          $customer_name = $username;
        } else {
          $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
        }
        //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
        $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Ticket booking confirmation'), trim($message));

        //Update return trip ID
        $dt = array(
          'return_trip_id' => $return_ticket_id
        );
        $uptd = $this->api->update_booking_details($dt , $ticket_id);

        //Adding return passenger details
        for($i=0; $i < sizeof($no_of_seat) ; $i++) { 
          $a = $this->api->get_address_from_book($no_of_seat[$i]);
          $data = array(
            'ticket_id' => $return_ticket_id,
            'trip_id' => $return_trip[0]['trip_id'],
            'cust_id' => (int)$cust_id,
            'operator_id' => $operator['cust_id'],
            'seat_type' =>  $_POST['return_bus_seat_type'],
            'cre_datetime' => date('Y-m-d H:i:s') ,
            'firstname' => $a['firstname'],
            'lastname' => $a['lastname'],
            'mobile' =>$a['mobile'],
            'email_id' => $a['email_id'] ,
            'gender' => $a['gender'],
            'country_id' => $a['country_id'],
            'age' => $a ['age'],
            'journey_date' => $_POST['journey_date'],
            'unique_id' => $return_unique_id,
            'ticket_status' => "booked",
          );
          $res = $this->api->create_bus_ticket_booking_seat_details($data);
          //Check if trip date time is greater than current datetime + 2hrs 
          $datetime1 = new DateTime();
          //echo json_encode($datetime1); die();
          $datetime2 = new DateTime($_POST['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00');
          $interval = $datetime1->diff($datetime2);
          //echo json_encode($interval); die();
          $years = $interval->y;
          $months = $interval->m;
          $days = $interval->d;
          $hours = $interval->h;
          $minutes = $interval->i;
          $seconds = $interval->s;
          $diff_type = $interval->invert;
          if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
          } else { 
            //Crone Job Alerts
            $crone_data_24_sms = array(
              'journey_date' => $_POST['return_date'],
              'journey_time' => $return_trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $return_unique_id, 
              'ticket_id' => $return_ticket_id,
              'trip_id' => $return_trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_24_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 24,
              'journey_datetime' => $_POST['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_24_sms);
            $crone_data_2_sms = array(
              'journey_date' => $_POST['return_date'],
              'journey_time' => $return_trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $return_unique_id, 
              'ticket_id' => $return_ticket_id,
              'trip_id' => $return_trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_2_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 2,
              'journey_datetime' => $_POST['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_2_sms);
            $crone_data = array(
              'journey_date' => $_POST['return_date'],
              'journey_time' => $return_trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $return_unique_id, 
              'ticket_id' => $return_ticket_id,
              'trip_id' => $return_trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => 'NULL',
              'email_body' => $email_body,
              'alert_type' => 1,
              'sms_duration' => 0,
              'journey_datetime' => $_POST['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data);
          }
        }//for

        //Update to workroom
        $workroom_update = array(
          'order_id' => $return_ticket_id,
          'cust_id' => (int)$cust_id,
          'deliverer_id' => $operator['cust_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $cust_id,
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $customer_name,
          'deliverer_name' => $operator['firstname'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);

        //Update to Global Workroom
        $global_workroom_update = array(
          'service_id' => $return_ticket_id,
          'cust_id' => $user_details['cust_id'],
          'deliverer_id' => $operator['cust_id'],
          'sender_id' => $user_details['cust_id'],
          'cat_id' => 281
        );
        $this->api->create_global_workroom($global_workroom_update);
      }
      
      if(!isset($data_master)) {

        $sms_body_24_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];
        $sms_body_2_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];
        $email_body = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_POST['journey_date'].' '.$trip[0]['trip_depart_time'];

        $data_master = array(
          'trip_id' => $trip[0]['trip_id'],
          'cust_id' => (int)$cust_id,
          'source_point_id' => $trip[0]['trip_source_id'] ,
          'source_point' => $trip[0]['trip_source'] ,
          'pickup_point' => $_POST['onward_pickup_point'] ,
          'destination_point_id' => $trip[0]['trip_destination_id'] ,
          'destination_point' => $trip[0]['trip_destination'] ,
          'drop_point' => $_POST['onward_drop_point'] ,
          'bus_id' => $trip[0]['bus_id'] ,
          'journey_date' => $_POST['journey_date'] ,
          'trip_start_date_time' => $trip[0]['trip_depart_time'] ,
          'bookig_date' => $today,
          'seat_type' =>  $_POST['bus_seat_type'],
          'no_of_seats' =>  sizeof($no_of_seat),
          'seat_price' =>  $_POST['seat_price'],
          'ticket_price' =>  ($_POST['seat_price']*sizeof($no_of_seat)),
          'currency_id' =>  $_POST['currency_id'],
          'currency_sign' =>  $_POST['currency_sign'],
          'cust_name' => $user_details['firstname'] ,
          'cust_contact' => $user_details['mobile1'] ,
          'cust_email' =>  $user_details['email1'],
          'operator_name' => $operator['firstname'],
          'operator_company_name' => $operator['company_name'],
          'operator_contact_name' =>  $operator['contact_no'],
          'operator_id' => $operator['cust_id'],
          'balance_amount' => ($_POST['seat_price']*sizeof($no_of_seat)),
          'ticket_status' => "booked",
          'status_updated_by' => (int)$cust_id,
          'status_updated_by_user_type' => "customer",
          'status_update_datetime' => $today,
          'is_return' => 0,
          'return_trip_id' => 0,
          'cat_id' => 281,
          'unique_id' => $unique_id,
          'booked_by' => 'customer',
          'country_id' => $country_id,
          'vip_seat_nos' => $ownward_selected_seat_nos,
        );
        $ticket_id = $this->api->create_bus_ticket_booking_master($data_master);

        //Send Booking SMS
        $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
        //$message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id;
        $seat_nos = ($ownward_selected_seat_nos == 'NULL')?'N/A':$ownward_selected_seat_nos;
        $bus_details = $this->api->get_operator_bus_details($trip[0]['bus_id']);
        $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id.'.  '.$this->lang->line('from').': '.$trip[0]['trip_source'].' '.$_POST['onward_pickup_point'].'.  '.$this->lang->line('To').': '.$trip[0]['trip_destination'].' '.$_POST['onward_drop_point'].'.  '.$this->lang->line('date').': '.$_POST['journey_date'].' '.$trip[0]['trip_depart_time'].'.  '.$this->lang->line('Vehicle').': '.$bus_details['bus_modal'].'. '.$this->lang->line('Seats').': '.$seat_nos . ' - ' . $operator['company_name'];
        
        $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
        $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

        if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
          $parts = explode("@", trim($user_details['email1']));
          $username = $parts[0];
          $customer_name = $username;
        } else {
          $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
        }
        //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
        $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Ticket booking confirmation'), trim($message));

        //Send Push Notifications
        //Get PN API Keys
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_customer = $this->api->get_user_device_details($cust_id);
        if(!is_null($device_details_customer)) {
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => $this->lang->line('Ticket booking confirmation'), 'type' => 'ticket-booking-confirmation', 'notice_date' => $today, 'desc' => $message);
          $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

          //Push Notification APN
          $msg_apn_customer =  array('title' => $this->lang->line('Ticket booking confirmation'), 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }

        for($i=0; $i < sizeof($no_of_seat) ; $i++) { 
          $a = $this->api->get_address_from_book($no_of_seat[$i]);
          $data = array(
            'ticket_id' => $ticket_id,
            'trip_id' => $trip[0]['trip_id'],
            'cust_id' => (int)$cust_id,
            'operator_id' => $operator['cust_id'],
            'seat_type' =>  $_POST['bus_seat_type'],
            'cre_datetime' => date('Y-m-d H:i:s') ,
            'firstname' => $a['firstname'],
            'lastname' => $a['lastname'],
            'mobile' =>$a['mobile'],
            'email_id' => $a['email_id'] ,
            'gender' => $a['gender'],
            'country_id' => $a['country_id'],
            'age' => $a ['age'],
            'journey_date' => $_POST['journey_date'],
            'unique_id' => $unique_id, 
            'ticket_status' => "booked",
          );
          $res = $this->api->create_bus_ticket_booking_seat_details($data);
          //Check if trip date time is greater than current datetime + 2hrs 
          $datetime1 = new DateTime();
          //echo json_encode($datetime1); die();
          $datetime2 = new DateTime($_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00');
          $interval = $datetime1->diff($datetime2);
          //echo json_encode($interval); die();
          $years = $interval->y;
          $months = $interval->m;
          $days = $interval->d;
          $hours = $interval->h;
          $minutes = $interval->i;
          $seconds = $interval->s;
          $diff_type = $interval->invert;
          if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
          } else { 
            //Crone Job Alerts
            $crone_data_24_sms = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_24_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 24,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_24_sms);
            $crone_data_2_sms = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => $sms_body_2_hr,
              'email_body' => 'NULL',
              'alert_type' => 0,
              'sms_duration' => 2,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data_2_sms);
            $crone_data = array(
              'journey_date' => $_POST['journey_date'],
              'journey_time' => $trip[0]['trip_depart_time'].':00',
              'passenger_name' => $a['firstname'].' '.$a['lastname'],
              'mobile' => $a['mobile'],
              'country_id' =>  $a['country_id'],
              'email' => $a['email_id'],
              'unique_id' => $unique_id,
              'ticket_id' => $ticket_id,
              'trip_id' => $trip[0]['trip_id'],
              'cre_datetime' => $today,
              'sms_body' => 'NULL',
              'email_body' => $email_body,
              'alert_type' => 1,
              'sms_duration' => 0,
              'journey_datetime' => $_POST['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
            );
            $res = $this->api->create_bus_booking_crone_alert($crone_data);
          }
        }//for
        //Update to workroom
        $workroom_update = array(
          'order_id' => $ticket_id,
          'cust_id' => $user_details['cust_id'],
          'deliverer_id' => $operator['cust_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $user_details['cust_id'],
          'type' => 'order_status',
          'file_type' => 'text',
          'cust_name' => $user_details['firstname'],
          'deliverer_name' => $operator['firstname'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);

        //Update to Global Workroom
        $global_workroom_update = array(
          'service_id' => $ticket_id,
          'cust_id' => $user_details['cust_id'],
          'deliverer_id' => $operator['cust_id'],
          'sender_id' => $user_details['cust_id'],
          'cat_id' => 281
        );
        $this->api->create_global_workroom($global_workroom_update);

      } //if 
      redirect(base_url('user-panel-bus/bus-ticket-payment-buyer').'/'.$ticket_id, 'refresh');
    }
    public function bus_ticket_payment_buyer()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/buyer-upcoming-trips'); }
      $details =  $this->api->get_trip_booking_details($ticket_id);
      //echo json_encode($details); die();

      //if ownward trip has return trip - get return trip details.
      $return_details = array();
      if($details['return_trip_id'] > 0) {
        $return_details =  $this->api->get_trip_booking_details($details['return_trip_id']);
      }

      //echo json_encode($details); die();
      $this->load->view('user_panel/bus_ticket_payment_buyer_view',compact('details','return_details'));
      $this->load->view('user_panel/footer');
    }
    public function bus_single_ticket_payment_buyer()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/buyer-upcoming-trips'); }
      $details =  $this->api->get_trip_booking_details($ticket_id);
      //echo json_encode($details); die();

      $this->load->view('user_panel/bus_single_ticket_payment_buyer_view',compact('details'));
      $this->load->view('user_panel/footer');
    }
    public function buyer_ticket_mark_as_cod()
    {
      //echo json_encode($_POST); die();
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $details = $this->api->get_trip_booking_details($ticket_id);
      $update_data = array(
        "payment_mode" => 'cod',
        'boarding_code' => $details['unique_id'].'_'.$details['ticket_id'],
      );
      //echo json_encode($update_data); die();
      if($this->api->update_booking_details($update_data, $ticket_id)) {
        //if ownward trip has return trip - get return trip details.
        $return_details = array();
        if($details['return_trip_id'] > 0) {
          $return_details =  $this->api->get_trip_booking_details($details['return_trip_id']);
          $update_data = array(
            "payment_mode" => 'cod',
            "boarding_code" => $return_details['unique_id'].'_'.$return_details['ticket_id'],
          );
          $this->api->update_booking_details($update_data, $return_details['ticket_id']);
        }
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-bus/buyer-upcoming-trips');
    }
    public function buyer_single_ticket_mark_as_cod()
    {
      //echo json_encode($_POST); die();
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $details = $this->api->get_trip_booking_details($ticket_id);
      $update_data = array(
        "payment_mode" => 'cod',
        'boarding_code' => $details['unique_id'].'_'.$details['ticket_id'],
      );
      //echo json_encode($update_data); die();
      if($this->api->update_booking_details($update_data, $ticket_id)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
      redirect('user-panel-bus/buyer-upcoming-trips');
    }
    public function buyer_ticket_confirm_payment()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);
      $today = date('Y-m-d h:i:s');
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      //echo json_encode($ticket_details); die();

      $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
      $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
      //echo json_encode($country_id); die();

      $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
      //echo json_encode($gonagoo_commission_percentage); die();
      $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

      $total_amount_paid = $ticket_details['ticket_price'];
      $transfer_account_number = "NULL";
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = $this->input->post('stripeEmail', TRUE);
      $mobile_number = "NULL";    

      $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
      $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
      $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
      $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
      
      //Send Push Notifications
      //Get PN API Keys
      $api_key = $this->config->item('delivererAppGoogleKey');
      $device_details_customer = $this->api->get_user_device_details($cust_id);
      if(!is_null($device_details_customer)) {
        $arr_customer_fcm_ids = array();
        $arr_customer_apn_ids = array();
        foreach ($device_details_customer as $value) {
          if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
        }
        $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
        $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

        //Push Notification APN
        $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
        if(is_array($arr_customer_apn_ids)) { 
          $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
          $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
        }
      }

      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => "web",
        "complete_paid" => 1,
        'boarding_code' => $ticket_details['unique_id'].'_'.$ticket_details['ticket_id'],
      );
      //echo json_encode($update_data); die();

      if($this->api->update_booking_details($update_data, $ticket_id)) {
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);

        //Update to workroom
          $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
          $workroom_update = array(
            'order_id' => $ticket_details['ticket_id'],
            'cust_id' => $ticket_details['cust_id'],
            'deliverer_id' => $ticket_details['operator_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $ticket_details['cust_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $ticket_details['cust_name'],
            'deliverer_name' => $ticket_details['operator_name'],
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 281
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);
        /*************************************** Payment Section ****************************************/
          //Add Ownward Trip Payment to customer account + History
          if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Deduct Ownward Trip Payment from customer account + History
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add Ownward Trip Payment to trip operator account + History
          if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => trim($ticket_details['operator_id']),
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          /* Ownward ticket invoice---------------------------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);
            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);    
            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "ticket-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* -------------------------------------------------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* Generate Ticket pdf --------------------------------------------------------------------- */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* Generate Ticket pdf --------------------------------------------------------------------- */
            
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */
          
          //Deduct Ownward Trip Commission From Operator Account
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($ownward_trip_commission),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add Ownward Trip Commission To Gonagoo Account
          if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "type" => 1,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($ownward_trip_commission),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($ticket_details['currency_sign']),
            "country_id" => trim($ownward_trip_operator_detail['country_id']),
            "cat_id" => trim($ticket_details['cat_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          
          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */

          //Return Trip Payment Calculation--------------------------------------
            if($ticket_details['return_trip_id'] > 0) {
              $return_ticket_details = $this->api->get_trip_booking_details($ticket_details['return_trip_id']);

              $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
              $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($return_ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
              $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);
              
              
              $return_update_data = array(
                "payment_method" => trim($payment_method),
                "paid_amount" => trim($return_ticket_details['ticket_price']),
                "balance_amount" => 0,
                "transaction_id" => trim($transaction_id),
                "payment_datetime" => $today,
                "payment_mode" => "online",
                "payment_by" => "web",
                "complete_paid" => 1,
                'boarding_code' => $return_ticket_details['unique_id'].'_'.$return_ticket_details['ticket_id'],
              );
              $this->api->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);
              //Update Gonagoo commission for ticket in seat details table
              $seat_update_data = array(
                "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
              );
              $this->api->update_booking_seat_details($seat_update_data, $return_ticket_details['ticket_id']);
              //Update to Workroom
              $workroom_update = array(
                'order_id' => $return_ticket_details['ticket_id'],
                'cust_id' => $return_ticket_details['cust_id'],
                'deliverer_id' => $return_ticket_details['operator_id'],
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $return_ticket_details['cust_id'],
                'type' => 'payment',
                'file_type' => 'text',
                'cust_name' => $return_ticket_details['cust_name'],
                'deliverer_name' => $return_ticket_details['operator_name'],
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 281
              );
              $this->api->add_bus_chat_to_workroom($workroom_update);

              //Add Return Trip Payment to customer account + History
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$return_ticket_details['ticket_id'],
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($return_ticket_details['ticket_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              //Deduct return Trip Payment from customer account + History
              $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_ticket_details['ticket_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)$return_ticket_details['ticket_id'],
                "user_id" => (int)$cust_id,
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'payment',
                "amount" => trim($return_ticket_details['ticket_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              //Add Return Trip Payment to trip operator account + History
              if($this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
              } else {
                $insert_data_customer_master = array(
                  "user_id" => trim($return_ticket_details['operator_id']),
                  "account_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                );
                $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
              }
              $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)trim($return_ticket_details['ticket_id']),
                "user_id" => (int)trim($return_ticket_details['operator_id']),
                "datetime" => $today,
                "type" => 1,
                "transaction_type" => 'add',
                "amount" => trim($return_ticket_details['ticket_price']),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              /* Return ticket invoice---------------------------------------------- */
                $return_trip_operator_detail = $this->api->get_user_details($return_ticket_details['operator_id']);

                $country_code = $this->api->get_country_code_by_id($return_trip_operator_detail['country_id']);
                $message = $this->lang->line('Payment recieved against ticket ID - ').$return_ticket_details['ticket_id'];
                $this->api->sendSMS((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);

                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($user_details['email1']));
                  $username = $parts[0];
                  $customer_name = $username;
                } else {
                  $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $user_country = $this->api->get_country_details(trim($user_details['country_id']));
                $user_state = $this->api->get_state_details(trim($user_details['state_id']));
                $user_city = $this->api->get_city_details(trim($user_details['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

                //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
                $rate = round(trim($return_ticket_details['ticket_price']),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                $workroom_invoice_url = "ticket-invoices/".$pdf_name;
                //Payment Invoice Update to workroom
                $workroom_update = array(
                  'order_id' => $return_ticket_details['ticket_id'],
                  'cust_id' => $return_ticket_details['cust_id'],
                  'deliverer_id' => $return_ticket_details['operator_id'],
                  'text_msg' => $this->lang->line('Invoice'),
                  'attachment_url' =>  $workroom_invoice_url,
                  'cre_datetime' => $today,
                  'sender_id' => $return_ticket_details['operator_id'],
                  'type' => 'raise_invoice',
                  'file_type' => 'pdf',
                  'cust_name' => $return_ticket_details['cust_name'],
                  'deliverer_name' => $return_ticket_details['operator_name'],
                  'thumbnail' => 'NULL',
                  'ratings' => 'NULL',
                  'cat_id' => 281
                );
                $this->api->add_bus_chat_to_workroom($workroom_update);
              /* -------------------------------------------------------------------- */
              
              /* -----------------Email Invoice to customer-------------------------- */
                $emailBody = $this->lang->line('Please download your ticket payment invoice.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
                $emailAddress = $user_details['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to customer-------------------- */
            
              /* -------------------------Return Ticket pdf ------------------------- */
                $return_operator_details = $this->api->get_bus_operator_profile($return_ticket_details['operator_id']);
                if($return_operator_details["avatar_url"]=="" || $return_operator_details["avatar_url"] == "NULL"){
                  $operator_avtr = base_url("resources/no-image.jpg");
                }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
                require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                  QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
                   $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
                   $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
                  if(rename($img_src, $img_dest));
                $html ='
                <page format="100x100" orientation="L" style="font: arial;">';
                $html .= '
                <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                    <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                    <div style="margin-top:27px">
                      <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
                      <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
                    </div>
                  </div>
                  <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                      <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                      <br />
                      <h6>'.$this->lang->line("slider_heading1").'</h6>
                  </div>                
                </div>
                <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                  <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                    '.$this->lang->line("Ownward_Trip").'<br/>
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
                  <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                  <h6 style="margin-top:-10px;">
                  <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
                  <h6 style="margin-top:-8px;"> 
                  <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                  $ticket_seat_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);

                  $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                  foreach ($ticket_seat_details as $seat)
                  {
                    $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                    if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                      
                     $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                  }
                $html .='</tbody></table></div></page>';            
                $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
                require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
                $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
                $html2pdf->pdf->SetDisplayMode('default');
                $html2pdf->writeHTML($html);
                $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
                $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
                rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
                //whatsapp api send ticket
                $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
                $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
                //echo json_encode($result); die();
              /* -------------------------Return Ticket pdf ------------------------- */
              
              /* ---------------------Email Ticket to customer----------------------- */
                $emailBody = $this->lang->line('Please download your e-ticket.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                $emailAddress = $user_details['email1'];
                $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                $fileName = $my_ticket_pdf;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* ---------------------Email Ticket to customer----------------------- */

              //Deduct Return Trip Commission From Operator Account
              $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

              $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
              $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
              $update_data_account_history = array(
                "account_id" => (int)$customer_account_master_details['account_id'],
                "order_id" => (int)trim($return_ticket_details['ticket_id']),
                "user_id" => (int)trim($return_ticket_details['operator_id']),
                "datetime" => $today,
                "type" => 0,
                "transaction_type" => 'ticket_commission',
                "amount" => trim($return_trip_commission),
                "account_balance" => trim($customer_account_master_details['account_balance']),
                "withdraw_request_id" => 0,
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "cat_id" => 281
              );
              $this->api->insert_payment_in_account_history($update_data_account_history);

              //Add Return Trip Commission To Gonagoo Account
              if($this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
              } else {
                $insert_data_gonagoo_master = array(
                  "gonagoo_balance" => 0,
                  "update_datetime" => $today,
                  "operation_lock" => 1,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                );
                $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
              }

              $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
              $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

              $update_data_gonagoo_history = array(
                "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                "order_id" => (int)trim($return_ticket_details['ticket_id']),
                "user_id" => (int)trim($return_ticket_details['operator_id']),
                "type" => 1,
                "transaction_type" => 'ticket_commission',
                "amount" => trim($return_trip_commission),
                "datetime" => $today,
                "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                "transfer_account_type" => trim($payment_method),
                "transfer_account_number" => trim($transfer_account_number),
                "bank_name" => trim($bank_name),
                "account_holder_name" => trim($account_holder_name),
                "iban" => trim($iban),
                "email_address" => trim($email_address),
                "mobile_number" => trim($mobile_number),
                "transaction_id" => trim($transaction_id),
                "currency_code" => trim($return_ticket_details['currency_sign']),
                "country_id" => trim($return_trip_operator_detail['country_id']),
                "cat_id" => trim($return_ticket_details['cat_id']),
              );
              $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);

              /* Return ticket commission invoice----------------------------------- */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
                $rate = round(trim($return_trip_commission),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Commission Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "commission_invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
              /* -------------------------------------------------------------------- */
              
              /* -----------------------Email Invoice to operator-------------------- */
                $emailBody = $this->lang->line('Commission Invoice');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Commission Invoice');
                $emailAddress = $return_trip_operator_detail['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to operator-------------------- */
            }
          //Return Trip Payment Calculation--------------------------------------
        /*************************************** Payment Section ****************************************/
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      redirect('user-panel-bus/buyer-upcoming-trips');
    }
    public function buyer_single_ticket_confirm_payment()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $payment_method = $this->input->post('payment_method', TRUE);
      $transaction_id = $this->input->post('stripeToken', TRUE);
      $ticket_price = $this->input->post('ticket_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);
      $today = date('Y-m-d h:i:s');
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      //echo json_encode($ticket_details); die();

      $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
      $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
      //echo json_encode($country_id); die();

      $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
      //echo json_encode($gonagoo_commission_percentage); die();
      $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

      $total_amount_paid = $ticket_details['ticket_price'];
      $transfer_account_number = "NULL";
      $bank_name = "NULL";
      $account_holder_name = "NULL";
      $iban = "NULL";
      $email_address = $this->input->post('stripeEmail', TRUE);
      $mobile_number = "NULL";    

      $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
      $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
      $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
      $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

      //Send Push Notifications
      //Get PN API Keys
      $api_key = $this->config->item('delivererAppGoogleKey');
      $device_details_customer = $this->api->get_user_device_details($cust_id);
      if(!is_null($device_details_customer)) {
        $arr_customer_fcm_ids = array();
        $arr_customer_apn_ids = array();
        foreach ($device_details_customer as $value) {
          if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
        }
        $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
        $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

        //Push Notification APN
        $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
        if(is_array($arr_customer_apn_ids)) { 
          $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
          $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
        }
      }

      $update_data = array(
        "payment_method" => trim($payment_method),
        "paid_amount" => trim($total_amount_paid),
        "balance_amount" => 0,
        "transaction_id" => trim($transaction_id),
        "payment_datetime" => $today,
        "payment_mode" => "online",
        "payment_by" => "web",
        "complete_paid" => 1,
        'boarding_code' => $ticket_details['unique_id'].'_'.$ticket_details['ticket_id'],
      );
      //echo json_encode($update_data); die();

      if($this->api->update_booking_details($update_data, $ticket_id)) {
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        //Update to workroom
        $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
        $workroom_update = array(
          'order_id' => $ticket_details['ticket_id'],
          'cust_id' => $ticket_details['cust_id'],
          'deliverer_id' => $ticket_details['operator_id'],
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $ticket_details['cust_id'],
          'type' => 'payment',
          'file_type' => 'text',
          'cust_name' => $ticket_details['cust_name'],
          'deliverer_name' => $ticket_details['operator_name'],
          'thumbnail' => 'NULL',
          'ratings' => 'NULL',
          'cat_id' => 281
        );
        $this->api->add_bus_chat_to_workroom($workroom_update);
        /*************************************** Payment Section ****************************************/
          //Add Ownward Trip Payment to customer account + History
          if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => $cust_id,
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Deduct Ownward Trip Payment from customer account + History
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)$cust_id,
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'payment',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add Ownward Trip Payment to trip operator account + History
          if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => trim($ticket_details['operator_id']),
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'add',
            "amount" => trim($ticket_details['ticket_price']),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          /* ------------------Ownward ticket invoice---------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));

            //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items

            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            $workroom_invoice_url = "ticket-invoices/".$pdf_name;
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  $workroom_invoice_url,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* ------------------Ownward ticket invoice---------------------------- */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */

          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

          //Deduct Ownward Trip Commission From Operator Account
          $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($ownward_trip_commission),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add Ownward Trip Commission To Gonagoo Account
          if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
          } else {
            $insert_data_gonagoo_master = array(
              "gonagoo_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
          }

          $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
          $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
          $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

          $update_data_gonagoo_history = array(
            "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "type" => 1,
            "transaction_type" => 'ticket_commission',
            "amount" => trim($ownward_trip_commission),
            "datetime" => $today,
            "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
            "transfer_account_type" => trim($payment_method),
            "transfer_account_number" => trim($transfer_account_number),
            "bank_name" => trim($bank_name),
            "account_holder_name" => trim($account_holder_name),
            "iban" => trim($iban),
            "email_address" => trim($email_address),
            "mobile_number" => trim($mobile_number),
            "transaction_id" => trim($transaction_id),
            "currency_code" => trim($ticket_details['currency_sign']),
            "country_id" => trim($ownward_trip_operator_detail['country_id']),
            "cat_id" => trim($ticket_details['cat_id']),
          );
          $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          
          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        /*************************************** Payment Section ****************************************/
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
      redirect('user-panel-bus/buyer-upcoming-trips');
    }
    public function cancel_ticket_buyer()
    {
      //echo json_encode($_POST); die();
      $ticket_id = $this->input->post('id', TRUE); $ticket_id = (int) $ticket_id;
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $today =date('Y-m-d H:i:s');

      if($ticket_details['complete_paid'] == "1") {
        //echo json_encode($ticket_details); die();

        $t = strtotime($ticket_details['journey_date']." ".$ticket_details['trip_start_date_time'].":00") - strtotime(date('Y-m-d H:i:s')); 
        $t = $t/3600;
        $cancellation_charge = 0; 
        $vehicle_type = $this->api->get_operator_bus_details($ticket_details['bus_id']);
        $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
        $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
        $cancellation = $this->api->get_cancellation_charges_by_country($ticket_details['operator_id'], $vehicle_type['vehical_type_id'],(int)$country_id);
        //echo json_encode($cancellation); die();
        foreach($cancellation as $cancel) {
          if($cancel['bcr_min_hours'] < round($t) && $cancel['bcr_max_hours'] > round($t)) {
            $cancellation_charge = $cancel['bcr_cancellation'];
          }
        }
        $charge = round(($ticket_details['ticket_price']/100) * $cancellation_charge,2);
        $return_amount = $ticket_details['ticket_price'] - $charge;
        //echo $return_amount; die();
        
        if($ticket_details['payment_mode'] == 'online') {
          //Deduct cancellation amount from operator account after charging
          if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
            $operator_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => trim($ticket_details['operator_id']),
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $operator_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($operator_account_master_details['account_balance']) - trim($return_amount);
          $this->api->update_payment_in_customer_master($operator_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance, $today);
          $operator_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$operator_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['operator_id']),
            "datetime" => $today,
            "type" => 0,
            "transaction_type" => 'cancel_refund',
            "amount" => trim($return_amount),
            "account_balance" => trim($operator_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);

          //Add refund amount to customer account
          if($this->api->customer_account_master_details(trim($ticket_details['cust_id']), trim($ticket_details['currency_sign']))) {
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['cust_id']), trim($ticket_details['currency_sign']));
          } else {
            $insert_data_customer_master = array(
              "user_id" => trim($ticket_details['cust_id']),
              "account_balance" => 0,
              "update_datetime" => $today,
              "operation_lock" => 1,
              "currency_code" => trim($ticket_details['currency_sign']),
            );
            $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['cust_id']), trim($ticket_details['currency_sign']));
          }
          $account_balance = trim($customer_account_master_details['account_balance']) + $return_amount;
          $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['cust_id']), $account_balance, date('Y-m-d H:i:s'));
          $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['cust_id']), trim($ticket_details['currency_sign']));
          $update_data_account_history = array(
            "account_id" => (int)$customer_account_master_details['account_id'],
            "order_id" => (int)$ticket_id,
            "user_id" => (int)trim($ticket_details['cust_id']),
            "datetime" => $today,
            "type" => 1,
            "transaction_type" => 'cancel_refund',
            "amount" => trim($return_amount),
            "account_balance" => trim($customer_account_master_details['account_balance']),
            "withdraw_request_id" => 0,
            "currency_code" => trim($ticket_details['currency_sign']),
            "cat_id" => 281
          );
          $this->api->insert_payment_in_account_history($update_data_account_history);
        }

        /* ticket cancel refund invoice---------------------------------------- */
          // $trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
          // if( trim($trip_operator_detail['firstname']) == 'NULL' || trim($trip_operator_detail['lastname'] == 'NULL') ) {
          //   $parts = explode("@", trim($trip_operator_detail['email1']));
          //   $username = $parts[0];
          //   $operator_name = $username;
          // } else {
          //   $operator_name = trim($trip_operator_detail['firstname']) . " " . trim($trip_operator_detail['lastname']);
          // }

          // $user_details = $this->api->get_user_details($ticket_details['cust_id']);
          // if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
          //   $parts = explode("@", trim($user_details['email1']));
          //   $username = $parts[0];
          //   $customer_name = $username;
          // } else {
          //   $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
          // }

          // $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
          // require_once($phpinvoice);
          // //Language configuration for invoice
          // $lang = $this->input->post('lang', TRUE);
          // if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
          // else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
          // else { $invoice_lang = "englishApi_lang"; }
          // //Invoice Configuration
          // $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
          // $invoice->setLogo("resources/fpdf-master/invoice-header.png");
          // $invoice->setColor("#000");
          // $invoice->setType("");
          // $invoice->setReference($ticket_details['ticket_id']);
          // $invoice->setDate(date('M dS ,Y',time()));

          // $operator_country = $this->api->get_country_details(trim($trip_operator_detail['country_id']));
          // $operator_state = $this->api->get_state_details(trim($trip_operator_detail['state_id']));
          // $operator_city = $this->api->get_city_details(trim($trip_operator_detail['city_id']));

          // $user_country = $this->api->get_country_details(trim($user_details['country_id']));
          // $user_state = $this->api->get_state_details(trim($user_details['state_id']));
          // $user_city = $this->api->get_city_details(trim($user_details['city_id']));

          // $gonagoo_address = $this->api->get_gonagoo_address(trim($trip_operator_detail['country_id']));
          // $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
          // $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

          // $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($trip_operator_detail['email1'])));

          // $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));
          
          // $eol = PHP_EOL;
          // /* Adding Items in table */
          // $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Refund Payment');
          // $rate = round(trim($return_amount),2);
          // $total = $rate;
          // $payment_datetime = substr($today, 0, 10);
          // //set items

          // $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

          // /* Add totals */
          // $invoice->addTotal($this->lang->line('sub_total'),$total);
          // $invoice->addTotal($this->lang->line('taxes'),'0');
          // $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
          // /* Set badge */ 
          // $invoice->addBadge($this->lang->line('Refund Payment Paid'));
          // /* Add title */
          // $invoice->addTitle($this->lang->line('tnc'));
          // /* Add Paragraph */
          // $invoice->addParagraph($gonagoo_address['terms']);
          // /* Add title */
          // $invoice->addTitle($this->lang->line('payment_dtls'));
          // /* Add Paragraph */
          // $invoice->addParagraph($gonagoo_address['payment_details']);
          // /* Add title */
          // $invoice->addTitleFooter($this->lang->line('thank_you_order'));
          // /* Add Paragraph */
          // $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
          // /* Set footer note */
          // $invoice->setFooternote($gonagoo_address['company_name']);
          // /* Render */
          // $invoice->render($ticket_details['ticket_id'].'_cancel_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
          // //Update File path
          // $pdf_name = $ticket_details['ticket_id'].'_cancel_trip.pdf';
          // //Move file to upload folder
          // rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

          // //Update Refund Invoice
          // $update_data = array(
          //   'refund_invoice_url' => "ticket-invoices/".$pdf_name
          // );
          // $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
        /* -------------------------------------------------------------------- */
        /* -----------------Email Invoice to customer-------------------------- */
          // $messageBody = $eol . $this->lang->line('Cancel Ticket Refund Invoice') . $eol;
          // $file = "resources/ticket-invoices/".$pdf_name;
          // $file_size = filesize($file);
          // $handle = fopen($file, "r");
          // $content = fread($handle, $file_size);
          // fclose($handle);
          // $content = chunk_split(base64_encode($content));
          // $uid = md5(uniqid(time()));
          // $name = basename($file);
          // //$eol = PHP_EOL;
          // $from_mail = $this->config->item('from_email');
          // $replyto = $this->config->item('from_email');
          // // Basic headers
          // $header = "From: ".$gonagoo_address['company_name']." <".$from_mail.">".$eol;
          // $header .= "Reply-To: ".$replyto.$eol;
          // $header .= "MIME-Version: 1.0\r\n";
          // $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";
          // // Put everything else in $message
          // $message = "--".$uid.$eol;
          // $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
          // $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
          // $message .= $messageBody.$eol;
          // $message .= "--".$uid.$eol;
          // $message .= "Content-Type: application/pdf; name=\"".$pdf_name."\"".$eol;
          // $message .= "Content-Transfer-Encoding: base64".$eol;
          // $message .= "Content-Disposition: attachment; filename=\"".$pdf_name."\"".$eol;
          // $message .= $content.$eol;
          // $message .= "--".$uid."--";
          // mail($user_details['email1'], $this->lang->line('Ownward Ticket Invoice'), $message, $header);
        /* -----------------------Email Invoice to customer-------------------- */

        $update_seat  = array('ticket_status' => 'cancelled', 'cancelled_by' => 'customer', 'cancelled_date' => $today);
        $seat_details = $this->api->get_std_id_of_seat_details($ticket_details['ticket_id']);
        foreach ($seat_details as $value){
          $this->api->update_bus_booking_seat_details($update_seat, $value['seat_id']);
        }
        $update_booking  = array('ticket_status' => 'cancelled', 'is_return' => 0, 'return_trip_id' => 0, 'cancelled_by' => 'customer', 'cancelled_date' => $today, 'refund_amount' => $return_amount);
        if($this->api->update_booking_details($update_booking, $ticket_details['ticket_id'])){ 
          if($ticket_details['return_trip_id'] > 0) {
            $update_return_booking  = array('is_return' => 0, 'return_trip_id' => 0);
            $this->api->update_booking_details($update_return_booking, $ticket_details['return_trip_id']);
          }

          $user_details = $this->api->get_user_details($ticket_details['cust_id']);
          //Send cancellation SMS
          $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
          $message = $this->lang->line('Your booking has been cancelled. Ticket ID - ').$ticket_id;
          $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
          
          //Email Ticket cancellation confirmation Customer name, customer Email, Subject, Message
          if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
            $parts = explode("@", trim($user_details['email1']));
            $username = $parts[0];
            $customer_name = $username;
          } else {
            $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
          }
          $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Ticket booking cancellation'), trim($message));

          //Send Push Notifications
          //Get PN API Keys
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($ticket_details['cust_id']);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Ticket cencellation'), 'type' => 'ticket-cencellation', 'notice_date' => $today, 'desc' => $message);
            $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

            //Push Notification APN
            $msg_apn_customer =  array('title' => $this->lang->line('Ticket cencellation'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }

          //Update to workroom
          $message = $this->lang->line('Your ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line(' has been cancelled.');
          $workroom_update = array(
            'order_id' => $ticket_details['ticket_id'],
            'cust_id' => $ticket_details['cust_id'],
            'deliverer_id' => $ticket_details['operator_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $ticket_details['cust_id'],
            'type' => 'ticket_cancel',
            'file_type' => 'text',
            'cust_name' => $ticket_details['cust_name'],
            'deliverer_name' => $ticket_details['operator_name'],
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 281
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);

          $cancelled_trip = array(
            'trip_id' => $ticket_details['trip_id'],
            'cancelled_date' => $ticket_details['journey_date'],
            'cancellation_time' => $today,
            'unique_id' => $ticket_details['unique_id'],
          );
          $this->api->insert_cancelled_trips($cancelled_trip);

          echo "cancel_successfull"; 
        } else {
          echo "delete_failed";
        } 
      } else {
        //echo json_encode($_POST); die();
        $r = $this->api->delete_bus_ticket_booking_master((int)$ticket_id);
        if($r == "ok") {
          $r1 =  $this->api->delete_bus_ticket_booking_seat((int)$ticket_id);
          //Make return trip as ownward trip
          if($ticket_details['return_trip_id'] > 0) {
            $update_return_booking  = array('is_return' => 0, 'return_trip_id' => 0);
            $this->api->update_booking_details($update_return_booking, $ticket_details['return_trip_id']);
          }
          if($r1 == "ok") {
            $user_details = $this->api->get_user_details($ticket_details['cust_id']);
            //Send cancellation SMS
            $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
            $message = $this->lang->line('Your booking has been cancelled. Ticket ID - ').$ticket_id;
            $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
            
            //Email Ticket cancellation confirmation Customer name, customer Email, Subject, Message
            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }
            $this->api_sms->send_email_text($customer_name, trim($user_details['email1']), $this->lang->line('Ticket booking cancellation'), trim($message));

            //Send Push Notifications
            //Get PN API Keys
            $api_key = $this->config->item('delivererAppGoogleKey');
            $device_details_customer = $this->api->get_user_device_details($ticket_details['cust_id']);
            if(!is_null($device_details_customer)) {
              $arr_customer_fcm_ids = array();
              $arr_customer_apn_ids = array();
              foreach ($device_details_customer as $value) {
                if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
              }
              $msg = array('title' => $this->lang->line('Ticket cencellation'), 'type' => 'ticket-cencellation', 'notice_date' => $today, 'desc' => $message);
              $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

              //Push Notification APN
              $msg_apn_customer =  array('title' => $this->lang->line('Ticket cencellation'), 'text' => $message);
              if(is_array($arr_customer_apn_ids)) { 
                $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
              }
            }

            //Update to workroom
            $message = $this->lang->line('Your ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line(' has been cancelled.');
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $message,
              'attachment_url' =>  'NULL',
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['cust_id'],
              'type' => 'ticket_cancel',
              'file_type' => 'text',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
            echo "cancel_successfull";
          }
          $cancelled_trip = array(
            'trip_id' => $ticket_details['trip_id'],
            'cancelled_date' => $ticket_details['journey_date'],
            'cancellation_time' => $today,
            'unique_id' => $ticket_details['unique_id'],
          );
          $this->api->insert_cancelled_trips($cancelled_trip);
        } else {
          echo "delete_failed";
        }
      }
    }
    public function buyer_ticket_orange_payment()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $payment_method = $this->input->post('payment_method', TRUE);
      $ticket_price = $this->input->post('ticket_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);

      $ticket_payment_id = 'gonagoo_'.time().'_'.$ticket_id;

      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "merchant_key" => "a8f8c61e", 
                            "currency" => "OUV", 
                            "order_id" => $ticket_payment_id, 
                            "amount" => "1",  
                            "return_url" => base_url('user-panel-bus/orange-payment-process'), 
                            "cancel_url" => base_url('user-panel-bus/orange-payment-process'), 
                            "notif_url" => base_url('test.php'), 
                            "lang" => "en", 
                            "reference" => $this->lang->line('Gonagoo - Service Payment') 
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      //echo json_encode($url_details); die();
      if(isset($url_details)) {
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('ticket_id', $ticket_id);
        $this->session->set_userdata('ticket_payment_id', $ticket_payment_id);
        $this->session->set_userdata('amount', 1);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-bus/buyer-upcoming-trips');
      }
    }
    public function orange_payment_process()
    {
      $session_token = $_SESSION['notif_token'];
      $ticket_id = $_SESSION['ticket_id'];
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $ticket_payment_id = $_SESSION['ticket_payment_id'];
      $amount = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "order_id" => $ticket_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      $transaction_id = $payment_details['txnid'];
      //echo json_encode($payment_details); die();

      //if($payment_details['status'] == 'SUCCESS' && $ticket_details['complete_paid'] == 0) {
      if(true) {
        $user_details = $this->api->get_user_details($ticket_details['cust_id']);
        $cust_id = (int)$user_details['cust_id'];
        $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
        $country_id = $this->api->get_country_id_by_name($location_details['country_name']);

        $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
        //echo json_encode($gonagoo_commission_percentage); die();
        $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

        $total_amount_paid = $ticket_details['ticket_price'];
        $transfer_account_number = "NULL";
        $bank_name = "NULL";
        $account_holder_name = "NULL";
        $iban = "NULL";
        $email_address = $this->input->post('stripeEmail', TRUE);
        $mobile_number = "NULL";    

        $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
        $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
        $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
        $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

        //Send Push Notifications
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_customer = $this->api->get_user_device_details($cust_id);

        if(!is_null($device_details_customer)) {
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
          $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

          //Push Notification APN
          $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }

        //update payment status in booking
        $trip_update = array(
          "payment_method" => 'orange',
          "paid_amount" => $amount,
          "balance_amount" => 0,
          "transaction_id" => $transaction_id,
          "payment_datetime" => $today,
          "payment_mode" => "online",
          "payment_by" => "web",
          "complete_paid" => 1,
          'boarding_code' => $ticket_details['unique_id'].'_'.$ticket_id,
        );
        $this->api->update_booking_details($trip_update, $ticket_id);
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        //Update to workroom---------------------------------------------------
          $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
          $workroom_update = array(
            'order_id' => $ticket_details['ticket_id'],
            'cust_id' => $ticket_details['cust_id'],
            'deliverer_id' => $ticket_details['operator_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $ticket_details['cust_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $ticket_details['cust_name'],
            'deliverer_name' => $ticket_details['operator_name'],
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 281
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);
        //---------------------------------------------------------------------
        /*************************************** Payment Section ****************************************/
          //Add Ownward Trip Payment to customer account + History-------------
            if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //-------------------------------------------------------------------

          //Deduct Ownward Trip Payment from customer account + History--------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //-------------------------------------------------------------------

          //Add Ownward Trip Payment to trip operator account + History--------
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //-------------------------------------------------------------------

          /* Ownward ticket invoice-------------------------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items

            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "ticket-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* ------------------------------------------------------------------ */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

          //Deduct Ownward Trip Commission From Operator Account---------------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //-------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account---------------------
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => 'orange',
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //-------------------------------------------------------------------

          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */

          //Return Trip Payment Calculation--------------------------------------
            if($ticket_details['return_trip_id'] > 0) {
              $return_ticket_details = $this->api->get_trip_booking_details($ticket_details['return_trip_id']);

              $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
              $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($return_ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
              $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
              $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

              //Send Push Notifications
              $api_key = $this->config->item('delivererAppGoogleKey');
              $device_details_customer = $this->api->get_user_device_details($cust_id);
              if(!is_null($device_details_customer)) {
                $arr_customer_fcm_ids = array();
                $arr_customer_apn_ids = array();
                foreach ($device_details_customer as $value) {
                  if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
                  else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
                }
                $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
                $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
                //Push Notification APN
                $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
                if(is_array($arr_customer_apn_ids)) { 
                  $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
                  $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
                }
              }
              //Update order payment details
              $return_update_data = array(
                "payment_method" => trim($payment_method),
                "paid_amount" => trim($return_ticket_details['ticket_price']),
                "balance_amount" => 0,
                "transaction_id" => trim($transaction_id),
                "payment_datetime" => $today,
                "payment_mode" => "online",
                "payment_by" => "web",
                "complete_paid" => 1,
                'boarding_code' => $return_ticket_details['unique_id'].'_'.$return_ticket_details['ticket_id'],
              );
              $this->api->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);
              //Update Gonagoo commission for ticket in seat details table
              $seat_update_data = array(
                "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
              );
              $this->api->update_booking_seat_details($seat_update_data, $return_ticket_details['ticket_id']);

              //Update to Workroom
              $workroom_update = array(
                'order_id' => $return_ticket_details['ticket_id'],
                'cust_id' => $return_ticket_details['cust_id'],
                'deliverer_id' => $return_ticket_details['operator_id'],
                'text_msg' => $message,
                'attachment_url' =>  'NULL',
                'cre_datetime' => $today,
                'sender_id' => $return_ticket_details['cust_id'],
                'type' => 'payment',
                'file_type' => 'text',
                'cust_name' => $return_ticket_details['cust_name'],
                'deliverer_name' => $return_ticket_details['operator_name'],
                'thumbnail' => 'NULL',
                'ratings' => 'NULL',
                'cat_id' => 281
              );
              $this->api->add_bus_chat_to_workroom($workroom_update);

              //Add Return Trip Payment to customer account + History--------------
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$return_ticket_details['ticket_id'],
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //-------------------------------------------------------------------

              //Deduct return Trip Payment from customer account + History---------
                $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)$return_ticket_details['ticket_id'],
                  "user_id" => (int)$cust_id,
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'payment',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //-------------------------------------------------------------------

              //Add Return Trip Payment to trip operator account + History---------
                if($this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_customer_master = array(
                    "user_id" => trim($return_ticket_details['operator_id']),
                    "account_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
                  $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                }
                $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 1,
                  "transaction_type" => 'add',
                  "amount" => trim($return_ticket_details['ticket_price']),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //-------------------------------------------------------------------

              /* Return ticket invoice--------------------------------------------- */
                $return_trip_operator_detail = $this->api->get_user_details($return_ticket_details['operator_id']);

                $country_code = $this->api->get_country_code_by_id($return_trip_operator_detail['country_id']);
                $message = $this->lang->line('Payment recieved against ticket ID - ').$return_ticket_details['ticket_id'];
                $this->api->sendSMS((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);

                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($user_details['email1']));
                  $username = $parts[0];
                  $customer_name = $username;
                } else {
                  $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $user_country = $this->api->get_country_details(trim($user_details['country_id']));
                $user_state = $this->api->get_state_details(trim($user_details['state_id']));
                $user_city = $this->api->get_city_details(trim($user_details['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));
                
                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
                $rate = round(trim($return_ticket_details['ticket_price']),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
                $workroom_invoice_url = "ticket-invoices/".$pdf_name;
                //Payment Invoice Update to workroom
                $workroom_update = array(
                  'order_id' => $return_ticket_details['ticket_id'],
                  'cust_id' => $return_ticket_details['cust_id'],
                  'deliverer_id' => $return_ticket_details['operator_id'],
                  'text_msg' => $this->lang->line('Invoice'),
                  'attachment_url' =>  $workroom_invoice_url,
                  'cre_datetime' => $today,
                  'sender_id' => $return_ticket_details['operator_id'],
                  'type' => 'raise_invoice',
                  'file_type' => 'pdf',
                  'cust_name' => $return_ticket_details['cust_name'],
                  'deliverer_name' => $return_ticket_details['operator_name'],
                  'thumbnail' => 'NULL',
                  'ratings' => 'NULL',
                  'cat_id' => 281
                );
                $this->api->add_bus_chat_to_workroom($workroom_update);
              /* ------------------------------------------------------------------ */
              
              /* -----------------Email Invoice to customer-------------------------- */
                $emailBody = $this->lang->line('Please download your ticket payment invoice.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
                $emailAddress = $user_details['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to customer-------------------- */
              
              /* ----------------------Return Ticket pdf --------------------------- */
                $return_operator_details = $this->api->get_bus_operator_profile($return_ticket_details['operator_id']);
                if($return_operator_details["avatar_url"]=="" || $return_operator_details["avatar_url"] == "NULL"){
                  $operator_avtr = base_url("resources/no-image.jpg");
                }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
                require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
                  QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
                   $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
                   $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
                  if(rename($img_src, $img_dest));
                $html ='
                <page format="100x100" orientation="L" style="font: arial;">';
                $html .= '
                <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                  <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                    <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                    <div style="margin-top:27px">
                      <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
                      <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
                    </div>
                  </div>
                  <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                      <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                      <br />
                      <h6>'.$this->lang->line("slider_heading1").'</h6>
                  </div>                
                </div>
                <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                  <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                    '.$this->lang->line("Ownward_Trip").'<br/>
                  <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
                  <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
                  <h6 style="margin-top:-10px;">
                  <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
                  <h6 style="margin-top:-8px;"> 
                  <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
                  <h6 style="margin-top:-8px;">
                  <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
                  $ticket_seat_details = $this->api->get_ticket_seat_details($return_ticket_details['ticket_id']);

                  $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
                  foreach ($ticket_seat_details as $seat)
                  {
                    $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                    if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                      
                     $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
                  }
                $html .='</tbody></table></div></page>';            
                $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
                require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
                $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
                $html2pdf->pdf->SetDisplayMode('default');
                $html2pdf->writeHTML($html);
                $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
                $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
                rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
                //whatsapp api send ticket
                $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
                $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
                //echo json_encode($result); die();
              /* ----------------------Return Ticket pdf --------------------------- */
              
              /* ---------------------Email Ticket to customer----------------------- */
                $emailBody = $this->lang->line('Please download your e-ticket.');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
                $emailAddress = $user_details['email1'];
                $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
                $fileName = $my_ticket_pdf;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* ---------------------Email Ticket to customer----------------------- */

              //Deduct Return Trip Commission From Operator Account----------------
                $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

                $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
                $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
                $customer_account_master_details = $this->api->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
                $update_data_account_history = array(
                  "account_id" => (int)$customer_account_master_details['account_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "datetime" => $today,
                  "type" => 0,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "account_balance" => trim($customer_account_master_details['account_balance']),
                  "withdraw_request_id" => 0,
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "cat_id" => 281
                );
                $this->api->insert_payment_in_account_history($update_data_account_history);
              //-------------------------------------------------------------------

              //Add Return Trip Commission To Gonagoo Account----------------------
                if($this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                } else {
                  $insert_data_gonagoo_master = array(
                    "gonagoo_balance" => 0,
                    "update_datetime" => $today,
                    "operation_lock" => 1,
                    "currency_code" => trim($return_ticket_details['currency_sign']),
                  );
                  $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
                  $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
                }

                $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
                $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
                $gonagoo_master_details = $this->api->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

                $update_data_gonagoo_history = array(
                  "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
                  "order_id" => (int)trim($return_ticket_details['ticket_id']),
                  "user_id" => (int)trim($return_ticket_details['operator_id']),
                  "type" => 1,
                  "transaction_type" => 'ticket_commission',
                  "amount" => trim($return_trip_commission),
                  "datetime" => $today,
                  "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
                  "transfer_account_type" => trim($payment_method),
                  "transfer_account_number" => trim($transfer_account_number),
                  "bank_name" => trim($bank_name),
                  "account_holder_name" => trim($account_holder_name),
                  "iban" => trim($iban),
                  "email_address" => trim($email_address),
                  "mobile_number" => trim($mobile_number),
                  "transaction_id" => trim($transaction_id),
                  "currency_code" => trim($return_ticket_details['currency_sign']),
                  "country_id" => trim($return_trip_operator_detail['country_id']),
                  "cat_id" => trim($return_ticket_details['cat_id']),
                );
                $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
              //-------------------------------------------------------------------

              /* Return ticket commission invoice----------------------------------- */
                if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
                  $parts = explode("@", trim($return_trip_operator_detail['email1']));
                  $username = $parts[0];
                  $operator_name = $username;
                } else {
                  $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
                }

                $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
                require_once($phpinvoice);
                //Language configuration for invoice
                $lang = $this->input->post('lang', TRUE);
                if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
                else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
                else { $invoice_lang = "englishApi_lang"; }
                //Invoice Configuration
                $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
                $invoice->setLogo("resources/fpdf-master/invoice-header.png");
                $invoice->setColor("#000");
                $invoice->setType("");
                $invoice->setReference($return_ticket_details['ticket_id']);
                $invoice->setDate(date('M dS ,Y',time()));

                $operator_country = $this->api->get_country_details(trim($return_trip_operator_detail['country_id']));
                $operator_state = $this->api->get_state_details(trim($return_trip_operator_detail['state_id']));
                $operator_city = $this->api->get_city_details(trim($return_trip_operator_detail['city_id']));

                $gonagoo_address = $this->api->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
                $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
                $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

                $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
                $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

                $eol = PHP_EOL;
                /* Adding Items in table */
                $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
                $rate = round(trim($return_trip_commission),2);
                $total = $rate;
                $payment_datetime = substr($today, 0, 10);
                //set items
                $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

                /* Add totals */
                $invoice->addTotal($this->lang->line('sub_total'),$total);
                $invoice->addTotal($this->lang->line('taxes'),'0');
                $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
                /* Set badge */ 
                $invoice->addBadge($this->lang->line('Commission Paid'));
                /* Add title */
                $invoice->addTitle($this->lang->line('tnc'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['terms']);
                /* Add title */
                $invoice->addTitle($this->lang->line('payment_dtls'));
                /* Add Paragraph */
                $invoice->addParagraph($gonagoo_address['payment_details']);
                /* Add title */
                $invoice->addTitleFooter($this->lang->line('thank_you_order'));
                /* Add Paragraph */
                $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
                /* Set footer note */
                $invoice->setFooternote($gonagoo_address['company_name']);
                /* Render */
                $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
                //Update File path
                $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
                //Move file to upload folder
                rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

                //Update Order Invoice
                $update_data = array(
                  "commission_invoice_url" => "ticket-invoices/".$pdf_name,
                );
                $this->api->update_booking_details($update_data, $return_ticket_details['ticket_id']);
              /* -------------------------------------------------------------------- */
              /* -----------------------Email Invoice to operator-------------------- */
                $emailBody = $this->lang->line('Commission Invoice');
                $gonagooAddress = $gonagoo_address['company_name'];
                $emailSubject = $this->lang->line('Commission Invoice');
                $emailAddress = $return_trip_operator_detail['email1'];
                $fileToAttach = "resources/ticket-invoices/$pdf_name";
                $fileName = $pdf_name;
                $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
              /* -----------------------Email Invoice to operator-------------------- */
            }
          //Return Trip Payment Calculation--------------------------------------
        /*************************************** Payment Section ****************************************/

        //unset all session data
        $this->session->unset_userdata('notif_token');
        $this->session->unset_userdata('ticket_id');
        $this->session->unset_userdata('ticket_payment_id');
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('pay_token');
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else {
        //unset all session data
        $this->session->unset_userdata('notif_token');
        $this->session->unset_userdata('ticket_id');
        $this->session->unset_userdata('ticket_payment_id');
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('pay_token');
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
      }
      redirect('user-panel-bus/buyer-upcoming-trips');
    }
    public function buyer_single_ticket_orange_payment()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int) $this->cust_id;
      $user_details = $this->api->get_user_details($cust_id);
      $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $payment_method = $this->input->post('payment_method', TRUE);
      $ticket_price = $this->input->post('ticket_price', TRUE);
      $currency_sign = $this->input->post('currency_sign', TRUE);

      $ticket_payment_id = 'gonagoo_'.time().'_'.$ticket_id;

      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "merchant_key" => "a8f8c61e", 
                            "currency" => "OUV", 
                            "order_id" => $ticket_payment_id, 
                            "amount" => "1",  
                            "return_url" => base_url('user-panel-bus/single-orange-payment-process'), 
                            "cancel_url" => base_url('user-panel-bus/single-orange-payment-process'), 
                            "notif_url" => base_url(), 
                            "lang" => "en", 
                            "reference" => $this->lang->line('Gonagoo - Service Payment') 
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $orange_url = curl_exec($ch);
      curl_close($ch);
      $url_details = json_decode($orange_url, TRUE);
      //echo json_encode($url_details); die();
      if(isset($url_details)) {
        $this->session->set_userdata('notif_token', $url_details['notif_token']);
        $this->session->set_userdata('ticket_id', $ticket_id);
        $this->session->set_userdata('ticket_payment_id', $ticket_payment_id);
        $this->session->set_userdata('amount', 1);
        $this->session->set_userdata('pay_token', $url_details['pay_token']);
        //echo json_encode($url_details); die();
        header('Location: '.$url_details['payment_url']);
      } else {
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
        redirect('user-panel-bus/buyer-upcoming-trips');
      }
    }
    public function single_orange_payment_process()
    {
      $session_token = $_SESSION['notif_token'];
      $ticket_id = $_SESSION['ticket_id'];
      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $ticket_payment_id = $_SESSION['ticket_payment_id'];
      $amount = $_SESSION['amount'];
      $pay_token = $_SESSION['pay_token'];
      $payment_method = 'orange';
      $today = date('Y-m-d h:i:s');
      //get access token
      $ch = curl_init("https://api.orange.com/oauth/v2/token?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
      $orange_token = curl_exec($ch);
      curl_close($ch);
      $token_details = json_decode($orange_token, TRUE);
      $token = 'Bearer '.$token_details['access_token'];

      // get payment link
      $json_post_data = json_encode(
                          array(
                            "order_id" => $ticket_payment_id, 
                            "amount" => $amount, 
                            "pay_token" => $pay_token
                          )
                        );
      //echo $json_post_data; die();
      $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: ".$token,
          "Accept: application/json"
      ));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
      $payment_res = curl_exec($ch);
      curl_close($ch);
      $payment_details = json_decode($payment_res, TRUE);
      $transaction_id = $payment_details['txnid'];
      //echo json_encode($payment_details); die();

      //if($payment_details['status'] == 'SUCCESS' && $ticket_details['complete_paid'] == 0) {
      if(true) {
        $user_details = $this->api->get_user_details($ticket_details['cust_id']);
        $cust_id = (int)$user_details['cust_id'];
        $location_details = $this->api->get_bus_location_details($ticket_details['source_point_id']);
        $country_id = $this->api->get_country_id_by_name($location_details['country_name']);

        $gonagoo_commission_percentage = $this->api->get_gonagoo_bus_commission_details((int)$country_id);
        //echo json_encode($gonagoo_commission_percentage); die();
        $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

        $total_amount_paid = $ticket_details['ticket_price'];
        $transfer_account_number = "NULL";
        $bank_name = "NULL";
        $account_holder_name = "NULL";
        $iban = "NULL";
        $email_address = $this->input->post('stripeEmail', TRUE);
        $mobile_number = "NULL";    

        $country_code = $this->api->get_country_code_by_id($user_details['country_id']);
        $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
        $this->api->sendSMS((int)$country_code.trim($user_details['mobile1']), $message);
        $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details['mobile1']), $message);

        //Send Push Notifications
        $api_key = $this->config->item('delivererAppGoogleKey');
        $device_details_customer = $this->api->get_user_device_details($cust_id);

        if(!is_null($device_details_customer)) {
          $arr_customer_fcm_ids = array();
          $arr_customer_apn_ids = array();
          foreach ($device_details_customer as $value) {
            if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
            else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
          }
          $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
          $this->api->sendFCM($msg, $arr_customer_fcm_ids, $api_key);

          //Push Notification APN
          $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
          if(is_array($arr_customer_apn_ids)) { 
            $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
            $this->api->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
          }
        }

        //update payment status in booking
        $trip_update = array(
          "payment_method" => 'orange',
          "paid_amount" => $amount,
          "balance_amount" => 0,
          "transaction_id" => $transaction_id,
          "payment_datetime" => $today,
          "payment_mode" => "online",
          "payment_by" => "web",
          "complete_paid" => 1,
          'boarding_code' => $ticket_details['unique_id'].'_'.$ticket_id,
        );
        $this->api->update_booking_details($trip_update, $ticket_id);
        //Update Gonagoo commission for ticket in seat details table
        $seat_update_data = array(
          "comm_percentage" => trim($gonagoo_commission_percentage['gonagoo_commission']),
        );
        $this->api->update_booking_seat_details($seat_update_data, $ticket_id);
        //Update to workroom---------------------------------------------------
          $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
          $workroom_update = array(
            'order_id' => $ticket_details['ticket_id'],
            'cust_id' => $ticket_details['cust_id'],
            'deliverer_id' => $ticket_details['operator_id'],
            'text_msg' => $message,
            'attachment_url' =>  'NULL',
            'cre_datetime' => $today,
            'sender_id' => $ticket_details['cust_id'],
            'type' => 'payment',
            'file_type' => 'text',
            'cust_name' => $ticket_details['cust_name'],
            'deliverer_name' => $ticket_details['operator_name'],
            'thumbnail' => 'NULL',
            'ratings' => 'NULL',
            'cat_id' => 281
          );
          $this->api->add_bus_chat_to_workroom($workroom_update);
        //---------------------------------------------------------------------
        /*************************************** Payment Section ****************************************/
          //Add Ownward Trip Payment to customer account + History-------------
            if($this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => $cust_id,
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //-------------------------------------------------------------------

          //Deduct Ownward Trip Payment from customer account + History--------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], $cust_id, $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details($cust_id, trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)$cust_id,
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'payment',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //-------------------------------------------------------------------

          //Add Ownward Trip Payment to trip operator account + History--------
            if($this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            } else {
              $insert_data_customer_master = array(
                "user_id" => trim($ticket_details['operator_id']),
                "account_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_customer_record($insert_data_customer_master);
              $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            }
            $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 1,
              "transaction_type" => 'add',
              "amount" => trim($ticket_details['ticket_price']),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //-------------------------------------------------------------------

          /* Ownward ticket invoice-------------------------------------------- */
            $ownward_trip_operator_detail = $this->api->get_user_details($ticket_details['operator_id']);
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            if( trim($user_details['firstname']) == 'NULL' || trim($user_details['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($user_details['email1']));
              $username = $parts[0];
              $customer_name = $username;
            } else {
              $customer_name = trim($user_details['firstname']) . " " . trim($user_details['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $user_country = $this->api->get_country_details(trim($user_details['country_id']));
            $user_state = $this->api->get_state_details(trim($user_details['state_id']));
            $user_city = $this->api->get_city_details(trim($user_details['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $invoice->setFrom(array($customer_name,trim($user_city['city_name']),trim($user_state['state_name']),trim($user_country['country_name']),trim($user_details['email1'])));
            
            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
            $rate = round(trim($ticket_details['ticket_price']),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items

            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
            //Payment Invoice Update to workroom
            $workroom_update = array(
              'order_id' => $ticket_details['ticket_id'],
              'cust_id' => $ticket_details['cust_id'],
              'deliverer_id' => $ticket_details['operator_id'],
              'text_msg' => $this->lang->line('Invoice'),
              'attachment_url' =>  "ticket-invoices/".$pdf_name,
              'cre_datetime' => $today,
              'sender_id' => $ticket_details['operator_id'],
              'type' => 'raise_invoice',
              'file_type' => 'pdf',
              'cust_name' => $ticket_details['cust_name'],
              'deliverer_name' => $ticket_details['operator_name'],
              'thumbnail' => 'NULL',
              'ratings' => 'NULL',
              'cat_id' => 281
            );
            $this->api->add_bus_chat_to_workroom($workroom_update);
          /* ------------------------------------------------------------------ */
          
          /* -----------------Email Invoice to customer-------------------------- */
            $emailBody = $this->lang->line('Please download your ticket payment invoice.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to customer-------------------- */
          
          /* ------------------------Generate Ticket pdf ------------------------ */
            $operator_details = $this->api->get_bus_operator_profile($ticket_details['operator_id']);
            if($operator_details["avatar_url"]=="" || $operator_details["avatar_url"] == "NULL"){
              $operator_avtr = base_url("resources/no-image.jpg");
            }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
            require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
              QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
               $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
               $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
              if(rename($img_src, $img_dest));
            $html ='
            <page format="100x100" orientation="L" style="font: arial;">';
            $html .= '
            <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
              <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
                <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
                <div style="margin-top:27px">
                  <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
                  <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
                </div>
              </div>
              <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
                  <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
                  <br />
                  <h6>'.$this->lang->line("slider_heading1").'</h6>
              </div>                
            </div>
            <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
              <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
                '.$this->lang->line("Ownward_Trip").'<br/>
              <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
              <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
              <h6 style="margin-top:-10px;">
              <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
              <h6 style="margin-top:-8px;"> 
              <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
              <h6 style="margin-top:-8px;">
              <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
              $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_details['ticket_id']);

              $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
              foreach ($ticket_seat_details as $seat)
              {
                $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
                if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
                 $html .='</td><td>'.$this->api->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
              }
            $html .='</tbody></table></div></page>';            
            $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
            require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
            $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
            $html2pdf->pdf->SetDisplayMode('default');
            $html2pdf->writeHTML($html);
            $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
            $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
            rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
            //whatsapp api send ticket
            $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
            $result =  $this->api_sms->send_pdf_whatsapp($country_code.trim($user_details['mobile1']) ,$ticket_location ,$my_ticket_pdf);
            //echo json_encode($result); die();
          /* ------------------------Generate Ticket pdf ------------------------ */
          
          /* ---------------------Email Ticket to customer----------------------- */
            $emailBody = $this->lang->line('Please download your e-ticket.');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
            $emailAddress = $user_details['email1'];
            $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
            $fileName = $my_ticket_pdf;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* ---------------------Email Ticket to customer----------------------- */

          //Deduct Ownward Trip Commission From Operator Account---------------
            $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
            $this->api->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
            $customer_account_master_details = $this->api->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
            $update_data_account_history = array(
              "account_id" => (int)$customer_account_master_details['account_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "datetime" => $today,
              "type" => 0,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "account_balance" => trim($customer_account_master_details['account_balance']),
              "withdraw_request_id" => 0,
              "currency_code" => trim($ticket_details['currency_sign']),
              "cat_id" => 281
            );
            $this->api->insert_payment_in_account_history($update_data_account_history);
          //-------------------------------------------------------------------

          //Add Ownward Trip Commission To Gonagoo Account---------------------
            if($this->api->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            } else {
              $insert_data_gonagoo_master = array(
                "gonagoo_balance" => 0,
                "update_datetime" => $today,
                "operation_lock" => 1,
                "currency_code" => trim($ticket_details['currency_sign']),
              );
              $this->api->insert_gonagoo_master_record($insert_data_gonagoo_master);
              $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));
            }

            $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
            $this->api->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
            $gonagoo_master_details = $this->api->gonagoo_master_details(trim($ticket_details['currency_sign']));

            $update_data_gonagoo_history = array(
              "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
              "order_id" => (int)$ticket_id,
              "user_id" => (int)trim($ticket_details['operator_id']),
              "type" => 1,
              "transaction_type" => 'ticket_commission',
              "amount" => trim($ownward_trip_commission),
              "datetime" => $today,
              "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
              "transfer_account_type" => trim($payment_method),
              "transfer_account_number" => trim($transfer_account_number),
              "bank_name" => trim($bank_name),
              "account_holder_name" => trim($account_holder_name),
              "iban" => trim($iban),
              "email_address" => trim($email_address),
              "mobile_number" => trim($mobile_number),
              "transaction_id" => trim($transaction_id),
              "currency_code" => trim($ticket_details['currency_sign']),
              "country_id" => trim($ownward_trip_operator_detail['country_id']),
              "cat_id" => trim($ticket_details['cat_id']),
            );
            $this->api->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
          //-------------------------------------------------------------------

          /* Ownward ticket commission invoice----------------------------------- */
            if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
              $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
              $username = $parts[0];
              $operator_name = $username;
            } else {
              $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
            }

            $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
            require_once($phpinvoice);
            //Language configuration for invoice
            $lang = $this->input->post('lang', TRUE);
            if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
            else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
            else { $invoice_lang = "englishApi_lang"; }
            //Invoice Configuration
            $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
            $invoice->setLogo("resources/fpdf-master/invoice-header.png");
            $invoice->setColor("#000");
            $invoice->setType("");
            $invoice->setReference($ticket_details['ticket_id']);
            $invoice->setDate(date('M dS ,Y',time()));

            $operator_country = $this->api->get_country_details(trim($ownward_trip_operator_detail['country_id']));
            $operator_state = $this->api->get_state_details(trim($ownward_trip_operator_detail['state_id']));
            $operator_city = $this->api->get_city_details(trim($ownward_trip_operator_detail['city_id']));

            $gonagoo_address = $this->api->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
            $gonagoo_country = $this->api->get_country_details(trim($gonagoo_address['country_id']));
            $gonagoo_city = $this->api->get_city_details(trim($gonagoo_address['city_id']));

            $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
            $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

            $eol = PHP_EOL;
            /* Adding Items in table */
            $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
            $rate = round(trim($ownward_trip_commission),2);
            $total = $rate;
            $payment_datetime = substr($today, 0, 10);
            //set items
            $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

            /* Add totals */
            $invoice->addTotal($this->lang->line('sub_total'),$total);
            $invoice->addTotal($this->lang->line('taxes'),'0');
            $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
            /* Set badge */ 
            $invoice->addBadge($this->lang->line('Commission Paid'));
            /* Add title */
            $invoice->addTitle($this->lang->line('tnc'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['terms']);
            /* Add title */
            $invoice->addTitle($this->lang->line('payment_dtls'));
            /* Add Paragraph */
            $invoice->addParagraph($gonagoo_address['payment_details']);
            /* Add title */
            $invoice->addTitleFooter($this->lang->line('thank_you_order'));
            /* Add Paragraph */
            $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
            /* Set footer note */
            $invoice->setFooternote($gonagoo_address['company_name']);
            /* Render */
            $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
            //Update File path
            $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
            //Move file to upload folder
            rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

            //Update Order Invoice
            $update_data = array(
              "commission_invoice_url" => "ticket-invoices/".$pdf_name,
            );
            $this->api->update_booking_details($update_data, $ticket_details['ticket_id']);
          /* -------------------------------------------------------------------- */
          /* -----------------------Email Invoice to operator-------------------- */
            $emailBody = $this->lang->line('Commission Invoice');
            $gonagooAddress = $gonagoo_address['company_name'];
            $emailSubject = $this->lang->line('Commission Invoice');
            $emailAddress = $ownward_trip_operator_detail['email1'];
            $fileToAttach = "resources/ticket-invoices/$pdf_name";
            $fileName = $pdf_name;
            $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
          /* -----------------------Email Invoice to operator-------------------- */
        /*************************************** Payment Section ****************************************/
        //unset all session data
        $this->session->unset_userdata('notif_token');
        $this->session->unset_userdata('ticket_id');
        $this->session->unset_userdata('ticket_payment_id');
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('pay_token');
        $this->session->set_flashdata('success', $this->lang->line('payment_completed'));
      } else {
        //unset all session data
        $this->session->unset_userdata('notif_token');
        $this->session->unset_userdata('ticket_id');
        $this->session->unset_userdata('ticket_payment_id');
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('pay_token');
        $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
      }
      redirect('user-panel-bus/buyer-upcoming-trips');
    }
  //End Buyer Trip Search, Booking and Payment-------------  
    
  //Buyer Trip List (Upcomming)----------------------------
    public function buyer_upcoming_trips()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $filter = $this->input->post('filter');
      if(isset($filter) && $filter == 'filter'){
        if(isset($_POST['trip_source'])) { $trip_source = $this->input->post('trip_source'); } else { $trip_source = ''; }
        if(isset($_POST['trip_destination'])) { $trip_destination = $this->input->post('trip_destination'); } else { $trip_destination = ''; }
        if(isset($_POST['journey_date'])) { $journey_date = $this->input->post('journey_date'); } else { $journey_date = ''; }
        $trip_list = $this->api->get_buyer_trip_master_list($cust_id, 'upcoming', $trip_source, $trip_destination, $journey_date);
      } else {
        $trip_list = $this->api->get_buyer_trip_master_list($cust_id, 'upcoming');
      }
      //echo $this->db->last_query(); die();
      //echo json_encode($trip_list); die();

      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_buyer_upcoming_trip_list_view', compact('trip_list','trip_sources','trip_destinations','trip_source','trip_destination','journey_date'));
      $this->load->view('user_panel/footer');
    }
    public function bus_upcoming_trip_details_buyer()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/buyer-upcoming-trips'); }

      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $trip_id = $ticket_details['trip_id'];
      $trip_details = $this->api->get_trip_master_details($ticket_details['trip_id']);
      $operator_details = $this->api->get_bus_operator_profile($trip_details[0]['cust_id']);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $trip_details[0]['cust_id']);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_details[0]['trip_id']);
      $src_dest_details = $this->api->get_trip_location_master($trip_details[0]['trip_id']);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_id);
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_details[0]['trip_id']);
      //echo json_encode($booking_details); die();
      $this->load->view('user_panel/bus_upcoming_trip_buyer_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details','ticket_seat_details'));
      $this->load->view('user_panel/footer');
    }
  //Buyer Trip List (Upcomming)----------------------------

  //Buyer Trip List (Current)------------------------------
    public function buyer_current_trips()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $filter = $this->input->post('filter');
      if(isset($filter) && $filter == 'filter'){
        if(isset($_POST['trip_source'])) { $trip_source = $this->input->post('trip_source'); } else { $trip_source = ''; }
        if(isset($_POST['trip_destination'])) { $trip_destination = $this->input->post('trip_destination'); } else { $trip_destination = ''; }
        if(isset($_POST['journey_date'])) { $journey_date = $this->input->post('journey_date'); } else { $journey_date = ''; }
        $trip_list = $this->api->get_buyer_trip_master_list($cust_id, 'current', $trip_source, $trip_destination, $journey_date);
      } else {
        $trip_list = $this->api->get_buyer_trip_master_list($cust_id, 'current');
      }
      //echo $this->db->last_query(); die();
      //echo json_encode($trip_list); die();

      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_buyer_current_trip_list_view', compact('trip_list','trip_sources','trip_destinations','trip_source','trip_destination','journey_date'));
      $this->load->view('user_panel/footer');
    }
    public function bus_current_trip_details_buyer()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/buyer-current-trips'); }

      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $trip_id = $ticket_details['trip_id'];
      $trip_details = $this->api->get_trip_master_details($ticket_details['trip_id']);
      $operator_details = $this->api->get_bus_operator_profile($trip_details[0]['cust_id']);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $trip_details[0]['cust_id']);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_details[0]['trip_id']);
      $src_dest_details = $this->api->get_trip_location_master($trip_details[0]['trip_id']);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_id);
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_details[0]['trip_id']);
      //echo json_encode($booking_details); die();
      $this->load->view('user_panel/bus_current_trip_buyer_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details','ticket_seat_details'));
      $this->load->view('user_panel/footer');
    }
  //Buyer Trip List (Current)------------------------------

  //Buyer Trip List (Previous)-----------------------------
    public function buyer_previous_trips()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $filter = $this->input->post('filter');
      if(isset($filter) && $filter == 'filter'){
        if(isset($_POST['trip_source'])) { $trip_source = $this->input->post('trip_source'); } else { $trip_source = ''; }
        if(isset($_POST['trip_destination'])) { $trip_destination = $this->input->post('trip_destination'); } else { $trip_destination = ''; }
        if(isset($_POST['journey_date'])) { $journey_date = $this->input->post('journey_date'); } else { $journey_date = ''; }
        $trip_list = $this->api->get_buyer_trip_master_list($cust_id, 'previous', $trip_source, $trip_destination, $journey_date);
      } else {
        $trip_list = $this->api->get_buyer_trip_master_list($cust_id, 'previous');
      }
      //echo $this->db->last_query(); die();
      //echo json_encode($trip_list); die();

      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_buyer_previous_trip_list_view', compact('trip_list','trip_sources','trip_destinations','trip_source','trip_destination','journey_date'));
      $this->load->view('user_panel/footer');
    }
    public function bus_previous_trip_details_buyer()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/buyer-previous-trips'); }

      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $trip_id = $ticket_details['trip_id'];
      $trip_details = $this->api->get_trip_master_details($ticket_details['trip_id']);
      $operator_details = $this->api->get_bus_operator_profile($trip_details[0]['cust_id']);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $trip_details[0]['cust_id']);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_details[0]['trip_id']);
      $src_dest_details = $this->api->get_trip_location_master($trip_details[0]['trip_id']);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_id);
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_details[0]['trip_id']);
      //echo json_encode($booking_details); die();
      $this->load->view('user_panel/bus_previous_trip_buyer_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details','ticket_seat_details'));
      $this->load->view('user_panel/footer');
    }
  //Buyer Trip List (Previous)-----------------------------

  //Buyer Trip List (cancelled)----------------------------
    public function buyer_cancelled_trips()
    {
      //echo json_encode($_POST); die();
      $cust_id = $this->cust_id;
      $filter = $this->input->post('filter');
      if(isset($filter) && $filter == 'filter'){
        if(isset($_POST['trip_source'])) { $trip_source = $this->input->post('trip_source'); } else { $trip_source = ''; }
        if(isset($_POST['trip_destination'])) { $trip_destination = $this->input->post('trip_destination'); } else { $trip_destination = ''; }
        if(isset($_POST['journey_date'])) { $journey_date = $this->input->post('journey_date'); } else { $journey_date = ''; }
        $trip_list = $this->api->get_buyer_trip_master_list($cust_id, 'cancelled', $trip_source, $trip_destination, $journey_date);
      } else {
        $trip_list = $this->api->get_buyer_trip_master_list($cust_id, 'cancelled');
      }
      //echo $this->db->last_query(); //die();
      //echo json_encode($trip_list); die();

      $trip_sources = $this->api->get_trip_master_source_list_buyer();
      $trip_destinations = $this->api->get_trip_master_destination_list_buyer();
      for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
      //echo json_encode($trip_list); die();
      $this->load->view('user_panel/bus_buyer_cancelled_trip_list_view', compact('trip_list','trip_sources','trip_destinations','trip_source','trip_destination','journey_date'));
      $this->load->view('user_panel/footer');
    }
    public function bus_cancelled_trip_details_buyer()
    {
      if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
      } else if(!is_null($this->uri->segment(3))) { $ticket_id = (int)$this->uri->segment(3);
      } else { redirect('user-panel-bus/buyer-cancelled-trips'); }

      $ticket_details = $this->api->get_trip_booking_details($ticket_id);
      $trip_id = $ticket_details['trip_id'];
      $trip_details = $this->api->get_trip_master_details($ticket_details['trip_id']);
      $operator_details = $this->api->get_bus_operator_profile($trip_details[0]['cust_id']);
      //$locations = $this->api->get_pickup_drop_operating_location_list($this->cust_id);
      $vehical_type_id = $trip_details[0]['vehical_type_id'];
      $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $trip_details[0]['cust_id']);
      $buses = $this->api->get_operator_bus_details($trip_details[0]['bus_id']);
      //get seat price details
      //$location_source_destination=$this->api->get_locations_pickup_drop_points($trip_details[0]['trip_id']);
      $source_desitnation=$this->api->get_source_destination_of_trip($trip_details[0]['trip_id']);
      $seat_type_price = $this->api->get_bus_trip_seat_type_price($trip_details[0]['trip_id']);
      $src_dest_details = $this->api->get_trip_location_master($trip_details[0]['trip_id']);
      $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
      $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
      $trip_from = explode(',', $trip_source[0]['lat_long']);
      $trip_to = explode(',', $trip_destination[0]['lat_long']);
      $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
      //get multiple source destinatin details with pickup drop timings
      $ticket_seat_details = $this->api->get_ticket_seat_details($ticket_id);
      //Get trip bookings list
      $booking_details = $this->api->get_trip_booking_list($trip_details[0]['trip_id']);
      //echo json_encode($booking_details); die();
      $this->load->view('user_panel/bus_cancelled_trip_buyer_details_view', compact('trip_id','locations','buses','trip_details','seat_type_price','src_dest_details','booking_details','trip_destination','trip_source','source_desitnation','trip_distance','operator_details','ticket_seat_details'));
      $this->load->view('user_panel/footer');
    }
  //Buyer Trip List (cancelled)----------------------------

  //Bus Workroom-------------------------------------------
    public function bus_work_room()
    {
      $ticket_id = $this->input->post('ticket_id');
      if(!$ticket_id) { $ticket_id = $this->session->userdata('workroom_ticket_id'); }
      $booking = $this->api->get_trip_booking_details($ticket_id); 
      $keys = ['operator_id' => $booking['operator_id'], 'cust_id' => $this->cust['cust_id'], 'ticket_id' => $booking['ticket_id']];
      $workroom = $this->api->get_bus_workroom_chat($ticket_id);
      $this->load->view('user_panel/bus_workroom_view',compact('booking','workroom','keys'));
      $this->load->view('user_panel/footer');   
    }
    public function add_bus_workroom_chat()
    {
      //echo  json_encode($_POST); die();
      $cust = $this->cust;
      $ticket_id = $this->input->post('ticket_id', TRUE);
      $text_msg = $this->input->post('message_text', TRUE);
      $booking = $this->api->get_trip_booking_details($ticket_id); 
      $cust_id = $booking['cust_id'];
      $operator_id = $booking['operator_id'];
      $sender_id = $cust['cust_id'];
      $cust_name = ($booking['cust_name']!="NULL")?$booking['cust_name']:"User";
      $operator_name = ($booking['operator_name']!="NULL")?$booking['operator_name']:"Operator";
      $today = date("Y-m-d H:i:s");

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('attachment')) {   
          $uploads    = $this->upload->data();  
          $attachment_url =  $config['upload_path'].$uploads["file_name"];  
        } else { $attachment_url = "NULL"; }
      } else { $attachment_url = "NULL"; }

      $insert_data = array(
        "order_id" => $ticket_id,
        "cust_id" => $cust_id,
        "deliverer_id" => $operator_id,
        "text_msg" => $text_msg,
        "attachment_url" => $attachment_url,
        "cre_datetime" => $today,
        "sender_id" => $sender_id,
        "type" => "chat",
        "file_type" => "NULL",
        "cust_name" => $cust_name,
        "deliverer_name" => $operator_name,
        "thumbnail" => "NULL",
        "ratings" => "NULL",
        "cat_id" => 281,
      );

      if( $ow_id = $this->api->add_bus_chat_to_workroom($insert_data)){
        if($sender_id == $cust_id) {  $device_details = $this->api->get_user_device_details($cust_id); } 
        else {  $device_details = $this->api->get_user_device_details($deliverer_id); }

        //Get Customer Device Reg ID's
        $arr_fcm_ids = $arr_apn_ids = array();
        foreach ($device_details as $value) {
          if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
          else {  array_push($arr_apn_ids, $value['reg_id']); }
        }

        $fcm_ids = $arr_fcm_ids;
        $apn_ids = $arr_apn_ids;

        // Get PN API Keys and PEM files
        // $api_key_customer = $this->config->item('delivererAppGoogleKey');
        // $api_key_deliverer_pem = $this->config->item('delivererAppPemFile');
        $msg =  array(
          'title' => $this->lang->line('new_message'), 
          'type' => 'chat', 
          'notice_date' => $today, 
          'desc' => $text_msg,
          'attachment' => $attachment_url,
          'cust_name' => $cust_name, 
          'operator_name' => $operator_name, 
          'ticket_id' => $ticket_id, 
          'cust_id' => $cust_id, 
          'operator_id' => $operator_id, 
          'file_type' => "NULL", 
          'thumbnail' => "NULL",
          "ratings" => "NULL",
        );
        
        // $this->api->sendFCM($msg, $fcm_ids, $api_key_customer);
        // //Push Notification APN
        // $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
        // if(is_array($apn_ids)) { 
        //     $apn_ids = implode(',', $apn_ids);
        //     $this->notification->sendPushIOS($msg_apn_customer, $apn_ids);
        // }
      }

      $array = array('workroom_ticket_id' => $ticket_id );      
      $this->session->set_userdata( $array );
      redirect('user-panel-bus/bus-work-room');
    }
    public function add_bus_workroom_rating()
    {
      $cust = $this->cust;
      $ticket_id = $this->input->post('ticket_id', TRUE);
      $rating = $this->input->post('rating', TRUE); $rating = (int) $rating;
      $text_msg = $this->input->post('review_text', TRUE);
      $booking = $this->api->get_trip_booking_details($ticket_id);
      $cust_id = $booking['cust_id'];
      $operator_id = $booking['operator_id'];
      $sender_id = $cust['cust_id'];
      $cust_name = $booking['cust_name'];
      $operator_name = $booking['operator_name'];
      $today = date("Y-m-d H:i:s");

      $update_data = array(
        "review_comment" => $text_msg,
        "rating" => $rating,
        "review_datetime" => $today,
      );

      if( $this->api->update_bus_trip_review($update_data, $ticket_id) ) {
        if(!$old_ratings = $this->api->get_bus_review_details($operator_id)) {
          $key = "insert";
          $operator_review = array(
            "ratings" => $rating,
            "total_ratings" => $rating,
            "no_of_ratings" => 1,
            "operator_id" => $operator_id,
            "update_date"=> $today
          );
          $this->api->update_bus_ratings($operator_review, $operator_id, $key);
        } else {
          $no_of_ratings = $old_ratings['no_of_ratings']+1;
          $total_ratings = $old_ratings['total_ratings']+$rating;
          $ratings = round($total_ratings/$no_of_ratings,1);
          $operator_review = array(
            "ratings" => $ratings,
            "total_ratings" => $total_ratings,
            "no_of_ratings" => $no_of_ratings,
            "update_date"=> $today
          );
          $this->api->update_bus_ratings($operator_review, $operator_id);  
        }
        if($deliver_old_rating = $this->api->get_deliverer_review_details($operator_id)){
          $no_of_ratings = $old_ratings['no_of_ratings']+$deliver_old_rating['no_of_ratings'];
          $total_ratings = $old_ratings['total_ratings']+$deliver_old_rating['total_ratings'];
          $ratings = round($total_ratings/$no_of_ratings,1);
          $update = array(
            "ratings" => $ratings,
            "total_ratings" => $total_ratings,
            "no_of_ratings" => $no_of_ratings
          );
          $this->api->update_deliverer_rating($update ,$operator_id);
        } else {
          $no_of_ratings = $old_ratings['no_of_ratings']+1;
          $total_ratings = $old_ratings['total_ratings']+$rating;
          $ratings = round($total_ratings/$no_of_ratings,1);
          $update = array(
            "ratings" => $ratings,
            "total_ratings" => $total_ratings,
            "no_of_ratings" => $no_of_ratings
          );
          $this->api->update_deliverer_rating($update ,$operator_id);
        }
        
        $cust_details = $this->api->get_consumer_datails($cust_id);
        $review_data = array(
          "ticket_id" => $ticket_id,
          "cust_id" => $cust_id,
          "cust_name" => $cust_details['firstname']. ' ' .$cust_details['lastname'],
          "cust_avatar" => $cust_details['avatar_url'],
          "review_comment" => $text_msg,
          "review_rating" => $rating,
          "operator_id" => $operator_id,
          "operator_name" => $operator_name,
          "review_datetime" => date('Y-m-d H:i:s'),
        );
        $this->api->post_to_review_bus_trip($review_data);

        $insert_data = array(
          "order_id" => $ticket_id,
          "cust_id" => $cust_id,
          "deliverer_id" => $operator_id,
          "text_msg" => $text_msg,
          "attachment_url" => "NULL",
          "cre_datetime" => $today,
          "sender_id" => $sender_id,
          "type" => "review_rating",
          "file_type" => "NULL",
          "cust_name" => $cust_name,
          "deliverer_name" => $operator_name,
          "thumbnail" => "NULL",
          "ratings" => $rating,
          "cat_id" => 281,
        );
        //ratings/total_ratings/no_of_ratings/
        if( $ow_id = $this->api->add_bus_chat_to_workroom($insert_data)){
          // if($sender_id == $cust_id) {  $device_details = $this->api->get_user_device_details($cust_id); } 
          // else {  $device_details = $this->api->get_user_device_details($deliverer_id); }

          // //Get Customer Device Reg ID's
          // $arr_fcm_ids = $arr_apn_ids = array();
          // foreach ($device_details as $value) {
          //   if($value['device_type'] == 1) {  array_push($arr_fcm_ids, $value['reg_id']); } 
          //   else {  array_push($arr_apn_ids, $value['reg_id']); }
          // }

          // $fcm_ids = $arr_fcm_ids;
          // $apn_ids = $arr_apn_ids;

          // //Get PN API Keys and PEM files
          // $api_key_customer = $this->config->item('delivererAppGoogleKey');
          // //$api_key_deliverer_pem = $this->config->item('delivererAppPemFile');

          // $msg =  array(
          //   'title' => 'Gonagoo - New message', 
          //   'type' => 'review_rating', 
          //   'notice_date' => $today, 
          //   'desc' => $text_msg,
          //   'attachment' => "NULL",
          //   'cust_name' => $cust_name, 
          //   'deliverer_name' => $deliverer_name, 
          //   'order_id' => $order_id, 
          //   'cust_id' => $cust_id, 
          //   'deliverer_id' => $deliverer_id, 
          //   'file_type' => "NULL", 
          //   'thumbnail' => "NULL",
          //   "ratings" => $rating
          // );
          // $this->api->sendFCM($msg, $fcm_ids, $api_key_customer);
          // //Push Notification APN
          // $msg_apn_customer =  array('title' => $this->lang->line('new_message'), 'text' => $text_msg);
          // if(is_array($apn_ids)) { 
          //     $apn_ids = implode(',', $apn_ids);
          //     $this->notification->sendPushIOS($msg_apn_customer, $apn_ids);
          // }
        }

      }
      
      $array = array('workroom_ticket_id' => $ticket_id );      
      $this->session->set_userdata( $array );
      redirect('user-panel-bus/bus-work-room');
    }
    public function workroom_reads()
    {
      $cust = $this->cust;
      $notification = $this->api->get_total_unread_workroom_notifications($cust['cust_id']);
      foreach ($notification as $n) {
        $this->api->make_workroom_notification_read($n['ow_id'], $cust['cust_id']);
      }
      echo 'success';
    }
  //Bus Workroom-------------------------------------------
  
  //Bus Account/E-Wallet/Withdrawals-----------------------
    public function bus_wallet()
    {
      $amount = $this->user->get_customer_account_withdraw_request_sum($this->cust_id, 'request');
      $balance = $this->user->check_user_account_balance($this->cust_id);
      $this->load->view('user_panel/bus_account_history_list_view', compact('balance','amount'));
      $this->load->view('user_panel/footer');
    }
    public function bus_account_history()
    {
      $history = $this->api->get_bus_account_history($this->cust_id);
      $this->load->view('user_panel/bus_transaction_history_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function bus_invoices_history()
    {
      $details = $this->api->get_bus_booking_invoices($this->cust_id);
      //echo json_encode($details); die();
      $this->load->view('user_panel/bus_invoice_history_list_view', compact('details'));
      $this->load->view('user_panel/footer');
    }
    public function create_bus_withdraw_request()
    {
      //echo json_encode($_POST); die();
      $account_id = $this->input->post('account_id', TRUE);
      $currency_code = $this->input->post('currency_code', TRUE);
      $withdraw_amount = $this->input->post('amount', TRUE);
      $amount = $this->api->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
      $accounts = $this->api->get_payment_details($this->cust_id);
      $balance = $this->api->check_user_account_balance_by_id((int)$account_id);
      $final_balance = $balance['account_balance'] - $amount['amount'];
      $currency_code = $balance['currency_code'];

      $this->load->view('user_panel/bus_withdraw_request_create_view',compact('final_balance','accounts','amount','currency_code'));
      $this->load->view('user_panel/footer');
    }
    public function withdraw_request_sent_bus_old()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $password = $this->input->post('password', TRUE);
      $user_details = $this->api->get_user_details($this->cust_id);
      if($user_details['password'] == trim($password)) {
      $amount = $this->input->post('amount', TRUE);
      $currency_code= $this->input->post('currency_code', TRUE);
      $transfer_account_type = $this->input->post('transfer_account_type', TRUE);
      $balance = $this->api->check_user_account_balance($this->cust_id, $currency_code);
      $requested_amount = $this->user->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
      $withdrawable_amount = $balance - $requested_amount['amount'];

      if($amount <= $withdrawable_amount) {
        $accounts = $this->api->get_payment_details($this->cust_id);
        $transfer_account_number = $accounts["$transfer_account_type"];
        $today = date('Y-m-d h:i:s');
        $data = array(
          "user_id" => (int)$cust_id,
          "amount" => trim($amount),
          "req_datetime" => $today,
          "user_type" => 0,
          "response" => 'request',
          "response_datetime" => 'NULL',
          "reject_remark" => 'NULL',
          "transfer_account_type" => $transfer_account_type,
          "transfer_account_number" => trim($transfer_account_number),
          "currency_code" => trim($currency_code),
        );
        if( $sa_id = $this->api->user_account_withdraw_request($data) ) {
          $this->session->set_flashdata('success', $this->lang->line('withdraw_request_sent'));
        } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_send_request')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('entered_wrong_amount')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('unauthorized_access')); }
      redirect('user-panel-bus/bus-wallet');
    }
    public function withdraw_request_sent_bus()
    {
      //echo json_encode($_POST); die();
      $cust_id = (int)$this->cust_id;
      $password = $this->input->post('password', TRUE);
      $user_details = $this->api->get_user_details($this->cust_id);
      if($user_details['password'] == trim($password)) {
      $amount = $this->input->post('amount', TRUE);
      $currency_code= $this->input->post('currency_code', TRUE);
      $transfer_account_type = $this->input->post('transfer_account_type', TRUE);
      $balance = $this->api->check_user_account_balance($this->cust_id, $currency_code);
      $requested_amount = $this->user->get_customer_account_withdraw_request_sum_currencywise($this->cust_id, 'request', $currency_code);
      $withdrawable_amount = $balance - $requested_amount['amount'];
      if($amount <= $withdrawable_amount) {
        $accounts = $this->api->get_payment_details($this->cust_id);
        foreach ($accounts as $ac){
          if(strpos($transfer_account_type ,$ac['card_number']) !== false){
            $transfer_account_number = $ac['card_number'];
          }
        }
        $today = date('Y-m-d h:i:s');
        $data = array(
          "user_id" => (int)$cust_id,
          "amount" => trim($amount),
          "req_datetime" => $today,
          "user_type" => 0,
          "response" => 'request',
          "response_datetime" => 'NULL',
          "reject_remark" => 'NULL',
          "transfer_account_type" => $transfer_account_type,
          "transfer_account_number" => trim($transfer_account_number),
          "currency_code" => trim($currency_code),
        );
        if( $sa_id = $this->api->user_account_withdraw_request($data) ) {
          $this->session->set_flashdata('success', $this->lang->line('withdraw_request_sent'));
        } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_send_request')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('entered_wrong_amount')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('unauthorized_access')); }
      redirect('user-panel-bus/bus-wallet');
    }
    public function bus_withdrawals_request()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'request');
      $this->load->view('user_panel/bus_withdrawal_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function bus_withdrawals_accepted()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'accept');
      $this->load->view('user_panel/bus_withdrawal_accepted_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function bus_withdrawals_rejected()
    {
      $history = $this->user->get_customer_account_withdraw_request_list($this->cust_id, 'reject');
      $this->load->view('user_panel/bus_withdrawal_rejected_request_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
    public function bus_withdrawal_request_details()
    {
      $req_id = $this->input->post('req_id'); 
      $details = $this->user->withdrawal_request_details((int)$req_id);
      $this->load->view('user_panel/bus_withdrawal_request_detail_view', compact('details'));
      $this->load->view('user_panel/footer');
    }
    public function my_scrow()
    {
      $balance = $this->api->deliverer_scrow_master_list($this->cust_id);
      $this->load->view('user_panel/bus_scrow_history_list_view', compact('balance'));
      $this->load->view('user_panel/footer');
    }
    public function user_scrow_history()
    {
      $history = $this->user->get_customer_scrow_history($this->cust_id);
      $this->load->view('user_panel/bus_scrow_account_history_list_view', compact('history'));
      $this->load->view('user_panel/footer');
    }
  //Bus Account/E-Wallet/Withdrawals-----------------------
  
  //Start User Profile-------------------------------------
    public function user_profile()
    { 
      $cust = $this->cust;
      $total_edu = $this->user->get_total_educations($cust['cust_id']);
      $education = $this->user->get_customer_educations($cust['cust_id'],1);

      $total_exp = $this->user->get_total_experiences($cust['cust_id'],false);
      $total_other_exp = $this->user->get_total_experiences($cust['cust_id'],true);

      $experience = $this->user->get_customer_experiences($cust['cust_id'],false, 1);
      $other_experience = $this->user->get_customer_experiences($cust['cust_id'],true, 1);

      $skills = $this->user->get_customer_skills($cust['cust_id'], 1);

      $total_portfolios = $this->user->get_total_portfolios($cust['cust_id']);
      $portfolio = $this->user->get_customer_portfolio($cust['cust_id'], 1);

      $total_docs = $this->user->get_total_documents($cust['cust_id']);
      $docs = $this->user->get_customer_documents($cust['cust_id'], 1);

      $total_pay_methods = $this->user->get_total_payment_methods($cust['cust_id']);
      $pay_methods = $this->user->get_customer_payment_methods($cust['cust_id'], 1);

      $per_counter = 0; 
      $total_counters = 15;

      if($cust['email_verified'] == 1) { $per_counter += 1; }
      if($cust['mobile_verified'] == 1) { $per_counter += 1; }
      if($cust['firstname'] != "NULL") { $per_counter += 1; }
      if($cust['lastname'] != "NULL") { $per_counter += 1; }
      if($cust['gender'] != "NULL") { $per_counter += 1; }
      if($cust['overview'] != "NULL") { $per_counter += 1; }
      if($cust['lang_known'] != "NULL") { $per_counter += 1; }
      if($cust['country_id'] > 0) { $per_counter += 1; }
      if($cust['state_id'] > 0) { $per_counter += 1; }
      if($cust['city_id'] > 0) { $per_counter += 1; }
      if($cust['avatar_url'] != "NULL") { $per_counter += 1; }
      if($total_edu > 0) { $per_counter += 1; }
      if($total_exp > 0) { $per_counter += 1; }
      if($experience) { $per_counter += 1; }
      if($skills) { $per_counter += 1; }

      $percentage = (int) (((int) $per_counter / (int) $total_counters ) *100 );

      $compact = compact('total_edu','education','total_exp','total_other_exp','experience','other_experience','skills','portfolio','total_portfolios','total_docs','docs','total_pay_methods','pay_methods', 'percentage');
      // echo json_encode($skills); die();
      $this->load->view('user_panel/bus_user_profile_view', $compact);
      $this->load->view('user_panel/footer');
    }
    public function edit_basic_profile_view()
    {
      $countries = $this->user->get_countries();
      $states = $this->user->get_state_by_country_id($this->cust['country_id']);
      if($this->cust['state_id'] > 0 ){ $cities = $this->user->get_city_by_state_id($this->cust['state_id']); } else{ $cities = array(); }
      $this->load->view('user_panel/bus_edit_basic_profile_view', compact('countries','states','cities'));
      $this->load->view('user_panel/footer');
    }
    public function get_state_by_country_id()
    {
      $country_id = $this->input->post('country_id');
      echo json_encode($this->user->get_state_by_country_id($country_id));
    }

    public function get_city_by_state_id()
    {
      $state_id = $this->input->post('state_id');
      echo json_encode($this->user->get_city_by_state_id($state_id));
    }

    public function update_basic_profile()
    {
      $mobile_no = $this->input->post('mobile_no');
      $firstname = $this->input->post('firstname');
      $lastname = $this->input->post('lastname');
      $country_id = $this->input->post('country_id');
      $state_id = $this->input->post('state_id');
      $city_id = $this->input->post('city_id');
      $gender = $this->input->post('gender');
      $session_mobile_no = $this->session->userdata('mobile_no');

      $cust = $this->cust; 
      $old_avatar_url = $cust['avatar_url'];
      $old_cover_url = $cust['cover_url'];
      
      if($state_id == NULL) { $state_id = $cust['state_id']; }
      if($city_id == NULL) { $city_id = $cust['city_id']; }

      if( !empty($_FILES["avatar"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/profile-images/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('avatar')) {   
          $uploads    = $this->upload->data();  
          $avatar_url =  $config['upload_path'].$uploads["file_name"];  

          if( $old_avatar_url != "NULL") { unlink($old_avatar_url); }           
        } else { $avatar_url = $old_avatar_url; }
      } else { $avatar_url = $old_avatar_url; }


      if( !empty($_FILES["cover"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/cover-images/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('cover')) {   
          $uploads    = $this->upload->data();  
          $cover_url =  $config['upload_path'].$uploads["file_name"];  

          if( $old_cover_url != "NULL") { unlink($old_cover_url); }           
        } else { $cover_url = $old_cover_url; }
      } else { $cover_url = $old_cover_url; }


      if($mobile_no === $session_mobile_no ) {
        $update_data = array(
          "firstname" => $firstname,
          "lastname" => $lastname,
          "mobile1" => $mobile_no,
          "city_id" => $city_id,
          "state_id" => $state_id,
          "country_id" => $country_id,
          "gender" => $gender,
          "avatar_url" => $avatar_url,
          "cover_url" => $cover_url,
        );    
      } else {
        $OTP = mt_rand(100000,999999);
        $update_data = array(
          "firstname" => $firstname,
          "lastname" => $lastname,
          "mobile1" => $mobile_no,
          "city_id" => $city_id,
          "state_id" => $state_id,
          "country_id" => $country_id,
          "avatar_url" => $avatar_url,
          "cover_url" => $cover_url,
          "gender" => $gender,
          "cust_otp" => $OTP,
          "mobile_verified" => 0,
        );
      }

      if($this->user->update_basic_profile($update_data)){
        if($mobile_no == $session_mobile_no ) { 
          $this->session->set_flashdata('success',$this->lang->line('updated_successfully')); 
          redirect('user-panel-bus/basic-profile/'. $this->cust_id);
        }
        else{
          $this->session->set_userdata('mobile_no', $mobile_no);
          $this->api->sendSMS($this->api->get_country_code_by_id((int)$country_id).$mobile_no, $this->lang->line('lbl_verify_otp'). ': ' . $OTP);
          redirect('verification/otp');
        }
      } else{
        $this->session->set_flashdata('error',$this->lang->line('update_or_no_change_found'));  
        redirect('user-panel-bus/basic-profile/'. $this->session->userdata('cust_id'));
      }
    }
  //End User Profile---------------------------------------
  
  //Start User Payment Methods-----------------------------
    public function edit_payment_method_view()
    {
      $id = $this->uri->segment(3);
      if( $pay = $this->user->get_payment_method_detail($id)){
        $this->load->view('user_panel/bus_edit_payment_method_view',compact('pay'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function edit_payment_method()
    {
      $pay_id = $this->input->post("payment_id");
      $pay_method = $this->input->post("pay_method");
      $brand_name = $this->input->post("brand_name");
      $card_no = $this->input->post("card_no");
      $month = $this->input->post("month");
      $year = $this->input->post("year");
      $bank_name = $this->input->post("bank_name");
      $bank_address = $this->input->post("bank_address");
      $account_title = $this->input->post("account_title");
      $bank_short_code = $this->input->post("bank_short_code");

      $edit_data = array(
        'payment_method' => trim($pay_method), 
        'brand_name' => trim($brand_name),
        'bank_name' => trim($bank_name),
        'bank_address' => trim($bank_address),
        'account_title' => trim($account_title),
        'bank_short_code' => trim($bank_short_code),
        'card_number' => trim($card_no),
        'expirty_date' => trim($month). '/'.trim($year),
      );
          
      if( $id = $this->api->update_payment_details($pay_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-bus/payment-method/'. $pay_id);
    }
    public function payment_method_list_view()
    {
      $payments = $this->api->get_payment_details($this->cust['cust_id']);
      if($payments) {
        $this->load->view('user_panel/bus_payment_method_list_view', compact('payments'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function delete_payment_method()
    {
      $id = $this->input->post("id");     
      if( $this->api->delete_payment_detail($this->cust['cust_id'], $id)) { echo "success"; } else { echo "failed"; }
    }
    public function add_payment_method_view()
    {
      $this->load->view('user_panel/bus_add_payment_method_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_payment_method()
    {
      $cust = $this->cust;
      $pay_method = $this->input->post("pay_method");
      $brand_name = $this->input->post("brand_name");
      $card_no = $this->input->post("card_no");
      $month = $this->input->post("month");
      $year = $this->input->post("year");
      $bank_name = $this->input->post("bank_name");
      $bank_address = $this->input->post("bank_address");
      $account_title = $this->input->post("account_title");
      $bank_short_code = $this->input->post("bank_short_code");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'payment_method' => trim($pay_method), 
        'brand_name' => trim($brand_name),
        'bank_name' => trim($bank_name),
        'account_title' => trim($account_title),
        'account_title' => trim($account_title),
        'bank_short_code' => trim($bank_short_code),
        'pay_status' => 1,
        'card_number' => trim($card_no),
        'expirty_date' => trim($month). '/'.trim($year),
        'cre_datetime' => $today,
      );
          
      if( $id = $this->api->register_payment_details($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-bus/payment-method/add');
    }
  //End User Payment Methods-------------------------------
    
  //Start User Educations----------------------------------
    public function education_list_view()
    {
      $educations = $this->user->get_customer_educations($this->cust['cust_id'],0);
      $this->load->view('user_panel/bus_eduction_list_view', compact('educations'));    
      $this->load->view('user_panel/footer');
    }
    public function add_education_view()
    {
      $this->load->view('user_panel/bus_add_education_view');   
      $this->load->view('user_panel/footer');
    }
    public function add_education()
    {
      $cust = $this->cust;
      $institute_name = $this->input->post("institute_name");
      $qualification = $this->input->post("qualification");
      $title = $this->input->post("title");
      $yr_qualification = $this->input->post("yr_qualification");
      $grade = $this->input->post("grade");
      $yr_attend = $this->input->post("yr_attend");
      $yr_attend_to = $this->input->post("yr_attend_to");
      $additional_info = $this->input->post("additional_info");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'institute_name' => $institute_name, 
        'qualification' => $qualification, 
        'description' => $additional_info,
        'start_date' => $yr_attend,
        'end_date' => $yr_attend_to,
        'grade' => $grade, 
        'qualify_year' => $yr_qualification, 
        'certificate_title' => $title, 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today, 
        'mod_datetime' => $today
      );

      if( $id = $this->api->add_user_education($add_data)) {
        $this->session->set_flashdata('success',$this->lang->line('added_successfully')); 
      } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }

      redirect('user-panel-bus/education/add');
    }
    public function edit_education_view()
    {
      $edu_id = $this->uri->segment(3);
      if( $education = $this->user->get_customer_education_by_id($edu_id)){
        $this->load->view('user_panel/bus_edit_education_view',compact('education'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel/user-profile'); }
    }
    public function edit_education()
    {   
      $edu_id = $this->input->post("education_id");
      $institute_name = $this->input->post("institute_name");
      $qualification = $this->input->post("qualification");
      $title = $this->input->post("title");
      $yr_qualification = $this->input->post("yr_qualification");
      $grade = $this->input->post("grade");
      $yr_attend = $this->input->post("yr_attend");
      $yr_attend_to = $this->input->post("yr_attend_to");
      $additional_info = $this->input->post("additional_info");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'edu_id' => (int) $edu_id,
        'institute_name' => $institute_name, 
        'qualification' => $qualification, 
        'description' => $additional_info,
        'start_date' => $yr_attend,
        'end_date' => $yr_attend_to,
        'grade' => $grade, 
        'qualify_year' => $yr_qualification, 
        'certificate_title' => $title, 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_education($edit_data)) {
        $this->session->set_flashdata('success',$this->lang->line('updated_successfully')); 
      } else { $this->session->set_flashdata('error',$this->lang->line('update_failed'));  }

      redirect('user-panel-bus/education/'.$edu_id);
    }
    public function delete_education()
    {
      $edu_id = $this->input->post("id");
      if( $id = $this->api->delete_user_education($edu_id)) { echo "success"; } else { echo "failed"; }
    }   
  //End User Educations------------------------------------

  //Start User Experience----------------------------------
    public function experience_list_view()
    {
      $experiences = $this->user->get_customer_experiences($this->cust['cust_id'],false, 0);
      $this->load->view('user_panel/bus_experience_list_view', compact('experiences'));   
      $this->load->view('user_panel/footer');
    }
    public function add_experience_view()
    {
      $this->load->view('user_panel/bus_add_experience_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_experience()
    {
      $cust = $this->cust;
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');
      
      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => date('Y-m-d',strtotime($start_date)),
        'end_date' => ($currently_working == "on" ) ? "NULL": date('Y-m-d',strtotime($end_date)),
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 0, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today,
        'mod_datetime' => $today
      );
      //echo json_encode($add_data); die();
      if( $id = $this->api->add_user_experience($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
      redirect('user-panel-bus/experience/add');
    }
    public function edit_experience_view()
    {
      $exp_id = $this->uri->segment(3);
      if( $experience = $this->user->get_customer_experience_by_id($exp_id)){
        $this->load->view('user_panel/bus_edit_experience_view',compact('experience'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel/user-profile'); }
    }
    public function edit_experience()
    {
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'exp_id' => (int) $exp_id,
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_experience($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-bus/experience/'.$exp_id);
    }
    public function delete_experience()
    {
      $exp_id = $this->input->post("id");
      if( $id = $this->api->delete_user_experience($exp_id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Experience------------------------------------

  //Start User Other Experience----------------------------
    public function other_experience_list_view()
    {
      $experiences = $this->user->get_customer_experiences($this->cust['cust_id'],true, 0);
      if($experiences) {
        $this->load->view('user_panel/bus_other_experience_list_view', compact('experiences'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel/user-profile'); }
    }
    public function add_other_experience_view()
    {
      $this->load->view('user_panel/bus_add_other_experience_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_other_experience()
    {
      $cust = $this->cust;
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $add_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 1, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'cre_datetime' => $today,
        'mod_datetime' => $today
      );

      if( $id = $this->api->add_user_experience($add_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-bus/other-experience/add');
    }
    public function edit_other_experience_view()
    {
      $exp_id = $this->uri->segment(3);
      if( $experience = $this->user->get_customer_experience_by_id($exp_id)){
        $this->load->view('user_panel/bus_edit_other_experience_view',compact('experience'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function edit_other_experience()
    {
      $exp_id = $this->input->post("experience_id");
      $org_name = $this->input->post("org_name");
      $location = $this->input->post("location");
      $title = $this->input->post("title");
      $job_desc = $this->input->post("description");
      $grade = $this->input->post("grade");
      $start_date = $this->input->post("start_date");
      $end_date = $this->input->post("end_date");
      $currently_working = $this->input->post("currently_working");
      $today = date('Y-m-d H:i:s');

      $edit_data = array(
        'exp_id' => (int) $exp_id,
        'org_name' => $org_name, 
        'job_title' => $title, 
        'job_location' => $location, 
        'job_desc' => $job_desc,
        'start_date' => $start_date,
        'end_date' => ($currently_working == "on" ) ? "NULL": $end_date,
        'currently_working' => ($currently_working == "on" ) ? 1: 0,
        'other' => 1, 
        'grade' => "NULL", 
        'institute_address' => "NULl", 
        'activities' => "NULL",
        'specialization' => "NULL",
        'remark' => "NULL", 
        'mod_datetime' => $today
      );

      if( $id = $this->api->update_user_experience($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      redirect('user-panel-bus/other-experience/'.$exp_id);
    }
    public function delete_other_experience()
    {
      $exp_id = $this->input->post("id");
      if( $id = $this->api->delete_user_experience($exp_id)) { echo "success"; } else { echo "failed"; }
    }
  //End User Other Experience------------------------------

  //Start User Skills--------------------------------------
    public function get_skills_by_category_id()
    {
      $cat_id = $this->input->post("category_id");
      echo json_encode($this->user->get_skill_by_cat_id($cat_id));
    }
    public function edit_skill_view()
    { 
      $skills = $this->user->get_customer_skills($this->cust['cust_id']);

      if($skills) {
        $junior_skills = explode(',', $skills['junior_skills']);
        $confirmed_skills = explode(',', $skills['confirmed_skills']);
        $senior_skills = explode(',', $skills['senior_skills']);
        $expert_skills = explode(',', $skills['expert_skills']);
      } 
      $category = $this->api->get_category();
      
      if( $this->cust['cust_id'] > 0 ){
        $this->load->view('user_panel/bus_edit_skill_view',compact('category','junior_skills', 'confirmed_skills', 'senior_skills','expert_skills'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function edit_skill()
    { 
      $cust = $this->cust;
      $cat_id = $this->input->post("category_id");
      $skill_level = $this->input->post("skill_level");
      $skills = $this->input->post("skills");   
      $today = date('Y-m-d H:i:s');
      $cust_skills = $this->user->get_customer_skills($this->cust['cust_id']);

      if($skills != NULL AND !empty($skills)){ $skills = implode(',', $skills); } else{ $skills = "NULL"; }

      $edit_data = array(
        'cust_id' => (int) $cust['cust_id'],
        'cs_id' => (int) $cust_skills['cs_id'],
        'cat_id' => $cat_id, 
        'skill_level' => $skill_level, 
        'junior_skills' => ($skill_level == "junior") ? $skills : $cust_skills['junior_skills'], 
        'confirmed_skills' => ($skill_level == "confirmed") ? $skills : $cust_skills['confirmed_skills'],
        'senior_skills' => ($skill_level == "senior") ? $skills : $cust_skills['senior_skills'],
        'expert_skills' => ($skill_level == "expert") ? $skills : $cust_skills['expert_skills'],
        'ext1' => "NULL", 
        'ext2' => "NULL", 
      );

      if( $this->api->update_user_skill($edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-bus/skill');
    } 
  //End User Skills----------------------------------------

  //Start User Portfolio-----------------------------------
    public function portfolio_list_view()
    { 
      $portfolios = $this->user->get_customer_portfolio($this->cust['cust_id']);
      
      if($portfolios) {
        $this->load->view('user_panel/bus_portfolio_list_view', compact('portfolios'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function get_category_by_cat_type_id()
    { 
      $cat_type_id = $this->input->post("cat_type_id");
      echo json_encode($this->user->get_category_by_cat_type_id($cat_type_id));
    }
    public function get_subcategories_by_category_id($value='')
    { 
      $category_id = $this->input->post("category_id");
      echo json_encode($this->user->get_subcategory_by_category_id($category_id));
    }
    public function add_portfolio_view()
    { 
      $category_types = $this->user->get_category_types();
      $this->load->view('user_panel/bus_add_portfolio_view', compact('category_types'));    
      $this->load->view('user_panel/footer');
    }
    public function add_portfolio()
    { 
      $cust = $this->cust;
      $title = $this->input->post("title");
      $category_id = $this->input->post("category_id");
      $subcat_ids = $this->input->post("subcat_ids");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      
      if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = "NULL"; }

      if( !empty($_FILES["portfolio"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('portfolio')) {   
          $uploads    = $this->upload->data();  
          $portfolio_url =  $config['upload_path'].$uploads["file_name"];  

          $add_data = array(
            'cust_id' => (int) $cust['cust_id'],
            'title' => $title, 
            'cat_id' => $category_id, 
            'subcat_ids' => $subcat_ids,
            'attachement_url' => $portfolio_url,
            'description' => $description,
            'tags' => "NULL",
            'cre_datetime' => $today,
          );
          
          if( $id = $this->api->register_portfolio($add_data)) {
            $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
          } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }
        }  else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));   }
      } else { $this->session->set_flashdata('error',$this->lang->line('unable_to_add'));  }

      redirect('user-panel-bus/portfolio/add');
    }
    public function edit_portfolio_view()
    { 
      $id = $this->uri->segment(3);
      if( $portfolio = $this->api->get_portfolio_detail($id)){
        $category_types = $this->user->get_category_types();
        $this->load->view('user_panel/bus_edit_portfolio_view',compact('portfolio','category_types'));    
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function edit_portfolio()
    { 
      $portfolio_id = $this->input->post("portfolio_id");
      $title = $this->input->post("title");
      $category_id = $this->input->post("category_id");
      $subcat_ids = $this->input->post("subcat_ids");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      
      $portfolio = $this->api->get_portfolio_detail($portfolio_id);

      if($category_id == NULL OR empty($category_id)) { 
        $category_id = $portfolio['cat_id']; 
        if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = "NULL"; }
      }else{ if($subcat_ids != NULL) { $subcat_ids = implode(',', $subcat_ids); } else { $subcat_ids = $portfolio['subcat_ids']; } }

      $old_attachement_url = $portfolio["attachement_url"];

      if( !empty($_FILES["portfolio"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
            $config['allowed_types']  = 'gif|jpg|png';       
            $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

        if($this->upload->do_upload('portfolio')) {   
          $uploads    = $this->upload->data();  
          $portfolio_url =  $config['upload_path'].$uploads["file_name"];  
          
          if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        } else { $portfolio_url = $old_attachement_url; }  
      } else { $portfolio_url = $old_attachement_url; }  
      
      $edit_data = array(
        'title' => $title, 
        'cat_id' => $category_id, 
        'subcat_ids' => $subcat_ids,
        'attachement_url' => $portfolio_url,
        'description' => nl2br(trim($description)),
      );
      
      if(  $this->api->update_portfolio($portfolio_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-bus/portfolios/'.$portfolio_id);
    }
    public function delete_portfolio()
    {
      $id = $this->input->post("id");
      $portfolio = $this->api->get_portfolio_detail($id);
      $old_attachement_url = $portfolio["attachement_url"];

      if( $id = $this->api->delete_portfolio($id)) { 
        if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
          echo "success"; 
      } else { echo "failed"; }
    }
  //End User Portfolio-------------------------------------

  //Start User Language------------------------------------
    public function get_languages_master()
    {
      $filter = $this->input->post('q');
      echo json_encode($this->user->get_language_master($filter));
    }
    public function update_languages()
    {
      $udate_data = array(
        "cust_id" => $this->session->userdata('cust_id'),
        "lang_known" => $this->input->post('langs'),
      );

      if( $this->api->update_user_language($udate_data) ){ echo 'success'; }
      else { echo 'failed'; }
    }
  //End User Language--------------------------------------

  //Start User Overview------------------------------------
    public function update_overview()
    {
      $cust_id = $this->session->userdata('cust_id');
      $udate_data = [ "overview" => $this->input->post('overview')];

      if( $this->api->update_overview($cust_id, $udate_data) ){ echo 'success'; }
      else { echo 'failed'; }
    }
  //End User Overvi---ew-----------------------------------

  //Start User Document------------------------------------
    public function document_list_view()
    {
      $documents = $this->api->get_documents($this->cust['cust_id']);
      
      if($documents) {
        $this->load->view('user_panel/bus_document_list_view', compact('documents'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function add_document_view()
    {
      $this->load->view('user_panel/bus_add_document_view');    
      $this->load->view('user_panel/footer');
    }
    public function add_document()
    {
      $cust = $this->cust;

      $title = $this->input->post("title");
      $description = $this->input->post("description");
      $today = date('Y-m-d H:i:s');
      
      if( !empty($_FILES["document"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('document')) {   
          $uploads    = $this->upload->data();  
          $document_url =  $config['upload_path'].$uploads["file_name"];  

          $add_data = array(
            'cust_id' => (int) $cust['cust_id'],
            'doc_title' => $title, 
            'attachement_url' => $document_url,
            'doc_desc' => $description,
            'cre_datetime' => $today,
          );
          
          if( $id = $this->api->register_document($add_data)) {
            $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
          } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }
        }  else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));  }
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-bus/document/add');
    }
    public function edit_document_view()
    {
      $id = $this->uri->segment(3);
      if( $document = $this->user->get_document_detail($id)){
        $this->load->view('user_panel/bus_edit_document_view',compact('document'));   
        $this->load->view('user_panel/footer');
      } else { redirect('user-panel-bus/user-profile'); }
    }
    public function edit_document()
    {   
      $document_id = $this->input->post("document_id");
      $title = $this->input->post("title");
      $description = $this->input->post("description");
      
       $document = $this->user->get_document_detail($document_id);

      $old_attachement_url = $document["attachement_url"];

      if( !empty($_FILES["document"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = 'gif|jpg|png';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('document')) {   
          $uploads    = $this->upload->data();  
          $document_url =  $config['upload_path'].$uploads["file_name"];  
          
          if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        } else { $document_url = $old_attachement_url; }  
      } else { $document_url = $old_attachement_url; }  
      
      $edit_data = array(
        'doc_title' => $title, 
        'attachement_url' => $document_url,
        'doc_desc' => trim(nl2br($description)),
      );
      
      if(  $this->api->update_document($document_id, $edit_data)) {
        $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }

      redirect('user-panel-bus/document/'.$document_id);
    }
    public function delete_document()
    {
      $id = $this->input->post("id");
      
      $document = $this->user->get_document_detail($id);
      $old_attachement_url = $document["attachement_url"];
      
      if( $this->api->delete_document($this->cust['cust_id'], $id)) { 
        if($old_attachement_url != "NULL") { unlink($old_attachement_url); }
        echo "success"; 
      } else { echo "failed"; }
    }
  //End User Document--------------------------------------

  //Start User Change Password-----------------------------
    public function change_password() {
      $this->load->view('user_panel/bus_change_password_view');   
      $this->load->view('user_panel/footer');
    }

    public function update_password()
    {   
      $old_pass = $this->input->post('old_password');
      $new_pass = $this->input->post('new_password');
    
      if($this->api->validate_old_password($this->cust_id, $old_pass)){
        if($this->api->update_new_password(['cust_id' => $this->cust_id, 'password' => $new_pass])){
          $this->session->set_flashdata('success', $this->lang->line('password_changed'));          
        } else { $this->session->set_flashdata('error', $this->lang->line('update_failed'));   }
      } else { $this->session->set_flashdata('error', $this->lang->line('old_password_not_matched'));  }    

      redirect('user-panel-bus/change-password');
    }
  //End User Change Password-------------------------------

  //Suggression and Support--------------------------------
    public function get_support_list()
    {
      $support_list = $this->user->get_support_list($this->cust_id);
      $this->load->view('user_panel/bus_support_list_view', compact('support_list'));
      $this->load->view('user_panel/footer');
    }
    public function get_support()
    {
      $this->load->view('user_panel/bus_get_support_view');
      $this->load->view('user_panel/footer');
    }
    public function add_support_details()
    {
      $type = $this->input->post('type', TRUE);
      $description = $this->input->post('description', TRUE);
      $cust_id = $this->cust_id;
      $today = date('Y-m-d h:i:s');

      if( !empty($_FILES["attachment"]["tmp_name"]) ) {
        $config['upload_path']    = 'resources/attachements/';                 
        $config['allowed_types']  = '*';       
        $config['max_size']       = '0';                          
        $config['max_width']      = '0';                          
        $config['max_height']     = '0';                          
        $config['encrypt_name']   = true; 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('attachment')) {   
          $uploads    = $this->upload->data();  
          $file_url =  $config['upload_path'].$uploads["file_name"];  
        } else {  $file_url = "NULL"; }
      } else {  $file_url = "NULL"; }

      $add_data = array(
        'type' => trim($type), 
        'description' => trim($description), 
        'file_url' => trim($file_url), 
        'cust_id' => (int) $cust_id, 
        'cre_datetime' => $today, 
        'response' => 'NULL', 
        'res_datetime' => 'NULL', 
        'status' => 'open', 
        'updated_by' => 0, 
      );
      
      //echo json_encode($add_data); die();
      if( $id = $this->user->register_new_support($add_data)) {
        $ticket_id = 'G-'.time().$id;
        $this->user->update_support_ticket($id, $ticket_id);
        $this->session->set_flashdata('success', $this->lang->line('added_successfully'));  
      } else { $this->session->set_flashdata('error', $this->lang->line('unable_to_add'));   }

      redirect('user-panel-bus/get-support');
    }
    public function view_support_reply_details()
    {
      $id = $this->uri->segment(3);
      $details = $this->user->get_support_details($id);
      $this->load->view('user_panel/bus_view_support_reply_details', compact('details'));   
      $this->load->view('user_panel/footer');
    }
  //End Suggression and Support----------------------------

  //Start Notification-------------------------------------
    public function user_notifications()
    {
      $cust = $this->cust;
      if($cust['user_type']) { $type = "Individuals"; } else { $type = "Business"; }
      $notifications = $this->api->get_notifications($cust['cust_id'], $type);        
      //echo $this->db->last_query();  die();
      $this->load->view('user_panel/bus_notifications_view',compact('notifications'));
      $this->load->view('user_panel/footer'); 
    }
    public function notification_detail()
    {
      $id = $this->input->post("id");
      $notice = $this->api->get_notification($id);

      $this->load->view('user_panel/bus_notifications_detail_view', compact('notice'));
      $this->load->view('user_panel/footer'); 
    }
    public function notifications_reads()
    {
      $cust = $this->cust;
      if($cust['user_type'] == 1 ){ $type ="Individuals"; } else { $type ="Business"; }
      $notification = $this->api->get_total_unread_notifications($cust['cust_id'], $type);

      foreach ($notification as $n) {
        $this->api->make_notification_read($n['id'], $cust['cust_id']);
      }
      echo 'success';
    }
  //End Notification---------------------------------------

  //Start Workroom Notifications---------------------------
    public function ticket_booking_list()
    {
      $cust_id = $this->cust['cust_id'];
      //$tickets = $this->api->get_workorder_list($cust_id);  
      $orders = $this->api->get_global_workroom_list((int)$cust_id);
      //echo $this->db->last_query();
      //echo json_encode($orders);  die();
      for ($i=0; $i < sizeof($orders); $i++) { 
        if($orders[$i]['cat_id'] == 6 || $orders[$i]['cat_id'] == 7) {
          if($service_details = $this->api->get_courier_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
            $orders[$i] += array('service_details' => $service_details);
          } else { $orders[$i] += array('service_details' => array()); }
        } else if($orders[$i]['cat_id'] == 9) {
          if($service_details = $this->api->get_laundry_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
            $service_details_details = $this->api->laundry_booking_details_list($service_details['booking_id']);
            $service_details += ['bookings_details' => $service_details_details];
            $orders[$i] += array('service_details' => $service_details);
          } else { $orders[$i] += array('service_details' => array()); }
        } else {
          if($service_details = $this->api->get_booking_workorder_list($orders[$i]['cust_id'], $orders[$i]['service_id'])) {
            $orders[$i] += array('service_details' => $service_details);
          } else { $orders[$i] += array('service_details' => array()); }
        }
      }
      $this->load->view('user_panel/bus_ticket_booking_list_view', compact('orders'));
      $this->load->view('user_panel/footer');
    }
  //End Workroom Notifications-----------------------------

  //Start Promo Code---------------------------------------
    public function promocode()
    {
      //echo json_encode($_POST); die(); 
      if(isset($_POST["promo_code"])){
        $ticket_id = $this->input->post('ticket_id');
        $details =  $this->api->get_trip_booking_details($ticket_id);
        $promo_code_name = $this->input->post("promo_code");
        if(!$details['promo_code']){
          if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
            $used_codes = $this->api->get_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version']);
            if(sizeof($used_codes) < $promocode['no_of_limit']){
              if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$this->cust_id)){
                    $discount_amount = ($details['ticket_price']/100)*$promocode['discount_percent'];
                    $update = array(
                      'ticket_price' => $details['ticket_price']-$discount_amount,
                      'balance_amount' => $details['ticket_price']-$discount_amount,
                      'promo_code' => $promo_code_name,
                      'promo_code_id' => $promocode['promo_code_id'],
                      'promo_code_version' => $promocode['promo_code_version'],
                      'discount_amount' => $discount_amount,
                      'discount_percent' => $promocode['discount_percent'],
                      'actual_price' => $details['ticket_price'],
                      'promo_code_datetime' => date('Y-m-d h:i:s')
                    );
                    $this->api->update_booking_details($update , $details['ticket_id']);

                    if($details['return_trip_id'] > 0 ){
                      $return_details =  $this->api->get_trip_booking_details($details['return_trip_id']);
                      if(!$return_details['promo_code']){
                        $discount_amount = ($return_details['ticket_price']/100)*$promocode['discount_percent'];
                        $update = array(
                          'ticket_price' => $return_details['ticket_price']-$discount_amount,
                          'balance_amount' => $return_details['ticket_price']-$discount_amount,
                          'promo_code' => $promo_code_name,
                          'promo_code_id' => $promocode['promo_code_id'],
                          'promo_code_version' => $promocode['promo_code_version'],
                          'discount_amount' => $discount_amount,
                          'discount_percent' => $promocode['discount_percent'],
                          'actual_price' => $return_details['ticket_price'],
                          'promo_code_datetime' => date('Y-m-d h:i:s')
                        );
                        $this->api->update_booking_details($update , $return_details['ticket_id']);
                      }
                    }      

                    $used_promo_update = array(
                      'promo_code_id' => $promocode['promo_code_id'],
                      'promo_code_version' => $promocode['promo_code_version'],
                      'cust_id' => $details['cust_id'],
                      'customer_email' => $details['cust_email'],
                      'discount_percent' => $promocode['discount_percent'],
                      'discount_amount' => $discount_amount,
                      'discount_currency' => $details['currency_sign'],
                      'cre_datetime' => date('Y-m-d h:i:s'),
                      'service_id' => '281',
                      'booking_id' => $details['ticket_id'],
                    );
                    $this->api->register_user_used_prmocode($used_promo_update);
                    $this->session->set_flashdata('success', $this->lang->line('Promocode Applied successfully'));
                    $details = $this->api->get_trip_booking_details($ticket_id);
                  //code end here
                }else{
                  $this->session->set_flashdata('error_promo', $this->lang->line('You are already used this promocode'));
                }
              }else{
                $this->session->set_flashdata('error_promo', $this->lang->line('This Promocode has been Expired'));
              }
            }else{
              $this->session->set_flashdata('error_promo', $this->lang->line('This Promo code is used maximum number of limit'));
            }
          }else{
            $this->session->set_flashdata('error_promo', $this->lang->line('Invalid Promocode Entered'));
          }
        }else{
          $this->session->set_flashdata('error_promo', $this->lang->line('You are already used promocode for this order'));
        }
      }
      
      if($_POST["page_name"] == "payment_buyer"){
        redirect('user-panel-bus/bus-ticket-payment-buyer/'.$ticket_id);
      }else if($_POST["page_name"] == "single_payment_buyer"){
        redirect('user-panel-bus/bus-single-ticket-payment-buyer/'.$ticket_id);
      }else if($_POST["page_name"] == "payment_seller"){
        redirect('user-panel-bus/bus-ticket-payment-seller/'.$ticket_id);
      }else if($_POST["page_name"] == "single_payment_seller"){
        redirect('user-panel-bus/bus-single-ticket-payment-seller/'.$ticket_id);
      }
    }
    public function promocode_ajax()
    {
      $is_return = 0;
      $promo_code_name =  $this->input->post('promocode');
      $ticket_id =  $this->input->post('ticket_id');
      $details =  $this->api->get_trip_booking_details($ticket_id);
      if($promo_code = $this->api->get_promocode_by_name($promo_code_name)){
        $service_id = explode(',',$promo_code['service_id']);
        $used_codes = $this->api->get_used_promo_code($promo_code['promo_code_id'],$promo_code['promo_code_version']);
        if(sizeof($used_codes) < $promo_code['no_of_limit']){
          if(!$details['promo_code']){
            if($promocode = $this->api->get_promocode_by_name($promo_code_name)){
              if(in_array("281",$service_id)){
                if(strtotime($promocode['code_expire_date']) >= strtotime(date('M-d-y'))){
                  if(!$this->api->check_used_promo_code($promocode['promo_code_id'],$promocode['promo_code_version'],$this->cust_id)){
                    $discount_amount = round(($details['ticket_price']/100)*$promocode['discount_percent'] , 2);

                    if($details['return_trip_id'] > 0 ){
                      $return_details =  $this->api->get_trip_booking_details($details['return_trip_id']);
                      if(!$return_details['promo_code']){
                        $return_discount_amount=round(($return_details['ticket_price']/100)*$promocode['discount_percent'] , 2);

                        if($return_details['currency_sign'] == 'XAF'){
                          $return_discount_amount = round($return_discount_amount , 0);
                        }
                        $return_ticket_price =  round($return_details['ticket_price']-$return_discount_amount ,2);
                        if($return_details['currency_sign'] == 'XAF'){
                          $return_ticket_price = round($return_ticket_price , 0);
                        }
                        $is_return = 1;
                      }
                    }
                    if($details['currency_sign'] == 'XAF'){
                      $discount_amount = round($discount_amount , 0);
                    }
                    $ticket_price =  round($details['ticket_price']-$discount_amount ,2);
                    if($details['currency_sign'] == 'XAF'){
                      $ticket_price = round($ticket_price , 0);
                    }

                    if($is_return == 1){
                      $discount_amount = $discount_amount+$return_discount_amount;
                      $ticket_price = $ticket_price + $return_ticket_price;
                    }

                    echo "success@".$this->lang->line('You will get')." ".$discount_amount." ".$details['currency_sign']." ".$this->lang->line('Discount')."$".$this->lang->line('Use this promocode and pay')." ".$ticket_price." ".$details['currency_sign']; 
                  }else{
                    echo "error@".$this->lang->line('You are already used this promocode');
                  }
                }else{
                  echo "error@".$this->lang->line('This Promocode has been Expired');
                }
              }else{
                echo "error@".$this->lang->line('This promocode is not valid for this category');
              }
            }else{
              echo "error@".$this->lang->line('Invalid Promocode Entered');
            }
          }else{
            echo "error@".$this->lang->line('You are already used promocode for this order');
          }
        }else{
          echo "error@".$this->lang->line('This Promo code is used maximum number of limit');
        }
      }else{
        echo "error@".$this->lang->line('Invalid Promocode Entered');
      }
    }
  //End Promo Code-----------------------------------------

  public function test_view()
  {
    $this->load->view('user_panel/test_view');
  }

  public function commission_refund_request()
  {
    //echo json_encode($_POST);  die();
    $ticket_id = $this->input->post('ticket_id', TRUE);
    $seat_id = $this->input->post('seat_id', TRUE);
    $today = date('Y-m-d H:i:s');
    $ticket_details = $this->api->get_trip_booking_details((int)$ticket_id);
    $seat_details = $this->api->get_seat_details((int)$seat_id);
    
    $request_data = array(
      "cust_id" => $ticket_details['cust_id'],
      "firstname" => trim($seat_details['firstname']),
      "lastname" => trim($seat_details['lastname']),
      "country_id" => trim($seat_details['country_id']),
      "mobile" => trim($seat_details['mobile']),
      "email_id" => trim($seat_details['email_id']),
      "ticket_id" => (int)$ticket_id, 
      "seat_id" => (int)$seat_id, 
      "source_point" => $ticket_details['source_point'], 
      "destination_point" => $ticket_details['destination_point'], 
      "journey_date" => $ticket_details['journey_date'], 
      "no_of_seats" => $ticket_details['no_of_seats'], 
      "seat_price" => $ticket_details['seat_price'], 
      "ticket_price" => $ticket_details['ticket_price'], 
      "paid_amount" => $ticket_details['paid_amount'], 
      "balance_amount" => $ticket_details['balance_amount'], 
      "currency_id" => $ticket_details['currency_id'], 
      "currency_sign" => $ticket_details['currency_sign'], 
      "operator_id" => $ticket_details['operator_id'], 
      "req_date" => $today, 
      "req_status" => 'open', 
      "req_status_date" => $today, 
    );
    //echo json_encode($request_data);  die();

    if($req_id = $this->api->add_refund_commission_request($request_data)) {
      $update_data = array(
        "commission_refund" => (int)$req_id,
      );
      $this->api->update_bus_booking_seat_details($update_data, (int)$seat_id);
      $this->session->set_flashdata('success',$this->lang->line('Commission refund request sent to Gonagoo admin. It will take 2 business days to process.'));
    } else { $this->session->set_flashdata('error',$this->lang->line('Error! while sending request!')); }
    redirect("user-panel-bus/seller-previous-trips-view-passengers/".(int)$ticket_id);
  }

}
/* End of file User_panel_bus.php */
/* Location: ./application/controllers/User_panel_bus.php */