<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify_docs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');
			$this->load->model('Verify_docs_model', 'docs');
                        $this->load->model('Notification_model', 'notice');
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	
	public function verify_documents()
	{	
                $auth_type_id = $this->session->userdata('auth_type_id');
	        if($auth_type_id > 1) {   $admin_country_id = $this->session->userdata('admin_country_id'); }
	        else { $admin_country_id = 0; }	
		$users_documents_list = $this->docs->get_user_documents($admin_country_id);
		$this->load->view('admin/users_documents_list_view', compact('users_documents_list'));			
		$this->load->view('admin/footer_view');		
	}

	public function rejected_documents()
	{		
		$users_rejected_documents_list = $this->docs->get_user_rejected_documents();
		$this->load->view('admin/users_rejected_documents_list_view', compact('users_rejected_documents_list'));			
		$this->load->view('admin/footer_view');		
	}

	public function view_users_details()
	{	
		$cust_id = (int) $this->uri->segment('3');
		$users_detail = $this->docs->get_users_detail($cust_id);
		$users_skill_list = $this->docs->get_customer_skills($cust_id);
		$users_country = $this->docs->get_country_detail($users_detail['country_id']);
		$users_state = $this->docs->get_state_detail($users_detail['state_id']);
		$users_city = $this->docs->get_city_detail($users_detail['city_id']);

		$this->load->view('admin/users_details_view', compact('users_detail','users_skill_list','users_country','users_state','users_city'));
		$this->load->view('admin/footer_view');
	}

	public function verify_it()
	{
		$doc_id = $this->input->post('id', TRUE);
		if( $this->docs->verify_document( (int) $doc_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}

	public function reject_it()
	{
		$doc_id = $this->input->post('id', TRUE);
		if( $this->docs->reject_document( (int) $doc_id) ) {	echo 'success';	}	else { 	echo 'failed';	}
	}
	
}

/* End of file Verify_docs.php */
/* Location: ./application/controllers/Verify_docs.php */