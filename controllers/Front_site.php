<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class Front_site extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    
    $this->load->model('api_model_services', 'api');
    $this->load->model('api_model_bus', 'api_bus');
    $this->load->model('api_model_laundry', 'api_laundry');
    // $this->load->model('api_model_services', 'm4');
    $this->load->model('api_model_sms', 'api_sms');
    $this->load->model('Web_user_model', 'user');

    $this->cust_id = $this->session->userdata("cust_id"); $this->cust_id = (int) $this->cust_id;
    $this->cust = $this->api->get_consumer_datails($this->cust_id, true);   $cust = $this->cust;

    $acc_type  = $cust['acc_type'];

    if($this->router->method != "logout") {       
      if($this->session->userdata('is_logged_in')==1 && $this->cust_id > 0) { 
        if($acc_type == 'both' || $acc_type == 'seller' ) { redirect(base_url('user-panel/landing')); } 
        else if( $acc_type == 'buyer') { redirect(base_url('user-panel/landing')); }        
      }
    } 
    
    $lang = $this->session->userdata('language');
    if($lang){ $this->config->set_item('language', strtolower($lang)); }
    else{ $lang = "french"; $this->config->set_item('language', 'french'); } 

    if(trim(strtolower($lang)) == 'french'){
      $this->lang->load('frenchApi_lang','french');
      $this->lang->load('frenchFront_lang','french');
    } 
    else if(trim(strtolower($lang)) == 'spanish'){  
      $this->lang->load('spanishApi_lang','spanish');
      $this->lang->load('spanishFront_lang','spanish');
    }
    else { 
      $this->lang->load('englishApi_lang','english');
      $this->lang->load('englishFront_lang','english');
    }

    if($this->input->method() != "post") {  $this->load->view('front_header_view'); }
  }
  public function index()
  {
    // echo json_encode("hii");die();
    $countries = $this->user->get_countries();
    $category_type = $this->api->get_category_type();
    //$category_type = $this->api->get_category_type_with_category();
    $category = $this->api->get_category();
    $promocodes = $this->api->get_promocodes();
    $this->load->view('front_index_view', compact('category_type','category','countries','promocodes'));
    $this->load->view('front_footer_view');
  }
  // public function courier_index()
  // {
  //   $default_trans = $this->user->get_transport_vehicle_list_by_type('earth', 7);
  //   $dimension = $this->api->get_standard_dimension_masters_cat_id(7,'earth');
  //   $promocodes = $this->api->get_promocodes(7);
  //   $this->load->view('front_courier_page', compact('promocodes','default_trans','dimension'));
  //   $this->load->view('front_footer_view');
  // }
  // public function log_in_booking()
  // {
  //   //echo json_encode($_REQUEST); die();
  //   $cat_id = $_GET['cat_id'];
  //   $frm_latitude = $_GET['frm_latitude'];
  //   $frm_longitude = $_GET['frm_longitude'];
  //   $frm_country = ($_GET['frm_country']=='')?'NULL':$_GET['frm_country'];
  //   $frm_state = ($_GET['frm_state']=='')?'NULL':$_GET['frm_state'];
  //   $frm_city = ($_GET['frm_city']=='')?'NULL':$_GET['frm_city'];
  //   $frm_formatted_address = $_GET['frm_formatted_address'];
  //   $frm_postal_code = $_GET['frm_postal_code'];
  //   $pickupdate = $_GET['pickupdate'];
  //   $pickuptime = $_GET['pickuptime'];
  //   $to_latitude = $_GET['to_latitude'];
  //   $to_longitude = $_GET['to_longitude'];
  //   $to_country = ($_GET['to_country']=='')?'NULL':$_GET['to_country'];
  //   $to_state = ($_GET['to_state']=='')?'NULL':$_GET['to_state'];
  //   $to_city = ($_GET['to_city']=='')?'NULL':$_GET['to_city'];
  //   $to_formatted_address = $_GET['to_formatted_address'];
  //   $to_postal_code = $_GET['to_postal_code'];
  //   $deliverdate = $_GET['deliverdate'];
  //   $delivertime = $_GET['delivertime'];
  //   $dimension_id = $_GET['dimension_id'];
  //   $c_quantity = $_GET['c_quantity'];
  //   $c_weight = $_GET['c_weight'];
  //   $vehicle = $_GET['vehicle'];
  //   $transport_type = $_GET['transport_type'];
  //   $from_address = $_GET['from_address'];
  //   $to_address = $_GET['to_address'];

  //   $dimension_details = $this->api->get_dimension_detail_by_id($dimension_id);
  //   //echo json_encode($dimension_details);  die();

  //   //get from address country/state/city ID's
  //   if($frm_country!='NULL') $from_country_id = $this->api_laundry->get_country_id_by_name($frm_country); else $from_country_id = 0;
  //   if($frm_state!='NULL') $from_state_id = $this->api_laundry->get_state_id_by_name($frm_state, $from_country_id); else $from_state_id = 0;
  //   if($frm_city!='NULL') $from_city_id = $this->api_laundry->get_city_id_by_name($frm_city, $from_state_id); else $from_city_id = 0;
  //   //echo $frm_state_id; die();
 
  //   //get to address country/state/city ID's
  //   $to_country_id = ($to_country!='NULL')?$this->api_laundry->get_country_id_by_name($to_country):0;
  //   $to_state_id = ($to_state!='NULL')?$this->api_laundry->get_state_id_by_name($to_state, $to_country_id):0;
  //   $to_city_id = ($to_city!='NULL')?$this->api_laundry->get_city_id_by_name($to_city, $to_state_id):0;
  //   //echo $to_state_id; die();

  //   if($from_country_id == $to_country_id && $from_state_id == $to_state_id && $from_city_id == $to_city_id) { $service_area_type = 'local';
  //   } else if($from_country_id == $to_country_id && $from_state_id != $to_state_id && $from_city_id != $to_city_id) { $service_area_type = 'national';
  //   } else { 
  //     if($from_country_id == $to_country_id) { $service_area_type = 'national'; 
  //     } else { $service_area_type = 'international'; }
  //   }
  //   //echo $service_area_type; die();

  //   $distance = round($this->api_laundry->GetDrivingDistance($frm_latitude, $frm_longitude, $to_latitude, $to_longitude), 2);
  //   //echo json_encode($distance);

  //   $calculate_price =  array (
  //     "cust_id" => 0,
  //     "category_id" => $cat_id,
      
  //     "delivery_datetime" => $deliverdate.' '.$delivertime.':00',
  //     "pickup_datetime" => $pickupdate.' '.$pickuptime.':00',
      
  //     "total_quantity" => $c_quantity,
      
  //     "width" => $dimension_details['width'],
  //     "height" => $dimension_details['height'],
  //     "length" => $dimension_details['length'],
      
  //     "total_weight" => $c_weight,
  //     "unit_id" => 1,
      
  //     "from_country_id" => $from_country_id,
  //     "from_state_id" => $from_state_id,
  //     "from_city_id" => $from_city_id,
  //     "from_latitude" => $frm_latitude,
  //     "from_longitude" => $frm_longitude,

  //     "to_country_id" => $to_country_id,
  //     "to_state_id" => $to_state_id,
  //     "to_city_id" => $to_city_id,
  //     "to_latitude" => $to_latitude,
  //     "to_longitude" => $to_longitude,

  //     "service_area_type" => $service_area_type,
  //     "transport_type" => $transport_type,
  //     "handling_by" => "0",
  //     "dedicated_vehicle" => "0",
  //     "package_value" => "0",

  //     "custom_clearance_by" => "self",
  //     "old_new_goods" => "NULL",
  //     "custom_package_value" => "0",
  //     "loading_time" => 0,
  //     "distance" => $distance,
  //     "dimension_type" => $dimension_details['dimension_type'],
  //   );
  //   //echo json_encode($calculate_price); die();

  //   $total_price = $this->api->calculate_order_price($calculate_price);
  //   //echo json_encode($total_price);  die();

  //   if($total_price['total_price'] <= 0) {
  //     $this->session->set_flashdata('error', $this->lang->line('Unable to find best price for you. Try again!'));
  //     if($cat_id == 7) redirect('courier','refresh'); 
  //     else if($cat_id == 6) redirect('transport','refresh'); 
  //     else redirect(base_url(),'refresh');
  //   } else {
  //     //Courier And Transport------------------------------------------------------
  //       //echo json_encode($_SESSION); die();
  //       if(isset($_SESSION['cat_id']) && ($_SESSION['cat_id'] == 6 || $_SESSION['cat_id'] == 7) ) { // courier search
  //         //uset previous data

  //         $this->session->unset_userdata('vehicle');
  //         $this->session->unset_userdata('pickupdate');
  //         $this->session->unset_userdata('pickuptime');
  //         $this->session->unset_userdata('c_quantity');
  //         $this->session->unset_userdata('dimension_id');
  //         $this->session->unset_userdata('deliverdate');
  //         $this->session->unset_userdata('delivertime');
  //         $this->session->unset_userdata('c_weight');
  //         $this->session->unset_userdata('image_url');
  //         $this->session->unset_userdata('c_width');
  //         $this->session->unset_userdata('c_height');
  //         $this->session->unset_userdata('c_length');
  //         $this->session->unset_userdata('order_mode');

  //         $this->session->unset_userdata('from_address');
  //         $this->session->unset_userdata('frm_latitude');
  //         $this->session->unset_userdata('frm_longitude');
  //         $this->session->unset_userdata('frm_formatted_address');
  //         $this->session->unset_userdata('frm_postal_code');
  //         $this->session->unset_userdata('frm_country');
  //         $this->session->unset_userdata('frm_state');
  //         $this->session->unset_userdata('frm_city');

  //         $this->session->unset_userdata('to_address');
  //         $this->session->unset_userdata('to_latitude');
  //         $this->session->unset_userdata('to_longitude');
  //         $this->session->unset_userdata('to_formatted_address');
  //         $this->session->unset_userdata('to_postal_code');
  //         $this->session->unset_userdata('to_country');
  //         $this->session->unset_userdata('to_state');
  //         $this->session->unset_userdata('to_city');

  //         $this->session->unset_userdata('transport_type');
  //         $this->session->unset_userdata('from_country_id');
  //         $this->session->unset_userdata('to_country_id');

  //         if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 6) {
  //           $this->session->unset_userdata('loading_free_hours');
  //         }
  //         $this->session->unset_userdata('cat_id');
  //       }
  //       if(isset($_GET['cat_id']) && ($_GET['cat_id'] == 7 || $_GET['cat_id'] == 6)) {
  //         //set current data
  //         if($dimension_details = $this->api->get_standard_dimension_details($_GET['dimension_id'], $_GET['cat_id'], 'earth')) {
            
  //           //echo json_encode($dimension_details); die();
  //           $dimension_id = $_GET['dimension_id'];
  //           $c_width = $dimension_details['width'];
  //           $c_height = $dimension_details['height'];
  //           $c_length = $dimension_details['length']; 
  //           $image_url = $dimension_details['image_url']; 
  //         } else {
  //           $dimension_id = 0;
  //           $c_width = 0;
  //           $c_height = 0;
  //           $c_length = 0; 
  //           $image_url = 'NULL'; 
  //         }

  //         $from_country_id = $this->api_bus->get_country_id_by_name(trim($frm_country));
  //         $to_country_id = $this->api_bus->get_country_id_by_name(trim($to_country));
          
  //         $this->session->set_userdata(array(
  //                                       'cat_id' => $_GET['cat_id'], 
  //                                       'vehicle' => $_GET['vehicle'], 
  //                                       'pickupdate' => $_GET['pickupdate'], 
  //                                       'pickuptime' => $_GET['pickuptime'], 
  //                                       'c_quantity' => $_GET['c_quantity'], 
  //                                       'dimension_id' => $dimension_id, 
  //                                       'c_width' => $c_width, 
  //                                       'c_height' => $c_height, 
  //                                       'c_length' => $c_length, 
  //                                       'image_url' => $image_url, 
  //                                       'deliverdate' => $_GET['deliverdate'], 
  //                                       'delivertime' => $_GET['delivertime'], 
  //                                       'c_weight' => $_GET['c_weight'], 
  //                                       'order_mode' => $service_area_type,

  //                                       'from_address' => $from_address, 
  //                                       'frm_latitude' => $frm_latitude, 
  //                                       'frm_longitude' => $frm_longitude, 
  //                                       'frm_formatted_address' => $frm_formatted_address, 
  //                                       'frm_postal_code' => $frm_postal_code, 
  //                                       'frm_country' => $frm_country, 
  //                                       'frm_state' => $frm_state, 
  //                                       'frm_city' => $frm_city, 
  //                                       'from_country_id' => (int)$from_country_id, 

  //                                       'to_address' => $to_address, 
  //                                       'to_latitude' => $to_latitude, 
  //                                       'to_longitude' => $to_longitude, 
  //                                       'to_formatted_address' => $to_formatted_address, 
  //                                       'to_postal_code' => $to_postal_code, 
  //                                       'to_country' => $to_country, 
  //                                       'to_state' => $to_state, 
  //                                       'to_city' => $to_city,
  //                                       'to_country_id' => (int)$to_country_id,

  //                                       'transport_type' => $transport_type,

  //                                       'loading_free_hours' => ($_GET['cat_id']==6)?$total_price['loading_free_hours']:0) );
  //       }
  //     //Courier And Transport------------------------------------------------------

  //     $this->load->helper('captcha');
  //     $values = array(
  //       'word' => '',
  //       'word_length' => 8,
  //       'img_path' => 'resources/captcha/',
  //       'img_url' => base_url() .'resources/captcha/',
  //       'font_path' => base_url() . 'resources/captcha/ostrich-regular.ttf',
  //       'img_width' => '150',
  //       'img_height' => 45,
  //       'expiration' => 7200
  //     );
  //     $data = create_captcha($values); 
  //     $this->session->set_userdata('captcha_code', $data['word']);
      
  //     $countries = $this->user->get_countries();
  //     $this->load->view('front_courier_booking_view' ,compact('$_GET','countries' , 'data' , 'calculate_price', 'total_price'));
  //     $this->load->view('front_footer_view');
  //   } 
  // }
  // public function get_transport_vehicle_list_by_type()
  // {
  //   $type = $this->input->post('type');
  //   $cat_id = $this->input->post('cat_id');
  //   echo json_encode($this->user->get_transport_vehicle_list_by_type($type, (int)$cat_id));
  // // }
  // public function get_dimension_list_by_transport_type()
  // {
  //   $type = $this->input->post('type');
  //   $cat_id = $this->input->post('cat_id');
  //   echo json_encode($this->api->get_standard_dimension_masters_cat_id($cat_id, $type));
  // }
  // public function transport_index()
  // {
  //   $default_trans = $this->user->get_transport_vehicle_list_by_type('earth', 6);
  //   $dimension = $this->api->get_standard_dimension_masters_cat_id(6,'earth');
  //   $promocodes = $this->api->get_promocodes(6);
  //   $this->load->view('front_transport_page', compact('promocodes','default_trans','dimension'));
  //   $this->load->view('front_footer_view');
  // }
  // public function ticket_index()
  // {
  //   $trip_sources = $this->api_bus->get_trip_master_source_list_buyer();
  //   $trip_destinations = $this->api_bus->get_trip_master_destination_list_buyer();
  //   $bus_providers = $this->api_bus->get_bus_operators_list();
  //   $filter = 'basic';
  //   $promocodes = $this->api->get_promocodes(281);
  //   $this->load->view('front_ticket_page', compact('promocodes','trip_sources','trip_destinations','filter','bus_providers'));
  //   $this->load->view('front_footer_view');
  // }
  // public function ticket_search()
  // {
  //   //echo json_encode($_GET); die();
  //   $trip_source = $this->input->get('trip_source');
  //   $trip_destination = $this->input->get('trip_destination');
  //   $jdate = $journey_date = $this->input->get('journey_date');
  //   $rdate = $return_date = $this->input->get('return_date');
  //   $no_of_seat = $this->input->get('no_of_seat');
    
  //   $journey_date=date_create($journey_date);
  //   $journey_date=date_format($journey_date,"Y-m-d");

  //   $search_data = array(
  //     "trip_source" => trim($trip_source), 
  //     "trip_destination" => trim($trip_destination), 
  //     "journey_date" => trim($journey_date), 
  //     "vehical_type_id" =>"",
  //     "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))),
  //     "return_date" => trim($return_date),
  //     "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
  //     "cust_id" => '', 
  //     "opr_id" => '', 
  //     "booking_type" => 'online', 
  //   );
  //   //echo json_encode($search_data); die();
  //   $trip_list = $this->api_bus->get_trip_source_destination_search_list($search_data);
  //   //echo $this->db->last_query(); 
  //   //echo json_encode($trip_list); die();
  //   if($return_date!="") {
  //     $return_date=date_create($return_date);
  //   $return_date=date_format($return_date,"Y-m-d");
  //     $search_data_return = array(
  //       "trip_destination" => trim($trip_source),
  //       "trip_source" => trim($trip_destination),
  //       "vehical_type_id" =>"",
  //       "return_date" => trim($return_date),
  //       "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
  //       "cust_id" => '',
  //       "opr_id" => '', 
  //       "booking_type" => 'online',
  //     );
  //     $trip_list_return = $this->api_bus->get_trip_source_destination_search_list_return($search_data_return);
  //   }
  //   //echo json_encode($trip_list_return); die();
  //   $filter = 'basic';
  //   $trip_sources = $this->api_bus->get_trip_master_source_list_buyer();
  //   $trip_destinations = $this->api_bus->get_trip_master_destination_list_buyer();
  //   // for($i=0; $i<sizeof($trip_list); $i++) { array_push($trip_list[$i],"safe");  }
  //   for($i=0; $i<sizeof($trip_list); $i++) {
  //     $unique_id = str_replace("/","",$journey_date)."_".$trip_list[$i]['trip_id'];
  //     if($cancelled_trips = $this->api_bus->get_cancelled_seller_trips()) {
  //       foreach ($cancelled_trips as $ct) {
  //         if($ct['unique_id']==$unique_id) { array_push($trip_list[$i],"delete");
  //         } else{ array_push($trip_list[$i],"safe");  }
  //       }  
  //     } else { array_push($trip_list[$i],"safe"); }
  //   }
  //   if(isset($trip_list_return)) {
  //     // for($i=0; $i<sizeof($trip_list_return); $i++) { array_push($trip_list_return[$i],"safe"); }
  //     for($i=0; $i<sizeof($trip_list_return); $i++) {
  //       $unique_id = str_replace("/","",$return_date)."_".$trip_list_return[$i]['trip_id'];
  //       if($cancelled_trips = $this->api_bus->get_cancelled_seller_trips()) {
  //         foreach ($cancelled_trips as $ct){
  //           if($ct['unique_id']==$unique_id){ array_push($trip_list_return[$i],"delete");
  //           } else { array_push($trip_list_return[$i],"safe"); }
  //         }  
  //       } else { array_push($trip_list_return[$i],"safe"); } 
  //     } 
  //   }
  //   //echo json_encode($trip_list); die();
  //   $journey_date = $jdate;
  //   $return_date = $rdate;
  //   $this->load->view('bus_trip_search_result_view', compact('trip_list','trip_list_return','filter','trip_source','trip_destination','trip_sources','trip_destinations','journey_date','return_date','no_of_seat'));
  //   $this->load->view('front_footer_view');
  // }
  // public function ticket_search_filter()
  // {
  //   //echo json_encode($_GET); die();
  //   $cust_id = $this->cust_id;
  //   $trip_source = $this->input->get('trip_source_filter');
  //   $trip_destination = $this->input->get('trip_destination_filter');
  //   $journey_date = $this->input->get('journey_date_filter');
  //   $return_date = $this->input->get('return_date_filter');
  //   $depart_from = $this->input->get('depart_from');
  //   $bus_seat_type = $this->input->get('bus_seat_type');
  //   $bus_ac = $this->input->get('bus_ac');
  //   $ticket_to = $this->input->get('ticket_to');
  //   $ticket_from = $this->input->get('ticket_from');
  //   $operator = $this->input->get('operator');
  //   $bus_amen = $this->input->get('bus_amenities');
  //   $vehical_type_id = $this->input->get('vehical_type_id');
  //   $no_of_seat = $this->input->get('no_of_seat');
  //   $search_data = array(
  //     "trip_source" => trim($trip_source), 
  //     "trip_destination" => trim($trip_destination), 
  //     "journey_date" => trim($journey_date), 
  //     "vehical_type_id" => trim($vehical_type_id),
  //     "journey_day" => trim(strtolower(date('D', strtotime($journey_date)))), 
  //     "return_date" => trim($return_date),
  //     "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
  //     "cust_id" => (int)trim($cust_id), 
  //     "opr_id" => (int)trim($operator), 
  //   );
  //   //echo json_encode($search_data); die();
  //   $trip_list = $this->api_bus->get_trip_source_destination_search_list($search_data);
  //   //echo json_encode($trip_list); die();
  //   //Return Trip
  //   if($return_date!=""){
  //     $search_data_return = array(
  //     "trip_destination" => trim($trip_source), 
  //     "trip_source" => trim($trip_destination), 
  //     "vehical_type_id" => trim($vehical_type_id), 
  //     "return_date" => trim($return_date),
  //     "return_day" => trim(strtolower(date('D', strtotime($return_date)))),
  //     "cust_id" => (int)trim($cust_id), 
  //     "opr_id" => (int)trim($operator), 
  //     );
  //     $trip_list_return = $this->api_bus->get_trip_source_destination_search_list_return($search_data_return);
  //   }
  //   //echo json_encode($trip_list_return); die();
    
  //   $filter = 'basic';
  //   $trip_sources = $this->api_bus->get_trip_master_source_list_buyer();
  //   $trip_destinations = $this->api_bus->get_trip_master_destination_list_buyer();
  //   function zaid_date_time_validator($date, $format = 'Y-m-d H:i:s') { 
  //     $d = DateTime::createFromFormat($format, $date); return $d && $d->format($format) == $date; 
  //   }
  //   for($i=0; $i<sizeof($trip_list); $i++) {
  //     $unique_id = str_replace("/","",$journey_date)."_".$trip_list[$i]['trip_id'];
  //     $t=0; $seat_ac_amen=0; $pr=0;
  //     if(zaid_date_time_validator($depart_from , 'H:i')) {
  //       $depart=$this->api_bus->get_departure_time_filter($trip_list[$i]['trip_id'],$depart_from);
  //       if($depart==true){ $t=0; } if($depart == false){ $t++; }
  //     }
  //     if($bus_seat_type=="SEATER" || $bus_seat_type=="SLEEPER" || $bus_seat_type=="SEMI-SLEEPER" || $bus_ac=="AC" || $bus_ac=="NON AC" || $bus_amen!="") {
  //       $b= $this->api_bus->get_seat_ac_amen_filter($trip_list[$i]['bus_id'],$bus_seat_type,$bus_ac,$bus_amen);
  //       if($b==true){ $seat_ac_amen=0; } if($b==false) { $seat_ac_amen++; }
  //     }
  //     if($ticket_to!="" || $ticket_from!="") {
  //       $price = $this->api_bus->get_trip_master_search_price_filter($trip_list[$i]['trip_id'],$ticket_to,$ticket_from);
  //       if($price==true){ $pr=0; }
  //       if($price==false){ $pr++; } 
  //     }
  //     if($cancelled_trips = $this->api_bus->get_cancelled_seller_trips()){
  //       foreach ($cancelled_trips as $ct) {
  //         if($ct['unique_id'] == $unique_id || $t > 0 || $seat_ac_amen > 0 || $pr > 0 ) { array_push($trip_list[$i],"delete");
  //         } else { array_push($trip_list[$i],"safe"); }
  //       }
  //     } else {
  //       if($t > 0 || $seat_ac_amen > 0 || $pr > 0) {
  //         array_push($trip_list[$i],"delete");
  //       } else { array_push($trip_list[$i],"safe"); }
  //     }   
  //   }
  //   //Return Trip Filter
  //   if(isset($trip_list_return)) {
  //     for($i=0; $i<sizeof($trip_list_return); $i++) {
  //       $unique_id = str_replace("/","",$return_date)."_".$trip_list_return[$i]['trip_id'];
  //       $t=0; $seat_ac_amen=0; $pr=0;
  //       if(zaid_date_time_validator($depart_from , 'H:i')) {
  //         $depart=$this->api_bus->get_departure_time_filter($trip_list_return[$i]['trip_id'],$depart_from);
  //         if($depart==true) { $t =0; } if($depart == false){ $t++; }
  //       }
  //       if($bus_seat_type=="SEATER" || $bus_seat_type=="SLEEPER" || $bus_seat_type=="SEMI-SLEEPER" || $bus_ac=="AC" || $bus_ac=="NON AC" || $bus_amen!="") {
  //         $b= $this->api_bus->get_seat_ac_amen_filter($trip_list_return[$i]['bus_id'],$bus_seat_type,$bus_ac,$bus_amen);
  //         if($b==true) { $seat_ac_amen=0; } if($b==false) { $seat_ac_amen++; }
  //       }
  //       if($ticket_to!="" || $ticket_from!="") {
  //         $price = $this->api_bus->get_trip_master_search_price_filter($trip_list_return[$i]['trip_id'],$ticket_to,$ticket_from);
  //         if($price==true) { $pr=0; }
  //         if($price==false) { $pr++; } 
  //       }  
  //       if($cancelled_trips = $this->api_bus->get_cancelled_seller_trips()) {
  //         foreach ($cancelled_trips as $ct){
  //           if($ct['unique_id'] == $unique_id || $t > 0 || $seat_ac_amen > 0 || $pr > 0 ) { array_push($trip_list_return[$i],"delete"); 
  //           } else{ array_push($trip_list_return[$i],"safe"); }
  //         }  
  //       } else {
  //         if($t > 0 || $seat_ac_amen > 0 || $pr > 0) {
  //           array_push($trip_list_return[$i],"delete");
  //         } else { array_push($trip_list_return[$i],"safe"); }
  //       }
  //     }
  //   }
  //   $this->load->view('bus_trip_search_result_view', compact('trip_list_return','trip_list','trip_sources','trip_destinations','depart','filter','trip_source','trip_destination','journey_date','return_date','depart_from','bus_seat_type','bus_ac','ticket_to','ticket_from','operator','bus_amen','vehical_type_id','no_of_seat'));
  //   $this->load->view('front_footer_view');
  // }
  // public function ticket_booking_process()
  // {
  //   //echo json_encode($_GET); die();
  //   $journey_date = $this->input->get('journey_date');
  //   $return_date = $this->input->get('return_date');
  //   $ownward_trip_id = $this->input->get('ownward_trip');
  //   $no_of_seat = $this->input->get('no_of_seat');
  //   $trip_source = $this->input->get('trip_source');
  //   $trip_destination = $this->input->get('trip_destination');
  //   $cat_id = $this->input->get('cat_id');
  //   $return_trip = $this->input->get('return_trip');
  //   $seat_type = $this->input->get('seat_type');
  //   $return_seat_type = $this->input->get('return_seat_type');
  //   $pickup_point = $this->input->get('pickup_point');
  //   $return_pickup_point = $this->input->get('return_pickup_point');
  //   $drop_point = $this->input->get('drop_point');
  //   $return_drop_point = $this->input->get('return_drop_point');

  //   $pickup_lat_lng = $this->api_bus->get_locations_pickup_drop_points_for_booking_info_page($pickup_point);
  //   $drop_lat_lng =$this->api_bus->get_locations_pickup_drop_points_for_booking_info_page($drop_point);

  //   ${'ownward_selected_seats_count_' . $ownward_trip_id} = $this->input->get('ownward_selected_seats_count_'.$ownward_trip_id);
  //   ${'ownward_selected_seat_nos_' . $ownward_trip_id} = $this->input->get('ownward_selected_seat_nos_'.$ownward_trip_id);
  //   ${'return_selected_seats_count_' . $return_trip} = ($return_trip > 0) ? $this->input->get('return_selected_seats_count_'.$return_trip) : 0;
  //   ${'return_selected_seat_nos_' . $return_trip} = ($return_trip > 0) ? $this->input->get('return_selected_seat_nos_'.$return_trip) : 'NULL';
  //   $seat_price = $this->input->get('seat_price');
  //   $distance = $this->input->get('distance');
  //   $duration = $this->input->get('duration');
  //   $ownward_currency_sign = $this->input->get('ownward_currency_sign');
  //   $ownward_currency_id = $this->input->get('ownward_currency_id');
  //   $return_seat_price = ($return_trip > 0) ? $this->input->get('return_seat_price') : 0;
  //   $return_trip_source = $this->input->get('return_trip_source');
  //   $return_trip_destination = $this->input->get('return_trip_destination');
  //   $return_currency_sign = ($return_trip > 0) ? $this->input->get('return_currency_sign') : 'NULL';
  //   //echo json_encode($return_currency_sign); die();


  //   $ownward_trip_details = $this->api_bus->get_source_destination_of_trip_by_sdd_id($ownward_trip_id);
  //   //echo json_encode($ownward_trip_details);
  //   $ownward_trip_details = $this->api_bus->get_trip_master_details($ownward_trip_details[0]['trip_id']);
  //   //echo json_encode($ownward_trip_details); die();
    
  //   $this->session->set_userdata(array('return_date' => "", 'return_trip'=> "", 'cat_id' => $cat_id, 'journey_date' => $journey_date, 'ownward_trip'=> $ownward_trip_id, 'no_of_seat' => $no_of_seat, 'seat_type' => $seat_type, 'pickup_point' => $pickup_point, 'drop_point' => $drop_point, 'is_return' => '0', 'return_seat_type' => $return_seat_type, 'return_pickup_point' => $return_pickup_point, 'return_drop_point' => $return_drop_point, 
  //     'seat_price' => $seat_price,
  //     'distance' => $distance,
  //     'duration' => $duration,
  //     'ownward_currency_sign' => $ownward_currency_sign,
  //     'ownward_currency_id' => $ownward_currency_id,
  //     'return_seat_price' => $return_seat_price,
  //     'return_trip_destination' => $return_trip_destination,
  //     'return_currency_sign' => $return_currency_sign,
      
  //     'ownward_selected_seats_count_'. $ownward_trip_id => ${'ownward_selected_seats_count_' . $ownward_trip_id},
  //     'ownward_selected_seat_nos_'. $ownward_trip_id => ${'ownward_selected_seat_nos_' . $ownward_trip_id},
  //     'return_selected_seats_count_'. $return_trip => ${'return_selected_seats_count_' . $return_trip},
  //     'return_selected_seat_nos_'. $return_trip => ${'return_selected_seat_nos_' . $return_trip},

  //     'trip_source' => $trip_source,
  //     'trip_destination' => $trip_destination,
  //     'return_trip_source' => $return_trip_source,
  //     'return_trip_destination' => $return_trip_destination,
  //    ));
  //   if($return_trip > 0) {
  //     $this->session->set_userdata(array('return_date' => $return_date , 'return_trip' => $return_trip,  'is_return' => '1'));
  //   }

  //   $this->load->helper('captcha');
  //   $values = array(
  //     'word' => '',
  //     'word_length' => 8,
  //     'img_path' => 'resources/captcha/',
  //     'img_url' => base_url() .'resources/captcha/',
  //     'font_path' => base_url() . 'resources/captcha/ostrich-regular.ttf',
  //     'img_width' => '150',
  //     'img_height' => 45,
  //     'expiration' => 7200
  //   );
  //   $data = create_captcha($values);        
  //   //echo json_encode($data); die();
  //   $this->session->set_userdata('captcha_code', $data['word']);
    
  //   $countries = $this->user->get_countries();
  //   $this->load->view('front_ticket_booking_view', compact('_GET', 'countries' , 'data' , 'ownward_trip_details','drop_lat_lng','pickup_lat_lng'));
  //   $this->load->view('front_footer_view');
  // }
  // public function more_passengers_details()
  // {
  //   //echo json_encode($_GET); die();
  //   $countries = $this->user->get_countries();
  //   $this->load->view('front_ticket_booking_add_passenger_view' , compact('countries' , '_GET'));
  //   $this->load->view('front_footer_view');
  // }
  // public function ticket_booking()
  // {
  //   //echo json_encode($_GET); die();
  //   $ownward_selected_seat_nos = rtrim($_GET['ownward_selected_seat_nos'], ',');
  //   $return_selected_seat_nos = rtrim($_GET['return_selected_seat_nos'], ','); //die();
  //   $trip = $this->api_bus->get_source_destination_of_trip_by_sdd_id($_GET['ownward_trip']);
  //   $no_of_seat = $_GET['no_of_seat'];
  //   $user_details = $this->api_bus->get_user_details(67);
  //   $operator = $this->api_bus->get_bus_operator_profile($trip[0]['cust_id']);
  //   $unique_id = str_replace("/","",$_GET['journey_date'])."_".$trip[0]['trip_id'];
  //   $today =date('Y-m-d H:i:s');
  //   //echo json_encode($unique_id); die();
  //   $passengers_details = array();
  //   for($i=0; $no_of_seat>$i; $i++ ){

  //       $from = new DateTime($_GET['age'.$i]);
  //       $to   = new DateTime('today');
  //       $age  = $from->diff($to)->y;

  //     $data = array(
  //       'firstname' => $_GET['firstname'.$i],
  //       'lastname' => $_GET['lastname'.$i],
  //       'email_id' => $_GET['email_id'.$i],
  //       'mobile' => $_GET['mobile'.$i],
  //       'gender' => $_GET['gender'.$i],
  //       'age' => $age,
  //       'dob' => $_GET['age'.$i],
  //       'country_id' => $_GET['country_id'.$i],
  //     );
  //     array_push($passengers_details , $data);
  //   }
  //   //echo json_encode($passengers_details); die();
    
  //   if($_GET['return_trip'] > 0) {

  //     $sms_body_24_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['journey_date'].' '.$trip[0]['trip_depart_time'];
  //     $sms_body_2_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['journey_date'].' '.$trip[0]['trip_depart_time'];
  //     $email_body = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['journey_date'].' '.$trip[0]['trip_depart_time'];

  //     $data_master = array(
  //       'trip_id' => $trip[0]['trip_id'],
  //       'cust_id' => $user_details['cust_id'],
  //       'source_point_id' => $trip[0]['trip_source_id'],
  //       'source_point' => $trip[0]['trip_source'],
  //       'pickup_point' => $_GET['pickup_point'],
  //       'destination_point_id' => $trip[0]['trip_destination_id'],
  //       'destination_point' => $trip[0]['trip_destination'],
  //       'drop_point' => $_GET['drop_point'],
  //       'bus_id' => $trip[0]['bus_id'],
  //       'journey_date' => $_GET['journey_date'],
  //       'trip_start_date_time' => $trip[0]['trip_depart_time'],
  //       'bookig_date' =>  date('Y-m-d H:i:s'),
  //       'seat_type' =>  $_GET['seat_type'],
  //       'no_of_seats' =>  $no_of_seat,
  //       'seat_price' =>  $_GET['seat_price'],
  //       'ticket_price' =>  ($_GET['seat_price']*$no_of_seat),
  //       'currency_id' =>  $_GET['ownward_currency_id'],
  //       'currency_sign' =>  $_GET['ownward_currency_sign'],
  //       'cust_name' => $passengers_details[0]['firstname']." ".$passengers_details[0]['lastname'],
  //       'cust_contact' => $passengers_details[0]['mobile'] ,
  //       'cust_email' =>  $passengers_details[0]['email_id'],
  //       'operator_name' => $operator['firstname'],
  //       'operator_company_name' => $operator['company_name'],
  //       'operator_contact_name' =>  $operator['contact_no'],
  //       'operator_id' => $trip[0]['cust_id'],
  //       'balance_amount' => ($_GET['seat_price']*$no_of_seat),
  //       'ticket_status' => "booked",
  //       'status_updated_by_user_type' => "operator",
  //       'status_update_datetime' => date('Y-m-d H:i:s'),
  //       'is_return' => 0,
  //       'return_trip_id' => 0,
  //       'unique_id' => $unique_id,
  //       'booked_by' => 'walk-in',
  //       'status_updated_by' => $user_details['cust_id'],
  //       'cat_id' => '281',
  //       'country_id' => $passengers_details[0]['country_id'],
  //       'vip_seat_nos' => $ownward_selected_seat_nos,
  //     );
  //     $ticket_id = $this->api_bus->create_bus_ticket_booking_master($data_master);
  //     for($i=0; $i < sizeof($passengers_details) ; $i++) { 
  //       $data = array(
  //         'ticket_id' => $ticket_id,
  //         'trip_id' => $trip[0]['trip_id'],
  //         'cust_id' => $user_details['cust_id'],
  //         'operator_id' => $trip[0]['cust_id'],
  //         'firstname' => $passengers_details[$i]['firstname'],
  //         'lastname' => $passengers_details[$i]['lastname'],
  //         'seat_type' =>  $_GET['seat_type'],
  //         'cre_datetime' => date('Y-m-d H:i:s') ,
  //         'mobile' =>$passengers_details[$i]['mobile'],
  //         'email_id' => $passengers_details[$i]['email_id'] ,
  //         'gender' => $passengers_details[$i]['gender'],
  //         'country_id' => $passengers_details[$i]['country_id'],
  //         'age' => $passengers_details[$i] ['age'],
  //         'dob' => $passengers_details[$i] ['dob'],
  //         'journey_date' => $_GET['journey_date'],
  //         'unique_id' => $unique_id,
  //         'ticket_status' => 'booked',
  //       );
  //       $res = $this->api_bus->create_bus_ticket_booking_seat_details($data);

  //       //Check if trip date time is greater than current datetime + 2hrs 
  //       $datetime1 = new DateTime();
  //       //echo json_encode($datetime1); die();
  //       $datetime2 = new DateTime($_GET['journey_date'].' '.$trip[0]['trip_depart_time'].':00');
  //       $interval = $datetime1->diff($datetime2);
  //       //echo json_encode($interval); die();
  //       $years = $interval->y;
  //       $months = $interval->m;
  //       $days = $interval->d;
  //       $hours = $interval->h;
  //       $minutes = $interval->i;
  //       $seconds = $interval->s;
  //       $diff_type = $interval->invert;
  //       if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
  //       } else { 
  //         //Crone Job Alerts
  //         $crone_data_24_sms = array(
  //           'journey_date' => $_GET['journey_date'],
  //           'journey_time' => $trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id,
  //           'ticket_id' => $ticket_id,
  //           'trip_id' => $trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => $sms_body_24_hr,
  //           'email_body' => 'NULL',
  //           'alert_type' => 0,
  //           'sms_duration' => 24,
  //           'journey_datetime' => $_GET['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data_24_sms);
  //         $crone_data_2_sms = array(
  //           'journey_date' => $_GET['journey_date'],
  //           'journey_time' => $trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id,
  //           'ticket_id' => $ticket_id,
  //           'trip_id' => $trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => $sms_body_2_hr,
  //           'email_body' => 'NULL',
  //           'alert_type' => 0,
  //           'sms_duration' => 2,
  //           'journey_datetime' => $_GET['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data_2_sms);
  //         $crone_data = array(
  //           'journey_date' => $_GET['journey_date'],
  //           'journey_time' => $trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id,
  //           'ticket_id' => $ticket_id,
  //           'trip_id' => $trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => 'NULL',
  //           'email_body' => $email_body,
  //           'alert_type' => 1,
  //           'sms_duration' => 0,
  //           'journey_datetime' => $_GET['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data);
  //       }

  //       //Send Booking SMS
  //       $country_code = $this->api_bus->get_country_code_by_id($passengers_details[$i]['country_id']);
  //       /*$message = $this->lang->line('Your booking from')." ".$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination']." ".$this->lang->line('is confirmed Ticket ID - ').$ticket_id;*/
  //       $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id.'.  '.$this->lang->line('from').': '.$trip[0]['trip_source'].' '.$_GET['pickup_point'].'.  '.$this->lang->line('To').': '.$trip[0]['trip_destination'].' '.$_GET['drop_point'].'.  '.$this->lang->line('date').': '.$_GET['journey_date'].' '.$trip[0]['trip_depart_time']. ' - ' . $operator['company_name'];

  //       $this->api_bus->sendSMS((int)$country_code.trim($passengers_details[$i]['mobile']), $message);
  //       $this->api_sms->send_sms_whatsapp((int)$country_code.trim($passengers_details[$i]['mobile']), $message);
        
  //       //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
  //       if( trim($passengers_details[$i]['firstname']) == 'NULL' || trim($passengers_details[$i]['lastname'] == 'NULL') ) {
  //         $parts = explode("@", trim($passengers_details[$i]['email_id']));
  //         $username = $parts[0];
  //         $customer_name = $username;
  //       } else {
  //         $customer_name = trim($passengers_details[$i]['firstname']) . " " . trim($passengers_details[$i]['lastname']);
  //       }
  //       $this->api_sms->send_email_text($customer_name, trim($passengers_details[$i]['email_id']), $this->lang->line('Ticket booking confirmation'), trim($message));
  //     }//for

  //     //Update to workroom
  //     $workroom_update = array(
  //       'order_id' => $ticket_id,
  //       'cust_id' => $trip[0]['cust_id'],
  //       'deliverer_id' => $trip[0]['cust_id'],
  //       'text_msg' => $message,
  //       'attachment_url' =>  'NULL',
  //       'cre_datetime' => $today,
  //       'sender_id' => $trip[0]['cust_id'],
  //       'type' => 'order_status',
  //       'file_type' => 'text',
  //       'cust_name' => $customer_name,
  //       'deliverer_name' => $customer_name,
  //       'thumbnail' => 'NULL',
  //       'ratings' => 'NULL',
  //       'cat_id' => 281
  //     );
  //     $this->api_bus->add_bus_chat_to_workroom($workroom_update);

  //     //Update to Global Workroom
  //     $global_workroom_update = array(
  //       'service_id' => $ticket_id,
  //       'cat_id' => 281
  //     );
  //     $this->api_bus->create_global_workroom($global_workroom_update);

  //     $return_trip = $this->api_bus->get_source_destination_of_trip_by_sdd_id($_GET['return_trip']);
  //     $unique_id = str_replace("/","",$_GET['return_date'])."_".$return_trip[0]['trip_id'];

  //     $sms_body_24_hr = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['return_date'].' '.$return_trip[0]['trip_depart_time'];
  //     $sms_body_2_hr = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['return_date'].' '.$return_trip[0]['trip_depart_time'];
  //     $email_body = $this->lang->line('Your trip from ').$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['return_date'].' '.$return_trip[0]['trip_depart_time'];

  //     $return_data_master = array(
  //       'trip_id' => $return_trip[0]['trip_id'],
  //       'cust_id' => $user_details['cust_id'],
  //       'source_point_id' => $return_trip[0]['trip_source_id'],
  //       'source_point' => $return_trip[0]['trip_source'],
  //       'pickup_point' =>$_GET['return_pickup_point'],
  //       'destination_point_id' => $return_trip[0]['trip_destination_id'],
  //       'destination_point' => $return_trip[0]['trip_destination'],
  //       'drop_point' => $_GET['return_drop_point'],
  //       'bus_id' => $return_trip[0]['bus_id'],
  //       'journey_date' => $_GET['return_date'],
  //       'trip_start_date_time' => $return_trip[0]['trip_depart_time'],
  //       'bookig_date' => date('Y-m-d H:i:s'),
  //       'seat_type' => $_GET['return_seat_type'],
  //       'no_of_seats' => $no_of_seat,
  //       'seat_price' => $_GET['return_seat_price'],
  //       'ticket_price' => ($_GET['return_seat_price']*$no_of_seat),
  //       'currency_id' => $_GET['ownward_currency_id'],
  //       'currency_sign' => $_GET['return_currency_sign'],
  //       'cust_name' => $passengers_details[0]['firstname']." ".$passengers_details[0]['lastname'],
  //       'cust_contact' => $passengers_details[0]['mobile'] ,
  //       'cust_email' =>  $passengers_details[0]['email_id'],
  //       'operator_name' => $operator['firstname'],
  //       'operator_company_name' => $operator['company_name'],
  //       'operator_contact_name' =>  $operator['contact_no'],
  //       'operator_id' => $return_trip[0]['cust_id'],
  //       'balance_amount' => ($_GET['return_seat_price']*$no_of_seat),
  //       'ticket_status' => "booked",
  //       'status_updated_by_user_type' => "operator",
  //       'status_update_datetime' => date('Y-m-d H:i:s'),
  //       'is_return' => 1,
  //       'return_trip_id' => $ticket_id,
  //       'unique_id' => $unique_id,
  //       'booked_by' => 'walk-in',
  //       'status_updated_by' => $user_details['cust_id'],
  //       'cat_id' => '281',
  //       'country_id' => $passengers_details[0]['country_id'],
  //       'vip_seat_nos' => $return_selected_seat_nos,
  //     );
  //     $return_ticket_id = $this->api_bus->create_bus_ticket_booking_master($return_data_master);

  //     //Update return trip ID
  //     $dt = array(
  //       'return_trip_id' => $return_ticket_id
  //     );
  //     $uptd = $this->api_bus->update_booking_details($dt , $ticket_id);

  //     //Adding return passenger details
  //     for($i=0; $i < sizeof($passengers_details) ; $i++) { 
  //       $data = array(
  //         'ticket_id' => $return_ticket_id,
  //         'trip_id' => $return_trip[0]['trip_id'],
  //         'cust_id' => $user_details['cust_id'],
  //         'operator_id' => $return_trip[0]['cust_id'],
  //         'firstname' => $passengers_details[$i]['firstname'],
  //         'lastname' => $passengers_details[$i]['lastname'],
  //         'seat_type' =>  $_GET['return_seat_type'],
  //         'cre_datetime' => date('Y-m-d H:i:s') ,
  //         'mobile' =>$passengers_details[$i]['mobile'],
  //         'email_id' => $passengers_details[$i]['email_id'] ,
  //         'gender' => $passengers_details[$i]['gender'],
  //         'country_id' => $passengers_details[$i]['country_id'],
  //         'age' => $passengers_details[$i] ['age'],
  //         'dob' => $passengers_details[$i] ['dob'],
  //         'journey_date' => $_GET['return_date'],
  //         'unique_id' => $unique_id,
  //         'ticket_status' => 'booked',
  //       );
  //       $res = $this->api_bus->create_bus_ticket_booking_seat_details($data);
  //       //Check if trip date time is greater than current datetime + 2hrs 
  //       //$passengers_details[$i]
  //       $datetime1 = new DateTime();
  //       //echo json_encode($datetime1); die();
  //       $datetime2 = new DateTime($_GET['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00');
  //       $interval = $datetime1->diff($datetime2);
  //       //echo json_encode($interval); die();
  //       $years = $interval->y;
  //       $months = $interval->m;
  //       $days = $interval->d;
  //       $hours = $interval->h;
  //       $minutes = $interval->i;
  //       $seconds = $interval->s;
  //       $diff_type = $interval->invert;
  //       if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
  //       } else { 
  //         //Crone Job Alerts
  //         $crone_data_24_sms = array(
  //           'journey_date' => $_GET['return_date'],
  //           'journey_time' => $return_trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id, 
  //           'ticket_id' => $return_ticket_id,
  //           'trip_id' => $return_trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => $sms_body_24_hr,
  //           'email_body' => 'NULL',
  //           'alert_type' => 0,
  //           'sms_duration' => 24,
  //           'journey_datetime' => $_GET['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data_24_sms);
  //         $crone_data_2_sms = array(
  //           'journey_date' => $_GET['return_date'],
  //           'journey_time' => $return_trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id, 
  //           'ticket_id' => $return_ticket_id,
  //           'trip_id' => $return_trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => $sms_body_2_hr,
  //           'email_body' => 'NULL',
  //           'alert_type' => 0,
  //           'sms_duration' => 2,
  //           'journey_datetime' => $_GET['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data_2_sms);
  //         $crone_data = array(
  //           'journey_date' => $_GET['return_date'],
  //           'journey_time' => $return_trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id, 
  //           'ticket_id' => $return_ticket_id,
  //           'trip_id' => $return_trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => 'NULL',
  //           'email_body' => $email_body,
  //           'alert_type' => 1,
  //           'sms_duration' => 0,
  //           'journey_datetime' => $_GET['journey_date'].' '.$return_trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data);
  //       }

  //       //Send Booking SMS
  //       $country_code = $this->api_bus->get_country_code_by_id($passengers_details[$i]['country_id']);
  //       //$message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$return_ticket_id;
  //       /*$message = $this->lang->line('Your booking from')." ".$return_trip[0]['trip_source'].$this->lang->line(' to ').$return_trip[0]['trip_destination']." ".$this->lang->line('is confirmed Ticket ID - ').$return_ticket_id;*/
  //       $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$return_ticket_id.'.  '.$this->lang->line('from').': '.$return_trip[0]['trip_source'].' '.$_GET['return_pickup_point'].'.  '.$this->lang->line('To').': '.$return_trip[0]['trip_destination'].' '.$_GET['return_drop_point'].'.  '.$this->lang->line('date').': '.$_GET['journey_date'].' '.$return_trip[0]['trip_depart_time']. ' - ' . $operator['company_name'];


  //       $this->api_bus->sendSMS((int)$country_code.trim($passengers_details[$i]['mobile']), $message);
        
  //       //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
  //       if( trim($passengers_details[$i]['firstname']) == 'NULL' || trim($passengers_details[$i]['lastname'] == 'NULL') ) {
  //         $parts = explode("@", trim($passengers_details[$i]['email_id']));
  //         $username = $parts[0];
  //         $customer_name = $username;
  //       } else {
  //         $customer_name = trim($passengers_details[$i]['firstname']) . " " . trim($passengers_details[$i]['lastname']);
  //       }
  //       $this->api_sms->send_email_text($customer_name, trim($passengers_details[$i]['email_id']), $this->lang->line('Ticket booking confirmation'), trim($message));
  //     }//for

  //     //Update to workroom
  //     $workroom_update = array(
  //       'order_id' => $return_ticket_id,
  //       'cust_id' => $return_trip[0]['cust_id'],
  //       'deliverer_id' => $return_trip[0]['cust_id'],
  //       'text_msg' => $message,
  //       'attachment_url' =>  'NULL',
  //       'cre_datetime' => $today,
  //       'sender_id' => $trip[0]['cust_id'],
  //       'type' => 'order_status',
  //       'file_type' => 'text',
  //       'cust_name' => $customer_name,
  //       'deliverer_name' => $customer_name,
  //       'thumbnail' => 'NULL',
  //       'ratings' => 'NULL',
  //       'cat_id' => 281
  //     );
  //     $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //   }
    
  //   if(!isset($data_master)) {

  //     $sms_body_24_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['journey_date'].' '.$trip[0]['trip_depart_time'];
  //     $sms_body_2_hr = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['journey_date'].' '.$trip[0]['trip_depart_time'];
  //     $email_body = $this->lang->line('Your trip from ').$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination'].$this->lang->line(' will start in 24hrs. Departure Date/Time: ').$_GET['journey_date'].' '.$trip[0]['trip_depart_time'];

  //     $data_master = array(
  //       'trip_id' => $trip[0]['trip_id'],
  //       'cust_id' => $user_details['cust_id'],
  //       'source_point_id' => $trip[0]['trip_source_id'],
  //       'source_point' => $trip[0]['trip_source'],
  //       'pickup_point' => $_GET['pickup_point'],
  //       'destination_point_id' => $trip[0]['trip_destination_id'],
  //       'destination_point' => $trip[0]['trip_destination'],
  //       'drop_point' => $_GET['drop_point'],
  //       'bus_id' => $trip[0]['bus_id'],
  //       'journey_date' => $_GET['journey_date'],
  //       'trip_start_date_time' => $trip[0]['trip_depart_time'],
  //       'bookig_date' =>  date('Y-m-d H:i:s'),
  //       'seat_type' =>  $_GET['seat_type'],
  //       'no_of_seats' =>  $no_of_seat,
  //       'seat_price' =>  $_GET['seat_price'],
  //       'ticket_price' =>  ($_GET['seat_price']*$no_of_seat),
  //       'currency_id' =>  $_GET['ownward_currency_id'],
  //       'currency_sign' =>  $_GET['ownward_currency_sign'],
  //       'cust_name' => $passengers_details[0]['firstname']." ".$passengers_details[0]['lastname'],
  //       'cust_contact' => $passengers_details[0]['mobile'] ,
  //       'cust_email' =>  $passengers_details[0]['email_id'],
  //       'operator_name' => $operator['firstname'],
  //       'operator_company_name' => $operator['company_name'],
  //       'operator_contact_name' =>  $operator['contact_no'],
  //       'operator_id' => $trip[0]['cust_id'],
  //       'balance_amount' => ($_GET['seat_price']*$no_of_seat),
  //       'ticket_status' => "booked",
  //       'status_updated_by_user_type' => "operator",
  //       'status_update_datetime' => date('Y-m-d H:i:s'),
  //       'is_return' => 0,
  //       'return_trip_id' => 0,
  //       'unique_id' => $unique_id,
  //       'booked_by' => 'walk-in',
  //       'status_updated_by' => $user_details['cust_id'],
  //       'cat_id' => '281',
  //       'country_id' => $passengers_details[0]['country_id'],
  //       'vip_seat_nos' => $ownward_selected_seat_nos,
  //     );
  //     $ticket_id = $this->api_bus->create_bus_ticket_booking_master($data_master);
  //     for($i=0; $i < sizeof($passengers_details) ; $i++) { 
  //       $data = array(
  //         'ticket_id' => $ticket_id,
  //         'trip_id' => $trip[0]['trip_id'],
  //         'cust_id' => $user_details['cust_id'],
  //         'operator_id' => $trip[0]['cust_id'],
  //         'firstname' => $passengers_details[$i]['firstname'],
  //         'lastname' => $passengers_details[$i]['lastname'],
  //         'seat_type' =>  $_GET['seat_type'],
  //         'cre_datetime' => date('Y-m-d H:i:s') ,
  //         'mobile' =>$passengers_details[$i]['mobile'],
  //         'email_id' => $passengers_details[$i]['email_id'] ,
  //         'gender' => $passengers_details[$i]['gender'],
  //         'country_id' => $passengers_details[$i]['country_id'],
  //         'age' => $passengers_details[$i] ['age'],
  //         'dob' => $passengers_details[$i] ['dob'],
  //         'journey_date' => $_GET['journey_date'],
  //         'unique_id' => $unique_id,
  //         'ticket_status' => 'booked',
  //       );
  //       $res = $this->api_bus->create_bus_ticket_booking_seat_details($data);

  //       //Check if trip date time is greater than current datetime + 2hrs 
  //       $datetime1 = new DateTime();
  //       //echo json_encode($datetime1); die();
  //       $datetime2 = new DateTime($_GET['journey_date'].' '.$trip[0]['trip_depart_time'].':00');
  //       $interval = $datetime1->diff($datetime2);
  //       //echo json_encode($interval); die();
  //       $years = $interval->y;
  //       $months = $interval->m;
  //       $days = $interval->d;
  //       $hours = $interval->h;
  //       $minutes = $interval->i;
  //       $seconds = $interval->s;
  //       $diff_type = $interval->invert;
  //       if($diff_type > 0 && $years <= 0 && $months <= 0 && $days <= 0 && $hours >= 2 && $minutes < 5) {
  //       } else { 
  //         //Crone Job Alerts
  //         $crone_data_24_sms = array(
  //           'journey_date' => $_GET['journey_date'],
  //           'journey_time' => $trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id,
  //           'ticket_id' => $ticket_id,
  //           'trip_id' => $trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => $sms_body_24_hr,
  //           'email_body' => 'NULL',
  //           'alert_type' => 0,
  //           'sms_duration' => 24,
  //           'journey_datetime' => $_GET['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data_24_sms);
  //         $crone_data_2_sms = array(
  //           'journey_date' => $_GET['journey_date'],
  //           'journey_time' => $trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id,
  //           'ticket_id' => $ticket_id,
  //           'trip_id' => $trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => $sms_body_2_hr,
  //           'email_body' => 'NULL',
  //           'alert_type' => 0,
  //           'sms_duration' => 2,
  //           'journey_datetime' => $_GET['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data_2_sms);
  //         $crone_data = array(
  //           'journey_date' => $_GET['journey_date'],
  //           'journey_time' => $trip[0]['trip_depart_time'].':00',
  //           'passenger_name' => $passengers_details[$i]['firstname'].' '.$passengers_details[$i]['lastname'],
  //           'mobile' => $passengers_details[$i]['mobile'],
  //           'country_id' =>  $passengers_details[$i]['country_id'],
  //           'email' => $passengers_details[$i]['email_id'],
  //           'unique_id' => $unique_id,
  //           'ticket_id' => $ticket_id,
  //           'trip_id' => $trip[0]['trip_id'],
  //           'cre_datetime' => $today,
  //           'sms_body' => 'NULL',
  //           'email_body' => $email_body,
  //           'alert_type' => 1,
  //           'sms_duration' => 0,
  //           'journey_datetime' => $_GET['journey_date'].' '.$trip[0]['trip_depart_time'].':00',
  //         );
  //         $res = $this->api_bus->create_bus_booking_crone_alert($crone_data);
  //       }

  //       //Send Booking SMS
  //       $country_code = $this->api_bus->get_country_code_by_id($passengers_details[$i]['country_id']);
  //       $message = $this->lang->line('Your booking from')." ".$trip[0]['trip_source'].$this->lang->line(' to ').$trip[0]['trip_destination']." ".$this->lang->line('is confirmed Ticket ID - ').$ticket_id;

  //       $message = $this->lang->line('Your booking is confirmed. Ticket ID - ').$ticket_id.'.  '.$this->lang->line('from').': '.$trip[0]['trip_source'].' '.$_GET['pickup_point'].'.  '.$this->lang->line('To').': '.$trip[0]['trip_destination'].' '.$_GET['drop_point'].'.  '.$this->lang->line('date').': '.$_GET['journey_date'].' '.$trip[0]['trip_depart_time']. ' - ' . $operator['company_name'];

  //       $this->api_bus->sendSMS((int)$country_code.trim($passengers_details[$i]['mobile']), $message);
        
  //       //Email Ticket Booking confirmation Customer name, customer Email, Subject, Message
  //       if( trim($passengers_details[$i]['firstname']) == 'NULL' || trim($passengers_details[$i]['lastname'] == 'NULL') ) {
  //         $parts = explode("@", trim($passengers_details[$i]['email_id']));
  //         $username = $parts[0];
  //         $customer_name = $username;
  //       } else {
  //         $customer_name = trim($passengers_details[$i]['firstname']) . " " . trim($passengers_details[$i]['lastname']);
  //       }
  //       $this->api_sms->send_email_text($customer_name, trim($passengers_details[$i]['email_id']), $this->lang->line('Ticket booking confirmation'), trim($message));
  //     }//for

  //     //Update to workroom
  //     $workroom_update = array(
  //       'order_id' => $ticket_id,
  //       'cust_id' => $trip[0]['cust_id'],
  //       'deliverer_id' => $trip[0]['cust_id'],
  //       'text_msg' => $message,
  //       'attachment_url' =>  'NULL',
  //       'cre_datetime' => $today,
  //       'sender_id' => $trip[0]['cust_id'],
  //       'type' => 'order_status',
  //       'file_type' => 'text',
  //       'cust_name' => $user_details['firstname'],
  //       'deliverer_name' => $operator['firstname'],
  //       'thumbnail' => 'NULL',
  //       'ratings' => 'NULL',
  //       'cat_id' => 281
  //     );
  //     $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //     //Update to Global Workroom
  //     $global_workroom_update = array(
  //       'service_id' => $ticket_id,
  //       'cat_id' => 281
  //     );
  //     $this->api_bus->create_global_workroom($global_workroom_update);
  //   } //if 
    
  //   redirect(base_url('ticket-payment').'/'.$ticket_id,'refresh');
  // }
  // public function bus_ticket_payment_view()
  // {
  //   //echo "zaid"; die();
  //   if(!is_null($this->input->post('ticket_id'))) { $ticket_id = (int) $this->input->post('ticket_id');
  //   } else if(!is_null($this->uri->segment(2))) { $ticket_id = (int)$this->uri->segment(2);
  //   } else { redirect(base_url('ticket')); }
  //   $details =  $this->api->get_trip_booking_details($ticket_id);

  //   //if ownward trip has return trip - get return trip details.
  //   $return_details = array();
  //   if($details['return_trip_id'] > 0) {
  //     $return_details =  $this->api->get_trip_booking_details($details['return_trip_id']);
  //   }

  //   //echo json_encode($details); die();
  //   $this->load->view('bus_ticket_payment_view',compact('details','return_details'));
  //   $this->load->view('front_footer_view');
  // }
  // public function ticket_mark_as_cod()
  // {
  //   //echo json_encode($_POST); die();
  //   $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
  //   $details = $this->api_bus->get_trip_booking_details($ticket_id);
  //   $update_data = array(
  //     "payment_mode" => 'cod',
  //     'boarding_code' => $details['unique_id'].'_'.$details['ticket_id'],
  //   );
  //   //echo json_encode($update_data); die();
  //   if($this->api_bus->update_booking_details($update_data, $ticket_id)) {
  //     //if ownward trip has return trip - get return trip details.
  //     $return_details = array();
  //     if($details['return_trip_id'] > 0) {
  //       $return_details =  $this->api_bus->get_trip_booking_details($details['return_trip_id']);
  //       $update_data = array(
  //         "payment_mode" => 'cod',
  //         "boarding_code" => $return_details['unique_id'].'_'.$return_details['ticket_id'],
  //       );
  //       $this->api_bus->update_booking_details($update_data, $return_details['ticket_id']);
  //     }
  //     $this->session->set_flashdata('success', $this->lang->line('updated_successfully'));
  //   } else { $this->session->set_flashdata('error', $this->lang->line('update_failed')); }
  //   redirect(base_url('ticket-payment-confirmation').'/'.$ticket_id,'refresh');
  // }
  // public function ticket_payment_by_mtn()
  // {
  //   //echo json_encode($_POST); die();
  //   $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
  //   $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
  //   $user_details = $this->api_bus->get_user_details($cust_id);
  //   $payment_method = 'mtn';
  //   $phone_no = $this->input->post('phone_no', TRUE);
  //   if(substr($phone_no, 0, 1) == 0) $phone_no = ltrim($phone_no, 0);
  //   $today = date('Y-m-d h:i:s');
  //   $ticket_details = $this->api_bus->get_trip_booking_details($ticket_id);
  //   $ticket_price = $this->input->post('ticket_price', TRUE); $ticket_price = (int) $ticket_price;
  //   $currency_sign = $ticket_details['currency_sign'];
  //   $seat_details = $this->api_bus->get_ticket_seat_details($ticket_id);
  //   //echo json_encode($ticket_details['cust_contact']); die();

  //   $country_code = $this->api_bus->get_country_code_by_id($ticket_details['country_id']);
  //   $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
  //   $this->api_bus->sendSMS((int)$country_code.trim($ticket_details['cust_contact']), $message);
  //   $this->api_sms->send_sms_whatsapp((int)$country_code.trim($ticket_details['cust_contact']), $message);
   
  //   //MTN Payment Gateway
  //   /*$url = "https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml?idbouton=2&typebouton=PAIE&_amount=".$ticket_price."&_tel=".$phone_no."&_clP=&_email=admin@gonagoo.com";
  //   $ch = curl_init();
  //   curl_setopt ($ch, CURLOPT_URL, $url);
  //   curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
  //   curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
  //   $contents = curl_exec($ch);
  //   $mtn_pay_res = json_decode($contents, TRUE);*/
  //   //var_dump($mtn_pay_res); die();

  //   $transaction_id = 123;
  //   //echo json_encode($ticket_details); die();

  //   $location_details = $this->api_bus->get_bus_location_details($ticket_details['source_point_id']);
  //   $country_id = $this->api_bus->get_country_id_by_name($location_details['country_name']);
  //   //echo json_encode($country_id); die();
  //   $gonagoo_commission_percentage = $this->api_bus->get_gonagoo_bus_commission_details((int)$country_id);
  //   //echo json_encode($gonagoo_commission_percentage); die();
  //   $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

  //   $total_amount_paid = $ticket_price;
  //   $transfer_account_number = $phone_no;
  //   $bank_name = "NULL";
  //   $account_holder_name = "NULL";
  //   $iban = "NULL";
  //   $email_address = "NULL";
  //   $mobile_number = $phone_no;    
    
  //   $payment_data = array();
  //   //if($mtn_pay_res['StatusCode'] === "01"){
  //   if(true) {
  //     $update_data = array(
  //       "payment_method" => trim($payment_method),
  //       "paid_amount" => trim($ticket_details['ticket_price']),
  //       "balance_amount" => 0,
  //       "transaction_id" => trim($transaction_id),
  //       "payment_datetime" => $today,
  //       "payment_mode" => "online",
  //       "payment_by" => "web",
  //       "complete_paid" => 1,
  //     );
  //     if($this->api_bus->update_booking_details($update_data, $ticket_id)){

  //       //---------------------Insert counter payment record----------------------
  //         $data_counter_sale_payment = array(
  //           "cat_id" => $ticket_details['cat_id'],
  //           "service_id" => $ticket_details['ticket_id'],
  //           "cust_id" => $ticket_details['operator_id'],
  //           "cre_datetime" => $today,
  //           "type" => 1,
  //           "transaction_type" => 'payment',
  //           "amount" => trim($ticket_details['ticket_price']),
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
  //           "cat_name" => 'ticket_booking',
  //           "customer_email" => trim($seat_details[0]['email_id']),
  //           "customer_mobile" => trim($seat_details[0]['mobile']),
  //           "customer_country_id" => trim($seat_details[0]['country_id']),
  //         );
  //         $this->api_bus->insert_counter_sale_payment($data_counter_sale_payment);
  //       //---------------------Insert counter payment record----------------------

  //       //************************Update to workroom*************************************
  //         $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);
  //         $workroom_update = array(
  //           'order_id' => $ticket_details['ticket_id'],
  //           'cust_id' => $ticket_details['cust_id'],
  //           'deliverer_id' => $ticket_details['operator_id'],
  //           'text_msg' => $message,
  //           'attachment_url' =>  'NULL',
  //           'cre_datetime' => $today,
  //           'sender_id' => $ticket_details['operator_id'],
  //           'type' => 'payment',
  //           'file_type' => 'text',
  //           'cust_name' => $ticket_details['cust_name'],
  //           'deliverer_name' => $ticket_details['operator_name'],
  //           'thumbnail' => 'NULL',
  //           'ratings' => 'NULL',
  //           'cat_id' => 281
  //         );
  //         $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //       //************************Update to workroom*************************************
  //       /************************ Payment Section **********************************/

  //         //Add Ownward Trip Payment to trip operator account + History
  //           if($this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
  //             $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //           } else {
  //             $insert_data_customer_master = array(
  //               "user_id" => trim($ticket_details['operator_id']),
  //               "account_balance" => 0,
  //               "update_datetime" => $today,
  //               "operation_lock" => 1,
  //               "currency_code" => trim($ticket_details['currency_sign']),
  //             );
  //             $this->api_bus->insert_gonagoo_customer_record($insert_data_customer_master);
  //             $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //           }
  //           $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
  //           $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
  //           $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //           $update_data_account_history = array(
  //             "account_id" => (int)$customer_account_master_details['account_id'],
  //             "order_id" => (int)$ticket_id,
  //             "user_id" => (int)trim($ticket_details['operator_id']),
  //             "datetime" => $today,
  //             "type" => 1,
  //             "transaction_type" => 'add',
  //             "amount" => trim($ticket_details['ticket_price']),
  //             "account_balance" => trim($customer_account_master_details['account_balance']),
  //             "withdraw_request_id" => 0,
  //             "currency_code" => trim($ticket_details['currency_sign']),
  //             "cat_id" => 281
  //           );
  //           $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //         //Add Ownward Trip Payment to trip operator account + History

  //         /* ------------------------Ownward ticket invoice-------------------------- */
  //           $ownward_trip_operator_detail = $this->api_bus->get_user_details($ticket_details['operator_id']);
  //           if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
  //             $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
  //             $username = $parts[0];
  //             $operator_name = $username;
  //           } else {
  //             $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
  //           }

  //           $user_details = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']); 
  //           //echo json_encode($user_details); die();
  //           if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
  //             $parts = explode("@", trim($user_details[0]['email_id']));
  //             $username = $parts[0];
  //             $customer_name = $username;
  //           } else {
  //             $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
  //           }

  //           $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //           require_once($phpinvoice);
  //           //Language configuration for invoice
  //           $lang = $this->input->post('lang', TRUE);
  //           if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //           else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //           else { $invoice_lang = "englishApi_lang"; }
  //           //Invoice Configuration
  //           $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
  //           $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //           $invoice->setColor("#000");
  //           $invoice->setType("");
  //           $invoice->setReference($ticket_details['ticket_id']);
  //           $invoice->setDate(date('M dS ,Y',time()));

  //           $operator_country = $this->api_bus->get_country_details(trim($ownward_trip_operator_detail['country_id']));
  //           $operator_state = $this->api_bus->get_state_details(trim($ownward_trip_operator_detail['state_id']));
  //           $operator_city = $this->api_bus->get_city_details(trim($ownward_trip_operator_detail['city_id']));

  //           $user_country = $this->api_bus->get_country_details(trim($user_details[0]['country_id']));
  //           //echo json_encode($user_country); die();
  //           // $user_state = $this->api_bus->get_state_details(trim($user_details['state_id']));
  //           // $user_city = $this->api_bus->get_city_details(trim($user_details['city_id']));
  //           $user_state = "---";
  //           $user_city = "---";

  //           $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
  //           $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //           $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //           $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

  //           $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']), $user_details[0]['email_id']));

  //           //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
  //           $eol = PHP_EOL;
  //           /* Adding Items in table */
  //           $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
  //           $rate = round(trim($ticket_details['ticket_price']),2);
  //           $total = $rate;
  //           $payment_datetime = substr($today, 0, 10);
  //           //set items
  //           $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //           /* Add totals */
  //           $invoice->addTotal($this->lang->line('sub_total'),$total);
  //           $invoice->addTotal($this->lang->line('taxes'),'0');
  //           $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //           /* Set badge */ 
  //           $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
  //           /* Add title */
  //           $invoice->addTitle($this->lang->line('tnc'));
  //           /* Add Paragraph */
  //           $invoice->addParagraph($gonagoo_address['terms']);
  //           /* Add title */
  //           $invoice->addTitle($this->lang->line('payment_dtls'));
  //           /* Add Paragraph */
  //           $invoice->addParagraph($gonagoo_address['payment_details']);
  //           /* Add title */
  //           $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //           /* Add Paragraph */
  //           $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //           /* Set footer note */
  //           $invoice->setFooternote($gonagoo_address['company_name']);
  //           /* Render */
  //           $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //           //Update File path
  //           $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
  //           //Move file to upload folder
  //           rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //           //Update Order Invoice
  //           $update_data = array(
  //             "invoice_url" => "ticket-invoices/".$pdf_name,
  //           );
  //           $this->api_bus->update_booking_details($update_data, $ticket_details['ticket_id']);
  //           $workroom_invoice_url = "ticket-invoices/".$pdf_name;
  //           //Payment Invoice Update to workroom
  //           $workroom_update = array(
  //             'order_id' => $ticket_details['ticket_id'],
  //             'cust_id' => $ticket_details['cust_id'],
  //             'deliverer_id' => $ticket_details['operator_id'],
  //             'text_msg' => $this->lang->line('Invoice'),
  //             'attachment_url' =>  $workroom_invoice_url,
  //             'cre_datetime' => $today,
  //             'sender_id' => $ticket_details['operator_id'],
  //             'type' => 'raise_invoice',
  //             'file_type' => 'pdf',
  //             'cust_name' => $ticket_details['cust_name'],
  //             'deliverer_name' => $ticket_details['operator_name'],
  //             'thumbnail' => 'NULL',
  //             'ratings' => 'NULL',
  //             'cat_id' => 281
  //           );
  //           $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //         /* ------------------------Ownward ticket invoice-------------------------- */
          
  //         /* -----------------Email Invoice to customer-------------------------- */
  //           $emailBody = $this->lang->line('Please download your ticket payment invoice.');
  //           $gonagooAddress = $gonagoo_address['company_name'];
  //           $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
  //           $emailAddress = $user_details[0]['email_id'];
  //           $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //           $fileName = $pdf_name;
  //           $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //         /* -----------------------Email Invoice to customer-------------------- */
          
  //         /* ------------------------Generate Ticket pdf ------------------------ */
  //           $user_detailss = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']);
  //           $cust_sms = $this->lang->line("Dear")."! ".$user_detailss[0]['firstname']." ".$user_detailss[0]['lastname']." ".$this->lang->line('Your')." ".$this->lang->line('Payment completed for ticket ID - ').$ticket_details['ticket_id']." , ".$this->lang->line('For')." ".$ticket_details['source_point']." ".$this->lang->line('To')." ".$ticket_details['destination_point'];
  //           //echo json_encode($cust_sms); die();
  //           $operator_details = $this->api_bus->get_bus_operator_profile($ticket_details['operator_id']);
  //           if($operator_details["avatar_url"]==" " || $operator_details["avatar_url"] == "NULL"){
  //             $operator_avtr = base_url("resources/no-image.jpg");
  //           }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
  //           require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
  //             QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
  //              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
  //              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
  //             if(rename($img_src, $img_dest));
  //           $html ='
  //           <page format="100x100" orientation="L" style="font: arial;">';
  //           $html .= '
  //           <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
  //             <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
  //               <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
  //               <div style="margin-top:27px">
  //                 <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
  //                 <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
  //               </div>
  //             </div>
  //             <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
  //                 <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
  //                 <br />
  //                 <h6>'.$this->lang->line("slider_heading1").'</h6>
  //             </div>                
  //           </div>
  //           <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
  //             <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
  //               '.$this->lang->line("Ownward_Trip").'<br/>
  //             <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
  //             <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
  //             <h6 style="margin-top:-10px;">
  //             <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
  //             <h6 style="margin-top:-8px;">
  //             <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
  //             <h6 style="margin-top:-8px;">
  //             <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
  //             <h6 style="margin-top:-8px;"> 
  //             <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
  //             <h6 style="margin-top:-8px;">
  //             <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
  //             $ticket_seat_details = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']);

  //             $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
  //             foreach ($ticket_seat_details as $seat)
  //             {
  //               $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
  //               if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
  //                $html .='</td><td>'.$this->api_bus->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
  //             }
  //           $html .='</tbody></table></div></page>';            
  //           $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
  //           require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
  //           $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
  //           $html2pdf->pdf->SetDisplayMode('default');
  //           $html2pdf->writeHTML($html);
  //           $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
  //           $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
  //           rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
  //           //whatsapp api send ticket
  //           $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
  //           $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
  //           $this->api_sms->send_sms_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$cust_sms);
  //           //echo json_encode($result); die();
  //         /* ------------------------Generate Ticket pdf ------------------------ */

  //         /* ---------------------Email Ticket to customer----------------------- */
  //           $emailBody = $this->lang->line('Please download your e-ticket.');
  //           $gonagooAddress = $gonagoo_address['company_name'];
  //           $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
  //           $emailAddress = $user_details[0]['email_id'];
  //           $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
  //           $fileName = $my_ticket_pdf;
  //           $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //         /* ---------------------Email Ticket to customer----------------------- */

  //         //Deduct Ownward Trip Commission From Operator Account
  //           $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
  //           $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
  //           $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //           $update_data_account_history = array(
  //             "account_id" => (int)$customer_account_master_details['account_id'],
  //             "order_id" => (int)$ticket_id,
  //             "user_id" => (int)trim($ticket_details['operator_id']),
  //             "datetime" => $today,
  //             "type" => 0,
  //             "transaction_type" => 'ticket_commission',
  //             "amount" => trim($ownward_trip_commission),
  //             "account_balance" => trim($customer_account_master_details['account_balance']),
  //             "withdraw_request_id" => 0,
  //             "currency_code" => trim($ticket_details['currency_sign']),
  //             "cat_id" => 281
  //           );
  //           $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //         //Deduct Ownward Trip Commission From Operator Account

  //         //Add Ownward Trip Commission To Gonagoo Account
  //           if($this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
  //             $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));
  //           } else {
  //             $insert_data_gonagoo_master = array(
  //               "gonagoo_balance" => 0,
  //               "update_datetime" => $today,
  //               "operation_lock" => 1,
  //               "currency_code" => trim($ticket_details['currency_sign']),
  //             );
  //             $this->api_bus->insert_gonagoo_master_record($insert_data_gonagoo_master);
  //             $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));
  //           }

  //           $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
  //           $this->api_bus->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
  //           $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));

  //           $update_data_gonagoo_history = array(
  //             "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
  //             "order_id" => (int)$ticket_id,
  //             "user_id" => (int)trim($ticket_details['operator_id']),
  //             "type" => 1,
  //             "transaction_type" => 'ticket_commission',
  //             "amount" => trim($ownward_trip_commission),
  //             "datetime" => $today,
  //             "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
  //             "transfer_account_type" => trim($payment_method),
  //             "transfer_account_number" => trim($transfer_account_number),
  //             "bank_name" => trim($bank_name),
  //             "account_holder_name" => trim($account_holder_name),
  //             "iban" => trim($iban),
  //             "email_address" => trim($email_address),
  //             "mobile_number" => trim($mobile_number),
  //             "transaction_id" => trim($transaction_id),
  //             "currency_code" => trim($ticket_details['currency_sign']),
  //             "country_id" => trim($ownward_trip_operator_detail['country_id']),
  //             "cat_id" => trim($ticket_details['cat_id']),
  //           );
  //           $this->api_bus->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
  //         //Add Ownward Trip Commission To Gonagoo Account
          
  //         /* -------------------Ownward ticket commission invoice-------------------- */
  //           if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
  //             $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
  //             $username = $parts[0];
  //             $operator_name = $username;
  //           } else {
  //             $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
  //           }

  //           $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //           require_once($phpinvoice);
  //           //Language configuration for invoice
  //           $lang = $this->input->post('lang', TRUE);
  //           if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //           else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //           else { $invoice_lang = "englishApi_lang"; }
  //           //Invoice Configuration
  //           $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
  //           $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //           $invoice->setColor("#000");
  //           $invoice->setType("");
  //           $invoice->setReference($ticket_details['ticket_id']);
  //           $invoice->setDate(date('M dS ,Y',time()));

  //           $operator_country = $this->api_bus->get_country_details(trim($ownward_trip_operator_detail['country_id']));
  //           $operator_state = $this->api_bus->get_state_details(trim($ownward_trip_operator_detail['state_id']));
  //           $operator_city = $this->api_bus->get_city_details(trim($ownward_trip_operator_detail['city_id']));

  //           $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
  //           $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //           $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //           $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
            
  //           $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

  //           $eol = PHP_EOL;
  //           /* Adding Items in table */
  //           $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
  //           $rate = round(trim($ownward_trip_commission),2);
  //           $total = $rate;
  //           $payment_datetime = substr($today, 0, 10);
  //           //set items
  //           $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //           /* Add totals */
  //           $invoice->addTotal($this->lang->line('sub_total'),$total);
  //           $invoice->addTotal($this->lang->line('taxes'),'0');
  //           $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //           /* Set badge */ 
  //           $invoice->addBadge($this->lang->line('Commission Paid'));
  //           /* Add title */
  //           $invoice->addTitle($this->lang->line('tnc'));
  //           /* Add Paragraph */
  //           $invoice->addParagraph($gonagoo_address['terms']);
  //           /* Add title */
  //           $invoice->addTitle($this->lang->line('payment_dtls'));
  //           /* Add Paragraph */
  //           $invoice->addParagraph($gonagoo_address['payment_details']);
  //           /* Add title */
  //           $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //           /* Add Paragraph */
  //           $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //           /* Set footer note */
  //           $invoice->setFooternote($gonagoo_address['company_name']);
  //           /* Render */
  //           $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //           //Update File path
  //           $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
  //           //Move file to upload folder
  //           rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //           //Update Order Invoice
  //           $update_data = array(
  //             "commission_invoice_url" => "ticket-invoices/".$pdf_name,
  //           );
  //           $this->api_bus->update_booking_details($update_data, $ticket_details['ticket_id']);
  //         /* -------------------Ownward ticket commission invoice----------------- */
          
  //         /* -----------------------Email Invoice to operator-------------------- */
  //           $emailBody = $this->lang->line('Commission Invoice');
  //           $gonagooAddress = $gonagoo_address['company_name'];
  //           $emailSubject = $this->lang->line('Commission Invoice');
  //           $emailAddress = $ownward_trip_operator_detail['email1'];
  //           $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //           $fileName = $pdf_name;
  //           $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //         /* -----------------------Email Invoice to operator-------------------- */

  //         //***********************Return Trip Payment Calculation****************** 
  //           if($ticket_details['return_trip_id'] > 0) {
  //             $return_ticket_details = $this->api_bus->get_trip_booking_details($ticket_details['return_trip_id']);

  //             $country_code = $this->api_bus->get_country_code_by_id($user_details[0]['country_id']);
  //             $message = $this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id'];
  //             $this->api_bus->sendSMS((int)$country_code.trim($user_details[0]['mobile']), $message);
  //             $this->api_sms->send_sms_whatsapp((int)$country_code.trim($user_details[0]['mobile']), $message);

  //             $return_update_data = array(
  //               "payment_method" => trim($payment_method),
  //               "paid_amount" => trim($return_ticket_details['ticket_price']),
  //               "balance_amount" => 0,
  //               "transaction_id" => trim($transaction_id),
  //               "payment_datetime" => $today,
  //               "payment_mode" => "online",
  //               "payment_by" => "web",
  //               "complete_paid" => 1,
  //             );
  //             $this->api_bus->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);
  //             //-------------------Update to Workroom---------------------------------
  //               $workroom_update = array(
  //                 'order_id' => $return_ticket_details['ticket_id'],
  //                 'cust_id' => $return_ticket_details['cust_id'],
  //                 'deliverer_id' => $return_ticket_details['operator_id'],
  //                 'text_msg' => $message,
  //                 'attachment_url' =>  'NULL',
  //                 'cre_datetime' => $today,
  //                 'sender_id' => $return_ticket_details['operator_id'],
  //                 'type' => 'payment',
  //                 'file_type' => 'text',
  //                 'cust_name' => $return_ticket_details['cust_name'],
  //                 'deliverer_name' => $return_ticket_details['operator_name'],
  //                 'thumbnail' => 'NULL',
  //                 'ratings' => 'NULL',
  //                 'cat_id' => 281
  //               );
  //               $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //             //-------------------Update to Workroom---------------------------------

  //             //Add Return Trip Payment to trip operator account + History
  //               if($this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
  //                 $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //               } else {
  //                 $insert_data_customer_master = array(
  //                   "user_id" => trim($return_ticket_details['operator_id']),
  //                   "account_balance" => 0,
  //                   "update_datetime" => $today,
  //                   "operation_lock" => 1,
  //                   "currency_code" => trim($return_ticket_details['currency_sign']),
  //                 );
  //                 $this->api_bus->insert_gonagoo_customer_record($insert_data_customer_master);
  //                 $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //               }
  //               $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
  //               $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
  //               $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //               $update_data_account_history = array(
  //                 "account_id" => (int)$customer_account_master_details['account_id'],
  //                 "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //                 "user_id" => (int)trim($return_ticket_details['operator_id']),
  //                 "datetime" => $today,
  //                 "type" => 1,
  //                 "transaction_type" => 'add',
  //                 "amount" => trim($return_ticket_details['ticket_price']),
  //                 "account_balance" => trim($customer_account_master_details['account_balance']),
  //                 "withdraw_request_id" => 0,
  //                 "currency_code" => trim($return_ticket_details['currency_sign']),
  //                 "cat_id" => 281
  //               );
  //               $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //             //Add Return Trip Payment to trip operator account + History

  //             /* ----------------------Return ticket invoice----------------------- */
  //               $return_trip_operator_detail = $this->api_bus->get_user_details($return_ticket_details['operator_id']);

  //               $country_code = $this->api_bus->get_country_code_by_id($return_trip_operator_detail['country_id']);
  //               $message = $this->lang->line('Payment recieved against ticket ID - ').$return_ticket_details['ticket_id'];
  //               $this->api_bus->sendSMS((int)$country_code.trim($return_trip_operator_detail['mobile1']), $message);
                
  //               if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
  //                 $parts = explode("@", trim($return_trip_operator_detail['email1']));
  //                 $username = $parts[0];
  //                 $operator_name = $username;
  //               } else {
  //                 $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
  //               }

  //               $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //               require_once($phpinvoice);
  //               //Language configuration for invoice
  //               $lang = $this->input->post('lang', TRUE);
  //               if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //               else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //               else { $invoice_lang = "englishApi_lang"; }
  //               //Invoice Configuration
  //               $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
  //               $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //               $invoice->setColor("#000");
  //               $invoice->setType("");
  //               $invoice->setReference($return_ticket_details['ticket_id']);
  //               $invoice->setDate(date('M dS ,Y',time()));

  //               $operator_country = $this->api_bus->get_country_details(trim($return_trip_operator_detail['country_id']));
  //               $operator_state = $this->api_bus->get_state_details(trim($return_trip_operator_detail['state_id']));
  //               $operator_city = $this->api_bus->get_city_details(trim($return_trip_operator_detail['city_id']));

  //               $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
  //               $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //               $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //               $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

  //               $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']), $user_details[0]['email_id']));

  //               //$invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
  //               $eol = PHP_EOL;
  //               /* Adding Items in table */
  //               $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
  //               $rate = round(trim($return_ticket_details['ticket_price']),2);
  //               $total = $rate;
  //               $payment_datetime = substr($today, 0, 10);
  //               //set items
  //               $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //               /* Add totals */
  //               $invoice->addTotal($this->lang->line('sub_total'),$total);
  //               $invoice->addTotal($this->lang->line('taxes'),'0');
  //               $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //               /* Set badge */ 
  //               $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
  //               /* Add title */
  //               $invoice->addTitle($this->lang->line('tnc'));
  //               /* Add Paragraph */
  //               $invoice->addParagraph($gonagoo_address['terms']);
  //               /* Add title */
  //               $invoice->addTitle($this->lang->line('payment_dtls'));
  //               /* Add Paragraph */
  //               $invoice->addParagraph($gonagoo_address['payment_details']);
  //               /* Add title */
  //               $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //               /* Add Paragraph */
  //               $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //               /* Set footer note */
  //               $invoice->setFooternote($gonagoo_address['company_name']);
  //               /* Render */
  //               $invoice->render($return_ticket_details['ticket_id'].'_return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //               //Update File path
  //               $pdf_name = $return_ticket_details['ticket_id'].'_return_trip.pdf';
  //               //Move file to upload folder
  //               rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //               //Update Order Invoice
  //               $update_data = array(
  //                 "invoice_url" => "ticket-invoices/".$pdf_name,
  //               );
  //               $this->api_bus->update_booking_details($update_data, $return_ticket_details['ticket_id']);
  //               $workroom_invoice_url = "ticket-invoices/".$pdf_name;
  //               //Payment Invoice Update to workroom
  //               $workroom_update = array(
  //                 'order_id' => $return_ticket_details['ticket_id'],
  //                 'cust_id' => $return_ticket_details['cust_id'],
  //                 'deliverer_id' => $return_ticket_details['operator_id'],
  //                 'text_msg' => $this->lang->line('Invoice'),
  //                 'attachment_url' =>  $workroom_invoice_url,
  //                 'cre_datetime' => $today,
  //                 'sender_id' => $return_ticket_details['operator_id'],
  //                 'type' => 'raise_invoice',
  //                 'file_type' => 'pdf',
  //                 'cust_name' => $return_ticket_details['cust_name'],
  //                 'deliverer_name' => $return_ticket_details['operator_name'],
  //                 'thumbnail' => 'NULL',
  //                 'ratings' => 'NULL',
  //                 'cat_id' => 281
  //               );
  //               $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //             /* ----------------------Return ticket invoice----------------------- */

  //             /* -----------------Email Invoice to customer-------------------------- */
  //               $emailBody = $this->lang->line('Please download your ticket payment invoice.');
  //               $gonagooAddress = $gonagoo_address['company_name'];
  //               $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
  //               $emailAddress = $user_details[0]['email_id'];
  //               $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //               $fileName = $pdf_name;
  //               $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //             /* -----------------------Email Invoice to customer-------------------- */
              
  //             /* -------------------------Return Ticket pdf ------------------------- */
  //               $return_operator_details = $this->api_bus->get_bus_operator_profile($return_ticket_details['operator_id']);
  //               $cust_sms = $this->lang->line("Dear")."! ".$user_detailss[0]['firstname']." ".$user_detailss[0]['lastname']." ".$this->lang->line('Your')." ".$this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id']." , ".$this->lang->line('For')." ".$return_ticket_details['source_point']." ".$this->lang->line('To')." ".$return_ticket_details['destination_point'];
  //               if($return_operator_details["avatar_url"]==" " || $return_operator_details["avatar_url"] == "NULL"){
  //                 $operator_avtr = base_url("resources/no-image.jpg");
  //               }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
  //               require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
  //                 QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
  //                  $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
  //                  $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
  //                 if(rename($img_src, $img_dest));
  //               $html ='
  //               <page format="100x100" orientation="L" style="font: arial;">';
  //               $html .= '
  //               <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
  //                 <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
  //                   <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
  //                   <div style="margin-top:27px">
  //                     <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
  //                     <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
  //                   </div>
  //                 </div>
  //                 <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
  //                     <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
  //                     <br />
  //                     <h6>'.$this->lang->line("slider_heading1").'</h6>
  //                 </div>                
  //               </div>
  //               <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
  //                 <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
  //                   '.$this->lang->line("Ownward_Trip").'<br/>
  //                 <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
  //                 <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
  //                 <h6 style="margin-top:-10px;">
  //                 <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
  //                 <h6 style="margin-top:-8px;">
  //                 <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
  //                 <h6 style="margin-top:-8px;">
  //                 <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
  //                 <h6 style="margin-top:-8px;"> 
  //                 <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
  //                 <h6 style="margin-top:-8px;">
  //                 <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
  //                 $ticket_seat_details = $this->api_bus->get_ticket_seat_details($return_ticket_details['ticket_id']);
    
  //                 $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
  //                 foreach ($ticket_seat_details as $seat)
  //                 {
  //                   $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
  //                   if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                      
  //                    $html .='</td><td>'.$this->api_bus->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
  //                 }
  //               $html .='</tbody></table></div></page>';            
  //               $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
  //               require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
  //               $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
  //               $html2pdf->pdf->SetDisplayMode('default');
  //               $html2pdf->writeHTML($html);
  //               $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
  //               $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
  //               rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
  //               //whatsapp api send ticket
  //               $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
  //               $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
  //               $this->api_sms->send_sms_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$cust_sms);
  //               //echo json_encode($result); die();
  //             /* -------------------------Return Ticket pdf ------------------------- */

  //             /* ---------------------Email Ticket to customer----------------------- */
  //               $emailBody = $this->lang->line('Please download your e-ticket.');
  //               $gonagooAddress = $gonagoo_address['company_name'];
  //               $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
  //               $emailAddress = $user_details[0]['email_id'];
  //               $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
  //               $fileName = $my_ticket_pdf;
  //               $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //             /* ---------------------Email Ticket to customer----------------------- */

  //             //Deduct Return Trip Commission From Operator Account
  //               $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

  //               $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
  //               $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
  //               $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //               $update_data_account_history = array(
  //                 "account_id" => (int)$customer_account_master_details['account_id'],
  //                 "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //                 "user_id" => (int)trim($return_ticket_details['operator_id']),
  //                 "datetime" => $today,
  //                 "type" => 0,
  //                 "transaction_type" => 'ticket_commission',
  //                 "amount" => trim($return_trip_commission),
  //                 "account_balance" => trim($customer_account_master_details['account_balance']),
  //                 "withdraw_request_id" => 0,
  //                 "currency_code" => trim($return_ticket_details['currency_sign']),
  //                 "cat_id" => 281
  //               );
  //               $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //             //Deduct Return Trip Commission From Operator Account

  //             //Add Return Trip Commission To Gonagoo Account
  //               if($this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
  //                 $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
  //               } else {
  //                 $insert_data_gonagoo_master = array(
  //                   "gonagoo_balance" => 0,
  //                   "update_datetime" => $today,
  //                   "operation_lock" => 1,
  //                   "currency_code" => trim($return_ticket_details['currency_sign']),
  //                 );
  //                 $this->api_bus->insert_gonagoo_master_record($insert_data_gonagoo_master);
  //                 $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
  //               }

  //               $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
  //               $this->api_bus->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
  //               $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

  //               $update_data_gonagoo_history = array(
  //                 "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
  //                 "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //                 "user_id" => (int)trim($return_ticket_details['operator_id']),
  //                 "type" => 1,
  //                 "transaction_type" => 'ticket_commission',
  //                 "amount" => trim($return_trip_commission),
  //                 "datetime" => $today,
  //                 "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
  //                 "transfer_account_type" => trim($payment_method),
  //                 "transfer_account_number" => trim($transfer_account_number),
  //                 "bank_name" => trim($bank_name),
  //                 "account_holder_name" => trim($account_holder_name),
  //                 "iban" => trim($iban),
  //                 "email_address" => trim($email_address),
  //                 "mobile_number" => trim($mobile_number),
  //                 "transaction_id" => trim($transaction_id),
  //                 "currency_code" => trim($return_ticket_details['currency_sign']),
  //                 "country_id" => trim($return_trip_operator_detail['country_id']),
  //                 "cat_id" => trim($return_ticket_details['cat_id']),
  //               );
  //               $this->api_bus->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
  //             //Add Return Trip Commission To Gonagoo Account

  //             /* ------------------Return ticket commission invoice------------------- */
  //               if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
  //                 $parts = explode("@", trim($return_trip_operator_detail['email1']));
  //                 $username = $parts[0];
  //                 $operator_name = $username;
  //               } else {
  //                 $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
  //               }

  //               $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //               require_once($phpinvoice);
  //               //Language configuration for invoice
  //               $lang = $this->input->post('lang', TRUE);
  //               if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //               else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //               else { $invoice_lang = "englishApi_lang"; }
  //               //Invoice Configuration
  //               $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
  //               $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //               $invoice->setColor("#000");
  //               $invoice->setType("");
  //               $invoice->setReference($return_ticket_details['ticket_id']);
  //               $invoice->setDate(date('M dS ,Y',time()));

  //               $operator_country = $this->api_bus->get_country_details(trim($return_trip_operator_detail['country_id']));
  //               $operator_state = $this->api_bus->get_state_details(trim($return_trip_operator_detail['state_id']));
  //               $operator_city = $this->api_bus->get_city_details(trim($return_trip_operator_detail['city_id']));

  //               $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
  //               $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //               $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //               $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
                
  //               $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

  //               $eol = PHP_EOL;
  //               /* Adding Items in table */
  //               $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
  //               $rate = round(trim($return_trip_commission),2);
  //               $total = $rate;
  //               $payment_datetime = substr($today, 0, 10);
  //               //set items
  //               $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //               /* Add totals */
  //               $invoice->addTotal($this->lang->line('sub_total'),$total);
  //               $invoice->addTotal($this->lang->line('taxes'),'0');
  //               $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //               /* Set badge */ 
  //               $invoice->addBadge($this->lang->line('Commission Paid'));
  //               /* Add title */
  //               $invoice->addTitle($this->lang->line('tnc'));
  //               /* Add Paragraph */
  //               $invoice->addParagraph($gonagoo_address['terms']);
  //               /* Add title */
  //               $invoice->addTitle($this->lang->line('payment_dtls'));
  //               /* Add Paragraph */
  //               $invoice->addParagraph($gonagoo_address['payment_details']);
  //               /* Add title */
  //               $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //               /* Add Paragraph */
  //               $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //               /* Set footer note */
  //               $invoice->setFooternote($gonagoo_address['company_name']);
  //               /* Render */
  //               $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //               //Update File path
  //               $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
  //               //Move file to upload folder
  //               rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //               //Update Order Invoice
  //               $update_data = array(
  //                 "commission_invoice_url" => "ticket-invoices/".$pdf_name,
  //               );
  //               $this->api_bus->update_booking_details($update_data, $return_ticket_details['ticket_id']);
  //             /* ------------------Return ticket commission invoice------------------- */
  //             /* -----------------------Email Invoice to operator-------------------- */
  //               $emailBody = $this->lang->line('Commission Invoice');
  //               $gonagooAddress = $gonagoo_address['company_name'];
  //               $emailSubject = $this->lang->line('Commission Invoice');
  //               $emailAddress = $ownward_trip_operator_detail['email1'];
  //               $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //               $fileName = $pdf_name;
  //               $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //             /* -----------------------Email Invoice to operator-------------------- */
  //           }
  //         //***********************Return Trip Payment Calculation******************
  //       /************************ Payment Section **********************************/
  //       $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed. Your e-ticket is sent to your email.'));
  //     } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
  //   } else {  $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
  //   redirect(base_url('ticket-payment-confirmation').'/'.$ticket_id,'refresh');
  // }
  // public function ticket_payment_by_stripe()
  // {
  //   //echo json_encode($_POST); die();
  //   $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;
  //   $user_details = $this->api_bus->get_user_details($cust_id);
  //   $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
  //   $payment_method = $this->input->post('payment_method', TRUE);
  //   $transaction_id = $this->input->post('stripeToken', TRUE);
  //   $ticket_price = $this->input->post('ticket_price', TRUE);
  //   $currency_sign = $this->input->post('currency_sign', TRUE);
  //   $today = date('Y-m-d h:i:s');
  //   $ticket_details = $this->api_bus->get_trip_booking_details($ticket_id);
  //   $seat_details = $this->api_bus->get_ticket_seat_details($ticket_id);
  //   //echo json_encode($seat_details); die();

  //   $location_details = $this->api_bus->get_bus_location_details($ticket_details['source_point_id']);
  //   $country_id = $this->api_bus->get_country_id_by_name($location_details['country_name']);
  //   //echo json_encode($country_id); die();

  //   $gonagoo_commission_percentage = $this->api_bus->get_gonagoo_bus_commission_details((int)$country_id);
  //   //echo json_encode($gonagoo_commission_percentage); die();
  //   $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

  //   //echo json_encode($ownward_trip_commission); die();
    
  //   $total_amount_paid = $ticket_details['ticket_price'];
  //   $transfer_account_number = "NULL";
  //   $bank_name = "NULL";
  //   $account_holder_name = "NULL";
  //   $iban = "NULL";
  //   $email_address = $this->input->post('stripeEmail', TRUE);
  //   $mobile_number = "NULL";    

  //   $country_code = $this->api_bus->get_country_code_by_id($ticket_details['country_id']);
  //   $message = $this->lang->line('Payment completed for ticket ID - ').$ticket_id;
  //   $this->api_bus->sendSMS((int)$country_code.trim($ticket_details['cust_contact']), $message);
  //   $this->api_sms->send_sms_whatsapp((int)$country_code.trim($ticket_details['cust_contact']), $message);

  //   //Send Push Notifications
  //   //Get PN API Keys
  //   $api_bus_key = $this->config->item('delivererAppGoogleKey');
  //   $device_details_customer = $this->api_bus->get_user_device_details($cust_id);
  //   if(!is_null($device_details_customer)) {
  //     $arr_customer_fcm_ids = array();
  //     $arr_customer_apn_ids = array();
  //     foreach ($device_details_customer as $value) {
  //       if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
  //       else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
  //     }
  //     $msg = array('title' => $this->lang->line('Ticket payment confirmation'), 'type' => 'ticket-payment-confirmation', 'notice_date' => $today, 'desc' => $message);
  //     $this->api_bus->sendFCM($msg, $arr_customer_fcm_ids, $api_bus_key);

  //     //Push Notification APN
  //     $msg_apn_customer =  array('title' => $this->lang->line('Ticket payment confirmation'), 'text' => $message);
  //     if(is_array($arr_customer_apn_ids)) { 
  //       $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
  //       $this->api_bus->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_bus_key);
  //     }
  //   }

  //   $update_data = array(
  //     "payment_method" => trim($payment_method),
  //     "paid_amount" => trim($total_amount_paid),
  //     "balance_amount" => 0,
  //     "transaction_id" => trim($transaction_id),
  //     "payment_datetime" => $today,
  //     "payment_mode" => "online",
  //     "payment_by" => "web",
  //     "complete_paid" => 1,
  //   );
  //   //echo json_encode($update_data); die();

  //   if($this->api_bus->update_booking_details($update_data, $ticket_id)) {
  //     //Update to workroom
  //     $workroom_update = array(
  //       'order_id' => $ticket_id,
  //       'cust_id' => $ticket_details['cust_id'],
  //       'deliverer_id' => $ticket_details['operator_id'],
  //       'text_msg' => $message,
  //       'attachment_url' =>  'NULL',
  //       'cre_datetime' => $today,
  //       'sender_id' => $ticket_details['operator_id'],
  //       'type' => 'payment',
  //       'file_type' => 'text',
  //       'cust_name' => $ticket_details['cust_name'],
  //       'deliverer_name' => $ticket_details['operator_name'],
  //       'thumbnail' => 'NULL',
  //       'ratings' => 'NULL',
  //       'cat_id' => 281
  //     );
  //     $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //     /********************* Payment Section ************************************/

  //       //---------------------Insert counter payment record----------------------
  //         $data_counter_sale_payment = array(
  //           "cat_id" => $ticket_details['cat_id'],
  //           "service_id" => $ticket_details['ticket_id'],
  //           "cust_id" => $ticket_details['operator_id'],
  //           "cre_datetime" => $today,
  //           "type" => 1,
  //           "transaction_type" => 'payment',
  //           "amount" => trim($ticket_details['ticket_price']),
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
  //           "cat_name" => 'ticket_booking',
  //           "customer_email" => trim($seat_details[0]['email_id']),
  //           "customer_mobile" => trim($seat_details[0]['mobile']),
  //           "customer_country_id" => trim($seat_details[0]['country_id']),
  //         );
  //         $this->api_bus->insert_counter_sale_payment($data_counter_sale_payment);
  //       //---------------------Insert counter payment record----------------------


  //       //Add Ownward Trip Payment to trip operator account + History
  //         if($this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
  //           $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //           //echo json_encode($customer_account_master_details); die();
  //         } else {
  //           $insert_data_customer_master = array(
  //             "user_id" => trim($ticket_details['operator_id']),
  //             "account_balance" => 0,
  //             "update_datetime" => $today,
  //             "operation_lock" => 1,
  //             "currency_code" => trim($ticket_details['currency_sign']),
  //           );
  //           $this->api_bus->insert_gonagoo_customer_record($insert_data_customer_master);
  //           $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //         }//else

  //         $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
  //         //echo json_encode($account_balance); die();

  //         $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
  //         $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
          
  //         //echo json_encode($customer_account_master_details); die();
  //         $update_data_account_history = array(
  //           "account_id" => (int)$customer_account_master_details['account_id'],
  //           "order_id" => (int)$ticket_id,
  //           "user_id" => (int)trim($ticket_details['operator_id']),
  //           "datetime" => $today,
  //           "type" => 1,
  //           "transaction_type" => 'add',
  //           "amount" => trim($ticket_details['ticket_price']),
  //           "account_balance" => trim($customer_account_master_details['account_balance']),
  //           "withdraw_request_id" => 0,
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "cat_id" => 281
  //         );
  //         $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //       //Add Ownward Trip Payment to trip operator account + History

  //       /* COD ticket invoice---------------------------------------------- */
  //         $ownward_trip_operator_detail = $this->api_bus->get_user_details($ticket_details['operator_id']);
  //         if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
  //           $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
  //           $username = $parts[0];
  //           $operator_name = $username;
  //         } else {
  //           $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
  //         }

  //         $user_details = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']);
  //         if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
  //           $parts = explode("@", trim($user_details[0]['email_id']));
  //           $username = $parts[0];
  //           $customer_name = $username;
  //         } else {
  //           $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
  //         }

  //         $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //         require_once($phpinvoice);
  //         //Language configuration for invoice
  //         $lang = $this->input->post('lang', TRUE);
  //         if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //         else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //         else { $invoice_lang = "englishApi_lang"; }
  //         //Invoice Configuration
  //         $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
  //         $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //         $invoice->setColor("#000");
  //         $invoice->setType("");
  //         $invoice->setReference($ticket_details['ticket_id']);
  //         $invoice->setDate(date('M dS ,Y',time()));

  //         $operator_country = $this->api_bus->get_country_details(trim($ownward_trip_operator_detail['country_id']));
  //         $operator_state = $this->api_bus->get_state_details(trim($ownward_trip_operator_detail['state_id']));
  //         $operator_city = $this->api_bus->get_city_details(trim($ownward_trip_operator_detail['city_id']));

  //         $user_country = $this->api_bus->get_country_details(trim($user_details[0]['country_id']));
  //         // $user_state = $this->api_bus->get_state_details(trim($user_details['state_id']));
  //         // $user_city = $this->api_bus->get_city_details(trim($user_details['city_id']));
  //         $user_state = "---";
  //         $user_city = "---";

  //         $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
  //         $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //         $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //         $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

  //         $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($user_details[0]['email_id'])));

  //         $eol = PHP_EOL;
  //         /* Adding Items in table */
  //         $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
  //         $rate = round(trim($ticket_details['ticket_price']),2);
  //         $total = $rate;
  //         $payment_datetime = substr($today, 0, 10);
  //         //set items
  //         $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //         /* Add totals */
  //         $invoice->addTotal($this->lang->line('sub_total'),$total);
  //         $invoice->addTotal($this->lang->line('taxes'),'0');
  //         $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //         /* Set badge */ 
  //         $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
  //         /* Add title */
  //         $invoice->addTitle($this->lang->line('tnc'));
  //         /* Add Paragraph */
  //         $invoice->addParagraph($gonagoo_address['terms']);
  //         /* Add title */
  //         $invoice->addTitle($this->lang->line('payment_dtls'));
  //         /* Add Paragraph* /
  //         $invoice->addParagraph($gonagoo_address['payment_details']);
  //         /* Add title */
  //         $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //         /* Add Paragraph */
  //         $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //         /* Set footer note */
  //         $invoice->setFooternote($gonagoo_address['company_name']);
  //         /* Render */
  //         $invoice->render($ticket_details['ticket_id'].'_Ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //         //Update File path
  //         $pdf_name = $ticket_details['ticket_id'].'_Ownward_trip.pdf';
  //         //Move file to upload folder
  //         rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //         //Update Order Invoice
  //         $update_data = array(
  //           "invoice_url" => "ticket-invoices/".$pdf_name,
  //         );
  //         $this->api_bus->update_booking_details($update_data, $ticket_details['ticket_id']);
  //         //Update to workroom
  //         $workroom_update = array(
  //           'order_id' => $ticket_details['ticket_id'],
  //           'cust_id' => $ticket_details['cust_id'],
  //           'deliverer_id' => $ticket_details['operator_id'],
  //           'text_msg' => $this->lang->line('Invoice'),
  //           'attachment_url' =>  "ticket-invoices/".$pdf_name,
  //           'cre_datetime' => $today,
  //           'sender_id' => $ticket_details['operator_id'],
  //           'type' => 'raise_invoice',
  //           'file_type' => 'pdf',
  //           'cust_name' => $ticket_details['cust_name'],
  //           'deliverer_name' => $ticket_details['operator_name'],
  //           'thumbnail' => 'NULL',
  //           'ratings' => 'NULL',
  //           'cat_id' => 281
  //         );
  //         $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //       /* -------------------------------------------------------------------- */
        
  //       /* -----------------Email Invoice to customer-------------------------- */
  //         $emailBody = $this->lang->line('Please download your ticket payment invoice.');
  //         $gonagooAddress = $gonagoo_address['company_name'];
  //         $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
  //         $emailAddress = $user_details[0]['email_id'];
  //         $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //         $fileName = $pdf_name;
  //         $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //       /* -----------------------Email Invoice to customer-------------------- */
        
  //       /* ------------------------Generate Ticket pdf ------------------------ */
  //         $user_detailss = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']);
  //           $cust_sms = $this->lang->line("Dear")."! ".$user_detailss[0]['firstname']." ".$user_detailss[0]['lastname']." ".$this->lang->line('Your')." ".$this->lang->line('Payment completed for ticket ID - ').$ticket_details['ticket_id']." , ".$this->lang->line('For')." ".$ticket_details['source_point']." ".$this->lang->line('To')." ".$ticket_details['destination_point'];
  //           $operator_details = $this->api_bus->get_bus_operator_profile($ticket_details['operator_id']);
  //           if($operator_details["avatar_url"]==" " || $operator_details["avatar_url"] == "NULL"){
  //             $operator_avtr = base_url("resources/no-image.jpg");
  //           }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
  //           require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
  //             QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
  //              $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
  //              $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
  //             if(rename($img_src, $img_dest));
  //           $html ='
  //           <page format="100x100" orientation="L" style="font: arial;">';
  //           $html .= '
  //           <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
  //             <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
  //               <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
  //               <div style="margin-top:27px">
  //                 <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
  //                 <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
  //               </div>
  //             </div>
  //             <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
  //                 <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
  //                 <br />
  //                 <h6>'.$this->lang->line("slider_heading1").'</h6>
  //             </div>                
  //           </div>
  //           <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
  //             <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
  //               '.$this->lang->line("Ownward_Trip").'<br/>
  //             <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
  //             <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
  //             <h6 style="margin-top:-10px;">
  //             <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
  //             <h6 style="margin-top:-8px;">
  //             <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
  //             <h6 style="margin-top:-8px;">
  //             <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
  //             <h6 style="margin-top:-8px;"> 
  //             <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
  //             <h6 style="margin-top:-8px;">
  //             <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
  //             $ticket_seat_details = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']);
    
  //             $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
  //             foreach ($ticket_seat_details as $seat)
  //             {
  //               $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
  //               if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                  
  //                $html .='</td><td>'.$this->api_bus->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
  //             }
  //           $html .='</tbody></table></div></page>';            
  //           $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
  //           require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
  //           $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
  //           $html2pdf->pdf->SetDisplayMode('default');
  //           $html2pdf->writeHTML($html);
  //           $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
  //           $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
  //           rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
  //           //whatsapp api send ticket
  //           $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
  //           $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
  //           $this->api_sms->send_sms_whatsapp($country_code.trim($user_detailss[0]['mobile']), $cust_sms);
  //           //echo json_encode($result); die();
  //       /* ------------------------Generate Ticket pdf ------------------------ */

  //       /* ---------------------Email Ticket to customer----------------------- */
  //         $emailBody = $this->lang->line('Please download your e-ticket.');
  //         $gonagooAddress = $gonagoo_address['company_name'];
  //         $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
  //         $emailAddress = $user_details[0]['email_id'];
  //         $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
  //         $fileName = $my_ticket_pdf;
  //         $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //       /* ---------------------Email Ticket to customer----------------------- */

  //       //Deduct Ownward Trip Commission From Operator Account
  //         $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
  //         $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
  //         $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //         $update_data_account_history = array(
  //           "account_id" => (int)$customer_account_master_details['account_id'],
  //           "order_id" => (int)$ticket_id,
  //           "user_id" => (int)trim($ticket_details['operator_id']),
  //           "datetime" => $today,
  //           "type" => 0,
  //           "transaction_type" => 'ticket_commission',
  //           "amount" => trim($ownward_trip_commission),
  //           "account_balance" => trim($customer_account_master_details['account_balance']),
  //           "withdraw_request_id" => 0,
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "cat_id" => 281
  //         );
  //         $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //       //Deduct Ownward Trip Commission From Operator Account
        
  //       //Add Ownward Trip Commission To Gonagoo Account
  //         if($this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
  //           $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));
  //         } else {
  //           $insert_data_gonagoo_master = array(
  //             "gonagoo_balance" => 0,
  //             "update_datetime" => $today,
  //             "operation_lock" => 1,
  //             "currency_code" => trim($ticket_details['currency_sign']),
  //           );
  //           $this->api_bus->insert_gonagoo_master_record($insert_data_gonagoo_master);
  //           $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));
  //         }

  //         $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
  //         $this->api_bus->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
  //         $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));

  //         $update_data_gonagoo_history = array(
  //           "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
  //           "order_id" => (int)$ticket_id,
  //           "user_id" => (int)trim($ticket_details['operator_id']),
  //           "type" => 1,
  //           "transaction_type" => 'ticket_commission',
  //           "amount" => trim($ownward_trip_commission),
  //           "datetime" => $today,
  //           "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
  //           "transfer_account_type" => trim($payment_method),
  //           "transfer_account_number" => trim($transfer_account_number),
  //           "bank_name" => trim($bank_name),
  //           "account_holder_name" => trim($account_holder_name),
  //           "iban" => trim($iban),
  //           "email_address" => trim($email_address),
  //           "mobile_number" => trim($mobile_number),
  //           "transaction_id" => trim($transaction_id),
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "country_id" => trim($ownward_trip_operator_detail['country_id']),
  //           "cat_id" => trim($ticket_details['cat_id']),
  //         );
  //         $this->api_bus->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
  //       //Add Ownward Trip Commission To Gonagoo Account
        
  //       /* Ownward ticket commission invoice----------------------------------- */
  //         if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
  //           $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
  //           $username = $parts[0];
  //           $operator_name = $username;
  //         } else {
  //           $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
  //         }

  //         $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //         require_once($phpinvoice);
  //         //Language configuration for invoice
  //         $lang = $this->input->post('lang', TRUE);
  //         if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //         else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //         else { $invoice_lang = "englishApi_lang"; }
  //         //Invoice Configuration
  //         $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
  //         $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //         $invoice->setColor("#000");
  //         $invoice->setType("");
  //         $invoice->setReference($ticket_details['ticket_id']);
  //         $invoice->setDate(date('M dS ,Y',time()));

  //         $operator_country = $this->api_bus->get_country_details(trim($ownward_trip_operator_detail['country_id']));
  //         $operator_state = $this->api_bus->get_state_details(trim($ownward_trip_operator_detail['state_id']));
  //         $operator_city = $this->api_bus->get_city_details(trim($ownward_trip_operator_detail['city_id']));

  //         $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
  //         $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //         $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //         $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
          
  //         $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

  //         $eol = PHP_EOL;
  //         /* Adding Items in table */
  //         $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
  //         $rate = round(trim($ownward_trip_commission),2);
  //         $total = $rate;
  //         $payment_datetime = substr($today, 0, 10);
  //         //set items
  //         $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //         /* Add totals */
  //         $invoice->addTotal($this->lang->line('sub_total'),$total);
  //         $invoice->addTotal($this->lang->line('taxes'),'0');
  //         $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //         /* Set badge */ 
  //         $invoice->addBadge($this->lang->line('Commission Paid'));
  //         /* Add title */
  //         $invoice->addTitle($this->lang->line('tnc'));
  //         /* Add Paragraph */
  //         $invoice->addParagraph($gonagoo_address['terms']);
  //         /* Add title */
  //         $invoice->addTitle($this->lang->line('payment_dtls'));
  //         /* Add Paragraph */
  //         $invoice->addParagraph($gonagoo_address['payment_details']);
  //         /* Add title */
  //         $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //         /* Add Paragraph */
  //         $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //         /* Set footer note */
  //         $invoice->setFooternote($gonagoo_address['company_name']);
  //         /* Render */
  //         $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //         //Update File path
  //         $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
  //         //Move file to upload folder
  //         rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //         //Update Order Invoice
  //         $update_data = array(
  //           "commission_invoice_url" => "ticket-invoices/".$pdf_name,
  //         );
  //         $this->api_bus->update_booking_details($update_data, $ticket_details['ticket_id']);
  //       /* -------------------------------------------------------------------- */
  //       /* -----------------------Email Invoice to operator-------------------- */
  //         $emailBody = $this->lang->line('Commission Invoice');
  //         $gonagooAddress = $gonagoo_address['company_name'];
  //         $emailSubject = $this->lang->line('Commission Invoice');
  //         $emailAddress = $ownward_trip_operator_detail['email1'];
  //         $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //         $fileName = $pdf_name;
  //         $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //       /* -----------------------Email Invoice to operator-------------------- */

  //       //----------------------Return Trip Payment Calculation-----------------
  //         if($ticket_details['return_trip_id'] > 0) {
  //           $return_ticket_details = $this->api_bus->get_trip_booking_details($ticket_details['return_trip_id']);
  //           $return_trip_operator_detail = $this->api_bus->get_user_details($return_ticket_details['operator_id']);

  //           $country_code = $this->api_bus->get_country_code_by_id($return_ticket_details['country_id']);
  //           $message = $this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id'];
  //           $this->api_bus->sendSMS((int)$country_code.trim($return_ticket_details['cust_contact']), $message);
  //           $this->api_sms->send_sms_whatsapp((int)$country_code.trim($return_ticket_details['cust_contact']), $message);

  //           $return_update_data = array(
  //             "payment_method" => trim($payment_method),
  //             "paid_amount" => trim($return_ticket_details['ticket_price']),
  //             "balance_amount" => 0,
  //             "transaction_id" => trim($transaction_id),
  //             "payment_datetime" => $today,
  //             "payment_mode" => "online",
  //             "payment_by" => "web",
  //             "complete_paid" => 1,
  //           );
  //           $this->api_bus->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);


  //           //--------------Insert counter payment record----------------
  //             $data_counter_sale_payment = array(
  //               "cat_id" => $return_ticket_details['cat_id'],
  //               "service_id" => $return_ticket_details['ticket_id'],
  //               "cust_id" => $return_ticket_details['operator_id'],
  //               "cre_datetime" => $today,
  //               "type" => 1,
  //               "transaction_type" => 'payment',
  //               "amount" => trim($return_ticket_details['ticket_price']),
  //               "currency_code" => trim($return_ticket_details['currency_sign']),
  //               "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
  //               "cat_name" => 'ticket_booking',
  //               "customer_email" => trim($seat_details[0]['email_id']),
  //               "customer_mobile" => trim($seat_details[0]['mobile']),
  //               "customer_country_id" => trim($seat_details[0]['country_id']),
  //             );
  //             $this->api_bus->insert_counter_sale_payment($data_counter_sale_payment);
  //           //--------------Insert counter payment record-----------------

  //           //-----------------Update to workroom--------------------------
  //             $workroom_update = array(
  //               'order_id' => $return_ticket_details['ticket_id'],
  //               'cust_id' => $return_ticket_details['cust_id'],
  //               'deliverer_id' => $return_ticket_details['operator_id'],
  //               'text_msg' => $message,
  //               'attachment_url' =>  "NULL",
  //               'cre_datetime' => $today,
  //               'sender_id' => $return_ticket_details['operator_id'],
  //               'type' => 'payment',
  //               'file_type' => 'text',
  //               'cust_name' => $return_ticket_details['cust_name'],
  //               'deliverer_name' => $return_ticket_details['operator_name'],
  //               'thumbnail' => 'NULL',
  //               'ratings' => 'NULL',
  //               'cat_id' => 281
  //             );
  //             $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //           //-------------------Update to workroom------------------------

  //           //------Add Return Trip Payment to trip operator account + History---
  //             if($this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
  //               $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             } else {
  //               $insert_data_customer_master = array(
  //                 "user_id" => trim($return_ticket_details['operator_id']),
  //                 "account_balance" => 0,
  //                 "update_datetime" => $today,
  //                 "operation_lock" => 1,
  //                 "currency_code" => trim($return_ticket_details['currency_sign']),
  //               );
  //               $this->api_bus->insert_gonagoo_customer_record($insert_data_customer_master);
  //               $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             }
  //             $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
  //             $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
  //             $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             $update_data_account_history = array(
  //               "account_id" => (int)$customer_account_master_details['account_id'],
  //               "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //               "user_id" => (int)trim($return_ticket_details['operator_id']),
  //               "datetime" => $today,
  //               "type" => 1,
  //               "transaction_type" => 'add',
  //               "amount" => trim($return_ticket_details['ticket_price']),
  //               "account_balance" => trim($customer_account_master_details['account_balance']),
  //               "withdraw_request_id" => 0,
  //               "currency_code" => trim($return_ticket_details['currency_sign']),
  //               "cat_id" => 281
  //             );
  //             $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //           //------Add Return Trip Payment to trip operator account + History---

  //           /* Return ticket invoice---------------------------------------------- */
  //             if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
  //               $parts = explode("@", trim($return_trip_operator_detail['email1']));
  //               $username = $parts[0];
  //               $operator_name = $username;
  //             } else {
  //               $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
  //             }

  //             $user_details = $this->api_bus->get_ticket_seat_details($return_ticket_details['ticket_id']);

  //             if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
  //               $parts = explode("@", trim($user_details[0]['email_id']));
  //               $username = $parts[0];
  //               $customer_name = $username;
  //             } else {
  //               $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
  //             }

  //             $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //             require_once($phpinvoice);
  //             //Language configuration for invoice
  //             $lang = $this->input->post('lang', TRUE);
  //             if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //             else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //             else { $invoice_lang = "englishApi_lang"; }
  //             //Invoice Configuration
  //             $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
  //             $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //             $invoice->setColor("#000");
  //             $invoice->setType("");
  //             $invoice->setReference($return_ticket_details['ticket_id']);
  //             $invoice->setDate(date('M dS ,Y',time()));

  //             $operator_country = $this->api_bus->get_country_details(trim($return_trip_operator_detail['country_id']));
  //             $operator_state = $this->api_bus->get_state_details(trim($return_trip_operator_detail['state_id']));
  //             $operator_city = $this->api_bus->get_city_details(trim($return_trip_operator_detail['city_id']));

  //             $user_country = $this->api_bus->get_country_details(trim($user_details[0]['country_id']));
  //             // $user_state = $this->api_bus->get_state_details(trim($user_details['state_id']));
  //             // $user_city = $this->api_bus->get_city_details(trim($user_details['city_id']));
  //             $user_state = "---";
  //             $user_city = "---";

  //             $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
  //             $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //             $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //             $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

  //             $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($user_details[0]['email_id'])));

  //             $eol = PHP_EOL;
  //             /* Adding Items in table */
  //             $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
  //             $rate = round(trim($return_ticket_details['ticket_price']),2);
  //             $total = $rate;
  //             $payment_datetime = substr($today, 0, 10);
  //             //set items
  //             $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //             /* Add totals */
  //             $invoice->addTotal($this->lang->line('sub_total'),$total);
  //             $invoice->addTotal($this->lang->line('taxes'),'0');
  //             $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //             /* Set badge */ 
  //             $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
  //             /* Add title */
  //             $invoice->addTitle($this->lang->line('tnc'));
  //             /* Add Paragraph */
  //             $invoice->addParagraph($gonagoo_address['terms']);
  //             /* Add title */
  //             $invoice->addTitle($this->lang->line('payment_dtls'));
  //             /* Add Paragraph* /
  //             $invoice->addParagraph($gonagoo_address['payment_details']);
  //             /* Add title */
  //             $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //             /* Add Paragraph */
  //             $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //             /* Set footer note */
  //             $invoice->setFooternote($gonagoo_address['company_name']);
  //             /* Render */
  //             $invoice->render($return_ticket_details['ticket_id'].'_Return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //             //Update File path
  //             $pdf_name = $return_ticket_details['ticket_id'].'_Return_trip.pdf';
  //             //Move file to upload folder
  //             rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //             //Update Order Invoice
  //             $update_data = array(
  //               "invoice_url" => "ticket-invoices/".$pdf_name,
  //             );
  //             $this->api_bus->update_booking_details($update_data, $return_ticket_details['ticket_id']);
  //             //Update to workroom
  //             $workroom_update = array(
  //               'order_id' => $return_ticket_details['ticket_id'],
  //               'cust_id' => $return_ticket_details['cust_id'],
  //               'deliverer_id' => $return_ticket_details['operator_id'],
  //               'text_msg' => $this->lang->line('Invoice'),
  //               'attachment_url' =>  "ticket-invoices/".$pdf_name,
  //               'cre_datetime' => $today,
  //               'sender_id' => $return_ticket_details['operator_id'],
  //               'type' => 'raise_invoice',
  //               'file_type' => 'pdf',
  //               'cust_name' => $return_ticket_details['cust_name'],
  //               'deliverer_name' => $return_ticket_details['operator_name'],
  //               'thumbnail' => 'NULL',
  //               'ratings' => 'NULL',
  //               'cat_id' => 281
  //             );
  //             $this->api_bus->add_bus_chat_to_workroom($workroom_update);
  //           /* -------------------------------------------------------------------- */
            
  //           /* -----------------Email Invoice to customer-------------------------- */
  //             $emailBody = $this->lang->line('Please download your ticket payment invoice.');
  //             $gonagooAddress = $gonagoo_address['company_name'];
  //             $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
  //             $emailAddress = $user_details[0]['email_id'];
  //             $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //             $fileName = $pdf_name;
  //             $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //           /* -----------------------Email Invoice to customer-------------------- */

  //           /* -------------------------Return Ticket pdf ------------------------- */
  //             $return_operator_details = $this->api_bus->get_bus_operator_profile($return_ticket_details['operator_id']);
  //             $cust_sms = $this->lang->line("Dear")."! ".$user_detailss[0]['firstname']." ".$user_detailss[0]['lastname']." ".$this->lang->line('Your')." ".$this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id']." , ".$this->lang->line('For')." ".$return_ticket_details['source_point']." ".$this->lang->line('To')." ".$return_ticket_details['destination_point'];
  //             if($return_operator_details["avatar_url"]==" " || $return_operator_details["avatar_url"] == "NULL"){
  //               $operator_avtr = base_url("resources/no-image.jpg");
  //             }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
  //             require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
  //               QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
  //                $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
  //                $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
  //               if(rename($img_src, $img_dest));
  //             $html ='
  //             <page format="100x100" orientation="L" style="font: arial;">';
  //             $html .= '
  //             <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
  //               <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
  //                 <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
  //                 <div style="margin-top:27px">
  //                   <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
  //                   <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
  //                 </div>
  //               </div>
  //               <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
  //                   <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
  //                   <br />
  //                   <h6>'.$this->lang->line("slider_heading1").'</h6>
  //               </div>                
  //             </div>
  //             <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
  //               <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
  //                 '.$this->lang->line("Ownward_Trip").'<br/>
  //               <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
  //               <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
  //               <h6 style="margin-top:-10px;">
  //               <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
  //               <h6 style="margin-top:-8px;">
  //               <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
  //               <h6 style="margin-top:-8px;">
  //               <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
  //               <h6 style="margin-top:-8px;"> 
  //               <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
  //               <h6 style="margin-top:-8px;">
  //               <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
  //               $ticket_seat_details = $this->api_bus->get_ticket_seat_details($return_ticket_details['ticket_id']);
  
  //               $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
  //               foreach ($ticket_seat_details as $seat)
  //               {
  //                 $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
  //                 if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                    
  //                  $html .='</td><td>'.$this->api_bus->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
  //               }
  //             $html .='</tbody></table></div></page>';            
  //             $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
  //             require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
  //             $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
  //             $html2pdf->pdf->SetDisplayMode('default');
  //             $html2pdf->writeHTML($html);
  //             $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
  //             $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
  //             rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
  //             //whatsapp api send ticket
  //             $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
  //             $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
  //             $this->api_sms->send_sms_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$cust_sms);
  //             //echo json_encode($result); die();
  //           /* -------------------------Return Ticket pdf ------------------------- */
            
  //           /* ---------------------Email Ticket to customer----------------------- */
  //             $emailBody = $this->lang->line('Please download your e-ticket.');
  //             $gonagooAddress = $gonagoo_address['company_name'];
  //             $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
  //             $emailAddress = $user_details[0]['email_id'];
  //             $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
  //             $fileName = $my_ticket_pdf;
  //             $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //           /* ---------------------Email Ticket to customer----------------------- */

  //           //Deduct Return Trip Commission From Operator Account
  //             $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

  //             $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
  //             $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
  //             $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             $update_data_account_history = array(
  //               "account_id" => (int)$customer_account_master_details['account_id'],
  //               "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //               "user_id" => (int)trim($return_ticket_details['operator_id']),
  //               "datetime" => $today,
  //               "type" => 0,
  //               "transaction_type" => 'ticket_commission',
  //               "amount" => trim($return_trip_commission),
  //               "account_balance" => trim($customer_account_master_details['account_balance']),
  //               "withdraw_request_id" => 0,
  //               "currency_code" => trim($return_ticket_details['currency_sign']),
  //               "cat_id" => 281
  //             );
  //             $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //           //Deduct Return Trip Commission From Operator Account

  //           //Add Return Trip Commission To Gonagoo Account
  //             if($this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
  //               $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
  //             } else {
  //               $insert_data_gonagoo_master = array(
  //                 "gonagoo_balance" => 0,
  //                 "update_datetime" => $today,
  //                 "operation_lock" => 1,
  //                 "currency_code" => trim($return_ticket_details['currency_sign']),
  //               );
  //               $this->api_bus->insert_gonagoo_master_record($insert_data_gonagoo_master);
  //               $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
  //             }

  //             $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
  //             $this->api_bus->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
  //             $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

  //             $update_data_gonagoo_history = array(
  //               "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
  //               "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //               "user_id" => (int)trim($return_ticket_details['operator_id']),
  //               "type" => 1,
  //               "transaction_type" => 'ticket_commission',
  //               "amount" => trim($return_trip_commission),
  //               "datetime" => $today,
  //               "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
  //               "transfer_account_type" => trim($payment_method),
  //               "transfer_account_number" => trim($transfer_account_number),
  //               "bank_name" => trim($bank_name),
  //               "account_holder_name" => trim($account_holder_name),
  //               "iban" => trim($iban),
  //               "email_address" => trim($email_address),
  //               "mobile_number" => trim($mobile_number),
  //               "transaction_id" => trim($transaction_id),
  //               "currency_code" => trim($return_ticket_details['currency_sign']),
  //               "country_id" => trim($return_trip_operator_detail['country_id']),
  //               "cat_id" => trim($return_ticket_details['cat_id']),
  //             );
  //             $this->api_bus->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
  //           //Add Return Trip Commission To Gonagoo Account

  //           /* Return ticket commission invoice----------------------------------- */
  //             if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
  //               $parts = explode("@", trim($return_trip_operator_detail['email1']));
  //               $username = $parts[0];
  //               $operator_name = $username;
  //             } else {
  //               $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
  //             }

  //             $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //             require_once($phpinvoice);
  //             //Language configuration for invoice
  //             $lang = $this->input->post('lang', TRUE);
  //             if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //             else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //             else { $invoice_lang = "englishApi_lang"; }
  //             //Invoice Configuration
  //             $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
  //             $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //             $invoice->setColor("#000");
  //             $invoice->setType("");
  //             $invoice->setReference($return_ticket_details['ticket_id']);
  //             $invoice->setDate(date('M dS ,Y',time()));

  //             $operator_country = $this->api_bus->get_country_details(trim($return_trip_operator_detail['country_id']));
  //             $operator_state = $this->api_bus->get_state_details(trim($return_trip_operator_detail['state_id']));
  //             $operator_city = $this->api_bus->get_city_details(trim($return_trip_operator_detail['city_id']));

  //             $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
  //             $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //             $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //             $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
  //             $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

  //             $eol = PHP_EOL;
  //             /* Adding Items in table */
  //             $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
  //             $rate = round(trim($return_trip_commission),2);
  //             $total = $rate;
  //             $payment_datetime = substr($today, 0, 10);
  //             //set items
  //             $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //             /* Add totals */
  //             $invoice->addTotal($this->lang->line('sub_total'),$total);
  //             $invoice->addTotal($this->lang->line('taxes'),'0');
  //             $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //             /* Set badge */ 
  //             $invoice->addBadge($this->lang->line('Commission Paid'));
  //             /* Add title */
  //             $invoice->addTitle($this->lang->line('tnc'));
  //             /* Add Paragraph */
  //             $invoice->addParagraph($gonagoo_address['terms']);
  //             /* Add title */
  //             $invoice->addTitle($this->lang->line('payment_dtls'));
  //             /* Add Paragraph */
  //             $invoice->addParagraph($gonagoo_address['payment_details']);
  //             /* Add title */
  //             $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //             /* Add Paragraph */
  //             $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //             /* Set footer note */
  //             $invoice->setFooternote($gonagoo_address['company_name']);
  //             /* Render */
  //             $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //             //Update File path
  //             $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
  //             //Move file to upload folder
  //             rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //             //Update Order Invoice
  //             $update_data = array(
  //               "commission_invoice_url" => "ticket-invoices/".$pdf_name,
  //             );
  //             $this->api_bus->update_booking_details($update_data, $return_ticket_details['ticket_id']);
  //           /* -------------------------------------------------------------------- */

  //           /* -----------------------Email Invoice to operator-------------------- */
  //             $emailBody = $this->lang->line('Commission Invoice');
  //             $gonagooAddress = $gonagoo_address['company_name'];
  //             $emailSubject = $this->lang->line('Commission Invoice');
  //             $emailAddress = $return_trip_operator_detail['email1'];
  //             $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //             $fileName = $pdf_name;
  //             $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //           /* -----------------------Email Invoice to operator-------------------- */
  //         }
  //       //----------------------Return Trip Payment Calculation-----------------
        
  //     /********************* Payment Section ************************************/
  //     $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed. Your e-ticket is sent to your email.'));
  //   } else { $this->session->set_flashdata('error', $this->lang->line('payment_failed')); }
  //   redirect(base_url('ticket-payment-confirmation').'/'.$ticket_id,'refresh');
  // }
  // public function ticket_payment_by_orange()
  // {
  //   //echo json_encode($_POST); die();
  //   $cust_id = $this->input->post('cust_id', TRUE); $cust_id = (int) $cust_id;  
  //   $user_details = $this->api->get_user_details($cust_id);
  //   $ticket_id = $this->input->post('ticket_id', TRUE); $ticket_id = (int) $ticket_id;
  //   $ticket_details = $this->api->get_trip_booking_details($ticket_id);
  //   $payment_method = $this->input->post('payment_method', TRUE);
  //   $ticket_price = $this->input->post('ticket_price', TRUE);
  //   $currency_sign = $this->input->post('currency_sign', TRUE);

  //   $ticket_payment_id = 'gonagoo_'.time().'_'.$ticket_id;

  //   //get access token
  //   $ch = curl_init("https://api.orange.com/oauth/v2/token?");
  //   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  //       'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
  //   ));
  //   curl_setopt($ch, CURLOPT_POST, 1);
  //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //   curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
  //   $orange_token = curl_exec($ch);
  //   curl_close($ch);
  //   $token_details = json_decode($orange_token, TRUE);
  //   $token = 'Bearer '.$token_details['access_token'];

  //   // get payment link
  //   $json_post_data = json_encode(
  //       array(
  //         "merchant_key" => "a8f8c61e", 
  //         "currency" => "OUV", 
  //         "order_id" => $ticket_payment_id, 
  //         "amount" => $ticket_price,  
  //         "return_url" => base_url('ticket-payment-orange-process'), 
  //         "cancel_url" => base_url('ticket-payment-orange-process'), 
  //         "notif_url" => base_url('test.php'), 
  //         "lang" => "en", 
  //         "reference" => $this->lang->line('Gonagoo - Service Payment') 
  //       )
  //     );
  //   //echo $json_post_data; die();
  //   $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/webpayment?");
  //   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  //       "Content-Type: application/json",
  //       "Authorization: ".$token,
  //       "Accept: application/json"
  //   ));
  //   curl_setopt($ch, CURLOPT_POST, 1);
  //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //   curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
  //   $orange_url = curl_exec($ch);
  //   curl_close($ch);
  //   $url_details = json_decode($orange_url, TRUE);
  //   //echo json_encode($url_details); die();
  //   if(isset($url_details)) {
  //     $this->session->set_userdata('notif_token', $url_details['notif_token']);
  //     $this->session->set_userdata('ticket_id', $ticket_id);
  //     $this->session->set_userdata('ticket_payment_id', $ticket_payment_id);
  //     $this->session->set_userdata('amount', $ticket_price);
  //     $this->session->set_userdata('pay_token', $url_details['pay_token']);
  //     //echo json_encode($url_details); die();
  //     header('Location: '.$url_details['payment_url']);
  //   } else {
  //     $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
  //     redirect(base_url('ticket-payment-confirmation').'/'.$ticket_id,'refresh');
  //   }
  // }
  // public function ticket_payment_by_orange_process()
  // {
  //   //echo json_encode($_SESSION); die();
  //   $session_token = $_SESSION['notif_token'];
  //   $ticket_id = $_SESSION['ticket_id'];
  //   $ticket_details = $this->api_bus->get_trip_booking_details($ticket_id);
  //   $ticket_payment_id = $_SESSION['ticket_payment_id'];
  //   $amount = $_SESSION['amount'];
  //   $pay_token = $_SESSION['pay_token'];
  //   $payment_method = 'orange';
  //   $today = date('Y-m-d h:i:s');
  //   //get access token
  //   $ch = curl_init("https://api.orange.com/oauth/v2/token?");
  //   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  //     'Authorization: Basic WkhZSks3Wk9WWkh5Q0ZPR1hkSkdnVXZaMWNJczZZREQ6WFhKWnJSS2ZQRVUzWlNzcA=='
  //   ));
  //   curl_setopt($ch, CURLOPT_POST, 1);
  //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //   curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=client_credentials"); 
  //   $orange_token = curl_exec($ch);
  //   curl_close($ch);
  //   $token_details = json_decode($orange_token, TRUE);
  //   $token = 'Bearer '.$token_details['access_token'];

  //   // get payment link
  //   $json_post_data = json_encode(
  //                       array(
  //                         "order_id" => $ticket_payment_id, 
  //                         "amount" => $amount, 
  //                         "pay_token" => $pay_token
  //                       )
  //                     );
  //   //echo $json_post_data; die();
  //   $ch = curl_init("https://api.orange.com/orange-money-webpay/dev/v1/transactionstatus?");
  //   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  //       "Content-Type: application/json",
  //       "Authorization: ".$token,
  //       "Accept: application/json"
  //   ));
  //   curl_setopt($ch, CURLOPT_POST, 1);
  //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //   curl_setopt($ch, CURLOPT_POSTFIELDS, $json_post_data); 
  //   $payment_res = curl_exec($ch);
  //   curl_close($ch);
  //   $payment_details = json_decode($payment_res, TRUE);
  //   //$transaction_id = $payment_details['txnid'];
  //   $transaction_id = 2345;
    
  //   //echo json_encode($payment_details); die();

  //   //if($payment_details['status'] == 'SUCCESS' && $ticket_details['complete_paid'] == 0) {
  //   if(1){ 
  //     $user_details = $this->api_bus->get_user_details($ticket_details['cust_id']);
  //     $cust_id = (int)$user_details['cust_id'];
  //     $location_details = $this->api_bus->get_bus_location_details($ticket_details['source_point_id']);
  //     $country_id = $this->api_bus->get_country_id_by_name($location_details['country_name']);

  //     $gonagoo_commission_percentage = $this->api_bus->get_gonagoo_bus_commission_details((int)$country_id);
  //     //echo json_encode($gonagoo_commission_percentage); die();
  //     $ownward_trip_commission = round(($ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

  //     $total_amount_paid = $ticket_details['ticket_price'];
  //     $transfer_account_number = "NULL";
  //     $bank_name = "NULL";
  //     $account_holder_name = "NULL";
  //     $iban = "NULL";
  //     $email_address = 'NULL';
  //     $mobile_number = "NULL";  

  //     $seat_details = $this->api_bus->get_ticket_seat_details($ticket_id); 
      
  //     $message = $this->lang->line('Payment recieved for Ticket ID: ').trim($ticket_details['ticket_id']).$this->lang->line('. Payment ID: ').trim($transaction_id);

  //     for ($i=0; $i < sizeof($seat_details); $i++) { 
  //       $country_code = $this->api_bus->get_country_code_by_id($seat_details[$i]['country_id']);
  //       $this->api_bus->sendSMS((int)$country_code.trim($seat_details[$i]['mobile']), $message);
  //       //$this->api_sms->send_sms_whatsapp((int)$country_code.trim($seat_details[$i]['mobile']), $message);
  //     }

  //     //echo json_encode($seat_details); die();
  //     //update payment status in booking
  //     $trip_update = array(
  //       "payment_method" => 'orange',
  //       "paid_amount" => $ticket_details['ticket_price'],
  //       "balance_amount" => 0,
  //       "transaction_id" => $transaction_id,
  //       "payment_datetime" => $today,
  //       "payment_mode" => "online",
  //       "payment_by" => "web",
  //       "complete_paid" => 1,
  //       'boarding_code' => $ticket_details['unique_id'].'_'.$ticket_id,
  //     );
  //     $this->api_bus->update_booking_details($trip_update, $ticket_id);
      
  //     /************************** Payment Section ********************************/
  //       //---------------------Insert counter payment record----------------------
  //         $data_counter_sale_payment = array(
  //           "cat_id" => $ticket_details['cat_id'],
  //           "service_id" => $ticket_details['ticket_id'],
  //           "cust_id" => $ticket_details['operator_id'],
  //           "cre_datetime" => $today,
  //           "type" => 1,
  //           "transaction_type" => 'payment',
  //           "amount" => trim($ticket_details['ticket_price']),
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
  //           "cat_name" => 'ticket_booking',
  //           "customer_email" => trim($seat_details[0]['email_id']),
  //           "customer_mobile" => trim($seat_details[0]['mobile']),
  //           "customer_country_id" => trim($seat_details[0]['country_id']),
  //         );
  //         $this->api_bus->insert_counter_sale_payment($data_counter_sale_payment);
  //       //---------------------Insert counter payment record----------------------

  //       $ownward_trip_operator_detail = $this->api_bus->get_user_details($ticket_details['operator_id']);
  //       if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
  //         $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
  //         $username = $parts[0];
  //         $operator_name = $username;
  //       } else {
  //         $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
  //       }

  //       $user_details = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']);
  //       if( trim($user_details[0]['firstname']) == 'NULL' || trim($user_details[0]['lastname'] == 'NULL') ) {
  //         $parts = explode("@", trim($user_details[0]['email_id']));
  //         $username = $parts[0];
  //         $customer_name = $username;
  //       } else {
  //         $customer_name = trim($user_details[0]['firstname']) . " " . trim($user_details[0]['lastname']);
  //       }
        
  //       /* -----------------------Ownward ticket invoice---------------------------- */
  //         $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //         require_once($phpinvoice);
  //         //Language configuration for invoice
  //         $lang = $this->input->post('lang', TRUE);
  //         if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //         else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //         else { $invoice_lang = "englishApi_lang"; }
  //         //Invoice Configuration
  //         $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
  //         $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //         $invoice->setColor("#000");
  //         $invoice->setType("");
  //         $invoice->setReference($ticket_details['ticket_id']);
  //         $invoice->setDate(date('M dS ,Y',time()));

  //         $operator_country = $this->api_bus->get_country_details(trim($ownward_trip_operator_detail['country_id']));
  //         $operator_state = $this->api_bus->get_state_details(trim($ownward_trip_operator_detail['state_id']));
  //         $operator_city = $this->api_bus->get_city_details(trim($ownward_trip_operator_detail['city_id']));

  //         $user_country = $this->api_bus->get_country_details(trim($seat_details[0]['country_id']));
  //         $user_state = '---';
  //         $user_city = '---';

  //         $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
  //         $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //         $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //         $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

  //         $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($seat_details[0]['email_id'])));
          
  //         $eol = PHP_EOL;
  //         /* Adding Items in table */
  //         $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
  //         $rate = round(trim($ticket_details['ticket_price']),2);
  //         $total = $rate;
  //         $payment_datetime = substr($today, 0, 10);
  //         //set items

  //         $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //         /* Add totals */
  //         $invoice->addTotal($this->lang->line('sub_total'),$total);
  //         $invoice->addTotal($this->lang->line('taxes'),'0');
  //         $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //         /* Set badge */ 
  //         $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
  //         /* Add title */
  //         $invoice->addTitle($this->lang->line('tnc'));
  //         /* Add Paragraph */
  //         $invoice->addParagraph($gonagoo_address['terms']);
  //         /* Add title */
  //         $invoice->addTitle($this->lang->line('payment_dtls'));
  //         /* Add Paragraph */
  //         $invoice->addParagraph($gonagoo_address['payment_details']);
  //         /* Add title */
  //         $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //         /* Add Paragraph */
  //         $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //         /* Set footer note */
  //         $invoice->setFooternote($gonagoo_address['company_name']);
  //         /* Render */
  //         $invoice->render($ticket_details['ticket_id'].'_ownward_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //         //Update File path
  //         $pdf_name = $ticket_details['ticket_id'].'_ownward_trip.pdf';
  //         //Move file to upload folder
  //         rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //         //Update Order Invoice
  //         $update_data = array(
  //           "invoice_url" => "ticket-invoices/".$pdf_name,
  //         );
  //         $this->api_bus->update_booking_details($update_data, $ticket_details['ticket_id']);
  //       /* -----------------------Ownward ticket invoice---------------------------- */
        
  //       /* -----------------Email Invoice to customer-------------------------- */
  //         $emailBody = $this->lang->line('Please download your ticket payment invoice.');
  //         $gonagooAddress = $gonagoo_address['company_name'];
  //         $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
  //         $emailAddress = $user_details[0]['email_id'];
  //         $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //         $fileName = $pdf_name;
  //         $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //       /* -----------------------Email Invoice to customer-------------------- */
        
  //       /* ------------------------Generate Ticket pdf ------------------------ */
  //         $user_detailss = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']);
  //         $cust_sms = $this->lang->line("Dear")."! ".$user_detailss[0]['firstname']." ".$user_detailss[0]['lastname']." ".$this->lang->line('Your')." ".$this->lang->line('Payment completed for ticket ID - ').$ticket_details['ticket_id']." , ".$this->lang->line('For')." ".$ticket_details['source_point']." ".$this->lang->line('To')." ".$ticket_details['destination_point'];
  //         $operator_details = $this->api_bus->get_bus_operator_profile($ticket_details['operator_id']);
  //         if($operator_details["avatar_url"]==" " || $operator_details["avatar_url"] == "NULL"){
  //           $operator_avtr = base_url("resources/no-image.jpg");
  //         }else{ $operator_avtr = base_url($operator_details["avatar_url"]); }
  //         require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
  //           QRcode::png($ticket_details['ticket_id'], $ticket_details['ticket_id'].".png", "L", 2, 2); 
  //            $img_src = $_SERVER['DOCUMENT_ROOT']."/".$ticket_details['ticket_id'].'.png';
  //            $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$ticket_details['ticket_id'].'.png';
  //           if(rename($img_src, $img_dest));
  //         $html ='
  //         <page format="100x100" orientation="L" style="font: arial;">';
  //         $html .= '
  //         <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
  //           <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
  //             <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
  //             <div style="margin-top:27px">
  //               <h6 style="margin: 5px 0;">'.$operator_details["company_name"].'</h6>
  //               <h6 style="margin: 5px 0;">'.$operator_details["firstname"]. ' ' .$operator_details["lastname"].'</h6>
  //             </div>
  //           </div>
  //           <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
  //               <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
  //               <br />
  //               <h6>'.$this->lang->line("slider_heading1").'</h6>
  //           </div>                
  //         </div>
  //         <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
  //           <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
  //             '.$this->lang->line("Ownward_Trip").'<br/>
  //           <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$ticket_details["ticket_id"].'</strong><br/></h5>
  //           <img src="'.base_url("resources/ticket-qrcode/").$ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
  //           <h6 style="margin-top:-10px;">
  //           <strong>'.$this->lang->line("Source").':</strong> '.$ticket_details["source_point"] .'</h6>
  //           <h6 style="margin-top:-8px;">
  //           <strong>'. $this->lang->line("Destination").':</strong> '. $ticket_details["destination_point"] .'</h6>
  //           <h6 style="margin-top:-8px;">
  //           <strong>'. $this->lang->line("Journey Date").':</strong> '. $ticket_details["journey_date"] .'</h6>
  //           <h6 style="margin-top:-8px;"> 
  //           <strong>'. $this->lang->line("Departure Time").':</strong>'. $ticket_details["trip_start_date_time"] .'</h6>
  //           <h6 style="margin-top:-8px;">
  //           <strong>'. $this->lang->line("no_of_seats").':</strong> '. $ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $ticket_details["ticket_price"] .' '. $ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
  //           $ticket_seat_details = $this->api_bus->get_ticket_seat_details($ticket_details['ticket_id']);
  
  //           $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
  //           foreach ($ticket_seat_details as $seat)
  //           {
  //             $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
  //             if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                
  //              $html .='</td><td>'.$this->api_bus->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
  //           }
  //         $html .='</tbody></table></div></page>';            
  //         $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$ticket_details['ticket_id'].'_ticket.pdf';
  //         require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
  //         $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
  //         $html2pdf->pdf->SetDisplayMode('default');
  //         $html2pdf->writeHTML($html);
  //         $html2pdf->output(__DIR__."/../../".$ticket_details['ticket_id'].'_ticket.pdf','F');
  //         $my_ticket_pdf = $ticket_details['ticket_id'].'_ticket.pdf';
  //         rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
  //         //whatsapp api send ticket
  //         $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
  //         $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
  //         $this->api_sms->send_sms_whatsapp($country_code.trim($user_detailss[0]['mobile']), $cust_sms);
  //         //echo json_encode($result); die();
  //       /* ------------------------Generate Ticket pdf ------------------------ */

  //       /* ---------------------Email Ticket to customer----------------------- */
  //         $emailBody = $this->lang->line('Please download your e-ticket.');
  //         $gonagooAddress = $gonagoo_address['company_name'];
  //         $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
  //         $emailAddress = $user_details[0]['email_id'];
  //         $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
  //         $fileName = $my_ticket_pdf;
  //         $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //       /* ---------------------Email Ticket to customer----------------------- */
        
  //       //Add Ownward Trip Payment to trip operator account + History---------
  //         if($this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
  //           $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //         } else {
  //           $insert_data_customer_master = array(
  //             "user_id" => trim($ticket_details['operator_id']),
  //             "account_balance" => 0,
  //             "update_datetime" => $today,
  //             "operation_lock" => 1,
  //             "currency_code" => trim($ticket_details['currency_sign']),
  //           );
  //           $this->api_bus->insert_gonagoo_customer_record($insert_data_customer_master);
  //           $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //         }
  //         $account_balance = trim($customer_account_master_details['account_balance']) + trim($ticket_details['ticket_price']);
  //         $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
  //         $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //         $update_data_account_history = array(
  //           "account_id" => (int)$customer_account_master_details['account_id'],
  //           "order_id" => (int)$ticket_id,
  //           "user_id" => (int)trim($ticket_details['operator_id']),
  //           "datetime" => $today,
  //           "type" => 1,
  //           "transaction_type" => 'add',
  //           "amount" => trim($ticket_details['ticket_price']),
  //           "account_balance" => trim($customer_account_master_details['account_balance']),
  //           "withdraw_request_id" => 0,
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "cat_id" => 281
  //         );
  //         $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //       //Add Ownward Trip Payment to trip operator account + History---------

  //       //Deduct Ownward Trip Commission From Operator Account---------------
  //         if($this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']))) {
  //           $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //         } else {
  //           $insert_data_customer_master = array(
  //             "user_id" => trim($ticket_details['operator_id']),
  //             "account_balance" => 0,
  //             "update_datetime" => $today,
  //             "operation_lock" => 1,
  //             "currency_code" => trim($ticket_details['currency_sign']),
  //           );
  //           $this->api_bus->insert_gonagoo_customer_record($insert_data_customer_master);
  //           $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //         }
  //         $account_balance = trim($customer_account_master_details['account_balance']) - trim($ownward_trip_commission);
  //         $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($ticket_details['operator_id']), $account_balance);
  //         $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($ticket_details['operator_id']), trim($ticket_details['currency_sign']));
  //         $update_data_account_history = array(
  //           "account_id" => (int)$customer_account_master_details['account_id'],
  //           "order_id" => (int)$ticket_id,
  //           "user_id" => (int)trim($ticket_details['operator_id']),
  //           "datetime" => $today,
  //           "type" => 0,
  //           "transaction_type" => 'ticket_commission',
  //           "amount" => trim($ownward_trip_commission),
  //           "account_balance" => trim($customer_account_master_details['account_balance']),
  //           "withdraw_request_id" => 0,
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "cat_id" => 281
  //         );
  //         $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //       //Deduct Ownward Trip Commission From Operator Account---------------

  //       //Add Ownward Trip Commission To Gonagoo Account---------------------
  //         if($this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']))) {
  //           $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));
  //         } else {
  //           $insert_data_gonagoo_master = array(
  //             "gonagoo_balance" => 0,
  //             "update_datetime" => $today,
  //             "operation_lock" => 1,
  //             "currency_code" => trim($ticket_details['currency_sign']),
  //           );
  //           $this->api_bus->insert_gonagoo_master_record($insert_data_gonagoo_master);
  //           $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));
  //         }

  //         $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($ownward_trip_commission);
  //         $this->api_bus->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
  //         $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($ticket_details['currency_sign']));

  //         $update_data_gonagoo_history = array(
  //           "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
  //           "order_id" => (int)$ticket_id,
  //           "user_id" => (int)trim($ticket_details['operator_id']),
  //           "type" => 1,
  //           "transaction_type" => 'ticket_commission',
  //           "amount" => trim($ownward_trip_commission),
  //           "datetime" => $today,
  //           "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
  //           "transfer_account_type" => 'orange',
  //           "transfer_account_number" => trim($transfer_account_number),
  //           "bank_name" => trim($bank_name),
  //           "account_holder_name" => trim($account_holder_name),
  //           "iban" => trim($iban),
  //           "email_address" => trim($email_address),
  //           "mobile_number" => trim($mobile_number),
  //           "transaction_id" => trim($transaction_id),
  //           "currency_code" => trim($ticket_details['currency_sign']),
  //           "country_id" => trim($ownward_trip_operator_detail['country_id']),
  //           "cat_id" => trim($ticket_details['cat_id']),
  //         );
  //         $this->api_bus->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
  //       //Add Ownward Trip Commission To Gonagoo Account---------------------

  //       /* Ownward ticket commission invoice----------------------------------- */
  //         if( trim($ownward_trip_operator_detail['firstname']) == 'NULL' || trim($ownward_trip_operator_detail['lastname'] == 'NULL') ) {
  //           $parts = explode("@", trim($ownward_trip_operator_detail['email1']));
  //           $username = $parts[0];
  //           $operator_name = $username;
  //         } else {
  //           $operator_name = trim($ownward_trip_operator_detail['firstname']) . " " . trim($ownward_trip_operator_detail['lastname']);
  //         }

  //         $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //         require_once($phpinvoice);
  //         //Language configuration for invoice
  //         $lang = $this->input->post('lang', TRUE);
  //         if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //         else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //         else { $invoice_lang = "englishApi_lang"; }
  //         //Invoice Configuration
  //         $invoice = new phpinvoice('A4',trim($ticket_details['currency_sign']),$invoice_lang);
  //         $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //         $invoice->setColor("#000");
  //         $invoice->setType("");
  //         $invoice->setReference($ticket_details['ticket_id']);
  //         $invoice->setDate(date('M dS ,Y',time()));

  //         $operator_country = $this->api_bus->get_country_details(trim($ownward_trip_operator_detail['country_id']));
  //         $operator_state = $this->api_bus->get_state_details(trim($ownward_trip_operator_detail['state_id']));
  //         $operator_city = $this->api_bus->get_city_details(trim($ownward_trip_operator_detail['city_id']));

  //         $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($ownward_trip_operator_detail['country_id']));
  //         $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //         $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //         $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
          
  //         $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($ownward_trip_operator_detail['email1'])));

  //         $eol = PHP_EOL;
  //         /* Adding Items in table */
  //         $order_for = $this->lang->line('Ticket ID: ').$ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
  //         $rate = round(trim($ownward_trip_commission),2);
  //         $total = $rate;
  //         $payment_datetime = substr($today, 0, 10);
  //         //set items
  //         $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //         /* Add totals */
  //         $invoice->addTotal($this->lang->line('sub_total'),$total);
  //         $invoice->addTotal($this->lang->line('taxes'),'0');
  //         $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //         /* Set badge */ 
  //         $invoice->addBadge($this->lang->line('Commission Paid'));
  //         /* Add title */
  //         $invoice->addTitle($this->lang->line('tnc'));
  //         /* Add Paragraph */
  //         $invoice->addParagraph($gonagoo_address['terms']);
  //         /* Add title */
  //         $invoice->addTitle($this->lang->line('payment_dtls'));
  //         /* Add Paragraph */
  //         $invoice->addParagraph($gonagoo_address['payment_details']);
  //         /* Add title */
  //         $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //         /* Add Paragraph */
  //         $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //         /* Set footer note */
  //         $invoice->setFooternote($gonagoo_address['company_name']);
  //         /* Render */
  //         $invoice->render($ticket_details['ticket_id'].'_ownward_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //         //Update File path
  //         $pdf_name = $ticket_details['ticket_id'].'_ownward_trip_commission.pdf';
  //         //Move file to upload folder
  //         rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //         //Update Order Invoice
  //         $update_data = array(
  //           "commission_invoice_url" => "ticket-invoices/".$pdf_name,
  //         );
  //         $this->api_bus->update_booking_details($update_data, $ticket_details['ticket_id']);
  //       /* Ownward ticket commission invoice----------------------------------- */
        
  //       /* -----------------------Email Invoice to operator-------------------- */
  //         $emailBody = $this->lang->line('Commission Invoice');
  //         $gonagooAddress = $gonagoo_address['company_name'];
  //         $emailSubject = $this->lang->line('Commission Invoice');
  //         $emailAddress = $ownward_trip_operator_detail['email1'];
  //         $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //         $fileName = $pdf_name;
  //         $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //       /* -----------------------Email Invoice to operator-------------------- */

  //       //-----------------------Return Trip Payment Calculation-----------------
  //         if($ticket_details['return_trip_id'] > 0) {
  //           $return_ticket_details = $this->api_bus->get_trip_booking_details($ticket_details['return_trip_id']);

  //           //Update order payment details
  //           $return_update_data = array(
  //             "payment_method" => trim($payment_method),
  //             "paid_amount" => trim($return_ticket_details['ticket_price']),
  //             "balance_amount" => 0,
  //             "transaction_id" => trim($transaction_id),
  //             "payment_datetime" => $today,
  //             "payment_mode" => "online",
  //             "payment_by" => "web",
  //             "complete_paid" => 1,
  //             'boarding_code' => $return_ticket_details['unique_id'].'_'.$return_ticket_details['ticket_id'],
  //           );
  //           $this->api_bus->update_booking_details($return_update_data, $return_ticket_details['ticket_id']);

  //           //---------------------Insert counter payment record---------------------
  //             $data_counter_sale_payment = array(
  //               "cat_id" => $return_ticket_details['cat_id'],
  //               "service_id" => $return_ticket_details['ticket_id'],
  //               "cust_id" => $return_ticket_details['operator_id'],
  //               "cre_datetime" => $today,
  //               "type" => 1,
  //               "transaction_type" => 'payment',
  //               "amount" => trim($return_ticket_details['ticket_price']),
  //               "currency_code" => trim($return_ticket_details['currency_sign']),
  //               "customer_name" => trim($seat_details[0]['firstname']) . ' ' . trim($seat_details[0]['lastname']),
  //               "cat_name" => 'ticket_booking',
  //               "customer_email" => trim($seat_details[0]['email_id']),
  //               "customer_mobile" => trim($seat_details[0]['mobile']),
  //               "customer_country_id" => trim($seat_details[0]['country_id']),
  //             );
  //             $this->api_bus->insert_counter_sale_payment($data_counter_sale_payment);
  //           //---------------------Insert counter payment record---------------------
            
  //           /* ------------------------Return ticket invoice-------------------- */
  //             $return_trip_operator_detail = $this->api_bus->get_user_details($return_ticket_details['operator_id']);

  //             if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
  //               $parts = explode("@", trim($return_trip_operator_detail['email1']));
  //               $username = $parts[0];
  //               $operator_name = $username;
  //             } else {
  //               $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
  //             }

  //             $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //             require_once($phpinvoice);
  //             //Language configuration for invoice
  //             $lang = $this->input->post('lang', TRUE);
  //             if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //             else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //             else { $invoice_lang = "englishApi_lang"; }
  //             //Invoice Configuration
  //             $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
  //             $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //             $invoice->setColor("#000");
  //             $invoice->setType("");
  //             $invoice->setReference($return_ticket_details['ticket_id']);
  //             $invoice->setDate(date('M dS ,Y',time()));

  //             $operator_country = $this->api_bus->get_country_details(trim($return_trip_operator_detail['country_id']));
  //             $operator_state = $this->api_bus->get_state_details(trim($return_trip_operator_detail['state_id']));
  //             $operator_city = $this->api_bus->get_city_details(trim($return_trip_operator_detail['city_id']));

  //             $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
  //             $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //             $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //             $invoice->setTo(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

  //             $invoice->setFrom(array($customer_name,trim($user_city),trim($user_state),trim($user_country['country_name']),trim($seat_details[0]['email_id'])));
              
  //             $eol = PHP_EOL;
  //             /* Adding Items in table */
  //             $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Payment');
  //             $rate = round(trim($return_ticket_details['ticket_price']),2);
  //             $total = $rate;
  //             $payment_datetime = substr($today, 0, 10);
  //             //set items
  //             $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //             /* Add totals */
  //             $invoice->addTotal($this->lang->line('sub_total'),$total);
  //             $invoice->addTotal($this->lang->line('taxes'),'0');
  //             $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //             /* Set badge */ 
  //             $invoice->addBadge($this->lang->line('Ticket Amount Paid'));
  //             /* Add title */
  //             $invoice->addTitle($this->lang->line('tnc'));
  //             /* Add Paragraph */
  //             $invoice->addParagraph($gonagoo_address['terms']);
  //             /* Add title */
  //             $invoice->addTitle($this->lang->line('payment_dtls'));
  //             /* Add Paragraph */
  //             $invoice->addParagraph($gonagoo_address['payment_details']);
  //             /* Add title */
  //             $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //             /* Add Paragraph */
  //             $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //             /* Set footer note */
  //             $invoice->setFooternote($gonagoo_address['company_name']);
  //             /* Render */
  //             $invoice->render($return_ticket_details['ticket_id'].'_return_trip.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //             //Update File path
  //             $pdf_name = $return_ticket_details['ticket_id'].'_return_trip.pdf';
  //             //Move file to upload folder
  //             rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //             //Update Order Invoice
  //             $update_data = array(
  //               "invoice_url" => "ticket-invoices/".$pdf_name,
  //             );
  //             $this->api_bus->update_booking_details($update_data, $return_ticket_details['ticket_id']);
  //             $workroom_invoice_url = "ticket-invoices/".$pdf_name;
  //           /* ------------------------Return ticket invoice-------------------- */
            
  //           /* -----------------Email Invoice to customer-------------------------- */
  //             $emailBody = $this->lang->line('Please download your ticket payment invoice.');
  //             $gonagooAddress = $gonagoo_address['company_name'];
  //             $emailSubject = $this->lang->line('Gonagoo - Ticket Invoice');
  //             $emailAddress = $user_details[0]['email_id'];
  //             $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //             $fileName = $pdf_name;
  //             $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //           /* -----------------------Email Invoice to customer-------------------- */
            
  //           /* -------------------------Return Ticket pdf ------------------------- */
  //             $return_operator_details = $this->api_bus->get_bus_operator_profile($return_ticket_details['operator_id']);
  //             $cust_sms = $this->lang->line("Dear")."! ".$user_detailss[0]['firstname']." ".$user_detailss[0]['lastname']." ".$this->lang->line('Your')." ".$this->lang->line('Payment completed for ticket ID - ').$return_ticket_details['ticket_id']." , ".$this->lang->line('For')." ".$return_ticket_details['source_point']." ".$this->lang->line('To')." ".$return_ticket_details['destination_point'];
  //             if($return_operator_details["avatar_url"]==" " || $return_operator_details["avatar_url"] == "NULL"){
  //               $operator_avtr = base_url("resources/no-image.jpg");
  //             }else{ $operator_avtr = base_url($return_operator_details["avatar_url"]); }
  //             require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php");
  //               QRcode::png($return_ticket_details['ticket_id'], $return_ticket_details['ticket_id'].".png", "L", 2, 2); 
  //                $img_src = $_SERVER['DOCUMENT_ROOT']."/".$return_ticket_details['ticket_id'].'.png';
  //                $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$return_ticket_details['ticket_id'].'.png';
  //               if(rename($img_src, $img_dest));
  //             $html ='
  //             <page format="100x100" orientation="L" style="font: arial;">';
  //             $html .= '
  //             <div style="margin-left:16px; margin-top:20px; width: 340px; height:80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
  //               <div class="col-md-6" style="width: 80px; border-right: dashed 1px #225595; display: flex;">
  //                 <img src="'.$operator_avtr.'" style="margin-left:15px; border:5px solid #3498db; width: 56px; height: auto; vertical-align:auto; float: left; margin-right: 5px">
  //                 <div style="margin-top:27px">
  //                   <h6 style="margin: 5px 0;">'.$return_operator_details["company_name"].'</h6>
  //                   <h6 style="margin: 5px 0;">'.$return_operator_details["firstname"]. ' ' .$return_operator_details["lastname"].'</h6>
  //                 </div>
  //               </div>
  //               <div class="col-md-6" style="margin-left: 190px; margin-top:-55px;">
  //                   <img src="'.base_url("resources/images/dashboard-logo.jpg") .'" style="width: 100px; height: auto;"/>
  //                   <br />
  //                   <h6>'.$this->lang->line("slider_heading1").'</h6>
  //               </div>                
  //             </div>
  //             <div style="margin-left:16px; width: 337px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
  //               <h5 style="color: #225595; text-decoration: underline; margin-top:5px;">
  //                 '.$this->lang->line("Ownward_Trip").'<br/>
  //               <strong style="margin-left: 240px; float: right; padding-right: 2px; margin-top: -15px">'.$this->lang->line("Ticket ID").': #'.$return_ticket_details["ticket_id"].'</strong><br/></h5>
  //               <img src="'.base_url("resources/ticket-qrcode/").$return_ticket_details["ticket_id"].'.png'.'" style="float: right; width: 84px; height: 84px; margin-top: -15px ; margin-left:4px;"  />
  //               <h6 style="margin-top:-10px;">
  //               <strong>'.$this->lang->line("Source").':</strong> '.$return_ticket_details["source_point"] .'</h6>
  //               <h6 style="margin-top:-8px;">
  //               <strong>'. $this->lang->line("Destination").':</strong> '. $return_ticket_details["destination_point"] .'</h6>
  //               <h6 style="margin-top:-8px;">
  //               <strong>'. $this->lang->line("Journey Date").':</strong> '. $return_ticket_details["journey_date"] .'</h6>
  //               <h6 style="margin-top:-8px;"> 
  //               <strong>'. $this->lang->line("Departure Time").':</strong>'. $return_ticket_details["trip_start_date_time"] .'</h6>
  //               <h6 style="margin-top:-8px;">
  //               <strong>'. $this->lang->line("no_of_seats").':</strong> '. $return_ticket_details["no_of_seats"] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'. $this->lang->line("Price").':</strong> '. $return_ticket_details["ticket_price"] .' '. $return_ticket_details["currency_sign"] .'</h6><h6 style="margin-top:-8px;"><strong>'. $this->lang->line("Passenger Details").':</strong></h6>';
  //               $ticket_seat_details = $this->api_bus->get_ticket_seat_details($return_ticket_details['ticket_id']);

  //               $html .= '<table style="margin-top:-10px"><thead><tr><th style="width:90px;">'. $this->lang->line("Name").'</th><th style="width:90px;">'. $this->lang->line("Gender").'</th><th style="width:90px;">'. $this->lang->line("Contact").'</th></tr></thead><tbody>';
  //               foreach ($ticket_seat_details as $seat)
  //               {
  //                 $html .= '<tr><td>'.$seat["firstname"].' ' .$seat["lastname"].'</td><td>';
  //                 if($seat["gender"]=="M"){$html .= 'Male';}else{$html .= 'Female';}
                    
  //                  $html .='</td><td>'.$this->api_bus->get_country_code_by_id($seat["country_id"]).$seat["mobile"].'</td></tr>';
  //               }
  //             $html .='</tbody></table></div></page>';            
  //             $ticket_pdf_locatoion = $_SERVER['DOCUMENT_ROOT'].'resources/ticket-pdf/'.$return_ticket_details['ticket_id'].'_ticket.pdf';
  //             require_once('resources/fpdf-master/new_fpdf/vendor/autoload.php');
  //             $html2pdf = new Html2Pdf('', 'A4', 'en', true, 'UTF-8', 0);
  //             $html2pdf->pdf->SetDisplayMode('default');
  //             $html2pdf->writeHTML($html);
  //             $html2pdf->output(__DIR__."/../../".$return_ticket_details['ticket_id'].'_return_ticket.pdf','F');
  //             $my_ticket_pdf = $return_ticket_details['ticket_id'].'_return_ticket.pdf';
  //             rename($my_ticket_pdf, "resources/ticket-pdf/".$my_ticket_pdf);  
  //             //whatsapp api send ticket
  //             $ticket_location = base_url().'resources/ticket-pdf/'.$my_ticket_pdf;
  //             $this->api_sms->send_pdf_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$ticket_location ,$my_ticket_pdf);
  //             $this->api_sms->send_sms_whatsapp($country_code.trim($user_detailss[0]['mobile']) ,$cust_sms);
  //             //echo json_encode($result); die();
  //           /* -------------------------Return Ticket pdf ------------------------- */
            
  //           /* ---------------------Email Ticket to customer----------------------- */
  //             $emailBody = $this->lang->line('Please download your e-ticket.');
  //             $gonagooAddress = $gonagoo_address['company_name'];
  //             $emailSubject = $this->lang->line('Gonagoo - e-Ticket');
  //             $emailAddress = $user_details[0]['email_id'];
  //             $fileToAttach = "resources/ticket-pdf/$my_ticket_pdf";
  //             $fileName = $my_ticket_pdf;
  //             $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //           /* ---------------------Email Ticket to customer----------------------- */

  //           //Add Return Trip Payment to trip operator account + History
  //             if($this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']))) {
  //               $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             } else {
  //               $insert_data_customer_master = array(
  //                 "user_id" => trim($return_ticket_details['operator_id']),
  //                 "account_balance" => 0,
  //                 "update_datetime" => $today,
  //                 "operation_lock" => 1,
  //                 "currency_code" => trim($return_ticket_details['currency_sign']),
  //               );
  //               $this->api_bus->insert_gonagoo_customer_record($insert_data_customer_master);
  //               $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             }
  //             $account_balance = trim($customer_account_master_details['account_balance']) + trim($return_ticket_details['ticket_price']);
  //             $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
  //             $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             $update_data_account_history = array(
  //               "account_id" => (int)$customer_account_master_details['account_id'],
  //               "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //               "user_id" => (int)trim($return_ticket_details['operator_id']),
  //               "datetime" => $today,
  //               "type" => 1,
  //               "transaction_type" => 'add',
  //               "amount" => trim($return_ticket_details['ticket_price']),
  //               "account_balance" => trim($customer_account_master_details['account_balance']),
  //               "withdraw_request_id" => 0,
  //               "currency_code" => trim($return_ticket_details['currency_sign']),
  //               "cat_id" => 281
  //             );
  //             $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //           //Add Return Trip Payment to trip operator account + History


  //           //Deduct Return Trip Commission From Operator Account-----------------
  //             $return_trip_commission = round(($return_ticket_details['ticket_price']/100) * $gonagoo_commission_percentage['gonagoo_commission'],2);

  //             $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             $account_balance = trim($customer_account_master_details['account_balance']) - trim($return_trip_commission);
  //             $this->api_bus->update_payment_in_customer_master($customer_account_master_details['account_id'], trim($return_ticket_details['operator_id']), $account_balance);
  //             $customer_account_master_details = $this->api_bus->customer_account_master_details(trim($return_ticket_details['operator_id']), trim($return_ticket_details['currency_sign']));
  //             $update_data_account_history = array(
  //               "account_id" => (int)$customer_account_master_details['account_id'],
  //               "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //               "user_id" => (int)trim($return_ticket_details['operator_id']),
  //               "datetime" => $today,
  //               "type" => 0,
  //               "transaction_type" => 'ticket_commission',
  //               "amount" => trim($return_trip_commission),
  //               "account_balance" => trim($customer_account_master_details['account_balance']),
  //               "withdraw_request_id" => 0,
  //               "currency_code" => trim($return_ticket_details['currency_sign']),
  //               "cat_id" => 281
  //             );
  //             $this->api_bus->insert_payment_in_account_history($update_data_account_history);
  //           //Deduct Return Trip Commission From Operator Account-----------------
            
  //           //Add Return Trip Commission To Gonagoo Account-----------------------
  //             if($this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']))) {
  //               $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
  //             } else {
  //               $insert_data_gonagoo_master = array(
  //                 "gonagoo_balance" => 0,
  //                 "update_datetime" => $today,
  //                 "operation_lock" => 1,
  //                 "currency_code" => trim($return_ticket_details['currency_sign']),
  //               );
  //               $this->api_bus->insert_gonagoo_master_record($insert_data_gonagoo_master);
  //               $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));
  //             }

  //             $gonagoo_balance = trim($gonagoo_master_details['gonagoo_balance']) + trim($return_trip_commission);
  //             $this->api_bus->update_payment_in_gonagoo_master($gonagoo_master_details['gonagoo_id'], $gonagoo_balance);
  //             $gonagoo_master_details = $this->api_bus->gonagoo_master_details(trim($return_ticket_details['currency_sign']));

  //             $update_data_gonagoo_history = array(
  //               "gonagoo_id" => (int)$gonagoo_master_details['gonagoo_id'],
  //               "order_id" => (int)trim($return_ticket_details['ticket_id']),
  //               "user_id" => (int)trim($return_ticket_details['operator_id']),
  //               "type" => 1,
  //               "transaction_type" => 'ticket_commission',
  //               "amount" => trim($return_trip_commission),
  //               "datetime" => $today,
  //               "gonagoo_balance" => trim($gonagoo_master_details['gonagoo_balance']),
  //               "transfer_account_type" => trim($payment_method),
  //               "transfer_account_number" => trim($transfer_account_number),
  //               "bank_name" => trim($bank_name),
  //               "account_holder_name" => trim($account_holder_name),
  //               "iban" => trim($iban),
  //               "email_address" => trim($email_address),
  //               "mobile_number" => trim($mobile_number),
  //               "transaction_id" => trim($transaction_id),
  //               "currency_code" => trim($return_ticket_details['currency_sign']),
  //               "country_id" => trim($return_trip_operator_detail['country_id']),
  //               "cat_id" => trim($return_ticket_details['cat_id']),
  //             );
  //             $this->api_bus->insert_payment_in_gonagoo_history($update_data_gonagoo_history);
  //           //Add Return Trip Commission To Gonagoo Account-----------------------

  //           /* Return ticket commission invoice--------------------------------- */
  //             if( trim($return_trip_operator_detail['firstname']) == 'NULL' || trim($return_trip_operator_detail['lastname'] == 'NULL') ) {
  //               $parts = explode("@", trim($return_trip_operator_detail['email1']));
  //               $username = $parts[0];
  //               $operator_name = $username;
  //             } else {
  //               $operator_name = trim($return_trip_operator_detail['firstname']) . " " . trim($return_trip_operator_detail['lastname']);
  //             }

  //             $phpinvoice = 'resources/fpdf-master/phpinvoice.php';
  //             require_once($phpinvoice);
  //             //Language configuration for invoice
  //             $lang = $this->input->post('lang', TRUE);
  //             if($lang == 'sp') { $invoice_lang = "spanishApi_lang"; }
  //             else if($lang == 'fr') { $invoice_lang = "frenchApi_lang"; }
  //             else { $invoice_lang = "englishApi_lang"; }
  //             //Invoice Configuration
  //             $invoice = new phpinvoice('A4',trim($return_ticket_details['currency_sign']),$invoice_lang);
  //             $invoice->setLogo("resources/fpdf-master/invoice-header.png");
  //             $invoice->setColor("#000");
  //             $invoice->setType("");
  //             $invoice->setReference($return_ticket_details['ticket_id']);
  //             $invoice->setDate(date('M dS ,Y',time()));

  //             $operator_country = $this->api_bus->get_country_details(trim($return_trip_operator_detail['country_id']));
  //             $operator_state = $this->api_bus->get_state_details(trim($return_trip_operator_detail['state_id']));
  //             $operator_city = $this->api_bus->get_city_details(trim($return_trip_operator_detail['city_id']));

  //             $gonagoo_address = $this->api_bus->get_gonagoo_address(trim($return_trip_operator_detail['country_id']));
  //             $gonagoo_country = $this->api_bus->get_country_details(trim($gonagoo_address['country_id']));
  //             $gonagoo_city = $this->api_bus->get_city_details(trim($gonagoo_address['city_id']));

  //             $invoice->setTo(array($gonagoo_address['company_name'],$gonagoo_city['city_name'],$gonagoo_address['no_street_name'],$gonagoo_country['country_name'],$gonagoo_address['email']));
              
  //             $invoice->setFrom(array($operator_name,trim($operator_city['city_name']),trim($operator_state['state_name']),trim($operator_country['country_name']),trim($return_trip_operator_detail['email1'])));

  //             $eol = PHP_EOL;
  //             /* Adding Items in table */
  //             $order_for = $this->lang->line('Ticket ID: ').$return_ticket_details['ticket_id']. ' - ' .$this->lang->line('Commission');
  //             $rate = round(trim($return_trip_commission),2);
  //             $total = $rate;
  //             $payment_datetime = substr($today, 0, 10);
  //             //set items
  //             $invoice->addItem("","$payment_datetime       $order_for","","","1",$rate,$total);

  //             /* Add totals */
  //             $invoice->addTotal($this->lang->line('sub_total'),$total);
  //             $invoice->addTotal($this->lang->line('taxes'),'0');
  //             $invoice->addTotal($this->lang->line('invoice_total'),$total,true);
  //             /* Set badge */ 
  //             $invoice->addBadge($this->lang->line('Commission Paid'));
  //             /* Add title */
  //             $invoice->addTitle($this->lang->line('tnc'));
  //             /* Add Paragraph */
  //             $invoice->addParagraph($gonagoo_address['terms']);
  //             /* Add title */
  //             $invoice->addTitle($this->lang->line('payment_dtls'));
  //             /* Add Paragraph */
  //             $invoice->addParagraph($gonagoo_address['payment_details']);
  //             /* Add title */
  //             $invoice->addTitleFooter($this->lang->line('thank_you_order'));
  //             /* Add Paragraph */
  //             $invoice->addParagraphFooter($this->lang->line('get_in_touch') ."\r\n". $gonagoo_address['company_name'] ."\r\n". $gonagoo_address['phone'] ."\r\n". $gonagoo_address['email']);
  //             /* Set footer note */
  //             $invoice->setFooternote($gonagoo_address['company_name']);
  //             /* Render */
  //             $invoice->render($return_ticket_details['ticket_id'].'_return_trip_commission.pdf','F'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */
  //             //Update File path
  //             $pdf_name = $return_ticket_details['ticket_id'].'_return_trip_commission.pdf';
  //             //Move file to upload folder
  //             rename($pdf_name, "resources/ticket-invoices/".$pdf_name);

  //             //Update Order Invoice
  //             $update_data = array(
  //               "commission_invoice_url" => "ticket-invoices/".$pdf_name,
  //             );
  //             $this->api_bus->update_booking_details($update_data, $return_ticket_details['ticket_id']);
  //           /* Return ticket commission invoice--------------------------------- */
            
  //           /* -----------------------Email Invoice to operator-------------------- */
  //             $emailBody = $this->lang->line('Commission Invoice');
  //             $gonagooAddress = $gonagoo_address['company_name'];
  //             $emailSubject = $this->lang->line('Commission Invoice');
  //             $emailAddress = $return_trip_operator_detail['email1'];
  //             $fileToAttach = "resources/ticket-invoices/$pdf_name";
  //             $fileName = $pdf_name;
  //             $this->api_sms->send_email($emailBody, $gonagooAddress, $emailSubject, $emailAddress, $fileToAttach, $fileName);
  //           /* -----------------------Email Invoice to operator-------------------- */
  //         }
  //       //-----------------------Return Trip Payment Calculation-----------------
  //     /*************************** Payment Section ********************************/
  //     //unset all session data
  //     $this->session->unset_userdata('notif_token');
  //     $this->session->unset_userdata('ticket_id');
  //     $this->session->unset_userdata('ticket_payment_id');
  //     $this->session->unset_userdata('amount');
  //     $this->session->unset_userdata('pay_token');
  //     $this->session->set_flashdata('success', $this->lang->line('Payment successfully completed. Your e-ticket is sent to your email.'));
  //   } else {
  //     //unset all session data
  //     $this->session->unset_userdata('notif_token');
  //     $this->session->unset_userdata('ticket_id');
  //     $this->session->unset_userdata('ticket_payment_id');
  //     $this->session->unset_userdata('amount');
  //     $this->session->unset_userdata('pay_token');
  //     $this->session->set_flashdata('error', $this->lang->line('payment_failed'));
  //   }
  //   redirect(base_url('ticket-payment-confirmation').'/'.$ticket_id,'refresh');
  // }
  // public function ticket_payment_confirmation()
  // {
  //   $ticket_id = (int)$this->uri->segment(2);
  //   $this->load->view('front_ticket_booking_payment_confirmation' ,compact('ticket_id'));
  //   $this->load->view('front_footer_view');
  // }
  // public function laundry_index()
  // {
  //   $this->api_laundry->delete_cloth_coun_temp_by_session_id($_SESSION['__ci_last_regenerate']);
  //   $laundry_providers = $this->api_laundry->get_laundry_provider_profile_list();
  //   $promocodes = $this->api->get_promocodes(9);
  //   $this->load->view('front_laundry_page', compact('promocodes','laundry_providers'));
  //   $this->load->view('front_footer_view');
  // }
  // public function get_laundry_sub_category_append()
  // {
  //   $cat_id = $this->input->post('cat_id');
  //   $sub_cat = $this->api_laundry->get_laundry_sub_category_list($cat_id);
  //   $counter = 1;

  //   foreach ($sub_cat as $cat){
  //     $charg = array(
  //       'cat_id' => $cat['cat_id'],
  //       'sub_cat_id' => $cat['sub_cat_id'],
  //       'country_id' => $this->cust['country_id'], 
  //     );
  //     $charges = $this->api_laundry->get_laundry_cherge($charg);
  //     $sub_cat_details = $this->api_laundry->get_sub_category_details((int)$cat['sub_cat_id']);
  //     $sub_cat_img = ($sub_cat_details['icon_url'] != 'NULL')?base_url($sub_cat_details['icon_url']):base_url('resources/no-image.jpg');

  //     echo '&nbsp;&nbsp;<a>'.
  //           '<div style="min-width:180px; max-height: 180px" class="classCategory">'.
  //             '<div class="panel panel-success" id="type_cat'.$cat['cat_id'].$counter.'">'.
  //               '<div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px;">'.
  //                 '<img src="'.$sub_cat_img.'" style="max-height: 50px; width: auto;">'.
  //                 '<h4 style="margin:0px; font-size:16px;" class="text-success">'.$cat['sub_cat_name'].'</h4>'.
  //                 '<button id="min'.$cat['cat_id'].$counter.'" class="btn btn-warning btn-sm">-</button>&nbsp;&nbsp;<label id="lable'.$cat['cat_id'].$counter.'" count="0" cat_id="'.$cat['cat_id'].'" sub_cat_id="'.$cat['sub_cat_id'].'" session_id="'.$_SESSION['__ci_last_regenerate'].'">0</label>&nbsp;&nbsp;<button class="btn btn-success btn-sm" id="plus'.$cat['cat_id'].$counter.'">+</button>'.
  //               '<label id="price_lable'.$cat['cat_id'].$counter.'" count="'.$charges["charge_amount"].'" currency_sign="'.$charges["currency_sign"].'">'.$charges["charge_amount"].'&nbsp;'.$charges["currency_sign"].'</label>'.
  //               '</div>
  //               </div>
  //             </div>
  //           </a>&nbsp;&nbsp;'; 
  //     echo '<script>
  //             $("#plus'.$cat['cat_id'].$counter.'").on("click", function(e) {
  //               e.preventDefault();
  //               var lable = $("#lable'.$cat['cat_id'].$counter.'");
  //               var price_lable = $("#price_lable'.$cat['cat_id'].$counter.'");
  //               var price = price_lable.attr("count");
  //               var currency_sign = price_lable.attr("currency_sign");
  //               var count = lable.attr("count");
  //               var session_id = lable.attr("session_id");
  //               var cat_id = lable.attr("cat_id");
  //               var sub_cat_id = lable.attr("sub_cat_id");
  //               var neww = parseInt(count)+1;
                
  //               if(cat_id == 1) {
  //                 var men_cnt = parseInt($("#type_category_man_count").text());
  //                 if(men_cnt >= 0) {
  //                   var men_total_cnt = men_cnt + 1;
  //                   $("#type_category_man_count").text(""+men_total_cnt+"");
  //                 } else {
  //                   $("#type_category_man_count").text(""+men_cnt+"");
  //                 }
  //               }
  //               if(cat_id == 2) {
  //                 var women_cnt = parseInt($("#type_category_woman_count").text());
  //                 var women_total_cnt = women_cnt + 1;
  //                 $("#type_category_woman_count").text(""+women_total_cnt+"");
  //               }
  //               if(cat_id == 3) {
  //                 var child_cnt = parseInt($("#type_category_child_count").text());
  //                 var child_total_cnt = child_cnt + 1;
  //                 $("#type_category_child_count").text(""+child_total_cnt+"");
  //               }
  //               if(cat_id == 4) {
  //                 var other_cnt = parseInt($("#type_category_other_count").text());
  //                 var other_total_cnt = other_cnt + 1;
  //                 $("#type_category_other_count").text(""+other_total_cnt+"");
  //               }

                
  //               lable.attr("count", neww );
  //               $("#lable'.$cat['cat_id'].$counter.'").text(neww);
  //               $("#type_cat'.$cat['cat_id'].$counter.'").addClass("active");
                
  //               $.ajax({
  //                 type: "POST",
  //                 url: "'.base_url('add-temp-cloth-count').'", 
  //                 data: {cat_id:cat_id,sub_cat_id:sub_cat_id,session_id:session_id},
  //                 success: function(res){
  //                   console.log(res);
  //                 }
  //               });

  //             });

  //             $("#min'.$cat['cat_id'].$counter.'").on("click", function(e) {
  //               e.preventDefault();
  //               var lable = $("#lable'.$cat['cat_id'].$counter.'");
  //               var count = lable.attr("count");
  //               var session_id = lable.attr("session_id");
  //               var cat_id = lable.attr("cat_id");
  //               var sub_cat_id = lable.attr("sub_cat_id");
  //               if(parseInt(count)==0) { 
  //                 var neww = 0;
  //               } else {
  //                 var neww = parseInt(count)-1; 
  //               }

  //               if(cat_id == 1) {
  //                 var men_cnt = parseInt($("#type_category_man_count").text());
  //                 if(men_cnt >= 1 && count > 0) {
  //                   var men_total_cnt = men_cnt - 1;
  //                   $("#type_category_man_count").text(""+men_total_cnt+"");
  //                 } else {
  //                   $("#type_category_man_count").text(""+men_cnt+"");
  //                 }
  //               }
  //               if(cat_id == 2) {
  //                 var women_cnt = parseInt($("#type_category_woman_count").text());
  //                 if(women_cnt >= 1 && count > 0) {
  //                   var women_total_cnt = women_cnt - 1;
  //                   $("#type_category_woman_count").text(""+women_total_cnt+"");
  //                 } else {
  //                   $("#type_category_woman_count").text(""+women_cnt+"");
  //                 }
  //               }
  //               if(cat_id == 3) {
  //                 var child_cnt = parseInt($("#type_category_child_count").text());
  //                 if(child_cnt >= 1 && count > 0) {
  //                   var child_total_cnt = child_cnt - 1;
  //                   $("#type_category_child_count").text(""+child_total_cnt+"");
  //                 } else {
  //                   $("#type_category_child_count").text(""+child_cnt+"");
  //                 }
  //               }
  //               if(cat_id == 4) {
  //                 var other_cnt = parseInt($("#type_category_other_count").text());
  //                 if(other_cnt >= 1 && count > 0) {
  //                   var other_total_cnt = other_cnt - 1;
  //                   $("#type_category_other_count").text(""+other_total_cnt+"");
  //                 } else {
  //                   $("#type_category_other_count").text(""+other_cnt+"");
  //                 }
  //               }
                
  //               if(neww==0)
  //               {$("#type_cat'.$cat['cat_id'].$counter.'").removeClass("active");}
  //               lable.attr("count", neww );
  //               $("#lable'.$cat['cat_id'].$counter.'").text(neww);

  //               $.ajax({
  //                 type: "POST",
  //                 url: "'.base_url('subs-temp-cloth-count').'", 
  //                 data: {cat_id:cat_id,sub_cat_id:sub_cat_id,session_id:session_id},
  //                 success: function(res){
  //                   console.log(res);
  //                 }
  //               });
  //             });
  //           </script>';
  //     $counter++;
  //   }
  // }
  // public function add_temp_cloth_count()
  // {
  //   //echo json_encode($_POST);
  //   $sub = $this->api_laundry->get_laundry_sub_category($_POST['sub_cat_id']);
  //   $insert = array(
  //     'session_id' => $_POST['session_id'],
  //     'cat_id' => $_POST['cat_id'],
  //     'sub_cat_id' => $_POST['sub_cat_id'],
  //     'weight' => $sub['weight'],
  //     'height' => $sub['height'],
  //     'width' => $sub['width'],
  //     'length' => $sub['length'],
  //     'count' => 1,
  //     );
  //   if($res = $this->api_laundry->check_cloth_coun_temp_by_session_id($insert)) {
  //     //echo json_encode($res); die();
  //     $insert2 = array(
  //     'session_id' => $_POST['session_id'],
  //     'cat_id' => $_POST['cat_id'],
  //     'sub_cat_id' => $_POST['sub_cat_id'],
  //     'weight' => $res['weight']+$sub['weight'],
  //     'height' => $res['height']+$sub['height'],
  //     'count' => $res['count']+1,
  //     );
  //     $this->api_laundry->update_cloth_coun_temp_by_session_id($insert2);
  //   } else { $this->api_laundry->add_cloth_coun_temp($insert); }
  // }
  // public function subs_temp_cloth_count()
  // {
  //   //echo json_encode($_POST);
  //   $sub = $this->api_laundry->get_laundry_sub_category($_POST['sub_cat_id']);
  //   $insert = array(
  //     'session_id' => $_POST['session_id'],
  //     'cat_id' => $_POST['cat_id'],
  //     'sub_cat_id' => $_POST['sub_cat_id'],
  //     'count' => 1,
  //   );
  //   if($res = $this->api_laundry->check_cloth_coun_temp_by_session_id($insert)){
  //     $insert2 = array(
  //       'session_id' => $_POST['session_id'],
  //       'cat_id' => $_POST['cat_id'],
  //       'sub_cat_id' => $_POST['sub_cat_id'],
  //       'weight' => $res['weight']-$sub['weight'],
  //       'height' => $res['height']-$sub['height'],
  //       'count' => $res['count']-1,
  //     );
  //     if($res['count']-1 == 0) { $this->api_laundry->update_cloth_coun_temp_by_session_id($insert2,1);
  //     } else { $this->api_laundry->update_cloth_coun_temp_by_session_id($insert2); }
  //   }
  // } 
  // public function laundry_providers()
  // {
  //   //echo json_encode($_REQUEST); die();
  //   $customer_lat_long = $_GET['customer_lat_long'];
  //   if(!isset($customer_lat_long) || $customer_lat_long == '') {
  //     $this->session->set_flashdata('error', $this->lang->line('We could not find your location, please enable your browser location.'));
  //     redirect('laundry','refresh');
  //   } else {
  //     $lat_long_array = explode(',', $customer_lat_long); //die();
  //     $cust_lat = $lat_long_array[0];
  //     $cust_long = $lat_long_array[1];
  //     $session_id = trim($_GET['session_id']);
  //     $details = $this->api_laundry->get_cloth_details_by_session_id($session_id);
  //     $provider = $this->api_laundry->get_laundry_provider_profile_list();

  //     $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($cust_lat).','.trim($cust_long).'&sensor=false&key=AIzaSyBSaP8CCJqjgZ8viUxwpMOdswi6Y04Ch0k';
  //     $json = @file_get_contents($url);
  //     $data = json_decode($json);
  //     //echo json_encode($data); 

  //     if(!$country_name = $data->results[(sizeof($data->results)-1)]->address_components[0]->long_name) {
  //       $country_name = 'France';
  //       $country_id = $this->api_laundry->get_country_id_by_name($country_name); 
  //     } else {
  //       $country_id = $this->api_laundry->get_country_id_by_name($country_name);
  //     }

  //     $this->load->view('front_laundry_providers_page', compact('details','provider','country_id','cust_lat','cust_long','session_id','customer_lat_long'));
  //     $this->load->view('front_footer_view');
  //   }
  // }
  // public function laundry_booking_details()
  // {
  //   //echo json_encode($_GET); die();
  //   $selected_provider_id = $this->input->get('selected_provider_id');
  //   $session_id = $this->input->get('session_id');
  //   $customer_lat_long = $this->input->get('customer_lat_long');
  //   $cat_id = $this->input->get('cat_id');

  //   $this->session->set_userdata(array('cat_id' => $cat_id , 'session_id' => $session_id , 'selected_provider_id' => $selected_provider_id , 'customer_lat_long' => $customer_lat_long));

  //   $details = $this->api_laundry->get_cloth_details_by_session_id($session_id);
  //   $provider = $this->api_laundry->get_laundry_provider_profile($selected_provider_id);

  //   $this->load->helper('captcha');
  //   $values = array(
  //     'word' => '',
  //     'word_length' => 8,
  //     'img_path' => 'resources/captcha/',
  //     'img_url' => base_url() .'resources/captcha/',
  //     'font_path' => base_url() . 'resources/captcha/ostrich-regular.ttf',
  //     'img_width' => '150',
  //     'img_height' => 45,
  //     'expiration' => 7200
  //   );
  //   $data = create_captcha($values);        
  //   //echo json_encode($data); die();
  //   $this->session->set_userdata('captcha_code', $data['word']);
    
  //   $countries = $this->user->get_countries();
  //   $this->load->view('front_laundry_booking_view' ,compact('customer_lat_long' , 'data' ,'countries' , 'details' , 'selected_provider_id' , 'session_id' , 'cat_id' , 'provider'));
  //   $this->load->view('front_footer_view');
  // }
  // public function gas_index()
  // {
  //   redirect('page-not-found','refresh');
  //   $this->load->view('front_gas_page');
  //   $this->load->view('front_footer_view');
  // }

  public function services_index()
  {
    $m4_categories = $this->api->get_category_types();
    $service_providers = $this->api->get_service_providers_list();
    $promocodes = $this->api->get_promocodes(281);
    $this->load->view('front_services_page', compact('promocodes','m4_categories','service_providers'));
    $this->load->view('front_footer_view');
  }

  public function available_offers() {
    $countries = $this->user->get_countries();
    $category_types = $this->api->get_service_providers_list();

    $filter = $this->input->get('filter', TRUE);
    if(!isset($filter) && $filter == '') { 
      $filter = 'basic';
      $country_id = 0;
      $state_id = 0;
      $city_id = 0;
      $sort_by = 'NULL';
      $cat_type_id = 0;
      $cat_id = 0;
      $delivered_in = 0;
      $start_price = 0;
      $end_price = 0;
      $latest = 'NULL';
      $active_status = 'active';
      $running_status = 'resumed';

      $search_data = array(
        "cust_id" => 0,
        "country_id" => trim($country_id),
        "state_id" => trim($state_id),
        "city_id" => trim($city_id),
        "sort_by" => trim($sort_by),
        "cat_type_id" => trim($cat_type_id),
        "cat_id" => trim($cat_id),
        "delivered_in" => trim($delivered_in),
        "start_price" => trim($start_price),
        "end_price" => trim($end_price),
        "latest" => trim($latest),
        "active_status" => trim($active_status),
        "running_status" => trim($running_status),
        "device" => 'web',
        "last_id" => 0,
        "offer_pause_resume" => 'all',
      );

      $offers = $this->api->get_service_provider_offers_filtered($search_data);
  
      $this->load->view('available_offers', compact('offers','filter','countries','category_types'));
    } else {
      $country_id = $this->input->get('country_id', TRUE); $country_id = (int)$country_id;
      if(!isset($country_id) && $filter == '') { $country_id = 0; }
      $state_id = $this->input->get('state_id', TRUE); $state_id = (int)$state_id;
      if(!isset($state_id) && $filter == '') { $state_id = 0; }
      $city_id = $this->input->get('city_id', TRUE); $city_id = (int)$city_id;
      if(!isset($city_id) && $filter == '') { $city_id = 0; }
      $sort_by = $this->input->get('sort_by', TRUE);
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
      if(!isset($cat_type_id) && $filter == '') { $cat_type_id = 0; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id) && $filter == '') { $cat_id = 0; }
      $delivered_in = $this->input->get('delivered_in', TRUE); $delivered_in = (int)$delivered_in;
      if(!isset($delivered_in) && $filter == '') { $delivered_in = 0; }
      $start_price = $this->input->get('start_price', TRUE); $start_price = (int)$start_price;
      if(!isset($start_price) && $filter == '') { $start_price = 0; }
      $end_price = $this->input->get('end_price', TRUE); $end_price = (int)$end_price;
      if(!isset($end_price) && $filter == '') { $end_price = 0; }
      $latest = $this->input->get('latest', TRUE);

      $active_status = 'active';
      $running_status = 'resumed';

      if(isset($country_id) && $country_id > 0) { $states = $this->api->get_country_states($country_id); } else { $states = array(); }
      if(isset($city_id) && $city_id > 0) { $cities = $this->api->get_state_cities($state_id); } else { $cities = array(); }       
      if(isset($cat_type_id) && $cat_type_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }       

      $search_data = array(
        "cust_id" => 0,
        "country_id" => trim($country_id),
        "state_id" => trim($state_id),
        "city_id" => trim($city_id),
        "sort_by" => trim($sort_by),
        "cat_type_id" => trim($cat_type_id),
        "cat_id" => trim($cat_id),
        "delivered_in" => trim($delivered_in),
        "start_price" => trim($start_price),
        "end_price" => trim($end_price),
        "latest" => trim($latest),
        "active_status" => trim($active_status),
        "running_status" => trim($running_status),
        "device" => 'web',
        "last_id" => 0,
        "offer_pause_resume" => 'all',
      );

      $offers = $this->api->get_service_provider_offers_filtered($search_data);
  
      $this->load->view('available_offers', compact('offers','filter','countries','category_types','country_id','state_id','city_id','sort_by','cat_type_id','cat_id','delivered_in','start_price','end_price','latest','active_status','running_status','states','cities','categories'));
    }
    $this->load->view('front_footer_view');
  }



  public function offers_details() {
    //echo json_encode($_POST); die();
    if( !is_null($this->input->post('offer_id')) ) { $offer_id = $this->input->post('offer_id'); }
    else if(!is_null($this->uri->segment(3))) { $offer_id = $this->uri->segment(3); }
    else { redirect(base_url('front_site/available_offers'),'refresh'); }
    //echo $offer_id; die(); 
    $offer_details = $this->api->get_service_provider_offer_details($offer_id);
    $offer_reviews = $this->api->get_offer_reviews((int)$offer_id);
    //echo json_encode($offer_reviews); die();

    /* $favorite_offers = $this->api->get_favorite_offers($this->cust['cust_id']);
    $favorite_offer_ids = '';
    foreach ($favorite_offers as $offer) {
      $favorite_offer_ids .= $offer['offer_id'].',';
    }
    //rtrim($favorite_offer_ids,',');
    $favorite_offer_ids = substr(trim($favorite_offer_ids), 0, -1);
    $favorite_offer_ids = explode(',', $favorite_offer_ids); */
    //echo json_encode($favorite_offer_ids); die();

    $search_data = array(
      "country_id" => $offer_details['country_id'],
      "cat_type_id" => $offer_details['cat_type_id'],
      "cat_id" => $offer_details['cat_id'],
    );
    $advance_payment = $this->api->get_country_cat_advance_payment_details($search_data);

    //update offer favorite count
    /*       if($this->cust['cust_id'] != $offer_details['cust_id']) {
      $update_data = array( "view_count" => ($offer_details['view_count'] + 1) );
      $this->api->update_offer_details($offer_id, $update_data);
    } */
     $this->load->view('front_header_view');
    $this->load->view('offer_detail_view', compact('offer_details','advance_payment','offer_reviews','favorite_offer_ids'));
    $this->load->view('front_footer_view');
  } 
  public function buy_offer()
  {
    $offer_id = $this->input->get('offer_id'); $offer_id = (int)$offer_id;
    $offer_details = $this->api->get_service_provider_offer_details($offer_id);
    $cust_id = $offer_details['cust_id'];
    $customer_details = $this->api->get_user_details($cust_id);
    //print_r($customer_details); exit(0);
    $this->load->view('front_offer_booking_view',compact('offer_details','customer_details'));
    $this->load->view('front_footer_view');
  }

  public function terms_conditions()
  {
    $this->load->view('terms_conditions');
    $this->load->view('front_footer_view');
  }
  public function about_us()
  {
    $this->load->view('about_us');
    $this->load->view('front_footer_view');
  }
  public function careers()
  {
    $this->load->view('careers');
    $this->load->view('front_footer_view');
  }
  public function send_resume()
  {
    //echo json_encode($_FILES); die();
    $applicant_name = $this->input->post("applicant_name");
    $today = date('Y-m-d H:i:s');

    if( !empty($_FILES["files"]["tmp_name"]) ) {
      $config['upload_path']    = 'resources/careers/';
      $config['allowed_types'] = "pdf|doc|docx";
      $config['file_ext_tolower'] = true;   
      $config['encrypt_name']   = true; 
      $this->load->library('upload', $config); 
      $this->upload->initialize($config); 

      if($this->upload->do_upload('files')) { 
        $uploads    = $this->upload->data(); 
        $resume_url =  $config['upload_path'].$uploads["file_name"]; 
        $filename = $uploads["file_name"]; 
        $add_data = array(
          'applicant_name' => trim($applicant_name), 
          'resume_url' => trim($resume_url), 
          'cre_datetime' => $today,
        );

        if( $id = $this->api->register_career_enquiry($add_data)) {
          /* -----------------Email Invoice to Gonagoo Admin-------------------------- */
            $eol = PHP_EOL;
            $messageBody = $eol . $this->lang->line('Career Application') . $eol;
            $file = $resume_url;
            $file_size = filesize($file);
            $handle = fopen($file, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
            $uid = md5(uniqid(time()));
            $name = basename($file);
            //$eol = PHP_EOL;
            $from_mail = $this->config->item('from_email');
            $replyto = $this->config->item('contact_gonagoo');
            // Basic headers
            $header = "From: ".$this->lang->line('Gonagoo career')." <".$from_mail.">".$eol;
            $header .= "Reply-To: ".$replyto.$eol;
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";
            // Put everything else in $message
            $message = "--".$uid.$eol;
            $message .= "Content-Type: text/html; charset=ISO-8859-1".$eol;
            $message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
            $message .= $messageBody.$eol;
            $message .= "--".$uid.$eol;
            $message .= "Content-Type: application/pdf; name=\"".$filename."\"".$eol;
            $message .= "Content-Transfer-Encoding: base64".$eol;
            $message .= "Content-Disposition: attachment; filename=\"".$filename."\"".$eol;
            $message .= $content.$eol;
            $message .= "--".$uid."--";
            mail($replyto, $messageBody, $message, $header);
          /* -----------------------Email Invoice to Gonagoo Admin-------------------- */
          $this->session->set_flashdata('success', $this->lang->line('Resume submitted successfully, we will get back to you shortly.'));
        } else { $this->session->set_flashdata('error', $this->lang->line('Unable to send resume. Please try it again!')); }
      } else { $this->session->set_flashdata('error', $this->lang->line('File type not supported!')); }
    } else { $this->session->set_flashdata('error', $this->lang->line('Please select resume!')); }
    redirect('careers');
  }
  public function page_not_found()
  {
    $this->load->view('page_not_found');
    $this->load->view('front_footer_view');
  }
  public function login_old()
  {
    $this->load->view('front_login_view');
    $this->load->view('front_footer_view');
  }
  public function login()
  {
    //echo json_encode($_REQUEST); die();
    if (isset($session_set_value['remember_me']) && $session_set_value['remember_me'] == "1") {
      $cust = $this->cust;  $acc_type  = $cust['acc_type'];
      if($acc_type == 'both' || $acc_type == 'seller' ) { redirect(base_url('user-panel/open-bookings')); } 
      else if( $acc_type == 'buyer') { redirect(base_url('user-panel/landing')); }
      // redirect('user-panel/dashboard');
    } else {
      //Courier And Transport------------------------------------------------------
        //echo json_encode($_SESSION); die();
        if(isset($_SESSION['cat_id']) && ($_SESSION['cat_id'] == 6 ||$_SESSION['cat_id'] == 7) ) { // courier search
          //uset previous data
          $this->session->unset_userdata('cat_id');
          $this->session->unset_userdata('vehicle');
          $this->session->unset_userdata('pickupdate');
          $this->session->unset_userdata('pickuptime');
          $this->session->unset_userdata('c_quantity');
          $this->session->unset_userdata('dimension_id');
          $this->session->unset_userdata('deliverdate');
          $this->session->unset_userdata('delivertime');
          $this->session->unset_userdata('c_weight');
          $this->session->unset_userdata('image_url');
          $this->session->unset_userdata('c_width');
          $this->session->unset_userdata('c_height');
          $this->session->unset_userdata('c_length');
        }
        if(isset($_GET['cat_id']) && ($_GET['cat_id'] == 7 || $_GET['cat_id'] == 6)) {
          //set current data
          if($dimension_details = $this->api->get_standard_dimension_details($_GET['dimension_id'], $_GET['cat_id'], 'earth')) {
            
            //echo json_encode($dimension_details); die();
            $dimension_id = $_GET['dimension_id'];
            $c_width = $dimension_details['width'];
            $c_height = $dimension_details['height'];
            $c_length = $dimension_details['length']; 
            $image_url = $dimension_details['image_url']; 
          } else {
            $dimension_id = 0;
            $c_width = 0;
            $c_height = 0;
            $c_length = 0; 
            $image_url = 'NULL'; 
          }

          $this->session->set_userdata(array('cat_id' => $_GET['cat_id'], 'vehicle' => $_GET['vehicle'], 'pickupdate' => $_GET['pickupdate'], 'pickuptime' => $_GET['pickuptime'], 'c_quantity' => $_GET['c_quantity'], 'dimension_id' => $dimension_id, 'c_width' => $c_width, 'c_height' => $c_height, 'c_length' => $c_length, 'image_url' => $image_url, 'deliverdate' => $_GET['deliverdate'], 'delivertime' => $_GET['delivertime'], 'c_weight' => $_GET['c_weight']) );
        }
      //Courier And Transport------------------------------------------------------

      //Ticket Booking-------------------------------------------------------------
        if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 281) {
          $this->session->unset_userdata('cat_id');
          $this->session->unset_userdata('return_date');
          $this->session->unset_userdata('no_of_seat');
          $this->session->unset_userdata('trip_source');
          $this->session->unset_userdata('trip_destination');
          $this->session->unset_userdata('journey_date');
          $this->session->unset_userdata('ownward_trip');
          $this->session->unset_userdata('return_trip');
          }
        if(isset($_GET['cat_id']) && $_GET['cat_id'] == 281) {
          if(!isset($_GET['return_trip'])) $return_trip = ''; else $return_trip = $_GET['return_trip'];
          $this->session->set_userdata(array('cat_id' => $_GET['cat_id'], 'return_date' => $_GET['return_date'], 'no_of_seat' => $_GET['no_of_seat'], 'trip_source' => $_GET['trip_source'], 'trip_destination' => $_GET['trip_destination'], 'journey_date' => $_GET['journey_date'], 'ownward_trip' => $_GET['ownward_trip'], 'return_trip' => $return_trip ) );
        }
      //Ticket Booking-------------------------------------------------------------

      //Laundry Booking------------------------------------------------------------
        if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) {
          $this->session->unset_userdata('cat_id');
          $this->session->unset_userdata('selected_provider_id');
          $this->session->unset_userdata('session_id');
        }
        if(isset($_GET['cat_id']) && $_GET['cat_id'] == 9) {
          $this->session->set_userdata(array('cat_id' => $_GET['cat_id']));
          $this->session->set_userdata(array('selected_provider_id' => $_GET['selected_provider_id']));
          $this->session->set_userdata(array('session_id' => $_GET['session_id']));
        }
      //Laundry Booking------------------------------------------------------------
      //echo json_encode($_SESSION); die();
        $this->load->helper('captcha');

        $values = array(
            'word' => '',
            'word_length' => 8,
            'img_path' => 'resources/captcha/',
            'img_url' => base_url() .'resources/captcha/',
            'font_path' => base_url() . 'resources/captcha/ostrich-regular.ttf',
            'img_width' => '150',
            'img_height' => 45,
            'expiration' => 7200
        );
        $data = create_captcha($values);        
        //echo json_encode($data); die();
        $this->session->set_userdata('captcha_code', $data['word']);
      
        $countries = $this->user->get_countries();

        $this->load->view('front_login_view',compact('countries','data'));
        $this->load->view('front_footer_view');
    }
  }
  public function signup()
  {
    $this->load->helper('captcha');

    $values = array(
      'word' => '',
      'word_length' => 8,
      'img_path' => 'resources/captcha/',
      'img_url' => base_url() .'resources/captcha/',
      'font_path' => base_url() . 'resources/captcha/ostrich-regular.ttf',
      'img_width' => '150',
      'img_height' => 45,
      'expiration' => 7200
    );
    $data = create_captcha($values);        
    //echo json_encode($data); die();
    $this->session->set_userdata('captcha_code', $data['word']);
    
    $countries = $this->user->get_countries();
    $this->load->view('front_signup_view',compact('countries','data'));
    $this->load->view('front_footer_view');
  }
  public function search()
  {
    $cat_name = $this->input->post('cat_id');
    $location = $this->input->post('location');
    $orders = $this->api->get_open_booking_list_front($cat_name, $location);
    $this->load->view('front_header_view');
    $this->load->view('front_search_view', compact('orders'));
    $this->load->view('front_footer_view');
  }
  public function courier_detail()
  {
    $this->load->view('front_courier_detail_view');
    $this->load->view('front_footer_view');
  }
  public function signup_customer()
  {
    //echo json_encode($this->input->post()); die();
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $country_id = $this->input->post('country_id');
    $mobile_no = $this->input->post('mobile_no'); if(substr($mobile_no, 0, 1) == 0) { $mobile_no = ltrim($mobile_no, 0); }     
    $login_type = $this->input->post('login_type');
    $avatar_url = $this->input->post('avatar_url');
    $cover_url = $this->input->post('cover_url');
    $firstname = $this->input->post('firstname');
    $lastname = $this->input->post('lastname');
    $gender = $this->input->post('gender');
    $register_as = $this->input->post('register_as');    // 1: individual, 0: company ( business )
    $buyer = $this->input->post('buyer');
    $seller = $this->input->post('seller');
    $both = $this->input->post('both');
    $company_name = $this->input->post('company_name');
    $country_code = $this->input->post('country_code');

    $timezone = $this->input->post('timezone');
    $arr = array('user_timezone' => $timezone,'default_timezone' => ini_get('date.timezone'));                        
    if($timezone){ $this->session->set_userdata($arr); }

    $today_date = date('Y-m-d H:i:s');
    $insert_data = array(); 

    if($seller == 1) { $acc_type = "seller"; $is_deliverer = 0; }
    else if( $both == 1 ) { $acc_type = "both"; $is_deliverer = 0; }
    else{ $acc_type = "buyer"; $is_deliverer = 0; }

    if(strtolower($login_type) == "linkedin" ) {
      $email = trim($password).'@linkedin.com';     
      $social_type = "LI"; $social_flag = 1; 
      $password = 'li__'.$password.'@';
    }
    else if ( strtolower($login_type) == "facebook" ) {
      $email = trim($password).'@facebook.com'; 
      $social_type = "FB"; $social_flag = 1; 
      $password = 'fb__'.$password.'@';
    }
    else { $social_type = "NULL"; $social_flag = 0;}    

    $avatar_url = !empty($avatar_url) ? trim($avatar_url) : "NULL";
    $cover_url = !empty($cover_url) ? trim($cover_url) : "NULL";
    
    $firstname = !empty($firstname) ? trim($firstname) : "NULL";
    $lastname = !empty($lastname) ? trim($lastname) : "NULL";
    $gender = !empty($gender) ? ((trim($gender)=="male")?"m":"f") : "m";

    $activation_hash = sha1(mt_rand(10000,99999).time().trim($email));
    $OTP = mt_rand(100000,999999);

    $user_insert_data = array(
      "usertype" =>  (int) $register_as,
      "email_id" => trim($email),
      "password" => trim($password),
      "mobile_no" => $mobile_no,
      "firstname" => $firstname, 
      "lastname" => $lastname, 
      "gender" => $gender,
      "country_id" => $country_id,
      "state_id" => 0,
      "city_id" => 0,
      "avatar_url" => trim($avatar_url),
      "cover_url" => trim($cover_url),
      "social_login" => $social_flag,
      "social_type" => strtoupper($social_type),
      "email_verified" => ($social_flag == 1 ) ? 1: 0,
      "activation_hash" => trim($activation_hash),
      "otp" => trim($OTP),
      "acc_type" => $acc_type,
      "is_deliverer" => (int)$is_deliverer,
      "company_name" => ($register_as == 0 ) ? trim($company_name) : "NULL",
      "driver_count" => 0,
    );

    //echo json_encode($user_insert_data); die();

    if(!is_array($this->api->is_emailId_exists(trim($email)))){ 

      if( $cust_id = $this->api->register_consumer($user_insert_data) ){
        // nulled skilled registration 
        $cs_id = $this->api->register_nulled_skilled($cust_id);
        if(substr($mobile_no, 0, 1) == 0) { $mobile_no = ltrim($mobile_no, 0); }
        // send SMS
        $this->api->sendSMS((int)$country_code.$mobile_no, $this->lang->line('register_sms_message'). ' ' . $OTP);
        
          if($social_flag == 0) {
            $subject = $this->lang->line('email_verification_title');
            $message = $this->lang->line('verify_email_text1').$email.$this->lang->line('verify_email_text2');
            $message .="</td></tr><tr><td align='center'><br />";
            $message .="<a href='".$this->config->item('base_url')."verify-email/".$activation_hash."' class='callout'>".$this->lang->line('verify_email_btn')."</a>";            
        
            if($firstname != "NULL" && $lastname != "NULL"){   $username = trim($firstname) .' '.trim($lastname); }
            else{ $username = "User"; }

            // Send activation email to given email id
            $this->api_sms->send_email_text($username, $email, $subject, trim($message));
          }

        $this->session->set_userdata(array('mobile_no' => $mobile_no, "cust_id" => $cust_id, "redirect" => "dashboard-services", "acc_type" => trim($acc_type) ) );
        $this->session->set_flashdata('success', $this->lang->line('register_success'));

        redirect('verification/otp');     

      } else{ $this->session->set_flashdata('error', $this->lang->line('register_failed')); redirect("sign-up"); }
    } else{ $this->session->set_flashdata('error', $this->lang->line('email_exists')); redirect("sign-up"); }
  }
  public function confirm_otp_old()
  {
    $mobile_no = $this->session->userdata('mobile_no');
    $cust_id = $this->cust_id;
    $cust = $this->cust;
    $OTP = $this->input->post('cf_otp');
    $job_id = $this->input->post('job_id');


    $user_details = $this->api->get_user_details((int)$cust_id);

    //echo json_encode($cust); die();

    if($cust['mobile_verified']=="0") {
      if( $cust['cust_otp'] == $OTP ) {       
        if($this->api->update_verified_email_or_otp($cust_id, 'otp')){
          $this->session->set_userdata('is_logged_in',1);
          $this->session->set_userdata('admin_country_id',$user_details['country_id']);

          $this->session->set_flashdata('success', $this->lang->line('otp_success'));

          if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 7) {
            redirect(base_url('user-panel/add-bookings'));
          } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 6) { 
            redirect(base_url('transport/add-bookings'));
          } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 281) { 
            if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') {
              redirect(base_url('user-panel-bus/dashboard-bus'));
            } else {
              redirect(base_url('user-panel-bus/bus-trip-booking-buyer'));
            }
          } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) { 
            if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') {
              redirect(base_url('user-panel-laundry/dashboard-laundry'));
            } else {
              redirect(base_url('user-panel-laundry/customer-create-laundry-bookings'));
            }
          } else {
            if($job_id!="NULL" && $job_id!="" && ($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both')){
              
            redirect(base_url('user-panel-services/dashboard-services-provider'));
          }

          else{
            redirect(base_url('user-panel-services/dashboard-services'));
          }
          }

          // redirect('user-panel/dashboard');
        
        } else { $this->session->set_flashdata('error', $this->lang->line('verification_failed')); redirect('verification/otp'); }    
      } else { $this->session->set_flashdata('error', $this->lang->line('otp_not_matched')); redirect('verification/otp'); }    
    } else { redirect('log-in'); }
  }
  public function confirm_login_old()
  {
    //echo json_encode($_SESSION); die(); 
    //echo json_encode($_REQUEST); die(); 
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $login_type = $this->input->post('login_type');
    $remember_me = $this->input->post('remember_me');
    $today_date = date('Y-m-d H:i:s');
    
    $timezone = $this->input->post('timezone');
    $arr = array('user_timezone' => $timezone,'default_timezone' => ini_get('date.timezone'));                        
    if($timezone){ $this->session->set_userdata($arr); }
    
    if(strtolower($login_type) == "linkedin" ) { $email = trim($password).'@linkedin.com';   $password = 'li__'.$password.'@';  }
    else if ( strtolower($login_type) == "facebook" ) { $email = trim($password).'@facebook.com';  $password = 'fb__'.$password.'@'; }
    else { $password = $password; }   

    if( (trim($email) != "NULL") AND ($cust = $this->api->is_emailId_exists(trim($email))) ){
      if(is_array($cust) AND $cust['cust_status'] == 1 ) {
        if( $cust_id = $this->api->validate_login(trim($email), trim($password)) ){ 
            
            $lang = $this->session->userdata('language');
            if($lang){ $this->config->set_item('language', strtolower($lang)); }
            else{ $lang = "french"; $this->config->set_item('language', 'french'); }
            
            $ip =   getenv('HTTP_CLIENT_IP')?:
                    getenv('HTTP_X_FORWARDED_FOR')?:
                    getenv('HTTP_X_FORWARDED')?:
                    getenv('HTTP_FORWARDED_FOR')?:
                    getenv('HTTP_FORWARDED')?:
                    getenv('REMOTE_ADDR');
                    
            $user_login_data = array(
                "cust_id" =>  (int) $cust_id,
                "cre_datetime" => trim($today_date),
                "mod_datetime" => trim($today_date),
                "hour_spends" => 0,
                "period_status" => 1,
                "user_agent" => "Web",
                "ip_address" => $ip,
            );
            $this->api->register_user_login_history($user_login_data);
            
            $this->session->set_userdata(array('mobile_no' => $cust['mobile1'], "cust_id" => $cust_id, "acc_type" => $cust['acc_type'], "language" => $lang, "last_login_date_time" => $cust['last_login_datetime'], 'admin_country_id' => $cust['country_id']) );
            if($cust['mobile_verified']) {    
              $this->session->set_userdata(array('is_logged_in' =>1, "redirect" => "user-panel-services/dashboard-services") );
  
              if($remember_me == "on") { $this->session->set_userdata('remember_me', true); }           
              $this->api->update_last_login($cust_id); 
              /*$acc_type  = $cust['acc_type'];
              if($acc_type == 'both' || $acc_type == 'seller' ) { redirect(base_url('user-panel/open-bookings')); } 
              else if( $acc_type == 'buyer') { redirect(base_url('user-panel/landing')); }*/


              if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 7) {
                redirect(base_url('user-panel/add-bookings'));
              } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 6) { 
                redirect(base_url('transport/add-bookings'));
              } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 281) { 
                if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') {
                  $this->session->unset_userdata('cat_id');
                  $this->session->unset_userdata('vehicle');
                  $this->session->unset_userdata('pickupdate');
                  $this->session->unset_userdata('pickuptime');
                  $this->session->unset_userdata('c_quantity');
                  $this->session->unset_userdata('dimension_id');
                  $this->session->unset_userdata('deliverdate');
                  $this->session->unset_userdata('delivertime');
                  $this->session->unset_userdata('c_weight');
                  $this->session->unset_userdata('image_url');
                  $this->session->unset_userdata('c_width');
                  $this->session->unset_userdata('c_height');
                  $this->session->unset_userdata('c_length');
                  $this->session->unset_userdata('order_mode');

                  $this->session->unset_userdata('from_address');
                  $this->session->unset_userdata('frm_latitude');
                  $this->session->unset_userdata('frm_longitude');
                  $this->session->unset_userdata('frm_formatted_address');
                  $this->session->unset_userdata('frm_postal_code');
                  $this->session->unset_userdata('frm_country');
                  $this->session->unset_userdata('frm_state');
                  $this->session->unset_userdata('frm_city');

                  $this->session->unset_userdata('to_address');
                  $this->session->unset_userdata('to_latitude');
                  $this->session->unset_userdata('to_longitude');
                  $this->session->unset_userdata('to_formatted_address');
                  $this->session->unset_userdata('to_postal_code');
                  $this->session->unset_userdata('to_country');
                  $this->session->unset_userdata('to_state');
                  $this->session->unset_userdata('to_city');

                  $this->session->unset_userdata('transport_type');
                  if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 6) {
                    $this->session->unset_userdata('loading_free_hours');
                  }
                  redirect(base_url('user-panel-bus/dashboard-bus'));
                } else {
                  redirect(base_url('user-panel-bus/bus-trip-booking-buyer'));
                }
              } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) { 
                if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') {
                  redirect(base_url('user-panel-laundry/dashboard-laundry'));
                } else {
                  redirect(base_url('user-panel-laundry/customer-create-laundry-bookings'));
                }
              } else {
                redirect(base_url('user-panel-services/dashboard-services'));
              }
            
              //redirect('user-panel/dashboard');         
            } else { redirect('verification/otp');  }       
        } else{ $this->session->set_flashdata('error', $this->lang->line('invalid_email')); redirect('log-in'); }
      } else{ $this->session->set_flashdata('error', $this->lang->line('user_inactive')); redirect('log-in'); }
    } else{ $this->session->set_flashdata('error', $this->lang->line('email_not_exists')); redirect('log-in'); }
  }
  public function resend_password()
  {
    $email_id = $this->input->post('email');

    if( $user = $this->api->is_emailId_exists($email_id,true)){

      $subject = $this->lang->line('forgot_password_title');

      $message ="<p class='lead'>You recently requested to forgot password for your Gonagoo account!</p>";
      $message .="</td></tr><tr><td align='center'><br />";
      $message .="<a class='callout'>Your password is: <strong>".$user['password']."</strong></a>";

      if($user['firstname'] != "NULL") {  $username = trim($user['firstname']) . trim($user['lastname']); }
      else { $username = "User"; }

      // Send email
      if( $this->api_sms->send_email_text($username, $email_id, $subject, trim($message))){
        if(substr($user['mobile1'], 0, 1) == 0) { $phone_no = ltrim($user['mobile1'], 0); } else { $phone_no = $user['mobile1']; }
        $country_code = $this->api->get_country_code_by_id($user['country_id']);
        if($user['mobile1']){ $this->api->sendSMS($country_code.$phone_no, $this->lang->line('forgot_password_sms_message').' '.$user['password']); }
        $this->session->set_flashdata('success', $this->lang->line('email_sending_success')); redirect('recovery/password');
      } else{ $this->session->set_flashdata('error', $this->lang->line('email_sending_failed')); redirect('recovery/password'); }
    } else{ $this->session->set_flashdata('error', $this->lang->line('email_not_exists')); redirect('recovery/password'); }
  }
  public function resend_otp()
  {
    if( ($this->session->userdata('mobile_no') == FALSE) AND ($this->session->userdata('cust_id') == FALSE )) { redirect(base_url()); }
    $mobile = $this->session->userdata('mobile_no');
    $cust_id =  $this->session->userdata('cust_id');

    $cust = $this->api->get_consumer_datails($cust_id,true);
    if(is_array($cust) AND $cust['cust_status'] == 1 ) {
      if($cust['mobile1']){ 
        $country_code = $this->api->get_country_code_by_id($cust['country_id']);
        if(substr($cust['mobile1'], 0, 1) == 0) { $phone_no = ltrim($cust['mobile1'], 0); } else { $phone_no = $cust['mobile1']; }
        if($this->api->sendSMS($country_code.$phone_no, $this->lang->line('forgot_otp_sms_message').' '.$cust['cust_otp'])){ 
          $this->session->set_userdata(array('mobile_no' => $cust['mobile1'], "cust_id" => $cust['cust_id'] ) );        
          $this->session->set_flashdata('success', $this->lang->line('resend_otp_success'));
          redirect('verify/otp');
        }else{  $this->session->set_flashdata('error', $this->lang->line('sms_sending_failed'));  }   
      }else{  $this->session->set_flashdata('error', $this->lang->line('user_inactive'));   }
    }else{  redirect('log-in'); }

    redirect('verify/otp');
  }
  public function set_language()
  {
    $lang = $this->input->post('lang');
    $this->session->set_userdata('language',$lang);
    echo 'success';
  }
  public function logout()
  {
    $this->session->unset_userdata('cust_id');
    $this->session->unset_userdata('mobile_no');
    $this->session->unset_userdata('is_logged_in');
    $this->session->unset_userdata('user_timezone');
    $this->session->unset_userdata('default_timezone');
    $this->session->unset_userdata('acc_type');
    $this->session->unset_userdata('admin_country_id');
    $this->session->unset_userdata('auth_type_id');

    if(isset($_SESSION['cat_id']) && ($_SESSION['cat_id']==7 || $_SESSION['cat_id']==6)) { // courier search

      $this->session->unset_userdata('vehicle');
      $this->session->unset_userdata('pickupdate');
      $this->session->unset_userdata('pickuptime');
      $this->session->unset_userdata('c_quantity');
      $this->session->unset_userdata('dimension_id');
      $this->session->unset_userdata('deliverdate');
      $this->session->unset_userdata('delivertime');
      $this->session->unset_userdata('c_weight');
      $this->session->unset_userdata('image_url');
      $this->session->unset_userdata('c_width');
      $this->session->unset_userdata('c_height');
      $this->session->unset_userdata('c_length');
      $this->session->unset_userdata('order_mode');

      $this->session->unset_userdata('from_address');
      $this->session->unset_userdata('frm_latitude');
      $this->session->unset_userdata('frm_longitude');
      $this->session->unset_userdata('frm_formatted_address');
      $this->session->unset_userdata('frm_postal_code');
      $this->session->unset_userdata('frm_country');
      $this->session->unset_userdata('from_country_id');
      $this->session->unset_userdata('frm_state');
      $this->session->unset_userdata('frm_city');

      $this->session->unset_userdata('to_address');
      $this->session->unset_userdata('to_latitude');
      $this->session->unset_userdata('to_longitude');
      $this->session->unset_userdata('to_formatted_address');
      $this->session->unset_userdata('to_postal_code');
      $this->session->unset_userdata('to_country');
      $this->session->unset_userdata('to_country_id');
      $this->session->unset_userdata('to_state');
      $this->session->unset_userdata('to_city');

      $this->session->unset_userdata('transport_type');
      $this->session->unset_userdata('from_country_id');
      $this->session->unset_userdata('to_country_id');

      if($_SESSION['cat_id'] == 6) {
        $this->session->unset_userdata('loading_free_hours');
      }
      $this->session->unset_userdata('cat_id');
    }

    if(isset($_SESSION['cat_id']) && $_SESSION['cat_id']==281) { // courier search
      $this->session->unset_userdata('cat_id');
      $this->session->unset_userdata('trip_source');
      $this->session->unset_userdata('trip_destination');
      $this->session->unset_userdata('journey_date');
      $this->session->unset_userdata('return_date');
      $this->session->unset_userdata('no_of_seat');
      $this->session->unset_userdata('ownward_trip');
      $this->session->unset_userdata('return_trip');
      if(isset($_SESSION['notif_token'])) {
        $this->session->unset_userdata('notif_token');
      }
      if(isset($_SESSION['ticket_id'])) {
        $this->session->unset_userdata('ticket_id');
      }
      if(isset($_SESSION['ticket_payment_id'])) {
        $this->session->unset_userdata('ticket_payment_id');
      }
      if(isset($_SESSION['amount'])) {
        $this->session->unset_userdata('amount');
      }
      if(isset($_SESSION['pay_token'])) {
        $this->session->unset_userdata('pay_token');
      }
    }

    if(isset($_SESSION['cat_id']) && $_SESSION['cat_id']==9) { // courier search
      $this->session->unset_userdata('cat_id');
      $this->session->unset_userdata('selected_provider_id');
    }


    $lang = $this->session->userdata('language');
    $this->session->set_userdata('language', $lang);
    $this->session->unset_userdata('redirect');
    redirect(base_url());
  }
  public function check_email_exists()
  {
    $user_email = $this->input->post('user_email');
    $data = $this->api->is_emailId_exists(trim($user_email));
    if($data) echo 1; 
    else echo 0;
  }
  public function send_android_app_link()
  {
    $country_id = $this->input->post('country_id');
    $country_code = $this->api->get_country_code_by_id($country_id);
    $phone_no = $this->input->post('phone_no');
    if(substr($phone_no, 0, 1) == 0) { $phone_no = ltrim($phone_no, 0); }
    $message = $this->lang->line('Download App: ')." ".$this->config->item('main_app_link');
    $this->api->sendSMS($country_code.$phone_no, $message);
    return '1';
  }
  public function send_ios_app_link()
  {
    //echo json_encode($_POST); die();
    $country_id = $this->input->post('country_id');
    $country_code = $this->api->get_country_code_by_id($country_id);
    $phone_no = $this->input->post('phone_no');
    if(substr($phone_no, 0, 1) == 0) { $phone_no = ltrim($phone_no, 0); }
    $message = $this->lang->line('Download App: ')." https://apple.co/2z5N4jA";
    $this->api->sendSMS($country_code.$phone_no, $message);
    return '1';
  }
  public function test()
  {
    
  }

  //Maaz
  public function projects()
  {
    $countries = $this->api->get_countries();
    $category_types = $this->api->get_service_providers_list();
    $filter = $this->input->get('filter', TRUE);
    $type=0;
    if(!isset($filter) && $filter == '') { 

      $filter = 'basic';
      $country_id = 0;
      $state_id = 0;
      $city_id = 0;
      $sort_by = 'NULL';
      $cat_type_id = 0;
      $cat_id = 0;
      $start_price = 0;
      $end_price = 0;

      $search_data = array(
        "cust_id" => 0,
        "country_id" => trim($country_id),
        "state_id" => trim($state_id),
        "city_id" => trim($city_id),
        "sort_by" => trim($sort_by),
        "cat_type_id" => trim($cat_type_id),
        "cat_id" => trim($cat_id),
        "start_price" => trim($start_price),
        "end_price" => trim($end_price),
        "device" => 'web',
        "last_id" => 0,
        "offer_pause_resume" => 'all',
      );
      $jobs = $this->api->get_job_list_with_filter($search_data);
      $category_types = $this->api->get_category_types();
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }
      $this->load->view('project_list_view', compact('jobs','filter','countries','category_types','type'));
      $this->load->view('front_footer_view');
    }else{
      $country_id = $this->input->get('country_id', TRUE); $country_id = (int)$country_id;
      if(!isset($country_id) && $filter == '') { $country_id = 0; }
      $state_id = $this->input->get('state_id', TRUE); $state_id = (int)$state_id;
      if(!isset($state_id) && $filter == '') { $state_id = 0; }
      $city_id = $this->input->get('city_id', TRUE); $city_id = (int)$city_id;
      if(!isset($city_id) && $filter == '') { $city_id = 0; }
      $sort_by = $this->input->get('sort_by', TRUE);
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
      if(!isset($cat_type_id) && $filter == '') { $cat_type_id = 0; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id) && $filter == '') { $cat_id = 0; }

      if(!isset($delivered_in) && $filter == '') { $delivered_in = 0; }
      $start_price = $this->input->get('start_price', TRUE); $start_price = (int)$start_price;
      if(!isset($start_price) && $filter == '') { $start_price = 0; }
      $end_price = $this->input->get('end_price', TRUE); $end_price = (int)$end_price;

      if(isset($country_id) && $country_id > 0) { $states = $this->api->get_state_by_country_id($country_id); } else { $states = array(); }
      if(isset($city_id) && $city_id > 0) { $cities = $this->api->get_city_by_state_id($state_id); } else { $cities = array(); }       
      if(isset($cat_type_id) && $cat_type_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }       

      $search_data = array(
        "cust_id" => 0,
        "country_id" => trim($country_id),
        "state_id" => trim($state_id),
        "city_id" => trim($city_id),
        "sort_by" => trim($sort_by),
        "cat_type_id" => trim($cat_type_id),
        "cat_id" => trim($cat_id),
        "start_price" => trim($start_price),
        "end_price" => trim($end_price),
        "device" => 'web',
        "last_id" => 0,
      );

      $jobs = $this->api->get_job_list_with_filter($search_data);
      $category_types = $this->api->get_category_types();
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }

      $this->load->view('project_list_view', compact('jobs','filter','countries','category_types','country_id','state_id','city_id','sort_by','cat_type_id','cat_id','start_price','end_price','states','cities','categories','type'));
      $this->load->view('front_footer_view');
    }
  }
  public function get_state_by_country_id()
  {
    $country_id = $this->input->post('country_id');
    echo json_encode($this->api->get_state_by_country_id($country_id));
  }
  public function get_cities_by_state_id()
  {
    $state_id = $this->input->post('state_id');
    // echo json_encode($state_id);die();
    echo json_encode($this->api->get_city_by_state_id($state_id));
  }
  public function get_categories_by_type()
  {
    $type_id = $this->input->post('type_id', TRUE);
    $categories = $this->api->get_categories_by_type($type_id);
    echo json_encode($categories);
  }
  public function view_proposal_detail()
  {
    $countries = $this->user->get_countries();
    $job_id = $this->input->get('job_id');
    $job_details = $this->api->get_customer_job_details($job_id);
    $job_customer_profile = $this->api->get_user_details($job_details['cust_id']);
    $completed_job = $this->api->get_last_job_of_customer($job_customer_profile['cust_id'] , 'completed_count');

    $proposals = $this->api->get_job_proposals_list($job_details['job_id']);
      $datetime1 = new DateTime(date('Y-m-d h:i:s'));
      $datetime2 = new DateTime($job_details['cre_datetime']);
      $difference = $datetime1->diff($datetime2);
      // echo json_encode($difference->d);
      $ending = 30-$difference->d;
       $last_job = $this->api->get_last_job_of_customer($job_customer_profile['cust_id'], 'last');
    // echo json_encode($last_job);die();
      for($i=0; $i<sizeof($proposals); $i++){
       $provider_details = $this->api->get_user_details($proposals[$i]['provider_id']);
       $proposals[$i]['provider_details'] = $provider_details;
      }
      // echo json_encode($job_id); die();
      $job_details['proposal_counts'] = sizeof($proposals);
        
      $this->load->view('view_detail_proposal', compact('job_details','job_customer_profile','completed_job','countries' , 'ending' , 'last_job'));
      $this->load->view('front_footer_view'); 
  }
  public function login_provider()
  {
    $job_id=$this->input->get('job_id');
    $job_details = $this->api->get_customer_job_details($job_id);
    $proposals = $this->api->get_job_proposals_list($job_details['job_id']);
    $job_customer_profile = $this->api->get_user_details($job_details['cust_id']);

    for($i=0; $i<sizeof($proposals); $i++){
     $provider_details = $this->api->get_user_details($proposals[$i]['provider_id']);
     $proposals[$i]['provider_details'] = $provider_details;
    }
    $job_details['proposal_counts'] = sizeof($proposals);

    $this->load->helper('captcha');

    $values = array(
        'word' => '',
        'word_length' => 8,
        'img_path' => 'resources/captcha/',
        'img_url' => base_url() .'resources/captcha/',
        'font_path' => base_url() . 'resources/captcha/ostrich-regular.ttf',
        'img_width' => '150',
        'img_height' => 45,
        'expiration' => 7200
    );
    $data = create_captcha($values);        
    //echo json_encode($data); die();
    $this->session->set_userdata('captcha_code', $data['word']);
  
    $countries = $this->user->get_countries();

    $this->load->view('provider_login_view',compact('countries','data','job_details' , 'job_customer_profile'));
    $this->load->view('front_footer_view');
  }
  public function confirm_login_provider()
  {

    $job_id = $this->input->post('job_id');
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $login_type = $this->input->post('login_type');
    $remember_me = $this->input->post('remember_me');
    $today_date = date('Y-m-d H:i:s');
    
    $timezone = $this->input->post('timezone');
    $arr = array('user_timezone' => $timezone,'default_timezone' => ini_get('date.timezone'));                        
    if($timezone){ $this->session->set_userdata($arr); }
    
    if(strtolower($login_type) == "linkedin" ) { $email = trim($password).'@linkedin.com';   $password = 'li__'.$password.'@';  }
    else if ( strtolower($login_type) == "facebook" ) { $email = trim($password).'@facebook.com';  $password = 'fb__'.$password.'@'; }
    else { $password = $password; }   

    if( (trim($email) != "NULL") AND ($cust = $this->api->is_emailId_exists(trim($email))) ){
      if(is_array($cust) AND $cust['cust_status'] == 1 ) {
        if( $cust_id = $this->api->validate_login(trim($email), trim($password)) ){ 
            // echo json_encode($cust);die();
            $lang = $this->session->userdata('language');
            if($lang){ $this->config->set_item('language', strtolower($lang)); }
            else{ $lang = "french"; $this->config->set_item('language', 'french'); }
            
            $ip =   getenv('HTTP_CLIENT_IP')?:
                    getenv('HTTP_X_FORWARDED_FOR')?:
                    getenv('HTTP_X_FORWARDED')?:
                    getenv('HTTP_FORWARDED_FOR')?:
                    getenv('HTTP_FORWARDED')?:
                    getenv('REMOTE_ADDR');
                    
            $user_login_data = array(
                "cust_id" =>  (int) $cust_id,
                "cre_datetime" => trim($today_date),
                "mod_datetime" => trim($today_date),
                "hour_spends" => 0,
                "period_status" => 1,
                "user_agent" => "Web",
                "ip_address" => $ip,
            );
            $this->api->register_user_login_history($user_login_data);
            
            $this->session->set_userdata(array('mobile_no' => $cust['mobile1'], "cust_id" => $cust_id, "acc_type" => $cust['acc_type'], "language" => $lang, "last_login_date_time" => $cust['last_login_datetime'], 'admin_country_id' => $cust['country_id']) );
            if($cust['mobile_verified']) {    
              $this->session->set_userdata(array('is_logged_in' =>1, "redirect" => "user-panel-services/dashboard-services") );
              // echo json_encode($_SESSION);die();
  
              if($remember_me == "on") { $this->session->set_userdata('remember_me', true); }           
              $this->api->update_last_login($cust_id); 
              /*$acc_type  = $cust['acc_type'];
              if($acc_type == 'both' || $acc_type == 'seller' ) { redirect(base_url('user-panel/open-bookings')); } 
              else if( $acc_type == 'buyer') { redirect(base_url('user-panel/landing')); }*/
              if($cust['acc_type']==="buyer"){

              redirect(base_url('user-panel-services/dashboard-services'));
              }else{ 
                $res=$this->db->query("SELECT * FROM `tbl_smp_job_proposals` WHERE job_id=$job_id AND provider_id=$cust_id")->row();
                if(!$res){

                ?>

                <form name='fr' action="<?=base_url('user-panel-services/send-proposal-view');?>" method='POST'>
                <input type='hidden' name='job_id' value="<?=$job_id;?>">
                </form>
                <script type='text/javascript'>
                document.fr.submit();
                </script>
                <?php
              }else{
                $this->session->set_flashdata('error', $this->lang->line('proposal already send'));
               redirect(base_url('user-panel-services/pending-job-proposals'));

              }
              }
                 
            } else { redirect('verification/otp');  }       
        } else{ $this->session->set_flashdata('error', $this->lang->line('invalid_email')); redirect('login-provider?job_id='.$job_id); }
      } else{ $this->session->set_flashdata('error', $this->lang->line('user_inactive')); redirect('login-provider?job_id='.$job_id); }
    } else{ $this->session->set_flashdata('error', $this->lang->line('email_not_exists')); redirect('login-provider?job_id='.$job_id); }
  }
  public function register_new_provider()
  {
    $job_id = $this->input->post('job_id');
   
    // redirect('verification/otp'); 
    // die();
    // echo json_encode($_POST); die();
    $email = $this->input->post('email');
    $job_id = $this->input->post('job_id');
    $password = $this->input->post('password');
    $country_id = $this->input->post('country_id');
    $mobile_no = $this->input->post('mobile_no'); if(substr($mobile_no, 0, 1) == 0) { $mobile_no = ltrim($mobile_no, 0); }     
    $login_type = $this->input->post('login_type');
    $avatar_url = $this->input->post('avatar_url');
    $cover_url = $this->input->post('cover_url');
    $firstname = $this->input->post('firstname');
    $lastname = $this->input->post('lastname');
    $gender = $this->input->post('gender');
    $register_as = $this->input->post('register_as');    // 1: individual, 0: company ( business )
    $buyer = $this->input->post('buyer');
    $seller = $this->input->post('seller');
    $both = $this->input->post('both');
    $company_name = $this->input->post('company_name');
    $country_code = $this->input->post('country_code');

    $timezone = $this->input->post('timezone');
    $arr = array('user_timezone' => $timezone,'default_timezone' => ini_get('date.timezone'));                        
    if($timezone){ $this->session->set_userdata($arr); }

    $today_date = date('Y-m-d H:i:s');
    $insert_data = array(); 

    if($seller == 1) { $acc_type = "seller"; $is_deliverer = 0; }
    else if( $both == 1 ) { $acc_type = "both"; $is_deliverer = 0; }
    else{ $acc_type = "buyer"; $is_deliverer = 0; }

    if(strtolower($login_type) == "linkedin" ) {
      $email = trim($password).'@linkedin.com';     
      $social_type = "LI"; $social_flag = 1; 
      $password = 'li__'.$password.'@';
    }
    else if ( strtolower($login_type) == "facebook" ) {
      $email = trim($password).'@facebook.com'; 
      $social_type = "FB"; $social_flag = 1; 
      $password = 'fb__'.$password.'@';
    }
    else { $social_type = "NULL"; $social_flag = 0;}    

    $avatar_url = !empty($avatar_url) ? trim($avatar_url) : "NULL";
    $cover_url = !empty($cover_url) ? trim($cover_url) : "NULL";
    
    $firstname = !empty($firstname) ? trim($firstname) : "NULL";
    $lastname = !empty($lastname) ? trim($lastname) : "NULL";
    $gender = !empty($gender) ? ((trim($gender)=="male")?"m":"f") : "m";

    $activation_hash = sha1(mt_rand(10000,99999).time().trim($email));
    $OTP = mt_rand(100000,999999);

    $user_insert_data = array(
      "usertype" =>  (int) $register_as,
      "email_id" => trim($email),
      "password" => trim($password),
      "mobile_no" => $mobile_no,
      "firstname" => $firstname, 
      "lastname" => $lastname, 
      "gender" => $gender,
      "country_id" => $country_id,
      "state_id" => 0,
      "city_id" => 0,
      "avatar_url" => trim($avatar_url),
      "cover_url" => trim($cover_url),
      "social_login" => $social_flag,
      "social_type" => strtoupper($social_type),
      "email_verified" => ($social_flag == 1 ) ? 1: 0,
      "activation_hash" => trim($activation_hash),
      "otp" => trim($OTP),
      "acc_type" => $acc_type,
      "is_deliverer" => (int)$is_deliverer,
      "company_name" => ($register_as == 0 ) ? trim($company_name) : "NULL",
      "driver_count" => 0,
    );

    //echo json_encode($user_insert_data); die();

    if(!is_array($this->api->is_emailId_exists(trim($email)))){ 

      if( $cust_id = $this->api->register_consumer($user_insert_data) ){
        // nulled skilled registration 
        $cs_id = $this->api->register_nulled_skilled($cust_id);
        if(substr($mobile_no, 0, 1) == 0) { $mobile_no = ltrim($mobile_no, 0); }
        // send SMS
        $this->api->sendSMS((int)$country_code.$mobile_no, $this->lang->line('register_sms_message'). ' ' . $OTP);
        
          if($social_flag == 0) {
            $subject = $this->lang->line('email_verification_title');
            $message = $this->lang->line('verify_email_text1').$email.$this->lang->line('verify_email_text2');
            $message .="</td></tr><tr><td align='center'><br />";
            $message .="<a href='".$this->config->item('base_url')."verify-email/".$activation_hash."' class='callout'>".$this->lang->line('verify_email_btn')."</a>";            
        
            if($firstname != "NULL" && $lastname != "NULL"){   $username = trim($firstname) .' '.trim($lastname); }
            else{ $username = "User"; }

            // Send activation email to given email id
            $this->api_sms->send_email_text($username, $email, $subject, trim($message));
          }

        $this->session->set_userdata(array('mobile_no' => $mobile_no, "cust_id" => $cust_id, "redirect" => "dashboard-services", "acc_type" => trim($acc_type) ) );
        $this->session->set_flashdata('success', $this->lang->line('register_success'));
         ?>
      <form name='fr' action="<?=base_url('verification/otp');?>" method='POST'>
                <input type='hidden' name='job_id' value="<?=$job_id;?>">
                </form>
                <script type='text/javascript'>
                document.fr.submit();
                </script>
    <?php

        // redirect('verification/otp');     

      } else{ $this->session->set_flashdata('error', $this->lang->line('register_failed')); redirect("sign-up"); }
    } else{ $this->session->set_flashdata('error', $this->lang->line('email_exists')); redirect("sign-up"); }
  }
  public function troubleshooter()
  {
    $countries = $this->api->get_countries();
    $category_types = $this->api->get_service_providers_list();
    $filter = $this->input->get('filter', TRUE);
    $type=1;
    if(!isset($filter) && $filter == '') { 

      $filter = 'basic';
      $country_id = 0;
      $state_id = 0;
      $city_id = 0;
      $sort_by = 'NULL';
      $cat_type_id = 0;
      $cat_id = 0;
      $start_price = 0;
      $end_price = 0;

      $search_data = array(
        "cust_id" => 0,
        "country_id" => trim($country_id),
        "state_id" => trim($state_id),
        "city_id" => trim($city_id),
        "sort_by" => trim($sort_by),
        "cat_type_id" => trim($cat_type_id),
        "cat_id" => trim($cat_id),
        "start_price" => trim($start_price),
        "end_price" => trim($end_price),
        "device" => 'web',
        "last_id" => 0,
        "offer_pause_resume" => 'all',
      );
      $jobs = $this->api->get_troubleshoot_list_with_filter($search_data);
      $category_types = $this->api->get_category_types();
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }

      $this->load->view('project_list_view', compact('jobs','filter','countries','category_types','type'));
      $this->load->view('front_footer_view');
    }else{

      $country_id = $this->input->get('country_id', TRUE); $country_id = (int)$country_id;
      // $states=$this->api->get_state_by_country_id($country_id);
      if(!isset($country_id) && $filter == '') { $country_id = 0; }
      $state_id = $this->input->get('state_id', TRUE); $state_id = (int)$state_id;
      if(!isset($state_id) && $filter == '') { $state_id = 0; }
      $city_id = $this->input->get('city_id', TRUE); $city_id = (int)$city_id;
      if(!isset($city_id) && $filter == '') { $city_id = 0; }
      $sort_by = $this->input->get('sort_by', TRUE);
      $cat_type_id = $this->input->get('cat_type_id', TRUE); $cat_type_id = (int)$cat_type_id;
      if(!isset($cat_type_id) && $filter == '') { $cat_type_id = 0; }
      $cat_id = $this->input->get('cat_id', TRUE); $cat_id = (int)$cat_id;
      if(!isset($cat_id) && $filter == '') { $cat_id = 0; }

      if(!isset($delivered_in) && $filter == '') { $delivered_in = 0; }
      $start_price = $this->input->get('start_price', TRUE); $start_price = (int)$start_price;
      if(!isset($start_price) && $filter == '') { $start_price = 0; }
      $end_price = $this->input->get('end_price', TRUE); $end_price = (int)$end_price;

      if(isset($country_id) && $country_id > 0) { $states = $this->api->get_state_by_country_id($country_id); } else { $states = array(); }
      if(isset($city_id) && $city_id > 0) { $cities = $this->api->get_city_by_state_id($state_id); } else { $cities = array(); }       
      if(isset($cat_type_id) && $cat_type_id > 0) { $categories = $this->api->get_categories_by_type($cat_type_id); } else { $categories = array(); }       

      $search_data = array(
        "cust_id" => 0,
        "country_id" => trim($country_id),
        "state_id" => trim($state_id),
        "city_id" => trim($city_id),
        "sort_by" => trim($sort_by),
        "cat_type_id" => trim($cat_type_id),
        "cat_id" => trim($cat_id),
        "start_price" => trim($start_price),
        "end_price" => trim($end_price),
        "device" => 'web',
        "last_id" => 0,
      );

      // echo json_encode($search_data);die();
      $jobs = $this->api->get_troubleshoot_list_with_filter($search_data);
      $category_types = $this->api->get_category_types();
      for($i=0; $i<sizeof($jobs); $i++) {
        $proposals = $this->api->get_job_proposals_list($jobs[$i]['job_id']);
        $jobs[$i]['proposal_counts'] = sizeof($proposals);
      }

      // $offers = $this->api->get_service_provider_offers_filtered($search_data);
  
      $this->load->view('project_list_view', compact('jobs','filter','countries','category_types','country_id','state_id','city_id','sort_by','cat_type_id','cat_id','start_price','end_price','states','cities','categories','type'));
      $this->load->view('front_footer_view');


    }
  }
  public function insert_jobs()
  {
    $deliveryData   = $this->session->userdata('deliverdata'); 
    $questions   = $this->session->userdata('questions'); 
    $answers   = $this->session->userdata('answers'); 
    $cust_id = $this->cust_id;
    $customer_details = $this->api->get_user_details($cust_id);
    $currency_details = $this->api->get_country_currency_detail($customer_details['country_id']);
    $country_name = $this->input->post('country', TRUE);
    $country_id = $this->api->get_country_id_by_name($country_name);
    $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']);
    $immdediate = $deliveryData['immediate'];
    $complete_date = $deliveryData['start_date'];

    $today = date('Y-m-d h:i:s');
    $res = array(
      "job_title" =>$deliveryData['job_title'],
      "cust_id" => $cust_id,
      "cat_id" => $deliveryData['cat_id'],
      "sub_cat_id" => $deliveryData['sub_cat_id'],
      "job_desc" => $deliveryData['job_desc'],

      "address" => $deliveryData['address'],
   
      "country_id" => trim($country_id),
      "cre_datetime" => trim($today),
      "cust_name" => $customer_name,
      "cust_contact" => $customer_details['mobile1'],
      "cust_email" => $customer_details['email1'],
      "job_type"=>1,
      "immediate" => $immdediate,
      "location_type"=>"Onsite",
      "visibility"=>"public",
      "currency_id"=>$currency_details['currency_id'],
      "currency_code"=>$currency_details['currency_sign'],
    );
    $res['complete_date']=($immdediate)?date("Y-m-d" , strtotime($complete_date)):"";
    // $insert_data['complete_date']=($immdediate)?date("Y-m-d" , strtotime($complete_date)):"";
    // echo json_encode($insert_data);die();
    if($job_id = $this->api->register_job_details($res)){

       $k=0;
        foreach ($answers as $ans) {
          $data=array(
            'question'=>$questions[$k],
            'answer'=>$answers[$k],
            'job_id'=>$job_id
          );
          if($answers[$k]){
            $this->db->insert("tbl_question_answer" , $data);
          // echo json_encode($data);
          } 
          $k++;
        }
        $job_details = $this->api->get_customer_job_details($job_id);
        $customer_details = $this->api->get_user_details($cust_id);
      
        if(trim($customer_details['firstname']) == 'NULL' || trim($customer_details['lastname'] == 'NULL')) {
          $customer_name = explode("@", trim($customer_details['email1']))[0];
        } else { $customer_name = trim($customer_details['firstname']) . " " . trim($customer_details['lastname']); }

        //Notifications to customers----------------
          $message = $this->lang->line('Hello')." ".$customer_name.", ".$this->lang->line('Your  Troubleshoot job is posted').": ".$job_id;
          //Send Push Notifications
          $api_key = $this->config->item('delivererAppGoogleKey');
          $device_details_customer = $this->api->get_user_device_details($cust_id);
          if(!is_null($device_details_customer)) {
            $arr_customer_fcm_ids = array();
            $arr_customer_apn_ids = array();
            foreach ($device_details_customer as $value) {
              if($value['device_type'] == 1) {  array_push($arr_customer_fcm_ids, $value['reg_id']); } 
              else {  array_push($arr_customer_apn_ids, $value['reg_id']); }
            }
            $msg = array('title' => $this->lang->line('Troubleshoot post'), 'type' => 'troubleshoot post', 'notice_date' => $today, 'desc' => $message);
            $this->api_sms->sendFCM($msg, $arr_customer_fcm_ids, $api_key);
            //APN
            $msg_apn_customer =  array('title' => $this->lang->line('Troubleshoot post'), 'text' => $message);
            if(is_array($arr_customer_apn_ids)) { 
              $arr_customer_apn_ids = implode(',', $arr_customer_apn_ids);
              $this->api_sms->sendPushIOS($msg_apn_customer, $arr_customer_apn_ids, $api_key);
            }
          }
          //SMS Notification
          $country_code = $this->api->get_country_code_by_id((int)$customer_details['country_id']);
          $this->api_sms->sendSMS((int)$country_code.trim($customer_details['mobile1']), $message);

          //Email Notification
          $this->api_sms->send_email_text($customer_name, trim($customer_details['email1']), $this->lang->line('Troubleshoot job post'), trim($message));
        //Notifications to customers----------------

        //Update to workstream master
        $update_data = array(
          'job_id' => (int)$job_id,
          'cust_id' => $cust_id,
          'provider_id' => 0,
          'cre_datetime' => $today,
          'status' => 1,
        );
        if($id = $this->api->check_workstream_master($update_data)){
          $workstream_id = $id;
        }else{
          $workstream_id = $this->api->update_to_workstream_master($update_data);
        }
        $update_data = array(
          'workstream_id' => (int)$workstream_id,
          'job_id' => (int)$job_id,
          'cust_id' => $cust_id,
          'provider_id' => 0,
          'text_msg' => $message,
          'attachment_url' =>  'NULL',
          'cre_datetime' => $today,
          'sender_id' => $cust_id,
          'type' => 'job_started',
          'file_type' => 'text',
          'proposal_id' => 0,
        );
        $this->api->update_to_workstream_details($update_data);
      redirect(base_url('user-panel-services/list-of-freelancers/'.$job_id),'refresh');
    }
  }
  public function post_project_job()
  {
    $category_types = $this->api->get_category_types();
    $countries = $this->api->get_countries();

    $this->load->view('post_project_job',compact('category_types','countries'));
    $this->load->view('front_footer_view');
  }
  public function post_project_login()
  {
    $post_project=$_GET;
    // echo json_encode($post_project);die();

    $this->session->set_userdata('post_project', $post_project);
    $this->load->helper('captcha');

    $values = array(
        'word' => '',
        'word_length' => 8,
        'img_path' => 'resources/captcha/',
        'img_url' => base_url() .'resources/captcha/',
        'font_path' => base_url() . 'resources/captcha/ostrich-regular.ttf',
        'img_width' => '150',
        'img_height' => 45,
        'expiration' => 7200
    );
    $data = create_captcha($values);        
    //echo json_encode($data); die();
    $this->session->set_userdata('captcha_code', $data['word']);
  
    $countries = $this->api->get_countries();

    $this->load->view('troubshoot_login_view',compact('countries','data'));
    $this->load->view('front_footer_view');
  }
  public function post_job_project()
  {
    $res=$_SESSION['post_project']; ?>
      <form name='fr' action="<?=base_url('user-panel-services/post-customer-job');?>" method='POST'>
      <input type='hidden' name='job_title' value="<?=$res['job_title'];?>">
      <input type='hidden' name='description' value="<?=$res['description'];?>">
      <input type='hidden' name='category' value="<?=$res['category'];?>">
      <input type='hidden' name='sub_category' value="<?=$res['sub_category'];?>">
      <input type='hidden' name='work_type' value="<?=$res['work_type'];?>">
      <input type='hidden' name='budget' value="<?=$res['budget'];?>">
      <input type='hidden' name='expiry_date' value="<?=$res['expiry_date'];?>">
      <input type='hidden' name='billing_period' value="<?=$res['billing_period'];?>">
      </form>
      <script type='text/javascript'>
      document.fr.submit();
      </script>
      <?php 
    // echo json_encode($res);
  }
  public function search_freelancer()
  {
    // echo json_encode($_GET);

    $project_5= null;
    $project_50=null;
    $country_id=null;
    $city_id=null;
    $state_id=null;
    $states=null;
    $cities=null;
    $rate_from=null;
    $rate_to=null;

    $country = $this->api->get_countries();
    $cust_id = $this->cust_id;
    if(isset($_GET['filter']) && $_GET['filter']=="advance"){
      
      $country_id = $this->input->get('country_id');
      $state_id = $this->input->get('state_id');
      $city_id = $this->input->get('city_id');
      $project_5 = $this->input->get('project_5');
      $project_50 = $this->input->get('project_50');
      $rate_from = $this->input->get('rate_from');
      $rate_to = $this->input->get('rate_to');
      $states = $this->api->get_state_by_country_id($country_id);
      $cities = $this->api->get_city_by_state_id($state_id);

     $data = array(
        'skill' => "", 
        'fav' => "no",
         'country_id' => $country_id,
        'state_id' => $state_id,
        'city_id' =>  $city_id,
        'rate_from' =>$rate_from,
        'rate_to' => $rate_to,
        'cust_id' => $this->cust_id
      );
      $is_fav = 'no';
    } else{
      $skills=(isset($_GET['search_skill']) && $_GET['search_skill']!="")?$_GET['search_skill']:"";
      $data = array(
        'skill' => $skills, 
        'fav' => "no",
         'country_id' => "",
        'state_id' => "",
        'city_id' => "",
        'rate_from' => "",
        'rate_to' => "",
        'cust_id' => 0
      );
      $is_fav = 'no';
    }      
  
    $freelancers = $this->api->list_of_provider($data);
    // echo  json_encode($freelancers);die();
    for($i=0; $i<sizeof($freelancers); $i++) {
      $job_count = $this->api->get_provider_job_counts($freelancers[$i]['cust_id']);
      $completed = $this->api->get_provider_job_counts($freelancers[$i]['cust_id'] , 'complete');
      // echo json_encode($completed);
      $portfolio = $this->api->get_total_portfolios($freelancers[$i]['cust_id']);
      if($completed['total_job'] != "0"){
       $percent =  (int)$completed['total_job']/(int)$job_count['total_job']*100;
      }else{$percent = 0;}  
      $freelancers[$i]['completed_job'] = (int)$completed['total_job'];
      $freelancers[$i]['total_job'] = (int)$job_count['total_job'];
      $freelancers[$i]['job_percent'] = $percent;
      $freelancers[$i]['portfolio'] = (int)$portfolio;
      $freelancers[$i]['project_5'] = "yes";
      if($project_5 != null ){
        if((int)$project_5 < (int)$freelancers[$i]['completed_job']){
          $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
        }
      }
      if($project_50 != null ){
        if((int)$project_50 < (int)$freelancers[$i]['completed_job']){
          $freelancers[$i]['project_5'] = "yes";}else{$freelancers[$i]['project_5'] = "no";
        }
      }
    }


    $filter = 'basic';
    $this->load->view('list_of_freelancers',compact('country','freelancers','filter','is_fav','country_id','city_id','state_id','states','cities','project_5','project_50','rate_from','rate_to'));
    $this->load->view('front_footer_view');   
  }
  public function provider_profile_detail()
  {
    // echo json_encode($this->uri->segment(2));die();
    if( !is_null($this->input->get('profile_code')) ) { $profile_code = $this->input->get('profile_code'); }
    else if(!is_null($this->uri->segment(2))) { $profile_code = $this->uri->segment(2); }
    else { redirect(base_url(),'refresh'); }
    $operator = $this->api->get_service_provider_profile_code($profile_code);
    $off_days = $this->api->get_service_provider_off_days((int)$operator['cust_id']);
    $user = $this->api->get_user_details((int)$operator['cust_id']);
    $skills = $this->api->get_customer_skills((int)$operator['cust_id']);
    $portfolios = $this->api->get_customers_portfolio((int)$operator['cust_id']);
    $offers = $this->api->get_service_provider_offers((int)$operator['cust_id']);
    $reviews = $this->api->get_service_provier_job_reviews((int)$operator['cust_id']);
    // echo json_encode($skills); die();
    $this->load->view('service_provider_profile_customer_detail', compact('operator', 'off_days', 'user', 'skills', 'portfolios', 'offers', 'reviews'));
    $this->load->view('front_footer_view');   
  }
  public function confirm_otp()
  {
    // echo json_encode($_POST);die();
    $mobile_no = $this->session->userdata('mobile_no');
    $cust_id = $this->cust_id;
    $cust = $this->cust;
    $OTP = $this->input->post('cf_otp');
    $job_id = $this->input->post('job_id');


    $user_details = $this->api->get_user_details((int)$cust_id);

    //echo json_encode($cust); die();

    if($cust['mobile_verified']=="0") {
      if( $cust['cust_otp'] == $OTP ) {       
        if($this->api->update_verified_email_or_otp($cust_id, 'otp')){
          $this->session->set_userdata('is_logged_in',1);
          $this->session->set_userdata('admin_country_id',$user_details['country_id']);

          $this->session->set_flashdata('success', $this->lang->line('otp_success'));

          if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 7) {
            redirect(base_url('user-panel/add-bookings'));
          } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 6) { 
            redirect(base_url('transport/add-bookings'));
          } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 281) { 
            if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') {
              redirect(base_url('user-panel-bus/dashboard-bus'));
            } else {
              redirect(base_url('user-panel-bus/bus-trip-booking-buyer'));
            }
          } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) { 
            if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') {
              redirect(base_url('user-panel-laundry/dashboard-laundry'));
            } else {
              redirect(base_url('user-panel-laundry/customer-create-laundry-bookings'));
            }
          } else {
            if($job_id!="NULL" && $job_id!="" && ($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both')){
              
            redirect(base_url('user-panel-services/dashboard-services-provider'));
          }else{
            if(isset($_SESSION['deliverdata']) && $_SESSION['acc_type'] != 'seller'){
               $this->insert_jobs();

            }else if(isset($_SESSION['post_project']) && $_SESSION['acc_type'] != 'seller'){
              $this->post_job_project();

            } else{
              redirect(base_url('user-panel-services/dashboard-services'));
            }
          }
          }

          // redirect('user-panel/dashboard');
        
        } else { $this->session->set_flashdata('error', $this->lang->line('verification_failed')); redirect('verification/otp'); }    
      } else { $this->session->set_flashdata('error', $this->lang->line('otp_not_matched')); redirect('verification/otp'); }    
    } else { redirect('log-in'); }
  }
  public function confirm_login()
  {

    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $login_type = $this->input->post('login_type');
    $remember_me = $this->input->post('remember_me');
    $today_date = date('Y-m-d H:i:s');
    
    $timezone = $this->input->post('timezone');
    $arr = array('user_timezone' => $timezone,'default_timezone' => ini_get('date.timezone'));                        
    if($timezone){ $this->session->set_userdata($arr); }
    
    if(strtolower($login_type) == "linkedin" ) { $email = trim($password).'@linkedin.com';   $password = 'li__'.$password.'@';  }
    else if ( strtolower($login_type) == "facebook" ) { $email = trim($password).'@facebook.com';  $password = 'fb__'.$password.'@'; }
    else { $password = $password; }   

    if( (trim($email) != "NULL") AND ($cust = $this->api->is_emailId_exists(trim($email))) ){
      if(is_array($cust) AND $cust['cust_status'] == 1 ) {
        if( $cust_id = $this->api->validate_login(trim($email), trim($password)) ){ 
            
            $lang = $this->session->userdata('language');
            if($lang){ $this->config->set_item('language', strtolower($lang)); }
            else{ $lang = "french"; $this->config->set_item('language', 'french'); }
            
            $ip =   getenv('HTTP_CLIENT_IP')?:
                    getenv('HTTP_X_FORWARDED_FOR')?:
                    getenv('HTTP_X_FORWARDED')?:
                    getenv('HTTP_FORWARDED_FOR')?:
                    getenv('HTTP_FORWARDED')?:
                    getenv('REMOTE_ADDR');
                    
            $user_login_data = array(
                "cust_id" =>  (int) $cust_id,
                "cre_datetime" => trim($today_date),
                "mod_datetime" => trim($today_date),
                "hour_spends" => 0,
                "period_status" => 1,
                "user_agent" => "Web",
                "ip_address" => $ip,
            );
            $this->api->register_user_login_history($user_login_data);
            
            $this->session->set_userdata(array('mobile_no' => $cust['mobile1'], "cust_id" => $cust_id, "acc_type" => $cust['acc_type'], "language" => $lang, "last_login_date_time" => $cust['last_login_datetime'], 'admin_country_id' => $cust['country_id']) );
            if($cust['mobile_verified']) {    
              $this->session->set_userdata(array('is_logged_in' =>1, "redirect" => "user-panel-services/dashboard-services") );
  
              if($remember_me == "on") { $this->session->set_userdata('remember_me', true); }           
              $this->api->update_last_login($cust_id); 
              /*$acc_type  = $cust['acc_type'];
              if($acc_type == 'both' || $acc_type == 'seller' ) { redirect(base_url('user-panel/open-bookings')); } 
              else if( $acc_type == 'buyer') { redirect(base_url('user-panel/landing')); }*/


              if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 7) {
                redirect(base_url('user-panel/add-bookings'));
              } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 6) { 
                redirect(base_url('transport/add-bookings'));
              } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 281) { 
                if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') {
                  $this->session->unset_userdata('cat_id');
                  $this->session->unset_userdata('vehicle');
                  $this->session->unset_userdata('pickupdate');
                  $this->session->unset_userdata('pickuptime');
                  $this->session->unset_userdata('c_quantity');
                  $this->session->unset_userdata('dimension_id');
                  $this->session->unset_userdata('deliverdate');
                  $this->session->unset_userdata('delivertime');
                  $this->session->unset_userdata('c_weight');
                  $this->session->unset_userdata('image_url');
                  $this->session->unset_userdata('c_width');
                  $this->session->unset_userdata('c_height');
                  $this->session->unset_userdata('c_length');
                  $this->session->unset_userdata('order_mode');

                  $this->session->unset_userdata('from_address');
                  $this->session->unset_userdata('frm_latitude');
                  $this->session->unset_userdata('frm_longitude');
                  $this->session->unset_userdata('frm_formatted_address');
                  $this->session->unset_userdata('frm_postal_code');
                  $this->session->unset_userdata('frm_country');
                  $this->session->unset_userdata('frm_state');
                  $this->session->unset_userdata('frm_city');

                  $this->session->unset_userdata('to_address');
                  $this->session->unset_userdata('to_latitude');
                  $this->session->unset_userdata('to_longitude');
                  $this->session->unset_userdata('to_formatted_address');
                  $this->session->unset_userdata('to_postal_code');
                  $this->session->unset_userdata('to_country');
                  $this->session->unset_userdata('to_state');
                  $this->session->unset_userdata('to_city');

                  $this->session->unset_userdata('transport_type');
                  if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 6) {
                    $this->session->unset_userdata('loading_free_hours');
                  }
                  redirect(base_url('user-panel-bus/dashboard-bus'));
                } else {
                  redirect(base_url('user-panel-bus/bus-trip-booking-buyer'));
                }
              } else if(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) { 
                if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') {
                  redirect(base_url('user-panel-laundry/dashboard-laundry'));
                } else {
                  redirect(base_url('user-panel-laundry/customer-create-laundry-bookings'));
                }
              } else {
                if(isset($_SESSION['post_project'])){
                  $this->post_job_project();
                }else{

                  redirect(base_url('user-panel-services/dashboard-services'));
                }
              }
            
              //redirect('user-panel/dashboard');         
            } else { redirect('verification/otp');  }       
        } else{ $this->session->set_flashdata('error', $this->lang->line('invalid_email')); redirect('log-in'); }
      } else{ $this->session->set_flashdata('error', $this->lang->line('user_inactive')); redirect('log-in'); }
    } else{ $this->session->set_flashdata('error', $this->lang->line('email_not_exists')); redirect('log-in'); }
  }
  public function post_troubleshoot()
  {
    $category_types = $this->api->get_category_types();
    $countries = $this->api->get_countries();

    $this->load->view('post_troubleshoot_job',compact('category_types','countries'));
    $this->load->view('front_footer_view');
  }

}

/* End of file Front_site.php */
/* Location: ./application/controllers/Front_site.php */