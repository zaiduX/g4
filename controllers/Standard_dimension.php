<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standard_dimension extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('standard_dimension_model', 'standard_dimension');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}

	public function index()
	{
		$standard_dimension = $this->standard_dimension->get_standard_dimension();
		$this->load->view('admin/standard_dimension_list_view', compact('standard_dimension'));			
		$this->load->view('admin/footer_view');		
	}

	public function add()
	{
		$categories = $this->standard_dimension->get_categories();
		$this->load->view('admin/standard_dimension_add_view',compact('categories'));
		$this->load->view('admin/footer_view');
	}
	
	public function register()
	{
		$dimension_type = $this->input->post('dimension_type', TRUE);
 		$width = $this->input->post('width', TRUE);
 		$height = $this->input->post('height', TRUE);
 		$length = $this->input->post('length', TRUE);
 		$on_air = $this->input->post('on_air', TRUE); $on_air = ($on_air == "on") ? "1": "0";
 		$on_sea = $this->input->post('on_sea', TRUE); $on_sea = ($on_sea == "on") ? "1": "0";
 		$on_earth = $this->input->post('on_earth', TRUE); $on_earth = ($on_earth == "on") ? "1": "0";
 		$services = $this->input->post('service_type', TRUE);
 		$dimension_image_url = "NULL";
 		$insert_flag = false;

 		if(!empty($_FILES["dimension_image"]["tmp_name"])) {
			$config['upload_path']    = 'resources/standard-dimension/';                 
  		$config['allowed_types']  = '*';       
  		$config['max_size']       = '0';                          
 			$config['max_width']      = '0';                          
  		$config['max_height']     = '0';                          
  		$config['encrypt_name']   = true; 
  		$this->load->library('upload', $config);
  		$this->upload->initialize($config);

  		if($this->upload->do_upload('dimension_image')) {   
    		$uploads    = $this->upload->data();  
    		$dimension_image_url =  $config['upload_path'].$uploads["file_name"];  
    	} 
		}

		foreach ($services as $id) {
			$category = $this->standard_dimension->get_category_name_by_id($id);
			$cat_name = $category['cat_name'];

	 		$insert_data = array(
	 			"dimension_type" => trim($dimension_type), 
	 			"width" =>  number_format(trim($width),2,'.',''),
	 			"height" =>  number_format(trim($height),2,'.',''),
	 			"length" =>  number_format(trim($length),2,'.',''),
	 			"volume" =>  number_format(($width*$height*$length),2,'.',''),
	 			"image_url" => $dimension_image_url, 
	 			"cre_datetime" =>  date("Y-m-d H:i:s"),
	 			"category_id" => $id,
//	 			"category_name" => $cat_name,
	 			"on_earth" => $on_earth,
	 			"on_air" => $on_air,
	 			"on_sea" => $on_sea,
	 		);
			//echo json_encode($insert_data); die();
			if(! $this->standard_dimension->check_standard_dimension($insert_data)) {	
				if( $this->standard_dimension->register_standard_dimension($insert_data)){ $insert_flag = true;}
			}
 		}
 		if($insert_flag){	$this->session->set_flashdata('success','Dimension successfully added!'); }
 		else{	$this->session->set_flashdata('error','Unable to add / Configration already exists! Try with different configrations...');	}
		redirect('admin/standard-dimensions/add');
	}

	public function edit()
	{	
		if( $id = $this->uri->segment(4) ) { 			
			$dimension = $this->standard_dimension->get_standard_dimension_detail($id);
			$this->load->view('admin/standard_dimension_edit_view', compact('dimension'));
			$this->load->view('admin/footer_view');
		} else { redirect('admin/standard-dimensions'); }
	}

	public function update()
	{
		$dimension_id = $this->input->post('dimension_id', TRUE);
		$dimension_type = $this->input->post('dimension_type', TRUE);
 		$width = $this->input->post('width', TRUE);
 		$height = $this->input->post('height', TRUE);
 		$length = $this->input->post('length', TRUE);
                $on_air = $this->input->post('on_air', TRUE); $on_air = ($on_air == "on") ? "1": "0";
 		$on_sea = $this->input->post('on_sea', TRUE); $on_sea = ($on_sea == "on") ? "1": "0";
 		$on_earth = $this->input->post('on_earth', TRUE); $on_earth = ($on_earth == "on") ? "1": "0";
 		$services = $this->input->post('service_type', TRUE);
 		$old_image_url = $this->input->post('old_image_url', TRUE);

 		if(!empty($_FILES["dimension_image"]["tmp_name"])) {
			$config['upload_path']    = 'resources/standard-dimension/';                 
  		$config['allowed_types']  = '*';       
  		$config['max_size']       = '0';                          
 			$config['max_width']      = '0';                          
  		$config['max_height']     = '0';                          
  		$config['encrypt_name']   = true; 
  		$this->load->library('upload', $config);
  		$this->upload->initialize($config);

  		if($this->upload->do_upload('dimension_image')) {   
    		$uploads    = $this->upload->data();  
    		$dimension_image_url =  $config['upload_path'].$uploads["file_name"];  

    		if($old_image_url != "NULL") { unlink($old_image_url); }

    	} else { $dimension_image_url =  $old_image_url;}
		} else { $dimension_image_url =  $old_image_url; }

 		$update_data = array(
 			"dimension_type" => trim($dimension_type), 
 			"width" =>  number_format(trim($width),2,'.',''),
 			"height" =>  number_format(trim($height),2,'.',''),
 			"length" =>  number_format(trim($length),2,'.',''),
 			"volume" =>  number_format(($width*$height*$length),2,'.',''),
 			"image_url" => $dimension_image_url, 
 			"on_earth" => $on_earth,
 			"on_air" => $on_air,
 			"on_sea" => $on_sea,
 		);

		if(! $this->standard_dimension->check_standard_dimension_for_update($update_data) ) {
			if($this->standard_dimension->update_standard_dimension($update_data, $dimension_id)){
				$this->session->set_flashdata('success','Dimension successfully updated!');
			} else { 	$this->session->set_flashdata('error','Unable to update dimension! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Dimension already exists!');	}

		redirect('admin/standard-dimensions/edit/'.$dimension_id);
	}

	public function delete()
	{
		$dimension_id = $this->input->post('id', TRUE);
		if($this->standard_dimension->delete_standard_dimension($dimension_id)){ echo 'success'; } else { echo "failed";	} 
	}

}

/* End of file Standard_dimension.php */
/* Location: ./application/controllers/Standard_dimension.php */