<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport_vehicles extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('transport_vehicles_model', 'vehicles');
            $this->load->model('standard_dimension_model', 'dimension');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}

	public function index()
	{
		$vehicles = $this->vehicles->get_vehicles();
		$this->load->view('admin/vehicles_list_view', compact('vehicles'));			
		$this->load->view('admin/footer_view');		
	}

	public function add()
	{
	    $categories = $this->dimension->get_categories();
		$this->load->view('admin/vehicles_add_view', compact('categories'));
		$this->load->view('admin/footer_view');
	}
	
	public function register()
	{
		$transport_mode = $this->input->post('transport_mode', TRUE);
 		$vehicle_type = $this->input->post('vehicle_type', TRUE);
 		$cat_id = $this->input->post('cat_id', TRUE);

 		$insert_data = array(
 			"transport_mode" => trim($transport_mode), 
 			"vehicle_type" =>  trim($vehicle_type), 
 			"vehicle_status" => 1, 
 			"cre_datetime" =>  date("Y-m-d H:i:s"),
 			"cat_id" =>  (int)$cat_id,  
 		);

		if(! $this->vehicles->check_vehicles($insert_data) ) {
			if($this->vehicles->register_vehicles($insert_data)){
				$this->session->set_flashdata('success','Vehicle successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new vehicle! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','vehicle already exists!');	}

		redirect('admin/transport-vehicles/add');
	}

	public function edit()
	{	
		if( $id = $this->uri->segment(4) ) { 
			if( $vehicle = $this->vehicles->get_vehicles_detail($id)){ 
			    $categories = $this->dimension->get_categories();
				$this->load->view('admin/vehicles_edit_veiw', compact('vehicle','categories'));
				$this->load->view('admin/footer_view');
			} else { redirect('admin/transport-vehicles'); }
		} else { redirect('admin/transport-vehicles'); }
	}

	public function update()
	{
		$vehicle_id = $this->input->post('vehicle_id', TRUE);
		$vehicle_type = $this->input->post('vehicle_type', TRUE);
		$transport_mode = $this->input->post('transport_mode', TRUE);
 		$cat_id = $this->input->post('cat_id', TRUE);

 		$update_data = array(
 			"transport_mode" => trim($transport_mode), 
 			"vehicle_type" =>  trim($vehicle_type), 
 			"cat_id" =>  (int)$cat_id,  
 		);

		if(! $this->vehicles->check_vehicles($update_data) ) {
			if($this->vehicles->update_vehicles($update_data, $vehicle_id)){
				$this->session->set_flashdata('success','Vehicle successfully updated!');
			} else { 	$this->session->set_flashdata('error','Unable to update vehicle! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','vehicle already exists!');	}

		redirect('admin/transport-vehicles/edit/'.$vehicle_id);
	}

	public function delete()
	{
		$id = $this->input->post('id', TRUE);
		if($this->vehicles->delete_vehicles($id)){ echo 'success'; } else { echo "failed";	} 
	}

	public function define_seat_type()
	{
		$vehical_type_id = $this->uri->segment(4);
		$seats = $this->vehicles->get_vehicles_seat_type_master((int)$vehical_type_id);
		$this->load->view('admin/vehicles_seat_type_list_view', compact('seats','vehical_type_id'));			
		$this->load->view('admin/footer_view');		
	}
	
	public function register_seat_type()
	{	
		//echo json_encode($_POST); die();
		$vehical_type_id = $this->input->post('vehical_type_id', TRUE); $vehical_type_id = (int)$vehical_type_id;
 		$st_name = $this->input->post('st_name', TRUE);
 		$vehicle_details = $this->vehicles->get_vehicles_detail($vehical_type_id);
 		$insert_data = array(
 			"vehical_type_id" => trim($vehical_type_id), 
 			"cat_id" => (int)trim($vehicle_details['cat_id']), 
 			"st_name" =>  trim($st_name), 
 			"st_cre_datetime" =>  date("Y-m-d H:i:s"),
 		);

		//echo json_encode($insert_data); die();
		if($this->vehicles->register_vehicle_seat_type($insert_data)){
			$this->session->set_flashdata('success','Seat type successfully added!');
		} else { 	$this->session->set_flashdata('error','Unable to add seat type! Try again...');	} 

		redirect(base_url("admin/transport-vehicles/define-seat-type/$vehical_type_id"));
	}

	public function delete_seat_type()
	{
		$st_id = $this->input->post('id', TRUE);
		if($this->vehicles->delete_vehicle_seat_type($st_id)){ echo 'success'; } else { echo "failed";	} 
	}

}

/* End of file Transport_vehicles.php */
/* Location: ./application/controllers/Transport_vehicles.php */