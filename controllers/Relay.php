<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relay extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('is_admin_logged_in')){
            $this->load->model('admin_model', 'admin'); 
            $this->load->model('Relay_model', 'relay');     
            $this->load->model('Notification_model', 'notice');
            $this->load->model('api_model_sms', 'api_sms'); 
            $id = $this->session->userdata('userid');
            $user = $this->admin->logged_in_user_details($id);
            $permissions = $this->admin->get_auth_permissions($user['type_id']);
            if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));   
        } else{ redirect('admin');}
    }
    
    public function index()
    {
        if(!$this->session->userdata('is_logged_in')){ $this->load->view('admin/login_view');} else { redirect('admin/dashboard');}
    }

    public function relay_point()
    {
        $auth_type_id = $this->session->userdata('auth_type_id');
          if($auth_type_id > 1) {   $admin_country_id = $this->session->userdata('admin_country_id'); }
          else { $admin_country_id = 0; }   
          $relays = array();
            $relay_points = $this->relay->get_relay_points($admin_country_id);
        foreach($relay_points as $rp => &$r){       
        $relay_points[$rp]['total_orders'] = $this->relay->total_orders_at_relay($r['relay_id']);
        }
                
        //echo json_encode($relay_points); die();
        $this->load->view('admin/relay_point_list_view', compact('relay_points'));          
        $this->load->view('admin/footer_view');     
    }

    public function active_relay_point()
    {
        $cd_id = $this->input->post('id', TRUE);
        if( $this->relay->activate_relay_point( (int) $cd_id) ) {   echo 'success'; }   else {  echo 'failed';  }
    }
    
    public function inactive_relay_point()
    {
        $cd_id = $this->input->post('id', TRUE);
        if( $this->relay->inactivate_relay_point( (int) $cd_id) ) { echo 'success'; }   else {  echo 'failed';  }
    }
    
    public function add_relay_point()
    {
        $countries = $this->relay->get_countries();
        $this->load->view('admin/relay_point_add_view', compact('countries'));
        $this->load->view('admin/footer_view');
    }

    public function get_state_by_country_id()
    {
        $country_id = $this->input->post('country_id');
        echo json_encode($this->relay->get_state_by_country_id($country_id));
    }

    public function get_city_by_state_id()
    {
        $state_id = $this->input->post('state_id');
        echo json_encode($this->relay->get_city_by_state_id($state_id));
    }

    public function relay_email_exists()
    {
        $email = $this->input->post('email', TRUE);
        if( $this->relay->relay_email_exists($email) ) {    echo 'true';    }   else {  echo 'false';   }
    }

    public function register_relay_point()
    {
        //var_dump($_POST); die();
        $first_name = $this->input->post('first_name', TRUE);
        $last_name = $this->input->post('last_name', TRUE);
        $mobile1 = $this->input->post('mobile1', TRUE);
        $this->input->post('mobile2', TRUE) == '' ? $mobile2 = 'NULL' : $mobile2 = $this->input->post('mobile2', TRUE);
        $rfirstname = $this->input->post('rfirstname', TRUE);
        $rlastname = $this->input->post('rlastname', TRUE);
        $rcomapny_name = $this->input->post('rcomapny_name', TRUE);
        $rmobile_no = $this->input->post('rmobile_no', TRUE);
        $rcountry_id = $this->input->post('rcountry_id', TRUE);
        $rstate_id = $this->input->post('rstate_id', TRUE);
        $rcity_id = $this->input->post('rcity_id', TRUE);
        $raddr_line1 = $this->input->post('raddr_line1', TRUE);
        $raddr_line2 = $this->input->post('raddr_line2', TRUE);
        $rstreet_name = $this->input->post('rstreet_name', TRUE);
        $rzipcode = $this->input->post('rzipcode', TRUE);
        $iban = $this->input->post('iban', TRUE);
        $mobile_money_acc1 = $this->input->post('mobile_money_acc1', TRUE);
        $mobile_money_acc2 = $this->input->post('mobile_money_acc2', TRUE);
        $deliver_instructions = $this->input->post('deliver_instructions', TRUE);
        $pickup_instructions = $this->input->post('pickup_instructions', TRUE);
        $relay_commission = $this->input->post('relay_commission', TRUE);
        $wheight = $this->input->post('wheight', TRUE);
        $wwidth = $this->input->post('wwidth', TRUE);
        $name = $this->input->post('name', TRUE);
        $wlength = $this->input->post('wlength', TRUE);
        $max_volume = round($wheight*$wwidth*$wlength, 0);
        $email = $this->input->post('email', TRUE);
        $password = $this->input->post('password', TRUE);
        $latitude = $this->input->post('latitude', TRUE);
        $longitude = $this->input->post('longitude', TRUE);
        $rbusiness_type = $this->input->post('rbusiness_type', TRUE);

        $monday_toggle = $this->input->post('monday_toggle', TRUE); (!isset($monday_toggle)) ? $monday_toggle = 0 : $monday_toggle = 1;
        $monday_open = $this->input->post('monday_open', TRUE); (!isset($monday_open)) ? $monday_open = '00.00' : $monday_open = $this->input->post('monday_open', TRUE);
        $monday_close = $this->input->post('monday_close', TRUE); (!isset($monday_close)) ? $monday_close = '00.00' : $monday_close = $this->input->post('monday_close', TRUE);
        $tuesday_toggle = $this->input->post('tuesday_toggle', TRUE); (!isset($tuesday_toggle)) ? $tuesday_toggle = 0 : $tuesday_toggle = 1;
        $tuesday_open = $this->input->post('tuesday_open', TRUE); (!isset($tuesday_open)) ? $tuesday_open = '00.00' : $tuesday_open = $this->input->post('tuesday_open', TRUE);
        $tuesday_close = $this->input->post('tuesday_close', TRUE); (!isset($tuesday_close)) ? $tuesday_close = '00.00' : $tuesday_close = $this->input->post('tuesday_close', TRUE);
        $wednesday_toggle = $this->input->post('wednesday_toggle', TRUE); (!isset($wednesday_toggle)) ? $wednesday_toggle = 0 : $wednesday_toggle = 1;
        $wednesday_open = $this->input->post('wednesday_open', TRUE); (!isset($wednesday_open)) ? $wednesday_open = '00.00' : $wednesday_open = $this->input->post('wednesday_open', TRUE);
        $wednesday_close = $this->input->post('wednesday_close', TRUE); (!isset($wednesday_close)) ? $wednesday_close = '00.00' : $wednesday_close = $this->input->post('wednesday_close', TRUE);
        $thursday_toggle = $this->input->post('thursday_toggle', TRUE); (!isset($thursday_toggle)) ? $thursday_toggle = 0 : $thursday_toggle = 1;
        $thursday_open = $this->input->post('thursday_open', TRUE); (!isset($thursday_open)) ? $thursday_open = '00.00' : $thursday_open = $this->input->post('thursday_open', TRUE);
        $thursday_close = $this->input->post('thursday_close', TRUE); (!isset($thursday_close)) ? $thursday_close = '00.00' : $thursday_close = $this->input->post('thursday_close', TRUE);
        $friday_toggle = $this->input->post('friday_toggle', TRUE); (!isset($friday_toggle)) ? $friday_toggle = 0 : $friday_toggle = 1;
        $friday_open = $this->input->post('friday_open', TRUE); (!isset($friday_open)) ? $friday_open = '00.00' : $friday_open = $this->input->post('friday_open', TRUE);
        $friday_close = $this->input->post('friday_close', TRUE); (!isset($friday_close)) ? $friday_close = '00.00' : $friday_close = $this->input->post('friday_close', TRUE);
        $saturday_toggle = $this->input->post('saturday_toggle', TRUE); (!isset($saturday_toggle)) ? $saturday_toggle = 0 : $saturday_toggle = 1;
        $saturday_open = $this->input->post('saturday_open', TRUE); (!isset($saturday_open)) ? $saturday_open = '00.00' : $saturday_open = $this->input->post('saturday_open', TRUE);
        $saturday_close = $this->input->post('saturday_close', TRUE); (!isset($saturday_close)) ? $saturday_close = '00.00' : $saturday_close = $this->input->post('saturday_close', TRUE);
        $sunday_toggle = $this->input->post('sunday_toggle', TRUE); (!isset($sunday_toggle)) ? $sunday_toggle = 0 : $sunday_toggle = 1;
        $sunday_open = $this->input->post('sunday_open', TRUE); (!isset($sunday_open)) ? $sunday_open = '00.00' : $sunday_open = $this->input->post('sunday_open', TRUE);
        $sunday_close = $this->input->post('sunday_close', TRUE); (!isset($sunday_close)) ? $sunday_close = '00.00' : $sunday_close = $this->input->post('sunday_close', TRUE);

        /*
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$rzipcode."&sensor=false";
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $details = curl_exec($ch);
        
        $result = json_decode($details,true);
        $latitude=$result['results'][0]['geometry']['location']['lat'];
        */

        if( isset($_FILES["profile_image"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/profile-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('profile_image')){   
                $uploads    = $this->upload->data();  
                $profile_image_url =  $config['upload_path'].$uploads["file_name"];  
            } else { $profile_image_url = "NULL"; }
                
        } else { $profile_image_url = "NULL"; } 

        if( isset($_FILES["rimage_url"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/profile-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('rimage_url')){   
                $uploads    = $this->upload->data();  
                $relay_image_url =  $config['upload_path'].$uploads["file_name"];  
            } else { $relay_image_url = "NULL"; }
                
        } else { $relay_image_url = "NULL"; }   
        
        $insert_data_manager = array(
            "cust_id" => 0,
            "first_name" => trim($first_name),
            "last_name" => trim($last_name),
            "email" => trim($email),
            "password" => trim($password),
            "mobile1" => trim($mobile1),
            "mobile2" => trim($mobile2),
            "cat_ids" => 'NULL',
            "available" => 'NULL',
            "avatar" => $profile_image_url,
            "latitude" => trim($latitude),
            "longitude" => trim($longitude),
            "loc_update_datetime" => 'NULL',
            "cre_datetime" => date('Y-m-d h:i:s'),
            "mod_datetime" => date('Y-m-d h:i:s'),
            "status" => 1,
            "online_status" => 0,
            "last_online_datetime" => date('Y-m-d h:i:s'),
        );
        if($cd_id = $this->relay->register_relay_manager($insert_data_manager)) {
            $insert_data_relay = array(
                "firstname" => trim($rfirstname),
                "lastname" => trim($rlastname),
                "email" => trim($email),
                "mobile_no" => trim($rmobile_no),
                "comapny_name" => trim($rcomapny_name),
                "image_url" => trim($relay_image_url),
                "country_id" => (int)$rcountry_id,
                "state_id" => (int)$rstate_id,
                "city_id" => (int)$rcity_id,
                "zipcode" => trim($rzipcode),
                "iban" => trim($iban),
                "mobile_money_acc1" => trim($mobile_money_acc1),
                "mobile_money_acc2" => trim($mobile_money_acc2),
                "street_name" => trim($rstreet_name),
                "addr_line1" => trim($raddr_line1),
                "addr_line2" => trim($raddr_line2),
                "latitude" => trim($latitude),
                "longitude" => trim($longitude),
                "cre_datetime" => date('Y-m-d h:i:s'),
                "is_active" => 1,
                "wh_count" => 1,
                "status" => 1,
                "relay_mgr_id" => $cd_id,
                "deliver_instructions" => trim($deliver_instructions),
                "pickup_instructions" => trim($pickup_instructions),
                "relay_commission" => trim($relay_commission),

                "monday_toggle" => trim($monday_toggle),
                "tuesday_toggle" => trim($tuesday_toggle),
                "wednesday_toggle" => trim($wednesday_toggle),
                "thursday_toggle" => trim($thursday_toggle),
                "friday_toggle" => trim($friday_toggle),
                "saturday_toggle" => trim($saturday_toggle),
                "sunday_toggle" => trim($sunday_toggle),

                "monday_open" => trim($monday_open),
                "monday_close" => trim($monday_close),
                "tuesday_open" => trim($tuesday_open),
                "tuesday_close" => trim($tuesday_close),
                "wednesday_open" => trim($wednesday_open),
                "wednesday_close" => trim($wednesday_close),
                "thursday_open" => trim($thursday_open),
                "thursday_close" => trim($thursday_close),
                "friday_open" => trim($friday_open),
                "friday_close" => trim($friday_close),
                "saturday_open" => trim($saturday_open),
                "saturday_close" => trim($saturday_close),
                "sunday_open" => trim($sunday_open),
                "sunday_close" => trim($sunday_close),
                "business_type" => trim($rbusiness_type),
            );
            //var_dump($insert_data_relay); die();
            if($relay_id = $this->relay->register_relay_point($insert_data_relay)) {
                $insert_data_warehouse = array(
                "relay_id" => (int)trim($relay_id),
                "height" => trim($wheight),
                "width" => trim($wwidth),
                "length" => trim($wlength),
                "max_volume" => trim($max_volume),
                "name" => trim($name),
                "volume_used" => 0,
                "cre_datetime" => date('Y-m-d h:i:s'),
                "status" => 1,
            );
            if($this->relay->register_relay_warehouse($insert_data_warehouse)) {
                $subject = 'Relay point credentials!';
                $message ="<p class='lead'>Relay point manager credentials.</p>";
                $message .="</td></tr><tr><td align='center'><br />";
                $message .="<a class='callout'>Registered Email Address: <strong>".$email."</strong></a>";
                $message .="<a class='callout'>Password: <strong>".$password."</strong></a>";
                $username = trim($first_name) . trim($last_name);
                $this->api_sms->send_email_text($username, $email, $subject, trim($message));
                $sms_msg = 'Gonagoo credentials: User Name: ' . $email . ' Password: ' . $password;
                $this->relay->sendSMS(trim($mobile1), $sms_msg);
                $this->session->set_flashdata('success','Relay point created successfully, and credentials sent to registered email address!');
        } } } else {    $this->session->set_flashdata('error','Unable to create relay point! Try again...'); }
        redirect('admin/add-relay-point');
    }

    public function edit_relay_point_details()
    {
        $countries = $this->relay->get_countries();
        $cd_id = (int) $this->uri->segment('3');
        $relay_details = $this->relay->get_relay_point_details($cd_id);
        $this->load->view('admin/relay_point_details_edit_view', compact('relay_details','countries'));
        $this->load->view('admin/footer_view');
    }

    public function update_relay_point_details()
    {
        $cd_id = $this->input->post('cd_id', TRUE);
        $rfirstname = $this->input->post('rfirstname', TRUE);
        $rlastname = $this->input->post('rlastname', TRUE);
        $rcomapny_name = $this->input->post('rcomapny_name', TRUE);
        $rmobile_no = $this->input->post('rmobile_no', TRUE);
        $rcountry_id = $this->input->post('rcountry_id', TRUE);
        $rstate_id = $this->input->post('rstate_id', TRUE);
        $hrstate_id = $this->input->post('hrstate_id', TRUE);
        $rstate_id == 0 ? $rstate_id = $hrstate_id : $rstate_id = $this->input->post('rstate_id', TRUE);

        $rcity_id = $this->input->post('rcity_id', TRUE);
        $hrcity_id = $this->input->post('hrcity_id', TRUE);
        $rcity_id == 0 ? $rcity_id = $hrcity_id : $rcity_id = $this->input->post('rcity_id', TRUE);

        $raddr_line1 = $this->input->post('raddr_line1', TRUE);
        $rstreet_name = $this->input->post('rstreet_name', TRUE);
        $rzipcode = $this->input->post('rzipcode', TRUE);
        $raddr_line2 = $this->input->post('raddr_line2', TRUE);
        $latitude = $this->input->post('latitude', TRUE);
        $longitude = $this->input->post('longitude', TRUE);
        $deliver_instructions = $this->input->post('deliver_instructions', TRUE);
        $pickup_instructions = $this->input->post('pickup_instructions', TRUE);
        $iban = $this->input->post('iban', TRUE);
        $mobile_money_acc1 = $this->input->post('mobile_money_acc1', TRUE);
        $mobile_money_acc2 = $this->input->post('mobile_money_acc2', TRUE);
        $relay_commission = $this->input->post('relay_commission', TRUE);
        $rbusiness_type = $this->input->post('rbusiness_type', TRUE);

        $monday_toggle = $this->input->post('monday_toggle', TRUE); (!isset($monday_toggle)) ? $monday_toggle = 0 : $monday_toggle = 1;
        $monday_open = $this->input->post('monday_open', TRUE); (!isset($monday_open)) ? $monday_open = '00.00' : $monday_open = $this->input->post('monday_open', TRUE);
        $monday_close = $this->input->post('monday_close', TRUE); (!isset($monday_close)) ? $monday_close = '00.00' : $monday_close = $this->input->post('monday_close', TRUE);
        $tuesday_toggle = $this->input->post('tuesday_toggle', TRUE); (!isset($tuesday_toggle)) ? $tuesday_toggle = 0 : $tuesday_toggle = 1;
        $tuesday_open = $this->input->post('tuesday_open', TRUE); (!isset($tuesday_open)) ? $tuesday_open = '00.00' : $tuesday_open = $this->input->post('tuesday_open', TRUE);
        $tuesday_close = $this->input->post('tuesday_close', TRUE); (!isset($tuesday_close)) ? $tuesday_close = '00.00' : $tuesday_close = $this->input->post('tuesday_close', TRUE);
        $wednesday_toggle = $this->input->post('wednesday_toggle', TRUE); (!isset($wednesday_toggle)) ? $wednesday_toggle = 0 : $wednesday_toggle = 1;
        $wednesday_open = $this->input->post('wednesday_open', TRUE); (!isset($wednesday_open)) ? $wednesday_open = '00.00' : $wednesday_open = $this->input->post('wednesday_open', TRUE);
        $wednesday_close = $this->input->post('wednesday_close', TRUE); (!isset($wednesday_close)) ? $wednesday_close = '00.00' : $wednesday_close = $this->input->post('wednesday_close', TRUE);
        $thursday_toggle = $this->input->post('thursday_toggle', TRUE); (!isset($thursday_toggle)) ? $thursday_toggle = 0 : $thursday_toggle = 1;
        $thursday_open = $this->input->post('thursday_open', TRUE); (!isset($thursday_open)) ? $thursday_open = '00.00' : $thursday_open = $this->input->post('thursday_open', TRUE);
        $thursday_close = $this->input->post('thursday_close', TRUE); (!isset($thursday_close)) ? $thursday_close = '00.00' : $thursday_close = $this->input->post('thursday_close', TRUE);
        $friday_toggle = $this->input->post('friday_toggle', TRUE); (!isset($friday_toggle)) ? $friday_toggle = 0 : $friday_toggle = 1;
        $friday_open = $this->input->post('friday_open', TRUE); (!isset($friday_open)) ? $friday_open = '00.00' : $friday_open = $this->input->post('friday_open', TRUE);
        $friday_close = $this->input->post('friday_close', TRUE); (!isset($friday_close)) ? $friday_close = '00.00' : $friday_close = $this->input->post('friday_close', TRUE);
        $saturday_toggle = $this->input->post('saturday_toggle', TRUE); (!isset($saturday_toggle)) ? $saturday_toggle = 0 : $saturday_toggle = 1;
        $saturday_open = $this->input->post('saturday_open', TRUE); (!isset($saturday_open)) ? $saturday_open = '00.00' : $saturday_open = $this->input->post('saturday_open', TRUE);
        $saturday_close = $this->input->post('saturday_close', TRUE); (!isset($saturday_close)) ? $saturday_close = '00.00' : $saturday_close = $this->input->post('saturday_close', TRUE);
        $sunday_toggle = $this->input->post('sunday_toggle', TRUE); (!isset($sunday_toggle)) ? $sunday_toggle = 0 : $sunday_toggle = 1;
        $sunday_open = $this->input->post('sunday_open', TRUE); (!isset($sunday_open)) ? $sunday_open = '00.00' : $sunday_open = $this->input->post('sunday_open', TRUE);
        $sunday_close = $this->input->post('sunday_close', TRUE); (!isset($sunday_close)) ? $sunday_close = '00.00' : $sunday_close = $this->input->post('sunday_close', TRUE);

        $update_data = array();
        if( !empty($_FILES["rimage_url"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/profile-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('rimage_url')) { 
                $uploads    = $this->upload->data(); 
                $profile_image_url =  $config['upload_path'].$uploads["file_name"]; 

                $update_data = array(
                    "firstname" => trim($rfirstname),
                    "lastname" => trim($rlastname),
                    "mobile_no" => trim($rmobile_no),
                    "comapny_name" => trim($rcomapny_name),
                    "image_url" => trim($profile_image_url),
                    "country_id" => (int)$rcountry_id,
                    "state_id" => (int)$rstate_id,
                    "city_id" => (int)$rcity_id,
                    "zipcode" => trim($rzipcode),
                    "iban" => trim($iban),
                    "mobile_money_acc1" => trim($mobile_money_acc1),
                    "mobile_money_acc2" => trim($mobile_money_acc2),
                    "street_name" => trim($rstreet_name),
                    "addr_line1" => trim($raddr_line1),
                    "addr_line2" => trim($raddr_line2),
                    "latitude" => trim($latitude),
                    "longitude" => trim($longitude),
                    "deliver_instructions" => trim($deliver_instructions),
                    "pickup_instructions" => trim($pickup_instructions),
                    "relay_commission" => trim($relay_commission),
                    "monday_toggle" => trim($monday_toggle),
	                "tuesday_toggle" => trim($tuesday_toggle),
	                "wednesday_toggle" => trim($wednesday_toggle),
	                "thursday_toggle" => trim($thursday_toggle),
	                "friday_toggle" => trim($friday_toggle),
	                "saturday_toggle" => trim($saturday_toggle),
	                "sunday_toggle" => trim($sunday_toggle),

	                "monday_open" => trim($monday_open),
	                "monday_close" => trim($monday_close),
	                "tuesday_open" => trim($tuesday_open),
	                "tuesday_close" => trim($tuesday_close),
	                "wednesday_open" => trim($wednesday_open),
	                "wednesday_close" => trim($wednesday_close),
	                "thursday_open" => trim($thursday_open),
	                "thursday_close" => trim($thursday_close),
	                "friday_open" => trim($friday_open),
	                "friday_close" => trim($friday_close),
	                "saturday_open" => trim($saturday_open),
	                "saturday_close" => trim($saturday_close),
	                "sunday_open" => trim($sunday_open),
	                "sunday_close" => trim($sunday_close),
	                "business_type" => trim($rbusiness_type),
                );
            }           
        } else { 
            $update_data = array(
                "firstname" => trim($rfirstname),
                "lastname" => trim($rlastname),
                "mobile_no" => trim($rmobile_no),
                "comapny_name" => trim($rcomapny_name),
                "country_id" => (int)$rcountry_id,
                "state_id" => (int)$rstate_id,
                "city_id" => (int)$rcity_id,
                "zipcode" => trim($rzipcode),
                "iban" => trim($iban),
                "mobile_money_acc1" => trim($mobile_money_acc1),
                "mobile_money_acc2" => trim($mobile_money_acc2),
                "street_name" => trim($rstreet_name),
                "addr_line1" => trim($raddr_line1),
                "addr_line2" => trim($raddr_line2),
                "latitude" => trim($latitude),
                "longitude" => trim($longitude),
                "deliver_instructions" => trim($deliver_instructions),
                "pickup_instructions" => trim($pickup_instructions),
                "relay_commission" => trim($relay_commission),
                "monday_toggle" => trim($monday_toggle),
                "tuesday_toggle" => trim($tuesday_toggle),
                "wednesday_toggle" => trim($wednesday_toggle),
                "thursday_toggle" => trim($thursday_toggle),
                "friday_toggle" => trim($friday_toggle),
                "saturday_toggle" => trim($saturday_toggle),
                "sunday_toggle" => trim($sunday_toggle),

                "monday_open" => trim($monday_open),
                "monday_close" => trim($monday_close),
                "tuesday_open" => trim($tuesday_open),
                "tuesday_close" => trim($tuesday_close),
                "wednesday_open" => trim($wednesday_open),
                "wednesday_close" => trim($wednesday_close),
                "thursday_open" => trim($thursday_open),
                "thursday_close" => trim($thursday_close),
                "friday_open" => trim($friday_open),
                "friday_close" => trim($friday_close),
                "saturday_open" => trim($saturday_open),
                "saturday_close" => trim($saturday_close),
                "sunday_open" => trim($sunday_open),
                "sunday_close" => trim($sunday_close),
                "business_type" => trim($rbusiness_type),
            );
        }
        
        if( $this->relay->update_relay_point_details((int) $cd_id, $update_data)){
            $this->session->set_flashdata('success','Relay point details successfully updated!');
        }   else {  $this->session->set_flashdata('error','Unable to update or no change found! Try again...'); }
        
        redirect('admin/edit-relay-point-details/'. $cd_id);
    }

    public function edit_relay_point()
    {
        $countries = $this->relay->get_countries();
        $cd_id = (int) $this->uri->segment('3');
        $manager = $this->relay->get_manager_details($cd_id);
        $this->load->view('admin/relay_point_edit_view', compact('countries','manager'));
        $this->load->view('admin/footer_view');
    }

    public function update_relay_point()
    {
        $cd_id = $this->input->post('cd_id', TRUE);
        $first_name = $this->input->post('first_name', TRUE);
        $last_name = $this->input->post('last_name', TRUE);
        $mobile1 = $this->input->post('mobile1', TRUE);
        $this->input->post('mobile2', TRUE) == '' ? $mobile2 = 'NULL' : $mobile2 = $this->input->post('mobile2', TRUE);
        $this->input->post('password', TRUE) != '' ? $password = $this->input->post('password', TRUE) : $password = $this->input->post('hiddenpassword', TRUE);

        $update_data = array();
        if( !empty($_FILES["profile_image"]["tmp_name"]) ) {
            $config['upload_path']    = 'resources/profile-images/';                 
            $config['allowed_types']  = '*';       
            $config['max_size']       = '0';                          
            $config['max_width']      = '0';                          
            $config['max_height']     = '0';                          
            $config['encrypt_name']   = true; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('profile_image')) {   
                $uploads    = $this->upload->data();  
                $profile_image_url =  $config['upload_path'].$uploads["file_name"];  

                $update_data = array(
                        "first_name" => trim($first_name),
                        "last_name" => trim($last_name),
                        "mobile1" => trim($mobile1),
                        "mobile2" => trim($mobile2),
                        "password" => trim($password),
                        "avatar" => $profile_image_url,
                    );
            }           
        } else { 
            $update_data = array(
                "first_name" => trim($first_name),
                "last_name" => trim($last_name),
                "mobile1" => trim($mobile1),
                "mobile2" => trim($mobile2),
                "password" => trim($password),
            );
        }
        
        if( $this->relay->update_manager((int) $cd_id, $update_data)){
            $this->session->set_flashdata('success','Manager details successfully updated!');
        }   else {  $this->session->set_flashdata('error','Unable to update or no change found! Try again...'); }
        
        redirect('admin/edit-relay-point/'. $cd_id);
    }

}

/* End of file Relay.php */
/* Location: ./application/controllers/Relay.php */