<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skills extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');		
			$this->load->model('skill_model', 'skill');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$permissions = $this->admin->get_auth_permissions($user['type_id']);
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}

	public function index()
	{
		$skills = $this->skill->get_skills();
		$this->load->view('admin/skill_list_view', compact('skills'));			
		$this->load->view('admin/footer_view');		
	}

	public function add_skill()
	{
		$active_category_types = $this->skill->get_active_category_types();
		$this->load->view('admin/skill_add_view', compact('active_category_types'));
		$this->load->view('admin/footer_view');
	}

	public function register_skill()
	{
		$skill_name = $this->input->post('skill_name', TRUE);
 		$cat_id = $this->input->post('cat_id', TRUE);

 		$insert_data = array(
						"skill_name" => trim($skill_name), 
						"cat_id" => (int) $cat_id, 
						"cre_datetime" => date('Y-m-d H:i:s'), 
						"skill_status" => 1 
						);

		if(! $this->skill->check_skill($skill_name, $cat_id) ) {
			if($this->skill->register_skill($insert_data)){
				$this->session->set_flashdata('success','Skill successfully added!');
			} else { 	$this->session->set_flashdata('error','Unable to add new skill! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Skill already exists!');	}
		redirect('admin/add-skill');
	}

	public function delete()
	{
		$skill_id = $this->input->post('id', TRUE);
		if($this->skill->delete_skills($skill_id)){ echo 'success'; } else { echo "failed";	} 
	}

	public function edit_skill()
	{
		if( !is_null($this->input->post('skill_id')) ) { $skill_id = (int) $this->input->post('skill_id'); }
		else if(!is_null($this->uri->segment(3))) { $skill_id = (int)$this->uri->segment(3); }
		else { redirect('admin/skill-master'); }
		$active_category_types = $this->skill->get_active_category_types();
		$skill_details = $this->skill->get_skill_details($skill_id);
		$this->load->view('admin/skill_edit_view', compact('active_category_types', 'skill_details'));
		$this->load->view('admin/footer_view');
	}

	public function update_skill()
	{	
 		$skill_id = $this->input->post('skill_id', TRUE); $skill_id = (int) $skill_id;
		$skill_name = $this->input->post('skill_name', TRUE);
 		$cat_id = $this->input->post('cat_id', TRUE);
		
		if(! $result = $this->skill->check_skill_excluding($skill_id, $skill_name, $cat_id) ) {
			$update_data = array(
						"skill_name" => trim($skill_name), 
						"cat_id" => (int) $cat_id,
						);
			if($this->skill->update_skill($update_data, $skill_id)) {
				$this->session->set_flashdata('success','Skill updated successfully!');
			} else { 	$this->session->set_flashdata('error','Unable to update skill! Try again...');	} 
		} else { 	$this->session->set_flashdata('error','Skill already exists!');	}
		redirect('admin/edit-skill/'.$skill_id);
	}

}

/* End of file Skills.php */
/* Location: ./application/controllers/Skills.php */