<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider_question_master extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('is_admin_logged_in')){
			$this->load->model('admin_model', 'admin');	
			$this->load->model('Provider_question_master_model', 'question_master');		
			$id = $this->session->userdata('userid');
			$user = $this->admin->logged_in_user_details($id);
			$user = $this->admin->logged_in_user_details($user['type_id']);
			$permissions = $this->admin->get_auth_permissions($id);
			
			if(!$this->input->is_ajax_request()) $this->load->view('admin/side_bar_view', compact('user','permissions'));	
		} else{ redirect('admin');}
	}
	public function index() {
		$questions = $this->question_master->get_questions_group_by_cat_id();
		$this->load->view('admin/services/know_provider_question_list_view', compact('questions'));			
		$this->load->view('admin/footer_view');
	}
	public function edit() {
		if( $cat_id = $this->uri->segment(4) ) { 			
			if( $questions = $this->question_master->get_questions_by_cat_id($cat_id) ){
				$this->load->view('admin/services/know_provider_question_edit_view', compact('questions'));
				$this->load->view('admin/footer_view');
			} else { redirect('admin/provider-question-master'); }
		} else { redirect('admin/provider-question-master'); }
	}
	public function update() {
		$question_id = $this->input->post('question_id', TRUE);
		$cat_id = $this->input->post('cat_id', TRUE);
		$question_title = $this->input->post('question_title', TRUE);

 		$update_data = array(
 			"question_title" =>  trim($question_title),
 			"update_datetime" => date("Y-m-d H:i:s")
 		);

		if($this->question_master->update_question($update_data, $question_id)){
			$this->session->set_flashdata('success','Question successfully updated!');
		} else { 	$this->session->set_flashdata('error','Unable to update question! Try again...');	} 

		$questions = $this->question_master->get_questions_by_cat_id($cat_id);
		if( sizeof($questions) > 0 ) {
			redirect('admin/provider-question-master/edit/'.$cat_id);
		} else { redirect('admin/provider-question-master'); }
	}
	public function delete() {
		$question_id = $this->input->post('question_id', TRUE);
		$cat_id = $this->input->post('cat_id', TRUE);

		if($this->question_master->delete_question($question_id)){
			$this->session->set_flashdata('success','Question successfully delete!');
		} else { 	$this->session->set_flashdata('error','Unable to delete question! Try again...');	} 

		$questions = $this->question_master->get_questions_by_cat_id($cat_id);
		if( sizeof($questions) > 0 ) {
			redirect('admin/provider-question-master/edit/'.$cat_id);
		} else { redirect('admin/provider-question-master'); }
	}
	public function add() {
		$category_type = $this->question_master->get_category_type();
		$this->load->view('admin/services/know_provider_question_add_view', compact('category_type'));
		$this->load->view('admin/footer_view');
	}
	public function get_categories_by_type() {
		$type_id = $this->input->post('type_id', TRUE);
		$parent_cats = $this->question_master->get_categories_by_type($type_id);
		echo json_encode($parent_cats);
	}
	public function register() {
		//echo json_encode($_POST); die();
		$question_title = $this->input->post('question_title', TRUE);
 		$cat_type_id = $this->input->post('cat_type_id', TRUE);
 		$cat_id = $this->input->post('cat_id', TRUE);
 		
 		for ($i=0; $i < sizeof($cat_id); $i++) {
	 		$insert_data = array(
	 			"question_title" => trim($question_title), 
	 			"cat_type_id" =>  trim($cat_type_id), 
	 			"cat_id" => trim($cat_id[$i]), 
	 			"cre_datetime" =>  date("Y-m-d H:i:s"), 
	 			"status" => 1,
	 			"update_datetime" =>  date("Y-m-d H:i:s"), 
	 		);
 			$this->question_master->register_question($insert_data);
 		}
 		$this->session->set_flashdata('success','Question successfully added!');
 		redirect('admin/provider-question-master/add');
	}
}

/* End of file Provider_question_master.php */
/* Location: ./application/controllers/Provider_question_master.php */