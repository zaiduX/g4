<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Create CAPTCHA
 *
 * @access  public
 * @param   array   array of data for the CAPTCHA
 * @param   string  path to create the image in
 * @param   string  URL to the CAPTCHA image folder
 * @param   string  server path to font
 * @return  string
 */
if ( ! function_exists('create_captcha'))
{
    function create_captcha($data = '', $img_path = '', $img_url = '', $font_path = '')
    {
        $defaults = array('word' => '', 'img_path' => '', 'img_url' => '', 'img_width' => '150', 'img_height' => '30', 'font_path' => '', 'expiration' => 7200, 'font_size' => 5);

        foreach ($defaults as $key => $val)
        {
            if ( ! is_array($data))
            {
                if ( ! isset($$key) OR $$key == '')
                {
                    $$key = $val;
                }
            }
            else
            {
                $$key = ( ! isset($data[$key])) ? $val : $data[$key];
            }
        }

        if ($img_path == '' OR $img_url == '')
        {
            return FALSE;
        }

        if ( ! @is_dir($img_path))
        {
            return FALSE;
        }

        if ( ! is_writable($img_path))
        {
            return FALSE;
        }

        if ( ! extension_loaded('gd'))
        {
            return FALSE;
        }

        // -----------------------------------
        // Remove old images
        // -----------------------------------

        list($usec, $sec) = explode(" ", microtime());
        $now = ((float)$usec + (float)$sec);

        $current_dir = @opendir($img_path);

        while ($filename = @readdir($current_dir))
        {
            if ($filename != "." and $filename != ".." and $filename != "index.html")
            {
                $name = str_replace(".jpg", "", $filename);

                if (($name + $expiration) < $now)
                {
                    @unlink($img_path.$filename);
                }
            }
        }

        @closedir($current_dir);

        // -----------------------------------
        // Do we have a "word" yet?
        // -----------------------------------

       if ($word == '')
       {
            $pool = 'abcdefghjkABCDEFGHJK2346mnprtuvwxyz789MNPRTUVWXYZ';

            $str = '';
            for ($i = 0; $i < 5; $i++)
            {
                $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
            }

            $word = $str;
       }

        // -----------------------------------
        // Determine angle and position
        // -----------------------------------

        $length = strlen($word);
        $angle  =  0;
        $x_axis = rand(6, (360/$length)-6);
        $y_axis = 160;


// die();
        // -----------------------------------
        // Create image
        // -----------------------------------

        // PHP.net recommends imagecreatetruecolor(), but it isn't always available
        if (function_exists('imagecreatetruecolor'))
        {
            $im = imagecreatetruecolor($img_width, $img_height);
        }
        else
        {
            $im = imagecreate($img_width, $img_height);
        }

        // -----------------------------------
        //  Assign colors
        // -----------------------------------

        $bg_color       = imagecolorallocate ($im, 255, 255, 255);
        $border_color   = imagecolorallocate ($im, 153, 102, 102);
        $text_color     = imagecolorallocate ($im, 204, 153, 153);
        $grid_color     = imagecolorallocate($im, 255, 182, 182);
        $shadow_color   = imagecolorallocate($im, 255, 230, 230);

        // -----------------------------------
        //  Create the rectangle
        // -----------------------------------

        ImageFilledRectangle($im, 0, 0, $img_width, $img_height, $bg_color);

        // -----------------------------------
        //  Create the spiral pattern
        // -----------------------------------

        $theta      = 1;
        $thetac     = 7;
        $radius     = 20;
        $circles    = 15;
        $points     = 25;

        for ($i = 0; $i < ($circles * $points) - 1; $i++)
        {
            $theta = $theta + $thetac;
            $rad = $radius * ($i / $points );
            $x = ($rad * cos($theta)) + $x_axis;
            $y = ($rad * sin($theta)) + $y_axis;
            $theta = $theta + $thetac;
            $rad1 = $radius * (($i + 1) / $points);
            $x1 = ($rad1 * cos($theta)) + $x_axis;
            $y1 = ($rad1 * sin($theta )) + $y_axis;
            imageline($im, $x, $y, $x1, $y1, $grid_color);
            $theta = $theta - $thetac;
        }

        // -----------------------------------
        //  Write the text
        // -----------------------------------

        $use_font = ($font_path != '' AND file_exists($font_path) AND function_exists('imagettftext')) ? TRUE : FALSE;

        $x = 40; $y = 15;
        for ($i = 0; $i < strlen($word); $i++)
        {
            
            imagestring($im, $font_size, $x, $y, substr($word, $i, 1), $text_color);
            $x += ($font_size*3);
            
        }


        // -----------------------------------
        //  Create the border
        // -----------------------------------

        imagerectangle($im, 0, 0, $img_width-1, $img_height-1, $border_color);

        // -----------------------------------
        //  Generate the image
        // -----------------------------------

        $img_name = $now.'.jpg';

        ImageJPEG($im, $img_path.$img_name);

       // $img = "<img src=\"$img_url$img_name\" width=\"$img_width\" height=\"$img_height\" style=\"border:0;\" alt=\" \" />";

        ImageDestroy($im);

        return array('word' => $word, 'image' => $img_name);
    }
}

// ------------------------------------------------------------------------

/* End of file captcha_helper.php */
/* Location: ./system/heleprs/captcha_helper.php */