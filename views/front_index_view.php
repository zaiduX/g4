<style>
  label{ font-weight: 10 !important; }
  .datepicker { background-color:white !important; }
</style>

<!-- SECTION IMAGE FULLSCREEN -->
<div class="w3-content w3-section" style="margin-top:0px !important; margin-bottom:0px !important; max-width: 100% !important;">
  <img class="mySlides" src="<?=base_url('resources/web/front_site/images/slider/freelancer.png');?>" style="width:100%">
  <div class="slider-content">
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden-xs">
        <h1 class="" style="-webkit-text-stroke: 1px black; font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Live your work dream'); ?></span></h1>
        <p class="lead" style="font-weight: bold; color: white !important; padding-bottom: 0px;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Our community of expert freelancers gives you the power to find the right person for any project in minutes.'); ?></span></p>
      </div>
      
      <div class="col-xs-12 hidden-xl hidden-lg hidden-md hidden-sm">
        <h6 class="" style="font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Live your work dream'); ?></span></h6>
        <h6 style="color: white !important; margin-bottom: 30px !important;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Our community of expert freelancers gives you the power to find the right person for any project in minutes.'); ?></span></h6>
      </div>

      <div class="col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12" style="margin-top: 130px">
        <div class="row search-form wow pulse" data-wow-delay="0.8s" style="background-color: #ffffff73 !important">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-sx-12" style="padding-left: 0px; padding-right: 0px">
            <form method="get" action="<?=base_url('search-freelancer');?>" class="form-inline md-form mr-auto mb-4" style="border-right: 0px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;">
              <input class="form-control mr-sm-2" type="text" name="search_skill" placeholder="<?= $this->lang->line('Search for any skill'); ?>" aria-label="<?= $this->lang->line('Search for any skill'); ?>" style="margin-right: -25px; border: 2px solid #1e5394; border-top-left-radius: 25px; border-bottom-left-radius: 25px; border-right: 0px; height: 42px; width: 60%; background-color: #FFF; padding-right: 30px;">
              <button class="btn aqua-gradient btn-rounded my-0" type="submit" style="color: #fff; background-color: #1e5394 !important; border-color: #1e5394; font-weight: 600; border-radius: 25px; border: 3px double #1e5394; opacity: unset; width: 180px;"><i class="fa fa-search"></i> <?=$this->lang->line('Freelancer')?></button>
            </form>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-sx-12" style="padding-left: 0px; padding-right: 0px">
            <form method="get" action="<?=base_url('troubleshooter');?>" class="form-inline md-form mr-auto mb-4" style="border-right: 0px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;">
              <input class="form-control mr-sm-2" type="text" name="skill_trouble" placeholder="<?= $this->lang->line('Search for any skill'); ?>" aria-label="<?= $this->lang->line('Search for any skill'); ?>" style="margin-right: -25px; border: 2px solid #1e5394; border-top-left-radius: 25px; border-bottom-left-radius: 25px; border-right: 0px; height: 42px; width: 60%; background-color: #FFF; padding-right: 30px;">
              <button class="btn aqua-gradient btn-rounded my-0" type="submit" style="color: #fff; background-color: #1e5394 !important; border-color: #1e5394; font-weight: 600; border-radius: 25px; border: 3px double #1e5394; opacity: unset; width: 180px;"><i class="fa fa-search"></i> <?=$this->lang->line('Troubleshooter')?></button>
            </form>
          </div>
        </div>
      </div>

      <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="margin-top: 15px;">
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
          <a href="projects" class="btn btn-default btn-delivery booknowBtn" style="border: 2px solid #FFF; opacity: unset;"><i class="fa fa-briefcase"></i> <?=$this->lang->line('Browse Jobs')?></a>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">

          <a href="troubleshooter" class="btn btn-default btn-delivery booknowBtn" style="border: 2px solid #FFF; opacity: unset;"><i class="fa fa-briefcase"></i> <?=$this->lang->line('Browse Troubleshoot')?></a>

        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
          <a href="offers" class="btn btn-default btn-delivery booknowBtn" style="border: 2px solid #FFF; opacity: unset;"><i class="fa fa-archive"></i> <?=$this->lang->line('Browse Offers')?></a>
        </div>
      </div>
      <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="margin-top: 15px;">
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
          <a href="post-project-job" class="btn btn-default btn-delivery booknowBtn" style="border: 2px solid #FFF; opacity: unset;; background-color: #4bcdff"><i class="fa fa-paper-plane-o"></i> <?=$this->lang->line('Post A Job')?></a>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
          <a href="post-troubleshoot" class="btn btn-default btn-delivery booknowBtn" style="border: 2px solid #FFF; opacity: unset;; background-color: #4bcdff"><i class="fa fa-paper-plane-o"></i> <?=$this->lang->line('Post A Troubleshoot')?></a>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
          <a class="btn btn-default btn-delivery booknowBtn" style="border: 2px solid #FFF; opacity: unset;; background-color: #4bcdff"><i class="fa fa-paper-plane-o"></i> <?=$this->lang->line('Post A Offer')?></a>
        </div>
      </div>
    </div>

    <div class="row hidden">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden-xs">
        <p class="lead" style="font-weight: bold; color: white !important; padding-bottom: 92px;"><span style="background-color: #00000000; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;">&nbsp;</span></p>
        <div class="row search-form wow pulse" data-wow-delay="0.8s" style=" padding-bottom: 0; padding-top: 15px; margin-left: 240px; margin-right: 240px; display:inline-block;">
          <div class="col-md-12 form-group">
            <?php foreach ($service_providers as $providers): ?>
              <?php if($providers['dedicated_provider']): ?>
                <img title="<?=$providers['company_name']?>" src="<?=base_url($providers['avatar_url'])?>" style="max-height:32px; width: auto;">&nbsp;&nbsp;
              <?php endif; ?>
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<section class="p-b-0" style="padding-top: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 20px">
        <h2><?= $this->lang->line('Offers And Promocodes'); ?></h2>
        <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>
    </div>
    <div class="container" style="padding-bottom: 20px">
        <div class="row stylish-panel">
            <?php $i=0; foreach ($promocodes as $promo){ $i++; ?>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <div>
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-1" >
                                <img style="width:100px; height:70px;" src='<?=base_url($promo["promocode_avtar_url"])?>' alt="">
                            </div>
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-11" >
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4" >
                                    <h6 style="margin: 0px 0px"> <?=$this->lang->line('Discount')?></h6>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" >
                                    <h6 style="margin: 0px 0px;"><?=$promo['discount_percent']?>%</h6>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding: 0px 0px;" >
                                    <h4 style="color: #5dccf7;"><?=$promo['promo_code']?></h4>
                                </div>
                            </div>
                        </div>
                    </div>    <!--<h3><?=$promo['promo_code']?></h3>-->
                  <?php if($i==3 || $i==6) echo "<br/>"; ?>  
                </div>

            <?php  } ?>
        </div>
    </div>
  </div>
</section>


<!-- How it works -->
<section class="background-grey" id="howworks" style="padding-top: 0px;padding-bottom: 0px;">
  <div class="container">
    <div class="heading heading-center text-center" style="margin-bottom: 10px;">
      <h2><?= $this->lang->line('BENEFITS OF BOOKING YOUR TICKET ON GONAGOO'); ?></h2>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          <div class="icon-box effect center light square">
            <div class="icon"> <a href="#"><i class="fa fa-archive"></i></a> </div>
            <h4 align="center"><?= $this->lang->line('Saving time and money'); ?></h4>
            <p align="center"><?= $this->lang->line('Book your ticket in 1mn, 7d / 7 24h / 24!'); ?><br /><?= $this->lang->line('Best market prices'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
      <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
        <div class="icon-box effect  center light square">
            <div class="icon"> <a href="#"><i class="fa fa-credit-card"></i></a> </div>
            <h4><?= $this->lang->line('Payment facility and others'); ?> </h4>
            <p align="center"><?= $this->lang->line('Payment by card, mobile money or cash.'); ?><br /><?= $this->lang->line('Get your e-ticket by sms or mail or to the agency'); ?></p>
        </div>
      </div>
      </div>
      <div class="col-md-3">
      <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
        <div class="icon-box effect  center light square">
          <div class="icon"> <a href="#"><i class="fa fa-users"></i></a> </div>
          <h4><?= $this->lang->line('Best agencies'); ?></h4>
          <p align="center"><?= $this->lang->line('The best travel agencies work with Gonagoo'); ?></p>
        </div>
      </div>
      </div>
      <div class="col-md-3">
      <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
        <div class="icon-box effect  center light square">
          <div class="icon"> <a href="#"><i class="fa fa-mobile"></i></a> </div>
          <h4><?= $this->lang->line('Mobile Apps'); ?></h4>
          <p align="center"><?= $this->lang->line('Web, Android or iOS: its up to you!'); ?><br /><?= $this->lang->line('Info in real time.'); ?></p>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>
<!-- END: How it works -->

<!-- CATEGORIES -->
<section class="p-b-0" style="padding-top: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 10px;">
      <h2><?= $this->lang->line('categories'); ?></h2>
      <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>
    </div>

    <!--
    <div class="section-content">
      <div class="row text-center color-border-bottom">
        <div class="col-xs-12 col-sm-6 col-md-3 col-md-offset-3 well">
            <i class="fa fa-truck fa-5x"></i> <br /><br />
            <h5 class="ac-title">Transport (freight)</h5>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 well">
            <i class="fa fa-envelope fa-5x"></i> <br /><br />
            <h5 class="ac-title">Courier & parcel delivery</h5>
        </div>
      </div>
    </div> -->
    <style type="text/css">
      .stylish-panel {
        padding: 20px 0;
        text-align: center;
      }
      .stylish-panel > div > div{
        padding: 10px;
        border: 1px solid #afe9ff;
        border-radius: 4px;
        transition: 0.2s;
      }
      .stylish-panel > div:hover > div{
        margin-top: -10px;
        border: 1px solid rgb(200, 200, 200);
        box-shadow: #afe9ff 0px 5px 5px 2px;
        background: rgba(200, 200, 200, 0.1);
        transition: 0.5s;
      }
      .stylish-panel > div:hover img {
        border-radius: 50%;
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    </style>

    <div class="container" style="padding-bottom: 20px">
      <div class="row stylish-panel">
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-truck fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('transportation'); ?></h3>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-envelope fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('courier'); ?></h3>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-dropbox fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('home_move'); ?></h3>
                    </a>
                </div>
            <br />
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-ticket fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('Ticket Booking'); ?></h3>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-recycle fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('Laundry Booking'); ?></h3>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-fire fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('Gas Booking'); ?></h3>
                    </a>
                </div>
            </div>
      </div>
    </div>

    <!--
    <ul class="grid grid-5-columns p-t-0">
        <?php foreach ($category_type as $cat) { ?>
            <li>
                <div class="accordion toggle clean color-border-bottom">
                    <div class="ac-item">
                        <h5 class="ac-title"> <i class="fa fa-bitbucket"></i><?= $cat['cat_type'] ?></h5>
                        <div class="ac-content">
                            <?php $category = $this->api->get_category_by_id($cat['cat_type_id']); ?>
                            <?php foreach ($category as $c) { ?>
                                <p><a href="#"><?= $c['cat_name'] ?></a></p> 
                            <?php } ?>
                        </div>
                    </div>
                </div>  
            </li>
        <?php } ?>
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-cutlery"></i><?= $this->lang->line('resturant'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-industry"></i><?= $this->lang->line('domestic_gas'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-archive"></i><?= $this->lang->line('courier'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-truck"></i><?= $this->lang->line('transportation'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li>
    </ul>-->
  </div>
</section>
<!-- END: CATEGORIES -->

<!-- OFFERS -->
<section class="parallax text-center text-light p-t-50 p-b-50 background-overlay" style="background-image: url('<?= $this->config->item('resource_url') . 'web/front_site/images/parallax/1.jpg';?>');" data-stellar-background-ratio="0.6">
  <div class="container">
    <div class="row">
      <div class="heading heading-center m-b-20">
      <h2><?= $this->lang->line('offer1'); ?></h2>
      <span class="lead"><?= $this->lang->line('offer2'); ?></span>
      </div>
      <!--<a href="<?= base_url().'sign-up'; ?>" class="button color rounded"><span><?= $this->lang->line('sign_up'); ?></span></a>  -->
    </div>
  </div>
</section>
<!-- END: OFFERS -->

<section id="benefits" style="padding-bottom: 10px;padding-top: 10px;">
  <div class="container">
     <div class="heading heading-center m-b-15">
          <h2><?= $this->lang->line('benefit'); ?></h2>
      </div> 
      <div class="col-md-4">
       <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
           <div><h3 style="color: #5dccf7;"><?= $this->lang->line('Travel agencies'); ?></h3></div>
           <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/carrier.jpg'; ?>" class="img-responsive"></div><br />
           <div>
               <ul type="none">
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('New customers'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Increase in sales'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Dashboards activity'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Customer relationship management'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Prepaid services'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Geolocation & Drivers App'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Increased visibility and reputation'); ?></span></li>
               </ul>
           </div> 
        </div>
      </div>
      <div class="col-md-4">
         <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
         <div><h3 style="color: #5dccf7;"><?= $this->lang->line('Businesses'); ?></h3></div>
           <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/business.jpg'; ?>" class="img-responsive"></div><br />
             <div>
                 <ul type="none">
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Attractive, transparent prices'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Online booking'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Ease, time saving, responsiveness'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Providers verified and rated'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Geolocation, real-time info'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Automated online management (contracts, orders, invoices)'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Transport costs reduced'); ?></span></li>
                 </ul>
             </div>
         </div> 
      </div>
      <div class="col-md-4">
         <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
          <div><h3 style="color: #5dccf7;"><?= $this->lang->line('individuals'); ?></h3></div>
           <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/individual.jpg'; ?>" class="img-responsive"></div><br />
             <div>
                 <ul type="none">
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Instant booking online 7/7'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Safety: agencies and drivers verified and rated'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Easy payment (mobile money, cash)'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Low prices'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Ease, time saving, e-ticket by sms, mail'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Real time road info'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Mobile, web, simple apps'); ?></span></li>
                 </ul>
             </div>
         </div> 
      </div>
      <div class="col-md-12 text-center">
          <a href="<?= base_url().'sign-up'; ?>" class="button color rounded"><span><?= $this->lang->line('sign_up'); ?></span></a>
      </div>
  </div>
</section>

<!-- CLIENTS -->
<section class="p-t-0" style="padding-bottom: 10px;">
  <div class="container">
      <div class="heading heading-center" style="margin-bottom: 10px;">
          <h2><?= $this->lang->line('clients'); ?></h2>
          <span class="lead"></span>
      </div>

      <div class="carousel" data-lightbox-type="gallery" data-carousel-col="3">
          <div class="featured-box">
              <div class="image-box circle-image small">
                  <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/1.jpg'; ?>" class="">
              </div>
              <div class="image-box-description text-center">
                  <h4><?= $this->lang->line('name_1'); ?></h4>
                  <p class="subtitle"><?= $this->lang->line('sub_title_1'); ?></p>
                  <hr class="line">
                  <div><?= $this->lang->line('description1'); ?></div>
                  
              </div>
          </div>
          <div class="featured-box">
              <div class="image-box circle-image small">
                  <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/2.jpg'; ?>" alt="">
              </div>
              <div class="image-box-description text-center">
                  <h4><?= $this->lang->line('name_2'); ?></h4>
                  <p class="subtitle"><?= $this->lang->line('sub_title_2'); ?></p>
                  <hr class="line">
                  <div><?= $this->lang->line('description2'); ?></div>
                  
              </div>
          </div>
          <div class="featured-box">
              <div class="image-box circle-image small">
                  <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/3.jpg'; ?>">
              </div>
              <div class="image-box-description text-center ">
                  <h4><?= $this->lang->line('name_3'); ?></h4>
                  <p class="subtitle"><?= $this->lang->line('sub_title_3'); ?></p>
                  <hr class="line">
                  <div><?= $this->lang->line('description3'); ?></div>
                  
              </div>
          </div>
          <div class="featured-box">
              <div class="image-box circle-image small">
                  <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/4.jpg'; ?>">
              </div>
              <div class="image-box-description text-center">
                  <h4><?= $this->lang->line('name_4'); ?></h4>
                  <p class="subtitle"><?= $this->lang->line('sub_title_4'); ?></p>
                  <hr class="line">
                  <div><?= $this->lang->line('description4'); ?></div>
                  
              </div>
          </div>
      </div>
  </div>
</section>
<!-- CLIENTS -->

<section class="hidden">
  <div class="container">
   <div class="col-md-12">
      <div class="col-md-3 text-center">
        <h4 class="widget-title"><?= $this->lang->line('they_talk_about_us'); ?> -</h4>
      </div>  
      <div class="col-md-9">
        <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/news.png'; ?>" style="width: 100%;">
      </div>
    </div>
  </div>
</section>
<script>
  $( "#btnFormCreate" ).click(function( e ) {
    e.preventDefault();
    var trip_source = $('#trip_source').val();
    var trip_destination = $('#trip_destination').val();
    var journey_date = $('#journey_date').val();
    var no_of_seat = $('#no_of_seat').val();
    
    if(trip_source == '' || trip_source == undefined) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select starting city!')?>", "error");
    } else if(trip_destination == '' || trip_destination == undefined) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select destination city!')?>", "error");
    } else if(journey_date == '' || journey_date == undefined) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select journey date!')?>", "error");
    } else if(no_of_seat == '' || no_of_seat == undefined) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('select number of seats!')?>", "error");
    } else if(no_of_seat > 10) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('The maximum number of seat is 40')?>", "error");
    } else if(no_of_seat <= 0) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Number of passengers cannot be zero (0).')?>", "error");
    } else { $("#createForm").submit(); }
  });
  
    $("#trip_source").on('change', function() {
        $('#trip_destination').select2('open');
    }); 
</script>