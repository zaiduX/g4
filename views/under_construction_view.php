<!DOCTYPE html>
<html lang="<?= $this->config->item('lang'); ?>">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="GONAGOO" />
  <meta name="description" content="">
  
  <link href="<?= $this->config->item('resource_url') . 'web/front_site/images/favicon.png';?>" rel="shortcut icon" />
  <title><?= $this->config->item('site_title'); ?></title>

  <!-- Bootstrap Core CSS -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/bootstrap/css/bootstrap.min.css'; ?>"  rel="stylesheet" />
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/fontawesome/css/font-awesome.min.css'; ?>"  rel="stylesheet">
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/animateit/animate.min.css'; ?>"  rel="stylesheet">

	<!-- Vendor css -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/owlcarousel/owl.carousel.css'; ?>"  rel="stylesheet">
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/magnific-popup/magnific-popup.css'; ?>" rel="stylesheet">

	<!-- Template base -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/theme-base.css'; ?>" rel="stylesheet">

	<!-- Template elements -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/theme-elements.css'; ?>" href="" rel="stylesheet">	

	<!-- Responsive classes -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/responsive.css'; ?>" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->	
	
	<!-- Template color -->
	<link href="<?= $this->config->item('resource_url') . 'web/front_site/css/blue.css'; ?>" rel="stylesheet" type="text/css" media="screen" title="blue">

	<!-- LOAD GOOGLE FONTS -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />

  
  	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('resource_url') . 'web/front_site/vendor/rs-plugin/css/settings.css'; ?>"  media="all" property="stylesheet" />
  	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('resource_url') . 'web/front_site/css/rs-plugin-styles.css'; ?>" />

	<!-- CSS CUSTOM STYLE -->
	<link rel="stylesheet" type="text/css" href="<?= $this->config->item('resource_url') . 'web/front_site/css/custom.css'; ?>"  media="screen" />

  	<!--VENDOR SCRIPT-->
  	<script src="<?= $this->config->item('resource_url') . 'web/front_site/vendor/jquery/jquery-1.11.2.min.js'; ?>"></script>
  	<script src="<?= $this->config->item('resource_url') . 'web/front_site/vendor/plugins-compressed.js'; ?>"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">

	<style>
	.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 26px;
    position: absolute;
    top: 10px;
    right: 1px;
    width: 20px;
	}
	.select2-container--default .select2-selection--single {
	    border-radius: 0px;
	}
	.select2-container .select2-selection--single {
	    height: 45px;
	    user-select: none;
	    -webkit-user-select: none;
	    border: 2px solid #ebebeb;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
		line-height: 1.333333;
	  padding: 12px 18px;
	}
</style>
</head>
<body class="wide page-loader " data-animation-icon ="<?= $this->config->item('resource_url') . 'web/front_site/images/svg-loaders/spin.svg'; ?>" >
	
	<div class="wrapper">	

		<!-- HEADER -->
    <header id="header" class="header header-transparent header-navigation-light">
      <div id="header-wrap">
        <div class="container-fluid">

          <!--LOGO-->
          <div id="logo">
          	<a href="<?= base_url(); ?>" class="Gonagoo logo">
            	<img src="<?= $this->config->item('resource_url') . 'web/front_site/images/logo-new.jpg'; ?>" alt="Gonagoo Logo" style="height:50px;margin-top: 5px;">
            </a>
          </div>
          <!--END: LOGO-->

        </div>
      </div>
    </header>
    <!-- END: HEADER -->

    <!-- SECTION IMAGE FULLSCREEN -->
<section class="parallax text-light fullscreen" style="background: url('<?= $this->config->item('resource_url') . 'web/front_site/images/slider/slide2.jpg';?>')" data-stellar-background-ratio="0.5">
	<div class="container container-fullscreen">
		<div class="text-middle text-center text-dark">
		  <h2 class="text-shad">Gonagoo - We are coming soon!!!</h2>
		  <p class="lead text-shad"><?= $this->lang->line('slider_heading2'); ?></p>
			<div class="container">
				
	    </div>
    </div>
  </div>
</section>
<!-- END: SECTION IMAGE FULLSCREEN -->

<!-- How it works -->
<section class="background-grey">
  <div class="container">
    <div class="heading heading-center text-center">
      <h2><?= $this->lang->line('how_it_works'); ?></h2>
    </div>
    <div class="row">
			<div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          	<div class="icon-box effect center light square">
				<div class="icon"> <a href="#"><i class="fa fa-map-marker"></i></a> </div>
				<h4 align="center"><?= $this->lang->line('tile1'); ?></h4>
			  	<p align="center"><?= $this->lang->line('tile2'); ?></p>
			</div>
        </div>
	    </div>
	    <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          	<div class="icon-box effect  center light square">
				<div class="icon"> <a href="#"><i class="fa fa-envelope"></i></a> </div>
				<h4><?= $this->lang->line('tile5'); ?> </h4>
			  	<p align="center"><?= $this->lang->line('tile6'); ?></p>
			</div>
        </div>
	    </div>
	    <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          	<div class="icon-box effect  center light square">
				<div class="icon"> <a href="#"><i class="fa fa-pencil"></i></a> </div>
				<h4><?= $this->lang->line('tile9'); ?></h4>
				<p align="center"><?= $this->lang->line('tile10'); ?></p>
			</div>
        </div>
	    </div>
	    <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          	<div class="icon-box effect  center light square">
				<div class="icon"> <a href="#"><i class="fa fa-truck"></i></a> </div>
				<h4><?= $this->lang->line('tile13'); ?></h4>
				<p align="center"><?= $this->lang->line('tile14'); ?></p>
			</div>
        </div>
	    </div>
	  </div>

  </div>
</section>
<!-- END: How it works -->



<section class="p-t-0 background-grey">
	<div class="container">
		<div class="text-center">
			<hr class="space">
			<div class="hr-title hr-long center"><abbr style="background-color: #f7f7f7;">Keep Me informed</abbr> </div>
		</div>
		<div class="row">
			<form id="keep_me_inform_form" method="post" action="<?= base_url(). 'under-construction/keep-me-inform'; ?>">
				<?php if($this->session->flashdata('success')): ?>
					<div role="alert" class="alert alert-success"> <strong>Success!</strong> <?= $this->session->flashdata('success'); ?> </div>
				<?php elseif($this->session->flashdata('error')): ?>
					<div role="alert" class="alert alert-danger"> <strong>Sorry!</strong><?= $this->session->flashdata('error'); ?></div>
				<?php endif; ?>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="upper" for="senderName">Name : <span class="text-danger">*</span></label>
							<input type="text" class="form-control required" name="senderName" placeholder="Enter your name" id="senderName">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="upper" for="senderEmail">Email Address : <span class="text-danger">*</span></label>
							<input type="email" class="form-control required email" name="senderEmail" placeholder="Enter your email address" id="senderEmail">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="upper" for="SenderPhone">Phone Number : <span class="text-danger">*</span></label>
							<input type="text" class="form-control required" name="SenderPhone" placeholder="Enter your phone" id="SenderPhone">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="upper" for="senderCompany">Your Company :</label>
							<input type="text" class="form-control" name="senderCompany" placeholder="Enter your company" id="senderCompany">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
							<label class="upper" for="senderType">I am : <span class="text-danger">*</span></label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						  	<label class="radio-inline"><input type="radio" name="optradio" value="Carrier (freight), Deliveryman">Carrier (freight), Deliveryman</label>
						  	<label class="radio-inline"><input type="radio" name="optradio" value="Business, enterprise">Business, enterprise</label>
						  	<label class="radio-inline"><input type="radio" name="optradio" value="Shop store, trader">Shop store, trader</label>
						  	<label class="radio-inline"><input type="radio" name="optradio" value="Individual">Individual</label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="upper" for="sendercountry">My country : <span class="text-danger">*</span></label>
							<select name="sendercountry" class="form-control" id="sendercountry">
								<option value="">Select Country</option>
								<?php foreach ($countries as $country) { ?>
									<option value="<?= $country['country_name'] ?>"><?= $country['country_name'] ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="upper" for="senderCity">My city : <span class="text-danger">*</span></label>
							<input type="text" class="form-control" name="senderCity" placeholder="Enter your city" id="senderCity">
						</div>
					</div>
				</div>				
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="upper" for="senderComment">Your comment or your activity :</label>
							<textarea class="form-control" name="senderComment" rows="6" placeholder="Your comment or your activity" id="senderComment" style="resize: none;"></textarea>
						</div>
					</div>
				</div>
				<div class="row">

					<div class="col-md-6">
						<div class="g-recaptcha" data-sitekey="6Lf8jTAUAAAAALIaOv9FETpuFm50zDVmCahYAH1K"></div>
					</div>
					<div class="col-md-6">
						<div class="form-group text-right">
							<button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane"></i>&nbsp; &nbsp;Submit &nbsp;</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

	<!-- FOOTER -->
	<!--<footer class="background-dark text-grey" id="footer">
		<div class="footer-content">
			<div class="container">
				<div class="row text-center">
					<img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/logo.png'; ?>" style="height:100px;">					
				</div>
				<div class="seperator seperator-dark seperator-simple"></div>
				<div class="row">
					<div class="col-md-3">
						<div class="widget clearfix widget-categories">
							<h4 class="widget-title"><?= $this->lang->line('in_complete_confidence'); ?></h4>
								<p><span><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/secure.png'; ?>" style="height: 30px" ></span> <?= $this->lang->line('secure_payment'); ?></p>
								<p><span><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/insurance.png'; ?>" style="height: 25px;"></span> <?= $this->lang->line('insurance'); ?></p>
								<p><span><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/24service.png'; ?>" style="height: 25px"></span> <?= $this->lang->line('assistance'); ?></p>  
						</div>
					</div>

					<div class="col-md-3">
						<div class="widget clearfix widget-categories">
							<h4 class="widget-title"><?= $this->lang->line('company_info'); ?></h4>
							<ul class="list list-arrow-icons">
								<li class="foot-list"> <a href="#" title=""><?= $this->lang->line('about_us'); ?> </a> </li>
								<li class="foot-list"> <a href="#" title=""><?= $this->lang->line('careers'); ?> </a> </li>
								<li class="foot-list"> <a href="#" title=""><?= $this->lang->line('press'); ?> </a> </li>
								<li class="foot-list"> <a href="#" title=""><?= $this->lang->line('terms_of_service'); ?> </a> </li>
								<li class="foot-list"> <a href="#" title=""><?= $this->lang->line('privacy_policy'); ?></a> </li>

							</ul>
						</div>
					</div>

					<div class="col-md-3">
						<div class="widget clearfix widget-contact-us-form">
							<h4 class="widget-title"><?= $this->lang->line('download_our_mobile_application'); ?></h4>
							<p>
								<img src="<?= $this->config->item('resource_url') . 'web/front_site/images/apple-app-store.png'; ?>" style="width: 200px;">
	            </p>
	            <p>
	            	<img src="<?= $this->config->item('resource_url') . 'web/front_site/images/google-play.png'; ?>" style="width: 200px;">
	            </p>     
	          </div>
					</div>

					<div class="col-md-3">
						<div class="widget clearfix widget-categories">
							<h4 class="widget-title"><?= $this->lang->line('address'); ?></h4>
							<ul class="list-large list-icons">
								<li><i class="fa fa-map-marker"></i>
									<strong><?= $this->lang->line('address'); ?>:</strong> 67 rue de Provence, 75009</li>
								<li><i class="fa fa-phone"></i><strong><?= $this->lang->line('phone'); ?>:</strong> +33 (0) 143 79 86 30</li>
								<li><i class="fa fa-envelope"></i><strong><?= $this->lang->line('email'); ?>:</strong> <a href="mailto:first.last@example.com">contact@gonagoo.com</a>
								</li>
								<li>
							    <a href="#"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/facebook.png'; ?>" class="sc-icons" style="height: 40px; width: 40px;margin-right:10px;"></a>
							    <a href="#"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/google.png'; ?>" class="sc-icons" style="height: 40px; width: 40px;margin-right:10px;"></a>
							    <a href="#"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/twitter.png'; ?>" class="sc-icons" style="height: 40px; width: 40px;margin-right:10px;"></a>
							    <a href="#"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/linkedin.png'; ?>" class="sc-icons" style="height: 43px; width: 43px;margin-right:10px;"></a>
								</li>    
							</ul>
						</div>
					</div>
					<div class="col-md-12">
					  <hr />
					  <div class="col-md-2">
	          	<h5 class="widget-title" style="line-height: 25px;"><?= $this->lang->line('they_talk_about_us'); ?> -</h5>
	          </div>  
	          <div class="col-md-10">
	          	<img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/news.png'; ?>" style="width: 100%;">
	          </div>  
					</div>
				</div>
			</div>
		</div>
		<div class="copyright-content m-t-0">
			<div class="container">
				<div class="row">
					<div class="copyright-text text-center"> &copy; 2017 Gonagoo  <a target="_blank" href="http://www.gonagoo.com"></a>
					</div>
				</div>
			</div>
		</div>
	</footer>-->
	<!-- END: FOOTER -->

  </div>
  <!-- END: WRAPPER -->
  
  	<!-- GO TOP BUTTON -->
  	<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>
	
	<!--Jquery Validation-->
	<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
	<script src="<?= $this->config->item('resource_url') . 'web/front_site/js/validation.js'; ?>"></script>
	
	<!-- Theme Base, Components and Settings -->
	<script src="<?= $this->config->item('resource_url') . 'web/front_site/js/theme-functions.js'; ?>"></script>

	<!-- Custom js file -->
	<script src="<?= $this->config->item('resource_url') . 'web/front_site/js/custom.js'; ?>"></script>

	<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>

	<script>
		$(function(){
			$("#keep_me_inform_form").validate({
				rules: {
					senderName: {
						required: true,
					},
					senderEmail: {
						required: true,
						email: true,
					},
					SenderPhone: {
						required: true,
						number: true,
					},
					sendercountry: {
						required: true,
					},
					senderCity: {
						required: true,
						minlength: 3,
					},
					optradio: {
						required: true,
					},
				},
				messages: {
					senderName: {
						required: "Your full name required.",
					},
					senderEmail: {
						required: "Your email address required.",
						email: "Enter valid email address."
					},
					SenderPhone: {
						required: "Your phone number required.",
						number: "Enter valid phone number only.",
					},
					sendercountry: {
						required: "Select your country.",
					},
					senderCity: {
						required: "Enter your city name.",
						minlength: "City name is too short.",
					},
					optradio: {
						required: "Select your category.",
					},
				}
			});
		});
	    
	    var onloadCallback = function() {
	      alert("grecaptcha is ready!");
	    };
	</script>
	<script>
		$(document).ready(function() {
		    var select2Options = { width: 'resolve' };    
		    $('select').select2(select2Options);                 
		});
	</script>
</body>
</html>