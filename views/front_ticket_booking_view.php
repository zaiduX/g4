<style>
  .custom_label { color: #175091; display: inline-block; margin-top: 0px; margin-bottom: 2px; font-size: 13px; }
  .error { margin-top: 0px !important; }
  /* Tabs panel */
  .tabbable-panel { padding: 10px; font-size: 12px; }
  /* Default mode */
  .tabbable-line > .nav-tabs { border: none; margin: 0px; }
  .tabbable-line > .nav-tabs > li { margin-right: 2px; }
  .tabbable-line > .nav-tabs > li > a { border: 0; margin-right: 0; color: #737373; }
  .tabbable-line > .nav-tabs > li > a > i { color: #a6a6a6; }
  .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover { border-bottom: 4px solid #fbcdcf; }
  .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a { border: 0; background: none !important; color: #333333; }
  .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i { color: #a6a6a6; }
  .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu { margin-top: 0px; }
  .tabbable-line > .nav-tabs > li.active { border-bottom: 4px solid #64ccf5; position: relative; }
  .tabbable-line > .nav-tabs > li.active > a { border: 0; color: #333333; }
  .tabbable-line > .nav-tabs > li.active > a > i { color: #404040; }
  .tabbable-line > .tab-content { margin-top: -3px; background-color: #fff; border: 0; border-top: 1px solid #eee; padding: 15px 0; }
  .portlet .tabbable-line > .tab-content { padding-bottom: 0; }
  /* Below tabs mode */
  .tabbable-line.tabs-below > .nav-tabs > li { border-top: 4px solid transparent; }
  .tabbable-line.tabs-below > .nav-tabs > li > a { margin-top: 0; }
  .tabbable-line.tabs-below > .nav-tabs > li:hover { border-bottom: 0; border-top: 4px solid #fbcdcf; }
  .tabbable-line.tabs-below > .nav-tabs > li.active { margin-bottom: -2px; border-bottom: 0; border-top: 4px solid #64ccf5; }
  .tabbable-line.tabs-below > .tab-content { margin-top: -10px; border-top: 0; border-bottom: 1px solid #eee; padding-bottom: 15px; }
  .nav-tabs > li, .nav-pills > li { float:none; display:inline-block; *display:inline; zoom:1; }
  .nav-tabs, .nav-pills { text-align:center; }
  .datepicker { background-color:white !important; }
</style>

<section class="p-t-30 m-t-0 background-grey" style="padding-bottom: 0px;">

  <?php if($this->session->flashdata()): ?>
    <section class="p-t-20 p-b-0 m-t-0 ">
      <div class="container">
        <div class="row">
          <?php if($this->session->flashdata('success')): ?>
          <div role="alert" class="alert alert-success text-center col-md-6 col-md-offset-3">
            <strong><span class="ai-warning">Success</span>!</strong> <?= $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('warning')):  ?>
          <div role="alert" class="alert alert-warning text-center col-md-6 col-md-offset-3">
            <strong><span class="ai-warning">Done</span>!</strong> <?= $this->session->flashdata('warning'); ?>
          </div>
        <?php else: ?>
          <div role="alert" class="alert alert-danger text-center col-md-6 col-md-offset-3">
            <strong><span class="ai-warning">Failed</span>!</strong> <?= $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        </div>
      </div>
    </section>
  <?php endif; ?>

  <div class="container">
    <div class="row" style="margin-top: -15px;">
      
      <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7" style="background-color: #FFF">
        <div class="tabbable-panel">
          <div class="tabbable-line">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#home"><?= $this->lang->line('Book & Pay Now'); ?></a></li>
              <li><a data-toggle="tab" href="#menu1" id="sign_up"><?= $this->lang->line('New user - Sign up'); ?></a></li>
              <li><a data-toggle="tab" href="#menu2"><?= $this->lang->line('Existing user - Login'); ?></a></li>
            </ul>
          </div>
        </div>

        <div class="tab-content">
          <div id="home" class="tab-pane fade in active">
            <?php if($_GET['no_of_seat'] > 1) { ?>
              <form action="<?=base_url('more-passengers-details')?>" method="GET" id="my_bookingform" class="bookingform">
            <?php } else { ?>  
              <form action="<?=base_url('ticket-booking')?>" method="GET" id="my_bookingform" class="bookingform">
            <?php } ?>
              <div class="row">
                <div class="col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding: 0px 0px;">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      <h6 style="color:#175091;margin: 7px 0px;" id="contact_first_name"><?= $this->lang->line('contact_first_name'); ?></h6>
                      <input type="text" id="firstname0" name="firstname0" class="form-control" placeholder="<?= $this->lang->line('enter_first_name'); ?>" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('contact_last_name');?></h6>
                      <input type="text" name="lastname0" class="form-control" id="lastname0" placeholder="<?= $this->lang->line('enter_last_name'); ?>" />
                    </div>
                  </div>

                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding: 0px 0px;">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">  
                      <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('email_address'); ?></h6>
                      <input type="text" class="form-control" id="email_id0" name="email_id0" placeholder="<?= $this->lang->line('enter_email_address'); ?>"/>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('mobile_number');?></h6>
                      <input type="text" id="mobile0" name="mobile0" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number');?>" onkeypress="return isNumberKey(event)"/>  
                    </div>
                  </div>

                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding: 0px 0px;">
                    <div class="col-md-3">  
                      <h6 style="color:#175091; margin: 7px 0px;"><?= $this->lang->line('gender'); ?></h6>
                      <select class="form-control" name="gender0" id="gender0">
                        <option value=""><?= $this->lang->line('Select gender'); ?></option>
                        <option value="M"><?= $this->lang->line('male'); ?></option>
                        <option value="F"><?= $this->lang->line('female'); ?></option>
                      </select>
                    </div>
                    <!-- <div class="col-md-3">
                      <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('Age'); ?></h6>
                      <input type="text" id="age0" name="age0" class="form-control" placeholder="<?= $this->lang->line('Enter age'); ?>" />  
                    </div> -->
                    <div class="col-md-3" style="padding-left: 0px;">
                      <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('DoB'); ?></h6>
                      <div class="input-group date">
                        <input type="text" class="form-control" id="age0" name="age0" autocomplete="off" >
                        <div class="input-group-addon dpAddon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('Select Country');?></h6>
                      <select class="form-control" name="country_id0" id="country_id0">
                        <option value=""><?= $this->lang->line('Select Country'); ?></option>
                        <?php foreach ($countries as $c) :?>
                          <option value="<?= $c['country_id'];?>"><?=$c['country_name'];?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <button id="form_submit" class="button blue-dark col-md-12"><?=($_GET['no_of_seat']>1)?$this->lang->line('prceed'):$this->lang->line('BOOK & PAY')?></button>
                  </div>
                  
                  <div class="col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12">
                    <h5>
                      <?= $this->lang->line('Offers: Create a Gonagoo account to get benefit of promotional codes.'); ?>
                      <a href="#" id="sign_up_now"><?= $this->lang->line('Sign up now'); ?></a>
                    </h5>
                  </div>

                  <!-- hidden fields -->
                  <input type="hidden" name="journey_date" value="<?=$_GET['journey_date']?>">
                  <input type="hidden" name="return_date" value="<?=$_GET['return_date']?>">
                  <input type="hidden" name="ownward_trip" value="<?=$_GET['ownward_trip']?>">
                  <input type="hidden" name="no_of_seat" value="<?=$_GET['no_of_seat']?>">
                  <input type="hidden" name="seat_price" value="<?=$_GET['seat_price']?>">
                  <input type="hidden" name="seat_type" value="<?=$_GET['seat_type']?>">
                  <input type="hidden" name="seat_type" value="<?=$_GET['seat_type']?>">
                  <input type="hidden" name="pickup_point" value="<?=$_GET['pickup_point']?>">
                  <input type="hidden" name="drop_point" value="<?=$_GET['drop_point']?>">
                  <input type="hidden" name="trip_source" value="<?=$_GET['trip_source']?>">
                  <input type="hidden" name="trip_destination" value="<?=$_GET['trip_destination']?>">
                  <input type="hidden" name="distance" value="<?=$_GET['distance']?>">
                  <input type="hidden" name="duration" value="<?=$_GET['duration']?>">
                  <input type="hidden" name="ownward_currency_sign" value="<?=$_GET['ownward_currency_sign']?>">
                  <input type="hidden" name="ownward_currency_id" value="<?=$_GET['ownward_currency_id']?>">
                  <input type="hidden" name="cat_id" value="<?=$_GET['cat_id']?>">
                  <input type="hidden" name="return_trip" value="<?=$_GET['return_trip']?>">
                  <input type="hidden" name="return_seat_price" value="<?=$_GET['return_seat_price']?>">
                  <input type="hidden" name="return_seat_type" value="<?=$_GET['return_seat_type']?>">
                  <input type="hidden" name="return_pickup_point" value="<?=$_GET['return_pickup_point']?>">
                  <input type="hidden" name="return_drop_point" value="<?=$_GET['return_drop_point']?>">
                  <input type="hidden" name="return_trip_source" value="<?=$_GET['return_trip_source']?>">
                  <input type="hidden" name="return_trip_destination" value="<?=$_GET['return_trip_destination']?>">
                  <input type="hidden" name="return_currency_sign" value="<?=$_GET['return_currency_sign']?>">

                  <?php $ownward_selected_seat_nos = 'ownward_selected_seat_nos_'.$_GET['ownward_trip'];  ?>
                  <input type="hidden" name="ownward_selected_seat_nos" value="<?=$_GET[$ownward_selected_seat_nos]?>">
                  <?php 
                    if($_GET['return_trip'] > 0) {
                      $return_selected_seat_nos = 'return_selected_seat_nos_'.$_GET['return_trip']; 
                      if(!isset($_GET[$return_selected_seat_nos]) || $_GET[$return_selected_seat_nos] == 'return_selected_seat_nos_0') { ?>
                        <input type="hidden" name="return_selected_seat_nos" value="NULL">
                      <?php } else { ?>
                      <input type="hidden" name="return_selected_seat_nos" value="<?=$_GET[$return_selected_seat_nos]?>">
                  <?php } 
                    } else { ?>
                      <input type="hidden" name="return_selected_seat_nos" value="NULL">
                  <?php } ?>
                  <!-- hidden fields -->

                </div>
              </div>
            </form>
            <script>
              $("#home").on('click', function(event) { 
                $(".select2-container--default").css('width', '100%');
                $(".select2-container--default").css('display', 'block');
              });

              $(window).load(function() {
                $('#age0').datepicker({
                  autoclose: true,
                }, 'setEndDate', new Date(), );
              });

              $( ".dpAddon" ).click(function( e ) { 
                $("#age0").focus();
              });
              
              $("#form_submit").on("click", function(e) {
                e.preventDefault();
                var firstname = $("#firstname0").val();
                var lastname = $("#lastname0").val();
                var email = $("#email_id0").val();
                var mobile = $("#mobile0").val();
                var age = $("#age0").val();
                var gender = $("#gender0").val();
                var country_id = $("#country_id0").val();
                if(!firstname) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('enter_first_name'))?>, "error"); 
                } else if(firstname.length < 3) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('First name should have 3 characters.'))?>, "error");
                } else if(!lastname) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('enter_last_name'))?>, "error");
                } else if(lastname.length < 3) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('Last name should have 3 characters.'))?>, "error");
                } else if(!email) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('enter_email_address'))?>, "error"); 
                } else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('Incorrect email format.'))?>, "error");
                } else if(!mobile) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('enter_mobile_number'))?>, "error");
                } else if(mobile.length < 6) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('Mobile number must have 6 or more digits.'))?>, "error");
                } else if(mobile.length > 16) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('Mobile number must have less than 16 digits.'))?>, "error");
                } else if(!gender) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('select_gender'))?>, "error");
                } else if(!age) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('Enter date of birth.'))?>, "error");
                } else if(!country_id) { swal(<?=json_encode($this->lang->line('error'))?>, <?=json_encode($this->lang->line('select_country'))?>, "error");   
                } else {  $("#my_bookingform").submit(); }
              });

              function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if ((charCode < 48 || charCode > 57)) return false;
                return true;
              }
            </script>
          </div>
          <div id="menu1" class="tab-pane fade">
            <div class="row">
              <div class="col-md-10 col-md-offset-1" style="margin-top: 15px; margin-bottom: 15px;">
                <div class="row">
                  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 text-center" style="margin-bottom: 5px;">
                    <button name="btn_social" class="btn-social loginBtn loginBtn--facebook" onclick="fbLogin_signup();"> <?= $this->lang->line('btn_fb'); ?> </button>                    
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 text-center" style="margin-bottom: 5px;">
                    <button name="btn_social" class="btn-social loginBtn loginBtn--linkedin" onclick="onLinkedInLoad()"> <?= $this->lang->line('btn_li'); ?> </button>                  
                  </div>
                </div>

                <div class="seperator" style="margin: 10px auto !important"><span><?= $this->lang->line('or_separator'); ?></span></div>

                <form action="<?= base_url();?>register-new-user" method="post" id="register_form" autocomplete="off">
                  <input type="hidden" id="login_type" name="login_type" />
                  <input type="hidden" id="avatar_url" name="avatar_url" />
                  <input type="hidden" id="cover_url" name="cover_url" />
                  <input type="hidden" id="gender" name="gender" />
                  <input type="hidden" id="timezone" name="timezone" />
                  <input type="hidden" id="cpt" name="cpt" value="<?= $this->session->userdata('captcha_code');?>" />
                  
                  <div class="row"> 
                    <div class="col-md-6 custom_label">
                        <label class=""><?= $this->lang->line('first_name'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('first_name'); ?>" name="firstname" id="firstname" />
                        <label class="text-center" id="error_firstname"></label>
                    </div>  
                    <div class="col-md-6 custom_label">
                        <label class=""><?= $this->lang->line('last_name'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('last_name'); ?>" name="lastname" id="lastname" />
                        <label class="text-center" id="error_lastname"></label>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12 custom_label">
                      <label class=""><?= $this->lang->line('email'); ?></label>
                      <input type="email" class="form-control" placeholder="<?= $this->lang->line('email'); ?>" name="email" id="email" />
                      <label class="text-center" id="error_email" style="color: red;"></label>
                    </div>
                  </div>
                  
                  <div class="row"> 
                    <div class="col-md-6 custom_label">
                      <label class=""><?= $this->lang->line('password'); ?></label>
                      <input type="password" class="form-control" placeholder="<?= $this->lang->line('password'); ?>" name="password" id="password" />
                      <label class="text-center" id="error_password"></label>
                    </div>  
                    <div class="col-md-6 custom_label">
                        <label class=""><?= $this->lang->line('confirm_password'); ?></label>
                        <input type="password" class="form-control" placeholder="<?= $this->lang->line('confirm_password'); ?>" name="repeatpassword" id="repeatpassword" />
                        <label class="text-center" id="error_repeatpassword"></label>
                    </div>  
                  </div>   

                  <div class="row"> 
                    <div class="col-md-4 custom_label">
                      <label class=""><?= $this->lang->line('select_country'); ?></label>
                      <select name="country_id" id="country_id" class="form-control select2">
                        <option value=""> <?= $this->lang->line('select_country'); ?></option>
                        <?php foreach ($countries as $c) :?>
                          <option value="<?= $c['country_id'];?>"><?=$c['country_name'];?></option>
                        <?php endforeach; ?>
                      </select>
                      <label class="text-center" id="error_country_id"></label>
                    </div>
                    <div class="col-md-3 custom_label">
                      <label class=""><?= $this->lang->line('country_code'); ?> <i class="fa fa-plus"></i></label>
                      <div class="left-inner-addon">
                        <input type="text" class="form-control" id="country_code" name="country_code" readonly />
                        <label class="text-center" id="error_country_code"></label>
                      </div>
                    </div>
                    <div class="col-md-5 custom_label">
                      <label class=""><?= $this->lang->line('mobile_no'); ?></label>
                      <input type="text" class="form-control" placeholder="<?= $this->lang->line('mobile_no'); ?>" name="mobile_no" id="mobile_no">
                      <label class="text-center" id="error_mobile_no"></label>
                    </div>
                  </div>  

                  <div class="row">
                    <div class="col-md-4"><hr/></div>
                    <div class="col-md-4 text-center"><h6 class="text-info" style="margin-bottom: 0px;"><?= $this->lang->line('register_as'); ?></h6></div>
                    <div class="col-md-4"><hr/></div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-5 col-md-offset-1 custom_label">
                      <input type="radio" name="register_as" id="as_individual" autocomplete="off" checked value="1" class="col-md-1 register_as">  
                      <label for="as_individual" class="control-label"> &nbsp; 
                        <i class="fa fa-user"></i> <span class=""><?= $this->lang->line('as_individual'); ?> </span>
                      </label>
                    </div>  
                    <div class="col-md-6 custom_label">
                      <input type="radio" name="register_as" id="as_company" autocomplete="off" value="0" class="col-md-1 register_as ">
                      <label for="as_company" class="control-label"> &nbsp; 
                        <i class="fa fa-users"></i> <span class=""><?=$this->lang->line('as_company');?> </span>
                      </label>
                    </div>   
                  </div>

                  <div class="row"> 
                    <div class="col-md-12">
                      <div class="company_div hidden">
                        <label for="company_name" class="custom_label"><?= $this->lang->line('compnay_name'); ?></label>
                        <input type="text" class="form-control" placeholder="<?= $this->lang->line('compnay_name'); ?>" name="company_name" id="company_name" />
                      </div>
                    </div>                            
                  </div>  
                  
                  <div class="row">
                    <div class="col-md-4"> <hr/></div>
                    <div class="col-md-4 text-center"><h6 class="text-info"><?= $this->lang->line('account_type'); ?></h6></div>
                    <div class="col-md-4"> <hr/></div>
                  </div>
                  <style>
                    .box-buyer{
                      margin-top: 0px;
                      height: 80px;
                      border: 1px solid #999;
                      width: 100px;
                      cursor: pointer;
                      width: 100%;
                      text-align: center;
                      background-image: url("<?=base_url('resources/web/front_site/images/buyer.png')?>");
                      background-repeat: no-repeat;
                      background-size: 92px;
                      background-position-x: center;
                      background-position-y: center;
                    }
                    .box-seller{
                      margin-top: 0px;
                      height: 80px;
                      border: 1px solid #999;
                      width: 100px;
                      cursor: pointer;
                      width: 100%;
                      text-align: center;
                      background-image: url("<?=base_url('resources/web/front_site/images/seller.png')?>");
                      background-repeat: no-repeat;
                      background-size: 80px;
                      background-position-x: center;
                    }
                    .box-both{
                      margin-top: 0px;
                      height: 80px;
                      border: 1px solid #999;
                      width: 100px;
                      cursor: pointer;
                      width: 100%;
                      text-align: center;
                      background-image: url("<?=base_url('resources/web/front_site/images/both.png')?>");
                      background-repeat: no-repeat;
                      background-size: 100px;
                      background-position-x: center;
                      background-position-y: bottom;
                    }
                    .icon {
                      display: block;
                      font-size: 60px;
                      font-size: 6rem;
                      height: 78px;
                      line-height: 80px;
                      background-color: transparent;
                      margin-bottom: 15px;
                      text-align: center;
                      -webkit-border-radius: 3px;
                      -moz-border-radius: 3px;
                      border-radius: 3px;
                      -webkit-background-clip: padding-box;
                      -moz-background-clip: 'padding';
                      background-clip: padding-box;
                      width: 100%;
                    }
                  </style>
                  <div class="row"> 
                    <div class="col-md-4 custom_label text-center">
                      <div class="box-buyer" id="icon_buyer">
                      </div>
                      <label id="lbl_buyer"><?= $this->lang->line('buyer'); ?></label>
                      <input type="hidden" name="buyer" id="buyer" value="0" />
                    </div>                      
                    <div class="col-md-4 custom_label text-center">
                      <div class="box-seller" id="icon_seller">
                      </div>
                      <label id="lbl_seller"><?= $this->lang->line('seller'); ?></label>
                      <input type="hidden" name="seller" id="seller" value="0" />
                    </div>
                    <div class="col-md-4 custom_label text-center">
                      <div class="box-both" id="icon_both">
                      </div>
                      <label id="lbl_both"><?= $this->lang->line('both'); ?></label>
                      <input type="hidden" name="both" id="both" value="0" />
                    </div>
                    <label class="text-center" id="error_account_type"></label>
                    <h6 class="text-mute" style="padding-left: 15px; margin-top: -5px;">
                      <span class="fa fa-angle-double-right"></span><?= $this->lang->line('account_type_line1'); ?><br/>
                      <span class="fa fa-angle-double-right"></span><?= $this->lang->line('account_type_line2'); ?>
                    </h6>                          
                  </div>

                  <div class="row">
                    <div class="col-md-4 custom_label">
                      <label class=""><?= $this->lang->line('are_you_human'); ?></label>                          
                      <img src="<?= $this->config->item('resource_url').'captcha/'.$data['image']; ?>" alt="<?= $data['image']; ?>" />
                    </div>
                    <div class="col-md-8 custom_label">
                      <label class=""><?= $this->lang->line('txt_captcha'); ?></label>                          
                      <input type="text" name="captcha" id="captcha" class="form-control" placeholder="<?= $this->lang->line('txt_captcha'); ?>" />
                    </div>                          
                  </div>

                  <div class="row">
                    <div class="col-md-6 col-md-offset-3 custom_label">
                      <button id="btn_submit" class="button blue-dark form-control" type="submit" style="border-radius: 3px;"><?= $this->lang->line('btn_signup'); ?></button>
                    </div>
                  </div> 
                </form>
              </div>
            </div>
          </div>
          <div id="menu2" class="tab-pane fade">
            <div class="row">
              <div class="col-md-8 col-md-offset-2" style="margin-top: 15px; margin-bottom: 15px;">
                <form method="post" action="<?=base_url('confirm/log-in')?>" id="login_form" autocomplete="off" >
                  <input type="hidden" id="login_type_login" name="login_type" />
                  <input type="hidden" id="timezone_login" name="timezone" />
                  <div class="form-group">
                    <label class="sr-only"><?= $this->lang->line('email'); ?></label>
                    <input type="email" class="form-control" placeholder="<?= $this->lang->line('enter_email_address'); ?> ..." id="email_login" name="email" />
                  </div>
                  <div class="form-group">
                    <label class="sr-only"><?= $this->lang->line('password'); ?></label>
                    <input type="password" class="form-control" placeholder="<?= $this->lang->line('enter_password'); ?> ..." id="password_login" name="password" />
                  </div>
                  <div class="form-inline form-group">
                    <div class="col-md-4 text-left custom_label">
                      <button class="button blue-dark" type="submit"> <?= $this->lang->line('btn_login'); ?> <i class="fa fa-unlock"></i></button>
                    </div>
                    <div class="col-md-8 text-right custom_label">
                      <div class="checkbox">
                        <input type="checkbox" name="remember_me"><?= $this->lang->line('remember_me'); ?>
                      </div>
                      <br />
                      <a href="<?= base_url('recovery/password'); ?>"><small><?= $this->lang->line('lost_pass'); ?></small></a>
                    </div>
                  </div>
                </form>
                  <div class="seperator" style="margin: 10px auto !important"><span><?= $this->lang->line('or'); ?></span></div>
                  <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 text-center" style="margin-bottom: 5px;">
                      <button id="btn_login" style="font-size: 12px" class="btn-social loginBtn loginBtn--facebook" onclick="fbLogin();"> <?= $this->lang->line('btn_Login with Facebook'); ?> </button>                    
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 text-center" style="margin-bottom: 5px;">
                      <button id="btn_linkedIn" style="font-size: 12px" class="btn-social loginBtn loginBtn--linkedin" onclick="onLinkedInLoad()"> <?= $this->lang->line('btn_Login with LinkedIn'); ?> </button>                 
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
        <!-- onward details -->
        <div class="panel panel-register-box" style="padding-top: 15px; padding-bottom: 0px; line-height: 0.5; margin-bottom: 10px;">
          <div class="hr-title hr-long center" style="margin:0px auto -5px; width: 80% !important;"><abbr style="font-size: smaller;"><?= $this->lang->line('Ownward Trip Details'); ?></abbr></div>
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-map-marker" style="font-size: 20px; color: #23b122;padding: 0px 4px;"></i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['trip_source']?></h6>
                </div>
                <hr style="margin: 10px 0px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-map-marker" style="font-size: 20px;color: #d83131;padding: 0px 4px;"> </i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['trip_destination']?></h6>
                </div>
                <hr style="margin: 10px 0px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-road" style="font-size: 20px;"></i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['distance'].' '.$this->lang->line('Km')?></h6>
                </div>
                <hr style="margin: 10px 0px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-clock-o" style="font-size: 20px;"> </i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['duration'].' '.$this->lang->line('hours')?></h6>
                </div>
                <hr style="margin: 10px 0px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-calendar-check-o" style="font-size: 20px;"> </i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$this->lang->line('Journey Date Time')." : ".$_GET['journey_date']." - ".$_GET['onward_pickup_time']?></h6>
                </div>
              </div>
            </div>
          <br />
        </div>
        <!-- onward details -->
         
        <!-- return details -->
        <?php if($_GET['return_trip']!=0){ ?>
        <div class="panel panel-register-box" style="padding-top: 15px; padding-bottom: 0px; line-height: 0.5; margin-bottom: 10px;">
          <div class="hr-title hr-long center" style="margin:0px auto -5px; width: 80% !important;"><abbr style="font-size: smaller;"><?= $this->lang->line('Return Trip Details'); ?></abbr></div>
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-map-marker" style="font-size: 20px; color: #23b122;padding: 0px 4px;"></i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['return_trip_source']?></h6>
                </div>
                <hr style="margin: 10px 0px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-map-marker" style="font-size: 20px;color: #d83131;padding: 0px 4px;"> </i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['return_trip_destination']?></h6>
                </div>
                <hr style="margin: 10px 0px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-road" style="font-size: 20px;"> </i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['distance'].' '.$this->lang->line('Km')?></h6>
                </div>
                <hr style="margin: 10px 0px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-clock-o" style="font-size: 20px;"> </i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['duration'].' '.$this->lang->line('hours')?></h6>
                </div>
                <hr style="margin: 10px 0px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <i class="fa fa-calendar-check-o" style="font-size: 20px;"> </i>
                </div>
                <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$this->lang->line('Return Date Time')." : ".$_GET['return_date']." - ".$_GET['return_pickup_time']?></h6>
                </div>
              </div>
            </div>
          <br />
        </div>
        <?php } ?>
        <!-- return details -->
          
        <!-- fare details -->
        <div class="panel panel-register-box" style="padding-top: 15px; padding-bottom: 0px; line-height: 0.5; margin-bottom: 10px;">
          <div class="hr-title hr-long center" style="margin:0px auto -5px; width: 80% !important;"><abbr style="font-size: smaller;"><?=$this->lang->line('Fare Details');?></abbr> </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <h6 style="font-size:14px; color:#3498db; margin: 0px 15px;"><?=$this->lang->line('Ownward Fare')?></h6>
              <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['seat_type'].' '.$this->lang->line('Fare').' ('.$_GET['no_of_seat'].' '.$this->lang->line('Seat').')';?></h6> 
              </div>
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                <h6 style="font-size:14px; color:#175091;margin: 0px 0px;"><?=$_GET['seat_price'].' '.$_GET['ownward_currency_sign'];?></h6> 
              </div>
              <hr style="margin: 3px 0px;">
              
              <?php if($_GET['return_trip']!=0) { ?>
                <h6 style="font-size:14px; color:#3498db; margin: 0px 15px;"><?=$this->lang->line('Return Fare')?></h6>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['return_seat_type'].' '.$this->lang->line('Fare').' ('.$_GET['no_of_seat'].' '.$this->lang->line('Seat').')';?></h6> 
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <h6 style="font-size:14px; color:#175091; margin: 0px 0px;"><?=$_GET['return_seat_price'].' '.$_GET['return_currency_sign'];?></h6> 
                </div>
                <hr style="margin: 3px 0px;">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                  <h6 style="font-size:14px; color:#3498db; margin: 0px 0px;"><?=$this->lang->line('Grand Total')?></h6> 
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <h6 style="font-size:14px; color:#3498db; margin: 0px 0px;"><?=($_GET['seat_price'] + $_GET['return_seat_price'])*$_GET['no_of_seat'] .' '.$_GET['ownward_currency_sign'];?></h6> 
                </div>
              <?php } else { ?>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                  <h6 style="font-size:14px; color:#3498db; margin: 0px 0px;"><?=$this->lang->line('Grand Total')?></h6> 
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                  <h6 style="font-size:14px; color:#3498db; margin: 0px 0px;"><?=($_GET['seat_price'] * $_GET['no_of_seat']).' '.$_GET['ownward_currency_sign'];?></h6> 
                </div>
              <?php } ?>
            </div>
          </div>
          <br />
        </div>
        <!-- fare details -->

        <!-- Map Rout -->
        <div class="panel panel-register-box"><br/>
          <div class="hr-title hr-long center" style="margin:-5px auto 0px;width: 82%;"><abbr style="font-size: smaller;"><?=$this->lang->line('direction_on_map');?></abbr>
          </div>
          <div class="row">
            <div class="col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
              <div class="row" style="padding-bottom:13px">
                <iframe
                  id="map_frame"
                  width="100%"
                  height="200"
                  frameborder="0" style="border:0"
                  src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin=<?=$pickup_lat_lng?>&destination=<?=$drop_lat_lng?>" >
                </iframe>
              </div>
            </div>
          </div>
        </div>
        <!-- Map Rout -->
        
      </div>
    </div>      
  </div>
</section>

<!-- booking script -->
<script> 
  /*$('#bookingform').validate({
    rules: {
      mobile0: {
        required: true,
        number: true,
        minlength: 6,
        maxlength: 16,
      },
      age0: {
        required: true,
      },
      email_id0: {
        required: true,
        email: true,
      },
      firstname0: {
        required: true,
      },
      lastname0: {
        required: true,
      },
      gender0: {
        required: true,
      },
      country_id0: {
        required: true,
      },
    }
  });*/
</script>

<!-- booking script -->
<!-- login script -->
  <script>
    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    $("#timezone").val(timezone);

    function onLinkedInLoad() {
      LinkedINAuth();
      IN.Event.on(IN, "auth", function () { getProfileData(); });
      // IN.Event.on(IN, "logout", function () { onLinkedInLogout(); });
    }

    function onSuccess(data) {
      // console.log(data);
      $("#email").val(data["emailAddress"]);
      $("#password").val(data["id"]);
      $("#login_type").val("linkedin");
      $("#login_form").get(0).submit();
    }
    function onError(error) { /*console.log(error);*/ }
  </script>

  <!-- facebook -->
  <script>
    function fbLogin() {
      FB.login(function (response) {      
        if (response.authResponse) {  setFbUserData(); } 
      },{scope: 'email'});        
    }

    function setFbUserData() {
      FB.api('/me', {locale: 'en_US', fields: 'id,email'}, function (data) {    
        // console.log(data['picture']['data']['url']);
        $("#email").val(data["email"]);
        $("#password").val(data["id"]);
        $("#login_type").val("facebook");
        $("#login_form").get(0).submit();
      });
    }
  </script>
<!-- login script -->

<!-- sign-up script -->
  <script>
    $(function(){
      $("#country_id").on('change', function(event) { event.preventDefault();
        var id = $.trim($(this).val());
        var countries = <?= json_encode($countries); ?>;
        if(id != "" ) {
          $.each(countries, function(i, v){
            if(id == v['country_id']){  $("#country_code").val((v['country_phonecode'])); }
          });
        } else { $("#country_code").val(''); }
      });

      $(".register_as").on('change', function(event) {  event.preventDefault();
        var as = $(this).val();
        if(as == 0){  $(".company_div").removeClass('hidden');  }
        else {  $(".company_div").addClass('hidden'); $("#company_name").val(''); }
      });

      $("#icon_buyer, #lbl_buyer").on('click', function(event) {  event.preventDefault();
        $(this).addClass('text-info').css("background-color","#dfe0e3");
        $("#buyer").val('1'); $("#seller, #both").val('0');
        $("#icon_seller, #icon_both").removeClass('text-info').css("background-color","transparent");
      });

      $("#icon_seller, #lbl_seller").on('click', function(event) {  event.preventDefault();
        $(this).addClass('text-info').css("background-color","#dfe0e3");
        $("#seller").val('1'); $("#buyer, #both").val('0');
        $("#icon_buyer, #icon_both").removeClass('text-info').css("background-color","transparent");
      });

      $("#icon_both, #lbl_both").on('click', function(event) {  event.preventDefault();
        $(this).addClass('text-info').css("background-color","#dfe0e3");
        $("#both").val('1'); $("#seller, #buyer").val('0');
        $("#icon_seller, #icon_buyer").removeClass('text-info').css("background-color","transparent");
      });

      $("#btn_submit").on('click', function(event) {event.preventDefault();
        var buyer = $("#buyer").val();
        var seller = $("#seller").val();
        var both = $("#both").val();
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var repeatpassword = $("#repeatpassword").val();
        var country_id = $("#country_id").val();
        var country_code = $("#country_code").val();
        var mobile_no = $("#mobile_no").val();
        var captcha = $("#captcha").val();
        
        var err = <?= json_encode($this->lang->line('error_account_type'));?>;
        var err_firstname = <?= json_encode($this->lang->line('enter_first_name'));?>;
        var err_lastname = <?= json_encode($this->lang->line('enter_last_name'));?>;
        var err_email = <?= json_encode($this->lang->line('enter_email_address'));?>;
        var err_password = <?= json_encode($this->lang->line('enter_new_password'));?>;
          var err_password_alpha_numeric = <?= json_encode($this->lang->line('A password should have one alphabet and a number.'));?>;
          var err_password_length = <?= json_encode($this->lang->line('A password should have 6 characters.'));?>;
        var err_repeatpassword = <?= json_encode($this->lang->line('enter_rewrite_password'));?>;
        var err_country_id = <?= json_encode($this->lang->line('select_country'));?>;
        var err_country_code = <?= json_encode($this->lang->line('country_code'));?>;
        var err_mobile_no = <?= json_encode($this->lang->line('enter_mobile_number'));?>;
        var err_captcha = <?= json_encode($this->lang->line('enter_captcha'));?>;
        
        //remove previous errors
        $("#error_firstname").css({"display":"none"}).html('');
        $("#error_lastname").css({"display":"none"}).html('');
        $("#error_password").css({"display":"none"}).html('');
        $("#error_repeatpassword").css({"display":"none"}).html('');
        $("#error_country_id").css({"display":"none"}).html('');
        $("#error_mobile_no").css({"display":"none"}).html('');

        var pass_digits = password.replace(/\D/g, '').length;
        var rx = /[a-zA-Z]/gi;
      var m = password.match(rx);
      if (m) { pass_alpha = m.length; } else { pass_alpha = 0; }
        
        if (firstname =='') {
          $("#error_firstname").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_firstname) +"<br/>";
          $( "#firstname" ).focus();
        } else if (lastname =='') {
          $("#error_lastname").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_lastname) +"<br/>";
          $( "#lastname" ).focus();
        } else if (email =='') {
          $("#error_email").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_email) +"<br/>";
          $( "#email" ).focus();
        } else if (password =='') {
          $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password) +"<br/>";
          $( "#password" ).focus();
        //} else if ( pass_digits < 1 || pass_alpha < 1 ) {
        } else if ( false ) {
          $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password_alpha_numeric) +"<br/>";
          $( "#password" ).focus();
          } else if ( str.length < 6 ) {
            $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password_length) +"<br/>";
            $( "#password" ).focus();
          } else if (repeatpassword =='') {
          $("#error_repeatpassword").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_repeatpassword) +"<br/>";
          $( "#repeatpassword" ).focus();
        } else if (country_id =='') {
          $("#error_country_id").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_country_id) +"<br/>";
          $( "#country_id" ).focus();
        } else if (country_code =='') {
          $("#error_country_code").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_country_code) +"<br/>";
          $( "#country_code" ).focus();
        } else if (mobile_no =='') {
          $("#error_mobile_no").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_mobile_no) +"<br/>";
          $( "#mobile_no" ).focus();
        } else if (captcha =='') {
          $("#error_captcha").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_captcha) +"<br/>";
          $( "#captcha" ).focus();
        } else if(buyer =='0' && seller == '0' && both == '0'){
          $("#error_account_type").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err) +"<br/>";
        } else{ 
          $("#error_account_type").html("");  
          $("#register_form").get(0).submit();
        }
      });
    });
  </script>

  <!-- LinkedIn -->
  <script>
    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    $("#timezone").val(timezone);
    
    function onLinkedInLoad() {  LinkedINAuth(); IN.Event.on(IN, "auth", function () { getProfileData(); });}

    function onSuccess(data) {
      // console.log(data);
      $("#email").val(btoa(data["id"])+'@linkedin.com').attr('readonly',true);
      $("#password,#repeatpassword").val(data["id"]).attr('readonly',true);
      $("#login_type").val("linkedin");
      $("#avatar_url").val(data["pictureUrl"]);
      $("#firstname").val(data["firstName"]);
      $("#lastname").val(data["lastName"]);
    }
    function onError(error) { /*console.log(error);*/ }
  </script> 
  <!-- End - LinkedIn -->

  <!-- facebook -->
  <script>
    function fbLogin_signup() {
      FB.login(function (response) {      
        if (response.authResponse) {  setFbUserData_signup(); } 
      },{scope: 'email'});        
    }

    function setFbUserData_signup() {
      FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,gender,picture,cover'}, function (data) {    
        console.log(data);      
        $("#email").val(btoa(data["id"])+'@facebook.com').attr('readonly',true);
        $("#password,#repeatpassword").val(data["id"]).attr('readonly',true);
        $("#login_type").val("facebook");
        $("#avatar_url").val(data['picture']['data']['url']);
        $("#cover_url").val(data['cover']['source']);
        $("#firstname").val(data["first_name"]);
        $("#lastname").val(data["last_name"]);
        $("#gender").val(data["gender"]);   
      });
    }
  </script>
  <!-- End - Facebook -->

  <script>
    $('#email').focusout(function() {
      var email = $('#email').val();
      $.ajax({
          type: "POST", 
          url: "check-email-exists", 
          data: { user_email:email },
          dataType: "json",
          success: function(res){   
             console.log(res);
             if(res == 1) { $('#error_email').text("<?=$this->lang->line('email_exists')?>"); $('#error_email').focus(); }
             else { $('#error_email').text(""); }
          },
          beforeSend: function(){ },
          error: function(){ }
      });
    });
  </script>
<!-- sign-up script -->

<!-- End - Facebook -->
<script>
  $("#sign_up_now").on('click', function(event) {event.preventDefault();
    $("#sign_up").click();
  });
</script>