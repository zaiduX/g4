<!-- Terms -->
<section id="features" style="padding-top: 0px;padding-bottom: 10px;">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
        <h2><span class="text-info"><?= $this->lang->line('terms_and'); ?> </span><?= $this->lang->line('conditions'); ?></h2>
      </div>
    </div>
    <div class="row m-t-lg">
      <div class="col-md-5 col-md-offset-1" style="border-right: 2px solid #31708f">
        <strong><?= $this->lang->line('user_commitments'); ?></strong>
        <h6><?= $this->lang->line('user_agrees_to'); ?>...</h6>
        <p><?= $this->lang->line('user_commitments1'); ?></p>
        <p><?= $this->lang->line('user_commitments2'); ?></p>
        <p><?= $this->lang->line('user_commitments3'); ?></p>
        <p><?= $this->lang->line('user_commitments4'); ?></p>
        <p><?= $this->lang->line('user_commitments5'); ?></p>
        <p><?= $this->lang->line('user_commitments6'); ?></p>
      </div>
      <div class="col-md-5">
        <strong><?= $this->lang->line('platform_commitments'); ?></strong>
        <p><?= $this->lang->line('platform_commitments1'); ?></p>
        <p><?= $this->lang->line('platform_commitments2'); ?></p>
        <p><?= $this->lang->line('platform_commitments3'); ?></p>
        <p><?= $this->lang->line('platform_commitments4'); ?></p>
      </div>
    </div>
  </div>
</section>
<!-- END: Terms -->