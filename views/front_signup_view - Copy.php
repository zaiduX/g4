<style type="text/css">
.left-inner-addon {
    position: relative;
}
.left-inner-addon input {
    padding-left: 30px;    
}
.left-inner-addon i {
    position: absolute;
    padding: 16px 12px;
    pointer-events: none;
}
.custom_label {
    color: #175091;
    display: inline-block;
    margin-top: 0px;
    margin-bottom: 2px;
    font-size: 13px;
  }

  .error {
    margin-top: 0px !important;
  }
</style>
<section class="p-t-10 m-t-0 background-grey" style="background: url(<?= $this->config->item('resource_url') . 'web/front_site/images/bg.jpg'; ?>); padding-bottom: 0px;">
<?php  if($this->session->flashdata('error') != NULL): ?>
<section class="p-t-20 p-b-0 m-b-10 ">
  <div class="container">
    <div class="row">
      <div role="alert" class="alert alert-danger text-center col-md-8 col-md-offset-2">
        <strong><span class="ai-warning"><?= $this->lang->line('failed'); ?></span>!</strong> <?= $this->session->flashdata('error'); ?>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-register-box"><br />
          <!--Login Form-->
          <div class="hr-title hr-long center" style="margin:0px auto 5px; width: 80% !important;"><abbr><?= $this->lang->line('register_form'); ?></abbr> </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              
              <div class="row">
                <div class="col-md-6 text-center">
                  <button name="btn_social" class="btn-social loginBtn loginBtn--facebook" onclick="fbLogin();"> <?= $this->lang->line('btn_fb'); ?> </button>                    
                </div>
                <div class="col-md-6 text-center">
                  <button name="btn_social" class="btn-social loginBtn loginBtn--linkedin" onclick="onLinkedInLoad()"> <?= $this->lang->line('btn_li'); ?> </button>                  
                </div>
              </div>

              <div class="seperator"  style="margin: 10px 0px 0px 0px !important"><span><?= $this->lang->line('or_separator'); ?></span></div>

              <form action="<?= base_url();?>register-new-user" method="post" id="register_form" autocomplete="off">
                <input type="hidden" id="login_type" name="login_type" />
                <input type="hidden" id="avatar_url" name="avatar_url" />
                <input type="hidden" id="cover_url" name="cover_url" />
                <input type="hidden" id="gender" name="gender" />
                <input type="hidden" id="timezone" name="timezone" />
                <input type="hidden" id="cpt" name="cpt" value="<?= $this->session->userdata('captcha_code');?>" />
                
                <div class="row"> 
                  <div class="col-md-6 custom_label">
                      <label class=""><?= $this->lang->line('first_name'); ?></label>
                      <input type="text" class="form-control" placeholder="<?= $this->lang->line('first_name'); ?>" name="firstname" id="firstname" />
                      <label class="text-center" id="error_firstname"></label>
                  </div>  
                  <div class="col-md-6 custom_label">
                      <label class=""><?= $this->lang->line('last_name'); ?></label>
                      <input type="text" class="form-control" placeholder="<?= $this->lang->line('last_name'); ?>" name="lastname" id="lastname" />
                      <label class="text-center" id="error_lastname"></label>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-md-12 custom_label">
                    <label class=""><?= $this->lang->line('email'); ?></label>
                    <input type="email" class="form-control" placeholder="<?= $this->lang->line('email'); ?>" name="email" id="email" />
                    <label class="text-center" id="error_email" style="color: red;"></label>
                  </div>
                </div>
                
                <div class="row"> 
                  <div class="col-md-6 custom_label">
                    <label class=""><?= $this->lang->line('password'); ?></label>
                    <input type="password" class="form-control" placeholder="<?= $this->lang->line('password'); ?>" name="password" id="password" />
                    <label class="text-center" id="error_password"></label>
                  </div>  
                  <div class="col-md-6 custom_label">
                      <label class=""><?= $this->lang->line('confirm_password'); ?></label>
                      <input type="password" class="form-control" placeholder="<?= $this->lang->line('confirm_password'); ?>" name="repeatpassword" id="repeatpassword" />
                      <label class="text-center" id="error_repeatpassword"></label>
                  </div>  
                </div> 

                <div class="row"> 
                  <div class="col-md-4 custom_label">
                    <label class=""><?= $this->lang->line('select_country'); ?></label>
                    <select name="country_id" id="country_id" class="form-control select2">
                      <option value=""> <?= $this->lang->line('select_country'); ?></option>
                      <?php foreach ($countries as $c) :?>
                        <option value="<?= $c['country_id'];?>"><?=$c['country_name'];?></option>
                      <?php endforeach; ?>
                    </select>
                    <label class="text-center" id="error_country_id"></label>
                  </div>
                  <div class="col-md-3 custom_label">
                      <label class=""><?= $this->lang->line('country_code'); ?> <i class="fa fa-plus"></i></label>
                      <div class="left-inner-addon">
                        <input type="text" class="form-control" id="country_code" name="country_code" readonly />
                        <label class="text-center" id="error_country_code"></label>
                      </div>
                  </div>
                  <div class="col-md-5 custom_label">
                    <label class=""><?= $this->lang->line('mobile_no'); ?></label>
                    <input type="text" class="form-control" placeholder="<?= $this->lang->line('mobile_no'); ?>" name="mobile_no" id="mobile_no">
                    <label class="text-center" id="error_mobile_no"></label>
                  </div>
                </div>  

                <div class="row">
                  <div class="col-md-4"><hr/></div>
                  <div class="col-md-4 text-center"><h6 class="text-info" style="margin-bottom: 0px;"><?= $this->lang->line('register_as'); ?></h6></div>
                  <div class="col-md-4"><hr/></div>
                </div>
                
                <div class="row">
                  <div class="col-md-5 col-md-offset-1 custom_label">
                    <input type="radio" name="register_as" id="as_individual" autocomplete="off" checked value="1" class="col-md-1 register_as">  
                    <label for="as_individual" class="control-label"> &nbsp; 
                      <i class="fa fa-user"></i> <span class=""><?= $this->lang->line('as_individual'); ?> </span>
                    </label>
                  </div>  
                  <div class="col-md-6 custom_label">
                    <input type="radio" name="register_as" id="as_company" autocomplete="off" value="0" class="col-md-1 register_as ">
                    <label for="as_company" class="control-label"> &nbsp; 
                      <i class="fa fa-users"></i> <span class=""><?=$this->lang->line('as_company');?> </span>
                    </label>
                  </div>   
                </div>

                <div class="row"> 
                  <div class="col-md-12">
                    <div class="company_div hidden">
                      <label for="company_name" class="custom_label"><?= $this->lang->line('compnay_name'); ?></label>
                      <input type="text" class="form-control" placeholder="<?= $this->lang->line('compnay_name'); ?>" name="company_name" id="company_name" />
                    </div>
                  </div>                            
                </div>  
                
                <div class="row">
                  <div class="col-md-4"> <hr/></div>
                  <div class="col-md-4 text-center"><h6 class="text-info"><?= $this->lang->line('account_type'); ?></h6></div>
                  <div class="col-md-4"> <hr/></div>
                </div>
                <style>
                  .box-buyer{
                    margin-top: 0px;
                    height: 80px;
                    border: 1px solid #999;
                    width: 100px;
                    cursor: pointer;
                    width: 100%;
                    text-align: center;
                    background-image: url("<?=base_url('resources/web/front_site/images/buyer.png')?>");
                    background-repeat: no-repeat;
                    background-size: 92px;
                    background-position-x: center;
                    background-position-y: center;
                  }
                  .box-seller{
                    margin-top: 0px;
                    height: 80px;
                    border: 1px solid #999;
                    width: 100px;
                    cursor: pointer;
                    width: 100%;
                    text-align: center;
                    background-image: url("<?=base_url('resources/web/front_site/images/seller.png')?>");
                    background-repeat: no-repeat;
                    background-size: 80px;
                    background-position-x: center;
                  }
                  .box-both{
                    margin-top: 0px;
                    height: 80px;
                    border: 1px solid #999;
                    width: 100px;
                    cursor: pointer;
                    width: 100%;
                    text-align: center;
                    background-image: url("<?=base_url('resources/web/front_site/images/both.png')?>");
                    background-repeat: no-repeat;
                    background-size: 100px;
                    background-position-x: center;
                    background-position-y: bottom;
                  }
                  .icon {
                    display: block;
                    font-size: 60px;
                    font-size: 6rem;
                    height: 78px;
                    line-height: 80px;
                    background-color: transparent;
                    margin-bottom: 15px;
                    text-align: center;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    -webkit-background-clip: padding-box;
                    -moz-background-clip: 'padding';
                    background-clip: padding-box;
                    width: 100%;
                  }
                </style>
                <div class="row"> 
                  <div class="col-md-4 custom_label text-center">
                    <div class="box-buyer" id="icon_buyer">
                    </div>
                    <label id="lbl_buyer"><?= $this->lang->line('buyer'); ?></label>
                    <input type="hidden" name="buyer" id="buyer" value="0" />
                  </div>                      
                  <div class="col-md-4 custom_label text-center">
                    <div class="box-seller" id="icon_seller">
                    </div>
                    <label id="lbl_seller"><?= $this->lang->line('seller'); ?></label>
                    <input type="hidden" name="seller" id="seller" value="0" />
                  </div>
                  <div class="col-md-4 custom_label text-center">
                    <div class="box-both" id="icon_both">
                    </div>
                    <label id="lbl_both"><?= $this->lang->line('both'); ?></label>
                    <input type="hidden" name="both" id="both" value="0" />
                  </div>
                  <label class="text-center" id="error_account_type"></label>
                  <h6 class="text-mute" style="padding-left: 15px; margin-top: -5px;">
                    <span class="fa fa-angle-double-right"></span><?= $this->lang->line('account_type_line1'); ?><br/>
                    <span class="fa fa-angle-double-right"></span><?= $this->lang->line('account_type_line2'); ?>
                  </h6>                          
                </div>

                <div class="row">
                  <div class="col-md-4 custom_label">
                    <label class=""><?= $this->lang->line('are_you_human'); ?></label>                          
                    <img src="<?= $this->config->item('resource_url').'captcha/'.$data['image']; ?>" alt="<?= $data['image']; ?>" />
                  </div>
                  <div class="col-md-8 custom_label">
                    <label class=""><?= $this->lang->line('txt_captcha'); ?></label>                          
                    <input type="text" name="captcha" id="captcha" class="form-control" placeholder="<?= $this->lang->line('txt_captcha'); ?>" />
                  </div>                          
                </div>

                <div class="row">
                  <div class="col-md-6 col-md-offset-3 custom_label">
                    <button id="btn_submit" class="button blue-dark form-control" type="submit" style="border-radius: 3px;"><?= $this->lang->line('btn_signup'); ?></button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>  
      </div>
    </div>    
  </div>
</section>

<script>
  $(function(){
    $("#country_id").on('change', function(event) { event.preventDefault();
      var id = $.trim($(this).val());
      var countries = <?= json_encode($countries); ?>;
      if(id != "" ) {
        $.each(countries, function(i, v){
          if(id == v['country_id']){  $("#country_code").val((v['country_phonecode'])); }
        });
      } else { $("#country_code").val(''); }
    });

    $(".register_as").on('change', function(event) {  event.preventDefault();
      var as = $(this).val();
      if(as == 0){  $(".company_div").removeClass('hidden');  }
      else {  $(".company_div").addClass('hidden'); $("#company_name").val(''); }
    });

    $("#icon_buyer, #lbl_buyer").on('click', function(event) {  event.preventDefault();
      $(this).addClass('text-info').css("background-color","#dfe0e3");
      $("#buyer").val('1'); $("#seller, #both").val('0');
      $("#icon_seller, #icon_both").removeClass('text-info').css("background-color","transparent");
    });

    $("#icon_seller, #lbl_seller").on('click', function(event) {  event.preventDefault();
      $(this).addClass('text-info').css("background-color","#dfe0e3");
      $("#seller").val('1'); $("#buyer, #both").val('0');
      $("#icon_buyer, #icon_both").removeClass('text-info').css("background-color","transparent");
    });

    $("#icon_both, #lbl_both").on('click', function(event) {  event.preventDefault();
      $(this).addClass('text-info').css("background-color","#dfe0e3");
      $("#both").val('1'); $("#seller, #buyer").val('0');
      $("#icon_seller, #icon_buyer").removeClass('text-info').css("background-color","transparent");
    });

    $("#btn_submit").on('click', function(event) {event.preventDefault();
      var buyer = $("#buyer").val();
      var seller = $("#seller").val();
      var both = $("#both").val();
      var firstname = $("#firstname").val();
      var lastname = $("#lastname").val();
      var email = $("#email").val();
      var password = $("#password").val();
      var repeatpassword = $("#repeatpassword").val();
      var country_id = $("#country_id").val();
      var country_code = $("#country_code").val();
      var mobile_no = $("#mobile_no").val();
      var captcha = $("#captcha").val();
      
      var err = <?= json_encode($this->lang->line('error_account_type'));?>;
      var err_firstname = <?= json_encode($this->lang->line('enter_first_name'));?>;
      var err_lastname = <?= json_encode($this->lang->line('enter_last_name'));?>;
      var err_email = <?= json_encode($this->lang->line('enter_email_address'));?>;
      var err_password = <?= json_encode($this->lang->line('enter_new_password'));?>;
      var err_password_alpha_numeric = <?= json_encode($this->lang->line('A password should have one alphabet and a number.'));?>;
      var err_password_length = <?= json_encode($this->lang->line('A password should have 6 characters.'));?>;
      var err_repeatpassword = <?= json_encode($this->lang->line('enter_rewrite_password'));?>;
      var err_country_id = <?= json_encode($this->lang->line('select_country'));?>;
      var err_country_code = <?= json_encode($this->lang->line('country_code'));?>;
      var err_mobile_no = <?= json_encode($this->lang->line('enter_mobile_number'));?>;
      var err_captcha = <?= json_encode($this->lang->line('enter_captcha'));?>;
      
      if (firstname =='') {
        $("#error_firstname").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_firstname) +"<br/>";
        $( "#firstname" ).focus();
      } else if (lastname =='') {
        $("#error_lastname").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_lastname) +"<br/>";
        $( "#lastname" ).focus();
      } else if (email =='') {
        $("#error_email").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_email) +"<br/>";
        $( "#email" ).focus();
      } else if (password =='') {
        $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password) +"<br/>";
        $( "#password" ).focus();
      } else if (!password.match('/^[a-zA-Z0-9]+/') ) {
        $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password_alpha_numeric) +"<br/>";
        $( "#password" ).focus();
      } else if ( str.length < 6 ) {
        $("#error_password").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_password_length) +"<br/>";
        $( "#password" ).focus();
      } else if (repeatpassword =='') {
        $("#error_repeatpassword").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_repeatpassword) +"<br/>";
        $( "#repeatpassword" ).focus();
      } else if (country_id =='') {
        $("#error_country_id").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_country_id) +"<br/>";
        $( "#country_id" ).focus();
      } else if (country_code =='') {
        $("#error_country_code").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_country_code) +"<br/>";
        $( "#country_code" ).focus();
      } else if (mobile_no =='') {
        $("#error_mobile_no").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_mobile_no) +"<br/>";
        $( "#mobile_no" ).focus();
      } else if (captcha =='') {
        $("#error_captcha").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err_captcha) +"<br/>";
        $( "#captcha" ).focus();
      } else if(buyer =='0' && seller == '0' && both == '0') {
        $("#error_account_type").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err) +"<br/>";
      } else { 
        $("#error_account_type").html("");  
        $("#register_form").get(0).submit();
      }
    });
  });
</script>

<!-- LinkedIn -->
<script>
  var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  $("#timezone").val(timezone);
  
  function onLinkedInLoad() {  LinkedINAuth(); IN.Event.on(IN, "auth", function () { getProfileData(); });}

  function onSuccess(data) {
    // console.log(data);
    $("#email").val(btoa(data["id"])+'@linkedin.com').attr('readonly',true);
    $("#password,#repeatpassword").val(data["id"]).attr('readonly',true);
    $("#login_type").val("linkedin");
    $("#avatar_url").val(data["pictureUrl"]);
    $("#firstname").val(data["firstName"]);
    $("#lastname").val(data["lastName"]);
  }
  function onError(error) { /*console.log(error);*/ }
</script> 
<!-- End - LinkedIn -->

<!-- facebook -->
<script>
  function fbLogin() {
    FB.login(function (response) {      
      if (response.authResponse) {  setFbUserData(); } 
    },{scope: 'email'});        
  }

  function setFbUserData() {
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,gender,picture,cover'}, function (data) {    
      console.log(data);      
      $("#email").val(btoa(data["id"])+'@facebook.com').attr('readonly',true);
      $("#password,#repeatpassword").val(data["id"]).attr('readonly',true);
      $("#login_type").val("facebook");
      $("#avatar_url").val(data['picture']['data']['url']);
      $("#cover_url").val(data['cover']['source']);
      $("#firstname").val(data["first_name"]);
      $("#lastname").val(data["last_name"]);
      $("#gender").val(data["gender"]);   
    });
  }
</script>
<!-- End - Facebook -->

<script>
  $('#email').focusout(function() {
    var email = $('#email').val();
    $.ajax({
      type: "POST", 
      url: "check-email-exists", 
      data: { user_email:email },
      dataType: "json",
      success: function(res){   
        console.log(res);
        if(res == 1) { $('#error_email').text("<?=$this->lang->line('email_exists')?>"); $('#error_email').focus(); }
        else { $('#error_email').text(""); }
      },
      beforeSend: function(){ },
      error: function(){ }
    });
  });
</script>