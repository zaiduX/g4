<section class="p-t-40 m-t-0 background-grey" style="background: url(<?= $this->config->item('resource_url') . 'web/front_site/images/bg.jpg'; ?>);">
	<section class="p-t-20 p-b-0 m-b-10 ">
		<?php if(($this->session->flashdata('error') != NULL) OR ( $this->session->flashdata('success') != NULL) ): ?>
			<div class="container">
				<div class="row">
					<?php if($this->session->flashdata('error')): ?>
					<div role="alert" class="alert alert-danger text-center col-md-8 col-md-offset-2">
						<strong><span class="ai-warning">Failed</span>!</strong> <?= $this->session->flashdata('error'); ?>
					</div>
				<?php elseif($this->session->flashdata('success')): ?>
					<div role="alert" class="alert alert-success text-center col-md-8 col-md-offset-2">
						<strong><span class="ai-warning">Success</span>!</strong> <?= $this->session->flashdata('success'); ?>
					</div>
				<?php endif; ?>

				</div>
			</div>
		<?php endif; ?>		
		
	</section>

	<div class="container">
	  	<div class="row">
      		<div class="col-md-6 col-md-offset-3">
        		<div class="panel panel-register-box">
					<div class="hr-title hr-long center" style="margin-bottom: 15px !important"><abbr><?= $this->lang->line('verify_otp'); ?></abbr> </div>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<!-- <form action="<?= base_url();?>confirm/otp" method="post" id="verify_otp" autocomplete="off">
						  		<div class="row">
                					<div class="col-md-12">
										<div class="form-group">
											<label for="cf_otp" class="text-center control-label font-light" style="color: #337ab7 !important; font-weight: 400"><?= $this->lang->line('verify_otp2'); ?></label>
											<input type="text" class="form-control required" placeholder="<?= $this->lang->line('verify_otp2'); ?>" id="cf_otp" name="cf_otp" autofocus required />
										</div>
									</div>
						  		</div>
							
								<div class="row">
									<div class="col-md-6 col-sm-6 text-left">
										<button type="submit" class="button blue-dark"> <?= $this->lang->line('btn_otp'); ?></button>
									</div>
									<div class="col-md-6 col-sm-6 text-right">
										<a href="<?= base_url('resend-otp');?>"><small><?= $this->lang->line('btn_resend_otp'); ?></small></a>
									</div> 
								</div> 
							</form> -->
							<form action="<?= base_url();?>confirm/otp" method="post" id="verify_otp" autocomplete="off">
						  		<div class="row">
                					<div class="col-md-12">
										<div class="form-group">
											<label for="cf_otp" class="text-center control-label font-light" style="color: #337ab7 !important; font-weight: 400"><?= $this->lang->line('verify_otp2'); ?></label>
											<input type="text" class="form-control required" placeholder="<?= $this->lang->line('verify_otp2'); ?>" id="cf_otp" name="cf_otp" autofocus required />
											
											<input type="hidden" name="job_id" value="<?=$support_details['job_id'];?>">
										</div>
									</div>
						  		</div>
							
								<div class="row">
									<div class="col-md-6 col-sm-6 text-left">
										<button type="submit" class="button blue-dark"> <?= $this->lang->line('btn_otp'); ?></button>
									</div>
									<div class="col-md-6 col-sm-6 text-right">
										<a href="<?= base_url('resend-otp');?>"><small><?= $this->lang->line('btn_resend_otp'); ?></small></a>
									</div> 
								</div> 
							</form>
						</div>
					</div>
					<br />
				</div>	
			</div>
		</div>	
		<div class="row">
      		<div class="col-md-6 col-md-offset-3">
        		<div class="panel panel-register-box">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
					  		<div class="row">
            					<div class="col-md-12">
									<div class="form-group">
										<label class="text-center control-label font-light" style="color: #337ab7 !important; font-weight: 400"><?= $this->lang->line('Having problems verifying your account? Contact us:'); ?></label>
										<label class="text-center control-label font-light" style="font-weight: 400"><i class="fa fa-mobile"></i> <?=($support_details['support_phone']!='NULL')?$support_details['support_phone']:$support_details['phone']?> / <?=$support_details['phone']?></label>
										<label class="text-center control-label font-light" style="font-weight: 400"><i class="fa fa-envelope"></i> <?=($support_details['support_email']!='NULL')?$support_details['support_email']:$support_details['email']?> / <?=$support_details['email']?></label>
										<label class="text-center control-label font-light" style="font-weight: 400"><?=$support_details['company_name']?></label>
									</div>
								</div>
					  		</div>
						</div>
					</div>
					<br />
				</div>	
			</div>
		</div>	
	</div>
</section>