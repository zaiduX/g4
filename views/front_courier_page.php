<style>
  label{ font-weight: 10 !important; }
  .earth, .air, .sea { margin-top: 0px !important; }
  .checkbox-inline, .radio-inline { vertical-align: baseline !important; }
  .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] { margin-left: -30px !important; }
  .datepicker { background-color:white !important; }
  #timepickup:hover{ cursor: pointer; }
  #Deliverytime:hover{ cursor: pointer; }
</style>

<!-- SECTION IMAGE FULLSCREEN -->
<div class="w3-content w3-section" style="margin-top:0px !important; margin-bottom:0px !important; max-width: 100% !important;">
  <img class="mySlides" src="<?=base_url('resources/web/front_site/images/slider/courier.jpg');?>" style="width:100%">
  <div class="slider-content">
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden-xs">
        <h1 class="" style="-webkit-text-stroke: 1px black; font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('slider_heading1'); ?></span></h1>
        <p class="lead" style="font-weight: bold; color: white !important; margin-bottom: 30px !important;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('slider_heading2'); ?></span></p>
      </div>
      <div class="col-xs-12 hidden-xl hidden-lg hidden-md hidden-sm">
        <h6 class="" style="font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('slider_heading1'); ?></span></h6>
        <h6 style="color: white !important; margin-bottom: 30px !important;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('slider_heading2'); ?></span></h6>
      </div>
      <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 hidden-xs">
        <div class="search-form wow pulse" data-wow-delay="0.8s" style="margin-top: 0px; padding-top: 0; background-color: #ffffffd6 !important">
          <form action="<?=base_url('log-in-booking')?>" id="createForm">
            <input type="hidden" name="cat_id" value="7">

            <div class="row">
                <div class="col-md-6 form-group" style="padding-right: 0;">
                    <div class="col-md-12 text-left"> 
                        <label><i class="fa fa-map-marker"></i> <?= $this->lang->line('From'); ?></label>
                    </div>
                    <div class="col-md-12">
                        <input type="" name="from_address" class="form-control" id="fromMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>" autocomplete="off">
                        <input type="hidden" from-data-geo="lat" name="frm_latitude" id="frm_latitude" value="" />
                        <input type="hidden" from-data-geo="lng" name="frm_longitude" id="frm_longitude" value="" />
                        <input type="hidden" from-data-geo="formatted_address" name="frm_formatted_address" id="frm_formatted_address" value="" />
                        <input type="hidden" from-data-geo="postal_code" name="frm_postal_code" id="frm_postal_code" value="" />
                        <input type="hidden" from-data-geo="country" name="frm_country" id="frm_country" value="" />
                        <input type="hidden" from-data-geo="administrative_area_level_1" name="frm_state" id="frm_state" value="" />
                        <input type="hidden" from-data-geo="locality" name="frm_city" id="frm_city" value="" />
                        <div id="map" class="map_canvas hidden" style="width: 100%; height: 250px;"></div>
                    </div>
                </div>
                <div class="col-md-3 form-group" style="padding-right: 0; padding-left: 0;">
                    <div class="col-md-12" style="padding-right: 0px">
                        <label for="example-date-input" style="float:left;"><i class="fa fa-calendar"></i> <?= $this->lang->line('pickup_date'); ?><small>(MM/DD/YYYY)</small></label>
                    </div>
                    <div class="col-md-12"> 
                        <div class="input-group date">
                            <input type="text" class="form-control" id="datepickup" name="pickupdate" value="<?=date('m/d/Y', strtotime('+ 0 day'))?>" autocomplete="off" >
                            <div class="input-group-addon dpAddon">
                              <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 form-group" style="padding-left: 0;">
                    <div class="col-md-12">
                        <label for="example-time-input" style="float:left;"><i class="fa fa-clock-o"></i> <?= $this->lang->line('Pickup Time'); ?></label>
                    </div> 
                    <div class="col-md-12">
                        <div class="input-group clockpicker" data-autoclose="true">
                            <input type="text" class="form-control" id="timepickup" name="pickuptime" autocomplete="off" style="background-color: white !important;" readonly />
                            <span class="input-group-addon">
                              <span class="fa fa-clock-o"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-6 form-group" style="padding-right: 0;">
                <div class="col-md-12 text-left" style="margin-top: -15px"> 
                  <label><i class="fa fa-map-marker"></i> <?= $this->lang->line('To'); ?></label>
                </div>
                <div class="col-md-12">
                  <input type="" name="to_address" class="form-control" id="toMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>" autocomplete="off">
                  <input type="hidden" to-data-geo="lat" name="to_latitude" id="to_latitude" value="" />
                  <input type="hidden" to-data-geo="lng" name="to_longitude" id="to_longitude" value="" />
                  <input type="hidden" to-data-geo="formatted_address" name="to_formatted_address" id="to_formatted_address" value="" />
                  <input type="hidden" to-data-geo="postal_code" name="to_postal_code" id="to_postal_code" value="" />
                  <input type="hidden" to-data-geo="country" name="to_country" id="to_country" value="" />
                  <input type="hidden" to-data-geo="administrative_area_level_1" name="to_state" id="to_state" value="" />
                  <input type="hidden" to-data-geo="locality" name="to_city" id="to_city" value="" />
                  <div id="map" class="map_canvas hidden" style="width: 100%; height: 250px;"></div>
                </div>
              </div>
              <div class="col-md-3 form-group" style="padding-right: 0; padding-left: 0;">
                <div class="col-md-12" style="margin-top: -15px">
                  <label for="example-date-input" style="float:left;"><i class="fa fa-calendar"></i> <?= $this->lang->line('delivery_date'); ?><small>(MM/DD/YYYY)</small></label>
                </div> 
                <div class="col-md-12"> 
                  <div class="input-group date">
                    <input type="text" class="form-control" id="Deliverydate" name="deliverdate" value="<?=date('m/d/Y', strtotime('+ 0 day'))?>" autocomplete="off" >
                    <div class="input-group-addon ddAddon">
                      <span class="glyphicon glyphicon-th"></span>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="col-md-3 form-group" style="padding-left: 0;">
                <div class="col-md-12" style="margin-top: -15px">
                  <label for="example-time-input" style="float:left;"><i class="fa fa-clock-o"></i> <?= $this->lang->line('Delivery Time'); ?></label>
                </div>
                <div class="col-md-12">
                  <div class="input-group clockpicker" data-autoclose="true">
                    <input type="text" class="form-control" id="Deliverytime" name="delivertime" autocomplete="off" style="background-color: white !important;" readonly />
                    <span class="input-group-addon">
                      <span class="fa fa-clock-o"></span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 form-group" style="padding-right: 0;">
                <div class="col-md-12" style="margin-top: -15px">
                  <label style="float:left;"><i class="fa fa-car"></i> <?= $this->lang->line('transport_type'); ?></label>
                </div>
                <div class="col-md-12">
                  <div class="radio radio-info radio-inline">
                    <input type="radio" id="earth" value="earth" name="transport_type" checked="checked">
                    <label for="earth" class="earth"> <?= $this->lang->line('earth'); ?> </label>
                  </div>
                  <div class="radio radio-info radio-inline">
                    <input type="radio" id="air" value="air" name="transport_type">
                    <label for="air" class="air"> <?= $this->lang->line('air'); ?> </label>
                  </div>
                  <div class="radio radio-info radio-inline">
                    <input type="radio" id="sea" value="sea" name="transport_type">
                    <label for="sea" class="sea"> <?= $this->lang->line('sea'); ?> </label>
                  </div>
                </div>
              </div>
              <div class="col-md-4 form-group" style="padding-right: 0; padding-left: 0;">
                <div class="col-md-12" style="margin-top: -15px">
                  <label style="float:left;"><i class="fa fa-flask"></i> <?= $this->lang->line('standard_dimension'); ?></label>
                </div>
                <div class="col-md-12">
                  <select id="dimension" name="dimension_id" class="selectpicker" data-live-search="true" data-live-search-style="begins">
                    <option value=""><?= $this->lang->line('select_standard_dimension'); ?></option>
                    <?php foreach ($dimension as $i => $d) :?> 
                        <option value="<?= $d['dimension_id']; ?>"><?= $d['dimension_type']; ?></option>
                    <?php endforeach; ?>
                    <option value="-1"><?= $this->lang->line('other'); ?></option>
                  </select>
                </div>
              </div>
              <div class="col-md-4 form-group" style="padding-left: 0;">
                <div class="col-md-12" style="margin-top: -15px"> 
                  <label for="example-number-input" style="float:left;"><i class="fa fa-sort-numeric-asc"></i> <?= $this->lang->line('quantity'); ?></label>
                </div>
                <div class="col-md-12">
                  <input class="form-control" type="number" name="c_quantity" value="1" id="quantity" min="1">
                </div>
              </div> 
            </div>
            <div class="row">
              <div class="col-md-4 form-group" style="margin-bottom: -10px; padding-right: 0;">
                <div class="col-md-12" style="margin-top: -15px"> 
                  <label for="example-number-input" style="float:left;"><i class="fa fa-bars"></i> <?= $this->lang->line('weight')?> KG</label>
                </div>
                <div class="col-md-12">
                  <input class="form-control" type="number" name="c_weight" value="1" id="weight" min="1">
                </div>
              </div>
              <div class="col-md-4 form-group" style="margin-bottom: -10px; padding-right: 0; padding-left: 0;">
                <div class="col-md-12" style="margin-top: -15px">
                  <label style="float:left;"><i class="fa fa-car"></i> <?= $this->lang->line('select_vehicle'); ?></label>
                </div>
                <div class="col-md-12">
                  <select id="vehicle" name="vehicle" class="selectpicker" data-live-search="true" data-live-search-style="begins" style="background:rgba(252, 252, 252, 0.35)">
                    <option value=""><?= $this->lang->line('select_transport_vehicle'); ?></option>
                    <?php foreach ($default_trans as $v): ?> 
                    <option value="<?= $v['vehical_type_id']; ?>"><?= $v['vehicle_type']; ?></option>
                    <?php endforeach; ?> 
                  </select>
                </div>
              </div>
              <div class="col-md-4 form-group" style="margin-bottom: -10px; padding-left: 0;">
                <div class="col-md-12" style="margin-top: -15px"> 
                  <label for="example-number-input" style="float:left;">&nbsp;</label>
                </div>
                <div class="col-md-12">
                  <button class="btn btn-default btn-delivery booknowBtn form-control" type="button" id="btnFormCreate"><?= $this->lang->line('Book Now'); ?></button> 
                </div>
              </div>
            </div>  
          </form>
          <script>
            $("#fromMapID").geocomplete({
              map:".map_canvas",
              location: "",
              mapOptions: { zoom: 11, scrollwheel: true, },
              markerOptions: { draggable: true, },
              details: "form",
              detailsAttribute: "from-data-geo", 
              types: ["geocode", "establishment"],
            }); 
            $("#toMapID").geocomplete({
              map:".map_canvas",
              location: "",
              mapOptions: { zoom: 11, scrollwheel: true, },
              markerOptions: { draggable: true, },
              details: "form",
              detailsAttribute: "to-data-geo", 
              types: ["geocode", "establishment"],
            });
            $( document ).ready(function() {
              $(".clockpicker").clockpicker({ autoclose: true });
            });
            $("input[name=transport_type]").on("change",function(){
              //alert("radio button selected");
              var v = $("input[name=transport_type]:checked").val();
              //alert('Value for v is = ' + v);
              $('#vehicle').empty();
              $.ajax({
                type: "POST", 
                url: "get-transport", 
                data: { type: v, cat_id: 7 },
                dataType: "json",
                success: function(res){ 
                  $('#vehicle').attr('disabled', false);
                  $('#vehicle').empty(); 
                  $('#vehicle').append('<option value=""><?= $this->lang->line("select_transport_vehicle"); ?></option>');
                  $.each( res, function(i1, v1){
                    $('#vehicle').append('<option value="'+v1['vehical_type_id']+'">'+v1['vehicle_type']+'</option>');
                  });
                  $('#vehicle').focus();
                },
                beforeSend: function(){
                  $('#vehicle').empty();
                  $('#vehicle').append("<option value=''><?=$this->lang->line('loading');?></option>");
                },
                error: function(){
                  $('#vehicle').attr('disabled', true);
                  $('#vehicle').empty();
                  $('#vehicle').append("<option value=''><?=$this->lang->line('no_option');?></option>");
                }
              });
              $.ajax({
                type: "POST", 
                url: "get-standard-dimensions", 
                data: { type: v, cat_id : 7 },
                dataType: "json",
                success: function(res){ 
                  //if(v == 'air'){
                      // alert(JSON.stringify(res));
                  //}
                  $('#dimension').attr('disabled', false);
                  $('#dimension').empty(); 
                  $('#dimension').append('<option value=""><?= $this->lang->line("select_standard_dimension"); ?></option>');
                  $.each( res, function(i2, v2){
                    $('#dimension').append('<option value="'+v2['dimension_id']+'">'+v2['dimension_type']+'</option>');
                  });
                  $('#dimension').append('<option value="-1"> <?= $this->lang->line('other'); ?> </option>');

                  $('.dimensionPackage').attr('disabled', false);
                  $('.dimensionPackage').empty(); 
                  $('.dimensionPackage').append('<option value=""><?= $this->lang->line("select_standard_dimension"); ?></option>');
                  $.each( res, function(i3, v3){
                    $('.dimensionPackage').append('<option value="'+v3['dimension_id']+'">'+v3['dimension_type']+'</option>');
                  });
                  $('.dimensionPackage').append('<option value="-1"> <?= $this->lang->line('other'); ?> </option>');
                },
                beforeSend: function(){
                  $('#dimension').empty();
                  $('#dimension').append("<option value=''><?=$this->lang->line('loading')?></option>");
                  $('.dimensionPackage').empty();
                  $('.dimensionPackage').append("<option value=''><?=$this->lang->line('loading')?></option>");
                },
                error: function(){
                  $('#dimension').attr('disabled', true);
                  $('#dimension').empty();
                  $('#dimension').append("<option value=''><?=$this->lang->line('no_option')?></option>");
                  $('.dimensionPackage').attr('disabled', true);
                  $('.dimensionPackage').empty();
                  $('.dimensionPackage').append("<option value=''><?=$this->lang->line('no_option')?></option>");
                }
              });
            });
          </script>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END: SECTION IMAGE FULLSCREEN -->

<section class="p-b-0" style="padding-top: 10px;">
  <div class="container">
      <div class="heading heading-center" style="margin-bottom: 20px">
          <h2><?= $this->lang->line('Offers And Promocodes'); ?></h2>
          <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>
      </div>
      <div class="container" style="padding-bottom: 20px">
          <div class="row stylish-panel">
              <?php $i=0; foreach ($promocodes as $promo){ $i++; ?>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                      <div>
                          <div class="row">
                              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-1" >
                                  <img style="width:100px; height:70px;" src='<?=base_url($promo["promocode_avtar_url"])?>' alt="">
                              </div>
                              <div class="col-xl-8 col-lg-8 col-md-8 col-sm-11" >
                                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4" >
                                      <h6 style="margin: 0px 0px"> <?=$this->lang->line('Discount')?></h6>
                                  </div>
                                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" >
                                      <h6 style="margin: 0px 0px;"><?=$promo['discount_percent']?>%</h6>
                                  </div>
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding: 0px 0px;" >
                                      <h4 style="color: #5dccf7;"><?=$promo['promo_code']?></h4>
                                  </div>
                              </div>
                          </div>
                      </div>    <!--<h3><?=$promo['promo_code']?></h3>-->
                    <?php if($i==3 || $i==6) echo "<br/>"; ?>  
                  </div>

              <?php  } ?>
          </div>
      </div>
  </div>
</section>

<!-- How it works -->
<section class="background-grey" id="howworks" style="padding-top: 0px;padding-bottom: 0px;">
  <div class="container">
    <div class="heading heading-center text-center" style="margin-bottom: 10px;">
      <h2><?= $this->lang->line('how_it_works'); ?></h2>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
            <div class="icon-box effect center light square">
                <div class="icon"> <a href="#"><i class="fa fa-map-marker"></i></a> </div>
                <h4 align="center"><?= $this->lang->line('tile1'); ?></h4>
                <p align="center"><?= $this->lang->line('tile2'); ?></p>
            </div>
        </div>
        </div>
        <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
            <div class="icon-box effect  center light square">
                <div class="icon"> <a href="#"><i class="fa fa-envelope"></i></a> </div>
                <h4><?= $this->lang->line('tile5'); ?> </h4>
                <p align="center"><?= $this->lang->line('tile6'); ?></p>
            </div>
        </div>
        </div>
        <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
            <div class="icon-box effect  center light square">
                <div class="icon"> <a href="#"><i class="fa fa-pencil"></i></a> </div>
                <h4><?= $this->lang->line('tile9'); ?></h4>
                <p align="center"><?= $this->lang->line('tile10'); ?></p>
            </div>
        </div>
        </div>
        <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
            <div class="icon-box effect  center light square">
                <div class="icon"> <a href="#"><i class="fa fa-truck"></i></a> </div>
                <h4><?= $this->lang->line('tile13'); ?></h4>
                <p align="center"><?= $this->lang->line('tile14'); ?></p>
            </div>
        </div>
        </div>
      </div>

  </div>
</section>
<!-- END: How it works -->

<!-- CATEGORIES -->
<section class="p-b-0" style="padding-top: 10px;">
  <div class="container">
      <div class="heading heading-center" style="margin-bottom: 0px;">
          <h2><?= $this->lang->line('categories'); ?></h2>
          <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>
      </div>

      <!--
      <div class="section-content">
        <div class="row text-center color-border-bottom">
          <div class="col-xs-12 col-sm-6 col-md-3 col-md-offset-3 well">
              <i class="fa fa-truck fa-5x"></i> <br /><br />
              <h5 class="ac-title">Transport (freight)</h5>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 well">
              <i class="fa fa-envelope fa-5x"></i> <br /><br />
              <h5 class="ac-title">Courier & parcel delivery</h5>
          </div>
        </div>
      </div> -->
          <style type="text/css">
              .stylish-panel {
              padding: 20px 0;
              text-align: center;
          }
          .stylish-panel > div > div{
              padding: 10px;
              border: 1px solid #afe9ff;
              border-radius: 4px;
              transition: 0.2s;
          }
          .stylish-panel > div:hover > div{
              margin-top: -10px;
              border: 1px solid rgb(200, 200, 200);
              box-shadow: #afe9ff 0px 5px 5px 2px;
              background: rgba(200, 200, 200, 0.1);
              transition: 0.5s;
          }

          .stylish-panel > div:hover img {
              border-radius: 50%;
              -webkit-transform: rotate(360deg);
              -moz-transform: rotate(360deg);
              -o-transform: rotate(360deg);
              -ms-transform: rotate(360deg);
              transform: rotate(360deg);
          }
          </style>

      <div class="container" style="padding-bottom: 20px">
          <div class="row stylish-panel">
              <div class="col-md-4">
                  <div>
                      <a href="<?= base_url('sign-up') ?>">
                          <i class="fa fa-truck fa-5x"></i> <br /><br />
                          <h3><?= $this->lang->line('transportation'); ?></h3>
                      </a>
                  </div>
              </div>
              <div class="col-md-4">
                  <div>
                      <a href="<?= base_url('sign-up') ?>">
                          <i class="fa fa-envelope fa-5x"></i> <br /><br />
                          <h3><?= $this->lang->line('courier'); ?></h3>
                      </a>
                  </div>
              </div>
              <div class="col-md-4">
                  <div>
                      <a href="<?= base_url('sign-up') ?>">
                          <i class="fa fa-dropbox fa-5x"></i> <br /><br />
                          <h3><?= $this->lang->line('home_move'); ?></h3>
                      </a>
                  </div>
              <br />
              </div>
              <div class="col-md-4">
                  <div>
                      <a href="<?= base_url('sign-up') ?>">
                          <i class="fa fa-ticket fa-5x"></i> <br /><br />
                          <h3><?= $this->lang->line('Ticket Booking'); ?></h3>
                      </a>
                  </div>
              </div>
              <div class="col-md-4">
                  <div>
                      <a href="<?= base_url('sign-up') ?>">
                          <i class="fa fa-recycle fa-5x"></i> <br /><br />
                          <h3><?= $this->lang->line('Laundry Booking'); ?></h3>
                      </a>
                  </div>
              </div>
              <div class="col-md-4">
                  <div>
                      <a href="<?= base_url('sign-up') ?>">
                          <i class="fa fa-fire fa-5x"></i> <br /><br />
                          <h3><?= $this->lang->line('Gas Booking'); ?></h3>
                      </a>
                  </div>
              </div>
         </div>
      </div>

      <!--
      <ul class="grid grid-5-columns p-t-0">
          <?php foreach ($category_type as $cat) { ?>
              <li>
                  <div class="accordion toggle clean color-border-bottom">
                      <div class="ac-item">
                          <h5 class="ac-title"> <i class="fa fa-bitbucket"></i><?= $cat['cat_type'] ?></h5>
                          <div class="ac-content">
                              <?php $category = $this->api->get_category_by_id($cat['cat_type_id']); ?>
                              <?php foreach ($category as $c) { ?>
                                  <p><a href="#"><?= $c['cat_name'] ?></a></p> 
                              <?php } ?>
                          </div>
                      </div>
                  </div>  
              </li>
          <?php } ?>

        
        <li>
          <div class="accordion toggle clean color-border-bottom">
              <div class="ac-item">
                  <h5 class="ac-title"><i class="fa fa-cutlery"></i><?= $this->lang->line('resturant'); ?></h5>
                  <div class="ac-content">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat. </p> 
                  </div>
              </div>
          </div>  
        </li> 
        <li>
          <div class="accordion toggle clean color-border-bottom">
              <div class="ac-item">
                  <h5 class="ac-title"><i class="fa fa-industry"></i><?= $this->lang->line('domestic_gas'); ?></h5>
                  <div class="ac-content">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat. </p> 
                  </div>
              </div>
          </div>  
        </li> 
        <li>
          <div class="accordion toggle clean color-border-bottom">
              <div class="ac-item">
                  <h5 class="ac-title"><i class="fa fa-archive"></i><?= $this->lang->line('courier'); ?></h5>
                  <div class="ac-content">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat. </p> 
                  </div>
              </div>
          </div>  
        </li> 
        <li>
          <div class="accordion toggle clean color-border-bottom">
              <div class="ac-item">
                  <h5 class="ac-title"><i class="fa fa-truck"></i><?= $this->lang->line('transportation'); ?></h5>
                  <div class="ac-content">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat. </p> 
                  </div>
              </div>
          </div>  
        </li>
        
      </ul>-->

  </div>
</section>
<!-- END: CATEGORIES -->

<!-- OFFERS -->
<section class="parallax text-center text-light p-t-50 p-b-50 background-overlay" style="background-image: url('<?= $this->config->item('resource_url') . 'web/front_site/images/parallax/1.jpg';?>');" data-stellar-background-ratio="0.6">
  <div class="container">
    <div class="row">
      <div class="heading heading-center m-b-20">
        <h2><?= $this->lang->line('offer1'); ?></h2>
        <span class="lead"><?= $this->lang->line('offer2'); ?></span>
      </div>
      <!--<a href="<?= base_url().'sign-up'; ?>" class="button color rounded"><span><?= $this->lang->line('sign_up'); ?></span></a>  -->
    </div>
  </div>
</section>
<!-- END: OFFERS -->

<section id="benefits" style="padding-top: 010px;padding-bottom: 10px;">
  <div class="container">
     <div class="heading heading-center m-b-15">
          <h2><?= $this->lang->line('benefit'); ?></h2>
      </div> 
      <div class="col-md-4">
       <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
           <div><h3 style="color: #5dccf7;"><?= $this->lang->line('carriers_delivery'); ?></h3></div>
           <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/carrier.jpg'; ?>" class="img-responsive"></div><br />
           <div>
               <ul type="none">
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('benefit1'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('benefit2'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('benefit3'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('benefit4'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('benefit5'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('benefit6'); ?></span></li>
                      <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('benefit7'); ?></span></li>
               </ul>
           </div> 
        </div>
      </div>
      <div class="col-md-4">
         <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
         <div><h3 style="color: #5dccf7;"><?= $this->lang->line('Businesses'); ?></h3></div>
           <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/business.jpg'; ?>" class="img-responsive"></div><br />
             <div>
                 <ul type="none">
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('company1'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('company2'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('company3'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('company4'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('company5'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('company6'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('company7'); ?></span></li>
                 </ul>
             </div>
         </div> 
      </div>
      <div class="col-md-4">
         <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
          <div><h3 style="color: #5dccf7;"><?= $this->lang->line('individuals'); ?></h3></div>
           <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/individual.jpg'; ?>" class="img-responsive"></div><br />
             <div>
                 <ul type="none">
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('individuals1'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('individuals2'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('individuals3'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('individuals4'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('individuals5'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('individuals6'); ?></span></li>
                   <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('individuals7'); ?></span></li>
                 </ul>
             </div>
         </div> 
      </div>
      <div class="col-md-12 text-center">
          <a href="<?= base_url().'sign-up'; ?>" class="button color rounded"><span><?= $this->lang->line('sign_up'); ?></span></a>
      </div>
  </div>
</section>

<!-- CLIENTS -->
<section class="p-t-0" style="padding-bottom: 10px;">
  <div class="container">
      <div class="heading heading-center" style="margin-bottom: 10px;">
          <h2><?= $this->lang->line('clients'); ?></h2>
          <span class="lead"></span>
      </div>

      <div class="carousel" data-lightbox-type="gallery" data-carousel-col="3">
                  <div class="featured-box">
                      <div class="image-box circle-image small">
                          <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/1.jpg'; ?>" class="">
                      </div>
                      <div class="image-box-description text-center">
                          <h4><?= $this->lang->line('name_1'); ?></h4>
                          <p class="subtitle"><?= $this->lang->line('sub_title_1'); ?></p>
                          <hr class="line">
                          <div><?= $this->lang->line('description1'); ?></div>
                          
                      </div>
                  </div>
                  <div class="featured-box">
                      <div class="image-box circle-image small">
                          <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/2.jpg'; ?>" alt="">
                      </div>
                      <div class="image-box-description text-center">
                          <h4><?= $this->lang->line('name_2'); ?></h4>
                          <p class="subtitle"><?= $this->lang->line('sub_title_2'); ?></p>
                          <hr class="line">
                          <div><?= $this->lang->line('description2'); ?></div>
                          
                      </div>
                  </div>
                  <div class="featured-box">
                      <div class="image-box circle-image small">
                          <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/3.jpg'; ?>">
                      </div>
                      <div class="image-box-description text-center ">
                          <h4><?= $this->lang->line('name_3'); ?></h4>
                          <p class="subtitle"><?= $this->lang->line('sub_title_3'); ?></p>
                          <hr class="line">
                          <div><?= $this->lang->line('description3'); ?></div>
                          
                      </div>
                  </div>
                  <div class="featured-box">
                      <div class="image-box circle-image small">
                          <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/4.jpg'; ?>">
                      </div>
                      <div class="image-box-description text-center">
                          <h4><?= $this->lang->line('name_4'); ?></h4>
                          <p class="subtitle"><?= $this->lang->line('sub_title_4'); ?></p>
                          <hr class="line">
                          <div><?= $this->lang->line('description4'); ?></div>
                          
                      </div>
                  </div>
              </div>

  </div>
</section>
<!-- CLIENTS -->

<section class="hidden">
  <div class="container">
     <div class="col-md-12">
          <div class="col-md-3 text-center">
              <h4 class="widget-title"><?= $this->lang->line('they_talk_about_us'); ?> -</h4>
          </div>  
          <div class="col-md-9">
              <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/news.png'; ?>" style="width: 100%;">
          </div>
      </div>
  </div>
</section>

<script>
  $(window).load(function() {
    $('#datepickup').datepicker('setStartDate', new Date());
  });

  $(document).ready(function() {
      $("#datepickup").datepicker({
          autoclose: true,
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('#Deliverydate').datepicker('setStartDate', minDate);
          $('#Deliverydate').val($("#datepickup").val());
      });

      $("#Deliverydate").datepicker({
          autoclose: true,
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('#datepickup').datepicker('setEndDate', minDate);
      });
  });

  $( ".dpAddon" ).click(function( e ) { 
    $("#datepickup").focus();
  });

  $( ".ddAddon" ).click(function( e ) { 
    $("#Deliverydate").focus();
  });

  $('#timepickup').on('change', function() { 
      var pickuptime = $('#timepickup').val();
      if(pickuptime.split(':')[0] < 23) {
          var hr = parseInt((pickuptime.split(':')[0]))+1;
          if(hr<=9) { hr = '0'+hr; }
      } else { var hr = '00'; }
      hrs = hr+':'+pickuptime.split(':')[1];
      $('#Deliverytime').val(hrs);
  });
  $( "#btnFormCreate" ).click(function( e ) {
      e.preventDefault();
      var fromMapID = $('#fromMapID').val();
      var frm_latitude = $('#frm_latitude').val();
      var frm_longitude = $('#frm_longitude').val();
      var toMapID = $('#toMapID').val();
      var to_latitude = $('#to_latitude').val();
      var to_longitude = $('#to_longitude').val();
      var vehicle = $('#vehicle').val();
      var pickupdate = $('#datepickup').val();
      var pickuptime = $('#timepickup').val();
      var c_quantity = $('#quantity').val();
      var dimension_id = $('#dimension').val();
      var deliverdate = $('#Deliverydate').val();
      var delivertime = $('#Deliverytime').val();
      var weight = $('#weight').val();
      var cat_id = $('#cat_id').val();
      
      if(fromMapID == '' || fromMapID == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Enter from address!')?>", "error");
      } else if(frm_latitude == '' || frm_latitude == undefined || frm_longitude == '' || frm_longitude == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('From address is invalid!')?>", "error");
      } else if(toMapID == '' || toMapID == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Enter to address!')?>", "error");
      } else if(to_latitude == '' || to_latitude == undefined || to_longitude == '' || to_longitude == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('To address is invalid!')?>", "error");
      } else if(vehicle == '' || vehicle == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select vehicle type!')?>", "error");
      } else if(pickupdate == '' || pickupdate == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select Pickup Date!')?>", "error");
      } else if(pickuptime == '' || pickuptime == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select Pickup Time!')?>", "error");
      } else if(c_quantity == '' || c_quantity == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Enter Quantity!')?>", "error");
      } else if(parseInt(c_quantity) <= 0) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Quantity should be greater than zero(0)!')?>", "error");
      }  else if(dimension_id == '' || dimension_id == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select Standard Dimension!')?>", "error");
      } else if(deliverdate == '' || deliverdate == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select Delivery Date!')?>", "error");
      } else if(delivertime == '' || delivertime == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select Deliver Time!')?>", "error");
      } else if(weight == '' || weight == undefined) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Enter Total Weight!')?>", "error");
      } else if(parseInt(weight) <= 0) {
          swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Weight should be greater than zero(0).')?>", "error");
      } else { $("#createForm").submit(); }
  });
</script>