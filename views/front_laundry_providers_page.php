<style type="text/css">
  .table > tbody > tr > td {
      border-top: none;
  }
  .dataTables_filter {
      display: none;
  }
  .funkyradio div {
    clear: both;
    overflow: hidden;
  }
  .funkyradio label {
    /*width: 100%;*/
    border-radius: 3px;
    border: 1px solid #D1D3D4;
    font-weight: normal;
  }
  .funkyradio input[type="radio"]:empty,
  .funkyradio input[type="checkbox"]:empty {
    display: none;
  }
  .funkyradio input[type="radio"]:empty ~ label,
  .funkyradio input[type="checkbox"]:empty ~ label {
    position: relative;
    line-height: 2.5em;
    text-indent: 1.25em;
    margin-top: 2em;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }
  .funkyradio input[type="radio"]:empty ~ label:before,
  .funkyradio input[type="checkbox"]:empty ~ label:before {
    position: absolute;
    display: block;
    top: 0;
    bottom: 0;
    left: 0;
    content: '';
    width: 2.5em;
    /*background: #D1D3D4;*/
    border-radius: 3px 0 0 3px;
  }
  .funkyradio input[type="radio"]:hover:not(:checked) ~ label,
  .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
    color: #888;
  }
  .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
  .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
    /*content: '\2714';*/
    text-indent: .9em;
    color: #C2C2C2;
  }
  .funkyradio input[type="radio"]:checked ~ label,
  .funkyradio input[type="checkbox"]:checked ~ label {
    color: #777;
  }
  .funkyradio input[type="radio"]:checked ~ label:before,
  .funkyradio input[type="checkbox"]:checked ~ label:before {
    /*content: '\2714';*/
    text-indent: .9em;
    color: #333;
    background-color: #ccc;
  }
  .funkyradio input[type="radio"]:focus ~ label:before,
  .funkyradio input[type="checkbox"]:focus ~ label:before {
    box-shadow: 0 0 0 3px #999;
  }
  .funkyradio-default input[type="radio"]:checked ~ label:before,
  .funkyradio-default input[type="checkbox"]:checked ~ label:before {
    color: #333;
    background-color: #ccc;
  }
  .funkyradio-primary input[type="radio"]:checked ~ label:before,
  .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #337ab7;
  }
  .funkyradio-success input[type="radio"]:checked ~ label:before,
  .funkyradio-success input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: transparent;
  }
  .funkyradio-danger input[type="radio"]:checked ~ label:before,
  .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #d9534f;
  }
  .funkyradio-warning input[type="radio"]:checked ~ label:before,
  .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #f0ad4e;
  }
  .funkyradio-info input[type="radio"]:checked ~ label:before,
  .funkyradio-info input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #5bc0de;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    /* background-color: white;
    border: 3px outset #0AD909 !important; */
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    /* background-color: white;
    border: 3px outset #0AD909 !important; */
  }
  .booknowBtn {
    border-radius: 25px;
  }
</style>
<section class="p-t-10 m-t-0 background-grey" style="padding-top: 0px">
  <div class="container">
    <div class="hpanel">
      <div class="panel-body" style="padding-top: 0px !important; padding-bottom: 0px !important">
        <div class="col-md-3 col-md-3 col-md-4 col-md-5">
          <h4 class="form-label" style="margin-top: 7px"><i class="fa fa-search"></i> <?= $this->lang->line('Quick Search'); ?></h4>
        </div>
        <div class="col-md-9 col-md-9 col-md-8 col-md-7" style="margin-top: 6px;">
          <input type="text" id="ordersearchbox" class="form-control" placeholder=<?= $this->lang->line('search'); ?>>
        </div>
      </div>
      <table id="provider_table" class="table">
        <thead><tr class="hidden"><th class="hidden">ID</th><th></th></tr></thead>
        <tbody>
          <?php
          foreach ($provider as $pro) { 
            /*echo json_encode($pro);*/
            static $cnt=0; $cnt++;
            $total = 0;
            $flag = 0;
            $cr_sign = "";
            foreach ($details as $det) {
              if($rate = $this->api_laundry->get_laundry_charges_list_apend($pro['cust_id'], $det['sub_cat_id'] , $country_id)){
                $total = $total + ($rate['charge_amount'] * $det['count']);
                $cr_sign= $rate['currency_sign'];
              } else { $flag++; }
            }
            if($flag == 0) {
              $provider_id = $pro["cust_id"]; 
              $provider_details = $this->api_laundry->get_laundry_provider_profile((int)$provider_id);

              $driving_distance = $this->api_laundry->GetDrivingDistance($provider_details['latitude'],$provider_details['longitude'],$cust_lat,$cust_long);

              $rating = (int)$this->api_laundry->get_laundry_review_details((int)$provider_id)['ratings']; ?>
              <tr>
                <td class="hidden"><?=$pro["deliverer_id"]?></td>
                <td>
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; box-shadow: all;">
                    <div class="panel-body" style="padding-bottom: 0px border-radius: 10px; padding-bottom: 5px; padding-top: 10px;">
                      <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-center" style="margin-left: 10px;">
                          <img src="<?=base_url($pro['avatar_url'])?>" class="img-responsive" style="margin-bottom:3px;">
                          <?php for($i=0; $i< $rating; $i++) { ?> <font color="#FFD700"><i class="fa fa-star fa-1x"></i></font>
                          <?php } for($i=0; $i<(5 - $rating); $i++) { ?> <font color="#FFD700"><i class="fa fa-star-o fa-1x"></i></font> <?php } ?>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7" style="margin-right: -15px;">
                          <h5 style="margin-top: 0px; margin-bottom: 0px;"><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp;&nbsp;<font color="#3498db"><?=$pro["company_name"]?></font><font color="gray"> [ <?=$pro["firstname"]?> <?=$pro["lastname"]?> ]</font></h5>
                          <h6 style="margin-bottom: 0px;"><i class="fa fa-address-book-o" aria-hidden="true"></i>&nbsp;&nbsp;<font color="#3498db"><?=$pro["street_name"]?>, <?=$pro["address"]?></font></h6>
                          <h6 style="margin-top: 0px; margin-bottom: 0px;"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<font color="#3498db"><?=$pro["contact_no"]?></font></h6>
                          <h6 style="margin-top: 0px; margin-bottom: 0px;"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;<font color="#3498db"><?=$pro["email_id"]?></font></h6>
                          <input type="hidden" name="lat_long_<?=$pro["cust_id"]?>" value="<?=$pro["latitude"]?>,<?=$pro["longitude"]?>" id="lat_long_<?=$pro["cust_id"]?>" />
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-right" style="margin-left: -15px;">
                          <h4 style="margin-top: 0px;"><strong><font color="red"><?=$total?> <?=$cr_sign?></font></strong></h4>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-5">
                          <label><img src="<?=base_url('resources/distance.png')?>" class="img-responsive" style="vertical-align:bottom; display: inline" /> <?=round($driving_distance,2)?> KM</label>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-7" style="padding-left: 30px;">
                          <form action="<?=base_url('laundry-booking-details')?>" method="GET" id="frm_<?=$cnt?>">
                            <input type="hidden" id="selected_provider_id_<?=$cnt?>" name="selected_provider_id" value="<?=$pro["cust_id"]?>">
                            <input type="hidden" name="session_id" value="<?=$session_id?>">
                            <input type="hidden" name="customer_lat_long" id="customer_lat_long" value="<?=$customer_lat_long?>">
                            <input type="hidden" id="cat_id" name="cat_id" value="9">
                            <button type="submit" class="btn btn-default btn-block booknowBtn" style="width: auto; float: right;"><?=$this->lang->line("Select This Provider")?></button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script>
  $(document).ready( function () {       
    $("#provider_table").dataTable( {
      "paging":   false,
      "ordering": false,
      "info":     false
    } );
  });
  $(document).ready(function() {
    var dataTable = $("#provider_table").dataTable();
    $("#ordersearchbox").keyup(function(){
      dataTable.fnFilter(this.value);
    });    
  });
</script>
<script>
  $( "#basicFilter" ).click(function( e ) {
    e.preventDefault();
    var trip_source = $('#trip_source').val();
    var trip_destination = $('#trip_destination').val();
    var journey_date = $('#journey_date').val();
    var no_of_seat = $('#no_of_seat').val();
    
    if(trip_source == '' || trip_source == undefined) {
        swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select starting city!')?>", "error");
    } else if(trip_destination == '' || trip_destination == undefined) {
        swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select destination city!')?>", "error");
    } else if(journey_date == '' || journey_date == undefined) {
        swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select journey date!')?>", "error");
    } else if(no_of_seat == '' || no_of_seat == undefined) {
        swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('select number of seats!')?>", "error");
    } else if(no_of_seat > 40) {
        swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('The maximum number of seat is 40')?>", "error");
    } else if(no_of_seat <= 0) {
        swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Number of passengers cannot be zero (0).')?>", "error");
    } else { $("#frmBasicSearch").submit(); }
  });
</script>