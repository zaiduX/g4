<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('profile'); ?></span></li>
          <li><span><?= $this->lang->line('details'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('edit_skill'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-photo fa-2x text-muted"></i> <?= $this->lang->line('skill'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('edit_skill_details'); ?></small>    
    </div>
  </div>
</div>
 
<div class="content">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel-bus/edit-skill'); ?>" method="post" class="form-horizontal" id="addskillForm" >    
        
          <div class="panel-body">              
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xl-offset-1 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
            
              <div class="row">
                <div class="form-group">
                  <?php $jr_names = $cf_names = $sr_names = $ex_names = array();
                    if($junior_skills != "NULL"){ foreach ($junior_skills as $jr ){ $jr_names[$jr] = $this->user->get_skillname_by_id($jr);}}
                    if($confirmed_skills!="NULL"){foreach ($confirmed_skills as $jr ){ $cf_names[$jr] = $this->user->get_skillname_by_id($jr);}}
                    if($senior_skills != "NULL") { foreach ($senior_skills as $jr ){ $sr_names[$jr] = $this->user->get_skillname_by_id($jr); }}
                    if($expert_skills != "NULL") { foreach ($expert_skills as $jr ){ $ex_names[$jr] = $this->user->get_skillname_by_id($jr); }}
                  ?>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <label class=""><?= $this->lang->line('skill_level'); ?></label>
                    <select id="skill_level" name="skill_level" class="form-control select2" data-allow-clear="true" data-placeholder="Select skill level">                      
                      <option value=""><?= $this->lang->line('select_skill_level'); ?></option>                      
                      <option value="junior"><?= $this->lang->line('junior'); ?></option>                      
                      <option value="confirmed"><?= $this->lang->line('confirmed'); ?></option>                      
                      <option value="senior"><?= $this->lang->line('senior'); ?></option>                      
                      <option value="expert"><?= $this->lang->line('Expert'); ?></option>                      
                    </select>
                  </div>

                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <label class=""><?= $this->lang->line('category'); ?></label>
                    <select id="category_id" name="category_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select category">
                      <option value=""><?= $this->lang->line('select_category'); ?></option>
                      <?php foreach ($category as $category): ?>
                        <option value="<?= $category['cat_id'] ?>"> <?= $category['cat_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  
                </div>
              </div>
              <div class="row">
                <div class="form-group">  
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <label class=""><?= $this->lang->line('skills'); ?></label>
                    <select id="skills" name="skills[]" class="form-control select2" data-allow-clear="true" data-placeholder="Select skills"  multiple>  
                    </select>
                  </div>
                </div>
              </div>
            </div>

          </div>        
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-left">
                  <a href="<?= base_url('user-panel-bus/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('update_detail'); ?></button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>



<script>

  $("#category_id").on('change', function(event) {  event.preventDefault();
    var category_id = $(this).val();
    if(category_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-skills-by-category-id", 
        data: { category_id: category_id },
        dataType: "json",
        success: function(res){ 
          var junior_skills = <?= json_encode($junior_skills);?>;
          var confirmed_skills = <?= json_encode($confirmed_skills);?>;
          var senior_skills = <?= json_encode($senior_skills);?>;
          var expert_skills = <?= json_encode($expert_skills);?>;

          // $('#skills').empty(); 
          $.each(res,function(key, value){
            if( ($.inArray(value['skill_id'], junior_skills) == -1 ) && ($.inArray(value['skill_id'], confirmed_skills) == -1 ) && ($.inArray(value['skill_id'], senior_skills) == -1 ) && ($.inArray(value['skill_id'], expert_skills) == -1 )) {
              $('#skills').append('<option value="'+$(this).attr('skill_id')+'">'+$(this).attr('skill_name')+'</option>');
            }
            
          });
          $('#skills').focus();
        }        
      });
    } else { 
      // $('#skills').empty(); 
      // $('#skills').attr('disabled', true); 
    }

  });
   

  $("#skill_level").on('change', function(event) {  event.preventDefault();
    var level = $(this).val();
    if(level == "junior") {        
      $('#skills').empty().trigger('change');
      var junior_name = <?= json_encode($jr_names);?>;
      $.each(junior_name,function(key, value){        
        if(key != "NULL") { $('#skills').append('<option value="'+key+'" selected>'+value+'</option>').trigger('change'); }
      });
    }

    else if(level == "confirmed") { 
      $('#skills').empty().trigger('change');
      var confimred_name = <?= json_encode($cf_names);?>;      
      $.each(confimred_name,function(key, value){        
        if(key != "NULL") { $('#skills').append('<option value="'+key+'" selected>'+value+'</option>').trigger('change'); }
      });
    }

    else if(level == "senior") { 
      $('#skills').empty().trigger('change');        
      var senior_name = <?= json_encode($sr_names);?>;
      $.each(senior_name,function(key, value){        
        if(key != "NULL") { $('#skills').append('<option value="'+key+'" selected>'+value+'</option>').trigger('change'); }
      });
    }

    else if(level == "expert") { 
      $('#skills').empty().trigger('change');
      var expert_name = <?= json_encode($ex_names);?>;
      $.each(expert_name,function(key, value){        
        if(key != "NULL") { $('#skills').append('<option value="'+key+'" selected>'+value+'</option>').trigger('change'); }
      });
    }

  });
  
 $(function(){
    $("#addskillForm").validate({
      ignore: [],
      rules: {
        skill_level: { required: true, },
        // "skills[]": { required: true, },
        // category_id: { required: true, },
      }, 
      messages: {
        skill_level: { required: "Select skill level.",   },
        // "skills[]": { required: "Select some skills.",  },
        // category_id: { required: "Select category.",  },
      }
    });
  });
</script>