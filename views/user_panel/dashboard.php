<div class="content" style="padding-top:30px;">
  <?php if($cust['email_verified'] == 0): ?>
    <div class="row">
      <div class="form-group"> 
        <div class="alert alert-warning alert-dismissible email-verify-alert" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?= $this->lang->line('warning'); ?></strong> <?= $this->lang->line('email_verification_warning'); ?> &nbsp; <a href="<?= base_url('resend-email-verification'); ?>"> <strong><?= $this->lang->line('resend_verification_email'); ?></strong></a>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center"> 
      <span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important; font-size: 2.5em;"><strong><?= $this->lang->line('welcome_to_gonagoo'); ?> </strong></span>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center"> 
      <h4 style="color: white;"><?= $this->lang->line('the'); ?> <strong><?= $this->lang->line('best'); ?></strong> <?= $this->lang->line('providers_and_freelancers'); ?></h4>
    </div>
  </div>
  <br />