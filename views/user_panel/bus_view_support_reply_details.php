    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/get-support-list'; ?>"><?= $this->lang->line('all_your_queries_and_suggestions'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('get_support'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('post_your_query'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('post_your_query_line'); ?></small>
            </div>
        </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="hpanel">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-1">
                            <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('request_id'); ?></label>
                                <div class="col-md-8">
                                    <p><?= strtoupper($details['ticket_id']) ?></p>
                                </div>
                            </div>
                        </div>

                    	<div class="col-md-9 col-md-offset-1">
                            <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('request_category'); ?></label>
                                <div class="col-md-8">
                                    <p><?= strtoupper($details['type']) ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9 col-md-offset-1">
                            <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('description'); ?></label>
                                <div class="col-md-8">
                                    <p><?= $details['description'] ?></p>
                                </div>
                            </div>
                        </div>
                        
                        <?php if($details['file_url'] != 'NULL') { ?>
                            <div class="col-md-9 col-md-offset-1">
                                <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('attachment'); ?></label>
                                    <div class="col-md-8">
                                        <p><a href="<?= $this->config->item('base_url') . $details['file_url']; ?>" target="_blank"><i class="fa fa-download"></i> <?= $this->lang->line('download'); ?></a></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-9 col-md-offset-1">
                            <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('create_date_time'); ?></label>
                                <div class="col-md-8">
                                    <p><?= strtoupper(date('d-M-Y h:i:s A',strtotime($details['cre_datetime']))) ?></p>
                                </div>
                            </div>
                        </div>
                        <?php if($details['response'] != 'NULL') { ?>
                            <div class="col-md-9 col-md-offset-1">
                                <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('gonagoo_response'); ?></label>
                                    <div class="col-md-8">
                                        <p><?= $details['response'] ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9 col-md-offset-1">
                                <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('response_datetime'); ?></label>
                                    <div class="col-md-8">
                                        <p><?= strtoupper(date('d-M-Y h:i:s A',strtotime($details['res_datetime']))) ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9 col-md-offset-1">
                                <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('status'); ?></label>
                                    <div class="col-md-8">
                                        <p><?= strtoupper($details['status']) ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9 col-md-offset-1">
                                <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('updated_by'); ?></label>
                                    <div class="col-md-8">
                                        <p><?= $this->lang->line('gonagoo_admin'); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-11 col-md-offset-1">
                            <div class="col-md-12">
                                <a href="<?= $this->config->item('base_url') . 'user-panel-bus/get-support-list'; ?>" class="btn btn-primary" type="submit"><i class="fa fa-arrow-left"></i> <?= $this->lang->line('back'); ?></a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>