
	<div class="normalheader small-header">
		<div class="hpanel">
		    <div class="panel-body">
		        <a class="small-header-action" href="">
		          <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
		        </a>
		        <div id="hbreadcrumb" class="pull-right">
	            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Passengers List'); ?></span></li>
	            </ol>
		        </div>
		        <h2 class="font-light m-b-xs">
		          <?= $this->lang->line('Passengers List'); ?> &nbsp;&nbsp;&nbsp;
             	<a class="btn btn-outline btn-info" href="<?=base_url('user-panel-bus/address-book-add'); ?>"><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?></a>
		        </h2>
		    </div>
		</div>
	</div>

	<div class="content">
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
						<table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
				            <thead>
				                <tr>
				                	<th><?= $this->lang->line('Sr. No'); ?></th>
				                	<th><?= $this->lang->line('Contact Name'); ?></th>
				                	<th><?= $this->lang->line('Contact No'); ?></th>
				                	<th><?= $this->lang->line('Email Address'); ?></th>
				                	<th><?= $this->lang->line('gender'); ?></th>
				                	<th><?= $this->lang->line('Age'); ?></th>
				                	<th><?= $this->lang->line('action'); ?></th>
				                </tr>
				            </thead>
				            <tbody class="panel-body">
					            <?php foreach ($address_details as $addr) { static $i = 0; $i++; ?>
					           		<tr>
					           			<td><?=$i?></td>
					           			<td><?=$addr['firstname'].' '.$addr['lastname']?></td>
					           			<td><?=$this->api->get_country_code_by_id($addr['country_id']). ' ' .$addr['mobile']?></td>
					           			<td><?=($addr['email_id'] == 'NULL')?'Not provided!':$addr['email_id']?></td>
					           			<td><?=($addr['gender'] == 'M')?'Male':'Female'?></td>
					           			<td><?=$addr['age']?></td>
					           			<td>
					           				<form action="<?=base_url('user-panel-bus/bus-address-book-edit')?>" method="post" style="display: inline-block;">
		                                        <input type="hidden" name="address_id" value="<?= $addr['address_id'] ?>">
		                                        <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-pencil"></i> <span class="bold"><?= $this->lang->line('edit'); ?></span></button>
		                                    </form>
		                                    <button style="display: inline-block;" class="btn btn-danger btn-sm addressDeleteAlert" id="<?= $addr['address_id'] ?>"><i class="fa fa-trash-o"></i> <span class="bold"><?= $this->lang->line('delete'); ?></span></button>
					           			</td>
					           		</tr>
								<?php } ?>
				           	</tbody>
				        </table>
				    </div>
				</div>
			</div>
		</div>
	</div>

	<script>
        $('.addressDeleteAlert').click(function () {
            var id = this.id;
            swal(
            {
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-bus/bus-address-delete')?>", { address_id: id }, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('Deleted!'); ?>", "<?= $this->lang->line('Passenger details have been deleted!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting passenger details!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Passenger details is safe!'); ?>", "error"); }
            });
        });
    </script>