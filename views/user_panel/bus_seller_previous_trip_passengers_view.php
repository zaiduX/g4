<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-previous-trips'; ?>"><?= $this->lang->line('Previous Trips'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Passenger Details'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Passenger Details'); ?>
      </h2>
      <small class="m-t-md"><?= $this->lang->line('Manage Trip Passengers &amp; Payments'); ?></small> 
    </div>
  </div>
</div>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php"); ?>
  <div class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="hpanel">
          <div class="panel-body">
            <div class="row">
              <div class="=col-lg-12">
                   <h3 class="stat-label" style='color: #3498db'><?= $this->lang->line('Trip Details'); ?></h3>
              </div>
              <br/>
              <div class="col-md-6">
                <h5><lable style='color: #3498db'> <?=$this->lang->line('Source')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['source_point']?></h5>
              </div>
              <div class="col-md-6">
                <h5><lable style='color: #3498db'> <?=$this->lang->line('Destination')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['destination_point']?></h5>
              </div>
              <div class="col-md-6">
                <h5><lable style='color: #3498db'> <?=$this->lang->line('Journey Date')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['journey_date']?></h5>
              </div>
              <div class="col-md-6">
                <h5><lable style='color: #3498db'> <?=$this->lang->line('Departure Time')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['trip_start_date_time']?></h5>
              </div>
            </div>
            <br/>
            <div class="row">
              <h3 class="stat-label" style='color: #3498db'><?= $this->lang->line('Passenger Details'); ?></h3>
            </div>
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                    <?php endif; ?>  
                </div>   
            </div>
            <table id="example12" class="table table-striped table-bordered table-hover" width="100%">
              <thead>
                <th><?= $this->lang->line('Ticket ID'); ?></th>
                <th><?= $this->lang->line('Name'); ?></th>
                <th><?= $this->lang->line('bus_seat_type'); ?></th>
                <th><?= $this->lang->line('Contact'); ?></th>
                <th><?= $this->lang->line('Gender'); ?></th>
                <th><?= $this->lang->line('Payment'); ?></th>
              </thead>
              <tbody>
                <?php foreach ($master_booking_details as $seats) { 
                  $seat_details = $this->api->get_ticket_seat_details($seats['ticket_id']);
                  foreach ($seat_details as $seat) {
                  ?>
                  <tr>
                    <td><?=$seat['ticket_id']?></td>
                    <td><?=$seat['firstname']." ".$seat['lastname'];?></td>
                    <td><?=$seat['seat_type']?></td>
                    <td><?=$this->api->get_country_code_by_id($seat['country_id']).$seat['mobile']?></td>
                    <td><?=($seat['gender']=='M')?'Male':'Female'?></td>
                    <td>
                      <div style="display: inline-flex;">
                        <form action="<?=base_url('user-panel-bus/bus-work-room')?>" method="post">
                          <input type="hidden" name="ticket_id" value="<?=$seat['ticket_id'] ?>">
                          <button type="submit" class="btn btn-outline btn-success" ><i class="fa fa-check"> </i> <?= $this->lang->line('workroom'); ?> </button>
                        </form>&nbsp;
                        <!-- <button id="<?=$seat['ticket_id']?>" class="btn btn-outline btn-info" data-toggle="modal" data-target="#myModal<?=$seat['ticket_id']?>"><i class="fa fa-check"></i> <?= $this->lang->line('Print Ticket'); ?> </button>
                        <div class="modal fade" id="myModal<?=$seat['ticket_id']?>" role="dialog">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title"><?= $this->lang->line('Print Ticket'); ?></h4>
                              </div>
                              <div class="modal-body" id="printThis<?=$seat['ticket_id']?>">
                                <div style="width:  340px; height: 80px; border: dashed 1px #225595; text-align: center; padding-top: 2px">
                                  <div class="col-md-12">
                                    <div class="col-md-6" style="margin-left:-30px;">
                                      <img src="<?=($operator_details['avatar_url']=='' || $operator_details['avatar_url'] == 'NULL')?base_url('resources/no-image.jpg'):base_url($operator_details['avatar_url'])?>"     style="border: 1px solid #3498db; width: 120px; height: 70px;">
                                    </div>
                                    <div class="col-md-6">
                                      <img src="<?= base_url('resources/images/dashboard-logo.jpg') ?>" style="width: 160px; height: auto;" /><br />
                                    </div>
                                  </div>
                                 <h5><?= $this->lang->line('slider_heading1'); ?></h5>
                                </div>
                                <div style="width: 340px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                                  <h5 style="color: #225595; text-decoration: underline;">
                                  <i class="fa fa-arrow-up"></i> <?= $this->lang->line('Ownward_Trip'); ?> <br />
                                  <strong style="float: right; padding-right: 2px; margin-top: -15px"><?= $this->lang->line('Ticket ID'); ?>: #<?=$seat['ticket_id']?></strong> 
                                  <br />
                                  <?php QRcode::png($seat['ticket_id'], $seat['ticket_id'].".png", "L", 2, 2); 
                                  $img_src = $_SERVER['DOCUMENT_ROOT']."/".$seat['ticket_id'].'.png';
                                  $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$seat['ticket_id'].'.png';
                                  if(rename($img_src, $img_dest));
                                  ?>
                                  <img src="<?php echo base_url('resources/ticket-qrcode/').$seat['ticket_id'].'.png'; ?>" style="float: right; width: 84px; padding-right: 2px; height: 84px; margin-top: -15px" />
                                  </h5>
                                  <h6 style="margin-top:-15px;"><strong><?= $this->lang->line('Source'); ?>:</strong> <?= $seat['source_point'] ?> - <?= $seats['pickup_point'] ?></h6>
                                  <h6><strong><?= $this->lang->line('Destination'); ?>:</strong> <?= $seat['destination_point'] ?> - <?= $seats['drop_point'] ?></h6>
                                  <h6><strong><?= $this->lang->line('Journey Date'); ?>:</strong> <?= $seat['journey_date'] ?></h6>
                                  <h6><strong><?= $this->lang->line('Departure Time'); ?>:</strong> <?= $seat['trip_start_date_time'] ?></h6>
                                  <h6><strong><?= $this->lang->line('Passenger Details'); ?>:</strong></h6>
                                  <?php 
                                    $ticket_seat_details = $this->api->get_ticket_seat_details($seat['ticket_id']);
                                  ?>
                                  <table name="<?=$seat['ticket_id']?>" style="width: 100%">
                                    <thead>
                                      <th><?= $this->lang->line('Name'); ?></th>
                                      <th><?= $this->lang->line('Gender'); ?></th>
                                      <th><?= $this->lang->line('Contact'); ?></th>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($name as $seat) { ?>
                                        <tr>
                                          <td><?=$seat['firstname']. ' ' .$seat['lastname']?></td>
                                          <td><?=($seat['gender']=='M')?'Male':'Female'?></td>
                                          <td><?=$this->api->get_country_code_by_id($seat['country_id']).$seat['mobile']?></td>
                                        </tr>
                                      <?php } ?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                              <div class="modal-footer" style="display: flex;">
                                <button style="float: right;" class="btn btn-sm pull-right btn-default" id="btnPrint<?=$seat['ticket_id']?>"><?= $this->lang->line('Print Ticket'); ?></button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <script type="text/javascript">
                          $("#btnPrint<?= $seats['ticket_id'] ?>").click(function () {
                            $('#printThis<?= $seats['ticket_id'] ?>').printThis();
                          });
                        </script>&nbsp; -->
                        <?php 
                          $trip_dt = explode('/', $seats['journey_date'])[2] . '-' . explode('/', $seats['journey_date'])[0] . '-' . explode('/', $seats['journey_date'])[1] . ' '. $seats['trip_start_date_time'] . ':00';
                          $claim_time_limit = date('Y-m-d H:i:s', strtotime("+24 hours", strtotime($trip_dt)));
                          $current_dt_tm = strtotime(date('Y-m-d H:i:s'));
                        ?>
                        <?php if($seat['commission_refund'] == 0 && $current_dt_tm > $claim_time_limit): ?>
                          <form action="<?=base_url('user-panel-bus/commission-refund-request')?>" method="post" id="frm_<?=$seat['seat_id']?>">
                            <input type="hidden" name="ticket_id" value="<?=$seat['ticket_id']?>" />
                            <input type="hidden" name="seat_id" value="<?=$seat['seat_id']?>" />
                            <button type="text" class="btn btn-outline btn-danger btn_<?=$seat['seat_id'] ?>" title="<?= $this->lang->line('Request for commission refund'); ?>"><i class="fa fa-times"></i> <?= $this->lang->line('Request Refund'); ?></button>
  
                            <script>
                              $(".btn_<?=$seat['seat_id']?>").click(function (e) {
                                e.preventDefault();
                                //alert(this.form.id);
                                swal({
                                  title: "<?= $this->lang->line('are_you_sure'); ?>",
                                  text: "<?= $this->lang->line('Request will send to Gonagoo admin for commission refund.'); ?>",
                                  type: "warning",
                                  showCancelButton: true,
                                  confirmButtonColor: "#DD6B55",
                                  confirmButtonText: "<?= $this->lang->line('Yes, send it.'); ?>",
                                  cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                                  closeOnConfirm: false,
                                  closeOnCancel: false 
                                },
                                function (isConfirm) {
                                  if (isConfirm) {
                                    $("#frm_<?=$seat['seat_id']?>").submit();
                                  } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Request not sent.'); ?>", "error"); }
                                });
                              }); 
                            </script>
                          </form>
                        <?php endif; ?>
                      </div>
                    </td>
                  </tr>
                  <?php } ?>
                <?php } ?>
              </tbody>
            </table>             
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="col-lg-6">
              <a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-previous-trips'; ?>" class="btn btn-info btn-outline " ><?= $this->lang->line('back'); ?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  $(function () {
    $('#example12').dataTable( {
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      buttons: [
        {extend: 'copy',className: 'btn-sm'},
        {extend: 'csv',title: 'ExampleFile', className: 'btn-sm'},
        {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
        {extend: 'print',className: 'btn-sm'}
      ]
    });
  });

  $("#btnPrint<?=$seats['ticket_id']?>").click( function() {
    $('#printThis<?=$seats['ticket_id']?>').printThis();
  });
</script>
