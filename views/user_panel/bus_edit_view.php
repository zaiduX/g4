    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-list'; ?>"><?= $this->lang->line('Vehicles'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Edit Vehicle Details'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light-xs">  <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Edit Vehicle Details'); ?> </h2>
          <small class="m-t-md"><?= $this->lang->line('Vehicle Details'); ?></small>    
        </div>
      </div>
    </div>
     
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <?php if($this->session->flashdata('error')):  ?>
                        <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                        <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>

                        <form method="post" class="form-horizontal" action="<?=base_url('user-panel-bus/bus-edit-details')?>" id="busEdit" enctype="multipart/form-data">
                            <input type="hidden" name="bus_id" value="<?=$bus_details['bus_id']?>">
                            <div class="row">
                                <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('bus_make'); ?></label>
                                    <div class="">
                                        <input type="text" placeholder="<?= $this->lang->line('bus_make'); ?>" class="form-control" name="bus_make" id="bus_make" value="<?=$bus_details['bus_make']?>">
                                    </div><br />
                                </div>
                                <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('bus_modal'); ?></label>
                                    <div class="">
                                        <input type="text" placeholder="<?= $this->lang->line('bus_modal'); ?>" class="form-control" name="bus_modal" id="bus_modal" value="<?=$bus_details['bus_modal']?>">
                                    </div><br />
                                </div>
                                <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('Name/No'); ?></label>
                                    <div class="">
                                        <input type="text" placeholder="<?= $this->lang->line('Name/No'); ?>" class="form-control" name="bus_no" id="bus_no" value="<?=$bus_details['bus_no']?>">
                                    </div><br />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('Image'); ?></label>
                                    <div class="">
                                        <input type="file" class="form-control" name="bus_image_url" id="bus_image_url">
                                        <input type="hidden" class="form-control" name="bus_image_url_old" value="<?=$bus_details['bus_image_url']?>">
                                    </div><br />
                                </div>
                                <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('Icon'); ?></label>
                                    <div class="">
                                        <input type="file" class="form-control" name="bus_icon_url" id="bus_icon_url">
                                        <input type="hidden" class="form-control" name="bus_icon_url_old" value="<?=$bus_details['bus_icon_url']?>">
                                    </div><br />
                                </div>
                                <div class="col-sm-4 hidden">
                                    <label class="text-left"><?= $this->lang->line('bus_seat_type'); ?></label>
                                    <div class="">
                                        <select class="js-source-states" style="width: 100%" name="bus_seat_type" id="bus_seat_type">
                                            <option value=""><?= $this->lang->line('select_bus_seat_type'); ?></option>
                                            <option value="SEATER" <?=($bus_details['bus_seat_type']=='SEATER')?'selected':''?> >SEATER</option>
                                            <option value="SLEEPER" <?=($bus_details['bus_seat_type']=='SLEEPER')?'selected':''?> >SLEEPER</option>
                                            <option value="SEMI-SLEEPER" <?=($bus_details['bus_seat_type']=='SEMI-SLEEPER')?'selected':''?> >SEMI-SLEEPER</option>
                                        </select>
                                    </div><br />
                                </div>
                                <div class="col-sm-4">
                                    <label class="text-left"><?= $this->lang->line('bus_ac_type'); ?></label>
                                    <div class="">
                                        <select class="js-source-states" style="width: 100%" name="bus_ac" id="bus_ac">
                                            <option value=""><?= $this->lang->line('select_bus_ac_type'); ?></option>
                                            <option value="AC" <?=($bus_details['bus_ac']=='AC')?'selected':''?>>AC</option>
                                            <option value="NON AC" <?=($bus_details['bus_ac']=='NON AC')?'selected':''?>>NON AC</option>
                                        </select>
                                    </div><br />
                                </div>
                            </div>
                            <div class="row">
                                <?php foreach ($bus_seat_details as $seat): ?>
                                    <div class="col-sm-4 <?=($seat['type_of_seat'] == 'VIP')?'hidden':''?>"><label class="text-left"><?= $this->lang->line('Number of'); ?> <?=$seat['type_of_seat']?> <?= $this->lang->line('Seats'); ?></label>
                                        <div class="">
                                            <input type="hidden" name="std_id[]" value="<?=$seat['std_id']?>">
                                            <input type="number" placeholder="<?= $this->lang->line('Enter number of seats'); ?>" class="form-control" name="seat_type_count[]" id="seat_type_count" value="<?=$seat['total_seats']?>" required>
                                        </div><br />
                                    </div>
                                <?php endforeach ?>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="text-left"><?= $this->lang->line('transport_vehicle'); ?></label>
                                    <div class="">
                                        <select class="js-source-states" style="width: 100%" name="bus_amenities[]" id="bus_amenities" multiple="multiple">
                                            <?php 
                                                $old_amenities = explode(',', $bus_details['bus_amenities']);
                                                foreach ($amenities as $amenity) { ?>
                                                <option value="<?= $amenity['am_title'] ?>" <?=(in_array($amenity['am_title'], $old_amenities))?'selected':''?>><?= $amenity['am_title'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div><br />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <br>
                                    <button class="btn btn-primary" type="submit"><?= $this->lang->line('save_details'); ?></button>
                                    <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-list'; ?>" class="btn btn-info" type="submit"><?= $this->lang->line('back'); ?></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $("#busEdit")
        .validate({
            ignore: [], 
            rules: {
                bus_make: { required : true },
                bus_modal: { required : true },
                bus_no: { required : true },
                bus_seat_type: { required : true },
                bus_ac: { required : true },
            },
            messages: {
                bus_make: { required : <?= json_encode($this->lang->line('Enter make year!'));?>, },
                bus_modal: { required : <?= json_encode($this->lang->line('Enter modal!'));?>, }, 
                bus_no: { required : <?= json_encode($this->lang->line('Enter Name/No!'));?>, },
                bus_seat_type: { required : <?= json_encode($this->lang->line('Select seat type!'));?>, },
                bus_ac: { required : <?= json_encode($this->lang->line('Select A.C. type!'));?>, },
            },
        });

</script>