	<div class="normalheader small-header">
		<div class="hpanel">
		    <div class="panel-body">
		        <a class="small-header-action" href="">
		          <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
		        </a>
		        <div id="hbreadcrumb" class="pull-right">
	            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Trip Master'); ?></span></li>
	            </ol>
		        </div>
		        <h2 class="font-light m-b-xs"> <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Trip Master'); ?> &nbsp;&nbsp;&nbsp;
             	<a class="btn btn-outline btn-info" href="<?=base_url('user-panel-bus/bus-trip-master-add'); ?>"><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?></a> &nbsp;&nbsp;&nbsp;
             	<a class="btn btn-outline btn-success" href="<?=base_url('user-panel-bus/bus-trip-ticket-booking-seller'); ?>"><i class="fa fa-ticket"></i> <?= $this->lang->line('Book a Ticket'); ?></a>
		        </h2>
		        <small class="m-t-md"><?= $this->lang->line('Manage Trip Details'); ?></small> 
		    </div>
		</div>
	</div>

	<div class="content">
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">

						<table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
				            <thead>
				                <tr>
				                	<th><?= $this->lang->line('Sr. No'); ?></th>
				                	<th><?= $this->lang->line('Source'); ?></th>
				                	<th><?= $this->lang->line('Destination'); ?></th>
				                	<th><?= $this->lang->line('Vehicle Type'); ?></th>
				                	<th><?= $this->lang->line('Vehicle'); ?></th>
				                	<th><?= $this->lang->line('Depart Time'); ?></th>
				                	<th><?= $this->lang->line('status'); ?></th>
				                	<th><?= $this->lang->line('action'); ?></th>
				                </tr>
				            </thead>
				            <tbody class="panel-body">
					            <?php foreach ($trip_master as $trip) { static $i = 0; $i++; ?>
					           		<tr>
					           			<?php
                    	  		//echo $trip['trip_end_date']."===".date('m/d/Y'); die();
                    				//echo strtotime('02/22/2019'); die();
                    			?>
					           			<td><?=$i?></td>
					           			<td><?=$trip['trip_source']?></td>
					           			<td><?=$trip['trip_destination']?></td>
					           			<td><?php $vehicle_types = $this->api->get_vehicle_type_by_id($trip['vehical_type_id']); echo $vehicle_types['vehicle_type']?></td>
					           			<td><?php $bus_details = $this->api->get_operator_bus_details($trip['bus_id']); echo $bus_details['bus_no']?></td>
					           			<td><?=$trip['trip_depart_time']?></td>
					           			<td><?=((strtotime($trip['trip_end_date'])<strtotime(date('m/d/Y')))) ? $this->lang->line('closed') : $this->lang->line('open') ?></td>
					           			<td style="display: flex;">
					           				<form action="<?=base_url('user-panel-bus/bus-trip-location-list')?>" method="post" style="display: inline-block;">
		                                        <input type="hidden" name="trip_id" value="<?= $trip['trip_id'] ?>">
		                                        <input type="hidden" name="vehical_type_id" value="<?= $trip['vehical_type_id'] ?>">
		                                        <button class="btn btn-warning btn-sm" type="submit"><i class="fa fa-map-marker"></i> <span class="bold"><?= $this->lang->line('Locations'); ?></span></button>
		                                    </form>&nbsp;
					           				<!--<form action="<?=base_url('user-panel-bus/bus-trip-master-edit')?>" method="post" style="display: inline-block;">
		                                        <input type="hidden" name="trip_id" value="<?= $trip['trip_id'] ?>">
		                                        <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-pencil"></i> <span class="bold"><?= $this->lang->line('edit'); ?></span></button>
		                                    </form>&nbsp; -->
		                                    <?php if(strtotime($trip['trip_end_date'])<=strtotime(date('m/d/Y'))) { //echo $trip['trip_end_date']; echo '--'.date('m/d/Y');  ?>
		                                    <form action="<?=base_url('user-panel-bus/bus-trip-master-enable')?>" method="post" style="display: inline-block;">
		                                        <input type="hidden" name="trip_id" value="<?= $trip['trip_id'] ?>">
		                                        <button class="btn btn-success btn-sm" type="submit"><i class="fa fa-check"></i> <span class="bold"><?= $this->lang->line('Enable'); ?></span></button>
		                                    </form>&nbsp;
		                                    <?php } else {  //echo $trip['trip_end_date']; echo '--'.date('m/d/Y');?>
		                                    <form action="<?=base_url('user-panel-bus/bus-trip-master-disable')?>" method="post" style="display: inline-block;">
		                                        <input type="hidden" name="trip_id" value="<?= $trip['trip_id'] ?>">
		                                        <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-times"></i> <span class="bold"><?= $this->lang->line('Disable'); ?></span></button>
		                                    </form>&nbsp;
		                                    <?php } ?>
		                                    <form action="<?=base_url('user-panel-bus/bus-trip-details-seller')?>" method="post" style="display: inline-block;">
	                                          	<input type="hidden" name="trip_id" value="<?= $trip['trip_id'] ?>">
	                                          	<button class="btn btn-success btn-sm" type="submit"><i class="fa fa-info-circle"></i> <span class="bold"><?= $this->lang->line('Detail'); ?></span></button>
	                                        </form>
		                                    <!--<button style="display: inline-block;" class="btn btn-danger btn-sm tripDeleteAlert" id="<?= $trip['trip_id'] ?>"><i class="fa fa-trash-o"></i> <span class="bold"><?= $this->lang->line('delete'); ?></span></button>-->
					           			</td>
					           		</tr>
								<?php } ?>
				           	</tbody>
				        </table>
				    </div>
				</div>
			</div>
		</div>
	</div>

	<script>
        $('.tripDeleteAlert').click(function () {
            var id = this.id;
            swal(
            {
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-bus/bus-trip-master-delete')?>", { trip_id: id }, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('Deleted!'); ?>", "<?= $this->lang->line('Trip and locations has been deleted!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting trip!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Trip and locations is safe!'); ?>", "error"); }
            });
        });
    </script>