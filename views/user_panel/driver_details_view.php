<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/driver-list'; ?>"><?= $this->lang->line('drivers'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('view_drivers'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('driver_profile'); ?></h2>
            <small class="m-t-md"><?= $this->lang->line('driver_personal_and_work_details'); ?></small>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-lg-4" >
            <div class="hpanel hgreen" >
                <div class="panel-heading">
                    <h4><strong><?= $this->lang->line('profile_info'); ?></strong></h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                        <?php if($drivers[0]['avatar'] == "NULL"): ?>
                            <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b avatar" alt="avatar" />
                            <?php else: ?>
                                <img src="<?= base_url($drivers[0]['avatar']); ?>" class="img-thumbnail m-b avatar" alt="avatar" />
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">
                            <h4 style="text-align: left">
                                <a><?= strtoupper($drivers[0]['first_name']) ?><br /><br />
                                <?php if($drivers[0]['online_status'] == 1 ) { ?>
                                    <span class="pull-left btn btn-success btn-xs"><?= $this->lang->line('online'); ?></span>
                                <?php } else { ?>
                                    <span class="pull-left btn btn-danger btn-xs"><?= $this->lang->line('offline'); ?></span>
                                <?php } ?>
                            </h4 style="text-align: left">
                        </div>
                        <div class="col-md-2">
                            <a href="<?= base_url('user-panel/edit-driver/').$drivers[0]['cd_id']; ?>" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></a>
                        </div>
                    </div>


                        <p><i class="fa fa-envelope"></i> <?= $this->lang->line('eauil'); ?> <?= $drivers[0]['email'] ?></p>
                        <p><i class="fa fa-phone"></i> <?= $this->lang->line('contact_1'); ?> <?= $drivers[0]['mobile1'] ?></p>
                        <p><i class="fa fa-phone"></i> <?= $this->lang->line('contact_2'); ?> <?= $drivers[0]['mobile2'] ?></p>
                        
                        <p><i class="fa fa-map"></i> <?= $this->lang->line('current_location'); ?></p>
                    <div class="border-right border-left">
                        <iframe
                          frameborder="0" style="border:0"
                          src="<?php echo "https://www.google.com/maps/embed/v1/place?key=AIzaSyCIj8TMFnqnxpvima7MDrJiySvGK-UVLqw
                            &q=".$drivers[0]['latitude'].",".$drivers[0]['longitude'] ?>" allowfullscreen>
                        </iframe>
                    </div>
                </div>
                <div class="panel-body">
                    <dl>
                        <dt><?= $this->lang->line('categories'); ?></dt>
                        <?php
                            for ($i=0; $i < sizeof($category_name); $i++) { ?>
                                <span class="btn btn-info btn-xs" style="display: inline-block;"><?= $category_name[$i] ?></span>
                        <?php } ?>
                    </dl>
                </div>
                <div class="panel-footer contact-footer">
                    <div class="row">
                        <div class="col-md-6 border-right">
                            <div class="contact-stat"><span><?= $this->lang->line('working_orders'); ?> </span> <strong><?= sizeof($accept_orders) ?></strong></div>
                        </div>
                        <div class="col-md-6">
                            <div class="contact-stat"><span><?= $this->lang->line('delivered_orders'); ?> </span> <strong><?= sizeof($delivered_orders) ?></strong></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="hpanel">
                <div class="hpanel">
                <div class="panel-heading">
                    <h4><strong>Recent Orders</strong></h4>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#step-1"><?= $this->lang->line('assigned_orders'); ?></a></li>
                    <li class=""><a data-toggle="tab" href="#step-2"><?= $this->lang->line('accepted_orders'); ?></a></li>
                    <li class=""><a data-toggle="tab" href="#step-3"><?= $this->lang->line('rejected_orders'); ?></a></li>
                    <li class=""><a data-toggle="tab" href="#step-4"><?= $this->lang->line('picked_up_orders'); ?></a></li>
                    <li class=""><a data-toggle="tab" href="#step-5"><?= $this->lang->line('delivered_orders'); ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="step-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th><?= $this->lang->line('order_id'); ?></th>
                                            <th><?= $this->lang->line('client_name'); ?></th>
                                            <th><?= $this->lang->line('from'); ?></th>
                                            <th><?= $this->lang->line('TO'); ?></th>
                                            <th><?= $this->lang->line('distance'); ?></th>
                                            <th><?= $this->lang->line('assigned_on'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($i=0; $i < sizeof($assign_orders); $i++) {  ?>
                                            <tr>
                                                <td><a href="<?php echo 'order-details/'.$assign_orders[$i]['order_id'].'' ?>"><?= $assign_orders[$i]['order_id'] ?></a></td>
                                                <td><?= $assign_orders[$i]['cust_name'] ?></td>
                                                <td><?= $assign_orders[$i]['from_address'] ?></td>
                                                <td><?= $assign_orders[$i]['to_address'] ?></td>
                                                <td><?= $assign_orders[$i]['distance_in_km'] ?></td>
                                                <td><?= $assign_orders[$i]['assigned_datetime'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="step-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                        <th><?= $this->lang->line('order_id'); ?></th>
                                        <th><?= $this->lang->line('client_name'); ?></th>
                                        <th><?= $this->lang->line('from'); ?></th>
                                        <th><?= $this->lang->line('TO'); ?></th>
                                        <th><?= $this->lang->line('distance'); ?></th>
                                        <th><?= $this->lang->line('assigned_on'); ?></th>>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($i=0; $i < sizeof($accept_orders); $i++) {  ?>
                                            <tr>
                                                <td><a href="<?php echo 'order-details/'.$accept_orders[$i]['order_id'].'' ?>"><?= $accept_orders[$i]['order_id'] ?></a></td>
                                                <td><?= $accept_orders[$i]['cust_name'] ?></td>
                                                <td><?= $accept_orders[$i]['from_address'] ?></td>
                                                <td><?= $accept_orders[$i]['to_address'] ?></td>
                                                <td><?= $accept_orders[$i]['distance_in_km'] ?></td>
                                                <td><?= $accept_orders[$i]['assigned_datetime'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="step-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Client Name</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Distance</th>
                                            <th>Assigned on</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($i=0; $i < sizeof($accept_orders); $i++) {  ?>
                                            <tr>
                                                <td><a href="<?php echo 'order-details/'.$accept_orders[$i]['order_id'].'' ?>"><?= $accept_orders[$i]['order_id'] ?></a></td>
                                                <td><?= $accept_orders[$i]['cust_name'] ?></td>
                                                <td><?= $accept_orders[$i]['from_address'] ?></td>
                                                <td><?= $accept_orders[$i]['to_address'] ?></td>
                                                <td><?= $accept_orders[$i]['distance_in_km'] ?></td>
                                                <td><?= $accept_orders[$i]['assigned_datetime'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="step-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                        <th><?= $this->lang->line('order_id'); ?></th>
                                        <th><?= $this->lang->line('client_name'); ?></th>
                                        <th><?= $this->lang->line('from'); ?></th>
                                        <th><?= $this->lang->line('TO'); ?></th>
                                        <th><?= $this->lang->line('distance'); ?></th>
                                        <th><?= $this->lang->line('assigned_on'); ?></th>>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($i=0; $i < sizeof($in_progress_orders); $i++) {  ?>
                                            <tr>
                                                <td><a href="<?php echo 'order-details/'.$in_progress_orders[$i]['order_id'].'' ?>"><?= $in_progress_orders[$i]['order_id'] ?></a></td>
                                                <td><?= $in_progress_orders[$i]['cust_name'] ?></td>
                                                <td><?= $in_progress_orders[$i]['from_address'] ?></td>
                                                <td><?= $in_progress_orders[$i]['to_address'] ?></td>
                                                <td><?= $in_progress_orders[$i]['distance_in_km'] ?></td>
                                                <td><?= $in_progress_orders[$i]['assigned_datetime'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="step-5" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                        <th><?= $this->lang->line('order_id'); ?></th>
                                        <th><?= $this->lang->line('client_name'); ?></th>
                                        <th><?= $this->lang->line('from'); ?></th>
                                        <th><?= $this->lang->line('TO'); ?></th>
                                        <th><?= $this->lang->line('distance'); ?></th>
                                        <th><?= $this->lang->line('assigned_on'); ?></th>>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($i=0; $i < sizeof($delivered_orders); $i++) {  ?>
                                            <tr>
                                                <td><a href="<?php echo 'order-details/'.$delivered_orders[$i]['order_id'].'' ?>"><?= $delivered_orders[$i]['order_id'] ?></a></td>
                                                <td><?= $delivered_orders[$i]['cust_name'] ?></td>
                                                <td><?= $delivered_orders[$i]['from_address'] ?></td>
                                                <td><?= $delivered_orders[$i]['to_address'] ?></td>
                                                <td><?= $delivered_orders[$i]['distance_in_km'] ?></td>
                                                <td><?= $delivered_orders[$i]['assigned_datetime'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>