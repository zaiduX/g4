<?php  $email_cut = explode('@', $cust['email1']);  $name = $email_cut[0];  ?>

<style> .avatar { width: 85%; } </style>
<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <?php $url = parse_url($cust['avatar_url']);?>
        <div class="profile-picture">
            <img src="<?= ($cust['avatar_url'] == "NULL")?base_url('resources/default-profile.jpg'):( isset($url['scheme']) AND ($url['scheme'] == "https" || $url['scheme'] == "http" )) ? $cust['avatar_url'] : base_url($cust['avatar_url']) ?>" class="img-thumbnail m-b avatar" alt="avatar" />

            <div class="stats-label text-color">
                <?php if( $cust['firstname'] != "NULL" || $cust['lastname'] != "NULL") : ?>
                    <span class="font-extra-bold font-uppercase"><?= $cust['firstname'] . ' ' .$cust['lastname']; ?></span>
                <?php else: ?>
                    <span class="font-extra-bold font-uppercase"><?= $name; ?></span>
                <?php endif; ?>
                <br /><br />
                <i class="fa fa-sign-out"></i> <a href="<?= base_url('log-out'); ?>"><?= $this->lang->line('logout'); ?></a>
            </div>
        </div>
    </div>
</aside>

<!-- Main Wrapper -->
<?php
$method_name = $this->router->fetch_method(); 
if($method_name == 'landing') { ?>
    <div id="wrapper" style="background: url(<?=base_url('resources/all_in.jpg')?>); background-repeat: no-repeat; background-size: 100%;" >
<?php } else { ?>
    <div id="wrapper" style="" >
<?php } ?>