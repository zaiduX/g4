    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>
          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/my-scrow'; ?>"><span><?= $this->lang->line('scrow'); ?></span></a></li>
              <li class="active"><span> <?= $this->lang->line('scrow_account_transactions'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('scrow_account_transactions'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel/my-scrow'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-money"></i> <?= $this->lang->line('my_scrow'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('user_scrow_transaction_details'); ?></small> 
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?= $this->lang->line('id'); ?></th>
                                    <th><?= $this->lang->line('date_time'); ?></th>
                                    <th><?= $this->lang->line('order_id'); ?></th>
                                    <th><?= $this->lang->line('transaction'); ?></th>
                                    <th><?= $this->lang->line('amount'); ?></th>
                                    <th><?= $this->lang->line('+_-'); ?></th>
                                    <th><?= $this->lang->line('balance'); ?></th>
                                    <th><?= $this->lang->line('Service Type'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($history as $histry) { ?>
                                <tr style="background-color: <?= $histry['type'] == '1' ? '#DFFFDF': '#FFDDDD'; ?>">
                                    <td><?= $histry['sh_id'] ?></td>
                                    <td><?= date('d-M-Y h:i:s A',strtotime($histry['datetime'])) ?></td>
                                    <td><?= $histry['order_id'] ?></td>
                                    <td><?= ucwords(str_replace('_',' ',$histry['transaction_type'])) ?></td>
                                    <td><?= $histry['currency_code'] . ' ' . $histry['amount'] ?></td>
                                    <td><?= $histry['type'] == '1' ? '<i class="fa fa-plus" style="color: green"></i>' : '<i class="fa fa-minus" style="color: red"></i>'; ?></td>
                                    <td><?= $histry['currency_code'] . ' ' . $histry['scrow_balance'] ?></td>
                                    <td><?php if($histry['cat_id']==281) echo $this->lang->line('Ticket Booking'); else if($histry['cat_id']==6) echo $this->lang->line('transport'); else echo $this->lang->line('lbl_courier') ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>