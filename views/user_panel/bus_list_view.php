    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Vehicles'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Vehicles'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-add'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('Available Vehicles'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th><?= $this->lang->line('id'); ?></th>
                                <th><?= $this->lang->line('Name/No'); ?></th>
                                <th><?= $this->lang->line('Modal').' ['.$this->lang->line('Make').']'; ?></th>
                                <th><?= $this->lang->line('Seat Type'); ?></th>
                                <th><?= $this->lang->line('A.C. Type'); ?></th>
                                <th><?= $this->lang->line('Total Seats'); ?></th>
                                <th><?= $this->lang->line('Vehicle Type'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($buses as $bus) { ?>
                            <tr>
                                <td><a href="<?= $this->config->item('base_url') . 'user-panel-bus/view-bus/' . $bus['bus_id'] ?>;" title="View details"><?= $bus['bus_id'] ?></a></td>
                                <td><?= strtoupper($bus['bus_no']) ?></td>
                                <td><?= strtoupper($bus['bus_modal']) . ' [' . strtoupper($bus['bus_make']).']' ?></td>
                                <td><?= strtoupper($bus['bus_seat_type']) ?></td>
                                <td><?= strtoupper($bus['bus_ac']) ?></td>
                                <td><?= strtoupper($bus['total_seats']) ?></td>
                                <td><?= $type = strtoupper($this->api->get_vehicle_type_by_id($bus['vehical_type_id'])['vehicle_type']) ?></td>
                                <td>
                                    <form action="bus-edit" method="post" style="display: inline-block;">
                                        <input type="hidden" name="bus_id" value="<?= $bus['bus_id'] ?>">
                                        <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-pencil"></i> <span class="bold"><?= $this->lang->line('edit'); ?></span></button>
                                    </form>
                                    <button style="display: inline-block;" class="btn btn-danger btn-sm busDeleteAlert" id="<?= $bus['bus_id'] ?>"><i class="fa fa-trash-o"></i> <span class="bold"><?= $this->lang->line('delete'); ?></span></button>
                                    <!-- <?php if(strtolower($type) == 'bus') { ?>
                                        <form action="bus-layout-manager" method="post" style="display: inline-block;">
                                            <input type="hidden" name="bus_id" value="<?= $bus['bus_id'] ?>">
                                            <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-crosshairs"></i> <span class="bold"><?= $this->lang->line('seat_layout'); ?></span></button>
                                        </form>
                                    <?php } ?> -->
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.busDeleteAlert').click(function () {
            var id = this.id;
            swal(
            {
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-bus/bus-delete')?>", {bus_id: id}, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('deleted'); ?>", "<?= $this->lang->line('Vehicle has been deleted!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting vehicle record!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Vehicle is safe!'); ?>", "error"); }
            });
        });
    </script>