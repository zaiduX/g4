    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-operator-cancellation-charges-list'; ?>"><?= $this->lang->line('cancellation_rescheduling_list'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('add_drivers'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('add_cancellation_rescheduling_details'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('enter_details'); ?></small>
            </div>
        </div>
    </div>
    
    <div class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="hpanel">
            <div class="panel-body">
              <form method="post" class="form-horizontal" action="add-bus-operator-cancellation-charges-details" enctype="multipart/form-data" id="cancellation_add">
                <div class="form-group">
                  <div class="col-md-12 text-center">
                    <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                    <?php endif; ?>  
                  </div>   
                </div>
                <div class="row form-group">
                  <div class="col-md-4">
                    <label class="control-label"><?= $this->lang->line('Select Vehicle Type'); ?></label>
                    <select class="form-control js-source-states" name="vehical_type_id" id="vehical_type_id">
                      <option value=""><?= $this->lang->line('Select Vehicle Type'); ?></option>
                        <?php foreach ($vehicle_types as $type) { ?>
                          <option value="<?= $type['vehical_type_id'] ?>"><?= $type['vehicle_type'] ?></option>
                        <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label class="control-label"><?= $this->lang->line('minimum_hours'); ?></label>
                    <input type="text" placeholder="<?= $this->lang->line('minimum_hours'); ?>" class="form-control" name="bcr_min_hours" id="bcr_min_hours">
                  </div>
                  <div class="col-md-4">
                    <label class="control-label"><?= $this->lang->line('maximum_hours'); ?></label>
                    <input type="text" placeholder="<?= $this->lang->line('maximum_hours'); ?>" class="form-control" name="bcr_max_hours" id="bcr_max_hours">
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-4">
                    <label class="control-label"><?= $this->lang->line('select_country'); ?></label>
                    <select name="country_id" id="country_id" class="form-control select2">
                      <option value=""> <?= $this->lang->line('select_country'); ?></option>
                      <?php foreach ($countries as $c) :?>
                        <option value="<?= $c['country_id'];?>"><?=$c['country_name'];?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label class="control-label"><?= $this->lang->line('cancellation_charges'); ?></label>
                    <input type="text" placeholder="<?= $this->lang->line('cancellation_charges'); ?>" class="form-control" name="bcr_cancellation" id="bcr_cancellation">
                  </div>
                  <div class="col-md-4">
                    <label class="control-label"><?= $this->lang->line('rescheduling_charges'); ?></label>
                    <input type="text" placeholder="<?= $this->lang->line('rescheduling_charges'); ?>" class="form-control" name="bcr_rescheduling" id="bcr_rescheduling">
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-md-12 text-right"><br />
                    <button class="btn btn-info" type="submit" id="btn_submit"><?= $this->lang->line('add_details'); ?></button>
                    <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-operator-cancellation-charges-list'; ?>" class="btn btn-primary" type="submit"><?= $this->lang->line('back'); ?></a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script>

  $(function(){

    $("#cancellation_add").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();

      var country_id = $("#country_id").val();
      var bcr_cancellation = parseFloat($("#bcr_cancellation").val()).toFixed(2);
      var bcr_rescheduling = parseFloat($("#bcr_rescheduling").val()).toFixed(2);
      var bcr_min_hours = parseFloat($("#bcr_min_hours").val()).toFixed(2);
      var bcr_max_hours = parseFloat($("#bcr_max_hours").val()).toFixed(2);

      if(isNaN(bcr_min_hours)) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Minimum hours value is Invalid! Enter Number Only.');?>","warning");   } 
      else if(bcr_min_hours < 0) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Minimum hours value is Invalid!');?>","warning");   } 
      
      else if(isNaN(bcr_max_hours)) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Maximum hours value is Invalid! Enter Number Only.');?>","warning");   } 
      else if(bcr_max_hours < 0) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Maximum hours value is Invalid!');?>","warning");   } 

      else if(parseFloat(bcr_max_hours) <= parseFloat(bcr_min_hours) ) { swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Invalid! Maximum Hours Should greater than Minimum Hours!');?>","warning")   }

      else if(country_id <= 0 ) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Please Select Country');?>","warning");   } 
      
      else if(isNaN(bcr_cancellation)) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Cancellation Percentage is Invalid! Enter Number Only.');?>","warning");   } 
      else if(bcr_cancellation < 0 && bcr_cancellation > 100) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Cancellation Percentage is Invalid!');?>","warning");   } 
      
      else if(isNaN(bcr_rescheduling)) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Rescheduling Percentage is Invalid! Enter Number Only.');?>","warning");   } 
      else if(bcr_rescheduling < 0 && bcr_rescheduling > 100) {  swal("<?=$this->lang->line('error');?>","<?=$this->lang->line('Rescheduling Percentage is Invalid!');?>","warning");   } 
      
      else {  $("#cancellation_add")[0].submit();  }
    });
  });
</script>