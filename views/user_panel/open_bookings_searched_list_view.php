<style type="text/css">
    .table > tbody > tr > td {
        border-top: none;
    }
    .dataTables_filter {
        display: none;
    }
</style>

    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('open_orders'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    <?= $this->lang->line('open_orders'); ?> &nbsp;&nbsp;&nbsp;
                </h2>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <!-- data Table -->
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="orderTableData" class="table">
                                    <thead><tr class="hidden"><th></th></tr></thead>
                                    <tbody>
                                        <?php foreach ($orders as $order) { ?>
                                        <tr>
                                            <td style="line-height: 15px;">
                                                <div class="hpanel filter-item" style="line-height: 15px;">
                                                    <a href="<?= $this->config->item('base_url') . 'user-panel/open-order-details/'.$order['order_id']; ?>">
                                                    <div class="panel-body" style="line-height: 15px;">
                                                        <div class="col-lg-12">
                                                        <div class="col-lg-2"> 
                                                            <h5 class="text-left text-light"><?= $this->lang->line('ref_G'); ?><?= $order['order_id'] ?></h5>
                                                        </div>
                                                        <div class="col-lg-6 text-center"> 
                                                          <h4>
                                                              <?php 
                                                              $category_details = $this->api->get_category_details_by_id($order['category_id']); 
                                                              $icon_url = $category_details['icon_url'];
                                                              if($order['category_id'] == 6) : ?>
                                                                <img src='<?= base_url($icon_url) ?>' /><?= $this->lang->line('transportation'); ?>
                                                              <?php else : ?>
                                                                <img src='<?= base_url($icon_url) ?>' /><?= $this->lang->line('lbl_courier'); ?>
                                                              <?php endif; ?>
                                                          </h4>
                                                          <h6 class=""><?= ($order['order_description'] !="NULL")?$order['order_description']:""; ?></h6>    
                                                        </div>
                                                        <div class="col-lg-4"> 
                                                            <h5 class="text-right text-light"><?= $this->lang->line('order_date'); ?>: <?= date('l, d M Y',strtotime($order['cre_datetime'])); ?></h5>
                                                            <h6 class="text-right text-light"><?= $this->lang->line('expiry_date'); ?>: <?= date('l, d M Y',strtotime($order['expiry_date'])); ?></h6>
                                                        </div>
                                                        <br/>
                                                    </div>
                                                        <h5 class="m-b-xs hidden"><?= $this->lang->line('cust_name'); ?> 
                                                      <?php 
                                                      if($order['cust_name'] == 'NULL' || $order['cust_name'] == 'NULL NULL') { 
                                                      $user_details = $this->api->get_user_details($order['cust_id']);
                                                      if($user_details['company_name'] != 'NULL') { echo $user_details['company_name']; } 
                                                      else { $email_cut = explode('@', $user_details['email1']); echo $email_cut[0]; }
                                                      } else { echo $order['cust_name']; } ?>
                                                    </h5>
                                                        <h5 class="m-b-xs"><?= $this->lang->line('from'); ?>: <?= ($order['from_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['from_address']; ?></h5>
                                                    <h5 class="m-b-xs"><?= $this->lang->line('to'); ?>: <?= ($order['to_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['to_address']; ?></h5>
                                                        <p class="small">
                                                            <div class="col-md-6"> <?= $this->lang->line('weight'); ?>: <?= $order['total_weight'] ?> <?= strtoupper($this->user->get_unit_name($order['unit_id'])); ?></div>
                                                            <div class="col-md-6"> <?= $this->lang->line('price'); ?>: <?= $order['currency_sign'] ?> <?= $order['order_price'] ?></div>
                                                        </p>
                                                        <p class="small">
                                                            <div class="col-md-6"> <?= $this->lang->line('advance'); ?>: <?= $order['currency_sign'] ?> <?= $order['advance_payment'] ?></div>
                                                            <div class="col-md-6"> <?= $this->lang->line('balance'); ?> <?= $order['currency_sign'] . ' '. ($order['order_price'] - $order['paid_amount']); ?></div>
                                                        </p>
                                                        <p class="small">
                                                            <div class="col-md-6"> <?= $this->lang->line('by'); ?>: <?= $this->user->get_vehicle_type_name($order['vehical_type_id']); ?></div>
                                                              <div class="col-md-6"> <?= $this->lang->line('no_of_packages'); ?>: <?= $order['package_count']; ?></div>
                                                        </p>
                                                        <p class="small">
                                                            <div class="col-md-6"> <?= $this->lang->line('pickup_date_time'); ?>: <?= $order['pickup_datetime'] ?></div>
                                                            <div class="col-md-6"> <?= $this->lang->line('deliver_date_time'); ?>: <?= $order['delivery_datetime'] ?></div>
                                                        </p>
                                                        <p class="small">
                                                            <div class="col-md-6 hidden"> <?= $this->lang->line('sender_phone'); ?>: <?= $order['from_address_contact'] == 'NULL' ? $this->lang->line('not_provided') : $order['from_address_contact'] ?></div>
                                                            <div class="col-md-6 hidden"> <?= $this->lang->line('reciever_phone'); ?>: <?= $order['to_address_contact'] == 'NULL' ? $this->lang->line('not_provided') : $order['to_address_contact'] ?></div>
                                                        </p>
                                                    </div>
                                                    </a>
                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-md-3 col-md-offset-9 text-right">
                                                                <?php if( $cust['is_deliverer'] == 0) : ?>
                                                                    <a href="<?= base_url('user-panel/deliverer-profile'); ?>" class="btn btn-outline btn-info"><?= $this->lang->line('accept'); ?></a>
                                                                <?php else: ?>
                                                                    <button id="<?= $order['order_id'] ?>" class="btn btn-outline btn-info acceptrequestalert" ><i class="fa fa-check"></i> <?= $this->lang->line('accept'); ?> </button>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


<script>
    $('.acceptrequestalert').click(function () {
        var id = this.id;
        swal({
            title: <?= json_encode($this->lang->line('are_you_sure'))?>,
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,
            cancelButtonText: <?= json_encode($this->lang->line('no'))?>,
            closeOnConfirm: false,
            closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    $.post('order-accepted-by-deliverer', {id: id, status: 'accept'}, function(res){
                        if($.trim(res) == 'order_accepted_workroom') {
                            swal(<?= json_encode($this->lang->line('accepted'))?>, "", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else if ($.trim(res) == 'order_accept_failed') {
                            swal(<?= json_encode($this->lang->line('error'))?>, "", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal(<?= json_encode($this->lang->line('error'))?>, "", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else {
                    swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");
                }
            });
        });
</script>