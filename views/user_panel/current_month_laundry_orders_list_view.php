<style>
    #map_wrapper { height: 300px; }
    #map_canvas { width: 100%; height: 100%; }
</style>

<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
            </a>
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry')?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                    <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry-users')?>"><span><?=$this->lang->line('My Dashboard');?></span></a></li>
                    <li class="active"><span><?=$this->lang->line('This month laundry orders');?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-shopping-basket fa-2x text-muted"></i> <?= $this->lang->line('This month laundry orders'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?=base_url('user-panel-laundry/dashboard-laundry-users')?>" class="btn btn-outline btn-info" ><i class="fa fa-users"></i> <?= $this->lang->line('My Dashboard'); ?></a></h2>
            <small class="m-t-md"><?=$this->lang->line('All platform orders for this month');?></small> 
        </div>
    </div>
</div>
<br />
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel hblue">
                <div class="panel-body">
                    <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th><?= $this->lang->line('Order ID'); ?></th>
                                <th><?= $this->lang->line('Customer Name'); ?></th>
                                <th><?= $this->lang->line('contact'); ?></th>
                                <th><?= $this->lang->line('Country'); ?></th>
                                <th><?= $this->lang->line('order_price'); ?></th>
                                <th><?= $this->lang->line('Order Date Time'); ?></th>
                                <th><?= $this->lang->line('order_status'); ?></th>
                                <th><?= $this->lang->line('Update Date Time'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($current_month_order_list as $order) { ?>
                            <tr>
                                <td><?=$order['booking_id']?></td>
                                <td><?=$order['cust_name']?></td>
                                <td><?=$this->api->get_country_code_by_id($order['country_id']).' '.$order['cust_contact']?></td>
                                <td><?=$this->api->get_country_name_by_id($order['country_id'])?></td>
                                <td><?=$order['total_price']. ' ' .$order['currency_sign']?></td>
                                <td><?=$order['cre_datetime']?></td>
                                <td><?=ucwords(str_replace('_', ' ', $order['booking_status']))?></td>
                                <td><?=$order['status_update_datetime']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>