    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('post_your_query'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs"> <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('post_your_query'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= base_url('user-panel-laundry/get-support'); ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?> </a></h2>
          <small class="m-t-md"><?= $this->lang->line('all_your_queries_and_suggestions'); ?></small> 
        </div>
      </div>
    </div>
   
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?= $this->lang->line('id'); ?></th>
                                    <th><?= $this->lang->line('date'); ?></th>
                                    <th><?= $this->lang->line('category'); ?></th>
                                    <th><?= $this->lang->line('description'); ?></th>
                                    <th><?= $this->lang->line('status'); ?></th>
                                    <th style="text-align: center;"><?= $this->lang->line('action'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($support_list as $sl) { ?>
                                <tr>
                                    <td><?= $sl['ticket_id'] ?></td>
                                    <td><?= $sl['cre_datetime'] ?></td>
                                    <td><?= $sl['type'] ?></td>
                                    <td><?= $sl['description'] ?></td>
                                    <td><?= strtoupper($sl['status']) ?></td>
                                    <td style="text-align: center;">
                                        <a title="<?= $this->lang->line('view_reply'); ?>" class="btn btn-warning btn-sm" href="<?= $this->config->item('base_url') . 'user-panel-laundry/view-support-reply-details/'.$sl['query_id']; ?>"><i class="fa fa-search"></i> <?= $this->lang->line('view_response'); ?></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>