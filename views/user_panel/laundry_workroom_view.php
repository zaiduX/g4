 <?php
  $provider_id = $keys["provider_id"];
  $cust_id = $keys["cust_id"];
  $booking_id = $keys["booking_id"];
?> 
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry')?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('work_room'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-comments fa-2x text-muted"></i> <?= $this->lang->line('work_room'); ?>
      <small class="m-t-md"><?= $this->lang->line('Laundry Bookings Workroom'); ?></small></h2>
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="hpanel hblue">
        <div class="panel-body mt-0 pt-0">
          <div class="row">
            <div class="=col-lg-12">
                 <h3 class="stat-label" style='color: #3498db'><?= $this->lang->line('Booking Details'); ?>                                 
                 </h3>
            </div>
            <br/>
            <div class="col-md-6">
              <h5><lable style='color: #3498db'> <?=$this->lang->line('provider')?></lable>&nbsp;:&nbsp;<?=$booking['operator_name']?></h5>
            </div>
            <div class="col-md-6">
              <h5><lable style='color: #3498db'> <?=$this->lang->line('Provider Company')?></lable>&nbsp;:&nbsp;<?=$booking['operator_company_name']?></h5>
            </div>
            <div class="col-md-6">
              <h5><lable style='color: #3498db'> <?=$this->lang->line('Booking Date')?></lable>&nbsp;:&nbsp;<?=$booking['cre_datetime']?></h5>
            </div>
            <div class="col-md-6">
              <h5><lable style='color: #3498db'> <?=$this->lang->line('No. of Items')?></lable>&nbsp;:&nbsp;<?=$booking['item_count']?></h5>
            </div>
          </div>           
        </div>
      </div>

      <div class="hpanel horange">
        <div class="panel-body no-padding">
          <div class="row">
            <div class="col-md-12">
              <ul class="chat-discussion">
                <?php $total = COUNT($workroom); foreach(array_reverse($workroom) as $v => $w): ?>
                  <?php date_default_timezone_set($this->session->userdata("default_timezone")); $ud = strtotime($w['cre_datetime']); ?>
                  <?php date_default_timezone_set($this->session->userdata("user_timezone")); ?>
                  <li class="chat-message <?= ($w['sender_id'] == $w['cust_id'])?'right':'left'; ?>" <?=($total==($v+1))?"tabindex='1'":'';?>>
                    <div class="message">
                      <?php if($w['sender_id'] == $w['cust_id']): ?>
                        <a class="message-author text-right"><?= ($w['cust_name'] !="NULL")?strtoupper($w['cust_name']):"User";?> </a>
                         <span class="message-date">  <?= date('l, M / d / Y  h:i a', $ud); ?></span>
                        <div class="row">
                          <?php if($w['type'] == "raise_invoice"): ?>
                            <div class="col-md-3 text-left">
                              <p style="margin-top:10px;"><a href="<?= base_url().'resources/'.$w['attachment_url'];?>" target="_blank">
                                <i class="fa fa-file-pdf-o fa-2x"></i> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a>
                              </p>
                            </div>
                            <div class="col-md-9">
                              <span class="message-content">
                                <h5><?= $this->lang->line('order_invoice'); ?></h5>
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                          <?php endif; ?>

                          <?php if($w['type'] == "review_rating"): ?>
                            <div class="col-md-3 text-left">
                              <?php for($i=0; $i < (int) $w['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                              <?php for($i=0; $i < ( 5-(int) $w['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                              ( <?= $w['ratings'] ?> / 5 )
                            </div>
                            <div class="col-md-9">
                              <span class="message-content">
                                <h5>Ratings &amp; Review</h5>
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                          <?php endif; ?>

                          <?php if($w['type'] == "order_status"): ?>
                            <div class="col-md-3 text-left"></div>
                            <div class="col-md-9">
                              <span class="message-content">
                                <h5><?= $this->lang->line('order_status'); ?></h5>
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                          <?php endif; ?>

                          <?php if($w['type'] == "chat"): ?>
                            <div class="col-md-3 text-left">
                              <?php if($w['attachment_url'] !="NULL"): ?>
                                <p>&nbsp;</p>
                                <p><a href="<?= base_url().$w['attachment_url'];?>" target="_blank"> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a><p>
                              <?php endif; ?>
                            </div>
                            <div class="col-md-9">
                              <span class="message-content">
                                <h5><?= $this->lang->line('message'); ?></h5>
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                          <?php endif; ?>
                          

                          <?php if($w['type'] == "payment"): ?>
                            <div class="col-md-9">
                              <span class="message-content">
                                <!-- <h5><?= $this->lang->line('message'); ?></h5> -->
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                          <?php endif; ?>


                        </div>
                      <?php else: ?>
                        <a class="message-author text-left"><?= ($w['deliverer_name']!="NULL" && $w['deliverer_name']!="NULL NULL")?strtoupper($w['deliverer_name']):"Deliverer";?> </a>
                        <span class="message-date"><?php $ud = strtotime($w['cre_datetime']); echo date('l, M / d / Y  h:i a', $ud); ?></span>
                        <div class="row">

                          <?php if($w['type'] == "raise_invoice"): ?>
                            <div class="col-md-9">
                              <span class="message-content">
                                <h5><?= $this->lang->line('order_invoice'); ?></h5>
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                            <div class="col-md-3 text-left">
                              <p style="margin-top:10px;"><a href="<?= base_url().'resources/'.$w['attachment_url'];?>" target="_blank"><i class="fa fa-file-pdf-o fa-2x"></i> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a></p>
                            </div>
                          <?php endif; ?>

                          <?php if($w['type'] == "review_rating"): ?>
                            <div class="col-md-9">
                              <span class="message-content">
                                <h5><?= $this->lang->line('ratings_review'); ?></h5>
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                            <div class="col-md-3 text-left">
                              <?php for($i=0; $i < (int) $w['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                              <?php for($i=0; $i < ( 5-(int) $w['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                              ( <?= $w['ratings'] ?> / 5 )
                            </div>
                          <?php endif; ?>

                          <?php if($w['type'] == "order_status"): ?>
                            <div class="col-md-9">
                              <span class="message-content">
                                <h5><?= $this->lang->line('order_status'); ?></h5>
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                            <div class="col-md-3 text-left"></div>
                          <?php endif; ?>

                          <?php if($w['type'] == "chat"): ?>
                            <div class="col-md-9">
                              <span class="message-content">
                                <h5><?= $this->lang->line('message'); ?></h5>
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                            <div class="col-md-3 text-left">
                              <?php if($w['attachment_url'] !="NULL"): ?>
                                <p>&nbsp;</p>
                                <p><a href="<?= base_url().$w['attachment_url'];?>" target="_blank"> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a><p>
                              <?php endif; ?>
                            </div>
                          <?php endif; ?>

                          <?php if($w['type'] == "payment"): ?>
                            <div class="col-md-9">
                              <span class="message-content">
                                <!-- <h5><?= $this->lang->line('message'); ?></h5> -->
                                <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                              </span>
                            </div>
                          <?php endif; ?>
                        </div>
                      <?php endif; ?>
                    </div>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="panel-footer borders">
          <div class="row form-group">
            <?php if($booking['provider_id'] != $cust_id): ?>
              <div class="col-lg-1">
                <select id="send_option" class="select2 form-group">
                  <option value="chat"><?= $this->lang->line('chat'); ?></option>
                  <option value="rating"><?= $this->lang->line('rating'); ?></option>
                </select>
              </div>
            <?php else: ?>
              <div class="col-lg-1"></div>
            <?php endif; ?>

            <div id="chat_div" class="col-lg-11">
              <form action="<?=base_url('user-panel-laundry/add-laundry-workroom-chat');?>" id="chatForm" method="post" enctype="multipart/form-data">
                <input type="hidden" name="booking_id" value="<?=$booking['booking_id']; ?>" />
                <div class="input-group">
                  <span class="input-group-btn">
                    <button id="upload_file" class="btn fa fa-paperclip fa-2x"></button>
                  </span>
                  <input type="text" name="message_text" class="form-control" placeholder="Type your message here...">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-success"><?= $this->lang->line('send'); ?></button>
                  </span>
                </div>
                <span id="file_name"></span>
                <input type="file" id="attachment" name="attachment" class="upload" accept="image/*" onchange="attachement_name(event)" />
              </form>
            </div>

            <div id="rating_div" class="col-lg-11 hidden">
              <form action="<?=base_url('user-panel-laundry/add-laundry-workroom-rating');?>" id="ratingForm" method="post">
                <input type="hidden" name="booking_id" value="<?= $booking['booking_id']; ?>" />
                <div class="form-group">
                  <div class="col-lg-3">
                    <input id="rating" name="rating" class="rating" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" required />
                    <span id="error_rating" class="text-danger"></span>
                  </div>
                  <div class="input-group">
                    <input type="text" id="review_text" name="review_text" class="form-control" placeholder="Type your review here...">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-success"><?= $this->lang->line('send'); ?></button>
                    </span>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script>

  $(function(){

    $("#send_option").on('change', function(event) {  event.preventDefault();
      var option = $(this).val();
      if(option =="rating") { $("#rating_div").removeClass('hidden'); $("#chat_div").addClass('hidden'); }
      else { $("#chat_div").removeClass('hidden'); $("#rating_div").addClass('hidden'); }
    });

    $("#ratingForm").submit(function(e) { e.preventDefault();
      var form = $(this).get(0);
      var rating = $("#rating").val();
      if(rating == 0 ) {
        $("#error_rating").removeClass('hidden').html('Give some rating.');
      } else { form.submit(); }
    });


  });
</script>