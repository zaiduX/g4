    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                  <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
                </a>
                <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-cancelled-trips'; ?>"><?= $this->lang->line('Cancel Trip'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Passenger Details'); ?></span></li>
                </ol>
                </div>
                <h2 class="font-light m-b-xs"> <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Passenger Details'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('Cancelled trip passengers details'); ?></small> 
            </div>
        </div>
    </div>
 <?php require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php"); ?>
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="=col-lg-12">
                                 <h3 class="stat-label" style='color: #3498db'><?= $this->lang->line('canceled')." ".$this->lang->line('Trip Details'); ?>                                 
                                 </h3>
                            </div>
                            <br/>
                            <div class="col-md-6">
                                <h5><lable style='color: #3498db'> <?=$this->lang->line('Source')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['source_point']?></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><lable style='color: #3498db'> <?=$this->lang->line('Destination')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['destination_point']?></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><lable style='color: #3498db'> <?=$this->lang->line('Journey Date')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['journey_date']?></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><lable style='color: #3498db'> <?=$this->lang->line('Departure Time')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['trip_start_date_time']?></h5>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <h3 class="stat-label" style='color: #3498db'><?= $this->lang->line('Passenger Details'); ?>                                 
                                 </h3>
                        </div>


                           <table id="example12" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <th><?= $this->lang->line('Ticket ID'); ?></th>
                                    <th><?= $this->lang->line('Name'); ?></th>
                                    <th><?= $this->lang->line('Total Seats'); ?></th>
                                    <th><?= $this->lang->line('bus_seat_type'); ?></th>
                                    <th><?= $this->lang->line('Total_Price'); ?></th>
                                    <th><?= $this->lang->line('Balance Amount'); ?></th>
                                    <th><?= $this->lang->line('Contact'); ?></th>
                                    <th><?= $this->lang->line('Gender'); ?></th>
                                   <!--  <th><?= $this->lang->line('Payment'); ?></th> -->
                                </thead>
                                <tbody>
                                    <?php $c="yes"; foreach ($master_booking_details as $seats) { ?>
                                    <tr>
                                        <td><?=$seats['ticket_id']?></td>
                                        <td>
                                            <?php $name = $this->api->get_ticket_seat_details($seats['ticket_id'] ,$c);
                                            echo $name[0]['firstname']." ".$name[0]['lastname'];
                                             if(sizeof($name)>1)
                                             {
                                                echo "<button style='color: #3498db' type='button' class='btn btn-link' data-toggle='modal' data-target='#".$name[0]['seat_id']."'>".$this->lang->line('...More')."</button>";
                                             } 
                                            ?>
                                            <div class="modal fade" id="<?=$name[0]['seat_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel"><?=$this->lang->line('More Seat Details')?></h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <table class="table" width="100%">
                                                        <thead>
                                                            <th><?= $this->lang->line('Ticket ID'); ?></th>
                                                            <th><?= $this->lang->line('Name'); ?></th>
                                                            <th><?= $this->lang->line('Gender'); ?></th>
                                                            <th><?= $this->lang->line('Contact'); ?></th>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($name as $seat) { ?>
                                                            <tr>
                                                                <td><?=$seat['ticket_id']?></td>
                                                                <td><?=$seat['firstname']. ' ' .$seat['lastname']?></td>
                                                                <td><?=($seat['gender']=='M')?'Male':'Female'?></td>
                                                                <td><?=$this->api->get_country_code_by_id($seat['country_id']).$seat['mobile']?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                        <td><?=sizeof($name)?></td>
                                        <td><?=$seats['seat_type']?></td>
                                        <td><?=$seats['ticket_price'].$seats['currency_sign']?></td>
                                        <td><?=$seats['balance_amount'].$seats['currency_sign']?></td>
                                        <td><?=$this->api->get_country_code_by_id($name[0]['country_id']).$name[0]['mobile']?></td>
                                        <td><?=($name[0]['gender']=='M')?'Male':'Female'?></td>
                                        
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>             
                    </div>
                </div>
            </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-lg-6">
                                <a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-cancelled-trips'; ?>" class="btn btn-info btn-outline " ><?= $this->lang->line('back'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
<script type="text/javascript">
     $(function () {
        $('#example12').dataTable( {
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            buttons: [
                {extend: 'copy',className: 'btn-sm'},
                {extend: 'csv',title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'print',className: 'btn-sm'}
            ]
        });
    });      
</script>
