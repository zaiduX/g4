<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('profile'); ?></span></li>
          <li><span><?= $this->lang->line('details'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('add_education:'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-graduation-cap fa-2x text-muted"></i> <?= $this->lang->line('education'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('add_education_details'); ?></small>     
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel/add-education'); ?>" method="post" class="form-horizontal" id="addEducation">
          <div class="panel-body">              
            <div class="col-lg-10 col-lg-offset-1">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
            
              <div class="row">
                <div class="form-group">
                  <label class=""><?= $this->lang->line('uni_insi'); ?></label>
                  <input name="institute_name" type="text" class="form-control" data-toggle="tooltip" data-placement="top" title="<?= $this->lang->line('uni_insi'); ?>" placeholder="<?= $this->lang->line('uni_insi'); ?>" />
                </div>
                <div class="form-group">
                  <label class=""><?= $this->lang->line('qualification'); ?></label>
                  <input name=" qualification" type="text" class="form-control" id="" placeholder="<?= $this->lang->line('qualification'); ?>" />
                </div>
                <div class="form-group">
                  <label class=""><?= $this->lang->line('title'); ?></label>
                  <input name="title" type="text" class="form-control" id="" placeholder="<?= $this->lang->line('title'); ?>" />
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <label class=""><?= $this->lang->line('year_of_quali'); ?></label>
                      <select name="yr_qualification" class="form-control select2" id="yr_qualification">
                        <?php $years = range(1910,date("Y")); rsort($years); ?>
                        <option value=""><?= $this->lang->line('select_year'); ?></option>
                        <?php foreach ($years as $y): ?>
                          <option value="<?= $y; ?>"><?= $y; ?></option>
                        <?php endforeach; ?>                        
                      </select>                      
                    </div>
                    <div class="col-lg-6">
                      <label class=""><?= $this->lang->line('grade'); ?></label>
                      <input name="grade" type="text" class="form-control" id="" placeholder="<?= $this->lang->line('grade'); ?>" />
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <label class=""><?= $this->lang->line('attend'); ?></label>
                      <select name="yr_attend" class="form-control select2" id="yr_attend">
                        <?php $years = range(1910,date("Y")); rsort($years); ?>
                        <option value=""><?= $this->lang->line('select_year'); ?></option>
                        <?php foreach ($years as $y): ?>
                          <option value="<?= $y; ?>"><?= $y; ?></option>
                        <?php endforeach; ?>                        
                      </select>                      
                    </div>
                    <div class="col-lg-6">
                      <label class=""><?= $this->lang->line('to'); ?></label>
                      <select name="yr_attend_to" class="form-control select2" id="yr_attend_to">
                        <?php $years = range(1910,date("Y")); rsort($years); ?>
                        <option value=""><?= $this->lang->line('select_year'); ?></option>
                        <?php foreach ($years as $y): ?>
                          <option value="<?= $y; ?>"><?= $y; ?></option>
                        <?php endforeach; ?>                        
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class=""><?= $this->lang->line('addtional_information_optional'); ?></label>
                  <textarea name="additional_info"  class="form-control" id="" rows="5" placeholder="<?= $this->lang->line('addtional_information_optional'); ?>" style="resize: none;"></textarea>
                </div> 
              </div>
                                                    
            </div>

          </div>        
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-lg-5 col-lg-offset-1 text-left">
                  <a href="<?= base_url('user-panel/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
               </div>
               <div class="col-lg-5 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('add_detail'); ?></button>
                  </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $("#addEducation").validate({
    ignore: [],
    rules: {
      institute_name: { required: true, },
      qualification: { required: true,},
      title: { required: true, },
      yr_qualification: { required: true, },
      grade: { required: true, },
      yr_attend: { required: true, },
      yr_attend_to: { required: true, },
    }, 
    messages: {
      institute_name: { required: <?= json_encode($this->lang->line('uni_insi'))?>,   },
      qualification: { required: <?= json_encode($this->lang->line('qualification'))?>, },
      title: { required: <?= json_encode($this->lang->line('title'))?>,  },
      yr_qualification: { required: <?= json_encode($this->lang->line('select_year'))?>,  },
      grade: { required: <?= json_encode($this->lang->line('grade'))?>,  },
      yr_attend: { required: <?= json_encode($this->lang->line('attend'))?>,  },
      yr_attend_to: { required: <?= json_encode($this->lang->line('select_year'))?>,  },
    }
  });
</script>