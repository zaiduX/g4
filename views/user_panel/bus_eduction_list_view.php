<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('profile'); ?></span></li>
          <li><span><?= $this->lang->line('education'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('list'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-graduation-cap fa-2x text-muted"></i> <?= $this->lang->line('education'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('list_of_added_educations'); ?></small>    
    </div>
  </div>
</div>
<div class="content">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" >
      <!-- Education Area -->
      <div class="row">
      <div class="hpanel hblue">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
              <h4>
                <strong> <?= $this->lang->line('list_of_added_educations'); ?> </strong> &nbsp;
                <a href="<?= base_url('user-panel-bus/education/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
              </h4>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
              <a href="<?= base_url('user-panel-bus/user-profile'); ?>" class="btn btn-primary"><i class="fa fa-left-arrow"></i> <?= $this->lang->line('go_to_profile'); ?></a>
            </div>
          </div>
        </div>
        
        <?php if($educations): ?>
          <?php foreach ($educations as $education): ?>
            <div class="panel-body">
              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4"> <p><strong><?= $this->lang->line('university_institute_name'); ?></strong></p> </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">  <p style="word-wrap: break-word;"><?= $education['institute_name']; ?></p> </div>  
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-right">
                  <a class="btn btn-info btn-circle" href="<?= base_url('user-panel-bus/education/').$education['edu_id'];?>">
                    <i class="fa fa-pencil"></i>
                  </a>
                  <a class="btn btn-danger btn-circle btn-delete" id="<?=$education['edu_id'];?>">
                    <i class="fa fa-trash"></i>
                  </a>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4"> <p><strong><?= $this->lang->line('qualification'); ?></strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $education['qualification']; ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4"> <p><strong><?= $this->lang->line('title'); ?></strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $education['certificate_title']; ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4"> <p><strong><?= $this->lang->line('years_of_qualification_attend_years'); ?></strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  
                  <p style="word-wrap: break-word;"><?= $education['qualify_year'] . ' [ '. date('Y', strtotime($education['start_date'])); ?> - <?= date('Y', strtotime($education['end_date'])). ' ]'; ?></p>
                </div>  
              </div>

              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4"> <p><strong><?= $this->lang->line('grade'); ?></strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $education['grade']; ?></p> </div>  
              </div>

              <div class="row hidden">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4"> <p><strong><?= $this->lang->line('remark'); ?></strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= nl2br($education['remark']); ?></p> </div>  
              </div> 

              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4"> <p><strong><?= $this->lang->line('additional_information'); ?></strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= nl2br($education['description']); ?></p> </div>  
              </div>
              

            </div> <br/>     
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
      </div>
      <!--End: Education Area -->
    </div>
  </div>
</div>

<script>
  $('.btn-delete').click(function () {            
    var id = this.id;            
    swal({                
      title: <?= json_encode($this->lang->line('are_you_sure'))?>,                
      text: "",                
      type: "warning",                
      showCancelButton: true,                
      confirmButtonColor: "#DD6B55",                
      confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,                
      cancelButtonText: <?= json_encode($this->lang->line('no'))?>,                
      closeOnConfirm: false,                
      closeOnCancel: false, 
    },                
    function (isConfirm) {                    
      if (isConfirm) {                        
        $.post('delete-education', {id: id}, 
        function(res){
          if($.trim(res) == "success") {
            swal(<?= json_encode($this->lang->line('deleted'))?>, "Education successfully deleted.", "success");                            
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal("Cancelled", "Failed to delete education! Try again...", "error");  }  
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, "Education delete cancelled", "error");  }  
    });            
  });
</script>
