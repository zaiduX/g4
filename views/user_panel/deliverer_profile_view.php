<style>     
  .avatar { width: 22.3%; max-height: 157px;  }   .cover {        <?php if($deliverer['cover_url'] == "NULL"): ?>         background-image: url(<?= $this->config->item('resource_url'). 'bg.jpg'; ?>) !important;        <?php else: ?>          background-image: url(<?= base_url($deliverer['cover_url']); ?>) !important;        <?php endif; ?>     background-position: center !important;    background-repeat: no-repeat !important;    background-size: cover !important;    height: 200px;  }
</style>

  <div class="normalheader small-header">
    <div class="hpanel">
      <div class="panel-body">
        <a class="small-header-action" href="">
          <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
          </div>
        </a>

        <div id="hbreadcrumb" class="pull-right">
          <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('deliverer_profile'); ?></span></li>
          </ol>
        </div>
        <h2 class="font-light m-b-xs">  <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('deliverer_profile'); ?> </h2>
        <small class="m-t-md"><?= $this->lang->line('manage_your_deliverer_profile_here'); ?></small>    
      </div>
    </div>
  </div>

  <div class="content content-boxed">
    <div class="row">
      <div class="col-lg-12" >
        <div class="hpanel hgreen" >
          <div class="panel-body cover">
            <?php if($deliverer['avatar_url'] == "NULL"): ?>
              <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b avatar" alt="avatar" />
            <?php else: ?>
              <img src="<?= base_url($deliverer['avatar_url']); ?>" class="img-thumbnail m-b avatar" alt="avatar" />
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="hpanel hblue">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                        <p><strong><?= $this->lang->line('first_name'); ?></strong></p>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4">    
                        <p>
                            <?= $deliverer['firstname'] != "NULL" ? $deliverer['firstname'] : $this->lang->line('not_provided') ?>
                        </p>
                    </div>  
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-right">
                      <a href="<?= $this->config->item('base_url') . 'user-panel/edit-deliverer-profile'; ?>" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                        <p><strong><?= $this->lang->line('last_name'); ?></strong></p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                        <p>
                            <?= $deliverer['lastname'] != "NULL" ? $deliverer['lastname'] : $this->lang->line('not_provided') ?>
                        </p>
                    </div>  
                </div>  
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                        <p><strong><?= $this->lang->line('company_name'); ?></strong></p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                        <p>
                            <?= $deliverer['company_name'] != "NULL" ? $deliverer['company_name'] : $this->lang->line('not_provided') ?>
                        </p>
                    </div>  
                </div> 
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                        <p><strong><?= $this->lang->line('email_address'); ?></strong></p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                        <p>
                            <?= $deliverer['email_id'] != "NULL" ? $deliverer['email_id'] : $this->lang->line('not_provided') ?>
                        </p>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                        <p><strong><?= $this->lang->line('mobile_number'); ?></strong></p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                        <p>
                            <?= $deliverer['contact_no'] != "NULL" ? $deliverer['contact_no'] : $this->lang->line('not_provided') ?>
                        </p>
                    </div>  
                </div>
                <div class="collapse in" id="education">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                            <p><strong><?= $this->lang->line('city_state_country'); ?></strong></p>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                            <p>
                                <?= $deliverer['city_id'] != "NULL" ? $this->api->get_city_name_by_id($deliverer['city_id']) : $this->lang->line('not_provided') ?> / 
                                <?= $deliverer['state_id'] != "NULL" ? $this->api->get_state_name_by_id($deliverer['state_id']) : $this->lang->line('not_provided') ?> / 
                                <?= $deliverer['country_id'] != "NULL" ? $this->api->get_country_name_by_id($deliverer['country_id']) : $this->lang->line('not_provided') ?>
                            </p>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                            <p><strong><?= $this->lang->line('address'); ?></strong></p>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                            <p>
                                <?= $deliverer['address'] != "NULL" ? $deliverer['address'] : $this->lang->line('not_provided') ?>
                            </p>
                        </div>  
                    </div> 
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                            <p><strong><?= $this->lang->line('zip_code'); ?></strong></p>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                            <p>
                                <?= $deliverer['zipcode'] != "NULL" ? $deliverer['zipcode'] : $this->lang->line('not_provided') ?>
                            </p>
                        </div>  
                    </div> 
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                            <p><strong><?= $this->lang->line('promotion_code'); ?></strong></p>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                            <p>
                                <?= $deliverer['promocode'] != "NULL" ? $deliverer['promocode'] : $this->lang->line('not_provided') ?>
                            </p>
                        </div>  
                    </div> 
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                            <p><strong><?= $this->lang->line('introduction'); ?></strong></p>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                            <p>
                                <?= $deliverer['introduction'] != "NULL" ? $deliverer['introduction'] : $this->lang->line('not_provided') ?>
                            </p>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">    
                            <p><strong><?= $this->lang->line('payment_informations'); ?></strong></p>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6">    
                            <p>
                                <?= $deliverer['shipping_mode'] == 0 ? 'Manual' : 'Automatic' ?>
                            </p>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="hpanel hred hidden">
            <div class="panel-heading">
                <h4><strong><?= $this->lang->line('payment_informations'); ?></strong></h4>
            </div>
            <div class="panel-body">
                <div class="row hidden">
                    <div class="col-md-4">    
                        <p><strong><?= $this->lang->line('shipping_accepting_mode'); ?></strong></p>
                    </div>
                    <div class="col-md-6">    
                        <p>
                            <?= $deliverer['shipping_mode'] != "0" ? 'Manual' : 'Automatic' ?>
                        </p>
                    </div>  
                    <div class="col-md-2 text-right">
                        <form action="edit-deliverer-bank" method="post" style="display: inline-block;">
                            <input type="hidden" name="cust_id" value="<?= $deliverer['cust_id'] ?>">
                            <button type="submit" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></button>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">    
                        <p><strong><?= $this->lang->line('iban'); ?></strong></p>
                    </div>
                    <div class="col-md-6">    
                        <p>
                            <?= ($deliverer['iban'] != "NULL") ? $deliverer['iban'] : $this->lang->line('not_provided') ?>
                        </p>
                    </div>  
                    <div class="col-md-2 text-right">
                        <form action="edit-deliverer-bank" method="post" style="display: inline-block;">
                            <input type="hidden" name="cust_id" value="<?= $deliverer['cust_id'] ?>">
                            <button type="submit" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></button>
                        </form>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-4">    
                        <p><strong><?= $this->lang->line('1_Mobile_Money_Account'); ?></strong></p>
                    </div>
                    <div class="col-md-8">    
                        <p>
                            <?= ($deliverer['mobile_money_acc1'] != "NULL") ? $deliverer['mobile_money_acc1'] : $this->lang->line('not_provided') ?>
                        </p>
                    </div>  
                </div> 
                <div class="row">
                    <div class="col-md-4">    
                        <p><strong><?= $this->lang->line('2_Mobile_Money_Account'); ?></strong></p>
                    </div>
                    <div class="col-md-8">    
                        <p>
                            <?= ($deliverer['mobile_money_acc2'] != "NULL") ? $deliverer['mobile_money_acc2'] : $this->lang->line('not_provided') ?>
                        </p>
                    </div>  
                </div>    
            </div>
        </div>
        <div class="hpanel hblue">
          <div class="panel-heading">
            <h4><strong><?= $this->lang->line('deliverer_document'); ?></strong></h4>
          </div>
          <div class="panel-body">
            <div class="col-xl-11 col-lg-11 col-md-11 col-sm-10">
              <?php for($i=0; $i < sizeof($documents); $i++) { ?>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                  <div class="hpanel">
                    <div class="panel-body file-body">
                      <i class="fa fa-file-pdf-o text-info"></i>
                    </div>
                    <div class="panel-footer text-center">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                          <h5><a href="<?= $this->config->item('base_url') . $documents[$i]['attachement_url']; ?>"><?= strtoupper($documents[$i]['doc_type']) ?></a></h5>
                          <button title="Delete Document" style="display: inline-block;" class="btn btn-danger btn-sm docdeleteatealert" id="<?= $documents[$i]['doc_id'] ?>"><i class="fa fa-trash"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
            <?php if(sizeof($documents) < 3) { ?>
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-2 text-right">
              <a title="Add Document" href="<?= $this->config->item('base_url') . 'user-panel/edit-deliverer-document'; ?>" class="btn btn-default btn-circle"><i class="fa fa-plus"></i></a>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>