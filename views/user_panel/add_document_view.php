<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span>Profile</span></li>
          <li><span>Details</span></li>
          <li class="active"><span>Add Document</span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-file-text-o fa-2x text-muted"></i> Document </h2>
      <small class="m-t-md">Upload Important Documents</small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel/add-document'); ?>" method="post" class="form-horizontal" id="addDoc" enctype="multipart/form-data">        
          <div class="panel-body">              
            <div class="col-lg-10 col-lg-offset-1">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
            
              <div class="row">                
                <div class="form-group">
                  <div class="col-lg-8">
                    <label class="">Title</label>
                    <input name="title" type="text" class="form-control" placeholder="Enter document title" required />
                  </div>
                  <div class="col-lg-4">                                      
                    <label class="">Document Image :</label>
                    <div class="input-group">
                      <span class="input-group-btn">
                        <button id="document_image" class="btn btn-green col-lg-12"><i class="fa fa-user"></i>&nbsp; Select Document Image</button> 
                      </span> 
                    </div>
                    <span id="document_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; Document Image Attached</span>
                    <input type="file" id="document" name="document" class="upload attachment" accept="image/*" onchange="document_name(event)"/>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <div class="col-lg-12">
                    <label class="">Description ( Optional ) :</label>
                    <textarea name="description"  class="form-control" id="description" rows="5" placeholder="Write document description ..." style="resize: none;"></textarea>
                  </div> 
                </div> 
              </div> 
            </div>
          </div>
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-lg-6 text-left">
                  <a href="<?= base_url('user-panel/user-profile'); ?>" class="btn btn-primary">Go to Profile</a>                            
               </div>
               <div class="col-lg-6 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in">Submit Detail</button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
$(function() {  
  $("#addDoc").validate({
    ignore: [],
    rules: {
      title: { required: true, },      
      document: { required: true, accept:"jpg,png,jpeg,gif" },
    }, 
    messages: {
      title: { required: "Enter document title.",   },
      document: { required: "Select document image.", accept: "Only image allowed."   },
    }
  });

  $("#document_image").on('click', function(e) { e.preventDefault(); $("#document").trigger('click'); });

});
  function document_name(e){ if(e.target.files[0].name !="") { $("#document_name").removeClass('hidden'); }}
</script>