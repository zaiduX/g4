<!-- Seller -->
<div class="row">
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel">
          <div class="panel-body text-center h-200">
              <?php for($i=0; $i<(int)$cust['ratings']; $i++){ echo ' <i class="fa fa-star fa-2x"></i> '; } ?>
              <?php for($i=0; $i<(5-$cust['ratings']); $i++){ echo ' <i class="fa fa-star-o fa-2x"></i> '; } ?>

              <h1 class="m-xs"><small> ( <?= $cust['ratings'] ?> / 5 )</small></h1>

              <h4 class="font-extra-bold no-margins text-success">
                  <?= $this->lang->line('deliverer_rating'); ?>
              </h4>
              <small><?= $this->lang->line('deliverer_rating_txt'); ?> <strong>Gonagoo</strong></small>
          </div>
      </div>
  </div>

  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                 <h4> <?= $this->lang->line('total_orders'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-portfolio fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= $total_awarded_jobs; ?></h3>
              <span class="font-bold no-margins">
                  <?= $this->lang->line('completed_orders'); ?>
              </span>
      
                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?= $posted_progress; ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $posted_progress; ?>" role="progressbar" class=" progress-bar progress-bar-success" title="<?= $posted_progress; ?>%">                              
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('awarded'); ?></small>
                          <h4><?= $total_awareded; ?> %</h4>
                      </div>

                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('delivered'); ?></small>
                          <h4><?= $total_delivered; ?> %</h4>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                  <h4><?= $this->lang->line('working_orders'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-note fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= $total_working_jobs; ?></h3>
              <span class="font-bold no-margins">
                 <?= $this->lang->line('in_progress_orders'); ?>
              </span>
      
                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?= $working_progress; ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $working_progress; ?>" role="progressbar" class=" progress-bar progress-bar-success" title="<?= $working_progress; ?>%">                              
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('accepted'); ?></small>
                          <h4><?= $total_accepted; ?> %</h4>
                      </div>

                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('in_progress'); ?></small>
                          <h4><?= $working_progress; ?> %</h4>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
          <h4><?= $this->lang->line('balance_summary'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
          <i class="pe-7s-cash fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h4 class="m-xs" style="margin-top: -15px;">XAF <?= $current_balance; ?></h4>
          <span class="font-bold no-margins">
            <?= $this->lang->line('current_balance'); ?>
          </span>
        </div>
        <div class="m-t-xs">
          <div class="row" style="margin-top: 0px;">
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('earned'); ?></small>
              <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_earned);?> </h4>
            </div>
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('spent'); ?></small>
              <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($current_balance - $total_spend);?></h4>
            </div>
          </div>
        </div>
        <div class="m-t-xs" style="margin-top: 0px;">
          <div class="row">
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('Service Earning'); ?></small>
              <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_earned_service);?> </h4>
            </div>
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('Service Spend'); ?></small>
              <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_spend_service);?></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 

</div>

<!-- buyer -->
<div class="row">
    
  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                   <h4><?= $this->lang->line('posted_jobs'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-portfolio fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= $total_jobs2; ?></h3>
              <span class="font-bold no-margins">
                  <?= $this->lang->line('awarded_jobs'); ?>
              </span>
      
                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?= $posted_progress2; ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $posted_progress2; ?>" role="progressbar" class=" progress-bar progress-bar-success" title="<?= $posted_progress2; ?>%">                              
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('awarded_jobs'); ?></small>
                          <h4><?= $posted_progress2; ?> %</h4>
                      </div>

                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('in_market'); ?></small>
                          <h4><?= $total_inmarket_jobs2; ?> %</h4>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                  <h4><?= $this->lang->line('working_jobs'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-note fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= $total_working_jobs2; ?></h3>
              <span class="font-bold no-margins">
                  <?= $this->lang->line('in_progress_jobs'); ?>
              </span>
      
                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?= $working_progress2; ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $working_progress2; ?>" role="progressbar" class=" progress-bar progress-bar-success" title="<?= $working_progress2; ?>%">                              
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('jobs_accepted'); ?></small>
                          <h4><?= $total_accepted_jobs2; ?> %</h4>
                      </div>

                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('in_progress'); ?></small>
                          <h4><?= $working_progress2; ?> %</h4>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                  <h4> <?= $this->lang->line('completed_jobs'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-like2 fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= $total_delivered_jobs2; ?></h3>
              <span class="font-bold no-margins">
                  <?= $this->lang->line('delivered_jobs'); ?>
              </span>

                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?= $completed_progress2; ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $completed_progress2; ?>" role="progressbar" class=" progress-bar progress-bar-success" title="<?= $completed_progress2; ?>%">                              
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('delivered_jobs'); ?></small>
                          <h4><?= $total_delivered2; ?> %</h4>
                      </div>

                      <div class="col-xs-6 hidden">
                          <small class="stats-label"><?= $this->lang->line('total_jobs'); ?></small>
                          <h4><?= $total_jobs2; ?></h4>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>      

</div>

<div class="row">
  
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading">          
         <?= $this->lang->line('latest_orders'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <tbody>
                    <?php if(!empty($latest_jobs2)):?>
                      <?php foreach ($latest_jobs2 as $v): ?>
                        <?php 
                          if($v['order_status'] == 'open') { $status2 = $this->lang->line('in_market');  } 
                          else if($v['order_status'] == 'accept') { $status2 = $this->lang->line('accepted');  } 
                          else if($v['order_status'] == 'in_progress') { $status2 = $this->lang->line('in_progress');  } 
                          else if($v['order_status'] == 'delivered') { $status2 = $this->lang->line('delivered');  } 
                        ?>
                        <tr>
                          <td><strong><?= $this->lang->line('order'); ?></strong><br />G-<?= $v['order_id'];?><small></small></td>
                          <td><strong><?= $this->lang->line('from_address'); ?></strong><br /><?= $v['from_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('to_address'); ?></strong><br /><?= $v['to_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('price'); ?></strong><br /><?= $v['currency_sign']. ' ' .$v['order_price']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('status'); ?></strong><br /><?= $status2; ?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading">          
         <?= $this->lang->line('latest_inprogress_orders'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <tbody>
                    <?php if(!empty($latest_inprogress_jobs2)):?>                      
                      <?php foreach ($latest_inprogress_jobs2 as $v): ?>                        
                        <tr>
                          <td><strong><?= $this->lang->line('order'); ?></strong><br />G-<?= $v['order_id'];?><small></small></td>
                          <td><strong><?= $this->lang->line('from_address'); ?></strong><br /><?= $v['from_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('to_address'); ?></strong><br /><?= $v['to_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('price'); ?></strong><br /><?= $v['currency_sign']. ' ' .$v['order_price']; ?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

</div>

<div class="row">
  
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading">          
          <?= $this->lang->line('latest_posted_jobs'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <tbody>
                    <?php if(!empty($latest_jobs)):?>
                      <?php foreach ($latest_jobs as $v): ?>
                        <?php 
                          if($v['order_status'] == 'open') { $status = $this->lang->line('in_market');  } 
                          else if($v['order_status'] == 'accept') { $status = $this->lang->line('accepted');  } 
                          else if($v['order_status'] == 'in_progress') { $status = $this->lang->line('in_progress');  } 
                          else if($v['order_status'] == 'delivered') { $status = $this->lang->line('delivered');  } 
                        ?>
                        <tr>
                          <td><strong><?= $this->lang->line('order'); ?></strong><br />G-<?= $v['order_id'];?><small></small></td>
                          <td><strong><?= $this->lang->line('from_address'); ?></strong><br /><?= $v['from_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('to_address'); ?></strong><br /><?= $v['to_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('price'); ?></strong><br /><?= $v['currency_sign']. ' ' .$v['order_price']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('status'); ?></strong><br /><?= $status; ?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading">          
         <?= $this->lang->line('latest_in_progress_jobs'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <tbody>
                    <?php if(!empty($latest_inprogress_jobs)):?>                      
                      <?php foreach ($latest_inprogress_jobs as $v): ?>                        
                        <tr>
                          <td><strong><?= $this->lang->line('order'); ?></strong><br />G-<?= $v['order_id'];?><small></small></td>
                          <td><strong><?= $this->lang->line('from_address'); ?></strong><br /><?= $v['from_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('to_address'); ?></strong><br /><?= $v['to_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('price'); ?></strong><br /><?= $v['currency_sign']. ' ' .$v['order_price']; ?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading"> 
         <?= $this->lang->line('recent_order_reviews'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <tbody>
                    <?php if(!empty($recent_order_reviews)):?> 
                      <?php foreach ($recent_order_reviews as $ror): ?> 
                        <tr>
                          <td><strong><?= $this->lang->line('order'); ?></strong><br />G-<?= $ror['order_id'];?><small></small></td>
                          <td><strong><?= $this->lang->line('from_address'); ?></strong><br /><?= $ror['from_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('to_address'); ?></strong><br /><?= $ror['to_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('price'); ?></strong><br /><?= $ror['currency_sign']. ' ' .$v['order_price']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('review'); ?></strong><br /><?= $ror['review_comment']=='NULL'? 'Not Provided': $ror['review_comment']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('rating'); ?></strong><br /><?php if($ror['rating']=='NULL') { echo 'Not Provided'; } else { for($i=0; $i<(int)$ror['rating']; $i++) { echo ' <i class="fa fa-star"></i> '; }  for($i=0; $i<(5-$ror['rating']); $i++) { echo ' <i class="fa fa-star-o"></i> '; } }  ?><small></small></td>
                        </tr>
                      <?php endforeach; ?> 
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

</div>

</div>