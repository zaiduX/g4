    <style type="text/css">
        #available[]-error {
            margin-top: 20px;
        }
    </style>
    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-master-list'; ?>"><span><?= $this->lang->line('Trip Master'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Add Trip'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Add Trip'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Add trip and availability details'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <!-- Add New Points -->
        <div class="row">
            <div class="col-lg-12">
              <div class="hpanel hblue">
                <form action="<?= base_url('user-panel-bus/bus-trip-master-add-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="tripAdd">
                    <div class="panel-body">
                        <?php if($this->session->flashdata('error')):  ?>
                            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <h3><?= $this->lang->line('Trip Details'); ?></h3>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label class="text-left"><?= $this->lang->line('Select Vehicle Type'); ?></label>
                                    <select class="form-control js-source-states" name="vehical_type_id" id="vehical_type_id">
                                        <option value=""><?= $this->lang->line('Select Vehicle Type'); ?></option>
                                        <?php foreach ($vehicle_types as $type) { ?>
                                            <option value="<?= $type['vehical_type_id'] ?>"><?= $type['vehicle_type'] ?></option>
                                        <?php } ?>
                                    </select><br />
                                    
                                    <label class=""><?= $this->lang->line('Select Vehicle'); ?></label>
                                    <select class="form-control select2" name="bus_id" id="bus_id">
                                        <option value=""><?= $this->lang->line('Select vehicle type first'); ?></option>
                                    </select><br />

                                    <label class=""><?= $this->lang->line('Booking Type'); ?></label>
                                    <select class="form-control select2" name="booking_type" id="booking_type">
                                        <option value=""><?= $this->lang->line('Select booking type'); ?></option>
                                        <option value="online"><?= $this->lang->line('Online + Internal'); ?></option>
                                        <option value="internal"><?= $this->lang->line('Internal'); ?></option>
                                    </select>
                                </div>
                                <div class="col-md-9" style="padding-left: 0px;">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <label class=""><?= $this->lang->line('Trip Availability'); ?></label> &nbsp;<small style="color: red;">*(<?= $this->lang->line('Select atleast 1 day!'); ?>)</small><br />
                                            <label> <input type="checkbox" class="i-checks" name="available[]" value="sun"> <?= $this->lang->line('sunday'); ?> </label>&nbsp;&nbsp;
                                            <label> <input type="checkbox" class="i-checks" name="available[]" value="mon"> <?= $this->lang->line('monday'); ?> </label>&nbsp;&nbsp;
                                            <label> <input type="checkbox" class="i-checks" name="available[]" value="tue"> <?= $this->lang->line('tuesday'); ?> </label>&nbsp;&nbsp;
                                            <label> <input type="checkbox" class="i-checks" name="available[]" value="wed"> <?= $this->lang->line('wednesday'); ?> </label>&nbsp;&nbsp;
                                            <label> <input type="checkbox" class="i-checks" name="available[]" value="thu"> <?= $this->lang->line('thursday'); ?> </label>&nbsp;&nbsp;
                                            <label> <input type="checkbox" class="i-checks" name="available[]" value="fri"> <?= $this->lang->line('friday'); ?> </label>&nbsp;&nbsp;
                                            <label> <input type="checkbox" class="i-checks" name="available[]" value="sat"> <?= $this->lang->line('saturday'); ?> </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3"><br />
                                            <label class=""><?= $this->lang->line('Departure Time'); ?></label>
                                            <div class="input-group clockpicker" data-autoclose="true">
                                                <input type="text" class="form-control" id="trip_depart_time" name="trip_depart_time" value="00:00" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3"><br />
                                            <label class=""><?= $this->lang->line('Start Date'); ?></label>
                                            <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                                                <input type="text" class="form-control" id="trip_start_date" name="trip_start_date" value="<?=date('m/d/Y')?>" />
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-md-3"><br />
                                            <label class=""><?= $this->lang->line('End Date'); ?></label>
                                            <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                                                <input type="text" class="form-control" id="trip_end_date" name="trip_end_date" value="<?=date('m/d/Y', strtotime('+3 month'));?>" />
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-md-3"><br />
                                            <label class=""><?= $this->lang->line('Trip Duration (Hrs.)'); ?></label>
                                            <input type="text" class="form-control" id="trip_duration" name="trip_duration" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="seatTypes">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row form-group">
                            <div class="col-md-12" style="padding-top: 0px;">
                                <h3><?= $this->lang->line('Location Details'); ?></h3>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <label class=""><?= $this->lang->line('Select Source'); ?></label>
                                    <select class="form-control select2" name="src_loc_id_1" id="src_loc_id" required="required">
                                        <option value=""><?= $this->lang->line('Select vehicle type first'); ?></option>
                                    </select>
                                    <div id="src_loc_id_points" style="max-height: 300px; scroll-behavior: auto; padding-right: 0px; padding-left: 0px">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <label class=""><?= $this->lang->line('Select Destination'); ?></label>
                                    <select class="form-control select2" name="dest_loc_id_1" id="dest_loc_id" required="required">
                                        <option value=""><?= $this->lang->line('Select vehicle type first'); ?></option>
                                    </select>
                                    <div id="dest_loc_id_points" style="max-height: 300px; scroll-behavior: auto; padding-right: 0px; padding-left: 0px">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    &nbsp;
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr />
                        </div>
                        <div class="wrapperField row form-group">
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <a class="btn btn-warning" data-style="zoom-in" id="btn_add_point"><?= $this->lang->line('Add More Location'); ?></a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('Add Trip'); ?></button>
                                <a href="<?= base_url('user-panel-bus/bus-trip-master-list'); ?>" class="btn btn-primary"><?= $this->lang->line('back'); ?></a>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>

    <script>
        var btn_counter = 2; 
        $(function(){
            var wrapper = $(".wrapperField");
            var add_button = $("#btn_add_point");

            $(add_button).click(function(e) { 
                e.preventDefault();
                $("#no_of_points").val(btn_counter);
                var vehical_type_id = $("#vehical_type_id").val();
            
                $(wrapper).append(
                    '<div class="col-md-12">'+
                        '<div class="col-md-5">'+
                            '<label class=""><?= $this->lang->line("Select Source"); ?></label>'+
                            '<select class="form-control select2" name="src_loc_id_'+btn_counter+'" id="src_loc_id_'+btn_counter+'">'+
                                '<option value=""><?= $this->lang->line("Select Source"); ?></option>'+
                            '</select>'+
                            '<div class="col-md-12"><h5><?= $this->lang->line("Pickup points and timings"); ?></h5></div>'+
                            '<div id="src_loc_id_points_'+btn_counter+'" style="max-height: 300px; scroll-behavior: auto; padding-right: 0px; padding-left: 0px"></div>'+
                        '</div>'+
                        '<div class="col-md-5">'+
                            '<label class=""><?= $this->lang->line("Select Destination"); ?></label>'+
                            '<select class="form-control select2" name="dest_loc_id_'+btn_counter+'" id="dest_loc_id_'+btn_counter+'">'+
                                '<option value=""><?= $this->lang->line("Select Destination"); ?></option>'+
                            '</select>'+
                            '<div class="col-md-12"><h5><?= $this->lang->line("Drop points and timings"); ?></h5></div>'+
                            '<div id="dest_loc_id_points_'+btn_counter+'" style="max-height: 300px; scroll-behavior: auto; padding-right: 0px; padding-left: 0px"></div>'+
                        '</div>'+
                        '<div class="col-md-2"><br />'+
                            '<a class="remove_field pull-right btn btn-danger" data-toggle="tooltip" data-placement="top" title="<?=$this->lang->line("Remove Point")?>" data-original-title="<?=$this->lang->line("Remove Point")?>"><i class="fa fa-trash"></i></a>'+
                        '</div>'+
                        '<div class="col-md-12">'+
                            '<hr />'+
                        '</div>'+
                    '</div>'
                );

                src_select_id = '#src_loc_id_'+btn_counter;
                $(src_select_id).select2();
                $(src_select_id).trigger('change');

                dest_select_id = '#dest_loc_id_'+btn_counter;
                $(dest_select_id).select2();
                $(dest_select_id).trigger('change');

                $.ajax({
                    type: "POST", 
                    url: "<?=base_url('user-panel-bus/get-locations-by-vehicle-type-id')?>", 
                    data: { id: vehical_type_id },
                    dataType: "json",
                    success: function(res){
                        //console.log(res);
                        $(src_select_id).empty(); 
                        $(src_select_id).trigger('change');
                        $(src_select_id).append("<option value=''><?=$this->lang->line('Select Location');?></option>");
                        $(src_select_id).trigger('change');
                        $.each( res, function() { $(src_select_id).append('<option value="'+$(this).attr('loc_id')+'">'+$(this).attr('loc_city_name')+'</option>'); }); 

                        
                        $(dest_select_id).empty(); 
                        $(dest_select_id).trigger('change');
                        $(dest_select_id).append("<option value=''><?=$this->lang->line('Select Location');?></option>");
                        $(dest_select_id).trigger('change');
                        $.each( res, function() { $(dest_select_id).append('<option value="'+$(this).attr('loc_id')+'">'+$(this).attr('loc_city_name')+'</option>'); }); 
                    },
                    beforeSend: function(){
                    },
                    error: function(){
                        console.log('Error');
                    }
                });

                //Source Points Times
                var src_points = '#src_loc_id_points_'+btn_counter;
                $(src_select_id).on('change', function() {
                    var src_id = this.value;
                    var vehical_type_id = $("#vehical_type_id").val();
                    $.ajax({
                        url: "<?=base_url('user-panel-bus/get-locations-pickup-points')?>",
                        type: "POST",
                        data: { id: src_id, type_id: vehical_type_id },
                        dataType: "json",
                        success: function(res){
                            //console.log(res);
                            $.each( res, function() { 
                                $(src_points).append('<div class="col-md-12">'+
                                                    '<p>'+$(this).attr('point_address')+'</p>'+
                                                    '<input type="hidden" value="'+$(this).attr('point_id')+'" name="src_point_id_'+btn_counter+'[]">'+
                                                    '<div class="input-group clockpicker" data-autoclose="true">'+
                                                    '<input type="text" class="form-control" id="pickuptime" name="src_point_time_'+btn_counter+'[]" value="00:00" />'+
                                                    '<span class="input-group-addon">'+
                                                        '<span class="fa fa-clock-o"></span>'+
                                                    '</span>'+
                                                    '</div></div>');
                                $('.clockpicker').clockpicker();
                            });
                        },
                        beforeSend: function(){
                            $(src_points).empty();
                        },
                        error: function(){
                            $(src_points).empty();
                        }
                    });
                });

                //Destination Points Times
                var dest_points = '#dest_loc_id_points_'+btn_counter;
                $(dest_select_id).on('change', function() {
                    var dest_id = this.value;
                    var vehical_type_id = $("#vehical_type_id").val();
                    $.ajax({
                        url: "<?=base_url('user-panel-bus/get-locations-drop-points')?>",
                        type: "POST",
                        data: { id: dest_id, type_id: vehical_type_id },
                        dataType: "json",
                        success: function(res){
                            //console.log(res);
                            $.each( res, function() { 
                                $(dest_points).append('<div class="col-md-12">'+
                                                    '<p>'+$(this).attr('point_address')+'</p>'+
                                                    '<input type="hidden" value="'+$(this).attr('point_id')+'" name="dest_point_id_'+btn_counter+'[]">'+
                                                    '<div class="input-group clockpicker" data-autoclose="true">'+
                                                    '<input type="text" class="form-control" id="pickuptime" name="dest_point_time_'+btn_counter+'[]" value="00:00" />'+
                                                    '<span class="input-group-addon">'+
                                                        '<span class="fa fa-clock-o"></span>'+
                                                    '</span>'+
                                                    '</div></div>');
                                $('.clockpicker').clockpicker();
                            });
                        },
                        beforeSend: function(){
                            $(dest_points).empty();
                        },
                        error: function(){
                            $(dest_points).empty();
                        }
                    });
                });

                btn_counter++;
            });

            //Remove Location
            $(wrapper).on("click",".remove_field", function(e) { e.preventDefault(); 
              $(this).parent().parent().remove(); 
              var pointCounter = $("#no_of_points").val();
              pointCounter -= 1;
              $("#no_of_points").val(pointCounter);
              if(btn_counter > 1 ) { } else { btn_counter = 0; } 
            });
        }); 

    </script>

    <script>
      $("#tripAdd")
        .validate({
          ignore: [],
          rules: {
            vehical_type_id: { required : true },
            bus_id: { required : true },
            trip_depart_time: { required : true },
            trip_start_date: { required : true },
            trip_end_date: { required : true },
            src_loc_id: { required : true },
            dest_loc_id: { required : true },
            booking_type: { required : true },
            trip_duration: { required : true, number: true, },
          },
          messages: {
              vehical_type_id: { required : "<?= $this->lang->line('Select vehicle type!'); ?>", },
              bus_id: { required : "<?= $this->lang->line('Select vehicle!'); ?>", },
              trip_depart_time: { required : "<?= $this->lang->line('Enter departure time!'); ?>", },
              trip_start_date: { required : "<?= $this->lang->line('Select start date!'); ?>", },
              trip_end_date: { required : "<?= $this->lang->line('Select end date!'); ?>" },
              src_loc_id: { required : "<?= $this->lang->line('Select source location!'); ?>", },
              dest_loc_id: { required : "<?= $this->lang->line('Select destination location!'); ?>", },
              booking_type: { required : "<?= $this->lang->line('Select booking type!'); ?>", },
              trip_duration: { required : "<?= $this->lang->line('Enter trip durationin hours!'); ?>", number : "<?= $this->lang->line('numbers_only'); ?>", },
          },
        });
    </script>

    <script>
        // Source Points
        $('#src_loc_id').on('change', function() {
            var src_id = this.value;
            var vehical_type_id = $("#vehical_type_id").val();

            $.ajax({
                url: "<?=base_url('user-panel-bus/get-locations-pickup-points')?>",
                type: "POST",
                data: { id: src_id, type_id: vehical_type_id },
                dataType: "json",
                success: function(res){
                    //console.log(res);
                    $.each( res, function() { 
                        $('#src_loc_id_points').append('<br /><div class="col-md-12">'+
                                                        '<p>'+$(this).attr('point_address')+'</p>'+
                                                        '<input type="hidden" value="'+$(this).attr('point_id')+'" name="src_point_id_1[]">'+
                                                        '<div class="input-group clockpicker" data-autoclose="true">'+
                                                        '<input type="text" class="form-control" id="pickuptime" name="src_point_time_1[]" value="00:00" />'+
                                                        '<span class="input-group-addon">'+
                                                            '<span class="fa fa-clock-o"></span>'+
                                                        '</span>'+
                                                        '</div></div>');
                        $('.clockpicker').clockpicker();
                    });
                },
                beforeSend: function(){
                    $('#src_loc_id_points').empty();
                    $('#src_loc_id_points').append('<div class="col-md-12">'+
                                                    '<h5><?= $this->lang->line("Pickup points and timings"); ?></h5>'+
                                                    '</div>'
                                                );
                },
                error: function(){
                    $('#src_loc_id_points').empty();
                }
            });
        });

        // destination points
        $('#dest_loc_id').on('change', function() {
            var src_id = this.value;
            var vehical_type_id = $("#vehical_type_id").val();
            $.ajax({
                url: "<?=base_url('user-panel-bus/get-locations-drop-points')?>",
                type: "POST",
                data: { id: src_id, type_id: vehical_type_id },
                dataType: "json",
                success: function(res){
                    //console.log(res);
                    $.each( res, function() { 
                        $('#dest_loc_id_points').append('<br /><div class="col-md-12">'+
                                                        '<p>'+$(this).attr('point_address')+'</p>'+
                                                        '<input type="hidden" value="'+$(this).attr('point_id')+'" name="dest_point_id_1[]">'+
                                                        '<div class="input-group clockpicker" data-autoclose="true">'+
                                                        '<input type="text" class="form-control" id="pickuptime" name="dest_point_id_time_1[]" value="00:00" />'+
                                                        '<span class="input-group-addon">'+
                                                            '<span class="fa fa-clock-o"></span>'+
                                                        '</span>'+
                                                        '</div></div>');
                        $('.clockpicker').clockpicker();
                    });
                },
                beforeSend: function(){
                    $('#dest_loc_id_points').empty();
                    $('#dest_loc_id_points').append('<div class="col-md-12">'+
                                                    '<h5><?= $this->lang->line("Drop points and timings"); ?></h5>'+
                                                    '</div>'
                                                );
                },
                error: function(){
                    $('#dest_loc_id_points').empty();
                }
            });
        })
    </script>

    <script>
        $('#vehical_type_id').on('change', function() {
            var vehical_type_id = this.value;
            //Remove additional Locations
            $(".remove_field").click();

            $.ajax({
                url: "<?=base_url('user-panel-bus/get-vehicle-seat-type')?>",
                type: "POST",
                data: { id: vehical_type_id },
                dataType: "json",
                success: function(res) {
                    //console.log(res);
                    $.each( res, function() { 
                        $('#seatTypes').append('<div class="col-md-4"><br /><label class="text-left">'+$(this).attr('st_name')+' <?= $this->lang->line('Seats'); ?><?= $this->lang->line('price'); ?></label>'+
                                            '<input type="hidden" name="seat_type_id[]" value="'+$(this).attr('st_id')+'">'+
                                            '<input type="hidden" name="seat_type[]" value="'+$(this).attr('st_name')+'">'+
                                            '<input type="number" placeholder="<?= $this->lang->line('Seat Price'); ?>" class="form-control" name="seat_type_price[]" id="seat_type_price" value="0" required><br /></div>');
                    });
                },
                beforeSend: function(){
                    $('#seatTypes').empty();
                },
                error: function(){
                    $('#seatTypes').empty();
                }
            });

            //Vehicles on change of vehicle type
            if(vehical_type_id != "" ) { 
              $.ajax({
                type: "POST", 
                url: "<?=base_url('user-panel-bus/get-vehicle-by-type-id')?>", 
                data: { id: vehical_type_id },
                dataType: "json",
                success: function(res){
                  //console.log(res); 
                  $('#bus_id').empty(); 
                  $('#bus_id').trigger('change');
                  $('#bus_id').append("<option value=''><?=$this->lang->line('select_vehicle');?></option>");
                  $('#bus_id').trigger('change');
                  $.each( res, function(){$('#bus_id').append('<option value="'+$(this).attr('bus_id')+'">'+$(this).attr('bus_no')+'</option>');});
                  $('#bus_id').focus();
                },
                beforeSend: function(){
                  $('#bus_id').empty(); 
                  $('#bus_id').trigger('change');
                },
                error: function(){
                  $('#bus_id').empty(); 
                  $('#bus_id').trigger('change');
                }
              });
            } else { 
              $('#bus_id').empty(); 
              $('#bus_id').append("<option value=''><?=$this->lang->line('select_vehicle');?></option>");
              $('#bus_id').trigger('change');
            }

            //Fill Source Destination Locations on vehicle Type Change
            if(vehical_type_id != "" ) { 
                $.ajax({
                    type: "POST", 
                    url: "<?=base_url('user-panel-bus/get-locations-by-vehicle-type-id')?>", 
                    data: { id: vehical_type_id },
                    dataType: "json",
                    success: function(res){
                        //console.log(res); 
                        //Source
                        $('#src_loc_id').empty(); 
                        $('#src_loc_id').trigger('change');
                        $('#src_loc_id').append("<option value=''><?=$this->lang->line('Select Location');?></option>");
                        $('#src_loc_id').trigger('change');
                        $.each( res, function(){$('#src_loc_id').append('<option value="'+$(this).attr('loc_id')+'">'+$(this).attr('loc_city_name')+'</option>');});
                        //destination
                        $('#dest_loc_id').empty(); 
                        $('#dest_loc_id').trigger('change');
                        $('#dest_loc_id').append("<option value=''><?=$this->lang->line('Select Location');?></option>");
                        $('#dest_loc_id').trigger('change');
                        $.each( res, function(){$('#dest_loc_id').append('<option value="'+$(this).attr('loc_id')+'">'+$(this).attr('loc_city_name')+'</option>');});
                    },
                    beforeSend: function(){
                        //Source
                        $('#src_loc_id').empty();
                        $('#src_loc_id').trigger('change');
                        $('#src_loc_id').append("<option value=''><?=$this->lang->line('loading');?></option>");
                        $('#src_loc_id').trigger('change');
                        //Destination
                        $('#dest_loc_id').empty();
                        $('#dest_loc_id').trigger('change');
                        $('#dest_loc_id').append("<option value=''><?=$this->lang->line('loading');?></option>");
                        $('#dest_loc_id').trigger('change');
                    },
                    error: function(){
                        //Source
                        $('#src_loc_id').empty();
                        $('#src_loc_id').trigger('change');
                        $('#src_loc_id').append("<option value=''><?=$this->lang->line('Select vehicle type first');?></option>");
                        $('#src_loc_id').trigger('change');
                        //Destnation
                        $('#dest_loc_id').empty();
                        $('#dest_loc_id').trigger('change');
                        $('#dest_loc_id').append("<option value=''><?=$this->lang->line('Select vehicle type first');?></option>");
                        $('#dest_loc_id').trigger('change');
                    }
                });
            } else { 
                //Source
                $('#src_loc_id').empty();
                $('#src_loc_id').trigger('change');
                $('#src_loc_id').append("<option value=''><?=$this->lang->line('Select vehicle type first');?></option>");
                $('#src_loc_id').trigger('change');
                //Destnation
                $('#dest_loc_id').empty();
                $('#dest_loc_id').trigger('change');
                $('#dest_loc_id').append("<option value=''><?=$this->lang->line('Select vehicle type first');?></option>");
                $('#dest_loc_id').trigger('change');
            }
        })
    </script>