<style>.dataTables_filter { display: none; } </style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('dash'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('workroom'); ?>
          </span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-briefcase fa-2x text-muted"></i> <?= $this->lang->line('workroom'); ?>
       </h2>
      <small class="m-t-md"><?= $this->lang->line('All service workroom listing'); ?></small>
    </div>
  </div>
</div>

<div class="content">
  <!-- <div class="row">
    <div class="col-lg-12">
      <div class="hpanel">
        <div class="panel-body">
          <div class="input-group">
            <input class="form-control" id="searchbox" type="text" placeholder="Search order ...">
            <div class="input-group-btn">
              <button class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->

  <!-- <?=json_encode($orders);?> -->
  <table id="addressTableData" class="table">
    <thead><tr><th class="hidden"></th></tr></thead>
    <tbody>
      <?php 
        foreach ($orders as $order): //echo json_encode($order); die(); 
          if($order['cat_id'] == 6 || $order['cat_id'] == 7) {
      ?>
            <tr>
              <td style="line-height: 15px;">
                <div class="hpanel" style="line-height: 15px; border: 1px solid #3498db; padding: 0px; margin-bottom:0px;">
                  <div class="panel-body" style="line-height: 15px; padding-bottom: 5px;">
                    <div class="row" style="line-height: 15px; margin-top: -15px; margin-bottom: -5px;">
                      <div class="col-lg-5"> 
                        <h5 class="text-left text-light" style="color:#3498db;"><?= $this->lang->line('ref_G'); ?><?= $order['service_details']['order_id'] ?></h5>
                        <h5 class="text-left text-light" style="color:#3498db;"><?= $this->lang->line('order_date'); ?> <?= date('l, d M Y',strtotime($order['service_details']['cre_datetime'])); ?></h5>
                        <!-- <h6 class="text-left text-light" style="color:#3498db;"><?= $this->lang->line('delivery_code'); ?>: <?= $order['service_details']['delivery_code'] ?></h6> -->
                      </div>
                      <div class="col-lg-3"> 
                        <h5>
                          <?php 
                          $category_details = $this->api->get_category_details_by_id($order['service_details']['category_id']); 
                          $icon_url = $category_details['icon_url'];
                          if($order['service_details']['category_id'] == 6) : ?>
                            <img src='<?= base_url($icon_url) ?>' /><?= $this->lang->line('transportation'); ?>
                          <?php else : ?>
                            <img src='<?= base_url($icon_url) ?>' /><?= $this->lang->line('lbl_courier'); ?>
                          <?php endif; ?>
                        </h5>
                        <!-- <h6 class=""><?= ($order['service_details']['order_description'] !="NULL")?$order['service_details']['order_description']:""; ?></h6> -->
                      </div>
                      <div class="col-lg-4 text-right" style="padding-right: 0px;"> 
                        <form action="<?= base_url('user-panel/courier-workroom'); ?>" method="post" class=" text-right col-lg-12">
                          <input type="hidden" name="order_id" value="<?= $order['service_details']['order_id'] ?>" />
                          <input type="hidden" name="type" value="workroom" />
                          <button type="submit" class="btn btn-outline btn-success" title="<?= $this->lang->line('go_to_workroom'); ?>">
                            <i class="fa fa-briefcase"></i> <?= $this->lang->line('go_to_workroom'); ?>
                          </button>
                        </form>
                      </div>
                    </div>
                    <h5 class="m-b-xs"><?= $this->lang->line('cust_name'); ?><font style="color:#3498db;">
                      <?php 
                      if($order['service_details']['cust_name'] == 'NULL' || $order['service_details']['cust_name'] == 'NULL NULL') { 
                      $user_details = $this->api->get_user_details($order['service_details']['cust_id']);
                      if($user_details['company_name'] != 'NULL') { echo $user_details['company_name']; } 
                      else { $email_cut = explode('@', $user_details['email1']); echo $email_cut[0]; }
                      } else { echo $order['service_details']['cust_name']; } ?>
                    </font></h5>

                    <h5 class="m-b-xs"><?= $this->lang->line('from'); ?>: <font style="color:#3498db;"><?= ($order['service_details']['from_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['service_details']['from_address']; ?></font></h5>
                    <h5 class="m-b-xs"><?= $this->lang->line('to'); ?>: <font style="color:#3498db;"><?= ($order['service_details']['to_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['service_details']['to_address']; ?></font></h5>
                    <p class="small">
                      <div class="col-md-6"> <?= $this->lang->line('weight'); ?> <?= $order['service_details']['total_weight'] ?> <?= strtoupper($this->user->get_unit_name($order['service_details']['unit_id'])); ?></div>
                      <div class="col-md-6"> <?= $this->lang->line('price'); ?>: <font style="color:#3498db;"><?= $order['service_details']['currency_sign'] ?> <?= $order['service_details']['order_price'] ?></font></div>
                    </p>
                    <p class="small">
                      <div class="col-md-6"> <?= $this->lang->line('advance'); ?>: <?= $order['service_details']['currency_sign'] ?> <?= $order['service_details']['advance_payment'] ?></div>
                      <div class="col-md-6"> <?= $this->lang->line('balance'); ?>: <font style="color:#3498db;"><?= $order['service_details']['currency_sign'] . ' '. ((+$order['service_details']['advance_payment']+$order['service_details']['pickup_payment']+$order['service_details']['deliver_payment'])-$order['service_details']['paid_amount']); ?></font></div>
                    </p>
                    <p class="small">
                      <div class="col-md-6"> <?= $this->lang->line('by'); ?>: <?= $this->user->get_vehicle_type_name($order['service_details']['vehical_type_id']); ?></div>
                      <div class="col-md-6"> <?= $this->lang->line('no_of_packages'); ?>: <?= $order['service_details']['package_count']; ?></div>
                    </p>
                    <p class="small">
                      <div class="col-md-6"> <?= $this->lang->line('pickup_date_time'); ?>: <?= $order['service_details']['pickup_datetime'] ?></div>
                      <div class="col-md-6"> <?= $this->lang->line('deliver_date_time'); ?>: <?= $order['service_details']['delivery_datetime'] ?></div>
                    </p>
                    <p class="small">
                      <div class="col-md-6"> <?= $this->lang->line('sender_phone'); ?>: <?= $order['service_details']['from_address_contact'] == 'NULL' ? $this->lang->line('not_provided') : $order['service_details']['from_address_contact'] ?></div>
                      <div class="col-md-6"> <?= $this->lang->line('reciever_phone'); ?>: <?= $order['service_details']['to_address_contact'] == 'NULL' ? $this->lang->line('not_provided') : $order['service_details']['to_address_contact'] ?></div>
                    </p>
                  </div>
                </div>
              </td>
            </tr>
      <?php 
          } else if($order['cat_id'] == 9) {
      ?>
            <tr>
              <td style="line-height: 15px;">
                <div class="hpanel" style="line-height: 15px; border: 1px solid #3498db; padding: 0px; margin-bottom:0px;">
                  <div class="panel-body" style="line-height: 15px; padding-bottom: 5px;">
                    <div class="row" style="line-height: 15px; margin-top: -15px; margin-bottom: -5px;">
                      <div class="col-lg-5"> 
                        <h5 class="text-left text-light" style="color:#3498db;"><?= $this->lang->line('ref_G'); ?><?= $order['service_details']['booking_id'] ?></h5>
                        <h5 class="text-left text-light" style="color:#3498db;"><?= $this->lang->line('order_date'); ?> <?= date('l, d M Y',strtotime($order['service_details']['cre_datetime'])); ?></h5>
                      </div>
                      <div class="col-lg-3"> 
                        <h5>
                          <?php $icon_url = $this->api->get_category_details_by_id($order['service_details']['cat_id'])['icon_url']; ?>
                          <img src='<?= base_url($icon_url) ?>' /> <?= $this->lang->line('Laundry Service'); ?>
                        </h5>
                      </div>
                      <div class="col-lg-4 text-right" style="padding-right: 0px;"> 
                        <form action="<?= base_url('user-panel-laundry/laundry-workroom'); ?>" method="post" class=" text-right col-lg-12">
                          <input type="hidden" name="order_id" value="<?= $order['service_details']['booking_id'] ?>" />
                          <input type="hidden" name="type" value="workroom" />
                          <button type="submit" class="btn btn-outline btn-success" title="<?= $this->lang->line('go_to_workroom'); ?>">
                            <i class="fa fa-briefcase"></i> <?= $this->lang->line('go_to_workroom'); ?>
                          </button>
                        </form>
                      </div>
                    </div>

                    <h5 class="m-b-xs"><?= $this->lang->line('cust_name'); ?><font style="color:#3498db;">
                      <?php 
                      if($order['service_details']['cust_name'] == 'NULL' || $order['service_details']['cust_name'] == 'NULL NULL') { 
                      $user_details = $this->api->get_user_details($order['service_details']['cust_id']);
                      if($user_details['company_name'] != 'NULL') { echo $user_details['company_name']; } 
                      else { $email_cut = explode('@', $user_details['email1']); echo $email_cut[0]; }
                      } else { echo $order['service_details']['cust_name']; } ?>
                    </font></h5>

                    <p class="small">
                      <div class="col-md-3"> <?= $this->lang->line('price'); ?>: <font style="color:#3498db;"><?= $order['service_details']['currency_sign'] ?> <?= $order['service_details']['booking_price'] ?></font></div>
                      <div class="col-md-3"> <?= $this->lang->line('balance'); ?>: <font style="color:#3498db;"><?= $order['service_details']['currency_sign'] . ' '. ($order['service_details']['booking_price'] - $order['service_details']['paid_amount']); ?></font></div>
                      <div class="col-md-3"> <?= $this->lang->line('advance'); ?>: <?= $order['service_details']['currency_sign'] ?> <?= $order['service_details']['advance_payment'] ?></div>
                      <div class="col-md-3">
                        <div style="float: right;">
                          <button class="btn btn-outline btn-sm btn-success btn_view_<?=$order['service_details']['booking_id']?>"><i class="fa fa-eye"></i> <?= $this->lang->line('Show Items'); ?></button>
                          <button class="btn btn-outline btn-sm btn-success btn_hide_<?=$order['service_details']['booking_id']?> hidden"><i class="fa fa-eye"></i> <?= $this->lang->line('Hide Items'); ?></button>
                        </div>
                      </div>
                    </p>
                    <p class="small">
                      <div class="col-md-3"> <?= $this->lang->line('Is Pickup'); ?>: <?=($order['service_details']['is_pickup']==1)?$this->lang->line('yes'):$this->lang->line('no')?></div>
                      <div class="col-md-3"> <?= $this->lang->line('Is Drop'); ?>: <?=($order['service_details']['is_drop']==1)?$this->lang->line('yes'):$this->lang->line('no')?></div>
                      <div class="col-md-3"> <?= $this->lang->line('No of items'); ?>: <?= $order['service_details']['item_count']; ?></div>
                    </p>
                  </div>
                  <div class="panel-body" id="details_<?=$order['gw_id']?>" style="display:none; padding-top: 0px; padding-bottom: 0px;">
                    <table style="width:100%" class="table display compact" id="tbl_<?=$order['gw_id']?>">
                      <thead>
                        <tr>
                          <th><?= $this->lang->line('Sr. No'); ?></th>
                          <th><?= $this->lang->line('Item Category'); ?></th>
                          <th><?= $this->lang->line('Item Name'); ?></th>
                          <th><?= $this->lang->line('Single Unit Price'); ?></th>
                          <th><?= $this->lang->line('quantity'); ?></th>
                          <th><?= $this->lang->line('total'); ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($order['service_details']['bookings_details'] as $dtls) { static $i=0; $i++; ?>
                          <tr>
                            <td><?=$i?></td>
                            <td><?=$dtls['cat_name']?></td>
                            <td><?=$dtls['sub_cat_name']?></td>
                            <td><?=$dtls['single_unit_price']?></td>
                            <td><?=$dtls['quantity']?></td>
                            <td><?=$dtls['total_price']?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <script>
                    $(".btn_view_<?=$order['service_details']['booking_id']?>").click(function(){
                      $("#details_<?=$order['gw_id']?>").slideToggle(500);
                      $(".btn_view_<?=$order['service_details']['booking_id']?>").addClass("hidden");
                      $(".btn_hide_<?=$order['service_details']['booking_id']?>").removeClass("hidden");
                    });
                    $(".btn_hide_<?=$order['service_details']['booking_id']?>").click(function(){
                      $("#details_<?=$order['gw_id']?>").slideToggle(500);
                      $(".btn_view_<?=$order['service_details']['booking_id']?>").removeClass("hidden");
                      $(".btn_hide_<?=$order['service_details']['booking_id']?>").addClass("hidden");
                    });
                    $(document).ready(function() {
                      $('#tbl_<?=$order['service_details']['booking_id']?>').DataTable({
                        "paging":   false,
                        "info":     false
                      });
                    });
                  </script>
                </div>
              </td>
            </tr>
      <?php 
          } else { 
            $customer_details = $this->api->get_user_details($order['service_details']['cust_id']);
            //echo json_encode($customer_details);
            $operator_details = $this->api->get_bus_operator_profile($order['service_details']['operator_id']);
            //echo json_encode($operator_details);
            $bus_details = $this->api->get_operator_bus_details($order['service_details']['bus_id']);
            //echo json_encode($bus_details);
            $trip_master_details = $this->api->get_trip_master_details($order['service_details']['trip_id']);
          ?>
            <tr>
              <td class="hidden" style="line-height: 15px;"><?=$order['service_details']['trip_id'];?></td>
              <td style="line-height: 15px;">
                <div class="hpanel" style="line-height: 15px; border: 1px solid #3498db; padding: 0px; margin-bottom:0px;">
                  <div class="panel-body" style="line-height: 15px; padding-bottom: 5px;">
                    <div class="row" style="line-height: 15px; margin-top: -15px">
                      <div class="col-lg-5"> 
                        <h5 style="color:#3498db;">
                        <?php 
                          if($order['service_details']['return_trip_id']>0) { 
                             $r_tList = $this->api->get_trip_booking_details($order['service_details']['return_trip_id']);
                            echo "<i class='fa fa-arrow-up'></i><i class='fa fa-arrow-down'></i> ".$this->lang->line('Return_Trip').$this->lang->line('Trip ID: ').$order['service_details']['trip_id'];  } 
                          else { echo "<i class='fa fa-arrow-up'></i> ".$this->lang->line('Ownward_Trip').$this->lang->line('Trip ID: ').$order['service_details']['trip_id']; } 
                          ?> <br />
                          <i class='fa fa-ticket'></i><i class='fa fa-ticket'></i> <?= $this->lang->line('Ticket No: '); ?><font color="#3498db"><?= $order['service_details']['ticket_id'] ?></font>
                        </h5>
                      </div>
                      <div class="col-lg-3">
                        <h5>
                          <?php 
                          $category_details = $this->api->get_category_details_by_id($order['service_details']['cat_id']); 
                          $icon_url = $category_details['icon_url'];
                          echo "<img src='".base_url($icon_url)."'> " . $this->lang->line('Ticket Booking');
                          /*if($bus_details['vehical_type_id']==53) { echo '<i class="fa fa-train"></i>'; } 
                          else if ($bus_details['vehical_type_id']==52) { echo '<i class="fa fa-plane"></i>'; } 
                          else if ($bus_details['vehical_type_id']==51) { echo '<i class="fa fa-ship"></i>'; } 
                          else { echo '<i class="fa fa-bus"></i>'; } */
                          ?></h5>
                      </div>
                      <div class="col-lg-4 text-right">
                        <?php if($order['service_details']['cust_id'] != $order['service_details']['operator_id']) { ?>
                          <form action="<?=base_url('user-panel-bus/bus-work-room')?>" method="post">
                            <input type="hidden" name="ticket_id" value="<?= $order['service_details']['ticket_id'] ?>">
                            <button type="submit" class="btn btn-outline btn-success" ><i class="fa fa-briefcase"></i> <?= $this->lang->line('go_to_workroom'); ?> </button>
                          </form>
                        <?php } ?>
                      </div>
                      <div class="col-lg-12" style="margin-bottom: -5px; margin-top: -5px;"> 
                        <!-- <div class="col-lg-2" style="padding-left: 0px; padding-right: 0px;">
                          <img src="<?=($operator_details['avatar_url']=='' || $operator_details['avatar_url'] == 'NULL')?base_url('resources/no-image.jpg'):base_url($operator_details['avatar_url'])?>" class="img-responsive thumbnail" style="border: 1px solid #3498db;">
                        </div> -->
                          
                        <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px;">
                          <div class="col-lg-8" style="padding-left: 0px; padding-right: 0px; line-height: 12px;">
                            <div class="col-lg-12"> 
                              <h5><?= $this->lang->line('Operator Company'); ?>: <font color="#3498db"><?= $operator_details['company_name'] ?></font></h5>
                            </div>
                            <div class="col-lg-12"> 
                              <h5><?= $this->lang->line('Booking Date'); ?>: <font color="#3498db"><?= $order['service_details']['bookig_date'] ?></font></h5>
                            </div>
                            <div class="col-lg-12"> 
                              <h5><?= $this->lang->line('Journey Date'); ?>: <font color="#3498db"><?= $order['service_details']['journey_date'] ?></font></h5>
                            </div>
                            <div class="col-lg-12"> 
                              <h5 class="m-b-xs"><?= $this->lang->line('from'); ?>: <font color="#3498db"><?=$order['service_details']['source_point']?></font></h5>
                              <!-- <h6><?= $this->lang->line('Pickup Points'); ?>: <?=$order['service_details']['pickup_point']?></h6> -->
                              <h5 class="m-b-xs"><?= $this->lang->line('to'); ?>: <font color="#3498db"><?=$order['service_details']['destination_point']?></font></h5>
                              <!-- <h6><?= $this->lang->line('Drop Points'); ?>: <?=$order['service_details']['drop_point']?></h6> -->
                            </div>
                          </div>

                          <div class="col-lg-4" style="padding-right: 0px;">
                            <div class="col-md-12" style="margin-bottom: 5px;">
                              <label><?= $this->lang->line('Seat Types and Pricing'); ?></label>
                            </div>
                            <!--<div class="col-md-6" style="margin-bottom: 5px;">
                              <?=$order['service_details']['seat_type'];?>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <strong><font color="#3498db"><?=$order['service_details']['seat_price'];?> <?=$order['service_details']['currency_sign'];?></font></strong>
                            </div>-->
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <?= $this->lang->line('no_of_seats'); ?>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <strong><font color="#3498db"><?=$order['service_details']['no_of_seats'];?></font></strong>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <?= $this->lang->line('Total_Price'); ?>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <strong><font color="#3498db"><?=$order['service_details']['ticket_price'].' '.$order['service_details']['currency_sign'];?></font></strong>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <?= $this->lang->line('Paid Amount'); ?>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <strong><font color="#3498db"><?=$order['service_details']['paid_amount'].' '.$order['service_details']['currency_sign'];?></font></strong>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <?= $this->lang->line('Balance Amount'); ?>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 5px;">
                              <strong><font color="#3498db"><?=$order['service_details']['balance_amount'].' '.$order['service_details']['currency_sign'];?></font></strong>
                            </div>
                          </div>
                          
                          <div class="col-lg-12" style=" padding-right: 0px;">
                            <div class="col-lg-4" style="padding-left: 0px;">
                              <h5 class="m-b-xs">
                                <?= $this->lang->line('Departure Time'). ': <font color="#3498db">'.$trip_master_details[0]['trip_depart_time'].'</font>'?>
                              </h5>
                            </div>
                            <div class="col-lg-4" style="padding-right: 0px; padding-left: 0px;">
                              <h5 class="m-b-xs">
                                <?= $this->lang->line('Trip Duration (Hrs.)'); ?>: <font color="#3498db"><?=$trip_master_details[0]['trip_duration']?></font>
                              </h5>
                            </div>
                            <div class="col-lg-4" style="padding-left: 0px; padding-right: 0px;">
                              <h5 class="m-b-xs">
                                <?= $this->lang->line('Trip_Distance'); ?>: 
                                <?php 
                                  $pickup_lat_long_string = $this->api->get_loc_lat_long_by_id($order['service_details']['source_point_id']);
                                  $pickup_lat_long_array = explode(',',$pickup_lat_long_string);
                                  //echo json_encode($pickup_lat_long_array);
                                  $drop_lat_long_string = $this->api->get_loc_lat_long_by_id($order['service_details']['destination_point_id']);
                                  $drop_lat_long_array = explode(',',$drop_lat_long_string);
                                  //echo json_encode($drop_lat_long_array);
                                ?>
                                <font color="#3498db">
                                  <?php if(sizeof($pickup_lat_long_array) == 2 && sizeof($drop_lat_long_array) == 2) { ?>
                                    <?php echo round($this->api->GetDrivingDistance($pickup_lat_long_array[0],$pickup_lat_long_array[1],$drop_lat_long_array[0],$drop_lat_long_array[1]),2) . 'KM'; ?>
                                  <?php } else { echo 'N/A'; } ?>
                                </font>
                              </h5>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
      <?php  }
        endforeach; ?>
    </tbody>
  </table>
</div>
