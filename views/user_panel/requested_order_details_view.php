<style>
    #map { height: 100%; }
</style>
<style>
    #map { height: 100%; }
    .numbers{ background-color: #3498db;
    color: #fff;
    padding: 1px 6px 1px 6px;
</style>

    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/order-requests'; ?>"><?= $this->lang->line('requested_orders'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('order_det'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    <?= $this->lang->line('order_det'); ?> &nbsp;&nbsp;&nbsp;
                </h2>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="hpanel filter-item">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 form-group">
                            <div>
                                <h3 class="stat-label"><?= $this->lang->line('order_timeline'); ?></h3>
                            </div>
                            <div class="hpanel" style="border: 1px solid #3498db">
                                <?php $status_array = array_reverse($order_status, true); ?>
                                <?php foreach ($status_array as $status) { static $i=1;  ?>
                                    <div class="v-timeline vertical-container animate-panel" data-child="vertical-timeline-block" data-delay="1">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon navy-bg" style="background-color: <?= $i==1 ? '#00B900' : 'white'; ?>">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <div class="p-sm">
                                                    <h5>
                                                        <?php 
                                                        if($status['status'] == 'open') { echo $this->lang->line('ordered'); } 
                                                        else if($status['status'] == 'accept' && $status['user_type'] == 'deliverer') { echo $this->lang->line('deliverer_accepted'); }
                                                        else if($status['status'] == 'assign') { echo $this->lang->line('driver_assigned'); }
                                                        else if($status['status'] == 'accept' && $status['user_type'] == 'driver') { echo $this->lang->line('driver_accepted'); }
                                                        else if($status['status'] == 'in_progress') { echo $this->lang->line('order_is_in_progress'); }
                                                        else if($status['status'] == 'src_relay_in') { echo $this->lang->line('recieved_at_source_relay_point'); }
                                                        else if($status['status'] == 'src_relay_out') { echo $this->lang->line('out_from_source_relay_point'); }
                                                        else if($status['status'] == 'dest_relay_in') { echo $this->lang->line('recieved_at_destination_relay_point'); }
                                                        else if($status['status'] == 'dest_relay_out') { echo $this->lang->line('out_from_destination_relay_point'); }
                                                        else if($status['status'] == 'delivered') { echo $this->lang->line('delivered_successfully'); }
                                                        ?>
                                                    </h5>
                                                </div>
                                                <div class="panel-footer">
                                                    <?= strtoupper(date('l',strtotime($status['mod_datetime']))) ?> <small><?= strtoupper(date('d-M-Y h:i:s A',strtotime($status['mod_datetime']))) ?></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $i++; } ?>
                              </div>
                              <div class="form-group text-center" style="border: 1px solid #3498db">
                                  <img class="img-responsive" src="<?php if($order['order_picture_url'] == "NULL") { echo base_url('resources/no-image.jpg'); } else { echo base_url($order['order_picture_url']); } ?>" />
                              </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 form-group">
                            <div class="row" style="">
                                <div class="col-md-12">
                                    <h3 class="stat-label" style="display: inline-block;"><?= $this->lang->line('order_details'); ?></h3>
                                    <div class="pull-right text-right" style="display: inline-block;">
                                        <h5 class="stat-label"><?= $this->lang->line('ref#'); ?><?= $order['order_id'] ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">1</span> <?= $this->lang->line('transport_type'); ?> &amp; <?= $this->lang->line('addresses'); ?></h5>
                                    <h4 class="m-b-xs hidden"><?= $this->lang->line('cust_name'); ?> 
                                      <?php 
                                      if($order['cust_name'] == 'NULL' || $order['cust_name'] == 'NULL NULL') { 
                                      $user_details = $this->api->get_user_details($order['cust_id']);
                                      if($user_details['company_name'] != 'NULL') { echo $user_details['company_name']; } 
                                      else { $email_cut = explode('@', $user_details['email1']); echo $email_cut[0]; }
                                      } else { echo $order['cust_name']; } ?>
                                    </h4>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="m-b-xs"><?= $this->lang->line('from'); ?>: <?= ($order['from_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['from_address']; ?></h4>
                                    <?php if($order['from_address_info'] != 'NULL' && $order['from_address_info'] != '') { ?><small>[<?php echo $order['from_address_info']; ?>]</small> <?php } ?>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 8px;">
                                    <h4 class="m-b-xs"><?= $this->lang->line('to'); ?>: <?= ($order['to_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['to_address']; ?></h4>
                                    <?php if($order['to_address_info'] != 'NULL' && $order['to_address_info'] != '') { ?><small>[<?php echo $order['to_address_info']; ?>]</small> <?php } ?>
                                </div>
                                <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('sender_name'); ?></strong> <?= $order['from_address_name'] == 'NULL' ? $this->lang->line('not_provided') : $order['from_address_name'] ?></div>
                                <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('reciever_name'); ?></strong> <?= $order['to_address_name'] == 'NULL' ? $this->lang->line('not_provided') : $order['to_address_name'] ?></div>
    
                                <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('sender_phone'); ?></strong> <?= $order['from_address_contact'] == 'NULL' ? $this->lang->line('not_provided') : $order['from_address_contact'] ?></div>
                                <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('reciever_phone'); ?></strong> <?= $order['to_address_contact'] == 'NULL' ? $this->lang->line('not_provided') : $order['to_address_contact'] ?></div>
                                <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('distance_in_km'); ?>:</strong> <?= $order['distance_in_km'] ?></div>
                                <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('by'); ?>:</strong> <?= $this->user->get_vehicle_type_name($order['vehical_type_id']); ?></div>
                                <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('shipping_type'); ?>:</strong> <?= $order['order_type'] ?></div>
                            </div>
                            <br />
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">2</span> <?= $this->lang->line('date_time');?></h5>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Order_Date'); ?>:</strong> <?= date('d-M-Y',strtotime($order['cre_datetime'])) ?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('expiry_date'); ?>:</strong> <?= date('d-M-Y',strtotime($order['expiry_date'])) ?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong style="color: #59bdd7"><?= $this->lang->line('pickup_date_time'); ?> <?= date('d-M-Y h:i:s A',strtotime($order['pickup_datetime'])) ?></strong></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong style="color: #59bdd7"><?= $this->lang->line('deliver_date_time'); ?> <?= date('d-M-Y h:i:s A',strtotime($order['delivery_datetime'])) ?></strong></div>
                                </div>
                            </div>
                            <br />
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">3</span> <?= $this->lang->line('price_activities_driver_status');?></h5>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><i class="fa fa-user"></i> <?= $this->lang->line('driver_name'); ?></strong> <?php if($order['cd_name']!="NULL") { echo $order['cd_name'].' [GD-'.$order['driver_id'].']'; } else { echo 'Not assigned...!'; } ?> </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('driver_contact'); ?></strong> <?php if($order['cd_name']!="NULL") { echo strtoupper($this->user->get_driver_contact_by_id($order['driver_id'])); } else { echo 'No contact...!'; } ?> </div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong style="color: #59bdd7"><?= $this->lang->line('advance'); ?> <?= $order['advance_payment'] ?> <?= $order['currency_sign'] ?> </strong></div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('pick_up'); ?></strong> <?= $order['pickup_payment'] ?> <?= $order['currency_sign'] ?> </div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('delivered'); ?></strong></strong> <?= $order['deliver_payment'] ?> <?= $order['currency_sign'] ?></div>
        
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('handling_fee'); ?></strong> <?= $order['handling_fee'] ?> <?= $order['currency_sign'] ?> </div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('urgent_fee'); ?></strong> <?= $order['urgent_fee'] ?> <?= $order['currency_sign'] ?> </div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('vehicle_fee'); ?></strong> <?= $order['vehicle_fee'] ?> <?= $order['currency_sign'] ?></div>
        
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('insurance_fee'); ?></strong> <?= $order['insurance'] == 1 ? $order['insurance_fee'] : 0; ?> <?= $order['currency_sign'] ?> </div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('custom_fee'); ?>:</strong> <?= $order['custom_clearance_fee']; ?> <?= $order['currency_sign'] ?> </div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong style="color: #59bdd7"><?= $this->lang->line('order_price'); ?> : <?= $order['order_price'] ?> <?= $order['currency_sign'] ?> </strong></div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Payment Mode'); ?> :</strong> <?=($order['payment_mode']!='NULL')?($order['payment_mode']=='payment')?$this->lang->line('Online'):$order['payment_mode']:$this->lang->line('Not paid') ?></div>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('order_type'); ?> :</strong>
                                    <?= ucfirst($order['service_area_type']) ?></div>
                                </div>
                            </div>
                            <br />
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">4</span> <?= $this->lang->line('options_special_instructions'); ?></h5>
                                    <?php if($order['category_id'] != 7) { ?>
                                    <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('need_tailgate'); ?>:</strong> <?= $order['need_tailgate'] == 1 ? $this->lang->line('yes') : $this->lang->line('no') ; ?> </div>
        
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('loading_unloading_charges'); ?>:</strong> <?= $order['loading_unloading_charges']; ?> <?= $order['currency_sign'] ?> </div>
        
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('loading_unloading_time'); ?>:</strong> <?= $order['loading_unloading_hours'] != '' ? $order['loading_unloading_hours'] . $this->lang->line('hours')  : $this->lang->line('na'); ?> </div>
                                    <?php }  ?>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('contents'); ?>: </strong><?= $order['order_contents'] == 'NULL' ? $this->lang->line('not_provided') : $order['order_contents'] ?> </div>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('description'); ?>: </strong> <?= $order['order_description'] == 'NULL' ? $this->lang->line('not_provided') : $order['order_description'] ?> </div>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('pickup_instructions'); ?>: </strong> <?= $order['pickup_instructions'] == 'NULL' ? $this->lang->line('not_provided') : $order['pickup_instructions'] ?> </div>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('deliver'); ?>: </strong> <?= $order['deliver_instructions'] == 'NULL' ? $this->lang->line('not_provided') : $order['deliver_instructions'] ?> </div>
                                </div>
                            </div>
                            <br />
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">5</span> <?= $this->lang->line('package_details'); ?></h5>
                                    <div class="col-md-12 table-responsive" style="margin-bottom: 8px;">
                                        <table id="tableData" class="table table-striped table-bordered table-hover">
                                            <tr>
                                              <th><?= $this->lang->line('dimension'); ?></th>
                                              <th><?= $this->lang->line('quantity'); ?></th>
                                              <th><?= $this->lang->line('Width'); ?></th>
                                              <th><?= $this->lang->line('height'); ?></th>
                                              <th><?= $this->lang->line('length'); ?></th>
                                              <th><?= $this->lang->line('total_weight'); ?></th>
                                              <th><?= $this->lang->line('contents'); ?></th>
                                              <th><?= $this->lang->line('dangerous_goods'); ?></th>
                                            </tr>
                                            <?php foreach ($packages as $package) { ?>
                                              <tr>
                                                <td><?=$this->user->get_dimension_type_name($package['dimension_id'])?></td>
                                                <td><?=$package['quantity']?></td>
                                                <td><?=$package['width']?></td>
                                                <td><?=$package['height']?></td>
                                                <td><?=$package['length']?></td>
                                                <td><?php $units = $this->transport->get_unit($package['unit_id']); echo $package['total_weight'].' '.$units['shortname']?></td>
                                                <td><?=$package['contents']?></td>
                                                <td>
                                                  <?php 
                                                    if($package['dangerous_goods_id'] > 0) {
                                                      $goods = $this->transport->get_dangerous_goods($package['dangerous_goods_id']); 
                                                      echo $goods['name']; } else { echo $this->lang->line('not_provided'); }
                                                  ?>
                                                </td>
                                              </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                            <br />
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">6</span> <?= $this->lang->line('direction_on_map'); ?></h5>
                                    <div class="col-md-12" style="margin-bottom: 8px;">
                                        <iframe
                                            width="100%"
                                            height="450"
                                            frameborder="0" style="border:0"
                                            src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin=<?= $order['from_latitude'] ?>,<?= $order['from_longitude'] ?>&destination=<?= $order['to_latitude'] ?>,<?= $order['to_longitude'] ?>" >
                                        </iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 form-group hidden">
                            <div class="col-md-12">
                                <h3 class="stat-label" style="display: inline-block;"><?= $this->lang->line('order_details'); ?></h3>
                                <div class="pull-right text-right" style="display: inline-block;">
                                    <h5 class="stat-label"><?= $this->lang->line('ref#'); ?><?= $order['order_id'] ?></h5>
                                </div>
                            </div>
                            <div class="col-md-12 hidden">
                                <h4 class="m-b-xs"><?= $this->lang->line('cust_name'); ?> 
                                  <?php 
                                  if($order['cust_name'] == 'NULL' || $order['cust_name'] == 'NULL NULL') { 
                                  $user_details = $this->api->get_user_details($order['cust_id']);
                                  if($user_details['company_name'] != 'NULL') { echo $user_details['company_name']; } 
                                  else { $email_cut = explode('@', $user_details['email1']); echo $email_cut[0]; }
                                  } else { echo $order['cust_name']; } ?>
                                </h4>
                            </div>
                            <div class="col-md-12">
                                <h4 class="m-b-xs"><?= $this->lang->line('from'); ?>: <?= ($order['from_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['from_address']; ?></h4>
                                <?php if($order['from_address_info'] != 'NULL' && $order['from_address_info'] != '') { ?><small>[<?php echo $order['from_address_info']; ?>]</small> <?php } ?>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 8px;">
                                <h4 class="m-b-xs"><?= $this->lang->line('to'); ?>: <?= ($order['to_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['to_address']; ?></h4>
                                <?php if($order['to_address_info'] != 'NULL' && $order['to_address_info'] != '') { ?><small>[<?php echo $order['to_address_info']; ?>]</small> <?php } ?>
                            </div>
                            
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><i class="fa fa-user"></i> <?= $this->lang->line('driver_name'); ?>:</strong> <?php if($order['cd_name']!="NULL") { echo $order['cd_name']; } else { echo $this->lang->line('not_assigned'); } ?> </div>
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('driver_contact'); ?>:</strong> <?php if($order['cd_name']!="NULL") { echo strtoupper($this->user->get_driver_contact_by_id($order['driver_id'])); } else { echo $this->lang->line('no_contact'); } ?> </div>

                            <!--<div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('dimension'); ?>:</strong> <?= strtoupper($this->user->get_dimension_type_name($order['dimension_id'])); ?></div>-->
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Order_Date'); ?>:</strong> <?= date('d-M-Y',strtotime($order['cre_datetime'])) ?></div>
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('expiry_date'); ?>:</strong> <?= date('d-M-Y',strtotime($order['expiry_date'])) ?></div>

                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('distance_in_km'); ?>:</strong> <?= $order['distance_in_km'] ?></div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('by'); ?>:</strong> <?= $this->user->get_vehicle_type_name($order['vehical_type_id']); ?></div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('shipping_type'); ?>:</strong> <?= $order['order_type'] ?></div>
                            <!--
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Width'); ?>:</strong> <?= $order['width'] ?> CM</div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('height'); ?>:</strong> <?= $order['height'] ?> CM</div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('length'); ?>:</strong> <?= $order['length'] ?> CM</div>

                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('weight'); ?>:</strong> <?= $order['total_weight'] ?> <?= strtoupper($this->user->get_unit_name($order['unit_id'])); ?></div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('quantity'); ?>:</strong> <?= $order['total_quantity'] ?></div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('volume'); ?>:</strong> <?= $order['total_volume'] ?></div>
                            -->
                        
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong style="color: #59bdd7"><?= $this->lang->line('advance'); ?> <?= $order['advance_payment'] ?> <?= $order['currency_sign'] ?> </strong></div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('pick_up'); ?>:</strong> <?= $order['pickup_payment'] ?> <?= $order['currency_sign'] ?> </div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('delivered'); ?>:</strong> <?= $order['deliver_payment'] ?> <?= $order['currency_sign'] ?></div>

                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('handling_fee'); ?>:</strong> <?= $order['handling_fee'] ?> <?= $order['currency_sign'] ?> </div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('urgent_fee'); ?>:</strong> <?= $order['urgent_fee'] ?> <?= $order['currency_sign'] ?> </div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('vehicle_fee'); ?>:</strong> <?= $order['vehicle_fee'] ?> <?= $order['currency_sign'] ?></div>

                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('insurance_fee'); ?>:</strong> <?= $order['insurance'] == 1 ? $order['insurance_fee'] : 0; ?> <?= $order['currency_sign'] ?> </div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('custom_fee'); ?>:</strong> <?= $order['custom_clearance_fee']; ?> <?= $order['currency_sign'] ?> </div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong style="color: #59bdd7"><?= $this->lang->line('order_price'); ?> : <?= $order['order_price'] ?> <?= $order['currency_sign'] ?> </strong></div>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('order_type'); ?> :</strong>
                                <?= ucfirst($order['service_area_type']) ?>
                            </div>
                            <?php if($order['category_id'] != 7) { ?>
                            <div class="col-md-4" style="margin-bottom: 8px;"><strong><?= $this->lang->line('need_tailgate'); ?>:</strong> <?= $order['need_tailgate'] == 1 ? $this->lang->line('yes') : $this->lang->line('no') ; ?> </div>

                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('loading_unloading_charges'); ?>:</strong> <?= $order['loading_unloading_charges']; ?> <?= $order['currency_sign'] ?> </div>

                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('loading_unloading_time'); ?>:</strong> <?= $order['loading_unloading_hours'] != '' ? $order['loading_unloading_hours'] . $this->lang->line('hours')  : $this->lang->line('na'); ?> </div>
                            <?php } else { ?>
                            <div class="col-md-8" style="margin-bottom: 8px;">&nbsp;</div>
                            <?php } ?>
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong style="color: #59bdd7"><?= $this->lang->line('pickup_date_time'); ?> <?= date('d-M-Y h:i:s A',strtotime($order['pickup_datetime'])) ?></strong></div>
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong style="color: #59bdd7"><?= $this->lang->line('deliver_date_time'); ?><?= date('d-M-Y h:i:s A',strtotime($order['delivery_datetime'])) ?></strong></div>
                        
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('sender_name'); ?>:</strong> <?= $order['from_address_name'] == 'NULL' ? $this->lang->line('not_provided') : $order['from_address_name'] ?></div>
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('reciever_name'); ?>:</strong> <?= $order['to_address_name'] == 'NULL' ? $this->lang->line('not_provided') : $order['to_address_name'] ?></div>

                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('sender_phone'); ?>:</strong> <?= $order['from_address_contact'] == 'NULL' ? $this->lang->line('not_provided') : $order['from_address_contact'] ?></div>
                            <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('reciever_phone'); ?>:</strong> <?= $order['to_address_contact'] == 'NULL' ? $this->lang->line('not_provided') : $order['to_address_contact'] ?></div>
    
                            <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('expiratio_date'); ?>:</strong> <?= $order['expiry_date'] ?></div>
                                        
                            <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('contents'); ?>: </strong><?= $order['order_contents'] == 'NULL' ? $this->lang->line('not_provided') : $order['order_contents'] ?> </div>

                            <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('description'); ?>: </strong> <?= $order['order_description'] == 'NULL' ? $this->lang->line('not_provided') : $order['order_description'] ?> </div>

                            <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('pickup_instructions'); ?>: </strong> <?= $order['pickup_instructions'] == 'NULL' ? $this->lang->line('not_provided') : $order['pickup_instructions'] ?> </div>

                            <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('deliver'); ?>: </strong> <?= $order['deliver_instructions'] == 'NULL' ? $this->lang->line('not_provided') : $order['deliver_instructions'] ?> </div>

                            <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('package_details'); ?>: </div>

                            <div class="col-md-12" style="margin-bottom: 8px;">
                              <table id="tableData" class="table table-striped table-bordered table-hover">
                                <tr>
                                  <th><?= $this->lang->line('dimension'); ?></th>
                                  <th><?= $this->lang->line('quantity'); ?></th>
                                  <th><?= $this->lang->line('Width'); ?></th>
                                  <th><?= $this->lang->line('height'); ?></th>
                                  <th><?= $this->lang->line('length'); ?></th>
                                  <th><?= $this->lang->line('total_weight'); ?></th>
                                  <th><?= $this->lang->line('contents'); ?></th>
                                  <th><?= $this->lang->line('dangerous_goods'); ?></th>
                                </tr>
                                <?php foreach ($packages as $package) { ?>
                                  <tr>
                                    <td><?=$this->user->get_dimension_type_name($package['dimension_id'])?></td>
                                    <td><?=$package['quantity']?></td>
                                    <td><?=$package['width']?></td>
                                    <td><?=$package['height']?></td>
                                    <td><?=$package['length']?></td>
                                    <td><?php $units = $this->transport->get_unit($package['unit_id']); echo $package['total_weight'].' '.$units['shortname']?></td>
                                    <td><?=$package['contents']?></td>
                                    <td>
                                      <?php 
                                        if($package['dangerous_goods_id'] > 0) {
                                          $goods = $this->transport->get_dangerous_goods($package['dangerous_goods_id']); 
                                          echo $goods['name']; } else { echo $this->lang->line('not_provided'); }
                                      ?>
                                    </td>
                                  </tr>
                                <?php } ?>
                              </table>
                            </div>


                            <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('direction_on_map'); ?></strong></div>
                            <div class="col-md-12" style="margin-bottom: 8px;">
                                <iframe
                                    width="100%"
                                    height="450"
                                    frameborder="0" style="border:0"
                                    src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin=<?= $order['from_latitude'] ?>,<?= $order['from_longitude'] ?>&destination=<?= $order['to_latitude'] ?>,<?= $order['to_longitude'] ?>" >
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <a href="<?= $this->config->item('base_url') . 'user-panel/order-requests'; ?>" class="btn btn-info btn-outline " ><?= $this->lang->line('back'); ?></a>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                        <button id="<?= $order['order_id'] ?>" class="btn btn-outline btn-info acceptrequestalert" ><i class="fa fa-check"></i> <?= $this->lang->line('accept'); ?> </button>
                        <button id="<?= $order['order_id'] ?>" class="btn btn-outline btn-danger rejectrequestalert" ><i class="fa fa-times"></i> <?= $this->lang->line('reject'); ?> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $('.acceptrequestalert').click(function () {
        var id = this.id;
        swal({
            title: <?= json_encode($this->lang->line('are_you_sure'))?>,
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,
            cancelButtonText: <?= json_encode($this->lang->line('no'))?>,
            closeOnConfirm: false,
            closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    $.post( '<?php echo base_url('user-panel/order-accepted-by-deliverer'); ?>' , {id: id, status: 'accept'}, function(res){
                        console.log(res);
                        if($.trim(res) == 'order_accepted_workroom') {
                            swal("<?= $this->lang->line('accepted'); ?>", "<?= $this->lang->line('Order has been accepted!'); ?>", "success");
                            setTimeout(function() { window.location.href = '<?php echo base_url('user-panel/order-requests'); ?>'; }, 2000);
                        } else if ($.trim(res) == 'order_accept_failed') {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While accepting order!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While updating status!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else {
                    swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('No change'); ?>", "error");
                }
            });
        });

    $('.rejectrequestalert').click(function () {
        var id = this.id;
        swal({
            title: <?= json_encode($this->lang->line('are_you_sure'))?>,
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText:  <?= json_encode($this->lang->line('yes'))?>,
            cancelButtonText:   <?= json_encode($this->lang->line('no'))?>,
            closeOnConfirm: false,
            closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    $.post( '<?php echo base_url('user-panel/order-accepted-by-deliverer'); ?>' , {id: id, status: 'reject'}, function(res){
                        if ($.trim(res) == 'order_rejected_deliverer') {
                            swal("Rejected!", "Order has been rejected!", "success");
                            setTimeout(function() { window.location.href = '<?php echo base_url('user-panel/order-requests'); ?>'; }, 2000);
                        } else if ($.trim(res) == 'order_reject_failed') {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While rejecting order!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While updating status!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else {
                    swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('No change'); ?>", "error");
                }
            });
        });
</script>