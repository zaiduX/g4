    <style type="text/css">
        #available[]-error {
            margin-top: 20px;
        }
    </style>
    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-special-rate-list'; ?>"><span><?= $this->lang->line('Trip Special Rate'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Setup Trip Special Rate'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Setup Trip Special Rate'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Enter special rates for special dates'); ?></small>    
        </div>
      </div>
    </div>
    <?php $avail = explode(',', $trip_details[0]['trip_availability'] );  ?>
    <div class="content">
        <!-- Add New Points -->
        <div class="row">
            <div class="col-lg-12">
              <div class="hpanel hblue">
                <form action="<?= base_url('user-panel-bus/bus-trip-special-rate-edit-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="tripAdd">
                    <input type="hidden" name="trip_id" value="<?=$trip_details[0]['trip_id']?>">
                    <div class="panel-body">
                        <?php if($this->session->flashdata('error')):  ?>
                            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <h3><?= $this->lang->line('Trip Details'); ?></h3>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label class=""><?= $this->lang->line('Source'); ?></label>
                                    <h5><?=$trip_details[0]['trip_source']?></h5>
                                </div>
                                <div class="col-md-6">
                                    <label class=""><?= $this->lang->line('Destination'); ?></label>
                                    <h5><?=$trip_details[0]['trip_destination']?></h5>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label class=""><?= $this->lang->line('Bus Details'); ?></label>
                                    <h5><?php foreach ($buses as $bus) { if($trip_details[0]['bus_id']==$bus['bus_id']) { echo $bus['bus_modal'].' ['.$bus['bus_make'].'] ['.$bus['bus_seat_type'].', '.$bus['bus_ac'].']'; } } ?></h5>
                                </div>
                                <div class="col-md-3">
                                    <label class=""><?= $this->lang->line('Start Date'); ?></label>
                                    <h5><?=$trip_details[0]['trip_start_date']?></h5>
                                </div>
                                <div class="col-md-3">
                                    <label class=""><?= $this->lang->line('End Date'); ?></label><br />
                                    <h5 id="trip_end_date"><?=$trip_details[0]['trip_end_date']?></h5>
                                </div>
                            </div>
                        </div>
                        <hr />

                        <div class="row form-group">
                            <div class="col-md-4">
                                <h3><?= $this->lang->line('Define Special Rates'); ?></h3>
                            </div>
                            <div class="col-md-4">
                                <label class=""><?= $this->lang->line('Special Rate Start Date'); ?></label><br />
                                <div class="input-group date" data-provide="datepicker" data-date-start-date="<?=$trip_details[0]['trip_start_date']?>" data-date-end-date="<?=$trip_details[0]['trip_end_date']?>">
                                    <input type="text" class="form-control" id="special_rate_start_date" name="special_rate_start_date" value="<?=($trip_details[0]['special_rate_start_date']!='NULL')?$trip_details[0]['special_rate_start_date']:''?>" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-4">
                                <label class=""><?= $this->lang->line('Special Rate End Date'); ?></label><br />
                                <div class="input-group date" data-provide="datepicker" data-date-start-date="<?=$trip_details[0]['trip_start_date']?>" data-date-end-date="<?=$trip_details[0]['trip_end_date']?>">
                                    <input type="text" class="form-control" id="special_rate_end_date" name="special_rate_end_date" value="<?=($trip_details[0]['special_rate_end_date']!='NULL')?$trip_details[0]['special_rate_end_date']:''?>" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <?php foreach ($seat_type_price as $price): ?>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Price'); ?></label>
                                        <input type="hidden" name="price_id[]" value="<?=$price['price_id']?>">
                                        <input class="form-control" type="text" name="seat_type_price[]" value="<?=$price['seat_type_price']?>" readonly="readonly">
                                    </div>
                                    <div class="col-md-6">
                                        <label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Special Price'); ?></label>
                                        <input class="form-control" type="text" name="special_price[]" value="<?=($price['special_price']=='NULL')?'0':$price['special_price']?>" required="required">
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                        
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('Save Special Rate'); ?></button>
                                <a href="<?= base_url('user-panel-bus/bus-trip-special-rate-list'); ?>" class="btn btn-primary"><?= $this->lang->line('back'); ?></a>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>

    <script>
        $("#tripAdd")
        .validate({
            ignore: [],
            rules: {
                special_rate_start_date: { required : true },
                special_rate_end_date: { required : true },
            },
            messages: {
                special_rate_start_date: { required : "<?= $this->lang->line('Select start date!'); ?>", },
                special_rate_end_date: { required : "<?= $this->lang->line('Select end date!'); ?>" },
            },
        });
    </script>
    <script>
        $('#special_rate_start_date').change(function(){
            var start_date = $('#special_rate_start_date').val();
            var end_date = $('#trip_end_date').text();
            $('#special_rate_end_date').datepicker('remove');
            $('#special_rate_end_date').datepicker({
                format: 'mm/dd/yyyy',
                startDate: start_date,
                endDate: end_date
            });
            $('#special_rate_end_date').datepicker('update');
        });
    </script>