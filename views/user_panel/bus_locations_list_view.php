
	<div class="normalheader small-header">
		<div class="hpanel">
		    <div class="panel-body">
		        <a class="small-header-action" href="">
		          <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
		        </a>
		        <div id="hbreadcrumb" class="pull-right">
	            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-master-list'; ?>"><?= $this->lang->line('Trip Master'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Trip Locations'); ?></span></li>
	            </ol>
		        </div>
		        <h2 class="font-light m-b-xs">  <i class="fa fa-map-marker fa-2x text-muted"></i> <?= $this->lang->line('Trip Locations'); ?></h2>
          		<small class="m-t-md"><?= $this->lang->line('Manage trip multiple locations'); ?></small>  
		    </div>
		</div>
	</div>

	<div class="content">
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
						<div class="row form-group">
							<form action="<?= base_url('user-panel-bus/bus-trip-location-add-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="locationAdd">
								<input type="hidden" name="trip_id" value="<?=$trip_id?>">
								<?php $trip_details = $this->api->get_trip_master_details($trip_id); ?>
								<input type="hidden" name="vehical_type_id" id="vehical_type_id" value="<?=$trip_details[0]['vehical_type_id']?>">
	                            <div class="col-md-12">
									<h3><?=$trip_details[0]['trip_source']. ' - ' .$trip_details[0]['trip_destination']?></h3>
	                            </div>
	                            <div class="col-md-12">
	                                <div class="col-md-5">
	                                    <label class=""><?= $this->lang->line('Select Source'); ?></label>
	                                    <select class="form-control select2" name="src_loc_id" id="src_loc_id">
	                                        <option value=""><?= $this->lang->line('Select Source'); ?></option>
	                                        <?php foreach ($locations as $location) { ?>
	                                            <option value="<?= $location['loc_id'] ?>"  ><?= $location['loc_city_name'] ?></option>
	                                        <?php } ?>
	                                    </select>
	                                    <div id="src_loc_id_points" style="max-height: 300px; scroll-behavior: auto; padding-right: 0px; padding-left: 0px">
	                                    </div>
	                                </div>
	                                <div class="col-md-5">
	                                    <label class=""><?= $this->lang->line('Select Destination'); ?></label>
	                                    <select class="form-control select2" name="dest_loc_id" id="dest_loc_id">
	                                        <option value=""><?= $this->lang->line('Select Destination'); ?></option>
	                                        <?php foreach ($locations as $location) { ?>
	                                            <option value="<?= $location['loc_id'] ?>" ><?= $location['loc_city_name'] ?></option>
	                                        <?php } ?>
	                                    </select>
	                                    <div id="dest_loc_id_points" style="max-height: 300px; scroll-behavior: auto; padding-right: 0px; padding-left: 0px">
	                                    </div>
	                                </div>
	                                <div class="col-md-2"><br />
	                                   	<button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('Add Location'); ?></button>
	                                </div>
	                            </div>
	                        </form>
                        </div>
				    </div>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
						<table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
				            <thead>
				                <tr>
				                	<th><?= $this->lang->line('Sr. No'); ?></th>
				                	<th><?= $this->lang->line('Source'); ?></th>
				                	<th><?= $this->lang->line('Destination'); ?></th>
				                	<th><?= $this->lang->line('Created On'); ?></th>
				                	<!--<th class="text-center"><?= $this->lang->line('action'); ?></th>-->
				                </tr>
				            </thead>
				            <tbody class="panel-body">
					            <?php foreach ($locations_list as $loc) { static $i = 0; $i++; ?>
					           		<tr>
					           			<?php if(!$loc['is_master']) { ?>
						           			<td><?=$i?></td>
						           			<td><?=$loc['trip_source']?></td>
						           			<td><?=$loc['trip_destination']?></td>
						           			<td><?=$loc['trip_cre_datetime']?></td>
						           			<!--<td class="text-center">
		                                    	<button style="display: inline-block;" class="btn btn-danger btn-sm locationDeleteAlert" id="<?= $loc['sdd_id'] ?>"><i class="fa fa-trash-o"></i> <span class="bold"><?= $this->lang->line('delete'); ?></span></button>
					           				</td>-->
		                                <?php } ?>
					           		</tr>
								<?php } ?>
				           	</tbody>
				        </table>
				    </div>
				</div>
			</div>
		</div>
	</div>

	<script>
        $('.locationDeleteAlert').click(function () {
            var id = this.id;
            swal(
            {
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-bus/bus-trip-location-delete')?>", { sdd_id: id }, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('Deleted!'); ?>", "<?= $this->lang->line('Location has been deleted!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting location!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Location is safe!'); ?>", "error"); }
            });
        });
    </script>

    <script type="text/javascript">
      $("#locationAdd")
        .validate({
          ignore: [],
          rules: {
            src_loc_id: { required : true },
            dest_loc_id: { required : true },
          },
          messages: {
              src_loc_id: { required : "<?= $this->lang->line('Select source location!'); ?>", },
              dest_loc_id: { required : "<?= $this->lang->line('Select destination location!'); ?>", },
          },
        });
    </script>

    <script>
        // Source Points
        $('#src_loc_id').on('change', function() {
            var src_id = this.value;
            var vehical_type_id = $("#vehical_type_id").val();
            $.ajax({
                url: "<?=base_url('user-panel-bus/get-locations-pickup-points')?>",
                type: "POST",
                data: { id: src_id, type_id: vehical_type_id },
                dataType: "json",
                success: function(res){
                    //console.log(res);
                    $.each( res, function() { 
                        $('#src_loc_id_points').append('<div class="col-md-12">'+
                                                        '<label>'+$(this).attr('point_address')+'</label>'+
                                                        '<input type="hidden" value="'+$(this).attr('point_id')+'" name="src_point_ids[]">'+
                                                        '<div class="input-group clockpicker" data-autoclose="true">'+
                                                        '<input type="text" class="form-control" id="pickuptime" name="src_point_times[]" value="00:00" />'+
                                                        '<span class="input-group-addon">'+
                                                            '<span class="fa fa-clock-o"></span>'+
                                                        '</span>'+
                                                        '</div></div>');
                        $('.clockpicker').clockpicker();
                    });
                },
                beforeSend: function(){
                    $('#src_loc_id_points').empty();
                },
                error: function(){
                    $('#src_loc_id_points').empty();
                }
            });
        });

        // destination points
        $('#dest_loc_id').on('change', function() {
            var src_id = this.value;
            var vehical_type_id = $("#vehical_type_id").val();
            $.ajax({
                url: "<?=base_url('user-panel-bus/get-locations-drop-points')?>",
                type: "POST",
                data: { id: src_id, type_id: vehical_type_id },
                dataType: "json",
                success: function(res){
                    //console.log(res);
                    $.each( res, function() { 
                        $('#dest_loc_id_points').append('<div class="col-md-12">'+
                                                        '<label>'+$(this).attr('point_address')+'</label>'+
                                                        '<input type="hidden" value="'+$(this).attr('point_id')+'" name="dest_point_ids[]">'+
                                                        '<div class="input-group clockpicker" data-autoclose="true">'+
                                                        '<input type="text" class="form-control" id="pickuptime" name="dest_point_times[]" value="00:00" />'+
                                                        '<span class="input-group-addon">'+
                                                            '<span class="fa fa-clock-o"></span>'+
                                                        '</span>'+
                                                        '</div></div>');
                        $('.clockpicker').clockpicker();
                    });
                },
                beforeSend: function(){
                    $('#dest_loc_id_points').empty();
                },
                error: function(){
                    $('#dest_loc_id_points').empty();
                }
            });
        })
    </script>