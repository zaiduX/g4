    
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('cancellation_rescheduling_charges'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">  <i class="fa fa-map-marker fa-2x text-muted"></i>
                    <?= $this->lang->line('cancellation_rescheduling_charges'); ?> &nbsp;&nbsp;&nbsp;
                    <a href="<?= $this->config->item('base_url') . 'user-panel-bus/add-bus-operator-cancellation-charges'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?> </a>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('details'); ?></small> 
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body"> <?php //echo json_encode($cancellation_details); ?>
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?= $this->lang->line('country'); ?></th>
                                <th><?= $this->lang->line('hours'); ?></th>
                                <th><?= $this->lang->line('cencellation_%'); ?></th>
                                <th><?= $this->lang->line('Rescheduling_%'); ?></th>
                                <th><?= $this->lang->line('Vehicle Type'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($cancellation_details as $detail) { static $i = 0; $i++; ?>
                                <tr>
                                    <td><?=$i?></td>
                                    <td><?= $this->user->get_country_name_by_id($detail['country_id']) ?></td>
                                    <td><?=$detail['bcr_min_hours'].' - '.$detail['bcr_max_hours']?></td>
                                    <td><?=$detail['bcr_cancellation']?></td>
                                    <td><?=$detail['bcr_rescheduling']?></td>
                                    <td><?=strtoupper($this->api->get_vehicle_type_by_id($detail['vehical_type_id'])['vehicle_type'])?></td>
                                    <td class="text-center">
                                        <form action="<?=base_url('user-panel-bus/edit-bus-operator-cancellation-charges')?>" method="post" style="display: inline-block;">
                                            <input type="hidden" name="bcr_id" value="<?= $detail['bcr_id'] ?>">
                                            <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-pencil"></i> <span class="bold"><?= $this->lang->line('edit'); ?></span></button>
                                        </form>
                                        <button style="display: inline-block;" class="btn btn-danger btn-sm deleteCharge" id="<?= $detail['bcr_id'] ?>"><i class="fa fa-times"></i> <span class="bold"><?= $this->lang->line('delete'); ?></span></button>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.deleteCharge').click(function () {
            var id = this.id;
            swal(
            {
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-bus/cencellation-rescheduling-charges-delete')?>", {bcr_id: id}, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('deleted'); ?>", "<?= $this->lang->line('Configuration has been deleted!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting configuration!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Configuration is safe!'); ?>", "error"); }
            });
        });
    </script>