<style type="text/css">
  .table > tbody > tr > td {
      border-top: none;
  }
  .dataTables_filter {
      display: none;
  }
  .funkyradio div {
    clear: both;
    overflow: hidden;
  }
  .funkyradio label {
    width: 100%;
    border-radius: 3px;
    border: 1px solid #D1D3D4;
    font-weight: normal;
  }
  .funkyradio input[type="radio"]:empty,
  .funkyradio input[type="checkbox"]:empty {
    display: none;
  }
  .funkyradio input[type="radio"]:empty ~ label,
  .funkyradio input[type="checkbox"]:empty ~ label {
    position: relative;
    line-height: 2.5em;
    text-indent: 3.25em;
    margin-top: 2em;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }
  .funkyradio input[type="radio"]:empty ~ label:before,
  .funkyradio input[type="checkbox"]:empty ~ label:before {
    position: absolute;
    display: block;
    top: 0;
    bottom: 0;
    left: 0;
    content: '';
    width: 2.5em;
    background: #D1D3D4;
    border-radius: 3px 0 0 3px;
  }
  .funkyradio input[type="radio"]:hover:not(:checked) ~ label,
  .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
    color: #888;
  }
  .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
  .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
    content: '\2714';
    text-indent: .9em;
    color: #C2C2C2;
  }
  .funkyradio input[type="radio"]:checked ~ label,
  .funkyradio input[type="checkbox"]:checked ~ label {
    color: #777;
  }
  .funkyradio input[type="radio"]:checked ~ label:before,
  .funkyradio input[type="checkbox"]:checked ~ label:before {
    content: '\2714';
    text-indent: .9em;
    color: #333;
    background-color: #ccc;
  }
  .funkyradio input[type="radio"]:focus ~ label:before,
  .funkyradio input[type="checkbox"]:focus ~ label:before {
    box-shadow: 0 0 0 3px #999;
  }
  .funkyradio-default input[type="radio"]:checked ~ label:before,
  .funkyradio-default input[type="checkbox"]:checked ~ label:before {
    color: #333;
    background-color: #ccc;
  }
  .funkyradio-primary input[type="radio"]:checked ~ label:before,
  .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #337ab7;
  }
  .funkyradio-success input[type="radio"]:checked ~ label:before,
  .funkyradio-success input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #5cb85c;
  }
  .funkyradio-danger input[type="radio"]:checked ~ label:before,
  .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #d9534f;
  }
  .funkyradio-warning input[type="radio"]:checked ~ label:before,
  .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #f0ad4e;
  }
  .funkyradio-info input[type="radio"]:checked ~ label:before,
  .funkyradio-info input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #5bc0de;
  }
</style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry'); ?>"><?= $this->lang->line('dash'); ?></a>
          </li>
          <li class="active"><span><?= $this->lang->line('Create Laundry Booking'); ?></span>
          </li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">
        <?= $this->lang->line('bookings'); ?>
      </h2>
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="hpanel hgreen">
      <div class="panel-body">
        <form name="simpleForm" id="simpleForm" action="<?=base_url('user-panel-laundry/provider-laundry-booking')?>" method="post">
          <input type="hidden" name="provider_id" id="provider_id" value="<?=$cust_id?>">
          <div class="text-center" id="wizardControl">
            <a style="pointer-events: none;" class="btn btn-success cloth_type" data-toggle="tab"><?=$this->lang->line('cloth category')?></a>
            <a style="pointer-events: none;" class="btn btn-success pick_drop"  data-toggle="tab"><?=$this->lang->line('pickup and drop')?></a>
            <a style="pointer-events: none;" class="btn btn-success laundry_list"  data-toggle="tab"><?=$this->lang->line("customers info")?></a>
            <a style="pointer-events: none;" class="btn btn-success booking_summary"  data-toggle="tab"><?=$this->lang->line('Booking summary')?></a>
          </div>
          <div class="tab-content">
            <div id="tab1" class="form-group p-m tab-pane active">
              <div class="row type_category" style="border: 1px solid #3498db">
                <a>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" class="classCategory" style="margin-bottom: -25px; margin-bottom: -25px; padding-left: 0px; padding-right: 0px;">
                    <div class="hpanel plan-box" id="type_category_man">
                      <div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px; padding-left: 5px !important; padding-right: 5px !important;">
                        <img src="<?=base_url('resources/category-icons/man.png')?>" style="max-width: 64px; height: auto;"><br />
                        <h5 class="font-extra-bold no-margins text-success"><?=$this->lang->line('man')?></h5>
                        <label style="font-size: 1em;">[<?=$this->lang->line('In Cart')?> - </label> <label id="type_category_man_count" style="font-size: 1em;"><?=(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) ? ($this->api->get_cloth_count_by_cat_id($cust_id, 1)[0]['cat_count'])?$this->api->get_cloth_count_by_cat_id($cust_id, 1)[0]['cat_count']:'0':'0'?></label><label style="font-size: 1em;">]</label>
                      </div>
                    </div>
                  </div>
                </a>
                <a>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" class="classCategory" style="margin-bottom: -25px; margin-bottom: -25px; padding-left: 0px; padding-right: 0px;">
                    <div class="hpanel plan-box" id="type_category_woman">
                      <div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px; padding-left: 5px !important; padding-right: 5px !important;">
                        <img src="<?=base_url('resources/category-icons/woman.png')?>" style="max-width: 64px; height: auto;"><br />
                        <h5 class="font-extra-bold no-margins text-success"><?=$this->lang->line('woman')?></h5>
                        <label style="font-size: 1em;">[<?=$this->lang->line('In Cart')?> - </label> <label id="type_category_woman_count" style="font-size: 1em;"><?=(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) ? ($this->api->get_cloth_count_by_cat_id($cust_id, 2)[0]['cat_count'])?$this->api->get_cloth_count_by_cat_id($cust_id, 2)[0]['cat_count']:'0':'0'?></label><label style="font-size: 1em;">]</label>
                      </div>
                    </div>
                  </div>
                </a>
                <a>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" class="classCategory" style="margin-bottom: -25px; margin-bottom: -25px; padding-left: 0px; padding-right: 0px;">
                    <div class="hpanel plan-box" id="type_category_child">
                      <div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px; padding-left: 5px !important; padding-right: 5px !important;">
                        <img src="<?=base_url('resources/category-icons/child.png')?>" style="max-width: 64px; height: auto;"><br />
                        <h5 class="font-extra-bold no-margins text-success"><?=$this->lang->line('child')?></h5>
                        <label style="font-size: 1em;">[<?=$this->lang->line('In Cart')?> - </label> <label id="type_category_child_count" style="font-size: 1em;"><?=(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) ? ($this->api->get_cloth_count_by_cat_id($cust_id, 3)[0]['cat_count'])?$this->api->get_cloth_count_by_cat_id($cust_id, 3)[0]['cat_count']:'0':'0'?></label><label style="font-size: 1em;">]</label>
                      </div>
                    </div>
                  </div>
                </a>
                <a>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" class="classCategory" style="margin-bottom: -25px; margin-bottom: -25px; padding-left: 0px; padding-right: 0px;">
                    <div class="hpanel plan-box" id="type_category_other">
                      <div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 5px; padding-left: 5px !important; padding-right: 5px !important;">
                        <img src="<?=base_url('resources/category-icons/other.png')?>" style="max-width: 64px; height: auto;"><br />
                        <h5 class="font-extra-bold no-margins text-success"><?=$this->lang->line('other')?></h5>
                        <label style="font-size: 1em;">[<?=$this->lang->line('In Cart')?> - </label> <label id="type_category_other_count" style="font-size: 1em;"><?=(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] == 9) ? ($this->api->get_cloth_count_by_cat_id($cust_id, 4)[0]['cat_count'])?$this->api->get_cloth_count_by_cat_id($cust_id, 4)[0]['cat_count']:'0':'0'?></label><label style="font-size: 1em;">]</label>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div class="row sub_categories_man" style="border: 1px solid #3498db;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  text-center div_men hidden">
                  <label class="not_found_man text-center" style="color:#3498db"></label>
                </div>
              </div>
              <div class="row sub_categories_woman" style="border: 1px solid #3498db;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center div_woman hidden">
                  <label class="not_found_woman text-center" style="color:#3498db"></label>
                </div>
              </div>
              <div class="row sub_categories_child" style="border: 1px solid #3498db;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center div_child hidden">
                  <label class="not_found_child text-center" style="color:#3498db"></label>
                </div>
              </div>
              <div class="row sub_categories_other" style="border: 1px solid #3498db;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center div_other hidden">
                  <label class="not_found_other text-center" style="color:#3498db"></label>
                </div>
              </div>

              <div class="text-right m-t-xs">
                <button class="btn btn-outline btn-success" id="tab1next"><?=$this->lang->line('select')." ".$this->lang->line('pickup and drop')?></button>
              </div>
            </div>
            <div id="tab2" class="p-m tab-pane">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="border: 1px solid #3498db">
                  <div class="row" style="padding-left: 10px;">
                    <h4 style="color:#3498db"><i class="fa fa-shopping-cart"></i> <?= $this->lang->line('my cart')?></h4>
                  </div>
                  <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                      <lable style="color:#3498db"><?=$this->lang->line("item")?></lable>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">  
                      <lable style="color:#3498db"><?=$this->lang->line("quantity")?></lable>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">  
                      <lable style="color:#3498db"><?=$this->lang->line("price")?></lable>
                    </div>
                  </div>
                  <hr style="margin-top:10px;margin-bottom:10px;">
                  <div class="selected_item">
                  </div>
                </div>
              </div>
              <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 form-group" style="padding-left: 0px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="border: 1px solid #3498db;">
                  <div class="row" style="padding-left: 10px; padding-top:10px; padding-bottom:10px;" >
                    <div class="col-xl-3 col-lg-3 col-md-5 col-sm-5" style="padding-right: 6px;">
                      <label class=""><?= $this->lang->line('Expected Return Date');?></label>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 24px;">
                      <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                        <input type="text" class="form-control" id="expected_return_date" name="expected_return_date" autocomplete="none" autocomplete="off" />
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div> 
                    </div>
                  </div> 

                  <div class="row" style="padding-left: 10px; padding-top:10px; padding-bottom:10px;" >
                    <div class="col-xl-3 col-lg-3 col-md-5 col-sm-5" style="padding-right: 6px;">
                      <label class=""><?= $this->lang->line('Booking Description');?></label>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right:0px; padding-left: 24px;">
                      <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                  </div>

                  <div class="row" style="padding-left: 10px; padding-top:10px; padding-bottom:10px;" >
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12" style="padding-right: 6px;">
                      <label class=""><?= $this->lang->line('Select Drop (Optional)');?></label>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4" style="padding-right: 6px;">
                      <label class="">
                        <div class="checkbox checkbox-success checkbox-inline">
                          <input type="checkbox" value="select_from_add" id="from_address_check" name="from_address_check">
                          <label for="from_address_check"></label>
                        </div>
                        <?= $this->lang->line('is pickup'); ?>
                      </label>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4" style="padding-right:0px; padding-left: 24px;">
                      <label class="">
                        <div class="checkbox checkbox-success checkbox-inline">
                          <input type="checkbox" value="select_to_add" id="to_address_check" name="to_address_check">
                          <label for="to_address_check"></label>
                        </div>                      
                        <?= $this->lang->line('is drop'); ?>
                      </label>
                    </div>
                  </div> 
                  <!-- checkbox -->
                  
                  <!-- address book and search map-->  
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-bottom:10px;">     
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 pickup_div" style="padding-right: 6px;">
                      <label><?=$this->lang->line('Select Pickup/Drop Address')?></label>
                      <div style="display: flex;">
                        <input type="hidden" name="from_address_lat_long_hidden" id="from_address_lat_long_hidden" value="" />
                        <a class="btn btn-default" id="btnFromAddrShow" onclick="FromAddrShow()" title="<?= $this->lang->line('show_my_saved_addresses'); ?>" style="display: none;"><i class="fa fa-address-book"></i></a>
                        <input type="" name="" class="form-control" id="fromMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>" style="display: none;" autocomplete="off">
                        <select class="form-control select2" name="from_address" id="from_address">
                          <option value=""><?= $this->lang->line('select')." ".$this->lang->line('drop_')." ".$this->lang->line('address'); ?></option>
                          <?php foreach ($address as $adr ) :?>
                            <option value="<?= $adr['addr_id']; ?>">
                              <?= ($adr['comapny_name'] !="NULL")? ucfirst($adr['comapny_name']) .' | ' .ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ' )' : ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ' )'; ?>
                            </option>
                          <?php endforeach; ?>
                        </select>
                        <a class="btn btn-default" id="btnFromGwidgetShow" onclick="FromGwidgetShow()" title="<?=$this->lang->line('add_address')?>"><i class="fa fa-pencil"></i></a>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <label class=""><?= $this->lang->line('Pickup/Drop Date Time'); ?></label>
                        <div class="">
                          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8" style="padding-left: 0px;">
                            <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                              <input type="text" class="form-control" id="pickupdate" name="pickupdate" autocomplete="none" autocomplete="off" />
                              <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                              </div>
                            </div> 
                          </div>
                          <div class="">
                            <div class="input-group clockpicker" data-autoclose="true">
                              <input type="text" class="form-control" id="pickuptime" name="pickuptime" autocomplete="off" />
                              <span class="input-group-addon">
                                <span class="fa fa-clock-o"></span>
                              </span>
                            </div> 
                          </div>
                        </div>
                      </div>

                      <div id="fromAddrDiv" style="display: none;">
                        <input type="hidden" from-data-geo="lat" name="frm_latitude" id="frm_latitude" value="" />
                        <input type="hidden" from-data-geo="lng" name="frm_longitude" id="frm_longitude" value="" />
                        <input type="hidden" from-data-geo="formatted_address" id="frm_street" name="frm_street" class="form-control" placeholder="<?= $this->lang->line('street_name'); ?>" />
                        <input type="hidden" from-data-geo="postal_code" id="frm_zipcode" name="frm_zipcode" class="form-control" placeholder="<?= $this->lang->line('zip_code'); ?>"  />
                        <input type="hidden" from-data-geo="country" id="frm_country" name="frm_country" class="form-control" placeholder="<?= $this->lang->line('country'); ?>" readonly/>
                        <input type="hidden" from-data-geo="administrative_area_level_1" id="frm_state" name="frm_state" class="form-control" id="" placeholder="<?= $this->lang->line('state'); ?>" readonly/>
                        <input type="hidden" from-data-geo="locality" id="frm_city" name="frm_city" class="form-control" placeholder="<?= $this->lang->line('city'); ?>" readonly/>
                        <div class="form-group" style="margin-top: 5px;">
                          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <label class=""><?= $this->lang->line('address_line_1'); ?></label>
                            <input type="text" from-data-geo="sublocality" id="frm_address1" name="frm_address1" class="form-control" placeholder="<?= $this->lang->line('address_line_1'); ?>"  />
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <label>Address Type</label>
                            <select id="frm_address_type" name="frm_address_type" class="form-control select2" data-allow-clear="true">
                              <option value="Individual"><?= $this->lang->line('individual'); ?></option></option>
                              <option value="Commercial"><?= $this->lang->line('commercial'); ?></option>
                            </select>
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <label><?= $this->lang->line('compnay_name'); ?></label>
                            <input type="text" class="form-control" id="frm_company" name="frm_company" placeholder="<?= $this->lang->line('enter_company_name'); ?>" readonly />
                          </div> 
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <label class=""><?= $this->lang->line('contact_first_name'); ?></label>
                            <input type="text" id="frm_firstname" name="frm_firstname" class="form-control" placeholder="<?= $this->lang->line('enter_first_name'); ?>" />
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <label class=""><?= $this->lang->line('contact_last_name'); ?></label>
                            <input type="text" name="frm_lastname" class="form-control" id="frm_lastname" placeholder="<?= $this->lang->line('enter_last_name'); ?>" />
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <label><?= $this->lang->line('email_address'); ?></label>
                            <input type="email" class="form-control" id="frm_email_id" name="frm_email_id" placeholder="<?= $this->lang->line('enter_email_address'); ?>" />
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                            <input type="text" id="frm_mobile" name="frm_mobile" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number'); ?>" />
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 hidden">
                            <label><?= $this->lang->line('photo'); ?></label>
                            <input type="file" accept="image/png, image/jpeg, image/gif" name="frm_address_image" id="frm_address_image" class="form-control" />
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <label class=""><?= $this->lang->line('pickup_instructions'); ?></label>
                            <textarea placeholder="<?= $this->lang->line('pickup_instructions'); ?>" class="form-control" name="frm_pickup_instruction" id="frm_pickup_instruction" rows="1" style="resize: none;"></textarea>
                          </div>
                          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <label class=""><?= $this->lang->line('deliver_instructions'); ?></label>
                            <textarea placeholder="<?= $this->lang->line('deliver_instructions'); ?>" class="form-control" name="frm_deliver_instruction" id="frm_deliver_instruction" rows="1" style="resize: none;"></textarea>
                          </div>
                          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <br /><a class="btn btn-info btn-sm" id="btnFromAddressSave"><?= $this->lang->line('save_address'); ?></a>
                          </div>
                        </div> 
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      <div id="map" class="map_canvas" style="display: none; width:100%; height: 160px;"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="text-right m-t-xs">
                  <button class="btn btn-outline btn-warning" id="tab2pre"><?=$this->lang->line('back to')." ".$this->lang->line('cloth category')?></button>
                  <button class="btn btn-outline btn-success" id="tab2next"><?=$this->lang->line("customers info")?></button>
                </div>
              </div>
            </div>
            <div id="tab3" class="p-m tab-pane">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="border: 1px solid #3498db">
                  <div class="row" style="padding-left: 10px;">
                    <h4 style="color:#3498db"><i class="fa fa-shopping-cart"></i> <?= $this->lang->line('my cart')?></h4>
                  </div>
                  <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                      <lable style="color:#3498db"><?=$this->lang->line("item")?></lable>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">  
                      <lable style="color:#3498db"><?=$this->lang->line("quantity")?></lable>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">  
                      <lable style="color:#3498db"><?=$this->lang->line("price")?></lable>
                    </div>
                  </div>
                  <hr style="margin-top:10px;margin-bottom:10px;">
                  <div class="selected_item">
                  </div>
                </div>
              </div>

              <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12" style="padding-left: 0px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="border: 1px solid #3498db">
                  <div class="row" style="padding-left: 10px;">
                    <h3 style="color:#3498db"><i class="fa fa-user"></i> <?= $this->lang->line("customers info")?></h3>
                  </div> 
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-top:0px; margin-bottom:15px;">
                    <h4><?= $this->lang->line('Select Existing Customer'); ?></h4>
                    <select class="form-control select2" name="walking_id" id="walking_id">
                      <option value=""><?= $this->lang->line('Search walk-in customer by Name/Mobile/Email'); ?></option>
                      <?php foreach ($walking_customers as $walkin ) :?>
                        <option value="<?=$walkin['walkin_id'].'~'.$walkin['firstname'].'~'.$walkin['lastname'].'~'.$walkin['phone'].'~'.$walkin['email'].'~'.$walkin['country_id']; ?>"><?= $walkin['firstname'].' '.$walkin['lastname'].' ['.$walkin['phone'].'] '.' ['.$walkin['email'].'] '; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>                  
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-top:0px; margin-bottom:15px;" id="customer_info">
                    <h4><?= $this->lang->line('OR Create New Customer'); ?></h4>
                    <div class="row form-group">
                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <label><?=$this->lang->line('first_name')?></label>
                        <input type="text" class="form-control" id="f_name" name="f_name" autocomplete="none" autocomplete="off" />
                      </div>
                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <label><?=$this->lang->line('last_name')?></label>
                        <input type="text" class="form-control" id="l_name" name="l_name" autocomplete="none" autocomplete="off" />
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <label><?=$this->lang->line('mobile_number')?></label>
                        <input type="text" class="form-control" id="customer_mobile" name="customer_mobile" autocomplete="none" autocomplete="off" />
                      </div>
                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <label><?=$this->lang->line('email')?></label>
                        <input type="text" class="form-control" id="customer_email" name="customer_email" autocomplete="none" autocomplete="off" />
                      </div>
                    </div>

                    <input type="hidden" name="customer_street_name" id="customer_street_name" value="NULL">
                    <input type="hidden" name="customer_addr_line1" id="customer_addr_line1" value="NULL">
                    <input type="hidden" name="customer_addr_line2" id="customer_addr_line2" value="NULL">
                    <input type="hidden" name="customer_latitude" id="customer_latitude" value="NULL">
                    <input type="hidden" name="customer_longitude" id="customer_longitude" value="NULL">
                    <div class="row form-group">
                      <!-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <label><?=$this->lang->line('gender')?></label>
                        <select class="form-control select2" name="customer_gender" id="customer_gender">
                          <option value="0"><?=$this->lang->line('select')?></option>
                          <option value="male"><?=$this->lang->line('male')?></option>
                          <option value="female"><?=$this->lang->line('female')?></option>
                        </select>
                      </div> -->
                      <!--<?=json_encode($_SESSION)?>-->
                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <label><?=$this->lang->line('country')?></label>
                        <select class="form-control select2" name="customer_country" id="customer_country">
                          <option value="0"><?=$this->lang->line('select')?></option>
                          <?php foreach ($countries as $country){  ?>
                            <option value="<?=$country['country_id']?>" <?=($country['country_id'] == $_SESSION['admin_country_id'])?'selected':''?>><?=$country['country_name']?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="text-right m-t-xs">
                  <button class="btn btn-outline btn-warning" id="tab3pre"><?=$this->lang->line('back to')." ".$this->lang->line('pickup and drop')?></button>
                  <button class="btn btn-outline btn-success" id="tab3next"><?=$this->lang->line('view')." ".$this->lang->line('Booking summary')?></button>
                </div>
              </div>
            </div>
            <div id="tab4" class="tab-pane">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-top:20px; margin-bottom:15px; padding-right: 0px;">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6" style="border: 1px solid #3498db; margin-right: 5px; margin-left: -15px;">
                  <div class="row" style="padding-left: 10px;">
                    <h4 style="color:#3498db"><i class="fa fa-shopping-cart"></i> <?= $this->lang->line('my cart')?></h4>
                  </div>
                  <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                      <lable style="color:#3498db"><?=$this->lang->line("item")?></lable>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">  
                      <lable style="color:#3498db"><?=$this->lang->line("quantity")?></lable>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">  
                      <lable style="color:#3498db"><?=$this->lang->line("price")?></lable>
                    </div>
                  </div>
                  <hr style="margin-top:10px;margin-bottom:10px;">
                  <div class="selected_item">
                  </div>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6" style="border: 1px solid #3498db; margin-left: 0px; margin-right: 5px;">
                  <div class="row" style="padding-left: 10px;">
                    <h4 style="color:#3498db"><i class="fa fa-user"></i> <?= $this->lang->line("customers info")?></h4>
                  </div>
                  <div class="row"> 
                    <div class="customer_information">
                    </div>
                  </div>
                </div>
              </div>

              <!-- <div class="col-lg-12">
                <div class="header-title text-center"><h4><?= $this->lang->line('advance_payment_details'); ?></h4></div>
                <br/>
              </div>

              <div class="col-lg-3">
                <label><h4><?= $this->lang->line('payment_mode'); ?></h4></label><br/>
                </div>
                <div class="col-lg-9">
                  <div class="radio radio-success radio-inline">
                    <input type="radio" id="payment_mode_cod" name="payment_mode" value="cod">
                    <label for="payment_mode"> <?= $this->lang->line('Cash Payment'); ?> </label>
                  </div>
                  <div class="radio radio-success radio-inline">
                    <input type="radio" id="payment_mode_mobile" name="payment_mode" value="payment">
                    <label for="payment_mode"> <?= $this->lang->line('pay_by_card_or_mobile_money'); ?> </label>
                  </div>
                  <?php if($user_details['allow_payment_delay'] == "1"): ?>
                     <div class="radio radio-success radio-inline">
                      <input type="radio" id="payment_mode_bank" name="payment_mode" value="bank">
                      <label for="payment_mode"> <?= $this->lang->line('Pay by Bank or Cheque'); ?> </label>
                    </div> 
                  <?php endif; ?>
                </div>
                <div class="col-lg-9 col-lg-offset-3">
                  <div class="radio radio-success hidden" id="pickupDiv">
                    <input type="radio" id="at_pickup" name="cod_payment_type" value="at_pickup">
                    <label for="at_pickup"> <?= $this->lang->line('at_pick_up'); ?> </label>
                  </div>
                  <div class="radio radio-success hidden" id="deliverDiv">
                    <input type="radio" id="at_deliver" name="cod_payment_type" value="at_deliver">
                    <label for="at_deliver"> <?= $this->lang->line('at_deliver'); ?> </label>
                  </div>
                </div> -->
              
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="text-right m-t-xs">
                  <button class="btn btn-outline btn-warning" id="tab4pre"><?=$this->lang->line('back to')." ".$this->lang->line('laundry provider')?></button>
                  <button class="btn btn-outline btn-success" id="tab4next"><?=$this->lang->line('Confirm Booking')?></button>
                </div>
              </div>
            </div>
          </div>

          <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src="<?=base_url('resources/Preloader_2.gif')?>" width="64" height="64" /></div>

          <!-- ***************hidden fields***************** -->
          <input type="hidden" name="customer_lat_long" id="customer_lat_long" class="customer_lat_long" />
          <input type="hidden" name="booking_price" id="booking_price" value="">
          <input type="hidden" name="pickup_courier_price" id="pickup_courier_price" value="">
          <input type="hidden" name="drop_courier_price" id="drop_courier_price" value="">
          <input type="hidden" name="total_price" id="total_price" value="">
          <input type="hidden" name="currency_sign" id="currency_sign" value="">
          <input type="hidden" name="balance_amount" id="balance_amount" value="">
          <input type="hidden" name="item_count" id="item_count" value="">
          <input type="hidden" name="is_pickup" id="is_pickup" value="">
          <input type="hidden" name="drop_courier_price" id="drop_courier_price" value="">
          <input type="hidden" name="pickup_payment" id="pickup_payment" value="">
          <input type="hidden" name="deliverer_payment" id="deliverer_payment" value="">
          <input type="hidden" name="is_drop" id="is_drop" value="">
          <!-- ***************hidden fields***************** -->
        </form>
      </div>
    </div>
  </div>
</div>
  
<script type="text/javascript">
  $(document).ajaxStart(function(){
    $("#wait").css("display", "block");
  });
  $(document).ajaxComplete(function(){
    $("#wait").css("display", "none");
  });

  $('.cloth_type').click(false);
  $('.r_p_d').click(false);
  $('.p_info').click(false);
  $('.t_p_details').click(false);
  $('.cancel_p').click(false);
  var sub_cat = $('.sub_categories');
  var label = $('#dd');
  var selecd = "";
  var provider_id = $('#provider_id').val();

  $(document).ready(function() {
    $(".cloth_type").addClass('active');
  });

  $('#from_address_check').on('change', function() {
    var from_selected = $("input[name='from_address_check']:checked").map(function(){return $(this).val();}).get();
    if(from_selected=="select_from_add") {
      $('.pickup_div').show(); 
    } else {
      if($("#to_address_check").is(':checked')) {
        $('.pickup_div').show(); 
      } else {
        $('.pickup_div').hide();
        $("#pickupdate").val('');
        $("#pickuptime").val('');
        $("#from_address").select2("val", "");
        document.getElementById('map').style.display = "none";
        $('#from_address').val("null");
        //Update Cart
        $('.selected_item').empty();
        $.ajax({
          type: "POST",
          url: "<?=base_url('user-panel-laundry/get-cloth-details-provider');?>",
          data: {provider_id:provider_id},
          success: function(res) {
            if(res!="not_found") {
              $('.selected_item').append(res);
            }
          }
        });
      }
    }
  }); 

  $('#to_address_check').on('change', function() {
    var to_selected = $("input[name='to_address_check']:checked").map(function(){return $(this).val();}).get();
    if(to_selected=="select_to_add") {
      $('.pickup_div').show(); 
    } else {
      if($("#from_address_check").is(':checked')) {
        $('.pickup_div').show(); 
      } else {
        $('.pickup_div').hide();
        $("#pickupdate").val('');
        $("#pickuptime").val('');
        $("#from_address").select2("val", "");
        document.getElementById('map').style.display = "none";
        $('#from_address').val("null");
        //Update Cart
        $('.selected_item').empty();
        $.ajax({
          type: "POST",
          url: "<?=base_url('user-panel-laundry/get-cloth-details-provider');?>",
          data: {provider_id:provider_id},
          success: function(res) {
            if(res!="not_found") {
              $('.selected_item').append(res);
            }
          }
        });
      }
    }
  });

  $("#tab1next").on("click", function(e) {
    e.preventDefault();

    if (navigator.geolocation) { navigator.geolocation.getCurrentPosition(showPosition);
    } else { console.log('Geolocation is not supported by this browser.'); $("#customer_lat_long").val("Geolocation is not supported by this browser."); }
    function showPosition(position) { $("#customer_lat_long").val(position.coords.latitude + "," + position.coords.longitude); }

    $.ajax({
      type: "POST",
      url: "<?=base_url('user-panel-laundry/get-cloth-details-provider');?>",
      data: {provider_id:provider_id},
      success: function(res) {
        if(res!="not_found") {
          $('.selected_item').append(res);
          //console.log(res);
        }
        if(res=='not_found') {
          swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('select')." ".$this->lang->line('atleast')." 1 ".$this->lang->line('cloth')?>", 'warning');
        } else {
          $("#tab2").toggle('slide');$("#tab1").hide();
          $(".cloth_type").removeClass('active');
          $(".pick_drop").addClass('active');
        }
      }
    });
  });

  $("#tab2next").on("click", function(e) { e.preventDefault();
    var lat_long = $(".customer_lat_long").val();
    var selectadd = $("#from_address").val();
    var date_ = $("#pickupdate").val();
    var time_ = $("#pickuptime").val();
    var expected_return_date = $("#expected_return_date").val();

    if(expected_return_date == '') { 
      swal("<?=$this->lang->line('error')?>", "<?=$this->lang->line('Select Expected Return Date')?>", 'warning');
    } else {
      if($('#from_address_check').prop("checked") == true || $('#to_address_check').prop("checked") == true){
        if(!selectadd) { 
          swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('Select Your Address')?>", 'warning');
        } else if(!date_) { 
          swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('Select Date')?>", 'warning');
        } else if(!time_) { 
          swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('Select Time')?>", 'warning');
        } else {
          $("#tab3").toggle('slide');$("#tab2").hide();
          $(".pick_drop").removeClass('active');
          $(".laundry_list").addClass('active');
          var from_address = $("#from_address").val();
          var pickupdate = $("#pickupdate").val();
          var pickuptime = $("#pickuptime").val();

          if($("#to_address_check").is(":checked")) {
            var to_address_check = 1;
          } else { var to_address_check = 0; }

          if($("#from_address_check").is(":checked")) {
            var from_address_check = 1;
          } else { var from_address_check = 0; }
          
          $.ajax({
            type: "POST",
            url: "<?=base_url('user-panel-laundry/set-customers-info')?>", 
            data: {from_address:from_address},
            dataType: "json",
            success: function(res){
              $("#customer_info").empty();
              $("#customer_info").append(res);
            }
          });

          $.ajax({
            type: "POST",
            url: "<?=base_url('user-panel-laundry/update-provider-cart-price')?>", 
            data: {provider_id:provider_id,from_address:from_address,pickupdate:pickupdate,pickuptime:pickuptime,to_address_check:to_address_check,from_address_check:from_address_check },
            success: function(res){
              $(".selected_item").empty();
              $(".selected_item").append(res);
              //console.log(res);
            }
          });
        }
      } else { 
        $("#tab3").toggle('slide');$("#tab2").hide();
        $(".pick_drop").removeClass('active');
        $(".laundry_list").addClass('active');
      }
    }
  });

  $("#tab3next").on("click", function(e) {
    e.preventDefault();
    var customer_f_name = $("#f_name").val();
    var customer_l_name = $("#l_name").val();
    var customer_mobile = $("#customer_mobile").val();
    var customer_email = $("#customer_email").val();
    //var customer_gender = $("#customer_gender").val();
    var customer_country = $("#customer_country").val();
    if(!customer_f_name){ 
      swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('enter_first_name')?>", 'warning');
    }else if(!customer_l_name){
      swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('enter_last_name')?>", 'warning');
    }else if(!customer_mobile){
      swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('enter_mobile_number')?>", 'warning');
    }else if(!customer_email){
      swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('enter_email_address')?>", 'warning');
    // }else if(customer_gender == 0){
    //   swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('select').' '.$this->lang->line('gender')?>", 'warning');
    }else if(customer_country == 0){
      swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('select').' '.$this->lang->line('country')?>", 'warning');
    }else{
      $("#tab4").toggle('slide');$("#tab3").hide();
      $(".laundry_list").removeClass('active');
      $(".booking_summary").addClass('active');
      $(".customer_information").empty();
      $(".customer_information").append(
        '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6" style="padding-bottom:7px;">'+
          '<lable style="color:#3498db"><?=$this->lang->line('cust_name')?></lable>'+
        '</div>'+
        '<div class="col-xl-8 col-lg-8 col-md-8 col-sm-6" style="padding-bottom:7px;">'+
          customer_f_name+' '+customer_l_name+
        '</div>'+
        '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6" style="padding-bottom:7px;">'+
          '<lable style="color:#3498db"><?=$this->lang->line('email_address')?>:</lable>'+
        '</div>'+
        '<div class="col-xl-8 col-lg-8 col-md-8 col-sm-6" style="padding-bottom:7px;">'+ 
          customer_email+
        '</div>'+
        '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6" style="padding-bottom:7px;">'+
          '<lable style="color:#3498db"><?=$this->lang->line('contact_number')?>:</lable>'+
        '</div>'+
        '<div class="col-xl-8 col-lg-8 col-md-8 col-sm-6" style="padding-bottom:7px;">'+
          customer_mobile+
        '</div>');
    }
  });

  //previous buttons
  $("#tab2pre").on("click", function(e) {
    e.preventDefault();
    $("#tab1").toggle('slide');$("#tab2").hide();
    $(".pick_drop").removeClass('active');
    $(".cloth_type").addClass('active');
    $('.selected_item').empty();
  });

  $("#tab3pre").on("click", function(e) {
    e.preventDefault();
    $("#tab2").toggle('slide');$("#tab3").hide();
    $(".laundry_list").removeClass('active');
    $(".pick_drop").addClass('active');
    // $('#providers_list').empty();
    // //reset cart values
    // $('.selected_item').empty();
    // $.ajax({
    //   type: "POST",
    //   url: "<?=base_url('user-panel-laundry/get-cloth-details');?>",
    //   success: function(res) {
    //     if(res!="not_found") {
    //       $('.selected_item').append(res);
    //     }
    //   }
    // });
  });

  $("#tab4pre").on("click", function(e) {
    e.preventDefault();
    $("#tab3").toggle('slide');$("#tab4").hide();
    $(".booking_summary").removeClass('active');
    $(".laundry_list").addClass('active');
    $("#at_pickup").removeAttr("checked");
    $("#at_deliver").removeAttr("checked");
    $("#pickupDiv").addClass("hidden");
    $("#deliverDiv").addClass("hidden");
    $("#payment_mode_cod").removeAttr("checked");
    $("#payment_mode_mobile").removeAttr("checked");
    $("#payment_mode_bank").removeAttr("checked");
  });
  //********************************************************************
  var cat_man = 0;
  $("#type_category_man").on("click", function(e) {
    e.preventDefault();
    var cat_id = 1;
    $("#type_category_man").addClass('active');
    $("#type_category_woman").removeClass('active');
    $("#type_category_child").removeClass('active');
    $("#type_category_other").removeClass('active');

    $(".sub_categories_man").show();
    $(".sub_categories_other").hide();
    $(".sub_categories_woman").hide();
    $(".sub_categories_child").hide();

    if(cat_man == 0) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('user-panel-laundry/provider-sub-category-append');?>", 
        data: {cat_id:cat_id},
        success: function(res){
          cat_man++;
          $('.sub_categories_man').append(res);
        }
      }); 
    }
  });

  var cat_woman = 0;
  $("#type_category_woman").on("click", function(e) {
    e.preventDefault();
    $("#type_category_woman").addClass('active');
    $("#type_category_man").removeClass('active');
    $("#type_category_child").removeClass('active');
    $("#type_category_other").removeClass('active');

    $(".sub_categories_woman").show();
    $(".sub_categories_man").hide();
    $(".sub_categories_other").hide();
    $(".sub_categories_child").hide();

    var cat_id = 2;
    if(cat_woman == 0) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('user-panel-laundry/provider-sub-category-append');?>", 
        data: {cat_id:cat_id},
        success: function(res){
          cat_woman++;
          $('.sub_categories_woman').append(res);
        }
      });
    }
  });

  var cat_child = 0;
  $("#type_category_child").on("click", function(e) {
    e.preventDefault();
    $("#type_category_child").addClass('active');
    $("#type_category_woman").removeClass('active');
    $("#type_category_man").removeClass('active');
    $("#type_category_other").removeClass('active');

    $(".sub_categories_child").show();
    $(".sub_categories_man").hide();
    $(".sub_categories_woman").hide();
    $(".sub_categories_other").hide();

    var cat_id = 3;
    if(cat_child == 0) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('user-panel-laundry/provider-sub-category-append');?>", 
        data: {cat_id:cat_id},
        success: function(res){
          cat_child++;
          $('.sub_categories_child').append(res);
        }
      });
    }
  });

  var cat_other = 0;
  $("#type_category_other").on("click", function(e) {
    e.preventDefault();
    $("#type_category_other").addClass('active');
    $("#type_category_woman").removeClass('active');
    $("#type_category_man").removeClass('active');
    $("#type_category_child").removeClass('active');

    $(".sub_categories_other").show();
    $(".sub_categories_man").hide();
    $(".sub_categories_woman").hide();
    $(".sub_categories_child").hide();

    var cat_id = 4;
    if(cat_other == 0) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('user-panel-laundry/provider-sub-category-append');?>", 
        data: {cat_id:cat_id},
        success: function(res){
          cat_other++;
          $(".sub_categories_other").append(res);
        }
      });
    }
  });

  $('#tab4next').click(function (e) {
    e.preventDefault();
    swal({
      title: "<?= $this->lang->line('are_you_sure'); ?>",
      text: "<?= $this->lang->line('To complete this booking.'); ?>",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "<?= $this->lang->line('Yes, Complete it.'); ?>",
      cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
      closeOnConfirm: false,
      closeOnCancel: false 
    },
    function (isConfirm) {
      if (isConfirm) {
        $('#simpleForm').submit();
      } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Your booking not created yet!'); ?>", "error"); }
    });            
  });

</script>
<!-- Add Quick Address Using Google Widget -->
<script>
  $(function() {
    $('.sub_categories_man').hide(); 
    $('.sub_categories_woman').hide(); 
    $('.sub_categories_child').hide(); 
    $('.sub_categories_other').hide(); 
    $('.pickup_div').hide(); 
    $("#addAddress").keypress(function(event) { return event.keyCode != 13; });
    $("#addAddress").validate({
      ignore: [],
      rules: {
        firstname: { required: true, },      
        lastname: { required: true, },      
        mobile: { required: true, number: true, },      
        street: { required: true, },      
        country_id: { required: true, },      
        state_id: { required: true, },      
        city_id: { required: true, },      
        street: { required: true, },      
        address2: { required: true, },      
        address_image: { accept:"jpg,png,jpeg,gif" },
        pickup_instruction: { required: true, },      
        deliver_instruction: { required: true, },      
      }, 
      messages: {
        firstname: { required: <?= json_encode($this->lang->line('enter_first_name')); ?>,   },
        lastname: { required: <?= json_encode($this->lang->line('enter_last_name')); ?>,   },
        mobile: { required: <?= json_encode($this->lang->line('enter_mobile_number')); ?>, number: <?= json_encode($this->lang->line('number_only')); ?>,  },
        street: { required: <?= json_encode($this->lang->line('street_name')); ?>,   },
        country_id: { required: <?= json_encode($this->lang->line('select_country')); ?>,   },
        state_id: { required: <?= json_encode($this->lang->line('select_state')); ?>,   },
        city_id: { required: <?= json_encode($this->lang->line('select_city')); ?>,   },
        address2: { required: <?= json_encode($this->lang->line('select_address_from_map')); ?>,   },
        address_image: { accept: <?= json_encode($this->lang->line('only_image_allowed')); ?>,   },
        pickup_instruction: { required: <?= json_encode($this->lang->line('pickup_instructions')); ?>,   },
        deliver_instruction: { required: <?= json_encode($this->lang->line('deliver_instructions')); ?>,   },
      }
    });
    $("#upload_image").on('click', function(e) { e.preventDefault(); $("#address_image").trigger('click'); });
  });

  $("#frm_address_type").on('change', function(event) {  event.preventDefault();
    var type = $(this).val();
    if(type=="Commercial") { $("#frm_company").prop('readonly', false);  } else { $("#frm_company").val('').prop('readonly', true); }
  });

  function FromAddrShow() {
    document.getElementById('fromMapID').style.display = "none";
    document.getElementById('btnFromAddrShow').style.display = "none";
    document.getElementById('map').style.display = "none";

    $("#fromMapID").val('');
    $("#frm_latitude").val('');
    $("#frm_longitude").val('');
    $("#frm_street").val('');
    $("#frm_address1").val('');
    $("#frm_zipcode").val('');
    $("#frm_country").val('');
    $("#frm_state").val('');
    $("#frm_city").val('');
    $("#frm_address_type").val('');
    $("#frm_company").val('');
    $("#frm_email_id").val('');
    $("#frm_mobile").val('');
    $("#frm_firstname").val('');
    $("#frm_lastname").val('');
    $("#frm_address_image").val('');
    $("#frm_pickup_instruction").val('');
    $("#frm_deliver_instruction").val('');

    document.getElementById('fromAddrDiv').style.display = "none";
    $("#from_address_lat_long_hidden").val('');

    document.getElementById('s2id_from_address').style.display = "block";
    document.getElementById('btnFromGwidgetShow').style.display = "block";
  }
  function FromGwidgetShow() {
    $("#from_address_lat_long_hidden").val('');
    document.getElementById('from_address').selectedIndex = 0;
    document.getElementById('s2id_from_address').style.display = "none";
    document.getElementById('btnFromGwidgetShow').style.display = "none";
    document.getElementById('fromMapID').style.display = "block";
    document.getElementById('btnFromAddrShow').style.display = "block";
    document.getElementById('fromAddrDiv').style.display = "block";
    document.getElementById('map').style.display = "block";
  }

  $("#fromMapID").geocomplete({
    map:".map_canvas",
    location: "",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: true, },
    details: "form",
    detailsAttribute: "from-data-geo", 
    types: ["geocode", "establishment"],
  });

  // from address save
  $("#btnFromAddressSave").click(function() {
    frm_latitude = $('#frm_latitude').val();
    frm_longitude = $('#frm_longitude').val();
    frm_street = $('#frm_street').val();
    frm_address1 = $('#frm_address1').val();
    frm_zipcode = $('#frm_zipcode').val();
    frm_country = $('#frm_country').val();
    frm_state = $('#frm_state').val();
    frm_city = $('#frm_city').val();
    frm_address_type = $('#frm_address_type').val();
    frm_company = $('#frm_company').val();
    frm_email_id = $('#frm_email_id').val();
    frm_mobile = $('#frm_mobile').val();
    frm_firstname = $('#frm_firstname').val();
    frm_lastname = $('#frm_lastname').val();
    frm_pickup_instruction = $('#frm_pickup_instruction').val();
    frm_deliver_instruction = $('#frm_deliver_instruction').val();

    if(frm_latitude == '') { 
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_latitude_not_found')?>",'warning'); 
    } else if(frm_longitude == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_longitude_not_found')?>",'warning'); 
    } else if(frm_street == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_address_not_found')?>",'warning'); 
    } else if(frm_email_id == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_email_not_found')?>",'warning'); 
    } else if(frm_mobile == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_mobile_not_found')?>",'warning'); 
    } else if(frm_firstname == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_first_name_not_found')?>",'warning'); 
    } else if(frm_lastname == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_last_name_not_found')?>",'warning'); 
    } else if(frm_pickup_instruction == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_pickup_not_found')?>",'warning'); 
    } else if(frm_deliver_instruction == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_deliver_not_found')?>",'warning'); 
    } else {
      var formData = new FormData(this);
      formData.append('frm_latitude', $.trim(frm_latitude));
      formData.append('frm_longitude', $.trim(frm_longitude));
      formData.append('frm_street', $.trim(frm_street));
      formData.append('frm_address1', $.trim(frm_address1));
      formData.append('frm_zipcode', $.trim(frm_zipcode));
      formData.append('frm_country', $.trim(frm_country));
      formData.append('frm_state', $.trim(frm_state));
      formData.append('frm_city', $.trim(frm_city));
      formData.append('frm_address_type', $.trim(frm_address_type));
      formData.append('frm_company', $.trim(frm_company));
      formData.append('frm_email_id', $.trim(frm_email_id));
      formData.append('frm_mobile', $.trim(frm_mobile));
      formData.append('frm_firstname', $.trim(frm_firstname));
      formData.append('frm_lastname', $.trim(frm_lastname));
      formData.append('frm_pickup_instruction', $.trim(frm_pickup_instruction));
      formData.append('frm_deliver_instruction', $.trim(frm_deliver_instruction));

      var frominput = document.querySelector('input[id=frm_address_image]'),
      fromfile = frominput.files[0];
      formData.append('frm_address_image', fromfile);
      $.ajax({
        url: "<?=base_url('user-panel/add-address-to-book-from-address')?>",
        type: 'POST',
        data: formData,
        success: function(data){
          console.log(data);
          var arr = data.split('~');
          FromAddrShow();
          $('#from_address').append($('<option>',{
            value: $.trim(arr[0]),
            text: $.trim(arr[1])
          },'</option>'));

          $('#from_address').val($.trim(arr[0])).trigger('change');
          $("#from_address_lat_long_hidden").val($.trim(arr[2]));
        },
        cache: false,
        contentType: false,
        processData: false
      });
      $("#pickup_instruction").val($('#frm_pickup_instruction').val());
      $("#pickup_instruction").text($('#frm_pickup_instruction').val()); 
    }
  });
</script>

<script>
  $("#payment_mode_cod").on('change', function(event) { event.preventDefault();
    console.log($("#payment_mode_cod").val());

    if($("#from_address_check").is(':checked') && $("#to_address_check").is(':checked')){
      $("#pickupDiv").removeClass("hidden");
      $("#deliverDiv").removeClass("hidden");
    } else { 
      if ($("#from_address_check").is(':checked')){
        $("#pickupDiv").removeClass("hidden");
      } else {
        if($("#to_address_check").is(':checked')){
          $("#deliverDiv").removeClass("hidden");
        } else {
          $("#pickupDiv").removeClass("hidden");
          $("#deliverDiv").removeClass("hidden");
        }
      }
    }

    $("#tab4next").attr('disabled', true).css('pointer-events','inherit');
  });

  $("#payment_mode_mobile").on('change', function(event) { event.preventDefault();
    console.log($("#payment_mode_mobile").val());
    $("#pickupDiv").addClass("hidden");
    $("#deliverDiv").addClass("hidden");
    $("#at_pickup").removeAttr("checked");
    $("#at_deliver").removeAttr("checked");
    $("#tab4next").attr('disabled', false).css('pointer-events','inherit');
  });

  $("#payment_mode_bank").on('change', function(event) { event.preventDefault();
    console.log($("#payment_mode_bank").val());
    $("#pickupDiv").addClass("hidden");
    $("#deliverDiv").addClass("hidden");
    $("#at_pickup").removeAttr("checked");
    $("#at_deliver").removeAttr("checked");
    $("#tab4next").attr('disabled', false).css('pointer-events','inherit');
  });

  $("#pickupDiv").on('change', function(event) { event.preventDefault();
    $("#tab4next").attr('disabled', false).css('pointer-events','inherit');
  });

  $("#deliverDiv").on('change', function(event) { event.preventDefault();
    $("#tab4next").attr('disabled', false).css('pointer-events','inherit');
  });

  $( document ).ready(function() {
      $("#type_category_other").click();
      $("#type_category_child").click();
      $("#type_category_woman").click();
      $("#type_category_man").click();
      $("#type_category_man").addClass('active');
      $(".sub_categories_man").show();
  });

  $('#at_pickup').click(function(){
    if ($(this).is(':checked')){
      $("#pickup_payment").val(1);
      $("#deliverer_payment").val(0);
    }
  });

  $('#at_deliver').click(function(){
    if ($(this).is(':checked')){
      $("#pickup_payment").val(0);
      $("#deliverer_payment").val(1);
    }
  });

  $(document).ready(function(){
    $(document).ajaxStart(function(){
      $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
      $("#wait").css("display", "none");
    });
  });

  $('#walking_id').on('change', function() {
    var walkin = $('#walking_id').val();
    var walk_split = walkin.split('~');
    $('#f_name').val(walk_split[1]);
    $('#l_name').val(walk_split[2]);
    $('#customer_mobile').val(walk_split[3]);
    $('#customer_email').val(walk_split[4]);
    if(walk_split[5] == undefined) {
      $('#customer_country').val(0);
      $('#customer_country').trigger('change');
    } else {
      $('#customer_country').val(walk_split[5]);
      $('#customer_country').trigger('change');
    }
  })
</script>