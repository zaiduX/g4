  <div class="row">
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel">
        <div class="panel-body h-200" style="padding: 8px;">
          <h4 class="font-extra-bold text-center no-margins text-success">
            <?= $this->lang->line('overall ratings'); ?>
          </h4>
          <br/>
          <div class="col-md-12" style="padding: 8px;">
            <div class="col-md-8 text-left" style="padding: 0px;">
              <?php for($i=0; $i<(int)$cust['ratings']; $i++){ echo ' <i class="fa fa-star fa-2x"></i> '; } ?>
              <?php for($i=0; $i<(5-$cust['ratings']); $i++){ echo ' <i class="fa fa-star-o fa-2x"></i> '; } ?>     
            </div>
            <div class="col-md-4 text-right" style="padding: 0px;">
              <h1 style="font-size: 18px; margin-top: 2px;">( <?= $cust['ratings'] ?> / 5 )</h1> 
            </div>
          </div>
          <div class="col-md-12" style="padding: 8px;"> 
            <div class="col-md-6 text-left" style="padding: 0px;">
              <label><?=$this->lang->line('bus ratings')?></label>
            </div>
            <div class="col-md-6 text-right" style="padding: 0px;">
              <h1 style="font-size: 18px; margin-top: 2px;">( <?=($bus_rating['ratings']!=null)?$bus_rating['ratings']:0?> / 5 )</h1> 
            </div>       
          </div>
          <div class="col-md-12" style="padding: 8px;">
            <div class="col-md-6 text-left" style="padding: 0px;">
              <label><?=$this->lang->line('deliverer_rating')?></label>
            </div>
            <div class="col-md-6 text-right" style="padding: 0px;">
              <h1 style="font-size: 18px; margin-top: 2px;">( <?=($deliverer_ratings['ratings'  ]!=null)?$deliverer_ratings['ratings']:0?> / 5 )</h1> 
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
        <div class="panel-body h-200">
          <div class="stats-title pull-left">
            <h4> <?= $this->lang->line('total trips'); ?></h4>
          </div>
          <div class="stats-icon pull-right">
            <i class="pe-7s-portfolio fa-4x"></i>
          </div>
          <div class="m-t-xl">
            <h3 class="m-xs"><?=sizeof($total_trips)?></h3>
            <span class="font-bold no-margins">
              <?= $this->lang->line('completed trips'); ?>
            </span>
            <?php
              $per = 0;
              if(sizeof($completed_trips)!= 0 || sizeof($total_trips)!= 0) {
                $per= round((sizeof($completed_trips)/sizeof($total_trips))*100,2);
              }
            ?>
            <div class="progress m-t-xs full progress-small progress-striped active">
              <div style="width:<?=$per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?>%">                              
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6">
                <small class="stats-label"><?= $this->lang->line('completed trips'); ?></small>
                <h4><?=sizeof($completed_trips)?></h4>
              </div>
              <div class="col-xs-6">
                <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                <h4><?=$per?>%</h4>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
        <div class="panel-body h-200">
          <div class="stats-title pull-left">
            <h4><?=$this->lang->line('upcoming trips');?></h4>
          </div>
          <div class="stats-icon pull-right">
            <i class="pe-7s-note fa-4x"></i>
          </div>
          <div class="m-t-xl">
            <h3 class="m-xs"><?= sizeof($upcoming); ?></h3>
            <span class="font-bold no-margins">
              <?= $this->lang->line('in_progress_orders'); ?>
            </span>      
            <?php
              $per = 0;
              if(sizeof($upcoming)!= 0 || sizeof($total_trips)!= 0) {
                $per= round((sizeof($upcoming)/sizeof($total_trips))*100,2);
              } 
            ?>
            <div class="progress m-t-xs full progress-small progress-striped active">
              <div style="width:<?=$per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="3" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?>%"></div>
            </div>
            <div class="row">
              <div class="col-xs-6">
                <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                <h4><?= $per;?> %</h4>
              </div>
              <!-- <div class="col-xs-6">
                <small class="stats-label"><?= $this->lang->line('in_progress'); ?></small>
                <h4><?= $working_progress; ?> %</h4>
              </div> -->
            </div> 
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
        <div class="panel-body h-200">
          <div class="stats-title pull-left">
            <h4><?= $this->lang->line('balance_summary'); ?></h4>
          </div>
          <div class="stats-icon pull-right">
            <i class="pe-7s-cash fa-4x"></i>
          </div>
          <div class="m-t-xl">
            <h4 class="m-xs" style="margin-top: -15px;">XAF <?= $current_balance; ?></h4>
            <span class="font-bold no-margins">
              <?= $this->lang->line('current_balance'); ?>
            </span>
          </div>
          <div class="m-t-xs">
            <div class="row" style="margin-top: 0px;">
              <div class="col-xs-6">
                <small class="stat-label"><?= $this->lang->line('earned'); ?></small>
                <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_earned);?> </h4>
              </div>
              <div class="col-xs-6">
                <small class="stat-label"><?= $this->lang->line('spent'); ?></small>
                <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_spend);?></h4>
              </div>
            </div>
          </div>
          <div class="m-t-xs" style="margin-top: 0px;">
            <div class="row">
              <div class="col-xs-6">
                <small class="stat-label"><?= $this->lang->line('Ticket Earning'); ?></small>
                <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_earned_service);?> </h4>
              </div>
              <div class="col-xs-6">
                <small class="stat-label"><?= $this->lang->line('Ticket Spend'); ?></small>
                <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_spend_service);?></h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
      <div class="hpanel stats">
        <div class="panel-heading" style="margin-top: -15px;">          
          <?= $this->lang->line('Current Trips'); ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
              <thead>
                <th><strong><?= $this->lang->line('Trip ID'); ?></strong></th>
                <th><strong><?= $this->lang->line('Source'); ?></strong></th>
                <th><strong><?= $this->lang->line('Destination'); ?></strong></th>
                <th><strong><?= $this->lang->line('Journey Date'); ?></strong></th>
              </thead>
              <tbody>
                <?php if(!empty($current)):?>                      
                  <?php foreach ($current as $v): ?>                        
                    <tr>
                      <td>G-<?= $v['trip_id'];?><small></small></td>
                      <td><?= $v['source_point']; ?><small></small></td>
                      <td><?= $v['destination_point']; ?><small></small></td>
                      <td><?= $v['journey_date'];?><small></small></td>
                    </tr>
                  <?php endforeach; ?>                      
                <?php else: ?>
                  <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>      
      </div>
    </div>
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
      <div class="hpanel stats">
        <div class="panel-heading" style="margin-top: -15px;">          
          <?= $this->lang->line('Upcoming Trips'); ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
              <thead>
                <th><strong><?= $this->lang->line('Trip ID'); ?></strong></th>
                <th><strong><?= $this->lang->line('Source'); ?></strong></th>
                <th><strong><?= $this->lang->line('Destination'); ?></strong></th>
                <th><strong><?= $this->lang->line('Journey Date'); ?></strong></th>
              </thead>
              <tbody>
                <?php if(!empty($upcoming)):?>                      
                  <?php foreach ($upcoming as $v): ?>                        
                    <tr>
                      <td>G-<?= $v['trip_id'];?><small></small></td>
                      <td><?= $v['source_point']; ?><small></small></td>
                      <td><?= $v['destination_point']; ?><small></small></td>
                      <td><?= $v['journey_date'];?><small></small></td>
                    </tr>
                  <?php endforeach; ?>                      
                <?php else: ?>
                  <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row"> 
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
      <div class="hpanel stats">
        <div class="panel-heading" style="margin-top: -15px;">          
           <?= $this->lang->line('Previous Trips'); ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
              <thead>
                <th><strong><?= $this->lang->line('Trip ID'); ?></strong></th>
                <th><strong><?= $this->lang->line('Source'); ?></strong></th>
                <th><strong><?= $this->lang->line('Destination'); ?></strong></th>
                <th><strong><?= $this->lang->line('Journey Date'); ?></strong></th>
              </thead>
              <tbody>
                <?php if(!empty($previous)):?>                      
                  <?php foreach ($previous as $v): ?>                        
                    <tr>
                      <td>G-<?= $v['trip_id'];?><small></small></td>
                      <td><?= $v['source_point']; ?><small></small></td>
                      <td><?= $v['destination_point']; ?><small></small></td>
                      <td><?= $v['journey_date'];?><small></small></td>
                    </tr>
                  <?php endforeach; ?>                      
                <?php else: ?>
                  <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>      
      </div>
    </div>
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
      <div class="hpanel stats">
        <div class="panel-heading" style="margin-top: -15px;">          
           <?= $this->lang->line('Cancelled Trips'); ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
              <thead>
                <th><strong><?= $this->lang->line('Trip ID'); ?></strong></th>
                <th><strong><?= $this->lang->line('Source'); ?></strong></th>
                <th><strong><?= $this->lang->line('Destination'); ?></strong></th>
                <th><strong><?= $this->lang->line('Journey Date'); ?></strong></th>
              </thead>
              <tbody>
                <?php if(!empty($cancel_trips)):?>                      
                  <?php foreach ($cancel_trips as $v): ?>
                    <?php $c = "yes";
                      $trip_list =  $this->api->get_ticket_master_booking_details_unique_id($v['unique_id'], $c,1);
                    ?>                    
                  <?php foreach ($trip_list as $v): ?>
                    <tr>
                      <td>G-<?= $v['trip_id'];?><small></small></td>
                      <td><?= $v['source_point']; ?><small></small></td>
                      <td><?= $v['destination_point']; ?><small></small></td>
                      <td><?= $v['journey_date'];?><small></small></td>
                    </tr>
                  <?php endforeach; ?>                      
                  <?php endforeach; ?>                      
                <?php else: ?>
                  <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>      
      </div>
    </div>  
  </div>

  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
      <div class="hpanel stats">
        <div class="panel-heading" style="margin-top: -15px;">          
           <?= $this->lang->line('Cancelled Tickets'); ?>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
              <thead>
                <th><?= $this->lang->line('Ticket ID'); ?></th>
                <th><?= $this->lang->line('Name'); ?></th>
                <!--  <th><?= $this->lang->line('Total Seats'); ?></th>
                <th><?= $this->lang->line('bus_seat_type'); ?></th>
                <th><?= $this->lang->line('Total_Price'); ?></th> -->
                <th><?= $this->lang->line('Contact'); ?></th>
                <th><?= $this->lang->line('Gender'); ?></th>
                <th><?= $this->lang->line('Trip Details'); ?></th>
              </thead>
              <tbody>
                <?php foreach ($cancel_ticket as $seats) { ?>
                <tr>
                  <td><?=$seats['ticket_id']?></td>
                  <td>
                    <?php $c="yes"; $name = $this->api->get_ticket_seat_details($seats['ticket_id'],$c);
                    echo $name[0]['firstname']." ".$name[0]['lastname'];
                     if(sizeof($name)>1) {
                      echo "<button style='color: #3498db' type='button' class='btn btn-link' data-toggle='modal' data-target='#".$name[0]['seat_id']."'>".$this->lang->line('...More')."</button>";
                     } 
                    ?>
                    <div class="modal fade" id="<?=$name[0]['seat_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><?=$this->lang->line('More Seat Details')?></h4>
                          </div>
                          <div class="modal-body">
                            <table class="table" width="100%">
                              <thead>
                                <th><?= $this->lang->line('Ticket ID'); ?></th>
                                <th><?= $this->lang->line('Name'); ?></th>
                                <th><?= $this->lang->line('Gender'); ?></th>
                                <th><?= $this->lang->line('Contact'); ?></th>
                              </thead>
                              <tbody>
                                <?php foreach ($name as $seat) { ?>
                                <tr>
                                  <td><?=$seat['ticket_id']?></td>
                                  <td><?=$seat['firstname']. ' ' .$seat['lastname']?></td>
                                  <td><?=($seat['gender']=='M')?'Male':'Female'?></td>
                                  <td><?=$this->api->get_country_code_by_id($seat['country_id']).$seat['mobile']?></td>
                                </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                  <!-- <td><?=sizeof($name)?></td> -->
                  <!-- <td><?=$seats['seat_type']?></td> -->
                  <!-- <td><?=$seats['ticket_price'].$seats['currency_sign']?></td> -->
                  <td><?=$this->api->get_country_code_by_id($name[0]['country_id']).$name[0]['mobile']?></td>
                  <td><?=($name[0]['gender']=='M')?'Male':'Female'?></td>
                  <td>
                    <div style="display: inline-flex;">
                      <button id="<?=$seats['ticket_id']?>" class="btn btn-outline btn-info" data-toggle="modal" data-target="#myModal<?=$seats['ticket_id']?>"><i class="fa fa-check"></i> <?= $this->lang->line('Trip Details'); ?> </button>
                      &nbsp;
                      <div class="modal fade" id="myModal<?=$seats['ticket_id']?>" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><?= $this->lang->line('Trip Details'); ?></h4>
                            </div>
                            <div class="modal-body" id="printThis<?=$seats['ticket_id']?>">
                               <?php $trip_details = $this->api->get_trip_booking_details($seats['ticket_id']);?>
                                <div class="row">
                                    <div class="=col-lg-12">
                                         <h3 class="stat-label" style='color: #3498db'><?= $this->lang->line('Trip Details'); ?>                                 
                                         </h3>
                                    </div>
                                    <br/>
                                    <div class="col-md-6">
                                        <h5><lable style='color: #3498db'> <?=$this->lang->line('Source')?></lable>&nbsp;:&nbsp;<?=$trip_details['source_point']?></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><lable style='color: #3498db'> <?=$this->lang->line('Destination')?></lable>&nbsp;:&nbsp;<?=$trip_details['destination_point']?></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><lable style='color: #3498db'> <?=$this->lang->line('Journey Date')?></lable>&nbsp;:&nbsp;<?=$trip_details['journey_date']?></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><lable style='color: #3498db'> <?=$this->lang->line('Departure Time')?></lable>&nbsp;:&nbsp;<?=$trip_details['trip_start_date_time']?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="display: flex;">
                              <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>      
      </div>
    </div>
  </div>
</div>