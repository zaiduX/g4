<style>
    .excelHtml5Btn {
        float: left;
    }
</style>
<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
            <div class="clip-header">
                <i class="fa fa-arrow-up"></i>
            </div>
        </a>
        <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li class="active"><span><?= $this->lang->line('Pending Payments'); ?></span></li>
            </ol>
        </div>
        <h2 class="font-light m-b-xs">  <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Pending Payments'); ?></h2>
        <small class="m-t-md"><?= $this->lang->line('All pending payments'); ?></small>    
        </div>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-lg-12" style="margin-bottom: -15px;">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <?= $this->lang->line('Filter Records by date and currency'); ?>
                </div>
                <div class="panel-body" style="padding-bottom: 0px; padding-top: 7px;">
                    <form action="<?=base_url('user-panel-laundry/customer-laundry-pending-payments');?>" method="post" id="frmFilter">
                        <input type="hidden" name="cust_id" value="<?=$cust['cust_id']?>" id="cust_id">
                        <input type="hidden" name="filter" value="1">
                        <div class="row">
                            <div class="col-sm-3 form-group">
                                <label class=control-label"><?= $this->lang->line('Date Range'); ?></label>
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-sm form-control" name="date_start" value="<?=(isset($start_date) && $start_date!=null)?$start_date:date('m/d/Y')?>" autocomplete="off" />
                                    <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                                    <input type="text" class="input-sm form-control" name="date_end" value="<?=(isset($end_date) && $end_date!=null)?$end_date:date('m/d/Y')?>" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-sm-3 form-group">
                              <label class="control-label">Select Currency</label>
                              <select class="form-control select2" name="currency_sign" id="currency_sign">
                                <option value="0">Select currency</option>
                                <?php foreach ($currency_list as $currency): ?>
                                  <option value="<?=$currency['currency_sign']?>" <?=(isset($currency_sign) && $currency_sign == $currency['currency_sign'])?'selected':''?> ><?=$currency['currency_sign']?></option>
                                <?php endforeach; ?>
                              </select>
                            </div>
                            <div class="col-md-3 form-group">
                              <label class="control-label">Select Country </label>                        
                              <select id="country_id" name="country_id" class="form-control select2">
                                <option value="">Select Country</option>
                                <?php foreach ($countries as $country): ?>                      
                                  <option value="<?= $country['country_id'] ?>" <?=(isset($country_id) && $country_id == $country['country_id'])?'selected':''?> ><?= $country['country_name']; ?></option>
                                <?php endforeach ?>
                              </select>
                              <span id="error_country_id" class="error"></span>                           
                            </div>
                            <div class="col-md-3 form-group">
                                <label>&nbsp;</label><br />
                                <button type="submit" class="btn btn-outline btn-info" id="">&nbsp;<?= $this->lang->line('Filter'); ?>&nbsp;</button>&nbsp;
                                <a href="<?=base_url('user-panel-laundry/customer-laundry-pending-payments')?>" class="btn btn-outline btn-warning"><?= $this->lang->line('reset'); ?></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="hpanel hblue">
                <div class="panel-body" style="padding-top: 10px; padding-bottom: 10px;">
                    <script>
                        jQuery( document ).ready( function( $ ) {
                            var $table2 = jQuery( '#table-2' );
                            $table2.DataTable( {
                                "aaSorting": [[1, "desc"]],
                                dom: 'Bfrtip',
                                buttons: [{
                                    extend: 'excelHtml5',
                                    footer: true,
                                    className: 'btn btn-success btn-outline excelHtml5Btn',
                                    text: "<i class='fa fa-file-excel-o'></i> <?= $this->lang->line('Export to Excel'); ?>",
                                    title: "<?= $this->lang->line('Pending-Payments-Orders-List'); ?>-"+"<?=date('F-Y')?>",
                                    /*exportOptions: {
                                        columns: ':not(:last-child)'
                                    },*/
                                    customize: function( xlsx ) {
                                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                        //Column Width
                                        var col = $('col', sheet);
                                        $(col[0]).attr('width', 8);
                                        $(col[1]).attr('width', 17);
                                        $(col[2]).attr('width', 18);
                                        $(col[5]).attr('width', 13);
                                        $(col[6]).attr('width', 11);
                                        $(col[7]).attr('width', 20);
                                        $(col[8]).attr('width', 18);
                                        //Table header Labels
                                        $('c[r=A2] t', sheet).text( "<?= $this->lang->line('Booking ID'); ?>" );
                                        $('c[r=B2] t', sheet).text( "<?= $this->lang->line('Create Date'); ?>" );
                                        $('c[r=C2] t', sheet).text( "<?= $this->lang->line('deliver_date'); ?>" );
                                        $('c[r=D2] t', sheet).text( "<?= $this->lang->line('Customer Name'); ?>" );
                                        $('c[r=E2] t', sheet).text( "<?= $this->lang->line('Price'); ?>" );
                                        $('c[r=F2] t', sheet).text( "<?= $this->lang->line('Due Amount'); ?>" );
                                        $('c[r=G2] t', sheet).text( "<?= $this->lang->line('currency'); ?>" );
                                        //Header text center
                                        $('row c[r^="A"]', sheet).attr( 's', '51' );
                                        $('row c[r^="B"]', sheet).attr( 's', '51' );
                                        $('row c[r^="C"]', sheet).attr( 's', '51' );
                                        $('row c[r^="D"]', sheet).attr( 's', '51' );
                                        $('row c[r^="E"]', sheet).attr( 's', '51' );
                                        $('row c[r^="F"]', sheet).attr( 's', '51' );
                                        $('row c[r^="G"]', sheet).attr( 's', '51' );
                                        //Make bold report title
                                        $('row:first c', sheet).attr( 's', '2' );
                                    },
                                }]
                            });
                            // Initalize Select Dropdown after DataTables is created
                            $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                                minimumResultsForSearch: -1
                            });
                        } );
                    </script>
                    
                    <table class="table table-bordered display compact table-striped" id="table-2" style="max-width:100%">
                        <thead>
                            <tr>
                                <th class="text-center"><?= $this->lang->line('Booking ID'); ?></th>
                                <th class="text-center"><?= $this->lang->line('Create Date'); ?></th>
                                <th class="text-center"><?= $this->lang->line('deliver_date'); ?></th>
                                <th class="text-center"><?= $this->lang->line('Customer Name'); ?></th>
                                <th class="text-center"><?= $this->lang->line('Price'); ?></th>
                                <th class="text-center"><?= $this->lang->line('Due Amount'); ?></th>
                                <th class="text-center"><?= $this->lang->line('currency'); ?></th>
                            </tr>
                        </thead>
                        <tbody>   
                            <?php foreach ($bookings as $booking): static $grand_total = 0; ?>
                                <tr>
                                  <td class="text-center"><?= $booking['booking_id'] ?></td>
                                  <td class="text-center"><?= date('d/m/Y', strtotime($booking['cre_datetime'])); ?></td> 
                                  <td class="text-center"><?= ($booking['status_update_datetime']!='NULL')?date('d/m/Y', strtotime($booking['status_update_datetime'])):'Pending'; ?></td>
                                  <td class="text-center"><?= $booking['cust_name']; ?></td>
                                  <td class="text-center"><?= $booking['total_price']; ?></td>
                                  <td class="text-center"><?= $booking['total_price']; ?></td>
                                  <td class="text-center"><?= $booking['currency_sign']; ?></td>
                                  <?php $grand_total+=$booking['total_price']; ?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <?php if(isset($filter) && $filter == 1): ?>
                            <tfoot>
                                <tr>
                                    <th colspan="4" class="text-left"></th>
                                    <th class="text-right"><?= $this->lang->line('Grand Total'); ?> (<?=(isset($booking['currency_sign']))?$booking['currency_sign']:''; ?>)</th>
                                    <th class="text-center"><?=(isset($grand_total))?$grand_total:0?></th>
                                    <th class="text-left"></th>
                                </tr>
                            </tfoot>
                        <?php endif ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>