<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
          <li><a href="<?= $this->config->item('base_url') . 'user-panel/vehicle-list'; ?>"><?= $this->lang->line('vehicle'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('edit_vehicles'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-truck fa-2x text-muted"></i> Veh<?= $this->lang->line('vehicles'); ?>icles </h2>
      <small class="m-t-md"><?= $this->lang->line('edit_vehicle_details'); ?></small>    
    </div>
  </div>
</div>
    
<div class="content">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
      <div class="hpanel">
        <div class="panel-body">
          <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
          <?php endif; ?>
          <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
          <?php endif; ?>
          <!-- <?php echo json_encode($transport_details); ?> -->
          <form method="post" class="form-horizontal" action="vehicle-edit-details" id="vehicleEdit">
            <input type="hidden" name="transport_id" value="<?= $transport_details['transport_id'] ?>">
            <div class="form-group">
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="">
                  <label class="text-left"><?= $this->lang->line('transport_mode'); ?></label>
                  <div class="">
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="sea" value="sea" name="mode" aria-label="Sea" <?php if(strtolower($transport_details['transport_mode']) == 'sea') { echo 'checked'; } ?> >
                      <label for="Sea"> <?= $this->lang->line('sea'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="earth" value="earth" name="mode" aria-label="Earth" <?php if(strtolower($transport_details['transport_mode']) == 'earth') { echo 'checked'; } ?>>
                      <label for="Earth"> <?= $this->lang->line('earth'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="air" value="air" name="mode" aria-label="Air" <?php if(strtolower($transport_details['transport_mode']) == 'air') { echo 'checked'; } ?>>
                      <label for="Air"> <?= $this->lang->line('air'); ?> </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <label class="text-left">By</label>
                <div class="">
                  <select class="js-source-states" style="width: 100%" name="vehical_type_id" id="vehical_type_id">
                    <option value=""> <?= $this->lang->line('select_mode_type_first'); ?> 
                    <?php foreach ($transport as $trn) { ?>
                      <option value="<?= $trn['vehical_type_id'] ?>" <?php if($trn['vehical_type_id'] == $transport_details['vehical_type_id']) { echo 'selected'; } ?>><?= $trn['vehicle_type'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <label class="text-left"><?= $this->lang->line('registration_number'); ?></label>
                <div class="">
                  <input type="text" placeholder="<?= $this->lang->line('registration_number'); ?>" class="form-control m-b" name="registration_no" id="registration_no" value="<?= $transport_details['registration_no'] ?>">
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <label class="text-left"><?= $this->lang->line('max_volume_cm3'); ?></label>
                <div class="">
                  <input type="text" placeholder="<?= $this->lang->line('max_volume_cm3'); ?>" class="form-control m-b" name="max_volume" id="max_volume" value="<?= $transport_details['max_volume'] ?>">
                </div>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3">
                <label class="text-left"><?= $this->lang->line('max_weight'); ?></label>
                <div class="">
                  <input type="text" placeholder="<?= $this->lang->line('max_weight'); ?>" class="form-control m-b" name="max_weight" id="max_weight" value="<?= $transport_details['max_weight'] ?>">
                </div>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3">
                <label class="text-left"><?= $this->lang->line('weight_unit'); ?></label>
                <select class="js-source-states" style="width: 100%" name="unit_id">
                  <?php foreach ($unit as $unt) { ?>
                    <option value="<?= $unt['unit_id'] ?>" <?php if($unt['unit_id'] == $transport_details['unit_id']) { echo 'selected'; } ?>><?= $unt['unit_type'] . ' [' . $unt['shortname'] . ']' ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <label class="text-left"><?= $this->lang->line('mark'); ?></label>
                <div class="">
                  <input type="text" placeholder="<?= $this->lang->line('mark'); ?>" class="form-control m-b" name="mark" id="mark" value="<?= $transport_details['mark'] ?>">
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <label class="text-left"><?= $this->lang->line('model'); ?></label>
                <div class="">
                  <input type="text" placeholder="<?= $this->lang->line('model'); ?>" class="form-control m-b" name="model" id="model" value="<?= $transport_details['model'] ?>">
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <label class="text-left"><?= $this->lang->line('symbolic_name'); ?></label>
                <div class="">
                  <input type="text" placeholder="<?= $this->lang->line('symbolic_name'); ?>" class="form-control m-b" name="symbolic_name" id="symbolic_name" value="<?= $transport_details['symbolic_name'] ?>">
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <br />
                <button class="btn btn-primary" type="submit"><?= $this->lang->line('save_details'); ?></button>
                <a href="<?= $this->config->item('base_url') . 'user-panel/vehicle-list'; ?>" class="btn btn-info" type="submit"><?= $this->lang->line('back'); ?></a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#vehicleEdit")
  .validate({
    ignore: [], 
    rules: {
      vehical_type_id: { required : true },
      registration_no: { required : true },
      max_volume: { required : true, number : true },
      max_weight: { required : true, number : true },
      unit_id: { required : true },
      mark: { required : true },
      model: { required : true },
      symbolic_name: { required : true },
    },
    messages: {
      vehical_type_id: { required : <?= json_encode($this->lang->line('select_vehicle'));?>, },
      registration_no: { required : <?= json_encode($this->lang->line('registration_number'));?>, },                    
      max_volume: { 
        required :  <?= json_encode($this->lang->line('max_volume'));?>,
        number : <?= json_encode($this->lang->line('number_only'));?>,
      },
      max_weight: { 
        required :  <?= json_encode($this->lang->line('max_weight'));?>,
        number : <?= json_encode($this->lang->line('number_only'));?>,
      },
      unit_id: { required : <?= json_encode($this->lang->line('select_unit'));?>, },
      mark: { required : <?= json_encode($this->lang->line('mark'));?>, },
      model: { required : <?= json_encode($this->lang->line('model'));?>, },
      symbolic_name: { required : <?= json_encode($this->lang->line('symbolic_name'));?>, },
    },
  });

  $("input[name=mode]").on("change",function(){
    var v = $("input[name=mode]:checked").val();
    $('#vehicle').empty();

    $.ajax({
      type: "POST", 
      url: "get-transport", 
      data: { type: v },
      dataType: "json",
      success: function(res){ 
      
        $('#vehical_type_id').attr('disabled', false);
        $('#vehical_type_id').empty(); 
        $('#vehical_type_id').append('<option value=""><?= json_encode($this->lang->line('select_mode_type')); ?></option>');
        $.each( res, function(i, v){
          $('#vehical_type_id').append('<option value="'+v['vehical_type_id']+'">'+v['vehicle_type']+'</option>');
        });
        $('#vehical_type_id').focus();
      },
      beforeSend: function(){
        $('#vehical_type_id').empty();
        $('#vehical_type_id').append('<option value=""><?= json_encode($this->lang->line('loading')); ?>,</option>');
      },
      error: function(){
        $('#vehical_type_id').attr('disabled', true);
        $('#vehical_type_id').empty();
        $('#vehical_type_id').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
      }
    });
  });

  var v = $("input[name=mode]:checked").val();

  $.ajax({
    type: "POST", 
    url: "get-transport", 
    data: { type: v },
    dataType: "json",
    success: function(res){ 
    
      $('#vehical_type_id').attr('disabled', false);
      $('#vehical_type_id').empty(); 
      $('#vehical_type_id').append('<option value=""><?= json_encode($this->lang->line('select_mode_type')); ?></option>');
      $('#vehical_type_id').trigger("change");
      $.each( res, function(i, v){
        $('#vehical_type_id').append('<option value="'+v['vehical_type_id']+'">'+v['vehicle_type']+'</option>');
      });
      $('#vehical_type_id').focus();
    },
    
    error: function(){
      $('#vehical_type_id').attr('disabled', true);
      $('#vehical_type_id').empty();
      $('#vehical_type_id').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
    }
  });
</script>