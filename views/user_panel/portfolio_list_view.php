<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('profile'); ?></span></li>
          <li><span><?= $this->lang->line('portfolio'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('list'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-photo fa-2x text-muted"></i> <?= $this->lang->line('portfolio'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('list_of_added_portfolios'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <!-- Portfolio Area -->
  <div class="row">
    <div class="hpanel hblue">
      <div class="panel-heading">
        <div class="row">
          <div class="col-lg-6">
            <h4>
              <strong> <?= $this->lang->line('portfolio'); ?> </strong> &nbsp;
              <a href="<?= base_url('user-panel/portfolio/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
            </h4>
          </div>
          <div class="col-lg-6 text-right">
            <a href="<?= base_url('user-panel/user-profile'); ?>" class="btn btn-primary"><i class="fa fa-left-arrow"></i> <?= $this->lang->line('go_to_profile'); ?></a> 
          </div>
        </div>
      </div>
      
      <?php foreach($portfolios as $portfolio ): ?>
        <div class="panel-body">
          
          <div class="row">
            <div class="col-md-3"> <p><strong><?= $this->lang->line('title'); ?></strong></p> </div>
            <div class="col-md-7">  <p style="word-wrap: break-word;"><?= $portfolio['title']; ?></p> </div>  
            <div class="col-md-2 text-right">
              <a class="btn btn-default btn-circle" href="<?= base_url('user-panel/portfolio/').$portfolio['portfolio_id'];?>">
                <i class="fa fa-pencil"></i>
              </a>
              <a class="btn btn-danger btn-circle btn-delete" id="<?=$portfolio['portfolio_id'];?>">
                <i class="fa fa-trash"></i>
              </a>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3"> <p><strong><?= $this->lang->line('category'); ?></strong></p> </div>
            <div class="col-md-9">  <p style="word-wrap: break-word;"><?= $this->user->get_category_name_by_id($portfolio['cat_id']); ?></p> </div>  
          </div>

          <div class="row">
            <div class="col-md-3"> <p><strong><?= $this->lang->line('sub_categories'); ?></strong></p> </div>
            <div class="col-md-9">  <p style="word-wrap: break-word;">
              <?php if($portfolio['subcat_ids'] != "NULL") { 
                $subcat_ids = explode(',', $portfolio['subcat_ids']); 
                  foreach($subcat_ids as $subid){ $subcats [] = $this->user->get_category_name_by_id($subid); }
                  echo implode(', ', $subcats);
                } else { echo  $this->lang->line('not_selected'); }
              ?>
              </p> 
            </div>  
          </div>

          <div class="row">
            <div class="col-md-3"> <p><strong><?= $this->lang->line('desc'); ?></strong></p> </div>
            <div class="col-md-9">  <p style="word-wrap: break-word;">
              <?= ($portfolio['description'] != "NULL") ? nl2br($portfolio['description']) : $this->lang->line('not_defined'); ?>
              </p></div>  
          </div>

          <div class="row">
            <div class="col-md-3"> <p><strong><?= $this->lang->line('attachment'); ?></strong></p> </div>
            <div class="col-md-9">  
              <p style="word-wrap: break-word;">
              <?php if($portfolio['attachement_url'] != "NULL"): ?>
                <img src="<?= base_url($portfolio['attachement_url']); ?>" class="img-responsive" style="width: 200px;">
              <?php else: ?>
                 <?= $this->lang->line('portfolio'); ?>;
              <?php endif; ?>
              </p>
            </div>  
          </div>
          <div class="row hidden">
            <div class="col-md-3"> <p><strong><?= $this->lang->line('tags'); ?>:</strong></p> </div>
            <div class="col-md-9">  <p style="word-wrap: break-word;">
              <?= ($portfolio['tags'] != "NULL") ? $portfolio['tags'] : $this->lang->line('portfolio'); ?>
              </p> </div>  
          </div>                        
        </div> 
        <br/>
      <?php endforeach; ?> 
    </div>
  </div>
  <!--End: Portfolio Area -->
</div>

<script>
  $('.btn-delete').click(function () {            
    var id = this.id;            
    swal({                
      title: <?= json_encode($this->lang->line('are_you_sure'))?>,                
      text: "",                
      type: "warning",                
      showCancelButton: true,                
      confirmButtonColor: "#DD6B55",                
      confirmButtonText:<?= json_encode($this->lang->line('yes'))?>,                
      cancelButtonText: <?= json_encode($this->lang->line('no'))?>,                
      closeOnConfirm: false,                
      closeOnCancel: false, 
    },                
    function (isConfirm) {                    
      if (isConfirm) {                        
        $.post('delete-portfolio', {id: id}, 
        function(res){
          if($.trim(res) == "success") {
            swal(<?= json_encode($this->lang->line('deleted'))?>, "", "success");                            
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal(<?= json_encode($this->lang->line('failed'))?>, "", "error");  }  
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");  }  
    });            
  });
</script>