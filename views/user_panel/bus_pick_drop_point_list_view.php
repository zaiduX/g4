    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Pickup Drop Locations'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Pickup Drop Locations'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-pick-drop-location-and-point-add'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i> <?= $this->lang->line('Add New Location'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('List of Pickup Drop Locations'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th><?= $this->lang->line('Location ID'); ?></th>
                                <th><?= $this->lang->line('Location/City Name'); ?></th>
                                <th><?= $this->lang->line('state'); ?></th>
                                <th><?= $this->lang->line('Country'); ?></th>
                                <th><?= $this->lang->line('Vehicle Type'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($locations as $location) { ?>
                            <tr>
                                <td><?= $location['loc_id'] ?></td>
                                <td><?= ucwords($location['loc_city_name']) ?></td>
                                <td><?= ucwords($location['loc_state_name']) ?></td>
                                <td><?= ucwords($location['loc_country_name']) ?></td>
                                <td><?= strtoupper($this->api->get_vehicle_type_by_id($location['vehical_type_id'])['vehicle_type']) ?></td>
                                <td style="display: flex;">
                                    <form action="<?= base_url('user-panel-bus/bus-pick-drop-point-list-view') ?>" method="post">
                                        <input type="hidden" name="loc_id" value="<?= $location['loc_id'] ?>">
                                        <input type="hidden" name="loc_city_name" value="<?= $location['loc_city_name'] ?>">
                                        <button class="btn btn-info btn-sm" type="submit" title="<?= $this->lang->line('View Pickup Drop Points'); ?>" <?php if($location['status'] == 0) { echo 'disabled'; } ?> ><i class="fa fa-eye"></i> <?= $this->lang->line('Pickup Drop Points'); ?></button>
                                    </form> &nbsp;
                                    <?php if($location['status'] == 1) { ?>
                                        <button class="btn btn-danger btn-sm busLocationInactivateAlert" id="<?= $location['loc_id'] ?>" title="<?= $this->lang->line('Inactivate Location'); ?>"><i class="fa fa-times"></i> <?= $this->lang->line('Inactivate Location'); ?></button>
                                    <?php } else { ?>
                                        <button class="btn btn-warning btn-sm busLocationActivateAlert" id="<?= $location['loc_id'] ?>" title="<?= $this->lang->line('Activate Location'); ?>"><i class="fa fa-check"></i> <?= $this->lang->line('Activate Location'); ?></button>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.busLocationInactivateAlert').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('your_record_will_be_inactivated'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_inactivate_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?=base_url('user-panel-bus/bus-location-inactivate')?>", {id: id}, function(res){
                            swal("<?= $this->lang->line('inactivated'); ?>", "<?= $this->lang->line('record_has_been_inactivated'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error");
                    }
                });
            });
    </script>

    <script>
        $('.busLocationActivateAlert').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('your_record_will_be_activated'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_activate_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?=base_url('user-panel-bus/bus-location-activate')?>", {id: id}, function(res){
                            swal("<?= $this->lang->line('activated'); ?>", "<?= $this->lang->line('record_has_been_activated'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error");
                    }
                });
            });
    </script>