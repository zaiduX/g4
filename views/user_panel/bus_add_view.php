    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-list'; ?>"><?= $this->lang->line('Vehicles'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Add Vehicle Details'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light-xs">  <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Vehicles'); ?> </h2>
          <small class="m-t-md"><?= $this->lang->line('Add Vehicle Details'); ?></small>    
        </div>
      </div>
    </div>
     
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">

                        <?php if($this->session->flashdata('error')):  ?>
                        <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                        <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>

                        <form method="post" class="form-horizontal" action="bus-add-details" id="busAdd" enctype="multipart/form-data">
                            
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="text-left"><?= $this->lang->line('Select Vehicle Type'); ?></label>
                                    <select class="form-control js-source-states" name="vehical_type_id" id="vehical_type_id">
                                        <option value=""><?= $this->lang->line('Select Vehicle Type'); ?></option>
                                        <?php foreach ($vehicle_types as $type) { ?>
                                            <option value="<?= $type['vehical_type_id'] ?>"><?= $type['vehicle_type'] ?></option>
                                        <?php } ?>
                                    </select><br />

                                    <div id="seatTypes">
                                        
                                    </div>


                                </div>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('bus_make'); ?></label>
                                            <div class="">
                                                <input type="text" placeholder="<?= $this->lang->line('bus_make'); ?>" class="form-control" name="bus_make" id="bus_make">
                                            </div><br />
                                        </div>
                                        <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('bus_modal'); ?></label>
                                            <div class="">
                                                <input type="text" placeholder="<?= $this->lang->line('bus_modal'); ?>" class="form-control" name="bus_modal" id="bus_modal">
                                            </div><br />
                                        </div>
                                        <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('Name/No'); ?></label>
                                            <div class="">
                                                <input type="text" placeholder="<?= $this->lang->line('Name/No'); ?>" class="form-control" name="bus_no" id="bus_no">
                                            </div>
                                        </div><br />
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('Image'); ?></label>
                                            <div class="">
                                                <input type="file" class="form-control" name="bus_image_url" id="bus_image_url">
                                            </div><br />
                                        </div>
                                        <div class="col-sm-4"><label class="text-left"><?= $this->lang->line('Icon'); ?></label>
                                            <div class="">
                                                <input type="file" class="form-control" name="bus_icon_url" id="bus_icon_url">
                                            </div><br />
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="text-left"><?= $this->lang->line('bus_seat_type'); ?></label>
                                            <div class="">
                                                <select class="js-source-states" style="width: 100%" name="bus_seat_type" id="bus_seat_type">
                                                    <option value=""><?= $this->lang->line('Select seat type'); ?></option>
                                                    <option value="SEATER">SEATER</option>
                                                    <option value="SLEEPER">SLEEPER</option>
                                                    <option value="SEMI-SLEEPER">SEMI-SLEEPER</option>
                                                </select>
                                            </div><br />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="text-left"><?= $this->lang->line('bus_ac_type'); ?></label>
                                            <div class="">
                                                <select class="js-source-states" style="width: 100%" name="bus_ac" id="bus_ac">
                                                    <option value=""><?= $this->lang->line('Select A.C. type'); ?></option>
                                                    <option value="AC">AC</option>
                                                    <option value="NON AC">NON AC</option>
                                                </select>
                                            </div><br />
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="text-left"><?= $this->lang->line('bus_amenities'); ?></label>
                                            <div class="">
                                                <select class="js-source-states" style="width: 100%" name="bus_amenities[]" id="bus_amenities" multiple="multiple">
                                                    <?php foreach ($amenities as $amenity) { ?>
                                                        <option value="<?= $amenity['am_title'] ?>"><?= $amenity['am_title'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div><br />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <br>
                                    <button class="btn btn-primary" type="submit" id="create_vehicle" name="create_vehicle" value="1"><?= $this->lang->line('Create Vehicle'); ?></button>
                                    <button class="btn btn-primary hidden" type="submit" id="manage_seat_layout" name="manage_seat_layout" value="1"><?= $this->lang->line('Configure VIP seats'); ?></button>
                                    <a href="<?=base_url('user-panel-bus/bus-list')?>" class="btn btn-info"><?= $this->lang->line('back'); ?></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $("#busAdd").validate({
        ignore: [], 
        rules: {
            bus_make: { required : true },
            bus_modal: { required : true },
            bus_image_url: { required : true },
            bus_icon_url: { required : true },
            bus_seat_type: { required : true },
            bus_ac: { required : true },
            bus_no: { required : true },
        },
        messages: {
            bus_make: { required : <?= json_encode($this->lang->line('Enter make year!'));?>, },
            bus_modal: { required : <?= json_encode($this->lang->line('Enter modal!'));?>, }, 
            bus_image_url: { required :  <?= json_encode($this->lang->line('Select image!'));?>, },
            bus_icon_url: { required :  <?= json_encode($this->lang->line('Select icon!'));?>, },
            bus_seat_type: { required : <?= json_encode($this->lang->line('Select seat type!'));?>, },
            bus_ac: { required : <?= json_encode($this->lang->line('Select A.C. type!'));?>, },
            bus_no: { required : <?= json_encode($this->lang->line('Enter Name/No!'));?>, },
        },
    });
</script>
<script>
    $('#vehical_type_id').on('change', function() {
        var vehical_type_id = this.value;
        $.ajax({
            url: "<?=base_url('user-panel-bus/get-vehicle-seat-type')?>",
            type: "POST",
            data: { id: vehical_type_id },
            dataType: "json",
            success: function(res){
                //console.log(res);
                $.each( res, function(){ $('#seatTypes').append('<label class="text-left"><?= $this->lang->line('Number of'); ?> '+$(this).attr('st_name')+' <?= $this->lang->line('Seats'); ?></label>'+
                                        '<input type="hidden" name="seat_type[]" value="'+$(this).attr('st_name')+'">'+
                                        '<input type="number" placeholder="<?= $this->lang->line('Enter number of seats'); ?>" class="form-control bus'+$(this).attr('st_name')+'" name="seat_type_count[]" id="seat_type_count" value="0" min="0" required><br />');

                                        if($(this).attr('st_name') === 'VIP') {
                                            $(".bus"+$(this).attr('st_name')).bind('keyup change click', function (e) {
                                                var vip_seat_count = $(this).val();
                                                if(vip_seat_count > 0) {
                                                    $('#create_vehicle').addClass('hidden');
                                                    $('#manage_seat_layout').removeClass('hidden');
                                                } else {
                                                    $('#manage_seat_layout').addClass('hidden');
                                                    $('#create_vehicle').removeClass('hidden');
                                                }
                                            });
                                        }
                });
            },
            beforeSend: function(){
                $('#seatTypes').empty();
            },
            error: function(){
                $('#seatTypes').empty();
            }
        });
    })
</script>