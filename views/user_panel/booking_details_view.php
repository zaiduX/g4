    <style>

    <!-- Progress with steps -->

    ol.progtrckr {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }

    ol.progtrckr li {
        display: inline-block;
        text-align: center;
        line-height: 3em;
    }

    ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
    ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
    ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
    ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
    ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
    ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
    ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
    ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

    ol.progtrckr li.progtrckr-done {
        color: black;
        border-bottom: 4px solid yellowgreen;
    }
    ol.progtrckr li.progtrckr-todo {
        color: silver; 
        border-bottom: 4px solid silver;
    }

    ol.progtrckr li:after {
        content: "\00a0\00a0";
    }
    ol.progtrckr li:before {
        position: relative;
        bottom: -2.5em;
        float: left;
        left: 50%;
        line-height: 1em;
    }
    ol.progtrckr li.progtrckr-done:before {
        content: "\2713";
        color: white;
        background-color: yellowgreen;
        height: 1.2em;
        width: 1.2em;
        line-height: 1.2em;
        border: none;
        border-radius: 1.2em;
    }
    ol.progtrckr li.progtrckr-todo:before {
        content: "\039F";
        color: silver;
        background-color: white;
        font-size: 1.5em;
        bottom: -1.6em;
    }

</style>
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>">Dashboard</a></li>
                        <li class="active"><span>Order Details</span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Order Details &nbsp;&nbsp;&nbsp;
                </h2>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel filter-item">
                            <div class="panel-body">
                                <div class="pull-right text-right">
                                    <small class="stat-label">Ref# G-015</small>
                                </div>
                                <h4 class="m-b-xs"><strong>FROM:</strong> From Address</h4>
                                <h4 class="m-b-xs"><strong>TO:</strong> To Address</h4>
                                <p class="small">
                                    <div class="col-md-6"><strong>Weight:</strong> 250 KG</div>
                                    <div class="col-md-6"><strong>Price:</strong> $ 300</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Advance:</strong> $ 200</div>
                                    <div class="col-md-6"><strong>Balance:</strong> $ 100</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>By:</strong> Avion</div>
                                    <div class="col-md-6"><strong>Dimension:</strong> Wallet, book (document)</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Pickup Date:</strong> 23 Sept. 2017 02:05:00 PM</div>
                                    <div class="col-md-6"><strong> Delivery Date:</strong> 28 Sept. 2017 02:05:00 PM</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Shipping Type:</strong> Shipping Only</div>
                                    <div class="col-md-6"><strong>Expiration Date:</strong> 28 Sept. 2017 02:05:00 PM</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Reciever Company Name:</strong> XYZ Pvt. Ltd</div>
                                    <div class="col-md-6"><strong>Reciever Name:</strong> Mr. Tabani</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-12"><strong>Description:</strong> This is courier description. This is courier description. This is courier description. This is courier description. This is courier description. This is courier description. </div>
                                </p>
                                <p class="small">
                                    <div class="col-md-12"><strong>Pick-up Instructions:</strong> This is pickup instructions. This is pickup instructions. This is pickup instructions. This is pickup instructions. This is pickup instructions. This is pickup instructions. </div>
                                </p>
                                <p class="small">
                                    <div class="col-md-12"><strong>Delivere Instructions:</strong> This is delivere instructions. This is delivere instructions. This is delivere instructions. This is delivere instructions. This is delivere instructions. This is delivere instructions. </div>
                                </p>
                                <p class="small">
                                    <div class="col-md-12"><strong>Ref. No.:</strong> 123456789ASDC123</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Assigned Driver:</strong> Mr. XYZ</div>
                                </p>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-md-12 form-group    ">
                                        <ol class="progtrckr" data-progtrckr-steps="4">
                                            <li class="progtrckr-done">Active</li>
                                            <li class="progtrckr-todo">Driver Accepted</li>
                                            <li class="progtrckr-todo">Pickup Done</li>
                                            <li class="progtrckr-todo">Delivered</li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center form-group    ">
                                        <a href="<?= $this->config->item('base_url') . 'user-panel/chat-room'; ?>" style="display: inline-block;" class="btn btn-outline btn-success" title="Chat with Driver"><i class="fa fa-comments"></i> Chat with Driver</a>
                                        <a href="<?= $this->config->item('base_url') . 'user-panel/courier-workroom'; ?>" style="display: inline-block;" class="btn btn-outline btn-success" title="Go to Workroom"><i class="fa fa-briefcase"></i> Go to Workroom</a>
                                        <button class="btn btn-outline btn-success" data-toggle="modal" data-target="#assign_driver" data-dismiss="modal"><i class="fa fa-plus"></i> Assign Driver </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Assign Driver-->
    <div class="modal fade hmodal-info in" id="assign_driver" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Assign Driver</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <dir class="col-md-12">
                            <label class="">Select Driver</label>
                            <select class="js-source-states" style="width: 100%" name="driver_id">
                                <option value="1">Driver 1</option>
                                <option value="2">Driver 2</option>
                                <option value="3">Driver 3</option>
                                <option value="4">Driver 4</option>
                            </select>
                        </dir>
                        <dir class="col-md-12 text-right">
                            <button class="btn btn-success" title="Assign Driver"><i class="fa fa-briefcase"></i> Assign Driver</button>
                        </dir>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>