    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/driver-list'; ?>"><span><?= $this->lang->line('Drivers'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('inactive_drivers'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('inactive_drivers'); ?> &nbsp;&nbsp;&nbsp;
          <a href="<?= $this->config->item('base_url') . 'user-panel/driver-list'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-check"></i> <?= $this->lang->line('active_drivers_list'); ?> </a></h2>
          <small class="m-t-md"><?= $this->lang->line('inactive_drivers_list'); ?></small> 
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?= $this->lang->line('driver_id'); ?></th>
                                    <th><?= $this->lang->line('driver_name'); ?></th>
                                    <th><?= $this->lang->line('email_address'); ?></th>
                                    <th><?= $this->lang->line('contact'); ?></th>
                                    <th><i class="fa fa-thumbs-up" title="<?= $this->lang->line('assigned_ord'); ?>"></i></th>
                                    <th><i class="fa fa-check" title="<?= $this->lang->line('accepted_ord'); ?>"></i></th>
                                    <th><i class="fa fa-recycle" title="<?= $this->lang->line('inprogress_ord'); ?>"></i></th>
                                    <th><i class="fa fa-times" title="<?= $this->lang->line('rejected_ord'); ?>"></i></th>
                                    <th><i class="fa fa-external-link-square" title="<?= $this->lang->line('pickup_ord'); ?>"></i></th>
                                    <th><i class="fa fa-check-square" title="<?= $this->lang->line('delivered_ord'); ?>"></i></th>
                                    <th><i class="fa fa-road" title="<?= $this->lang->line('inprogress_working_distance_ord'); ?>"></i></th>
                                    <th style="text-align: center;"><?= $this->lang->line('action'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($drivers as $dr) { ?>
                                <tr>
                                    <td>GD-<?= $dr['cd_id'] ?></td>
                                    <td><a href="<?= $this->config->item('base_url') . 'user-panel/view-driver/'.$dr['cd_id']; ?>" title="View details"><?= $dr['first_name'] . ' ' . $dr['last_name'] ?></a></td>
                                    <td><?= $dr['email'] ?></td>
                                    <td><?= $dr['mobile1'] ?></td>
                                    <td><?= $this->user->get_order_count($dr['cd_id'],'assign') ?></td>
                                    <td><?= $this->user->get_order_count($dr['cd_id'],'accept') ?></td>
                                    <td><?= $this->user->get_order_count($dr['cd_id'],'in_progress') ?></td>
                                    <td><?= $this->user->get_order_count($dr['cd_id'],'rejected') ?></td>
                                    <td><?= $this->user->get_order_count($dr['cd_id'],'in_progress') ?></td>
                                    <td><?= $this->user->get_order_count($dr['cd_id'],'delivered') ?></td>
                                    <td><?= $this->user->get_inprogress_working_distance($dr['cd_id']) ?></td>
                                    <td class="text-center">
                                        <a class="btn btn-warning btn-sm" href="<?= $this->config->item('base_url') . 'user-panel/view-driver/'.$dr['cd_id']; ?>"><i class="fa fa-search"></i> <?= $this->lang->line('view'); ?></a>
                                        <button style="display: inline-block;" class="btn btn-info btn-sm driveractivatealert" id="<?= $dr['cd_id'] ?>"><i class="fa fa-check"></i> <span class="bold"><?= $this->lang->line('activate'); ?></span></button>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>