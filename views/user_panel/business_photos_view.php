    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li class="active"><span><?= $this->lang->line('business_photos'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-picture-o fa-2x text-muted"></i> <?= $this->lang->line('business_photos'); ?> </h2>
          <small class="m-t-md"><?= $this->lang->line('busi_photos'); ?></small>    
        </div>
      </div>
    </div>
    
  <div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <?= $this->lang->line('up_photos'); ?>
                </div>
                <div class="panel-body">
                  <form action="business-photos-upload" enctype="multipart/form-data" method="POST" id="uploadPhoto">
                      <p>
                          <div class="col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                    <span class="glyphicon glyphicon-remove"></span> <?= $this->lang->line('clear'); ?>
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                    <span class="glyphicon glyphicon-folder-open"></span>
                                    <span class="image-preview-input-title"><?= $this->lang->line('browse'); ?></span>
                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="business_photo" id="business_photo"  /> <!-- rename it -->

                                </div>
                                <button type="submit" class="btn btn-outline btn-success" id="upload"><?= $this->lang->line('btn_upload'); ?></button>
                            </span>
                        </div><!-- /input-group image-preview [TO HERE]--> 
                        <span id="errorMsg" class="text-danger hidden"><strong><?= $this->lang->line('error'); ?></strong> <?= $this->lang->line('please'); ?></span>
                    </div>
                      </p>
                  </form>
                </div>
            </div>
        </div>
    </div>
      <div class="row">
          <div class="col-lg-12">
              <div class="hpanel">
                  <div class="panel-body">
                      <div class="lightBoxGallery">
                        <?php $i=0; foreach ($photos as $pic) { $i++; ?>
                          <div style="display: inline-block; position: relative;">
                              <a href="<?= $this->config->item('base_url') . $pic['photo_url']; ?>" title="Business Photo" data-gallery=""><img style="max-width: 240px; height: 240px;" src="<?= $this->config->item('base_url') . $pic['photo_url']; ?>"></a>
                              <button style="position: absolute; right: 10px; top: 10px;" title="Delete" class="btn btn-danger btn-circle photodeletealert" type="button" id="<?= $pic['photo_id'] ?>"><i class="fa fa-trash"></i></button>
                          </div>
                        <?php } ?>
                      </div>
                  </div>
                  <div class="panel-footer">
                      <i class="fa fa-picture-o"> </i> <?= $i ?> <?= $this->lang->line('photo'); ?>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <script type="text/javascript">
    $("#uploadPhoto").submit(function(e){ e.preventDefault(); });
    $("#upload").on('click', function(e) { e.preventDefault(); 
      var file = $('#business_photo').val();
      if(file !="") { $("#uploadPhoto").get(0).submit();  } 
      else { $('#errorMsg').removeClass('hidden'); }
    });
  </script>