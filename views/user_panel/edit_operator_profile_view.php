<style type="text/css">
  #attachment, .attachment {            position: absolute;            top: 0;            right: 0;            margin: 0;            padding: 0;            font-size: 20px;            cursor: pointer;            opacity: 0;            filter: alpha(opacity=0);        }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-bus/bus-operator-profile'); ?>"><span><?= $this->lang->line('operator_profile'); ?></span></a></li>
          <li class="active"><span><?= $this->lang->line('edit_education'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-building fa-2x text-muted"></i> <?= $this->lang->line('operator_profile'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('edit_details'); ?></small>    
    </div>
  </div>
</div>


<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <div class="panel-body">

          <form action="<?= base_url('user-panel-bus/update-operator-profile'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="updateProfile">
            
            <?php if($this->session->flashdata('error')):  ?>
            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

            <input type="hidden" name="deliverer_id" value="<?= $operator['deliverer_id'] ?>">
            <div class="row form-group">
              <div class="col-md-6">
                  <label class=""><?= $this->lang->line('profile_image'); ?></label>
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button id="upload_avatar" class="btn btn-green"  ><i class="fa fa-user"></i>&nbsp; <?= $this->lang->line('select_avatar'); ?></button>
                    </span>
                  </div>
                  <span id="avatar_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('avatar_attached'); ?></span>
                  <input type="file" id="avatar"  name="avatar" class="upload attachment" accept="image/*" onchange="avatar_name(event)" />
              </div>
              <div class="col-md-6">
                  <label class=""><?= $this->lang->line('cover_image'); ?></label>
                  <div class="input-group">
                    <span class="input-group-btn">
                      <button id="upload_cover" class="btn btn-green"><i class="fa fa-photo"></i>&nbsp; <?= $this->lang->line('select_cover'); ?></button>
                    </span>                      
                  </div>
                  <span id="cover_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('cover_attached'); ?></span>
                  <input type="file" id="cover" name="cover" class="upload attachment" accept="image/*" onchange="cover_name(event)" />
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-6">
                  <label class=""><?= $this->lang->line('first_name'); ?></label>
                  <input type="text" class="form-control" id="firstname" placeholder="First Name" value="<?= $operator['firstname'] ?>" name="firstname" >
              </div>
              <div class="col-md-6">
                  <label class=""><?= $this->lang->line('last_name'); ?></label>
                  <input type="text" class="form-control" id="lastname" placeholder="Last Name" value="<?= $operator['lastname'] ?>" name="lastname" >
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-6">
                <label class=""><?= $this->lang->line('company_name'); ?></label>
                <input type="text" class="form-control" id="company_name" placeholder="Company Name" value="<?= $operator['company_name'] ?>" name="company_name" >
              </div>
              <div class="col-md-6">
                <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                <input type="text" class="form-control" id="contact_no" placeholder="Mobile Number" value="<?= $operator['contact_no'] ?>" name="contact_no" >
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-6">
                <label class=""><?= $this->lang->line('email'); ?></label>
                <input type="text" class="form-control" id="email_id" placeholder="Email Address" value="<?= $operator['email_id'] ?>" name="email_id">
              </div>
              <div class="col-md-6">
                <label class=""><?= $this->lang->line('promotion_code'); ?></label>
                <input type="text" class="form-control" id="promocode" placeholder="Promotion Code" value="<?php if($operator['promocode'] != 'NULL') echo $operator['promocode']; ?>" name="promocode">
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                <label class=""><?= $this->lang->line('introduction'); ?></label>
                <textarea placeholder="<?= $this->lang->line('introduction'); ?>" class="form-control" id="introduction" name="introduction"><?= $operator['introduction'] ?></textarea>
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-6">
                <label class="control-label"><?= $this->lang->line('carrier_type'); ?></label>
                <select id="carrier_type" name="carrier_type" class="form-control select2" data-allow-clear="true">
                  <option value=""><?= $this->lang->line('sel_c_type'); ?></option>
                  <?php foreach ($carrier_type as $ctype): ?>
                  	<option value="<?= strtolower($ctype['carrier_title']); ?>" <?php if( strtolower($ctype['carrier_title']) == strtolower($operator['carrier_type']) ) { echo 'selected'; } ?> ><?= $ctype['carrier_title']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-md-6">
                <label class="control-label"><?= $this->lang->line('gender'); ?></label>
                <div class="radio radio-success radio-inline">
                  <input type="radio" id="genderMale" value="m" name="gender" aria-label="Local" <?php if($operator['gender']=='m') { echo 'checked'; } ?> >
                  <label for="m"> <?= $this->lang->line('male'); ?> </label>
                </div>
                <div class="radio radio-success radio-inline">
                  <input type="radio" id="genderFemale" value="f" name="gender" aria-label="National" <?php if($operator['gender']=='f') { echo 'checked'; } ?> >
                  <label for="f"> <?= $this->lang->line('female'); ?> </label>
                </div>
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-6">
                <label class="control-label"><?= $this->lang->line('country'); ?></label>
                <select id="country_id" name="country_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Country">
                  <option value=""><?= $this->lang->line('select_country'); ?></option>
                  <?php foreach ($countries as $country): ?>
                      <option value="<?= $country['country_id'] ?>" <?php if($operator['country_id'] == $country['country_id']) { echo "selected"; } ?>><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-md-6">
              <label class="control-label"><?= $this->lang->line('state'); ?></label>
                <input type="hidden" name="state_id_hidden" value="<?= $operator['state_id'] ?>">
                <select id="state_id" name="state_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select State" disabled>
                    <option value=""><?php if($operator['state_id']!=0) { echo $this->user->get_state_name_by_id($operator['state_id']); } ?></option>
                </select>
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-6">
                <label class="control-label"><?= $this->lang->line('city'); ?></label>
                <input type="hidden" name="city_id_hidden" value="<?= $operator['city_id'] ?>">
                <select id="city_id" name="city_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select City" disabled>
                  <option value=""><?php if($operator['city_id']!=0) { echo $this->user->get_city_name_by_id($operator['city_id']); } ?></option>
                </select>
              </div>
              <div class="col-md-6">
                <label class=""><?= $this->lang->line('zip'); ?></label>
                <input type="text" class="form-control" id="zipcode" placeholder="Zip" value="<?= $operator['zipcode'] ?>" name="zipcode">
              </div>
            </div>
            <?php $services = explode(',', $operator['service_list']);?>
            <div class="row form-group hidden">
              <div class="col-md-12">
                <label class="control-label"><?= $this->lang->line('service_list'); ?> </label><br/>
                <label class="checkbox-inline"> 
                  <input type="checkbox" class="i-checks" name="service_list[]" value="Courier" <?= (in_array('Courier',$services))?"checked":"";?>> &nbsp; <?= $this->lang->line('courier'); ?>
                </label>
                <label class="checkbox-inline"> 
                  <input type="checkbox" class="i-checks" name="service_list[]" value="Transport" <?= (in_array('Transport',$services))?"checked":"";?>> &nbsp; <?= $this->lang->line('transport'); ?> 
                </label>
                <label class="checkbox-inline hidden"> 
                  <input type="checkbox" class="i-checks" name="service_list[]" value="Laundary" <?= (in_array('Laundary',$services))?"checked":"";?>> &nbsp; <?= $this->lang->line('laundary'); ?> 
                </label>
                <label class="checkbox-inline hidden"> 
                  <input type="checkbox" class="i-checks" name="service_list[]" value="Resturant" <?= (in_array('Resturant',$services))?"checked":"";?>> &nbsp; <?= $this->lang->line('resturant'); ?> 
                </label>
                <label class="checkbox-inline hidden"> 
                  <input type="checkbox" class="i-checks" name="service_list[]" value="Domestic Gas" <?= (in_array('Domestic Gas',$services))?"checked":"";?>> &nbsp; <?= $this->lang->line('domestic_gas'); ?> 
                </label>
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                <label>Address</label>
                <input type="text" class="form-control" id="us3-address" name="address" required="required" />
              </div>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                <div id="us3" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
            <div class="row form-group hidden">
              <label class="col-sm-2 control-label">Radius:</label>
              <input type="text" class="form-control" id="us3-radius" />
            </div>
            <div class="row col-md-6 hidden">
              <div class="form-group">
                  <label class="control-label">Latitude </label>
                  <input type="text" class="form-control" id="us3-lat" name="latitude" />
              </div>
            </div>
            <div class="row col-md-6 hidden">
              <div class="form-group">
                  <label class="control-label">Longitude </label>
                  <input type="text" class="form-control" id="us3-lon" name="longitude" />
              </div>
            </div>
        </div>
        <div class="panel-footer"> 
          <div class="row form-group">
             <div class="col-lg-12 text-left">
                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('save_details'); ?></button>               
                <a href="<?= base_url('user-panel-bus/bus-operator-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
             </div>
           </div>         
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
    $('#city_id').attr('disabled', true);

    $.ajax({
      url: "get-state-by-country-id",
      type: "POST",
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){ 
        $('#state_id').attr('disabled', false);
        $('#state_id').empty(); 
        $('#state_id').append('<option value=""><?= json_encode($this->lang->line('select_state')); ?></option>');
        $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
        $('#state_id').focus();
      },
      beforeSend: function(){
        $('#state_id').empty();
        $('#state_id').append('<option value=""><?= json_encode($this->lang->line('loading')); ?></option>');
      },
      error: function(){
        $('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?></option>');
      }
    })
  });

  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $.ajax({
      type: "POST",
      url: "get-cities-by-state-id",
      data: { state_id: state_id },
      dataType: "json",
      success: function(res){ 
        $('#city_id').attr('disabled', false);
        $('#city_id').empty(); 
        $('#city_id').append('<option value=""><?= json_encode($this->lang->line('select_city')); ?></option>');
        $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
        $('#city_id').focus();
      },
      beforeSend: function(){
        $('#city_id').empty();
        $('#city_id').append('<option value=""><?= json_encode($this->lang->line('loading')); ?></option>');
      },
      error: function(){
        $('#city_id').attr('disabled', true);
        $('#city_id').empty();
        $('#city_id').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?></option>');
      }
    })
  });
</script>
<script>
   $(function(){
    $("#updateProfile").validate({
      ignore: [],
      rules: {
        avatar: {  accept:"jpg,png,jpeg,gif" },
        cover: {  accept:"jpg,png,jpeg,gif" },
        firstname: { required: true, },
        lastname: { required: true, },
        company_name: { required: true, },
        contact_no: { required: true, },
        country_id: { required: true, },
        state_id: { required: true, },
        city_id: { required: true, },
        zipcode: { required: true, },
        address: { required: true, },
        email_id: { required: true, },
        introduction: { required: true, },
      }, 
      messages: {
        avatar: {  accept: <?= json_encode($this->lang->line('only_image_allowed')); ?>,   },
        cover: {  accept: <?= json_encode($this->lang->line('only_image_allowed')); ?>,   },
        firstname: { required: <?= json_encode($this->lang->line('enter_first_name')); ?>,   },
        lastname: { required: <?= json_encode($this->lang->line('enter_last_name')); ?>,  },
        company_name: { required: <?= json_encode($this->lang->line('enter_company_name')); ?>,  },
        contact_no: { required: <?= json_encode($this->lang->line('enter_mobile_number')); ?>, },
        country_id: { required: <?= json_encode($this->lang->line('select_country')); ?>,   },
        state_id: { required: <?= json_encode($this->lang->line('select_state')); ?>,   },
        city_id: { required: <?= json_encode($this->lang->line('select_city')); ?>,  },
        zipcode: { required: <?= json_encode($this->lang->line('enter_zipcode')); ?>,   },
        address: { required: <?= json_encode($this->lang->line('enter_address')); ?>,   },
        email_id: { required: <?= json_encode($this->lang->line('enter_email_address'));?>, },
        introduction: { required: <?= json_encode($this->lang->line('enter_some_introduction'));?>, },
      }
    });

    $("#upload_avatar").on('click', function(e) { e.preventDefault(); $("#avatar").trigger('click'); });
    $("#upload_cover").on('click', function(e) { e.preventDefault(); $("#cover").trigger('click'); });
  });
  function avatar_name(e){ if(e.target.files[0].name !="") { $("#avatar_name").removeClass('hidden'); }}
  function cover_name(e){ if(e.target.files[0].name !="") { $("#cover_name").removeClass('hidden'); }}
</script>


<script>
  /*
  $(function() {
        var geocoder;
        geocoder = new google.maps.Geocoder();
        var lat = '';
        var lng = '';
        var address = document.getElementById("zipcode").value;
        //alert(address);
        geocoder.geocode({
            'address': address
        }, function(results, status) {
          alert(results[0].geometry.location.lat());
          alert(results[0].geometry.location.lng());
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        //alert('Latitude: ' + lat + ' Logitude: ' + lng);
  });
  */
  $('#us3').locationpicker({
    location: {
      latitude: <?= $operator['latitude'] ?>,
      longitude: <?= $operator['longitude'] ?>
    },
    radius: 300,
    inputBinding: {
      latitudeInput: $('#us3-lat'),
      longitudeInput: $('#us3-lon'),
      radiusInput: $('#us3-radius'),
      locationNameInput: $('#us3-address')
    },
    enableAutocomplete: true,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
      //Uncomment line below to show alert on each Location Changed event
      //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    }
  });
</script>
