<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Cache-Control" content="no-cache">
    <!-- Page title -->
    <title><?= $this->config->item('site_title'); ?></title>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="<?= $this->config->item('resource_url') . 'images/152x152.png';?>" />
    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/fontawesome/css/font-awesome.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/metisMenu/dist/metisMenu.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/animate.css/animate.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap/dist/css/bootstrap.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/select2-3.5.2/select2.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/select2-bootstrap/select2-bootstrap.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/sweetalert/lib/sweet-alert.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/clockpicker/dist/bootstrap-clockpicker.min.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/blueimp-gallery/css/blueimp-gallery.min.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/ladda/dist/ladda-themeless.min.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap-star-rating/css/star-rating.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/vendor/tagEditor/jquery.tag-editor.css'; ?>" />
    <script src="//maps.googleapis.com/maps/api/js?libraries=places&language=en&key=AIzaSyBSaP8CCJqjgZ8viUxwpMOdswi6Y04Ch0k"></script>
    <script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/jquery/dist/jquery.min.js'; ?>"></script>
    <script src="//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="//cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="<?= $this->config->item('resource_url') . 'js/jquery.geocomplete.min.js'; ?>"></script>    
    <script src="<?= $this->config->item('resource_url') . 'web-panel/scripts/locationpicker.jquery.min.js'; ?>"></script>
    <script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/datatables/media/js/jquery.dataTables.min.js'; ?>"></script>
    <script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js'; ?>"></script>

    <!-- App styles -->
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/fonts/pe-icon-7-stroke/css/helper.css'; ?>" />
    <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/styles/style.css'; ?>">
    <style type="text/css">
        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
        #attachment, .attachment {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
    </style>
<style>
/* Paste this css to your style sheet file or under head tag */
/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(<?=$this->config->item('resource_url').'Preloader_2.gif'?>) center no-repeat #fff;
}
</style>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
    //paste this code under head tag or in a seperate js file.
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
</head>
<body class="fixed-small-header fixed-navbar sidebar-scroll">

<!-- Paste this code after body tag -->
    <div class="se-pre-con"></div>
    <!-- Ends -->

<style>
.skin-option { position: fixed; text-align: center; right: -1px; padding: 10px; top: 80px; width: 150px; height: 133px; text-transform: uppercase; background-color: #ffffff; box-shadow: 0 1px 10px rgba(0, 0, 0, 0.05), 0 1px 4px rgba(0, 0, 0, .1); border-radius: 4px 0 0 4px; z-index: 100; }
.navbar-a { color:#fff  !important; }
.navbar-a:hover, .navbar-a:focus { color:#225595 !important; }
.header-link { background: #225595 !important; border-left: 1px solid #225595;border-right: 1px solid #225595; }
#logo.light-version { background-color: #ffffff; }

.navbar.navbar-left li a:hover { 
    color: white !important;
    background-color: #072963 !important;
}
.navbar.navbar-right li a:hover { 
    color: white !important;
    background-color: #072963 !important;
}
.navbar.navbar-right li ul li a:hover { 
    color: #072963 !important;
    background-color: transparent !important;
}
.navbar.navbar-right li ul li a { 
    color: #072963 !important;
}
</style>
<script>
    setInterval(function(){ $('#demo-star').toggleClass('text-success'); }, 300)
</script>
<!-- End skin option / for demo purpose only -->

<!-- Header -->
<div id="header" style="background-color:#225595;">
    <div id="logo" class="light-version" style="border-bottom: none;">
        <a href="<?= base_url(); ?>">
            <img src="<?=base_url('resources/images/dashboard-logo.jpg')?>" alt="<?=$this->lang->line('logo')?>" style=" margin-top: -5px; margin-left: -20px;">
        </a>
    </div>
    <nav role="navigation">
        <div class="header-link hide-menu" style="padding: 18px 12px 18px 12px !important"><i class="fa fa-bars" style="color:#fff;"></i></div>
        <div class="small-logo" style="padding-top: 3px;">
            <h3 class="text-primary text-white" ><?= $this->lang->line('gonagoo'); ?></h3>
        </div>
        <div class="navbar navbar-left">
            <ul class="nav navbar-nav">
              <li style="margin-left: -16px;"><a style="padding: 15px 12px !important; <?=($this->router->fetch_class()=='user_panel' && $this->router->fetch_method()=='landing')?'color: #072963; background-color:white;':''?>" href="<?= base_url('user-panel/landing');?>"><i class="fa fa-home" title="<?=$this->lang->line('Home')?>"></i></a></li>
              <li><a style="padding: 15px 12px !important; <?=(($this->router->fetch_class()=='user_panel' || $this->router->fetch_class()=='transport') && $this->router->fetch_method()!='landing')?'color: #072963; background-color:white;':''?>font-size: 1.2em;" href="<?= base_url('user-panel/dashboard');?>"><img src="<?=base_url('resources/images/courier.png')?>"> <?=$this->lang->line('Courier & Transport')?></a></li>
              <li><a style="padding: 15px 12px !important; <?=($this->router->fetch_class()=='user_panel_bus')?'color: #072963; background-color:white;':''?>font-size: 1.2em;" href="<?= base_url('user-panel-bus/dashboard-bus');?>"><img src="<?=base_url('resources/images/bus.png')?>"> <?=$this->lang->line('Ticket')?></a></li>
              <li><a style="padding: 15px 12px !important; <?=($this->router->fetch_class()=='user_panel_laundry')?'color: #072963; background-color:white;':''?>font-size: 1.2em;" href="<?= base_url('user-panel-laundry/dashboard-laundry');?>"><img src="<?=base_url('resources/images/laundry.png')?>"> <?=$this->lang->line('Laundry')?></a></li>
              <!--<li><a style="padding: 15px 12px !important; <?=($this->router->fetch_class()=='user_panel_gas')?'color: #072963; background-color:white;':''?>font-size: 1.2em;" href="<?= base_url('user-panel/landing');?>"><img src="<?=base_url('resources/images/gas.png')?>"> <?=$this->lang->line('Gas')?></a></li>-->
            </ul>
         </div> 
        <div class="mobile-menu">
            <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                <i class="fa fa-chevron-down text-white"></i>
            </button>
            <div class="collapse mobile-navbar" id="mobile-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="" href="<?= base_url('log-out'); ?>"><?= $this->lang->line('logout');?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="navbar navbar-right">
            <ul class="nav navbar-nav no-borders" style="margin-right:15px;">
                <li class="dropdown">
                    <a style="padding: 15px 12px !important;" class="dropdown-toggle" href="#" data-toggle="dropdown" title="<?= $this->lang->line('change_language'); ?>">
                        <?php if( (isset($_SESSION['language']) AND trim(strtolower($_SESSION['language'])) == 'english') || (isset($_SESSION['language']) AND trim(strtolower($_SESSION['language'])) == 'anglais') ) { ?>
                            <img style="width:32px; height:auto; max-width:32px; max-height:auto;" src="<?= base_url('resources/web-panel/images/english-32x16.png') ?>" />
                        <?php } else { ?> <img style="width:32px; height:auto; max-width:32px; max-height:auto;" src="<?= base_url('resources/web-panel/images/france-32x16.png') ?>" />
                        <?php } ?>
                    </a>
                    <ul class="dropdown-menu hdropdown notification animated flipInX">                        
                        <li>
                            <a class="btn_lang" data-id="english"><img style="width:32px; height:auto; max-width:32px; max-height:auto;" src="<?= base_url('resources/web-panel/images/english-32x16.png') ?>" /><?= $this->lang->line('english'); ?> </a>
                        </li>
                        <li>
                            <a class="btn_lang" data-id="french"><img style="width:32px; height:auto; max-width:32px; max-height:auto;" src="<?= base_url('resources/web-panel/images/france-32x16.png') ?>" /><?= $this->lang->line('french'); ?> </a>
                        </li>
                        <li class="hidden">
                            <a class="btn_lang" data-id="spanish"><img style="width:32px; height:auto; max-width:32px; max-height:auto;" src="<?= base_url('resources/web-panel/images/france-32x16.png') ?>" /><?= $this->lang->line('spanish'); ?> </a>
                        </li>
                    </ul>
                </li>
                <!-- <li class="dropdown">
                    <a href="<?= base_url('user-panel/get-support-list');?>" title="<?= $this->lang->line('any_suggestion_or_request_a_support'); ?>">
                        <i class="pe-7s-help2 pe-rotate-90"></i>
                    </a>
                </li>  -->
                <li class="dropdown">
                    <a style="padding: 15px 12px !important;" class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown" title="Notifications" id="notification">
                        <i class="pe-7s-bell"></i>
                        <?php if($total_unread_notifications > 0 ): ?>
                            <span id="counter" class="label label-success"><?= $total_unread_notifications; ?></span>
                        <?php endif; ?>
                    </a>
                    <ul class="dropdown-menu hdropdown notification animated flipInX">
                        <?php if(COUNT($notification)): rsort($notification); $counter = 0; $nt_ids = array(); ?>
                            <?php foreach ($notification as $n):  $counter ++; $nt_ids[] = $n['id']; ?>
                                <?php if($counter > 5 ) { break; } ?>
                                <li><a><span class="label label-success notify_span"><?= $this->lang->line('new'); ?></span> 
                                    <?php if(str_word_count($n['notify_title']) > 5 ): ?>
                                        <?= $this->api->get_words($n['notify_title'],5).'...'; ?>
                                    <?php else: ?>
                                        <?= $n['notify_title']; ?>
                                    <?php endif; ?>
                                </a></li>
                            <?php endforeach; ?>
                        <?php endif; ?>                        
                        <li class="summary"><a href="<?= $this->config->item('base_url') . 'user-panel/user-notifications'; ?>"><?= $this->lang->line('see_all_admin_notifications');?></a></li>
                    </ul>
                </li>
                
                <li class="dropdown">
                    <a style="padding: 15px 12px !important;" class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown" title="<?= $this->lang->line('workroom'); ?>">
                        <i class="pe-7s-mail"></i>
                        <?php if($total_workroom_notifications > 0 ): ?>
                            <span id="wr_counter" class="label label-success"><?= $total_workroom_notifications; ?></span>
                        <?php endif; ?>
                    </a>
                    <ul class="dropdown-menu hdropdown animated flipInX">
                        <?php if(COUNT($nt_workroom)): rsort($nt_workroom); $wr_counter = 0; $nt_ids = array(); ?>
                            <?php foreach ($nt_workroom as $n):  $wr_counter ++; $nt_ids[] = $n['ow_id']; ?>
                                <?php if($wr_counter > 5 ) { break; } ?>                                
                                <li class="workroom-notification">
                                    <?php if($n['cat_id'] == 9) { ?>
                                        <form class="workroomForm" action="<?= base_url('user-panel-laundry/laundry-work-room');?>" method="post">
                                        <input type="hidden" value="<?= $n['order_id'] ?>" name="booking_id" />
                                    <?php } else if($n['cat_id'] == 281) { ?>
                                        <form class="workroomForm" action="<?= base_url('user-panel-bus/bus-work-room');?>" method="post">
                                        <input type="hidden" value="<?= $n['order_id'] ?>" name="ticket_id" />
                                    <?php } else { ?>
                                        <form class="workroomForm" action="<?= base_url('user-panel/courier-work-room');?>" method="post">
                                        <input type="hidden" value="<?= $n['order_id'] ?>" name="order_id" />
                                    <?php } ?>
                                        <input type="hidden" value="workroom" name="type" />
                                        <a class="btn_workroom">
                                            <?php if($n['cat_id']== 9) { ?>
                                                <span class="label label-success wr_span"><?= $this->lang->line('Laundry Booking'); ?></span> 
                                            <?php } else if($n['cat_id']== 281) { ?>
                                                <span class="label label-success wr_span"><?= $this->lang->line('Ticket Booking'); ?></span>
                                            <?php } else { ?>
                                                <span class="label label-success wr_span"><?= $this->lang->line('Courier/Transport'); ?></span> 
                                            <?php } ?>
                                            <?php if(str_word_count($n['text_msg']) > 5 ): ?>
                                                <?= $this->api->get_words($n['text_msg'],5).'...'; ?>
                                            <?php else: ?>
                                                <?= $n['text_msg']; ?>
                                            <?php endif; ?>
                                        </a>
                                    </form>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>  
                        <li class="summary"><a href="<?= base_url('user-panel/courier-order-list'); ?>"><?= $this->lang->line('see_all_workroom_messagesw'); ?></a></li>
                    </ul>
                </li>
                
                <?php if($_SESSION['acc_type'] == 'buyer' || $_SESSION['acc_type'] == 'both') { ?>
                    <li class="dropdown">
                        <a style="padding: 15px 12px !important;" class="dropdown-toggle label-menu-corner" title="Chat Notifications" href="#" data-toggle="dropdown" href="<?= $this->config->item('base_url') . 'user-panel/chat-room'; ?>">
                            <i class="pe-7s-users"></i>
                            <?php if($total_chatroom_notifications > 0 ): ?>
                                <span class="label label-success"><?= $total_chatroom_notifications; ?></span>
                            <?php endif; ?>
                        </a>
                        <ul class="dropdown-menu hdropdown animated flipInX">
                            <?php if(COUNT($nt_chatroom)): rsort($nt_chatroom); $ch_counter = 0; $nt_ids = array(); ?>
                                <?php foreach ($nt_chatroom as $n):  $ch_counter ++; $nt_ids[] = $n['chat_id']; ?>
                                    <?php if($ch_counter > 5 ) { break; } ?>                                
                                    <li class="chatroom-notification">
                                        <form class="chatroomForm" action="<?= base_url('user-panel/courier-chat-room');?>" method="post">
                                            <input type="hidden" value="<?= $n['order_id'] ?>" name="order_id" />
                                            <input type="hidden" value="chatroom" name="type" />
                                            <a class="btn_chatroom">
                                                <span class="label label-success"><?= $this->lang->line('new');?></span> 
                                                <?php if(str_word_count($n['text_msg']) > 5 ): ?>
                                                    <?= $this->api->get_words($n['text_msg'],5).'...'; ?>
                                                <?php else: ?>
                                                    <?= $n['text_msg']; ?>
                                                <?php endif; ?>
                                            </a>
                                        </form>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>  
                            <li class="summary"><a href="<?= base_url('user-panel/in-progress-bookings'); ?>"><?= $this->lang->line('see_all_chatroom_messages'); ?></a></li>
                        </ul>
                    </li>
                <?php } ?>

                <li class="dropdown">
                    <a style="padding: 15px 12px !important;" href="<?= base_url('log-out');?>">
                        <i class="pe-7s-upload pe-rotate-90"></i>
                    </a>
                </li>                
            </ul>
        </div>
    </nav>
</div>

<script>
    $("#notification").on('click', function(event) {    event.preventDefault();
        $.get('notifications-read', function(res) { 
           if(res == "success"){ $("#counter").text(''); setTimeout(function(){ $(".notify_span").text('');},2500); } 
        });        
    });

    $(".btn_workroom").on('click', function(event) {   event.preventDefault();
        var wr_counter = $("#wr_counter").text();
        var form = $(this).parent();
        $("#wr_counter").text((wr_counter - 1 ));
        form.get(0).submit();
        form.parent().remove();
    });

    $(".btn_chatroom").on('click', function(event) {   event.preventDefault();
        var ch_counter = $("#ch_counter").text();
        var form = $(this).parent();
        $("#ch_counter").text((ch_counter - 1 ));
        form.get(0).submit();
        form.parent().remove();
    });
</script>
<script>
    $(".btn_lang").on('click', function(event) { event.preventDefault();
        var lang = $(this).data("id");
        //alert(lang);
        //console.log(window.location.href);

        $.post("<?=base_url()?>"+"user-panel/set-lang", {lang: lang}, function(res) {
            //console.log(res);
            window.location.reload();               
        });
        return false;           
    });

    $("a[name='create_link']").on('click',function(e){ e.preventDefault();
            var redirect = $(this).data('redirect');
            var cat_id = $(this).data('cat_id');

            var newForm = $('<form>', {
                'action': <?= json_encode(base_url());?> + redirect,
                'method': 'post'
            }).append($('<input>', {
                'name': 'category_id',
                'value': cat_id,
                'type': 'hidden'
            }));
            $(document.body).append(newForm);
            newForm.submit();
        }); 
</script>

