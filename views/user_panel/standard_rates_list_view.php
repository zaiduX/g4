  <div class="normalheader small-header">
    <div class="hpanel">
      <div class="panel-body">
        <a class="small-header-action" href="">
          <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
          </div>
        </a>

        <div id="hbreadcrumb" class="pull-right">
          <ol class="hbreadcrumb breadcrumb">
            <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
            <li class="active"><span><?= $this->lang->line('Standard Rates'); ?></span></li>
          </ol>
        </div>
        <h2 class="font-light m-b-xs">  <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Standard Rates'); ?> &nbsp;&nbsp;&nbsp;
          <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-info btn-offline dropdown-toggle"> 
              <i class="fa fa-plus"></i>  <?= $this->lang->line('Add New'); ?> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
              <li class="text-center">
                <a href="<?= base_url('user-panel/add-volume-based'); ?>"> <?= $this->lang->line('Volume Based Rate'); ?> </a>
              </li>
              <li class="divider"></li>
              <li class="text-center">
                <a href="<?= base_url('user-panel/add-weight-based'); ?>"> <?= $this->lang->line('Weight Based Rate'); ?> </a>
              </li>
              <li class="divider"></li>
              <li class="text-center">
                <a href="<?= base_url('user-panel/add-formula-volume-based'); ?>"> <?= $this->lang->line('Formula Volume Based Rate'); ?> </a> 
              </li>
              <li class="divider"></li>
              <li class="text-center">
                <a href="<?= base_url('user-panel/add-formula-weight-based'); ?>"> <?= $this->lang->line('Formula Weight Based Rate'); ?> </a> 
              </li>
            </ul>
          </div>
        </h2>
        <small class="m-t-md"><?= $this->lang->line('Available Rates'); ?></small>    
      </div>
    </div>
  </div>
  <br />
  <div class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="hpanel hblue">
          <div class="panel-body">
            <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
              <thead>
              <tr>
                <th class="text-center">#</th>
                <th class="text-center"><?= $this->lang->line('Category'); ?></th>
                <th class="text-center"><?= $this->lang->line('Country'); ?></th>
                <th class="text-center"><?= $this->lang->line('Distance'); ?></th>
                <th class="text-center"><?= $this->lang->line('Volume'); ?></th>
                <th class="text-center"><?= $this->lang->line('Weight'); ?></th>
                <th class="text-center"><?= $this->lang->line('Rate Type'); ?></th>
                <th class="text-center"><?= $this->lang->line('action'); ?></th>
              </tr>
              </thead>
              <tbody>
                <?php $offset = $this->uri->segment(3,0) + 1; ?>
                <?php foreach ($rates as $rate):  ?>
                  <tr>
                    <td class="text-center"><?= $offset++; ?></td>
                    <td class="text-center"><?= $rate['cat_name']; ?></td>
                    <td class="text-center"><?= ucfirst($rate['country_name']); ?></td>
                    
                    <?php if($rate['is_formula_volume_rate'] == 0 && $rate['is_formula_weight_rate'] == 0): ?>
                      <td class="text-center"><?= $rate['min_distance'] . ' - ' . $rate['max_distance'] . ' KM'; ?></td>
                    <?php else: ?>
                      <!-- <td class="text-center">1 KM</td> -->
                      <td class="text-center"><?= $rate['min_distance'] . ' - ' . $rate['max_distance'] . ' KM'; ?></td>
                    <?php endif; ?>
                        
                    <?php if($rate['unit_id'] == 0): ?>
                        <?php if($rate['is_formula_volume_rate'] == 0 && $rate['is_formula_weight_rate'] == 0): ?>
                          <td class="text-center"><?= $rate['min_volume'] . ' - ' . $rate['max_volume'] . ' cm<sup>3</sup>'; ?></td>
                        <?php else: ?>
                          <?php if($rate['is_formula_volume_rate'] == 1): ?>
                            <td class="text-center">1 cm<sup>3</sup></td>
                          <?php else: ?>
                            <td class="text-center"><?= 'NA'; ?></td>
                          <?php endif; ?>
                        <?php endif; ?>
                    <?php else: ?>
                      <td class="text-center"><?= 'NA'; ?></td>
                    <?php endif; ?>

                    <?php if($rate['unit_id'] > 0): $unit = $this->api->get_unit_detail($rate['unit_id']); ?>
                      <?php if($rate['is_formula_volume_rate'] == 0 && $rate['is_formula_weight_rate'] == 0): ?>
                        <td class="text-center"><?= $rate['min_weight'] . ' - ' . $rate['max_weight'] .' '. $unit['shortname'] ; ?></td>
                      <?php else: ?>
                        <?php if($rate['is_formula_volume_rate'] == 1): ?>
                          <td class="text-center"><?= 'NA'; ?></td>
                        <?php else: ?>
                          <td class="text-center">1 <?=$unit['shortname']?></td>
                        <?php endif; ?>
                      <?php endif; ?>
                    <?php else: ?>
                      <td class="text-center"><?= 'NA'; ?></td>
                    <?php endif; ?>

                    <?php if($rate['is_formula_volume_rate'] == 1): ?>
                      <td class="text-center"><?= $this->lang->line('Formula Volume'); ?></td>
                    <?php elseif($rate['is_formula_weight_rate'] == 1): ?>
                      <td class="text-center"><?= $this->lang->line('Formula Weight'); ?></td>
                    <?php elseif($rate['unit_id'] > 0 && $rate['is_formula_weight_rate'] == 0 && $rate['is_formula_volume_rate'] == 0): ?>
                      <td class="text-center"><?= $this->lang->line('weight'); ?></td>
                    <?php else: ?>
                      <td class="text-center"><?= $this->lang->line('volume'); ?></td>
                    <?php endif; ?>

                    <td class="text-center" style="display:flex;">
                      <form action="<?=base_url('user-panel/edit-standard-rates/');?>" method="post">
                        <input type="hidden" name="rate_id" value="<?=$rate["rate_id"];?>">
                        <button type="submit" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= $this->lang->line('Click to View / Edit Rates'); ?>"><i class="entypo-pencil"></i> <?= $this->lang->line('View / Edit'); ?></button>
                      </form>&nbsp;
                      <button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_rate('<?= $rate["rate_id"]; ?>');">
                        <i class="entypo-cancel"></i> <?= $this->lang->line('Delete'); ?>
                      </button>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <script>
    function delete_rate(id=0){   
      swal({
        title: "<?= $this->lang->line('are_you_sure'); ?>",
        text: "<?= $this->lang->line('You want to to delete this Rate?'); ?>",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "<?= $this->lang->line('Yes, delete it!'); ?>",
        cancelButtonText: "<?= $this->lang->line('No, cancel!'); ?>",
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: true },
        function() {
          $.post("<?=base_url('user-panel/delete-standard-rates')?>", {id: id}, function(res) {
            if(res == 'success') {
              swal(
                "<?= $this->lang->line('deleted'); ?>",
                "<?= $this->lang->line('Record has been deleted.'); ?>",
                'success'
              ); 
              setTimeout(function() { window.location.reload(); }, 2000);
            } else {
              swal(
                "<?= $this->lang->line('error'); ?>",
                "<?= $this->lang->line('While deleting details.'); ?>",
                'error'
              )
            }
          });
        })
    }
  </script>