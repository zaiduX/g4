<style type="text/css">
    .table > tbody > tr > td {
        border-top: none;
    }
    .dataTables_filter {
        display: none;
    }
</style>
<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= base_url('user-panel-laundry/dashboard-laundry'); ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('in_progress_bookings'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"><i class="fa fa-gears fa-2x text-muted"></i> 
                <?= $this->lang->line('in_progress_bookings'); ?> &nbsp;&nbsp;&nbsp;
            </h2>
            <small><?= $this->lang->line('My Bookings'); ?></small>
        </div>
    </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="hpanel">
        <div class="panel-body" style="padding: 10px 17px 5px 17px; margin-bottom: -10px;">

          <div class="row" id="basicSearch" style="">
            <div class="col-md-2">
              <h4><i class="fa fa-search"></i> <?= $this->lang->line('search'); ?></h4>
            </div>
            <div class="col-md-10">
              <input type="text" id="ordersearchbox" class="form-control" placeholder="<?= $this->lang->line('search'); ?>">
            </div>
          </div>
        </div>
        <br />

        <div class="panel-body" style="padding: 0px 10px 5px 10px;">
          <div class="row">
            <div class="col-lg-12">
              <?php if($this->session->flashdata('error')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                </div>
              </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php"); ?>
              <table id="orderTableData" class="table">
                <thead><tr class="hidden"><th></th></tr></thead>
                <tbody>
                  <?php foreach ($bookings as $order) {  ?>
                    <tr>
                      <td style="line-height: 15px;">
                        <div class="hpanel" style="line-height: 15px; margin-bottom: 0;">
                          <div class="panel-body" style="line-height: 15px; padding-top: 5px; padding-bottom: 5px;">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"> 
                                <h5 class="text-left text-light"><?= $this->lang->line('ref_G'); ?><?= $order['booking_id'] ?></h5>
                              </div>
                              <div class="col-xl-6 col-lg-6 col-md-4 col-sm-4 text-center"> 
                                <h4 style="margin-top: 0px;">
                                  <?php $icon_url = $this->api->get_category_details_by_id($order['cat_id'])['icon_url']; ?>
                                  <img style="width:24px; height: auto;" src='<?= base_url($icon_url) ?>' /> <?= $this->lang->line('Laundry Service'); ?>
                                </h4>
                              </div>
                              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6"> 
                                <h6 class="text-right text-light"><strong> <?= $this->lang->line('order_date'); ?> <?= date('l, d M Y',strtotime($order['cre_datetime'])); ?></strong></h6>
                                <h6 class="text-right text-light"><?= $this->lang->line('Expected Complete Date'); ?>: <?= date('l, d M Y',strtotime($order['expected_return_date'])); ?></h6>
                              </div>
                              <br/>
                            </div>

                            <dir class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right:0px;margin-top:-15px;margin-bottom: 0px">
                              <?php $provider_pofile = $this->api->get_laundry_provider_profile($order['provider_id']); ?>
                              <h5 class="m-b-xs" style="color:#59bdd7"><i class="fa fa-building-o" aria-hidden="true"></i>
                                <?=$provider_pofile['company_name']." [ ".$provider_pofile['firstname']." ".$provider_pofile['lastname']." ]"?>                                  
                              </h5>
                            </dir>
                            
                            <dir class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right:0px;margin-top: 0px;margin-bottom: 0px"> 
                              <dir class="col-xl-10 col-lg-10 col-md-10 col-sm-10" style="padding-left: 0px; padding-right:0px;margin-top: 0px;margin-bottom: 0px">
                                
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Laundry Charges'); ?>: <?= $order['currency_sign'] ?> <?= $order['booking_price'] ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('balance'); ?>: <?= $order['currency_sign'] . ' '. ($order['total_price'] - $order['paid_amount']); ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('advance'); ?>: <?= $order['currency_sign'] ?> <?= $order['advance_payment'] ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Is Pickup'); ?>: <?=($order['is_pickup']==1)?$this->lang->line('yes'):$this->lang->line('no')?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Is Drop'); ?>: <?=($order['is_drop']==1)?$this->lang->line('yes'):$this->lang->line('no')?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('No of items'); ?>: <?= $order['item_count']; ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Pickup Charges'); ?>: <?=$order['currency_sign'] . ' '. $order['pickup_courier_price']?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Delivery Charges'); ?>: <?=$order['currency_sign'] . ' '. $order['drop_courier_price']?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Total Charges'); ?>: <?=$order['currency_sign'] . ' '. $order['total_price']; ?></div>
                              </dir>
                              <dir class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-left: 0px; padding-right:0px;margin-top: 0px;margin-bottom: 0px">
                                <button class="btn btn-outline btn-sm btn-success btn_view_<?=$order['booking_id']?>"><i class="fa fa-eye"></i> <?= $this->lang->line('Show Items'); ?></button>
                                <button class="btn btn-outline btn-sm btn-success btn_hide_<?=$order['booking_id']?> hidden"><i class="fa fa-eye"></i> <?= $this->lang->line('Hide Items'); ?></button>&nbsp;
                                <?php if($order['is_drop'] == 0 ) { ?>
                                  <form action="<?=base_url('user-panel-laundry/customer-mark-is-drop-view')?>" method="post">
                                    <input type="hidden" name="booking_id" value="<?=$order['booking_id']?>">
                                    <input type="hidden" name="page_name" value="<?=$this->router->fetch_method()?>">
                                    <button class="btn btn-outline btn-sm btn-warning"><i class="fa fa-check"></i> <?= $this->lang->line('Is Drop? Yes.'); ?></button>
                                  </form>
                                <?php } ?>
                              </dir>
                            </dir>
                            
                          </div>
                          <div class="panel-body" id="details_<?=$order['booking_id']?>" style="display:none; padding-top: 0px; padding-bottom: 0px;">
                            <table style="width:100%" class="table display compact" id="tbl_<?=$order['booking_id']?>">
                              <thead>
                                <tr>
                                  <th><?= $this->lang->line('Sr. No'); ?></th>
                                  <th><?= $this->lang->line('Item Category'); ?></th>
                                  <th><?= $this->lang->line('Item Name'); ?></th>
                                  <th><?= $this->lang->line('Single Unit Price'); ?></th>
                                  <th><?= $this->lang->line('quantity'); ?></th>
                                  <th><?= $this->lang->line('total'); ?></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($order['bookings_details'] as $dtls) { static $i=0; $i++; ?>
                                  <tr>
                                    <td><?=$i?></td>
                                    <td><?=$dtls['cat_name']?></td>
                                    <td><?=$dtls['sub_cat_name']?></td>
                                    <td><?=$dtls['single_unit_price']?></td>
                                    <td><?=$dtls['quantity']?></td>
                                    <td><?=$dtls['total_price']?></td>
                                  </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>
                          <div class="panel-footer" style="padding-top: 5px;padding-bottom: 5px;">
                            <div class="row">
                              <div class="col-md-4">
                                <h5 style="color: #59bdd7; margin-top: 0px">
                                  <?php
                                    if($order['drop_order_id'] > 0) {
                                      $status = $this->api->order_status_list($order['drop_order_id']);
                                      if($status[(sizeof($status)-1)]['status'] == 'open') { echo $this->lang->line('ordered'); } 
                                      else if($status[(sizeof($status)-1)]['status'] == 'accept' && $status[(sizeof($status)-1)]['user_type'] == 'deliverer') { echo $this->lang->line('deliverer_accepted'); }
                                      else if($status[(sizeof($status)-1)]['status'] == 'assign') { echo $this->lang->line('driver_assigned'); }
                                      else if($status[(sizeof($status)-1)]['status'] == 'accept' && $status[(sizeof($status)-1)]['user_type'] == 'driver') { echo $this->lang->line('driver_accepted'); }
                                      else if($status[(sizeof($status)-1)]['status'] == 'in_progress') { echo $this->lang->line('order_is_in_progress'); }
                                      else if($status[(sizeof($status)-1)]['status'] == 'src_relay_in') { echo $this->lang->line('recieved_at_source_relay_point'); }
                                      else if($status[(sizeof($status)-1)]['status'] == 'src_relay_out') { echo $this->lang->line('out_from_source_relay_point'); }
                                      else if($status[(sizeof($status)-1)]['status'] == 'dest_relay_in') { echo $this->lang->line('recieved_at_destination_relay_point'); }
                                      else if($status[(sizeof($status)-1)]['status'] == 'dest_relay_out') { echo $this->lang->line('out_from_destination_relay_point'); }
                                      else if($status[(sizeof($status)-1)]['status'] == 'delivered') { echo $this->lang->line('delivered_successfully'); }
                                      echo ' - '.strtoupper($status[(sizeof($status)-1)]['mod_datetime']);
                                    } ?>
                                </h5>
                                <?php
                                  if($order['drop_order_id'] > 0 && $order['drop_order_details'][0]['from_addr_type'] == 0) { 
                                    if( $order['drop_order_details'][0]['driver_status_update']=='accept' 
                                      && ( ($order['drop_order_details'][0]['payment_mode']=='payment' && $order['drop_order_details'][0]['complete_paid']==1)
                                      || ($order['drop_order_details'][0]['is_bank_payment']==1)
                                      || ($order['drop_order_details'][0]['payment_mode']=='cod' && $order['drop_order_details'][0]['cod_payment_type']=='at_pickup' && $order['drop_order_details'][0]['complete_paid']==1)
                                      || ($order['drop_order_details'][0]['payment_mode']=='cod' && $order['drop_order_details'][0]['cod_payment_type']=='at_deliver' && $order['drop_order_details'][0]['complete_paid']==0) ) ) { ?>
                                        <form action="<?= base_url('user-panel/customer-order-pickup'); ?>" method="post">
                                          <input type="hidden" name="cd_id" value="<?= $order['drop_order_details'][0]['driver_id'] ?>" />
                                          <input type="hidden" name="order_id" value="<?= $order['drop_order_details'][0]['order_id'] ?>" />
                                          <input type="hidden" name="order_status" value="in_progress" />
                                          <input type="hidden" name="cust_id" value="<?= $order['drop_order_details'][0]['cust_id'] ?>" />
                                          <input type="hidden" name="deliverer_id" value="<?= $order['drop_order_details'][0]['deliverer_id'] ?>" />
                                          <input type="text" name="driver_code" placeholder="<?= $this->lang->line('Pickup Code'); ?>" class="form-control" id="driver_code" style="height:31px;" required/>
                                          <button type="submit" class="btn btn-outline btn-success btn-sm"><i class="fa fa-hand-paper-o"></i> <?= $this->lang->line('pick_up'); ?></button>
                                        </form>
                                      <?php } ?>
                                    <?php } ?>
                              </div>
                              <div class="col-md-4">
                                <?php if(isset($order['drop_order_details'])): ?>
                                  <h5 style="color: #59bdd7; margin-top: 0px"><?php if($order['drop_order_details'][0]['deliverer_company_name'] != 'NULL') { echo $this->lang->line('Deliverer: ').$order['drop_order_details'][0]['deliverer_company_name']; } ?>
                                  </h5>
                                  <?php if($order['drop_order_details'][0]['driver_code'] != 'NULL') { ?>
                                    <?= $this->lang->line('driver_code'); ?> : <?= $order['drop_order_details'][0]['driver_code'] ?>
                                  <?php } ?>
                                  <?php if($order['drop_order_details'][0]['pickup_code'] != 'NULL') { ?>
                                    <?= $this->lang->line('Pickup Code'); ?> : <?= $order['drop_order_details'][0]['pickup_code'] ?>
                                  <?php } ?>
                                <?php endif; ?>
                              </div>
                              <div class="col-md-4 text-right" style="display: inline-flex;">

                                <button id="<?= $order['booking_id'] ?>" class="btn btn-outline btn-info btn-sm" data-toggle="modal" data-target="#printModal<?= $order['booking_id'] ?>"><i class="fa fa-ticket"></i> <?= $this->lang->line('Print Ticket'); ?></button>&nbsp;
                                  <div class="modal fade" id="printModal<?=$order['booking_id']?>" role="dialog">
                                    <div class="modal-dialog">
                                      <div class="modal-content" style="width: 400px;">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title"><?= $this->lang->line('Print Ticket'); ?></h4>
                                        </div>
                                        <div class="modal-body" id="printThis<?= $order['booking_id'] ?>">
                                          <?php
                                            $customer_details = $this->api->get_user_details($order['cust_id']);
                                            $operator_details = $this->api->get_laundry_provider_profile($order['provider_id']);
                                          ?>
                                          <div style="width: 340px; height: 80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                                            <div class="col-md-6" style="border-right: dashed 1px #225595; display: flex;">
                                              <img src="<?=($operator_details['avatar_url']=='' || $operator_details['avatar_url'] == 'NULL')?base_url('resources/no-image.jpg'):base_url($operator_details['avatar_url'])?>" style="border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                                              <div>
                                              <h6><?=$operator_details['company_name']?></h6>
                                              <h6><?=$operator_details['firstname']. ' ' .$operator_details['lastname']?></h6>
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                              <img src="<?= base_url('resources/images/dashboard-logo.jpg') ?>" style="width: 100px; height: auto;" /><br />
                                              <h6><?= $this->lang->line('slider_heading1'); ?></h6>
                                            </div>
                                          </div>
                                          <div style="width: 340px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                                            <h5 style="color: #225595; text-decoration: underline;">
                                              <i class="fa fa-arrow-up"></i> <?= $this->lang->line('Booking Details'); ?> <br />
                                              <strong style="float: right; padding-right: 2px; margin-top: -15px"><?= $this->lang->line('Booking ID'); ?>: #<?= $order['booking_id'] ?></strong> 
                                              <br />
                                              <?php QRcode::png($order['booking_id'], $order['booking_id'].".png", "L", 2, 2); 
                                                $img_src = $_SERVER['DOCUMENT_ROOT']."/".$order['booking_id'].'.png';
                                                $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/laundry-qrcode/".$order['booking_id'].'.png';
                                                if(rename($img_src, $img_dest));
                                              ?>
                                              <img src="<?php echo base_url('resources/laundry-qrcode/').$order['booking_id'].'.png'; ?>" style="float: right; width: 84px; padding-right: 2px; height: 84px; margin-top: -15px" />
                                            </h5>
                                            <h6 style="margin-top:-15px;"><strong><?= $this->lang->line('Customer'); ?>:</strong> <?= $order['cust_name'] ?></h6>
                                            <h6><strong><?= $this->lang->line('Booking Date'); ?>:</strong> <?= $order['cre_datetime'] ?></h6>
                                            <h6><strong><?= $this->lang->line('Total items'); ?>:</strong> <?= $order['item_count'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                <strong><?= $this->lang->line('Price'); ?>:</strong> <?= $order['total_price'] ?> <?= $order['currency_sign'] ?></h6>
                                            <h6><strong><?= $this->lang->line('Items Details'); ?>:</strong></h6>
                                            
                                            <table width="100%">
                                              <thead>
                                                <th><?= $this->lang->line('Item Name'); ?></th>
                                                <th><?= $this->lang->line('quantity'); ?></th>
                                                <th><?= $this->lang->line('total'); ?></th>
                                              </thead>
                                              <tbody>
                                                <?php foreach ($order['bookings_details'] as $dtls) { static $i=0; $i++; ?>
                                                  <tr>
                                                    <td><?=$dtls['sub_cat_name']?></td>
                                                    <td><?=$dtls['quantity']?></td>
                                                    <td><?=$dtls['total_price']?></td>
                                                  </tr>
                                                <?php } ?>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                        <div class="modal-footer" style="display: flex;">
                                          <button style="float: right;" class="btn btn-sm pull-right btn-default" id="btnPrint<?= $order['booking_id'] ?>"><?= $this->lang->line('Print Ticket'); ?></button>
                                          <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <script>
                                      $("#btnPrint<?= $order['booking_id'] ?>").click(function () {
                                        $('#printThis<?= $order['booking_id'] ?>').printThis();
                                    });
                                  </script>

                                <form action="<?=base_url('user-panel-laundry/laundry-work-room')?>" method="post">
                                  <input type="hidden" name="booking_id" value="<?=$order['booking_id']?>">
                                  <button class="btn btn-outline btn-sm btn-warning"><i class="fa fa-briefcase"></i> <?= $this->lang->line('go_to_workroom'); ?></button>
                                </form>
                              </div>
                            </div>
                          </div>
                          <script>
                            $(".btn_view_<?=$order['booking_id']?>").click(function(){
                              $("#details_<?=$order['booking_id']?>").slideToggle(500);
                              $(".btn_view_<?=$order['booking_id']?>").addClass("hidden");
                              $(".btn_hide_<?=$order['booking_id']?>").removeClass("hidden");
                            });
                            $(".btn_hide_<?=$order['booking_id']?>").click(function(){
                              $("#details_<?=$order['booking_id']?>").slideToggle(500);
                              $(".btn_view_<?=$order['booking_id']?>").removeClass("hidden");
                              $(".btn_hide_<?=$order['booking_id']?>").addClass("hidden");
                            });
                            $(document).ready(function() {
                              $('#tbl_<?=$order['booking_id']?>').DataTable({
                                "paging":   false,
                                "info":     false
                              });
                            });
                            $("#btn_mark_<?=$order['booking_id']?>").on('click',function(e){
                              e.preventDefault();
                              var form = $(this).parents('form');
                              swal({
                                title: "<?= $this->lang->line('are_you_sure'); ?>",
                                text: "<?= $this->lang->line('Booking status will mark as completed and booking will move to completed list.'); ?>",
                                type: "warning",
                                showCancelButton: true,
                                cancelButtonText: "<?= $this->lang->line('cancel'); ?>",
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "<?= $this->lang->line('yes'); ?>",
                                closeOnConfirm: false
                              }, function(isConfirm){
                                if(isConfirm) form.submit();
                              });
                          });
                          </script>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>