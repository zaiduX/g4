<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('profile'); ?></span></li>
          <li><span><?= $this->lang->line('payment_methods'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('list'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-credit-card fa-2x text-muted"></i> <?= $this->lang->line('payment_methods'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('list_of_added_payment_methods'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <!-- Payment Method Area -->
  <div class="row">
    <div class="hpanel hblue">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <h4>
              <strong><?= $this->lang->line('payment_methods'); ?>  </strong> &nbsp;
              <a href="<?= base_url('user-panel-laundry/payment-method/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a> </h4>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
            <a href="<?= base_url('user-panel-laundry/user-profile'); ?>" class="btn btn-primary"><i class="fa fa-left-arrow"></i> <?= $this->lang->line('go_to_profile'); ?> </a> 
          </div>
        </div>
      </div>

      <?php foreach($payments as $pay ): ?>
        <div class="panel-body">
              
              <div class="row">
                <div class="col-md-3"> <p><strong><?= $this->lang->line('payment_methods'); ?></strong></p> </div>
                <div class="col-md-7">  <p style="word-wrap: break-word;"><?= $pay['payment_method']; ?></p> </div>  
                <div class="col-md-2 text-right">
                  <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-laundry/payment-method/'). $pay['pay_id'];?>">
                    <i class="fa fa-pencil"></i>
                  </a>
                  <a class="btn btn-danger btn-circle btn-delete" id="<?=$pay['pay_id'];?>">
                    <i class="fa fa-trash"></i>
                  </a>
                </div>
              </div>
            <?php if($pay['brand_name'] != 'NULL' && $pay['brand_name'] != '') { ?>
              <div class="row">
                <div class="col-md-3"> <p><strong><?= $this->lang->line('payment_method_brand'); ?></strong></p> </div>
                <div class="col-md-8">  <p style="word-wrap: break-word;"><?= $pay['brand_name']; ?></p> </div>  
              </div>
            <?php } ?>
              <div class="row">
                <div class="col-md-3"> <p><strong><?= $this->lang->line('card_number'); ?></strong></p> </div>
                <div class="col-md-8">  <p style="word-wrap: break-word;"><?= $pay['card_number']; ?></p> </div>  
              </div>
            <?php if($pay['bank_name'] != 'NULL' && $pay['bank_name'] != '') { ?>
              <div class="row">
                <div class="col-md-3"> <p><strong><?= $this->lang->line('bank_name'); ?></strong></p> </div>
                <div class="col-md-8">  
                  <p style="word-wrap: break-word;"><?= $pay['bank_name']; ?></p>
                </div>  
              </div>
            <?php } ?>
            <?php if($pay['bank_address'] != 'NULL' && $pay['bank_address'] != '' && !is_null($pay['bank_address'])) { ?>
              <div class="row">
                <div class="col-md-3"> <p><strong><?= $this->lang->line('bank_address'); ?></strong></p> </div>
                <div class="col-md-8">  <p style="word-wrap: break-word;"><?= $pay['bank_address']; ?></p> </div>  
              </div>
            <?php } ?> 
            <?php if($pay['bank_short_code'] != 'NULL' && $pay['bank_short_code'] != '' && !is_null($pay['bank_short_code'])) { ?>
              <div class="row">
                <div class="col-md-3"> <p><strong><?= $this->lang->line('bank_short_code_swift_code'); ?></strong></p> </div>
                <div class="col-md-8">  <p style="word-wrap: break-word;"><?= $pay['bank_short_code']; ?></p> </div>  
              </div>
            <?php } ?> 
            <?php if($pay['expirty_date'] != 'NULL' && $pay['expirty_date'] != '/') { ?>
              <div class="row">
                <div class="col-md-3"> <p><strong><?= $this->lang->line('expiry_date'); ?></strong></p> </div>
                <div class="col-md-8">  <p style="word-wrap: break-word;"><?= date("m/y" , strtotime($pay['expirty_date'])); ?></p> </div>  
              </div> 
            <?php } ?>
            </div> 
        <br/>
      <?php endforeach; ?> 
    </div>
  </div>
  <!--End: Payment method Area -->
</div>

<script>
  $('.btn-delete').click(function () {            
    var id = this.id;            
    swal({                
      title: <?= json_encode($this->lang->line('are_you_sure'))?>,                
      text: "",                
      type: "warning",                
      showCancelButton: true,                
      confirmButtonColor: "#DD6B55",                
      confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,                
      cancelButtonText: <?= json_encode($this->lang->line('no'))?>,                
      closeOnConfirm: false,                
      closeOnCancel: false, 
    },                
    function (isConfirm) {                    
      if (isConfirm) {                        
        $.post('delete-payment-method', {id: id}, 
        function(res){
          if($.trim(res) == "success") {
            swal(<?= json_encode($this->lang->line('deleted'))?>, "", "success");                            
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal(<?= json_encode($this->lang->line('failed'))?>, "", "error");  }  
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");  }  
    });            
  });
</script>