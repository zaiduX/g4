<style> textarea { resize: none; } </style>
<style type="text/css">
  #attachment, .attachment {            position: absolute;            top: 0;            right: 0;            margin: 0;            padding: 0;            font-size: 20px;            cursor: pointer;            opacity: 0;            filter: alpha(opacity=0);        }
</style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
          <li><span><?= $this->lang->line('Laundry Claims'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('Claim Details'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-exclamation-triangle fa-2x text-muted"></i> <?= $this->lang->line('Claim Details'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Claim details and Provider response'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="hpanel hblue">
        <div class="panel-body">              
          <div class="col-md-12">

            <?php if($this->session->flashdata('error')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            
            <div class="row">
              <div class="form-group">
                <div class="col-md-5">
                  <div class="col-md-12 form-group" style="margin-bottom: 0px;">
                    <h3><?=$this->lang->line('Item Qunatity Claimed')?></h3>
                  </div>
                  <div class="col-md-8 form-group" style="margin-bottom: 0px;">
                    <label><?= $this->lang->line('Booking Items'); ?></label>
                  </div>
                  <div class="col-md-4 form-group text-center" style="margin-bottom: 0px;">
                    <label><?= $this->lang->line('Claim Quantity'); ?></label>
                  </div>
                  <?php 
                    $item_counts = explode(',',$claim_details['item_counts']);
                    $cat_ids = explode(',',$claim_details['cat_ids']);
                    $sub_cat_ids = explode(',',$claim_details['sub_cat_ids']);
                    for ($i=0; $i < sizeof($cat_ids); $i++) { 
                      if($item_counts[$i] > 0) { ?>
                        <div class="col-md-8 form-group" style="margin-bottom: 0px;">
                          <h5><?=strtoupper($this->api->get_laundry_category_details($cat_ids[$i])['cat_name']).' - '.ucwords($this->api->get_laundry_sub_category($sub_cat_ids[$i])['sub_cat_name'])?></h5>
                        </div>
                        <div class="col-md-4 form-group text-center" style="margin-bottom: 0px;">
                          <h5><?=$item_counts[$i]?></h5>
                        </div>
                  <?php } } ?>
                </div>
                <div class="col-md-7" style="border-left: 1px solid #225595">
                  <div class="col-md-12 form-group" style="margin-bottom: 0px;">
                    <h3><?=$this->lang->line('Claim Details')?></h3>
                  </div>
                  <div class="col-md-4 form-group">
                    <label class=""><?=$this->lang->line('Claim Type')?></label>
                  </div>
                  <div class="col-md-8 form-group">
                    <label><?=$claim_details['claim_type']?></label>
                  </div>
                  <div class="col-md-4 form-group">
                    <label class=""><?=$this->lang->line('Claim Description')?></label>
                  </div>
                  <div class="col-md-8 form-group">
                    <label><?=$claim_details['claim_desc']?></label>
                  </div>
                  <div class="col-md-4 form-group">
                    <label class=""><?=$this->lang->line('Claim image')?></label>
                  </div>
                  <div class="col-md-8 form-group">
                    <a class="btn btn-info btn-sm" target="_blank" href="<?=($claim_details['image_url']=='NULL')?base_url('resources/noimage.png'):base_url($claim_details['image_url'])?>"><i class="fa fa-image"></i> <?=$this->lang->line('Click to view')?></a>
                  </div>
                  <div class="col-md-4 form-group">
                    <label class=""><?=$this->lang->line('Claim document')?></label>
                  </div>
                  <div class="col-md-8 form-group">
                    <a class="btn btn-info btn-sm" target="_blank" href="<?=($claim_details['doc_url']=='NULL')?base_url('resources/noimage.png'):base_url($claim_details['doc_url'])?>"><i class="fa fa-file"></i> <?=$this->lang->line('Click to view')?></a>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <form action="<?=base_url('user-panel-laundry/provider-claim-response')?>" method="post" class="form-horizontal" id="addClaim" enctype="multipart/form-data"> 
                <input type="hidden" name="claim_id" value="<?=$claim_details['claim_id']?>">
                <div class="col-md-12" style="margin-bottom: 0px;">
                  <h3><?=$this->lang->line('Provider Response')?></h3>
                </div>
                <div class="col-md-3">
                  <label class=""><?=$this->lang->line('Provider Remark')?></label>
                </div>
                <div class="col-md-5">
                  <textarea id="claim_solution_desc" name="claim_solution_desc" class="form-control" placeholder="<?=$this->lang->line('Enter solution remark here...')?>"></textarea>
                </div>
                <div class="col-md-2">
                  <select name="claim_status" id="claim_status" class="form-control select2">
                    <option value=""><?=$this->lang->line('Select status')?></option>
                    <option value="open"><?=$this->lang->line('Open')?></option>
                    <option value="in_progress"><?=$this->lang->line('In-progress')?></option>
                    <option value="standby"><?=$this->lang->line('Standby')?></option>
                    <option value="closed"><?=$this->lang->line('Closed')?></option>
                  </select>
                </div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-info"><?=$this->lang->line('save')?></button>
                  <a href="<?= base_url('user-panel-laundry/provider-laundry-claims'); ?>" class="btn btn-primary"><?= $this->lang->line('back'); ?></a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function avatar_name(e){ if(e.target.files[0].name !="") { $("#avatar_name").removeClass('hidden'); }}
  function cover_name(e){ if(e.target.files[0].name !="") { $("#cover_name").removeClass('hidden'); }}
  $("#upload_image").on('click', function(e) { e.preventDefault(); $("#image_url").trigger('click'); });
  $("#upload_document").on('click', function(e) { e.preventDefault(); $("#doc_url").trigger('click'); });
  $("#addClaim").keypress(function(event) { return event.keyCode != 13; });
    
  $("#addClaim").validate({
    ignore: [],
    rules: {
      claim_type: { required: true, },      
      claim_desc: { required: true, },
      image_url: { accept:"jpg,png,jpeg,gif" },
    }, 
    messages: {
      claim_type: { required: "<?=$this->lang->line('Select claim type')?>", },
      claim_desc: { required: "<?=$this->lang->line('Enter short description')?>", },
      image_url: { accept: "<?=$this->lang->line('only_image_allowed')?>", },
    }
  });
</script>


