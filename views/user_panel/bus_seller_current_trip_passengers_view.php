    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                  <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
                </a>
                <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-current-trips'; ?>"><?= $this->lang->line('Current Trips'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Passenger Details'); ?></span></li>
                </ol>
                </div>
                <h2 class="font-light m-b-xs"> <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Passenger Details'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('Manage Trip Passengers &amp; Payments'); ?></small> 
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="=col-lg-12">
                                 <h3 class="stat-label" style='color: #3498db'><?= $this->lang->line('Trip Details'); ?>                                 
                                 </h3>
                            </div>
                            <br/>
                            <?php require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php"); ?>
                            <div class="col-md-6">
                                <h5><lable style='color: #3498db'> <?=$this->lang->line('Source')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['source_point']?></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><lable style='color: #3498db'> <?=$this->lang->line('Destination')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['destination_point']?></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><lable style='color: #3498db'> <?=$this->lang->line('Journey Date')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['journey_date']?></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><lable style='color: #3498db'> <?=$this->lang->line('Departure Time')?></lable>&nbsp;:&nbsp;<?=$master_booking_details[0]['trip_start_date_time']?></h5>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <h3 class="stat-label" style='color: #3498db'><?= $this->lang->line('Passenger Details'); ?>                                 
                                 </h3>
                        </div>
                            <table id="example13" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <th><?= $this->lang->line('Ticket ID'); ?></th>
                                    <th><?= $this->lang->line('Name'); ?></th>
                                    <th><?= $this->lang->line('Total Seats'); ?></th>
                                    <th><?= $this->lang->line('bus_seat_type'); ?></th>
                                    <th><?= $this->lang->line('Total_Price'); ?></th>
                                    <th><?= $this->lang->line('Balance Amount'); ?></th>
                                    <th><?= $this->lang->line('Contact'); ?></th>
                                    <th><?= $this->lang->line('Gender'); ?></th>
                                    <th><?= $this->lang->line('Payment'); ?></th>
                                </thead>
                                <tbody>
                                    <?php foreach ($master_booking_details as $seats) { ?>
                                    <tr>
                                        <td><?=$seats['ticket_id']?></td>
                                        <td>
                                            <?php $name = $this->api->get_ticket_seat_details($seats['ticket_id']);
                                            echo $name[0]['firstname']." ".$name[0]['lastname'];
                                             if(sizeof($name)>1)
                                             {
                                                echo "<button style='color: #3498db' type='button' class='btn btn-link' data-toggle='modal' data-target='#".$name[0]['seat_id']."'>".$this->lang->line('...More')."</button>";
                                             } 
                                            ?>
                                            <div class="modal fade" id="<?=$name[0]['seat_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel"><?=$this->lang->line('More Seat Details')?></h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <table class="table" width="100%">
                                                        <thead>
                                                            <th><?= $this->lang->line('Ticket ID'); ?></th>
                                                            <th><?= $this->lang->line('Name'); ?></th>
                                                            <th><?= $this->lang->line('Gender'); ?></th>
                                                            <th><?= $this->lang->line('Contact'); ?></th>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($name as $seat) { ?>
                                                            <tr>
                                                                <td><?=$seat['ticket_id']?></td>
                                                                <td><?=$seat['firstname']. ' ' .$seat['lastname']?></td>
                                                                <td><?=($seat['gender']=='M')?'Male':'Female'?></td>
                                                                <td><?=$this->api->get_country_code_by_id($seat['country_id']).$seat['mobile']?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                        <td><?=sizeof($name)?></td>
                                        <td><?=$seats['seat_type']?></td>
                                        <td><?=$seats['ticket_price'].$seats['currency_sign']?></td>
                                        <td><?=$seats['balance_amount'].$seats['currency_sign']?></td>
                                        <td><?=$this->api->get_country_code_by_id($name[0]['country_id']).$name[0]['mobile']?></td>
                                        <td><?=($name[0]['gender']=='M')?'Male':'Female'?></td>
                                        <td>
                                        <div style="display: inline-flex;">
                                            <?php if($seats['complete_paid'] == 0 && ($seats['payment_mode'] == 'online' || 'cod')) { ?>
                                                <form action="<?=base_url('user-panel-bus/bus-ticket-payment-seller')?>" method="post">
                                                  <input type="hidden" id="ticket_id" name="ticket_id" value="<?=$seats['ticket_id'] ?>">
                                                  <button type="submit" class="btn btn-outline btn-warning" ><i class="fa fa-check"></i> <?= $this->lang->line('complete_payment'); ?></button>
                                                </form>&nbsp;
                                            <?php }else{ ?>
                                               <form action="<?=base_url('user-panel-bus/bus-work-room')?>" method="post">
                                                <input type="hidden" name="ticket_id" value="<?=$seats['ticket_id'] ?>">
                                                <button type="submit" class="btn btn-outline btn-success" ><i class="fa fa-check"> </i> <?= $this->lang->line('workroom'); ?> </button>
                                               </form>&nbsp;
                                            <?php } ?>
                                            <?php if($seats['complete_paid'] != 0 && ($seats['payment_mode'] != 'online' || 'cod')) { ?>
                                                <button id="<?=$seats['ticket_id']?>" class="btn btn-outline btn-info" data-toggle="modal" data-target="#myModal<?=$seats['ticket_id']?>"><i class="fa fa-check"></i> <?= $this->lang->line('Print Ticket'); ?> </button>
                                                <div class="modal fade" id="myModal<?=$seats['ticket_id']?>" role="dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title"><?= $this->lang->line('Print Ticket'); ?></h4>
                                                            </div>
                                                            <div class="modal-body" id="printThis<?=$seats['ticket_id']?>">
                                                                <div style="width:  340px; height: 80px; border: dashed 1px #225595; text-align: center; padding-top: 2px">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-6" style="margin-left:-30px;">
                                                                            <img src="<?=($operator_details['avatar_url']=='' || $operator_details['avatar_url'] == 'NULL')?base_url('resources/no-image.jpg'):base_url($operator_details['avatar_url'])?>"     style="border: 1px solid #3498db; width: 120px; height: 70px;">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <img src="<?= base_url('resources/images/dashboard-logo.jpg') ?>" style="width: 160px; height: auto;" /><br />
                                                                        </div>
                                                                    </div>
                                                                   <!--  <h5><?= $this->lang->line('slider_heading1'); ?></h5> -->
                                                                </div>
                                                                <div style="width: 340px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                                                                    <h5 style="color: #225595; text-decoration: underline;">
                                                                    <i class="fa fa-arrow-up"></i> <?= $this->lang->line('Ownward_Trip'); ?> <br />
                                                                    <strong style="float: right; padding-right: 2px; margin-top: -15px"><?= $this->lang->line('Ticket ID'); ?>: #<?=$seats['ticket_id']?></strong> 
                                                                    <br />
                                                                   <?php QRcode::png($seats['ticket_id'], $seats['ticket_id'].".png", "L", 2, 2); 
                                                                    $img_src = $_SERVER['DOCUMENT_ROOT']."/".$seats['ticket_id'].'.png';
                                                                    $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$seats['ticket_id'].'.png';
                                                                    if(rename($img_src, $img_dest));
                                                                    ?>
                                                                    <img src="<?php echo base_url('resources/ticket-qrcode/').$seats['ticket_id'].'.png'; ?>" style="float: right; width: 84px; padding-right: 2px; height: 84px; margin-top: -15px" />
                                                                    </h5>
                                                                    <h6 style="margin-top:-15px;"><strong><?= $this->lang->line('Source'); ?>:</strong> <?= $seats['source_point'] ?> - <?= $seats['pickup_point'] ?></h6>
                                                                    <h6><strong><?= $this->lang->line('Destination'); ?>:</strong> <?= $seats['destination_point'] ?> - <?= $seats['drop_point'] ?></h6>
                                                                    <h6><strong><?= $this->lang->line('Journey Date'); ?>:</strong> <?= $seats['journey_date'] ?></h6>
                                                                    <h6><strong><?= $this->lang->line('Departure Time'); ?>:</strong> <?= $seats['trip_start_date_time'] ?></h6>
                                                                    <h6><strong><?= $this->lang->line('Passenger Details'); ?>:</strong></h6>
                                                                    <?php 
                                                                      $ticket_seat_details = $this->api->get_ticket_seat_details($seats['ticket_id']);
                                                                    ?>
                                                                    <table name="<?=$seats['ticket_id']?>" style="width: 100%">
                                                                        <thead>
                                                                            <th><?= $this->lang->line('Name'); ?></th>
                                                                            <th><?= $this->lang->line('Gender'); ?></th>
                                                                            <th><?= $this->lang->line('Contact'); ?></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php foreach ($name as $seat) { ?>
                                                                              <tr>
                                                                                <td><?=$seat['firstname']. ' ' .$seat['lastname']?></td>
                                                                                <td><?=($seat['gender']=='M')?'Male':'Female'?></td>
                                                                                <td><?=$this->api->get_country_code_by_id($seat['country_id']).$seat['mobile']?></td>
                                                                              </tr>
                                                                            <?php } ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer" style="display: flex;">
                                                                <button style="float: right;" class="btn btn-sm pull-right btn-default" id="btnPrint<?=$seats['ticket_id']?>"><?= $this->lang->line('Print Ticket'); ?></button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    $("#btnPrint<?= $seats['ticket_id'] ?>").click(function () {
                                                    $('#printThis<?= $seats['ticket_id'] ?>').printThis();
                                                });
                                              </script>
                                                  &nbsp;
                                            <?php } ?>
                                            <button type="text" class="btn btn-outline btn-danger cancel_ticket" value="<?=$seats['ticket_id'] ?>" ><i class="fa fa-times"></i> <?= $this->lang->line('cancel'); ?> </button>
                                        </div>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>             
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-6">
                            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-current-trips'; ?>" class="btn btn-info btn-outline " ><?= $this->lang->line('back'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
     $(function () {
        $('#example13').dataTable( {
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            buttons: [
                {extend: 'copy',className: 'btn-sm'},
                {extend: 'csv',title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'print',className: 'btn-sm'}
            ]
        });
    });

    $('.cancel_ticket').click(function (e) {
        e.preventDefault();
        var ticket_id = $(this).attr("value");
            swal(
            {
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('To Cancel This Ticket'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('Yes, Cancel This Ticket.'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                $.ajax({
                    url: "<?=base_url('user-panel-bus/delete-bus-ticket-seller')?>",
                    type: 'POST' ,
                    data: {ticket_id: ticket_id},
                    success: function(data)
                    {   
                        console.log(data);
                        if(data == 'cancel_successfull') {
                            swal("<?= $this->lang->line('cancelled'); ?>", "<?= $this->lang->line('Ticket has been cancelled!'); ?>", "success");
                            setTimeout(function() { window.location.reload("<?=base_url('user-panel-bus/seller-upcoming-trips/'.$master_booking_details[0]['unique_id'])?>"); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While cancelling ticket!'); ?>", "error");
                            setTimeout(function() { window.location.reload("<?=base_url('user-panel-bus/seller-upcoming-trips/'.$master_booking_details[0]['unique_id'])?>"); }, 2000);
                        }
                    }
                });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error"); }
            });        
    });       
</script>