<style>
  #map_wrapper {
      height: 300px;
  }
  #map_canvas {
      width: 100%;
      height: 100%;
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
          <li class="active"><span><?= $this->lang->line('drivers'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('drivers'); ?> &nbsp;&nbsp;&nbsp;
        <a href="<?= $this->config->item('base_url') . 'user-panel/add-driver'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?> </a>
        <a href="<?= $this->config->item('base_url') . 'user-panel/driver-inactive-list'; ?>" class="btn btn-outline btn-warning" ><i class="fa fa-times"></i> <?= $this->lang->line('inactive_drivers'); ?> </a></h2>
      <small class="m-t-md"><?= $this->lang->line('active_drivers_list'); ?></small> 
    </div>
  </div>
</div>
   
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel">
        <div class="panel-body">
          <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
            <thead>
              <tr>
                <th><?= $this->lang->line('driver_id'); ?></th>
                <th><?= $this->lang->line('driver_name'); ?></th>
                <th><?= $this->lang->line('email_address'); ?></th>
                <th><?= $this->lang->line('contact'); ?></th>
                <th><i class="fa fa-thumbs-up" title="<?= $this->lang->line('assigned_ord'); ?>"></i></th>
                <th><i class="fa fa-check" title="<?= $this->lang->line('accepted_ord'); ?>"></i></th>
                <th><i class="fa fa-recycle" title="<?= $this->lang->line('inprogress_ord'); ?>"></i></th>
                <th><i class="fa fa-times" title="<?= $this->lang->line('rejected_ord'); ?>"></i></th>
                <th><i class="fa fa-external-link-square" title="<?= $this->lang->line('pickup_ord'); ?>"></i></th>
                <th><i class="fa fa-check-square" title="<?= $this->lang->line('delivered_ord'); ?>"></i></th>
                <th><i class="fa fa-road" title="<?= $this->lang->line('inprogress_working_distance_ord'); ?>"></i></th>
                <th style="text-align: center;"><?= $this->lang->line('action'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($drivers as $dr) { ?>
              <tr>
                <td>GD-<?= $dr['cd_id'] ?></td>
                <td><a href="<?= $this->config->item('base_url') . 'user-panel/view-driver/'.$dr['cd_id']; ?>" title="View details"><?= $dr['first_name'] . ' ' . $dr['last_name'] ?></a></td>
                <td><?= $dr['email'] ?></td>
                <td><?= $dr['mobile1'] ?></td>
                <td><?= $this->user->get_order_count($dr['cd_id'],'assign') ?></td>
                <td><?= $this->user->get_order_count($dr['cd_id'],'accept') ?></td>
                <td><?= $this->user->get_order_count($dr['cd_id'],'in_progress') ?></td>
                <td><?= $this->user->get_order_count($dr['cd_id'],'rejected') ?></td>
                <td><?= $this->user->get_order_count($dr['cd_id'],'in_progress') ?></td>
                <td><?= $this->user->get_order_count($dr['cd_id'],'delivered') ?></td>
                <td><?= $this->user->get_inprogress_working_distance($dr['cd_id']) ?></td>
                <td>
                  <div style="display: flex;">
                    <a title="<?= $this->lang->line('view_drivers'); ?>" class="btn btn-warning btn-sm" href="<?= $this->config->item('base_url') . 'user-panel/view-driver/'.$dr['cd_id']; ?>"><i class="fa fa-search"></i></a> &nbsp;
                    <form action="edit-driver" method="post">
                      <input type="hidden" name="cd_id" value="<?= $dr['cd_id'] ?>">
                      <button class="btn btn-info btn-sm" type="submit" title="<?= $this->lang->line('edit'); ?>"><i class="fa fa-pencil"></i></button>
                    </form> &nbsp;
                    <button class="btn btn-danger btn-sm driverinactivatealert" id="<?= $dr['cd_id'] ?>" title="<?= $this->lang->line('inactivate'); ?>"><i class="fa fa-times"></i></button>
                  </div>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div><br />
      <div class="hpanel">
        <div id="map_wrapper">
            <div id="map_canvas" class="mapping"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?callback=initialize&key=AIzaSyCIj8TMFnqnxpvima7MDrJiySvGK-UVLqw";
    document.body.appendChild(script);
  });

  function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
      mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
      <?php foreach ($drivers as $dr) { if($dr['latitude'] != 'NULL' && $dr['longitude'] != 'NULL') {?>
          ["<?= $dr['country_name'] ?>", <?= $dr['latitude'] ?>,<?= $dr['longitude'] ?>],
      <?php } } ?>
    ];
                        
    // Info Window Content
    var infoWindowContent = [
      <?php foreach ($drivers as $dr) { if($dr['latitude'] != 'NULL' && $dr['longitude'] != 'NULL') {?>
          ["<div class='info_content'>" +
          "<h3><?= $dr['first_name'] . ' ' . $dr['last_name'] ?></h3>" +
          "<p>Mobile: <?= $dr['mobile1'] ?></p>" +        '</div>'],
      <?php } } ?>
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
      var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
      bounds.extend(position);
      marker = new google.maps.Marker({
        position: position,
        map: map,
        title: markers[i][0]
      });
      
      // Allow each marker to have an info window    
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infoWindow.setContent(infoWindowContent[i][0]);
          infoWindow.open(map, marker);
        }
      })(marker, i));

      // Automatically center the map fitting all markers on the screen
      map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
      this.setZoom(7);
      google.maps.event.removeListener(boundsListener);
    }); 
  }
</script>