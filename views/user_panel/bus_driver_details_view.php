<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/driver-list'; ?>"><?= $this->lang->line('bus_drivers'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('view_drivers'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('driver_profile'); ?></h2>
            <small class="m-t-md"><?= $this->lang->line('driver_personal_and_work_details'); ?></small>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-lg-4" >
            <div class="hpanel hgreen" >
                <div class="panel-heading">
                    <h4><strong><?= $this->lang->line('profile_info'); ?></strong></h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                        <?php if($drivers[0]['avatar'] == "NULL"): ?>
                            <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b avatar" alt="avatar" />
                            <?php else: ?>
                                <img src="<?= base_url($drivers[0]['avatar']); ?>" class="img-thumbnail m-b avatar" alt="avatar" />
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">
                            <h4 style="text-align: left">
                                <a><?= strtoupper($drivers[0]['first_name']) ?><br /><br />
                                <?php if($drivers[0]['online_status'] == 1 ) { ?>
                                    <span class="pull-left btn btn-success btn-xs"><?= $this->lang->line('online'); ?></span>
                                <?php } else { ?>
                                    <span class="pull-left btn btn-danger btn-xs"><?= $this->lang->line('offline'); ?></span>
                                <?php } ?>
                            </h4 style="text-align: left">
                        </div>
                        <div class="col-md-2">
                            <a href="<?= base_url('user-panel-bus/edit-driver/').$drivers[0]['cd_id']; ?>" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></a>
                        </div>
                    </div>


                        <p><i class="fa fa-envelope"></i> <?= $this->lang->line('eauil'); ?> <?= $drivers[0]['email'] ?></p>
                        <p><i class="fa fa-phone"></i> <?= $this->lang->line('contact_1'); ?> <?= $drivers[0]['mobile1'] ?></p>
                        <p><i class="fa fa-phone"></i> <?= $this->lang->line('contact_2'); ?> <?= $drivers[0]['mobile2'] ?></p>
                        
                        <p><i class="fa fa-map"></i> <?= $this->lang->line('current_location'); ?></p>
                    <div class="border-right border-left">
                        <iframe
                          frameborder="0" style="border:0"
                          src="<?php echo "https://www.google.com/maps/embed/v1/place?key=AIzaSyCIj8TMFnqnxpvima7MDrJiySvGK-UVLqw
                            &q=".$drivers[0]['latitude'].",".$drivers[0]['longitude'] ?>" allowfullscreen>
                        </iframe>
                    </div>
                </div>
                <div class="panel-body">
                    <dl>
                        <dt><?= $this->lang->line('categories'); ?></dt>
                        <?php
                            for ($i=0; $i < sizeof($category_name); $i++) { ?>
                                <span class="btn btn-info btn-xs" style="display: inline-block;"><?= $category_name[$i] ?></span>
                        <?php } ?>
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="hpanel">
                <div class="hpanel">
                <div class="panel-heading">
                    <h4><strong><?=$this->lang->line('recent trips')?></strong></h4>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#step-1"><?= $this->lang->line('Current Trips'); ?></a></li>
                    <li class=""><a data-toggle="tab" href="#step-2"><?= $this->lang->line('upcoming trips'); ?></a></li>
                    <li class=""><a data-toggle="tab" href="#step-3"><?= $this->lang->line('Previous Trips'); ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="step-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th><?= $this->lang->line('Trip id'); ?></th>
                                            <th><?= $this->lang->line('Source'); ?></th>
                                            <th><?= $this->lang->line('Destination'); ?></th>
                                            <th><?= $this->lang->line('Journey Date'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($i=0; $i < sizeof($current); $i++) {  ?>
                                            <tr>
                                                <td><?= $current[$i]['trip_id'] ?></td>
                                                <td><?= $current[$i]['source_point'] ?></td>
                                                <td><?= $current[$i]['destination_point'] ?></td>
                                                <td><?= $current[$i]['journey_date'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="step-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                        <th><?= $this->lang->line('Trip id'); ?></th>
                                        <th><?= $this->lang->line('Source'); ?></th>
                                        <th><?= $this->lang->line('Destination'); ?></th>
                                        <th><?= $this->lang->line('Journey Date'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($i=0; $i < sizeof($upcoming); $i++) {  ?>
                                            <tr>
                                                <td><?= $upcoming[$i]['trip_id'] ?></td>
                                                <td><?= $upcoming[$i]['source_point'] ?></td>
                                                <td><?= $upcoming[$i]['destination_point'] ?></td>
                                                <td><?= $upcoming[$i]['journey_date'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="step-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th><?= $this->lang->line('Trip id'); ?></th>
                                            <th><?= $this->lang->line('Source'); ?></th>
                                            <th><?= $this->lang->line('Destination'); ?></th>
                                            <th><?= $this->lang->line('Journey Date'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($i=0; $i < sizeof($previous); $i++) {  ?>
                                            <tr>
                                                <td><?= $previous[$i]['trip_id'] ?></td>
                                                <td><?= $previous[$i]['source_point'] ?></td>
                                                <td><?= $previous[$i]['destination_point'] ?></td>
                                                <td><?= $previous[$i]['journey_date'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>