<style type="text/css">
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dashboard'); ?>
                        </a></li>
                        <?php
                            if ($details['response'] == 'request') { ?>
                                <li><a href="<?= $this->config->item('base_url') . 'user-panel/my-withdrawals-request'; ?>"><span><?= $this->lang->line('withdrawal_requests'); ?>
                                </span></a></li>
                        <?php }
                            if ($details['response'] == 'accept') { ?>
                                <li><a href="<?= $this->config->item('base_url') . 'user-panel/my-withdrawals-accepted'; ?>"><span><?= $this->lang->line('accepted'); ?>
                                </span></a></li>
                        <?php }
                            if ($details['response'] == 'reject') { ?>
                                <li><a href="<?= $this->config->item('base_url') . 'user-panel/my-withdrawals-rejected'; ?>"><span><?= $this->lang->line('rejected'); ?>
                                </span></a></li>
                        <?php } ?>
                        <li class="active"><span><?= $this->lang->line('request_details'); ?>
                        </span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   <i class="fa fa-table fa-2x text-muted"></i> <?= $this->lang->line('withdrawal'); ?>
                </h2>
                <small class="m-t-md"> <?= $this->lang->line('details'); ?></small>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <form method="post" class="form-horizontal" action="add-driver-details" enctype="multipart/form-data" id="driverAdd">
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <?php if($this->session->flashdata('error')):  ?>
                                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                                    <?php endif; ?>
                                    <?php if($this->session->flashdata('success')):  ?>
                                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                                    <?php endif; ?>  
                                </div>   
                            </div>
                            <div class="form-group">
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-3 text-right"><?= $this->lang->line('requested_amount'); ?>
                                        </label>
                                        <p class="col-md-8"><?= $details['currency_code'] . ' ' . $details['amount'] ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 text-right"><?= $this->lang->line('request_date_time'); ?>
                                        </label>
                                        <p class="col-md-8"><?= date('d-M-Y h:i:s A',strtotime($details['req_datetime'])) ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 text-right"><?= $this->lang->line('request_status'); ?></label>
                                        <p class="col-md-8">
                                        	<?php 
                                        		if($details['response'] == 'request') {
                                        			echo $this->lang->line('requested');
                                        		} 
                                        		if($details['response'] == 'accept') {
                                        			echo $this->lang->line('accepted');
                                        		} 
                                        		if($details['response'] == 'reject') {
                                        			echo $this->lang->line('rejected');
                                        		} 
                                        	?>
										</p>
                                    </div>
                                    <?php if($details['response'] != 'NULL') { ?> 
                                        <div class="form-group">
                                            <label class="col-md-3 text-right"><?= $this->lang->line('response_date_time'); ?></label>
                                            <p class="col-md-8"><?= date('d-M-Y h:i:s A',strtotime($details['response_datetime'])) ?></p>
                                        </div>
                                    <?php } ?>
                                    <?php if($details['transfer_account_type'] != 'NULL') { ?> 
                                        <div class="form-group">
                                            <label class="col-md-3 text-right"><?= $this->lang->line('account_transfer_type'); ?></label>
                                            <p class="col-md-8"><?= ucwords(str_replace('_',' ',$details['transfer_account_type'])) ?></p>
                                        </div>
                                    <?php } ?>
                                    <?php if($details['transfer_account_number'] != 'NULL') { ?> 
                                        <div class="form-group">
                                            <label class="col-md-3 text-right"><?= $this->lang->line('account_number'); ?></label>
                                            <p class="col-md-8"><?= $details['transfer_account_number'] ?></p>
                                        </div>
                                    <?php } ?>
                                    <?php if($details['transaction_id'] != 'NULL') { ?> 
                                        <div class="form-group">
                                            <label class="col-md-3 text-right"><?= $this->lang->line('mobile_transfer_number'); ?></label>
                                            <p class="col-md-8"><?= $details['transaction_id'] ?></p>
                                        </div>
                                    <?php } ?>
                                    <?php if($details['amount_transferred'] != 'NULL') { ?> 
                                        <div class="form-group">
                                            <label class="col-md-3 text-right"><?= $this->lang->line('transfered_amount'); ?></label>
                                            <p class="col-md-8"><?= $details['currency_code'] . ' ' . $details['amount_transferred'] ?></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $("#driverAdd")
            .validate({
                rules: {
                    first_name: { required : true },
                    last_name: { required : true },
                    email: { required : true, email : true },
                    mobile1: { required : true, number : true },
                    mobile2: { number : true },
                    password: { required : true, minlength : 5, maxlength : 16 },
                    rePassword: { required : true, minlength : 5, maxlength : 16, equalTo: "#password"},
                },
                messages: {
                    first_name: { required : <?= json_encode($this->lang->line('enter_first_name')); ?>, },
                    last_name: { required : <?= json_encode($this->lang->line('enter_last_name')); ?>, },
                    email: { required : <?= json_encode($this->lang->line('enter_email_address'));?>, },
                    mobile1: { 
                        required : <?= json_encode($this->lang->line('enter_mobile_number')); ?>,
                        number : <?= json_encode($this->lang->line('number_only'));?>, 
                    },
                    mobile2: { 
                        number : <?= json_encode($this->lang->line('number_only'));?>, 
                    },
                    password: { 
                        required : <?= json_encode($this->lang->line('enter_password'));?>,
                        minlength : <?= json_encode($this->lang->line('6_characters'));?>,
                        maxlength : <?= json_encode($this->lang->line('max_16_characters'));?>,
                    },
                    rePassword: { 
                        required : <?= json_encode($this->lang->line('enter_rewrite_password'));?>,
                        minlength : <?= json_encode($this->lang->line('6_characters'));?>,
                        maxlength : <?= json_encode($this->lang->line('max_16_characters'));?>,
                        equalTo : <?= json_encode($this->lang->line('password_not_matched'));?>,
                    },
                },
            });
    </script>