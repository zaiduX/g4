<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
    display: none;
  }
  input[type=checkbox] { display: none; }
  input[type=checkbox] + label {
    position: relative;
    height: 30px;
    width: 32px;
    display: block;
    transition: box-shadow 0.2s, border 0.2s;
    box-shadow: 0 0 1px #FFF;/* Soften the jagged edge */
    cursor: pointer;
  }
  input[type=checkbox] + label:hover,
  input[type=checkbox]:checked + label {
    border: solid 2px #1b5497;
    box-shadow: 0 0 1px #1b5497;
    border-radius: 5px;
  }
  input[type=checkbox]:checked + label:after {
    /* content: '\2714'; */
    /*content is required, though it can be empty - content: '';*/
    height: 1em;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    margin: auto;
    color: green;
    line-height: 1;
    font-size: 15px;
    text-align: center;
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 20px 0px rgba(0,0,0,2.0);
    background-color: #FFF;
    border: 3px outset #0AD909 !important;
  }
  .btn-sm { padding: 3px 5px !important; margin-bottom: 3px !important; }
  .truncate {
    width: 230px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  .truncate:hover {
    overflow: visible; 
    white-space: normal;
    width: auto;
  }
</style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Previous Ticket'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-ticket fa-2x text-muted"></i> <?= $this->lang->line('Previous Ticket'); ?></h2>
      <small class="m-t-md"><?= $this->lang->line('Previous ticket details'); ?></small> 
    </div>
  </div>
</div>

  <div class="content">
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="hpanel">
          <div class="panel-body" style="padding: 5px 20px 10px 20px;">
            <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
            <?php endif; ?>
            
            <?php
              $src_points = array();
              $dest_points = array();
              foreach ($trip_list as $source) { 
                array_push($src_points, $source['source_point']);
                array_push($dest_points, $source['destination_point']);
              }
              $src_points = array_unique($src_points);
              $dest_points = array_unique($dest_points);
            ?>

            <div class="row" id="basicSearch">
              <form action="<?= base_url('user-panel-bus/buyer-previous-trips') ?>" method="post"> 
                <input type="hidden" name="filter" value="filter">
                <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                      <label class="text-left"><?= $this->lang->line('From'); ?></label>
                      <select class="form-control select2" name="trip_source" id="trip_source" style="box-shadow: 0 0 5px #3498db;">
                          <option value=""><?= $this->lang->line('Starting city'); ?></option>
                          <?php foreach ($src_points as $src) { ?>
                              <option value="<?= $src ?>" <?php if(isset($trip_source) && $trip_source==$src){ echo "selected"; } ?> > <?= $src ?></option>
                          <?php } ?> 
                      </select>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                      <label class="text-left"><?= $this->lang->line('To'); ?></label>
                      <select class="form-control select2" name="trip_destination" id="trip_destination" style="box-shadow: 0 0 5px #3498db;">
                          <option value=""><?= $this->lang->line('Destination city'); ?></option>
                           <?php foreach ($dest_points as $dest) { ?>
                              <option value="<?= $dest ?>" <?php if(isset($trip_destination) && $trip_destination==$dest){ echo "selected"; } ?> ><?= $dest ?></option>
                          <?php } ?>
                      </select>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                      <label class="text-left"><?= $this->lang->line('Journey Date'); ?></label>
                      <div class="input-group date" data-provide="datepicker" data-date-start-date="0d" data-date-end-date="" style="box-shadow: 0 0 5px #3498db;">
                        <input type="text" class="form-control" id="journey_date" name="journey_date" value="<?php if(isset($journey_date)){ echo $journey_date; } ?>" />
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                      </div> 
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4">
                      </br>
                      <input type="submit" class="btn btn-success form-control" value="<?= $this->lang->line('search'); ?>" style="min-height: 40px; box-shadow: 0 0 5px #225595;" >
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                      </br>
                      <a href="<?=base_url('user-panel-bus/buyer-previous-trips')?>">
                        <input type="button" class="btn btn-success form-control" value="<?=$this->lang->line('reset');?>" style="min-height: 40px; box-shadow: 0 0 5px #225595;">
                      </a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            
          </div>
          
          <div class="panel-body" style="padding-top: 12px; padding-bottom: 12px;">
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <table id="orderTableData" class="table">
                  <thead><tr class="hidden"><th class="hidden">ID</th><th></th></tr></thead>
                  <tbody>
                    <?php require_once($_SERVER['DOCUMENT_ROOT']."/resources/phpqrcode/qrlib.php"); ?>
                    <?php foreach ($trip_list as $tList) { 
                      $customer_details = $this->api->get_user_details($tList['cust_id']);
                      //echo json_encode($customer_details);
                      $operator_details = $this->api->get_bus_operator_profile($tList['operator_id']);
                      //echo json_encode($operator_details);
                      $bus_details = $this->api->get_operator_bus_details($tList['bus_id']);
                      //echo json_encode($bus_details);
                      $trip_master_details = $this->api->get_trip_master_details($tList['trip_id']);
                      //echo json_encode($trip_master_details);
                      if($tList[0]=="safe")
                      {
                    ?>
                      <tr>
                        <td class="hidden" style="line-height: 15px;"><?=$tList['trip_id'];?></td>
                        <td class="hoverme" style="line-height: 15px; border: 1px solid #3498db; padding: 0px;">
                          <div class="hpanel" style="line-height: 15px; margin-bottom: 0px !important;">
                            <div class="panel-body" style="line-height: 15px; padding-bottom: 0px; padding-top: 6px; padding-bottom: 10px;">
                              <div class="row" style="line-height: 15px;">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="height:75px">
                                  <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="row">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                        <img style="max-width: 14px; height: auto;" src="<?=base_url('resources/images/icon-connection-top@2x.png')?>" style="float: left;">
                                      </div>
                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px; height: 31px;">
                                        <h6 style="margin-top:5px; margin-bottom:0px; margin-left: 10px"><strong><?=$trip_master_details[0]['trip_depart_time']?></strong></h5>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                                        <h6 style="margin-top:0px; height:22px; margin-bottom:3px; font-size: 14px; display: inline-flex;"><i class="fa fa-clock-o" style="font-size: 20px;"></i>&nbsp;&nbsp;<?=$trip_master_details[0]['trip_duration']?><?=$this->lang->line('Hrs')?></h6>
                                      </div>
                                      
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                        <img style="max-width: 14px; height: auto;" src="<?=base_url('resources/images/icon-connection-bottom@2x.png')?>" style="float: left;">
                                      </div>

                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px; height: 31px;">
                                        <h6 style="margin-top:10px; margin-bottom:0px; margin-left: 10px"><?=date('H:i', strtotime($trip_master_details[0]['trip_depart_time'])+(60*60*$trip_master_details[0]['trip_duration']))?></h6>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="row">
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                          <i class="fa fa-map-marker" style="font-size: 25px; color: #23b122;padding: 0px 4px;"></i>
                                        </div>
                                        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                          <h5 class="truncate"><font color="#3498db"><?=$tList['source_point']?></font></h5>
                                        </div>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 2px; padding-right: 0px;">
                                          <span style="font-size: 32px; color: #3498db">|</span>
                                        </div>
                                        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                          <h5 class="m-b-xs" style="margin-left: 5px;"> 
                                            <?php 
                                              $pickup_lat_long_string = $this->api->get_loc_lat_long_by_id($tList['source_point_id']);
                                              $pickup_lat_long_array = explode(',',$pickup_lat_long_string);
                                              //echo json_encode($pickup_lat_long_array);
                                              $drop_lat_long_string = $this->api->get_loc_lat_long_by_id($tList['destination_point_id']);
                                              $drop_lat_long_array = explode(',',$drop_lat_long_string);
                                              //echo json_encode($drop_lat_long_array);
                                            ?>
                                            <font color="#3498db">
                                              <?php if(sizeof($pickup_lat_long_array) == 2 && sizeof($drop_lat_long_array) == 2) { ?>
                                                <?php echo round($this->api->GetDrivingDistance($pickup_lat_long_array[0],$pickup_lat_long_array[1],$drop_lat_long_array[0],$drop_lat_long_array[1]),2) . ' KM'; ?>
                                              <?php } else { echo 'N/A'; } ?>
                                            </font>
                                          </h5>
                                        </div>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                          <i class="fa fa-map-marker" style="font-size: 25px;color: #d83131;padding: 0px 4px;"> </i>
                                        </div>
                                        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                          <h5 class="truncate"><font color="#3498db"><?=$tList['destination_point']?></font></h5>
                                        </div>
                                      </div>  
                                    </div>
                                  </div>

                                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="row">
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <h5>
                                          <?php 
                                            if($tList['return_trip_id']>0) { 
                                              $r_tList = $this->api->get_trip_booking_details($tList['return_trip_id']);
                                              echo "<i class='fa fa-arrow-up' style='font-size: 17px; padding: 2px 0px;'></i> GT-".$tList['trip_id'];  } 
                                            else { echo "<i class='fa fa-arrow-up' style='font-size: 17px; padding: 2px 0px;'></i> GT-".$tList['trip_id']; } 
                                          ?> 
                                        </h5>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                          <i class="fa fa-ticket" style="font-size: 19px; padding: 2px 0px;"></i>
                                        </div>
                                        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                          <h6>&nbsp;&nbsp;&nbsp;<?=$this->lang->line("Ticket")?><?='-'.$tList['ticket_id']?></h6>
                                        </div>
                                      </div>
                                      <div class="col-lg-12">
                                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                          <i class="fa fa-calendar" style="font-size: 17px; padding: 2px 0px;"></i>
                                        </div>
                                        <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                          <h6>&nbsp;&nbsp;<?=' '.$tList['journey_date'] ?></h6>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <a class="btn btn-link btn_view_vehicle_<?=$tList['ticket_id']?>" style="color:#3498db;padding-top:5px; padding-bottom:0px; padding-left:0px; font-size:12px;"><?=$this->lang->line('Vehicle Detail')?></a>
                                      <a class="btn btn-link btn_hide_vehicle_<?=$tList['ticket_id']?>" style="color:#3498db; padding-top:5px; padding-bottom:0px; padding-left:0px; font-size:12px;"><?=$this->lang->line("Vehicle Detail")?></a>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <a class="btn btn-link btn_view_cancelation_<?=$tList['ticket_id']?>" style="color:#3498db; padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:11px;"><?=$this->lang->line("cancellation_charges")?></a>
                                      <a class="btn btn-link btn_hide_cancelation_<?=$tList['ticket_id']?>" style="color:#3498db; padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:11px;"><?=$this->lang->line("cancellation_charges")?></a>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <a class="btn btn-link btn_view_seats_<?=$tList['ticket_id']?>" style="color:#3498db; padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:11px;"><?=$this->lang->line("Seat Details")?></a>
                                      <a class="btn btn-link btn_hide_seats_<?=$tList['ticket_id']?>" style="color:#3498db; padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:11px;"><?=$this->lang->line("Seat Details")?></a>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <a class="btn btn-link btn_view_provider_<?=$tList['ticket_id']?>" style="color:#3498db; padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:11px;"><?=$this->lang->line("provider")?></a>
                                      <a class="btn btn-link btn_hide_provider_<?=$tList['ticket_id']?>" style="color:#3498db; padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:11px;"><?=$this->lang->line("provider")?></a>
                                    </div>
                                  </div>

                                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-right" style="padding-left: 0px; padding-right: 0px;">
                                    <button id="<?= $tList['ticket_id'] ?>" class="btn btn-sm btn-outline btn-info" data-toggle="modal" data-target="#myModal<?= $tList['ticket_id'] ?>"><i class="fa fa-ticket"></i> <?= $this->lang->line('Print Ticket'); ?> 
                                    </button>
                                    <?php if($tList['cust_id'] != $tList['operator_id']) { ?>
                                      <form action="<?=base_url('user-panel-bus/bus-work-room')?>" method="post">
                                        <input type="hidden" name="ticket_id" value="<?= $tList['ticket_id'] ?>">
                                        <button type="submit" class="btn btn-sm btn-outline btn-warning" ><i class="fa fa-check"></i> <?= $this->lang->line('workroom'); ?> </button>
                                      </form>
                                    <?php } ?>
                                    <div class="modal fade" id="myModal<?= $tList['ticket_id'] ?>" role="dialog">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title"><?= $this->lang->line('Print_Ticket'); ?></h4>
                                          </div>
                                          <div class="modal-body" id="printThis<?= $tList['ticket_id'] ?>">
                                            <div style="width: 340px; height: 80px; border: dashed 1px #225595; padding-top: 10px; display: flex;">
                                              <div class="col-md-6" style="border-right: dashed 1px #225595; display: flex;">
                                                <img src="<?=($operator_details['avatar_url']=='' || $operator_details['avatar_url'] == 'NULL')?base_url('resources/no-image.jpg'):base_url($operator_details['avatar_url'])?>" style="border: 1px solid #3498db; width: 56px; height: 56px; float: left; margin-right: 5px">
                                                <div>
                                                <h6><?=$operator_details['company_name']?></h6>
                                                <h6><?=$operator_details['firstname']. ' ' .$operator_details['lastname']?></h6>
                                                </div>
                                              </div>
                                              <div class="col-md-6">
                                                <img src="<?= base_url('resources/images/dashboard-logo.jpg') ?>" style="width: 100px; height: auto;" /><br />
                                                <h6><?= $this->lang->line('slider_heading1'); ?></h6>
                                              </div>
                                            </div> 
                                            <div style="width: 340px; height: auto; border: dashed 1px #225595; padding-left: 5px; word-break: break-all;">
                                              <h5 style="color: #225595; text-decoration: underline;">
                                                <i class="fa fa-arrow-up"></i> <?= $this->lang->line('Ownward_Trip'); ?> <br />
                                                <strong style="float: right; padding-right: 2px; margin-top: -15px"><?= $this->lang->line('Ticket ID'); ?>: #<?= $tList['ticket_id'] ?></strong> 
                                                <br />
                                                <?php QRcode::png($tList['ticket_id'], $tList['ticket_id'].".png", "L", 2, 2); 
                                                  $img_src = $_SERVER['DOCUMENT_ROOT']."/".$tList['ticket_id'].'.png';
                                                  $img_dest = $_SERVER['DOCUMENT_ROOT']."/resources/ticket-qrcode/".$tList['ticket_id'].'.png';
                                                  if(rename($img_src, $img_dest));
                                                ?>
                                                <img src="<?php echo base_url('resources/ticket-qrcode/').$tList['ticket_id'].'.png'; ?>" style="float: right; width: 84px; padding-right: 2px; height: 84px; margin-top: -15px" />
                                              </h5>
                                              <h6 style="margin-top:-15px;"><strong><?= $this->lang->line('Source'); ?>:</strong> <?= $tList['source_point'] ?> - <?= $tList['pickup_point'] ?></h6>
                                              <h6><strong><?= $this->lang->line('Destination'); ?>:</strong> <?= $tList['destination_point'] ?> - <?= $tList['drop_point'] ?></h6>
                                              <h6><strong><?= $this->lang->line('Journey Date'); ?>:</strong> <?= $tList['journey_date'] ?></h6>
                                              <h6><strong><?= $this->lang->line('Departure Time'); ?>:</strong> <?= $tList['trip_start_date_time'] ?></h6>
                                              <h6><strong><?= $this->lang->line('no_of_seats'); ?>:</strong> <?= $tList['no_of_seats'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                  <strong><?= $this->lang->line('Price'); ?>:</strong> <?= $tList['ticket_price'] ?> <?= $tList['currency_sign'] ?></h6>
                                              <h6><strong><?= $this->lang->line('Passenger Details'); ?>:</strong></h6>
                                              <?php 
                                                $ticket_seat_details = $this->api->get_ticket_seat_details($tList['ticket_id']);
                                              ?>
                                              <table width="100%">
                                                <thead>
                                                  <th><?= $this->lang->line('Name'); ?></th>
                                                  <th><?= $this->lang->line('Gender'); ?></th>
                                                  <th><?= $this->lang->line('Contact'); ?></th>
                                                </thead>
                                                <tbody>
                                                  <?php foreach ($ticket_seat_details as $seat) { ?>
                                                    <tr>
                                                      <td><?=$seat['firstname']. ' ' .$seat['lastname']?></td>
                                                      <td><?=($seat['gender']=='M')?'Male':'Female'?></td>
                                                      <td><?=$this->api->get_country_code_by_id($seat['country_id']).$seat['mobile']?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                              </table>
                                            </div>
                                           
                                          </div>
                                          <div class="modal-footer" style="display: flex;">
                                              <button style="float: right;" class="btn btn-sm btn-sm pull-right btn-default" id="btnPrint<?= $tList['ticket_id'] ?>"><?= $this->lang->line('Print_Ticket'); ?></button>
                                              <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <script type="text/javascript">
                                        $("#btnPrint<?= $tList['ticket_id'] ?>").click(function () {
                                          $('#printThis<?= $tList['ticket_id'] ?>').printThis();
                                      });
                                    </script>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="panel-body" id="view_vehicle<?=$tList['ticket_id']?>" style="display:none; padding-bottom: 5px; padding-top: 10px;">
                              <div class="row">
                                <div class="col-md-12" style="margin-top: 0px;"> 
                                  <h6 style="margin-top: 0px; margin-bottom: 0px;">
                                    <strong><?= $this->lang->line('Amenities'); ?>: </strong><?=' '.str_replace('_',' ',str_replace(',',', ',$bus_details['bus_amenities']))?>
                                  </h6>
                                </div>
                                <div class="col-md-12" style="padding-right: 0px;">
                                  <h6 style="margin-top:3px;">
                                    <strong><?= $this->lang->line('Vehicle'); ?>: </strong><?=' '.$bus_details['bus_no'].' - '.$bus_details['bus_ac']. ', ' .$bus_details['bus_seat_type']?>
                                  </h6>
                                </div>                                 
                              </div>
                            </div>

                            <div class="panel-body" id="view_seats<?=$tList['ticket_id']?>" style="display:none; padding-bottom: 5px; padding-top: 10px;">
                              <div class="row">
                                <div class="col-md-12" style="margin-top: 0px;"> 
                                  <div class="col-md-6" style="margin-top: 0px;padding-left: 0px">
                                    <div class="col-md-12" style="margin-bottom: 5px;padding-left: 0px">
                                      <label><?= $this->lang->line('Seat Types and Pricing'); ?></label>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                      <?= $this->lang->line('no_of_seats'); ?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                      <strong><font color="#3498db"><?=$tList['no_of_seats'];?></font></strong>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                      <?= $this->lang->line('Total_Price'); ?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                      <strong><font color="#3498db"><?=$tList['ticket_price'].' '.$tList['currency_sign'];?></font></strong>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                      <?= $this->lang->line('Paid Amount'); ?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                      <strong><font color="#3498db"><?=$tList['paid_amount'].' '.$tList['currency_sign'];?></font></strong>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                      <?= $this->lang->line('Balance Amount'); ?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                      <strong><font color="#3498db"><?=$tList['balance_amount'].' '.$tList['currency_sign'];?></font></strong>
                                    </div>
                                  </div> 
                                </div>                                 
                              </div>
                            </div>

                            <div class="panel-body" id="cancelation_charge_<?=$tList['ticket_id']?>" style="display:none; padding-bottom: 0px; padding-top: 10px;">
                              <?php
                                $o_d = $this->api->get_source_destination_of_trip($tList['trip_id']);
                                $location_details = $this->api->get_bus_location_details($o_d[0]['trip_source_id']);
                                $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
                                $table_data= $this->api->get_cancellation_charges_by_country($o_d[0]['cust_id'],$o_d[0]['vehical_type_id'],$country_id);
                              ?>
                              <div class="row">
                                <div class="col-md-6">
                                  <table id="policy_table" class="table">
                                    <thead>
                                      <tr>
                                        <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('Cancellation Time')?></h6></th>
                                        <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('cancellation_charges')?></h6></th>
                                        <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('rescheduling_charges')?></h6></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                        if(sizeof($table_data) > 0) {
                                         foreach ($table_data as $td)
                                          {
                                            echo "<tr><td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>From ".$td['bcr_min_hours']." To ".$td['bcr_max_hours']." Hrs</h6></td>"; 
                                            echo "<td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>".$td['bcr_cancellation']."%</h6></td>";    
                                            echo "<td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>".$td['bcr_rescheduling']."%</h6></td></tr>"; 
                                          }
                                        } else {
                                          echo "<tr><td style='padding-top:2px; padding-bottom:2px;' colspan='3'><h6 style='margin-top:0px; margin-bottom:0px;'>".$this->lang->line('No policy found!')."</h6></td>"; 
                                        } 
                                      ?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>

                            <div class="panel-body" id="view_provider<?=$tList['ticket_id']?>" style="display:none; padding-bottom: 5px; padding-top: 10px;">
                              <div class="row">
                                <div class="col-md-12" style="margin-top: 0px;">
                      
                                  <div class="col-lg-2" style="padding-left: 0px; padding-right: 0px;">
                                    <img src="<?=($operator_details['avatar_url']=='' || $operator_details['avatar_url'] == 'NULL')?base_url('resources/no-image.jpg'):base_url($operator_details['avatar_url'])?>" class="img-responsive thumbnail" style="border: 1px solid #3498db;">
                                  </div>

                                  <div class="col-lg-8" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="col-lg-7" style="padding-left: 0px; padding-right: 0px;">
                                      <div class="col-lg-12"> 
                                        <h6><?= $this->lang->line('Operator Company'); ?>: <font color="#3498db"><?= $operator_details['company_name'] ?></font></h6>
                                      </div>
                                      <div class="col-lg-12"> 
                                        <h6><?= $this->lang->line('Operator Name'); ?>: <font color="#3498db"><?= $operator_details['firstname']." ".$operator_details['lastname'] ?></font></h6>
                                      </div>
                                      <div class="col-lg-12"> 
                                        <h6><?= $this->lang->line('Operator Contact'); ?>: <font color="#3498db"><?= $operator_details['contact_no']?></font></h6>
                                      </div>
                                      <div class="col-lg-12"> 
                                        <h6><?= $this->lang->line('Operator Email'); ?>: <font color="#3498db"><?= $operator_details['email_id']?></font></h6>
                                      </div>
                                      <div class="col-lg-12"> 
                                        <h6><?= $this->lang->line('Operator Address'); ?>: <font color="#3498db"><?= $operator_details['address']?></font></h6>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- scripts -->
                              <script>
                                $(".btn_hide_vehicle_<?=$tList['ticket_id']?>").addClass("hidden");
                                $(".btn_view_cancelation_<?=$tList['ticket_id']?>").addClass("hidden");
                                $(".btn_hide_seats_<?=$tList['ticket_id']?>").addClass("hidden");
                                $(".btn_hide_provider_<?=$tList['ticket_id']?>").addClass("hidden");
                              </script>
                              <script>
                                $(".btn_view_vehicle_<?=$tList['ticket_id']?>").click(function(){
                                  $("#view_vehicle<?=$tList['ticket_id']?>").slideToggle(500);
                                  $(".btn_view_vehicle_<?=$tList['ticket_id']?>").addClass("hidden");
                                  $(".btn_hide_vehicle_<?=$tList['ticket_id']?>").removeClass("hidden");
                                });
                                $(".btn_hide_vehicle_<?=$tList['ticket_id']?>").click(function(){
                                  $("#view_vehicle<?=$tList['ticket_id']?>").slideToggle(500);
                                  $(".btn_view_vehicle_<?=$tList['ticket_id']?>").removeClass("hidden");
                                  $(".btn_hide_vehicle_<?=$tList['ticket_id']?>").addClass("hidden");
                                });
                              </script>
                              <script>
                                $(".btn_view_cancelation_<?=$tList['ticket_id']?>").click(function(){
                                  $("#cancelation_charge_<?=$tList['ticket_id']?>").slideToggle(500);
                                  $(".btn_view_cancelation_<?=$tList['ticket_id']?>").addClass("hidden");
                                  $(".btn_hide_cancelation_<?=$tList['ticket_id']?>").removeClass("hidden");
                                });
                                $(".btn_hide_cancelation_<?=$tList['ticket_id']?>").click(function(){
                                  $("#cancelation_charge_<?=$tList['ticket_id']?>").slideToggle(500);
                                  $(".btn_view_cancelation_<?=$tList['ticket_id']?>").removeClass("hidden");
                                  $(".btn_hide_cancelation_<?=$tList['ticket_id']?>").addClass("hidden");
                                });
                              </script>
                              <script>
                                $(".btn_view_seats_<?=$tList['ticket_id']?>").click(function(){
                                  $("#view_seats<?=$tList['ticket_id']?>").slideToggle(500);
                                  $(".btn_view_seats_<?=$tList['ticket_id']?>").addClass("hidden");
                                  $(".btn_hide_seats_<?=$tList['ticket_id']?>").removeClass("hidden");
                                });
                                $(".btn_hide_seats_<?=$tList['ticket_id']?>").click(function(){
                                  $("#view_seats<?=$tList['ticket_id']?>").slideToggle(500);
                                  $(".btn_view_seats_<?=$tList['ticket_id']?>").removeClass("hidden");
                                  $(".btn_hide_seats_<?=$tList['ticket_id']?>").addClass("hidden");
                                });
                              </script>
                              <script>
                                $(".btn_view_provider_<?=$tList['ticket_id']?>").click(function(){
                                  $("#view_provider<?=$tList['ticket_id']?>").slideToggle(500);
                                  $(".btn_view_provider_<?=$tList['ticket_id']?>").addClass("hidden");
                                  $(".btn_hide_provider_<?=$tList['ticket_id']?>").removeClass("hidden");
                                });
                                $(".btn_hide_provider_<?=$tList['ticket_id']?>").click(function(){
                                  $("#view_provider<?=$tList['ticket_id']?>").slideToggle(500);
                                  $(".btn_view_provider_<?=$tList['ticket_id']?>").removeClass("hidden");
                                  $(".btn_hide_provider_<?=$tList['ticket_id']?>").addClass("hidden");
                                });
                              </script>
                            <!-- scripts -->
                          </div>
                        </td>
                      </tr>
                    <?php } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<script>
  $('.cancelticket').click(function () {
    var id = this.id;
    swal({
      title: <?= json_encode($this->lang->line('are_you_sure'))?>,
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,
      cancelButtonText: <?= json_encode($this->lang->line('no'))?>,
      closeOnConfirm: false,
      closeOnCancel: false },
      function (isConfirm) {
        if (isConfirm) {
          $.post('', {id: id, status: 'accept'}, function(res){
            if($.trim(res) == 'order_accepted_workroom') {
              swal(<?= json_encode($this->lang->line('accepted'))?>, "<?= $this->lang->line('Order has been accepted!')?>", "success");
              setTimeout(function() { window.location.reload(); }, 2000);
            } else if ($.trim(res) == 'order_accept_failed') {
              swal(<?= json_encode($this->lang->line('error'))?>, "<?= $this->lang->line('While accepting order!')?>", "error");
              setTimeout(function() { window.location.reload(); }, 2000);
            } else {
              swal(<?= json_encode($this->lang->line('error'))?>, "<?= $this->lang->line('While updating status!')?>", "error");
              setTimeout(function() { window.location.reload(); }, 2000);
            }
          });
        } else {
          swal(<?= json_encode($this->lang->line('canceled'))?>, "<?= $this->lang->line('No change')?>", "error");
        }
      });
    });
</script>

<script>
   $("#frmBooking").submit(function(e){
      var ownward_trip = $("input[name='ownward_trip']:checked").val();
      if (typeof ownward_trip === "undefined") {
        e.preventDefault(); 
        swal(<?= json_encode($this->lang->line('Select Onward Trip!'))?>, "", "error"); 
      } else {
        $("#frmBooking").submit();
      }
    });
</script>
<script>
  $(function() {
    $("a[name='create_link']").on('click',function(e) { e.preventDefault();
      var redirect = $(this).data('redirect');
      var cat_id = $(this).data('cat_id');
      var newForm = $('<form>', {
        'action': <?= json_encode(base_url());?> + redirect,
        'method': 'post'
      }).append($('<input>', {
        'name': 'category_id',
        'value': cat_id,
        'type': 'hidden'
      }));
      $(document.body).append(newForm);
      newForm.submit();
    }); 

    $('.orderTable').dataTable( {
      "aaSorting": [[ 0, "desc" ]]
    } );

    $('#advanceBtn').click(function() {
      if($('#advanceSearch').css('display') == 'none') {
        //$('#basicSearch').hide("slow");
        $('#advanceSearch').show("slow");
        $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Hide Filters'))?>);
      } else {
        $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Show Filters'))?>);
        $('#advanceSearch').hide("slow");
        $('#basicSearch').show("slow");
      }
      return false;
    });
  });
</script>

<script>
  $("#frmBasicSearch")
    .validate({
      ignore: [], 
      rules: {
        trip_source: { required : true },
        trip_destination: { required : true },
        journey_date: { required : true },
      },
      messages: {
        trip_source: { required : <?= json_encode($this->lang->line('Select starting city!')); ?> },
        trip_destination: { required : <?= json_encode($this->lang->line('Select destination city!')); ?> },
        journey_date: { required : <?= json_encode($this->lang->line('Select journey date!')); ?> },
      },
    });
</script>

<script>
  $('.cancelorder').click(function () { 
    var id = this.id;
    swal({ 
      title: <?= json_encode($this->lang->line('are_you_sure')) ?>, 
      text: "", 
      type: "warning", 
      showCancelButton: true, 
      confirmButtonColor: "#DD6B55", 
      confirmButtonText: <?= json_encode($this->lang->line('yes')) ?>, 
      cancelButtonText: <?= json_encode($this->lang->line('no')) ?>, 
      closeOnConfirm: false, 
      closeOnCancel: false, 
    }, 
    function (isConfirm) { 
      if (isConfirm) { 
        $.post('cancel-order', {id: id}, 
        function(res){
          if($.trim(res) == "success") {
            swal(<?= json_encode($this->lang->line('deleted'))?>, "", "success");
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal(<?= json_encode($this->lang->line('failed'))?>, "", "error");  } 
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");  } 
    }); 
  });
</script>