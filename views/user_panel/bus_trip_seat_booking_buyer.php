<style type="text/css" media="screen">
    body{
  background: #fff;
}
.box {
  transition: box-shadow .3s;
  width: 350px;
  height: 100px;
  margin: 3px;
  border-radius:5px;
  background: #fff;
  float: left;
  padding: 20px;
  
}
.box:hover {
  box-shadow: 0 0 100px rgba(32,32,32,.2); 
}
hr {
  margin-top: 1rem;
  margin-bottom: 1rem;
  border: 0;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
}

.table>tbody>tr>td {
    border-top: none;
}

.dataTables_filter {
    display: none;
}

.labl {
    display : block;
    width: 400px;
}
.labl > input{ /* HIDE RADIO */
    visibility: hidden; /* Makes input not-clickable */
    position: absolute; /* Remove input from document flow */
}
.labl > input + div{ /* DIV STYLES */
    cursor:pointer;
    border:2px solid transparent;
}
.labl > input:checked + div{ /* (RADIO CHECKED) DIV STYLES */
    background-color: #ffd6bb;
    border: 1px solid #ff6600;
}
</style>

<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-ticket-search'; ?>"><?= $this->lang->line('Search Ticket'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('Book Ticket'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-ticket fa-2x text-muted"></i> <?= $this->lang->line('Book Ticket'); ?></h2>
            <small class="m-t-md"><?= $this->lang->line('Select Your Seat And pay'); ?></small> 
        </div>
    </div>
</div>
<div class="content">
 <div class="row">
  <div class="col-md-12">
   <div class="hpanel">
     <div class="panel-body">
        <!-- for cancellation chareges --> 
            <?php
            $o_d = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['ownward_trip']);
            $location_details = $this->api->get_bus_location_details($o_d[0]['trip_source_id']);
            $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
            $table_data= $this->api->get_cancellation_charges_by_country($o_d[0]['cust_id'],$o_d[0]['vehical_type_id'],$country_id);
            //echo json_encode($table_data);
            if(isset($_POST['return_trip']) && $_POST['return_trip'] != '') {
                $r_d=$this->api->get_source_destination_of_trip_by_sdd_id($_POST['return_trip']);
                $location_details = $this->api->get_bus_location_details($r_d[0]['trip_source_id']);
                $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
                $return_table_data= $this->api->get_cancellation_charges_by_country($r_d[0]['cust_id'],$r_d[0]['vehical_type_id'],$country_id);  
                //echo json_encode($table_data);
            }
            // echo json_encode($table_data);
            // echo $_POST['ownward_trip'];
            // echo json_encode($_POST); die(); 
            ?>
        <!-- cancellation chareges End -->
        <form name="simpleForm" id="simpleForm" action="<?= base_url('user-panel-bus/bus-trip-booking-process-buyer'); ?>" method="post">
            <div class="text-center" id="wizardControl">
                <!-- <a style="pointer-events: none;" class="btn btn-success o_p_d" data-toggle="tab"><?=$this->lang->line('Onward Pickup Drop')?></a>
                <?php
                if(isset($_POST['return_trip']) && $_POST['return_trip'] != '')
                {
                    echo "<a style='pointer-events: none;' class='btn btn-success r_p_d'  data-toggle='tab'>".$this->lang->line('Return Pickup Drop')."</a>";
                } 
                ?> -->
                <a style="pointer-events: none;" class="btn btn-success p_info hidden"  data-toggle="tab"><?=$this->lang->line('Passenger info')?></a>
                <!-- <a class="btn btn-success u_info"  data-toggle="tab"><?=$this->lang->line('Your info')?></a> -->
                <a style="pointer-events: none;" class="btn btn-success t_p_details hidden"  data-toggle="tab"><?=$this->lang->line('Trip Summary')?></a>
                <!-- <a style="pointer-events: none;" class="btn btn-success cancel_p"  data-toggle="tab"><?=$this->lang->line('Cancellation policy')?></a> -->
            </div>
        
            <div class="tab-content" style="margin-top: -15px;">
                <div id="tab1" class="p-m tab-pane">
                    <div class='row'>
                        <div class="col-lg-8 text-right">
                            <h4><label style="color:#3498db;" ><?=$this->lang->line('Select Ownward Pickup Drop')?></label></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-lg-5">
                            <h3><?=$this->lang->line('Pickup Point')?></h3>
                            <div style="height:300px;width:400px;overflow-y:scroll;">
                                <input type="text" class="form-control" id="pickupsearch" placeholder="<?=$this->lang->line('search')?>" name="pickupsearch">
                                <?php
                                    $src_point = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['ownward_trip']);
                                   // echo json_encode($src_point);
                                    echo '<input type="hidden" name="ownward_source" id="ownward_source" value="'.$src_point[0]['trip_source'].'">';
                                    echo '<input type="hidden" name="ownward_destination" id="ownward_destination" value="'.$src_point[0]['trip_destination'].'">';
                                    $src_point_array = explode(',',$src_point[0]['src_point_ids']);
                                     ?>
                                    <input type="hidden" name="no_of_seat" id="no_of_seat" value="<?=$_POST['no_of_seat']?>">
                                    <table id="OnwardPickupTabel" class="table">
                                        <thead><tr class="hidden"><th></th></tr></thead>
                                        <tbody>
                                            <?php for($i=0; $i < sizeof($src_point_array) ; $i++){ ($i==0)?$selected='checked':$selected=''; ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                        $src = $this->api->get_location_point_address_by_id__($src_point_array[$i]); ?>
                                                        <label class="labl"><div class="box"><div class="row"><div class="col-md-1" style="margin-right:12px; margin-top:8px"><div class="radio radio-success">
                                                            <input style="margin-right: 5px;" type="radio" 
                                                            <?=(isset($_POST['pickup_point']))?($src['point_address'] == $_POST['pickup_point'])?"checked":"":""?> id="onward_pickup_point" value="<?=$src['point_address']?>" name="onward_pickup_point" <?=$selected?>>
                                                            <label></label></div></div><div class="col-md-3"><label><?=$src_point[0]['trip_depart_time']?></label><br/><label><?=$_POST['journey_date']?></label></div><div class="col-md-7"><?=$src['point_address']?></div></div></div></lable> 
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <h3><?=$this->lang->line('Drop Point')?></h3>
                            <div style="height:300px;width:400px;overflow-y:scroll;">
                                <input type="text" class="form-control" id="dropsearch" placeholder="<?=$this->lang->line('search')?>" name="dropsearch">
                                <?php
                                    $dest_point = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['ownward_trip']); 
                                    $dest_point_array = explode(',',$dest_point[0]['dest_point_ids']);
                                    ?>
                                    <table id="OnwardDropTabel" class="table">
                                        <thead><tr class="hidden"><th></th></tr></thead>
                                        <tbody>
                                            <?php for($i=0; $i < sizeof($dest_point_array) ; $i++){ ($i==0)?$selected='checked':$selected=''; ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                        $dest = $this->api->get_location_point_address_by_id__($dest_point_array[$i]); ?>
                                                        <label class="labl"><div class="box"><div class="row"><div class="col-md-1" style="margin-right:12px; margin-top:8px"><div class="radio radio-success"><input style="margin-right: 5px;" type="radio" id="onward_drop_point" value="<?=$dest['point_address']?>" name="onward_drop_point"
                                                            <?=(isset($_POST['drop_point']))?($dest['point_address'] == $_POST['drop_point'])?"checked":"":""?> <?=$selected?> ><label></label></div></div><div class="col-md-3"><label><?=$dest_point[0]['trip_depart_time']?></label><br/><label><?=$_POST['journey_date']?></label></div><div class="col-md-7"><?=$dest['point_address']?></div></div></div></label>
                                                    
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                    <div class="text-right m-t-xs">
                        <?php
                        if(isset($_POST['return_trip']) && $_POST['return_trip'] != '') {
                          echo '<button class="btn btn-outline btn-success next" id="tab1btn">'.$this->lang->line('Select Return Trip Pickup/Drop Point').'</button>';
                        } else {
                          echo '<button class="btn btn-outline btn-success next" id="tab111btn">'.$this->lang->line('Select Seat and Passengers').'</button>';
                        }
                        ?>
                    </div>
                </div>
                <div id="tab2" class="p-m tab-pane">
                    <div class='row'>
                        <div class="col-lg-8 text-right">
                            <h4><label style="color:#3498db;" ><?=$this->lang->line('Select Return Pickup Drop')?></label></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                            <div class="step0">
                                
                            </div> 
                        </div>
                        <div class="col-lg-5">
                            <h3><?=$this->lang->line('Pickup Point')?></h3>
                            <div style="height:300px;width:400px;overflow-y:scroll;">
                                <input type="text" class="form-control" id="r_pickup_search" placeholder="<?=$this->lang->line('search')?>" name="r_pickup_search">
                                <?php
                                  if(isset($_POST['return_trip']) && $_POST['return_trip'] != '')
                                  {
                                    $return_src_point = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['return_trip']); 
                                   // echo json_encode($src_point);
                                    $return_src_point_array = explode(',',$return_src_point[0]['src_point_ids']);
                                    echo '<input type="hidden" name="return_source" id="return_source" value="'.$return_src_point[0]['trip_source'].'">';
                                    echo '<input type="hidden" name="return_destination" id="return_destination" value="'.$return_src_point[0]['trip_destination'].'">';
                                ?>  

                                    <table id="ReturnPickupTabel" class="table">
                                        <thead><tr class="hidden"><th></th></tr></thead>
                                        <tbody>
                                            <?php for($i=0; $i < sizeof($return_src_point_array) ; $i++){ ($i==0)?$selected='checked':$selected=''; ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                        $src = $this->api->get_location_point_address_by_id__($return_src_point_array[$i]); ?>
                                                        <label class="labl"><div class="box"><div class="row"><div class="col-md-1" style="margin-right:12px; margin-top:8px"><div class="radio radio-success"><input style="margin-right: 5px;" type="radio" id="return_pickup_point" value="<?=$src['point_address']?>" name="return_pickup_point" <?= (isset($_POST['return_pickup_point']))?($src['point_address'] == $_POST['return_pickup_point'])?"checked":"":""?> <?=$selected?> ><label></label></div></div><div class="col-md-3"><label><?=$return_src_point[0]['trip_depart_time']?></label><br/><label><?=$_POST['return_date']?></label></div><div class="col-md-7"><?=$src['point_address']?></div></div></div></label>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <h3><?=$this->lang->line('Drop Point')?></h3>
                            <div style="height:300px;width:400px;overflow-y:scroll;">
                                <input type="text" class="form-control" id="r_drop_search" placeholder="<?=$this->lang->line('search')?>" name="r_drop_search">
                               <?php
                                  if(isset($_POST['return_trip']) && $_POST['return_trip'] != '')
                                  {
                                    $return_dest_point = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['return_trip']); 
                                   // echo json_encode($src_point);
                                    $return_dest_point_array = explode(',',$return_dest_point[0]['dest_point_ids']);
                                ?>
                                    
                                     <table id="ReturnDropTabel" class="table">
                                        <thead><tr class="hidden"><th></th></tr></thead>
                                        <tbody>
                                            <?php for($i=0; $i < sizeof($return_dest_point_array) ; $i++){ ($i==0)?$selected='checked':$selected=''; ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                        $dest = $this->api->get_location_point_address_by_id__($return_dest_point_array[$i]); ?>
                                                        <label class="labl"><div class="box"><div class="row"><div class="col-md-1" style="margin-right:12px; margin-top:8px"><div class="radio radio-success"><input style="margin-right: 5px;" type="radio" id="return_drop_point" value="<?=$dest['point_address']?>" name="return_drop_point" <?= (isset($_POST['return_drop_point']))?($_POST['return_drop_point'] == $dest['point_address'])?"checked":"":""?> <?=$selected?> ><label></label></div></div><div class="col-md-3"><label><?=$return_dest_point[0]['trip_depart_time']?></label><br/><label><?=$_POST['return_date']?></label></div><div class="col-md-7"><?=$dest['point_address']?></div></div></div></label>
                                                </td>
                                            </tr>
                                            <?php }} ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                    <div class="text-right m-t-xs">
                        <button class="btn btn-outline btn-warning next" id="prev_2"><?=$this->lang->line('previous')?></button>
                        <button class="btn btn-outline btn-success next" id="tab2btn"><?=$this->lang->line('Select Seat and Passengers')?></button>
                    </div>
                </div>
                <div id="tab3" class="p-m tab-pane active"> 
                    <div class="row">         
                        <div class="col-md-9 form-group" style="border: 1px solid; margin-left: -5px;">
                            <input type="hidden" name="return_trip_sdd_id" value="<?=$_POST['return_trip']?>">
                            <input type="hidden" name="return_date" value="<?=$_POST['return_date']?>" id="return_date">
                            <input type="hidden" name="ownward_selected_seat_nos" value="<?=$_POST['ownward_selected_seat_nos_'.$_POST['ownward_trip']]?>">
                            <?php if($_POST['return_trip'] > 0) { ?>
                                <input type="hidden" name="return_selected_seat_nos" value="<?=$_POST['return_selected_seat_nos_'.$_POST['return_trip']]?>">
                                <input type="hidden" name="return_bus_seat_type" value="<?=$_POST['return_seat_type']?>">
                                <input type="hidden" name="return_seat_price" value="<?=$_POST['return_seat_price']?>">
                                <input type="hidden" name="return_currency_id" value="<?=$_POST['ownward_currency_id']?>">
                                <input type="hidden" name="return_currency_sign" value="<?=$_POST['ownward_currency_sign']?>">
                            <?php } else { ?>
                                <input type="hidden" name="return_selected_seat_nos" value="NULL">
                                <input type="hidden" name="return_bus_seat_type" value="NULL">
                                <input type="hidden" name="return_seat_price" value="0">
                                <input type="hidden" name="return_currency_id" value="0">
                                <input type="hidden" name="return_currency_sign" value="NULL">
                            <?php } ?>
                            <input type="hidden" name="bus_seat_type" value="<?=$_POST['seat_type']?>">
                            <input type="hidden" name="onward_bus_seat_type" value="<?=$_POST['seat_type']?>">
                            <input type="hidden" name="seat_price" value="<?=$_POST['seat_price']?>">
                            <input type="hidden" name="currency_id" value="<?=$_POST['ownward_currency_id']?>">
                            <input type="hidden" name="currency_sign" value="<?=$_POST['ownward_currency_sign']?>">
                            <!-- <div class="row">
                              <div class="col-md-4">
                               <label class="control-label"><?= $this->lang->line('Select seat type'); ?></label>
                               <select class="js-source-states" style="width: 100%" name="<?php if(isset($_POST['return_trip']) && $_POST['return_trip'] != ''){ echo "onward_bus_seat_type";}else{ echo "bus_seat_type";}?>" id="<?php if(isset($_POST['return_trip']) && $_POST['return_trip'] != ''){ echo "onward_bus_seat_type";}else{ echo "bus_seat_type";}?>">
                                       <option value=""><?= $this->lang->line('Seat Type'); ?></option>
                                       <?php $trip_details = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['ownward_trip']);$seat_details = $this->api->get_operator_bus_seat_details($trip_details[0]['bus_id']);for($i=0; $i < sizeof($seat_details); $i++)
                                           { if($seat_details[$i]['type_of_seat'] != 'VIP'): ?>
                                               <option value="<?=$seat_details[$i]['type_of_seat']?>" 
                                               <?=(isset($_POST['seat_type']))?($seat_details[$i]['type_of_seat'] == $_POST['seat_type'])?"selected":"":""?>>
                                               <?=$seat_details[$i]['type_of_seat']?></option>';
                                          <?php endif; } ?>
                               </select>
                               <div class="qwerty">
                               </div>
                              </div>
                               <?php if(isset($_POST['return_trip']) && $_POST['return_trip'] != ''){ ?>    
                                   <div class="col-md-4">
                                       <input type="hidden" name="return_trip_sdd_id" value="<?=$_POST['return_trip']?>">
                                       <input type="hidden" name="return_date" value="<?=$_POST['return_date']?>" id="return_date">
                                       <label class="control-label"><?= $this->lang->line('Select Return seat type'); ?></label>
                                       <select class="js-source-states" style="width: 100%" name="return_bus_seat_type" id="return_bus_seat_type">
                                           <option value=""><?= $this->lang->line('Seat Type'); ?></option>
                                           <?php $trip_details = $this->api->get_source_destination_of_trip_by_sdd_id($_POST['return_trip']);$seat_details = $this->api->get_operator_bus_seat_details($trip_details[0]['bus_id']);for($i=0; $i < sizeof($seat_details); $i++)
                                               { ?>
                                                   <option value="<?=$seat_details[$i]['type_of_seat']?>" <?=(isset($_POST['return_seat_type']))?($_POST['return_seat_type'] == $seat_details[$i]['type_of_seat'])?"selected":"":"" ?> >
                                                       <?=$seat_details[$i]['type_of_seat']?>
                                                   </option>
                                              <?php } ?>
                                       </select>  
                                       <div class="return_qwerty">
                                       </div>
                                  </div>
                               <?php } ?> 
                              <div class="col-md-4">
                                   <div class="append_seatype">   
                                   </div> 
                              </div>
                            </div>  -->                       
                            <div id="hiddendiv" style="padding: 5px; margin-left: 0px;">
                                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                                  <label id="select_from_add" style='color:#3498db'><?=$this->lang->line('Select Passengers From Address Book')?></label>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6" style="padding-top: 10px;">
                                  <input type="text" class="form-control" name="addr_search" id="addr_search" placeholder="<?=$this->lang->line('search')?>">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                  <input type="hidden" name="ownward_trip_sdd_id" value="<?=$_POST['ownward_trip']?>">
                                  <input type="hidden" name="journey_date" value="<?=$_POST['journey_date']?>" id="journey_date">
                                  <button type="button" id="add_adresss" style="margin: 10px" class="btn btn-success" data-toggle="modal" data-target="#myModal" ><span class="fa fa-plus"> <?=$this->lang->line('add new passenger')?></span></button>
                                </div>

                                <table id="addressTableData_s" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?=$this->lang->line('select')?></th>
                                            <th><?=$this->lang->line('name')?></th>
                                            <th><?=$this->lang->line('contact_number')?></th>
                                            <th><?=$this->lang->line('email_address')?></th>
                                            <th><?=$this->lang->line('gender')?></th>
                                            <th><?=$this->lang->line('Age')?></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table_body_append">
                                        <?php foreach ($address_details as $addr) { ?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox" class="checkaddressclass" name="selected_passengers[]" value="<?=$addr['address_id']?>" style="-ms-transform: scale(1.5); /* IE */
                                                      -moz-transform: scale(1.5); /* FF */
                                                      -webkit-transform: scale(1.5); /* Safari and Chrome */
                                                      -o-transform: scale(1.5); /* Opera */
                                                      transform: scale(1.5);"><label></label>
                                                </td>
                                                <td><?=$addr['firstname']." ".$addr['lastname']?></td>
                                                <td><?=$addr['mobile']?></td><td><?=$addr['email_id']?></td>
                                                <td><?php if($addr['gender']=="M"){echo 'Male';}else{echo 'Female';}?></td>
                                                <td><?=$addr['age']?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="zz row">
                            </div>                 
                        </div> 
                        <div class="col-md-3" style="border: 1px solid;margin-left: 10px; padding-left: 30px; margin-right: -15px;">
                            <div class='row'>
                                <h4><label style='color:#3498db;'><?=$this->lang->line('Booking Details')?></label></h4>
                            </div>
                            <div class='row'>
                                <h5><i class="fa fa-map-marker"></i> <?=$this->lang->line('From')?><br /><font style='color:#3498db;'><?=$_POST['trip_source']?></font></h5>
                            </div>
                            <div class='row'>
                                <h5><i class="fa fa-map-marker"></i> <?=$this->lang->line('To')?><br /><font style='color:#3498db;'><?=$_POST['trip_destination']?></font></h5>
                            </div>
                            <hr style="margin-left: -15px;" />
                            <div class='row'>
                                <label style='color:#3498db;'><?=$this->lang->line('Ownward Fare')?></label><br/>
                                <div class='col-md-8'>
                                    <label><?=$_POST['seat_type']?> <?=$this->lang->line('Fare')?> ( <?=$_POST['no_of_seat']?> <?=$this->lang->line('Seat')?>)</label>
                                </div>
                                <div class='col-md-4'>
                                    <label><?=$_POST['no_of_seat']*$_POST['seat_price']?> <?=$_POST['ownward_currency_sign']?></label>
                                </div>
                                <?php if($_POST['seat_type'] == 'VIP'): ?>
                                    <div class='col-md-12'>
                                        <h6><?=$this->lang->line('Seat Numbers')?>: <?=rtrim($_POST["ownward_selected_seat_nos_".$_POST['ownward_trip']], ',')?></h6>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php if($_POST['return_trip'] > 0): ?>
                                <div class='row'>
                                    <label style='color:#3498db;'><?=$this->lang->line('Return Fare')?></label><br/>
                                    <div class='col-md-8'>
                                        <label><?=$_POST['return_seat_type']?> <?=$this->lang->line('Fare')?> ( <?=$_POST['no_of_seat']?> <?=$this->lang->line('Seat')?>)</label>
                                    </div>
                                    <div class='col-md-4'>
                                        <label><?=$_POST['no_of_seat']*$_POST['return_seat_price']?> <?=$_POST['return_currency_sign']?></label>
                                    </div>
                                    <?php if($_POST['return_seat_type'] == 'VIP'): ?>
                                        <div class='col-md-12'>
                                            <h6><?=$this->lang->line('Seat Numbers')?>: <?=rtrim($_POST["return_selected_seat_nos_".$_POST['return_trip']], ',')?></h6>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <hr style="margin-left: -15px;"/>
                            <div class='row'>
                                <div class='col-md-8' style="margin-left: -15px">
                                    <label style='color:#3498db;'><?=$this->lang->line('Grand Total')?>asd </label>
                                </div>
                                <?php if($_POST['return_trip']>0) { ?>
                                    <div class='col-md-4' style="margin-left: 15px">
                                        <label><?=($_POST['no_of_seat']*$_POST['seat_price'])+($_POST['no_of_seat']*$_POST['return_seat_price'])?> <?=$_POST['ownward_currency_sign']?></label>
                                    </div>
                                <?php } else { ?>
                                    <div class='col-md-4' style="margin-left: 15px">
                                        <label><?=($_POST['no_of_seat']*$_POST['seat_price'])?> <?=$_POST['ownward_currency_sign']?></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>            
                    </div>
                    <div class="text-right m-t-xs">
                        <!-- <?php if(isset($_POST['return_trip']) && $_POST['return_trip'] != '') { ?> -->
                            <a href="<?=base_url('user-panel-bus/bus-ticket-search')?>" class="btn btn-outline btn-warning next"><?=$this->lang->line('previous');?></a>
                        <!-- <?php } ?> -->
                        <?php 
                        echo '<button class="btn btn-outline btn-success next" id="tab333btn">'.$this->lang->line('View Booking Summary').'</button>';
                        /*if (isset($_POST['return_trip']) && $_POST['return_trip'] != '')
                            { echo '<button class="btn btn-outline btn-success next" id="tab333btn">'.$this->lang->line('View Booking Summary').'</button>';} else { echo '<button class="btn btn-outline btn-success next"  id="tab3btn">'.$this->lang->line('View Booking Summary').'</button>'; }*/
                        ?>
                    </div>
                </div>
                <div id="tab4" class="tab-pane">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="step3">        
                            </div>
                        </div>         
                        <div class=" col-md-9">
                            <div class="row">
                                <?php 
                                    $user_details=$this->api->get_user_details($_SESSION['cust_id']);
                                    echo '<div class="col-md-4"><label class="control-label">'.$this->lang->line("mobile_no").'</label>
                                        <input type="text" class="form-control" value="'.$user_details['mobile1'].'" id="m_no" name="m_no" placeholder="'.$this->lang->line("enter_mobile_number").'"></div>';
                                    echo '<div class=" col-md-4">
                                            <label class="control-label">'.$this->lang->line("Email").'</label>
                                            <input type="text" class="form-control" value="'.$user_details['email1'].'" id="email_address" name="email_address" placeholder="'.$this->lang->line("enter_email_address").'">
                                        </div>';
                                ?>  
                            </div>
                        </div>
                    </div>
                    <div class="text-right m-t-xs">
                        <button class="btn btn-outline btn-warning next"  id="pre_4"><?=$this->lang->line('previous')?></button>
                        <?php
                            if (isset($_POST['return_trip']) && $_POST['return_trip'] != '') {
                                echo '<button class="btn btn-outline btn-success next"  id="tab444btn">'.$this->lang->line('next').'</button>';}else{echo '<button class="btn btn-default next"  id="tab4btn">'.$this->lang->line('next').'</button>';} 
                        ?>
                    </div>
                </div>
                <div id="tab5" class="tab-pane">
                    <div class="row" style="margin-top: 15px;"> 
                        <div class="col-md-12">
                            <div class="col-md-4 form-group">
                                <div class="form-group" style="border:1px solid #3498db; margin-left: -15px;">
                                    <div class='row' style="padding-left: 30px;">
                                        <h5 style='color:#3498db;'><?=$this->lang->line('direction_on_map')?></h5>
                                    </div>
                                    <div class="row" style="padding-left:15px; padding-right:15px;">
                                        <div class="col-md-12 direc_map" style="margin-bottom: 8px;">
                                            <!-- Trip Map -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5 form-group">
                                <div class="row" style="border: 1px solid #3498db">
                                    <div class="col-md-12">
                                       <h5 style="color: #3498db"><?= $this->lang->line("Ownward Trip Details"); ?></h5>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <h5><strong><?=$this->lang->line("Journey Date")?>&nbsp;:&nbsp;<?=$_POST['journey_date']?></strong></h5>
                                        </div>
                                        <div class="col-md-12">
                                            <h5><strong><?=$this->lang->line("Source")?>&nbsp;:&nbsp;<?=$_POST['trip_source']?></strong></h5>
                                        </div>
                                        <div class="col-md-12">
                                            <strong><?=$this->lang->line("Pickup Point")?>&nbsp;:&nbsp;</strong><?=$_POST['pickup_point']?>
                                        </div>
                                        <br/>
                                        <div class="col-md-12">
                                            <h5><strong><?=$this->lang->line("Destination")?>&nbsp;:&nbsp;<?=$_POST['trip_destination']?></strong></h5>
                                        </div>
                                        <div class="col-md-12">
                                            <strong><?=$this->lang->line("Drop Point")?>&nbsp;:&nbsp;</strong><?=$_POST['drop_point']?>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <?php if($_POST['return_trip'] > 0): ?>
                                    <div class="row" style="border: 1px solid #3498db">
                                        <div class="col-md-12">
                                            <h5 style="color: #3498db"><?= $this->lang->line("Return Trip Details"); ?></h5>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <h5><strong><?=$this->lang->line("Return Date")?>&nbsp;:&nbsp;<?=$_POST['return_date']?></strong></h5>
                                            </div>
                                            <div class="col-md-12">
                                                <h5><strong><?=$this->lang->line("Source")?>&nbsp;:&nbsp;<?=$_POST['return_trip_source']?></strong></h5>
                                            </div>
                                            <div class="col-md-12">
                                                <strong><?=$this->lang->line("Pickup Point")?>&nbsp;:&nbsp;</strong><?=$_POST['return_pickup_point']?>
                                            </div>
                                            <br/>
                                            <div class="col-md-12">
                                                <h5><strong><?=$this->lang->line("Destination")?>&nbsp;:&nbsp;<?=$_POST['return_trip_destination']?></strong></h5>
                                            </div>
                                            <div class="col-md-12">
                                                <strong><?=$this->lang->line("Drop Point")?>&nbsp;:&nbsp;</strong><?=$_POST['return_drop_point']?>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                <?php endif ?>

                                <div class="row" style="border: 1px solid #3498db">
                                    <div class="col-md-12">
                                        <h5 style="color: #3498db"><?=$this->lang->line("Passengers Info")?></h5>
                                    </div>
                                    <div class="col-md-12 p_infooo">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <div class="form-group" style="border:1px solid #3498db; margin-left: 0px; margin-right: -15px; padding-right: 5px;">
                                    <div class='row' style="padding-left: 30px;">
                                        <h5 style='color:#3498db;'><?=$this->lang->line('Fare Details')?></h5>
                                    </div>
                                    <div class='row' style="padding-left: 30px;">
                                        <label style='color:#3498db;'><?=$this->lang->line('Ownward Fare')?></label><br/>
                                        <div class='col-md-7'>
                                            <label><?=$_POST['seat_type']?> <?=$this->lang->line('Fare')?> ( <?=$_POST['no_of_seat']?> <?=$this->lang->line('Seat')?>)</label>
                                        </div>
                                        <div class='col-md-5'>
                                            <label><?=$_POST['no_of_seat']*$_POST['seat_price']?> <?=$_POST['ownward_currency_sign']?></label>
                                        </div>
                                        <?php if($_POST['seat_type'] == 'VIP'): ?>
                                            <div class='col-md-12'>
                                                <h6><?=$this->lang->line('Seat Numbers')?>: <?=rtrim($_POST["ownward_selected_seat_nos_".$_POST['ownward_trip']], ',')?></h6>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php if($_POST['return_trip'] > 0): ?>
                                        <div class='row' style="padding-left: 30px;">
                                            <label style='color:#3498db;'><?=$this->lang->line('Return Fare')?></label><br/>
                                            <div class='col-md-7'>
                                                <label><?=$_POST['return_seat_type']?> <?=$this->lang->line('Fare')?> ( <?=$_POST['no_of_seat']?> <?=$this->lang->line('Seat')?>)</label>
                                            </div>
                                            <div class='col-md-5'>
                                                <label><?=$_POST['no_of_seat']*$_POST['return_seat_price']?> <?=$_POST['return_currency_sign']?></label>
                                            </div>
                                            <?php if($_POST['return_seat_type'] == 'VIP'): ?>
                                                <div class='col-md-12'>
                                                    <h6><?=$this->lang->line('Seat Numbers')?>: <?=rtrim($_POST["return_selected_seat_nos_".$_POST['return_trip']], ',')?></h6>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                    <hr />
                                    <div class='row' style="padding-left: 30px;">
                                        <div class='col-md-7' style="margin-left: -15px">
                                            <label style='color:#3498db;'><?=$this->lang->line('Grand Total')?></label>
                                        </div>
                                        <?php if($_POST['return_trip']>0) { ?>
                                            <div class='col-md-5' style="margin-left: 15px">
                                                <label><?=($_POST['no_of_seat']*$_POST['seat_price'])+($_POST['no_of_seat']*$_POST['return_seat_price'])?> <?=$_POST['ownward_currency_sign']?></label>
                                            </div>
                                        <?php } else { ?>
                                            <div class='col-md-5' style="margin-left: 15px">
                                                <label><?=($_POST['no_of_seat']*$_POST['seat_price'])?> <?=$_POST['ownward_currency_sign']?></label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group" style="">
                                    <div class="row" style="padding-left: 15px;">
                                        <input type="submit" class="btn btn-outline btn-success btn-block" id="seatbookingsubmit" name="aaa" value="<?=$this->lang->line('Proceed to Pay')?>"><br/>
                                        <button class="btn btn-outline btn-warning btn-block" id="pre_5"><?=$this->lang->line('previous')?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="text-right m-t-xs">
                        <button class="btn btn-outline btn-warning" id="pre_5"><?=$this->lang->line('previous')?></button>
                        <button class="btn btn-outline btn-success" id="tab5btn"><?=$this->lang->line('View Cancellation Policies')?></button>
                        <input type="submit" class="btn btn-outline btn-success" id="seatbookingsubmit" name="aaa" value="<?=$this->lang->line('Proceed to Pay')?>">
                    </div> -->
                </div>
                <div id="tab6" class="tab-pane">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="step4">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-lg-10">
                                    <table id="policy_table" class="table">
                                        <h3><?=$this->lang->line('Cancellation and Rescheduling Policies')?></h3>
                                        <h3><?=$this->lang->line('Ownward Ticket')?></h3>
                                        <thead>
                                            <tr>
                                                <th><?=$this->lang->line('Cancellation Time')?></th>
                                                <th><?=$this->lang->line('cancellation_charges')?></th>
                                                <th><?=$this->lang->line('rescheduling_charges')?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($table_data as $td) {
                                                    echo "<tr><td>From ".$td['bcr_min_hours']." To ".$td['bcr_max_hours']." Hrs</td>"; 
                                                    echo "<td>".$td['bcr_cancellation']."%</td>";    
                                                    echo "<td>".$td['bcr_rescheduling']."%</td></tr>"; 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php if(isset($_POST['return_trip']) && $_POST['return_trip'] != ''){ ?>
                                <div class="row">
                                    <div class="col-lg-10">
                                        <table id="return_policy_table" class="table">
                                            <h3><?=$this->lang->line('Return Ticket')?></h3>
                                            <thead>
                                                <tr>
                                                    <th><?=$this->lang->line('Cancellation Time')?></th>
                                                    <th><?=$this->lang->line('cancellation_charges')?></th>
                                                    <th><?=$this->lang->line('rescheduling_charges')?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($return_table_data as $td) {
                                                        echo "<tr><td>From ".$td['bcr_min_hours']." To ".$td['bcr_max_hours']." Hrs</td>"; 
                                                        echo "<td>".$td['bcr_cancellation']."%</td>";    
                                                        echo "<td>".$td['bcr_rescheduling']."%</td></tr>"; 
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="text-right m-t-xs">
                        <button class="btn btn-outline btn-warning" id="pre_6"><?=$this->lang->line('previous')?></button>
                        <input type="submit" class="btn btn-outline btn-success" id="seatbookingsubmit" name="aaa" value="<?=$this->lang->line('Proceed to Pay')?>">
                    </div>
                </div>
            </div>
            <!-- hidden fields -->
            <input type="hidden" id="front_return_trip" value="<?=(isset($_POST['return_trip']))?$_POST['return_trip']:"NULL"?>">
            <input type="hidden" id="front_seat_type" value="<?=(isset($_POST['seat_type']))?$_POST['seat_type']:"NULL"?>">
            <input type="hidden" id="front_is_return" value="<?=(isset($_POST['is_return']))?$_POST['is_return']:"NULL"?>">
            <input type="hidden" id="passengar_count" value="0" name="passengar_count" />
            <input type="hidden" id="passengar_ids" value="" name="passengar_ids" />
            <!-- hidden fields -->
        </form>
        <!-- Address Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title"><?=$this->lang->line('add_new')?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-lg-3">
                                      <label class=""><?= $this->lang->line('contact_first_name'); ?></label>
                                      <input type="text" id="firstname" name="firstname" class="form-control" placeholder="<?= $this->lang->line('enter_first_name'); ?>" />
                                    </div>
                                    <div class="col-lg-3">
                                      <label class=""><?= $this->lang->line('contact_last_name'); ?></label>
                                      <input type="text" name="lastname" class="form-control" id="lastname" placeholder="<?= $this->lang->line('enter_last_name'); ?>" />
                                    </div>
                                    <div class="col-lg-3">
                                      <label><?= $this->lang->line('email_address'); ?></label>
                                      <input type="email" class="form-control" id="add_email_id" name="add_email_id" placeholder="<?= $this->lang->line('enter_email_address'); ?>" />
                                    </div>
                                    <div class="col-lg-3">
                                      <label><?= $this->lang->line('gender'); ?></label>
                                      <select class="form-control" name="add_gender" id="add_gender">
                                        <option value=""><?= $this->lang->line('Select gender'); ?></option>
                                        <option value="M"><?= $this->lang->line('male'); ?></option>
                                        <option value="F"><?= $this->lang->line('female'); ?></option>
                                      </select>
                                    </div>
                                </div>
                            </div>  <!-- row -->
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="col-lg-3">
                                  <label class=""><?= $this->lang->line('Select Country'); ?></label>
                                  <select class="form-control select2" name="country_id" id="country_id">
                                    <option value=""><?= $this->lang->line('Select Country'); ?></option>
                                    <?php foreach ($countries as $c) :?>
                                      <option value="<?= $c['country_id'];?>"><?=$c['country_name'];?></option>
                                    <?php endforeach; ?>
                                  </select>
                                </div>
                                <div class="col-lg-3">
                                  <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                                  <input type="number" id="add_mobile" name="add_mobile" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number'); ?>" />
                                </div> 
                                <div class="col-lg-3">
                                  <!-- <label class=""><?= $this->lang->line('Age'); ?></label>
                                  <input type="number" id="add_age" name="add_age" class="form-control" placeholder="<?= $this->lang->line('Enter age'); ?>" /> -->

                                  <label class=""><?= $this->lang->line('DoB'); ?></label>
                                  <div class="input-group date">
                                    <input type="text" class="form-control" id="add_age" name="add_age" autocomplete="off" >
                                    <div class="input-group-addon dpAddon">
                                      <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                  </div>

                                </div>
                              </div>     
                            </div>  <!-- row -->
                        </div> <!-- panel_body -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button class="btn btn-info" id="add_addr"><?=$this->lang->line('Save Address')?></button>
                    </div>
                </div>
            </div>
        </div>
     </div>
   </div>
  </div>
 </div>
</div>

<script>
    //$(document).ready(function() {
        //var front_return_trip = $('#front_return_trip').val();
        //var front_seat_type = $('#front_seat_type').val();
        //var is_return = $('#front_is_return').val();


        /*if(is_return != "NULL" && is_return=='0') {
            if($("#onward_drop_point").is(":checked") && $("#onward_pickup_point").is(":checked")) {
                $("#tab3").toggle('slide');$("#tab1").hide();$(".o_p_d").removeClass('active');$(".p_info").addClass('active');
            }
            if( $("#bus_seat_type").val() == front_seat_type) {
                $('.append_seatype').empty();
                o_seat_type = $("#bus_seat_type").val();
                journey_date = "<?php echo $_POST['journey_date']; ?>";
                $(document).find('input[type=checkbox]:checked').removeAttr('checked');
                var sdd_id = "<?php echo $_POST['ownward_trip'] ?>";
                $.ajax({
                    type: "POST", 
                    url: "<?php echo base_url('user-panel-bus/get-seat'); ?>", 
                    data: {seat_type:o_seat_type,sdd_id:sdd_id, journey_date: journey_date},
                    success: function(res) {
                        var resp   = res.split(',');
                        response = resp;
                        console.log(response);
                        $(".zz").empty();
                        $(".price_info").empty();
                        $(".qwerty").empty();
                        if(res!="chooseright") {
                            $(".qwerty").append("<label style='color:#3498db;'><?=$this->lang->line('Available Seats In')?>&nbsp;"+ o_seat_type +":</label><label>&nbsp;"+ response[0] +" </label>"+ "<label style='color:#3498db;'>"+ o_seat_type +"&nbsp;<?=$this->lang->line('Seat Price :')?>&nbsp;</label><label>&nbsp;"+ response[1]+"&nbsp;"+response[2]+"</label><input type='hidden' name='currency_id' value='"+response[3]+"'><input type='hidden' name='currency_sign' value='"+response[2]+"'><input type='hidden' name='seat_price' value='"+response[1]+"'>");
                            $('#hiddendiv').removeClass('hidden');   
                            $(document).on('click', '.checkaddressclass', function() {
                                $(".price_info").empty();
                                selected = $("input[name='selected_passengers[]']:checked").map(function(){ return $(this).val(); }).get();
                                var select = selected.toString();
                                no_of_passengers = select.split(',');
                                console.log(no_of_passengers);
                                
                                $(".price_info").append("<div class='row'><h4><label style='color:#3498db;'><?=$this->lang->line('Fare Details')?></label></h4></div>");
                                $(".price_info").append("<div class='row'><label style='color:#3498db;'><?=$this->lang->line('Ownward Fare')?></label><br/><div class='col-md-8'><label>"+o_seat_type+"&nbsp;<?=$this->lang->line('Fare')?>&nbsp;("+no_of_passengers.length+"&nbsp;<?=$this->lang->line('Seat')?>)</label></div><div class='col-md-4'><label>"+(no_of_passengers.length*parseInt(response[1]))+"&nbsp;"+response[2]+"</label></div></div><hr/>");
                                $(".price_info").append("<div class='row'><div class='col-md-8'><label style='color:#3498db;'><?=$this->lang->line('Grand Total')?></label></div><div class='col-md-4'><label>"+(no_of_passengers.length*parseInt(response[1]))+"&nbsp;"+response[2]+"</label></div></div>");
                                if(selected==""){$(".price_info").empty();}
                            }); 
                        } else { $('#hiddendiv').addClass('hidden'); } 
                    }//success function
                });
            }
        } else if(is_return == '1') {
            $("#tab3").toggle('slide');$("#tab1").hide();$(".o_p_d").removeClass('active');$(".p_info").addClass('active');
            $('.append_seatype').empty();
            seat_type = $("#onward_bus_seat_type").val();
            journey_date = "<?php echo $_POST['journey_date']; ?>";
            $(document).find('input[type=checkbox]:checked').removeAttr('checked');
            var sdd_id = "<?php echo $_POST['ownward_trip'] ?>";

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('user-panel-bus/get-seat'); ?>", 
                data: { seat_type:seat_type, sdd_id:sdd_id, journey_date: journey_date },
                success: function(res){
                    var resp   = res.split(',');
                    response = resp;
                    console.log(response);
                    $(".zz").empty();
                    $(".price_info").empty();
                    $(".qwerty").empty();
                    if(res!="chooseright") {
                        $(".qwerty").append("<label style='color:#3498db;'><?=$this->lang->line('Available Seats In')?>&nbsp;"+ seat_type +":</label><label>&nbsp;"+ response[0] +" </label>"+ "<label style='color:#3498db;'>"+ seat_type +"&nbsp;<?=$this->lang->line('Seat Price :')?>&nbsp;</label><label>&nbsp;"+response[1] +"&nbsp;"+response[2]+"</label><input type='hidden' name='currency_id' value='"+response[3]+"'><input type='hidden' name='currency_sign' value='"+response[2]+"'><input type='hidden' name='seat_price' value='"+response[1]+"'>");
                    } else { $('#hiddendiv').addClass('hidden'); } 
                }//success funtion
            });
            var r_seat_type = $("#return_bus_seat_type").val();
            return_seat = r_seat_type;
            //console.log(response);
            $(document).find('input[type=checkbox]:checked').removeAttr('checked'); 
            var r_sdd_id = "<?php if(isset($_POST['return_trip']) && $_POST['return_trip'] != '') { echo $_POST['return_trip'];}else { echo ""; } ?>";
            $.ajax({
                type: "POST", 
                url: "<?php echo base_url('user-panel-bus/get-seat'); ?>", 
                data: {seat_type:r_seat_type, sdd_id:r_sdd_id, journey_date: journey_date},
                success: function(respo){
                    var respp   = respo.split(',');
                    return_response = respp;
                    $(".return_qwerty").empty();
                    $(".append_seatype").empty();
                    if(respo!="chooseright" && response!="chooseright") {
                        $(".return_qwerty").append("<label style='color:#3498db;'><?=$this->lang->line('Available Seats In')?>&nbsp;"+r_seat_type +":</label><label>&nbsp;"+ return_response[0] +" </label>"+ "<label style='color:#3498db;'>"+r_seat_type +"&nbsp;<?=$this->lang->line('Seat Price :')?>&nbsp;</label><label>&nbsp;"+ return_response[1] +"&nbsp;"+return_response[2]+"</label><input type='hidden' name='return_currency_id' value='"+return_response[3]+"'><input type='hidden' name='return_currency_sign' value='"+return_response[2]+"'><input type='hidden' name='return_seat_price' value='"+return_response[1]+"'>");     
                        $('#hiddendiv').removeClass('hidden');

                        $(".price_info").empty();
                        //console.log(seat_type);
                        $(document).on('click', '.checkaddressclass', function() {
                            selected = $("input[name='selected_passengers[]']:checked").map(function(){ return $(this).val(); }).get();
                            var select = selected.toString();
                            no_of_passengers = select.split(',');
                            console.log(no_of_passengers);
                            $(".price_info").empty();

                            $(".price_info").append("<div class='row'><h4><label style='color:#3498db;'><?=$this->lang->line('Fare Details')?></label></h4></div>");
                            $(".price_info").append("<div class='row'><label style='color:#3498db;'><?=$this->lang->line('Ownward Fare')?></label><br/><div class='col-md-8'><label>"+seat_type+"&nbsp;<?=$this->lang->line('Fare')?>&nbsp;("+no_of_passengers.length+"&nbsp;<?=$this->lang->line('Seat')?>)</label></div><div class='col-md-4'><label>"+(no_of_passengers.length*parseInt(response[1]))+"&nbsp;"+response[2]+"</label></div></div>");
                            $(".price_info").append("<br/><div class='row'><label style='color:#3498db;'><?=$this->lang->line('Return Fare')?></label><br/><div class='col-md-8'><label>"+return_seat+"&nbsp;<?=$this->lang->line('Fare')?>&nbsp;("+no_of_passengers.length+"&nbsp;<?=$this->lang->line('Seat')?>)</label></div><div class='col-md-4'><label>"+(no_of_passengers.length*parseInt(return_response[1]))+"&nbsp;"+return_response[2]+"</label></div></div><hr/><br/>");
                            $(".price_info").append("<div class='row'><div class='col-md-8'><label style='color:#3498db;'><?=$this->lang->line('Grand Total')?></label></div><div class='col-md-4'><label>"+((no_of_passengers.length*parseInt(return_response[1]))+(no_of_passengers.length*parseInt(response[1])))+"&nbsp;"+response[2]+"</label></div></div>");
                            if(selected==""){$(".price_info").empty();}
                        });
                    } else { $('#hiddendiv').addClass('hidden'); } 
                }//success funtion
            });//ajax
        }   */
    //});

    //onward_bus_seat_type
    $('.o_p_d').click(false);
    $('.r_p_d').click(false);
    $('.p_info').click(false);
    $('.t_p_details').click(false);
    $('.cancel_p').click(false);
    function emailValidate(email) {
        var check = "" + email;
        if((check.search('@')>=0)&&(check.search(/\./)>=0))
        if(check.search('@')<check.split('@')[1].search(/\./)+check.search('@')) return true;
        else return false;
        else return false;
    }

    var response = [];
    var return_response = [];
    var seat_type = "";
    var return_seat = "";
    var selected = "";
    var no_of_passengers = "";
    var no_of_seat = $("#no_of_seat").val();
    var dataT = "";
    $(document).ready(function() {
        dataT = $('#addressTableData_s').dataTable({
            "bLengthChange": false,
            "bInfo" : false,
            "pageLength": 6,
            "ordering": false,
            /*"language": {
                "paginate": {
                "previous":'←' ,
                "next" : '→'
                }
            }*/
        });
        $("#addr_search").keyup(function() {
            dataT.fnFilter(this.value);
        });  
    });

    $(document).ready(function() {
        $(".o_p_d").addClass('active');
    });

    $("#onward_bus_seat_type").on('change', function(){
        $('.append_seatype').empty();
        seat_type = $("#onward_bus_seat_type").val();
        journey_date = "<?php echo $_POST['journey_date']; ?>";
        $(document).find('input[type=checkbox]:checked').removeAttr('checked');
        var sdd_id = "<?php echo $_POST['ownward_trip'] ?>";
       
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('user-panel-bus/get-seat'); ?>", 
            data: {seat_type:seat_type, sdd_id:sdd_id, journey_date: journey_date},
            success: function(res){
                var resp   = res.split(',');
                response = resp;
              $(".zz").empty();
              $(".price_info").empty();
              $(".qwerty").empty();
              if(res!="chooseright")
              {
                $(".qwerty").append("<label style='color:#3498db;'><?=$this->lang->line('Available Seats In')?>&nbsp;"+ seat_type +":</label><label>&nbsp;"+ response[0] +" </label>"+
                "<label style='color:#3498db;'>"+ seat_type +"&nbsp;<?=$this->lang->line('Seat Price :')?>&nbsp;</label><label>&nbsp;"+response[1] +"&nbsp;"+response[2]+"</label><input type='hidden' name='currency_id' value='"+response[3]+"'><input type='hidden' name='currency_sign' value='"+response[2]+"'><input type='hidden' name='seat_price' value='"+response[1]+"'>");
              } else { $('#hiddendiv').addClass('hidden'); } 
            }//success funtion
        });

        $("#return_bus_seat_type").on('change', function(){
            var r_seat_type = $("#return_bus_seat_type").val();
            return_seat = r_seat_type;
            //console.log(response);
            $(document).find('input[type=checkbox]:checked').removeAttr('checked'); 
            var r_sdd_id = "<?php if(isset($_POST['return_trip']) && $_POST['return_trip'] != ''){ echo $_POST['return_trip']; } else { echo ""; } ?>";
            $.ajax({
                type: "POST", 
                url: "<?php echo base_url('user-panel-bus/get-seat'); ?>", 
                data: {seat_type:r_seat_type, sdd_id:r_sdd_id, journey_date: journey_date},
                success: function(respo) {
                    var respp   = respo.split(',');
                    return_response = respp;
                    $(".return_qwerty").empty();
                    $(".append_seatype").empty();
                    if(respo!="chooseright" && response!="chooseright") {
                        $(".return_qwerty").append("<label style='color:#3498db;'><?=$this->lang->line('Available Seats In')?>&nbsp;"+r_seat_type +":</label><label>&nbsp;"+ return_response[0] +" </label>"+ "<label style='color:#3498db;'>"+r_seat_type +"&nbsp;<?=$this->lang->line('Seat Price :')?>&nbsp;</label><label>&nbsp;"+ return_response[1] +"&nbsp;"+return_response[2]+"</label><input type='hidden' name='return_currency_id' value='"+return_response[3]+"'><input type='hidden' name='return_currency_sign' value='"+return_response[2]+"'><input type='hidden' name='return_seat_price' value='"+return_response[1]+"'>");     
                        $('#hiddendiv').removeClass('hidden');

                        $(".price_info").empty();
                        //console.log(seat_type);
                        $(document).on('click', '.checkaddressclass', function() {
                            selected = $("input[name='selected_passengers[]']:checked").map(function(){ return $(this).val();} ).get();
                            var select = selected.toString();
                            no_of_passengers = select.split(',');
                            $(".price_info").empty();

                            $(".price_info").append("<div class='row'><h4><label style='color:#3498db;'><?=$this->lang->line('Fare Details')?></label></h4></div>");
                            $(".price_info").append("<div class='row'><label style='color:#3498db;'><?=$this->lang->line('Ownward Fare')?></label><br/><div class='col-md-8'><label>"+seat_type+"&nbsp;<?=$this->lang->line('Fare')?>&nbsp;("+no_of_passengers.length+"&nbsp;<?=$this->lang->line('Seat')?>)</label></div><div class='col-md-4'><label>"+(no_of_passengers.length*parseInt(response[1]))+"&nbsp;"+response[2]+"</label></div></div>");
                            $(".price_info").append("<br/><div class='row'><label style='color:#3498db;'><?=$this->lang->line('Return Fare')?></label><br/><div class='col-md-8'><label>"+return_seat+"&nbsp;<?=$this->lang->line('Fare')?>&nbsp;("+no_of_passengers.length+"&nbsp;<?=$this->lang->line('Seat')?>)</label></div><div class='col-md-4'><label>"+(no_of_passengers.length*parseInt(return_response[1]))+"&nbsp;"+return_response[2]+"</label></div></div><hr/><br/>");
                            $(".price_info").append("<div class='row'><div class='col-md-8'><label style='color:#3498db;'><?=$this->lang->line('Grand Total')?></label></div><div class='col-md-4'><label>"+((no_of_passengers.length*parseInt(return_response[1]))+(no_of_passengers.length*parseInt(response[1])))+"&nbsp;"+response[2]+"</label></div></div>");
                            if(selected==""){ $(".price_info").empty(); }
                        });
                    } else { $('#hiddendiv').addClass('hidden'); } 
                }//success funtion
            });//ajax
        });
    });

    $("#add_addr").on("click", function(e){
        var count= 0;
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var add_email_id = $("#add_email_id").val();
        var add_gender = $('#add_gender :selected').val();
        var gender = '';
        if(add_gender=='M') { gender = 'Male'; } else { gender = 'Female'; }
        var country_id = $('#country_id :selected').val();
        var add_mobile = $("#add_mobile").val();
        var add_age = $("#add_age").val();
        if(!firstname){ count++; swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('enter_first_name')?>", 'warning');  }
        else if(!emailValidate(add_email_id)){ count++; swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('invalid_email')?>", 'warning');  }
        else if(!add_mobile){ count++; swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('enter_mobile_number')?>", 'warning');  }
        else if(add_mobile.length<6){ count++; swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Minimum 6 characters required!')?>", 'warning');  }
        else if(add_mobile.length>16){ count++; swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Maximum 16 characters required!')?>", 'warning');  }
        else if(!lastname){ count++; swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('enter_last_name')?>", 'warning');  }
        else if(!add_age){ count++; swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Enter Age')?>", 'warning');  }

        //console.log(firstname+" "+lastname+" "+add_email_id+" =>"+add_gender+"<= "+country_id+" "+add_mobile+" "+add_age);
        if(count==0) {
            $.ajax({
                type: "POST",
                url: "<?=base_url('user-panel-bus/address-book-add-details-from-booking-page');?>", 
                data: {
                    firstname:firstname,
                    lastname:lastname,
                    lastname:lastname,
                    email_id:add_email_id,
                    gender:add_gender,
                    country_id:country_id,
                    mobile:add_mobile,
                    age:add_age,
                },
                success: function(res) {
                    if(res!="failed") {
                        var res = res.split('@');
                        $('#myModal').modal('hide');
                        $("#addressTableData_s").dataTable().fnDestroy();
                        $('#table_body_append').append('<tr><td class="text-center"><input type="checkbox" class="checkaddressclass" name="selected_passengers[]"  value="'+res[0]+'" style="-ms-transform: scale(1.5); -moz-transform: scale(1.5); -webkit-transform: scale(1.5); -o-transform: scale(1.5); transform: scale(1.5);" ><label></label></td><td>'+firstname+' '+lastname+'</td><td>'+add_mobile+'</td><td>'+add_email_id+'</td><td>'+gender+'</td><td>'+res[1]+'</td></tr>');
                        //$('#addressTableData_s').dataTable();
                        $("#addressTableData_s").dataTable({
                            "bLengthChange": false,
                            "bInfo" : false,
                            "pageLength": 6,
                            "ordering": false,
                        });
                    }
                }//success funtion
            });        
        }
    });

    var o_seat_type= "";
    var response = [];
    $("#bus_seat_type").on('change', function(){
        $('.append_seatype').empty();
        o_seat_type = $("#bus_seat_type").val();
        journey_date = "<?php echo $_POST['journey_date']; ?>";
        $(document).find('input[type=checkbox]:checked').removeAttr('checked');
        var sdd_id = "<?php echo $_POST['ownward_trip'] ?>";
        $.ajax({
            type: "POST", 
            url: "<?php echo base_url('user-panel-bus/get-seat'); ?>", 
            data: {seat_type:o_seat_type, sdd_id:sdd_id, journey_date: journey_date},
            success: function(res) {
                var resp   = res.split(',');
                response = resp;
                $(".zz").empty();
                $(".price_info").empty();
                $(".qwerty").empty();
                if(res!="chooseright") {
                    $(".qwerty").append("<label style='color:#3498db;'><?=$this->lang->line('Available Seats In')?>&nbsp;"+ o_seat_type +":</label><label>&nbsp;"+ response[0] +" </label>"+ "<label style='color:#3498db;'>"+ o_seat_type +"&nbsp;<?=$this->lang->line('Seat Price :')?>&nbsp;</label><label>&nbsp;"+ response[1]+"&nbsp;"+response[2]+"</label><input type='hidden' name='currency_id' value='"+response[3]+"'><input type='hidden' name='currency_sign' value='"+response[2]+"'><input type='hidden' name='seat_price' value='"+response[1]+"'>");
                    $('#hiddendiv').removeClass('hidden');   
                    $(document).on('click', '.checkaddressclass', function() {
                        $(".price_info").empty();
                        selected = $("input[name='selected_passengers[]']:checked").map(function(){ return $(this).val(); }).get();
                        var select = selected.toString();
                        no_of_passengers = select.split(',');
                        
                        $(".price_info").append("<div class='row'><h4><label style='color:#3498db;'><?=$this->lang->line('Fare Details')?></label></h4></div>");
                        $(".price_info").append("<div class='row'><label style='color:#3498db;'><?=$this->lang->line('Ownward Fare')?></label><br/><div class='col-md-8'><label>"+o_seat_type+"&nbsp;<?=$this->lang->line('Fare')?>&nbsp;("+no_of_passengers.length+"&nbsp;<?=$this->lang->line('Seat')?>)</label></div><div class='col-md-4'><label>"+(no_of_passengers.length*parseInt(response[1]))+"&nbsp;"+response[2]+"</label></div></div><hr/>");
                        $(".price_info").append("<div class='row'><div class='col-md-8'><label style='color:#3498db;'><?=$this->lang->line('Grand Total')?></label></div><div class='col-md-4'><label>"+(no_of_passengers.length*parseInt(response[1]))+"&nbsp;"+response[2]+"</label></div></div>");
                        if(selected==""){ $(".price_info").empty(); }    
                    });
                }else { $('#hiddendiv').addClass('hidden'); }
            } //success function
        });
    });

    $("#prev_2").on("click", function(e) {
        e.preventDefault();
        $("#tab1").toggle('slide');
        $("#tab2").hide();
        $(".r_p_d").removeClass('active');$(".o_p_d").addClass('active');
    });

    $("#pre_3").on("click", function(e) {
        e.preventDefault();
        $("#tab2").toggle('slide');
        $("#tab3").hide();
        $(".p_info").removeClass('active');$(".r_p_d").addClass('active');
    });

    $("#preee_3").on("click", function(e) {
        e.preventDefault();
        $("#tab1").toggle('slide');
        $("#tab3").hide();
        $(".p_info").removeClass('active');$(".o_p_d").addClass('active');
    });

    $("#pre_5").on("click", function(e) {
        e.preventDefault();
        $("#tab3").toggle('slide');
        $("#tab5").hide();
        $(".t_p_details").removeClass('active');$(".p_info").addClass('active');
    });

    $("#pre_6").on("click", function(e) {
        e.preventDefault();
        $("#tab5").toggle('slide');
        $("#tab6").hide();
        $(".cancel_p").removeClass('active');$(".t_p_details").addClass('active');
    });

    $("#tab1btn").on("click", function(e) {
        e.preventDefault();
        var onward_pickup_point = $('input:radio[name="onward_pickup_point"]:checked').val();
        var onward_drop_point = $('input:radio[name="onward_drop_point"]:checked').val();
        if(!onward_pickup_point){ 
            swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Select Pickup Point')?>", 'warning');  
        } else if(!onward_drop_point){ swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Select Drop Point')?>", 'warning');  
        } else { $("#tab2").toggle('slide');$("#tab1").hide();$(".o_p_d").removeClass('active');$(".r_p_d").addClass('active'); }
    });

    $("#tab111btn").on("click", function(e) {
        e.preventDefault();
        var onward_pickup_point = $('input:radio[name="onward_pickup_point"]:checked').val();
        var onward_drop_point = $('input:radio[name="onward_drop_point"]:checked').val();
        if(!onward_pickup_point){ 
            swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Select Pickup Point')?>", 'warning');  
        } else if(!onward_drop_point){ 
            swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Select Drop Point')?>", 'warning');  
        } else { $("#tab3").toggle('slide');$("#tab1").hide();$(".o_p_d").removeClass('active');$(".p_info").addClass('active');}
    });

    $("#tab2btn").on("click", function(e) {
        e.preventDefault();
        var return_pickup_point = $('input:radio[name="return_pickup_point"]:checked').val();
        var return_drop_point = $('input:radio[name="return_drop_point"]:checked').val();
        if(!return_pickup_point){ 
            swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Select Pickup Point')?>", 'warning');  
        } else if(!return_drop_point){ 
            swal('<?=$this->lang->line('error')?>',"<?=$this->lang->line('Select Drop Point')?>", 'warning'); 
        } else { $("#tab3").toggle('slide');$("#tab2").hide();$(".r_p_d").removeClass('active');$(".p_info").addClass('active'); }
    });

    $("#tab3btn").on("click", function(e) {
        e.preventDefault();
        if(no_of_passengers.length==no_of_seat && selected!="") {
            $("#tab5").toggle('slide');
            $("#tab3").hide(); 
            $(".p_info").removeClass('active');$(".t_p_details").addClass('active'); 
        } else {
            swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('select').' '.$_POST['no_of_seat'].' '.$this->lang->line('passenger')?>", 'warning');
        }
        var mobile_no = $("#m_no").val();
        var email_address = $("#email_address").val();
        var src = $("#ownward_source").val();
        var dest = $("#ownward_destination").val();
        var journey_date = $("#journey_date").val();
        var onward_pickup_point = $('input:radio[name="onward_pickup_point"]:checked').val();
        var onward_drop_point = $('input:radio[name="onward_drop_point"]:checked').val();
        var asw = $(".infopage");
        $(".infopage").empty();
        //console.log(o_seat_type);
        $(asw).append('<div class="col-md-12">'+
                            '<div class="col-md-4 form-group">'+
                                '<div class="form-group text-center" style="border:1px solid #3498db;">'+
                                '<div class="row" style="padding-left:15px; padding-right:15px;">'+
                                   '<h5 style="color: #3498db"><?= $this->lang->line("direction_on_map"); ?></h5>'+
                                    '<div class="col-md-12 direc_map" style="margin-bottom: 8px;">'+
                                            
                                        '</div>'+
                                '</div></div>'+
                            '</div><!--*****************End left side*****************-->'+
                            '<div class="col-md-8 form-group"><!--*****************Start Right Side*****************-->'+
                                '<div class="row" style="border: 1px solid #3498db">'+
                                    '<div class="col-md-12">'+
                                       '<h5 style="color: #3498db"><?= $this->lang->line("Ownward Trip Details"); ?></h5>'+
                                    '</div>'+
                                    '<div class="col-md-12">'+
                                        '<div class="col-md-12">'+
                                            '<h5><strong><?=$this->lang->line("Journey Date")?>&nbsp;:&nbsp;'+journey_date+'</strong></h5>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<h5><strong><?=$this->lang->line("Source")?>&nbsp;:&nbsp;'+src+'</strong></h5>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<strong><?=$this->lang->line("Pickup Point")?>&nbsp;:&nbsp;</strong>'+onward_pickup_point+''+
                                        '</div>'+
                                        '<br/>'+
                                        '<div class="col-md-12">'+
                                            '<h5><strong><?=$this->lang->line("Destination")?>&nbsp;:&nbsp;'+dest+'</strong></h5>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<strong><?=$this->lang->line("Drop Point")?>&nbsp;:&nbsp;</strong>'+onward_drop_point+''+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<br/>'+
                                '<div class="row" style="border: 1px solid #3498db">'+
                                    '<div class="col-md-12">'+
                                        '<h5 style="color: #3498db"><?=$this->lang->line("Passengers Info")?></h5>'+
                                    '</div>'+
                                    '<div class="col-md-12 p_infooo">'+
                                    '</div>'+
                                    '<div class="col-md-12">'+
                                        '<div class="col-md-6">'+
                                            '<strong><?=$this->lang->line('lbl_email')?>&nbsp;:&nbsp;</strong>'+email_address+''+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<strong><?=$this->lang->line('mobile_no')?>&nbsp;:&nbsp;</strong>'+mobile_no+''+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<br/>'+
                                '<div class="row" style="border: 1px solid #3498db">'+
                                    '<div class="col-md-12">'+
                                        '<h5 style="color: #3498db"><?= $this->lang->line("Fare Details"); ?></h5>'+
                                    '</div>'+
                                    '<div class="col-md-12">'+
                                        '<div class="col-md-12">'+
                                            '<strong><?=$this->lang->line("Ownward Fare")?></strong>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="col-md-4">'+
                                                '<strong>'+
                                                o_seat_type+
                                                '&nbsp;<?=$this->lang->line("Fare")?>('+no_of_passengers.length+'&nbsp;<?=$this->lang->line("Seat")?>)&nbsp;:&nbsp;</strong>'+
                                            '</div>'+
                                            '<div class="col-md-8">'+
                                                no_of_passengers.length*parseInt(response[1])+   
                                            '&nbsp;'+response[2]+'</div><hr/>'+
                                            '<div class="col-md-4">'+
                                                '<label style="color:#3498db;"><?=$this->lang->line("Grand Total")?>'+   
                                            '</div>'+
                                            '<div class="col-md-8">'+
                                                no_of_passengers.length*parseInt(response[1])+   
                                            '&nbsp;'+response[2]+'</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div><!--*****************End Right side*****************-->'+
                        '</div>');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('user-panel-bus/get-bus-address-book'); ?>", 
            data: { no_of_passengers:no_of_passengers },
            success: function(res) { 
               $('.p_infooo').append(res);
            }//success funtion
        });     
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('user-panel-bus/get-bus-trip-pickup-drop-locations'); ?>", 
            data: { onward_pickup_point:onward_pickup_point, onward_drop_point:onward_drop_point },
            success: function(lat){
                var lati   = lat.split('@');
                var lat_long = [];
                lat_long = lati;
                //console.log(lat_long[0]+"<=>"+lat_long[1]);
                
                $('.direc_map').append('<iframe width="100%" height="300" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin='+lat_long[0]+'&destination='+lat_long[1]+'"></iframe>');
            }//success funtion
        });
    });

    $("#tab333btn").on("click", function(e) {
        e.preventDefault();
        $('.direc_map').empty();
        $('.p_infooo').empty();
        var no_of_seat = "<?=$_POST['no_of_seat']?>";
        var passengar_count = $("#passengar_count").val();
        //console.log(passengar_count);

        //if(no_of_passengers.length==no_of_seat && selected!="") {
        if(parseInt(no_of_seat) == parseInt(passengar_count)) {
            $("#tab5").toggle('slide');
            $("#tab3").hide(); 
            $(".p_info").removeClass('active');$(".t_p_details").addClass('active'); 

            var no_of_passengers = $("#passengar_ids").val();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('user-panel-bus/get-bus-address-book'); ?>", 
                data: { no_of_passengers:no_of_passengers },
                success: function(res) { 
                   $('.p_infooo').append(res);
                }
            });

            var onward_pickup_point = "<?=$_POST['pickup_point']?>";
            var onward_drop_point = "<?=$_POST['drop_point']?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('user-panel-bus/get-bus-trip-pickup-drop-locations'); ?>", 
                data: { onward_pickup_point:onward_pickup_point, onward_drop_point:onward_drop_point },
                success: function(lat){
                    var lati   = lat.split('@');
                    var lat_long = [];
                    lat_long = lati;
                    //console.log(lat_long[0]+"<=>"+lat_long[1]);
                    $('.direc_map').append('<iframe width="100%" height="300" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin='+lat_long[0]+'&destination='+lat_long[1]+'"></iframe>');
                }
            });
        } else {
            swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('select').' '.$_POST['no_of_seat'].' '.$this->lang->line('passenger')?>", 'warning');
        }
    });

    $("#tab5btn").on("click", function(e){
        e.preventDefault();
        $("#tab6").toggle('slide');
        $("#tab5").hide();
        $(".t_p_details").removeClass('active');$(".cancel_p").addClass('active');
    });

    $(document).ready(function() {
        var dataTable = $('#OnwardPickupTabel').dataTable({
            "bLengthChange": false,
            "ordering": false,
            "paging": false,
            "bInfo" : false
        });
        $("#pickupsearch").keyup(function() {
            dataTable.fnFilter(this.value);
        });    
    });

    $(document).ready(function() {
        var dataTable = $('#OnwardDropTabel').dataTable({
            "bLengthChange": false,
            "ordering": false,
            "paging": false,
            "bInfo" : false
        });
        $("#dropsearch").keyup(function() {
            dataTable.fnFilter(this.value);
        });    
    });

    $(document).ready(function() {
        var dataTable = $('#ReturnPickupTabel').dataTable({
            "bLengthChange": false,
            "ordering": false,
            "paging": false,
            "bInfo" : false
        });
        $("#r_pickup_search").keyup(function() {
            dataTable.fnFilter(this.value);
        });    
    });

    $(document).ready(function() {
        var dataTable = $('#ReturnDropTabel').dataTable({
            "bLengthChange": false,
            "ordering": false,
            "paging": false,
            "bInfo" : false
        });
        $("#r_drop_search").keyup(function() {
            dataTable.fnFilter(this.value);
        });
    });

    $(document).ready(function() {
        var dataT = $('#policy_table').dataTable({
            "bLengthChange": false,
            "ordering": false,
            "paging": false,
            "bInfo" : false
        });
    });

    $('#seatbookingsubmit').click(function (e) {
        e.preventDefault();
        swal({
            title: "<?= $this->lang->line('are_you_sure'); ?>",
            text: "<?= $this->lang->line('To Book This Ticket'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?= $this->lang->line('Yes, Book This Ticket.'); ?>",
            cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
            closeOnConfirm: false,
            closeOnCancel: false 
        },
        function (isConfirm) {
            if (isConfirm) { $('#simpleForm').submit();
            } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Ticket Not Booked Yet'); ?>", "error"); }
        }); 
    });
</script>


<script>
  $(document).on('click', '.checkaddressclass', function() {
    var no_of_seat = "<?=$_POST['no_of_seat']?>";
    var passengar_count = $("#passengar_count").val();
    var p_id = $(this).val()
    var passengar_ids = $("#passengar_ids").val()
    //console.log($(this).val());
    if($(this.checked)[0] && $(this.checked)[0] != undefined) { 
      if(parseInt(no_of_seat) > parseInt(passengar_count)) {
        $("#passengar_count").val((parseInt(passengar_count)+1));
        $("#passengar_ids").val($("#passengar_ids").val()+p_id+',');
      } else {
        $(this).prop('checked', false);
        swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('You can select only').$_POST['no_of_seat'].$this->lang->line(' seats'); ?>","error");
      }
    } else { 
      $("#passengar_count").val((parseInt(passengar_count)-1));
      $("#passengar_ids").val(passengar_ids.replace(p_id+',', ''));
    }
    console.log($("#passengar_ids").val());
  });
</script>