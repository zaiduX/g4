  <div class="normalheader small-header">
    <div class="hpanel">
      <div class="panel-body">
        <a class="small-header-action" href="">
          <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
          </div>
        </a>

        <div id="hbreadcrumb" class="pull-right">
          <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/standard-rates-list'; ?>"><?= $this->lang->line('Standard Rates'); ?></a></li>
              <li class="active"><span><?= $this->lang->line('Edit'); ?></span></li>
          </ol>
        </div>
        <h2 class="font-light m-b-xs">  <i class="fa fa-cogs fa-2x text-muted"></i> <?= $this->lang->line('Formula Weight Based Standard Rates'); ?> </h2>
        <small class="m-t-md"><?= $this->lang->line('Edit Standard Rates Details'); ?></small>    
      </div>
    </div>
  </div>
     
  <div class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="hpanel hblue">
          <div class="panel-body">
              
            <?php if($error = $this->session->flashdata('error')): ?>
              <div class="alert alert-danger text-center"><?= $error = $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

            <form role="form" action="<?= base_url('user-panel/update-standard-rates'); ?>" class="form-horizontal form-groups-bordered" id="standard_rate_add" method="post">
              <input type="hidden" name="rate_id" value="<?= $rates['rate_id']; ?>" />
              <div class="row">
                <div class="col-md-4">
                  <label for="rate_type" class="col-sm-4 control-label"><?= $this->lang->line('Country :'); ?></label>           
                  <div class="col-sm-8">              
                    <label class="control-label"><?= ucfirst($rates['country_name']); ?></label>                
                  </div>
                </div>
                <div class="col-md-4">
                  <label class="col-sm-4 control-label"><?= $this->lang->line('Currency :'); ?></label>
                  <div class="col-sm-8">               
                    <label class="control-label"><?= $rates['currency_title']. '  ( ' . $rates['currency_sign']. ' )'; ?></label>
                  </div>
                </div>
                <div class="col-md-4">
                  <label class="col-sm-4 control-label"><?= $this->lang->line('Category :'); ?></label>
                  <div class="col-sm-8">  
                    <label class="control-label"><?= $rates['cat_name']; ?></label>              
                  </div>
                </div>
              </div> <hr style="margin-bottom: 0px;"/>
              <div class="clear"></div><br />

              <div class="row">
                <div class="col-md-6">
                  <label for="rate_type" class="col-sm-4 control-label"><?=$this->lang->line('Min. Distance :');?></label>           
                  <div class="col-sm-8">    
                    <label class="control-label"><?= $rates['min_distance']. ' km'; ?></label>                
                  </div>
                </div>
                <div class="col-md-6">
                  <label class="col-sm-4 control-label"><?=$this->lang->line('Max. Distance :');?></label>
                  <div class="col-sm-8">  
                    <label class="control-label"><?= $rates['max_distance']. ' km'; ?></label>              
                  </div>
                </div>
              </div>          
              <div class="clear"></div><br />

              <?php $unit = $this->api->get_unit_detail($rates['unit_id']); ?>

              <!-- edit -->
              <div class="row">
                <div class="col-md-4"><hr/></div>
                <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Earth Rates &amp; Durations'); ?></h4></div>
                <div class="col-md-4"><hr/></div>
              </div>

              <div class="row">           
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="earth_local_rate" class="control-label"><?= $this->lang->line('Local Rate Per KM Per'); ?> <?=$unit['shortname']?></label>           
                  <div class="">              
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span>
                      <input type="number" class="form-control" id="earth_local_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="earth_local_rate" min="0" value="<?= $rates['earth_local_rate'];?>" disabled /> 
                    </div>
                    <span id="error_earth_local_rate"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="earth_national_rate" class="control-label"><?= $this->lang->line('National Rate Per KM Per'); ?> <?=$unit['shortname']?></label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span>
                      <input type="number" class="form-control" id="earth_national_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="earth_national_rate" min="0" value="<?= $rates['earth_national_rate'];?>" disabled  /> 
                    </div>
                    <span id="error_earth_national_rate"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label class="control-label"><?= $this->lang->line('International Per KM Per'); ?> <?=$unit['shortname']?></label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span>
                      <input type="number" class="form-control" id="earth_international_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="earth_international_rate" min="0" value="<?= $rates['earth_international_rate'];?>" disabled  />  
                    </div>
                    <span id="error_earth_international_rate"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />
              
              <div class="row">           
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="earth_local_duration" class="control-label"><?= $this->lang->line('Local Duration'); ?> </label>           
                  <div class="">              
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_local_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="earth_local_duration" min="1" value="<?= $rates['earth_local_duration'];?>" disabled  /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_earth_local_duration"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="earth_national_duration" class="control-label"><?= $this->lang->line('National Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_national_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="earth_national_duration" min="1" value="<?= $rates['earth_national_duration'];?>"  disabled /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_earth_national_duration"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="earth_international_duration" class="control-label"><?= $this->lang->line('International Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_international_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="earth_international_duration" min="1" value="<?= $rates['earth_international_duration'];?>" disabled  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_earth_international_duration"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />

              <div class="row">
                <div class="col-md-4"><hr/></div>
                <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Air Rates &amp; Durations'); ?></h4></div>
                <div class="col-md-4"><hr/></div>
              </div>
              <div class="row">
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="air_local_rate" class="control-label"><?= $this->lang->line('Local Rate Per KM Per'); ?> <?=$unit['shortname']?></label>           
                  <div class="">
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span>
                      <input type="number" class="form-control" id="air_local_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="air_local_rate" min="0" value="<?= $rates['air_local_rate'];?>" disabled /> 
                    </div>
                    <span id="error_air_local_rate"></span>             
                  </div>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="air_national_rate" class="control-label"><?= $this->lang->line('National Rate Per KM Per'); ?> <?=$unit['shortname']?></label>
                  <div class="">
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span> 
                      <input type="number" class="form-control" id="air_national_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="air_national_rate" min="0" value="<?= $rates['air_national_rate'];?>"  disabled /> 
                    </div>
                    <span id="error_air_national_rate"></span>
                  </div>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="air_international_rate" class="control-label"><?= $this->lang->line('International Per KM Per'); ?> <?=$unit['shortname']?></label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span> 
                      <input type="number" class="form-control" id="air_international_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="air_international_rate" min="0" value="<?= $rates['air_international_rate'];?>"  disabled />  
                    </div>
                    <span id="error_air_international_rate"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />
              <div class="row">           
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="air_local_duration" class="control-label"><?= $this->lang->line('Local Duration'); ?> </label>           
                  <div class="">              
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_local_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="air_local_duration" min="1" value="<?= $rates['air_local_duration'];?>" disabled /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_air_local_duration"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="air_national_duration" class="control-label"><?= $this->lang->line('National Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_national_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="air_national_duration" min="1" value="<?= $rates['air_national_duration'];?>" disabled  /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_air_national_duration"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="air_international_duration" class="control-label"><?= $this->lang->line('International Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_international_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="air_international_duration" min="1" value="<?= $rates['air_international_duration'];?>"  disabled />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_air_international_duration"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />

              <div class="row">
                <div class="col-md-4"><hr/></div>
                <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Sea Rates &amp; Durations'); ?></h4></div>
                <div class="col-md-4"><hr/></div>
              </div>      
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_local_rate" class="control-label"><?= $this->lang->line('Local Rate Per KM Per'); ?> <?=$unit['shortname']?></label>           
                  <div class="">
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span>             
                      <input type="number" class="form-control" id="sea_local_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" min="0" name="sea_local_rate" value="<?= $rates['sea_local_rate'];?>" disabled /> 
                    </div>
                    <span id="error_sea_local_rate"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_national_rate" class="control-label"><?= $this->lang->line('National Rate Per KM Per'); ?> <?=$unit['shortname']?></label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span>
                      <input type="number" class="form-control" id="sea_national_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" min="0" name="sea_national_rate" value="<?= $rates['sea_national_rate'];?>"  disabled /> 
                    </div>
                    <span id="error_sea_national_rate"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_international_rate" class="control-label"><?= $this->lang->line('International Per KM Per'); ?> <?=$unit['shortname']?></label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon"><strong class="fa"><?= $rates['currency_sign'];?></strong></span>
                      <input type="number" class="form-control" id="sea_international_rate" min="0" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="sea_international_rate" value="<?= $rates['sea_international_rate'];?>"  disabled />  
                    </div>
                    <span id="error_sea_international_rate"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />
              <div class="row">           
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="sea_local_duration" class="control-label"><?= $this->lang->line('Local Duration'); ?> </label>           
                  <div class="">              
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_local_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="sea_local_duration" min="1" value="<?= $rates['sea_local_duration'];?>" disabled /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_sea_local_duration"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_national_duration" class="control-label"><?= $this->lang->line('National Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_national_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="sea_national_duration" min="1" value="<?= $rates['sea_national_duration'];?>"  disabled  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_sea_national_duration"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_international_duration" class="control-label"><?= $this->lang->line('International Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_international_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="sea_international_duration" min="1" value="<?= $rates['sea_international_duration'];?>" disabled  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span id="error_sea_international_duration"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br /><hr/>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                  <a href="<?=base_url('user-panel/standard-rates-list')?>" class="btn btn-warning btn-outline"><i class="fa fa-arrow-left"></i> &nbsp; <?= $this->lang->line('back'); ?></a>            
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                  <div class="text-right">
                    <a id="btn_edit" class="btn btn-success">&nbsp; <?= $this->lang->line('Edit'); ?> &nbsp;</a>
                    <button id="btn_submit" type="submit" class="btn btn-primary hidden">&nbsp; <?= $this->lang->line('save_details'); ?> &nbsp;</button>
                  </div>
                </div>
              </div>
              
            </form>
      
          </div>
        </div>
      </div>
    </div>
  </div>

<script>
  var inputChanged = false; 
  $("#point_to_point_rates_edit input").change(function(){
    inputChanged = true;
  });

  $("#btn_edit").on('click', function(){
    $(this).addClass('hidden');
    $("input").prop('disabled',false);
    $("#btn_submit").removeClass('hidden');
  });

  $("input").not("input[type=submit]").on('change', function(event) { event.preventDefault();
    var input = $(this);
    $("span[id^='error_']").removeClass('error').text("");
  });

  $("#btn_submit").click(function(e){ e.preventDefault();
    $(".alert").removeClass('alert-danger alert-success').addClass('hidden').html("");            
    
    var earth_local_rate = parseFloat($("#earth_local_rate").val()).toFixed(2);
    var earth_national_rate = parseFloat($("#earth_national_rate").val()).toFixed(2);
    var earth_international_rate = parseFloat($("#earth_international_rate").val()).toFixed(2);       
    var air_local_rate = parseFloat($("#air_local_rate").val()).toFixed(2);
    var air_national_rate = parseFloat($("#air_national_rate").val()).toFixed(2);
    var air_international_rate = parseFloat($("#air_international_rate").val()).toFixed(2);       
    var sea_local_rate = parseFloat($("#sea_local_rate").val()).toFixed(2);
    var sea_national_rate = parseFloat($("#sea_national_rate").val()).toFixed(2);
    var sea_international_rate = parseFloat($("#sea_international_rate").val()).toFixed(2);

    var earth_local_duration = parseFloat($("#earth_local_duration").val()).toFixed(2);
    var earth_national_duration = parseFloat($("#earth_national_duration").val()).toFixed(2);
    var earth_international_duration = parseFloat($("#earth_international_duration").val()).toFixed(2);       
    var air_local_duration = parseFloat($("#air_local_duration").val()).toFixed(2);
    var air_national_duration = parseFloat($("#air_national_duration").val()).toFixed(2);
    var air_international_duration = parseFloat($("#air_international_duration").val()).toFixed(2);       
    var sea_local_duration = parseFloat($("#sea_local_duration").val()).toFixed(2);
    var sea_national_duration = parseFloat($("#sea_national_duration").val()).toFixed(2);
    var sea_international_duration = parseFloat($("#sea_international_duration").val()).toFixed(2);

    if(isNaN(earth_local_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration").removeClass('error').html("");
      $("#error_earth_local_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#earth_local_rate").focus();
    } else if(isNaN(earth_national_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_local_rate").removeClass('error').html("");
      $("#error_earth_national_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#earth_national_rate").focus();
    } else if(isNaN(earth_international_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_national_rate").removeClass('error').html("");
      $("#error_earth_international_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#earth_international_rate").focus();
    } else if(isNaN(earth_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");      
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#earth_local_duration").focus();
    } else if(isNaN(earth_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_earth_local_duration, #error_earth_local_rate").removeClass('error').html("");
      $("#error_earth_national_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#earth_national_duration").focus();
    } else if(isNaN(earth_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_earth_local_duration, #error_earth_national_rate, #error_earth_national_duration").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_earth_international_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#earth_international_duration").focus();
    } else if(isNaN(air_local_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_international_rate").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_air_local_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#air_local_rate").focus();
    } else if(isNaN(air_national_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_local_rate").removeClass('error').html("");
      $("#error_air_national_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#air_national_rate").focus();
    } else if(isNaN(air_international_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_national_rate").removeClass('error').html("");
      $("#error_air_international_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#air_international_rate").focus();
    } else if(isNaN(air_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_international_rate").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_air_local_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#air_local_duration").focus();
    } else if(isNaN(air_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_local_rate").removeClass('error').html("");
      $("#error_air_national_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#air_national_duration").focus();
    } else if(isNaN(air_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension, #error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_national_rate").removeClass('error').html("");
      $("#error_air_international_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#air_international_duration").focus();
    } else if(isNaN(sea_local_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_international_rate").removeClass('error').html("");
      $("#error_sea_local_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#sea_local_rate").focus();
    } else if(isNaN(sea_national_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_local_rate").removeClass('error').html("");
      $("#error_sea_national_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#sea_national_rate").focus();
    } else if(isNaN(sea_international_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_national_rate").removeClass('error').html("");
      $("#error_sea_international_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#sea_international_rate").focus();
    } else if(isNaN(sea_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_international_rate").removeClass('error').html("");
      $("#error_sea_local_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#sea_local_duration").focus();
    } else if(isNaN(sea_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_local_rate").removeClass('error').html("");
      $("#error_sea_national_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#sea_national_duration").focus();
    } else if(isNaN(sea_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_national_rate").removeClass('error').html("");
      $("#error_sea_international_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#sea_international_duration").focus();
    } else {          
      $("#error_max_duration, #error_sea_international_rate").removeClass('error').html(''); 
      $("#standard_rate_add")[0].submit(); 
    }
  });
</script>