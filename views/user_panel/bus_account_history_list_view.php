    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>
          
          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('e_wallet'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('e_wallet'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-account-history'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-exchange"></i> <?= $this->lang->line('transactions'); ?> </a>
            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-invoices-history'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-file-pdf-o"></i> <?= $this->lang->line('invoices'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('user_main_account_details'); ?></small> 
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                    <?php endif; ?>  
                </div>   
            </div>
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?= $this->lang->line('account_id'); ?></th>
                                    <th><?= $this->lang->line('date_time'); ?></th>
                                    <th class="hidden">Account Balance</th>
                                    <th><?= $this->lang->line('currency'); ?></th>
                                    <th><?= $this->lang->line('account_balance'); ?></th>
                                    <th><?= $this->lang->line('action'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($balance as $bal) { 
                                        $requested_amount = $this->user->get_customer_account_withdraw_request_sum_currencywise($bal['user_id'],'request',$bal['currency_code']); ?>
                                <tr>
                                    <td><?= $bal['account_id'] ?></td>
                                    <td><?= date('d-M-Y h:i:s A',strtotime($bal['update_datetime'])) ?></td>
                                    <td class="hidden"><?= $bal['currency_code'] . ' ' . $bal['account_balance'] ?></td>
                                    <td><?= $bal['currency_code'] ?></td>
                                    <td><?= $bal['account_balance'] - $requested_amount['amount'] ?></td>
                                    <td>
                                        <?php if($bal['account_balance'] - $requested_amount['amount'] > 0) { ?>
                                            <form action="<?= $this->config->item('base_url') . 'user-panel-bus/create-bus-withdraw-request'; ?>" method="post">
                                                <input type="hidden" name="account_id" value="<?= $bal['account_id'] ?>" />
                                                <input type="hidden" name="currency_code" value="<?= $bal['currency_code'] ?>" />
                                                <input type="hidden" name="amount" value="<?= $bal['account_balance'] - $requested_amount['amount'] ?>" />
                                                <button type="submit" class="btn btn-outline btn-info" ><i class="fa fa-retweet"></i> <?= $this->lang->line('withdraw'); ?> </button></td>
                                            </form>
                                        <?php } ?>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>