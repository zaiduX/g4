<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-bus/bus-operator-profile'); ?>"><span><?= $this->lang->line('operator_profile'); ?></span></a></li>
          <li class="active"><span><?= $this->lang->line('edit_documents'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-files-o fa-2x text-muted"></i> <?= $this->lang->line('documents'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('edit_details'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <div class="panel-body">
          <form action="<?= base_url('user-panel-bus/add-operator-document'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="uploadPhoto">
            <div class="form-group">
              <div class="col-md-12 text-center">
                  <?php if($this->session->flashdata('error')):  ?>
                  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  <?php endif; ?>
                  <?php if($this->session->flashdata('success')):  ?>
                  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  <?php endif; ?>  
              </div>   
            </div>
            <div class="row">
              <div class="col-md-6">
                <label class=""><?= $this->lang->line('document_type'); ?></label>
                <?php $arrType = array(); for ($i=0; $i < sizeof($document); $i++) { 
                          array_push($arrType, $document[$i]['doc_type']);
                  } ?>
                <select class="js-source-states" style="width: 100%" name="doc_type">
                  <?php if(!in_array('business', $arrType)) { ?>
                          <option value="business"><?= $this->lang->line('business'); ?></option>
                  <?php } ?>
                  <?php if(!in_array('insurance', $arrType)) { ?>
                          <option value="insurance"><?= $this->lang->line('insurance'); ?></option>
                  <?php } ?>
                  <?php if(!in_array('other', $arrType)) { ?>
                          <option value="other"><?= $this->lang->line('other'); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="row">
              <div class="col-md-6">
                <label class="">Select Document</label>
                <input type="file" onchange="ValidateSize(this)" class="form-control" name="attachment" id="fileAttachment" accept=".doc,.docx,.pdf">
                <span id="errorMsg" class="text-danger hidden"><strong><?= $this->lang->line('error'); ?></strong> <?= $this->lang->line('please_select_document'); ?></span>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="panel-footer"> 
              <div class="row">
                <div class="col-lg-12 text-left">
                  <button type="submit" class="btn btn-info" data-style="zoom-in" id="upload"><?= $this->lang->line('upload_document'); ?></button>  
                  <a href="<?= base_url('user-panel-bus/bus-operator-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>
                </div>
              </div>         
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#uploadPhoto").submit(function(e){ e.preventDefault(); });
  $("#upload").on('click', function(e) { 
    e.preventDefault(); 
    var file = $('#fileAttachment').val();
    if(file !=""){
      $("#uploadPhoto").get(0).submit();
    } 
    else { $('#errorMsg').removeClass('hidden'); }
  });

  function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 5) {
            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('Max file size is 5 MB'); ?>", "error");
            $(file).val('');
        } else {

        }
    }

</script>