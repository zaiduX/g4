	<style type="text/css">
		.table > tbody > tr > td {
			border-top: none;
		}
		.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
			padding-left: 0px; padding-right: 0px;
		}
		.dataTables_filter {
     display: none;
}
	</style>
	<div class="normalheader small-header">
		<div class="hpanel">
		    <div class="panel-body">
		        <a class="small-header-action" href="">
		          <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
		        </a>
		        <div id="hbreadcrumb" class="pull-right">
	            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('address_book'); ?></span></li>
	            </ol>
		        </div>
		        <h2 class="font-light m-b-xs">
		          <?= $this->lang->line('address_book'); ?> &nbsp;&nbsp;&nbsp;
             	<a class="btn btn-outline btn-info" href="<?=base_url('user-panel-laundry/address-book/add'); ?>"><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?></a>
		        </h2>
		    </div>
		</div>
	</div>

	<div class="content">
		<div class="row">
			<div class="col-md-1">
				<h4><i class="fa fa-search"></i> <?= $this->lang->line('search'); ?></h4>
			</div>
			<div class="col-md-11">
				<input type="text" id="searchbox" class="form-control">
			</div>
		</div>
		<table id="addressTableData" class="table hpanel">
            <thead>
                <tr>
                	<th class="hidden"></th>
                </tr>
            </thead>
            <tbody class="panel-body">
            <?php foreach ($address_details as $addr) { static $i = 0; $i++; ?>
           	<tr class="col-md-3">
           		<td>
		        	<div class="hpanel <?php if( $i%4 == 0) { echo 'hgreen'; } else if( $i%3 == 0) { echo 'hviolet'; } else if( $i%2 == 0) { echo 'hblue'; } else { echo "hyellow"; } ?>">
			            <div class="panel-body" style="height: 335px;">
			            	<div class="col-md-12">
			                <span class="pull-right">
		                    <a class="btn btn-info btn-circle" href="<?= base_url('user-panel-laundry/address-book/').$addr['addr_id'];?>">
		                    	<i class="fa fa-pencil"></i>
		                    </a>
		                    <a class="btn btn-danger btn-circle btn-delete" id="<?=$addr['addr_id'];?>">
			                    <i class="fa fa-trash"></i>
			                  </a>
			                  <br/><br/>
				           <?php if($addr['addr_type'] != 'NULL'): ?>
				           	<span class="btn btn-success btn-xs"><?= $addr['addr_type'];?><br /></span>
				           <?php endif; ?>
					            </span>
					            	<?php if($addr['image_url'] != 'NULL') { ?>
			                			<img style="max-width: 90px; height: auto;" alt="logo" class="img-thumbnail m-b" src="<?= $this->config->item('base_url') . $addr['image_url']; ?>">
					            	<?php } else { ?>
			                			<img style="max-width: 90px; height: auto;" alt="logo" class="img-thumbnail m-b" src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>">
			                		<?php } ?>
			            	</div>
			            	<div class="col-md-12">
			                	<h3><a> <?php if($addr['firstname'] != 'NULL') { echo $addr['firstname'].' '; } if($addr['lastname'] != 'NULL') { echo $addr['lastname']; } ?> </a></h3>
			                	<?php if($addr['comapny_name'] != 'NULL'): ?>
			                		<h5><i class="fa fa-building"></i> <a> <?= $addr['comapny_name']; ?></a></h5>
			                	<?php endif; ?>
			                	
			                	<h5><i class="fa fa-phone"></i> <a> <?php if($addr['mobile_no'] != 'NULL') {  echo $addr['mobile_no']; } else { echo $this->lang->line('not_provided'); } ?> </a></h5>
			                	<?php if($addr['email'] != 'NULL'): ?>
			                		<h5><i class="fa fa-envelope"></i> <a> <?= $addr['email']; ?> </a></h5>
			                	<?php endif; ?>
			                	
			                </div>
			            	<div class="col-md-12">
			            		<div class="text-muted font-bold m-b-xs hidden">
			            			<i class="fa fa-map-marker"></i> 
			            			<?php 
			            				if($addr['city_id'] > 0) {
			            					echo $this->user->get_city_name_by_id($addr['city_id']) . ', ';
			            				}
			            				if($addr['state_id'] > 0) {
			            					echo $this->user->get_state_name_by_id($addr['state_id']) . '<br />';
			            				}
			            				if($addr['country_id'] > 0) {
			            					echo $this->user->get_country_name_by_id($addr['country_id']) . ', ';
			            				}
			            				if($addr['zipcode'] != 'NULL') {
			            					echo $addr['zipcode'];
			            				}
			            			?>
			            	</div>
			            	<div class="col-md-12" style="word-break: break-all;">
				                <p>
				                	<i class="fa fa-map"></i>
				                	<?php if($addr['street_name'] != 'NULL') {  echo $addr['street_name'] . '<br />'; } else { echo $this->lang->line('not_provided').'<br />'; } ?>
				                </p>
				            </div>
			            </div>
		        	</div>
			    </td>
			</tr>
			<?php } ?>
           	</tbody>
        </table>
	</div>


<script>
  $('.btn-delete').click(function () {            
    var id = this.id;            
    swal({                
      title: <?= json_encode($this->lang->line('are_you_sure')); ?>,                
      text: "",                
      type: "warning",                
      showCancelButton: true,                
      confirmButtonColor: "#DD6B55",                
      confirmButtonText: <?= json_encode($this->lang->line('yes')); ?>,                
      cancelButtonText: <?= json_encode($this->lang->line('no')); ?>,                
      closeOnConfirm: false,                
      closeOnCancel: false, 
    },                
    function (isConfirm) {                    
      if (isConfirm) {                        
        $.post("<?=base_url('user-panel-laundry/delete-address')?>", {id: id}, 
        function(res){
          if($.trim(res) == "success") {          
            swal(<?= json_encode($this->lang->line('deleted')); ?>, "", "success");                            
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, "", "error");  }  
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled')); ?>, "", "error");  }  
    });            
  });
</script>