<style> .avatar2 { width: 35%; max-height: 180px;  } </style>
<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('Deliverer Details'); ?></span></li>
                </ol>
            </div>
            <form action="<?=base_url('user-panel/search-for-deliverers'); ?>" method="post" class="col-md-1">
                <button type="submit" class="btn btn-default btn-sm"> <i class="fa fa-long-arrow-left"></i> <?= $this->lang->line('back'); ?> </button>
            </form>
            <h2 class="font-light m-b-xs text-left" style="line-height: 25px;"> | &nbsp; <?= $this->lang->line('Deliverer Details'); ?> &nbsp;</h2>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-lg-4">
            <div class="hpanel hgreen">
                <div class="panel-body">                    
                    <?php if($profile['avatar_url'] !="NULL"): ?>
                        <img alt="logo" class="img-thumbnail avatar2" src="<?= base_url().$profile['avatar_url']; ?>"/>
                    <?php else: ?>
                        <img alt="logo" class="img-thumbnail avatar2" src="<?= $this->config->item('resource_url').'default-profile.jpg'; ?>"/>
                    <?php endif; ?>
                    <?php   
                        $country_name = $this->user->get_country_name_by_id($profile['country_id']); 
                        $state_name = $this->user->get_state_name_by_id($profile['state_id']);
                        $city_name = $this->user->get_city_name_by_id($profile['city_id']);

                        $today = date('Y-m-d H:i:s'); 
                        $last_login = strtotime($customer['last_login_datetime']);
                        $interval  = abs(strtotime($today) - $last_login);
                        $last_min   = round($interval / 60);
                    ?>
                    <h3>
                        <a><?= $profile['firstname']. ' '. $profile['lastname'] ?></a> 
                        <?php if($last_min < 10 ): ?>
                            <span class="pull-right btn btn-success btn-xs"><?= $this->lang->line('Online'); ?></span>
                        <?php else: ?>
                            <span class="pull-right btn btn-danger btn-xs"><?= $this->lang->line('Offline'); ?></span>
                        <?php endif; ?>
                    </h3>
                    <div class="text-muted font-bold m-b-xs"><?= $city_name. ', '. $country_name;  ?></div>
                    <p><?= $this->lang->line('email'); ?> <?= $profile['email_id'];?></p>
                    <p><?= $this->lang->line('contact'); ?> <?= $profile['contact_no'];?></p>                    
                    <hr/>
                    <p>
                        <h5><?= $this->lang->line('ratings'); ?> &nbsp; 
                            <?php for($i=0; $i < (int) $customer['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                            <?php for($i=0; $i < ( 5-(int) $customer['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                            ( <?= (int) $customer['ratings'] ?> / 5 )                        
                        </h5>
                    </p>
                </div>                
                <div class="panel-footer contact-footer">
                    <div class="row">
                        <div class="col-md-4 border-right">
                            <div class="contact-stat"><span><?= $this->lang->line('accepted_orders'); ?> </span> <strong><?= $accepted; ?></strong></div>
                        </div>
                        <div class="col-md-4">
                            <div class="contact-stat"><span><?= $this->lang->line('in_progress_orders'); ?> </span> <strong><?= $in_progress; ?></strong></div>
                        </div>
                        <div class="col-md-4 border-right">
                            <div class="contact-stat"><span><?= $this->lang->line('delivered_orders'); ?> </span> <strong><?= $delivered; ?></strong></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="hpanel">
                <div class="hpanel">

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#step-1"><?= $this->lang->line('company_details'); ?></a></li>
                        <li class=""><a data-toggle="tab" href="#step-2"><?= $this->lang->line('personal_details'); ?></a></li>
                        <li class=""><a data-toggle="tab" href="#step-3"><?= $this->lang->line('reviews'); ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="step-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('company_name'); ?></label>
                                    <div class="col-sm-8">
                                        <?= ($profile['company_name'] !="NULL") ? strtoupper($profile['company_name']) : "Not define"; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('introduction'); ?></label>
                                    <div class="col-sm-8">
                                        <?= ($profile['introduction'] !="NULL") ? strtoupper($profile['introduction']) : "Not define"; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('email_address'); ?></label>
                                    <div class="col-sm-8">
                                        <?= ($profile['email_id'] !="NULL") ? trim($profile['email_id']) : "Not define"; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('contact_number'); ?></label>
                                    <div class="col-sm-8">
                                        <?= ($profile['contact_no'] !="NULL") ? trim($profile['contact_no']) : "Not define"; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('address'); ?></label>
                                    <div class="col-sm-8">
                                        <?= ($profile['address'] !="NULL") ? trim($profile['address']) : "Not define"; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('zip'); ?></label>
                                    <div class="col-sm-8">
                                        <?= ($profile['zipcode'] !="NULL") ? trim($profile['zipcode']) : "Not define"; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('city_state_country'); ?></label>
                                    <div class="col-sm-8">
                                        <?= $city_name. ' / '. $state_name. ' / '. $country_name; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('shipping_mode'); ?></label>
                                    <div class="col-sm-8">
                                        <?= ($profile['shipping_mode']==0)?"Manual":"Automatic"; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="step-2" class="tab-pane">
                            <div class="panel-body">                                
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('gender'); ?></label>
                                    <div class="col-sm-8">
                                        <?= ($customer['gender']=='m')?"Male":"Female";?>
                                    </div>
                                </div>                                
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('overview'); ?></label>
                                    <div class="col-sm-8">
                                        <?= (trim($customer['overview'])!='NULL')?trim($customer['overview']):$this->lang->line('not_defined');;?>
                                    </div>
                                </div>
                                <?php if($customer['profession'] !="NULL"): ?>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('profession'); ?></label>
                                    <div class="col-sm-8">
                                        <?= trim($customer['overview']);?>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php if($customer['lang_known'] !="NULL"): ?>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('language_known'); ?></label>
                                    <div class="col-sm-8">
                                        <?= trim($customer['lang_known']);?>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('certificates_license'); ?></label>
                                    <div class="col-sm-8">
                                    <?php if($verify_certificates['is_verified'] == 0 AND $verify_certificates['is_rejected'] == 1 ): ?>
                                        <button class="btn btn-danger btn-circle" title="Rejected"><i class="fa fa-times-circle"></i></button>
                                    <?php elseif($verify_certificates['is_verified'] == 1 AND $verify_certificates['is_rejected'] == 0 ): ?>
                                        <button class="btn btn-info btn-circle" title="Verified"><i class="fa fa-check"></i></button>
                                    <?php else: ?>
                                        <button class="btn btn-warning btn-circle" title="<?= $this->lang->line('waiting for verification'); ?>"><i class="fa fa-exclamation-circle"></i></button>
                                    <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('insurance'); ?></label>
                                    <div class="col-sm-8">
                                        <?php if($verify_insurence['is_verified'] == 0 AND $verify_insurence['is_rejected'] == 1 ): ?>
                                        <button class="btn btn-danger btn-circle" title="Rejected"><i class="fa fa-times"></i></button>
                                    <?php elseif($verify_insurence['is_verified'] == 1 AND $verify_insurence['is_rejected'] == 0 ): ?>
                                        <button class="btn btn-info btn-circle" title="Verified"><i class="fa fa-check"></i></button>
                                    <?php else: ?>
                                        <button class="btn btn-warning btn-circle" title="<?= $this->lang->line('waiting for verification'); ?>"><i class="fa fa-exclamation-circle"></i></button>
                                    <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('already_carried_out_a_mission'); ?></label>
                                    <div class="col-sm-8">
                                    <?php if($is_delivered > 0 ): ?>
                                        <button class="btn btn-info btn-circle" ><i class="fa fa-check"></i></button>
                                    <?php else: ?>
                                        <button class="btn btn-danger btn-circle"><i class="fa fa-times"></i></button>
                                    <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row form-group hidden">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('availability'); ?></label>
                                    <div class="col-sm-8">
                                        <button class="btn btn-info btn-circle" type="button"><i class="fa fa-check"></i></button>
                                    </div>
                                </div>
                                <div class="row form-group hidden">
                                    <label class="col-sm-4 control-label"><?= $this->lang->line('average_response_time'); ?></label>
                                    <div class="col-sm-8">
                                        <?= $this->lang->line('within_an_hour'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        <div id="step-3" class="tab-pane">
                            <div class="panel-body no-padding">
                                <div class="chat-discussion" style="height: auto">
                                <?php if(COUNT($reviews)>0): ?>
                                    <?php foreach ($reviews as $rw): ?>
                                        <div class="chat-message">
                                            <?php if($rw['cust_avatar'] !="NULL"): ?>
                                                <img alt="logo" class="img-thumbnail" src="<?= base_url().$rw['cust_avatar']; ?>" />
                                            <?php else: ?>
                                                <img alt="logo" class="img-thumbnail" src="<?= $this->config->item('resource_url').'default-profile.jpg'; ?>" />
                                            <?php endif; ?>
                                            <div class="message">
                                                <a class="message-author"> <?= $rw['cust_name']; ?> </a>
                                                <span class="message-date"> <?= date('l, d-M-Y', strtotime($rw['review_datetime'])); ?> </span>
                                                    <span class="message-content">
                                                    <?= $rw['review_comment']; ?>
                                                    </span>
                                                <div class="m-t-md">
                                                    Ratings : 
                                                    <?php for($i=0; $i<(int)$rw['review_rating']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                                                    <?php for($i=0; $i<(5-(int) $rw['review_rating']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                                                    ( <?= (int) $rw['review_rating'] ?> / 5 )
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <div class="text-center">
                                        <?= $this->lang->line('no_record_found'); ?>                                        
                                    </div>
                                <?php endif; ?>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".send_request").on('click', function(event) {   event.preventDefault();
        var btn = $(this);
        var delivererid = $(this).data('delivererid');        
        var courier_order_id = $(this).data('orderid');        
        var request = $(this).data('request');        
        swal({                
          title: <?= json_encode($this->lang->line('are_you_sure'))?>,                
          text: "",                
          type: "warning",                
          showCancelButton: true,                
          confirmButtonColor: "#DD6B55",                
          confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,                
          cancelButtonText: <?= json_encode($this->lang->line('no'))?>,                
          closeOnConfirm: false,                
          closeOnCancel: false, 
        },                
        function (isConfirm) {                    
          if (isConfirm) {                        
            $.post('send-request-to-deliverer', {deliverer_id: delivererid, courier_order_id: courier_order_id, request: request }, 
            function(res){                
              if(res == "success") {
                btn.parent().remove();
                swal(<?= json_encode($this->lang->line('success'))?>, "", "success");                                            
              } else if(res == "exists") {
                swal(<?= json_encode($this->lang->line('failed'))?>, "", "warning");                            
                setTimeout(function() { window.location.reload(); }, 2000); 
              }
              else { swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");  }  
            }); 
          } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");  }  
        });       
    });
</script>