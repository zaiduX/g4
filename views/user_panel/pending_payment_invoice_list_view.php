    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>
          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Pending Payments'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Pending Payments'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('View Pending Payments'); ?></small> 
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th><?= $this->lang->line('cust_name'); ?></th>
                                    <th><?= $this->lang->line('from_address'); ?></th>
                                    <th><?= $this->lang->line('to_address'); ?></th>
                                    <th><?= $this->lang->line('distance'); ?></th>
                                    <th><?= $this->lang->line('pickup_date_time'); ?></th>
                                    <th><?= $this->lang->line('deliver_date_time'); ?></th>
                                    <th><?= $this->lang->line('order_price'); ?></th>
                                    <!-- <th><?= $this->lang->line('order_invoice'); ?></th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($details as $dtls) { ?>
                                <tr>
                                    <td><?= $dtls['cust_name'] ?></td>
                                    <td><?= $dtls['from_address'] ?></td>
                                    <td><?= $dtls['to_address'] ?></td>
                                    <td><?= $dtls['distance_in_km'] ?> KM</td>
                                    <td><?= $dtls['pickup_datetime'] ?></td>
                                    <td><?= $dtls['delivered_datetime'] ?></td>
                                    <td><?= $dtls['currency_sign'] . ' ' . $dtls['order_price'] ?></td>
                                    <!-- <td class="text-center"><a href="<?= base_url('resources/'.$dtls['invoice_url']) ?>" target="_blank"><i class="fa fa-file-pdf-o fa-2x"></i></a></td> -->
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>