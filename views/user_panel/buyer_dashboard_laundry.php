<div class="row">
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
          <h4> <?= $this->lang->line('total bookings'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
          <i class="pe-7s-portfolio fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h3 class="m-xs"><?=sizeof($total_bookings)?></h3>
        <span class="font-bold no-margins">
          <?= $this->lang->line('Completed Bookings'); ?>
        </span>
        <?php (sizeof($completed)>0 && sizeof($total_bookings)>0)?$per=round((sizeof($completed)/sizeof($total_bookings))*100,2):$per=0;?>
          <div class="progress m-t-xs full progress-small progress-striped active">
            <div style="width: <?=$per?> %" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?=$per?> %" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?> %"></div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('Completed'); ?></small>
              <h4><?=sizeof($completed)?></h4>
            </div>
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
              <h4><?=$per?>%</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
          <h4><?=$this->lang->line('accepted_bookings');?></h4>
        </div>
        <div class="stats-icon pull-right">
          <i class="pe-7s-note fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h3 class="m-xs"><?= sizeof($accepted); ?></h3>
        <span class="font-bold no-margins">
          <?= $this->lang->line('Cancelled Bookings'); ?>
        </span> 
          <?php (sizeof($accepted)>0 && sizeof($cancelled)>0)?$per=round((sizeof($accepted)/sizeof($cancelled))*100,2):$per=0; ?>  
          <div class="progress m-t-xs full progress-small progress-striped active">
            <div style="width: <?=$per?> %" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?=$per?> %" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?> %"></div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('canceled'); ?></small>
              <h4><?= sizeof($accepted) ?></h4>
            </div>
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
              <h4><?= $per; ?> %</h4>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
           <h4> <?= $this->lang->line('Completed Bookings'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
            <i class="pe-7s-trash fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h3 class="m-xs"><?=sizeof($completed)?></h3>
          <span class="font-bold no-margins">
            <?= $this->lang->line('in_progress_bookings'); ?>
          </span>
          <?php $per = (sizeof($completed)>0 && sizeof($in_progress)>0)?round((sizeof($completed)/sizeof($in_progress))*100,2):0; ?>  
          <div class="progress m-t-xs full progress-small progress-striped active">
            <div style="width: <?=$per?> %" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?=$per?> %" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?> %"></div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('in_progress'); ?></small>
              <h4><?= sizeof($in_progress); ?></h4>
            </div>
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
              <h4><?= $per; ?> %</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
          <h4><?= $this->lang->line('balance_summary'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
          <i class="pe-7s-cash fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h4 class="m-xs">XAF <?= $current_balance; ?></h4>
          <span class="font-bold no-margins">
            <?= $this->lang->line('current_balance'); ?>
          </span>
        </div>
        <div class="m-t-xs" style="margin-top: 0px;">
          &nbsp;
        </div>
        <div class="m-t-xs">
          <div class="row" style="margin-top: 0px;">
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('spent'); ?></small>
              <h4>XAF <?= $this->api->convert_big_int($total_spend);?> </h4>
            </div>
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('Laundry Spend'); ?></small>
              <h4>XAF <?= $this->api->convert_big_int($total_spend_service);?></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">  
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
        <?= $this->lang->line('accepted_bookings'); ?>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
            <thead>
              <th><strong><?= $this->lang->line('ID'); ?></strong></th>
              <th><strong><?= $this->lang->line('provider'); ?></strong></th>
              <th><strong><?= $this->lang->line('Items'); ?></strong></th>
              <th><strong><?= $this->lang->line('Price'); ?></strong></th>
            </thead>
            <tbody>
              <?php if(!empty($accepted)):?>                      
                <?php foreach ($accepted as $v): ?>                        
                  <tr>
                    <td>G-<?= $v['booking_id'];?><small></small></td>
                    <td><?= $v['operator_name']; ?><small></small></td>
                    <td><?= $v['item_count']; ?><small></small></td>
                    <td><?= $v['total_price'];?><small></small></td>
                  </tr>
                <?php endforeach; ?>                      
              <?php else: ?>
                <tr class="text-center"><td colspan="4"> <?= $this->lang->line('no_record_found'); ?></td></tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>      
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
        <?= $this->lang->line('in_progress_bookings'); ?>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
            <thead>
              <th><strong><?= $this->lang->line('ID'); ?></strong></th>
              <th><strong><?= $this->lang->line('provider'); ?></strong></th>
              <th><strong><?= $this->lang->line('Items'); ?></strong></th>
              <th><strong><?= $this->lang->line('Price'); ?></strong></th>
            </thead>
            <tbody>
              <?php if(!empty($in_progress)):?>                      
                <?php foreach ($in_progress as $v): ?>                        
                  <tr>
                    <td>G-<?= $v['booking_id'];?><small></small></td>
                    <td><?= $v['operator_name']; ?><small></small></td>
                    <td><?= $v['item_count']; ?><small></small></td>
                    <td><?= $v['total_price'];?><small></small></td>
                  </tr>
                <?php endforeach; ?>                      
              <?php else: ?>
                <tr class="text-center"><td colspan="4"> <?= $this->lang->line('no_record_found'); ?></td></tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div> 
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
         <?= $this->lang->line('Completed Bookings'); ?>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
            <thead>
              <th><strong><?= $this->lang->line('ID'); ?></strong></th>
              <th><strong><?= $this->lang->line('provider'); ?></strong></th>
              <th><strong><?= $this->lang->line('Items'); ?></strong></th>
              <th><strong><?= $this->lang->line('Price'); ?></strong></th>
            </thead>
            <tbody>
              <?php if(!empty($completed)):?>                      
                <?php foreach ($completed as $v): ?>                        
                  <tr>
                    <td>G-<?= $v['booking_id'];?><small></small></td>
                    <td><?= $v['operator_name']; ?><small></small></td>
                    <td><?= $v['item_count']; ?><small></small></td>
                    <td><?= $v['total_price'];?><small></small></td>
                  </tr>
                <?php endforeach; ?>                      
              <?php else: ?>
                <tr class="text-center"><td colspan="4"> <?= $this->lang->line('no_record_found'); ?></td></tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>     
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
         <?= $this->lang->line('Cancelled Bookings'); ?>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
            <thead>
              <th><strong><?= $this->lang->line('ID'); ?></strong></th>
              <th><strong><?= $this->lang->line('provider'); ?></strong></th>
              <th><strong><?= $this->lang->line('Items'); ?></strong></th>
              <th><strong><?= $this->lang->line('Price'); ?></strong></th>
            </thead>
            <tbody>
              <?php if(!empty($cancelled)):?>                      
                <?php foreach ($cancelled as $v): ?>                        
                  <tr>
                    <td>G-<?= $v['booking_id'];?><small></small></td>
                    <td><?= $v['operator_name']; ?><small></small></td>
                    <td><?= $v['item_count']; ?><small></small></td>
                    <td><?= $v['total_price'];?><small></small></td>
                  </tr>
                <?php endforeach; ?>                      
              <?php else: ?>
                <tr class="text-center"><td colspan="4"> <?= $this->lang->line('no_record_found'); ?></td></tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>     
    </div>
  </div>  
</div>