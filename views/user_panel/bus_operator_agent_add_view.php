 <style type="text/css">
  #sticky {
    padding: 0.5px;
    background-color: #225595;
    color: #fff;
    font-size: 1em;
    border-radius: 0.5ex;
}

#sticky.stick {
    margin-top: 133px !important;
    position: fixed;
    top: 0;
    z-index: 10000;
    border-radius: 0 0 0.5em 0.5em;
    width: 79%;
}
</style>
    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-list'; ?>"><?= $this->lang->line('Agent'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Add Agent'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light-xs">  <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('Agent'); ?> </h2>
          <small class="m-t-md"><?= $this->lang->line('Add agent details'); ?></small>    
        </div>
      </div>
    </div>
     
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">

                        <?php if($this->session->flashdata('error')):  ?>
                            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div><br />
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div><br />
                        <?php endif; ?>

                        <form method="post" class="form-horizontal" action="bus-operator-agent-add-details" id="agentAdd" enctype="multipart/form-data">
                            
                            <div class="form-group">
                              <div class="col-lg-2">
                                <label class=""><?= $this->lang->line('update_avatar'); ?></label>
                                <div class="input-group">
                                  <span class="input-group-btn">
                                    <button id="upload_avatar" class="col-lg-12 btn btn-green"><i class="fa fa-user"></i>&nbsp; <?= $this->lang->line('select_avatar'); ?></button> 
                                  </span>                      
                                </div>
                                <span id="avatar_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('avatar_attached'); ?></span>
                                <input type="file" id="avatar" name="avatar" class="upload attachment" accept="image/*" onchange="avatar_name(event)" style="width: inherit;" />
                              </div>
                              <div class="col-lg-2">
                                <label class=""><?= $this->lang->line('update_cover'); ?></label>
                                <div class="input-group">
                                  <span class="input-group-btn">
                                    <button id="upload_cover" class="col-lg-12 btn btn-green"><i class="fa fa-photo"></i>&nbsp; <?= $this->lang->line('select_cover'); ?></button>
                                  </span>                      
                                </div>
                                <span id="cover_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('cover_attached'); ?></span>
                                <input type="file" id="cover" name="cover" class="upload attachment" accept="image/*" onchange="cover_name(event)" style="width: inherit;" />
                              </div>
                              <div class="col-lg-4">
                                <label class=""><?= $this->lang->line('email_address'); ?></label>
                                <input type="text" class="form-control" id="email1" name="email1" />
                              </div>
                              <div class="col-lg-4">
                                <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                                <input type="text" class="form-control" name="mobile1" id="mobile1" />
                                
                              </div>                  
                            </div>

                            <div class="form-group">
                              <div class="col-lg-4">
                                <label class=""><?= $this->lang->line('first_name'); ?></label>
                                <input name="firstname" type="text" class="form-control" id="firstname" placeholder="Enter Your First Name" />
                              </div>
                              <div class="col-lg-4">
                                <label class=""><?= $this->lang->line('last_name'); ?></label>
                                <input name="lastname" type="text" class="form-control" id="lastname" placeholder="Enter Your Last Name" />
                              </div>
                              <div class="col-lg-4">
                                <label class=""><?= $this->lang->line('gender'); ?></label>
                                <select id="gender" name="gender" class="form-control select2" data-allow-clear="true" data-placeholder="Select Gender">
                                  <option value=""><?= $this->lang->line('Select Gender'); ?></option>
                                  <option value="M"><?= $this->lang->line('male'); ?></option>
                                  <option value="F"><?= $this->lang->line('female'); ?></option>
                                </select>
                              </div>
                            </div>

                            <div class="form-group">
                              <div class="col-lg-4">
                                <label class=""><?= $this->lang->line('password'); ?></label>
                                <input name="password" type="password" class="form-control" id="password" placeholder="Enter Password" />
                              </div>
                              <div class="col-lg-4">
                                <label class=""><?= $this->lang->line('Confirm Password'); ?></label>
                                <input name="confirm_password" type="password" class="form-control" id="confirm_password" placeholder="Confirm Password" />
                              </div>
                              <div class="col-lg-4">
                                <label class=""><?= $this->lang->line('country'); ?></label>
                                <select id="country_id" name="country_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Country">
                                  <option value=""><?= $this->lang->line('select_country'); ?></option>
                                  <?php foreach ($countries as $country): ?>
                                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                                  <?php endforeach ?>
                                </select>
                              </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <br>
                                    <button class="btn btn-primary" type="submit"><?= $this->lang->line('Add Agent Profile'); ?></button>
                                    <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-operator-agent-active-list'; ?>" class="btn btn-info" type="submit"><?= $this->lang->line('back'); ?></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
 $(function(){
    $("#agentAdd").validate({
      ignore: [],
      rules: {
        firstname: { required: true, },
        lastname: { required: true, },
        email1: { required: true, email: true, },
        password: { required: true, minlength: 6, maxlength: 16 },
        confirm_password: { required: true, equalTo: "#password"},
        mobile1: { required: true, number: true },
        gender: { required: true, },
        avatar: {  accept:"jpg,png,jpeg,gif" },
        cover: {  accept:"jpg,png,jpeg,gif" },
        country_id: { required: true, },
      }, 
      messages: {
        firstname: { required: <?= json_encode($this->lang->line('Enter First Name!'));?>,   },
        lastname: { required: <?= json_encode($this->lang->line('Enter Last name!'));?>,  },
        email1: { required: <?= json_encode($this->lang->line('Enter email address!'));?>, email: <?= json_encode($this->lang->line('Enter valid email address!'));?>, },
        password: { required: <?= json_encode($this->lang->line('Enter password!'));?>, minlength: <?= json_encode($this->lang->line('Password must be 6 characters!'));?>, maxlength: <?= json_encode($this->lang->line('Password must be less than 16 characters!'));?>, },
        confirm_password: { required: <?= json_encode($this->lang->line('Enter confirm password!'));?>, equalTo: <?= json_encode($this->lang->line('Password not matched!'));?>, },
        mobile1: { required: <?= json_encode($this->lang->line('enter_last_name'));?>, number: <?= json_encode($this->lang->line('Numbers only!'));?>, },
        gender: { required: <?= json_encode($this->lang->line('Select Gender!'));?>,  },
        avatar: {  accept: <?= json_encode($this->lang->line('only_image_allowed'));?>,   },
        cover: {  accept: <?= json_encode($this->lang->line('only_image_allowed'));?>,   },
        country_id: { required: <?= json_encode($this->lang->line('select_country'));?>,  },
      }
    });

    $("#upload_avatar").on('click', function(e) { e.preventDefault(); $("#avatar").trigger('click'); });
    $("#upload_cover").on('click', function(e) { e.preventDefault(); $("#cover").trigger('click'); });
  });

  function avatar_name(e){ if(e.target.files[0].name !="") { $("#avatar_name").removeClass('hidden'); }}
  function cover_name(e){ if(e.target.files[0].name !="") { $("#cover_name").removeClass('hidden'); }}
</script>