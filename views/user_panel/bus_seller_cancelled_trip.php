<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
    display: none;
  }
  input[type=checkbox] { display: none; }
  input[type=checkbox] + label {
    position: relative;
    height: 30px;
    width: 32px;
    display: block;
    transition: box-shadow 0.2s, border 0.2s;
    box-shadow: 0 0 1px #FFF;/* Soften the jagged edge */
    cursor: pointer;
  }
  input[type=checkbox] + label:hover,
  input[type=checkbox]:checked + label {
    border: solid 2px #1b5497;
    box-shadow: 0 0 1px #1b5497;
    border-radius: 5px;
  }
  input[type=checkbox]:checked + label:after {
    /* content: '\2714'; */
    /*content is required, though it can be empty - content: '';*/
    height: 1em;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    margin: auto;
    color: green;
    line-height: 1;
    font-size: 15px;
    text-align: center;
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 20px 0px rgba(0,0,0,2.0);
    background-color: #FFF;
    border: 3px outset #0AD909 !important;
  }
  .btn-sm { padding: 3px 5px !important; margin-bottom: 3px !important; }
  .truncate {
    width: 230px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  .truncate:hover {
    overflow: visible; 
    white-space: normal;
    width: auto;
  }
</style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url').'user-panel/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Cancel Trip'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-ticket fa-2x text-muted"></i> <?= $this->lang->line('Cancel Trip'); ?></h2>
      <small class="m-t-md"><?= $this->lang->line('Cancel Trip')." ".$this->lang->line('Detail'); ?></small> 
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
      <div class="hpanel">
        <div class="panel-body" style="padding-top: 12px; padding-bottom: 12px;">
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <table id="orderTableData" class="table">
                <thead><tr class="hidden"><th class="hidden">ID</th><th></th></tr></thead>
                <tbody>
                  <?php 
                    foreach ($cancelled_trips as $cancelled) {
                    $c = "yes";
                    $trip_list =  $this->api->get_ticket_master_booking_details_unique_id($cancelled['unique_id'], $c);               
                    // echo json_encode($trip_list);
                    foreach ($trip_list as $tList) { 
                    $customer_details = $this->api->get_user_details($tList['cust_id']);
                    $operator_details = $this->api->get_bus_operator_profile($tList['operator_id']);
                    //echo json_encode($operator_details);
                    $bus_details = $this->api->get_operator_bus_details($tList['bus_id']);
                    //echo json_encode($bus_details);
                    $trip_master_details = $this->api->get_trip_master_details($tList['trip_id']);
                    //echo json_encode($trip_master_details);
                    $total_passengers = sizeof($this->api->get_ticket_seat_details_unique_id($tList['unique_id']));
                  ?>
                    <tr>
                      <td class="hidden" style="line-height: 15px;"><?=$tList['trip_id'];?></td>
                      <td class="hoverme" style="line-height: 15px; border: 1px solid #3498db; padding: 0px;">
                        <div class="hpanel" style="line-height: 15px; margin-bottom: 0px !important;">
                          <div class="panel-body" style="line-height: 15px; padding-bottom: 0px; padding-top: 6px;">
                            <div class="row" style="line-height: 15px;">
                              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                
                                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-2" style="padding-left: 0px; padding-right: 0px;">
                                  <div class="row">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                        <img style="max-width: 14px; height: auto;" src="<?=base_url('resources/images/icon-connection-top@2x.png')?>" style="float: left;">
                                      </div>

                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px; height: 31px;">
                                        <h6 style="margin-top:5px; margin-bottom:0px; margin-left: 10px"><strong><?=$trip_master_details[0]['trip_depart_time']?></strong></h5>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                                        <h6 style="margin-top:0px; height:22px; margin-bottom:3px; font-size: 14px; display: inline-flex;"><i class="fa fa-clock-o" style="font-size: 20px;"></i>&nbsp;&nbsp;<?=$trip_master_details[0]['trip_duration']?><?=$this->lang->line('Hrs')?></h6>
                                      </div>
                                      
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                        <img style="max-width: 14px; height: auto;" src="<?=base_url('resources/images/icon-connection-bottom@2x.png')?>" style="float: left;">
                                      </div>

                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px; height: 31px;">
                                        <h6 style="margin-top:10px; margin-bottom:0px; margin-left: 10px"><?=date('H:i', strtotime($trip_master_details[0]['trip_depart_time'])+(60*60*$trip_master_details[0]['trip_duration']))?></h6>
                                      </div>

                                  </div>
                                </div>

                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-7" style="padding-left: 0px; padding-right: 0px;">
                                  <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                        <i class="fa fa-map-marker" style="font-size: 25px; color: #23b122;padding: 0px 4px;"></i>
                                      </div>
                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                        <h5 class="truncate"><font color="#3498db"><?=$tList['source_point']?></font></h5>
                                      </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 2px; padding-right: 0px;">
                                        <span style="font-size: 32px; color: #3498db">|</span>
                                      </div>
                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                        <h5 class="m-b-xs" style="margin-left: 5px;"> 
                                          <?php 
                                            $pickup_lat_long_string = $this->api->get_loc_lat_long_by_loc_id($tList['source_point_id']);
                                            $pickup_lat_long_array = explode(',',$pickup_lat_long_string);
                                            //echo json_encode($pickup_lat_long_array);
                                            $drop_lat_long_string = $this->api->get_loc_lat_long_by_loc_id($tList['destination_point_id']);
                                            $drop_lat_long_array = explode(',',$drop_lat_long_string);
                                            //echo json_encode($drop_lat_long_array);
                                          ?>
                                          <font color="#3498db">
                                            <?php if(sizeof($pickup_lat_long_array) == 2 && sizeof($drop_lat_long_array) == 2) { ?>
                                              <?php echo round($this->api->GetDrivingDistance($pickup_lat_long_array[0],$pickup_lat_long_array[1],$drop_lat_long_array[0],$drop_lat_long_array[1]),2) . ' KM'; ?>
                                            <?php } else { echo 'N/A'; } ?>
                                          </font>
                                        </h5>
                                      </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                        <i class="fa fa-map-marker" style="font-size: 25px;color: #d83131;padding: 0px 4px;"> </i>
                                      </div>
                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                        <h5 class="truncate"><font color="#3498db"><?=$tList['destination_point']?></font></h5>
                                      </div>
                                    </div>  
                                  </div>
                                </div>

                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3" style="padding-left: 0px; padding-right: 0px;">
                                  <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <h6><strong>
                                        <?php 
                                          if($tList['return_trip_id']>0) { 
                                            $r_tList = $this->api->get_trip_booking_details($tList['return_trip_id']);
                                            echo "#GT-".$tList['unique_id'];  } 
                                          else { echo "#GT-".$tList['unique_id']; } 
                                        ?> 
                                      </strong></h6>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                        <i class="fa fa-calendar" style="font-size: 17px; padding: 2px 0px;"></i>
                                      </div>
                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                        <h6>&nbsp;&nbsp;&nbsp;<?= $tList['journey_date'] ?></h6>
                                      </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px; padding-right: 0px;">
                                        <i class="fa fa-bus" style="font-size: 17px; padding: 2px 0px;"></i>
                                      </div>
                                      <div class="col-xl-11 col-lg-11 col-md-11 col-sm-11" style="padding-left: 0px; padding-right: 0px;">
                                        <h6>&nbsp;&nbsp;<?=' '.$bus_details['bus_no']?></h6>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4" style="padding-left: 0px; padding-right: 0px;">
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <a class="btn btn-link btn_view_vehicle_<?=$tList['unique_id']?>" style="color:#3498db;padding-top:5px; padding-bottom:0px; padding-left:0px; font-size:12px;"><?=$this->lang->line('Vehicle Detail')?></a>
                                    <a class="btn btn-link btn_hide_vehicle_<?=$tList['unique_id']?>" style="color:#3498db; padding-top:5px; padding-bottom:0px; padding-left:0px; font-size:12px;"><?=$this->lang->line("Vehicle Detail")?></a>
                                  </div>
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <a class="btn btn-link btn_view_cancelation_<?=$tList['unique_id']?>" style="color:#3498db; padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:11px;"><?=$this->lang->line("cancellation_charges")?></a>
                                    <a class="btn btn-link btn_hide_cancelation_<?=$tList['unique_id']?>" style="color:#3498db; padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:11px;"><?=$this->lang->line("cancellation_charges")?></a>
                                  </div>
                                </div>

                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 text-right" style="padding-left: 0px; padding-right: 0px;">
                                  <button class="btn btn-sm btn-outline btn-danger cancel_trip" ><i class="fa fa-times"></i> <?= $this->lang->line('Trip Cancelled'); ?> </button>
                                  <form action="<?=base_url('user-panel-bus/seller-cancelled-trips-view-passengers')?>" method="post">
                                    <input type="hidden" name="unique_id" value="<?= $tList['unique_id'] ?>">
                                    <button type="submit" class="btn btn-sm btn-outline btn-success" ><i class="fa fa-check"></i> <?= $this->lang->line('View Passengers'); ?> [<?=$total_passengers?>]</button>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>


                          <div class="panel-body" id="view_vehicle<?=$tList['unique_id']?>" style="display:none; padding-bottom: 5px; padding-top: 10px;">
                            <div class="row">
                              <div class="col-md-12" style="margin-top: 0px;"> 
                                <h6 style="margin-top: 0px; margin-bottom: 0px;">
                                  <strong><?= $this->lang->line('Amenities'); ?>: </strong><?=' '.str_replace('_',' ',str_replace(',',', ',$bus_details['bus_amenities']))?>
                                </h6>
                              </div>
                              <div class="col-md-12" style="padding-right: 0px;">
                                <h6 style="margin-top:3px;">
                                  <strong><?= $this->lang->line('Vehicle'); ?>: </strong><?=' '.$bus_details['bus_no'].' - '.$bus_details['bus_ac']. ', ' .$bus_details['bus_seat_type']?>
                                </h6>
                              </div>                                 
                            </div>
                          </div>
                          <div class="panel-body" id="cancelation_charge_<?=$tList['unique_id']?>" style="display:none; padding-bottom: 0px; padding-top: 10px;">
                            <?php
                              $o_d = $this->api->get_source_destination_of_trip($tList['trip_id']);
                              $location_details = $this->api->get_bus_location_details($o_d[0]['trip_source_id']);
                              $country_id = $this->api->get_country_id_by_name($location_details['country_name']);
                              $table_data= $this->api->get_cancellation_charges_by_country($o_d[0]['cust_id'],$o_d[0]['vehical_type_id'],$country_id);
                            ?>
                            <div class="row">
                              <div class="col-md-6">
                                <table id="policy_table" class="table">
                                  <thead>
                                    <tr>
                                      <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('Cancellation Time')?></h6></th>
                                      <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('cancellation_charges')?></h6></th>
                                      <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('rescheduling_charges')?></h6></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      if(sizeof($table_data) > 0) {
                                       foreach ($table_data as $td) {
                                          echo "<tr><td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>From ".$td['bcr_min_hours']." To ".$td['bcr_max_hours']." Hrs</h6></td>"; 
                                          echo "<td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>".$td['bcr_cancellation']."%</h6></td>";    
                                          echo "<td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>".$td['bcr_rescheduling']."%</h6></td></tr>"; 
                                        }
                                      } else {
                                        echo "<tr><td style='padding-top:2px; padding-bottom:2px;' colspan='3'><h6 style='margin-top:0px; margin-bottom:0px;'>".$this->lang->line('No policy found!')."</h6></td>"; 
                                      } 
                                    ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          <!-- scripts -->
                            <script>
                              $(".btn_hide_vehicle_<?=$tList['unique_id']?>").addClass("hidden");
                              $(".btn_view_cancelation_<?=$tList['unique_id']?>").addClass("hidden");
                              $(".btn_hide_seats_<?=$tList['unique_id']?>").addClass("hidden");
                            </script>
                            <script>
                              $(".btn_view_vehicle_<?=$tList['unique_id']?>").click(function(){
                                $("#view_vehicle<?=$tList['unique_id']?>").slideToggle(500);
                                $(".btn_view_vehicle_<?=$tList['unique_id']?>").addClass("hidden");
                                $(".btn_hide_vehicle_<?=$tList['unique_id']?>").removeClass("hidden");
                              });
                              $(".btn_hide_vehicle_<?=$tList['unique_id']?>").click(function(){
                                $("#view_vehicle<?=$tList['unique_id']?>").slideToggle(500);
                                $(".btn_view_vehicle_<?=$tList['unique_id']?>").removeClass("hidden");
                                $(".btn_hide_vehicle_<?=$tList['unique_id']?>").addClass("hidden");
                              });
                            </script>
                            <script>
                              $(".btn_view_cancelation_<?=$tList['unique_id']?>").click(function(){
                                $("#cancelation_charge_<?=$tList['unique_id']?>").slideToggle(500);
                                $(".btn_view_cancelation_<?=$tList['unique_id']?>").addClass("hidden");
                                $(".btn_hide_cancelation_<?=$tList['unique_id']?>").removeClass("hidden");
                              });
                              $(".btn_hide_cancelation_<?=$tList['unique_id']?>").click(function(){
                                $("#cancelation_charge_<?=$tList['unique_id']?>").slideToggle(500);
                                $(".btn_view_cancelation_<?=$tList['unique_id']?>").removeClass("hidden");
                                $(".btn_hide_cancelation_<?=$tList['unique_id']?>").addClass("hidden");
                              });
                            </script>
                            <script>
                              $(".btn_view_seats_<?=$tList['unique_id']?>").click(function(){
                                $("#view_seats<?=$tList['unique_id']?>").slideToggle(500);
                                $(".btn_view_seats_<?=$tList['unique_id']?>").addClass("hidden");
                                $(".btn_hide_seats_<?=$tList['unique_id']?>").removeClass("hidden");
                              });
                              $(".btn_hide_seats_<?=$tList['unique_id']?>").click(function(){
                                $("#view_seats<?=$tList['unique_id']?>").slideToggle(500);
                                $(".btn_view_seats_<?=$tList['unique_id']?>").removeClass("hidden");
                                $(".btn_hide_seats_<?=$tList['unique_id']?>").addClass("hidden");
                              });
                            </script>
                          <!-- scripts -->
                        </div>
                      </td>
                    </tr>
                  <?php }  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $('#aaad').click(false);   
</script>