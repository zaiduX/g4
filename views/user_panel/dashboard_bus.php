<div class="content" style="padding-top:30px;">
  <?php if($cust['email_verified'] == 0): ?>
    <div class="row">
      <div class="form-group"> 
        <div class="alert alert-warning alert-dismissible email-verify-alert" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?= $this->lang->line('warning'); ?></strong> <?= $this->lang->line('email_verification_warning'); ?> &nbsp; <a href="<?= base_url('resend-email-verification'); ?>"> <strong><?= $this->lang->line('resend_verification_email'); ?></strong></a>
        </div>
      </div>
    </div>
  <?php endif; ?>


  <div class="row">
    <div class="col-xl-1 col-lg-1 col-md-0 col-sm-0 text-center">&nbsp;</div>
    <div class="col-xl-8 col-lg-8 col-md-9 col-sm-9 text-center">
      <span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important; font-size: 2em;"><strong><?= $this->lang->line('Cheap bus tickets with the best agencies'); ?> </strong></span>
    </div>
    <div class="col-xl-1 col-lg-1 col-md-3 col-sm-3 text-right"> 
      <?php if($_SESSION['acc_type'] == 'seller' || $_SESSION['acc_type'] == 'both') { ?>
        <a class="btn btn-default" style="background-color:#1b5497; color: #fff; border: 1px solid #fff; border-radius: 20px;" href="<?=base_url('user-panel-bus/bus-trip-ticket-booking-seller')?>"><?= $this->lang->line('Book Ticket Now'); ?></a>
      <?php } else { ?>
        <a class="btn btn-default" style="background-color:#1b5497; color: #fff; border: 1px solid #fff; border-radius: 20px;" href="<?=base_url('user-panel-bus/bus-ticket-search')?>"><?= $this->lang->line('Book Ticket Now'); ?></a>
      <?php } ?>
    </div>
  </div>
  
  <div class="row" style="margin-bottom: 25px;">
    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-0 text-center">&nbsp;</div>
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">
      <h4 style="color: white"><strong><?= $this->lang->line('Save time, book and get your ticket by SMS, mail or withdrawal at the agency'); ?></strong></h4>
    </div>
    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-0 text-right">
      &nbsp;
    </div>
  </div>