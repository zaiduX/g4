    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>
          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('scrow_account'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('scrow_account'); ?>  &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/user-scrow-history'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-exchange"></i> <?= $this->lang->line('transactions'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('user_scrow_account_details'); ?></small> 
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?= $this->lang->line('scrow_id'); ?></th>
                                    <th><?= $this->lang->line('account_balance'); ?></th>
                                    <th><?= $this->lang->line('updated_datetime'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($balance as $bal) { ?>
                                <tr>
                                    <td><?= $bal['scrow_id'] ?></td>
                                    <td><?= $bal['currency_code'] . ' ' . $bal['scrow_balance'] ?></td>
                                    <td><?= date('d-M-Y h:i:s A',strtotime($bal['update_datetime'])) ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>