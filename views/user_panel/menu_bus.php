<?php  $email_cut = explode('@', $cust['email1']);  $name = $email_cut[0];  ?>
<style> .avatar { width: 85%; } </style>
<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <?php $url = parse_url($cust['avatar_url']);?>
        <div class="profile-picture">
            <a>
              <?php if($cust['avatar_url'] == "NULL"): ?>
                <a href="<?= $this->config->item('base_url') . 'user-panel-bus/user-profile'; ?>"><img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b avatar" alt="avatar" /></a>
              <?php else: ?>
                <a href="<?= $this->config->item('base_url') . 'user-panel-bus/user-profile'; ?>"><img src="<?= ( isset($url['scheme']) AND ($url['scheme'] == "https" || $url['scheme'] == "http" )) ? $cust['avatar_url'] : base_url($cust['avatar_url']); ?>" class="img-thumbnail m-b avatar" alt="avatar" /></a>
              <?php endif; ?>
            </a>

            <div class="stats-label text-color">
            <?php if( $cust['firstname'] != "NULL" || $cust['lastname'] != "NULL") : ?>
                <span class="font-extra-bold font-uppercase"><?= $cust['firstname'] . ' ' .$cust['lastname']; ?></span>
            <?php else: ?>
                <span class="font-extra-bold font-uppercase"><?= $name; ?></span>
            <?php endif; ?>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <small class="text-muted">Profile <b class="caret"></b></small>                      
                    </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/user-profile'; ?>"><?= $this->lang->line('edit_profile'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/change-password'; ?>"><?= $this->lang->line('change_password'); ?></a></li>
                        <li class="divider"></li>
                        <li><a href="<?= base_url('log-out'); ?>"><?= $this->lang->line('logout'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav" id="side-menu">
            <?php 
                $method_name = $this->router->fetch_method(); 
                $basic_info = array('bus_operator_profile','edit_bus_operator_profile','edit_bus_operator_bank','edit_bus_operator_document');
                
                $dashboard = array('dashboard_bus');

                $address_book = array('address_book_list','address_book_add','bus_address_book_edit');
                //Deliverer Profile
                $basic_info = array('bus_operator_profile','edit_deliverer_profile','edit_deliverer_bank','edit_deliverer_document');
                $business_photo = array('business_photos');
                $manage_bus = array('bus_list','bus_add','bus_edit');
                $pickup_drop_list = array('bus_pick_drop_point_list','bus_pick_drop_location_and_point_add','bus_pick_drop_point_list_view');
                $drivers = array('driver_list','add_driver','driver_inactive_list','edit_driver','view_driver');
                $agents = array('bus_operator_agent_active_list','bus_operator_agent_add','bus_operator_agent_inactive_list','bus_operator_agent_edit');
                $cancel_and_reschedule = array('bus_operator_cancellation_charges_list','add_bus_operator_cancellation_charges','edit_bus_operator_cancellation_charges');
                $special_rates = array('bus_trip_special_rate_list','bus_trip_special_rate_edit');
                //buyer Bookings
                $buyer_booking = array('bus_ticket_search','bus_trip_search_result_buyer','bus_trip_booking_buyer','bus_ticket_payment_buyer');
                $buyer_upcoming = array('buyer_upcoming_trips','bus_upcoming_trip_details_buyer','bus_ticket_payment_buyer');
                $buyer_current = array('buyer_current_trips','bus_current_trip_details_buyer',);
                $buyer_previous = array('buyer_previous_trips','bus_previous_trip_details_buyer');
                $buyer_cancelled = array('buyer_cancelled_trips','bus_cancelled_trip_details_buyer');

                //Seller booking
                $seller_booking = array('bus_trip_master_list','bus_trip_master_add','bus_trip_location_list','bus_trip_details_seller','bus_trip_ticket_booking_seller','bus_trip_search_result_seller','bus_trip_booking_pickup_drop_details','bus_ticket_payment_seller');
                $seller_upcoming = array('seller_upcoming_trips','bus_upcoming_trip_details_seller','seller_upcoming_trips_view_passengers');
                $seller_current = array('seller_current_trips','bus_current_trip_details_seller','seller_current_trips_view_passengers');
                $seller_previous = array('seller_previous_trips','bus_previous_trip_details_seller','selle_previous_trips_view_passengers');
                $seller_cancelled_tickets = array('seller_cancelled_tickets');
                $seller_cancelled_trips = array('seller_cancelled_trips');
            
                //Bus Accounts
                $wallet = array('bus_wallet','bus_account_history','create_bus_withdraw_request','bus_invoices_history');
                $scrow = array('my_scrow','user_scrow_history');
                $withdraw = array('bus_withdrawals_request','bus_withdrawals_accepted','bus_withdrawals_rejected','bus_withdrawal_request_details','withdraw_request_sent');

                $support_request = array('get_support_list','get_support','view_support_reply_details');
            ?>
            
            <?php 
            $orderCount = 0;
            if(isset($_SESSION['last_login_date_time'])) {
                $orderCount = $this->api->requested_order_count($_SESSION['cust_id'], $_SESSION['last_login_date_time']); } ?>

            <li class="<?php if(in_array($method_name, $dashboard)) { echo 'active'; } ?>">
                <a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"> <span class="nav-label"><?= $this->lang->line('dash'); ?></span></a>
            </li>

            <li class="<?php if(in_array($method_name, $address_book)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/address-book-list'; ?>"><?= $this->lang->line('Passengers List'); ?></a>
            </li>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
                <li class="<?php if(in_array($method_name, $basic_info) || in_array($method_name, $business_photo) || in_array($method_name, $manage_bus) || in_array($method_name, $pickup_drop_list) || in_array($method_name, $drivers) || in_array($method_name, $agents) || in_array($method_name, $cancel_and_reschedule) || in_array($method_name, $special_rates) ) { echo 'active'; } ?>">
                    <a href="#"><span class="nav-label"><?= $this->lang->line('operator_profile'); ?></span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level">
                        <li class="<?php if(in_array($method_name, $basic_info)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-operator-profile'; ?>"><?= $this->lang->line('basic_info'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $business_photo)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/business-photos'; ?>"><?= $this->lang->line('business_photos'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $manage_bus)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-list'; ?>"><?= $this->lang->line('Manage Vehicles'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $pickup_drop_list)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-pick-drop-point-list'; ?>"><?= $this->lang->line('Pickup Drop Locations'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $drivers)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/driver-list'; ?>"><?= $this->lang->line('drivers'); ?></a></li>
                        
                        <li class="hidden <?php if(in_array($method_name, $drivers)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/agent-permissions-list'; ?>"><?= $this->lang->line('Agent Permissions'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $agents)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-operator-agent-active-list'; ?>"><?= $this->lang->line('agents'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $cancel_and_reschedule)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-operator-cancellation-charges-list'; ?>"><?= $this->lang->line('cancellation_recheduling'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $special_rates)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-special-rate-list'; ?>"><?= $this->lang->line('special_rates'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'buyer') { ?>
                <li class="<?php if(in_array($method_name, $buyer_booking) || in_array($method_name, $buyer_upcoming) || in_array($method_name, $buyer_current) || in_array($method_name, $buyer_previous) || in_array($method_name, $buyer_cancelled) ) { echo 'active'; } ?>">
                    <a href="#"><span class="nav-label"><?= $this->lang->line('my_trips'); ?></span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level">
                        <li class="<?php if(in_array($method_name, $buyer_booking)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-ticket-search'; ?>"><?= $this->lang->line('Book Ticket'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $buyer_upcoming)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/buyer-upcoming-trips'; ?>"><?= $this->lang->line('upcoming'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $buyer_current)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/buyer-current-trips'; ?>"><?= $this->lang->line('current'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $buyer_previous)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/buyer-previous-trips'; ?>"><?= $this->lang->line('previous'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $buyer_cancelled)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/buyer-cancelled-trips'; ?>"><?= $this->lang->line('Cancelled Tickets'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
                <li class="<?php if(in_array($method_name, $seller_booking) || in_array($method_name, $seller_upcoming) || in_array($method_name, $seller_current) || in_array($method_name, $seller_previous) || in_array($method_name, $seller_cancelled_tickets) || in_array($method_name, $seller_cancelled_trips) ){ echo 'active'; } ?>">
                    <a href="#"><span class="nav-label"><?= $this->lang->line('manage_trips'); ?></span> <span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level">
                        <li class="<?php if(in_array($method_name, $seller_booking)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-master-list'; ?>"><?= $this->lang->line('Trip Master'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $seller_upcoming)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-upcoming-trips'; ?>"><?= $this->lang->line('upcoming'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $seller_current)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-current-trips'; ?>"><?= $this->lang->line('current'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $seller_previous)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-previous-trips'; ?>"><?= $this->lang->line('previous'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $seller_cancelled_trips)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-cancelled-trips'; ?>"><?= $this->lang->line('Cancelled Trips'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $seller_cancelled_tickets)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-cancelled-tickets'; ?>"><?= $this->lang->line('Cancelled Tickets'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <li class="<?php if(in_array($method_name, $wallet) || in_array($method_name, $withdraw) || in_array($method_name, $scrow)) { echo 'active'; } ?>">
                <a href="#"><span class="nav-label"><?= $this->lang->line('my_accounts'); ?></span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li class="<?php if(in_array($method_name, $wallet)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-wallet'; ?>"><?= $this->lang->line('e_wallet'); ?></a></li>
                    <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?><li class="<?php if(in_array($method_name, $scrow)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/my-scrow'; ?>"><?= $this->lang->line('scrow_account'); ?></a></li><?php } ?>
                    <li class="<?php if(in_array($method_name, $withdraw)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-withdrawals-request'; ?>"><?= $this->lang->line('withdrawals'); ?></a></li>
                </ul>
            </li>

            <li class="<?php if(in_array($method_name, $support_request)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/get-support-list'; ?>"><?= $this->lang->line('get_support'); ?></a>
            </li>
        </ul>
    </div>
</aside>

<!-- Main Wrapper -->
<?php
$method_name = $this->router->fetch_method(); 
if($method_name == 'dashboard_bus') { ?>
    <div id="wrapper" style="background: url(<?=base_url('resources/bus_services.jpg')?>); background-repeat: no-repeat; background-size: 100%;" >
<?php } else { ?>
    <div id="wrapper" style="" >
<?php } ?>