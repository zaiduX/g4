<style>
    input[type=checkbox] {
  display: none;
}
/* 
- Style each label that is directly after the input
- position: relative; will ensure that any position: absolute children will position themselves in relation to it
*/

input[type=checkbox] + label {
    position: relative;
    <?php if($_POST['bus_seat_type'] == 'SLEEPER') { ?>
        background: url("<?=base_url('resources/images/bed.png')?>") no-repeat;
    <?php } else { ?>
        background: url("<?=base_url('resources/images/busseat.png')?>") no-repeat;
    <?php } ?>
    height: 45px;
    width: 45px;
    display: block;
    transition: box-shadow 0.2s, border 0.2s;
    box-shadow: 0 0 1px #FFF;/* Soften the jagged edge */
    cursor: pointer;
}
/* Provide a border when hovered and when the checkbox before it is checked */

input[type=checkbox] + label:hover,
input[type=checkbox]:checked + label {
  border: solid 2px #1b5497;
  box-shadow: 0 0 1px #1b5497;
  border-radius: 5px;
  /* Soften the jagged edge */
}
/* 
- Create a pseudo element :after when checked and provide a tick
- Center the content
*/

input[type=checkbox]:checked + label:after {
  /* content: '\2714'; */
  /*content is required, though it can be empty - content: '';*/
  height: 1em;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  margin: auto;
  color: green;
  line-height: 1;
  font-size: 18px;
  text-align: center;
}
</style>


    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-list'; ?>"><?= $this->lang->line('Vehicles'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Add Vehicle Details'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light-xs">  <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Vehicles'); ?> </h2>
          <small class="m-t-md"><?= $this->lang->line('Add Vehicle Details'); ?></small>    
        </div>
      </div>
    </div>
     
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">

                        <?php if($this->session->flashdata('error')):  ?>
                        <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                        <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>

                        <form method="post" class="form-horizontal" action="bus-add-details" id="busAdd" enctype="multipart/form-data">
                            <input type="hidden" name="vehical_type_id" value="<?=$_POST['vehical_type_id']?>">
                            <input type="hidden" name="seat_type" value="<?=implode(',', $_POST['seat_type'])?>">
                            <input type="hidden" name="seat_type_count" value="<?=implode(',', $_POST['seat_type_count'])?>">
                            <input type="hidden" name="bus_make" value="<?=$_POST['bus_make']?>">
                            <input type="hidden" name="bus_modal" value="<?=$_POST['bus_modal']?>">
                            <input type="hidden" name="bus_no" value="<?=$_POST['bus_no']?>">
                            <input type="hidden" name="bus_seat_type" value="<?=$_POST['bus_seat_type']?>">
                            <input type="hidden" name="bus_ac" value="<?=$_POST['bus_ac']?>">
                            <input type="hidden" name="bus_amenities" value="<?=implode(',', $_POST['bus_amenities'])?>">
                            <input type="hidden" name="is_vip_seat" value="1">
                            <input type="hidden" name="image_url" value="<?=$image_url?>">
                            <input type="hidden" name="icon_url" value="<?=$icon_url?>">

                            <div class="row">
                                <div class="col-md-6">
                                    <h3><?= $this->lang->line('Bus Details'); ?></h3>
                                    <div class="col-md-5">
                                        <label><?= $this->lang->line('Bus Modal/No/Make'); ?></label>
                                    </div>
                                    <div class="col-md-7">
                                        <label><?=$_POST['bus_modal'].' '.$_POST['bus_no']. ' ['.$_POST['bus_make'].']' ?></label>
                                    </div>
                                    <div class="col-md-5">
                                        <label><?= $this->lang->line('bus_seat_type'); ?></label>
                                    </div>
                                    <div class="col-md-7">
                                        <label><?=$_POST['bus_seat_type']. ' ['.$_POST['bus_ac'].']' ?></label>
                                    </div>
                                    <div class="col-md-5">
                                        <label><?= $this->lang->line('Total Seats'); ?></label>
                                    </div>
                                    <div class="col-md-7">
                                        <label><?=(int)array_sum(explode(',', implode(',', $_POST['seat_type_count'])))?></label>
                                    </div>
                                    <div class="col-md-5">
                                        <label><?= $this->lang->line('vip_seats'); ?></label>
                                    </div>
                                    <div class="col-md-7">
                                        <label><?=$_POST['seat_type_count'][0] ?></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3><?= $this->lang->line('VIP selected seats'); ?> - <label id="lbl_selected_count">0</label></h3>
                                    <textarea class="form-control" type="text" name="vip_seats" value="" id="vip_seats" rows="3" readonly="readonly" style="resize: none; font-size: large;"></textarea>
                                </div>
                            </div>

                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <h3><?= $this->lang->line('Select VIP seats'); ?></h3>
                                </div>
                            </div>
                            <div class="row" style="border: 1px solid #000; border-radius: 15px; padding-top: 10px; padding-bottom: 5px;">
                                <div class="col-md-1" style="margin-top: 140px;">
                                    <img src="<?=base_url('resources/images/steering-wheel.png')?>" />
                                </div>
                                <div class="col-md-11">
                                    <?php
                                        //if($_POST['bus_seat_type'] == 'SLEEPER') {
                                            $total_seats = (int)array_sum(explode(',', implode(',', $_POST['seat_type_count'])));
                                            $vip_seats = explode(',', implode(',', $_POST['seat_type_count']))[0];
                                            $total_rows = ceil($total_seats/4);
                                            $odd_seats = $total_seats%4;
                                            $booked_seats = array(); //Booked Seats
                                            $seat_rows = array("A","B","E","C","D"); //Seats per row - E is walkway

                                            $walkway_seats = array();
                                            if($odd_seats == 1) {
                                                for($j=1; $j <= $total_rows-2; $j++) {
                                                    array_push($walkway_seats, "E".$j); //Walkway seats
                                                }
                                            } else {
                                                for($j=1; $j <= $total_rows; $j++) {
                                                    array_push($walkway_seats, "E".$j); //Walkway seats
                                                }
                                            }
                                            $s_types_a = $s_types_b = $s_types_c = $s_types_d = $s_types_e = '';
                                            echo "<table>";
                                            foreach($seat_rows as $i) {
                                                //$tot_row = $total_rows;
                                                if($odd_seats == 0) { if($odd_seats == 0) $tot_row = $total_rows+1; else $tot_row = $total_rows;
                                                } else if( ($odd_seats == 1 || $odd_seats == 2 || $odd_seats == 3) && $i == 'A') { 
                                                    if($odd_seats == 1) {
                                                        $tot_row = $total_rows;
                                                    } else {
                                                        $tot_row = $total_rows+1;
                                                    }
                                                } else if( ($odd_seats == 2 || $odd_seats == 3) && $i == 'B') { $tot_row = $total_rows+1;
                                                } else if($odd_seats == 3 && $i == 'C') { $tot_row = $total_rows+1;
                                                } else { $tot_row = $total_rows; }

                                                echo "<tr>";
                                                
                                                for($r=1; $r < $tot_row; $r++) {
                                                    $seat = $i.$r;
                                                    if(in_array($seat, $booked_seats)) {
                                                        $image = "<input type='checkbox' id='check_".$seat."' title='check_".$seat."' class='chck_".$seat."' value='".$seat."' />
                                                            <label style='padding-top:12px; padding-left:7px; font-size:10px' for='check_".$seat."'>".$seat."</label>
                                                        ";
                                                    } else if (!in_array($seat, $walkway_seats)) {
                                                        $image = "<input type='checkbox' id='check_".$seat."' title='check_".$seat."' class='chck_".$seat."' value='".$seat."' />
                                                            <label style='padding-top:12px; padding-left:7px; font-size:10px' for='check_".$seat."'>".$seat."</label>
                                                        ";
                                                    } else { $image = "&nbsp;"; }
                                                    echo "<td>".$image."</td>"; ?>
                                                    <script>
                                                        $(".chck_<?=$seat?>").bind('change', function (e) {
                                                            //console.log($(this).is(':checked'));
                                                            if(!$(this).is(':checked')){ 
                                                                var selected = $("#vip_seats").text(); 
                                                                var selected_len = ((selected.match(/,/g) || []).length) - 1 ;
                                                                $("#vip_seats").text(selected.replace($(this).val()+',', "").replace(/^,|,$/g,'')+',');
                                                                if($("#vip_seats").text() == ',') $("#vip_seats").text('');
                                                                $("#lbl_selected_count").text(selected_len);
                                                            } else { 
                                                                var total_seats = "<?=$vip_seats?>";
                                                                var selected = $("#vip_seats").text(); 
                                                                var selected_len = ((selected.match(/,/g) || []).length) + 1 ;
                                                                if(selected_len <= total_seats) {
                                                                    $("#vip_seats").text(($("#vip_seats").text()+$(this).val()).replace(/^,|,$/g,'')+','); 
                                                                    if($("#vip_seats").text() == ',') $("#vip_seats").text('');
                                                                    $("#lbl_selected_count").text(selected_len);
                                                                } else {
                                                                    swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('VIP seat limit exceeded!')?>",'warning');
                                                                    $('.chck_<?=$seat?>').prop('checked', false); // Unchecks it
                                                                }
                                                            }
                                                        });
                                                    </script>
                                                    <?php 
                                                    if($i == 'A') { $s_types_a = $s_types_a.','.$seat; }
                                                    if($i == 'B') { $s_types_b = $s_types_b.','.$seat; }
                                                    if($i == 'C') { $s_types_c = $s_types_c.','.$seat; }
                                                    if($i == 'D') { $s_types_d = $s_types_d.','.$seat; }
                                                    if($i == 'E') { if($odd_seats == 1) $s_types_e = $s_types_e.','.$seat; }
                                                } 
                                                echo "</tr>"; 
                                                
                                                if($odd_seats == 1 || 2 || 3) $tot_row--;

                                            } echo "</table>";
                                        /*} else {
                                            $total_seats = (int)array_sum(explode(',', implode(',', $_POST['seat_type_count'])));
                                            $vip_seats = explode(',', implode(',', $_POST['seat_type_count']))[0];
                                            $total_rows = ceil($total_seats/4);
                                            $odd_seats = $total_seats%4;
                                            $booked_seats = array(); //Booked Seats
                                            $seat_rows = array("A","B","E","C","D"); //Seats per row - E is walkway

                                            $walkway_seats = array();
                                            if($odd_seats == 1) {
                                                for($j=1; $j <= $total_rows-2; $j++) {
                                                    array_push($walkway_seats, "E".$j); //Walkway seats
                                                }
                                            } else {
                                                for($j=1; $j <= $total_rows; $j++) {
                                                    array_push($walkway_seats, "E".$j); //Walkway seats
                                                }
                                            }

                                            echo "<table>";
                                            foreach($seat_rows as $i) {
                                                //$tot_row = $total_rows;
                                                if($odd_seats == 0) { if($odd_seats == 0) $tot_row = $total_rows+1; else $tot_row = $total_rows;
                                                } else if( ($odd_seats == 1 || $odd_seats == 2 || $odd_seats == 3) && $i == 'A') { 
                                                    if($odd_seats == 1) {
                                                        $tot_row = $total_rows;
                                                    } else {
                                                        $tot_row = $total_rows+1;
                                                    }
                                                } else if( ($odd_seats == 2 || $odd_seats == 3) && $i == 'B') { $tot_row = $total_rows+1;
                                                } else if($odd_seats == 3 && $i == 'C') { $tot_row = $total_rows+1;
                                                } else { $tot_row = $total_rows; }

                                                echo "<tr>";
                                                for($r=1; $r < $tot_row; $r++) {
                                                    $seat = $i.$r;
                                                    if(in_array($seat, $booked_seats)) {
                                                        $image = "<input type='checkbox' id='check_".$seat."' title='check_".$seat."' class='chck_".$seat."' value='".$seat."' />
                                                            <label style='padding-top:12px; padding-left:7px; font-size:10px' for='check_".$seat."'>".$seat."</label>
                                                        ";
                                                    } else if (!in_array($seat, $walkway_seats)) {
                                                        $image = "<input type='checkbox' id='check_".$seat."' title='check_".$seat."' class='chck_".$seat."' value='".$seat."' />
                                                            <label style='padding-top:12px; padding-left:7px; font-size:10px' for='check_".$seat."'>".$seat."</label>
                                                        ";
                                                    } else { $image = "&nbsp;"; }
                                                    echo "<td>".$image."</td>"; ?>
                                                
                                                <script>
                                                    $(".chck_<?=$seat?>").bind('change', function (e) {
                                                        //console.log($(this).is(':checked'));
                                                        if(!$(this).is(':checked')){ 
                                                            var selected = $("#vip_seats").text(); 
                                                            var selected_len = ((selected.match(/,/g) || []).length) - 1 ;
                                                            $("#vip_seats").text(selected.replace($(this).val()+',', "").replace(/^,|,$/g,'')+',');
                                                            if($("#vip_seats").text() == ',') $("#vip_seats").text('');
                                                            $("#lbl_selected_count").text(selected_len);
                                                        } else { 
                                                            var total_seats = "<?=$vip_seats?>";
                                                            var selected = $("#vip_seats").text(); 
                                                            var selected_len = ((selected.match(/,/g) || []).length) + 1 ;
                                                            if(selected_len <= total_seats) {
                                                                $("#vip_seats").text(($("#vip_seats").text()+$(this).val()).replace(/^,|,$/g,'')+','); 
                                                                if($("#vip_seats").text() == ',') $("#vip_seats").text('');
                                                                $("#lbl_selected_count").text(selected_len);
                                                            } else {
                                                                swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('VIP seat limit exceeded!')?>",'warning');
                                                                $('.chck_<?=$seat?>').prop('checked', false); // Unchecks it
                                                            }
                                                        }
                                                    });
                                                </script>
                                            <?php } echo "</tr>"; if($odd_seats == 1 || 2 || 3) $tot_row--;
                                            } echo "</table>";
                                        }*/
                                    ?>
                                    <input type="hidden" name="s_types_a" value="<?=trim($s_types_a, ',')?>">
                                    <input type="hidden" name="s_types_b" value="<?=trim($s_types_b, ',')?>">
                                    <input type="hidden" name="s_types_c" value="<?=trim($s_types_c, ',')?>">
                                    <input type="hidden" name="s_types_d" value="<?=trim($s_types_d, ',')?>">
                                    <input type="hidden" name="s_types_e" value="<?=trim($s_types_e, ',')?>">
                                </div>
                            </div>

                            
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <br>
                                    <button class="btn btn-primary" type="submit" id="create_vehicle" name="create_vehicle" value="1"><?= $this->lang->line('Create Vehicle'); ?></button>
                                    <a href="<?=base_url('user-panel-bus/bus-list')?>" class="btn btn-info"><?= $this->lang->line('back'); ?></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $("#create_vehicle").click(function(event){
        event.preventDefault();
        var sel_count = $("#lbl_selected_count").text();
        var total_seats = "<?=$vip_seats?>";
        if(sel_count == total_seats) { this.form.submit();
        } else { swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('Select VIP seats!')?>",'warning'); }
    });
</script>

<script>
    $('#vehical_type_id').on('change', function() {
        var vehical_type_id = this.value;
        $.ajax({
            url: "<?=base_url('user-panel-bus/get-vehicle-seat-type')?>",
            type: "POST",
            data: { id: vehical_type_id },
            dataType: "json",
            success: function(res){
                //console.log(res);
                $.each( res, function(){ $('#seatTypes').append('<label class="text-left"><?= $this->lang->line('Number of'); ?> '+$(this).attr('st_name')+' <?= $this->lang->line('Seats'); ?></label>'+
                                        '<input type="hidden" name="seat_type[]" value="'+$(this).attr('st_name')+'">'+
                                        '<input type="number" placeholder="<?= $this->lang->line('Enter number of seats'); ?>" class="form-control bus'+$(this).attr('st_name')+'" name="seat_type_count[]" id="seat_type_count" value="0" min="0" required><br />');

                                        if($(this).attr('st_name') === 'VIP') {
                                            $(".bus"+$(this).attr('st_name')).bind('keyup change click', function (e) {
                                                var vip_seat_count = $(this).val();
                                                if(vip_seat_count > 0) {
                                                    $('#create_vehicle').addClass('hidden');
                                                    $('#manage_seat_layout').removeClass('hidden');
                                                } else {
                                                    $('#manage_seat_layout').addClass('hidden');
                                                    $('#create_vehicle').removeClass('hidden');
                                                }
                                            });
                                        }


                                        });
            },
            beforeSend: function(){
                $('#seatTypes').empty();
            },
            error: function(){
                $('#seatTypes').empty();
            }
        });
    })
</script>