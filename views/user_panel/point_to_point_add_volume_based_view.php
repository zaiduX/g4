 <style>
   input:focus {
    background-color: #ffb606;
}
 </style>
  <div class="normalheader small-header">
    <div class="hpanel">
      <div class="panel-body">
        <a class="small-header-action" href="">
          <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
          </div>
        </a>

        <div id="hbreadcrumb" class="pull-right">
          <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/ptop-rates-list'; ?>"><?= $this->lang->line('Point to Point Rates'); ?></a></li>
              <li class="active"><span><?= $this->lang->line('Volume Based'); ?></span></li>
          </ol>
        </div>
        <h2 class="font-light m-b-xs">  <i class="fa fa-cogs fa-2x text-muted"></i> <?= $this->lang->line('Volume Based Point to Point Rates'); ?> </h2>
        <small class="m-t-md"><?= $this->lang->line('Add Rates Details'); ?></small>    
      </div>
    </div>
  </div>
     
  <div class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="hpanel hblue">
          <div class="panel-body">
              
            <?php $error = $data = array(); if($error = $this->session->flashdata('error')): $data = $error['data']; ?>
              <div class="alert alert-danger text-center"><?= $error['error_msg']; ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

            <form role="form" action="<?= base_url('user-panel/register-ptop-volume-based'); ?>" class="form-horizontal form-groups-bordered" id="point_to_point_rates_add" method="post">
              <style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
              
              <div class="border">
                <div class="row">
                  <div class="col-md-12">
                    <label for="category" class="control-label"><?= $this->lang->line('select_category'); ?></label>              
                    <select id="category_id" name="category[]" class="form-control select2" multiple placeholder="<?= $this->lang->line('select_category'); ?>">
                      <?php foreach ($categories as $c ): ?>
                        <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
                          <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>

              <div class="border">
                <div class="row">
                  <div class="col-md-3">
                    <label for="from_country_id" class="control-label"><?= $this->lang->line('From Country'); ?></label>           
                    <select id="from_country_id" name="from_country_id" class="form-control select2">
                      <option value="0"><?= $this->lang->line('select_country'); ?></option>
                      <?php foreach ($countries_list as $country): ?>                     
                        <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                      <?php endforeach ?>                   
                    </select>
                  </div>            
                  <div class="col-md-3">
                    <label for="from_state_id" class="control-label"><?= $this->lang->line('From State'); ?></label>           
                    <select id="from_state_id" name="from_state_id" class="form-control select2" placeholder="<?= $this->lang->line('Select State'); ?>"  data-allow-clear="true" data-placeholder="<?= $this->lang->line('Select State'); ?>" disabled>
                      <option value="0"><?= $this->lang->line('Select State'); ?></option>                
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label for="from_city_id" class="control-label"><?= $this->lang->line('From City'); ?></label>           
                    <select id="from_city_id" name="from_city_id" class="form-control select2" placeholder="<?= $this->lang->line('Select City'); ?>"  data-allow-clear="true" data-placeholder="<?= $this->lang->line('Select City'); ?>" disabled>
                      <option value="0"><?= $this->lang->line('Select City'); ?></option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label class="control-label"><?= $this->lang->line('Country Currency'); ?></label>
                    <input type="hidden" name="currency_id" id="currency_id" value="0" />
                    <input type="text" name="currency_name" id="currency_name" class="form-control" disabled placeholder="<?= $this->lang->line('Country Currency'); ?>" />           
                  </div>
                </div>
                <div class="clear"></div><br />
            
                <div class="row">
                  <div class="col-md-4">
                    <label for="to_country_id" class="control-label"><?= $this->lang->line('To Country'); ?></label>           
                    <select id="to_country_id" name="to_country_id" class="form-control select2">
                      <option value="0"><?= $this->lang->line('select_country'); ?></option>
                      <?php foreach ($countries_list as $country): ?>                     
                        <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                      <?php endforeach ?>                   
                    </select>
                  </div>            
                  <div class="col-md-4">
                    <label for="to_state_id" class="control-label"><?= $this->lang->line('To State'); ?></label>           
                    <select id="to_state_id" name="to_state_id" class="form-control select2"  data-allow-clear="true" data-placeholder="<?= $this->lang->line('Select State'); ?>" disabled >
                      <option value="0"><?= $this->lang->line('Select State'); ?></option>                
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label for="to_city_id" class="control-label"><?= $this->lang->line('To City'); ?></label>           
                    <select id="to_city_id" name="to_city_id" class="form-control select2"  data-allow-clear="true" data-placeholder="<?= $this->lang->line('Select City'); ?>" disabled>
                      <option value="0"><?= $this->lang->line('Select City'); ?></option>
                    </select>
                  </div>            
                </div>
                <div class="clear"></div><br />
              </div>
                        
              <div class="border">
                <div class="row">
                  <div class="col-md-6">
                    <label for="min_dimension" class="control-label"><?= $this->lang->line('Min. Dimension'); ?></label>           
                    <select id="min_dimension" name="min_dimension" class="form-control select2">
                      <option value="0"><?= $this->lang->line('select_dimension'); ?></option>
                      <?php foreach ($dimensions_list as $dimension): ?>                      
                        <option value="<?= $dimension['dimension_id'] ?>" <?= (!empty($data) && ($data['min_dimension_id']==$dimension['dimension_id']))? "selected":"";?>><?= $dimension['dimension_type']; ?></option>
                      <?php endforeach ?>
                      <option value="other">Custom</option>
                    </select>
                    <div style="color:red;" id="error_min_dimension"></div>  
                    <input type="hidden" id="min_dimension_volume" name="min_dimension_volume" value="<?= (!empty($data))? $data['min_volume']:'0';?>" />
                    </div>

                  <div class="col-md-6">
                    <label for="max_dimension" class="control-label"><?= $this->lang->line('Max. Dimension'); ?></label>                         
                    <select id="max_dimension" name="max_dimension" class="form-control select2">
                      <option value="0"><?= $this->lang->line('select_dimension'); ?></option>
                      <?php foreach ($dimensions_list as $dimension): ?>                      
                        <option value="<?= $dimension['dimension_id'] ?>" <?= (!empty($data) && ($data['max_dimension_id']==$dimension['dimension_id']))? "selected":"";?>><?= $dimension['dimension_type']; ?></option>
                      <?php endforeach ?>
                      <option value="other"><?= $this->lang->line('Custom'); ?></option>
                    </select>
                    <div style="color:red;" id="error_max_dimension"></div> 
                    <input type="hidden" id="max_dimension_volume" name="max_dimension_volume" value="<?= (!empty($data))? $data['max_volume']:'0';?>" />
                  </div>
                </div>          
                <div class="clear"></div>
              
                <div class="row">
                  <div class="col-md-12">&nbsp;</div>
                  <div class="col-md-6">                            
                    <div class="row">
                      <div class="col-sm-4">
                        <label for="min_dimension" class="control-label"><?= $this->lang->line('Custom Min. Width'); ?></label>           
                        <input type="number" class="form-control" id="min_width" placeholder="<?= $this->lang->line('Width'); ?>" name="min_width" min="0" <?php if(!empty($data)){ echo 'value="'.$data['min_width'].'"'; } ?> readonly/>                     
                      </div>
                      <div class="col-sm-4">
                        <label for="min_dimension" class="control-label"><?= $this->lang->line('Custom Min. Height'); ?></label>           
                        <input type="number" class="form-control" id="min_height" placeholder="<?= $this->lang->line('height'); ?>" name="min_height" min="0" <?php if(!empty($data)){ echo 'value="'.$data['min_height'].'"'; } ?> readonly/>                     
                      </div>
                      <div class="col-sm-4">
                        <label for="min_dimension" class="control-label"><?= $this->lang->line('Custom Min. Length'); ?></label>           
                        <input type="number" class="form-control" id="min_length" placeholder="<?= $this->lang->line('length'); ?>" name="min_length" min="0" <?php if(!empty($data)){ echo 'value="'.$data['min_length'].'"'; } ?> readonly/>                     
                      </div>
                    </div>              
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-sm-4">
                        <label for="min_dimension" class="control-label"><?= $this->lang->line('Custom Max. Width'); ?></label>
                        <input type="number" class="form-control" id="max_width" placeholder="<?= $this->lang->line('Width'); ?>" name="max_width" max="0" <?php if(!empty($data)){ echo 'value="'.$data['max_width'].'"'; } ?> readonly/>                     
                      </div>
                      <div class="col-sm-4">
                        <label for="min_dimension" class="control-label"><?= $this->lang->line('Custom Max. Height'); ?></label>           
                        <input type="number" class="form-control" id="max_height" placeholder="<?= $this->lang->line('height'); ?>" name="max_height" max="0" <?php if(!empty($data)){ echo 'value="'.$data['max_height'].'"'; } ?> readonly/>                     
                      </div>
                      <div class="col-sm-4">
                        <label for="min_dimension" class="control-label"><?= $this->lang->line('Custom Max. Length'); ?></label>
                        <input type="number" class="form-control" id="max_length" placeholder="<?= $this->lang->line('length'); ?>" name="max_length" max="0" <?php if(!empty($data)){ echo 'value="'.$data['max_length'].'"'; } ?> readonly/>                     
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>
              
              <div class="border">
                <div class="row">
                  <div class="col-md-4"><hr/></div>
                  <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Earth Rates &amp; Durations'); ?></h4></div>
                  <div class="col-md-4"><hr/></div>
                </div>
            
                <div class="row">           
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('Local Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>
                          <input type="number" class="form-control" id="earth_local_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="earth_local_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_local_rate'].'"'; } ?> /> 
                        </div>
                      </div>
                      <div class="col-md-6">              
                        <div class="input-group">
                          <input type="number" class="form-control" id="earth_local_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="earth_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_local_duration'].'"'; } ?> /> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('National Rate &amp; Duration'); ?></label> 
                    <div class="row">    
                      <div class="col-md-6">                
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>
                          <input type="number" class="form-control" id="earth_national_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="earth_national_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_national_rate'].'"'; } ?>  /> 
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="earth_national_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="earth_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_national_duration'].'"'; } ?>  /> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('International Rate &amp; Duration'); ?></label> 
                    <div class="row">    
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>
                          <input type="number" class="form-control" id="earth_international_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="earth_international_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_international_rate'].'"'; } ?>  />  
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="earth_international_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="earth_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_international_duration'].'"'; } ?>  />  
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />        
              </div>
              
              <div class="border">
                <div class="row">
                  <div class="col-md-4"><hr/></div>
                  <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Air Rates &amp; Durations'); ?></h4></div>
                  <div class="col-md-4"><hr/></div>
                </div>
                <div class="row">
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('Local Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>
                          <input type="number" class="form-control" id="air_local_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="air_local_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_local_rate'].'"'; } ?> /> 
                        </div>
                      </div>                
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="air_local_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="air_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_local_duration'].'"'; } ?> /> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('National Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>  
                          <input type="number" class="form-control" id="air_national_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="air_national_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_national_rate'].'"'; } ?>  /> 
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="air_national_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="air_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_national_duration'].'"'; } ?>  /> 
                          <span class="input-group-addon">hrs</span>
                        </div>                
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('International Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>  
                          <input type="number" class="form-control" id="air_international_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="air_international_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_international_rate'].'"'; } ?>  />  
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="air_international_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="air_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_international_duration'].'"'; } ?>  />  
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>
              <div class="border">
                <div class="row">
                  <div class="col-md-4"><hr/></div>
                  <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Sea Rates &amp; Durations'); ?></h4></div>
                  <div class="col-md-4"><hr/></div>
                </div>      
                <div class="row">
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('Local Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>              
                          <input type="number" class="form-control" id="sea_local_rate" placeholder="<?= $this->lang->line('Rate'); ?>" min="0" name="sea_local_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_local_rate'].'"'; } ?> /> 
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="sea_local_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="sea_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_local_duration'].'"'; } ?> /> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>                
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('National Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>
                          <input type="number" class="form-control" id="sea_national_rate" placeholder="<?= $this->lang->line('Rate'); ?>" min="0" name="sea_national_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_national_rate'].'"'; } ?>  /> 
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="sea_national_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="sea_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_national_duration'].'"'; } ?>  /> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('International Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa">?</span>
                          <input type="number" class="form-control" id="sea_international_rate" min="0" placeholder="<?= $this->lang->line('Rate'); ?>" name="sea_international_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_international_rate'].'"'; } ?>  />  
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="sea_international_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="sea_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_international_duration'].'"'; } ?>  />  
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br /><hr/>
              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                  <a href="<?=base_url('user-panel/ptop-rates-list')?>" class="btn btn-warning btn-outline"><i class="fa fa-arrow-left"></i> &nbsp; <?= $this->lang->line('back'); ?></a>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                  <button id="btn_submit" type="submit" class="btn btn-success btn-outline"><i class="fa fa-plus"></i> &nbsp; <?= $this->lang->line('Add Rate'); ?></button>
                </div>
              </div>
              
            </form>
              
          </div>
        </div>
      </div>
    </div>
  </div>

<script>
  
  $("input").not("input[type=submit]").on('change', function(event) { event.preventDefault();
    var input = $(this);
    $("span[id^='error_']").removeClass('error').text("");
  });

  $("#from_country_id").on('change', function(event) { event.preventDefault();
    $(".currency").addClass('hidden').html('');
    $('#from_state_id').empty();
    $('#from_city_id').empty().append("<option value='0'><?= $this->lang->line('Select City'); ?></option>");

    var country_id = $(this).val();

    $.post('get-states', {country_id: country_id}, function(data) {
      data = $.parseJSON(data);
      if(data.length > 0 ){
        $('#from_state_id').attr('disabled', false);
        $('#from_state_id').empty(); 
        $('#from_state_id').append("<option value='0'><?= $this->lang->line('Select State'); ?></option>");
        $.each( data, function(){    
          $('#from_state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');              
        });
        $('#from_state_id').focus();
      } else {
        $('#from_state_id').attr('disabled', true);
        $('#from_state_id').empty();
        $('#from_state_id').append("<option value='0'><?= $this->lang->line('No state found!'); ?></option>");
      }
    });

    $.ajax({
      type: "POST", 
      url: "get-country-currencies", 
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        if(res.length > 0 ){ console.log(res[0]['currency_id']);
          $("#currency_id").val(res[0]['currency_id']);
          $("#currency_name").val(res[0]['currency_title'] + '( ' + res[0]['currency_sign'] + ' )');
          if(res[0]['currency_sign'] != null){ $(".currency").removeClass('hidden').html('<strong>'+res[0]['currency_sign']+'</strong>'); }
          else { $(".currency").addClass('hidden').html(''); }
        } 
        else{
          $('#currency_id').val('0');
          $('#currency_name').val("<?= $this->lang->line('No currency found!'); ?>");
          $(".currency").removeClass('hidden').html('?');
        }
      },
      beforeSend: function(){
      },
      error: function(){
        $(".currency").removeClass('hidden').html('?'); 
      }
    });
  });

  $("#from_state_id").on('change', function(event) { event.preventDefault();
    $('#from_city_id').empty();

    var state_id = $(this).val();

    $.post('get-cities', {state_id: state_id}, function(cities) {
      cities = $.parseJSON(cities);
      if(cities.length > 0 ){
        $('#from_city_id').attr('disabled', false);
          $('#from_city_id').empty(); 
          $('#from_city_id').append("<option value='0'><?= $this->lang->line('Select City'); ?></option>");
          $.each(cities, function(){    
              $('#from_city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>');              
          });
          $('#from_city_id').focus();
      }
      else{
          $('#from_city_id').attr('disabled', true);
          $('#from_city_id').empty();
          $('#from_city_id').append("<option value='0'><?= $this->lang->line('No city found!'); ?></option>");
        }
    });
  });

  $("#to_country_id").on('change', function(event) { event.preventDefault();
    $('#to_state_id').empty();
    $('#to_city_id').empty().append("<option value='0'><?= $this->lang->line('Select City'); ?></option>");

    var country_id = $(this).val();

    $.post('get-states', {country_id: country_id}, function(to_states) {
      to_states = $.parseJSON(to_states);
      if(to_states.length > 0 ){
        $('#to_state_id').attr('disabled', false);
          $('#to_state_id').empty(); 
          $('#to_state_id').append("<option value='0'><?= $this->lang->line('Select State'); ?></option>");
          $.each( to_states, function(){    
              $('#to_state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');              
          });
          $('#to_state_id').focus();
      }
      else{
          $('#to_state_id').attr('disabled', true);
          $('#to_state_id').empty();
          $('#to_state_id').append("<option value='0'><?= $this->lang->line('No state found!'); ?></option>");
        }
    });
  });

  $("#to_state_id").on('change', function(event) { event.preventDefault();
    $('#to_city_id').empty();

    var state_id = $(this).val();

    $.post('get-cities', {state_id: state_id}, function(cities) {
      cities = $.parseJSON(cities);
      if(cities.length > 0 ){
        $('#to_city_id').attr('disabled', false);
          $('#to_city_id').empty(); 
          $('#to_city_id').append("<option value='0'><?= $this->lang->line('Select City'); ?></option>");
          $.each(cities, function(){    
            $('#to_city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>');              
          });
          $('#to_city_id').focus();
      }
      else{
          $('#to_city_id').attr('disabled', true);
          $('#to_city_id').empty();
          $('#to_city_id').append("<option value='0'><?= $this->lang->line('No city found!'); ?></option>");
        }
    });
  });
  
  $("#currency_id").on('change', function(event) {  event.preventDefault();
    var currency = $("#currency_id option:selected").text();
    var sign = currency.match(/\((.*)\)/);
    if(sign != null){ $(".currency").removeClass('hidden').html('<strong>'+sign[1]+'</strong>'); }
    else { $(".currency").addClass('hidden').html(''); }
  });

  $("#min_dimension").on('change', function(event) {  event.preventDefault();
    var dimensions_list = <?= json_encode($dimensions_list); ?>;
    var id = $(this).val();
    console.log(dimensions_list);
    if(id != "other" && id > 0) {
      $("#min_width, #min_height, #min_length").prop('readonly', true);
      $.each(dimensions_list, function(i, el) {
        if(dimensions_list[i].dimension_id == id) { 
          $("#min_dimension_volume").val(dimensions_list[i].volume);
          $("#min_width").val(dimensions_list[i].width);
          $("#min_height").val(dimensions_list[i].height);
          $("#min_length").val(dimensions_list[i].length);
          $("#error_min_dimension").removeClass('error').addClass('text-primary').css('font-size','x-small').html("<?= $this->lang->line('Width:'); ?> "+dimensions_list[i].width +" x <?= $this->lang->line('Height:'); ?> "+dimensions_list[i].height +" x <?= $this->lang->line('Length:'); ?> "+dimensions_list[i].length +" = <?= $this->lang->line('Volume:'); ?> "+dimensions_list[i].volume);
        }
      });
    } else{ $("#min_width, #min_height, #min_length").val('').prop('readonly', false); $("#error_min_dimension").html('');  }
  });

  $("#max_dimension").on('change', function(event) {  event.preventDefault();
    var dimensions_list = <?= json_encode($dimensions_list); ?>;
    var id = $(this).val();
    if(id != "other" && id > 0) {
      $("#max_width, #max_height, #max_length").prop('readonly', true);
      $.each(dimensions_list, function(i, el) {
        if(dimensions_list[i].dimension_id == id) { 
          $("#max_dimension_volume").val(dimensions_list[i].volume);  
          $("#max_width").val(dimensions_list[i].width);
          $("#max_height").val(dimensions_list[i].height);
          $("#max_length").val(dimensions_list[i].length);    
          $("#error_max_dimension").removeClass('error').addClass('text-primary').css('font-size','x-small').html("<?= $this->lang->line('Width:'); ?> "+dimensions_list[i].width +" x <?= $this->lang->line('Height:'); ?> "+dimensions_list[i].height +" x <?= $this->lang->line('Length:'); ?> "+dimensions_list[i].length +" = <?= $this->lang->line('Volume:'); ?> "+dimensions_list[i].volume);
        }
      });
    } else{ $("#max_width, #max_height, #max_length").val('').prop('readonly', false); $("#error_max_dimension").html('');  }
  });

  $("#btn_submit").click(function(e){ e.preventDefault();
    $(".alert").removeClass('alert-danger alert-success').addClass('hidden').html("");        
    var categories = $("#category_id").val();
    console.log(categories); 
    var from_country_id = $("#from_country_id").val();
    var from_state_id = $("#from_state_id").val();
    var from_city_id = $("#from_city_id").val();

    var to_country_id = $("#to_country_id").val();
    var to_state_id = $("#to_state_id").val();
    var to_city_id = $("#to_city_id").val();

    var currency_id = $("#currency_id").val();
    var min_dimension = $("#min_dimension").val();
    var max_dimension = $("#max_dimension").val();

    var min_dimension_volume = 0; 
    var max_dimension_volume = 0; 

    if(min_dimension == "other") { 
      min_dimension_volume = (parseFloat($("#min_width").val()) * parseFloat($("#min_height").val()) *parseFloat($("#min_length").val()));
      min_dimension_volume = min_dimension_volume.toFixed(2);
      $("#min_dimension_volume").val(min_dimension_volume);
    } else {  
      min_dimension = parseFloat(min_dimension).toFixed(2);
      min_dimension_volume = parseFloat($("#min_dimension_volume").val()).toFixed(2); 
    }

    if(max_dimension == "other") { 
      max_dimension_volume = parseFloat($("#max_width").val()) * parseFloat($("#max_height").val()) *parseFloat($("#max_length").val()); 
      max_dimension_volume =  max_dimension_volume.toFixed(2);
      $("#max_dimension_volume").val(max_dimension_volume);
    } else { 
      max_dimension = parseFloat(max_dimension).toFixed(2);
      max_dimension_volume = parseFloat($("#max_dimension_volume").val()).toFixed(2); 
    }

    var min_distance = parseFloat($("#min_distance").val()).toFixed(2);
    var max_distance = parseFloat($("#max_distance").val()).toFixed(2);

    var earth_local_rate = parseFloat($("#earth_local_rate").val()).toFixed(2);
    var earth_national_rate = parseFloat($("#earth_national_rate").val()).toFixed(2);
    var earth_international_rate = parseFloat($("#earth_international_rate").val()).toFixed(2);       
    var air_local_rate = parseFloat($("#air_local_rate").val()).toFixed(2);
    var air_national_rate = parseFloat($("#air_national_rate").val()).toFixed(2);
    var air_international_rate = parseFloat($("#air_international_rate").val()).toFixed(2);       
    var sea_local_rate = parseFloat($("#sea_local_rate").val()).toFixed(2);
    var sea_national_rate = parseFloat($("#sea_national_rate").val()).toFixed(2);
    var sea_international_rate = parseFloat($("#sea_international_rate").val()).toFixed(2);

    var earth_local_duration = parseFloat($("#earth_local_duration").val()).toFixed(2);
    var earth_national_duration = parseFloat($("#earth_national_duration").val()).toFixed(2);
    var earth_international_duration = parseFloat($("#earth_international_duration").val()).toFixed(2);       
    var air_local_duration = parseFloat($("#air_local_duration").val()).toFixed(2);
    var air_national_duration = parseFloat($("#air_national_duration").val()).toFixed(2);
    var air_international_duration = parseFloat($("#air_international_duration").val()).toFixed(2);       
    var sea_local_duration = parseFloat($("#sea_local_duration").val()).toFixed(2);
    var sea_national_duration = parseFloat($("#sea_national_duration").val()).toFixed(2);
    var sea_international_duration = parseFloat($("#sea_international_duration").val()).toFixed(2);

    if(!categories) {  
      swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select at-least one Category.'); ?>",type:'warning'},function(){ $('#s2id_category_id', function() { $('#s2id_category_id').siblings('select').select2('open'); }); }); }

    else if(from_country_id == 0 ) {  swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select From Country'); ?>",type:'warning'},function(){ 
      $('#s2id_from_country_id', function() { $('#s2id_from_country_id').siblings('select').select2('open'); });  }); }
    else if(from_state_id == 0 ) {  swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select From State'); ?>",type:'warning'},function(){ 
      $('#s2id_from_state_id', function() { $('#s2id_from_state_id').siblings('select').select2('open'); });  }); }
    else if(from_city_id == 0 ) {  swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select From City'); ?>",type:'warning'},function(){ 
      $('#s2id_from_city_id', function() { $('#s2id_from_city_id').siblings('select').select2('open'); });  }); }

    else if(currency_id == 0 ) { swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('No currency found for this country. Please contact to Gonagoo admin.'); ?>",'warning'); }

    else if(to_country_id == 0 ) {  swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select To Country'); ?>",type:'warning'},function(){ 
      $('#s2id_to_country_id', function() { $('#s2id_to_country_id').siblings('select').select2('open'); });  }); }
    else if(to_state_id == 0 ) {  swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select From State'); ?>",type:'warning'},function(){ 
      $('#s2id_to_state_id', function() { $('#s2id_to_state_id').siblings('select').select2('open'); });  }); }
    else if(to_city_id == 0 ) {  swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select From City'); ?>",type:'warning'},function(){ 
      $('#s2id_to_city_id', function() { $('#s2id_to_city_id').siblings('select').select2('open'); });  }); }

    else if(min_dimension == 0 ) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select Minimum dimension'); ?>",type:'warning'},function(){ $('#s2id_min_dimension', function() { $('#s2id_min_dimension').siblings('select').select2('open'); }); }); }
    else if(max_dimension == 0 ) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select Maximum Dimension'); ?>",type:'warning'},function(){ $('#s2id_max_dimension', function() { $('#s2id_max_dimension').siblings('select').select2('open'); }); }); } 
    else if(parseInt(max_dimension) == parseInt(min_dimension) ) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Select Different Minimum and Maximum Dimension.'); ?>",type:'warning'},function(){ $('#s2id_max_dimension', function() { $('#s2id_max_dimension').siblings('select').select2('open'); }); }); }
    else if(parseFloat(max_dimension_volume) <= parseFloat(min_dimension_volume) ) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Invalid! Minimum Dimension is greater than Maximum Dimension!'); ?>",type:'warning'},function(){ $('#s2id_max_dimension', function() { $('#s2id_max_dimension').siblings('select').select2('open'); }); }); }
    // Earth
    else if(isNaN(earth_local_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth Local Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_local_rate').focus(); }, 0);  }); }
    else if(isNaN(earth_local_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth Local Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_local_duration').focus(); }, 0); }); }
    else if(parseFloat(earth_local_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth Local Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_local_duration').focus(); }, 0);  }); }    

    else if(isNaN(earth_national_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth National Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_national_rate').focus(); }, 0); }); }
    else if(isNaN(earth_national_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth National Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_national_duration').focus(); }, 0); }); }
    else if(parseFloat(earth_national_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth National Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_national_duration').focus(); }, 0); }); }

    else if(isNaN(earth_international_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth International Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_international_rate').focus(); }, 0); }); }
    else if(isNaN(earth_international_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth International Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_international_duration').focus(); }, 0); }); }
    else if(parseFloat(earth_international_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth International Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_international_duration').focus(); }, 0); }); }
    // Air
    else if(isNaN(air_local_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air Local Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_local_rate').focus(); }, 0); }); }
    else if(isNaN(air_local_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air Local Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_local_duration').focus(); }, 0); }); }
    else if(parseFloat(air_local_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air Local Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_local_duration').focus(); }, 0); }); }    

    else if(isNaN(air_national_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air National Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_national_rate').focus(); }, 0); }); }
    else if(isNaN(air_national_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air National Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_national_duration').focus(); }, 0); }); }
    else if(parseFloat(air_national_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air National Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_national_duration').focus(); }, 0); }); }

    else if(isNaN(air_international_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air International Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_international_rate').focus(); }, 0); }); }
    else if(isNaN(air_international_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air International Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_international_duration').focus(); }, 0); }); }
    else if(parseFloat(air_international_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air International Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_international_duration').focus(); }, 0); }); }
    // Sea
    else if(isNaN(sea_local_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea Local Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_local_rate').focus(); }, 0); }); }
    else if(isNaN(sea_local_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea Local Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_local_duration').focus(); }, 0); }); }
    else if(parseFloat(sea_local_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea Local Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_local_duration').focus(); }, 0); }); }    

    else if(isNaN(sea_national_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea National Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_national_rate').focus(); }, 0); }); }
    else if(isNaN(sea_national_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea National Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_national_duration').focus(); }, 0); }); }
    else if(parseFloat(sea_national_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea National Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_national_duration').focus(); }, 0); }); }

    else if(isNaN(sea_international_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea International Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_international_rate').focus(); }, 0); }); }
    else if(isNaN(sea_international_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea International Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_international_duration').focus(); }, 0); $("#sea_international_duration").focus();  }); }
    else if(parseFloat(sea_international_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea International Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_international_duration').focus(); }, 0); }); }
    else { $("#point_to_point_rates_add")[0].submit();  }
  });

</script>