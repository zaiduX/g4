<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-laundry/dashboard-bus'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-laundry/user-profile'); ?>"><?= $this->lang->line('user_profile'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('change_password'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-exchange fa-2x text-muted"></i> <?= $this->lang->line('change_password'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('upd_profile'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="col-lg-12"><p>&nbsp;</p></div>
  <div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel-laundry/update-change'); ?>" method="post" class="form-horizontal" id="changePassForm">
        <div class="col-lg-12">
          <?php if($this->session->flashdata('error')):  ?>
            <div class="row">
              <div class="form-group"> 
                <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
              </div>
            </div>
          <?php endif; ?>
          <?php if($this->session->flashdata('success')):  ?>
            <div class="row">
              <div class="form-group"> 
                <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
              </div>
            </div>
          <?php endif; ?>
          <div class="panel-body">

            <div class="col-lg-10 col-lg-offset-1">
              <label for="old_password"><?= $this->lang->line('c_pass'); ?></label>
              <input type="password" name="old_password" id="old_password" placeholder="<?= $this->lang->line('enter_current_password'); ?>" required class="form-control" />
            </div>
            <div class="col-lg-10 col-lg-offset-1">
              <label for="new_password"><?= $this->lang->line('n_pass'); ?></label>
              <input type="password" name="new_password" id="new_password" placeholder="<?= $this->lang->line('enter_new_password'); ?>" required class="form-control" />
            </div>
            <div class="col-lg-10 col-lg-offset-1">
              <label for="rewrite_password"><?= $this->lang->line('re_pass'); ?></label>
              <input type="password" name="rewrite_password" id="rewrite_password" placeholder="<?= $this->lang->line('enter_rewrite_password'); ?>" required class="form-control" />
            </div>
          </div>
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-lg-6 text-left">
                  <a href="<?= base_url('user-panel-laundry/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
               </div>
               <div class="col-lg-6 text-right">
                <button id="btn_submit" type="submit" class="btn btn-info"><?= $this->lang->line('change_password'); ?></button>
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>

  $(function(){
    $("#changePassForm").validate({
      ignore: [],
      rules: {
        old_password: { required: true, },
        new_password: { required: true, minlength: 6, },
        rewrite_password: { required: true, minlength: 6, equalTo: "#new_password" },
      }, 
      messages: {
        old_password: { required: <?= json_encode($this->lang->line('enter_current_password')); ?>,  },
        new_password: { 
            required: <?= json_encode($this->lang->line('enter_new_password')); ?>, 
            minlength: <?= json_encode($this->lang->line('6_characters')); ?>,  
        },
        rewrite_password: { 
            required: <?= json_encode($this->lang->line('enter_rewrite_password')); ?>, 
            minlength: <?= json_encode($this->lang->line('6_characters')); ?>, 
            equalTo: <?= json_encode($this->lang->line('password_not_matched')); ?>,  
        },
      }
    });
  });

</script>
            
