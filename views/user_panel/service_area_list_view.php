    
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('service_area'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">  <i class="fa fa-map-marker fa-2x text-muted"></i>
                    <?= $this->lang->line('service_area'); ?> &nbsp;&nbsp;&nbsp;
                    <a href="<?= $this->config->item('base_url') . 'user-panel/service-area-add'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?> </a>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('available_service_areas'); ?></small> 
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?= $this->lang->line('type'); ?></th>
                                <th><?= $this->lang->line('country'); ?></th>
                                <th><?= $this->lang->line('state'); ?></th>
                                <th><?= $this->lang->line('city'); ?></th>
                                <th><?= $this->lang->line('zip'); ?></th>
                                <th><?= $this->lang->line('area'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($service_area as $area) { ?>
                                <tr>
                                    <td><?= $area['sa_id'] ?></td>
                                    <td><?php if($area['type'] == '1') { echo 'Local'; } elseif ($area['type'] == '2') { echo 'National'; } else { echo 'International'; } ?></td>
                                    <td><?= $this->user->get_country_name_by_id($area['country_id']) ?></td>
                                    <td><?= $this->user->get_state_name_by_id($area['state_id']) ?></td>
                                    <td><?= $this->user->get_city_name_by_id($area['city_id']) ?></td>
                                    <td><?php if($area['zip'] == 'NULL') { echo ''; } else { echo $area['zip']; } ?></td>
                                    <td><?php if($area['area_name'] == 'NULL') { echo ''; } else { echo $area['area_name']; } ?></td>
                                    <td class="text-center">
                                        <button style="display: inline-block;" class="btn btn-danger btn-sm areadeleteatealert" id="<?= $area['sa_id'] ?>"><i class="fa fa-times"></i> <span class="bold"><?= $this->lang->line('delete'); ?></span></button>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>