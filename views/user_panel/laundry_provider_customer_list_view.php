<link rel="stylesheet" href="<?= base_url('resources/web-panel/vendor/c3/c3.min.css'); ?>">

<style>
    #map_wrapper { height: 300px; }
    #map_canvas { width: 100%; height: 100%; }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry')?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
          <li class="active"><span><?=$this->lang->line('My Dashboard');?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('My Dashboard'); ?> &nbsp;&nbsp;&nbsp;
      <a href="<?=base_url('user-panel-laundry/best-customers')?>" class="btn btn-outline btn-info" ><i class="fa fa-certificate"></i> <?= $this->lang->line('Best Customers'); ?></a></h2>
      <small class="m-t-md"><?=$this->lang->line('List of customers, orders and sales statistics');?></small> 
    </div>
  </div>
</div>
<br />
<div class="content">
  <div class="row">
    <div class="panel-group" id="accordion">
      <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><i class="fa fa-money"></i> <?= $this->lang->line('My financial dasboard'); ?></a></h4>
          </div>
          <div id="collapse3" class="panel-collapse collapse in">
            <div class="panel-body">
              <!-- <?=sizeof($all_order_list)?> -->
              <!-- <?=json_encode($_SESSION)?> -->
              <div class="row col-md-12">
                <div>
                  <div id="stocked"></div>
                </div>
                <script src="<?=base_url('resources/web-panel/vendor/d3/d3.min.js')?>"></script>
                <script src="<?=base_url('resources/web-panel/vendor/c3/c3.min.js')?>"></script>
                <script>
                  $(function () {
                    function Last7Days () {
                      return '0123456789'.split('').map(function(n) {
                        var d = new Date();
                        d.setDate(d.getDate() - n);
                        
                        return (function(day, month, year) {
                          return [day<10 ? '0'+day : day, month<10 ? '0'+month : month, year].join('/');
                        })(d.getDate(), (d.getMonth()+1), d.getFullYear());
                      }).join(',');
                    }

                    var last7days = Last7Days();
                    var last7daysarray = last7days.split(',');
                    //console.log(last7daysarray);

                    var currency_sign = "<?=$currency_sign?>";
                    var currency_title = "<?=$currency_title?>";

                    c3.generate({
                      bindto: '#stocked',
                      data:{
                        x : 'x',
                        columns: [
                          ['x', last7daysarray[9],last7daysarray[8],last7daysarray[7],last7daysarray[6],last7daysarray[5],last7daysarray[4],last7daysarray[3],last7daysarray[2],last7daysarray[1],last7daysarray[0]],
                          [currency_sign, <?=$todays_transaction_9?>,<?=$todays_transaction_8?>,<?=$todays_transaction_7?>,<?=$todays_transaction_6?>,<?=$todays_transaction_5?>,<?=$todays_transaction_4?>,<?=$todays_transaction_3?>,<?=$todays_transaction_2?>,<?=$todays_transaction_1?>,<?=$todays_transaction?>]
                        ],
                        colors:{ currency_sign: '#225595' },
                        type: 'spline'
                      },
                      axis: {
                        x: {
                          type: 'category',
                          tick: {
                            rotate: 0,
                            multiline: false
                          }
                        }
                      }
                    });
                  });
                </script>
              </div>
            </div>
          </div>
      </div>
      <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><i class="fa fa-wrench"></i> <?= $this->lang->line('My activity dashboard'); ?></a></h4>
          </div>
          <div id="collapse4" class="panel-collapse collapse">
              <div class="panel-body">
                  <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#Orders"><?= $this->lang->line('Orders'); ?></a></li>
                      <li><a data-toggle="tab" href="#Sales"><?= $this->lang->line('Sales'); ?></a></li>
                      <li><a data-toggle="tab" href="#CleaningStatus"><?= $this->lang->line('Recent orders'); ?></a></li>
                  </ul>

                  <div class="tab-content">
                      <div id="Orders" class="tab-pane fade in active" style="padding-top: 10px;">
                          <a href="<?=base_url('user-panel-laundry/today-laundry-orders')?>">
                              <div class="col-md-3">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">

                                          <?php if(sizeof($all_order_list) == 0) { $today_per = 0; 
                                          } else { $today_per = round((sizeof($today_order_list)/sizeof($all_order_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Today'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?=date('dS'); echo '<br />'. date('F');?></h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($today_order_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$today_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$today_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($all_order_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$today_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </a>
                          <a href="<?=base_url('user-panel-laundry/current-week-laundry-orders')?>">
                              <div class="col-md-3">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($all_order_list) == 0) { $week_per = 0; 
                                          } else { $week_per = round((sizeof($week_order_list)/sizeof($all_order_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('This week'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?php 
                                          if(date('D') != 'Mon') echo date('d', strtotime('last Monday')); else echo date('d'); 
                                          echo ' - '; 
                                          if(date('D') != 'Sat') echo date('d', strtotime('next Sunday')); else echo date('d');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($week_order_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$week_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$week_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($all_order_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$week_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </a>
                          <a href="<?=base_url('user-panel-laundry/current-month-laundry-orders')?>">
                              <div class="col-md-3">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($all_order_list) == 0) { $month_per = 0; 
                                          } else { $month_per = round((sizeof($month_order_list)/sizeof($all_order_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('This month'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?php 
                                          echo '1'; 
                                          echo ' - '; 
                                          echo date('t');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($month_order_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$month_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$month_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($all_order_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$month_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </a>
                          <a href="<?=base_url('user-panel-laundry/till-date-laundry-orders')?>">
                              <div class="col-md-3">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($all_order_list) == 0) { $till_date_per = 0; 
                                          } else { $till_date_per = round((sizeof($till_date_order_list)/sizeof($all_order_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Till date'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center">
                                          <?php if(sizeof($all_order_list) > 0) { 
                                              echo explode('-', explode(' ', $all_order_list[sizeof($all_order_list)-1]['cre_datetime'])[0])[2]; 
                                              echo ' - '; }
                                          //echo explode('-', explode(' ', $all_order_list[0]['cre_datetime'])[0])[2];
                                          echo date('d');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($till_date_order_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$till_date_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$till_date_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($all_order_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$till_date_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div id="Sales" class="tab-pane fade" style="padding-top: 10px;">
                          <a title="<?= $this->lang->line('View transactions'); ?>" href="<?=base_url('user-panel-laundry/today-laundry-sales')?>">
                              <div class="col-md-3">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php 
                                              $country_id = $_SESSION['admin_country_id'];
                                              $currency_sign = $this->api->get_country_currencies($country_id)[0]['currency_sign'];
                                              $today_booking_price = $all_booking_price = 0;

                                              for ($i=0; $i < sizeof($today_order_list) ; $i++) { 
                                                  if($today_order_list[$i]['country_id'] == $country_id)
                                                      $today_booking_price += $today_order_list[$i]['booking_price'];
                                              }

                                              for ($i=0; $i < sizeof($all_order_list) ; $i++) { 
                                                  if($all_order_list[$i]['country_id'] == $country_id)
                                                      $all_booking_price += $all_order_list[$i]['booking_price'];
                                              }
                                              if($all_booking_price == 0) { $today_sales_per = 0; 
                                              } else { $today_sales_per = round(($today_booking_price/$all_booking_price)*100,0); }
                                          ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Today'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?=date('dS'); echo '<br />'. date('F');?></h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h4 class="m-xs"><?=$currency_sign?> <?=$this->api->convert_big_int($today_booking_price)?></h4>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$today_sales_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$today_sales_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=$currency_sign?> <?=$this->api->convert_big_int($all_booking_price)?></h4>
                                                  </div>
                                                  <div class="col-md-6 text-center">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$today_sales_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </a>
                          <a title="<?= $this->lang->line('View transactions'); ?>" href="<?=base_url('user-panel-laundry/current-week-laundry-sales')?>">
                              <div class="col-md-3">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php 
                                              $country_id = $_SESSION['admin_country_id'];
                                              $week_booking_price = 0;

                                              for ($i=0; $i < sizeof($week_order_list) ; $i++) { 
                                                  if($week_order_list[$i]['country_id'] == $country_id)
                                                      $week_booking_price += $week_order_list[$i]['booking_price'];
                                              }

                                              if($all_booking_price == 0) { $week_sales_per = 0; 
                                              } else { $week_sales_per = round(($week_booking_price/$all_booking_price)*100,0); }
                                          ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('This week'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?php 
                                          if(date('D') != 'Mon') echo date('d', strtotime('last Monday')); else echo date('d'); 
                                          echo ' - '; 
                                          if(date('D') != 'Sat') echo date('d', strtotime('next Sunday')); else echo date('d');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h4 class="m-xs"><?=$currency_sign?> <?=$this->api->convert_big_int($week_booking_price)?></h4>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$week_sales_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$week_sales_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=$currency_sign?> <?=$this->api->convert_big_int($all_booking_price)?></h4>
                                                  </div>
                                                  <div class="col-md-6 text-center">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$week_sales_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </a>
                          <a title="<?= $this->lang->line('View transactions'); ?>" href="<?=base_url('user-panel-laundry/current-month-laundry-sales')?>">
                              <div class="col-md-3">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php 
                                              $country_id = $_SESSION['admin_country_id'];
                                              $month_booking_price = 0;

                                              for ($i=0; $i < sizeof($month_order_list) ; $i++) { 
                                                  if($month_order_list[$i]['country_id'] == $country_id)
                                                      $month_booking_price += $month_order_list[$i]['booking_price'];
                                              }

                                              if($all_booking_price == 0) { $month_sales_per = 0; 
                                              } else { $month_sales_per = round(($month_booking_price/$all_booking_price)*100,0); }
                                          ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('This month'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?php 
                                          echo '1'; 
                                          echo ' - '; 
                                          echo date('t');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h4 class="m-xs"><?=$currency_sign?> <?=$this->api->convert_big_int($month_booking_price)?></h4>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$month_sales_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$month_sales_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=$currency_sign?> <?=$this->api->convert_big_int($all_booking_price)?></h4>
                                                  </div>
                                                  <div class="col-md-6 text-center">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$month_sales_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </a>
                          <a title="<?= $this->lang->line('View transactions'); ?>" href="<?=base_url('user-panel-laundry/till-date-laundry-sales')?>">
                              <div class="col-md-3">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php 
                                              $country_id = $_SESSION['admin_country_id'];
                                              $till_date_booking_price = 0;

                                              for ($i=0; $i < sizeof($till_date_order_list) ; $i++) { 
                                                  if($till_date_order_list[$i]['country_id'] == $country_id)
                                                      $till_date_booking_price += $till_date_order_list[$i]['booking_price'];
                                              }

                                              if($all_booking_price == 0) { $till_date_sales_per = 0; 
                                              } else { $till_date_sales_per = round(($till_date_booking_price/$all_booking_price)*100,0); }
                                          ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Till date'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center">
                                          <?php if(sizeof($all_order_list) > 0) {
                                              echo explode('-', explode(' ', $all_order_list[sizeof($all_order_list)-1]['cre_datetime'])[0])[2]; 
                                              echo ' - '; }
                                          //echo explode('-', explode(' ', $all_order_list[0]['cre_datetime'])[0])[2];
                                          echo date('d');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h4 class="m-xs"><?=$currency_sign?> <?=$this->api->convert_big_int($till_date_booking_price)?></h4>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$till_date_sales_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$till_date_sales_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=$currency_sign?> <?=$this->api->convert_big_int($all_booking_price)?></h4>
                                                  </div>
                                                  <div class="col-md-6 text-center">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$till_date_sales_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div id="CleaningStatus" class="tab-pane fade" style="padding-top: 10px;">
                          
                          <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#AcceptedOrders"><?= $this->lang->line('accepted'); ?></a></li>
                              <li><a data-toggle="tab" href="#InprogressOrders"><?= $this->lang->line('in_progress'); ?></a></li>
                              <li><a data-toggle="tab" href="#CompletedOrders"><?= $this->lang->line('Completed'); ?></a></li>
                              <li><a data-toggle="tab" href="#CancelledOrders"><?= $this->lang->line('canceled'); ?></a></li>
                          </ul>

                          <div class="tab-content">
                              <div id="AcceptedOrders" class="tab-pane fade in active" style="padding-top: 10px;">
                                  <table id="tableAcceptedOrders" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                                      <thead>
                                          <tr>
                                              <th><?= $this->lang->line('Order ID'); ?></th>
                                              <th><?= $this->lang->line('Customer Name'); ?></th>
                                              <th><?= $this->lang->line('contact'); ?></th>
                                              <th><?= $this->lang->line('Country'); ?></th>
                                              <th><?= $this->lang->line('order_price'); ?></th>
                                              <th><?= $this->lang->line('Order Date Time'); ?></th>
                                              <th><?= $this->lang->line('order_status'); ?></th>
                                              <th><?= $this->lang->line('Update Date Time'); ?></th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                              $count = 0;
                                              foreach ($accepted_order_list as $order) { if($count < 5) { $count++; ?>
                                                  <tr>
                                                      <td><?=$order['booking_id']?></td>
                                                      <td><?=$order['cust_name']?></td>
                                                      <td><?=$this->api->get_country_code_by_id($order['country_id']).' '.$order['cust_contact']?></td>
                                                      <td><?=$this->api->get_country_name_by_id($order['country_id'])?></td>
                                                      <td><?=$order['total_price']. ' ' .$order['currency_sign']?></td>
                                                      <td><?=$order['cre_datetime']?></td>
                                                      <td><?=ucwords(str_replace('_', ' ', $order['booking_status']))?></td>
                                                      <td><?=$order['status_update_datetime']?></td>
                                                  </tr>
                                          <?php } } ?>
                                      </tbody>
                                      <?php if($count == 5) { ?>
                                          <tfoot>
                                              <td colspan="8" class="text-right"><a href="<?=base_url('user-panel-laundry/accepted-laundry-bookings')?>"><?= $this->lang->line('More'); ?></a></td>
                                          </tfoot>
                                      <?php } ?>
                                  </table>
                                  <script>
                                      $('#tableAcceptedOrders').dataTable({
                                          "deferRender":  true,
                                          "aaSorting":    [],
                                          "paging":       false,
                                          "ordering":     false,
                                          "info":         false,
                                          "searching":    false,
                                      });
                                  </script>
                              </div>
                              <div id="InprogressOrders" class="tab-pane fade" style="padding-top: 10px;">
                                  <table id="tableInprogressOrders" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                                      <thead>
                                          <tr>
                                              <th><?= $this->lang->line('Order ID'); ?></th>
                                              <th><?= $this->lang->line('Customer Name'); ?></th>
                                              <th><?= $this->lang->line('contact'); ?></th>
                                              <th><?= $this->lang->line('Country'); ?></th>
                                              <th><?= $this->lang->line('order_price'); ?></th>
                                              <th><?= $this->lang->line('Order Date Time'); ?></th>
                                              <th><?= $this->lang->line('order_status'); ?></th>
                                              <th><?= $this->lang->line('Update Date Time'); ?></th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                              $count = 0;
                                              foreach ($in_progress_order_list as $order) { if($count < 5) { $count++; ?>
                                                  <tr>
                                                      <td><?=$order['booking_id']?></td>
                                                      <td><?=$order['cust_name']?></td>
                                                      <td><?=$this->api->get_country_code_by_id($order['country_id']).' '.$order['cust_contact']?></td>
                                                      <td><?=$this->api->get_country_name_by_id($order['country_id'])?></td>
                                                      <td><?=$order['total_price']. ' ' .$order['currency_sign']?></td>
                                                      <td><?=$order['cre_datetime']?></td>
                                                      <td><?=ucwords(str_replace('_', ' ', $order['booking_status']))?></td>
                                                      <td><?=$order['status_update_datetime']?></td>
                                                  </tr>
                                          <?php } } ?>
                                      </tbody>
                                      <?php if($count == 5) { ?>
                                          <tfoot>
                                              <td colspan="8" class="text-right"><a href="<?=base_url('user-panel-laundry/in-progress-laundry-bookings')?>"><?= $this->lang->line('More'); ?></a></td>
                                          </tfoot>
                                      <?php } ?>
                                  </table>
                                  <script>
                                      $('#tableInprogressOrders').dataTable({
                                          "deferRender":  true,
                                          "aaSorting":    [],
                                          "paging":       false,
                                          "ordering":     false,
                                          "info":         false,
                                          "searching":    false,
                                      });
                                  </script>
                              </div>
                              <div id="CompletedOrders" class="tab-pane fade" style="padding-top: 10px;">
                                  <table id="tableCompletedOrders" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                                      <thead>
                                          <tr>
                                              <th><?= $this->lang->line('Order ID'); ?></th>
                                              <th><?= $this->lang->line('Customer Name'); ?></th>
                                              <th><?= $this->lang->line('contact'); ?></th>
                                              <th><?= $this->lang->line('Country'); ?></th>
                                              <th><?= $this->lang->line('order_price'); ?></th>
                                              <th><?= $this->lang->line('Order Date Time'); ?></th>
                                              <th><?= $this->lang->line('order_status'); ?></th>
                                              <th><?= $this->lang->line('Update Date Time'); ?></th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                              $count = 0;
                                              foreach ($completed_order_list as $order) { if($count < 5) { $count++; ?>
                                                  <tr>
                                                      <td><?=$order['booking_id']?></td>
                                                      <td><?=$order['cust_name']?></td>
                                                      <td><?=$this->api->get_country_code_by_id($order['country_id']).' '.$order['cust_contact']?></td>
                                                      <td><?=$this->api->get_country_name_by_id($order['country_id'])?></td>
                                                      <td><?=$order['total_price']. ' ' .$order['currency_sign']?></td>
                                                      <td><?=$order['cre_datetime']?></td>
                                                      <td><?=ucwords(str_replace('_', ' ', $order['booking_status']))?></td>
                                                      <td><?=$order['status_update_datetime']?></td>
                                                  </tr>
                                          <?php } } ?>
                                      </tbody>
                                      <?php if($count == 5) { ?>
                                          <tfoot>
                                              <td colspan="8" class="text-right"><a href="<?=base_url('user-panel-laundry/completed-laundry-bookings')?>"><?= $this->lang->line('More'); ?></a></td>
                                          </tfoot>
                                      <?php } ?>
                                  </table>
                                  <script>
                                      $('#tableCompletedOrders').dataTable({
                                          "deferRender":  true,
                                          "aaSorting":    [],
                                          "paging":       false,
                                          "ordering":     false,
                                          "info":         false,
                                          "searching":    false,
                                      });
                                  </script>
                              </div>
                              <div id="CancelledOrders" class="tab-pane fade" style="padding-top: 10px;">
                                  <table id="tableCancelledOrders" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                                      <thead>
                                          <tr>
                                              <th><?= $this->lang->line('Order ID'); ?></th>
                                              <th><?= $this->lang->line('Customer Name'); ?></th>
                                              <th><?= $this->lang->line('contact'); ?></th>
                                              <th><?= $this->lang->line('Country'); ?></th>
                                              <th><?= $this->lang->line('order_price'); ?></th>
                                              <th><?= $this->lang->line('Order Date Time'); ?></th>
                                              <th><?= $this->lang->line('order_status'); ?></th>
                                              <th><?= $this->lang->line('Update Date Time'); ?></th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                              $count = 0;
                                              foreach ($cancelled_order_list as $order) { if($count < 5) { $count++; ?>
                                                  <tr>
                                                      <td><?=$order['booking_id']?></td>
                                                      <td><?=$order['cust_name']?></td>
                                                      <td><?=$this->api->get_country_code_by_id($order['country_id']).' '.$order['cust_contact']?></td>
                                                      <td><?=$this->api->get_country_name_by_id($order['country_id'])?></td>
                                                      <td><?=$order['total_price']. ' ' .$order['currency_sign']?></td>
                                                      <td><?=$order['cre_datetime']?></td>
                                                      <td><?=ucwords(str_replace('_', ' ', $order['booking_status']))?></td>
                                                      <td><?=$order['status_update_datetime']?></td>
                                                  </tr>
                                          <?php } } ?>
                                      </tbody>
                                      <?php if($count == 5) { ?>
                                          <tfoot>
                                              <td colspan="8" class="text-right"><a href="<?=base_url('user-panel-laundry/cancelled-laundry-bookings')?>"><?= $this->lang->line('More'); ?></a></td>
                                          </tfoot>
                                      <?php } ?>
                                  </table>
                                  <script>
                                      $('#tableCancelledOrders').dataTable({
                                          "deferRender":  true,
                                          "aaSorting":    [],
                                          "paging":       false,
                                          "ordering":     false,
                                          "info":         false,
                                          "searching":    false,
                                      });
                                  </script>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><i class="fa fa-users"></i> <?= $this->lang->line('My customers dashboard'); ?></a></h4>
          </div>
          <div id="collapse1" class="panel-collapse collapse">
              <div class="panel-body">
                  <table id="tableData" class="table table-striped table-bordered table-hover" style="min-width:100%">
                      <thead>
                          <tr>
                              <th><?= $this->lang->line('ID'); ?></th>
                              <th><?= $this->lang->line('Customer Name'); ?></th>
                              <th><?= $this->lang->line('email_address'); ?></th>
                              <th><?= $this->lang->line('contact'); ?></th>
                              <th><?= $this->lang->line('Country'); ?></th>
                              <th><i class="fa fa-cart-plus" title="<?= $this->lang->line('Total Orders'); ?>"></i></th>
                              <th><i class="fa fa-check" title="<?= $this->lang->line('accepted'); ?>"></i></th>
                              <th><i class="fa fa-recycle" title="<?= $this->lang->line('in_progress'); ?>"></i></th>
                              <th><i class="fa fa-thumbs-up" title="<?= $this->lang->line('Completed'); ?>"></i></th>
                              <th><i class="fa fa-times" title="<?= $this->lang->line('canceled'); ?>"></i></th>
                              <th><?= $this->lang->line('action'); ?></th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                              $total_all = $total_accepted = $total_in_progress = $total_completed = $total_cancelled = 0;
                              foreach ($customers_list as $list) { ?>
                          <tr>
                              <td><?=$list['cust_id']?></td>
                              <td><?=($list['walkin_cust_id'] > 0)?'Walking Customer':$list['cust_name']?></td>
                              <td><?=($list['walkin_cust_id'] > 0)?'':$list['cust_email']?></td>
                              <td><?=($list['walkin_cust_id'] > 0)?'':$this->api->get_country_code_by_id($list['country_id']).' '.$list['cust_contact']?></td>
                              <td><?=($list['walkin_cust_id'] > 0)?'':$this->api->get_country_name_by_id($list['country_id'])?></td>
                              <td class="text-center"><?php echo $all = sizeof($this->api->laundry_booking_and_customer_provider_list($list['cust_id'],'all','customer','provider',$cust_id,0,0,0))?></td>
                              <td class="text-center"><?php echo $accepted = sizeof($this->api->laundry_booking_and_customer_provider_list($list['cust_id'],'accepted','customer','provider',$cust_id,0,0,0))?></td>
                              <td class="text-center"><?php echo $in_progress = sizeof($this->api->laundry_booking_and_customer_provider_list($list['cust_id'],'in_progress','customer','provider',$cust_id,0,0,0))?></td>
                              <td class="text-center"><?php echo $completed = sizeof($this->api->laundry_booking_and_customer_provider_list($list['cust_id'],'completed','customer','provider',$cust_id,0,0,0))?></td>
                              <td class="text-center"><?php echo $cancelled = sizeof($this->api->laundry_booking_and_customer_provider_list($list['cust_id'],'cancelled','customer','provider',$cust_id,0,0,0))?></td>
                              <td>
                                  <form action="<?=base_url('user-panel-laundry/laundry-customer-consumption-history')?>" method="post">
                                      <input type="hidden" name="c_id" id="c_id" value="<?=$list['cust_id']?>" />
                                      <button class="btn btn-sm btn-info" type="submit" title="<?= $this->lang->line('View Consumption History'); ?>"><i class="fa fa-money"></i> <?= $this->lang->line('Consumption History'); ?></button>
                                  </form>
                              </td>
                          </tr>
                          <?php $total_all += $all; $total_accepted += $accepted; $total_in_progress += $in_progress; $total_completed += $completed; $total_cancelled += $cancelled; } ?>
                      </tbody>
                      <tfoot>
                          <tr>
                              <th class="text-center">&nbsp;</th>
                              <th class="text-center">&nbsp;</th>
                              <th class="text-center">&nbsp;</th>
                              <th class="text-center">&nbsp;</th>
                              <th class="text-right"><?= $this->lang->line('total_orders'); ?></th>
                              <th class="text-center"><?=$total_all?></th>
                              <th class="text-center"><?=$total_accepted?></th>
                              <th class="text-center"><?=$total_in_progress?></th>
                              <th class="text-center"><?=$total_completed?></th>
                              <th class="text-center"><?=$total_cancelled?></th>
                              <th class="text-center">&nbsp;</th>
                          </tr>
                      </tfoot>
                  </table>
              </div>
          </div>
      </div>
      <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><i class="fa fa-spinner"></i> <?= $this->lang->line('RFM (Regency, Frequency, Monetary)'); ?></a></h4>
          </div>
          <div id="collapse2" class="panel-collapse collapse">
              <div class="panel-body" style="padding-bottom: 0px;">
                  <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#RecentlyConnected"><?= $this->lang->line('Recently connected'); ?></a></li>
                      <li><a data-toggle="tab" href="#FrequentlyConnected"><?= $this->lang->line('Frequently connected'); ?></a></li>
                      <li><a data-toggle="tab" href="#HighMoneySpent"><?= $this->lang->line('High money spent'); ?></a></li>
                      <li><a data-toggle="tab" href="#LowMoneySpent"><?= $this->lang->line('Low money spent'); ?></a></li>
                      <li><a data-toggle="tab" href="#HighOrders"><?= $this->lang->line('High orders'); ?></a></li>
                      <li><a data-toggle="tab" href="#LowOrders"><?= $this->lang->line('Low orders'); ?></a></li>
                  </ul>

                  <div class="tab-content">
                      <div id="RecentlyConnected" class="tab-pane fade in active" style="padding-top: 10px;">
                          <table id="tableDataRecentUsers" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                              <thead>
                                  <tr>
                                      <th><?= $this->lang->line('ID'); ?></th>
                                      <th><?= $this->lang->line('Customer Name'); ?></th>
                                      <th><?= $this->lang->line('email_address'); ?></th>
                                      <th><?= $this->lang->line('contact'); ?></th>
                                      <th><?= $this->lang->line('Country'); ?></th>
                                      <th><?= $this->lang->line('Last connected on'); ?></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php 
                                  if($recentlyConnectedUsers) {
                                      foreach ($recentlyConnectedUsers as $recently) { ?>
                                      <tr>
                                          <td><?=$recently['cust_id']?></td>
                                          <td><?=$recently['firstname'].' '.$recently['lastname']?></td>
                                          <td><?=$recently['email1']?></td>
                                          <td><?=$this->api->get_country_code_by_id($recently['country_id']).' '.$recently['mobile1']?></td>
                                          <td><?=$this->api->get_country_name_by_id($recently['country_id'])?></td>
                                          <td><?=$recently['last_login_datetime']?></td>
                                      </tr>
                                      <?php } } else { ?>
                                      <tr>
                                          <td colspan="6" class="text-center"><?= $this->lang->line('no_record_found'); ?></td>
                                      </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                          <script>
                              $('#tableDataRecentUsers').dataTable({
                                  "deferRender": true,
                                  "aaSorting": []
                              });
                          </script>
                      </div>
                      <div id="FrequentlyConnected" class="tab-pane fade" style="padding-top: 10px;">
                          <table id="tableDataFrequentUsers" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                              <thead>
                                  <tr>
                                      <th><?= $this->lang->line('ID'); ?></th>
                                      <th><?= $this->lang->line('Customer Name'); ?></th>
                                      <th><?= $this->lang->line('email_address'); ?></th>
                                      <th><?= $this->lang->line('contact'); ?></th>
                                      <th><?= $this->lang->line('Country'); ?></th>
                                      <th><?= $this->lang->line('Connection count'); ?></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php 
                                  if($FrequentlyConnectedUsers) {
                                      foreach ($FrequentlyConnectedUsers as $frequently) { ?>
                                          <tr>
                                              <td><?=$frequently['cust_id']?></td>
                                              <td><?=$frequently['firstname'].' '.$frequently['lastname']?></td>
                                              <td><?=$frequently['email1']?></td>
                                              <td><?=$this->api->get_country_code_by_id($frequently['country_id']).' '.$frequently['mobile1']?></td>
                                              <td><?=$this->api->get_country_name_by_id($frequently['country_id'])?></td>
                                              <td><?=$frequently['userCount']?></td>
                                          </tr>
                                      <?php } } else { ?>
                                      <tr>
                                          <td colspan="6" class="text-center"><?= $this->lang->line('no_record_found'); ?></td>
                                      </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                          <script>
                              $('#tableDataFrequentUsers').dataTable({
                                  "deferRender": true,
                                  "aaSorting": []
                              });
                          </script>
                      </div>
                      <div id="HighMoneySpent" class="tab-pane fade" style="padding-top: 10px;">
                          <table id="tableDataHighMoneySpent" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                              <thead>
                                  <tr>
                                      <th><?= $this->lang->line('ID'); ?></th>
                                      <th><?= $this->lang->line('Customer Name'); ?></th>
                                      <th><?= $this->lang->line('email_address'); ?></th>
                                      <th><?= $this->lang->line('contact'); ?></th>
                                      <th><?= $this->lang->line('Country'); ?></th>
                                      <th><?= $this->lang->line('Money spent'); ?></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($customers_list as $highorders) { ?>
                                  <tr>
                                      <td><?=$highorders['cust_id']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'Walking Customer':$highorders['cust_name']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$highorders['cust_email']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$this->api->get_country_code_by_id($highorders['country_id']).' '.$highorders['cust_contact']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$this->api->get_country_name_by_id($highorders['country_id'])?></td>
                                      <td><?=$highorders['amount_spent']?></td>
                                  </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                          <script>
                              $('#tableDataHighMoneySpent').dataTable({
                                  "deferRender": true,
                                  "order": [[ 5, "desc" ]]
                              });
                          </script>
                      </div>
                      <div id="LowMoneySpent" class="tab-pane fade" style="padding-top: 10px;">
                          <table id="tableDataLowMoneySpent" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                              <thead>
                                  <tr>
                                      <th><?= $this->lang->line('ID'); ?></th>
                                      <th><?= $this->lang->line('Customer Name'); ?></th>
                                      <th><?= $this->lang->line('email_address'); ?></th>
                                      <th><?= $this->lang->line('contact'); ?></th>
                                      <th><?= $this->lang->line('Country'); ?></th>
                                      <th><?= $this->lang->line('Money spent'); ?></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($customers_list as $highorders) { ?>
                                  <tr>
                                      <td><?=$highorders['cust_id']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'Walking Customer':$highorders['cust_name']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$highorders['cust_email']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$this->api->get_country_code_by_id($highorders['country_id']).' '.$highorders['cust_contact']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$this->api->get_country_name_by_id($highorders['country_id'])?></td>
                                      <td><?=$highorders['amount_spent']?></td>
                                  </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                          <script>
                              $('#tableDataLowMoneySpent').dataTable({
                                  "deferRender": true,
                                  "order": [[ 5, "asc" ]]
                              });
                          </script>
                      </div>
                      <div id="HighOrders" class="tab-pane fade" style="padding-top: 10px;">
                          <table id="tableDataHighOrders" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                              <thead>
                                  <tr>
                                      <th><?= $this->lang->line('ID'); ?></th>
                                      <th><?= $this->lang->line('Customer Name'); ?></th>
                                      <th><?= $this->lang->line('email_address'); ?></th>
                                      <th><?= $this->lang->line('contact'); ?></th>
                                      <th><?= $this->lang->line('Country'); ?></th>
                                      <th><?= $this->lang->line('No of orders'); ?></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($customers_list as $highorders) { ?>
                                  <tr>
                                      <td><?=$highorders['cust_id']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'Walking Customer':$highorders['cust_name']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$highorders['cust_email']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$this->api->get_country_code_by_id($highorders['country_id']).' '.$highorders['cust_contact']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$this->api->get_country_name_by_id($highorders['country_id'])?></td>
                                      <td><?=$highorders['booking_count']?></td>
                                  </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                          <script>
                              $('#tableDataHighOrders').dataTable({
                                  "deferRender": true,
                                  "order": [[ 5, "desc" ]]
                              });
                          </script>
                      </div>
                      <div id="LowOrders" class="tab-pane fade" style="padding-top: 10px;">
                          <table id="tableDataLowOrders" class="table table-striped table-bordered table-hover" style="min-width:100%;">
                              <thead>
                                  <tr>
                                      <th><?= $this->lang->line('ID'); ?></th>
                                      <th><?= $this->lang->line('Customer Name'); ?></th>
                                      <th><?= $this->lang->line('email_address'); ?></th>
                                      <th><?= $this->lang->line('contact'); ?></th>
                                      <th><?= $this->lang->line('Country'); ?></th>
                                      <th><?= $this->lang->line('No of orders'); ?></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($customers_list as $highorders) { ?>
                                  <tr>
                                      <td><?=$highorders['cust_id']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'Walking Customer':$highorders['cust_name']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$highorders['cust_email']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$this->api->get_country_code_by_id($highorders['country_id']).' '.$highorders['cust_contact']?></td>
                                      <td><?=($highorders['walkin_cust_id'] > 0)?'':$this->api->get_country_name_by_id($highorders['country_id'])?></td>
                                      <td><?=$highorders['booking_count']?></td>
                                  </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                          <script>
                              $('#tableDataLowOrders').dataTable({
                                  "deferRender": true,
                                  "order": [[ 5, "asc" ]]
                              });
                          </script>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><i class="fa fa-exclamation-triangle"></i> <?= $this->lang->line('Claim/Anomaly dashboard'); ?></a></h4>
          </div>
          <div id="collapse5" class="panel-collapse collapse">
              <div class="panel-body">
                  <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#Claim"><?= $this->lang->line('Total Claims'); ?></a></li>
                      <li><a data-toggle="tab" href="#ClaimStatus"><?= $this->lang->line('Status-wise Claims'); ?></a></li>
                  </ul>

                  <div class="tab-content">
                      <div id="Claim" class="tab-pane fade in active" style="padding-top: 10px;">
                          <div class="col-md-3">
                              <a href="<?=base_url('user-panel-laundry/provider-laundry-claims')?>">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php 
                                          if(sizeof($till_date_claim_list) == 0) { $today_per = 0; 
                                          } else { $today_per = round((sizeof($today_claim_list)/sizeof($till_date_claim_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Today'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?=date('dS'); echo '<br />'. date('F');?></h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($today_claim_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$today_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$today_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($till_date_claim_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$today_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                          <div class="col-md-3">
                              <a href="<?=base_url('user-panel-laundry/provider-laundry-claims')?>">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php 
                                          if(sizeof($till_date_claim_list) == 0) { $week_per = 0; 
                                          } else { $week_per = round((sizeof($week_claim_list)/sizeof($till_date_claim_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('This week'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?php 
                                          if(date('D') != 'Mon') echo date('d', strtotime('last Monday')); else echo date('d'); 
                                          echo ' - '; 
                                          if(date('D') != 'Sat') echo date('d', strtotime('next Sunday')); else echo date('d');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($week_claim_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$week_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$week_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($till_date_claim_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$week_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                          <div class="col-md-3">
                              <a href="<?=base_url('user-panel-laundry/provider-laundry-claims')?>">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($till_date_claim_list) == 0) { $month_per = 0; 
                                          } else { $month_per = round((sizeof($month_claim_list)/sizeof($till_date_claim_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('This month'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?php 
                                          echo '1'; 
                                          echo ' - '; 
                                          echo date('t');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($month_claim_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$month_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$month_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($till_date_claim_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$month_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                          <div class="col-md-3">
                              <a href="<?=base_url('user-panel-laundry/provider-laundry-claims')?>">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($till_date_claim_list) == 0) { $till_date_per = 0; 
                                          } else { $till_date_per = round((sizeof($till_date_claim_list)/sizeof($till_date_claim_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Till date'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><?php 
                                          if(sizeof($till_date_claim_list) > 0) { 
                                              echo explode('-', explode(' ', $till_date_claim_list[sizeof($till_date_claim_list)-1]['cre_datetime'])[0])[2]; 
                                              echo ' - '; 
                                          }
                                          //echo explode('-', explode(' ', $till_date_claim_list[0]['cre_datetime'])[0])[2];
                                          echo date('d');
                                          echo '<br />'. date('F');?>
                                          </h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($till_date_claim_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$till_date_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$till_date_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($till_date_claim_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$till_date_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                      </div>
                      <div id="ClaimStatus" class="tab-pane fade" style="padding-top: 10px;">
                          <div class="col-md-3">
                              <a href="<?=base_url('user-panel-laundry/provider-laundry-claims')?>">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($till_date_claim_list) == 0) { $today_per = 0; 
                                          } else { $today_per = round((sizeof($open_claim_list)/sizeof($till_date_claim_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Open Claims'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><i class="fa fa-envelope-open-o fa-2x"></i></h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($open_claim_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$today_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$today_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($till_date_claim_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$today_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                          <div class="col-md-3">
                              <a href="<?=base_url('user-panel-laundry/provider-laundry-claims')?>">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($till_date_claim_list) == 0) { $week_per = 0; 
                                          } else { $week_per = round((sizeof($in_progress_claim_list)/sizeof($till_date_claim_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('In-progress Claims'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><i class="fa fa-spinner fa-2x"></i></h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($in_progress_claim_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$week_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$week_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($till_date_claim_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$week_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                          <div class="col-md-3">
                              <a href="<?=base_url('user-panel-laundry/provider-laundry-claims')?>">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($till_date_claim_list) == 0) { $month_per = 0; 
                                          } else { $month_per = round((sizeof($standby_claim_list)/sizeof($till_date_claim_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Standby Claims'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><i class="fa fa-hand-paper-o fa-2x"></i></h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($standby_claim_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$month_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$month_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($till_date_claim_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$month_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                          <div class="col-md-3">
                              <a href="<?=base_url('user-panel-laundry/provider-laundry-claims-closed')?>">
                                  <div class="hpanel stats contact-panel" style="margin-bottom: 0px;">
                                      <div class="panel-body" style="border: solid 1px #225595;">
                                          <?php if(sizeof($till_date_claim_list) == 0) { $till_date_per = 0; 
                                          } else { $till_date_per = round((sizeof($closed_claim_list)/sizeof($till_date_claim_list))*100,0); } ?>
                                          <div class="stats-title pull-left"><h4 style="margin-top: 0px"><?= $this->lang->line('Closed Claims'); ?></h4></div>
                                          <div class="stats-icon pull-right"><h4 class="text-center"><i class="fa fa-check-square-o fa-2x"></i></h4></div>
                                          <div class="m-t-xl" style="margin-top:30px !important">
                                              <h3 class="m-xs"><?=sizeof($closed_claim_list)?></h3>
                                              <div class="progress m-t-xs full progress-small progress-striped active">
                                                  <div style="width:<?=$till_date_per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$till_date_per?>%"></div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('All'); ?></small>
                                                      <h4><?=sizeof($till_date_claim_list)?></h4>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                                                      <h4><?=$till_date_per?>%</h4>
                                                  </div>
                                              </div> 
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>