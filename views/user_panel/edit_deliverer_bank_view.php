<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel/deliverer-profile'); ?>"><span><?= $this->lang->line('deliverer_profile'); ?></span></a></li>
          <li class="active"><span><?= $this->lang->line('edit_payment_method'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-university fa-2x text-muted"></i> <?= $this->lang->line('payment_method'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('edit_details'); ?></small>    
    </div>
  </div>
</div>



<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel/update-deliverer-bank'); ?>" method="post" class="form-horizontal" name="simpleForm" >
          <div class="panel-body">

            <?php if($this->session->flashdata('error')):  ?>
            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

            <input type="hidden" name="cust_id" value="<?= $deliverer['cust_id'] ?>">
            <div class="row hidden">
              <div class="col-md-12">
                <label class=""><?= $this->lang->line('shipping_mode'); ?></label>
                <select class="js-source-states" style="width: 100%" name="shipping_mode">
                  <option value="1" <?php if($deliverer['shipping_mode'] == "1") { echo 'selected'; } ?> ><?= $this->lang->line('automatic'); ?></option>
                  <option value="0" <?php if($deliverer['shipping_mode'] == "0") { echo 'selected'; } ?> ><?= $this->lang->line('manual'); ?></option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class=""><?= $this->lang->line('iban'); ?></label>
                  <input type="text" class="form-control" id="" placeholder="<?= $this->lang->line('iban'); ?>" name="iban" value="<?= $deliverer['iban']=='NULL'?'':$deliverer['iban'] ?>">
                </div>
                <div class="col-md-6">
                  <label class=""><?= $this->lang->line('bank_name'); ?></label>
                  <input type="text" class="form-control" name="bank_name" id="bank_name" value="<?= $deliverer['bank_name']=='NULL'?'':$deliverer['bank_name'] ?>" placeholder="<?= $this->lang->line('bank_name'); ?>" />
                </div>
                <div class="col-md-4">
                  <label class=""><?= $this->lang->line('bank_address'); ?></label>
                  <input type="text" class="form-control" name="bank_address" id="bank_address" value="<?= $deliverer['bank_address']=='NULL'?'':$deliverer['bank_address'] ?>" placeholder="<?= $this->lang->line('bank_address'); ?>" />
                </div>
                <div class="col-md-4">
                  <label class=""><?= $this->lang->line('bank_short_code_swift_code'); ?></label>
                  <input type="text" class="form-control" name="bank_short_code" id="bank_short_code" value="<?= $deliverer['bank_short_code']=='NULL'?'':$deliverer['bank_short_code'] ?>" placeholder="<?= $this->lang->line('bank_short_code_swift_code'); ?>" />
                </div>
                <div class="col-md-4">
                  <label class=""><?= $this->lang->line('account_owner_name'); ?></label>
                  <input type="text" class="form-control" name="bank_title" id="bank_title" value="<?= $deliverer['bank_title']=='NULL'?'':$deliverer['bank_title'] ?>" placeholder="<?= $this->lang->line('account_owner_name'); ?>" />
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class=""><?= $this->lang->line('1_Mobile_Money_Account'); ?></label>
                  <input type="text" class="form-control" id="" placeholder="<?= $this->lang->line('1_Mobile_Money_Account'); ?>" name="mobile_money_acc1" value="<?= $deliverer['mobile_money_acc1']=='NULL'?'':$deliverer['mobile_money_acc1'] ?>">
                </div>
                <div class="col-md-6">
                  <label class=""><?= $this->lang->line('2_Mobile_Money_Account'); ?></label>
                  <input type="text" class="form-control" id="" placeholder="<?= $this->lang->line('2_Mobile_Money_Account'); ?>" name="mobile_money_acc2" value="<?= $deliverer['mobile_money_acc2']=='NULL'?'':$deliverer['mobile_money_acc2'] ?>"> <br />
                </div>
              </div>
            </div>
            <div class="panel-footer"> 
              <div class="row">
                <div class="col-lg-12 text-left">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('save_details'); ?></button>             
                  <a href="<?= base_url('user-panel/deliverer-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>
                </div>         
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>