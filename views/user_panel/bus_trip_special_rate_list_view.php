	<div class="normalheader small-header">
		<div class="hpanel">
		    <div class="panel-body">
		        <a class="small-header-action" href="">
		          <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
		        </a>
		        <div id="hbreadcrumb" class="pull-right">
	            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('Trip Special Rate'); ?></span></li>
	            </ol>
		        </div>
		        <h2 class="font-light m-b-xs"> <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Trip Special Rate'); ?>
		        </h2>
		        <small class="m-t-md"><?= $this->lang->line('Manage Trip Special Rates'); ?></small> 
		    </div>
		</div>
	</div>

	<div class="content">
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
						<table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
				            <thead>
				                <tr>
				                	<th><?= $this->lang->line('Sr. No'); ?></th>
				                	<th><?= $this->lang->line('Source'); ?></th>
				                	<th><?= $this->lang->line('Destination'); ?></th>
				                	<th><?= $this->lang->line('Vehicle Type'); ?></th>
				                	<th><?= $this->lang->line('From'); ?></th>
				                	<th><?= $this->lang->line('To'); ?></th>
				                	<th><?= $this->lang->line('action'); ?></th>
				                </tr>
				            </thead>
				            <tbody class="panel-body">
					            <?php foreach ($trip_master as $trip) { static $i = 0; $i++; ?>
					           		<tr>
					           			<td><?=$i?></td>
					           			<td><?=$trip['trip_source']?></td>
					           			<td><?=$trip['trip_destination']?></td>
					           			<td><?php $vehicle_types = $this->api->get_vehicle_type_by_id($trip['vehical_type_id']); echo $vehicle_types['vehicle_type']?></td>
					           			<td><?=($trip['special_rate_start_date']=='NULL')?'N/A':$trip['special_rate_start_date']?></td>
					           			<td><?=($trip['special_rate_end_date']=='NULL')?'N/A':$trip['special_rate_end_date']?></td>
					           			<td style="display: flex;">
					           				<form action="<?=base_url('user-panel-bus/bus-trip-special-rate-edit')?>" method="post" style="display: inline-block;">
		                                        <input type="hidden" name="trip_id" value="<?= $trip['trip_id'] ?>">
		                                        <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-wrench"></i> <span class="bold"><?= $this->lang->line('Setup'); ?></span></button>
		                                    </form>
		                                </td>
					           		</tr>
								<?php } ?>
				           	</tbody>
				        </table>
				    </div>
				</div>
			</div>
		</div>
	</div>

	<script>
        $('.tripDeleteAlert').click(function () {
            var id = this.id;
            swal(
            {
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-bus/bus-trip-master-delete')?>", { trip_id: id }, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('Deleted!'); ?>", "<?= $this->lang->line('Trip and locations has been deleted!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting trip!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Trip and locations is safe!'); ?>", "error"); }
            });
        });
    </script>