    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>
          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('invoices'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('invoices'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel-laundry/laundry-wallet'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-money"></i> <?= $this->lang->line('my_wallet'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('view_and_download_order_invoices'); ?></small> 
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th><?= $this->lang->line('cust_name'); ?></th>
                                    <th><?= $this->lang->line('Source'); ?></th>
                                    <th><?= $this->lang->line('Destination'); ?></th>
                                    <!-- <th><?= $this->lang->line('distance'); ?></th> -->
                                    <th><?= $this->lang->line('Journey Date'); ?></th>
                                    <!-- <th><?= $this->lang->line('deliver_date_time'); ?></th> -->
                                    <th><?= $this->lang->line('ticket price'); ?></th>
                                    <th><?= $this->lang->line('invoice'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($details as $dtls) { ?>
                                <tr>
                                    <td><?= $dtls['cust_name'] ?></td>
                                    <td><?= $dtls['source_point'] ?></td>
                                    <td><?= $dtls['destination_point'] ?></td>
                                    <!-- <td><?= $dtls['distance_in_km'] ?> KM</td> -->
                                    <td><?= $dtls['journey_date'] ?></td>
                                    <!-- <td><?= $dtls['delivered_datetime'] ?></td> -->
                                    <td><?= $dtls['ticket_price'] . ' ' . $dtls['currency_sign'] ?></td>
                                    <td class="text-center"><a href="<?= base_url('resources/'.$dtls['invoice_url']) ?>" target="_blank"><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>