<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href=""><div class="clip-header"><i class="fa fa-arrow-up"></i></div></a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('notifications'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-bell-o fa-2x text-muted"></i> <?= $this->lang->line('notifications'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('list_of_notifications_from_gonagoo_administration'); ?></small>    
    </div>
  </div>
</div>
        
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <h3>&nbsp;</h3>
      <div class="hpanel">
        <div class="panel-body">
          <table id="tbl_notifications" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th class="text-center" ><?= $this->lang->line('title'); ?></th>
                <th class="text-center" ><?= $this->lang->line('date'); ?></th>
                <th class="text-center"><?= $this->lang->line('attachment'); ?></th>
                <th class="text-center"><?= $this->lang->line('action'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($notifications as $nt): ?>
                <tr>
                  <td class="text-center" >
                    <?php if(str_word_count($nt['notify_title']) > 10 ): ?>
                      <?= $this->api->get_words($nt['notify_title'],10 ).' ...'; ?>
                    <?php else: ?>
                      <?= $nt['notify_title']; ?>
                    <?php endif; ?>                    
                  </td>
                  <td class="text-center" ><?= date('d, F Y',strtotime($nt['cre_datetime'])); ?></td>
                  <td class="text-center"> 
                    <?php if(trim($nt['attachement_url']) != "NULL"): ?>
                      <label class="label label-info icon-left"><a href="<?= base_url(). $nt['attachement_url']?>" target="_blank" class="text-white"> <i class="fa fa-download"></i><?= $this->lang->line('download'); ?> </a></label>
                    <?php else: ?>
                      <label class="label label-danger icon-left"> <i class="fa fa-times"></i><?= $this->lang->line('not_found'); ?></label>
                    <?php endif; ?>
                  </td>                                
                  <td class="text-center">
                    <form action="<?= base_url('user-panel/notification-detail'); ?>" method="post">
                      <input type="hidden" name="id" value="<?= $nt['id']; ?>">
                      <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> <span class="bold"><?= $this->lang->line('details'); ?></span></button>
                      
                    </form>
                  </td>
                </tr>
              <?php endforeach; ?>              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>  $("#tbl_notifications").dataTable({"aaSorting": [[ 1, "desc" ]]});  </script>




