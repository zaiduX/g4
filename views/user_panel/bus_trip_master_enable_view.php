    <style type="text/css">
        #available[]-error {
            margin-top: 20px;
        }
    </style>
    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-master-list'; ?>"><span><?= $this->lang->line('Trip Master List'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Enable Trip'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Enable Trip'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Enable trip for booking'); ?></small>    
        </div>
      </div>
    </div>
    <?php $avail = explode(',', $trip_details[0]['trip_availability'] );  ?>
    <div class="content">
        <!-- Add New Points -->
        <div class="row">
            <div class="col-lg-12">
              <div class="hpanel hblue">
                <div class="panel-body">
                    <?php if($this->session->flashdata('error')):  ?>
                        <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('success')):  ?>
                        <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                    <?php endif; ?>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <h3><?= $this->lang->line('Trip Details'); ?></h3>
                        </div>                            
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label class=""><?= $this->lang->line('Source'); ?></label>
                                <h5><?=$trip_details[0]['trip_source']?></h5>
                            </div>
                            <div class="col-md-6">
                                <label class=""><?= $this->lang->line('Destination'); ?></label>
                                <h5><?=$trip_details[0]['trip_destination']?></h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label class=""><?= $this->lang->line('Bus Details'); ?></label>
                                <h5><?php foreach ($buses as $bus) { if($trip_details[0]['bus_id']==$bus['bus_id']) { echo $bus['bus_modal'].' ['.$bus['bus_make'].'] ['.$bus['bus_seat_type'].', '.$bus['bus_ac'].']'; } } ?></h5>
                            </div>
                            <div class="col-md-3">
                                <label class=""><?= $this->lang->line('Start Date'); ?></label>
                                <h5><?=$trip_details[0]['trip_start_date']?></h5>
                            </div>
                            <div class="col-md-3">
                                <label class=""><?= $this->lang->line('End Date'); ?></label><br />
                                <h5><?=$trip_details[0]['trip_end_date']?></h5>
                            </div>
                        </div>
                    </div>
                    <hr />

                    <div class="row form-group">
                        <div class="col-md-12">
                            <h3><?= $this->lang->line('Enable Trip For Booking'); ?></h3>
                        </div>                            
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <form action="<?= base_url('user-panel-bus/bus-trip-master-enable-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="enableTrip">
                                <input type="hidden" name="trip_id" value="<?=$trip_details[0]['trip_id']?>">
                                <div class="col-md-4">
                                    <label class=""><?= $this->lang->line('Trip Enable From'); ?></label>
                                    <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                                        <input type="text" class="form-control" id="trip_start_date" name="trip_start_date" />
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-4">
                                    <label class=""><?= $this->lang->line('Trip Enable To'); ?></label>
                                    <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                                        <input type="text" class="form-control" id="trip_end_date" name="trip_end_date" />
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    <button type="submit" id="btnEnableTrip" style="min-height: 40px;" class="btn btn-success form-control" data-style="zoom-in"><?= $this->lang->line('Enable Trip'); ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        $('#btnEnableTrip').click(function (e) {
            e.preventDefault();

            var trip_start_date = $('#trip_start_date').val();
            var trip_end_date = $('#trip_end_date').val();
            if(trip_start_date === '' || trip_start_date == null) {
                swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('Select from date!'); ?>", "error");
            } else if (trip_end_date === '' || trip_end_date == null) {
                swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('Select to date!'); ?>", "error");
            } else {
                swal(
                {
                    title: "<?= $this->lang->line('are_you_sure'); ?>",
                    text: "<?= $this->lang->line('Trip will become open for booking.'); ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->lang->line('Yes, enable it.'); ?>",
                    cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                    closeOnConfirm: false,
                    closeOnCancel: false 
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#enableTrip').submit();
                    } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('No change'); ?>", "error"); }
                });
            }
        });
    </script>

    <script>
        $('#trip_start_date').change(function(){
            var start_date = $('#trip_start_date').val();
            var end_date = $('#trip_end_date').text();
            $('#trip_end_date').datepicker('remove');
            $('#trip_end_date').datepicker({
                format: 'mm/dd/yyyy',
                startDate: start_date,
                endDate: end_date
            });
            $('#trip_end_date').datepicker('update');
        });
    </script>