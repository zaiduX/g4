    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-list'; ?>"><?= $this->lang->line('Vehicles'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('seat_layout'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light-xs">  <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('seat_layout'); ?> </h2>
          <small class="m-t-md"><?= $this->lang->line('Apply seat layout to bus'); ?></small>    
        </div>
      </div>
    </div>
     
    <div class="content">
        <div class="row">
            <?php if($this->session->flashdata('error')):  ?>
            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
            <div class="col-lg-6">
                <div class="hpanel hred">
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <label class="text-left"><?= $this->lang->line('bus_seat_type'); ?></label>
                            <div class="">
                                <select class="js-source-states" style="width: 100%" name="bus_seat_type" id="bus_seat_type">
                                    <option value=""><?= $this->lang->line('Select seat type'); ?></option>
                                    <option value="SEATER">SEATER</option>
                                    <option value="SLEEPER">SLEEPER</option>
                                </select>
                            </div><br />
                        </div>
                        <div class="col-sm-6">
                            <label class="text-left"><?= $this->lang->line('Total Seats'); ?></label>
                            <div class="">
                                <input type="number" name="total_seat" id="total_seat" class="form-control" />
                            </div><br />
                        </div>
                        <div class="col-sm-6">
                            <label class="text-left"><?= $this->lang->line('Layout Type'); ?></label>
                            <div class="">
                                <select class="js-source-states" style="width: 100%" name="layout_type" id="layout_type">
                                    <option value=""><?= $this->lang->line('Select Layout Type'); ?></option>
                                    <option value="1">1X1</option>
                                    <option value="2">1X2</option>
                                    <option value="3">1X3</option>
                                    <option value="4">2X1</option>
                                    <option value="5">2X2</option>
                                    <option value="6">3X1</option>
                                </select>
                            </div><br />
                        </div>
                        <div class="col-sm-6">
                            <label class="text-left"><?= $this->lang->line('No of last row seats'); ?></label>
                            <div class="">
                                <input type="number" name="last_row_seat" id="last_row_seat" class="form-control" />
                            </div><br />
                        </div>
                        <div class="col-sm-12">
                            <label class="text-left">&nbsp;</label>
                            <div class="">
                                <input type="button" name="view_layout" id="view-layout" class="btn btn-info form-control" value="<?= $this->lang->line('View Layout'); ?>">
                            </div><br />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel hblue">
                    <div class="panel-body layout-details" style="display: none;">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function() {
        $("#view-layout").click(function(){
            $('.layout-details').hide();
            var bus_seat_type = $('#bus_seat_type').val();
            var total_seat = $('#total_seat').val();
            var layout_type = $('#layout_type').val();
            var last_row_seat = $('#last_row_seat').val();
            console.log(bus_seat_type);
            console.log(total_seat);
            console.log(layout_type);
            console.log(last_row_seat);
            $('.layout-details').show();
        }); 
    });
</script>



<script type="text/javascript">
    $("#busEdit")
        .validate({
            ignore: [], 
            rules: {
                bus_make: { required : true },
                bus_modal: { required : true },
                bus_no: { required : true },
                bus_seat_type: { required : true },
                bus_ac: { required : true },
            },
            messages: {
                bus_make: { required : <?= json_encode($this->lang->line('Enter make year!'));?>, },
                bus_modal: { required : <?= json_encode($this->lang->line('Enter modal!'));?>, }, 
                bus_no: { required : <?= json_encode($this->lang->line('Enter Name/No!'));?>, },
                bus_seat_type: { required : <?= json_encode($this->lang->line('Select seat type!'));?>, },
                bus_ac: { required : <?= json_encode($this->lang->line('Select A.C. type!'));?>, },
            },
        });
</script>


