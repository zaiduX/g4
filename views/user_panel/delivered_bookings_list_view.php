<style type="text/css">
  .table > tbody > tr > td {
      border-top: none;
  }
  .dataTables_filter {
      display: none;
  }
</style>
<div class="normalheader small-header">
  <div class="hpanel">
      <div class="panel-body">
          <a class="small-header-action" href="">
              <div class="clip-header">
                  <i class="fa fa-arrow-up"></i>
              </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
              <ol class="hbreadcrumb breadcrumb">
                  <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                  <li class="active"><span><?= $this->lang->line('delivered_bookings'); ?></span></li>
              </ol>
          </div>
          <h2 class="font-light m-b-xs">
              <?= $this->lang->line('my_delivered_bookings'); ?> &nbsp;&nbsp;&nbsp;
          </h2>
      </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="hpanel">
        <div class="panel-body" style="padding: 5px 10px 0px 10px !important; margin-top: -15px;">
          <div class="row" id="basicSearch" style="<?= $filter == 'advance' ? 'display:none' : '' ?>;">
            <div class="col-md-2">
              <h4><i class="fa fa-search"></i> <?= $this->lang->line('search'); ?></h4>
            </div>
            <div class="col-md-10">
              <input type="text" id="ordersearchbox" class="form-control" placeholder="<?= $this->lang->line('search'); ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-md-offset-10 text-right">
              <a class="btn btn-link btn-block advanceBtn" id="advanceBtn"><i class="fa fa-search"></i> <?= $filter == 'advance' ? $this->lang->line('basic_search') : $this->lang->line('advance_search') ?></a>
            </div>
          </div>
          <form action="<?= base_url('user-panel/delivered-bookings') ?>" method="post" id="orderFilter">    
            <div class="row" id="advanceSearch" style="<?= ($filter == 'basic') ? 'display:none' : '' ?>;">
              <div class="col-md-12 form-group" style="margin-bottom: 5px !important;">
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('source_country'); ?></label>
                      <select id="country_id_src" name="country_id_src" class="form-control select2" data-allow-clear="true" data-placeholder="Select Country">
                          <option value="0"><?= $this->lang->line('select_country'); ?></option>
                          <?php foreach ($countries as $country): ?>
                              <option value="<?= $country['country_id'] ?>" <?php if(isset($from_country_id) && $from_country_id == $country['country_id']) { echo 'selected'; } ?>><?= $country['country_name']; ?></option>
                          <?php endforeach ?>
                      </select>
                  </div>
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('source_state'); ?></label>
                      <select id="state_id_src" name="state_id_src" class="form-control select2" data-allow-clear="true" data-placeholder="Select State" disabled> 
                          <option value="<?php if(isset($from_state_id)) { echo $from_state_id; } ?>">
                              <?php if(isset($from_state_id)) { echo $this->user->get_state_name_by_id($from_state_id); } ?>
                          </option>
                      </select>
                  </div>
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('source_city'); ?></label>
                      <select id="city_id_src" name="city_id_src" class="form-control select2" data-allow-clear="true" data-placeholder="Select City" disabled>
                          <option value="<?php if(isset($from_city_id)) { echo $from_city_id; } ?>">
                              <?php if(isset($from_city_id)) { echo $this->user->get_city_name_by_id($from_city_id); } ?>
                          </option>
                      </select>
                  </div>
              
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('destination_country'); ?></label>
                      <select id="country_id_dest" name="country_id_dest" class="form-control select2" data-allow-clear="true" data-placeholder="Select Country">
                          <option value="0"><?= $this->lang->line('select_country'); ?></option>
                          <?php foreach ($countries as $country): ?>
                              <option value="<?= $country['country_id'] ?>"<?php if(isset($to_country_id) && $to_country_id == $country['country_id']) { echo 'selected'; } ?>><?= $country['country_name']; ?></option>
                          <?php endforeach ?>
                      </select>
                  </div>
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('destination_state'); ?></label>
                      <select id="state_id_dest" name="state_id_dest" class="form-control select2" data-allow-clear="true" data-placeholder="Select State" disabled>
                          <option value="<?php if(isset($to_state_id)) { echo $to_state_id; } ?>">
                              <?php if(isset($to_state_id)) { echo $this->user->get_state_name_by_id($to_state_id); } ?>
                          </option>
                      </select>
                  </div>
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('destination_city'); ?></label>
                      <select id="city_id_dest" name="city_id_dest" class="form-control select2" data-allow-clear="true" data-placeholder="Select City" disabled>
                          <option value="<?php if(isset($to_city_id)) { echo $to_city_id; } ?>">
                              <?php if(isset($to_city_id)) { echo $this->user->get_city_name_by_id($to_city_id); } ?>
                          </option>
                      </select>
                  </div>
              </div>
              <div class="col-md-12 form-group" style="margin-bottom: 5px !important;">
                  <div class="col-md-4">
                      <label class="control-label"><?= $this->lang->line('source_address'); ?></label>
                      <input type="text" name="from_address" class="form-control" id="from_address" value="<?php if(isset($from_address)) { echo $from_address; } ?>">
                  </div>
                  <div class="col-md-4">
                      <label class="control-label"><?= $this->lang->line('destination_address'); ?></label>
                      <input type="text" name="to_address" class="form-control" id="to_address" value="<?php if(isset($to_address)) { echo $to_address; } ?>">
                  </div>
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('max_weight'); ?></label>
                      <input type="number" name="max_weight" class="form-control" id="max_weight" value="<?php if(isset($max_weight)) { echo $max_weight; } ?>">
                  </div>
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('weight_unit'); ?></label>
                      <select class="form-control m-b select2" name="c_unit_id"  id="c_unit_id">
                          <option value=""> <?= $this->lang->line('select_unit'); ?> </option>
                          <?php foreach ($unit as $u ) :?>
                            <option value="<?= $u['unit_id']; ?>" <?php if(isset($unit_id) && $unit_id == $u['unit_id']) { echo 'selected'; } ?>> 
                              <?= ucfirst($u['unit_type']).'(' . strtoupper($u['shortname']) . ')'; ?> 
                            </option>
                          <?php endforeach; ?>
                      </select>
                  </div>
              </div>
              <div class="col-md-12 form-group" style="margin-bottom: 5px !important;">
                  <div class="col-md-3">
                      <label class="control-label"><?= $this->lang->line('creation_between'); ?></label>
                      <div class="input-daterange input-group" id="datepicker">
                          <input type="text" class="input-sm form-control" name="creation_start" value="<?php if(isset($creation_start)) { echo $creation_start; } ?>" />
                          <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                          <input type="text" class="input-sm form-control" name="creation_end" value="<?php if(isset($creation_end)) { echo $creation_end; } ?>" />
                      </div>
                  </div>
                  <div class="col-md-3">
                      <label class="control-label"><?= $this->lang->line('pickup_between'); ?></label>
                      <div class="input-daterange input-group" id="datepicker">
                          <input type="text" class="input-sm form-control" name="pickup_start" value="<?php if(isset($pickup_start)) { echo $pickup_start; } ?>" />
                          <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                          <input type="text" class="input-sm form-control" name="pickup_end" value="<?php if(isset($pickup_end)) { echo $pickup_end; } ?>" />
                      </div>
                  </div>
                  <div class="col-md-3">
                      <label class="control-label"><?= $this->lang->line('delivery_between'); ?></label>
                      <div class="input-daterange input-group" id="datepicker">
                          <input type="text" class="input-sm form-control" name="delivery_start" value="<?php if(isset($delivery_start)) { echo $delivery_start; } ?>" />
                          <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                          <input type="text" class="input-sm form-control" name="delivery_end" value="<?php if(isset($delivery_end)) { echo $delivery_end; } ?>" />
                      </div>
                  </div>
                  <div class="col-md-3">
                      <label class="control-label"><?= $this->lang->line('expiry_between'); ?></label>
                      <div class="input-daterange input-group" id="datepicker">
                          <input type="text" class="input-sm form-control" name="expiry_start" value="<?php if(isset($expiry_start)) { echo $expiry_start; } ?>" />
                          <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                          <input type="text" class="input-sm form-control" name="expiry_end" value="<?php if(isset($expiry_end)) { echo $expiry_end; } ?>" />
                      </div>
                  </div>
              </div>
              <div class="col-md-12 form-group" style="margin-bottom: 5px !important;">
                  <div class="col-md-3">
                      <label class="control-label"><?= $this->lang->line('distance_between').' (KM)'; ?></label>
                      <div class="input-range input-group">
                          <input type="number" class="input-sm form-control" name="distance_start" min="0" value="<?php if(isset($distance_start)) { echo $distance_start; } ?>" />
                          <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                          <input type="number" class="input-sm form-control" name="distance_end" min="0" value="<?php if(isset($distance_end)) { echo $distance_end; } ?>" />
                      </div>
                  </div>
                  <div class="col-md-2">
                      <label class="control-label"><?= $this->lang->line('max_price'); ?></label>
                      <input type="text" name="price" class="form-control" min="0" id="price" value="<?php if(isset($price)) { echo $price; } ?>">
                  </div>
                  <div class="col-md-3">
                      <label class="control-label"><?= $this->lang->line('standard_dimension'); ?></label>
                      <select class="js-source-states" style="width: 100%" name="dimension_id">
                          <option value=""><?= $this->lang->line('select_dimension'); ?></option>
                          <?php foreach ($dimensions as $dimension) { ?>
                              <option value="<?= $dimension['dimension_id'] ?>" <?php if(isset($dimension_id) && $dimension_id == $dimension['dimension_id']) { echo 'selected'; } ?>><?= $dimension['dimension_type'] ?></option>
                          <?php } ?>
                      </select>
                  </div>
                  <div class="col-md-4">
                      <label class="control-label"><?= $this->lang->line('order_type'); ?> </label> &nbsp;&nbsp;
                      <div class="">
                          <div class="radio radio-success radio-inline">
                              <input type="radio" id="local" value="Shipping_Only" name="order_type" aria-label="order_type" <?php if(isset($order_type) && trim($order_type) == 'Shipping_Only' ) { echo 'checked'; } ?>>
                              <label for="order_type"> <?= $this->lang->line('shipping_only'); ?> </label>
                          </div>
                          <div class="radio radio-success radio-inline">
                              <input type="radio" id="local" value="Purchase_and_Deliver" name="order_type" aria-label="order_type" <?php if(isset($order_type) && trim($order_type) == 'Purchase_and_Deliver' ) { echo 'checked'; } ?>>
                              <label for="order_type"> <?= $this->lang->line('purchase_and_deliver'); ?> </label>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-12 form-group" style="margin-bottom: 5px !important;">
                <div class="col-lg-3">
                  <label class="text-left"><?= $this->lang->line('transport_type'); ?> </label>
                  <div class="">
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="earth" value="earth" name="transport_type" <?= ( isset($transport_type) && $transport_type =="earth")?"checked":'';?> >
                      <label for="earth"> <?= $this->lang->line('earth'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="air" value="air" name="transport_type" <?= ( isset($transport_type) && $transport_type =="air")?"checked":'';?>>
                      <label for="air"> <?= $this->lang->line('air'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="sea" value="sea" name="transport_type" <?= ( isset($transport_type) && $transport_type =="sea")?"checked":'';?>>
                      <label for="sea"> <?= $this->lang->line('sea'); ?> </label>
                    </div>
                  </div>
                </div>
                <div class="col-lg-2">
                  <label class=""><?= $this->lang->line('transport_vehicle'); ?></label>
                  <div class="">
                    <select id="vehicle" name="vehicle" class="form-control select2" data-allow-clear="true" data-placeholder="Select Transport Vehicle" <?=( isset($transport_type))?"":"disabled";?>>
                        <option value=""> <?= $this->lang->line('select_transport_vehicle'); ?> </option>
                        <?php if( isset($transport_type) && trim($transport_type)=="earth"):?>
                            <?php foreach ($earth_trans as $v): ?>
                                <option value="<?= $v['vehical_type_id']; ?>" <?=($vehicle_id == $v['vehical_type_id'])?"selected":"";?>><?= $v['vehicle_type']; ?></option>
                            <?php endforeach; ?>
                        <?php elseif( isset($transport_type) && trim($transport_type)=="air"): ?>
                            <?php foreach ($air_trans as $v): ?>
                                <option value="<?= $v['vehical_type_id']; ?>" <?=($vehicle_id == $v['vehical_type_id'])?"selected":"";?>><?= $v['vehicle_type']; ?></option>
                            <?php endforeach; ?>
                        <?php elseif( isset($transport_type) && trim($transport_type)=="sea"): ?>
                            <?php foreach ($sea_trans as $v): ?>
                                <option value="<?= $v['vehical_type_id']; ?>" <?=($vehicle_id == $v['vehical_type_id'])?"selected":"";?>><?= $v['vehicle_type']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                                                    
                    </select>
                  </div> 
                </div>
                <div class="col-lg-3">
                	<label class=""><?= $this->lang->line('sel_category'); ?></label>
                	<div class="">
              			<select id="cat_id" name="cat_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('sel_category'); ?>">
                  		<option value=""> <?= $this->lang->line('sel_category'); ?> </option>
                      <option value="6" <?=($cat_id == 6)?"selected":"";?>><?= $this->lang->line('transportation'); ?></option>
                      <option value="7" <?=($cat_id == 7)?"selected":"";?>><?= $this->lang->line('courier'); ?></option>
                      <option value="280" <?=($cat_id == 280)?"selected":"";?>><?= $this->lang->line('home_move'); ?></option> 
                		</select>
                	</div> 
                </div>
              	<div class="col-md-4 text-right">
                  <div style="float: right;">
              			<br />
                    <button class="btn btn-success" type="submit"><i class="fa fa-search"></i> <?= $this->lang->line('apply_filter'); ?></button>
                    <a class="btn btn-success" href="<?= $this->config->item('base_url') . 'user-panel/delivered-bookings'; ?>"><i class="fa fa-ban"></i> <?= $this->lang->line('reset'); ?></a>
                  </div>
              	</div>
	          	</div>
		        </div>
          </form>
        </div>
       	<br />
        <div class="panel-body" style="padding: 10px !important; margin-top: -10px;">
          <div class="row" style="margin-top: -10px;">
            <div class="col-lg-12">
              <table id="orderTableData" class="table">
                <thead><tr class="hidden"><th></th></tr></thead>
                <tbody>
                  <?php foreach ($orders as $order) { ?>
                  <tr>
                    <td style="line-height: 15px;">
                    	<div class="hpanel filter-item" style="line-height: 15px;">
                        <a href="<?= base_url('user-panel/delivered-booking-order-details/'. $order['order_id']) ?>">
                        	<div class="panel-body" style="line-height: 15px; padding: 0px 0px 10px 15px !important;">
                            <div class="col-md-12" style="line-height: 15px;">
                              <div class="col-xl-2 col-lg-2 col-md-1 col-sm-1"> 
                                <h5 class="text-left text-light"><?= $this->lang->line('ref#'); ?><?= $order['order_id'] ?></h5>
                                <h6 class="text-left text-light hidden"><?= $this->lang->line('delivery_code'); ?>: <?= $order['delivery_code'] ?></h6>
                              </div>
                              <div class="col-xl-6 col-lg-6 col-md-4 col-sm-4 text-center"> 
                                <h4>
                                  <?php 
                                  $category_details = $this->api->get_category_details_by_id($order['category_id']); 
                                  $icon_url = $category_details['icon_url'];
                                  if($order['category_id'] == 6) : ?>
                                    <img style="max-height: 28px" src='<?= base_url($icon_url) ?>' /><?= $this->lang->line('transportation'); ?>
                                  <?php else : ?>
                                    <img style="max-height: 28px" src='<?= base_url($icon_url) ?>' /><?= $this->lang->line('lbl_courier'); ?>
                                  <?php endif; ?>
                                </h4>
                                <!-- <h6 class=""><?= ($order['order_description'] !="NULL")?$order['order_description']:""; ?></h6>    --> 
                              </div>
                              <div class="col-xl-4 col-lg-4 col-md-7 col-sm-7"> 
                                <h5 class="text-right text-light">Order Date: <font style="color: #59bdd7;"><?= date('l, d M Y',strtotime($order['cre_datetime'])); ?></font></h5>
                                <h6 class="text-right text-light">Expiry Date: <?= date('l, d M Y',strtotime($order['expiry_date'])); ?></h6>
                              </div>
                              <br/>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 3px; margin-left: -15px; padding-right: 0; padding-left: 30px;">
                              <h5 style="margin-left: -15px;">
                                <div class="col-md-6">
                                  <i class="fa fa-map-marker"></i> <?= $this->lang->line('from'); ?>: <font style="color: #59bdd7;"><?= ($order['from_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?=$order['from_address']?></font>
                                </div>
                                <div class="col-md-3">
                                  <i class="fa fa-phone"></i>&nbsp;<?=$this->lang->line('sender')?>: <?=($order['from_address_contact'] == 'NULL')?$this->lang->line('not_provided'):$order['from_address_contact']?>
                                </div>
                                <div class="col-md-3">
                                  <i class="fa fa-calendar"></i>&nbsp;<?=$this->lang->line('pickup_'); ?>: <font style="color: #59bdd7;"><?= $order['pickup_datetime'] ?></font>
                                </div>
                              </h5>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 5px; margin-left: -15px; padding-right: 0; padding-left: 30px;">
                              <h5 style="margin-left: -15px;">
                                <div class="col-md-6">
                                  <i class="fa fa-map-marker"></i> <?= $this->lang->line('to'); ?>: <font style="color: #59bdd7;"><?= ($order['to_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['to_address'];?></font>
                                </div>
                                <div class="col-md-3">
                                  <i class="fa fa-phone"></i>&nbsp;<?=$this->lang->line('receiver')?>: <?=($order['to_address_contact'] == 'NULL')?$this->lang->line('not_provided'):$order['to_address_contact']?>
                                </div>
                                <div class="col-md-3">
                                  <i class="fa fa-calendar"></i>&nbsp;<?=$this->lang->line('deliver_'); ?>: <font style="color: #59bdd7;"><?= $order['delivery_datetime']?></font>
                                </div>
                              </h5>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 0px; margin-left: -15px; padding-right: 0; padding-left: 30px;">
                              <h5 class="small" style="margin-left: -15px;">
                                <div class="col-md-6"> <?= $this->lang->line('price'); ?>: <font style="color: #59bdd7;"><?= $order['currency_sign'] ?> <?= $order['order_price'] ?></font></div>
                                <div class="col-md-3"> <?= $this->lang->line('advance'); ?>: <font style="color: #59bdd7;"><?= $order['currency_sign'] ?> <?= $order['advance_payment'] ?></font></div>
                                <div class="col-md-3"> <?= $this->lang->line('balance'); ?>: <font style="color: #59bdd7;"><?= $order['currency_sign'] . ' '. ($order['order_price'] - $order['paid_amount']); ?></font></div>
                              </h5>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 0px; margin-left: -15px; padding-right: 0; padding-left: 30px;">
                              <h5 class="small" style="margin-left: -15px;">
                                <div class="col-md-6"> <?= $this->lang->line('weight'); ?>: <?= $order['total_weight'] ?> <?= strtoupper($this->user->get_unit_name($order['unit_id'])); ?></div>
                                <div class="col-md-3"> <?= $this->lang->line('by'); ?>: <?= $this->user->get_vehicle_type_name($order['vehical_type_id']); ?></div>
                                
                                <div class="col-md-3"> <?= $this->lang->line('no_of_packages'); ?>: <?= $order['package_count']; ?></div>
                              </h5>
                            </div>
                          </div>
                        </a>
                      	<div class="panel-footer" style="padding: 5px 15px !important;">
                          <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 form-group">
                              <h5 style="color: #59bdd7">
                                <?php
                                  $status = $this->api->order_status_list($order['order_id']);
                                  //echo strtoupper($status[(sizeof($status)-1)]['status'])
                                  if($status[(sizeof($status)-1)]['status'] == 'open') { echo $this->lang->line('ordered'); } 
                                  else if($status[(sizeof($status)-1)]['status'] == 'accept' && $status[(sizeof($status)-1)]['user_type'] == 'deliverer') { echo $this->lang->line('deliverer_accepted'); }
                                  else if($status[(sizeof($status)-1)]['status'] == 'assign') { echo $this->lang->line('driver_assigned'); }
                                  else if($status[(sizeof($status)-1)]['status'] == 'accept' && $status[(sizeof($status)-1)]['user_type'] == 'driver') { echo $this->lang->line('driver_accepted'); }
                                  else if($status[(sizeof($status)-1)]['status'] == 'in_progress') { echo $this->lang->line('order_is_in_progress'); }
                                  else if($status[(sizeof($status)-1)]['status'] == 'src_relay_in') { echo $this->lang->line('recieved_at_source_relay_point'); }
                                  else if($status[(sizeof($status)-1)]['status'] == 'src_relay_out') { echo $this->lang->line('out_from_source_relay_point'); }
                                  else if($status[(sizeof($status)-1)]['status'] == 'dest_relay_in') { echo $this->lang->line('recieved_at_destination_relay_point'); }
                                  else if($status[(sizeof($status)-1)]['status'] == 'dest_relay_out') { echo $this->lang->line('out_from_destination_relay_point'); }
                                  else if($status[(sizeof($status)-1)]['status'] == 'delivered') { echo $this->lang->line('delivered_successfully'); }
                                  echo ' - '.strtoupper($status[(sizeof($status)-1)]['mod_datetime']);
                                ?>
                              </h5>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 form-group">
                              <h5 style="color: #59bdd7"><?php if($order['deliverer_company_name'] != 'NULL') { echo $this->lang->line('Deliverer: ').$order['deliverer_company_name']; } ?> ( Driver : <?= $order['driver_id'] == 'NULL' || $order['driver_id'] == 0 ? $this->lang->line('not_assigned') : $order['cd_name'] ?> )
                              </h5>
                              <?php if($order['driver_code'] != 'NULL') { ?>
                              	<?= $this->lang->line('driver_code'); ?> : <?= $order['driver_code'] ?>
                              <?php } ?>
                              <?php if($order['pickup_code'] != 'NULL') { ?>
                                <?= $this->lang->line('Pickup Code'); ?> : <?= $order['pickup_code'] ?>
                              <?php } ?>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 form-group"> 
                              <form action="<?= base_url('user-panel/courier-workroom'); ?>" method="post" class=" text-right ">
                                <input type="hidden" name="order_id" value="<?= $order['order_id'] ?>" />
                                <input type="hidden" name="type" value="workroom" />
                                <button type="submit" class="btn btn-sm btn-outline btn-info">
                                  <i class="fa fa-briefcase"></i> <?= $this->lang->line('go_to_workroom'); ?>
                                </button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function() {
      $('#advanceBtn').click(function() {
          if($('#advanceSearch').css('display') == 'none') {
              $('#basicSearch').hide("slow");
              $('#advanceSearch').show("slow");
              $('#advanceBtn').html("<i class='fa fa-search'></i> <?=$this->lang->line('basic_search')?>");

          } else {
              $('#advanceBtn').html("<i class='fa fa-search'></i> <?=$this->lang->line('advance_search')?>");
              $('#advanceSearch').hide("slow");
              $('#basicSearch').show("slow");
          }
          return false;
      }); 



      $('#datepicker').datepicker();
      $("#datepicker").on("changeDate", function(event) {
          $("#my_hidden_input").val(
                  $("#datepicker").datepicker('getFormattedDate')
          )
      });

      $('#datapicker2').datepicker();
      $('.input-group.date').datepicker({ });
      $('.input-daterange').datepicker({ });

      $("input[name=transport_type]").on("change",function(){
      var v = $("input[name=transport_type]:checked").val();
      $('#vehicle').empty();

      $.ajax({
        type: "POST", 
        url: "get-transport", 
        data: { type: v },
        dataType: "json",
        success: function(res){ 
          // console.log(res);
          $('#vehicle').attr('disabled', false);
          $('#vehicle').empty(); 
          $('#vehicle').append('<option value=""><?= $this->lang->line('select_transport_vehicle'); ?></option>');
          $.each( res, function(i, v){
            $('#vehicle').append('<option value="'+v['vehical_type_id']+'">'+v['vehicle_type']+'</option>');
          });
          $('#vehicle').focus();
        },
        beforeSend: function(){
          $('#vehicle').empty();
          $('#vehicle').append('<option value=""><?= json_encode($this->lang->line('loading')); ?>,</option>');
        },
        error: function(){
          $('#vehicle').attr('disabled', true);
          $('#vehicle').empty();
          $('#vehicle').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
        }
      });
    });
  });
</script>

<script>
  $("#country_id_src").on('change', function(event) {  event.preventDefault();
      var country_id_src = $(this).val();
      $('#city_id_src').attr('disabled', true);

      $.ajax({
        url: "get-state-by-country-id",
        type: "POST",
        data: { country_id: country_id_src },
        dataType: "json",
        success: function(res){
          console.log(res);
          $('#state_id_src').attr('disabled', false);
          $('#state_id_src').empty();
          $('#state_id_src').append('<option value="0"><?= json_encode($this->lang->line('select_state'))?></option>');
          $.each( res, function(){$('#state_id_src').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
          $('#state_id_src').focus();
        },
        beforeSend: function(){
          $('#state_id_src').empty();
          $('#state_id_src').append('<option value="0"><?= json_encode($this->lang->line('loading'))?></option>');
        },
        error: function(){
          $('#state_id_src').attr('disabled', true);
          $('#state_id_src').empty();
          $('#state_id_src').append('<option value="0"><?= json_encode($this->lang->line('no_option'))?></option>');
        }
      })
    });

  $("#state_id_src").on('change', function(event) {  event.preventDefault();
      var state_id_src = $(this).val();
      $.ajax({
        type: "POST",
        url: "get-cities-by-state-id",
        data: { state_id: state_id_src },
        dataType: "json",
        success: function(res){
          $('#city_id_src').attr('disabled', false);
          $('#city_id_src').empty();
          $('#city_id_src').append('<option value="0"><?= json_encode($this->lang->line('select_city'))?></option>');
          $.each( res, function(){ $('#city_id_src').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
          $('#city_id_src').focus();
        },
        beforeSend: function(){
          $('#city_id_src').empty();
          $('#city_id_src').append('<option value="0"><?= json_encode($this->lang->line('loading'))?></option>');
        },
        error: function(){
          $('#city_id_src').attr('disabled', true);
          $('#city_id_src').empty();
          $('#city_id_src').append('<option value="0"><?= json_encode($this->lang->line('no_option'))?></option>');
        }
      });
  });
</script>

<script>
  $("#country_id_dest").on('change', function(event) {  event.preventDefault();
  var country_id_dest = $(this).val();
  $('#city_id_dest').attr('disabled', true);

  $.ajax({
    url: "get-state-by-country-id",
    type: "POST",
    data: { country_id: country_id_dest },
    dataType: "json",
    success: function(res){
      console.log(res);
      $('#state_id_dest').attr('disabled', false);
      $('#state_id_dest').empty();
      $('#state_id_dest').append('<option value="0"><?= json_encode($this->lang->line('select_state'))?></option>');
      $.each( res, function(){$('#state_id_dest').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
      $('#state_id_dest').focus();
    },
    beforeSend: function(){
      $('#state_id_dest').empty();
      $('#state_id_dest').append('<option value="0"><?= json_encode($this->lang->line('loading'))?></option>');
    },
    error: function(){
      $('#state_id_dest').attr('disabled', true);
      $('#state_id_dest').empty();
      $('#state_id_dest').append('<option value="0"><?= json_encode($this->lang->line('no_option'))?></option>');
    }
  })
});

$("#state_id_dest").on('change', function(event) {  event.preventDefault();
  var state_id_dest = $(this).val();
  $.ajax({
    type: "POST",
    url: "get-cities-by-state-id",
    data: { state_id: state_id_dest },
    dataType: "json",
    success: function(res){
      $('#city_id_dest').attr('disabled', false);
      $('#city_id_dest').empty();
      $('#city_id_dest').append('<option value="0"><?= json_encode($this->lang->line('select_city'))?></option>');
      $.each( res, function(){ $('#city_id_dest').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
      $('#city_id_dest').focus();
    },
    beforeSend: function(){
      $('#city_id_dest').empty();
      $('#city_id_dest').append('<option value="0"><?= json_encode($this->lang->line('loading'))?></option>');
    },
    error: function(){
      $('#city_id_dest').attr('disabled', true);
      $('#city_id_dest').empty();
      $('#city_id_dest').append('<option value="0"><?= json_encode($this->lang->line('no_option'))?></option>');
    }
  })
});
</script>

<script>
  $("#orderFilter")
      .validate({
          ignore: [], 
          rules: {
              price: { number : true },
          },
          messages: {
              price: { number : <?= json_encode($this->lang->line('number_only')); ?> },
          },
      });
</script>