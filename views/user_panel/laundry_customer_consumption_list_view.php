<style>
    #map_wrapper { height: 300px; }
    #map_canvas { width: 100%; height: 100%; }
</style>

<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
            </a>
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry')?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                    <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry-users')?>"><span><?=$this->lang->line('My Dashboard');?></span></a></li>
                    <li class="active"><span><?=$this->lang->line('Consumption History');?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-certificate fa-2x text-muted"></i> <?= $this->lang->line('Consumption History'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?=base_url('user-panel-laundry/dashboard-laundry-users')?>" class="btn btn-outline btn-info" ><i class="fa fa-users"></i> <?= $this->lang->line('My Dashboard'); ?></a></h2>
            <small class="m-t-md"><?=$this->lang->line('Best customers as per orders and amount paid');?></small> 
        </div>
    </div>
</div>
<br />

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="hpanel hblue" style="margin-bottom: 10px !important">
                <div class="panel-body">
                    <div class="row">
                            <div class="col-md-3">
                                <label><?=$this->lang->line('Filter by currency');?></label>
                            </div>
                            <div class="col-md-6">
                                <form action="<?=base_url('user-panel-laundry/laundry-customer-consumption-history')?>" method="post">
                                    <select class="form-control select2" name="currency_sign" id="currency_sign">
                                        <option value="null"><?=$this->lang->line('Select Currency');?></option>
                                        <?php foreach ($currencies as $currency) { ?>
                                            <option value="<?=$currency['currency_sign']?>" <?=($currency_sign==$currency['currency_sign'])?'selected':''?> ><?=$currency['currency_title'].' ['.$currency['currency_sign'].']'?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-3" style="display: inline-flex;">
                                    <input type="hidden" name="c_id" value="<?=$c_id?>" />
                                    <button type="submit" class="btn btn-info"><?=$this->lang->line('Filter');?></button>
                                </form>&nbsp;
                                <form action="<?=base_url('user-panel-laundry/laundry-customer-consumption-history')?>" method="post">
                                    <input type="hidden" name="c_id" value="<?=$c_id?>" />
                                    <input type="hidden" name="currency_sign" value="null" />
                                    <button type="submit" class="btn btn-warning"><?=$this->lang->line('reset');?></button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>

            <div class="hpanel hblue">
                <div class="panel-body">
                    <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th><?= $this->lang->line('Order ID'); ?></th>
                                <th><?= $this->lang->line('Order Price'); ?></th>
                                <th><?= $this->lang->line('Currency'); ?></th>
                                <th><?= $this->lang->line('Created On'); ?></th>
                                <th><?= $this->lang->line('Booking Status'); ?></th>
                                <th><?= $this->lang->line('Payment Type'); ?></th>
                                <th><?= $this->lang->line('Amount Paid'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $total_all = 0;
                                foreach ($booking_list as $list) { ?>
                            <tr>
                                <td><?=$list['booking_id']?></td>
                                <td><?=$list['total_price']?></td>
                                <td><?=$list['currency_sign']?></td>
                                <td><?=$list['cre_datetime']?></td>
                                <td class="text-center"><?=ucwords(str_replace('_', ' ', $list['booking_status']))?></td>
                                <td class="text-center"><?=strtoupper($list['payment_method'])?></td>
                                <td class="text-center"><?php echo $all = $list['paid_amount']?></td>
                            </tr>
                            <?php $total_all += $all; } ?>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <th class="text-right" colspan="6"><?= $this->lang->line('Total Consumptions'); ?></th>
                                <th class="text-center"><?=$total_all?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>