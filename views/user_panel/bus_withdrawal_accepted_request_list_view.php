    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>
          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?>
              </span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-withdrawals-request'; ?>"><span><?= $this->lang->line('withdrawal_requests'); ?>
              </span></a></li>
              <li class="active"><span><?= $this->lang->line('accepted'); ?>
              </span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs"> <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('accepted_withdrawal_requests'); ?>
          &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-withdrawals-request'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i><?= $this->lang->line('withdrawal_requests'); ?>
             </a></h2>
          <small class="m-t-md"><?= $this->lang->line('accepted_withdrawal_requests_details'); ?>
          </small> 
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th><?= $this->lang->line('date_time'); ?>
                                    </th>
                                    <th><?= $this->lang->line('amount'); ?>
                                    </th>
                                    <th><?= $this->lang->line('account_type'); ?>
                                    </th>
                                    <th class="text-center"><?= $this->lang->line('action'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($history as $histry) { ?>
                                <tr>
                                    <td><?= $histry['req_id'] ?></td>
                                    <td><?= date('d-M-Y h:i:s A',strtotime($histry['req_datetime'])) ?></td>
                                    <td><?= $histry['currency_code'] . ' ' . $histry['amount'] ?></td>
                                    <td><?= ucwords(str_replace('_',' ',$histry['transfer_account_type'])) ?></td>
                                    <td class="text-center">
                                        <form action="bus-withdrawal-request-details" method="post" style="display: inline-block;">
                                            <input type="hidden" name="req_id" value="<?= $histry['req_id'] ?>">
                                            <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-eye"></i> <span class="bold"> <?= $this->lang->line('view_details'); ?>
                                            </span></button>
                                        </form>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>