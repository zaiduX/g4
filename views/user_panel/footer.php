    <!-- Footer-->
    <footer class="footer">
        <span class="pull-right">
            <?=$this->lang->line('Servicesgoo')?>
        </span>
        &copy; <?=$this->lang->line('Copyright')?> <?= date('Y'); ?>
    </footer>
</div>
<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!-- Vendor scripts -->
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/jquery-ui/jquery-ui.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/slimScroll/jquery.slimscroll.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap/dist/js/bootstrap.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/jquery-flot/jquery.flot.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/jquery-flot/jquery.flot.resize.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/jquery-flot/jquery.flot.pie.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/flot.curvedlines/curvedLines.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/jquery.flot.spline/index.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/metisMenu/dist/metisMenu.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/iCheck/icheck.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/peity/jquery.peity.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/sparkline/index.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/datatables/media/js/jquery.dataTables.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/sweetalert/lib/sweet-alert.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/moment/moment.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/select2-3.5.2/select2.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/blueimp-gallery/js/jquery.blueimp-gallery.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/clockpicker/dist/bootstrap-clockpicker.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/ladda/dist/spin.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/ladda/dist/ladda.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/ladda/dist/ladda.jquery.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap-star-rating/js/star-rating.min.js'; ?>"></script>

<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/tagEditor/jquery.caret.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/tagEditor/jquery.tag-editor.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/scripts/printThis.js'; ?>"></script>



<link href="//cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" />
<script src="//cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<!-- App scripts -->
<script src="<?= $this->config->item('resource_url') . 'web-panel/scripts/homer.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/scripts/charts.js'; ?>"></script>


<script>
    $(function(){
        $('#datepicker').datepicker({ autoclose: true, });
        $('.input-group.date').datepicker({ autoclose: true, });
        $('.input-daterange').datepicker({ autoclose: true, });

        $("#demo1").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10
        });
        $("#demo2").TouchSpin({
            verticalbuttons: true
        });
        $("#rating_filter").TouchSpin({
            verticalbuttons: true,
            max: 5,
            min: 0,
        });
        $(".select2").select2();
    });
</script>
<style>
    .lightBoxGallery {
        text-align: center;
    }

    .lightBoxGallery a {
        margin: 5px;
        display: inline-block;
    }
</style>
<script>
    $('#tableData').dataTable({
        "deferRender": true,
        "scrollX": true,
    });
    $('#tableDataBooking').dataTable({
        "deferRender": true,
        "bFilter": false,
        "bLengthChange": false,
        "bInfo": false,
        "pageLength": 5,
    });
</script>
<script>
    window.setTimeout(function() {
        $(".alert").not(".email-verify-alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 4000);
</script>
<script>
    $(function () {
        $('.photodeletealert').click(function () {
            var id = this.id;
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post('business-photos-delete', {id: id}, function(res){
                            swal("Deleted!", "Photo has been deleted.", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("Cancelled", "Photo is safe :)", "error");
                    }
                });
            });

        $('.vehicledeletealert').click(function () {
            var id = this.id;
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post('vehicle-delete', {id: id}, function(res){
                            swal("Deleted!", "Vehicle has been deleted.", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("Cancelled", "Vehicle is safe :)", "error");
                    }
                });
            });

        $('.driverinactivatealert').click(function () {
            var id = this.id;
            swal({
                title: "Are you sure?",
                text: "Your record will be inactivate!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, inactivate it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post('driver-inactive', {id: id}, function(res){
                            swal("Inactivated!", "Record has been inactivated!", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("Cancelled", "No change :)", "error");
                    }
                });
            });

        $('.driveractivatealert').click(function () {
            var id = this.id;
            swal({
                title: "Are you sure?",
                text: "Your record will be activate!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, activate it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post('driver-active', {id: id}, function(res){
                            swal("Inactivated!", "Record has been activated!", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("Cancelled", "No change :)", "error");
                    }
                });
            });

        $('.areadeleteatealert').click(function () {
            var id = this.id;
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post('service-area-delete', {id: id}, function(res){
                            swal("Deleted!", "Service area has been deleted.", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("Cancelled", "Service area is safe :)", "error");
                    }
                });
            });

        $('.docdeleteatealert').click(function () {
            var id = this.id;
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post('deliverer-document-delete', {id: id}, function(res){
                            swal("Deleted!", "Document has been deleted.", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("Cancelled", "Document is safe :)", "error");
                    }
                });
            });
    });
</script>
<script>
    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     
    });
</script>
<script>
    $(function(){
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('a[data-toggle="tab"]').removeClass('btn-primary');
            $('a[data-toggle="tab"]').addClass('btn-default');
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
        })
        $('.next').click(function(){
            var nextId = $(this).parents('.tab-pane').next().attr("id");
            $('[href=#'+nextId+']').tab('show');
        })
        $('.prev').click(function(){
            var prevId = $(this).parents('.tab-pane').prev().attr("id");
            $('[href=#'+prevId+']').tab('show');
        })
        $('.submitWizard').click(function(){
            var approve = $(".approveCheck").is(':checked');
            if(approve) {
                // Got to step 1
                $('[href=#step1]').tab('show');

                // Serialize data to post method
                var datastring = $("#simpleForm").serialize();

                // Show notification
                swal({
                    title: "Thank you!",
                    text: "You approved our example form!",
                    type: "success"
                });
    //            Example code for post form
    //            $.ajax({
    //                type: "POST",
    //                url: "your_link.php",
    //                data: datastring,
    //                success: function(data) {
    //                    // Notification
    //                }
    //            });
            } else {
                // Show notification
                swal({
                    title: "Error!",
                    text: "You have to approve form checkbox.",
                    type: "error"
                });
            }
        })
    });
</script>
<script>
    $('.btnNext').click(function(){
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });
        $('.btnPrev').click(function(){
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
        $('.submitWizard').click(function(){
        $('#myModal5').modal('hide');
        
    });
</script>
<script>
    $(function(){
        $(".js-source-states").select2();
    });
</script>
<script>
    function fun_local() {
        document.getElementById('divareaname').style.display = "inherit";
        document.getElementById('divzip').style.display = "inherit";
        document.getElementById('divcity').style.display = "inherit";
        document.getElementById('divstate').style.display = "inherit";
    }
    function fun_national() {
        document.getElementById('divareaname').style.display = "none";
        document.getElementById('divzip').style.display = "none";
        document.getElementById('divcity').style.display = "inherit";
        document.getElementById('divstate').style.display = "inherit";
    }
    function fun_international() {
        document.getElementById('divareaname').style.display = "none";
        document.getElementById('divzip').style.display = "none";
        document.getElementById('divcity').style.display = "none";
        document.getElementById('divstate').style.display = "none";
    }
</script>
<script>
    $(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
    });
    $(function() {
        // Create the close button
        var closebtn = $('<button/>', {
            type:"button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        // Set the popover default content
        $('.image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
            content: "There's no image",
            placement:'bottom'
        });
        // Clear event
        $('.image-preview-clear').click(function(){
            $('.image-preview').attr("data-content","").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Browse"); 
        }); 
        // Create the preview image
        $(".image-preview-input input:file").change(function (){     
            var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
            });      
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".image-preview-input-title").text("Change");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(file.name);            
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }        
            reader.readAsDataURL(file);
        });  
    });
</script>
<script>
    $(function(){
        // ClockPicker
        $('.clockpicker').clockpicker({autoclose: true});
        // DateTimePicker
        $('#datetimepicker1').datetimepicker();
        // Touch Spin
        $("#demo2").TouchSpin({
            verticalbuttons: true
        });
        $("#width").TouchSpin({
            verticalbuttons: true
        });
        $("#height").TouchSpin({
            verticalbuttons: true
        });
        $("#length").TouchSpin({
            verticalbuttons: true
        });
        $("#weight").TouchSpin({
            verticalbuttons: true
        });
    });
</script>

<script>
    $(function () {

        // Bind normal buttons
        $( '.ladda-button' ).ladda( 'bind', { timeout: 2000 } );

        // Bind progress buttons and simulate loading progress
        Ladda.bind( '.progress-demo .ladda-button',{
            callback: function( instance ){
                var progress = 0;
                var interval = setInterval( function(){
                    progress = Math.min( progress + Math.random() * 0.1, 1 );
                    instance.setProgress( progress );

                    if( progress === 1 ){
                        instance.stop();
                        clearInterval( interval );
                    }
                }, 200 );
            }
        });
        var l = $( '.ladda-button-demo' ).ladda();
        l.click(function(){
            // Start loading
            l.ladda( 'start' );
            // Timeout example
            // Do something in backend and then stop ladda
            setTimeout(function(){
                l.ladda('stop');
            },12000)
        });
    });
</script>
<script>
    $(function(){
        $("#upload_file").on('click', function(e) { e.preventDefault();
            $("#attachment").trigger('click');
        });        

        $('li').last().addClass('active-li').css('outline', 'none').focus();
    });

    function attachement_name(e) {      
        $("#file_name").text(e.target.files[0].name);
    }
</script>
<script type="text/javascript">
    $('#orderTableData').dataTable( {
        "pageLength": 8,
        "bLengthChange": false,
        "ordering": false,
    } );

    $('#orderTableData1').dataTable( {
        "pageLength": 8,
        "bLengthChange": false,
        "ordering": false,
    } );

    $(document).ready(function() {
        var dataTable = $('#orderTableData').dataTable();
        $("#ordersearchbox").keyup(function() {
            dataTable.fnFilter(this.value);
        });    
    });
</script>


<!-- By Shaz -->
<script>
    $.validator.addMethod("notEqualTo", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "value has to be different...");

    $.validator.addMethod("greaterThan", function(value, element, param) {
        return this.optional(element) || value >= $(param).val();
    }, "value has to be greater...");

</script>
<script type="text/javascript">
    $('#addressTableData').dataTable( {
        "pageLength": 8,
        "bLengthChange": false,
        "ordering": false,
    } );

    $(document).ready(function() {
        var dataTable = $('#addressTableData').dataTable();
        $("#searchbox").keyup(function() {
            dataTable.fnFilter(this.value);
        });    
    });
</script>
<script>
    $(function(){
        $("#upload_file").on('click', function(e) { e.preventDefault();
            $("#attachment").trigger('click');
        });        

        $('li').last().addClass('active-li').css('outline', 'none').focus();
    });

    function attachement_name(e) {      
        $("#file_name").text(e.target.files[0].name);
    }
</script>

</body>
</html>