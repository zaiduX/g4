<style type="text/css">
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style> 
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/driver-list'; ?>"><?= $this->lang->line('bus_drivers'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('edit_drivers'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('edit_drivers_details'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('update_driver_details'); ?></small>
            </div>
        </div>
    </div>
    <?php
        $catgory = explode(',', $driver_details[0]['cat_ids'] ); 
        $avail = explode(',', $driver_details[0]['available'] ); 
    ?> 
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <?php if($this->session->flashdata('error')):  ?>
                                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                                    <?php endif; ?>
                                    <?php if($this->session->flashdata('success')):  ?>
                                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                                    <?php endif; ?>  
                                </div>
                            </div>
                        </form>   
                        <div class="form-group">

                            <form method="post" class="form-horizontal" action="<?= $this->config->item('base_url') . 'user-panel-bus/driver-avatar-update' ?>" enctype="multipart/form-data">
                                <input type="hidden" name="cd_id" value="<?= $driver_details[0]['cd_id'] ?>">
                                <div class="col-md-2 col-md-offset-1 text-center">
                                    <img id='img-upload' src="<?php if($driver_details[0]['avatar'] == 'NULL') { echo $this->config->item('resource_url') . 'default-profile.jpg'; } else { echo $this->config->item('base_url') . $driver_details[0]['avatar']; } ?>" style="max-height: 150px; max-width: 150px; width: auto; height: auto;" /><br />
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                <?= $this->lang->line('browse'); ?> <input type="file" id="imgInp" name="profile_image">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly>
                                    </div>
                                    <button class="btn btn-info btn-outline form-control" type="submit"><?= $this->lang->line('upload_profile_picture'); ?></button>
                                </div>
                            </form>

                            <form method="post" class="form-horizontal" action="<?= $this->config->item('base_url') . 'user-panel-bus/update-driver-details' ?>" id="driverEdit">
                                <input type="hidden" name="cd_id" value="<?= $driver_details[0]['cd_id'] ?>">
                                <div class="col-md-9">

                                  <div class="row">
                                    <div class="col-md-5">
                                      <label class="control-label"><?= $this->lang->line('Vehicle Type'); ?></label>
                                      <select class="form-control js-source-states" name="vehical_type_id" id="vehical_type_id">
                                        <option value=""><?= $this->lang->line('Select Vehicle Type'); ?></option>
                                        <?php foreach ($vehicle_types as $type) { ?>
                                          <option value="<?= $type['vehical_type_id'] ?>" <?=($type['vehical_type_id']==$driver_details[0]['vehical_type_id'])?'selected':''?> ><?= $type['vehicle_type'] ?></option>
                                        <?php } ?>
                                      </select>
                                    </div>
                                    <div class="col-md-5">
                                      <label class="control-label"><?= $this->lang->line('select_vehicle'); ?></label>
                                      <select class="form-control js-source-states" name="bus_id" id="bus_id">
                                        <?php foreach ($vehicles as $vehicle) { ?>
                                          <option value="<?= $vehicle['bus_id'] ?>" <?=($vehicle['bus_id']==$driver_details[0]['vehicle_id'])?'selected':''?> ><?= $vehicle['bus_no'] ?></option>
                                        <?php } ?>
                                      </select><br />
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                  </div>
                                    
                                  <div class="row">
                                    <div class="col-md-5">
                                      <label class="control-label"><?= $this->lang->line('first_name'); ?></label>
                                      <input type="text" placeholder="<?= $this->lang->line('first_name'); ?>" class="form-control m-b" name="first_name" id="first_name" value="<?= $driver_details[0]['first_name'] ?>">
                                    </div>
                                    <div class="col-md-5">
                                      <label class="control-label"><?= $this->lang->line('last_name'); ?></label>
                                      <input type="text" placeholder="<?= $this->lang->line('last_name'); ?>" class="form-control m-b" name="last_name" id="last_name" value="<?= $driver_details[0]['last_name'] ?>">
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-5">
                                      <label class="control-label"><?= $this->lang->line('access_id'); ?></label>
                                      <input type="text" placeholder="<?= $this->lang->line('phone_or_email_or_any_code'); ?>" class="form-control m-b" name="email" id="email" value="<?= $driver_details[0]['email'] ?>">
                                    </div>
                                    <div class="col-md-5">
                                      <label class="control-label"><?= $this->lang->line('select_country'); ?></label>
                                      <select name="country_id" id="country_id" class="form-control select2">
                                        <option value=""> <?= $this->lang->line('select_country'); ?></option>
                                        <?php foreach ($countries as $c) :?>
                                          <option value="<?= $c['country_id'];?>" <?= $driver_details[0]['country_id']==$c['country_id'] ? 'selected':''; ?> ><?=$c['country_name'];?></option>
                                        <?php endforeach; ?>
                                      </select>
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                  </div>
                              
                                  <div class="row">
                                    <div class="col-md-2">
                                    <label class="control-label">&nbsp;</label>
                                      <div class="input-group m-b">
                                          <span class="input-group-addon">+</span>
                                          <input type="text" class="form-control" id="country_code" name="country_code" value="<?= $driver_details[0]['country_code'] ?>" readonly>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <label class="control-label"><?= $this->lang->line('mobile_1'); ?></label>
                                      <input type="text" placeholder="<?= $this->lang->line('mobile_1'); ?>" class="form-control m-b" name="mobile1" id="mobile1" value="<?= $driver_details[0]['mobile1'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                      <label class="control-label"><?= $this->lang->line('mobile_2'); ?></label>
                                      <input type="text" placeholder="<?= $this->lang->line('mobile_2'); ?>" class="form-control m-b" name="mobile2" id="mobile2" value="<?= $driver_details[0]['mobile2'] ?>">
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-5">
                                      <label class="control-label"><?= $this->lang->line('password'); ?></label>
                                      <input type="password" placeholder="<?= $this->lang->line('password'); ?>" class="form-control m-b" name="password" id="password">
                                    </div>
                                    <div class="col-md-5">
                                      <label class="control-label"><?= $this->lang->line('confirm_password'); ?></label>
                                      <input type="password" placeholder="<?= $this->lang->line('confirm_password'); ?>" class="form-control m-b" name="rePassword" id="rePassword">
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-3">
                                      <label class="control-label"><?= $this->lang->line('permitted_category'); ?></label>
                                    </div>
                                    <div class="col-md-7">
                                      <?php foreach ($categories as $cat) { ?>
                                        <label><input type="checkbox" class="i-checks" name="cat_ids[]" value="<?= $cat['pc_id'] ?>" <?php if(in_array($cat['pc_id'], $catgory)) { echo 'checked'; } ?>> <?= $cat['short_name'] ?> </label>&nbsp;
                                      <?php } ?>
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-3">
                                      <label class="control-label"><?= $this->lang->line('driver_availability'); ?>*</label><br /><small>(*) <?= $this->lang->line('Select_atleast_one_day'); ?></small>
                                    </div>
                                    <div class="col-md-7" style="line-height: 50px;">
                                      <label> <input type="checkbox" class="i-checks" name="available[]" value="sun" <?=(in_array('sun', $avail))? 'checked' : '';?> > <?= $this->lang->line('sunday'); ?> </label>
                                      <label> <input type="checkbox" class="i-checks" name="available[]" value="mon" <?=(in_array('mon', $avail))? 'checked' : '';?> > <?= $this->lang->line('monday'); ?> </label>
                                      <label> <input type="checkbox" class="i-checks" name="available[]" value="tue" <?=(in_array('tue', $avail))? 'checked' : '';?> > <?= $this->lang->line('tuesday'); ?> </label>
                                      <label> <input type="checkbox" class="i-checks" name="available[]" value="wed" <?=(in_array('wed', $avail))? 'checked' : '';?> > <?= $this->lang->line('wednesday'); ?> </label>
                                      <label> <input type="checkbox" class="i-checks" name="available[]" value="thu" <?=(in_array('thu', $avail))? 'checked' : '';?> > <?= $this->lang->line('thursday'); ?> </label>
                                      <label> <input type="checkbox" class="i-checks" name="available[]" value="fri" <?=(in_array('fri', $avail))? 'checked' : '';?> > <?= $this->lang->line('friday'); ?> </label>
                                      <label> <input type="checkbox" class="i-checks" name="available[]" value="sat" <?=(in_array('sat', $avail))? 'checked' : '';?> > <?= $this->lang->line('saturday'); ?> </label>
                                    </div>
                                    <div class="col-md-2">&nbsp;</div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-10 text-right">
                                      <button class="btn btn-info" type="submit"><?= $this->lang->line('save_details'); ?></button>
                                      <a href="<?= $this->config->item('base_url') . 'user-panel-bus/driver-list'; ?>" class="btn btn-primary" type="submit"><?= $this->lang->line('cancel'); ?></a>
                                    </div>
                                  </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
      $("#driverEdit")
        .validate({
          ignore: [],
          rules: {
            first_name: { required : true },
            last_name: { required : true },
            email: { required : true, },
            country_id: { required : true },
            mobile1: { required : true, number : true },
            mobile2: { number : true },
            password: { minlength : 5, maxlength : 16 },
            rePassword: { minlength : 5, maxlength : 16, equalTo: "#password"},
            'available[]': { required: true, minlength: 1 },
            vehical_type_id: { required : true },
            bus_id: { required : true },
          },
          messages: {
            first_name: { required : "<?= $this->lang->line('enter_first_name'); ?>", },
            last_name: { required : "<?= $this->lang->line('enter_last_name'); ?>", },
            email: { required : "<?= $this->lang->line('enter_access_id'); ?>", },
            country_id: { required : "<?= $this->lang->line('select_country'); ?>", },
            mobile1: { required : "<?= $this->lang->line('enter_mobile_number'); ?>", number : "<?= $this->lang->line('numbers_only'); ?>" },
            mobile2: { number : "<?= $this->lang->line('enter_mobile_number'); ?>" },
            password: { minlength : "<?= $this->lang->line('min_5_char_required'); ?>", maxlength : "<?= $this->lang->line('max_16_char_allowed'); ?>", },
            rePassword: { minlength : "<?= $this->lang->line('min_5_char_required'); ?>", maxlength : "<?= $this->lang->line('max_16_char_allowed'); ?>", equalTo : "<?= $this->lang->line('password_not_matching'); ?>" },
            'available[]': '',
            vehical_type_id: { required : "<?= $this->lang->line('Select Vehicle Type'); ?>", },
            bus_id: { required : "<?= $this->lang->line('select_vehicle'); ?>", },
          },
        });
    </script>
    <script>
      $(function(){
        $("#country_id").on('change', function(event) { event.preventDefault();
          var id = $.trim($(this).val());
          console.log(id);
          var countries = <?= json_encode($countries); ?>;
          if(id != "" ) {
            $.each(countries, function(i, v){
              if(id == v['country_id']){  $("#country_code").val((v['country_phonecode'])); }
            });
          } else { $("#country_code").val(''); }
        });
      });
    </script>
    <script>
      $("#vehical_type_id").on('change', function(event) {  event.preventDefault();
        var vehical_type_id = $(this).val();
        
        if(vehical_type_id != "" ) { 
          $.ajax({
            type: "POST", 
            url: "<?=base_url('user-panel-bus/get-vehicle-by-type-id')?>", 
            data: { id: vehical_type_id },
            dataType: "json",
            success: function(res){
              console.log(res); 
              $('#bus_id').empty().trigger('change');
              $('#bus_id').trigger('change'); 
              //$('#bus_id').trigger('change');
              $.each( res, function(){$('#bus_id').append('<option value="'+$(this).attr('bus_id')+'">'+$(this).attr('bus_no')+'</option>');});
              $('#bus_id').focus();
            },
            beforeSend: function(){
              $('#bus_id').empty().trigger('change');
              $('#bus_id').append("<option value=''><?=$this->lang->line('Select Vehicle Type');?></option>");
              $('#bus_id').trigger('change'); 
            },
            error: function(){
              $('#bus_id').empty().trigger('change');
              $('#bus_id').append("<option value=''><?=$this->lang->line('Select Vehicle Type');?></option>");
              $('#bus_id').trigger('change'); 
            }
          });
        } else { 
          $('#bus_id').empty().trigger('change'); 
          $('#bus_id').append("<option value=''><?=$this->lang->line('Select Vehicle Type');?></option>");
          $('#bus_id').trigger('change');
        }
      });
    </script>