    
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>">Dashboard</a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/my-bookings'; ?>">Bookings</a></li>
                        <li class="active"><span>Add Booking</span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Add Booking Details
                </h2>
            </div>
        </div>
    </div>
        
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <form method="post" class="form-horizontal" action="<?= $this->config->item('base_url') . 'user-panel/advance-payment'; ?>">

                            <div class="form-group"><label class="col-sm-2 control-label">Type</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-6">
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="local" value="1" name="mode" aria-label="Local" ">
                                            <label for="Local"> Local </label>
                                        </div>
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="national" value="2" name="mode" aria-label="National">
                                            <label for="National"> National </label>
                                        </div>
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="international" value="3" name="mode" aria-label="International">
                                            <label for="International"> International </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <div class="col-lg-6">
                                    <label>From Address</label>
                                    <div class="hpanel hyellow contact-panel">
                                        <div class="panel-body">
                                            <span class="pull-right">
                                                <button title="Pick from address book" class="btn btn-info btn-circle" type="button" data-toggle="modal" data-target="#from_address"><i class="fa fa-address-book"></i></button>
                                            </span>
                                            <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                            <h3><a href=""> Bradly Danforth </a></h3>
                                            <div class="text-muted font-bold m-b-xs">California, LA</div>
                                            <p>
                                                Address Line 1 <br />
                                                Address Line 2 <br />
                                                City, State, Country
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label>To Address</label>
                                    <div class="hpanel hviolet contact-panel">
                                        <div class="panel-body">
                                            <span class="pull-right">
                                                <button title="Pick from address book" class="btn btn-info btn-circle" type="button" data-toggle="modal" data-target="#to_address"><i class="fa fa-address-book"></i></button>
                                            </span>
                                            <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                            <h3><a href=""> Bradly Danforth </a></h3>
                                            <div class="text-muted font-bold m-b-xs">California, LA</div>
                                            <p>
                                                Address Line 1 <br />
                                                Address Line 2 <br />
                                                City, State, Country
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Date of Pickup</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-6">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="text" class="form-control">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-group clockpicker" data-autoclose="true">
                                            <input type="text" class="form-control" value="09:30" >
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Date of Deliver</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-6">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="text" class="form-control">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-group clockpicker" data-autoclose="true">
                                            <input type="text" class="form-control" value="09:30" >
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Expiry Date</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-6">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="text" class="form-control">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="demo2" type="text"  name="demo2" placeholder="Quantity">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Mode</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-6">
                                        <select class="form-control m-b" name="cat_ids">
                                            <option>Avion</option>
                                            <option>Container</option>
                                            <option>Bike</option>
                                            <option>Motorbike</option>
                                            <option>Car</option>
                                            <option>Van</option>
                                            <option>Truck</option>
                                            <option>Grouping Container</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="local" value="1" name="mode" aria-label="Local" ">
                                            <label for="Local"> Shipping only </label>
                                        </div>
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="national" value="2" name="mode" aria-label="National">
                                            <label for="National"> Purchase & Ship </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Content</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="Content" class="form-control m-b" name="email" id="email">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Mode</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-12">
                                        <select class="form-control m-b" name="cat_ids">
                                            <option>Avion</option>
                                            <option>Container</option>
                                            <option>Bike</option>
                                            <option>Motorbike</option>
                                            <option>Car</option>
                                            <option>Van</option>
                                            <option>Truck</option>
                                            <option>Grouping Container</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Width/Height/Length</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-4">
                                        <input id="width" type="text"  name="width" placeholder="Width">
                                    </div>
                                    <div class="col-sm-4">
                                        <input id="height" type="text"  name="height" placeholder="Height">
                                    </div>
                                    <div class="col-sm-4">
                                        <input id="length" type="text"  name="length" placeholder="Length">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Weight/Unit</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-6">
                                        <input id="weight" type="text"  name="weight" placeholder="Width">
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control m-b" name="cat_ids">
                                            <option>KG</option>
                                            <option>Lbs.</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Insurance</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-2">
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="insurance_no" value="2" name="mode" aria-label="National" checked=""  onClick="hide_insurance_field()">
                                            <label for="National"> No </label>
                                        </div>
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="insurance_yes" value="1" name="mode" aria-label="Local" onClick="show_insurance_field()">
                                            <label for="Local"> Yes </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" placeholder="Enter Package Value" class="form-control m-b" name="email" id="package_value" style="display: none;">
                                    </div>
                                    <div class="col-sm-6">
                                        <h4 for="insurance_fee" id="insurance_fee" style="display: none;">Insurance Fee: $ 25</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Handling By</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-4">
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" name="handling_by_driver" id="handling_by_driver" value="0" aria-label="handling_self" checked>
                                            <label for="handling_self"> Self </label>
                                        </div>
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" name="handling_by_driver" id="handling_by_driver" value="1" aria-label="handling_driver">
                                            <label for="handling_driver"> Driver </label>
                                        </div>
                                    </div>
                                    <label class="col-sm-4 control-label">Do you want dedicated vehicle?</label>
                                    <div class="col-sm-4">
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="dedicated_vehicle_yes" value="1" name="dedicated_vehicle" aria-label="Yes">
                                            <label for="Local"> Yes </label>
                                        </div>
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="dedicated_vehicle_no" value="0" name="dedicated_vehicle" aria-label="No" checked>
                                            <label for="National"> No </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Travel with driver</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-4">
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="dedicated_vehicle_yes" value="1" name="dedicated_vehicle" aria-label="Yes">
                                            <label for="Local"> Yes </label>
                                        </div>
                                        <div class="radio radio-success radio-inline">
                                            <input type="radio" id="dedicated_vehicle_no" value="0" name="dedicated_vehicle" aria-label="No" checked>
                                            <label for="National"> No </label>
                                        </div>
                                    </div>
                                    <label class="col-sm-4 control-label">Reference</label>
                                    <div class="col-sm-4">
                                        <input type="text" placeholder="Reference" class="form-control m-b" name="email" id="email">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Pickup Instructions</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-6">
                                        <textarea class="form-control"></textarea>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-group image-preview">
                                            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                            <span class="input-group-btn">
                                                <!-- image-preview-clear button -->
                                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                    <span class="glyphicon glyphicon-remove"></span> Clear
                                                </button>
                                                <!-- image-preview-input -->
                                                <div class="btn btn-default image-preview-input">
                                                    <span class="glyphicon glyphicon-folder-open"></span>
                                                    <span class="image-preview-input-title">Browse</span>
                                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                                </div>
                                            </span>
                                        </div><!-- /input-group image-preview [TO HERE]--> 
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Delivery Instructions</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-6">
                                        <textarea class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Price</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-3">
                                        <h4 for="price" id="price">$ 75</h4>
                                    </div>
                                    <div class="col-sm-3 text-right">
                                        <a class="ladda-button btn btn-success" data-style="expand-left"><span class="ladda-label">Find Best Price</span><span class="ladda-spinner"></span></a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Create Booking</button>
                                    <a href="<?= $this->config->item('base_url') . 'user-panel/my-bookings'; ?>" class="btn btn-warning" type="submit">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Pick From Address Modals-->
    <div class="modal fade hmodal-info" id="from_address" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <h4 class="modal-title" style="display: inline-block;">Pick From Address</h4>
                    <button class="btn btn-outline btn-info" data-toggle="modal" data-target="#from_add_address" data-dismiss="modal" style="display: inline-block; float: right;"><i class="fa fa-plus"></i> Add New </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hpanel">
                                <div class="panel-body">
                                    <div class="input-group">
                                        <input class="form-control" type="text" placeholder="Search courier..">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hpanel hgreen contact-panel">
                                <div class="panel-body">
                                    <div class="radio pull-right">
                                        <input type="radio" id="singleRadio1" value="option1" name="radioSingle1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                                    <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                    <h3><a href=""> Bradly Danforth </a></h3>
                                    <div class="text-muted font-bold m-b-xs">California, LA</div>
                                    <p>
                                        Address Line 1 <br />
                                        Address Line 2 <br />
                                        City, State, Country
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hpanel hyellow contact-panel">
                                <div class="panel-body">
                                    <div class="radio pull-right">
                                        <input type="radio" id="singleRadio1" value="option1" name="radioSingle1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                                    <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                    <h3><a href=""> Bradly Danforth </a></h3>
                                    <div class="text-muted font-bold m-b-xs">California, LA</div>
                                    <p>
                                        Address Line 1 <br />
                                        Address Line 2 <br />
                                        City, State, Country
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hpanel hviolet contact-panel">
                                <div class="panel-body">
                                    <div class="radio pull-right">
                                        <input type="radio" id="singleRadio1" value="option1" name="radioSingle1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                                    <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                    <h3><a href=""> Bradly Danforth </a></h3>
                                    <div class="text-muted font-bold m-b-xs">California, LA</div>
                                    <p>
                                        Address Line 1 <br />
                                        Address Line 2 <br />
                                        City, State, Country
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hpanel hblue contact-panel">
                                <div class="panel-body">
                                    <div class="radio pull-right">
                                        <input type="radio" id="singleRadio1" value="option1" name="radioSingle1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                                    <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                    <h3><a href=""> Bradly Danforth </a></h3>
                                    <div class="text-muted font-bold m-b-xs">California, LA</div>
                                    <p>
                                        Address Line 1 <br />
                                        Address Line 2 <br />
                                        City, State, Country
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Add Address From Modals-->
    <div class="modal fade hmodal-info" id="from_add_address" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Add New Address</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="form-group"> 
                            <label class="">Address Photo</label>
                            <!-- image-preview-filename input [CUT FROM HERE]-->
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                    </div>
                                </span>
                            </div><!-- /input-group image-preview [TO HERE]--> 
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="">First Name</label>
                                <input type="text" " class="form-control" placeholder="First Name">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="">last Name</label>
                                <input type="text" class="form-control" id="" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="">Mobile Number</label>
                                <input type="text" class="form-control" id="" placeholder="Mobile Number">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="">Zip</label>
                                <input type="text" class="form-control" id="" placeholder="Zip">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="">Country</label>
                                <select class="js-source-states form-control" style="width: 100%" name="country_id">
                                    <option value="1">India</option>
                                    <option value="2">US</option>
                                    <option value="3">UK</option>
                                    <option value="4">France</option>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="">State</label>
                                <select class="js-source-states form-control" style="width: 100%" name="state_id">
                                    <option value="1">Maharashtra</option>
                                    <option value="2">Delhi</option>
                                    <option value="3">Gujrat</option>
                                    <option value="4">Tamil Nadu</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="">City</label>
                                <select class="js-source-states form-control" style="width: 100%" name="city_id">
                                    <option value="1">Malegaon</option>
                                    <option value="2">Jalgaon</option>
                                    <option value="3">Pune</option>
                                    <option value="4">Mumbai</option>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="">Street Name</label>
                                <input type="text" class="form-control" id="" placeholder="Street Name">
                            </div>
                        </div>                     
                        <div class="form-group">
                            <label class="">Address Line 1</label>
                            <textarea placeholder="Address Line 1" class="form-control"></textarea>
                        </div>                        
                        <div class="form-group">
                            <label class="">Address Line 2</label>
                            <textarea placeholder="Address Line 2" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="">Deliver Instructions</label>
                            <textarea placeholder="Deliver Instructions" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="">Pickup Instructions</label>
                            <textarea placeholder="Pickup Instructions" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal" data-target="#from_address">Close</button>
                    <button type="button" class="btn btn-primary">Add Address</button>
                </div>
            </div>
        </div>
    </div>

    <!--Pick To Address Modals-->
    <div class="modal fade hmodal-info" id="to_address" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <h4 class="modal-title" style="display: inline-block;">Pick From Address</h4>
                    <button class="btn btn-outline btn-info" data-toggle="modal" data-target="#to_add_address" data-dismiss="modal" style="display: inline-block; float: right;"><i class="fa fa-plus"></i> Add New </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hpanel">
                                <div class="panel-body">
                                    <div class="input-group">
                                        <input class="form-control" type="text" placeholder="Search courier..">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hpanel hgreen contact-panel">
                                <div class="panel-body">
                                    <div class="radio pull-right">
                                        <input type="radio" id="singleRadio1" value="option1" name="radioSingle1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                                    <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                    <h3><a href=""> Bradly Danforth </a></h3>
                                    <div class="text-muted font-bold m-b-xs">California, LA</div>
                                    <p>
                                        Address Line 1 <br />
                                        Address Line 2 <br />
                                        City, State, Country
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hpanel hyellow contact-panel">
                                <div class="panel-body">
                                    <div class="radio pull-right">
                                        <input type="radio" id="singleRadio1" value="option1" name="radioSingle1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                                    <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                    <h3><a href=""> Bradly Danforth </a></h3>
                                    <div class="text-muted font-bold m-b-xs">California, LA</div>
                                    <p>
                                        Address Line 1 <br />
                                        Address Line 2 <br />
                                        City, State, Country
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hpanel hviolet contact-panel">
                                <div class="panel-body">
                                    <div class="radio pull-right">
                                        <input type="radio" id="singleRadio1" value="option1" name="radioSingle1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                                    <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                    <h3><a href=""> Bradly Danforth </a></h3>
                                    <div class="text-muted font-bold m-b-xs">California, LA</div>
                                    <p>
                                        Address Line 1 <br />
                                        Address Line 2 <br />
                                        City, State, Country
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hpanel hblue contact-panel">
                                <div class="panel-body">
                                    <div class="radio pull-right">
                                        <input type="radio" id="singleRadio1" value="option1" name="radioSingle1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                                    <img alt="logo" class="img-circle m-b" src="<?= $this->config->item('resource_url') . 'web-panel/images/profile.jpg'; ?>">
                                    <h3><a href=""> Bradly Danforth </a></h3>
                                    <div class="text-muted font-bold m-b-xs">California, LA</div>
                                    <p>
                                        Address Line 1 <br />
                                        Address Line 2 <br />
                                        City, State, Country
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Add Address To Modals-->
    <div class="modal fade hmodal-info" id="to_add_address" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Add New Address</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="form-group"> 
                            <label class="">Address Photo</label>
                            <!-- image-preview-filename input [CUT FROM HERE]-->
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                                    </div>
                                </span>
                            </div><!-- /input-group image-preview [TO HERE]--> 
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="">First Name</label>
                                <input type="text" " class="form-control" placeholder="First Name">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="">last Name</label>
                                <input type="text" class="form-control" id="" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="">Mobile Number</label>
                                <input type="text" class="form-control" id="" placeholder="Mobile Number">
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="">Zip</label>
                                <input type="text" class="form-control" id="" placeholder="Zip">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="">Country</label>
                                <select class="js-source-states form-control" style="width: 100%" name="country_id">
                                    <option value="1">India</option>
                                    <option value="2">US</option>
                                    <option value="3">UK</option>
                                    <option value="4">France</option>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="">State</label>
                                <select class="js-source-states form-control" style="width: 100%" name="state_id">
                                    <option value="1">Maharashtra</option>
                                    <option value="2">Delhi</option>
                                    <option value="3">Gujrat</option>
                                    <option value="4">Tamil Nadu</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="">City</label>
                                <select class="js-source-states form-control" style="width: 100%" name="city_id">
                                    <option value="1">Malegaon</option>
                                    <option value="2">Jalgaon</option>
                                    <option value="3">Pune</option>
                                    <option value="4">Mumbai</option>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="">Street Name</label>
                                <input type="text" class="form-control" id="" placeholder="Street Name">
                            </div>
                        </div>                     
                        <div class="form-group">
                            <label class="">Address Line 1</label>
                            <textarea placeholder="Address Line 1" class="form-control"></textarea>
                        </div>                        
                        <div class="form-group">
                            <label class="">Address Line 2</label>
                            <textarea placeholder="Address Line 2" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="">Deliver Instructions</label>
                            <textarea placeholder="Deliver Instructions" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="">Pickup Instructions</label>
                            <textarea placeholder="Pickup Instructions" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#to_address" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Add Address</button>
                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
    .modal { overflow: auto !important; }
    </style>