<!-- Seller -->
<div class="row">
 
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
        <div class="hpanel stats">
            <div class="panel-body h-200">
                <div class="stats-title pull-left">
                   <h4> <?= $this->lang->line('total bookings'); ?></h4>
                </div>
                <div class="stats-icon pull-right">
                    <i class="pe-7s-portfolio fa-4x"></i>
                </div>
                <div class="m-t-xl">
                    <h3 class="m-xs"><?=sizeof($total_trips)?></h3>
                <span class="font-bold no-margins">
                    <?= $this->lang->line('completed trips'); ?>
                </span>
                <?php
                  $per = 0;
                  if(sizeof($completed_trips)!= 0 || sizeof($total_trips)!= 0)
                  {
                      $per= round((sizeof($completed_trips)/sizeof($total_trips))*100,2);
                  }
                ?>
                    <div class="progress m-t-xs full progress-small progress-striped active">
                        <div style="width: <?=$per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?=$per?>%" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?>%">                              
                        </div>
                    </div>

                   <div class="row">
                        <div class="col-xs-6">
                            <small class="stats-label"><?= $this->lang->line('completed trips'); ?></small>
                            <h4><?=sizeof($completed_trips)?></h4>
                        </div>

                        <div class="col-xs-6">
                            <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                          <h4><?=$per?>%</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>

  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                  <h4><?= $this->lang->line('upcoming')." ".$this->lang->line('booking'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-note fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= sizeof($upcoming); ?></h3>
              <span class="font-bold no-margins">
                 <?= $this->lang->line('in_progress_orders'); ?>
              </span> 
                <?php
                  $per = 0;
                  if(sizeof($upcoming)!= 0 || sizeof($total_trips)!= 0)
                  {
                      $per= round((sizeof($upcoming)/sizeof($total_trips))*100,2);
                  } 
                ?>  
                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?=$per?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?=$per?>%" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?>%">                              
                      </div>
                  </div>

                   <div class="row">
                     <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
                          <h4><?= $per;?> %</h4>
                      </div>

                     <!--  <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('in_progress'); ?></small>
                          <h4><?= $working_progress; ?> %</h4>
                      </div> -->
                  </div> 
              </div>
          </div>
      </div>
  </div>

  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                 <h4> <?= $this->lang->line('Cancelled Tickets'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-trash fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?=sizeof($cancel_ticket)?></h3>
              <span class="font-bold no-margins">
                  <?= $this->lang->line('completed trips'); ?>
              </span>
      
                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: 1%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="1%">                              
                      </div>
                  </div>

                  <!-- <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('awarded'); ?></small>
                          <h4><?= $total_awareded; ?> %</h4>
                      </div>

                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('delivered'); ?></small>
                          <h4><?= $total_delivered; ?> %</h4>
                      </div>
                  </div> -->
              </div>
          </div>
      </div>
  </div>

  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
          <h4><?= $this->lang->line('balance_summary'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
          <i class="pe-7s-cash fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h4 class="m-xs">XAF <?= $current_balance; ?></h4>
          <span class="font-bold no-margins">
            <?= $this->lang->line('current_balance'); ?>
          </span>
        </div>
        <div class="m-t-xs" style="margin-top: 0px;">
          &nbsp;
        </div>
        <div class="m-t-xs">
          <div class="row" style="margin-top: 0px;">
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('spent'); ?></small>
              <h4>XAF <?= $this->api->convert_big_int($total_spend);?> </h4>
            </div>
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('Ticket Spend'); ?></small>
              <h4>XAF <?= $this->api->convert_big_int($total_spend_service);?></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div> <!-- row -->

<div class="row">
  
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
         <?= $this->lang->line('Current Trips'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
               <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <thead>
                   <th><strong><?= $this->lang->line('Trip ID'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Source'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Destination'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Journey Date'); ?></strong></th>
                  </thead>
                  <tbody>
                    <?php if(!empty($current)):?>                      
                      <?php foreach ($current as $v): ?>                        
                        <tr>
                          <td>G-<?= $v['trip_id'];?><small></small></td>
                          <td><?= $v['source_point']; ?><small></small></td>
                          <td><?= $v['destination_point']; ?><small></small></td>
                          <td><?= $v['journey_date'];?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
         <?= $this->lang->line('Upcoming Trips'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
             <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <thead>
                   <th><strong><?= $this->lang->line('Trip ID'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Source'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Destination'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Journey Date'); ?></strong></th>
                  </thead>
                  <tbody>
                    <?php if(!empty($upcoming)):?>                      
                      <?php foreach ($upcoming as $v): ?>                        
                        <tr>
                          <td>G-<?= $v['trip_id'];?><small></small></td>
                          <td><?= $v['source_point']; ?><small></small></td>
                          <td><?= $v['destination_point']; ?><small></small></td>
                          <td><?= $v['journey_date'];?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="row"> 
  
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
         <?= $this->lang->line('Previous Trips'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <thead>
                   <th><strong><?= $this->lang->line('Trip ID'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Source'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Destination'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Journey Date'); ?></strong></th>
                  </thead>
                  <tbody>
                    <?php if(!empty($previous)):?>                      
                      <?php foreach ($previous as $v): ?>                        
                        <tr>
                          <td>G-<?= $v['trip_id'];?><small></small></td>
                          <td><?= $v['source_point']; ?><small></small></td>
                          <td><?= $v['destination_point']; ?><small></small></td>
                          <td><?= $v['journey_date'];?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
         <?= $this->lang->line('Cancelled Tickets'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <thead>
                   <th><strong><?= $this->lang->line('Trip ID'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Source'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Destination'); ?></strong></th>
                   <th><strong><?= $this->lang->line('Journey Date'); ?></strong></th>
                  </thead>
                  <tbody>
                    <?php if(!empty($cancel_ticket)):?>                      
                      <?php foreach ($cancel_ticket as $v): ?>                        
                        <tr>
                          <td>G-<?= $v['trip_id'];?><small></small></td>
                          <td><?= $v['source_point']; ?><small></small></td>
                          <td><?= $v['destination_point']; ?><small></small></td>
                          <td><?= $v['journey_date'];?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>  
</div>

</div>