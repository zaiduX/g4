<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
    float: left;
    border: 1px solid #225595;
    background-color: #3498db;
    width: 30%;
    height: 350px;
}

/* Style the buttons inside the tab */
.tab button {
    display: block;
    background-color: #3498db;
    color: white;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #225595;
}

/* Create an active/current "tab button" class */
.tab button.active {
    background-color: #225595;
}

/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #225595;
    width: 70%;
    border-left: none;
    height: 350px;
}
</style>
<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-upcoming-trips'; ?>"><?= $this->lang->line('Upcoming Trips'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('Ticket Booking Payment'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"><i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Ticket Booking Payment'); ?></h2>
            <small class="m-t-md"><?= $this->lang->line('Choose your payment option and complete you booking.'); ?></small>
        </div>
    </div>
</div>
        
<div class="content">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
            
                <?php if($this->session->flashdata('error_promo')):  ?>
                    <div class="row">
                        <div class="form-group"> 
                          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error_promo'); ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if($this->session->flashdata('success_promo')):  ?>
                    <div class="row">
                        <div class="form-group"> 
                          <div class="alert alert-success text-center"><?= $this->session->flashdata('success_promo'); ?></div>
                        </div>
                    </div>
                <?php endif; ?>

            <div class="hpanel">
                <div class="panel-body">
                    <?php 
                        $ticket_price = $details['ticket_price'];
                        $user_details = $this->api->get_user_details(trim($details['cust_id']));
                        $sender_reference = trim($details['cust_id']);
                        $currency = trim($details['currency_sign']);
                        $lang = substr($_SESSION['language'], 0, 2);
                    ?>
                    <div class="tab" style="width:15%">
                        <button class="tablinks" onclick="openTab(event, 'Payment')" id="defaultOpen"><?= $this->lang->line('complete_payment'); ?></button>
                        <button class="tablinks" onclick="openTab(event, 'CoD')"><?= $this->lang->line('Pay at office'); ?></button>
                    </div>
                    <div id="Payment" class="tabcontent" style="width:85%">
                            <div class="form-group">
                                <h2 class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center"><?= $this->lang->line('Ticket Booking Payment'); ?></h2>
                            </div>

                            <div class="form-group">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                                    <div class="hpanel stats">
                                        <div class="panel-body" style="padding-top:0px; padding-bottom:0px">

                                            <div>
                                                <i class="pe-7s-cash fa-5x"></i>
                                                <h1 class="m-xs text-success"><?= $details['currency_sign'] . ' '. $ticket_price ?></h1>
                                            </div>

                                            <p style="margin: 0 0 1px;">
                                                <?= $this->lang->line('str_c_pay'); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- <form action="<?=base_url().'user-panel-bus/promocode'?>" method="post" id="submit_promo_form">
                                <div class="row" style="padding:10px">
                                  <div class="col-md-8 col-md-offset-2">
                                    <div class="col-md-10 text-center">
                                    <input type="hidden" name="ticket_id" id="ticket_id" value="<?= $details['ticket_id']; ?>">
                                    <input type="hidden" name="page_name" id="page_name" value="single_payment_seller">
                                    <input type="text" class="form-control" name="promo_code" id="promo_code"  placeholder="Enter Promocode..." style="text-transform:uppercase" autocomplete="off" maxlength="12"/>
                                    </div>
                                    <div class="col-md-2" style="padding-left:0px; padding-right:0px; ">
                                      <button type="submit" class="btn btn-info" id="btn_submit_promo"><?= $this->lang->line('Apply'); ?></button>
                                    </div>
                                  </div>
                                </div>
                            </form> -->

                            <?php
                                //Get User Payment Methods from profile
                                function searchForId($id, $array) {
                                   foreach ($array as $key => $val) {
                                       if ($val['brand_name'] === $id) {
                                           return $key;
                                       }
                                   }
                                   return null;
                                }
                                $payment_mode = $this->user->get_customer_payment_methods(trim($details['cust_id']));
                                //echo json_encode($payment_mode);
                                $posMTN = searchForId("MTN",$payment_mode);
                                //Get user details
                                $user_details = $this->api->get_user_details(trim($details['cust_id']));

                                $sender_reference = trim($details['cust_id']);
                                $currency = trim($details['currency_sign']);
                                $lang = substr($_SESSION['language'], 0, 2);
                            ?>
                            <div class="row col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xl-offset-<?=($currency != 'XAF' || $currency != 'XOF')?1:2?> col-lg-offset-<?=($currency != 'XAF' || $currency != 'XOF')?1:2?>" style="display: inline-flex;">
                                <?php if($currency != 'XAF' || $currency != 'XOF') { ?>
                                    <div class="form-group">
                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 text-right">
                                            <a style="cursor: pointer;" data-toggle="modal" data-target="#paymentMTN" class="btn btn-warning btn-outline"><img style="padding:0px;  margin: 0px;" src="<?=base_url('resources/mtn-logo.png')?>">  &nbsp;&nbsp;&nbsp;<?= $this->lang->line('Pay By MTN'); ?></a>
                                            <div class="modal fade" id="paymentMTN" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title"><?= $this->lang->line('Enter MTN Mobile Number'); ?></h4>
                                                        </div>
                                                        <form id="formmomo" method="POST" action="<?=base_url('user-panel-bus/seller-mtn-single')?>" target="_top">
                                                            <input type="hidden" name="ticket_id" value="<?=$details['ticket_id']?>">
                                                            <input type="hidden" name="ticket_price" value="<?= $ticket_price; ?>" />
                                                            <div class="modal-body">
                                                              <div class="row">
                                                                    <div class="col-md-12 form-group">
                                                                        <div class="col-md-12">
                                                                            <label class="form-label"><?= $this->lang->line('MTN Mobile Number'); ?></label>
                                                                            <input type="text" name="phone_no" id="phone_no" class="form-control" placeholder="<?= $this->lang->line('Enter MTN Mobile Number'); ?>" value="<?=(isset($posMTN))?$payment_mode[$posMTN]['card_number']:'';?>">
                                                                        </div>
                                                                    </div>
                                                              </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                                                              <button type="submit" class="btn btn-info" id="btn_submit"><?= $this->lang->line('Pay Now'); ?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            
                            
                                <!--<div class="form-group">
                                    <div class="col-sm-4 text-right">
                                        <form action="<?= base_url('bus_payment/wecashup');?>" method="POST">
                                            <script async src="https://www.wecashup.cloud/demo/2-form/js/MobileMoney.js" class="wecashup_button"
                                              data-receiver-uid="kZudktr3nxX0AMM633dXyHEesV13"
                                              data-receiver-public-key="DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl"
                                              data-transaction-receiver-total-amount="<?= $ticket_price; ?>"
                                              data-transaction-receiver-currency="<?= $currency; ?>"
                                              data-name="Gonagoo" 
                                              data-transaction-receiver-reference="<?= trim($details['ticket_id']); ?>"
                                              data-transaction-sender-reference="<?=$sender_reference?>"
                                              data-style="1"
                                              data-image="<?= base_url('resources/images/72x72-payment.png'); ?>"
                                              data-cash="false"
                                              data-telecom="true"
                                              data-m-wallet="false"
                                              data-split="true"
                                              data-sender-lang="<?= trim($lang); ?>"
                                              data-sender-phonenumber="+237696532137">
                                            </script>
                                        </form>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <div class="colcol-xl-3 col-lg-3 col-md-12 col-sm-12 text-left">
                                        <form  class="pay" action="<?= base_url('user-panel-bus/seller-single-ticket-confirm-payment');?>" method="POST">
                                            <input type="hidden" name="ticket_id" value="<?= $details['ticket_id']; ?>" />
                                            <input type="hidden" name="payment_method" value="stripe" />
                                            <input type="hidden" name="ticket_price" value="<?= $ticket_price; ?>" />
                                            <input type="hidden" name="currency_sign" value="<?= $details['currency_sign']; ?>" />
                                              <script
                                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                data-key="pk_test_zbkyUBSvx5wMSon5cemiBCqO"
                                                data-amount="<?= $ticket_price; ?>"
                                                data-name="Gonagoo"
                                                data-description="<?= $this->lang->line('order_payment'); ?>"
                                                data-image="<?=base_url('resources/images/72x72-payment.png')?>"
                                                data-locale="auto"
                                                data-zip-code="false"
                                                data-allow-remember-me=false
                                                data-label="<?= $this->lang->line('pay_with_card'); ?>"
                                                data-currency="<?= $details['currency_sign']; ?>"
                                                data-email="<?=$user_details['email1']?>"
                                                >
                                              </script>
                                              <a style="cursor:pointer;" id="btn_mtn_pay" class="btn btn-info btn-outline"><img src="<?=base_url('resources/card_icon.png')?>">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('pay_with_card'); ?></a>
                                        </form>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 text-left">
                                        <form  class="pay" action="<?= base_url('user-panel-bus/seller-single-ticket-orange-payment');?>" method="POST" id="pay_orange">
                                            <input type="hidden" name="ticket_id" value="<?= $details['ticket_id']; ?>" />
                                            <input type="hidden" name="payment_method" value="orange" />
                                            <input type="hidden" name="ticket_price" value="<?= $ticket_price; ?>" />
                                            <input type="hidden" name="currency_sign" value="<?= $details['currency_sign']; ?>" />
                                            <a style="cursor:pointer;" id="btn_orange_pay" class="btn btn-danger btn-outline"><img src="<?=base_url('resources/logo-orange.png')?>">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('Pay with Orange Money'); ?></a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div id="CoD" class="tabcontent" style="width:85%">
                        <div class="form-group">
                            <h2 class="col-sm-12 text-center"><?= $this->lang->line('Cash at Office'); ?></h2>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12 text-center">
                                <div class="hpanel stats">
                                    <div class="panel-body" style="margin-bottom: 25px;">
                                        <div>
                                            <i class="pe-7s-cash fa-5x"></i>
                                            <h1 class="m-xs text-success"><?= $details['currency_sign'] . ' '. $ticket_price ?></h1>
                                        </div>
                                        <p><?= $this->lang->line('cod_msg2'); ?></p> <br /> 
                                    </div>
                                    <div class="row">
                                        <form action="<?= base_url('user-panel-bus/seller-single-ticket-confirm-payment');?>" method="POST" id="formCOD">
                                            <input type="hidden" name="ticket_id" value="<?=$details['ticket_id']?>">
                                            <input type="hidden" name="payment_method" value="cod">
                                            <input type="hidden" name="ticket_price" value="<?=$ticket_price?>">
                                            <input type="hidden" name="currency_sign" value="<?=$details['currency_sign']?>">
                                            <input type="hidden" name="stripeToken" value="NULL">
                                            <input type="hidden" name="stripeEmail" value="NULL">
                                            <button type="submit" class="btn btn-info btn-lg"><?= $this->lang->line('Confirm Payment'); ?></button>
                                            <!--<button type="button" id="cancel_trip" class="btn btn-danger btn-lg"><?= $this->lang->line('Cancel Ticket'); ?></button>-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>
</div>

<script>
    function openTab(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    //Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>

<script>
    $("#formCOD1").validate({
        ignore: [],
        rules:{
          cod_payment_type: { required:true },
        },
        messages:{
          cod_payment_type: { required:"Please select cash on delivery type!" },
        }
    });
</script>
<script>
    $("#formmomo").validate({
        ignore: [],
        rules:{
          phone_no: { required:true },
        },
        messages:{
          phone_no: { required:"Please enter MTN mobile number!" },
        }
    });
</script>
<script>
    $('#cancel_trip').click(function (e) {
        e.preventDefault();
        swal(
        {
            title: "<?= $this->lang->line('are_you_sure'); ?>",
            text: "<?= $this->lang->line('To Cancel This Ticket'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?= $this->lang->line('Yes, Cancel This Ticket.'); ?>",
            cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
            closeOnConfirm: false,
            closeOnCancel: false 
        },
        function (isConfirm) {
            if (isConfirm) {
                
                var ticket_id ="<?=$details['ticket_id']?>";
            $.ajax({
                url: "<?=base_url('user-panel-bus/delete-bus-ticket-seller')?>",
                type: 'POST' ,
                data: {ticket_id: ticket_id},
                success: function(data) {
                    console.log(data);
                    if(data == "cancel_successfull") {
                        window.location.replace("<?=base_url('user-panel-bus/bus-trip-ticket-booking-seller')?>");
                    } else {
                        swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('Error in ticket cancellation!'); ?>", "error");
                    }
                }
            });
            } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Seat is Stil Available Book Now'); ?>", "error"); }
        });          
    });   
</script>

<!-- promo -->

<script>
    $(".stripe-button-el").addClass("hidden");
    $('#btn_orange_pay').click(function(){
      $('#pay_orange').submit();
    });

    $('#btn_mtn_pay').click(function(){
      $('.stripe-button-el').click();
    });
</script>

<script>
    $("#submit_promo_form").validate({
        ignore: [],
        rules:{
          promo_code: { 
            required:true,
            minlength:12,
            maxlength:12     
            },
        },
        messages:{
          promo_code: { required:"<?=$this->lang->line('Please enter promocode!')?>",minlength:"<?=$this->lang->line('it should be 12 characters')?>",maxlength:"<?=$this->lang->line('it should be 12 characters')?>"},
        }
    });
</script>


<script>
$('#btn_submit_promo').click(function(e){
    e.preventDefault();
    var promocode = $('#promo_code').val();
    var ticket_id = $('#ticket_id').val();
    
    //user-panel/promocode-ajax

    $.ajax({
        type: "POST", 
        url: "<?php echo base_url('user-panel-bus/promocode-ajax'); ?>", 
        data: {ticket_id:ticket_id , promocode:promocode},
        success: function(res)
        {
            //console.log(res);
             var respp   = res.split('@');
            //console.log(respp[0]);
            if(respp[0] == "success"){
                var lines = respp[1].split('$');
                //console.log(lines[1]);
                
                swal(
                {
                    title:lines[0] ,
                    text: lines[1],
                    type: "success",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->lang->line('Yes Use this promocode');?>",
                    cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                    closeOnConfirm: false,
                    closeOnCancel: false 
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#submit_promo_form').submit();
                    } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Promocode not used'); ?>", "error"); }
                });

            }else{
                swal ( "Oops" ,  respp[1] ,  "error" )
            }
            
        }//success
    });//ajax
});
</script>