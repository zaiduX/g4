    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Laundry Charges'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Laundry Charges'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Country Currency-wise Laundry Charges'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel hblue" style="margin-bottom: 5px !important">
                    <div class="panel-body" style="padding-top: 10px;">

                        <?php if($this->session->flashdata('error')):  ?>
                        <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                        <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>

                        <form method="post" class="form-horizontal" action="laundry-charges-add-details" id="chargeAdd">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="text-left"><?= $this->lang->line('select_category'); ?></label>
                                    <select class="form-control js-source-states" name="cat_id" id="cat_id">
                                        <option value=""><?= $this->lang->line('select_category'); ?></option>
                                        <?php foreach ($category as $cat) { ?>
                                            <option value="<?= $cat['cat_id'] ?>"><?= ucwords($cat['cat_name']) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="text-left"><?= $this->lang->line('sub_categories'); ?></label>
                                    <select class="form-control js-source-states" name="sub_cat_id" id="sub_cat_id">
                                        <option value=""><?= $this->lang->line('Select category first'); ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="text-left"><?= $this->lang->line('select_country'); ?></label>
                                    <select class="form-control js-source-states" name="country_id" id="country_id">
                                        <option value=""><?= $this->lang->line('select_country'); ?></option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?= $country['country_id'] ?>"><?= ucwords($country['country_name']) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="text-left"><?= $this->lang->line('Rate'); ?> (<span class="currency">?</span>)
                                    <input type="hidden" name="currency_sign" id="currency_sign" /></label>
                                    <div class="">
                                        <input type="number" class="form-control" id="charge_amount" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="charge_amount" min="0" /> 
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <label class="text-left">&nbsp;</label>
                                    <div class="">
                                        <button class="btn btn-info" type="submit"><?= $this->lang->line('save'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="hpanel hblue">
                    <div class="panel-body" style="padding-top: 10px; padding-bottom: 10px;">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?= $this->lang->line('Category'); ?></th>
                                <th><?= $this->lang->line('Sub Category'); ?></th>
                                <th><?= $this->lang->line('Rate'); ?></th>
                                <th><?= $this->lang->line('Currency'); ?></th>
                                <th><?= $this->lang->line('Country'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($charges as $charge) { static $i=0; $i++; ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= strtoupper($charge['cat_name']) ?></td>
                                <td><?= strtoupper($charge['sub_cat_name']); ?></td>
                                <td>
                                    <div>
                                        <label id="charge_label_<?=$i?>" class=""><?= $charge['charge_amount'] ?></label>
                                        <button style="float: right" class="btn btn-info btn-sm" id="btn-edit-<?=$i?>" title="<?= $this->lang->line('edit'); ?>"><i class="fa fa-pencil"></i></button>
                                    </div>
                                    <div action="#" id="laundry-charge-edit-<?=$i?>" style="display: flex;" class="hidden">
                                        <input type="hidden" name="charge_id_<?=$i?>" value="<?= $charge['charge_id'] ?>" id="charge_id_<?=$i?>">
                                        <input type="number" class="form-control" id="new_amount_<?=$i?>" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="new_amount" min="0" value="<?=$charge['charge_amount']?>" />&nbsp;
                                        <button class="btn btn-info btn-sm" id="btn-save-<?=$i?>" title="<?= $this->lang->line('save'); ?>"><i class="fa fa-save"></i></button>
                                    </div>
                                    <script>
                                        $('#btn-edit-'+<?=$i?>).click(function () {
                                            $('#laundry-charge-edit-'+<?=$i?>).removeClass("hidden");
                                            $('#charge_label_'+<?=$i?>).addClass("hidden");
                                            $('#btn-edit-'+<?=$i?>).addClass("hidden");
                                            $("#tableData").dataTable().fnDestroy()
                                            $("#tableData").dataTable({ });
                                        });

                                        $('#btn-save-'+<?=$i?>).click(function () {
                                            var charge_id = $('#charge_id_'+<?=$i?>).val();
                                            var new_amount = $('#new_amount_'+<?=$i?>).val();
                                            $.ajax({
                                                type: "POST", 
                                                url: "<?=base_url('user-panel-laundry/update-laundry-charge-amount')?>", 
                                                data: { charge_id: charge_id, new_amount: new_amount },
                                                dataType: "json",
                                                success: function(res) {
                                                    swal("<?= $this->lang->line('success'); ?>", "<?= $this->lang->line('updated_successfully'); ?>", "success");
                                                },
                                                beforeSend: function(res){
                                                },
                                                error: function(res){
                                                    swal("<?= $this->lang->line('failed'); ?>", "<?= $this->lang->line('update_failed'); ?>", "error");
                                                }
                                            });
                                            $('#laundry-charge-edit-'+<?=$i?>).addClass("hidden");
                                            $('#charge_label_'+<?=$i?>).removeClass("hidden");
                                            $('#charge_label_'+<?=$i?>).text(new_amount);
                                            $('#btn-edit-'+<?=$i?>).removeClass("hidden");
                                            $("#tableData").dataTable().fnDestroy()
                                            $("#tableData").dataTable({ });
                                        });
                                    </script>
                                </td>
                                <td><?= strtoupper($charge['currency_sign']) ?></td>
                                <td><?= strtoupper($charge['country_name']) ?></td>
                                <td class="text-center">
                                    <button style="display: inline-block;" class="btn btn-danger btn-sm chargeDeleteAlert" id="<?= $charge['charge_id'] ?>"><i class="fa fa-trash-o"></i> <span class="bold"><?= $this->lang->line('Delete'); ?></span></button>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.chargeDeleteAlert').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-laundry/laundry-charges-delete')?>", {charge_id: id}, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('deleted'); ?>", "<?= $this->lang->line('Deleted successfully.'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting record!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Record is safe!'); ?>", "error"); }
            });
        });

        $("#country_id").on('change', function(event) { event.preventDefault();
            $(".currency").addClass('hidden').html('');
            var country_id = $(this).val();
            $.ajax({
                type: "POST", 
                url: "<?=base_url('user-panel/get-country-currencies')?>", 
                data: { country_id: country_id },
                dataType: "json",
                success: function(res){
                    if(res.length > 0 ){
                        $("#currency_id").val(res[0]['currency_id']);
                        $(".currency").removeClass('hidden').html('<strong>'+res[0]['currency_sign']+'</strong>'); 
                        $('#currency_sign').val(res[0]['currency_sign']);
                    } else {
                        $('#currency_id').val('0');
                        $(".currency").removeClass('hidden').html('?'); 
                        $('#currency_name').val("<?= $this->lang->line('No currency found!'); ?>");
                    }
                },
                beforeSend: function(){
                    $('#currency_id').empty();
                },
                error: function(){
                    $('#currency_id').val('0');
                    $(".currency").removeClass('hidden').html('?');
                    $('#currency_name').val("<?= $this->lang->line('No currency found!'); ?>");
                }
            });
        });

        $("#cat_id").on('change', function(event) { event.preventDefault();
            var cat_id = $(this).val();
            $.ajax({
                type: "POST", 
                url: "<?=base_url('user-panel-laundry/get-laundry-sub-category')?>", 
                data: { cat_id: cat_id },
                dataType: "json",
                success: function(res) {
                    if(res.length > 0 ) {
                        $('#sub_cat_id').empty();
                        $('#sub_cat_id').append("<option value=''><?= $this->lang->line('Select sub category'); ?></option>");
                        $('#sub_cat_id').select2();
                        $.each( res, function(){ 
                            $('#sub_cat_id').append('<option value="'+$(this).attr('sub_cat_id')+'">'+$(this).attr('sub_cat_name')+'</option>'); 
                        });
                        $('#sub_cat_id').select2('open');
                    } else {
                        $('#sub_cat_id').empty();
                        $('#sub_cat_id').append("<option value=''><?= $this->lang->line('Select category first'); ?></option>");
                        $('#sub_cat_id').select2();
                        $('#sub_cat_id').append("<option value=''><?= $this->lang->line('No sub category!'); ?></option>");
                    }
                },
                beforeSend: function(){
                    $('#sub_cat_id').empty();
                    $('#sub_cat_id').append("<option value=''><?= $this->lang->line('loading'); ?></option>");
                },
                error: function(){
                    $('#sub_cat_id').empty();
                    $('#sub_cat_id').append("<option value=''><?= $this->lang->line('No sub category!'); ?></option>");
                }
            });
        });

        $("#chargeAdd").validate({
            ignore: [], 
            rules: {
                cat_id: { required : true },
                sub_cat_id: { required : true },
                country_id: { required : true },
                currency_sign: { required : true },
                charge_amount: { required : true },
            },
            messages: {
                cat_id: { required : <?= json_encode($this->lang->line('select_category_type'));?>, },
                sub_cat_id: { required : <?= json_encode($this->lang->line('Select sub category'));?>, }, 
                country_id: { required :  <?= json_encode($this->lang->line('select_country'));?>, },
                currency_sign: { required :  <?= json_encode($this->lang->line('No currency'));?>, },
                charge_amount: { required :  <?= json_encode($this->lang->line('Enter Rate'));?>, },
            },
        });
    </script>
    <script>
        $("#sub_cat_id").on('change', function() {
            $('#country_id').select2('open');
        });
        $("#sub_cat_id").on('change', function() {
            $('#charge_amount').focus();
        });
    </script>