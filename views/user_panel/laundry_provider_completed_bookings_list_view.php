<style type="text/css">
  .table > tbody > tr > td {
      border-top: none;
  }
  .dataTables_filter {
      display: none;
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
          <div class="clip-header">
              <i class="fa fa-arrow-up"></i>
          </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
          <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= base_url('user-panel-laundry/dashboard-laundry'); ?>"><?= $this->lang->line('dash'); ?></a></li>
              <li class="active"><span><?= $this->lang->line('completed_orders'); ?></span></li>
          </ol>
      </div>
      <h2 class="font-light m-b-xs">
          <i class="fa fa-check fa-2x text-muted"></i> <?= $this->lang->line('completed_orders'); ?> &nbsp;&nbsp;&nbsp;
      </h2>
      <small><?= $this->lang->line('My Orders'); ?></small>
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="hpanel">
        <div class="panel-body" style="padding: 10px 17px 5px 17px; margin-bottom: -10px;">

          <div class="row" id="basicSearch">
            <div class="col-md-2">
              <h4><i class="fa fa-search"></i> <?= $this->lang->line('search'); ?></h4>
            </div>
            <div class="col-md-10">
              <input type="text" id="ordersearchbox" class="form-control" placeholder="<?= $this->lang->line('search'); ?>">
            </div>
          </div>
        </div>
        <br />

        <div class="panel-body" style="padding: 0px 10px 5px 10px;">
          <div class="row">
            <div class="col-lg-12">
              <?php if($this->session->flashdata('error')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                </div>
              </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <table id="orderTableData" class="table">
                <thead><tr class="hidden"><th></th></tr></thead>
                <tbody>
                  <?php foreach ($bookings as $order) { /*echo json_encode($order)*/ ?>
                    <tr>
                      <td style="line-height: 15px;">
                        <div class="hpanel" style="line-height: 15px; margin-bottom: 0;">
                          <div class="panel-body" style="line-height: 15px; padding-top: 5px; padding-bottom: 5px;">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"> 
                                <h5 class="text-left text-light"><?= $this->lang->line('ref_G'); ?><?= $order['booking_id'] ?></h5>
                              </div>
                              <div class="col-xl-6 col-lg-6 col-md-4 col-sm-4 text-center"> 
                                <h4 style="margin-top: 0px;">
                                  <?php $icon_url = $this->api->get_category_details_by_id($order['cat_id'])['icon_url']; ?>
                                  <img style="width:24px; height: auto;" src='<?= base_url($icon_url) ?>' /> <?= $this->lang->line('Laundry Service'); ?>
                                </h4>
                              </div>
                              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6"> 
                                <h6 class="text-right text-light"><strong> <?= $this->lang->line('order_date'); ?> <?= date('l, d M Y',strtotime($order['cre_datetime'])); ?></strong></h6>
                                <h6 class="text-right text-light"><?= $this->lang->line('Expected Complete Date'); ?>: <?= date('l, d M Y',strtotime($order['expected_return_date'])); ?></h6>
                              </div>
                              <br/>
                            </div>

                            <dir class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right:0px;margin-top: -15px;margin-bottom: 0px">
                              <h5 class="m-b-xs" style="color:#59bdd7"><i class="fa fa-user" aria-hidden="true"></i>
                                <?php 
                                if($order['cust_name'] == 'NULL' || $order['cust_name'] == 'NULL NULL') { 
                                  $user_details = $this->api->get_user_details($order['cust_id']);
                                  if($user_details['company_name'] != 'NULL') { echo $user_details['company_name']; } 
                                  else { $email_cut = explode('@', $user_details['email1']); echo $email_cut[0]; }
                                } else { echo $order['cust_name']; } ?>
                              </h5>
                            </dir>
                            <dir class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right:0px;margin-top: 0px;margin-bottom: 0px"> 
                              <dir class="col-xl-10 col-lg-10 col-md-10 col-sm-10" style="padding-left: 0px; padding-right:0px;margin-top: 0px;margin-bottom: 0px">
                                
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Laundry Charges'); ?>: <?= $order['currency_sign'] ?> <?= $order['booking_price'] ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('balance'); ?>: <?= $order['currency_sign'] . ' '. ($order['total_price'] - $order['paid_amount']); ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('advance'); ?>: <?= $order['currency_sign'] ?> <?= $order['advance_payment'] ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Is Pickup'); ?>: <?=($order['is_pickup']==1)?$this->lang->line('yes'):$this->lang->line('no')?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Is Drop'); ?>: <?=($order['is_drop']==1)?$this->lang->line('yes'):$this->lang->line('no')?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('No of items'); ?>: <?= $order['item_count']; ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Pickup Charges'); ?>: <?=$order['currency_sign'] . ' '. $order['pickup_courier_price']?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Delivery Charges'); ?>: <?=$order['currency_sign'] . ' '. $order['drop_courier_price']?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Total Charges'); ?>: <?=$order['currency_sign'] . ' '. $order['total_price']; ?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Payment Mode'); ?>: <?=strtoupper($order['payment_mode'])?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('COD Payment Type'); ?>: <?=strtoupper(str_replace('_', ' ', $order['cod_payment_type']))?></div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-left: 0px; padding-right:0px"> <?= $this->lang->line('Paid Amount'); ?>: <?=$order['currency_sign'] . ' '. $order['paid_amount']; ?></div>
                              </dir>
                              <dir class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-left: 0px; padding-right:0px;margin-top: 0px;margin-bottom: 0px">
                                <button class="btn btn-outline btn-sm btn-success btn_view_<?=$order['booking_id']?>"><i class="fa fa-eye"></i> <?= $this->lang->line('Show Items'); ?></button>
                                <button class="btn btn-outline btn-sm btn-success btn_hide_<?=$order['booking_id']?> hidden"><i class="fa fa-eye"></i> <?= $this->lang->line('Hide Items'); ?></button>&nbsp;
                                <?php if($order['is_drop'] == 0 ) { ?>
                                  <form action="<?=base_url('user-panel-laundry/customer-mark-is-drop-view')?>" method="post">
                                    <input type="hidden" name="booking_id" value="<?=$order['booking_id']?>">
                                    <input type="hidden" name="page_name" value="<?=$this->router->fetch_method()?>">
                                    <button class="btn btn-outline btn-sm btn-warning"><i class="fa fa-check"></i> <?= $this->lang->line('Is Drop? Yes.'); ?></button>
                                  </form>
                                <?php } ?>
                              </dir>
                            </dir>
                            
                          </div>
                          <div class="panel-body" id="details_<?=$order['booking_id']?>" style="display:none; padding-top: 0px; padding-bottom: 0px;">
                            <table style="width:100%" class="table display compact" id="tbl_<?=$order['booking_id']?>">
                              <thead>
                                <tr>
                                  <th><?= $this->lang->line('Sr. No'); ?></th>
                                  <th><?= $this->lang->line('Item Category'); ?></th>
                                  <th><?= $this->lang->line('Item Name'); ?></th>
                                  <th><?= $this->lang->line('Single Unit Price'); ?></th>
                                  <th><?= $this->lang->line('quantity'); ?></th>
                                  <th><?= $this->lang->line('total'); ?></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($order['bookings_details'] as $dtls) { static $i=0; $i++; ?>
                                  <tr>
                                    <td><?=$i?></td>
                                    <td><?=$dtls['cat_name']?></td>
                                    <td><?=$dtls['sub_cat_name']?></td>
                                    <td><?=$dtls['single_unit_price']?></td>
                                    <td><?=$dtls['quantity']?></td>
                                    <td><?=$dtls['total_price']?></td>
                                  </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                          </div>
                          <div class="panel-footer" style="padding-top: 5px;padding-bottom: 5px;">
                            <div class="row">
                              <div class="col-md-8">
                                <h5 style="color: #59bdd7">
                                  <?= $this->lang->line('Completed on'); ?>: <?=date('l, d M Y h:i:s',strtotime($order['status_update_datetime']))?>
                                </h5>
                              </div>
                              <div style="float: right;">
                                <div class="col-md-4 text-right" style="display: inline-flex;">
                                  <?php 
                                    if(isset($order['drop_order_details']) && $order['drop_order_details'][0]['order_status'] != 'delivered' && $order['drop_order_details'][0]['driver_id'] > 0) { ?>
                                      <form action="<?=base_url('user-panel-laundry/laundry-chat-room')?>" method="post">
                                        <input type="hidden" name="order_id" value="<?=$order['drop_order_id']?>">
                                        <button type="submit" class="btn btn-outline btn-sm btn-success"><i class="fa fa-gears"></i> <?= $this->lang->line('Driver Chat'); ?></button>
                                      </form>&nbsp;
                                  <?php } ?>
                                  
                                  <?php if($order['is_drop'] > 0 && $order['drop_order_id'] == 0) { ?>
                                    <button class="btn btn-outline btn-sm btn-success btn_drop_<?=$order['booking_id']?>" data-toggle="modal" data-target="#myModal_<?=$order['booking_id']?>"><i class="fa fa-check"></i> <?= $this->lang->line('Create Parcel Drop Job'); ?></button>
                                    <div class="modal fade" id="myModal_<?=$order['booking_id']?>" role="dialog">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"><?=$this->lang->line('Create Drop Booking');?></h4>
                                          </div>
                                          <div class="modal-body">
                                            <?php $address_details = $this->api->get_address_from_book((int)$order['drop_address_id']); ?>
                                            <form action="<?=base_url('user-panel-laundry/create-drop-booking')?>" method="post" id="create_<?=$order['booking_id']?>">
                                              <input type="hidden" name="booking_id" value="<?=$order['booking_id']?>">
                                              <input type="hidden" name="page_name" value="<?=$this->router->fetch_method()?>">
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <h4 class=""><i class="fa fa-map-marker"></i> <?= $this->lang->line('Delivered to');?>: <br /><font style="color: #59bdd7"><?=$address_details['street_name']?></font></h4>
                                                </div>
                                                <div class="col-md-6">
                                                  <label class=""><?= $this->lang->line('pickup_date');?></label>
                                                  <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                                                    <input type="text" class="form-control" id="pickupdate_<?=$order['booking_id']?>" name="pickupdate" autocomplete="none" autocomplete="off" />
                                                    <div class="input-group-addon">
                                                      <span class="glyphicon glyphicon-th"></span>
                                                    </div>
                                                  </div>  
                                                </div>
                                                <div class="col-md-6">
                                                  <label class=""><?= $this->lang->line('Pickup Time');?></label>
                                                  <div class="input-group clockpicker" data-autoclose="true">
                                                    <input type="text" class="form-control" id="pickuptime_<?=$order['booking_id']?>" name="pickuptime" autocomplete="off" />
                                                    <span class="input-group-addon">
                                                      <span class="fa fa-clock-o"></span>
                                                    </span>
                                                  </div>
                                                </div>
                                              </div><br />
                                              <div class="row">
                                                <div class="col-md-12 text-right">
                                                  <input type="submit" id="createdelivery_<?=$order['booking_id']?>" class="btn btn-success createdelivery_<?=$order['booking_id']?>" value="<?= $this->lang->line('create_booking'); ?>">
                                                  <button type="button" class="btn btn-outline btn-success" data-dismiss="modal"><?= $this->lang->line('cancel'); ?></button>
                                                </div>
                                              </div>
                                            </form>
                                            <script>
                                              $("#createdelivery_<?=$order['booking_id']?>").on('click', function(e) {  
                                                e.preventDefault();
                                                var pickupdate = $("#pickupdate_<?=$order['booking_id']?>").val();
                                                var pickuptime = $("#pickuptime_<?=$order['booking_id']?>").val();
                                                if(pickupdate == '' ) {  swal('<?=$this->lang->line("error")?>','<?=$this->lang->line("Select Date")?>','warning'); } 
                                                else if(pickuptime == '' ) {  swal('<?=$this->lang->line("error")?>','<?=$this->lang->line("Select Time")?>','warning'); } 
                                                else $("#create_<?=$order['booking_id']?>")[0].submit();
                                              });
                                            </script>
                                          </div>
                                          <div class="modal-footer">
                                            <small>Gonagoo</small>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    &nbsp;
                                  <?php } ?>

                                  <form action="<?=base_url('user-panel-laundry/laundry-work-room')?>" method="post">
                                    <input type="hidden" name="booking_id" value="<?=$order['booking_id']?>">
                                    <button class="btn btn-outline btn-sm btn-warning"><i class="fa fa-briefcase"></i> <?= $this->lang->line('go_to_workroom'); ?></button>
                                  </form>&nbsp;
                                  <?php if($order['complete_paid'] == 0 && $order['payment_mode'] == 'cod' && $order['cod_payment_type'] == 'at_deliver'): ?>
                                    <form action="<?=base_url('user-panel-laundry/laundry-booking-payment-by-provider')?>" method="post">
                                      <input type="hidden" name="booking_id" value="<?=$order['booking_id']?>">
                                      <button class="btn btn-outline btn-sm btn-danger" id="btn_payment_<?=$order['booking_id']?>"><i class="fa fa-gears"></i> <?= $this->lang->line('Complete Payment'); ?></button>
                                    </form>
                                  <?php endif; ?>
                                </div>
                              </div>
                            </div>
                          </div>
                          <script>
                            $(".btn_view_<?=$order['booking_id']?>").click(function(){
                              $("#details_<?=$order['booking_id']?>").slideToggle(500);
                              $(".btn_view_<?=$order['booking_id']?>").addClass("hidden");
                              $(".btn_hide_<?=$order['booking_id']?>").removeClass("hidden");
                            });
                            $(".btn_hide_<?=$order['booking_id']?>").click(function(){
                              $("#details_<?=$order['booking_id']?>").slideToggle(500);
                              $(".btn_view_<?=$order['booking_id']?>").removeClass("hidden");
                              $(".btn_hide_<?=$order['booking_id']?>").addClass("hidden");
                            });
                            $(document).ready(function() {
                              $('#tbl_<?=$order['booking_id']?>').DataTable({
                                "paging":   false,
                                "info":     false
                              });
                            });
                            $("#btn_mark_<?=$order['booking_id']?>").on('click',function(e){
                              e.preventDefault();
                              var form = $(this).parents('form');
                              swal({
                                title: "<?= $this->lang->line('are_you_sure'); ?>",
                                text: "<?= $this->lang->line('Booking status will mark as in-progress and booking will move to in-progress list.'); ?>",
                                type: "warning",
                                showCancelButton: true,
                                cancelButtonText: "<?= $this->lang->line('cancel'); ?>",
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "<?= $this->lang->line('yes'); ?>",
                                closeOnConfirm: false
                              }, function(isConfirm){
                                if (isConfirm) form.submit();
                              });
                          });
                          </script>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>