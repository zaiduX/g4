    
<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/service-area'; ?>"><?= $this->lang->line('service_area'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('add_service_area'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">  <i class="fa fa-map-marker fa-2x text-muted"></i>
                <?= $this->lang->line('service_area'); ?>
            </h2>
            <small class="m-t-md"><?= $this->lang->line('add_details'); ?></small> 
        </div>
    </div>
</div>
    
<div class="content">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
      <div class="hpanel">
        <div class="panel-body">
          <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
          <?php endif; ?>
          <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
          <?php endif; ?>
          <form method="post" class="form-horizontal" action="service-area-add-details" id="serviceAdd">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
              <label class="control-label"><?= $this->lang->line('Type'); ?></label>
              <div class="">
                <div class="radio radio-success radio-inline">
                  <input type="radio" id="local" value="1" name="mode" aria-label="Local" onclick="fun_local()" checked="checked">
                  <label for="Local"> <?= $this->lang->line('local'); ?> </label>
                </div>
                <div class="radio radio-success radio-inline">
                  <input type="radio" id="national" value="2" name="mode" aria-label="National" onclick="fun_national()">
                  <label for="National"> <?= $this->lang->line('national'); ?> </label>
                </div>
                <div class="radio radio-success radio-inline">
                  <input type="radio" id="international" value="3" name="mode" aria-label="International" onclick="fun_international()">
                  <label for="International"> <?= $this->lang->line('international'); ?> </label>
                </div>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
              <label class="control-label"><?= $this->lang->line('country'); ?></label>
              <div class="">
                <select id="country_id" name="country_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Country">
                  <option value=""><?= $this->lang->line('select_country'); ?></option>
                  <?php foreach ($countries as $country): ?>
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" id="divstate">
              <label class="control-label"><?= $this->lang->line('state'); ?></label>
              <div class="">
                <select id="state_id" name="state_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select State" disabled>
                  <option value=""><?= $this->lang->line('sel_state'); ?></option>
                </select>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" id="divcity">
              <label class="control-label"><?= $this->lang->line('city'); ?></label>
              <div class="">
                <select id="city_id" name="city_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select City" disabled>
                  <option value=""><?= $this->lang->line('sel_state'); ?></option>
                </select>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" id="divzip">
              <label class="control-label"><?= $this->lang->line('zip'); ?></label>
              <div class="">
                <input type="text" placeholder="Zip" class="form-control m-b" name="zip" id="zip">
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" id="divareaname">
              <label class="control-label"><?= $this->lang->line('area_name'); ?></label>
              <div class="">
                <input type="text" placeholder="Area Name" class="form-control m-b" name="area_name" id="area_name">
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
              <br />
              <button class="btn btn-primary" type="submit"><?= $this->lang->line('create_service_area'); ?></button>
              <a href="<?= $this->config->item('base_url') . 'user-panel/service-area'; ?>" class="btn btn-info"><?= $this->lang->line('cancel'); ?></a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#serviceAdd")
  .validate({
    ignore: [], 
    rules: {
      country_id: { required : true, },
    },
    messages: {
      country_id: { required : "Please select country!", },
    },
  });
</script>

<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
    $('#city_id').attr('disabled', true);

    $.ajax({
      url: "get-state-by-country-id",
      type: "POST",
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        console.log(res);
        $('#state_id').attr('disabled', false);
        $('#state_id').empty();
        $('#state_id').append('<option value="0"><?= json_encode($this->lang->line('select_state'))?></option>');
        $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
        $('#state_id').focus();
      },
      beforeSend: function(){
        $('#state_id').empty();
        $('#state_id').append('<option value="0"><?= json_encode($this->lang->line('loading'))?></option>');
      },
      error: function(){
        $('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append('<option value="0"><?= json_encode($this->lang->line('no_options'))?></option>');
      }
    })
  });

  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $.ajax({
      type: "POST",
      url: "get-cities-by-state-id",
      data: { state_id: state_id },
      dataType: "json",
      success: function(res){
        $('#city_id').attr('disabled', false);
        $('#city_id').empty();
        $('#city_id').append('<option value="0"><?= json_encode($this->lang->line('select_city'))?></option>');
        $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
        $('#city_id').focus();
      },
      beforeSend: function(){
        $('#city_id').empty();
        $('#city_id').append('<option value="0"><?= json_encode($this->lang->line('loading'))?></option>');
      },
      error: function(){
        $('#city_id').attr('disabled', true);
        $('#city_id').empty();
        $('#city_id').append('<option value="0"><?= json_encode($this->lang->line('no_options'))?></option>');
      }
    })
  });
</script>