<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
    float: left;
    border: 1px solid #225595;
    background-color: #3498db;
    width: 30%;
    height: 350px;
}

/* Style the buttons inside the tab */
.tab button {
    display: block;
    background-color: #3498db;
    color: white;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #225595;
}

/* Create an active/current "tab button" class */
.tab button.active {
    background-color: #225595;
}

/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #225595;
    width: 70%;
    border-left: none;
    height: 350px;
}
</style>
<div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/buyer-upcoming-trips'; ?>"><?= $this->lang->line('Upcoming Trips'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('Ticket Booking Payment'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs"><i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Ticket Booking Payment'); ?></h2>
                <small class="m-t-md"><?= $this->lang->line('Choose your payment option and complete you booking.'); ?></small>
            </div>
        </div>
    </div>
        
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <?php
                        if(!empty($return_details))
                            $ticket_price = $details['ticket_price'] + $return_details['ticket_price'];
                        else
                            $ticket_price = $details['ticket_price'];
                        ?>                
        
                        <div class="tab">
                            <button class="tablinks" onclick="openTab(event, 'Payment')" id="defaultOpen"><?= $this->lang->line('complete_payment'); ?></button>
                            <?php if($details['payment_mode'] != 'cod') { ?>
                                <button class="tablinks" onclick="openTab(event, 'CoD')"><?= $this->lang->line('Pay at office'); ?></button>
                            <?php } ?>
                        </div>

                        <div id="Payment" class="tabcontent">
                            <div class="form-group">
                                <h2 class="col-sm-12 text-center"><?= $this->lang->line('Ticket Booking Payment'); ?></h2>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <div class="hpanel stats">
                                        <div class="panel-body">

                                            <div>
                                                <i class="pe-7s-cash fa-5x"></i>
                                                <h1 class="m-xs text-success"><?= $details['currency_sign'] . ' '. $ticket_price ?></h1>
                                            </div>

                                            <p>
                                                <?= $this->lang->line('str_c_pay'); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                                //Get User Payment Methods from profile
                                function searchForId($id, $array) {
                                   foreach ($array as $key => $val) {
                                       if ($val['brand_name'] === $id) {
                                           return $key;
                                       }
                                   }
                                   return null;
                                }
                                $payment_mode = $this->user->get_customer_payment_methods(trim($details['cust_id']));
                                //echo json_encode($payment_mode);
                                $posMTN = searchForId("MTN",$payment_mode);
                                //Get user details
                                $user_details = $this->api->get_user_details(trim($details['cust_id']));

                                $sender_reference = trim($details['cust_id']);
                                $currency = trim($details['currency_sign']);
                                $lang = substr($_SESSION['language'], 0, 2);
                            ?>
                            <div class="row col-md-8 col-md-offset-<?=($currency == 'XAF' || $currency == 'XOF')?1:2?>" style="display: inline-flex;">
                                <?php if($currency == 'XAF' || $currency == 'XOF') { ?>
                                    <div class="form-group">
                                        <div class="col-sm-6 text-right">
                                            <a style="cursor: pointer;" data-toggle="modal" data-target="#paymentMTN" class="btn btn-info"><?= $this->lang->line('Pay By MTN'); ?></a>
                                            <div class="modal fade" id="paymentMTN" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title"><?= $this->lang->line('Enter MTN Mobile Number'); ?></h4>
                                                        </div>
                                                        <form id="formmomo" method="POST" action="<?=base_url('payment-bus/mtn')?>" target="_top">
                                                            <input type="hidden" name="ticket_id" value="<?=$details['ticket_id']?>">
                                                            <div class="modal-body">
                                                              <div class="row">
                                                                    <div class="col-md-12 form-group">
                                                                        <div class="col-md-12">
                                                                            <label class="form-label"><?= $this->lang->line('MTN Mobile Number'); ?></label>
                                                                            <input type="text" name="phone_no" id="phone_no" class="form-control" placeholder="<?= $this->lang->line('Enter MTN Mobile Number'); ?>" value="<?=(isset($posMTN))?$payment_mode[$posMTN]['card_number']:'';?>">
                                                                        </div>
                                                                    </div>
                                                              </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
                                                              <button type="submit" class="btn btn-info" id="btn_submit"><?= $this->lang->line('Pay Now'); ?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            
                            
                                <!--<div class="form-group">
                                    <div class="col-sm-4 text-right">
                                        <form action="<?= base_url('bus_payment/wecashup');?>" method="POST">
                                            <script async src="https://www.wecashup.cloud/demo/2-form/js/MobileMoney.js" class="wecashup_button"
                                              data-receiver-uid="kZudktr3nxX0AMM633dXyHEesV13"
                                              data-receiver-public-key="DcWEKS6f4GT7DlYXSPi6XeYDZT1C6q228XNdgO5HmIWl"
                                              data-transaction-receiver-total-amount="<?= $ticket_price; ?>"
                                              data-transaction-receiver-currency="<?= $currency; ?>"
                                              data-name="Gonagoo" 
                                              data-transaction-receiver-reference="<?= trim($details['ticket_id']); ?>"
                                              data-transaction-sender-reference="<?=$sender_reference?>"
                                              data-style="1"
                                              data-image="<?= base_url('resources/images/72x72-payment.png'); ?>"
                                              data-cash="false"
                                              data-telecom="true"
                                              data-m-wallet="false"
                                              data-split="true"
                                              data-sender-lang="<?= trim($lang); ?>"
                                              data-sender-phonenumber="+237696532137">
                                            </script>
                                        </form>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <div class="col-md-4 text-left">
                                        <form  class="pay" action="<?= base_url('user-panel-bus/ticket-confirm-payment');?>" method="POST">
                                            <input type="hidden" name="ticket_id" value="<?= $details['ticket_id']; ?>" />
                                            <input type="hidden" name="payment_method" value="stripe" />
                                              <script
                                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                data-key="pk_test_zbkyUBSvx5wMSon5cemiBCqO"
                                                data-amount="<?= $ticket_price; ?>"
                                                data-name="Gonagoo"
                                                data-description="<?= $this->lang->line('order_payment'); ?>"
                                                data-image="<?=base_url('resources/images/72x72-payment.png')?>"
                                                data-locale="auto"
                                                data-zip-code="false"
                                                data-allow-remember-me=false
                                                data-label="<?= $this->lang->line('pay_with_card'); ?>"
                                                data-currency="<?= $details['currency_sign']; ?>"
                                                data-email="<?=$user_details['email1']?>"
                                                >
                                              </script>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($details['payment_mode'] != 'cod') { ?>
                        <div id="CoD" class="tabcontent">
                            <h3><?= $this->lang->line('Cash at Office'); ?></h3>
                            <p><?= $this->lang->line('Save cash! you can complete your ticket payment by card or mobile money from Complete Payment section and save cash.'); ?></p> <br />
                            <br />
                            <form action="<?= base_url('user-panel-bus/ticket-mark-as-cod');?>" method="POST" id="formCOD">
                                <input type="hidden" name="ticket_id" value="<?=$details['ticket_id']?>">
                                <button type="submit" class="btn btn-info btn-lg"><?= $this->lang->line('prceed'); ?></button>
                            </form>
                        </div>
                        <?php } ?>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    function openTab(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>

<script>
    $("#formCOD1").validate({
        ignore: [],
        rules:{
          cod_payment_type: { required:true },
        },
        messages:{
          cod_payment_type: { required:"Please select cash on delivery type!" },
        }
    });
</script>
<script>
    $("#formmomo").validate({
        ignore: [],
        rules:{
          phone_no: { required:true },
        },
        messages:{
          phone_no: { required:"Please enter MTN mobile number!" },
        }
    });
</script>