<div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/user-notifications'; ?>"><?= $this->lang->line('notifications'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('detail'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    <?= $this->lang->line('notification_detail'); ?>
                </h2>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-4 text-right"> <?= $this->lang->line('title'); ?> :</div>
                            <div class="col-md-9 col-lg-9 col-sm-8"> <?= $notice['notify_title']; ?></div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-4 text-right"> <?= $this->lang->line('date'); ?> :</div>
                            <div class="col-md-4 col-lg-4 col-sm-4">  <?= date('l, d F Y', strtotime($notice['cre_datetime'])); ?></div>
                        </div>

                        <hr/>

                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-4 text-right"> <?= $this->lang->line('detail'); ?> :</div>
                            <div class="col-md-9 col-lg-9 col-sm-8"> <?= nl2br($notice['notify_text']); ?></div>
                        </div>

                        <hr/>

                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-4 text-right"> <?= $this->lang->line('attachment'); ?> :</div>
                            <?php if($notice['attachement_url'] != "NULL"): ?>
                                <div class="col-md-4 col-lg-4 col-sm-4"> 
                                    <label class="label label-info icon-left">
                                        <a href="<?= base_url(). $notice['attachement_url']?>" target="_blank" class="text-white"> <i class="fa fa-download"></i> <?= $this->lang->line('download'); ?> </a>
                                    </label>
                                </div>
                            <?php else: ?>
                                <div class="col-md-4 col-lg-4 col-sm-4"> <?= $this->lang->line('not_found'); ?></div>
                            <?php endif; ?>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <a href="<?= base_url('user-panel-laundry/user-notifications');?>" class="btn btn-outline btn-primary" type="button"><i class="fa fa-chevron-left"></i> <?= $this->lang->line('notifications'); ?></a>
                    </div>

                </div>
            </div>
        </div>
    </div>