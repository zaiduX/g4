<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
    display: none;
  }

  .bg {
    z-index: 1;
    background: url("<?=base_url('resources/web/front_site/images/slider/bus_bg.jpg');?>") center center;
    opacity: 0.9;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-attachment: fixed;
    max-width: 100%;
    justify-content: center;
    align-items: flex-end;
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Search Ticket'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-search fa-2x text-muted"></i> <?= $this->lang->line('Search Ticket'); ?></h2>
        <small class="m-t-md"><?= $this->lang->line('Search ticket and book your seat'); ?></small> 
    </div>
  </div>
</div>

<div class="content bg">
  <div class="row">
    <div class="col-md-12" style="min-height: 520px;">
      <div class="hpanel">
        <div class="panel-body" style="background-color:transparent !important; border: none !important;">
          <div class="row">
              <div class="row form-group">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
                  <span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important; font-size: 2.25em;"><?= $this->lang->line('Cheap bus tickets with the best agencies'); ?></span><br />
                  <span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important; font-size: 1.25em;"><?= $this->lang->line('Save time, book and get your ticket by SMS, mail or withdrawal at the agency'); ?></span>
                </div>
              </div> 
          </div>
        </div>
        <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12">
            <div class="panel-body" style="background-color: #fffffffa !important; padding-bottom: 0px; border-radius: 3px; margin-top: 140px;">
          <div class="row" style="margin-top: -10px;">
            <form action="<?= base_url('user-panel-bus/bus-trip-search-result-seller') ?>" method="post" id="frmBasicSearch"> 
              <input type="hidden" name="cat_id" value="281">
              <div class="col-md-6 form-group">
                <label><i class="fa fa-arrow-up"></i> <?= $this->lang->line('From'); ?></label>
                <select class="select2 form-control" name="trip_source" id="trip_source">
                  <option value=""><?= $this->lang->line('Starting city'); ?></option>
                  <?php foreach ($trip_sources as $source) { ?>
                  <option value="<?= $source['trip_source'] ?>"><?= $source['trip_source'] ?></option>
                  <?php } ?>
                </select> 
              </div> 
              <div class="col-md-6 form-group">
                <label><i class="fa fa-arrow-down"></i> <?= $this->lang->line('To'); ?></label>
                <select class="select2 form-control" name="trip_destination" id="trip_destination">
                  <option value=""><?= $this->lang->line('Destination city'); ?></option>
                  <?php foreach ($trip_destinations as $destination) { ?>
                  <option value="<?= $destination['trip_destination'] ?>"><?= $destination['trip_destination'] ?></option>
                  <?php } ?>
                </select>
              </div> 
              <div class="col-md-3 form-group">
                <label for="example-number-input"><i class="fa fa-users"></i> <?= $this->lang->line('Passengers'); ?></label>
                <input class="form-control" type="number" value="1" id="no_of_seat" name="no_of_seat" min="1" max="40">
              </div>
              <div class="col-md-3 form-group">
                <label for="example-date-input"><i class="fa fa-calendar"></i> <?= $this->lang->line('Journey Date'); ?></label>
                <div class="input-group">
                  <input type="text" class="form-control" id="journey_date1" name="journey_date" value="<?=date('m/d/Y', strtotime('+ 0 day'))?>" autocomplete="off" >
                  <div class="input-group-addon dpAddon">
                    <span class="glyphicon glyphicon-th"></span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 form-group">
                <label for="example-date-input"><i class="fa fa-calendar"></i> <?= $this->lang->line('Return Date'); ?></label>
                <div class="input-group">
                    <input type="text" class="form-control" id="return_date1" name="return_date" autocomplete="off">
                    <div class="input-group-addon ddAddon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
              </div>
              <div class="col-md-3 form-group">
                <label for="example-date-input">&nbsp;</label>
                <button class="btn btn-info btn-delivery btn-block booknowBtn" type="submit" id="btnFormCreate"><?=$this->lang->line('Book Now')?></button>
              </div>
            </form>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
      $('#journey_date1').datepicker({
        startDate: new Date(),
        autoclose: true,
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('#return_date1').datepicker({
            startDate: minDate,
            autoclose: true,
          });     
        });
    });

    $(".dpAddon" ).click(function( e ) { 
      $("#journey_date1").focus();
    });
    $(".ddAddon" ).click(function( e ) { 
      $("#return_date1").focus();
    });
</script>

<script>
  $("#frmBasicSearch").validate({
      ignore: [], 
      rules: {
        trip_source: { required : true },
        trip_destination: { required : true },
        journey_date1: { required : true },
        no_of_seat: { required : true, min : 0, max : 10 },
      },
      messages: {
        trip_source: { required : <?= json_encode($this->lang->line('Select starting city!')); ?> },
        trip_destination: { required : <?= json_encode($this->lang->line('Select destination city!')); ?> },
        journey_date1: { required : <?= json_encode($this->lang->line('Select journey date!')); ?> },
        no_of_seat: { required : <?= json_encode($this->lang->line('select number of seats!')); ?>, min : <?= json_encode($this->lang->line('Number of passengers cannot be zero (0).')); ?>, max : <?= json_encode($this->lang->line('The maximum number of seat is 40')); ?>, },
      }
    });
    
    $("#trip_source").on('change', function() {
        $('#trip_destination').select2('open');
    });

</script>

