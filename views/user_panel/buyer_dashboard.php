<div class="row">
    
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                  <h4><?= $this->lang->line('posted_jobs'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-portfolio fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= $total_jobs; ?></h3>
              <span class="font-bold no-margins">
                  <?= $this->lang->line('awarded_jobs'); ?>
              </span>
      
                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?= $posted_progress; ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $posted_progress; ?>" role="progressbar" class=" progress-bar progress-bar-success" title="<?= $posted_progress; ?>%">                              
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('awarded_jobs'); ?></small>
                          <h4><?= $posted_progress; ?> %</h4>
                      </div>

                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('in_market'); ?></small>
                          <h4><?= $total_inmarket_jobs; ?> %</h4>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                  <h4><?= $this->lang->line('working_jobs'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-note fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= $total_working_jobs; ?></h3>
              <span class="font-bold no-margins">
                  <?= $this->lang->line('in_progress_jobs'); ?>
              </span>
      
                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?= $working_progress; ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $working_progress; ?>" role="progressbar" class=" progress-bar progress-bar-success" title="<?= $working_progress; ?>%">                              
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('jobs_accepted'); ?></small>
                          <h4><?= $total_accepted_jobs; ?> %</h4>
                      </div>

                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('in_progress'); ?></small>
                          <h4><?= $working_progress; ?> %</h4>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
      <div class="hpanel stats">
          <div class="panel-body h-200">
              <div class="stats-title pull-left">
                  <h4> <?= $this->lang->line('completed_jobs'); ?></h4>
              </div>
              <div class="stats-icon pull-right">
                  <i class="pe-7s-like2 fa-4x"></i>
              </div>
              <div class="m-t-xl">
                  <h3 class="m-xs"><?= $total_delivered_jobs; ?></h3>
              <span class="font-bold no-margins">
                  <?= $this->lang->line('delivered_jobs'); ?>
              </span>

                  <div class="progress m-t-xs full progress-small progress-striped active">
                      <div style="width: <?= $completed_progress; ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $completed_progress; ?>" role="progressbar" class=" progress-bar progress-bar-success" title="<?= $completed_progress; ?>%">                              
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-6">
                          <small class="stats-label"><?= $this->lang->line('delivered_jobs'); ?></small>
                          <h4><?= $total_delivered; ?> %</h4>
                      </div>

                      <div class="col-xs-6 hidden">
                          <small class="stats-label"><?= $this->lang->line('total_jobs'); ?></small>
                          <h4><?= $total_jobs; ?></h4>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>      
  
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
            <h4><?= $this->lang->line('balance_summary'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
            <i class="pe-7s-cash fa-4x"></i>
        </div>
        <div class="clearfix"></div>
        <div class="flot-chart">
            <div class="flot-chart-content" id="flot-income-chart"></div>
        </div>
        <div class="m-t-xs">
          <div class="row">
            <div class="col-xs-5">
                <small class="stat-label"><?= $this->lang->line('spent'); ?></small>
                <h4>XAF <?= $this->api->convert_big_int($total_spend);?> </h4>
            </div>

            <div class="col-xs-7">
                <small class="stat-label"><?= $this->lang->line('current'); ?></small>
                <h4>XAF <?= $this->api->convert_big_int($current_balance);?></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="row">
  
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading">          
        <?= $this->lang->line('latest_posted_jobs'); ?>
      </div>
       
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <tbody>
                    <?php if(!empty($latest_jobs)):?>
                      <?php foreach ($latest_jobs as $v): ?>
                        <?php 
                          if($v['order_status'] == 'open') { $status = $this->lang->line('in_market');  } 
                          else if($v['order_status'] == 'accept') { $status = $this->lang->line('accepted');  } 
                          else if($v['order_status'] == 'in_progress') { $status = $this->lang->line('in_progress');  } 
                          else if($v['order_status'] == 'delivered') { $status = $this->lang->line('delivered');  } 
                        ?>
                        <tr>
                          <td><strong><?= $this->lang->line('order'); ?></strong><br />G-<?= $v['order_id'];?><small></small></td>
                          <td><strong><?= $this->lang->line('from_address'); ?></strong><br /><?= $v['from_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('to_address'); ?></strong><br /><?= $v['to_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('price'); ?></strong><br /><?= $v['currency_sign']. ' ' .$v['order_price']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('status'); ?></strong><br /><?= $status; ?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading">          
         <?= $this->lang->line('latest_in_progress_jobs'); ?>
      </div>
      <div class="panel-body">
          <div class="table-responsive">
              <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
                  <tbody>
                    <?php if(!empty($latest_inprogress_jobs)):?>                      
                      <?php foreach ($latest_inprogress_jobs as $v): ?>                        
                        <tr>
                          <td><strong><?= $this->lang->line('order'); ?></strong><br />G-<?= $v['order_id'];?><small></small></td>
                          <td><strong><?= $this->lang->line('from_address'); ?></strong><br /><?= $v['from_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('to_address'); ?></strong><br /><?= $v['to_address']; ?><small></small></td>
                          <td><strong><?= $this->lang->line('price'); ?></strong><br /><?= $v['currency_sign']. ' ' .$v['order_price']; ?><small></small></td>
                        </tr>
                      <?php endforeach; ?>                      
                    <?php else: ?>
                      <tr class="text-center"><td> <?= $this->lang->line('no_record_found'); ?></td></tr>
                    <?php endif; ?>
                  </tbody>
              </table>
          </div>
      </div>      
    </div>
  </div>

</div>


</div>