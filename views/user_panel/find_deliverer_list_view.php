<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></a></li>
                    <li class="active"><span><?= $this->lang->line('find_deliverers'); ?></span></li>
                </ol>
            </div>
            <a href="<?=base_url('user-panel/my-bookings');?>" class="btn btn-default btn-sm col-lg-1"> <i class="fa fa-long-arrow-left"></i> <?= $this->lang->line('back'); ?> </a>
            <h2 class="font-light m-b-xs text-left" style="line-height: 25px;"> &nbsp; &nbsp;| <?= $this->lang->line('find_deliverers'); ?> &nbsp;</h2>
        </div>
    </div>
</div>


<div class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="hpanel horange">
                <div class="panel-body">
                    <div class="m-b-md">
                        <h4> <?= $this->lang->line('filters'); ?> </h4>
                        <small> <?= $this->lang->line('Filter_deliverers_basend_on_diferent_options_below.'); ?> </small>
                    </div>
                    <form action="<?= base_url('user-panel/find-deliverer') ?>" method="post" id="dilevererFilter">
                        <input type="hidden" name="courier_order_id" value="<?= $courier_order_id; ?>" />
                        <div class="row">
                            <div class="col-sm-12">
                            <label class="control-label"><?= $this->lang->line('country'); ?> : <?= $country_name ?></label>
                                <select id="country_id_src" name="country_id_src" class="form-control select2" data-allow-clear="true" data-placeholder="Select Country">
                                    <option value="0"><?= $this->lang->line('select_country'); ?></option>
                                    <?php foreach ($countries as $country): ?>
                                        <option value="<?= $country['country_id'] ?>" <?php if(isset($country_id) && $country_id > 0 && $country['country_id'] == $country_id) { echo 'selected'; } ?> ><?= $country['country_name']; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="divstate">
                            <div class="col-sm-12">
                                <label class="control-label"><?= $this->lang->line('state'); ?>: <?= $state_name ?></label>
                                <select id="state_id_src" name="state_id_src" class="form-control select2" data-allow-clear="true" data-placeholder="Select State" readonly>
                                <option value="0"> <?= $this->lang->line('select_state'); ?> </option>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="divcity">
                            <div class="col-sm-12">
                                <label class="control-label"><?= $this->lang->line('city'); ?> : <?= $city_name ?></label>
                                <select id="city_id_src" name="city_id_src" class="form-control select2" data-allow-clear="true" data-placeholder="Select City" readonly>
                                <option value="0"> <?= $this->lang->line('select_city'); ?> </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?= $this->lang->line('ratings'); ?></label>
                            <div class="input-group">
                                <input id="rating_filter" type="number" name="rating_filter" value="<?php if(isset($rating) && $rating > 0) { echo $rating; } else { echo "0"; } ?>" min="0" max="5">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                              <input type="checkbox" name="favorite" <?php if(isset($favorite) && $favorite == 1) { echo 'checked'; } ?>/>
                              <label class="control-label">Sort by Favorite</label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-warning2 btn-block"><?= $this->lang->line('apply_filter'); ?></button>
                    </form><br />
                    <form action="<?= base_url('user-panel/find-deliverer') ?>" method="POST">
                      <input type="hidden" name="courier_order_id" value="<?= $courier_order_id; ?>" />
                      <button type="submit" class="btn btn-info btn-block"><?= $this->lang->line('clear_filter'); ?></button>
                    </form>
                </div>

            </div>
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-lg-12"> 
                    <?php if(!empty($deliverers) AND is_array($deliverers)): ?>
                        <?php if(isset($favorite) && $favorite == 1) : ?>
                            <?php $i=0; foreach ($deliverers as $d):?> 
                              <?php if(in_array($d['cust_id'], $favourite_deliverer_ids)): ?> 
                                <?php  
                                    if($d['avatar_url'] != "NULL"){ $avatar_url= base_url($d['avatar_url']);  }
                                    else { $avatar_url = $this->config->item('resource_url') . 'default-profile.jpg'; }
                                    
                                    $country_name = $this->user->get_country_name_by_id($d['country_id']); 
                                    $state_name = $this->user->get_state_name_by_id($d['state_id']);
                                    $city_name = $this->user->get_city_name_by_id($d['city_id']);
                                ?>
                                <div class="col-lg-4">
                                    <div class="hpanel <?=($i%2==0)?'hgreen':'hyellow';?> contact-panel">
                                        <div class="panel-body" style="min-height:315px; max-height:315px; ">
                                            <span class="pull-right">
                                            <?php if(in_array($d['cust_id'], $favourite_deliverer_ids)): ?> 
                                                <button title="Remove from favorite" class="btn btn-danger btn-circle remove_favorite" data-id="<?= $d['cust_id'];?>"><i class="fa fa-heart"></i></button>&nbsp;
                                            <?php else: ?> 
                                                <button title="Add to favorite" class="btn btn-warning btn-circle add_favorite" data-id="<?= $d['cust_id'];?>"><i class="fa fa-heart"></i></button>&nbsp;
                                            <?php endif; ?>                                            
                                                <form action="<?= base_url('user-panel/deliverer-details');?>" method="post" class="pull-right">
                                                    <input type="hidden" name="deliverer_id" value="<?= $d['cust_id'];?>" />
                                                    <input type="hidden" name="courier_order_id" value="<?= $courier_order_id; ?>" />
                                                    <input type="hidden" name="requested_deliverer" value="send_request" />
                                                    <input type="hidden" name="redirect" value="user-panel/find-deliverer" />
                                                    <button title="Profile View" class="btn btn-info btn-circle"><i class="fa fa-eye"></i></button>
                                                </form>
                                            </span>
                                            <img alt="logo" class="img-thumbnail m-b" src="<?= $avatar_url; ?>" />
                                            <h3><a><?= strtoupper($d['firstname']. ' '.$d['lastname']); ?></a></h3>
                                            <div class="text-muted font-bold m-b-xs"><?= $city_name . ', '. $country_name; ?></div>
                                            <div class="text-muted font-bold m-b-xs"></div>
                                            <p>
                                                <?php if($d['company_name'] !="NULL"): ?>
                                                    <?= $this->lang->line('company'); ?>:  <?= strtoupper($d['company_name']); ?> <br />
                                                <?php else: ?>
                                                    <?= $this->lang->line('company'); ?>: <?= $this->lang->line('not_provided'); ?> <br />
                                                <?php endif; ?>
                                                <?= $this->lang->line('address'); ?> : <?= $d['address']; ?> <br />
                                                <?= $city_name.', '. $state_name .', '.$country_name; ?>
                                            </p>  <hr/>
                                            <p>
                                                <?= $this->lang->line('rating'); ?> : 
                                                <?php for($i=0; $i<(int)$d['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                                                <?php for($i=0; $i<(5-(int) $d['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                                                ( <?= (int) $d['ratings'] ?> / 5 )
                                            </p>                          
                                        </div>
                                        <div class="panel-footer text-center">
                                            <button title="send Request" class="btn btn-info send_request" data-delivererid="<?= $d['cust_id'];?>"  data-orderid="<?= $courier_order_id;?>"><i class="fa fa-send"></i> <?= $this->lang->line('send_request'); ?></button> 
                                        </div>
                                    </div>
                                </div>
                            <?php endif; $i++; endforeach; ?>

                            <?php $i=0; foreach ($deliverers as $d):?> 
                              <?php if(!in_array($d['cust_id'], $favourite_deliverer_ids)): ?> 
                                <?php  
                                    if($d['avatar_url'] != "NULL"){ $avatar_url= base_url($d['avatar_url']);  }
                                    else { $avatar_url = $this->config->item('resource_url') . 'default-profile.jpg'; }
                                    
                                    $country_name = $this->user->get_country_name_by_id($d['country_id']); 
                                    $state_name = $this->user->get_state_name_by_id($d['state_id']);
                                    $city_name = $this->user->get_city_name_by_id($d['city_id']);
                                ?>
                                <div class="col-lg-4">
                                    <div class="hpanel <?=($i%2==0)?'hgreen':'hyellow';?> contact-panel">
                                        <div class="panel-body" style="min-height:315px; max-height:315px; ">
                                            <span class="pull-right">
                                            <?php if(in_array($d['cust_id'], $favourite_deliverer_ids)): ?> 
                                                <button title="Remove from favorite" class="btn btn-danger btn-circle remove_favorite" data-id="<?= $d['cust_id'];?>"><i class="fa fa-heart"></i></button>&nbsp;
                                            <?php else: ?> 
                                                <button title="Add to favorite" class="btn btn-warning btn-circle add_favorite" data-id="<?= $d['cust_id'];?>"><i class="fa fa-heart"></i></button>&nbsp;
                                            <?php endif; ?>                                            
                                                <form action="<?= base_url('user-panel/deliverer-details');?>" method="post" class="pull-right">
                                                    <input type="hidden" name="deliverer_id" value="<?= $d['cust_id'];?>" />
                                                    <input type="hidden" name="courier_order_id" value="<?= $courier_order_id; ?>" />
                                                    <input type="hidden" name="requested_deliverer" value="send_request" />
                                                    <input type="hidden" name="redirect" value="user-panel/find-deliverer" />
                                                    <button title="Profile View" class="btn btn-info btn-circle"><i class="fa fa-eye"></i></button>
                                                </form>
                                            </span>
                                            <img alt="logo" class="img-thumbnail m-b" src="<?= $avatar_url; ?>" />
                                            <h3><a><?= strtoupper($d['firstname']. ' '.$d['lastname']); ?></a></h3>
                                            <div class="text-muted font-bold m-b-xs"><?= $city_name . ', '. $country_name; ?></div>
                                            <div class="text-muted font-bold m-b-xs"></div>
                                            <p>
                                                <?php if($d['company_name'] !="NULL"): ?>
                                                    <?= $this->lang->line('company'); ?>:  <?= strtoupper($d['company_name']); ?> <br />
                                                <?php else: ?>
                                                    <?= $this->lang->line('company'); ?>: <?= $this->lang->line('not_provided'); ?> <br />
                                                <?php endif; ?>
                                                <?= $this->lang->line('address'); ?> : <?= $d['address']; ?> <br />
                                                <?= $city_name.', '. $state_name .', '.$country_name; ?>
                                            </p>  <hr/>
                                            <p>
                                                <?= $this->lang->line('rating'); ?> : 
                                                <?php for($i=0; $i<(int)$d['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                                                <?php for($i=0; $i<(5-(int) $d['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                                                ( <?= (int) $d['ratings'] ?> / 5 )
                                            </p>                          
                                        </div>
                                        <div class="panel-footer text-center">
                                            <button title="send Request" class="btn btn-info send_request" data-delivererid="<?= $d['cust_id'];?>"  data-orderid="<?= $courier_order_id;?>"><i class="fa fa-send"></i> <?= $this->lang->line('send_request'); ?></button> 
                                        </div>
                                    </div>
                                </div>
                            <?php endif; $i++; endforeach; ?>
                        <?php else: ?>
                            <?php $i=0; foreach ($deliverers as $d):?> 
                                <?php  
                                    if($d['avatar_url'] != "NULL"){ $avatar_url= base_url($d['avatar_url']);  }
                                    else { $avatar_url = $this->config->item('resource_url') . 'default-profile.jpg'; }
                                    
                                    $country_name = $this->user->get_country_name_by_id($d['country_id']); 
                                    $state_name = $this->user->get_state_name_by_id($d['state_id']);
                                    $city_name = $this->user->get_city_name_by_id($d['city_id']);
                                ?>
                                <div class="col-lg-4">
                                    <div class="hpanel <?=($i%2==0)?'hgreen':'hyellow';?> contact-panel">
                                        <div class="panel-body" style="min-height:315px; max-height:315px; ">
                                            <span class="pull-right">
                                            <?php if(in_array($d['cust_id'], $favourite_deliverer_ids)): ?> 
                                                <button title="Remove from favorite" class="btn btn-danger btn-circle remove_favorite" data-id="<?= $d['cust_id'];?>"><i class="fa fa-heart"></i></button>&nbsp;
                                            <?php else: ?> 
                                                <button title="Add to favorite" class="btn btn-warning btn-circle add_favorite" data-id="<?= $d['cust_id'];?>"><i class="fa fa-heart"></i></button>&nbsp;
                                            <?php endif; ?>                                            
                                                <form action="<?= base_url('user-panel/deliverer-details');?>" method="post" class="pull-right">
                                                    <input type="hidden" name="deliverer_id" value="<?= $d['cust_id'];?>" />
                                                    <input type="hidden" name="courier_order_id" value="<?= $courier_order_id; ?>" />
                                                    <input type="hidden" name="requested_deliverer" value="send_request" />
                                                    <input type="hidden" name="redirect" value="user-panel/find-deliverer" />
                                                    <button title="Profile View" class="btn btn-info btn-circle"><i class="fa fa-eye"></i></button>
                                                </form>
                                            </span>
                                            <img alt="logo" class="img-thumbnail m-b" src="<?= $avatar_url; ?>" />
                                            <h3><a><?= strtoupper($d['firstname']. ' '.$d['lastname']); ?></a></h3>
                                            <div class="text-muted font-bold m-b-xs"><?= $city_name . ', '. $country_name; ?></div>
                                            <div class="text-muted font-bold m-b-xs"></div>
                                            <p>
                                                <?php if($d['company_name'] !="NULL"): ?>
                                                    <?= $this->lang->line('company'); ?>:  <?= strtoupper($d['company_name']); ?> <br />
                                                <?php else: ?>
                                                    <?= $this->lang->line('company'); ?>: <?= $this->lang->line('not_provided'); ?> <br />
                                                <?php endif; ?>
                                                <?= $this->lang->line('address'); ?> : <?= $d['address']; ?> <br />
                                                <?= $city_name.', '. $state_name .', '.$country_name; ?>
                                            </p>  <hr/>
                                            <p>
                                                <?= $this->lang->line('rating'); ?> : 
                                                <?php for($i=0; $i<(int)$d['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                                                <?php for($i=0; $i<(5-(int) $d['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                                                ( <?= (int) $d['ratings'] ?> / 5 )
                                            </p>                          
                                        </div>
                                        <div class="panel-footer text-center">
                                            <button title="send Request" class="btn btn-info send_request" data-delivererid="<?= $d['cust_id'];?>"  data-orderid="<?= $courier_order_id;?>"><i class="fa fa-send"></i> <?= $this->lang->line('send_request'); ?></button> 
                                        </div>
                                    </div>
                                </div>
                            <?php $i++; endforeach; ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <div class="hpanel hgreen contact-panel">
                            <div class="panel-body"> 
                                <h5 class="text-center"><?= $this->lang->line('no_required_found'); ?></h5>
                            </div>  
                        </div>  
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  $("#country_id_src").on('change', function(event) {  event.preventDefault();
    var country_id = $.trim($(this).val());

    $('#city_id_src').attr('disabled', true);
    $('#state_id_src').empty();

    if(country_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-state-by-country-id", 
        data: { country_id: country_id },
        dataType: "json",
        success: function(res){ 
          $('#state_id_src').attr('disabled', false);
          $('#state_id_src').empty(); 
          $('#state_id_src').append('<option value=""><?= json_encode($this->lang->line('select_state')); ?></option>');
          $.each( res, function(){$('#state_id_src').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
          $('#state_id_src').focus();
        },
        beforeSend: function(){
          $('#state_id_src').empty();
          $('#state_id_src').append('<option value=""><?= json_encode($this->lang->line('loading')); ?></option>');
        },
        error: function(){
          $('#state_id_src').attr('disabled', true);
          $('#state_id_src').empty();
          $('#state_id_src').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?></option>');
        }
      });
    } else { 
      $('#state_id_src').empty(); 
      $('#state_id_src').append('<option value=""><?= json_encode($this->lang->line('select_state')); ?></option>');
      $('#state_id_src, #city_id_src').attr('disabled', true); 
    }

  });

  $("#state_id_src").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    if(state_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-cities-by-state-id", 
        data: { state_id: state_id },
        dataType: "json",
        success: function(res){ 
          $('#city_id_src').attr('disabled', false);
          $('#city_id_src').empty(); 
          $('#city_id_src').append('<option value=""><?= json_encode($this->lang->line('select_city')); ?></option>');
          $.each( res, function(){ $('#city_id_src').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
          $('#city_id_src').focus();
        },
        beforeSend: function(){
          $('#city_id_src').empty();
          $('#city_id_src').append('<option value=""><?= json_encode($this->lang->line('loading')); ?></option>');
        },
        error: function(){
          $('#city_id_src').attr('disabled', true);
          $('#city_id_src').empty();
          $('#city_id_src').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?></option>');
        }
      });
    } else { 
      $('#city_id_src').empty(); 
      $('#city_id_src').append('<option value=""><?= json_encode($this->lang->line('select_city')); ?></option>');
      $('#city_id_src').attr('disabled', true); 
    }

  });


    $(".remove_favorite").on('click', function(event) {   event.preventDefault();
        var id = $(this).data('id');
        swal({                
          title: <?= json_encode($this->lang->line('are_you_sure')); ?>,                
          text: "",
          type: "warning",                
          showCancelButton: true,                
          confirmButtonColor: "#DD6B55",                
          confirmButtonText: <?= json_encode($this->lang->line('yes')); ?>,                
          cancelButtonText: <?= json_encode($this->lang->line('no')); ?>,                
          closeOnConfirm: false,                
          closeOnCancel: false, 
        },                
        function (isConfirm) {                    
          if (isConfirm) {                        
            $.post('remove-deliverer-from-favourite', {id: id}, 
            function(res){
              //console.log(res);
              if($.trim(res) == "success") {
                 swal(<?= json_encode($this->lang->line('success')); ?>, "", "success");                           
                setTimeout(function() { window.location.reload(); }, 2000); 
              } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, "", "error");   }  
            }); 
          } else {  swal(<?= json_encode($this->lang->line('canceled')); ?>, "", "error");  }  
        });       
    });

    $(".add_favorite").on('click', function(event) {   event.preventDefault();
        var id = $(this).data('id');        
        swal({                
          title: <?= json_encode($this->lang->line('are_you_sure')); ?>,
          text: "",                
          type: "warning",                
          showCancelButton: true,                
          confirmButtonColor: "#DD6B55",                
          confirmButtonText: <?= json_encode($this->lang->line('yes')); ?>,                
          cancelButtonText: <?= json_encode($this->lang->line('no')); ?>,                
          closeOnConfirm: false,                
          closeOnCancel: false, 
        },                
        function (isConfirm) {                    
          if (isConfirm) {                        
            $.post('make-deliverer-favourite', {id: id}, 
            function(res){
                //console.log(res);
              if($.trim(res) == "success") {
                swal(<?= json_encode($this->lang->line('success')); ?>, "", "success");                            
                setTimeout(function() { window.location.reload(); }, 2000); 
              } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, "", "error");  }  
            }); 
          } else {  swal(<?= json_encode($this->lang->line('canceled')); ?>, "", "error");  }  
        });       
    });

    $(".send_request").on('click', function(event) {   event.preventDefault();
        var delivererid = $(this).data('delivererid');        
        var courier_order_id = $(this).data('orderid');        
        swal({                
          title: <?= json_encode($this->lang->line('are_you_sure')); ?>,
          text: "",                
          type: "warning",                
          showCancelButton: true,                
          confirmButtonColor: "#DD6B55",                
          confirmButtonText: <?= json_encode($this->lang->line('yes')); ?>,                
          cancelButtonText: <?= json_encode($this->lang->line('no')); ?>,                
          closeOnConfirm: false,                
          closeOnCancel: false, 
        },                
        function (isConfirm) {                    
          if (isConfirm) {                        
            $.post('send-request-to-deliverer', {deliverer_id: delivererid, courier_order_id: courier_order_id }, 
            function(res){
                //console.log(res);
              if($.trim(res) == "success") {
                swal(<?= json_encode($this->lang->line('success')); ?>, "", "success");                            
                setTimeout(function() { window.location.reload(); }, 2000); 
              } else if(res == "exists") {
                swal(<?= json_encode($this->lang->line('failed')); ?>, "", "warning");                            
                setTimeout(function() { window.location.reload(); }, 2000); 
              }
              else { swal(<?= json_encode($this->lang->line('failed')); ?>, "", "error");  }  
            }); 
          } else {  swal(<?= json_encode($this->lang->line('canceled')); ?>, "", "error");  }  
        });       
    });

</script>