<style> textarea { resize: none; } </style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('address_book'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('add_new_address'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-address-book fa-2x text-muted"></i> <?= $this->lang->line('add_address'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('add_new_address_in_book'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel-laundry/add-address-book'); ?>" method="post" class="form-horizontal" id="addAddress" enctype="multipart/form-data">        
          <div class="panel-body">              
            <div class="col-lg-12">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>

              <div class="row">
                <div class="form-group">

                  <div class="col-lg-5">
                    <div class="col-lg-12">
                      <label><?= $this->lang->line('search_address_in_map_booking'); ?></label>
                      <input type="text" name="toMapID" class="form-control" id="toMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>">
                    </div>
                    <div class="col-lg-12">
                      <div id="map" class="map_canvas" style="width: 100%; height: 200px;"></div>
                    </div>
                  </div>

                  <div class="col-lg-7" style="padding-left: 0px;">
                    
                    <div class="col-lg-12">
                      <label class=""><?= $this->lang->line('address_line_1'); ?></label>
                      <input type="text" id="address1" name="address1" class="form-control" placeholder="<?= $this->lang->line('address_line_1'); ?>" autocomplete="off" />
                    </div>
                    <div class="col-lg-3">
                      <label>Address Type</label>
                      <select id="address_type" name="address_type" class="form-control select2" data-allow-clear="true">
                        <option value="Individual"><?= $this->lang->line('individual'); ?></option></option>
                        <option value="Commercial"><?= $this->lang->line('commercial'); ?></option>
                      </select>
                    </div>
                    <div class="col-lg-9">
                      <label><?= $this->lang->line('compnay_name'); ?></label>
                      <input type="text" class="form-control" id="company" name="company" placeholder="<?= $this->lang->line('enter_company_name'); ?>" readonly />
                    </div>
                    <div class="col-lg-6">
                      <label class=""><?= $this->lang->line('contact_first_name'); ?></label>
                      <input type="text" id="firstname" name="firstname" class="form-control" placeholder="<?= $this->lang->line('enter_first_name'); ?>" />
                    </div>
                    <div class="col-lg-6">
                      <label class=""><?= $this->lang->line('contact_last_name'); ?></label>
                      <input type="text" name="lastname" class="form-control" id="lastname" placeholder="<?= $this->lang->line('enter_last_name'); ?>" />
                    </div>
                    <div class="col-lg-6">
                      <label><?= $this->lang->line('email_address'); ?></label>
                      <input type="email" class="form-control" id="email_id" name="email_id" placeholder="<?= $this->lang->line('enter_email_address'); ?>" />
                    </div>
                    <div class="col-lg-6">
                      <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                      <input type="text" id="mobile" name="mobile" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number'); ?>" />
                    </div>
                  </div>

                </div>
              </div>

              <div class="row">
                <div class="form-group">
                  
                  <div class="col-lg-12">
                    <div class="col-lg-4">
                      <label class=""><?= $this->lang->line('pickup_instructions'); ?></label>
                      <input type="text" name="pickup_instruction" id="pickup_instruction" class="form-control" placeholder="<?= $this->lang->line('pickup_instructions'); ?>">
                      
                    </div>
                    <div class="col-lg-4">
                      <label class=""><?= $this->lang->line('deliver_instructions'); ?></label>
                      <input type="text" name="deliver_instruction" id="deliver_instruction" class="form-control" placeholder="<?= $this->lang->line('deliver_instructions'); ?>">
                    </div>
                    <div class="col-lg-4">
                      <label><?= $this->lang->line('photo'); ?></label>
                      <label><?= $this->lang->line('change_address_photo_optional'); ?></label>
                      <div class="">
                        <div class="input-group image-preview">
                          <input type="text" class="form-control image-preview-filename" disabled="disabled" />
                          <span class="input-group-btn">
                            <!-- image-preview-clear button -->
                            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                              <span class="glyphicon glyphicon-remove"></span> <?= $this->lang->line('clear'); ?>
                            </button>
                            <!-- image-preview-input -->
                            <div class="btn btn-default image-preview-input">
                              <span class="glyphicon glyphicon-folder-open"></span>
                              <span class="image-preview-input-title"><?= $this->lang->line('browse'); ?></span>
                              <input type="file" accept="image/png, image/jpeg, image/gif" name="address_image"/> <!-- rename it -->
                            </div>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

              <input type="hidden" to-data-geo="lat" name="latitude" id="latitude" value="" />
              <input type="hidden" to-data-geo="lng" name="longitude" id="longitude" value="" />
              <input type="hidden" to-data-geo="formatted_address" id="street" name="street" class="form-control" placeholder="<?= $this->lang->line('street_name'); ?>" />
              <input type="hidden" to-data-geo="postal_code" id="zipcode" name="zipcode" class="form-control" placeholder="<?= $this->lang->line('zip_code'); ?>"  />
              <input type="hidden" to-data-geo="country" id="country" name="country" class="form-control" placeholder="<?= $this->lang->line('country'); ?>" readonly/>
              <input type="hidden" to-data-geo="administrative_area_level_1" id="state" name="state" class="form-control" id="tlaceholder="<?= $this->lang->line('state'); ?>" readonly/>
              <input type="hidden" to-data-geo="locality" id="city" name="city" class="form-control" placeholder="<?= $this->lang->line('city'); ?>" readonly/>
                 
            </div>
          </div>
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-lg-6 text-left">
                  <a href="<?= base_url('user-panel-laundry/address-book-list'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_address_book'); ?></a>
               </div>
               <div class="col-lg-6 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('save_address'); ?></button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  $("#toMapID").geocomplete({
    map:".map_canvas",
    location: "",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: false, },
    details: "form",
    detailsAttribute: "to-data-geo", 
    types: ["geocode", "establishment"],
  });
</script>
<script>
  $(function() {  
    $("#address_type").on('change', function(event) {  event.preventDefault();
        var type = $(this).val();
        if(type=="Commercial") { 
            $("#company").prop('readonly', false);
            $("#addAddress").validate();
            $("#company").rules("add", "required");
        } else { 
            $("#company").val('').prop('readonly', true);
            $("#addAddress").validate();
            $("#company").rules("remove", "required");
        }
    });

    $("#addAddress").keypress(function(event) { return event.keyCode != 13; });
    
    $("#addAddress").validate({
      ignore: [],
      rules: {
        firstname: { required: true, },      
        lastname: { required: true, },
        mobile: { required: true, number: true, minlength: 8, maxlength: 16, },      
        email_id: { required: true, email: true, },      
        toMapID: { required: true, },      
        street: { required: true, },      
        address1: { required: true, },      
        pickup_instruction: { required: true, },      
        deliver_instruction: { required: true, },      
        address_image: { accept:"jpg,png,jpeg,gif" },
      }, 
      messages: {
        firstname: { required: <?= json_encode($this->lang->line('enter_first_name')); ?>,   },
        lastname: { required: <?= json_encode($this->lang->line('enter_last_name')); ?>,   },
        company: { required: <?= json_encode($this->lang->line('enter_company_name')); ?>,   },
        toMapID: { required: <?= json_encode($this->lang->line('search_address_in_map_booking')); ?>,   },
        mobile: { required: <?= json_encode($this->lang->line('enter_mobile_number')); ?>, number: <?= json_encode($this->lang->line('number_only')); ?>, minlength: <?= json_encode($this->lang->line('6_characters')); ?>, maxlength: <?= json_encode($this->lang->line('max_16_characters')); ?>,  },
        email_id: { required: <?= json_encode($this->lang->line('enter_email_address')); ?>, email: <?= json_encode($this->lang->line('invalid_email')); ?>,  },
        street: { required: <?= json_encode($this->lang->line('street_name')); ?>,   },
        address1: { required: <?= json_encode($this->lang->line('enter_address')); ?>,   },
        pickup_instruction: { required: <?= json_encode($this->lang->line('pickup_instructions')); ?>,   },
        deliver_instruction: { required: <?= json_encode($this->lang->line('deliver_instructions')); ?>,   },
        address_image: { accept: <?= json_encode($this->lang->line('only_image_allowed')); ?>,   },
      }
    });

    $("#upload_image").on('click', function(e) { e.preventDefault(); $("#address_image").trigger('click'); });

  });

  function addr_image_name(e){ if(e.target.files[0].name !="") { $("#addr_image_name").removeClass('hidden'); }}


</script>


