<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                    <li class="active"><span><?= $this->lang->line('Closed Claims'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-exclamation-triangle fa-2x text-muted"></i> <?= $this->lang->line('Closed Claims'); ?> &nbsp;&nbsp;&nbsp;
                <a class="btn btn-outline btn-info" href="<?=base_url('user-panel-laundry/provider-laundry-claims'); ?>"><i class="fa fa-check"></i> <?= $this->lang->line('Open Claims'); ?></a>
            </h2>
            <small class="m-t-md"><?= $this->lang->line('Claims closed by provider'); ?></small>
        </div>
    </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <div class="panel-body">
            
          <?php if($this->session->flashdata('error')):  ?>
              <div class="row">
                  <div class="form-group"> 
                      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
              </div>
          <?php endif; ?>
          <?php if($this->session->flashdata('success')):  ?>
              <div class="row">
                  <div class="form-group"> 
                      <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
              </div>
          <?php endif; ?>

          <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
            <thead>
              <tr>
                <th><?= $this->lang->line('Claim ID'); ?></th>
                <th><?= $this->lang->line('Booking ID'); ?></th>
                <th><?= $this->lang->line('Booking Date'); ?></th>
                <th><?= $this->lang->line('category'); ?></th>
                <th><?= $this->lang->line('status'); ?></th>
                <th style="text-align: center;"><?= $this->lang->line('action'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($claim_list as $claim) { ?>
                <tr>
                  <td><?= $claim['claim_id'] ?></td>
                  <td><?= $claim['service_id'] ?></td>
                  <td><?= $claim['cre_datetime'] ?></td>
                  <td><?= $claim['claim_type'] ?></td>
                  <td><?= strtoupper($claim['claim_status']) ?></td>
                  <td style="text-align: center; display: inline-flex;">
                    <form action="<?=base_url('user-panel-laundry/provider-closed-claim-details')?>" method="POST">
                      <input type="hidden" name="claim_id" value="<?= $claim['claim_id'] ?>">
                      <input type="hidden" name="service_id" value="<?= $claim['service_id'] ?>">
                      <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-eye"></i> <?= $this->lang->line('view_details'); ?></button>
                    </form>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>