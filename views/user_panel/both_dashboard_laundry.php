<div class="row">
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel">
      <div class="panel-body h-200" style="padding: 8px;">
        <h4 class="font-extra-bold text-center no-margins text-success">
          <?= $this->lang->line('overall ratings'); ?>
        </h4>
        <br/>
        <div class="col-md-12" style="padding: 8px;">
          <div class="col-md-8 text-left" style="padding: 0px;">
            <?php for($i=0; $i<(int)$cust['ratings']; $i++){ echo ' <i class="fa fa-star fa-2x"></i> '; } ?>
            <?php for($i=0; $i<(5-$cust['ratings']); $i++){ echo ' <i class="fa fa-star-o fa-2x"></i> '; } ?>     
          </div>
          <div class="col-md-4 text-right" style="padding: 0px;">
            <h1 style="font-size: 18px; margin-top: 2px;">( <?= $cust['ratings'] ?> / 5 )</h1> 
          </div>
        </div>
        <div class="col-md-12" style="padding: 8px;"> 
          <div class="col-md-6 text-left" style="padding: 0px;">
            <label><?=$this->lang->line('Laundry Ratings')?></label>
          </div>
          <div class="col-md-6 text-right" style="padding: 0px;">
            <h1 style="font-size: 18px; margin-top: 2px;">( <?=($laundry_rating['ratings']!=null)?$laundry_rating['ratings']:0?> / 5 )</h1> 
          </div>       
        </div>
        <div class="col-md-12" style="padding: 8px;">
          <div class="col-md-6 text-left" style="padding: 0px;">
            <label><?=$this->lang->line('Provider Ratings')?></label>
          </div>
          <div class="col-md-6 text-right" style="padding: 0px;">
            <h1 style="font-size: 18px; margin-top: 2px;">( <?=($deliverer_ratings['ratings'  ]!=null)?$deliverer_ratings['ratings']:0?> / 5 )</h1> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
          <h4> <?= $this->lang->line('Total Orders'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
          <i class="pe-7s-portfolio fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h3 class="m-xs"><?=sizeof($total_orders)?></h3>
          <span class="font-bold no-margins">
            <?= $this->lang->line('completed trips'); ?>
          </span>
          <?php (sizeof($completed)!=0 || sizeof($total_orders)!=0)?$per=round((sizeof($completed)/sizeof($total_orders))*100,2):$per=0;?>
          <div class="progress m-t-xs full progress-small progress-striped active">
            <div style="width:<?=$per?> %" aria-valuemax="100" aria-valuemin="0" aria-valuenow="1" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?> %">                              
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('completed trips'); ?></small>
              <h4><?=sizeof($completed)?></h4>
            </div>
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
              <h4><?=$per?>%</h4>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
           <h4> <?= $this->lang->line('Completed Orders'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
            <i class="pe-7s-note fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h3 class="m-xs"><?=sizeof($completed)?></h3>
          <span class="font-bold no-margins">
            <?= $this->lang->line('In-progress Orders'); ?>
          </span>
          <?php (sizeof($completed)>0 && sizeof($in_progress)>0)?$per=round((sizeof($completed)/sizeof($in_progress))*100,2):$per=0;?>  
          <div class="progress m-t-xs full progress-small progress-striped active">
            <div style="width: <?=$per?> %" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?=$per?> %" role="progressbar" class=" progress-bar progress-bar-success" title="<?=$per?> %"></div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('in_progress'); ?></small>
              <h4><?= sizeof($in_progress); ?></h4>
            </div>
            <div class="col-xs-6">
              <small class="stats-label"><?= $this->lang->line('percentage'); ?></small>
              <h4><?= $per; ?> %</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    <div class="hpanel stats">
      <div class="panel-body h-200">
        <div class="stats-title pull-left">
          <h4><?= $this->lang->line('balance_summary'); ?></h4>
        </div>
        <div class="stats-icon pull-right">
          <i class="pe-7s-cash fa-4x"></i>
        </div>
        <div class="m-t-xl">
          <h4 class="m-xs" style="margin-top: -15px;">XAF <?= $current_balance; ?></h4>
          <span class="font-bold no-margins">
            <?= $this->lang->line('current_balance'); ?>
          </span>
        </div>
        <div class="m-t-xs">
          <div class="row" style="margin-top: 0px;">
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('earned'); ?></small>
              <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_earned);?> </h4>
            </div>
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('spent'); ?></small>
              <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_spend);?></h4>
            </div>
          </div>
        </div>
        <div class="m-t-xs" style="margin-top: 0px;">
          <div class="row">
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('Laundry Earning'); ?></small>
              <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_earned_service);?> </h4>
            </div>
            <div class="col-xs-6">
              <small class="stat-label"><?= $this->lang->line('Laundry Spend'); ?></small>
              <h4 style="margin-top: 0px">XAF <?= $this->api->convert_big_int($total_spend_service);?></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">  
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
        <?= $this->lang->line('Accepted Orders'); ?>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
            <thead>
              <th><strong><?= $this->lang->line('ID'); ?></strong></th>
              <th><strong><?= $this->lang->line('Customer'); ?></strong></th>
              <th><strong><?= $this->lang->line('Items'); ?></strong></th>
              <th><strong><?= $this->lang->line('Price'); ?></strong></th>
            </thead>
            <tbody>
              <?php if(!empty($accepted)): $i=0; ?>                      
                <?php foreach ($accepted as $v): $i++; if($i<=5): ?>                        
                  <tr>
                    <td>G-<?= $v['booking_id'];?></td>
                    <td><?= $v['cust_name']; ?></td>
                    <td><?= $v['item_count']; ?></td>
                    <td><?= $v['total_price'];?></td>
                  </tr>
                <?php endif; endforeach; ?>                      
              <?php else: ?>
                <tr class="text-center"><td colspan="4"> <?= $this->lang->line('no_record_found'); ?></td></tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>      
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
        <?= $this->lang->line('In-progress Orders'); ?>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
            <thead>
              <th><strong><?= $this->lang->line('ID'); ?></strong></th>
              <th><strong><?= $this->lang->line('Customer'); ?></strong></th>
              <th><strong><?= $this->lang->line('Items'); ?></strong></th>
              <th><strong><?= $this->lang->line('Price'); ?></strong></th>
            </thead>
            <tbody>
              <?php if(!empty($in_progress)): $i=0; ?>                      
                <?php foreach ($in_progress as $v): $i++; if($i<=5): ?>                        
                  <tr>
                    <td>G-<?= $v['booking_id'];?></td>
                    <td><?= $v['cust_name']; ?></td>
                    <td><?= $v['item_count']; ?></td>
                    <td><?= $v['total_price'];?></td>
                  </tr>
                <?php endif; endforeach; ?>                      
              <?php else: ?>
                <tr class="text-center"><td colspan="4"> <?= $this->lang->line('no_record_found'); ?></td></tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div> 
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
         <?= $this->lang->line('Completed Orders'); ?>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
            <thead>
              <th><strong><?= $this->lang->line('ID'); ?></strong></th>
              <th><strong><?= $this->lang->line('Customer'); ?></strong></th>
              <th><strong><?= $this->lang->line('Items'); ?></strong></th>
              <th><strong><?= $this->lang->line('Price'); ?></strong></th>
            </thead>
            <tbody>
              <?php if(!empty($completed)): $i=0; ?>                      
                <?php foreach ($completed as $v): $i++; if($i<=5): ?>                        
                  <tr>
                    <td>G-<?= $v['booking_id'];?></td>
                    <td><?= $v['cust_name']; ?></td>
                    <td><?= $v['item_count']; ?></td>
                    <td><?= $v['total_price'];?></td>
                  </tr>
                <?php endif; endforeach; ?>                      
              <?php else: ?>
                <tr class="text-center"><td colspan="4"> <?= $this->lang->line('no_record_found'); ?></td></tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>     
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
    <div class="hpanel stats">
      <div class="panel-heading" style="margin-top: -15px;">          
         <?= $this->lang->line('Cancelled Orders'); ?>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table cellpadding="1" cellspacing="1" class="table table-condensed table-striped">
            <thead>
              <th><strong><?= $this->lang->line('ID'); ?></strong></th>
              <th><strong><?= $this->lang->line('Customer'); ?></strong></th>
              <th><strong><?= $this->lang->line('Items'); ?></strong></th>
              <th><strong><?= $this->lang->line('Price'); ?></strong></th>
            </thead>
            <tbody>
              <?php if(!empty($cancelled)): $i=0; ?>                      
                <?php foreach ($cancelled as $v): $i++; if($i<=5): ?>                        
                  <tr>
                    <td>G-<?= $v['booking_id'];?></td>
                    <td><?= $v['cust_name']; ?></td>
                    <td><?= $v['item_count']; ?></td>
                    <td><?= $v['total_price'];?></td>
                  </tr>
                <?php endif; endforeach; ?>                      
              <?php else: ?>
                <tr class="text-center"><td colspan="4"> <?= $this->lang->line('no_record_found'); ?></td></tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>     
    </div>
  </div>  
</div>