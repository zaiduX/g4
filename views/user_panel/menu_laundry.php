<?php  $email_cut = explode('@', $cust['email1']);  $name = $email_cut[0];  ?>
<style> .avatar { width: 85%; } </style>
<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <?php $url = parse_url($cust['avatar_url']);?>
        <div class="profile-picture">
            <a>
                <?php if($cust['avatar_url'] == "NULL"): ?>
                    <a href="<?= base_url('user-panel-laundry/user-profile'); ?>"><img src="<?= base_url('resources/default-profile.jpg'); ?>" class="img-thumbnail m-b avatar" alt="avatar" /></a>
                <?php else: ?>
                    <a href="<?= base_url('user-panel-laundry/user-profile'); ?>"><img src="<?= ( isset($url['scheme']) AND ($url['scheme'] == "https" || $url['scheme'] == "http" )) ? $cust['avatar_url'] : base_url($cust['avatar_url']); ?>" class="img-thumbnail m-b avatar" alt="avatar" /></a>
                <?php endif; ?>
            </a>

            <div class="stats-label text-color">
            <?php if( $cust['firstname'] != "NULL" || $cust['lastname'] != "NULL") : ?>
                <span class="font-extra-bold font-uppercase"><?= $cust['firstname'] . ' ' .$cust['lastname']; ?></span>
            <?php else: ?>
                <span class="font-extra-bold font-uppercase"><?= $name; ?></span>
            <?php endif; ?>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <small class="text-muted"><?= $this->lang->line('profile'); ?><b class="caret"></b></small>                      
                    </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li><a href="<?= base_url('user-panel-laundry/user-profile'); ?>"><?= $this->lang->line('edit_profile'); ?></a></li>
                        <li><a href="<?= base_url('user-panel-laundry/change-password'); ?>"><?= $this->lang->line('change_password'); ?></a></li>
                        <li class="divider"></li>
                        <li><a href="<?= base_url('log-out'); ?>"><?= $this->lang->line('logout'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav" id="side-menu">
            <?php 
                $method_name = $this->router->fetch_method(); 
                
                $dashboard = array('dashboard_laundry');
                $dashboard_users = array('dashboard_laundry_users');
                $address_book = array('address_book_list','add_address_book_view','edit_address_book_view');
                $basic_info = array('provider_profile','edit_provider_profile','edit_provider_document');
                $business_photo = array('business_photos');
                $manage_bus = array('bus_list','bus_add','bus_edit');
                $laundry_charges = array('laundry_charges_list');
                $laundry_special_charges = array('laundry_special_charges_list');
                $laundry_cancellation_charges = array('laundry_provider_cancellation_charges_list');
                
                //buyer Bookings
                $customer_create_booking = array('booking_laundry_ui', 'laundry_booking_payment_by_cutomer');
                $customer_accepted_booking = array('customer_accepted_laundry_bookings');
                $customer_in_progress_booking = array('customer_in_progress_laundry_bookings');
                $customer_completed_booking = array('customer_completed_laundry_bookings');
                $customer_cancelled_booking = array('customer_cancelled_laundry_bookings');
                $customer_favourite_deliverers = array('favourite_provider','search_for_provider','search_provider_details_view');
                $laundry_pending_payments = array('customer_laundry_pending_payments');
                $customer_laundry_claims = array('customer_laundry_claims');

                //Seller booking
                $provider_create_booking = array('provider_create_laundry_bookings', 'laundry_booking_payment_by_provider');
                $provider_accepted_booking = array('accepted_laundry_bookings');
                $provider_in_progress_booking = array('in_progress_laundry_bookings');
                $provider_completed_booking = array('completed_laundry_bookings');
                $provider_cancelled_booking = array('cancelled_laundry_bookings');
                $provider_favourite_deliverers = array('favourite_deliverers');
                $laundry_pending_invoices = array('provider_laundry_pending_invoices');
                $laundry_claims = array('provider_laundry_claims');
                

                //Laundry Accounts
                $wallet = array('laundry_wallet','laundry_account_history','create_laundry_withdraw_request','laundry_invoices_history');
                $scrow = array('my_scrow','user_scrow_history');
                $withdraw = array('laundry_withdrawals_request','laundry_withdrawals_accepted','laundry_withdrawals_rejected','laundry_withdrawal_request_details','withdraw_request_sent');

                $support_request = array('get_support_list','get_support','view_support_reply_details');
            ?>
            
            <?php 
            $orderCount = 0;
            if(isset($_SESSION['last_login_date_time'])) {
                $orderCount = $this->api->requested_order_count($_SESSION['cust_id'], $_SESSION['last_login_date_time']); } ?>

            <li class="<?php if(in_array($method_name, $dashboard)) { echo 'active'; } ?>">
                <a href="<?= base_url('user-panel-laundry/dashboard-laundry'); ?>"> <span class="nav-label"><?= $this->lang->line('dash'); ?></span></a>
            </li>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
                <li class="<?php if(in_array($method_name, $dashboard_users)) { echo 'active'; } ?>">
                    <a href="<?= base_url('user-panel-laundry/dashboard-laundry-users'); ?>"> <span class="nav-label"><?= $this->lang->line('Activity Dashboard'); ?></span></a>
                </li>
            <?php } ?>

            <li class="<?php if(in_array($method_name, $address_book)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/address-book-list'); ?>"><?= $this->lang->line('address_book'); ?></a>
            </li>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
                <li class="<?php if(in_array($method_name, $basic_info) || in_array($method_name, $business_photo) || in_array($method_name, $laundry_charges) || in_array($method_name, $laundry_cancellation_charges) || in_array($method_name, $laundry_special_charges) ) { echo 'active'; } ?>">
                    <a href="#"><span class="nav-label"><?= $this->lang->line('Provider Profile'); ?></span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level">
                        <li class="<?php if(in_array($method_name, $basic_info)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/provider-profile'); ?>"><?= $this->lang->line('basic_info'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $business_photo)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/business-photos'); ?>"><?= $this->lang->line('business_photos'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $laundry_charges)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/laundry-charges-list'); ?>"><?= $this->lang->line('Laundry Charges'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $laundry_special_charges)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/laundry-special-charges-list'); ?>"><?= $this->lang->line('Special Charges'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $laundry_cancellation_charges)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/laundry-provider-cancellation-charges-list'); ?>"><?= $this->lang->line('cancellation_charges'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'buyer') { ?>
                <li class="<?php if(in_array($method_name, $customer_create_booking) || in_array($method_name, $customer_accepted_booking) || in_array($method_name, $customer_in_progress_booking) || in_array($method_name, $customer_completed_booking) || in_array($method_name, $customer_cancelled_booking) || in_array($method_name, $customer_favourite_deliverers) || in_array($method_name, $laundry_pending_payments) || in_array($method_name, $customer_laundry_claims) ) { echo 'active'; } ?>">
                    <a href="#"><span class="nav-label"><?= $this->lang->line('my_bookings'); ?></span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level">
                        <li class="<?php if(in_array($method_name, $customer_create_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/customer-create-laundry-bookings'); ?>"><?= $this->lang->line('create_booking'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $customer_accepted_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/customer-accepted-laundry-bookings'); ?>"><?= $this->lang->line('accepted'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $customer_in_progress_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/customer-in-progress-laundry-bookings'); ?>"><?= $this->lang->line('in_progress'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $customer_completed_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/customer-completed-laundry-bookings'); ?>"><?= $this->lang->line('Completed'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $customer_cancelled_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/customer-cancelled-laundry-bookings'); ?>"><?= $this->lang->line('cancelled'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $customer_favourite_deliverers)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/favourite-provider'); ?>"><?= $this->lang->line('Favorite Providers'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $laundry_pending_payments)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/customer-laundry-pending-payments'); ?>"><?= $this->lang->line('Pending Payments'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $customer_laundry_claims)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/customer-laundry-claims'); ?>"><?= $this->lang->line('Claims Reported'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
                <li class="<?php if(in_array($method_name, $provider_accepted_booking) || in_array($method_name, $provider_in_progress_booking) || in_array($method_name, $provider_completed_booking) || in_array($method_name, $provider_cancelled_booking) || in_array($method_name, $provider_favourite_deliverers) || in_array($method_name, $provider_create_booking) || in_array($method_name, $laundry_pending_invoices) || in_array($method_name, $laundry_claims)){ echo 'active'; } ?>">
                    <a href="#"><span class="nav-label"><?= $this->lang->line('orders'); ?></span> <span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level">
                        <li class="<?php if(in_array($method_name, $provider_create_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/provider-create-laundry-bookings'); ?>"><?= $this->lang->line('Create Order'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $provider_accepted_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/accepted-laundry-bookings'); ?>"><?= $this->lang->line('accepted'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $provider_in_progress_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/in-progress-laundry-bookings'); ?>"><?= $this->lang->line('in_progress'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $provider_completed_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/completed-laundry-bookings'); ?>"><?= $this->lang->line('Completed'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $provider_cancelled_booking)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/cancelled-laundry-bookings'); ?>"><?= $this->lang->line('cancelled'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $provider_favourite_deliverers)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/favourite-deliverers'); ?>"><?= $this->lang->line('favourite_deliverers'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $laundry_pending_invoices)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/provider-laundry-pending-invoices'); ?>"><?= $this->lang->line('Pending Invoices'); ?></a></li>
                        <li class="<?php if(in_array($method_name, $laundry_claims)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/provider-laundry-claims'); ?>"><?= $this->lang->line('Claims'); ?></a></li>
                    </ul>
                </li>
            <?php } ?>

            <li class="<?php if(in_array($method_name, $wallet) || in_array($method_name, $withdraw) || in_array($method_name, $scrow)) { echo 'active'; } ?>">
                <a href="#"><span class="nav-label"><?= $this->lang->line('my_accounts'); ?></span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li class="<?php if(in_array($method_name, $wallet)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/laundry-wallet'); ?>"><?= $this->lang->line('e_wallet'); ?></a></li>
                    <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?><li class="<?php if(in_array($method_name, $scrow)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/my-scrow'); ?>"><?= $this->lang->line('scrow_account'); ?></a></li><?php } ?>
                    <li class="<?php if(in_array($method_name, $withdraw)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel-laundry/laundry-withdrawals-request'); ?>"><?= $this->lang->line('withdrawals'); ?></a></li>
                </ul>
            </li>

            <li class="<?php if(in_array($method_name, $support_request)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/get-support-list'; ?>"><?= $this->lang->line('get_support'); ?></a>
            </li>
        </ul>
    </div>
</aside>

<!-- Main Wrapper -->
<?php
$method_name = $this->router->fetch_method(); 
if($method_name == 'dashboard_laundry') { ?>
    <div id="wrapper" style="background: url(<?=base_url('resources/laundry_services.jpg')?>); background-repeat: no-repeat; background-size: 100%;" >
<?php } else { ?>
    <div id="wrapper" style="" >
<?php } ?>