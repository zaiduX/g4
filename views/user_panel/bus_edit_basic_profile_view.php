<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-bus/dashboard-bus'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-bus/user-profile'); ?>"><?= $this->lang->line('user_profile'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('basic_details'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('basic_details'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('update_your_basic_profile_details'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel-bus/basic-profile/update'); ?>" method="post" class="form-horizontal" id="addEducationForm" enctype="multipart/form-data">
          <div class="panel-body">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <div class="row">
                <div class="form-group">
                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                    <label class=""><?= $this->lang->line('update_avatar'); ?></label>
                    <div class="input-group">
                      <span class="input-group-btn">
                        <button id="upload_avatar" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 btn btn-green"><i class="fa fa-user"></i>&nbsp; <?= $this->lang->line('select_avatar'); ?></button> 
                      </span>                      
                    </div>
                    <span id="avatar_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('avatar_attached'); ?></span>
                    <input type="file" id="avatar" name="avatar" class="upload attachment" accept="image/*" onchange="avatar_name(event)" style="width: inherit;" />
                  </div>
                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                    <label class=""><?= $this->lang->line('update_cover'); ?></label>
                    <div class="input-group">
                      <span class="input-group-btn">
                        <button id="upload_cover" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 btn btn-green"><i class="fa fa-photo"></i>&nbsp; <?= $this->lang->line('select_cover'); ?></button>
                      </span>                      
                    </div>
                    <span id="cover_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('cover_attached'); ?></span>
                    <input type="file" id="cover" name="cover" class="upload attachment" accept="image/*" onchange="cover_name(event)" style="width: inherit;" />
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <label class=""><?= $this->lang->line('email_address'); ?></label>
                    <input type="text" class="form-control" value="<?= $cust['email1'] ?>" readonly/>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <label class=""><?= $this->lang->line('mobile_number'); ?> [ <small class="text-help text-muted text-info"><?= $this->lang->line('otp_verification_must'); ?></small> ]</label>
                    <input type="text" class="form-control" name="mobile_no" value="<?= $cust['mobile1'] ?>" />
                    
                  </div>                  
                </div>
              </div>

              <div class="row">
                <div class="form-group">
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <label class=""><?= $this->lang->line('first_name'); ?></label>
                    <input name="firstname" type="text" class="form-control" value="<?= ($cust['firstname'] != 'NULL') ? $cust['firstname'] :''; ?>" placeholder="Enter Your First Name" />
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <label class=""><?= $this->lang->line('last_name'); ?></label>
                    <input name="lastname" type="text" class="form-control" value="<?= ($cust['lastname'] != 'NULL') ? $cust['lastname'] :''; ?>" placeholder="Enter Your Last Name" />
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <label class=""><?= $this->lang->line('gender'); ?></label>
                    <select id="gender" name="gender" class="form-control select2" data-allow-clear="true" data-placeholder="Select Gender">
                      <option value="">Select Gender</option>
                      <option value="m" <?= ($cust['gender'] == "m")? "selected" :"" ?> ><?= $this->lang->line('male'); ?></option>
                      <option value="f" <?= ($cust['gender'] == "f")? "selected" :"" ?> ><?= $this->lang->line('female'); ?></option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group">
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <label class=""><?= $this->lang->line('country'); ?></label>
                    <select id="country_id" name="country_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Country">
                      <option value=""><?= $this->lang->line('select_country'); ?></option>
                      <?php foreach ($countries as $country): ?>
                        <option value="<?= $country['country_id'] ?>" <?=(($cust['country_id']==$country['country_id'])) ?"selected":""?>> <?= $country['country_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <label class=""><?= $this->lang->line('state'); ?></label>
                    <select id="state_id" name="state_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select State" <?= ($cust['country_id'] > 0 ) ? "":"disabled";?>>
                      <option value=""><?= $this->lang->line('select_state'); ?></option>
                      <?php foreach ($states as $state): ?>
                        <option value="<?= $state['state_id'] ?>" <?=(($cust['state_id']==$state['state_id'])) ?"selected":""?>> <?= $state['state_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                    <label class=""><?= $this->lang->line('city'); ?> :</label>
                    <select id="city_id" name="city_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select City" <?= ($cust['state_id'] > 0 ) ? "":"disabled";?> >                      
                        <option value=""><?= $this->lang->line('select_city'); ?></option>
                        <?php foreach ($cities as $city): ?>
                        <option value="<?= $city['city_id'] ?>" <?=(($cust['city_id']==$city['city_id'])) ?"selected":""?>> <?= $city['city_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <div class="panel-footer"> 
            <div class="row">
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-left">
                  <a href="<?= base_url('user-panel-bus/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                <button id="btn_submit" type="submit" class="btn btn-info"><?= $this->lang->line('submit_details'); ?></button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>

  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
   
    $('#city_id').attr('disabled', true);
    $('#state_id').empty();

    if(country_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-state-by-country-id", 
        data: { country_id: country_id },
        dataType: "json",
        success: function(res){ 
          $('#state_id').attr('disabled', false);
          $('#state_id').empty(); 
          $('#state_id').append('<option value=""><?= json_encode($this->lang->line('select_state'));?></option>');
          $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
          $('#state_id').focus();
        },
        beforeSend: function(){
          $('#state_id').empty();
          $('#state_id').append('<option value=""><?= json_encode($this->lang->line('loading'));?></option>');
        },
        error: function(){
          $('#state_id').attr('disabled', true);
          $('#state_id').empty();
          $('#state_id').append('<option value=""><?= json_encode($this->lang->line('no_option'));?></option>');
        }
      });
    } else { 
      $('#state_id').empty(); 
      $('#state_id').append('<option value=""><?= json_encode($this->lang->line('select_state'));?></option>');
      $('#state_id, #city_id').attr('disabled', true); 
    }

  });

  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $('#city_id').empty();
    
    if(country_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-cities-by-state-id", 
        data: { state_id: state_id },
        dataType: "json",
        success: function(res){ 
          $('#city_id').attr('disabled', false);
          $('#city_id').empty(); 
          $('#city_id').append('<option value=""><?= json_encode($this->lang->line('select_city'));?></option>');
          $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
          $('#city_id').focus();
        },
        beforeSend: function(){
          $('#city_id').empty();
          $('#city_id').append('<option value=""><?= json_encode($this->lang->line('loading'));?></option>');
        },
        error: function(){
          $('#city_id').attr('disabled', true);
          $('#city_id').empty();
          $('#city_id').append('<option value=""><?= json_encode($this->lang->line('no_option'));?></option>');
        }
      });
    } else { 
      $('#city_id').empty(); 
      $('#city_id').append('<option value=""><?= json_encode($this->lang->line('select_city'));?></option>');
      $('#city_id').attr('disabled', true); 
    }

  });
 
 $(function(){
    $("#addEducationForm").validate({
      ignore: [],
      rules: {
        avatar: {  accept:"jpg,png,jpeg,gif" },
        cover: {  accept:"jpg,png,jpeg,gif" },
        mobile_no: { required: true, number: true },
        firstname: { required: true, },
        lastname: { required: true, },
        gender: { required: true, },
        country_id: { required: true, },
        state_id: { required: true, },
        city_id: { required: true, },
      }, 
      messages: {
        avatar: {  accept: <?= json_encode($this->lang->line('only_image_allowed'));?>,   },
        cover: {  accept: <?= json_encode($this->lang->line('only_image_allowed'));?>,   },
        mobile_no: { required: <?= json_encode($this->lang->line('enter_mobile_number'));?>, number: <?= json_encode($this->lang->line('number_only'));?>, },
        firstname: { required: <?= json_encode($this->lang->line('enter_first_name'));?>,   },
        lastname: { required: <?= json_encode($this->lang->line('enter_last_name'));?>,  },
        gender: { required: <?= json_encode($this->lang->line('gender'));?>,  },
        country_id: { required: <?= json_encode($this->lang->line('select_country'));?>,  },
        state_id: { required: <?= json_encode($this->lang->line('select_state'));?>,  },
        city_id: { required: <?= json_encode($this->lang->line('select_city'));?>,  },
      }
    });

    $("#upload_avatar").on('click', function(e) { e.preventDefault(); $("#avatar").trigger('click'); });
    $("#upload_cover").on('click', function(e) { e.preventDefault(); $("#cover").trigger('click'); });
  });

  function avatar_name(e){ if(e.target.files[0].name !="") { $("#avatar_name").removeClass('hidden'); }}
  function cover_name(e){ if(e.target.files[0].name !="") { $("#cover_name").removeClass('hidden'); }}
</script>