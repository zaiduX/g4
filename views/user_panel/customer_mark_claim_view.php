<style> textarea { resize: none; } </style>
<style type="text/css">
  #attachment, .attachment {            position: absolute;            top: 0;            right: 0;            margin: 0;            padding: 0;            font-size: 20px;            cursor: pointer;            opacity: 0;            filter: alpha(opacity=0);        }
</style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('Laundry Claims'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('Report claim'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-exclamation-triangle fa-2x text-muted"></i> <?= $this->lang->line('Report claim'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Provide claim details'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="hpanel hblue">
        <form action="<?=base_url('user-panel-laundry/mark-claim-process')?>" method="post" class="form-horizontal" id="addClaim" enctype="multipart/form-data"> 
          <input type="hidden" name="service_id" value="<?=$booking_details['booking_id']?>">
          <div class="panel-body">              
            <div class="col-md-12">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              
              <div class="row">
                <div class="form-group">

                  <div class="col-md-7">
                    <div class="row">
                      <div class="col-md-6">
                        <label><?= $this->lang->line('Booking Items'); ?></label>
                      </div>
                      <div class="col-md-3 text-center">
                        <label><?= $this->lang->line('quantity'); ?></label>
                      </div>
                      <div class="col-md-3">
                        <label><?= $this->lang->line('Claim Quantity'); ?></label>
                      </div>
                    </div>
                    <?php foreach ($booking_details_list as $list): ?>
                      <div class="row" style="margin-bottom: 5px;">
                        <div class="col-md-6">
                          <h5><?=strtoupper($list['cat_name']).' - '.ucwords($list['sub_cat_name'])?></h5>
                        </div>
                        <div class="col-md-3 text-center">
                          <h5><?=$list['quantity']?></h5>
                        </div>
                        <div class="col-md-3">
                          <input type="number" name="claimed_quantity_<?=$list['bd_id']?>" class="form-control" value="0" min="0" max="<?=$list['quantity']?>" />
                        </div>
                      </div>
                    <?php endforeach ?>
                  </div>

                  <div class="col-md-5" style="padding-left: 0px;">
                    
                    <div class="col-md-12">
                      <label class=""><?=$this->lang->line('Claim Type')?></label>
                      <select id="claim_type" name="claim_type" class="form-control select2" data-allow-clear="true">
                        <option value=""><?=$this->lang->line('Select claim type')?></option>
                        <?php foreach ($claim_type as $ctype): ?>
                          <option value="<?= $ctype['sct_name']; ?>"><?= $ctype['sct_name']; ?></option>
                        <?php endforeach ?>
                      </select>
                    </div>
                    <div class="col-md-12">
                      <label><?=$this->lang->line('Claim Description')?></label>
                      <textarea class="form-control" id="claim_desc" name="claim_desc" placeholder="<?=$this->lang->line('Enter claim description')?>"></textarea>
                    </div>
                    <div class="col-md-6">
                      <label><?= $this->lang->line('Image (if any)'); ?></label>
                      <div class="input-group">
                        <span class="input-group-btn">
                          <button id="upload_image" class="btn btn-green"><i class="fa fa-photo"></i>&nbsp; <?= $this->lang->line('Select Image'); ?></button>
                        </span>
                      </div>
                      <span id="avatar_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('Image attached'); ?></span>
                      <input type="file" id="image_url" name="image_url" class="upload attachment form-control" accept="image/*" onchange="avatar_name(event)" />
                    </div>
                    <div class="col-md-6">
                      <label><?= $this->lang->line('Document (if any)'); ?></label>
                      <div class="input-group">
                        <span class="input-group-btn">
                          <button id="upload_document" class="btn btn-green"><i class="fa fa-file"></i>&nbsp; <?= $this->lang->line('Select Document'); ?></button>
                        </span>                      
                      </div>
                      <span id="cover_name" class="hidden"><i class="fa fa-paperclip"></i> &nbsp; <?= $this->lang->line('Document attached'); ?></span>
                      <input type="file" id="doc_url" name="doc_url" class="upload attachment form-control" onchange="cover_name(event)" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-md-6 text-left">
                  <a href="<?= base_url('user-panel-laundry/customer-completed-laundry-bookings'); ?>" class="btn btn-primary"><?= $this->lang->line('back'); ?></a>
               </div>
               <div class="col-md-6 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('Send Claim'); ?></button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  function avatar_name(e){ if(e.target.files[0].name !="") { $("#avatar_name").removeClass('hidden'); }}
  function cover_name(e){ if(e.target.files[0].name !="") { $("#cover_name").removeClass('hidden'); }}
  $("#upload_image").on('click', function(e) { e.preventDefault(); $("#image_url").trigger('click'); });
  $("#upload_document").on('click', function(e) { e.preventDefault(); $("#doc_url").trigger('click'); });
  $("#addClaim").keypress(function(event) { return event.keyCode != 13; });
    
  $("#addClaim").validate({
    ignore: [],
    rules: {
      claim_type: { required: true, },      
      claim_desc: { required: true, },
      image_url: { accept:"jpg,png,jpeg,gif" },
    }, 
    messages: {
      claim_type: { required: "<?=$this->lang->line('Select claim type')?>", },
      claim_desc: { required: "<?=$this->lang->line('Enter short description')?>", },
      image_url: { accept: "<?=$this->lang->line('only_image_allowed')?>", },
    }
  });
</script>


