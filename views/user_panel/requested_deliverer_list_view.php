<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('requested_deliverers'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                <?= $this->lang->line('requested_deliverers'); ?> &nbsp;&nbsp;&nbsp;
            </h2>
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
       <div class="col-lg-12"> 
       <?php if(!empty($deliverers)): ?>
            <?php $j=0;  foreach ($deliverers as $d):?> 
                <?php  
                    if($d['avatar_url'] != "NULL"){ $avatar_url= base_url($d['avatar_url']);  }
                    else { $avatar_url = $this->config->item('resource_url') . 'default-profile.jpg'; }
                    
                    $country_name = $this->user->get_country_name_by_id($d['country_id']); 
                    $state_name = $this->user->get_state_name_by_id($d['state_id']);
                    $city_name = $this->user->get_city_name_by_id($d['city_id']);
                ?>
                <div class="col-lg-4">
                    <div class="hpanel <?=($j%2==0)?'hyellow':'hgreen';?> contact-panel">
                        <div class="panel-body">
                            <form action="<?= base_url('user-panel/deliverer-details');?>" method="post" class="pull-right">
                                <input type="hidden" name="deliverer_id" value="<?= $d['cust_id'];?>" />
                                <input type="hidden" name="courier_order_id" value="<?= $order_id; ?>" />
                                <input type="hidden" name="requested_deliverer" value="requested_deliverer" />
                                <input type="hidden" name="redirect" value="user-panel/requested-deliverer" />
                                <button title="Profile View" class="btn btn-info btn-circle"><i class="fa fa-eye"></i></button>
                            </form>
                            <img alt="logo" class="img-thumbnail m-b" src="<?= $avatar_url; ?>" />
                            <h3><a><?= strtoupper($d['firstname']. ' '.$d['lastname']); ?></a></h3>
                            <div class="text-muted font-bold m-b-xs"><?= $city_name . ', '. $country_name; ?></div>
                            <div class="text-muted font-bold m-b-xs"></div>
                            <p>
                                <?php if($d['company_name'] !="NULL"): ?>
                                    <?= $this->lang->line('company'); ?>  <?= strtoupper($d['company_name']); ?> <br />
                                <?php else: ?>
                                    <?= $this->lang->line('company_not_defined'); ?> <br />
                                <?php endif; ?>
                                  <?= $this->lang->line('tile1'); ?>  <?= $d['address']; ?> <br />
                                <?= $city_name.', '. $state_name .', '.$country_name; ?>
                            </p>                            
                        </div>
                        <div class="panel-footer">
                            <?= $this->lang->line('ratings'); ?> : 
                            <?php for($i=0; $i<(int)$d['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                            <?php for($i=0; $i<(5-(int) $d['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                            ( <?= (int) $d['ratings'] ?> / 5 )
                        </div>
                    </div>
                </div>
            <?php $j++; endforeach; ?>
        <?php else: ?>            
		<div class="hpanel hgreen contact-panel">
                        <div class="panel-body text-center">
                          <h3> <?= $this->lang->line('no_record_found'); ?></h3>
                        </div>
                </div>                        
	<?php endif; ?>           
            
            
        </div>
    </div>
</div>