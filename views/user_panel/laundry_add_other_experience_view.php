<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('profile'); ?></span></li>
          <li><span><?= $this->lang->line('details'); ?></span></li>
          <li class="active"><span>Add Other Experience</span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-wrench fa-2x text-muted"></i> <?= $this->lang->line('other_experience'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('add_new_other_experience_details'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel-laundry/add-other-experience'); ?>" method="post" class="form-horizontal" id="addExperience" >        
          <div class="panel-body">                      
            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xl-offset-1 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              
              <div class="row">
                <div class="form-group">
                  <label class=""><?= $this->lang->line('organization / company_name'); ?> </label>
                  <input name="org_name" type="text" class="form-control" required  placeholder="<?= $this->lang->line('organization / company_name'); ?>" autofocus />
                </div>                                
                <div class="form-group">
                  <label class=""><?= $this->lang->line('job_title'); ?></label>
                  <input name="title" type="text" class="form-control" placeholder="<?= $this->lang->line('job_title'); ?>" required />
                </div>
                <div class="form-group">
                  <label class=""><?= $this->lang->line('job_location'); ?></label>
                  <input name="location" type="text" class="form-control" id="" placeholder="<?= $this->lang->line('job_location'); ?>" required  />
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                      <label class=""><?= $this->lang->line('Job_start_Date'); ?></label>
                      <div class="input-group date" data-provide="datepicker">
                        <input type="text" class="form-control" id="start_date" name="start_date" />
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>              
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                      <label class=""><?= $this->lang->line('job_end_date'); ?></label>
                      <div class=" end_date_picker input-group date" data-provide="datepicker">
                        <input type="text" class="form-control" name="end_date" />
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>                                           
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                      <label class="">&nbsp;</label>                      
                      <div>
                        <label class="checkbox-inline"><input type="checkbox" name="currently_working" > <?= $this->lang->line('currently_working'); ?></label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class=""><?= $this->lang->line('description_( optional )'); ?></label>
                  <textarea name="description"  class="form-control" id="" rows="5" placeholder="<?= $this->lang->line('description_( optional )'); ?>" style="resize: none;"></textarea>
                </div> 
              </div>
                                                    
            </div>

          </div>        
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xl-offset-1 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 text-left">
                  <a href="<?= base_url('user-panel-laundry/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
               </div>
               <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('submit_detail'); ?></button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>

  $.validator.addMethod("greaterThan", function(value, element, params) {
    if (!/Invalid|NaN/.test(new Date(value))) { return new Date(value) > new Date($(params).val()); }
      return isNaN(value) && isNaN($(params).val())  || (Number(value) > Number($(params).val())); 
  },'Must be greater than start date.');
 

  $("#addExperience").validate({
    ignore: [],
    rules: {
      org_name: { required: true, },
      title: { required: true, },
      location: { required: true,},
      start_date: { required: true, date: true, },
      end_date: { required: false, date: true, greaterThan: "#start_date" },
    }, 
    messages: {
      org_name: { required: "Enter organisation / company name.",   },
      title: { required: "Enter job title.",  },
      location: { required: "Enter location.",},
      start_date: { required: "Select date.", date: "Invalid date.",  },
      end_date: { date: "Invalid date.",  },
    }
  });

  $(function(){

    $("input[type=checkbox]").change(function() {
      if(!this.checked) {  
        $(".end_date_picker").attr("data-provide","datepicker").addClass('date').css({  "cursor": "pointer", "pointer-events": "auto" });
        $("input[name=end_date]").attr("disabled", false);
      }
      else{
       $(".end_date_picker").removeAttr('data-provide').removeClass('date').css({  "cursor": "wait", "pointer-events": "none" });
        $("input[name=end_date]").removeClass('error').val("").attr("disabled", true); 
        $("#end_date-error").text('').removeClass('error');
      }
    });
  });
</script>