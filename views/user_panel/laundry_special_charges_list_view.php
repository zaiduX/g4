    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                  <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
                </a>
                <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('laundry Special Rate'); ?></span></li>
                </ol>
                </div>
                <h2 class="font-light m-b-xs"> <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('laundry Special Rate'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('Manage Laundry Special Rates'); ?></small> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <form action="<?=base_url('user-panel-laundry/laundry-special-charges-add')?>" method="post" style="display:inline-block;">
                    <button class="btn btn-success btn-sm" type="submit" style="margin-top:-20px;"><i class="fa fa-plus"></i> <span class="bold"><?= $this->lang->line('Add Laundry Special charge'); ?></span></button>
                </form>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= $this->lang->line('Sr. No'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('from'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('to'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('monday'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('tuesday'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('wednesday'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('thursday'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('friday'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('saturday'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('sunday'); ?></th>
                                    <th class="text-center"><?= $this->lang->line('action'); ?></th>
                                </tr>
                            </thead>
                            <tbody class="panel-body">
                                <?php foreach ($charges as $data) { static $i = 0; $i++; ?>
                                    <tr>
                                        <td class="text-center"><?=$i?></td>
                                        <td class="text-center"><?=($data['start_date']=='NULL')?'N/A':$data['start_date']?></td>
                                        <td class="text-center"><?=($data['end_date']=='NULL')?'N/A':$data['end_date']?></td>
                                        <td class="text-center"><?=$data['mon']?>%</td>
                                        <td class="text-center"><?=$data['tue']?>%</td>
                                        <td class="text-center"><?=$data['wed']?>%</td>
                                        <td class="text-center"><?=$data['thu']?>%</td>
                                        <td class="text-center"><?=$data['fri']?>%</td>
                                        <td class="text-center"><?=$data['sat']?>%</td>
                                        <td class="text-center"><?=$data['sun']?>%</td>
                                        <td>
                                            <form action="<?=base_url('user-panel-laundry/laundry-special-charges-edit')?>" method="post" style="display: inline-block;">
                                                <input type="hidden" name="charge_id" value="<?= $data['charge_id'] ?>">
                                                <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-wrench"></i> <span class="bold"><?= $this->lang->line('Setup'); ?></span></button>
                                            </form>
                                            <button style="display: inline-block;" class="btn btn-danger btn-sm charegeDeleteAlert" id="<?= $data['charge_id']?>"><i class="fa fa-trash-o"></i> <span class="bold"><?= $this->lang->line('delete'); ?></span></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.charegeDeleteAlert').click(function () {
            var id = this.id;
            swal(
            {
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-laundry/laundry-special-charges-delete')?>", {id: id}, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('deleted'); ?>", "<?= $this->lang->line('Special charge has been deleted!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting Special charge record!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Special charge is safe!'); ?>", "error"); }
            });
        });
    </script>