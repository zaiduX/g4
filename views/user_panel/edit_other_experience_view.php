<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('profile'); ?></span></li>
          <li><span><?= $this->lang->line('details'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('edit_other_experience'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-graduation-cap fa-2x text-muted"></i> <?= $this->lang->line('other_experience'); ?> </h2>
      <small class="m-t-md">E<?= $this->lang->line('edit_other_experience_details'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel/edit-other-experience'); ?>" method="post" class="form-horizontal" id="editExperience" >        
          <div class="panel-body">          
            <input type="hidden" name="experience_id" value="<?= $experience['exp_id']; ?>" />
            <div class="col-lg-10 col-lg-offset-1">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              
              <div class="row">
                <div class="form-group">
                  <label class=""><?= $this->lang->line('organisation_company_name'); ?></label>
                  <input name="org_name" type="text" class="form-control" required value="<?= $experience['org_name']; ?>" />
                </div>                                
                <div class="form-group">
                  <label class=""><?= $this->lang->line('job_title'); ?></label>
                  <input name="title" type="text" class="form-control" placeholder="Enter job title" required value="<?= $experience['job_title']; ?>" />
                </div>
                <div class="form-group">
                  <label class=""><?= $this->lang->line('job_location'); ?></label>
                  <input name="location" type="text" class="form-control" id="" placeholder="Enter job location" required value="<?= $experience['job_location']; ?>" />
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-4">
                      <label class=""><?= $this->lang->line('Job_start_Date'); ?></label>
                      <div class="input-group date" data-provide="datepicker">
                        <input type="text" class="form-control" id="start_date" name="start_date" value="<?= $experience['start_date']; ?>" />
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>              
                    </div>
                    <div class="col-lg-4">
                      <label class=""><?= $this->lang->line('job_end_date'); ?></label>
                      <?php if ($experience['currently_working'] == 0): ?>
                        <div class=" end_date_picker input-group date" data-provide="datepicker">
                          <input type="text" class="form-control" name="end_date" value="<?= $experience['end_date']; ?>" />
                          <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                          </div>
                        </div>                     
                      <?php else: ?>
                        <div class="end_date_picker input-group" data-date-format="yyyy-mm-dd">
                          <input type="text" class="form-control" name="end_date" id="end_date" disabled />
                          <div class="input-group-addon  disabled">
                            <span class="glyphicon glyphicon-th"></span>
                          </div>
                        </div>
                      <?php endif; ?>
                    </div>
                    <div class="col-lg-4">
                      <label class="">&nbsp;</label>                      
                      <div>
                        <label class="checkbox-inline">
                          <input type="checkbox" name="currently_working" <?= ($experience['currently_working'] == 1)? "checked" :""; ?> >
                            <?= $this->lang->line('currently_working'); ?>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class=""><?= $this->lang->line('description_optional'); ?></label>
                  <textarea name="description"  class="form-control" id="" rows="5" placeholder="Write Some Addtional Information ..." style="resize: none;"><?= nl2br($experience['job_desc']); ?></textarea>
                </div> 
              </div>
                                                    
            </div>

          </div>        
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-lg-5 col-lg-offset-1 text-left">
                  <a href="<?= base_url('user-panel/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
               </div>
               <div class="col-lg-5 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('update_detail'); ?></button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>

  $.validator.addMethod("greaterThan", function(value, element, params) {
    if (!/Invalid|NaN/.test(new Date(value))) { return new Date(value) > new Date($(params).val()); }
      return isNaN(value) && isNaN($(params).val())  || (Number(value) > Number($(params).val())); 
  },'Must be greater than start date.');
 

  $("#editExperience").validate({
    ignore: [],
    rules: {
      org_name: { required: true, },
      title: { required: true, },
      location: { required: true,},
      start_date: { required: true, date: true, },
      end_date: { date: true, greaterThan: "#start_date" },
    }, 
    messages: {
      org_name: { required: "Enter organisation / company name.",   },
      title: { required: "Enter job title.",  },
      location: { required: "Enter location.",},
      start_date: { required: "Select date.", date: "Invalid date.",  },
      end_date: { date: "Invalid date.",  },
    }
  });

  $(function(){

    $("input[type=checkbox]").change(function() {
      if(!this.checked) {  
        $(".end_date_picker").attr("data-provide","datepicker").addClass('date').css({  "cursor": "pointer", "pointer-events": "auto" });
        $("input[name=end_date]").attr("disabled", false);
      }
      else{
       $(".end_date_picker").removeAttr('data-provide').removeClass('date').css({  "cursor": "wait", "pointer-events": "none" });
        $("input[name=end_date]").removeClass('error').val("").attr("disabled", true); 
        $("#end_date-error").text('').removeClass('error');
      }
    });
  });
</script>