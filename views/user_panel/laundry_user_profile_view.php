<?php  
  $email_cut = explode('@', $cust['email1']); $name = $email_cut[0];
  $url2 = parse_url($cust['cover_url']);
?>
<style>     
  .cover {
    <?php if($cust['cover_url'] == "NULL"): ?>
      background-image: url(<?= $this->config->item('resource_url'). 'bg.jpg'; ?>) !important;
    <?php else: ?>
      background-image: url(<?= ( isset($url2['scheme']) AND ($url2['scheme'] == "https" || $url2['scheme'] == "http" )) ? $cust['cover_url'] : base_url($cust['cover_url']); ?>) !important;
    <?php endif; ?>
    background-position: center !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
    height: 200px;
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header"> <i class="fa fa-arrow-up"></i>  </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('user_profile'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><?= $this->lang->line('user_profile'); ?> </h2>
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <?php $url = parse_url($cust['avatar_url']);?>
    <div class="col-lg-12" >
        <div class="hpanel hgreen" >                
            <div class="panel-body cover"> 
                    <?php if($cust['avatar_url'] == "NULL"): ?>
            <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b" alt="avatar" style="width: 15%;" />
          <?php else: ?>
            <img src="<?= ( isset($url['scheme']) AND ($url['scheme'] == "https" || $url['scheme'] == "http" )) ? $cust['avatar_url'] : base_url($cust['avatar_url']); ?>" class="img-thumbnail m-b" alt="avatar"  style="width: 15%;" />
          <?php endif; ?>
            </div>
        </div>
    </div>
  </div>
<br/>
  <div class="row">
    
    <div class="col-lg-4 col-lg-4 col-md-4 col-sm-4" >
        <div class="hpanel hgreen" >
          <div class="panel-heading">
              <h4><strong><?= $this->lang->line('basic_info'); ?></strong></h4>
          </div>
          <div class="panel-body">            
            <div class="row">
              <div class="col-md-10">
                <?php if( $cust['firstname'] != "NULL") : ?>
                  <h3 class="font-uppercase">
                    <?= $cust['firstname']; ?>
                        <?php if( $cust['lastname'] != "NULL") { echo $cust['lastname'];  } ?>
                  </h3>

                <?php else: ?>
                  <h3 class="font-uppercase"><?= $name; ?></h3>
                <?php endif; ?>
                
                <?php 
                  if($cust['country_id'] > 0 ): 
                    $country_name = $this->user->get_country_name_by_id($cust['country_id']); 
                    $city_name = $this->user->get_city_name_by_id($cust['city_id']);
                ?>
                  <div class="text-muted font-bold m-b-xs"><?= $city_name . ', '.$country_name; ?></div>
                <?php endif; ?>

              </div>

              <div class="col-md-2">
                  <a href="<?= base_url('user-panel-laundry/basic-profile/').$cust['cust_id']; ?>" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></a>
              </div>

            </div>

              <p><strong><?= $this->lang->line('email'); ?> :</strong> <?= $cust['email1'];  ?></p>
              <p><strong><?= $this->lang->line('mobile_number'); ?> :</strong> <?= $cust['mobile1'];  ?></p>
              <h5><a href=""><?= $this->lang->line('profile_completed'); ?>  <?= $percentage.'%'; ?></a></h5>
              <div class="progress m-t-xs full progress-small progress-striped">
                <div style="width: <?= $percentage.'%'; ?>" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class=" progress-bar progress-bar-success">
                </div>
                <span class="sr-only"><?= $this->lang->line('35_%_completed'); ?></span>
              </div>
          </div>
          <div class="panel-footer contact-footer hidden">
              <div class="row">
                  <div class="col-md-5 border-right">
                      <div class="contact-stat"><span><?= $this->lang->line('projects'); ?>: </span> <strong>200</strong></div>
                  </div>
                  <div class="col-md-5 border-right">
                      <div class="contact-stat"><span><?= $this->lang->line('messages'); ?>: </span> <strong>300</strong></div>
                  </div>
                  <div class="col-md-5">
                      <div class="contact-stat"><span><?= $this->lang->line('views'); ?>: </span> <strong>400</strong></div>
                  </div>
              </div>
          </div>
      </div>
        </div>
        <!-- col-lg-8 -->
      <div class="col-lg-8 col-lg-8 col-md-8 col-sm-8" >


         <!-- Payment details Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('payment_methods'); ?> </strong></h4></div>              
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                    <a href="<?= base_url('user-panel-laundry/payment-method/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
                    <?php if($total_pay_methods > 0 ): ?>
                        <a href="<?= base_url('user-panel-laundry/payment-methods'); ?>" class="btn btn-danger btn-circle btn-delete""><i class="fa fa-trash"></i></a>
                    <?php endif; ?>
                    </div>
                </div>
              </div>
            
            <?php if($pay_methods): ?>
              <div class="panel-body"> 
                <div class="row">
                  <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"><p><strong><?= $this->lang->line('payment_methods'); ?>:</strong></p> </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><p style="word-wrap: break-word;"><?= $pay_methods[0]['payment_method']; ?></p> </div>  
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                    <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-laundry/payment-method/'). $pay_methods[0]['pay_id'];?>">
                      <i class="fa fa-pencil"></i>
                    </a>
                  </div>
                </div>
            <?php if($pay_methods[0]['brand_name'] != 'NULL' && $pay_methods[0]['brand_name'] != '') { ?>
              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('payment_method_brand'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $pay_methods[0]['brand_name']; ?></p> </div>  
              </div>
            <?php } ?>
              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('card_number'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $pay_methods[0]['card_number']; ?></p> </div>  
              </div>
            <?php if($pay_methods[0]['bank_name'] != 'NULL' && $pay_methods[0]['bank_name'] != '') { ?>
              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('bank_name'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  
                    <p style="word-wrap: break-word;"><?= $pay_methods[0]['bank_name']; ?></p>
                </div>  
              </div>
            <?php } ?>
            <?php if($pay_methods[0]['bank_address'] != 'NULL' && $pay_methods[0]['bank_address'] != '' && !is_null($pay_methods[0]['bank_address'])) { ?>
              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('bank_address'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $pay_methods[0]['bank_address']; ?></p> </div>  
              </div>
            <?php } ?>
            <?php if($pay_methods[0]['bank_short_code'] != 'NULL' && $pay_methods[0]['bank_short_code'] != '' && !is_null($pay_methods[0]['bank_short_code'])) { ?>
              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('bank_short_code_swift_code'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $pay_methods[0]['bank_short_code']; ?></p> </div>  
              </div>
            <?php } ?>
            <?php if($pay_methods[0]['expirty_date'] != 'NULL' && $pay_methods[0]['expirty_date'] != '/') { ?>
              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('expiry_date'); ?></strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= date("m/y" , strtotime($pay_methods[0]['expirty_date'])); ?></p> </div>  
              </div> 
            <?php } ?>
                        </div>          
                        <div class="panel-footer">
                <a href="<?= base_url('user-panel-laundry/payment-methods'); ?>" class="btn btn-xs btn-info"><?= $this->lang->line('see_more'); ?></a>                
                <button class="btn btn-xs pull-right"><?= $total_pay_methods; ?></button>
              </div>
                    <?php else: ?>     
                <div class="panel-body">
                    <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"><p></p></div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7"><p><?= $this->lang->line('no_record_found'); ?></p></div>  
                    </div>
                </div>
                    <?php endif; ?>
          </div>
            </div>
            <!--End: Payment details Area -->


            
            <div id="more_details" style="display: none" >
                
                 <!-- Education Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('education'); ?> </strong></h4></div>            
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                    <a href="<?= base_url('user-panel-laundry/education/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
                                <?php if($total_edu > 0 ): ?>
                        <a href="<?= base_url('user-panel-laundry/educations'); ?>" class="btn btn-danger btn-circle btn-delete""> <i class="fa fa-trash"></i></a>
                    <?php endif; ?>
                    </div>
                </div>
              </div>
            
                    <?php if($education): ?>
                <div class="panel-body">
                            
                            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('university_institute_name'); ?>:</strong></p> </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">  <p style="word-wrap: break-word;"><?= $education[0]['institute_name']; ?></p> </div>  
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-laundry/education/').$education[0]['edu_id'];?>">
                    <i class="fa fa-pencil"></i>
                  </a>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('qualification'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $education[0]['qualification']; ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('title'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $education[0]['certificate_title']; ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('years_of_qualification_attend_years'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  
                    <p style="word-wrap: break-word;"><?= $education[0]['qualify_year'] . ' [ '. date('Y', strtotime($education[0]['start_date'])); ?> - <?= date('Y', strtotime($education[0]['end_date'])). ' ]'; ?></p>
                </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('grade'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $education[0]['grade']; ?></p> </div>  
              </div>
              

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('additional_information'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= nl2br($education[0]['description']); ?></p> </div>  
              </div>
                            
                        </div>          
                        <div class="panel-footer">              
                <a href="<?= base_url('user-panel-laundry/educations'); ?>" class="btn btn-xs btn-info"><?= $this->lang->line('see_more'); ?></a>                 
                <button class="btn btn-xs pull-right"><?= $total_edu; ?></button>
              </div>
                    <?php else: ?>     
                <div class="panel-body">
                    <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"><p></p></div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7"><p><?= $this->lang->line('no_record_found'); ?></p></div>  
                    </div>
                </div>
                    <?php endif; ?>
          </div>
            </div>
            <!--End: Education Area -->

            <!-- Experience Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('experience'); ?> </strong></h4></div>           
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                    <a href="<?= base_url('user-panel-laundry/experience/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
                    <?php if($total_exp > 0 ): ?>
                        <a href="<?= base_url('user-panel-laundry/experiences'); ?>" class="btn btn-danger btn-circle btn-delete""><i class="fa fa-trash"></i></a>
                    <?php endif; ?>
                    </div>
                </div>
              </div>
                    <?php if($experience): ?>
                <div class="panel-body">
                            
                            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('organisation_company_name'); ?>:</strong></p> </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">  <p style="word-wrap: break-word;"><?= $experience[0]['org_name']; ?></p> </div>  
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">  
                    <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-laundry/experience/').$experience[0]['exp_id'];?>"><i class="fa fa-pencil"></i></a>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('job_title'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $experience[0]['job_title']; ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('job_location'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $experience[0]['job_location']; ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('total_experience_start_end_date'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  
                    <p style="word-wrap: break-word;"><?php                     
                        $start_date = new DateTime($experience[0]['start_date']);
                                                                            
                        if($experience[0]['currently_working']) { $today = date('Y-m-d'); $end_date = new DateTime($today); }
                        else { $end_date = new DateTime($experience[0]['end_date']);     }                      
                        echo $end_date->diff($start_date)->y. ' years ' . $end_date->diff($start_date)->m . ' months';

                        echo ' [ ' .date('d-M-Y', strtotime($experience[0]['start_date'])). ' / ';
                        if($experience[0]['currently_working']){  echo "Currently Working"; } 
                        else{   echo date('d-M-Y', strtotime($experience[0]['end_date'])); }
                        echo ' ]';
                    ?></p>
                </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('description'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= nl2br($experience[0]['job_desc']); ?></p> </div>  
              </div> 
                            
                        </div>          
                        <div class="panel-footer">              
                <a href="<?= base_url('user-panel-laundry/experiences'); ?>" class="btn btn-xs btn-info"><?= $this->lang->line('see_more'); ?></a>                               
                <button class="btn btn-xs pull-right"><?= $total_exp; ?></button>
              </div>
                    <?php else: ?>     
                <div class="panel-body">
                    <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"><p></p></div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7"><p><?= $this->lang->line('no_record_found'); ?></p></div>  
                    </div>
                </div>
                    <?php endif; ?>
                </div>
            </div>
            <!--End: Experience Area -->

            <!-- Other Experience Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('other_experience'); ?> </strong></h4></div>             
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                    <a href="<?= base_url('user-panel-laundry/other-experience/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
                    <?php if($total_other_exp > 0 ): ?>
                        <a href="<?= base_url('user-panel-laundry/other-experiences'); ?>" class="btn btn-danger btn-circle btn-delete""><i class="fa fa-trash"></i></a>
                    <?php endif; ?>
                    </div>
                </div>
              </div>
                    <?php if($other_experience): ?>
                <div class="panel-body">
                            
                            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('organisation_company_name'); ?>:</strong></p> </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">  <p style="word-wrap: break-word;"><?= $other_experience[0]['org_name']; ?></p> </div>  
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">  
                    <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-laundry/other-experience/').$other_experience[0]['exp_id'];?>"><i class="fa fa-pencil"></i></a>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('job_title'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $other_experience[0]['job_title']; ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('job_location'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $other_experience[0]['job_location']; ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('total_experience_start_end_date'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  
                    <p style="word-wrap: break-word;"><?php                     
                        $start_date = new DateTime($other_experience[0]['start_date']);                                                 
                        if($other_experience[0]['currently_working']) { $today = date('Y-m-d'); $end_date = new DateTime($today); }
                        else { $end_date = new DateTime($other_experience[0]['end_date']);   }                      
                        echo $end_date->diff($start_date)->y. ' years ' . $end_date->diff($start_date)->m . ' months';

                        echo ' [ ' .date('d-M-Y', strtotime($other_experience[0]['start_date'])). ' / ';
                        if($other_experience[0]['currently_working']){  echo "Currently Working"; } 
                        else{   echo date('d-M-Y', strtotime($other_experience[0]['end_date'])); }
                        echo ' ]';
                    ?></p>
                </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('description'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= nl2br($other_experience[0]['job_desc']); ?></p> </div>  
              </div> 
                            
                        </div>          
                        <div class="panel-footer">                          
                <a href="<?= base_url('user-panel-laundry/other-experiences'); ?>" class="btn btn-xs btn-info"><?= $this->lang->line('see_more'); ?></a>                  
                <button class="btn btn-xs pull-right"><?= $total_other_exp; ?></button>
              </div>
                    <?php else: ?>     
                <div class="panel-body">
                    <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"><p></p></div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7"><p><?= $this->lang->line('no_record_found'); ?></p></div>  
                    </div>
                </div>
                    <?php endif; ?>
                </div>
            </div>
            <!--End: Other Experience Area -->
            
            <!-- Skills Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('skills'); ?> </strong></h4></div>           
                </div>
              </div>
            
                    <?php if($skills):?>
                        
                        <?php $jr_names = $cf_names = $sr_names = $ex_names = array(); 
              if($skills['junior_skills'] != "NULL"){ 
                $junior_skills = explode(',', $skills['junior_skills']);
                foreach ($junior_skills as $jr ){ $jr_names[] = $this->user->get_skillname_by_id($jr);}
              }
              if($skills['confirmed_skills']!="NULL"){
                $confirmed_skills = explode(',', $skills['confirmed_skills']);
                foreach ($confirmed_skills as $jr ){ $cf_names[] = $this->user->get_skillname_by_id($jr);}
              }
              if($skills['senior_skills'] != "NULL") { 
                $senior_skills = explode(',', $skills['senior_skills']);
                foreach ($senior_skills as $jr ){ $sr_names[] = $this->user->get_skillname_by_id($jr); }
              }
              if($skills['expert_skills'] != "NULL") { 
                $expert_skills = explode(',', $skills['expert_skills']);
                foreach ($expert_skills as $jr ){ $ex_names[] = $this->user->get_skillname_by_id($jr); }
              }
            ?>

                <div class="panel-body">
                            
                            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('junior_level'); ?>:</strong></p> </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">  
                    <p style="word-wrap: break-word;">
                    <?php if($skills['junior_skills'] != "NULL" AND !empty($skills['junior_skills']) ) { echo '<strong>' . implode(', ', $jr_names).'</strong>';} else { echo $this->lang->line('not_defined'); }   ?>
                    </p> 
                </div>  
                <div class="col-md-2 text-right">
                  <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-laundry/skill');?>">
                    <i class="fa fa-pencil"></i>
                  </a>                  
                </div>
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('confirmed_level'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  
                                    <p style="word-wrap: break-word;">
                        <?php if($skills['confirmed_skills'] != "NULL" AND !empty($skills['junior_skills']) ) { echo '<strong>' . implode(', ', $cf_names) .'</strong>';} else { echo $this->lang->line('not_defined'); }
                        ?>
                    </p>
                </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('senior_level'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  
                                    <p style="word-wrap: break-word;">
                        <?php if($skills['senior_skills'] != "NULL" AND !empty($skills['junior_skills']) ) { echo '<strong>' . implode(', ', $sr_names) . '</strong>';} else { echo  $this->lang->line('not_defined'); }
                        ?>
                    </p>
                </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('expert_level'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  
                                    <p style="word-wrap: break-word;">
                        <?php if($skills['expert_skills'] != "NULL" AND !empty($skills['junior_skills']) ) { echo '<strong>' . implode(', ', $ex_names) .'</strong>';} else { echo $this->lang->line('not_defined'); }
                        ?>
                    </p>
                </div>  
              </div>

                        </div>                                  
                    <?php else: ?>     
                <div class="panel-body">
                    <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"><p></p></div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7"><p><?= $this->lang->line('no_record_found'); ?></p></div>  
                    </div>
                </div>
                    <?php endif; ?>
          </div>
            </div>
            <!--End: Skills Area -->
            
            <!-- Portfolio Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('portfolio'); ?> </strong></h4></div>            
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                    <a href="<?= base_url('user-panel-laundry/portfolio/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
                    <?php if($total_portfolios > 0 ): ?>                        
                        <a href="<?= base_url('user-panel-laundry/portfolios'); ?>" class="btn btn-danger btn-circle btn-delete""><i class="fa fa-trash"></i></a>
                    <?php endif; ?>
                    </div>
                </div>
              </div>
            
                    <?php if($portfolio): ?>
                <div class="panel-body">
                            
                            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('title'); ?>:</strong></p> </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">  <p style="word-wrap: break-word;"><?= $portfolio[0]['title']; ?></p> </div>  
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-laundry/portfolios/').$portfolio[0]['portfolio_id'];?>">
                    <i class="fa fa-pencil"></i>
                  </a>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('category'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= $this->user->get_category_name_by_id($portfolio[0]['cat_id']); ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('sub_categories'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= ($portfolio[0]['subcat_ids'] != "NULL") ? $portfolio[0]['subcat_ids'] : $this->lang->line('not_defined'); ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('description'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;"><?= ($portfolio[0]['description'] != "NULL") ? nl2br($portfolio[0]['description']) : $this->lang->line('not_defined');  ?></p> </div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('attachment'); ?>:</strong></p> </div>
                <div class="col-md-4">  
                    <p style="word-wrap: break-word;">
                    <?php if($portfolio[0]['attachement_url'] != "NULL"): ?>
                        <div class="lightBoxGallery">
                            <a href="<?= base_url($portfolio[0]['attachement_url']); ?>" title="Image from Unsplash" data-gallery="">
                                <img src="<?= base_url($portfolio[0]['attachement_url']); ?>" class="img-responsive">
                            </a>
                        </div>
                    <?php else: ?>
                                        <?= $this->lang->line('not_defined'); ?>
                    <?php endif; ?>
                  </p>
                </div>  
              </div>

              <div class="row hidden">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('tags'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;">
                    <?php if($portfolio[0]['tags'] != "NULL") { echo $portfolio[0]['tags']; } else { echo $this->lang->line('not_defined'); } ?>
                    </p> </div>  
              </div>              
                            
                        </div>  
                        <div class="panel-footer">                      
               <a href="<?= base_url('user-panel-laundry/portfolios'); ?>" class="btn btn-xs btn-info"><?= $this->lang->line('see_more'); ?></a>              
               <button class="btn btn-xs pull-right"><?= $total_portfolios; ?></button>
              </div>
                    <?php else: ?>     
                <div class="panel-body">
                    <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"><p></p></div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7"><p><?= $this->lang->line('no_record_found'); ?></p></div>  
                    </div>
                </div>
                    <?php endif; ?>
          </div>
            </div>
            <!--End: Portfolio Area -->
            
            <!-- Languages Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('languages'); ?> </strong></h4></div>                              
                </div>
              </div>
            
                <div class="panel-body">                            
                            <div class="row">
                                <div class="form-group">
                  <div class="col-lg-10">
                      <div class="input-group">                                         
                        <textarea id="hero-demo" style="min-width: 200px;"></textarea>                      
                        <span id="span_lang" class="text-center"></span>
                      </div>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                            <button id="btn_apply" class="btn btn-sm btn-info"> <?= $this->lang->line('save'); ?></button>
                     </div>
                </div>
                            </div>
                        </div>                                  
                
          </div>
            </div>
            <!--End: Languages Area -->

            <!-- Overview Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('overview'); ?> </strong></h4></div>             
                </div>
              </div>
            
            <div class="panel-body">                            
                            <div class="row">
                                <div class="form-group">
                  <div class="col-lg-10">
                        <textarea id="overview" class="form-control" style="resize: none;" rows="3" placeholder="Enter Overview ...."><?php if($cust['overview'] != "NULL") { echo nl2br($cust['overview']); } ?></textarea>                        
                        <span id="span_overview" class="text-center"></span>
                     </div>
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                            <button id="btn_overview" class="btn btn-sm btn-info"> <?= $this->lang->line('save'); ?></button>
                     </div>
                </div>
                            </div>
                        </div>
                
          </div>
            </div>
            <!--End: Overview Area -->
            
            <!-- Document Area -->
            <div class="row">
                <div class="hpanel hblue">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6"><h4><strong> <?= $this->lang->line('documents'); ?> </strong></h4></div>            
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                    <a href="<?= base_url('user-panel-laundry/document/add'); ?>" class="btn btn-info btn-circle"><i class="fa fa-plus"></i></a>
                    <?php if($total_docs > 0 ): ?>
                        <a href="<?= base_url('user-panel-laundry/documents'); ?>" class="btn btn-danger btn-circle btn-delete""><i class="fa fa-trash"></i></a>
                    <?php endif; ?>
                    </div>
                </div>
              </div>
            
                    <?php if($docs): ?>
                <div class="panel-body">
                            
                            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('name'); ?>:</strong></p> </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">  <p style="word-wrap: break-word;"><?= $docs[0]['doc_title']; ?></p> </div>  
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                  <a class="btn btn-default btn-circle" href="<?= base_url('user-panel-laundry/document/').$docs[0]['doc_id'];?>">
                    <i class="fa fa-pencil"></i>
                  </a>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('description'); ?>:</strong></p> </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">  <p style="word-wrap: break-word;">
                    <?php if( $docs[0]['doc_desc'] != "NULL"){ echo nl2br($docs[0]['doc_desc']); } else { echo 'Not defined'; } ?>
                </p></div>  
              </div>

              <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"> <p><strong><?= $this->lang->line('attachment'); ?>:</strong></p> </div>
                <div class="col-md-4">  <p style="word-wrap: break-word;">
                <?php if($docs[0]['attachement_url'] != "NULL"): ?>
                    <div class="lightBoxGallery">
                        <a href="<?= base_url($docs[0]['attachement_url']); ?>" title="Image from Unsplash" data-gallery="">
                            <img src="<?= base_url($docs[0]['attachement_url']); ?>" class="img-responsive">
                        </a>
                    </div>
                <?php else: ?>
                    <?= $this->lang->line('not_defined'); ?>
                <?php endif; ?>
                </p> </div>  
              </div>
                        </div>          
                        <div class="panel-footer">              
                <a href="<?= base_url('user-panel-laundry/documents'); ?>" class="btn btn-xs btn-info"><?= $this->lang->line('see_more'); ?></a>                  
                <button class="btn btn-xs pull-right"><?= $total_docs; ?></button>
              </div>
                    <?php else: ?>     
                <div class="panel-body">
                    <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5"><p></p></div>
                    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7"><p><?= $this->lang->line('no_record_found'); ?></p></div>  
                    </div>
                </div>
                    <?php endif; ?>
          </div>
            </div>
            <!--End: Document Area -->

            </div>
                    <!--  Collapsible div close  -->

          <div class="button-div text-right" style="float-right">
            <button type="button" id="expand" class="btn btn-info btn-collapsed">See More <i class="fa fa-angle-down" aria-hidden="true"></i> </button>
            <button type="button" id="collapse" class="btn btn-info btn-collapsed" style="display: none">See Less <i class="fa fa-angle-up" aria-hidden="true"></i></button>
                    <br><br>
          </div>

        </div>
        <!-- END: col-lg-8 -->
    </div>
</div>

<script>
    var cache = {};

    function googleSuggest(request, response) {
        var term = request.term;
        if (term in cache) { response(cache[term]); return; }
        $.ajax({
            url: 'get-languages-master',
            type: 'post',
            data: { q: term },
            success: function(data) {
                try { var results = JSON.parse(data); } catch(e) { var results = []; }

                cache[term] = results;
                response(results);
            }
        });
    }

    $(function(){
        <?php 
            if($cust['lang_known'] != "NULL") { $initial_tags = explode(',', $cust['lang_known']); }
            else { $initial_tags = array(); }
        ?>;

        var initial_tags = <?= json_encode($initial_tags); ?>

        // console.log(initial_tags);
        $('#hero-demo').tagEditor({
            initialTags: initial_tags,
            placeholder: 'Enter Languages known ...',
            autocomplete: { source: googleSuggest, minLength: 3, delay: 250, html: true, position: { collision: 'flip' } }
        });

        $("#btn_apply").on('click', function(e) { e.preventDefault();
            var langs = $("#hero-demo").val();            
            if(langs.length > 0 ) {
                $.ajax({
                    url: 'update-languages',
                    type: 'post',
                    data: { langs: langs },
                    success: function(res) {
                      if($.trim(res) == 'success') { 
                        setTimeout(function(){ 
                          $("#span_lang").removeClass('text-danger').addClass('text-success').html("<strong> <?= $this->lang->line('success')?> : </strong><?= $this->lang->line('language_updated_successfully')?>"); 
                        },1500);
                      }
                      else {
                        setTimeout(function(){ 
                          $("#span_lang").removeClass('text-success').addClass('text-danger').html("<strong> <?= $this->lang->line('error')?> : </strong> <?= $this->lang->line('failed_update_language')?>"); 
                        },1500);
                      }
                    }
                });
            }
        });

        $("#btn_overview").on('click', function(e) { e.preventDefault();
            var overview = $("#overview").val();            
            if(overview.length > 0 ) {
                $.ajax({
                    url: 'update-overview',
                    type: 'post',
                    data: { overview: overview },
                    success: function(res) {
                    //console.log(res);
                      if($.trim(res)=='success'){
                        setTimeout(function(){ 
                          $("#span_overview").removeClass('text-danger').addClass('text-success').html("<strong> <?= $this->lang->line('success')?>: </strong> <?= $this->lang->line('overview_update_successfully')?>");
                        },1500);
                      }
                      else { 
                        setTimeout(function(){ 
                          $("#span_overview").removeClass('text-success').addClass('text-danger').html("<strong> <?= $this->lang->line('error')?>: </strong> <?= $this->lang->line('failed_to_update_overview')?>"); 
                        },1500);
                      }
                    }
                });
            }
        });
        
        $('#collapse').click(function(){
            $('#more_details').hide();
            $('#collapse').hide();
            $('#expand').show();
        });
        $('#expand').click(function(){
            $('#more_details').show();
            $('#expand').hide();
            $('#collapse').show();
        });
    });
</script>
