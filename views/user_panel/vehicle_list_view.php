    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('vehicles'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-truck fa-2x text-muted"></i> <?= $this->lang->line('vehicles'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel/vehicle-add'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('available_vehicles'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th><?= $this->lang->line('reg_no'); ?></th>
                                <th><?= $this->lang->line('mark'); ?></th>
                                <th><?= $this->lang->line('symbolic_name'); ?></th>
                                <th><?= $this->lang->line('mark'); ?></th>
                                <th><?= $this->lang->line('transport_by'); ?></th>
                                <th><?= $this->lang->line('transport_mode'); ?></th>
                                <th><?= $this->lang->line('max_volume'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($vehicles as $v) { ?>
                            <tr>
                                <td><a href="<?= $this->config->item('base_url') . 'user-panel/view-vehicle/' . $v['transport_id'] ?>;" title="View details"><?= $v['registration_no'] ?></a></td>
                                <td><?= strtoupper($v['mark']) ?></td>
                                <td><?= strtoupper($v['model']) ?></td>
                                <td><?= strtoupper($v['symbolic_name']) ?></td>
                                <td><?= $this->api->get_vehicle_type_name_by_id($v['vehical_type_id']) ?></td>
                                <td><?= strtoupper($v['transport_mode']) ?></td>
                                <td><?= $v['max_volume'] ?></td>
                                <td class="text-center">
                                    <form action="vehicle-edit" method="post" style="display: inline-block;">
                                        <input type="hidden" name="transport_id" value="<?= $v['transport_id'] ?>">
                                        <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-pencil"></i> <span class="bold"><?= $this->lang->line('edit'); ?></span></button>
                                    </form>
                                    <button style="display: inline-block;" class="btn btn-danger btn-sm vehicledeletealert" id="<?= $v['transport_id'] ?>"><i class="fa fa-trash-o"></i> <span class="bold"><?= $this->lang->line('delete'); ?></span></button>
                                    <button class="btn btn-danger btn-sm inactivatealert hidden"><i class="fa fa-times"></i> <span class="bold"><?= $this->lang->line('inactivate'); ?></span></button>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>