<style>
    #map_wrapper { height: 300px; }
    #map_canvas { width: 100%; height: 100%; }
</style>

<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
            </a>
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry')?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                    <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry-users')?>"><span><?=$this->lang->line('My Dashboard');?></span></a></li>
                    <li class="active"><span><?=$this->lang->line('My Best Customers');?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-certificate fa-2x text-muted"></i> <?= $this->lang->line('My Best Customers'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?=base_url('user-panel-laundry/dashboard-laundry-users')?>" class="btn btn-outline btn-info" ><i class="fa fa-users"></i> <?= $this->lang->line('My Dashboard'); ?></a></h2>
            <small class="m-t-md"><?=$this->lang->line('Best customers as per orders and amount paid');?></small> 
        </div>
    </div>
</div>
<br />
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel hblue">
                <div class="panel-body">
                    <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th><?= $this->lang->line('ID'); ?></th>
                                <th><?= $this->lang->line('Customer Name'); ?></th>
                                <th><?= $this->lang->line('email_address'); ?></th>
                                <th><?= $this->lang->line('contact'); ?></th>
                                <th><?= $this->lang->line('Country'); ?></th>
                                <th><?= $this->lang->line('Orders count'); ?></th>
                                <th><?= $this->lang->line('Amount Paid'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $total_all = $total_accepted = $total_in_progress = $total_completed = $total_cancelled = 0;
                                foreach ($customers_list as $list) { ?>
                            <tr>
                                <td><?=$list['cust_id']?></td>
                                <td><?=($list['walkin_cust_id'] > 0)?'Walking Customer':$list['cust_name']?></td>
                                <td><?=($list['walkin_cust_id'] > 0)?'':$list['cust_email']?></td>
                                <td><?=($list['walkin_cust_id'] > 0)?'':$this->api->get_country_code_by_id($list['country_id']).' '.$list['cust_contact']?></td>
                                <td><?=($list['walkin_cust_id'] > 0)?'':$this->api->get_country_name_by_id($list['country_id'])?></td>
                                <td><?=$list['booking_count']?></td>
                                <td><?=$list['amount_spent']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>