    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Laundry Cancellation Charges'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('Laundry Cancellation Charges'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Country Currency-wise Laundry Cancellation Charges'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel hblue" style="margin-bottom: 5px !important">
                    <div class="panel-body" style="padding-top: 10px;">

                        <?php if($this->session->flashdata('error')):  ?>
                        <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                        <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>

                        <form method="post" class="form-horizontal" action="<?=base_url('user-panel-laundry/laundry-cancellation-charges-add')?>" id="chargeAdd">
                            <div class="row">
                                <div class="col-sm-5">
                                    <label class="text-left"><?= $this->lang->line('select_country'); ?></label>
                                    <select class="form-control js-source-states" name="country_id" id="country_id">
                                        <option value=""><?= $this->lang->line('select_country'); ?></option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?= $country['country_id'] ?>"><?= ucwords($country['country_name']) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-5">
                                    <label class="text-left"><?= $this->lang->line('Cancellation Percentage'); ?></label>
                                    <div class="">
                                        <input type="number" class="form-control" id="charge_per" placeholder="<?= $this->lang->line('Enter Percentage'); ?>" name="charge_per" min="0" max="100" /> 
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label class="text-left">&nbsp;</label>
                                    <div class="">
                                        <button class="btn btn-info form-control" type="submit">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('save'); ?>&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="hpanel hblue">
                    <div class="panel-body" style="padding-top: 10px; padding-bottom: 10px;">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?= $this->lang->line('Country'); ?></th>
                                <th><?= $this->lang->line('Cancellation Percentage'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($cancellation_details as $charge) { static $i=0; $i++; ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= strtoupper($this->api->get_country_name_by_id($charge['country_id'])); ?></td>
                                <td>
                                    <div>
                                        <label id="charge_label_<?=$i?>" class=""><?= $charge['charge_per'] ?></label>
                                        <button style="float: right" class="btn btn-info btn-sm" id="btn-edit-<?=$i?>" title="<?= $this->lang->line('edit'); ?>"><i class="fa fa-pencil"></i></button>
                                    </div>
                                    <div action="#" id="laundry-charge-edit-<?=$i?>" style="display: flex;" class="hidden">
                                        <input type="hidden" name="charge_id_<?=$i?>" value="<?= $charge['charge_id'] ?>" id="charge_id_<?=$i?>">
                                        <input type="number" class="form-control" id="new_percentage_<?=$i?>" placeholder="<?= $this->lang->line('Enter Percentage'); ?>" name="new_percentage" min="0" value="<?=$charge['charge_per']?>" />&nbsp;
                                        <button class="btn btn-info btn-sm" id="btn-save-<?=$i?>" title="<?= $this->lang->line('save'); ?>"><i class="fa fa-save"></i></button>
                                    </div>
                                    <script>
                                        $('#btn-edit-'+<?=$i?>).click(function () {
                                            $('#laundry-charge-edit-'+<?=$i?>).removeClass("hidden");
                                            $('#charge_label_'+<?=$i?>).addClass("hidden");
                                            $('#btn-edit-'+<?=$i?>).addClass("hidden");
                                            $("#tableData").dataTable().fnDestroy()
                                            $("#tableData").dataTable({ });
                                        });

                                        $('#btn-save-'+<?=$i?>).click(function () {
                                            var charge_id = $('#charge_id_'+<?=$i?>).val();
                                            var new_percentage = $('#new_percentage_'+<?=$i?>).val();
                                            if(new_percentage == '') {
                                                swal("<?= $this->lang->line('failed'); ?>", "<?= $this->lang->line('Enter Percentage'); ?>", "error");
                                                $('#new_percentage_'+<?=$i?>).focus();
                                            } else if(parseFloat(new_percentage) < 0) {
                                                swal("<?= $this->lang->line('failed'); ?>", "<?= $this->lang->line('Not less than zero(0)'); ?>", "error");
                                                $('#new_percentage_'+<?=$i?>).focus();
                                            } else if(parseFloat(new_percentage) > 100) {
                                                swal("<?= $this->lang->line('failed'); ?>", "<?= $this->lang->line('Not greater than hundred(100)'); ?>", "error");
                                                $('#new_percentage_'+<?=$i?>).focus();
                                            } else {
                                                $.ajax({
                                                    type: "POST", 
                                                    url: "<?=base_url('user-panel-laundry/update-laundry-cancellation-percentage')?>", 
                                                    data: { charge_id: charge_id, new_percentage: new_percentage },
                                                    dataType: "json",
                                                    success: function(res) {
                                                        swal("<?= $this->lang->line('success'); ?>", "<?= $this->lang->line('updated_successfully'); ?>", "success");
                                                    },
                                                    beforeSend: function(res){
                                                    },
                                                    error: function(res){
                                                        swal("<?= $this->lang->line('failed'); ?>", "<?= $this->lang->line('update_failed'); ?>", "error");
                                                    }
                                                });
                                                $('#laundry-charge-edit-'+<?=$i?>).addClass("hidden");
                                                $('#charge_label_'+<?=$i?>).removeClass("hidden");
                                                $('#charge_label_'+<?=$i?>).text(new_percentage);
                                                $('#btn-edit-'+<?=$i?>).removeClass("hidden");
                                                $("#tableData").dataTable().fnDestroy()
                                                $("#tableData").dataTable({ });
                                            }
                                        });
                                    </script>
                                </td>
                                <td class="text-center">
                                    <button style="display: inline-block;" class="btn btn-danger btn-sm chargeDeleteAlert" id="<?= $charge['charge_id'] ?>"><i class="fa fa-trash-o"></i> <span class="bold"><?= $this->lang->line('Delete'); ?></span></button>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.chargeDeleteAlert').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('you_will_not_be_able_to_recover_this_record'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_delete_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false 
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?=base_url('user-panel-laundry/laundry-cancellation-charges-delete')?>", {charge_id: id}, function(res){
                        console.log(res);
                        if(res == 'success') {
                            swal("<?= $this->lang->line('deleted'); ?>", "<?= $this->lang->line('Deleted successfully.'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        } else {
                            swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('While deleting record!'); ?>", "error");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        }
                    });
                } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('Record is safe!'); ?>", "error"); }
            });
        });

        $("#chargeAdd").validate({
            ignore: [], 
            rules: {
                country_id: { required : true },
                charge_per: { required : true, min: 0, max:100 },
            },
            messages: {
                country_id: { required : <?= json_encode($this->lang->line('select_country'));?>, },
                charge_per: { required : <?= json_encode($this->lang->line('Enter Percentage'));?>, min: <?= json_encode($this->lang->line('Not less than zero(0)'));?>, max: <?= json_encode($this->lang->line('Not greater than hundred(100)'));?> },
            },
        });
    </script>