    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-pick-drop-point-list'; ?>"><span><?= $this->lang->line('List of Pickup Drop Locations'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Add Location'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Add Location'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Add location details with pickup and drop points'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <!-- Add New Points -->
        <div class="row">
            <div class="col-lg-12">
              <div class="hpanel hblue">
                <form action="<?= base_url('user-panel-bus/bus-pick-drop-location-and-point-add-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="addPoint">
                    <div class="panel-body">
                        <?php if($this->session->flashdata('error')):  ?>
                            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <h3><?= $this->lang->line('Location Details'); ?></h3>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class=""><?= $this->lang->line('Enter Location Name'); ?></label>
                                <input type="text" name="city_name" class="form-control" id="city_name" placeholder="<?=$this->lang->line('Enter Location Name')?>">
                            </div>
                            <div class="col-md-3">
                                <label class="text-left"><?= $this->lang->line('Select Vehicle Type'); ?></label>
                                <select class="form-control js-source-states" name="vehical_type_id" id="vehical_type_id">
                                    <option value=""><?= $this->lang->line('Select Vehicle Type'); ?></option>
                                    <?php foreach ($vehicle_types as $type) { ?>
                                        <option value="<?= $type['vehical_type_id'] ?>"><?= $type['vehicle_type'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="map_canvas_location hidden" style="width: 100%; height: 200px;"></div>
                            <input type="hidden" data-geo-location="lat" name="latitude_location" id="latitude_location" />
                            <input type="hidden" data-geo-location="lng" name="longitude_location" id="longitude_location" />
                            <input type="hidden" data-geo-location="country" name="country_location" id="country_location" />
                            <input type="hidden" data-geo-location="administrative_area_level_1" name="state_location" id="state_location" />
                            <input type="hidden" data-geo-location="locality" name="city_location" id="city_location" />
                            <input type="hidden" data-geo-location="formatted_address" name="street_location" id="street_location" />
                            <input type="hidden" data-geo-location="postal_code" name="zipcode_location" id="zipcode_location" />
                            <input type="hidden" name="no_of_points" id="no_of_points" value="1" />
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <h3><?= $this->lang->line('Pickup Drop Points'); ?></h3>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-4">
                                <label class=""><?= $this->lang->line('Enter Point Name'); ?></label>
                                <input type="text" name="point_address_1" class="form-control" id="point_address_1" placeholder="<?=$this->lang->line('Type pickup/drop point name')?>">
                            </div>
                            <div class="col-md-4">
                                <label class=""><?= $this->lang->line('Enter landmark'); ?></label>
                                <input type="text" name="point_landmark_1" class="form-control" id="point_landmark_1" placeholder="<?=$this->lang->line('Landmark')?>">
                            </div>
                            <div class="col-md-3">
                                <label><?= $this->lang->line('Point Type'); ?></label><br />
                                <div class="checkbox checkbox-success checkbox-inline">
                                    <input type="checkbox" id="point_is_pickup_1" name="point_is_pickup_1" aria-label="is_pickup" />
                                    <label for="point_is_pickup_1"> <?= $this->lang->line('Pickup Point'); ?> </label>
                                </div>
                                <div class="checkbox checkbox-success checkbox-inline">
                                  <input type="checkbox" id="point_is_drop_1" name="point_is_drop_1" aria-label="is_drop" />
                                  <label for="point_is_drop_1"> <?= $this->lang->line('Drop Point'); ?> </label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                &nbsp;
                            </div>
                            <div class="map_canvas_point_1 hidden" style="width: 100%; height: 200px;"></div>
                            <input type="hidden" data-geo-point_1="lat" name="point_latitude_1" id="point_latitude_1" />
                            <input type="hidden" data-geo-point_1="lng" name="point_longitude_1" id="point_longitude_1" />
                            <input type="hidden" data-geo-point_1="country" name="point_country_1" id="point_country_1" />
                            <input type="hidden" data-geo-point_1="administrative_area_level_1" name="point_state_1" id="point_state_1" />
                            <input type="hidden" data-geo-point_1="locality" name="point_city_1" id="point_city_1" />
                            <input type="hidden" data-geo-point_1="formatted_address" name="point_street_1" id="point_street_1" />
                            <input type="hidden" data-geo-point_1="postal_code" name="point_zipcode_1" id="point_zipcode_1" />
                            <div class="col-md-12">
                                <hr />
                            </div>
                        </div>
                        <div class="wrapperField">
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <a class="btn btn-warning" data-style="zoom-in" id="btn_add_point"><?= $this->lang->line('Add More Point'); ?></a>
                                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('Add Location'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>

    <script>
        $("#city_name").geocomplete({
            map:".map_canvas_location",
            location: "",
            mapOptions: { zoom: 11, scrollwheel: true, },
            markerOptions: { draggable: false, },
            details: "form",
            detailsAttribute: "data-geo-location", 
            types: ["geocode", "establishment"],
        });
    </script>

    <script>
        $("#point_address_1").geocomplete({
            map:".map_canvas_point_1",
            location: "",
            mapOptions: { zoom: 11, scrollwheel: true, },
            markerOptions: { draggable: false, },
            details: "form",
            detailsAttribute: "data-geo-point_1", 
            types: ["geocode", "establishment"],
        });
    </script>

    <script>
        var btn_counter = 2; 
        $(function(){
            var wrapper = $(".wrapperField");
            var add_button = $("#btn_add_point");

            $(add_button).click(function(e) { 
                e.preventDefault();
                $("#no_of_points").val(btn_counter);

                $(wrapper).append(
                    '<div class="row form-group">'+

                        '<div class="col-md-4">'+
                          '<label><?= $this->lang->line("Enter Point Name"); ?></label>'+
                            '<input type="text" name="point_address_'+btn_counter+'" class="form-control" id="point_address_'+btn_counter+'" placeholder="<?=$this->lang->line("Type pickup/drop point name")?>" />'+
                        '</div>'+

                        '<div class="col-md-4">'+
                            '<label><?= $this->lang->line("Enter landmark"); ?></label>'+
                            '<input type="text" name="point_landmark_'+btn_counter+'" class="form-control" id="point_landmark_'+btn_counter+'" placeholder="<?=$this->lang->line("Landmark")?>">'+
                        '</div>'+
                        
                        '<div class="col-md-3">'+
                            '<label><?= $this->lang->line("Point Type"); ?></label><br />'+
                            '<div class="checkbox checkbox-success checkbox-inline">'+
                                '<input type="checkbox" id="is_pickup_'+btn_counter+'" name="point_is_pickup_'+btn_counter+'" aria-label="is_pickup" />'+
                                '<label for="is_pickup"> <?= $this->lang->line("Pickup Point"); ?> </label>'+
                            '</div>'+
                            '<div class="checkbox checkbox-success checkbox-inline">'+
                              '<input type="checkbox" id="is_drop_'+btn_counter+'" name="point_is_drop_'+btn_counter+'" aria-label="is_drop" />'+
                              '<label for="is_drop"> <?= $this->lang->line("Drop Point"); ?> </label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-1"><br />'+
                            '<a class="remove_field pull-right btn btn-danger" data-toggle="tooltip" data-placement="top" title="<?=$this->lang->line("Remove Point")?>" data-original-title="<?=$this->lang->line("Remove Point")?>"><i class="fa fa-trash"></i></a>'+
                        '</div>'+
                        '<div class="col-md-12">'+
                            '<hr />'+
                        '</div>'+
                        '<div class="map_canvas_point_'+btn_counter+' hidden" style="width: 100%; height: 200px;"></div>'+
                        '<input type="hidden" data-geo-point_'+btn_counter+'="lat" name="point_latitude_'+btn_counter+'" id="point_latitude_'+btn_counter+'" />'+
                        '<input type="hidden" data-geo-point_'+btn_counter+'="lng" name="point_longitude_'+btn_counter+'" id="point_longitude_'+btn_counter+'" />'+
                        '<input type="hidden" data-geo-point_'+btn_counter+'="country" name="point_country_'+btn_counter+'" id="point_country_'+btn_counter+'" />'+
                        '<input type="hidden" data-geo-point_'+btn_counter+'="administrative_area_level_1" name="point_state_'+btn_counter+'" id="point_state_'+btn_counter+'" />'+
                        '<input type="hidden" data-geo-point_'+btn_counter+'="locality" name="point_city_'+btn_counter+'" id="point_city_'+btn_counter+'" />'+
                        '<input type="hidden" data-geo-point_'+btn_counter+'="formatted_address" name="point_street_'+btn_counter+'" id="point_street_'+btn_counter+'" />'+
                        '<input type="hidden" data-geo-point_'+btn_counter+'="postal_code" name="point_zipcode_'+btn_counter+'" id="point_zipcode_'+btn_counter+'" />'+
                    '</div>'+

                    '<script>'+
                        '$("#point_address_'+btn_counter+'").geocomplete({'+
                            'map:".map_canvas_point_'+btn_counter+'",'+
                            'location: "",'+
                            'mapOptions: { zoom: 11, scrollwheel: true, },'+
                            'markerOptions: { draggable: false, },'+
                            'details: "form",'+
                            'detailsAttribute: "data-geo-point_'+btn_counter+'", '+
                            'types: ["geocode", "establishment"],'+
                        '});'+
                    '<'+'/'+'script>'+
                    '<script>'+
                        '$("#point_address_'+btn_counter+'").rules("add", { required : true, messages: { required: "<?= $this->lang->line('Enter Point Name'); ?>",} } );'+
                        '$("#point_landmark_'+btn_counter+'").rules("add", { required : true, messages: { required: "<?= $this->lang->line('Enter landmark'); ?>",} } );'+
                    '<'+'/'+'script>'
                );
                btn_counter++;
            });

            $(wrapper).on("click",".remove_field", function(e) { e.preventDefault(); 
              $(this).parent().parent().remove(); 
              var pointCounter = $("#no_of_points").val();
              pointCounter -= 1;
              $("#no_of_points").val(pointCounter);
              if(btn_counter > 1 ) { } else { btn_counter = 0; } 
            });
        });
    </script>

    <script type="text/javascript">
      $("#addPoint")
        .validate({
          ignore: [],
          rules: {
            city_name: { required : true },
            vehical_type_id: { required : true },
            point_address_1: { required : true, },
            point_landmark_1: { required : true },
          },
          messages: {
            city_name: { required : "<?= $this->lang->line('Enter Location Name'); ?>", },
            vehical_type_id: { required : "<?= $this->lang->line('Select Vehicle Type'); ?>", },
            point_address_1: { required : "<?= $this->lang->line('Enter Point Name'); ?>", },
            point_landmark_1: { required : "<?= $this->lang->line('Enter landmark'); ?>", },
          },
        });
    </script>