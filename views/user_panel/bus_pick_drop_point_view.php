    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-pick-drop-point-list'; ?>"><span><?= $this->lang->line('List of Pickup Drop Locations'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Pickup Drop Points'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Pickup Drop Points'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('List of Pickup Drop Points'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <!-- Add New Points -->
        <div class="row">
            <div class="col-lg-12">
              <div class="hpanel hblue">
                <form action="<?= base_url('user-panel-bus/bus-pick-drop-point-add-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="addPoint">
                    <div class="panel-header">
                    <h2><?= $this->lang->line('Add New Point'); ?> - <b><?=$loc_city_name?></b></h2>
                    </div>
                    <div class="panel-body">
                        <?php if($this->session->flashdata('error')):  ?>
                            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label class=""><?= $this->lang->line('Enter Point Name'); ?></label>
                                <input type="text" name="point_address" class="form-control" id="point_address" placeholder="<?=$this->lang->line('Type pickup/drop point name')?>">
                            </div>
                            <div class="col-md-3">
                                <label class=""><?= $this->lang->line('Enter landmark'); ?></label>
                                <input type="text" name="point_landmark" class="form-control" id="point_landmark" placeholder="<?=$this->lang->line('Landmark')?>">
                            </div>
                            <div class="col-md-3">
                                <label><?= $this->lang->line('Point Type'); ?></label><br />
                                <div class="checkbox checkbox-success checkbox-inline">
                                    <input type="checkbox" id="is_pickup" name="is_pickup" aria-label="is_pickup" />
                                    <label for="is_pickup"> <?= $this->lang->line('Pickup Point'); ?> </label>
                                </div>
                                <div class="checkbox checkbox-success checkbox-inline">
                                  <input type="checkbox" id="is_drop" name="is_drop" aria-label="is_drop" />
                                  <label for="is_drop"> <?= $this->lang->line('Drop Point'); ?> </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <br />
                                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('Add Point'); ?></button>
                            </div>
                        </div>
                        <input type="hidden" name="loc_id" id="loc_id" value="<?=$loc_id?>" />
                        <input type="hidden" name="loc_city_name" id="loc_city_name" value="<?=$loc_city_name?>" />
                        
                        <div id="map" class="map_canvas hidden" style="width: 100%; height: 200px;"></div>
                        <input type="hidden" data-geo="lat" name="latitude" id="latitude" value="" />
                        <input type="hidden" data-geo="lng" name="longitude" id="longitude" value="" />
                        <input type="hidden" data-geo="formatted_address" id="street" name="street" class="form-control" placeholder="<?= $this->lang->line('street_name'); ?>" />
                        <input type="hidden" data-geo="postal_code" id="zipcode" name="zipcode" class="form-control" placeholder="<?= $this->lang->line('zip_code'); ?>"  />
                        <input type="hidden" data-geo="country" id="country" name="country" class="form-control" placeholder="<?= $this->lang->line('country'); ?>" readonly/>
                        <input type="hidden" data-geo="administrative_area_level_1" id="state" name="state" class="form-control" id="tlaceholder="<?= $this->lang->line('state'); ?>" readonly/>
                        <input type="hidden" data-geo="locality" id="city" name="city" class="form-control" placeholder="<?= $this->lang->line('city'); ?>" readonly/>
                    </div>
                </form>
              </div>
            </div>
        </div>

        <!-- Points of Locations -->
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-header">
                    <h2><?= $this->lang->line('Pickup Drop Points'); ?></h2>
                    </div>
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th><?= $this->lang->line('Point ID'); ?></th>
                                <th><?= $this->lang->line('Point Name'); ?></th>
                                <th><?= $this->lang->line('Is Pickup'); ?></th>
                                <th><?= $this->lang->line('Is Drop'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($points as $point) { ?>
                            <tr>
                                <td><?= $point['point_id'] ?></td>
                                <td><?= ucwords($point['point_landmark']) ?></td>
                                <td><?php if($point['is_pickup']=='1') { ?>
                                        Yes <button class="btn btn-danger btn-sm removeAsPickup" style="float: right;" id="<?= $point['point_id'] ?>"><i class="fa fa-times"></i> <?= $this->lang->line('Remove as pickup'); ?></button>
                                    <?php } else { ?>
                                        No <button class="btn btn-success btn-sm markAsPickup" style="float: right;" id="<?= $point['point_id'] ?>"><i class="fa fa-check"></i> <?= $this->lang->line('Make as pickup'); ?></button>
                                    <?php } ?>
                                </td>
                                <td><?php if($point['is_drop']=='1') { ?>
                                        Yes <button class="btn btn-danger btn-sm removeAsDrop" style="float: right;" id="<?= $point['point_id'] ?>"><i class="fa fa-times"></i> <?= $this->lang->line('Remove as drop'); ?></button>
                                    <?php } else { ?>
                                        No <button class="btn btn-success btn-sm markAsDrop" style="float: right;" id="<?= $point['point_id'] ?>"><i class="fa fa-check"></i> <?= $this->lang->line('Make as drop'); ?></button>
                                    <?php } ?>
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-danger btn-sm removePoint" id="<?= $point['point_id'] ?>" title="<?= $this->lang->line('Delete Point'); ?>"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.removeAsPickup').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('Point will remove as pickup!'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('Yes remove it!'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?=base_url('user-panel-bus/point-remove-as-pickup')?>", {id: id}, function(res){
                            swal("<?= $this->lang->line('Removed'); ?>", "<?= $this->lang->line('Point removed as pickup!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error");
                    }
                });
            });

        $('.markAsPickup').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('Point will mark as pickup!'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('Yes mark it!'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?=base_url('user-panel-bus/point-mark-as-pickup')?>", {id: id}, function(res){
                            swal("<?= $this->lang->line('Marked!'); ?>", "<?= $this->lang->line('Point marked as pickup!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error");
                    }
                });
            });

        $('.removeAsDrop').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('Point will remove as drop!'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('Yes remove it!'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?=base_url('user-panel-bus/point-remove-as-drop')?>", {id: id}, function(res){
                            swal("<?= $this->lang->line('Removed'); ?>", "<?= $this->lang->line('Point removed as drop!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error");
                    }
                });
            });

        $('.markAsDrop').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('Point will mark as drop!'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('Yes mark it!'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?=base_url('user-panel-bus/point-mark-as-drop')?>", {id: id}, function(res){
                            swal("<?= $this->lang->line('Marked!'); ?>", "<?= $this->lang->line('Point marked as drop!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error");
                    }
                });
            });

        $('.removePoint').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('Point will be deleted!'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('Yes delete it!'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?=base_url('user-panel-bus/bus-location-point-delete')?>", {id: id}, function(res){
                            swal("<?= $this->lang->line('Deleted!'); ?>", "<?= $this->lang->line('Point deleted!'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error");
                    }
                });
            });
    </script>
    <script>
        $("#point_address").geocomplete({
            map:".map_canvas",
            location: "",
            mapOptions: { zoom: 11, scrollwheel: true, },
            markerOptions: { draggable: false, },
            details: "form",
            detailsAttribute: "data-geo", 
            types: ["geocode", "establishment"],
        });
    </script>

    <script type="text/javascript">
      $("#addPoint")
        .validate({
          ignore: [],
          rules: {
            point_address: { required : true },
            point_landmark: { required : true },
          },
          messages: {
            point_address: { required : "<?= $this->lang->line('Enter Point Name'); ?>", },
            point_landmark: { required : "<?= $this->lang->line('Enter landmark'); ?>", },
          },
        });
    </script>