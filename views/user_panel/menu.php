<?php  $email_cut = explode('@', $cust['email1']);  $name = $email_cut[0];  ?>

<style> .avatar { width: 85%; } </style>
<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
    <?php $url = parse_url($cust['avatar_url']);?>
    <div class="profile-picture">
        <a>
          <?php if($cust['avatar_url'] == "NULL"): ?>
            <a href="<?= $this->config->item('base_url') . 'user-panel/user-profile'; ?>"><img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b avatar" alt="avatar" /></a>
          <?php else: ?>
            <a href="<?= $this->config->item('base_url') . 'user-panel/user-profile'; ?>"><img src="<?= ( isset($url['scheme']) AND ($url['scheme'] == "https" || $url['scheme'] == "http" )) ? $cust['avatar_url'] : base_url($cust['avatar_url']); ?>" class="img-thumbnail m-b avatar" alt="avatar" /></a>
          <?php endif; ?>
        </a>

        <div class="stats-label text-color">
        <?php if( $cust['firstname'] != "NULL" || $cust['lastname'] != "NULL") : ?>
            <span class="font-extra-bold font-uppercase"><?= $cust['firstname'] . ' ' .$cust['lastname']; ?></span>
        <?php else: ?>
            <span class="font-extra-bold font-uppercase"><?= $name; ?></span>
        <?php endif; ?>
            <div class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <small class="text-muted"><?= $this->lang->line('profile'); ?> <b class="caret"></b></small>                      
                </a>
                <ul class="dropdown-menu animated flipInX m-t-xs">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/user-profile'; ?>"><?= $this->lang->line('edit_profile'); ?></a></li>
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/change-password'; ?>"><?= $this->lang->line('change_password'); ?></a></li>
                    <li class="divider"></li>
                    <li><a href="<?= base_url('log-out'); ?>"><?= $this->lang->line('logout'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

        <ul class="nav" id="side-menu">
            <?php 
                $method_name = $this->router->fetch_method(); 
                $dashboard = array('dashboard');

                $address_book = array('address_book','add_address_book_view','edit_address_book_view');
                //Deliverer Profile
                $basic_info = array('deliverer_profile','edit_deliverer_profile','edit_deliverer_bank','edit_deliverer_document');
                $business_photo = array('business_photos');
                $vehicles = array('vehicle_list','vehicle_add','vehicle_edit');
                $service_area = array('service_area_list','service_area_add');
                $drivers = array('driver_list','add_driver','driver_inactive_list','edit_driver');
                //Standard Rate
                $standard_rates = array('standard_rates_list','add_volume_based','add_weight_based','add_formula_volume_based','add_formula_weight_based','edit_standard_rates');
                $ptop_rates = array('ptop_rates_list','add_ptop_volume_based','add_ptop_weight_based','add_ptop_formula_volume_based','add_ptop_formula_weight_based','edit_ptop_rates');

                //Bookings
                $open_booking = array('my_bookings','add_bookings','requested_deliverer','find_deliverer','my_booking_order_details');
                $accept_booking = array('accepted_bookings','accept_booking_order_details');
                $inprogress_booking = array('in_progress_bookings','in_progress_orders_details');
                $delivered_booking = array('delivered_bookings','delivered_booking_details');
                $favorite_deliverer = array('favourite_deliverers');
                //Orders
                $browse_bookings = array('open_bookings','open_order_details');
                $order_requested = array('order_requests','requested_order_details');
                $order_accepted = array('accepted_orders','assign_driver');
                $order_inprogress = array('in_progress_orders','in_progress_order_details');
                $order_delivered = array('delivered_orders','delivered_order_details');
                //Accounts
                $wallet = array('my_wallet','create_withdraw_request','user_account_history');
                $scrow = array('my_scrow','user_scrow_history');
                $withdraw = array('my_withdrawals_request','my_withdrawals_accepted','my_withdrawals_rejected','withdrawal_request_details','withdraw_request_sent');
                $support_request = array('get_support_list','get_support','view_support_reply_details');
            ?>
            <?php 
                $orderCount = 0;
                if(isset($_SESSION['last_login_date_time'])) {
                    $orderCount = $this->api->requested_order_count($_SESSION['cust_id'], $_SESSION['last_login_date_time']); } ?>

            <li class="<?php if(in_array($method_name, $dashboard)) { echo 'active'; } ?>">
                <a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"> <span class="nav-label"><?= $this->lang->line('dash'); ?></span> </a>
            </li>

            <li class="<?php if(in_array($method_name, $address_book)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/address-book'; ?>"><?= $this->lang->line('address_book'); ?></a>
            </li>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
	            <li class="<?php if(in_array($method_name, $basic_info) || in_array($method_name, $business_photo) || in_array($method_name, $vehicles) || in_array($method_name, $service_area) || in_array($method_name, $drivers)) { echo 'active'; } ?>">
	                <a href="#"><span class="nav-label"><?= $this->lang->line('deliverer_profile'); ?></span><span class="fa arrow"></span> </a>
	                <ul class="nav nav-second-level">
	                    <li class="<?php if(in_array($method_name, $basic_info)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/deliverer-profile'; ?>"><?= $this->lang->line('basic_info'); ?></a></li>
	                    <li class="<?php if(in_array($method_name, $business_photo)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/business-photos'; ?>"><?= $this->lang->line('business_photos'); ?></a></li>
	                    <li class="<?php if(in_array($method_name, $vehicles)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/vehicle-list'; ?>"><?= $this->lang->line('vehicles'); ?></a></li>
	                    <li class="<?php if(in_array($method_name, $service_area)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/service-area'; ?>"><?= $this->lang->line('service_area'); ?></a></li>
	                    <li class="<?php if(in_array($method_name, $drivers)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/driver-list'; ?>"><?= $this->lang->line('drivers'); ?></a></li>
	                </ul>
	            </li>
	        <?php } ?>

            <?php if( $cust['is_dedicated'] == 1 && ($cust['acc_type'] == 'both' || $cust['acc_type'] == 'buyer') ) { ?>
            <li class="<?php if(in_array($method_name, $standard_rates) || in_array($method_name, $ptop_rates)) { echo 'active'; } ?>">
                <a href="#"><span class="nav-label"><?= $this->lang->line('Define Rates'); ?></span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li class="<?php if(in_array($method_name, $standard_rates)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/standard-rates-list'; ?>"><?= $this->lang->line('Standard Rates'); ?></a></li>
                    <li class="<?php if(in_array($method_name, $ptop_rates)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/ptop-rates-list'; ?>"><?= $this->lang->line('Point-to-Point Rates'); ?></a></li>
                </ul>
            </li>
            <?php } ?>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'buyer') { ?>
            <li class="<?php if(in_array($method_name, $open_booking) || in_array($method_name, $accept_booking) || in_array($method_name, $inprogress_booking) || in_array($method_name, $delivered_booking) || in_array($method_name, $favorite_deliverer)) { echo 'active'; } ?>">
                <a href="#"><span class="nav-label"><?= $this->lang->line('my_bookings'); ?></span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li class="<?php if(in_array($method_name, $open_booking)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/my-bookings'; ?>"><?= $this->lang->line('open'); ?></a></li>
                    <li class="<?php if(in_array($method_name, $accept_booking)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/accepted-bookings'; ?>"><?= $this->lang->line('accepted'); ?></a></li>
                    <li class="<?php if(in_array($method_name, $inprogress_booking)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/in-progress-bookings'; ?>"><?= $this->lang->line('in_progress'); ?></a></li>
                    <li class="<?php if(in_array($method_name, $delivered_booking)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/delivered-bookings'; ?>"><?= $this->lang->line('delivered'); ?></a></li>
                    <li class="<?php if(in_array($method_name, $favorite_deliverer)) { echo 'active'; } ?>"><a href="<?= base_url('user-panel/favourite-deliverers'); ?>"><span class="nav-label"><?= $this->lang->line('favourite_deliverers'); ?></span></a></li>
                </ul>
            </li>
            <?php } ?>

            <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?>
	            <li class="<?php if(in_array($method_name, $browse_bookings) || in_array($method_name, $order_requested) || in_array($method_name, $order_accepted) || in_array($method_name, $order_inprogress) || in_array($method_name, $order_delivered)) { echo 'active'; } ?>">
	                <a href="#"><span class="nav-label"><?= $this->lang->line('orders'); ?> <span class="label label-success"><?php if($orderCount > 0) { echo $orderCount; } ?></span> </span> <span class="fa arrow"></span> </a>
	                <ul class="nav nav-second-level">
                        <li class="<?php if(in_array($method_name, $browse_bookings)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/open-bookings'; ?>"><?= $this->lang->line('browse_bookings'); ?></a></li>
	                    <li class="<?php if(in_array($method_name, $order_requested)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/order-requests'; ?>"><?= $this->lang->line('request'); ?> <span class="label label-success pull-right"><?php if($orderCount > 0) { echo $orderCount; } ?></span> </a></li>
	                    <li class="<?php if(in_array($method_name, $order_accepted)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/accepted-orders'; ?>"><?= $this->lang->line('accepted'); ?></a></li>
	                    <li class="<?php if(in_array($method_name, $order_inprogress)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/in-progress-orders'; ?>"><?= $this->lang->line('in_progress'); ?></a></li>
	                    <li class="<?php if(in_array($method_name, $order_delivered)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/delivered-orders'; ?>"><?= $this->lang->line('delivered'); ?></a></li>
	                </ul>
	            </li>
	        <?php } ?>

            <li class="<?php if(in_array($method_name, $wallet) || in_array($method_name, $scrow) || in_array($method_name, $withdraw)) { echo 'active'; } ?>">
                <a href="#"><span class="nav-label"><?= $this->lang->line('my_accounts'); ?></span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li class="<?php if(in_array($method_name, $wallet)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/my-wallet'; ?>"><?= $this->lang->line('e_wallet'); ?></a></li>
                    <?php if($cust['acc_type'] == 'both' || $cust['acc_type'] == 'seller') { ?><li class="<?php if(in_array($method_name, $scrow)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/my-scrow'; ?>"><?= $this->lang->line('scrow_account'); ?></a></li><?php } ?>
                    <li class="<?php if(in_array($method_name, $withdraw)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/my-withdrawals-request'; ?>"><?= $this->lang->line('withdrawals'); ?></a></li>
                </ul>
            </li>

            <li class="<?php if(in_array($method_name, $support_request)) { echo 'active'; } ?>"><a href="<?= $this->config->item('base_url') . 'user-panel/get-support-list'; ?>"><?= $this->lang->line('get_support'); ?></a>
            </li>
        </ul>
    </div>
</aside>

<!-- Main Wrapper -->

<?php
$method_name = $this->router->fetch_method(); 
if($method_name == 'dashboard') { ?>
    <div id="wrapper" style="background: url(<?=base_url('resources/courier-bg.jpg')?>); background-repeat: no-repeat; background-size: 100%;" >
<?php } else { ?>
    <div id="wrapper" style="" >
<?php } ?>