<style>
    #map_wrapper { height: 300px; }
    #map_canvas { width: 100%; height: 100%; }
</style>

<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
            </a>
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry')?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                    <li><a href="<?=base_url('user-panel-laundry/dashboard-laundry-users')?>"><span><?=$this->lang->line('My Dashboard');?></span></a></li>
                    <li class="active"><span><?=$this->lang->line('Current week laundry transactions');?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-shopping-basket fa-2x text-muted"></i> <?= $this->lang->line('Current week laundry transactions'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?=base_url('user-panel-laundry/dashboard-laundry-users')?>" class="btn btn-outline btn-info" ><i class="fa fa-users"></i> <?= $this->lang->line('My Dashboard'); ?></a></h2>
            <small class="m-t-md"><?=$this->lang->line('All order transactions for this week');?></small> 
        </div>
    </div>
</div>
<br />
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel hblue" style="margin-bottom: 10px !important">
                <div class="panel-body">
                    <div class="row">
                            <div class="col-md-3">
                                <label><?=$this->lang->line('Filter by currency');?></label><br />
                                <small>(<?=$this->lang->line('Get transaction total by currency filter');?>)</small>
                            </div>
                            <div class="col-md-6">
                                <form action="<?=base_url('user-panel-laundry/current-week-laundry-sales')?>" method="post">
                                    <select class="form-control select2" name="currency_sign" id="currency_sign">
                                        <option value="NULL"><?=$this->lang->line('Select Currency');?></option>
                                        <?php foreach ($currencies as $currency) { ?>
                                            <option value="<?=$currency['currency_sign']?>" <?=($currency_sign==$currency['currency_sign'])?'selected':''?> ><?=$currency['currency_title'].' ['.$currency['currency_sign'].']'?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-3" style="display: inline-flex;">
                                    <button type="submit" class="btn btn-info"><?=$this->lang->line('Filter');?></button>
                                </form>&nbsp;
                                <form action="<?=base_url('user-panel-laundry/current-week-laundry-sales')?>" method="post">
                                    <input type="hidden" name="currency_sign" value="NULL" />
                                    <button type="submit" class="btn btn-warning"><?=$this->lang->line('reset');?></button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>

            <div class="hpanel hblue">
                <div class="panel-body">
                    <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th><?= $this->lang->line('Tr. ID'); ?></th>
                                <th><?= $this->lang->line('Tr. Date Time'); ?></th>
                                <th><?= $this->lang->line('order_id'); ?></th>
                                <th><?= $this->lang->line('transaction'); ?></th>
                                <th><?= $this->lang->line('amount'); ?></th>
                                <th>+</th>
                                <th>-</th>
                                <th><?= $this->lang->line('currency'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $income = $expense = 0;
                                foreach ($week_transaction_list as $transaction) { ?>
                                <tr style="background-color: <?= $transaction['type'] == '1' ? '#DFFFDF': '#FFDDDD'; ?>">
                                    <td><?=$transaction['ah_id']?></td>
                                    <td><?=date('d-M-Y h:i:s A',strtotime($transaction['datetime']))?></td>
                                    <td><?=$transaction['order_id']?></td>
                                    <td><?=ucwords(str_replace('_',' ',$transaction['transaction_type']))?></td>
                                    <td><?=$transaction['amount']?></td>
                                    <td><?=($transaction['type']==1)?$transaction['amount']:0?></td>
                                    <td><?=($transaction['type']==0)?$transaction['amount']:0?></td>
                                    <td><?=$transaction['currency_code']?></td>
                                </tr>
                            <?php $transaction['type'] == '1' ? $income += $transaction['amount']: $expense += $transaction['amount']; } ?>
                        </tbody>
                        <?php if($currency_sign!='NULL'): ?>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><?= $this->lang->line('total'); ?></td>
                                    <td><?=$income?></td>
                                    <td><?=$expense?></td>
                                    <td><?=$currency_sign?></td>
                                </tr>
                            </tfoot>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>