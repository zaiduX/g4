<?php 
  $driver_id = $keys["driver_id"]; 
  $cust_id = $keys["cust_id"]; 
  $order_id = $keys["order_id"]; 
?>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><?= $this->lang->line('dash'); ?></li>
          <li class="active"><span><?= $this->lang->line('chat_room'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-comments fa-2x text-muted"></i> <?= $this->lang->line('chat_room'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Chat with Driver'); ?></small>  
    </div>
  </div>
</div>
<style>.chat-users, .chat-discussion{height:400px;}</style>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="hpanel ">
        <div class="panel-body no-padding">
          <div class="row">
            <div class="col-md-12">
              <ul class="chat-discussion">    
                <?php $total = COUNT($chats); foreach($chats as $v => $w): ?>
                  <?php date_default_timezone_set($this->session->userdata("default_timezone")); $ud = strtotime($w['cre_datetime']); ?>
                  <?php date_default_timezone_set($this->session->userdata("user_timezone")); ?>
                <li class="chat-message <?= ($w['sender_id'] == $w['cust_id'])?'right':'left'; ?>" <?=($total==($v+1))?"tabindex='1'":'';?>>
                  <div class="message">
                    <?php if($w['sender_id'] == $w['cust_id']): ?>
                      <a class="message-author text-right"><?php if(strtoupper($w['cust_name']) == 'NULL' || strtoupper($w['cust_name']) == 'NULL NULL') { echo 'Customer'; } else { echo strtoupper($w['cust_name']); } ?> </a>
                      <span class="message-date">  <?= date('l, M / d / Y  h:i a', $ud); ?></span>
                      <div class="row">
                        <div class="col-md-3 text-left">
                          <?php if($w['attachment_url'] !="NULL"): ?> 
                            <p>&nbsp;</p>
                            <p><a href="<?= base_url().$w['attachment_url'];?>" target='_blank'> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a><p>
                          <?php endif; ?>
                        </div>
                        <div class="col-md-9">
                          <span class="message-content">
                            <h5><?= $this->lang->line('msg'); ?></h5> 
                            <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                          </span>
                        </div>                          
                      </div>
                    <?php else: ?>
                      <a class="message-author text-left"><?php if(strtoupper($w['cd_name']) == 'NULL' || strtoupper($w['cd_name']) == 'NULL NULL') { echo 'Driver'; } else { echo strtoupper($w['cd_name']); } ?> </a>
                      <span class="message-date">  <?= date('l, M / d / Y  h:i a', $ud); ?></span>
                      <div class="row">
                        <div class="col-md-9">
                          <span class="message-content">
                            <h5><?= $this->lang->line('msg'); ?></h5> 
                            <p><?= (trim($w["text_msg"])!= "NULL")? trim($w["text_msg"]) : ""; ?></p>
                          </span>
                        </div>
                        <div class="col-md-3 text-left">
                          <?php if($w['attachment_url'] !="NULL"): ?> 
                            <p>&nbsp;</p>
                            <p><a href="<?= base_url().$w['attachment_url'];?>" target='_blank'> <?= $this->lang->line('download'); ?> <i class="fa fa-download"></i></a><p>
                          <?php endif; ?>
                        </div>
                      </div>
                    <?php endif; ?>
                  </div>
                </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="panel-footer borders">
          <div class="row form-group">
            <div id="chat_div" class="col-lg-12">
              <form action="<?=base_url('user-panel-laundry/add-order-chat');?>" id="chatForm" method="post" enctype="multipart/form-data">
                <input type="hidden" name="order_id" value="<?= $order_id; ?>" />
                <div class="input-group">
                  <span class="input-group-btn">
                    <button id="upload_file" class="btn fa fa-paperclip fa-2x"></button>                            
                  </span>
                  <input type="text" name="message_text" class="form-control" placeholder="<?= $this->lang->line('type_your_message'); ?>">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-success"><?= $this->lang->line('Send'); ?></button>
                  </span>
                </div>
                <span id="file_name"></span>
                <input type="file" id="attachment" name="attachment" class="upload" accept="image/*" onchange="attachement_name(event)" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>