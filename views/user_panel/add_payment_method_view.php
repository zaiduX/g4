<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('profile'); ?></span></li>
          <li><span><?= $this->lang->line('details'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('add_payment_methods'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-photo fa-2x text-muted"></i> <?= $this->lang->line('payment_methods'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('add_payment_methods_details'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel/add-payment-method'); ?>" method="post" class="form-horizontal" id="addPayment">          
          <div class="panel-body">              
            <div class="col-lg-10 col-lg-offset-1">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
            
              <div class="row"> 
                <div class="form-group">
                  <div class="col-lg-6">
                    <label class=""><?= $this->lang->line('payment_method'); ?> :</label>
                    <select id="pay_method" name="pay_method" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('payment_method'); ?>">
                      <option value="0"><?= $this->lang->line('select_payment_method'); ?></option>
                      <option value="Credit Card"><?= $this->lang->line('credit_card'); ?></option>
                      <option value="Debit Card"><?= $this->lang->line('debit_card'); ?></option>
                      <option value="Mobile Wallet"><?= $this->lang->line('mobile_money'); ?></option>
                      <option value="Bank Account"><?= $this->lang->line('bank_account_[iban'); ?></option>
                    </select>
                  </div>
                  <div class="col-lg-6">
                    <label class=""><?= $this->lang->line('brand_name'); ?></label>
                    <select id="brand_name" name="brand_name" class="form-control select2" data-allow-clear="true" data-placeholder="Select Brand Name">
                      <option value=""><?= $this->lang->line('Select_Brand_Name'); ?></option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row"> 
                <div class="form-group">
                  <div class="col-lg-6">
                    <label class=""><?= $this->lang->line('card_number'); ?> / <?= $this->lang->line('mobile_wallet_no'); ?> / <?= $this->lang->line('bank_account_[iban'); ?></label>
                    <input type="text" name="card_no"  class="form-control"  placeholder="<?= $this->lang->line('card_number'); ?>" required />
                  </div>
                  <div class="col-lg-6">
                    <label class=""><?= $this->lang->line('expiry_date'); ?></label>
                    <div class="row">
                      <div class="col-lg-6">
                        <select name="month" class="form-control select2" id="month">
                          <?php $months = range(1 ,12); ?>
                          <option value=""><?= $this->lang->line('select_month'); ?></option>
                          <?php foreach ($months as $m): ?>
                            <option value="<?= $m; ?>"><?= str_pad($m, 2,0,STR_PAD_LEFT); ?></option>
                          <?php endforeach; ?>                        
                        </select> 
                      </div>
                      <div class="col-lg-6">
                        <select name="year" class="form-control select2" id="year">
                          <?php $years = range(date("y") ,date("y") + 30); ?>
                          <option value=""><?= $this->lang->line('select_year'); ?></option>
                          <?php foreach ($years as $y): ?>
                            <option value="<?= $y; ?>"><?= $y; ?></option>
                          <?php endforeach; ?>                        
                        </select> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row"> 
                <div class="form-group">
                  <div class="col-lg-6">
                    <label class=""><?= $this->lang->line('bank_name'); ?></label>
                    <input type="text" name="bank_name" class="form-control"  placeholder="<?= $this->lang->line('bank_name'); ?>" />
                  </div>
                  <div class="col-lg-6">
                    <label class=""><?= $this->lang->line('bank_address'); ?></label>
                    <input type="text" name="bank_address" class="form-control" placeholder="<?= $this->lang->line('bank_address'); ?>" />
                  </div>
                </div>
              </div>

              <div class="row"> 
                <div class="form-group">
                  <div class="col-lg-6">
                    <label class=""><?= $this->lang->line('bank_short_code_swift_code'); ?></label>
                    <input type="text" name="bank_short_code" class="form-control" placeholder="<?= $this->lang->line('bank_short_code_swift_code'); ?>" />
                  </div>
                  <div class="col-lg-6">
                    <label class=""><?= $this->lang->line('account_owner_name'); ?></label>
                    <input type="text" name="account_title" class="form-control" placeholder="<?= $this->lang->line('account_owner_name'); ?>" />
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-lg-6 text-left">
                  <a href="<?= base_url('user-panel/user-profile'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_profile'); ?></a>                            
               </div>
               <div class="col-lg-6 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('submit_detail'); ?></button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
    $("#addPayment").validate({
      ignore: [],
      rules: {
      pay_method: { required: true, },
      brand_name: { required: true,},
      card_no: { required: true, number:true },
      month: { required: true, },
      year: { required: true, },
      }, 
      messages: {
      pay_method: { required: "Select Payment Method.",   },
      brand_name: { required: "Select brand Name.",},
      card_no: { required: "Enter card number.", number:"Enter valid card number." },
      month: { required: "Select month.",  },
      year: { required: "Select year.",  },
      }
    });

    $(document).ready(function(){ 
      $('#pay_method').change(function(){ 
        var pay_method = $('#pay_method :selected').val();
        if(pay_method == 0) {
          $("#brand_name option[value='Orange']").remove();
          $("#brand_name option[value='MTN']").remove();
          $("#brand_name option[value='Stripe']").remove();
          $('input[name="bank_name"]').prop("readonly", false);
          $('input[name="bank_address"]').prop("readonly", false);
          $('input[name="account_title"]').prop("readonly", false);
          $('input[name="bank_short_code"]').prop("readonly", false);
        } else if(pay_method == 'Mobile Wallet') {
          $("#s2id_brand_name").select2('enable');
          $("#brand_name option[value='Stripe']").remove();
          $('#brand_name').append($('<option>', {
            value: 'Orange',
            text: "<?= $this->lang->line('orange'); ?>"
          }));
          $('#brand_name').append($('<option>', {
            value: 'MTN',
            text: "<?= $this->lang->line('mtn'); ?>"
          }));
          $('input[name="bank_name"]').prop("readonly", true);
          $('input[name="bank_address"]').prop("readonly", true);
          $('input[name="account_title"]').prop("readonly", true);
          $('input[name="bank_short_code"]').prop("readonly", true);
          $("#s2id_month").select2('disable');
          $("#s2id_year").select2('disable');
        } else if(pay_method == 'Bank Account') {
          $("#s2id_brand_name").select2('disable');
          $("#s2id_month").select2('disable');
          $("#s2id_year").select2('disable');
          $('input[name="bank_name"]').prop("readonly", false);
          $('input[name="bank_address"]').prop("readonly", false);
          $('input[name="account_title"]').prop("readonly", false);
          $('input[name="bank_short_code"]').prop("readonly", false);
        } else {
          $("#s2id_brand_name").select2('enable');
          $("#s2id_month").select2('enable');
          $("#s2id_year").select2('enable');
          $("#brand_name option[value='Orange']").remove();
          $("#brand_name option[value='MTN']").remove();
          $("#brand_name option[value='Stripe']").remove();
          $('#brand_name').append($('<option>', {
            value: 'Stripe',
            text: "<?= $this->lang->line('stripe'); ?>"
        }));
          // make read only
          $('input[name="bank_name"]').prop("readonly", true);
          $('input[name="bank_name"]').val('');
          $('input[name="bank_address"]').prop("readonly", true);
          $('input[name="bank_address"]').val('');
          $('input[name="account_title"]').prop("readonly", true);
          $('input[name="account_title"]').val('');
          $('input[name="bank_short_code"]').prop("readonly", true);
          $('input[name="bank_short_code"]').val('');
        }
      });
  });

</script>