 <style type="text/css">
  #sticky {
    padding: 0.5px;
    background-color: #225595;
    color: #fff;
    font-size: 1em;
    border-radius: 0.5ex;
}

#sticky.stick {
    margin-top: 133px !important;
    position: fixed;
    top: 0;
    z-index: 10000;
    border-radius: 0 0 0.5em 0.5em;
    width: 79%;
}
</style>
    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-list'; ?>"><?= $this->lang->line('buses'); ?></a></li>
                <li class="active"><span><?= $this->lang->line('add_bus'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light-xs">  <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('buses'); ?> </h2>
          <small class="m-t-md"><?= $this->lang->line('add_bus_details'); ?></small>    
        </div>
      </div>
    </div>
     
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">

                        <?php if($this->session->flashdata('error')):  ?>
                            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div><br />
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div><br />
                        <?php endif; ?>

                        <form method="post" class="form-horizontal" action="group-permission-add-details" id="permissionAdd" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="group_name" class="col-sm-2 control-label"><?=$this->lang->line('Agent Group Name')?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="group_name" placeholder="<?=$this->lang->line('Enter group name!')?>" name="group_name" />    
                                </div>
                            </div>

                            <div id="sticky-anchor" class="form-group" style="margin-left: 0px; margin-right: 0px;">
                            </div>
                              
                            <div id="sticky" class="form-group" style="margin-left: 0px; margin-right: 0px;">
                                <label class="col-sm-2" style="text-align: right;"><?=$this->lang->line('Functionality')?></label>
                                <label class="col-sm-2" style="text-align: center;"><?=$this->lang->line('View')?></label>
                                <label class="col-sm-2" style="text-align: center;"><?=$this->lang->line('Add')?></label>
                                <label class="col-sm-2" style="text-align: center;"><?=$this->lang->line('Edit')?></label>
                                <label class="col-sm-2" style="text-align: center;"><?=$this->lang->line('Activate/Deactivate')?></label>
                                <label class="col-sm-2" style="text-align: center;"><?=$this->lang->line('Delete')?></label>
                            </div>

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('Select All'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" id="all_view"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" id="all_add"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" id="all_edit"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" id="all_activate"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" id="all_delete"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('dash'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="dashboard_view" id="dashboard_view" value="1"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('address_book'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="address_book_view" id="address_book_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="address_book_add" id="address_book_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="address_book_update" id="address_book_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="address_book_delete" id="address_book_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('basic_info'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="basic_info_view" id="basic_info_view" value="1"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="basic_info_update" id="basic_info_update" value="3"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('business_photos'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="business_photos_view" id="business_photos_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="business_photos_add" id="business_photos_add" value="2"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="business_photos_delete" id="business_photos_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('manage_bus'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="manage_bus_view" id="manage_bus_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="manage_bus_add" id="manage_bus_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="manage_bus_update" id="manage_bus_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="manage_bus_delete" id="manage_bus_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('operating_locations'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="operating_locations_view" id="operating_locations_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="operating_locations_add" id="operating_locations_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="operating_locations_update" id="operating_locations_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="operating_locations_delete" id="operating_locations_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('drivers'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="drivers_view" id="drivers_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="drivers_add" id="drivers_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="drivers_update" id="drivers_update" value="3"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="drivers_active" id="drivers_active" value="4"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('Agent Permissions'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="agent_permissions_view" id="agent_permissions_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="agent_permissions_add" id="agent_permissions_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="agent_permissions_update" id="agent_permissions_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="agent_permissions_delete" id="agent_permissions_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('agents'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="agents_view" id="agents_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="agents_add" id="agents_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="agents_update" id="agents_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="agents_delete" id="agents_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('cancellation_recheduling'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="cancel_reschedule_view" id="cancel_reschedule_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="cancel_reschedule_add" id="cancel_reschedule_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="cancel_reschedule_update" id="cancel_reschedule_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="cancel_reschedule_delete" id="cancel_reschedule_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('special_rates'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="special_rates_view" id="special_rates_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="special_rates_add" id="special_rates_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="special_rates_update" id="special_rates_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="special_rates_delete" id="special_rates_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('my_trips'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="my_trips_view" id="my_trips_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="my_trips_add" id="my_trips_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="my_trips_update" id="my_trips_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="my_trips_delete" id="my_trips_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('manage_trips'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="manage_trip_view" id="manage_trip_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="manage_trip_add" id="manage_trip_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="manage_trip_update" id="manage_trip_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="manage_trip_delete" id="manage_trip_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('my_accounts'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="my_accounts_view" id="my_accounts_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="my_accounts_add" id="my_accounts_add" value="2"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="my_accounts_update" id="my_accounts_update" value="3"></div>
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="my_accounts_delete" id="my_accounts_delete" value="5"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('notifications'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="view_notifications_view" id="view_notifications_view" value="1"></div>
                            </div>
                            <hr />

                            <div class="form-group text-center">
                                <label class="col-sm-2 control-label"><?= $this->lang->line('work_room'); ?></label>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="work_room_view" id="work_room_view" value="1"></div>
                                <div class="col-sm-2"><input type="checkbox" class="i-checks" name="work_room_add" id="work_room_add" value="2"></div>
                            </div>
                            <hr />

                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <br>
                                    <button class="btn btn-primary" type="submit"><?= $this->lang->line('Create Group'); ?></button>
                                    <a href="<?= $this->config->item('base_url') . 'user-panel-bus/agent-permissions-list'; ?>" class="btn btn-info" type="submit"><?= $this->lang->line('back'); ?></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $("#permissionAdd")
    .validate({
        ignore: [], 
        rules: {
            group_name: { required : true },
        },
        messages: {
            group_name: { required : <?= json_encode($this->lang->line('Enter group name!'));?>, },
        },
    });
</script>

<script>
    $('#all_view').on('ifChanged', function (event) { 
        if ($(this).is(':checked')) {
            $('#dashboard_view').prop('checked',true).iCheck('update'); 
            $('#address_book_view').prop('checked',true).iCheck('update'); 
            $('#basic_info_view').prop('checked',true).iCheck('update'); 
            $('#business_photos_view').prop('checked',true).iCheck('update'); 
            $('#manage_bus_view').prop('checked',true).iCheck('update'); 
            $('#operating_locations_view').prop('checked',true).iCheck('update'); 
            $('#drivers_view').prop('checked',true).iCheck('update'); 
            $('#agent_permissions_view').prop('checked',true).iCheck('update'); 
            $('#agents_view').prop('checked',true).iCheck('update'); 
            $('#cancel_reschedule_view').prop('checked',true).iCheck('update'); 
            $('#special_rates_view').prop('checked',true).iCheck('update'); 
            $('#my_trips_view').prop('checked',true).iCheck('update'); 
            $('#manage_trip_view').prop('checked',true).iCheck('update'); 
            $('#my_accounts_view').prop('checked',true).iCheck('update'); 
            $('#view_notifications_view').prop('checked',true).iCheck('update'); 
            $('#work_room_view').prop('checked',true).iCheck('update'); 
        } else {
            $('#dashboard_view').prop('checked',false).iCheck('update');
            $('#address_book_view').prop('checked',false).iCheck('update'); 
            $('#basic_info_view').prop('checked',false).iCheck('update'); 
            $('#business_photos_view').prop('checked',false).iCheck('update'); 
            $('#manage_bus_view').prop('checked',false).iCheck('update'); 
            $('#operating_locations_view').prop('checked',false).iCheck('update'); 
            $('#drivers_view').prop('checked',false).iCheck('update'); 
            $('#agent_permissions_view').prop('checked',false).iCheck('update'); 
            $('#agents_view').prop('checked',false).iCheck('update'); 
            $('#cancel_reschedule_view').prop('checked',false).iCheck('update'); 
            $('#special_rates_view').prop('checked',false).iCheck('update'); 
            $('#my_trips_view').prop('checked',false).iCheck('update'); 
            $('#manage_trip_view').prop('checked',false).iCheck('update'); 
            $('#my_accounts_view').prop('checked',false).iCheck('update'); 
            $('#view_notifications_view').prop('checked',false).iCheck('update'); 
            $('#work_room_view').prop('checked',false).iCheck('update'); 
        }
    });

    $('#all_add').on('ifChanged', function (event) { 
        if ($(this).is(':checked')) {
            $('#address_book_add').prop('checked',true).iCheck('update'); 
            $('#business_photos_add').prop('checked',true).iCheck('update'); 
            $('#manage_bus_add').prop('checked',true).iCheck('update'); 
            $('#operating_locations_add').prop('checked',true).iCheck('update'); 
            $('#drivers_add').prop('checked',true).iCheck('update'); 
            $('#agent_permissions_add').prop('checked',true).iCheck('update'); 
            $('#agents_add').prop('checked',true).iCheck('update'); 
            $('#cancel_reschedule_add').prop('checked',true).iCheck('update'); 
            $('#special_rates_add').prop('checked',true).iCheck('update'); 
            $('#my_trips_add').prop('checked',true).iCheck('update'); 
            $('#manage_trip_add').prop('checked',true).iCheck('update'); 
            $('#my_accounts_add').prop('checked',true).iCheck('update'); 
            $('#work_room_add').prop('checked',true).iCheck('update'); 
        } else {
            $('#address_book_add').prop('checked',false).iCheck('update'); 
            $('#business_photos_add').prop('checked',false).iCheck('update'); 
            $('#manage_bus_add').prop('checked',false).iCheck('update'); 
            $('#operating_locations_add').prop('checked',false).iCheck('update'); 
            $('#drivers_add').prop('checked',false).iCheck('update'); 
            $('#agent_permissions_add').prop('checked',false).iCheck('update'); 
            $('#agents_add').prop('checked',false).iCheck('update'); 
            $('#cancel_reschedule_add').prop('checked',false).iCheck('update'); 
            $('#special_rates_add').prop('checked',false).iCheck('update'); 
            $('#my_trips_add').prop('checked',false).iCheck('update'); 
            $('#manage_trip_add').prop('checked',false).iCheck('update'); 
            $('#my_accounts_add').prop('checked',false).iCheck('update'); 
            $('#work_room_add').prop('checked',false).iCheck('update'); 
        }
    });

    $('#all_edit').on('ifChanged', function (event) { 
        if ($(this).is(':checked')) {
            $('#address_book_update').prop('checked',true).iCheck('update'); 
            $('#basic_info_update').prop('checked',true).iCheck('update'); 
            $('#manage_bus_update').prop('checked',true).iCheck('update'); 
            $('#operating_locations_update').prop('checked',true).iCheck('update'); 
            $('#drivers_update').prop('checked',true).iCheck('update'); 
            $('#agent_permissions_update').prop('checked',true).iCheck('update'); 
            $('#agents_update').prop('checked',true).iCheck('update'); 
            $('#cancel_reschedule_update').prop('checked',true).iCheck('update'); 
            $('#special_rates_update').prop('checked',true).iCheck('update'); 
            $('#my_trips_update').prop('checked',true).iCheck('update'); 
            $('#manage_trip_update').prop('checked',true).iCheck('update'); 
            $('#my_accounts_update').prop('checked',true).iCheck('update'); 
        } else {
            $('#address_book_update').prop('checked',false).iCheck('update'); 
            $('#basic_info_update').prop('checked',false).iCheck('update'); 
            $('#manage_bus_update').prop('checked',false).iCheck('update'); 
            $('#operating_locations_update').prop('checked',false).iCheck('update'); 
            $('#drivers_update').prop('checked',false).iCheck('update'); 
            $('#agent_permissions_update').prop('checked',false).iCheck('update'); 
            $('#agents_update').prop('checked',false).iCheck('update'); 
            $('#cancel_reschedule_update').prop('checked',false).iCheck('update'); 
            $('#special_rates_update').prop('checked',false).iCheck('update'); 
            $('#my_trips_update').prop('checked',false).iCheck('update'); 
            $('#manage_trip_update').prop('checked',false).iCheck('update'); 
            $('#my_accounts_update').prop('checked',false).iCheck('update'); 
        }
    });

    $('#all_activate').on('ifChanged', function (event) { 
        if ($(this).is(':checked')) {
            $('#drivers_active').prop('checked',true).iCheck('update'); 
        } else {
            $('#drivers_active').prop('checked',false).iCheck('update'); 
        }
    });

    $('#all_delete').on('ifChanged', function (event) { 
        if ($(this).is(':checked')) {
            $('#address_book_delete').prop('checked',true).iCheck('update'); 
            $('#business_photos_delete').prop('checked',true).iCheck('update'); 
            $('#manage_bus_delete').prop('checked',true).iCheck('update'); 
            $('#operating_locations_delete').prop('checked',true).iCheck('update'); 
            $('#agent_permissions_delete').prop('checked',true).iCheck('update'); 
            $('#agents_delete').prop('checked',true).iCheck('update'); 
            $('#cancel_reschedule_delete').prop('checked',true).iCheck('update'); 
            $('#special_rates_delete').prop('checked',true).iCheck('update'); 
            $('#my_trips_delete').prop('checked',true).iCheck('update'); 
            $('#manage_trip_delete').prop('checked',true).iCheck('update'); 
            $('#my_accounts_delete').prop('checked',true).iCheck('update'); 
        } else {
            $('#address_book_delete').prop('checked',false).iCheck('update'); 
            $('#business_photos_delete').prop('checked',false).iCheck('update'); 
            $('#manage_bus_delete').prop('checked',false).iCheck('update'); 
            $('#operating_locations_delete').prop('checked',false).iCheck('update'); 
            $('#agent_permissions_delete').prop('checked',false).iCheck('update'); 
            $('#agents_delete').prop('checked',false).iCheck('update'); 
            $('#cancel_reschedule_delete').prop('checked',false).iCheck('update'); 
            $('#special_rates_delete').prop('checked',false).iCheck('update'); 
            $('#my_trips_delete').prop('checked',false).iCheck('update'); 
            $('#manage_trip_delete').prop('checked',false).iCheck('update');  
            $('#my_accounts_delete').prop('checked',false).iCheck('update');  
        }
    });
</script>

<script>
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var div_top = $('#sticky-anchor').offset().top;
        if (window_top > div_top) {
            $('#sticky').addClass('stick');
            $('#sticky-anchor').height($('#sticky').outerHeight());
        } else {
            $('#sticky').removeClass('stick');
            $('#sticky-anchor').height(0);
        }
    }

    $(function() {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });

    var dir = 1;
    var MIN_TOP = 200;
    var MAX_TOP = 350;

    function autoscroll() {
        var window_top = $(window).scrollTop() + dir;
        if (window_top >= MAX_TOP) {
            window_top = MAX_TOP;
            dir = -1;
        } else if (window_top <= MIN_TOP) {
            window_top = MIN_TOP;
            dir = 1;
        }
        $(window).scrollTop(window_top);
        window.setTimeout(autoscroll, 100);
    }
</script>