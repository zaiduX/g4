    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Agents'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Agents'); ?> &nbsp;&nbsp;&nbsp;
            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-operator-agent-add'; ?>" class="btn btn-outline btn-info" ><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?> </a> <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-operator-agent-active-list'; ?>" class="btn btn-outline btn-warning" ><i class="fa fa-times"></i> <?= $this->lang->line('Active Agents'); ?> </a> </h2>
          <small class="m-t-md"><?= $this->lang->line('List of inactive agents'); ?></small>    
        </div>
      </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th><?= $this->lang->line('Agent ID'); ?></th>
                                <th><?= $this->lang->line('Agent Name'); ?></th>
                                <th><?= $this->lang->line('Contact'); ?></th>
                                <th><?= $this->lang->line('Email'); ?></th>
                                <th><?= $this->lang->line('Gender') ?></th>
                                <th><?= $this->lang->line('Country'); ?></th>
                                <th class="text-center"><?= $this->lang->line('action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($agents as $agent) { ?>
                            <tr>
                                <td><?= $agent['agent_id'] ?></td>
                                <td><?= ucwords($agent['firstname']) . ' ' . ucwords($agent['lastname']) ?></td>
                                <td><?= $this->api->get_country_code_by_id($agent['country_id']). ' ' .$agent['mobile1'] ?></td>
                                <td><?= $agent['email1'] ?></td>
                                <td><?= ($agent['gender']=='M')?'Male':'Female'; ?></td>
                                <td><?= $this->user->get_country_name_by_id($agent['country_id']) ?></td>
                                <td class="text-center">
                                    <button class="btn btn-danger btn-sm busAgentActivateAlert" id="<?= $agent['agent_id'] ?>" title="<?= $this->lang->line('Activate'); ?>"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.busAgentActivateAlert').click(function () {
            var id = this.id;
            swal({
                title: "<?= $this->lang->line('are_you_sure'); ?>",
                text: "<?= $this->lang->line('your_record_will_be_activated'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->lang->line('yes_activate_it'); ?>",
                cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?=base_url('user-panel-bus/bus-operator-agent-activate')?>", {id: id}, function(res){
                            swal("<?= $this->lang->line('activated'); ?>", "<?= $this->lang->line('record_has_been_activated'); ?>", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                        });
                    } else {
                        swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('no_change'); ?>", "error");
                    }
                });
            });
    </script>