<style> textarea { resize: none; } </style>
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span><?= $this->lang->line('Passengers List'); ?></span></li>
          <li class="active"><span><?= $this->lang->line('Edit Passenger'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-address-book fa-2x text-muted"></i> <?= $this->lang->line('Edit Passenger'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Update Passenger Details'); ?></small>    
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel-bus/bus-address-book-edit-details'); ?>" method="post" class="form-horizontal" id="editAddress" enctype="multipart/form-data">        
          <div class="panel-body">              
            <div class="col-lg-12">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <input type="hidden" name="address_id" value="<?=$address['address_id']?>">
              <div class="row">
                <div class="form-group">
                  <div class="col-lg-12">
                    <div class="col-lg-3">
                      <label class=""><?= $this->lang->line('contact_first_name'); ?></label>
                      <input type="text" id="firstname" name="firstname" class="form-control" placeholder="<?= $this->lang->line('enter_first_name'); ?>" value="<?=$address['firstname']?>" />
                    </div>
                    <div class="col-lg-3">
                      <label class=""><?= $this->lang->line('contact_last_name'); ?></label>
                      <input type="text" name="lastname" class="form-control" id="lastname" placeholder="<?= $this->lang->line('enter_last_name'); ?>" value="<?=$address['lastname']?>" />
                    </div>
                    <div class="col-lg-3">
                      <label><?= $this->lang->line('email_address'); ?></label>
                      <input type="email" class="form-control" id="email_id" name="email_id" placeholder="<?= $this->lang->line('enter_email_address'); ?>" value="<?=$address['email_id']?>" />
                    </div>
                    <div class="col-lg-3">
                      <label><?= $this->lang->line('gender'); ?></label>
                      <select class="form-control" name="gender">
                        <option value=""><?= $this->lang->line('Select gender'); ?></option>
                        <option value="M" <?=($address['gender']=='M')?'selected':''?>><?= $this->lang->line('male'); ?></option>
                        <option value="F" <?=($address['gender']=='F')?'selected':''?>><?= $this->lang->line('female'); ?></option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="form-group">
                  <div class="col-lg-12">
                    <div class="col-lg-3">
                      <label class=""><?= $this->lang->line('Select Country'); ?></label>
                      <select class="form-control select2" name="country_id">
                        <option value=""><?= $this->lang->line('Select Country'); ?></option>
                        <?php foreach ($countries as $c) :?>
                          <option value="<?= $c['country_id'];?>" <?=($address['country_id']==$c['country_id'])?'selected':''?>><?=$c['country_name'];?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                      <input type="text" id="mobile" name="mobile" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number'); ?>" value="<?=$address['mobile']?>" />
                    </div>
                    <div class="col-lg-3">
                      <!-- <label class=""><?= $this->lang->line('DoB'); ?></label>
                      <input type="number" id="age" name="age" class="form-control" placeholder="<?= $this->lang->line('Enter age'); ?>" value="<?=$address['age']?>" /> -->
          
                      <label class=""><?= $this->lang->line('DoB'); ?></label>
                      <div class="input-group date">
                        <input type="text" class="form-control" id="age" name="age" value="<?=$address['dob']?>" autocomplete="off" >
                        <div class="input-group-addon dpAddon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-lg-6 text-left">
                  <a href="<?= base_url('user-panel-bus/address-book-list'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_address_book'); ?></a>
               </div>
               <div class="col-lg-6 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('save_address'); ?></button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(function() {  
    $("#editAddress").validate({
      ignore: [],
      rules: {
        firstname: { required: true, },      
        lastname: { required: true, },
        email_id: { email: true, },      
        gender: { required: true, },      
        country: { required: true, },      
        mobile: { required: true, number: true, },      
        age: { required: true, },
      }, 
      messages: {
        firstname: { required: <?= json_encode($this->lang->line('enter_first_name')); ?>,   },
        lastname: { required: <?= json_encode($this->lang->line('enter_last_name')); ?>,   },
        email_id: { email: <?= json_encode($this->lang->line('Email is invalid!')); ?>,   },
        gender: { required: <?= json_encode($this->lang->line('Select gender')); ?>,   },
        country: { required: <?= json_encode($this->lang->line('Select Country')); ?>  },
        mobile: { required: <?= json_encode($this->lang->line('enter_mobile_number')); ?>, number: <?= json_encode($this->lang->line('number_only')); ?>,  },
        age: { required: <?= json_encode($this->lang->line('Enter age')); ?>, number: <?= json_encode($this->lang->line('number_only')); ?>,   },
      }
    });
  });
 $(window).load(function() {
    $('#age').datepicker({
      autoclose: true,
    }, 'setEndDate', new Date(), );
  });

  $( ".dpAddon" ).click(function( e ) { 
    $("#age").focus();
  });
</script>


