    
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>">Dashboard</a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/my-bookings'; ?>">Bookings</a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/add-bookings'; ?>">Create Bookings</a></li>
                        <li class="active"><span>Booking Advance Payment</span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Booking Advance Payment Details
                </h2>
            </div>
        </div>
    </div>
        
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <form method="post" class="form-horizontal" action="<?= $this->config->item('base_url') . 'user-panel/courier-payment'; ?>">

                            <div class="form-group">
                                <h2 class="col-sm-12 text-center">The cost will be</h2>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6 text-center col-sm-offset-3">
                                    <div class="hpanel stats">
                                        <div class="panel-body">

                                            <div>
                                                <i class="pe-7s-cash fa-5x"></i>
                                                <h1 class="m-xs text-success">$75</h1>
                                            </div>

                                            <p>
                                                You will only be debited at the end of the service. Your payment is just put in a scrow by a secure way and your money belongs to you until the end of the service.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <label>Advance</label>
                                    <input type="text" placeholder="Advacnce Amount" class="form-control m-b" name="advance" id="advance">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <label>At Pick-up</label>
                                    <input type="text" placeholder="Pick-up Amount" class="form-control m-b" name="pickup" id="pickup">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <label>At Deliver</label>
                                    <input type="text" placeholder="Delivere Amount" class="form-control m-b" name="deliver" id="deliver">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-3 text-center">
                                    <button class="btn btn-primary" type="submit">Continue to pay</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>