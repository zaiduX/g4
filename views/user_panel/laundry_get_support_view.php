    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/get-support-list'; ?>"><?= $this->lang->line('all_your_queries_and_suggestions'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('get_support'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('post_your_query'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('post_your_query_line'); ?></small>
            </div>
        </div>
    </div>
    
    <div class="content">
        <div class="row">
            <div class="hpanel">
                <div class="panel-body">
                    <form method="post" class="form-horizontal" action="<?= base_url('user-panel-laundry/add-support-details') ?>" enctype="multipart/form-data" id="suggestionAdd">
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <?php if($this->session->flashdata('error')):  ?>
                                <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                                <?php endif; ?>
                                <?php if($this->session->flashdata('success')):  ?>
                                <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                                <?php endif; ?>  
                            </div>
                        </div>

                        <div class="form-group">
                        	<div class="col-md-9 col-md-offset-1">
                                <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('request_category'); ?></label>
                                    <div class="col-md-8">
                                        <select name="type" class="form-control" id="type">
                                        	<option value=""><?= $this->lang->line('select_category'); ?></option>
                                        	<option value="incident"><?= $this->lang->line('incident'); ?></option>
                                        	<option value="suggestion"><?= $this->lang->line('suggestion'); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9 col-md-offset-1 text-right">
                                <div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('enter_details'); ?></label>
                                    <div class="col-md-8">
                                        <textarea placeholder="<?= $this->lang->line('enter_details'); ?>" class="form-control m-b" name="description" id="description"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9 col-md-offset-1 text-center">
                            	<div class="form-group"><label class="col-md-4 control-label"><?= $this->lang->line('select_document'); ?></label>
				                  <div class="input-group">
				                  	<div class="col-md-8">
				                  		<input type="file" name="attachment" id="attachment_file" style="font-size: initial;" />
					                </div>
                            	</div>
                            </div>

                            <div class="col-md-11 col-md-offset-1 text-right">
	                            <div class="col-md-12">
                                    <button class="btn btn-info" type="submit" ><?= $this->lang->line('send_details'); ?></button> &nbsp;
                                    <a href="<?= $this->config->item('base_url') . 'user-panel-laundry/get-support-list'; ?>" class="btn btn-primary" type="submit"><?= $this->lang->line('back'); ?></a>
	                            </div>
	                        </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $("#suggestionAdd")
            .validate({
                ignore: [],
                rules: {
                    type: { required : true },
                    description: { required : true },
                },
                messages: {
                    type: { required : "<?= $this->lang->line('please_select_category'); ?>", },
                    description: { required : "<?= $this->lang->line('please_enter_description'); ?>", },
                },
            });
    </script>