    <style type="text/css">
        #available[]-error {
            margin-top: 20px;
        }
    </style>
    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-master-list'; ?>"><span><?= $this->lang->line('Trip Master'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Edit Trip'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Edit Trip'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Edit trip and availability details'); ?></small>    
        </div>
      </div>
    </div>
    <?php $avail = explode(',', $trip_details[0]['trip_availability'] );  ?>
    <div class="content">
        <!-- Add New Points -->
        <div class="row">
            <div class="col-lg-12">
              <div class="hpanel hblue">
                <form action="<?= base_url('user-panel-bus/bus-trip-master-edit-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="tripAdd">
                    <input type="hidden" name="trip_id" value="<?=$trip_details[0]['trip_id']?>">
                    <div class="panel-body">
                        <?php if($this->session->flashdata('error')):  ?>
                            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <h3><?= $this->lang->line('Trip Details'); ?></h3>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="col-md-8">
                                    <label class=""><?= $this->lang->line('Select Bus'); ?></label>
                                    <select class="form-control select2" name="bus_id" id="bus_id">
                                        <option value=""><?= $this->lang->line('Select Bus'); ?></option>
                                        <?php foreach ($buses as $bus) { ?>
                                            <option value="<?= $bus['bus_id'] ?>" <?=($trip_details[0]['bus_id']==$bus['bus_id']) ? 'selected' : ''; ?> ><?= $bus['bus_modal'].' ['.$bus['bus_make'].'] ['.$bus['bus_seat_type'].', '.$bus['bus_ac'].']' ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class=""><?= $this->lang->line('Booking Type'); ?></label>
                                    <select class="form-control select2" name="booking_type" id="booking_type">
                                        <option value=""><?= $this->lang->line('Select booking type'); ?></option>
                                        <option value="online" <?=($trip_details[0]['booking_type']=='online') ? 'selected' : ''; ?>><?= $this->lang->line('Online + Internal'); ?></option>
                                        <option value="internal" <?=($trip_details[0]['booking_type']=='internal') ? 'selected' : ''; ?>><?= $this->lang->line('Internal'); ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <?php foreach ($seat_type_price as $price): ?>
                                    <div class="col-md-4">
                                        <label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Price'); ?></label>
                                        <input type="hidden" name="price_id[]" value="<?=$price['price_id']?>">
                                        <input class="form-control" type="text" name="seat_type_price[]" value="<?=$price['seat_type_price']?>" required="required">
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <label class=""><?= $this->lang->line('Trip Availability'); ?></label> &nbsp;<small style="color: red;">*(<?= $this->lang->line('Select atleast 1 day!'); ?>)</small><br />
                                    <label> <input type="checkbox" class="i-checks" name="available[]" value="sun" <?=(in_array('sun', $avail))? 'checked' : '';?> > <?= $this->lang->line('sunday'); ?> </label>&nbsp;&nbsp;
                                    <label> <input type="checkbox" class="i-checks" name="available[]" value="mon" <?=(in_array('mon', $avail))? 'checked' : '';?> > <?= $this->lang->line('monday'); ?> </label>&nbsp;&nbsp;
                                    <label> <input type="checkbox" class="i-checks" name="available[]" value="tue" <?=(in_array('tue', $avail))? 'checked' : '';?> > <?= $this->lang->line('tuesday'); ?> </label>&nbsp;&nbsp;
                                    <label> <input type="checkbox" class="i-checks" name="available[]" value="wed" <?=(in_array('wed', $avail))? 'checked' : '';?> > <?= $this->lang->line('wednesday'); ?> </label>&nbsp;&nbsp;
                                    <label> <input type="checkbox" class="i-checks" name="available[]" value="thu" <?=(in_array('thu', $avail))? 'checked' : '';?> > <?= $this->lang->line('thursday'); ?> </label>&nbsp;&nbsp;
                                    <label> <input type="checkbox" class="i-checks" name="available[]" value="fri" <?=(in_array('fri', $avail))? 'checked' : '';?> > <?= $this->lang->line('friday'); ?> </label>&nbsp;&nbsp;
                                    <label> <input type="checkbox" class="i-checks" name="available[]" value="sat" <?=(in_array('sat', $avail))? 'checked' : '';?> > <?= $this->lang->line('saturday'); ?> </label>
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <label class=""><?= $this->lang->line('Departure Time'); ?></label>
                                    <div class="input-group clockpicker" data-autoclose="true">
                                        <input type="text" class="form-control" id="pickuptime" name="trip_depart_time" value="<?=$trip_details[0]['trip_depart_time']?>" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-clock-o"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class=""><?= $this->lang->line('Start Date'); ?></label><br />
                                    <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                                        <input type="text" class="form-control" id="trip_start_date" name="trip_start_date" value="<?=$trip_details[0]['trip_start_date']?>" />
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-4">
                                    <label class=""><?= $this->lang->line('End Date'); ?></label><br />
                                    <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                                        <input type="text" class="form-control" id="trip_end_date" name="trip_end_date" value="<?=$trip_details[0]['trip_end_date']?>" />
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <h3><?= $this->lang->line('Location Details'); ?></h3>
                            </div>
                        </div>
                        
                        <?php foreach ($src_dest_details as $detail): ?>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="hidden" name="sdd_id[]" value="<?=$detail['sdd_id']?>">
                                    <div class="col-md-6">
                                        <h5 class=""><?= $this->lang->line('Source'); ?></h5>
                                        <input type="text" class="form-control" name="" value="<?=$detail['trip_source']?>" readonly="readonly">
                                        <h5><?= $this->lang->line("Pickup points and timings"); ?></h5>
                                        <?php 
                                            $src_point_ids = explode(',', $detail['src_point_ids']);
                                            $src_point_times = explode(',', $detail['src_point_times']);
                                            for ($i=0; $i < sizeof($src_point_ids); $i++) { 
                                                //get point name from table
                                                $point_address = $this->api->get_location_point_address_by_id($src_point_ids[$i]); ?>
                                            <p class=""><?=$point_address?></p>
                                            <div class="input-group clockpicker" data-autoclose="true">
                                                <input type="text" class="form-control" name="<?='src_point_times_'.$detail['sdd_id'].'[]'?>" value="<?=$src_point_times[$i]?>" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                        <label class=""><?= $this->lang->line('Destination'); ?></label>
                                        <input type="text" class="form-control" name="" value="<?=$detail['trip_destination']?>" readonly="readonly">
                                        <h5><?= $this->lang->line("Drop points and timings"); ?></h5>
                                        <?php 
                                            $dest_point_ids = explode(',', $detail['dest_point_ids']);
                                            $dest_point_times = explode(',', $detail['dest_point_times']);
                                            for ($i=0; $i < sizeof($dest_point_ids); $i++) { 
                                                //get point name from table
                                                $point_address = $this->api->get_location_point_address_by_id($dest_point_ids[$i]); ?>
                                            <p class=""><?=$point_address?></p>
                                            <div class="input-group clockpicker" data-autoclose="true">
                                                <input type="text" class="form-control" name="<?='dest_point_times_'.$detail['sdd_id'].'[]'?>" value="<?=$dest_point_times[$i]?>" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-12">
                                        <hr />
                                    </div>
                                </div>
                            </div>

                        <?php endforeach ?>

                      
                        <div class="wrapperField row form-group hidden">
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6 hidden">
                                <a class="btn btn-warning" data-style="zoom-in" id="btn_add_point"><?= $this->lang->line('Add More Location'); ?></a>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('Save Trip'); ?></button>
                                <a href="<?= base_url('user-panel-bus/bus-trip-master-list'); ?>" class="btn btn-primary"><?= $this->lang->line('back'); ?></a>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>

    <script>
        var btn_counter = 2; 
        $(function(){
            var wrapper = $(".wrapperField");
            var add_button = $("#btn_add_point");

            $(add_button).click(function(e) { 
                e.preventDefault();
                $("#no_of_points").val(btn_counter);
                $(wrapper).append(
                    '<div class="col-md-12">'+
                        '<div class="col-md-5">'+
                            '<label class=""><?= $this->lang->line("Select Source"); ?></label>'+
                            '<select class="form-control select2" name="src_loc_id_'+btn_counter+'" id="src_loc_id_'+btn_counter+'">'+
                                '<option value=""><?= $this->lang->line("Select Source"); ?></option>'+
                                '<?php foreach ($locations as $location) { ?>'+
                                    '<option value="<?= $location['loc_id'] ?>"><?= $location['loc_city_name'] ?></option>'+
                                '<?php } ?>'+
                            '</select>'+
                        '</div>'+
                        '<div class="col-md-5">'+
                            '<label class=""><?= $this->lang->line("Select Destination"); ?></label>'+
                            '<select class="form-control select2" name="dest_loc_id_'+btn_counter+'" id="dest_loc_id_'+btn_counter+'">'+
                                '<option value=""><?= $this->lang->line("Select Destination"); ?></option>'+
                                '<?php foreach ($locations as $location) { ?>'+
                                    '<option value="<?= $location['loc_id'] ?>"><?= $location['loc_city_name'] ?></option>'+
                                '<?php } ?>'+
                            '</select>'+
                        '</div>'+
                        '<div class="col-md-2"><br />'+
                            '<a class="remove_field pull-right btn btn-danger" data-toggle="tooltip" data-placement="top" title="<?=$this->lang->line("Remove Point")?>" data-original-title="<?=$this->lang->line("Remove Point")?>"><i class="fa fa-trash"></i></a>'+
                        '</div>'+
                        '<div class="col-md-12">'+
                            '<hr />'+
                        '</div>'+
                    '</div>'
                );
                btn_counter++;
            });

            $(wrapper).on("click",".remove_field", function(e) { e.preventDefault(); 
              $(this).parent().parent().remove(); 
              var pointCounter = $("#no_of_points").val();
              pointCounter -= 1;
              $("#no_of_points").val(pointCounter);
              if(btn_counter > 1 ) { } else { btn_counter = 0; } 
            });
        }); 

    </script>

    <script type="text/javascript">
        $("#tripAdd")
        .validate({
            ignore: [],
            rules: {
                bus_id: { required : true },
                pickuptime: { required : true },
                trip_start_date: { required : true },
                trip_end_date: { required : true },
            booking_type: { required : true },
            },
            messages: {
                bus_id: { required : "<?= $this->lang->line('Select vehicle!'); ?>", },
                pickuptime: { required : "<?= $this->lang->line('Enter departure time!'); ?>", },
                trip_start_date: { required : "<?= $this->lang->line('Select start date!'); ?>", },
                trip_end_date: { required : "<?= $this->lang->line('Select end date!'); ?>" },
              booking_type: { required : "<?= $this->lang->line('Select booking type!'); ?>", },
            },
        });
    </script>