<style type="text/css">
  .btn-file {  position: relative; overflow: hidden;  }
  .btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
  }
  #img-upload{  width: 100%;  }
  #map { height: 100%; }
  .pac-container { z-index: 100000; }
  .select2-container.form-control { overflow: hidden; }
  .numbers{ background-color: #3498db;
    color: #fff;
    padding: 1px 6px 1px 6px;  
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href=""><div class="clip-header"><i class="fa fa-arrow-up"></i></div></a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= $this->config->item('base_url') . 'transport/my-bookings'; ?>"><?= $this->lang->line('bookings'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('add_new_booking'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"> <i class="fa fa-plus-square fa-2x text-muted"></i>  <?= $this->lang->line('add_new_booking'); ?>  </h2>
      <small class="m-t-md"><?= $this->lang->line('add_booking_details_to_create_new_booking'); ?></small>  
    </div>
  </div>
</div>

<div class="content">
  <div class="row">
    <div class="hpanel hblue">
      <form action="<?= base_url('transport/register-booking'); ?>" method="post" class="form-horizontal" id="addBookingForm" enctype="multipart/form-data">
        <input id="category_id" name="category_id" type="hidden" value="<?=(isset($_SESSION['cat_id']) && $_SESSION['cat_id'] > 0) ? $_SESSION['cat_id'] : $category_id; ?>">   
        <input id="frm_addr_type" name="frm_addr_type" type="hidden" value="address">   
        <input id="to_addr_type" name="to_addr_type" type="hidden" value="address">   
        <input id="from_country_id" name="from_country_id" type="hidden" value="<?=(isset($_SESSION['from_country_id']) && $_SESSION['from_country_id'] > 0) ? $_SESSION['from_country_id'] : 0; ?>" />   
        <input id="to_country_id" name="to_country_id" type="hidden" value="<?=(isset($_SESSION['to_country_id']) && $_SESSION['to_country_id'] > 0) ? $_SESSION['to_country_id'] : 0; ?>" /> 

        <div class="panel-body" style="padding-left: 45px;">
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12" style="padding-right: 0px;">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 form-group" style="border: 1px solid #3498db; margin-bottom: 10px;">  
                <h5 class="" style="color: #3498db"><?= $this->lang->line('Photo'); ?></h5>
                <div class="text-center">
                <img class="img-thumbnail" id='img-upload' class="" src="<?= $this->config->item('resource_url') . 'noimage.png'; ?>" style="max-height: 105px; max-width: 200px; width: auto; height: auto;" /><br />
                </div>
                <div class="input-group">
                  <span class="input-group-btn">
                    <span class="btn btn-default btn-file"><?= $this->lang->line('browse'); ?> <input type="file" id="imgInp" name="profile_image" accept="image/*"></span>
                  </span>
                  <input type="text" class="form-control" readonly><br/>
                </div>
              </div>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 form-group" style="border: 1px solid #3498db; margin-bottom: 10px;" id="map_div">
                <div class="col-lg-12" style="padding-left: 0px;">
                  <h5 style="color: #3498db"><?= $this->lang->line('travelling_directions'); ?></h5>
                </div>
                <iframe
                  id="map_frame"
                  width="100%"
                  height="300"
                  frameborder="0" style="border:0"
                  src="" >
                </iframe>
              </div>  
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 form-group" style="border: 1px solid #3498db; word-wrap: break-word; padding-left: 0; padding-right: 0;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <h5 style="color: #3498db"><?= $this->lang->line('summary_of_your_shipping'); ?></h5>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <?= $this->lang->line('departure'); ?>: <label id="lbl_departure"><?= $this->lang->line('select_address') ?></label>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <?= $this->lang->line('arrival'); ?>: <label id="lbl_arrival"><?= $this->lang->line('select_address') ?></label>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <?= $this->lang->line('distance_in_km'); ?>: <label id="lbl_distance_in_km"></label>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <?= $this->lang->line('pickup_date_time'); ?>: <label id="lbl_pickup_date_time"><?= Date('m/d/Y') ?></label>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <?= $this->lang->line('deliver_date_time'); ?>: <label id="lbl_deliver_date_time"><?= Date('m/d/Y') ?></label>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <?php if(isset($_SESSION['c_quantity'])) { ?>
                    <?= $this->lang->line('no_of_packages'); ?>: <label id="lbl_no_of_packages"> 1</label> <br />
                  <?php } else { ?>
                    <?= $this->lang->line('no_of_packages'); ?>: <label id="lbl_no_of_packages"> 0</label> <br />
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-8 col-sm-12">
              <div class="col-lg-12 form-group" style="border: 1px solid #3498db; margin-bottom: 10px;">
                <div class="col-lg-12">
                  <h5 style="color: #3498db"><span class="numbers">1</span> <?= $this->lang->line('transport_type'); ?> &amp; <?= $this->lang->line('addresses'); ?></h5>
                </div>
                <div class="col-lg-6">
                  <label class="text-left"><?= $this->lang->line('transport_type'); ?> </label>
                  <div class="">
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="earth" value="earth" name="transport_type" 
                        <?php 
                          if(isset($_SESSION['transport_type']) && $_SESSION['transport_type']=='earth') echo 'checked'; 
                          else if(!isset($_SESSION['transport_type'])) echo 'checked'; 
                          else echo ''; 
                        ?> >
                      <label for="earth"> <?= $this->lang->line('earth'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="air" value="air" name="transport_type" <?=(isset($_SESSION['transport_type']) && $_SESSION['transport_type']=='air')?'checked':''?>>
                      <label for="air"> <?= $this->lang->line('air'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="sea" value="sea" name="transport_type" <?=(isset($_SESSION['transport_type']) && $_SESSION['transport_type']=='sea')?'checked':''?>>
                      <label for="sea"> <?= $this->lang->line('sea'); ?> </label>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6" style="margin-bottom: -5px;">
                  <label class="text-left"><?= $this->lang->line('courier_mode_type'); ?> </label>
                  <div class="">
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="local" value="local" name="order_mode" 
                      <?php 
                        if(isset($_SESSION['order_mode']) && $_SESSION['order_mode']=='local') echo 'checked'; 
                        else if(!isset($_SESSION['order_mode'])) echo 'checked'; 
                        else echo ''; 
                      ?> >
                      <label for="local"> <?= $this->lang->line('local'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="national" value="national" name="order_mode" <?=(isset($_SESSION['order_mode']) && $_SESSION['order_mode']=='national')?'checked':''?>>
                      <label for="national"> <?= $this->lang->line('national'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="international" value="international" name="order_mode" <?=(isset($_SESSION['order_mode']) && $_SESSION['order_mode']=='international')?'checked':''?>>
                      <label for="international"> <?= $this->lang->line('international'); ?> </label>
                    </div>
                  </div> <br/>
                </div>
                <div class="col-lg-4">
                  <label class=""><?= $this->lang->line('transport_vehicle'); ?></label>
                  <div class="">
                    <select id="vehicle" name="vehicle" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('transport_vehicle'); ?>" >
                      <option value=""> <?= $this->lang->line('select_transport_vehicle'); ?> </option>
                      <?php foreach ($default_trans as $v): ?>                        
                        <option value="<?= $v['vehical_type_id']; ?>" <?=(isset($_SESSION['vehicle']) && $_SESSION['vehicle']==$v['vehical_type_id'])?'selected':''?> ><?= $v['vehicle_type']; ?></option>
                      <?php endforeach; ?>                        
                    </select>
                  </div>                  
                </div> 
                <div class="col-lg-5">
                  <label class=""><?= $this->lang->line('shipping_type'); ?></label>
                  <div class="">
                     <div class="radio radio-success radio-inline">
                      <input type="radio" id="shipping" value="Shipping_Only" name="shipping_mode" checked>
                      <label for="shipping"> <?= $this->lang->line('shipping_only'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="purchase" value="Purchase_and_Deliver" name="shipping_mode">
                      <label for="purchase"> <?= $this->lang->line('purchase_deliver'); ?> </label>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3" style="margin-bottom: -5px;">
                  <label class=""><?= $this->lang->line('need_tailgate'); ?></label>
                  <div class="">
                     <div class="radio radio-success radio-inline">
                      <input type="radio" id="tailgate_no" value="0" name="need_tailgate" checked>
                      <label for="tailgate_no"> <?= $this->lang->line('no'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="tailgate_yes" value="1" name="need_tailgate">
                      <label for="tailgate_yes"> <?= $this->lang->line('yes'); ?> </label>
                    </div>
                  </div> <br/> 
                </div>                    
                <div class="col-lg-12" style="padding-left: 0px; margin-bottom: -5px;">
                  <div class="col-lg-6" style="padding-right: 6px;">
                    <div>
                      <label class="">
                        <div class="radio radio-success radio-inline">
                          <input type="radio" id="from_address_check" name="from_check" checked>
                          <label for="from_address_check"></label>
                        </div>
                        <?= $this->lang->line('from_address'); ?>
                      </label>
                    </div>
                    <div style="display: flex;">
                      <input type="hidden" name="from_address_lat_long_hidden" id="from_address_lat_long_hidden" value="" />
                      <a class="btn btn-default" id="btnFromAddrShow" onclick="FromAddrShow()" title="<?= $this->lang->line('show_my_saved_addresses'); ?>" style="display: none;"><i class="fa fa-address-book"></i></a>
                      <input type="" name="" class="form-control" id="fromMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>" style="display: none;" autocomplete="off" value="<?=(isset($_SESSION['from_address']) && $_SESSION['from_address'] != '') ? $_SESSION['from_address'] : '' ?>">
                      <select class="form-control select2" name="from_address" id="from_address">
                        <option value=""><?= $this->lang->line('select_address'); ?></option>
                        <?php foreach ($address as $adr ) :?>
                          <option value="<?= $adr['addr_id']; ?>">
                            <?= ($adr['comapny_name'] !="NULL")? ucfirst($adr['comapny_name']) .' | ' .ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ' )' : ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ' )'; ?>
                          </option>
                        <?php endforeach; ?>
                      </select>
                      <a class="btn btn-default" id="btnFromGwidgetShow" onclick="FromGwidgetShow()" title="<?=$this->lang->line('add_address')?>"><i class="fa fa-pencil"></i></a>
                    </div>

                    <div id="fromAddrDiv" style="display: none;">
                      <input type="hidden" from-data-geo="lat" name="frm_latitude" id="frm_latitude" value="<?=(isset($_SESSION['frm_latitude']) && $_SESSION['frm_latitude'] != '') ? $_SESSION['frm_latitude'] : '' ?>" />
                      <input type="hidden" from-data-geo="lng" name="frm_longitude" id="frm_longitude" value="<?=(isset($_SESSION['frm_longitude']) && $_SESSION['frm_longitude'] != '') ? $_SESSION['frm_longitude'] : '' ?>" />
                      <input type="hidden" from-data-geo="formatted_address" id="frm_street" name="frm_street" class="form-control" placeholder="<?= $this->lang->line('street_name'); ?>" value="<?=(isset($_SESSION['frm_formatted_address']) && $_SESSION['frm_formatted_address'] != '') ? $_SESSION['frm_formatted_address'] : '' ?>" />
                      <input type="hidden" from-data-geo="postal_code" id="frm_zipcode" name="frm_zipcode" class="form-control" placeholder="<?= $this->lang->line('zip_code'); ?>" value="<?=(isset($_SESSION['frm_postal_code']) && $_SESSION['frm_postal_code'] != '') ? $_SESSION['frm_postal_code'] : '' ?>" />
                      <input type="hidden" from-data-geo="country" id="frm_country" name="frm_country" class="form-control" placeholder="<?= $this->lang->line('country'); ?>" value="<?=(isset($_SESSION['frm_country']) && $_SESSION['frm_country'] != 'NULL') ? $_SESSION['frm_country'] : '' ?>" readonly/>
                      <input type="hidden" from-data-geo="administrative_area_level_1" id="frm_state" name="frm_state" class="form-control" id="" placeholder="<?= $this->lang->line('state'); ?>" value="<?=(isset($_SESSION['frm_state']) && $_SESSION['frm_state'] != 'NULL') ? $_SESSION['frm_state'] : '' ?>" readonly/>
                      <input type="hidden" from-data-geo="locality" id="frm_city" name="frm_city" class="form-control" placeholder="<?= $this->lang->line('city'); ?>" value="<?=(isset($_SESSION['frm_city']) && $_SESSION['frm_city'] != 'NULL') ? $_SESSION['frm_city'] : '' ?>" readonly/>
                      <div class="form-group" style="margin-top: 5px;"> 

                        <div class="col-lg-12">
                          <label class=""><?= $this->lang->line('address_line_1'); ?></label>
                          <input type="text" from-data-geo="formatted_address" id="frm_address1" name="frm_address1" class="form-control" placeholder="<?= $this->lang->line('address_line_1'); ?>" value="<?=(isset($_SESSION['frm_formatted_address']) && $_SESSION['frm_formatted_address'] != '') ? $_SESSION['frm_formatted_address'] : '' ?>" />
                        </div>
                        <div class="col-lg-5">
                          <label><?= $this->lang->line('address_type'); ?></label>
                          <select id="frm_address_type" name="frm_address_type" class="form-control select2" data-allow-clear="true">
                            <option value="Individual"><?= $this->lang->line('individual'); ?></option></option>
                            <option value="Commercial"><?= $this->lang->line('commercial'); ?></option>
                          </select>
                        </div>
                        <div class="col-lg-7">
                          <label><?= $this->lang->line('compnay_name'); ?></label>
                          <input type="text" class="form-control" id="frm_company" name="frm_company" placeholder="<?= $this->lang->line('enter_company_name'); ?>" readonly />
                        </div> 
                        <div class="col-lg-6">
                          <label class=""><?= $this->lang->line('contact_first_name'); ?></label>
                          <input type="text" id="frm_firstname" name="frm_firstname" class="form-control" placeholder="<?= $this->lang->line('enter_first_name'); ?>" />
                        </div>
                        <div class="col-lg-6">
                          <label class=""><?= $this->lang->line('contact_last_name'); ?></label>
                          <input type="text" name="frm_lastname" class="form-control" id="frm_lastname" placeholder="<?= $this->lang->line('enter_last_name'); ?>" />
                        </div>
                        <div class="col-lg-12">
                          <label><?= $this->lang->line('email_address'); ?></label>
                          <input type="email" class="form-control" id="frm_email_id" name="frm_email_id" placeholder="<?= $this->lang->line('enter_email_address'); ?>" />
                        </div>
                        <div class="col-lg-12">
                          <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                          <input type="text" id="frm_mobile" name="frm_mobile" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number'); ?>" />
                        </div>
                        <div class="col-lg-12 hidden">
                          <label><?= $this->lang->line('photo'); ?></label>
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="frm_address_image" id="frm_address_image" class="form-control" />
                        </div>
                        <div class="col-lg-12">
                          <label class=""><?= $this->lang->line('pickup_instructions'); ?></label>
                          <textarea placeholder="<?= $this->lang->line('pickup_instructions'); ?>" class="form-control" name="frm_pickup_instruction" id="frm_pickup_instruction" rows="1" style="resize: none;"></textarea>
                        </div>
                        <div class="col-lg-12">
                          <label class=""><?= $this->lang->line('deliver_instructions'); ?></label>
                          <textarea placeholder="<?= $this->lang->line('deliver_instructions'); ?>" class="form-control" name="frm_deliver_instruction" id="frm_deliver_instruction" rows="1" style="resize: none;"></textarea>
                        </div>
                        <div class="col-lg-12">
                          <br /><a class="btn btn-info btn-sm" id="btnFromAddressSave"><?= $this->lang->line('save_address'); ?></a>
                        </div>

                      </div> 

                    </div>
                  </div>
                  <div class="col-lg-6" style="padding-right: 0px; padding-left: 24px; margin-bottom: -5px;">
                    <div>
                      <label class="">
                        <div class="radio radio-success radio-inline">
                          <input type="radio" id="to_address_check" name="to_check" checked>
                          <label for="to_address_check"></label>
                        </div>                      
                        <?= $this->lang->line('to_address'); ?>
                      </label>
                    </div>
                    <div style="display: flex;">
                      <input type="hidden" name="to_address_lat_long_hidden" id="to_address_lat_long_hidden" value="" />
                      <a class="btn btn-default" id="btnToAddrShow" onclick="ToAddrShow()" title="<?= $this->lang->line('show_my_saved_addresses'); ?>" style="display: none;"><i class="fa fa-address-book"></i></a>
                      <input type="" name="" class="form-control" id="toMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>" style="display: none;" autocomplete="off" value="<?=(isset($_SESSION['to_address']) && $_SESSION['to_address'] != '') ? $_SESSION['to_address'] : '' ?>">
                      <select class="form-control select2" name="to_address" id="to_address">
                        <option value=""><?= $this->lang->line('select_address'); ?></option>
                        <?php foreach ($address as $adr ) :?>
                          <option value="<?= $adr['addr_id']; ?>"> 
                            <?= ($adr['comapny_name'] !="NULL")? ucfirst($adr['comapny_name']) .' | ' .ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ' )' : ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ' )'; ?>  
                          </option>
                        <?php endforeach; ?>
                      </select>
                      <a class="btn btn-default" id="btnToGwidgetShow" onclick="ToGwidgetShow()" title="<?=$this->lang->line('add_address')?>"><i class="fa fa-pencil"></i></a>
                    </div>

                    <div id="toAddrDiv" style="display: none;">
                      <input type="hidden" to-data-geo="lat" name="t_latitude" id="t_latitude" value="<?=(isset($_SESSION['to_latitude']) && $_SESSION['to_latitude'] != '') ? $_SESSION['to_latitude'] : '' ?>" />
                      <input type="hidden" to-data-geo="lng" name="t_longitude" id="t_longitude" value="<?=(isset($_SESSION['to_longitude']) && $_SESSION['to_longitude'] != '') ? $_SESSION['to_longitude'] : '' ?>" />
                      <input type="hidden" to-data-geo="formatted_address" id="t_street" name="t_street" class="form-control" placeholder="<?= $this->lang->line('street_name'); ?>" value="<?=(isset($_SESSION['to_formatted_address']) && $_SESSION['to_formatted_address'] != '') ? $_SESSION['to_formatted_address'] : '' ?>" />
                      <input type="hidden" to-data-geo="street_number" id="t_address1" name="t_address1" class="form-control" placeholder="<?= $this->lang->line('address_line_1'); ?>" value="<?=(isset($_SESSION['to_formatted_address']) && $_SESSION['to_formatted_address'] != '') ? $_SESSION['to_formatted_address'] : '' ?>" />
                      <input type="hidden" to-data-geo="postal_code" id="t_zipcode" name="t_zipcode" class="form-control" placeholder="<?= $this->lang->line('zip_code'); ?>" value="<?=(isset($_SESSION['to_postal_code']) && $_SESSION['to_postal_code'] != '') ? $_SESSION['to_postal_code'] : '' ?>" />
                      <input type="hidden" to-data-geo="country" id="t_country" name="t_country" class="form-control" placeholder="<?= $this->lang->line('country'); ?>" value="<?=(isset($_SESSION['to_country']) && $_SESSION['to_country'] != '') ? $_SESSION['to_country'] : '' ?>" readonly/>
                      <input type="hidden" to-data-geo="administrative_area_level_1" id="t_state" name="t_state" class="form-control" id="tlaceholder="<?= $this->lang->line('state'); ?>" value="<?=(isset($_SESSION['to_state']) && $_SESSION['to_state'] != '') ? $_SESSION['to_state'] : '' ?>" readonly/>
                      <input type="hidden" to-data-geo="locality" id="t_city" name="t_city" class="form-control" placeholder="<?= $this->lang->line('city'); ?>" value="<?=(isset($_SESSION['to_city']) && $_SESSION['to_city'] != '') ? $_SESSION['to_city'] : '' ?>" readonly/>
                      <div class="form-group" style="margin-top: 5px;"> 
                        <div class="col-lg-12">
                          <label class=""><?= $this->lang->line('address_line_1'); ?></label>
                          <input type="text" to-data-geo="formatted_address" id="t_address1" name="t_address1" class="form-control" placeholder="<?= $this->lang->line('address_line_1'); ?>" value="<?=(isset($_SESSION['to_formatted_address']) && $_SESSION['to_formatted_address'] != '') ? $_SESSION['to_formatted_address'] : '' ?>" />
                        </div>
                        <div class="col-lg-5">
                          <label>Address Type</label>
                          <select id="t_address_type" name="t_address_type" class="form-control select2" data-allow-clear="true">
                            <option value="Individual"><?= $this->lang->line('individual'); ?></option></option>
                            <option value="Commercial"><?= $this->lang->line('commercial'); ?></option>
                          </select>
                        </div>
                        <div class="col-lg-7">
                          <label><?= $this->lang->line('compnay_name'); ?></label>
                          <input type="text" class="form-control" id="t_company" name="t_company" placeholder="<?= $this->lang->line('enter_company_name'); ?>" readonly />
                        </div> 
                        <div class="col-lg-6">
                          <label class=""><?= $this->lang->line('contact_first_name'); ?></label>
                          <input type="text" id="t_firstname" name="t_firstname" class="form-control" placeholder="<?= $this->lang->line('enter_first_name'); ?>" />
                        </div>
                        <div class="col-lg-6">
                          <label class=""><?= $this->lang->line('contact_last_name'); ?></label>
                          <input type="text" name="t_lastname" class="form-control" id="t_lastname" placeholder="<?= $this->lang->line('enter_last_name'); ?>" />
                        </div>
                        <div class="col-lg-12">
                          <label><?= $this->lang->line('email_address'); ?></label>
                          <input type="email" class="form-control" id="t_email_id" name="t_email_id" placeholder="<?= $this->lang->line('enter_email_address'); ?>" />
                        </div>
                        <div class="col-lg-12">
                          <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                          <input type="text" id="t_mobile" name="t_mobile" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number'); ?>" />
                        </div>
                        <div class="col-lg-12 hidden">
                          <label><?= $this->lang->line('photo'); ?></label>
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="t_address_image" id="t_address_image" class="form-control" />
                        </div>
                        <div class="col-lg-12">
                          <label class=""><?= $this->lang->line('pickup_instructions'); ?></label>
                          <textarea placeholder="<?= $this->lang->line('pickup_instructions'); ?>" class="form-control" name="t_pickup_instruction" id="t_pickup_instruction" rows="1" style="resize: none;"></textarea>
                        </div>
                        <div class="col-lg-12">
                          <label class=""><?= $this->lang->line('deliver_instructions'); ?></label>
                          <textarea placeholder="<?= $this->lang->line('deliver_instructions'); ?>" class="form-control" name="t_deliver_instruction" id="t_deliver_instruction" rows="1" style="resize: none;"></textarea>
                        </div>
                        <div class="col-lg-12">
                          <br /><a class="btn btn-info btn-sm" id="btnToAddressSave"><?= $this->lang->line('save_address'); ?></a>
                        </div>
                      </div> 
                    </div>
                    <br />
                  </div>
                </div>
                <div class="col-lg-6 hidden">
                  <label class="">
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="from_relay_check" name="from_check">
                      <label for="from_relay_check"></label>
                    </div>
                    <?= $this->lang->line('from_relay'); ?>
                  </label>
                
                  <div id="filterarea1" style="display: none;">
                    <label class=""><?= $this->lang->line('country'); ?></label>
                    <select id="country_id1" name="country_id1" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('country'); ?>">
                      <option value=""><?= $this->lang->line('select_country'); ?></option>
                      <?php foreach ($countries as $country): ?>
                        <option value="<?= $country['country_id'] ?>"> <?= $country['country_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                    <label class=""><?= $this->lang->line('state'); ?></label>
                    <select id="state_id1" name="state_id1" class="form-control select2" data-allow-clear="true" data-placeholder="<?= json_encode($this->lang->line('select_state')); ?>," disabled>
                      <option value=""><?= $this->lang->line('select_state'); ?></option>
                    </select>
                    <label class=""><?= $this->lang->line('city'); ?></label>
                    <select id="city_id1" name="city_id1" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('city'); ?>" disabled>
                      <option value=""><?= $this->lang->line('select_city'); ?></option>
                    </select> <br/>
                    <button type="button" name="filterarea1_btn" id="filterarea1_btn" class="btn btn-sm btn-link filterarea1_btn"><i class="fa fa-search"></i> Get Relay Points</button>
                  </div> <br/>
                
                  <div class="">
                    <select class="form-control m-b select2" name="from_relay"  id="from_relay" disabled>
                      <option value=""> <?= $this->lang->line('select_relay'); ?> </option>
                      <?php foreach ($relay as $adr ) :
                          $city_name = $this->api->get_city_name_by_id($adr['city_id']); ?>
                        <option value="<?= $adr['relay_id']; ?>"> 
                          <?= ($adr['comapny_name'] !="NULL")? ucfirst($adr['comapny_name']) .' | ' .ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ', '. $city_name . ' )' : ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ', '. $city_name .' )'; ?>
                        </option>
                      <?php endforeach; ?>
                    </select> 
                    <input type="hidden" name="from_relay_lat_long_hidden" id="from_relay_lat_long_hidden" />
                  </div> <br/>                      
                </div>
                <div class="col-lg-6 hidden">
                  <label class="">
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="to_relay_check" name="to_check">
                      <label for="to_relay_check"></label>
                    </div>
                    <?= $this->lang->line('to_relay'); ?>
                  </label>
                
                  <div id="filterarea2" style="display: none;">
                    <label class=""><?= $this->lang->line('country'); ?></label>
                    <select id="country_id2" name="country_id2" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('country'); ?>">
                      <option value=""><?= $this->lang->line('select_country'); ?></option>
                      <?php foreach ($countries as $country): ?>
                        <option value="<?= $country['country_id'] ?>"> <?= $country['country_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                    <label class=""><?= $this->lang->line('state'); ?></label>
                    <select id="state_id2" name="state_id2" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_state'); ?>," disabled>
                      <option value=""><?= $this->lang->line('select_state'); ?></option>
                    </select>
                    <label class=""><?= $this->lang->line('city'); ?></label>
                    <select id="city_id2" name="city_id2" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('city'); ?>" disabled>
                      <option value=""><?= $this->lang->line('select_city'); ?></option>
                    </select> <br/>
                    <button type="button" name="filterarea2_btn" id="filterarea2_btn" class="btn btn-sm btn-link filterarea2_btn"><i class="fa fa-search"></i> Get Relay Points</button>
                  </div> <br/>
                
                  <div class="">
                    <?php //var_dump($relay); ?>
                    <select class="form-control m-b select2" name="to_relay"  id="to_relay" disabled>
                      <option value=""> <?= $this->lang->line('select_relay'); ?> </option>
                      <?php foreach ($relay as $adr ) :
                        $city_name = $this->api->get_city_name_by_id($adr['city_id']); ?>
                        <option value="<?= $adr['relay_id']; ?>"> 
                          <?= ($adr['comapny_name'] !="NULL")? ucfirst($adr['comapny_name']) .' | ' .ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ', '. $city_name . ' )' : ucfirst($adr['firstname']). ' ' . ucfirst($adr['lastname']). ' ( '. $adr['street_name']. ', '. $city_name .' )'; ?> 
                        </option>
                      <?php endforeach; ?>
                    </select> 
                    <input type="hidden" name="to_relay_lat_long_hidden" id="to_relay_lat_long_hidden" />
                  </div> <br/>
                </div>
              </div>
              <div class="col-lg-12 form-group" style="border: 1px solid #3498db;  margin-bottom: 10px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <h5 style="color: #3498db"><span class="numbers">2</span> <?= $this->lang->line('date_time');?></h5>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                  <label class=""><?= $this->lang->line('pickup_date_time'); ?></label>
                  <script>
                    $(document).ready( function() { 
                      var pickdt = "<?=(isset($_SESSION['pickupdate']))?$_SESSION['pickupdate']:''?>";
                      if(pickdt != '') { $( function() { $( "#pickupdate" ).datepicker("setDate" , pickdt); } ); }
                      var delidt = "<?=(isset($_SESSION['deliverdate']))?$_SESSION['deliverdate']:''?>";
                      if(delidt != '') { $( function() { $( "#deliverdate" ).datepicker("setDate" , delidt); } ); }
                    });
                  </script>
                  <div class="">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-left: 0px;">
                      <div class="input-group date">
                        <input type="text" class="form-control" id="pickupdate" name="pickupdate" autocomplete="none" value="" />
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div> 
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      <input type="hidden" name="pick_time_session" value="<?=(isset($_SESSION['pickuptime']))?$_SESSION['pickuptime']:''?>" id="pick_time_session">
                      <div class="input-group clockpicker" data-autoclose="true">
                        <input type="text" class="form-control" id="pickuptime" name="pickuptime" style="background-color: white !important;" readonly />
                        <span class="input-group-addon">
                          <span class="fa fa-clock-o"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                  <label class=""><?= $this->lang->line('deliver_date_time'); ?></label>
                  <div class="">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-left: 0px;">
                      <div class="input-group">
                        <input type="text" class="form-control" id="deliverdate" name="deliverdate" autocomplete="none" value="" />
                        <div class="input-group-addon ddAddon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                      <input type="hidden" name="deliver_time_session" value="<?=(isset($_SESSION['delivertime']))?$_SESSION['delivertime']:''?>" id="deliver_time_session">
                      <div class="input-group clockpicker" data-autoclose="true">
                        <input type="text" class="form-control" id="delivertime" name="delivertime" style="background-color: white !important;" readonly />
                        <span class="input-group-addon">
                          <span class="fa fa-clock-o"></span>
                        </span>
                      </div> <br/>
                    </div>                       
                  </div> 
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                  <label class=""><?= $this->lang->line('expiry_date'); ?></label>
                  <div class="">
                    <div class="input-group">
                      <input type="text" id="expiry_date" name="expiry_date" class="form-control" autocomplete="none" />
                      <div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="margin-bottom: -10px;">
                  <label class=""><?= $this->lang->line('loading_unloading_time'); ?></label>
                  <div class="input-group">
                    <input type="number" class="form-control" id="loading_time" name="loading_unloading_hours" min="0" />
                    <span class="input-group-addon">
                      <span class="fa">hrs</span>
                    </span>
                  </div>
                  <span id="hours_offred" class="label label-success">
                    <?php if(isset($_SESSION['loading_free_hours'])) { ?>
                      [ <?=$this->lang->line('free_hours')?> : <?=$_SESSION['loading_free_hours']?> <?= $this->lang->line('hours_offered'); ?> ]
                    <?php } else { echo $this->lang->line('Loading / Unloading Hours not available.'); } ?>
                  </span><br/>
                </div>   
              </div>
              <div class="col-lg-12 form-group" style="border: 1px solid #3498db; margin-bottom: 10px;">
                <div class="input_fields_wrap">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <h5 style="color: #3498db"><span class="numbers">3</span> <?= $this->lang->line('package_dimension_details'); ?></h5>
                  </div> 

                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
                    <img id='img-dimension' class="text-center img-thumbnail img-dimension" src="<?=(isset($_SESSION['image_url']) && $_SESSION['image_url'] != 'NULL') ? base_url($_SESSION['image_url']) : base_url('resources/noimage.png')?>" style="max-height: 80px; max-width: 90px; height: auto;" />
                  </div>

                  <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right: 0px;">
                      <label class=""><?= $this->lang->line('quantity'); ?></label>
                      <div class="input-group">
                        <input id="c_quantity" type="number" name="c_quantity[]" placeholder="<?= $this->lang->line('quantity'); ?>" min="1" class="form-control" value="<?=(isset($_SESSION['c_quantity']))?$_SESSION['c_quantity']:''?>" />
                        <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                      </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right: 0px; margin-bottom: -10px;">
                      <input type="hidden" name="dimension_id[]" id="dimension_id" value="0" />
                      <label class=""><?= $this->lang->line('standard_dimension'); ?> </label>
                      <select class="form-control m-b select2" name="dimension[]"  id="dimension">
                        <option value=""> <?= $this->lang->line('select_standard_dimension'); ?> </option>
                        <?php foreach ($dimension as $i => $d) :?>                          
                          <option value="<?= $d['dimension_id']; ?>" <?=(isset($_SESSION['dimension_id']) && $_SESSION['dimension_id'] > 0 && $_SESSION['dimension_id']==$d['dimension_id'])?'selected':''?> ><?= $d['dimension_type']; ?></option>
                        <?php endforeach; ?>
                        <option value="-1"> <?= $this->lang->line('other'); ?> </option>
                      </select>  <br/>                                              
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-right: 0px;">
                      <label class=""><?= $this->lang->line('Width'); ?></label>
                      <div class="input-group">
                        <input id="c_width" type="number"  name="c_width[]" placeholder="<?= $this->lang->line('Width'); ?>"  class="form-control" readonly value="<?=(isset($_SESSION['c_width']) && $_SESSION['c_width'] > 0)?$_SESSION['c_width']:''?>" />
                        <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                      </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-right: 0px;">
                      <label class=""><?= $this->lang->line('height'); ?></label>
                      <div class="input-group">
                        <input id="c_height" type="number"  name="c_height[]" placeholder="<?= $this->lang->line('height'); ?>" class="form-control" readonly value="<?=(isset($_SESSION['c_height']) && $_SESSION['c_height'] > 0)?$_SESSION['c_height']:''?>" />
                        <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                      </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-right: 0px; margin-bottom: -10px;">
                      <label class=""><?= $this->lang->line('length'); ?></label>
                      <div class="input-group">
                        <input id="c_length" type="number"  name="c_length[]" placeholder="<?= $this->lang->line('length'); ?>" class="form-control" readonly value="<?=(isset($_SESSION['c_length']) && $_SESSION['c_length'] > 0)?$_SESSION['c_length']:''?>" />
                        <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                      </div><br/>
                    </div> 
                  </div>

                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6" style="padding-right: 0px;">
                      <label class=""><?= $this->lang->line('total_weight'); ?> </label>
                      <div class="">
                        <div class="input-group">
                          <input id="c_weight" type="number" name="c_weight[]" placeholder="<?= $this->lang->line('total_weight'); ?>" min="1" class="form-control" value="<?=(isset($_SESSION['c_weight']))?$_SESSION['c_weight']:''?>" />
                          <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6" style="padding-right: 0px;">
                      <label class=""><?= $this->lang->line('weight_unit'); ?> </label>
                      <div class="">
                        <div class="input-group">
                          <select class="form-control m-b select2" name="c_unit_id[]"  id="c_unit_id">
                            <option value=""> <?= $this->lang->line('select_unit'); ?> </option>
                            <?php foreach ($unit as $u ) :?>
                              <option value="<?= $u['unit_id']; ?>" <?= (strtolower($u['shortname']) == 'kg')?"selected":"" ?> > 
                                <?= ucfirst($u['unit_type']).' ( ' . strtoupper($u['shortname']) . ' )'; ?> 
                              </option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div> <br/>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right: 0px;">
                      <label class=""><?= $this->lang->line('contents'); ?></label>
                      <input id="content" name="content[]" type="text" class="form-control" placeholder="<?= $this->lang->line('contents'); ?>" autocomplete="none" > <br/>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right: 0px;">
                      <label class=""><?= $this->lang->line('dangerous_goods'); ?> ( <?= $this->lang->line("optional"); ?> )</label>
                      <div class="">
                        <select id="dangerous_goods" name="dangerous_goods[]" class="form-control dangerous_goods" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_dangerous_goods'); ?>" disabled>
                          <option value="0"> <?= $this->lang->line('select_dangerous_goods'); ?> </option>                                                  
                        </select>
                      </div>   
                    </div>
                  </div>  
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-right">
                  <button type="button" id="btn_add_package" class="btn btn-info btn-sm" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> <?= $this->lang->line('add_package'); ?></button>
                </div>
              </div>

              <div class="col-lg-12 form-group" style="border: 1px solid #3498db; margin-bottom: 10px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                  <h5 style="color: #3498db"><span class="numbers">4</span><?= $this->lang->line('options_special_instructions'); ?></h5>
                </div>
                <div class="row" style="padding-right: 15px">
                  <div class="col-xl-2 col-lg-2 col-md-3 col-sm-6" id="insurence_no_div" style="padding-left: 30px;">
                    <label class=""><?= $this->lang->line('insurance_?'); ?></label>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" value="0" id="insurance_no" name="insurance" checked onClick="hide_insurance_field()">
                      <label for="insurance_no"> <?= $this->lang->line('no'); ?> </label>
                    </div>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-3 col-sm-6" id="insurence_yes_div">
                    <label class="">&nbsp;</label>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" value="1" id="insurance_yes" name="insurance" onClick="show_insurance_field();">
                      <label for="insurance_yes"> <?= $this->lang->line('yes'); ?> </label>
                    </div>
                  </div>
                  <div class="col-xl-3 col-lg-3 col-md-4 col-sm-8" id="package_value_div">
                    <label class=""><?= $this->lang->line('package_value'); ?></label>
                    <div class="">
                      <div class="input-group">
                        <input type="number" placeholder="Value" class="form-control" name="package_value" id="package_value" min="1" readonly/>
                        <div class="input-group-addon package_value_currency_icon">?</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4" style="margin-top: 25px;">
                    <button class="btn btn-sm btn-info" id="btn_fee" style="pointer-events: none;" disabled><?= $this->lang->line('get_fee');?></button>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12" style="margin-top: 25px;">
                    <h5 for="insurance_fee" id="insurance_fee" class="text-success" style="margin-top: 10px;"><?= $this->lang->line('insurance_fee'); ?> <span id="fee" name="fee"></span>
                    </h5>
                  </div>
                </div>
                <div class="row" style="padding-right: 15px">
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-left: 30px;">
                    <label class=""><?= $this->lang->line('dedicated_vehicle'); ?> </label>
                    <div class=""> 
                      <div class="radio radio-success radio-inline">
                        <input type="radio" id="dedicated_no" value="0" name="dedicated_vehicle" checked onClick="hide_travel_option()">
                        <label for="dedicated_no"> <?= $this->lang->line('no'); ?> </label>
                      </div>
                      <div class="radio radio-success radio-inline">
                        <input type="radio" id="dedicated_yes" value="1" name="dedicated_vehicle" onClick="show_travel_option()">
                        <label for="dedicated_yes"> <?= $this->lang->line('yes'); ?> </label> <br/>
                      </div>  
                    </div> 
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <label class=""><?= $this->lang->line('Handling By'); ?></label>
                    <div class="">         
                      <div class="radio radio-success radio-inline">
                        <input type="radio" value="0" id="handle_self" name="handle_by" checked>
                        <label for="handle_self"> <?= $this->lang->line('self'); ?> </label>
                      </div>
                      <div class="radio radio-success radio-inline">
                        <input type="radio" value="1" id="handle_driver" name="handle_by">
                        <label for="handle_driver"> <?= $this->lang->line('driver'); ?> </label>
                      </div>
                    </div>                      
                  </div>
                </div>
                <div class="row" style="padding-right: 15px">
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-left: 30px;">
                    <label class=""><?= $this->lang->line('travel_with_driver'); ?> </label>
                    <div class="">                      
                      <div class="radio radio-success radio-inline">
                        <input type="radio" id="driver_no" value="0" name="travel_with" checked disabled>
                        <label for="driver_no"> <?= $this->lang->line('no'); ?> </label>
                      </div>
                      <div class="radio radio-success radio-inline">
                        <input type="radio" id="driver_yes" value="1" name="travel_with" disabled>
                        <label for="driver_yes"> <?= $this->lang->line('yes'); ?> </label>
                      </div>                        
                    </div>
                  </div>  
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                    <label class=""><?= $this->lang->line('your_ref'); ?> </label>
                    <div class="">                      
                      <input type="text" placeholder="<?= $this->lang->line('your_ref'); ?>" class="form-control" name="ref_no" id="ref_no" autocomplete="none" />
                    </div>                      
                  </div>
                </div>
                <div class="col-lg-6 hidden ">
                  <label class=""><?= $this->lang->line('pickup_instructions'); ?></label>
                  <div class="">  
                    <textarea placeholder="<?= $this->lang->line('pickup_instructions'); ?>" class="form-control" id="pickup_instruction" name="pickup_instruction" rows="2" style="resize:none;"></textarea>
                  </div>
                </div>
                <div class="col-lg-6 hidden ">
                  <label class=""><?= $this->lang->line('deliver_instructions'); ?></label>
                  <div class="">
                    <textarea placeholder="<?= $this->lang->line('deliver_instructions'); ?>" class="form-control" id="deliver_instruction" name="deliver_instruction" rows="2" style="resize:none;"></textarea><br/>
                  </div>
                </div>
                <div class="row" style="padding-right: 15px; padding-left: 15px">
                  <div class="col-xl-<?=($user_details['is_dedicated']=="1")?6:12?> col-lg-<?=($user_details['is_dedicated']=="1")?6:12?> col-md-<?=($user_details['is_dedicated']=="1")?6:12?> col-sm-<?=($user_details['is_dedicated']=="1")?6:12?>" style="margin-bottom: -5px;">
                    <label class=""><?= $this->lang->line('description'); ?> </label>
                    <div class="">
                      <textarea placeholder="<?= $this->lang->line('description'); ?>" class="form-control" name="description" rows="1" style="resize:none;"></textarea><br />
                    </div>
                  </div>
                  <?php if($user_details['is_dedicated'] == "1"): ?>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="margin-bottom: -5px;">
                      <label class=""><?= $this->lang->line('Visible only by my deliverers'); ?> </label>
                      <div class="">                      
                        <div class="radio radio-success radio-inline">
                          <input type="radio" id="visible_yes" value="1" name="visible_by" checked>
                          <label for="visible_yes"> <?= $this->lang->line('yes'); ?> </label>
                        </div> 
                        <div class="radio radio-success radio-inline">
                          <input type="radio" id="visible_no" value="0" name="visible_by">
                          <label for="visible_no"> <?= $this->lang->line('no'); ?> </label>
                        </div>               
                      </div>
                    </div>
                  <?php endif; ?> 
                </div>
              </div>
              <div id="custom_charge_div" class="col-lg-12 form-group hidden" style="border: 1px solid #3498db">
                <div class="col-lg-12">
                  <h5 style="color: #3498db"><span class="numbers">5</span> <?= $this->lang->line('custom_charge_details');?></h5>
                </div>

                <div class="col-lg-3">
                  <label class=""><?= $this->lang->line('custom_clearance'); ?> </label><br/>
                  <div class="">                      
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="self" value="Self" name="custom_clearance">
                      <label for="self"> <?= $this->lang->line('self'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="provider" value="Provider" name="custom_clearance">
                      <label for="provider"> <?= $this->lang->line('provider'); ?> </label>
                    </div>                        
                  </div>
                </div>

                <div class="col-lg-5">
                  <label class=""><?= $this->lang->line('custom_package_value'); ?> </label>
                  <input type="text" class="form-control" name="custom_package_value" id="custom_package_value" placeholder="<?= $this->lang->line('custom_package_value'); ?>" readonly />
                </div>

                <div class="col-lg-4">
                  <label class=""><?= $this->lang->line('old_new_goods'); ?> </label>
                  <div class="">                      
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="old_goods" value="Old" name="old_new_goods">
                      <label for="old_goods"> <?= $this->lang->line('old_goods'); ?> </label>
                    </div>
                    <div class="radio radio-success radio-inline">
                      <input type="radio" id="new_goods" value="New" name="old_new_goods">
                      <label for="new_goods"> <?= $this->lang->line('new_goods'); ?> </label>
                    </div>                        
                  </div><br/>
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 text-center" style="margin-top: 0px;">
                <div class="">
                  <a id="btn_total_price" class="btn btn-success"><i class="fa fa-search"></i><br /><?= $this->lang->line('find_best_price'); ?></a>
                </div>
              </div>
              <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12" style="margin-bottom: -30px; margin-top: -20px;">  
                <div class="hpanel m-t-md " style="box-shadow: 1px 1px 5px #ccc;">
                  <div class="panel-body" style="padding: 10px 0px 0px 0px !important;">
                    <input type="hidden" id="total_price" value="0" />
                    <input type="hidden" id="standard_price" value="0" />
                    <input type="hidden" id="total_price_with_promocode" name="total_price_with_promocode"  value="0">
                    <input type="hidden" id="hidden_promo_code" name="hidden_promo_code"  value="NULL">
                    <p class="text-left col-lg-7" style="border-right: 1px solid #ccc;">
                      <span id="core_price"><?= $this->lang->line('best_price');?> : ?</span><br/>
                      <span id="urgent_fee"><?= $this->lang->line('urgency_fee');?> : ?</span><br/>
                      <span id="insurance_fee2"><?= $this->lang->line('insurance_fee');?> : ?</span><br/>
                      <span id="handling_fee"><?= $this->lang->line('handling_fee');?> : ?</span><br/>
                      <span id="dedicated_vehicle_fee"><?= $this->lang->line('dedicated_vehicle_fee');?> : ?</span><br/>
                      <span id="custom_clearance_fee"><?= $this->lang->line('custom_clearance_fee');?> : ?</span><br/>
                      <span id="loding_charges"><?= $this->lang->line('loading_unloading_charges');?> : ?</span><br/>
                      <span id="promo_code_discount" style="color: #3498db"><?= $this->lang->line("Promo Code Distcount"); ?> : ?</span><br/>                    
                    </p>
                    <div class="text-center col-lg-5">                          
                      <i class="fa fa-money fa-3x"></i>
                      <h4 class="m-xs"><span> <?= $this->lang->line('Total_Price'); ?> </span></h4>
                      <h4 class="m-xs text-success" id="price2">?</h4>
                      <h5 class="text-success" id="price_promo_ind"></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xl-offset-4 col-lg-offset-3" style="margin-bottom: -30px; margin-top: 0px;">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="margin-top: 20px;padding-left: 0px;">
                  <label class> <?=$this->lang->line('use promocode')?> </label>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9" style="margin-top: 15px; display: inline-flex;padding-right: 0px;">
                  <input type="text" class="form-control" name="promo_code" id="promo_code"  placeholder="<?= $this->lang->line('Enter Promocode'); ?>" style="text-transform:uppercase" autocomplete="off" readonly/> &nbsp;&nbsp;&nbsp;
                  <button type="button" class="btn btn-info" id="btn_submit_promo"><?= $this->lang->line('Apply'); ?></button>
                </div>
              </div>
            </div>

          </div>
          <hr style="margin-top: 40px;"/>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-bottom: -15px; margin-top: -15px;">
            <div class="header-title text-center"><h4><?= $this->lang->line('advance_payment_details'); ?></h4></div>
            <br/>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
            <label><?= $this->lang->line('minimum_advance'); ?> (<span id="min_adv2">0</span>)</label>
            <input type="hidden" id="min_adv" value="0" />
            <input type="number" placeholder="<?= $this->lang->line('minimum_advance'); ?>" class="form-control pay" name="advance_pay" id="advance_pay" readonly />
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
            <label><?= $this->lang->line('at_pick_up'); ?></label>
            <input type="number" placeholder="<?= $this->lang->line('at_pick_up'); ?>" class="form-control pay" name="pickup_pay" id="pickup_pay" readonly min="0" />                
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="margin-bottom: -5px;">
            <label><?= $this->lang->line('at_deliver'); ?></label>
            <input type="number" placeholder="<?= $this->lang->line('at_deliver'); ?>" class="form-control pay" name="deliver_pay" id="deliver_pay" readonly min="0" />  <br/>            
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
            <label><h4><?= $this->lang->line('payment_mode'); ?></h4></label><br/>
          </div>
          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
            <div class="radio radio-success radio-inline">
              <input type="radio" id="payment_mode_cod" name="payment_mode" value="cod" disabled="disabled">
              <label for="payment_mode"> <?= $this->lang->line('Cash Payment'); ?> </label>
            </div>
            <div class="radio radio-success radio-inline">
              <input type="radio" id="payment_mode_mobile" name="payment_mode" value="payment" disabled="disabled">
              <label for="payment_mode"> <?= $this->lang->line('pay_by_card_or_mobile_money'); ?> </label>
            </div>
            <?php if($user_details['allow_payment_delay'] == "1"): ?>
              <div class="radio radio-success radio-inline">
                <input type="radio" id="payment_mode_bank" name="payment_mode" value="bank" disabled="disabled">
                <label for="payment_mode"> <?= $this->lang->line('Pay by Bank or Cheque'); ?> </label>
              </div>
            <?php endif; ?>
          </div>
          <div class="col-xl-8 col-xl-offset-4 col-lg-8 col-lg-offset-4 col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4">
            <div class="radio radio-success hidden" id="pickupDiv">
              <input type="radio" id="at_pickup" name="cod_payment_type" value="at_pickup">
              <label for="at_pickup"> <?= $this->lang->line('at_pick_up'); ?> </label>
            </div>
            <div class="radio radio-success hidden" id="deliverDiv">
              <input type="radio" id="at_deliver" name="cod_payment_type" value="at_deliver">
              <label for="at_deliver"> <?= $this->lang->line('at_deliver'); ?> </label>
            </div>
          </div>
        </div>
        <div class="panel-footer"> 
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-left">
              <a href="<?= base_url('transport/my-bookings'); ?>" class="btn btn-primary" type="submit"><?= $this->lang->line('back'); ?></a>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
              <button class="btn btn-info" type="submit" id="btn_create" style="pointer-events: none;" disabled><?= $this->lang->line('post_to_market_place'); ?></button>
            </div>
          </div>         
        </div>
      </form>
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center"><?= $this->lang->line('add_new_address'); ?></h4>
            </div>
            <div class="modal-body">
              <form method="post" class="" id="addAddress" enctype="multipart/form-data"> 

                <div class="map-data">

                  <div class="row">
                    <div class="form-group">
                      <label for="location"><?= $this->lang->line('search_address_in_map'); ?></label>
                      <input type="text" class="form-control" id="location" name="address2" placeholder="<?= $this->lang->line('search_address_in_map'); ?>"  />
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <div id="map" class="map_canvas" style="width: 100%; height: 250px;"></div>
                    </div>
                  </div>

                  <div class="row">
                    <input type="hidden" data-geo="lat" name="latitude" value="" />
                    <input type="hidden" data-geo="lng" name="longitude" value="" />
                    <div class="form-group">                 
                      <div class="col-lg-2">
                        <label class=""><?= $this->lang->line('zip_code'); ?></label>
                        <input type="text" data-geo="postal_code" id="zipcode" name="zipcode" class="form-control" placeholder="<?= $this->lang->line('zip_code'); ?>"  />
                      </div>
                      <div class="col-lg-6">
                        <label class=""><?= $this->lang->line('address_line_1'); ?></label>
                        <input type="text" data-geo="formatted_address" id="address1" name="address1" class="form-control" placeholder="<?= $this->lang->line('address_line_1'); ?>"  />
                      </div>
                      <div class="col-lg-4">
                        <label class=""><?= $this->lang->line('street_name'); ?></label>
                        <input type="text" data-geo="street_number" id="street" name="street" class="form-control" placeholder="<?= $this->lang->line('street_name'); ?>" />  <br/>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">                 
                      <div class="col-lg-4">
                        <label for=""><?= $this->lang->line('country'); ?></label>
                        <input type="text" data-geo="country" id="country" name="country" class="form-control" placeholder="<?= $this->lang->line('country'); ?>" readonly/>
                      </div>
                      <div class="col-lg-4">
                        <label for="administrative_area_level_1"><?= $this->lang->line('state'); ?></label>
                        <input type="text" data-geo="administrative_area_level_1" id="state" name="state" class="form-control" id="" placeholder="<?= $this->lang->line('state'); ?>" readonly/>                      
                      </div>
                      <div class="col-lg-4">
                        <label class=""><?= $this->lang->line('city'); ?></label>
                        <input type="text" data-geo="locality" id="city" name="city" class="form-control" placeholder="<?= $this->lang->line('city'); ?>" readonly/> <br/>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-lg-4">
                        <label>Address Type</label>
                        <select id="address_type" name="address_type" class="form-control select2" data-allow-clear="true">
                          <option value="Individual"><?= $this->lang->line('individual'); ?></option></option>
                          <option value="Commercial"><?= $this->lang->line('commercial'); ?></option>
                        </select>
                      </div>
                      <div class="col-lg-8">
                        <label><?= $this->lang->line('compnay_name'); ?></label>
                        <input type="text" class="form-control" id="company" name="company" placeholder="Enter Compnay Name" readonly /> <br/>
                      </div>                    
                    </div> 
                  </div>

                  <div class="row">
                    <div class="form-group">                    
                      <div class="col-lg-6">
                        <label class=""><?= $this->lang->line('mobile_number'); ?></label>
                        <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Enter mobile number" />
                      </div>
                      <div class="col-lg-6">
                        <label><?= $this->lang->line('email_address'); ?></label>
                        <input type="email" class="form-control" id="email_id" name="email_id" placeholder="Enter Email Address" /> <br/>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group"> 
                      <div class="col-lg-4">
                        <label><?= $this->lang->line('address_photo:'); ?></label>
                        <!-- image-preview-filename input [CUT FROM HERE]-->
                        <div class="">
                          <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled" />
                            <span class="input-group-btn">
                              <!-- image-preview-clear button -->
                              <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                <span class="glyphicon glyphicon-remove"></span> <?= $this->lang->line('clear'); ?>
                              </button>
                              <!-- image-preview-input -->
                              <div class="btn btn-default image-preview-input">
                                <span class="glyphicon glyphicon-folder-open"></span>
                                <span class="image-preview-input-title"><?= $this->lang->line('browse'); ?></span>
                                <input type="file" accept="image/png, image/jpeg, image/gif" name="address_image"/> <!-- rename it -->
                              </div>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <label class=""><?= $this->lang->line('first_name'); ?></label>
                        <input type="text" data-geo="name" id="firstname" name="firstname" class="form-control" placeholder="Enter First Name" />
                      </div>
                      <div class="col-lg-4">
                        <label class=""><?= $this->lang->line('last_name'); ?></label>
                        <input type="text" id="lastname" name="lastname" class="form-control" id="" placeholder="Enter Last Name" /> <br/>
                      </div> 
                    </div> 
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-lg-6">
                        <label class=""><?= $this->lang->line('deliver_instructions'); ?></label>
                        <textarea placeholder="Deliver Instructions" class="form-control" name="deliver_instruction" rows="3" style="resize: none;"></textarea>
                      </div>
                      <div class="col-lg-6">
                        <label class=""><?= $this->lang->line('pickup_instructions'); ?></label>
                        <textarea placeholder="Pickup Instructions" class="form-control" name="pickup_instruction" rows="3" style="resize: none;"></textarea> <br/>
                      </div> 
                    </div>
                  </div>

                </div>

                <div class="panel-footer"> 
                  <div class="row">
                      <div class="col-lg-6 text-left">
                        <a href="<?= base_url('user-panel/address-book'); ?>" class="btn btn-primary"><?= $this->lang->line('go_to_address_book'); ?></a>                            
                      </div>
                      <div class="col-lg-6 text-right">
                        <button class="btn btn-info" data-style="zoom-in" id="btnAddAddress"><?= $this->lang->line('submit_detail'); ?></button>
                      </div>
                   </div>         
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function initialize() {
    $("#location").geocomplete({
      map:".map_canvas",
      location: "Camaroon",
      mapOptions: { zoom: 11, scrollwheel: true, },
      markerOptions: { draggable: true, },
      details: "form",
      detailsAttribute: "data-geo", 
      types: ["geocode", "establishment"],
    });    
  }
  $('#myModal').on('shown.bs.modal', function (e) { initialize();  });
</script>

<script>
  var btn_counter = 1; 
  $(function(){
    var wrapper = $(".input_fields_wrap");
    var add_button = $("#btn_add_package");

    $(add_button).click(function(e) { 
      e.preventDefault();
      $(wrapper).append(
        '<div class="input_fields_wrap">'+
          '<div class="col-xl-10 col-lg-10 col-md-10 col-sm-10"> <hr style="margin-bottom: 10px;" />'+

            '<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right: 0px;">'+
              '<label class=""><?= $this->lang->line('quantity'); ?></label>'+
              '<div class="input-group">'+
                '<input id="c_quantity_'+ btn_counter+'" type="number" name="c_quantity[]" placeholder=<?= $this->lang->line("quantity"); ?> min="1" class="form-control" />'+
                '<div class="input-group-addon"><i class="fa fa-hashtag"></i></div>'+
              '</div>'+
            '</div>'+

            '<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right: 0px; margin-bottom: -10px;">' +
              '<input type="hidden" name="dimension_id[]" id="dimension_id_'+btn_counter+'" value="0" />' +
              '<label class=""><?= $this->lang->line('standard_dimension'); ?> </label>' +
              '<select class="form-control m-b select2 dimensionPackage" name="dimension[]" data-id="'+btn_counter+'"  id="dimension_'+ btn_counter+'" onchange="make_dependent_changes('+btn_counter+');">' +
              '</select>  <br/> ' +
            '</div>' +

            '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-right: 0px;">' +
              '<label class=""><?= $this->lang->line('Width'); ?></label>' +
              '<div class="input-group">' +
                '<input id="c_width_'+ btn_counter+'" type="number"  name="c_width[]" placeholder=<?= $this->lang->line("Width"); ?>  class="form-control" readonly />' +
                '<div class="input-group-addon"><i class="fa fa-hashtag"></i></div>' +
              '</div>' +
            '</div>' +

            '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-right: 0px;">'+
              '<label class=""><?= $this->lang->line('height'); ?></label>'+
              '<div class="input-group">'+
                '<input id="c_height_'+ btn_counter+'" type="number"  name="c_height[]" placeholder=<?= $this->lang->line("height"); ?> class="form-control" readonly />'+
                '<div class="input-group-addon"><i class="fa fa-hashtag"></i></div>'+
              '</div>'+
            '</div>'+

            '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4" style="padding-right: 0px; margin-bottom: -10px;">'+
              '<label class=""><?= $this->lang->line('length'); ?></label>'+
              '<div class="input-group">'+
                '<input id="c_length_'+ btn_counter+'" type="number"  name="c_length[]" placeholder=<?= $this->lang->line("length"); ?> class="form-control" readonly />'+
                '<div class="input-group-addon"><i class="fa fa-hashtag"></i></div>'+
              '</div><br/>'+
            '</div> '+

          '</div>'+

          '<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"> <hr /> &nbsp; <a class="remove_field pull-right text-danger" style="font-size:18px; font-weight:bold; margin-top:-35px; margin-right:-18px;" data-toggle="tooltip" data-placement="top" title="Remove Current Package Detail" data-original-title="Remove Cureent Package Detail"> x </a>' +
            '<div class="form-group" style="padding-top: 5px;">' +
              '<img id="img_dimension_'+ btn_counter+'" class="text-center img-thumbnail img-dimension" src="<?= $this->config->item('resource_url').'noimage.png'; ?>" style="max-height: 80px; max-width: 90px; height: auto;" />' +
            '</div> <br/>'+
          '</div>'+
          
          '<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">' +
            '<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6" style="padding-right: 0px;">' +
              '<label class=""><?= $this->lang->line('total_weight'); ?> </label>' +
              '<div class="">' +
                '<div class="input-group">' +
                  '<input id="c_weight_'+ btn_counter+'" type="number"  name="c_weight[]" placeholder=<?= $this->lang->line("total_weight"); ?> min="1" class="form-control" />' +
                  '<div class="input-group-addon"><i class="fa fa-hashtag"></i></div>' +
                '</div>' +
              '</div>' +
            '</div>' +

            '<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6" style="padding-right: 0px;">' +
              '<label class=""><?= $this->lang->line('weight_unit'); ?> </label>' +
              '<div class="">' +
                '<div class="input-group">' +
                  '<select class="form-control m-b" name="c_unit_id[]"  id="c_unit_id_'+ btn_counter+'">' +
                    '<option value=""> <?= $this->lang->line('select_unit'); ?> </option>' +
                    '<?php foreach ($unit as $u ) :?>' +
                      '<option value="<?= $u['unit_id']; ?>" <?= (strtolower($u['shortname']) == 'kg')?"selected":"" ?> > ' +
                        '<?= ucfirst($u['unit_type']).' ( ' . strtoupper($u['shortname']) . ' )'; ?> ' +
                      '</option>' +
                    '<?php endforeach; ?>' +
                  '</select>' +
                '</div>' +
              '</div> <br/>' +
            '</div>' +
            
            '<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right: 0px;">' +
              '<label class=""><?= $this->lang->line('contents'); ?></label>' +
              '<input id="content_'+ btn_counter+'" name="content[]" type="text" class="form-control" placeholder=<?= $this->lang->line("contents"); ?>> <br/>' +
            '</div>' +

            '<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" style="padding-right: 0px;">' +
              '<label><?= $this->lang->line("dangerous_goods"); ?> ( <?= $this->lang->line("optional"); ?> )</label>' +
              '<div>'+
                '<select id="dangerous_goods_'+btn_counter+'" name="dangerous_goods[]" class="form-control dangerous_goods" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_dangerous_goods'); ?>" disabled>' +
                  '<option value="0"> <?= $this->lang->line("select_dangerous_goods"); ?> </option>' +
                '</select>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>'
      ); 

      var v = $("input[name=transport_type]:checked").val();
      //console.log(v);
      var btnCounter = 1;
      $.ajax({
        type: "POST", 
        url: "get-standard-dimensions", 
        data: { type: v, cat_id : 6 },
        dataType: "json",
        success: function(res){ 
          //console.log(btn_counter);
          btnCounter = btn_counter - 1;
          //console.log(JSON.stringify(res));
          $('#dimension_'+btnCounter).attr('disabled', false);
          $('#dimension_'+btnCounter).empty(); 
          $('#dimension_'+btnCounter).append('<option value="0"><?= $this->lang->line("select_standard_dimension"); ?></option>');
          $.each( res, function(i4, v4){
            $('#dimension_'+btnCounter).append('<option value="'+v4['dimension_id']+'">'+v4['dimension_type']+'</option>');
          });
          $('#dimension_'+btnCounter).append('<option value="-1"> <?= $this->lang->line('other'); ?> </option>');
        },
        error: function(){
          $('#dimension_'+btnCounter).attr('disabled', true);
          $('#dimension_'+btnCounter).empty();
          $('#dimension_'+btnCounter).append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
        }
      });

      var from_country_id = $("#from_country_id").val();
      var to_country_id = $("#to_country_id").val();

      if( from_country_id > 0  && to_country_id > 0 ) {
        $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id }, function(data) {
          //console.log('#dangerous_goods_'+btn_counter);
          if(data['dangerous_goods']){                
            $('#dangerous_goods_'+(btn_counter-1)).attr('disabled', false);
            $('#dangerous_goods_'+(btn_counter-1)).empty(); 
            $('#dangerous_goods_'+(btn_counter-1)).append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');

            $.each( data['dangerous_goods'], function(i, v) {                  
              $('#dangerous_goods_'+(btn_counter-1)).append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>');                  
            });
          } else {
            $('#dangerous_goods_'+(btn_counter-1)).attr('disabled', true);
            $('#dangerous_goods_'+(btn_counter-1)).empty();
            $('#dangerous_goods_'+(btn_counter-1)).append('<option value=""><?= $this->lang->line('no_option'); ?>,</option>');
          }
        },"json");        
      } else {
        $('#dangerous_goods_'+(btn_counter-1)).attr('disabled', true);
        $('#dangerous_goods_'+(btn_counter-1)).empty(); 
        $('#dangerous_goods_'+(btn_counter-1)).append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
      }
      btn_counter++; 
    });
  
    $(wrapper).on("click",".remove_field", function(e){ e.preventDefault(); 
      $(this).parent().parent().remove(); 
      if(btn_counter > 1 ) { btn_counter--; } else { btn_counter = 0; } 
      $("#lbl_no_of_packages").text(btn_counter);
    });
  });

  function make_dependent_changes(id){
    var d = $("#dimension_"+id).val();
    var selected_id = $("#dimension_"+id).val();
    //console.log(d);
    $("#lbl_no_of_packages").text(btn_counter);
    $("#img_dimension_"+id).prop('src',"");
    var d_ar = <?= json_encode($dimension);?>;
    //console.log(d_ar);
    d--;
    if(d > -1){
      $.each(d_ar, function(i, v) { 
        if(v['dimension_id'] == selected_id) { 
          $("#dimension_id_"+id).val(v['dimension_id']);
          $("#c_height_"+id).prop('readonly', true).val(v['height']);
          $("#c_width_"+id).prop('readonly', true).val(v['width']);
          $("#c_length_"+id).prop('readonly', true).val(v['length']);

          //SA-------------------------------
          $("#lbl_width_"+id).text(v['width']);
          $("#lbl_height_"+id).text(v['height']);
          $("#lbl_length_"+id).text(v['length']);
          //---------------------------------

          $("#btn_total_price_"+id).attr('disabled', false);
          var base = <?= json_encode(base_url()); ?>;
          //console.log(base+v['image_url']);
          if(v['image_url'] != "NULL") { $("#img_dimension_"+id).prop('src', base + v['image_url']); }
          else { $("#img_dimension_"+id).prop('src', base + 'resources/no-image.jpg'); }
        } 
      });
    } else {
      if(d == -1) { $("#c_height_"+id+", #c_width_"+id+",#c_length_"+id).prop('readonly', true).val(''); 
      } else { $("#c_height_"+id+", #c_width_"+id+",#c_length_"+id).prop('readonly', false).val(''); } 
       
      $("#dimension_id_"+id).val('0'); 
      //SA-------------------------------
      $("#lbl_width_"+id).text(0);
      $("#lbl_height_"+id).text(0);
      $("#lbl_length_"+id).text(0); 
      //---------------------------------
      var base = <?= json_encode(base_url()); ?>;
      $("#img_dimension_"+id).prop('src', base + 'resources/no-image.jpg');
    }
  }
</script>

<!-- International selection  -->
<script>
  $(function(){ 
    $("#international").on('click',function(){      
      $("#custom_charge_div").removeClass('hidden');       
    });

    $("#local, #national").on('click',function(){      
      $("#custom_charge_div").addClass('hidden');       
    });

    $("#provider").on('click',function(){      
      $("#custom_package_value").prop('readonly', false);       
    });

    $("#self").on('click',function(){      
      $("#custom_package_value").prop('readonly', true);       
    });

    $("input[name='old_new_goods']").on('click',function(){      
      var frm_adr = $("#from_address_check").is(":checked");
      var frm_rly = $("#from_relay_check").is(":checked");

      if(frm_adr) { 
        if($.trim($("#from_address").val()) == "") { 
          swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_address"); ?>','error'); 
        }         
      }
      else if(frm_rly) { 
        if($.trim($("#from_relay").val()) == "") { 
          swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_relay"); ?>','error'); 
        }         
      }
    });
  });
</script>

<script>
  function show_insurance_field() { 
    var frm_adr = $("#from_address_check").is(":checked");
    var frm_rly = $("#from_relay_check").is(":checked");

    if(frm_adr) { 
      var from_id = $("#from_address").val();
      $.ajax({
        type: "POST", 
        url: "<?=base_url('user-panel/get-currency-by-country-relay-id')?>", 
        data: { from_id: from_id, address_type: 'address' },
        dataType: "json",
        success: function(res) { 
          $('.package_value_currency_icon').text(res);
        }
      });

      if($.trim($("#from_address").val()) == "") { 
        swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_address"); ?>'); 
        $("#insurence_no").prop('checked', 'checked');
        $("#insurance_yes").prop('checked', false);
      } else { 
        $('#package_value').prop("readonly",false);   
        $('#btn_fee').prop('disabled', false).css("pointer-events",'inherit'); 
      }
    } else if(frm_rly) { 
      var from_id = $("#from_relay").val();
      $.ajax({
        type: "POST", 
        url: "<?=base_url('user-panel/get-currency-by-country-relay-id')?>", 
        data: { from_id: from_id, address_type: 'address' },
        dataType: "json",
        success: function(res){ 
          $('.package_value_currency_icon').text(res);
        }
      });

      if($.trim($("#from_relay").val()) == "") { 
        swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_relay"); ?>'); 
        $("#insurence_no").prop('checked', 'checked');
        $("#insurence_yes").prop('checked', false);
      } else { 
        $('#package_value').prop("readonly",false);   
        $('#btn_fee').prop('disabled', false).css("pointer-events",'inherit'); 
     }
    } else { 
      $('#package_value').prop("readonly",true);   
      $('#btn_fee').prop('disabled', true).css("pointer-events",'none');   
    }    
  }
  function hide_insurance_field() { 
    $('#package_value').prop('readonly', true).val('');  
    $('#btn_fee').css('pointer-events', 'none').prop('disabled', true);  
    //$('#insurance_fee').addClass('hidden');  
    $("#fee").text('');
  }
  function show_travel_option() {   
    $("#driver_no").prop({'disabled': false, 'checked': "checked"});  
    $("#driver_yes").prop({'disabled': false, 'checked': false});
  }
  function hide_travel_option() {   
    $("#driver_no").prop({'disabled': true, 'checked': "checked"});  
    $("#driver_yes").prop({'disabled': true, 'checked': false});
  }
  // $('input, select, radio').on('change', function() { $(this).valid();   });

  function dayDiff(firstDate, secondDate) {
    firstDate = new Date(firstDate);
    secondDate = new Date(secondDate);
    if (!isNaN(firstDate) && !isNaN(secondDate)) {
      firstDate.setHours(0, 0, 0, 0); //ignore time part
      secondDate.setHours(0, 0, 0, 0); //ignore time part
      var dayDiff = secondDate - firstDate;
      dayDiff = dayDiff / 86400000; // divide by milisec in one day
      return dayDiff;
    } else { return false; }
  }

  function getDiffInHours(fdate, sdata) {
    return Math.round(Math.abs(fdate - sdata) / 36e5);
  }

  $(function(){   
    var tDate = new Date();
    var day = (tDate.getDate() < 10 ) ? '0'+tDate.getDate() : tDate.getDate();
    var month = ((tDate.getMonth()+1) < 10 ) ? '0'+(tDate.getMonth()+1) : (tDate.getMonth()+1);
    var year = tDate.getFullYear();
    var hr = tDate.getHours();
    var min = tDate.getMinutes();
    var expiry_month = 0;
    var current_month = tDate.getMonth()+1; 
    var next_month = tDate.getMonth()+2;
    var expiry_year = year;
    var year_change = 0;
    if(current_month<12) { expiry_month = (next_month < 10 ) ? '0'+next_month : next_month; } else { expiry_month = '01'; year_change = 1; }
    if(year_change == 1) { expiry_year += 1 }

    $("#pickupdate").val(month +'/'+day+'/'+year);

    var pick_time_session = $("#pick_time_session").val();
    if(pick_time_session != '') {
      $("#pickuptime").clockpicker({  
      }).val($("#pick_time_session").val());
    } else { $("#pickuptime").val(hr+':'+min); }

    $("#deliverdate").val(month +'/'+day+'/'+year);

    var deliver_time_session = $("#deliver_time_session").val();
    if(deliver_time_session != '') {
      $("#delivertime").clockpicker({  
      }).val($("#deliver_time_session").val());
    } else { $("#delivertime").val((hr+1)+':'+min); }

    $("#expiry_date").val(expiry_month +'/'+day+'/'+expiry_year);

    $('#addBookingForm :input').not("#addBookingForm textarea, #addBookingForm input[name='travel_with'], #addBookingForm input[name='shipping_mode'], #addBookingForm .pay, #ref_no, #payment_mode_cod, #payment_mode_mobile, #payment_mode_bank, #at_pickup, #at_deliver, #promo_code").change(function(){
      $("#price").css('display', 'none');
      $("#total_price").val('0');
      $("#comission, #comission2").text('');
      $("#price").text('');
      $("#price2").text('?');
      $("#min_adv").val('0');
      $("#min_adv2").text('');
      $("#advance_pay").val('').attr('readonly', true);
      $("#pickup_pay").val("").attr('readonly', true);
      $("#deliver_pay").val("").attr('readonly', true);
      $("#promo_code").val("").attr('readonly', true);
      $("#total_price_with_promocode").val('0');
      $("#price_promo_ind").text("");

      $("#core_price").text('<?= $this->lang->line("best_price"); ?> : ?');
      $("#urgent_fee").text('<?= $this->lang->line("urgency_fee"); ?> : ?');
      $("#insurance_fee2").text('<?= $this->lang->line("insurance_fee"); ?> : ?');
      $("#handling_fee").text('<?= $this->lang->line("handling_fee"); ?> : ?');
      $("#dedicated_vehicle_fee").text('<?= $this->lang->line("dedicated_vehicle_fee"); ?> : ?');
      $("#custom_clearance_fee").text('<?= $this->lang->line("custom_clearance_fee"); ?> : ?');
      $("#promo_code_discount").text('<?= $this->lang->line("Promo Code Distcount"); ?> : ?');

      $("#payment_mode_cod").removeAttr("checked");
      $("#payment_mode_mobile").removeAttr("checked");
      $("#payment_mode_bank").removeAttr("checked");
      $('#payment_mode_cod').prop("disabled", true);
      $('#payment_mode_mobile').prop("disabled", true);
      $('#payment_mode_bank').prop("disabled", true);
      $("#at_pickup").removeAttr("checked");
      $("#at_deliver").removeAttr("checked");
      $("#pickupDiv").addClass("hidden");
      $("#deliverDiv").addClass("hidden");
      $("#btn_create").attr('disabled', true).css('pointer-events','inherit');
    });

    $("#addBookingForm").validate({
      ignore: [],
      rules:{
        // transport_type: { required:true },
        from_address: { required:true },
        to_address: { required:true, notEqualTo: "#from_address" },
        from_relay: { required:true },
        to_relay: { required:true, notEqualTo: "#from_relay" },
        quantity: {  required:true, },
        content: {  required:true, },
        vehicle: {  required:true, },
        dimension: {  required:true, },
        c_width: {  required:function(element){ return $("#dimension").val() !=""; } , number: true, },
        c_height: {  required:function(element){ return $("#dimension").val() !=""; }, number: true, },
        c_length: {  required:function(element){ return $("#dimension").val() !=""; }, number: true, },
        c_weight: {  required:true, number: true, },
        c_unit: {  required:function(element){ return $("#c_weight").val() !=""; } },
        advance_pay: {  required:true, number:true, greaterThan: "#min_adv", },        
        custom_package_value: {  required:function(element){ return $("#provider").is(":chekced"); }, number:true, },        
      },
      messages:{
        // transport_type: { required:"Select transport type" },
        from_address: { required:"<?= $this->lang->line('from_address_not_found'); ?>" },
        to_address: { required:"<?= $this->lang->line('to_address_not_found'); ?>", notEqualTo: "<?= $this->lang->line('same_address_selected'); ?>" },
        from_relay: { required:"<?= $this->lang->line('from_relay_not_found'); ?>" },
        quantity: { required:"<?= $this->lang->line('enter_total_qty'); ?>" },
        content: { required:"<?= $this->lang->line('enter_content_details'); ?>" },
        vehicle: { required:"<?= $this->lang->line('select_transport_vehicle'); ?>" },
        dimension: { required:"<?= $this->lang->line('select_standard_dimension'); ?>" },
        c_width: { required:"<?= $this->lang->line('enter_width'); ?>", number: "<?= $this->lang->line('enter_numbers_only'); ?>" },
        c_height: { required:"<?= $this->lang->line('enter_height'); ?>", number: "<?= $this->lang->line('enter_numbers_only'); ?>" },
        c_length: { required:"<?= $this->lang->line('enter_length'); ?>", number: "<?= $this->lang->line('enter_numbers_only'); ?>" },
        c_weight: { required:"<?= $this->lang->line('enter_weight'); ?>", number: "<?= $this->lang->line('enter_numbers_only'); ?>" },
        c_unit: { required:"<?= $this->lang->line('select_unit'); ?>" },
        advance_pay: {  required:"<?= $this->lang->line('enter_advance_payment'); ?>", number:"<?= $this->lang->line('enter_numbers_only'); ?>", greaterThan: "<?= $this->lang->line('pay_min_advance_price'); ?>" },        
        custom_package_value: {  required:"<?= $this->lang->line('enter_custom_package_value'); ?>", number:"<?= $this->lang->line('enter_numbers_only'); ?>", },        
      }
    });

    $("#from_relay_check").on('click',function(){
      if( $(this).is(':checked')) {
        $("#frm_addr_type").val('relay');
        $("#from_address").attr("disabled", true).prop('selectedIndex', 0).change();
        $("#filterarea1").slideDown(1000);
        $("#lbl_departure").text('<?= $this->lang->line('select_relay'); ?>');
        $("#from_address_lat_long_hidden").val('');
      }
    });

    $("#from_address_check").on('click',function(){
      if( $(this).is(':checked')) {
        $("#frm_addr_type").val('address');
        $("#from_address").attr("disabled", false);
        $("#from_relay").attr("disabled", true).prop('selectedIndex', 0).change();
        $("#filterarea1").slideUp(1000);
        $("#lbl_departure").text('<?= $this->lang->line('select_address'); ?>');
        $("#from_relay_lat_long_hidden").val('');
      }
    });

    $("#to_address_check").on('click',function(){ 
      if( $(this).is(':checked')) {
        $("#to_addr_type").val('address');
        $("#to_address").attr("disabled", false);
        $("#to_relay").attr("disabled", true).prop('selectedIndex', 0).change();
        $("#filterarea2").slideUp(1000);
        $("#lbl_arrival").text('<?= $this->lang->line('select_address'); ?>');
        $("#to_relay_lat_long_hidden").val('');
      }
    });

    $("#to_relay_check").on('click',function(){
      if( $(this).is(':checked')) {
        $("#to_addr_type").val('relay');
        //$("#to_relay").attr("disabled", false);
        $("#to_address").attr("disabled", true).prop('selectedIndex', 0).change();
        $("#filterarea2").slideDown(1000);
        $("#lbl_arrival").text("<?= $this->lang->line('select_relay'); ?>");
        $("#to_address_lat_long_hidden").val('');
      }
    });

    $("#filterarea1_btn").on('click',function() {
      var country_id1 = $("#country_id1").val();
      var state_id1 = $("#state_id1").val();
      var city_id1 = $("#city_id1").val();

      $("#from_country_id").val(country_id1);
      if(country_id1 != "" ) { 
        $.ajax({
          type: "POST", 
          url: "get-relay-by-location", 
          data: { country_id:country_id1, state_id:state_id1, city_id:city_id1  },
          dataType: "json",
          success: function(res){ 
            $('#from_relay').attr('disabled', false);
            $('#from_relay').empty(); 
            $('#from_relay').append('<option value=""><?= json_encode($this->lang->line('select_relay')); ?></option>');
            $.each( res, function(){
              if(typeof($(this).attr('city_name'))  === "undefined") var city_name = '';
              else var city_name = $(this).attr('city_name');
              if(typeof($(this).attr('street_name'))  === "undefined") var street_name = '';
              else var street_name = $(this).attr('street_name');
              if(typeof($(this).attr('lastname'))  === "undefined") var lastname = '';
              else var lastname = $(this).attr('lastname');
              if(typeof($(this).attr('firstname'))  === "undefined") var firstname = '';
              else var firstname = $(this).attr('firstname');
              if(typeof($(this).attr('comapny_name'))  === "undefined") var comapny_name = '';
              else var comapny_name = $(this).attr('comapny_name');

              $('#from_relay').append('<option value="'+$(this).attr('relay_id')+'">'+comapny_name+' | '+city_name+' | '+street_name +'</option>');});
            $('#from_relay').focus();
          },
          beforeSend: function(){
            $('#from_relay').empty();
            $('#from_relay').append('<option value=""><?= json_encode($this->lang->line('loading')); ?>,</option>');
          },
          error: function(){
            $('#from_relay').attr('disabled', true);
            $('#from_relay').empty();
            $('#from_relay').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
          }
        });
      } else { 
        $('#from_relay').empty(); 
        $('#from_relay').append('<option value=""><?= json_encode($this->lang->line('select_relay')); ?></option>');
        $('#from_relay').attr('disabled', true); 
      }
    });

    $("#filterarea2_btn").on('click',function(){
      var country_id2 = $("#country_id2").val();
      var state_id2 = $("#state_id2").val();
      var city_id2 = $("#city_id2").val();
      $("#to_country_id").val(country_id2);
      //alert(country_id2+' '+state_id2+' '+city_id2);
      if(country_id2 != "" ) { 
        $.ajax({
          type: "POST", 
          url: "get-relay-by-location", 
          data: { country_id:country_id2, state_id:state_id2, city_id:city_id2  },
          dataType: "json",
          success: function(res){ 
            // console.log(res);              
            $('#to_relay').attr('disabled', false);
            $('#to_relay').empty(); 
            $('#to_relay').append('<option value=""><?= $this->lang->line("select_relay"); ?></option>');
            $.each( res, function(){                
              if(typeof($(this).attr('city_name'))  === "undefined") var city_name = '';
              else var city_name = $(this).attr('city_name');
              if(typeof($(this).attr('street_name'))  === "undefined") var street_name = '';
              else var street_name = $(this).attr('street_name');
              if(typeof($(this).attr('lastname'))  === "undefined") var lastname = '';
              else var lastname = $(this).attr('lastname');
              if(typeof($(this).attr('firstname'))  === "undefined") var firstname = '';
              else var firstname = $(this).attr('firstname');
              if(typeof($(this).attr('comapny_name'))  === "undefined") var comapny_name = '';
              else var comapny_name = $(this).attr('comapny_name');

              $('#to_relay').append('<option value="'+$(this).attr('relay_id')+'">'+comapny_name+' | '+city_name+' | '+street_name +'</option>');});
            $('#to_relay').focus();
          },
          beforeSend: function(){
            $('#to_relay').empty();
            $('#to_relay').append('<option value=""><?= json_encode($this->lang->line('loading')); ?>,</option>');
          },
          error: function(){
            $('#to_relay').attr('disabled', true);
            $('#to_relay').empty();
            $('#to_relay').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
          }
        });
      } else { 
        $('#to_relay').empty(); 
        $('#to_relay').append('<option value=""><?= $this->lang->line("select_relay"); ?></option>');
        $('#to_relay').attr('disabled', true); 
      }
    });

    $("#dimension").on('change', function(event) {  event.preventDefault();
      var d = $(this).val();
      $("#img-dimension").prop('src',"");
      var d_ar = <?= json_encode($dimension);?>;
    
      if(d > -1) {
        $.each(d_ar, function(i, v) {
          if(v['dimension_id'] == d) {
            $("#dimension_id").val(v['dimension_id']);
            $("#c_height").prop('readonly', true).val(v['height']);
            $("#c_width").prop('readonly', true).val(v['width']);
            $("#c_length").prop('readonly', true).val(v['length']);

            //SA-------------------------------
            $("#lbl_width").text(v['width']);
            $("#lbl_height").text(v['height']);
            $("#lbl_length").text(v['length']);
            //---------------------------------

            $("#btn_total_price").attr('disabled', false);
            var base = <?= json_encode(base_url()); ?>;
            //alert("if " + v['dimension_id']);
            if(v['image_url'] != "NULL" ) { $("#img-dimension").prop('src', base + v['image_url']); }
            else {  $("#img-dimension").prop('src', base + 'resources/no-image.jpg'); }
          } else {
            if(d == '') {
              var base = <?= json_encode(base_url()); ?>;
              $("#img-dimension").prop('src', base + 'resources/no-image.jpg');
              $("#c_height").prop('readonly', true).val('');
              $("#c_width").prop('readonly', true).val('');
              $("#c_length").prop('readonly', true).val('');
            }
          } 
        });
      } else {  
        $("#c_height, #c_width, #c_length").prop('readonly', false).val(''); 
        $("#dimension_id").val('0'); 
        //SA-------------------------------
        $("#lbl_width").text(0);
        $("#lbl_height").text(0);
        $("#lbl_length").text(0); 
        //---------------------------------
        var base = <?= json_encode(base_url()); ?>;
        $("#img-dimension").prop('src', base + 'resources/no-image.jpg');
      }
    });

    $("input[name=transport_type]").on("change",function(){ 
      var v = $("input[name=transport_type]:checked").val(); 
      $('#vehicle').empty();

      $.ajax({
        type: "POST", 
        url: "get-transport", 
        data: { type: v, cat_id: 6 },
        dataType: "json",
        success: function(res){ 
          $('#vehicle').attr('disabled', false);
          $('#vehicle').empty(); 
          $('#vehicle').append('<option value=""><?= $this->lang->line("select_transport_vehicle"); ?></option>');
          $.each( res, function(i1, v1){
            $('#vehicle').append('<option value="'+v1['vehical_type_id']+'">'+v1['vehicle_type']+'</option>');
          });
          $('#vehicle').focus();
        },
        beforeSend: function(){
          $('#vehicle').empty();
          $('#vehicle').append('<option value=""><?= json_encode($this->lang->line('loading')); ?>,</option>');
        },
        error: function(){
          $('#vehicle').attr('disabled', true);
          $('#vehicle').empty();
          $('#vehicle').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
        }
      });

      $.ajax({
        type: "POST", 
        url: "get-standard-dimensions", 
        data: { type: v, cat_id : 6 },
        dataType: "json",
        success: function(res) {  
          $('#dimension').attr('disabled', false);
          $('#dimension').empty(); 
          $('#dimension').append('<option value=""><?= $this->lang->line("select_standard_dimension"); ?></option>');
          $.each( res, function(i2, v2){
            $('#dimension').append('<option value="'+v2['dimension_id']+'">'+v2['dimension_type']+'</option>');
          });
          $('#dimension').append('<option value="-1"> <?= $this->lang->line('other'); ?> </option>');

          $('.dimensionPackage').attr('disabled', false);
          $('.dimensionPackage').empty(); 
          $('.dimensionPackage').append('<option value=""><?= $this->lang->line("select_standard_dimension"); ?></option>');
          $.each( res, function(i3, v3){
            $('.dimensionPackage').append('<option value="'+v3['dimension_id']+'">'+v3['dimension_type']+'</option>');
          });
          $('.dimensionPackage').append('<option value="-1"> <?= $this->lang->line('other'); ?> </option>');

        },
        beforeSend: function(){
          $('#dimension').empty();
          $('#dimension').append('<option value=""><?= json_encode($this->lang->line('loading')); ?>,</option>');

          $('.dimensionPackage').empty();
          $('.dimensionPackage').append('<option value=""><?= json_encode($this->lang->line('loading')); ?>,</option>');
        },
        error: function(){
          $('#dimension').attr('disabled', true);
          $('#dimension').empty();
          $('#dimension').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');

          $('.dimensionPackage').attr('disabled', true);
          $('.dimensionPackage').empty();
          $('.dimensionPackage').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
        }
      });
    });

    $("#btn_fee").on('click', function(event) {  event.preventDefault();      
      $("#insurance_fee").removeClass('hidden');
      $("#fee").text(''); 
      var frm_adr = $("#from_address_check").is(":checked");
      var frm_rly = $("#from_relay_check").is(":checked");
      var addr_id = 0;
      var addr_type = null;
      var value = $("#package_value").val();
      var category_id = $("#category_id").val();

      if(frm_adr) { addr_id = $("#from_address").val(); addr_type = "address";  }
      else { addr_id = $("#from_relay").val(); addr_type = "relay"; } 
      if(value > 0 && value != "") {
        $.post('insurance-price', { addr_id: addr_id, addr_type: addr_type, value: value, category_id: category_id }, function(res) {        
          //console.log(res);
          if($.trim(res) != 'false' && $.trim(res) != 'null'){  
            var v = $.parseJSON(res);          
            $("#insurance_fee").css('display', 'inherit');
            $("#fee").text(v['currency_sign'] + ' ' +v['ins_fee']);
          }
          else{ $("#fee").text('?'); swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("no_value_found"); ?>'); }
        });
      } else { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_valid_package_value"); ?>'); }
    });   

    $("#btn_total_price").on('click', function(event) { event.preventDefault();
      var btn_total_price = $(this);      
      var frm_adr = $("#from_address_check").is(":checked");
      var to_adr = $("#to_address_check").is(":checked");
      var frm_rly = $("#from_relay_check").is(":checked");
      var to_rly = $("#to_relay_check").is(":checked");
      var frm_addr_id = 0;
      var to_addr_id = 0;
      var frm_addr_type = null;
      var to_addr_type = null;

      $("#total_price_with_promocode").val(0);
      $("#price_promo_ind").text('');
      $("#promo_code_discount").text('<?= $this->lang->line("Promo Code Distcount"); ?> : ?');

      var pickupdate = $("#pickupdate").val();
      var pickuptime = $("#pickuptime").val(); 
      var pickupdatetime = pickupdate + ' ' + pickuptime;

      var deliverdate = $("#deliverdate").val();
      var delivertime = $("#delivertime").val(); 
      var deliverdatetime = deliverdate +' ' +delivertime;
      var diffInHrs = getDiffInHours(deliverdatetime, pickupdatetime);

      var loading_time = $("#loading_time").val();
      var dangerous_goods = $("#dangerous_goods").val();
      //console.log(dangerous_goods);
      var c_width = $("input[name='c_width[]']").map(function(){return $(this).val();}).get();
      var c_height = $("input[name='c_height[]']").map(function(){return $(this).val();}).get();
      var c_length = $("input[name='c_length[]']").map(function(){return $(this).val();}).get();
      var c_quantity = $("input[name='c_quantity[]']").map(function(){return $(this).val();}).get();
      var c_weight = $("input[name='c_weight[]']").map(function(){return $(this).val();}).get();
      var c_unit_id = $("select[name='c_unit_id[]']").map(function(){return $(this).val();}).get();
      var insurance_check = 0; 
      var package_value = 0; 

      var transport_type = "";
      if($("#earth").is(":checked")){ transport_type = "earth"; }
      else if($("#air").is(":checked")){ transport_type = "air"; }
      else if($("#sea").is(":checked")){ transport_type = "sea"; }
      else { transport_type =""; }

      var order_mode = "";
      if($("#local").is(":checked")){ order_mode = "local"; }
      else if($("#national").is(":checked")){ order_mode = "national"; }
      else if($("#international").is(":checked")){ order_mode = "international"; }
      else { order_mode =""; }

      var handling_by = "";
      if($("#handle_self").is(":checked")){ handling_by = "0"; }
      else if($("#handle_driver").is(":checked")){ handling_by = "1"; }
      else { handling_by = "0"; }

      var dedicated_vehicle = "";
      if($("#dedicated_no").is(":checked")){ dedicated_vehicle = "0"; }
      else if($("#dedicated_yes").is(":checked")){ dedicated_vehicle = "1"; }
      else { dedicated_vehicle = "0"; }

      if($("#insurance_yes").is(":checked")){ 
        package_value = $("#package_value").val();        
        insurance_check = 1; 
      } else { insurance_check = 0 ; package_value=0; }

      if(frm_adr) { frm_addr_id = $("#from_address").val(); frm_addr_type = "address";  }
      else { frm_addr_id = $("#from_relay").val(); frm_addr_type = "relay"; } 

      if(to_adr) { to_addr_id = $("#to_address").val(); to_addr_type = "address";  }
      else { to_addr_id = $("#to_relay").val(); to_addr_type = "relay"; } 

      var custom_provider = "NULL";
      if($("#provider").is(":checked")){  custom_provider = "Provider"; }
      else if($("#self").is(":checked")){  custom_provider = "Self"; }

      var tailgate = "NULL";
      if($("#tailgate_yes").is(":checked")){  tailgate = "1"; }
      else if($("#tailgate_no").is(":checked")){  tailgate = "0"; }

      var custom_package_value = $("#custom_package_value").val();

      var old_new_goods = "NULL";
      if($("#old_goods").is(":checked")){  old_new_goods = "Old"; }
      else if($("#new_goods").is(":checked")){  old_new_goods = "New"; }

      var category_id = $("#category_id").val();

      if(transport_type == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_transport_type"); ?>', 'warning', false, "#DD6B55");}
      else if(order_mode == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_mode_type"); ?>', 'warning', false, "#DD6B55");}
      else if(frm_addr_id == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_from_address"); ?>', 'warning', false, "#DD6B55");}
      else if(to_addr_id == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_to_address"); ?>', 'warning', false, "#DD6B55");}
      else if(to_addr_id==frm_addr_id){swal('<?= $this->lang->line("error"); ?>','<?= $this->lang->line("from_to_address_same"); ?>','warning',false,"#DD6B55");}      
      else if(diffInHrs < 1) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_pickup_date_time_greater"); ?>', 'warning', false, "#DD6B55");}
      else if(loading_time == "") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_loading_unloading_time"); ?>', 'warning', false, "#DD6B55");}
      else if(c_quantity.length == 0) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_quantity"); ?>', 'warning', false, "#DD6B55");}
      else if(btn_counter > 1 && c_quantity.length != btn_counter) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_quantity"); ?>', 'warning', false, "#DD6B55");}
      else if(c_width.length == 0) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_dimension_or_width"); ?>', 'warning', false, "#DD6B55");}
      else if(btn_counter > 1 && c_width.length != btn_counter) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_width"); ?>', 'warning', false, "#DD6B55");}
      else if(c_height.length == 0) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_dimension_or_height"); ?>', 'warning', false, "#DD6B55");}
      else if(btn_counter > 1 && c_height.length != btn_counter) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_height"); ?>', 'warning', false, "#DD6B55");}
      else if(c_length.length == 0) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("select_dimension_or_length"); ?>', 'warning', false, "#DD6B55");}
      else if(btn_counter > 1 && c_length.length != btn_counter) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_height"); ?>', 'warning', false, "#DD6B55");}
      else if(c_weight.length == 0) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_weight"); ?>', 'warning', false, "#DD6B55");}
      else if(btn_counter > 1 && c_weight.length != btn_counter) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_weight"); ?>', 'warning', false, "#DD6B55");}
      else if(c_unit_id.length == 0) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_weight_unit"); ?>', 'warning', false, "#DD6B55");}
      else if(btn_counter > 1 && c_unit_id.length != btn_counter) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_weight_unit"); ?>', 'warning', false, "#DD6B55");}
      else if(insurance_check==1 && package_value==0) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_package_value"); ?>', 'warning', false, "#DD6B55");}
      else if(order_mode=="international" && custom_provider== true && custom_package_value =="") { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("enter_custom_package_value"); ?>', 'warning', false, "#DD6B55");}
      else if(dangerous_goods=="" || parseInt(dangerous_goods)<1) { swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("Select dangrous goods!"); ?>', 'warning', false, "#DD6B55");}
      else { 
        btn_total_price.button('<?= json_encode($this->lang->line('loading')); ?>,');
        $.post('total-price', {  category_id:category_id, delivery_datetime:deliverdatetime, pickup_datetime:pickupdatetime, transport_type:transport_type, service_area_type:order_mode, frm_addr_id: frm_addr_id, to_addr_id: to_addr_id, frm_addr_type: frm_addr_type, to_addr_type: to_addr_type, c_width: c_width, c_height: c_height, c_length: c_length, total_weight: c_weight, c_unit_id: c_unit_id, insurance_check: insurance_check, package_value: package_value, handling_by: handling_by, dedicated_vehicle:dedicated_vehicle,total_quantity:c_quantity,custom_clearance:custom_provider,custom_package_value:custom_package_value,old_new_goods:old_new_goods, loading_time:loading_time  }, function(res) {          
          //console.log(res);
          btn_total_price.button('reset');
          if(res!= 'false'){                  
            var v = $.parseJSON(res);     
            if(v['total_price'] > 0) {
              $("#price").css('display', 'inherit');
              var standard_price = v['standard_price'];
              var total_price = v['total_price'];
              currency_sign2 = v['currency_sign'];
              $("#standard_price").val(standard_price);
              $("#total_price").val(total_price);
              $("#price, #price2").text( v['currency_sign'] + ' ' + total_price);
 
              $("#core_price").text('<?= $this->lang->line('best_price');?> : ' + v['currency_sign'] + ' ' + v['standard_price']);
              $("#urgent_fee").text("<?= $this->lang->line('urgency_fee');?> :  " + v['currency_sign'] + ' ' + v['urgent_fee']);
              $("#insurance_fee2").text('<?= $this->lang->line('insurance_fee');?> : ' + v['currency_sign'] + ' ' + v['ins_fee']);
              $("#handling_fee").text('<?= $this->lang->line('handling_fee');?> : ' + v['currency_sign'] + ' ' + v['handling_fee'] );
              $("#dedicated_vehicle_fee").text('<?= $this->lang->line('dedicated_vehicle_fee');?> : ' + v['currency_sign'] + ' ' + v['dedicated_vehicle_fee']);
              $("#loding_charges").text('<?= $this->lang->line('loading_unloading_charges');?> : ' + v['currency_sign'] + ' ' + v['loading_unloading_charge']);

              if($("#new_goods").is(':checked')){  
                $("#custom_clearance_fee").text('<?= $this->lang->line('custom_clearance_fee');?> : ' + v['currency_sign'] + ' ' + v['new_goods_custom_commission_fee']);              
              }
              else if($("#new_goods").is(':checked')){  
                $("#custom_clearance_fee").text('<?= $this->lang->line('custom_clearance_fee');?> : ' + v['currency_sign'] + ' ' + v['old_goods_custom_commission_fee']);
              }
              else{ $("#custom_clearance_fee").text('<?= $this->lang->line("custom_clearance_fee"); ?> : ' + v['currency_sign'] + ' 0.00'); }

              var advance_payment = v['advance_payment'];            
              $("#min_adv").val(advance_payment);
              if(v['advance_percent'] != null) {
                $("#min_adv2").text(v['currency_sign'] + ' ' + advance_payment + ' - ' + v['advance_percent'] + ' %');
                advance_percent2 = v['advance_percent'];
              } else {
                $("#min_adv2").text(v['currency_sign'] + ' ' + advance_payment + ' - ' + '0 %');
              }

              $("#advance_pay").val(advance_payment).attr('readonly', false);
              $("#promo_code").val("").attr('readonly', false);
              var remaining = ( parseFloat(total_price).toFixed(2) - parseFloat(advance_payment)).toFixed(2);              
              $("#pickup_pay").val(remaining).attr('readonly', false);
              $("#deliver_pay").val("0").attr('readonly', false);

              $("#payment_mode_cod").removeAttr("checked");
              $("#payment_mode_mobile").removeAttr("checked");
              $("#payment_mode_bank").removeAttr("checked");
              $('#payment_mode_cod').prop("disabled", false);
              $('#payment_mode_mobile').prop("disabled", false);
              $('#payment_mode_bank').prop("disabled", false);
              $("#at_pickup").removeAttr("checked");
              $("#at_deliver").removeAttr("checked");
              $("#pickupDiv").addClass("hidden");
              $("#deliverDiv").addClass("hidden");

              $("#btn_create").attr('disabled', true).css('pointer-events','inherit');
            } else { swal("<?= $this->lang->line("error"); ?>", "<?= $this->lang->line("Price not found for selected address."); ?>"); }
          } else { swal("<?= $this->lang->line("error"); ?>", "<?= $this->lang->line("Price not found for selected address."); ?>"); }
        });
      }
    });

    $("#btn_create").on('click', function(event) { event.preventDefault();
      var total_price = parseFloat($("#total_price").val()).toFixed(2);
      var total_price_with_promocode = parseFloat($("#total_price_with_promocode").val()).toFixed(2);
      //alert(total_price_with_promocode);
      if(total_price_with_promocode!=0){
        total_price = total_price_with_promocode;
      }
      var advance_pay = $("#advance_pay").val();
      var pickup_pay = $("#pickup_pay").val();
      var deliver_pay = $("#deliver_pay").val();
      var standard_price = $("#standard_price").val();

      if(standard_price > 0 ){

        if(advance_pay != "" ) { advance_pay = parseFloat(advance_pay).toFixed(2); } else { advance_pay = 0 ; }
        if(pickup_pay != "" ) { pickup_pay = parseFloat(pickup_pay).toFixed(2); } else { pickup_pay = 0 ; }
        if(deliver_pay != "" ) { deliver_pay = parseFloat(deliver_pay).toFixed(2); } else { deliver_pay = 0 ; }
        var cal_price = parseFloat(advance_pay) + parseFloat(pickup_pay) + parseFloat(deliver_pay);
        var cal_price = parseFloat(cal_price).toFixed(2);
        // console.log(advance_pay + '-------' + pickup_pay + '--------' + deliver_pay + ' ------ ' + cal_price + ' === ' + total_price);
        if(cal_price == total_price) { $("#addBookingForm").get(0).submit();  } 
        else {  swal('<?= $this->lang->line("error"); ?>', "<?= $this->lang->line('detail_not_equal_total_error'); ?>",'error');   }
      } else {  swal('<?= $this->lang->line("error"); ?>', "<?= $this->lang->line('base_price_eror'); ?>",'error');   }
    });
  });
</script>

<!-- country1 state1 change -->
<script>
  var currency_sign2 = "";
  var advance_percent2 = "";
  $("#country_id1").on('click', function(event) {  event.preventDefault();
    var country_id = $(this).val();
   
    $('#city_id1').attr('disabled', true);
    $('#state_id1').empty();

    if(country_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-state-by-country-id", 
        data: { country_id: country_id },
        dataType: "json",
        success: function(res){ 
          $('#state_id1').attr('disabled', false);
          $('#state_id1').empty(); 
          $('#state_id1').append('<option value=""><?= json_encode($this->lang->line("select_state")); ?>,</option>');
          $.each( res, function(){$('#state_id1').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
          $('#state_id1').focus();
        },
        beforeSend: function(){
          $('#state_id1').empty();
          $('#state_id1').append('<option value=""><?= json_encode($this->lang->line("loading")); ?>,</option>');
        },
        error: function(){
          $('#state_id1').attr('disabled', true);
          $('#state_id1').empty();
          $('#state_id1').append('<option value=""><?= json_encode($this->lang->line("no_option")); ?>,</option>');
        }
      });
    } else { 
      $('#state_id1').empty(); 
      $('#state_id1').append('<option value=""><?= json_encode($this->lang->line("select_state")); ?>,</option>');
      $('#state_id1, #city_id1').attr('disabled', true); 
    }
  });

  $("#state_id1").on('click', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    if(state_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-cities-by-state-id", 
        data: { state_id: state_id },
        dataType: "json",
        success: function(res){ 
          $('#city_id1').attr('disabled', false);
          $('#city_id1').empty(); 
          $('#city_id1').append('<option value=""><?= json_encode($this->lang->line("select_city")); ?></option>');
          $.each( res, function(){ $('#city_id1').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
          $('#city_id1').focus();
        },
        beforeSend: function(){
          $('#city_id1').empty();
          $('#city_id1').append('<option value=""><?= json_encode($this->lang->line("loading")); ?></option>');
        },
        error: function(){
          $('#city_id1').attr('disabled', true);
          $('#city_id1').empty();
          $('#city_id1').append('<option value=""><?= json_encode($this->lang->line("no_option")); ?></option>');
        }
      });
    } else { 
      $('#city_id1').empty(); 
      $('#city_id1').append('<option value=""><?= json_encode($this->lang->line("select_city")); ?></option>');
      $('#city_id1').attr('disabled', true); 
    }
  });
</script>

<!-- country2 state2 change -->
<script>
  $("#country_id2").on('click', function(event) {  event.preventDefault();
    var country_id = $(this).val();
   
    $('#city_id2').attr('disabled', true);
    $('#state_id2').empty();

    if(country_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-state-by-country-id", 
        data: { country_id: country_id },
        dataType: "json",
        success: function(res){ 
          $('#state_id2').attr('disabled', false);
          $('#state_id2').empty(); 
          $('#state_id2').append('<option value=""><?= json_encode($this->lang->line("select_state")); ?>,</option>');
          $.each( res, function(){$('#state_id2').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
          $('#state_id2').focus();
        },
        beforeSend: function(){
          $('#state_id2').empty();
          $('#state_id2').append('<option value=""><?= json_encode($this->lang->line("loading")); ?></option>');
        },
        error: function(){
          $('#state_id2').attr('disabled', true);
          $('#state_id2').empty();
          $('#state_id2').append('<option value=""><?= json_encode($this->lang->line("no_option")); ?></option>');
        }
      });
    } else { 
      $('#state_id2').empty(); 
      $('#state_id2').append('<option value=""><?= json_encode($this->lang->line("select_state")); ?>,</option>');
      $('#state_id2, #city_id2').attr('disabled', true); 
    }
  });

  $("#state_id2").on('click', function(event) {  event.preventDefault();
    var state_id = $(this).val();

    if(state_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-cities-by-state-id", 
        data: { state_id: state_id },
        dataType: "json",
        success: function(res){ 
          $('#city_id2').attr('disabled', false);
          $('#city_id2').empty(); 
          $('#city_id2').append('<option value=""><?= json_encode($this->lang->line("select_city")); ?></option>');
          $.each( res, function(){ $('#city_id2').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
          $('#city_id2').focus();
        },
        beforeSend: function(){
          $('#city_id2').empty();
          $('#city_id2').append('<option value=""><?= json_encode($this->lang->line("loading")); ?></option>');
        },
        error: function(){
          $('#city_id2').attr('disabled', true);
          $('#city_id2').empty();
          $('#city_id2').append('<option value=""><?= json_encode($this->lang->line("no_option")); ?></option>');
        }
      });
    } else { 
      $('#city_id2').empty(); 
      $('#city_id2').append('<option value=""><?= json_encode($this->lang->line("select_city")); ?></option>');
      $('#city_id2').attr('disabled', true); 
    }
  });
</script>

<!-- address and relay -->
<script>
  var loading_free_hours = "<?=(isset($_SESSION['loading_free_hours']) && $_SESSION['loading_free_hours'] > 0)?$_SESSION['loading_free_hours']:0?>";
  if(parseInt(loading_free_hours) <= 0 || loading_free_hours == undefined) {
    $('#loading_time').attr('readonly', true);
  }
  $("#c_quantity").on('change', function(event) { 
    $("#lbl_quantity").text($(this).val());
  });
  $("#dimension").on('change', function(event) { 
    $("#lbl_volume").text($("#dimension option:selected").text());
  });
  $("#c_weight").on('change', function(event) { $("#lbl_weight").text($(this).val()); });
  $("#c_unit_id").on('change', function(event) { $("#lbl_unit").text($("#c_unit_id option:selected").text()); });

  $("#c_width").on('change', function(event) { $("#lbl_width").text($(this).val()); });
  $("#c_height").on('change', function(event) { $("#lbl_height").text($(this).val()); });
  $("#c_length").on('change', function(event) { $("#lbl_length").text($(this).val()); });

  $("#pickupdate").on('change', function(event) { 
    if($(this).val() != '') {
      var tDate = new Date($(this).val());
      var numberOfDaysToAdd = 30;
      tDate.setDate(tDate.getDate() + numberOfDaysToAdd); 
      var dd = tDate.getDate();
      var mm = tDate.getMonth() + 1;
      var y = tDate.getFullYear();
      var day = (tDate.getDate() < 10 ) ? '0'+tDate.getDate() : tDate.getDate();
      var month = ((tDate.getMonth()+1) < 10 ) ? '0'+(tDate.getMonth()+1) : (tDate.getMonth()+1);
      var someFormattedDate = month +'/'+day+'/'+y;
      //console.log(someFormattedDate);
      $("#expiry_date").val(someFormattedDate);

      var tDate = new Date($(this).val());
      tDate.setDate(tDate.getDate() + 1); 
      var dd = tDate.getDate();
      var mm = tDate.getMonth();
      var y = tDate.getFullYear();
      var day = (tDate.getDate() < 10 ) ? '0'+tDate.getDate() : tDate.getDate();
      var day = day-1;
      var month = ((tDate.getMonth()+1) < 10 ) ? '0'+(tDate.getMonth()+1) : (tDate.getMonth()+1);
      var someFormattedDate = month +'/'+day+'/'+y;
      
      $('#deliverdate').datepicker('setStartDate', someFormattedDate);
      $("#deliverdate").val(someFormattedDate);
      $('#expiry_date').datepicker('setStartDate', someFormattedDate);

      $("#lbl_pickup_date_time").text($(this).val()); 
    }
  });

  $( ".ddAddon" ).click(function( e ) { 
    $("#deliverdate").focus();
  });

  $(window).load(function() {
    $('#deliverdate').datepicker('setStartDate', new Date());
    $('#expiry_date').datepicker('setStartDate', new Date());
  });

  $("#deliverdate").on('change', function(event) { $("#lbl_deliver_date_time").text($(this).val()); });
  
  /* 
  // working code
    $("#from_address").on('change', function(event) { 
      $("#lbl_departure").text($("#from_address option:selected").text());
      var from_address_id = $("#from_address option:selected").val();
      $('#loading_time').attr('readonly', false);
      if(from_address_id != '' && from_address_id > 0) {
        var addr = <?= json_encode($address); ?>;
        var ad_id = $(this).val();

        var to_country_id = $("#to_country_id").val();
        var from_country_id = $("#from_country_id").val();
        var category_id = $("#category_id").val();

        $.each(addr, function(i,v){        
          if(v['addr_id'] == ad_id) { 
            $("#from_country_id").val(v['country_id']);
            from_country_id = v['country_id'];
            if(v['pickup_instructions'] != "NULL"){ $("#pickup_instruction").val(v['pickup_instructions']);  }
          }
        });

        //alert(from_address_id);
        $.post('get-address-relay-lat-long', { addr_id: from_address_id, type: 'address' }, function(res) {        
          // console.log(res);
          if(res != "null"){ 
            var v = $.parseJSON(res);
            $("#from_address_lat_long_hidden").val(v);
            //alert($("#to_address_lat_long_hidden").val());
            //alert($("#to_relay_lat_long_hidden").val());
            if($("#to_address_lat_long_hidden").val() == ',') var to_lat_long = $("#to_relay_lat_long_hidden").val();
            else var to_lat_long = $("#to_address_lat_long_hidden").val();
            if(to_lat_long != '') {
              var url = "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin="+v+"&destination="+to_lat_long;
              $('#map_frame').attr('src', url);
              //get distance and set in summary
              $.post('get-driving-distance', { from_lat_long: v, to_lat_long: to_lat_long }, function(res) { 
                // console.log(res);
                $("#lbl_distance_in_km").text($.parseJSON($.trim(res)));

                if( from_country_id > 0  && to_country_id > 0 ) {
                  $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id, category_id:category_id}, function(data) {
                    console.log(data);
                    if(data){                
                      $('.dangerous_goods').attr('disabled', false);
                      $('.dangerous_goods').empty(); 
                      $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
                      $.each( data['dan'], function(i, v) {                  
                        $('.dangerous_goods').append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>');                  
                      });

                    }
                    else{
                      $('.dangerous_goods').attr('disabled', true);
                      $('.dangerous_goods').empty();
                      $('.dangerous_goods').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
                    }
                  },"json");        
                } else {
                  $('.dangerous_goods').attr('disabled', true);
                  $('.dangerous_goods').empty(); 
                  $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
                }
              });
            }
          }
          else{ swal('Error', 'No latitude and longitude!'); }
        });
      }
    });
  */

  $("#from_address").on('change', function(event) { 
    $('.package_value_currency_icon').text('?');
    var from_id = $("#from_address").val();
    if(parseInt(from_id)>0 || from_id!=undefined) {
      $.ajax({
        type: "POST", 
        url: "<?=base_url('user-panel/get-currency-by-country-relay-id')?>", 
        data: { from_id: from_id, address_type: 'address' },
        dataType: "json",
        success: function(res){ 
          //console.log(res);
          $('.package_value_currency_icon').text(res);
        }
      });
    }

    $("#lbl_departure").text($("#from_address option:selected").text());
    var from_address_id = $("#from_address option:selected").val();
    $('#loading_time').attr('readonly', false);
    if(from_address_id != '' && from_address_id > 0) {
      var addr = <?= json_encode($address); ?>;
      var ad_id = $(this).val();


      $.each(addr, function(i,v){        
        if(v['addr_id'] == ad_id) { 
          $("#from_country_id").val(v['country_id']);
          from_country_id = v['country_id'];
          if(v['pickup_instructions'] != "NULL"){ $("#pickup_instruction").val(v['pickup_instructions']);  }
        }
      });

      var to_country_id = $("#to_country_id").val();
      var from_country_id = $("#from_country_id").val();
      var category_id = $("#category_id").val();
      //console.log(from_country_id + to_country_id);

      if( ($.trim(parseInt(from_country_id)) > 0 && $.trim(parseInt(to_country_id)) > 0) || $.trim(parseInt(from_country_id)) > 0 ) {
        //console.log(from_country_id + to_country_id);
        $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id, category_id:category_id }, function(data) {
          console.log(data);
          if(data['dangerous_goods']){                
            $('.dangerous_goods').attr('disabled', false);
            $('.dangerous_goods').empty(); 
            $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
            $.each( data['dangerous_goods'], function(i, v) {                  
              $('.dangerous_goods').append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>'); 
            });
            $('#hours_offred').show();
            if(data.loading_unloading_free_hrs != 0 && data.loading_unloading_free_hrs != null){
              $('#hours_offred').text( '[ <?= $this->lang->line('free_hours'); ?> : ' + data.loading_unloading_free_hrs + ' <?= $this->lang->line('hours_offered'); ?> ]');
            } else { $('#hours_offred').text('[ <?= $this->lang->line('free_hour_not_provided'); ?> ]'); }
          } else {
            $('.dangerous_goods').empty();
            $('.dangerous_goods').attr('disabled', true);
            $('.dangerous_goods').append('<option value=""><?= json_encode($this->lang->line('select_dangerous_goods')); ?>,</option>');
          }
        },"json");        
      } else {
        $('.dangerous_goods').empty(); 
        $('.dangerous_goods').attr('disabled', true);
        $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
      }

      //alert(from_address_id);
      $.post('get-address-relay-lat-long', { addr_id: from_address_id, type: 'address' }, function(res) {        
        // console.log(res);
        if(res != "null"){ 
          var v = $.parseJSON(res);
          $("#from_address_lat_long_hidden").val(v);
          //alert($("#to_address_lat_long_hidden").val());
          //alert($("#to_relay_lat_long_hidden").val());
          if($("#to_address_lat_long_hidden").val() == ',') var to_lat_long = $("#to_relay_lat_long_hidden").val();
          else var to_lat_long = $("#to_address_lat_long_hidden").val();
          if(to_lat_long != '') {
              var url = "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin="+v+"&destination="+to_lat_long;
              $('#map_frame').attr('src', url);
              //get distance and set in summary
              $.post('get-driving-distance', { from_lat_long: v, to_lat_long: to_lat_long }, function(res) { 
                 // console.log(res);
                  $("#lbl_distance_in_km").text($.parseJSON($.trim(res)));
                  // post call for dangrous goods
              });
          }
        }
        else{ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("no_lat_long_found"); ?>'); }
      });
    }
  });

  $("#from_relay").on('change', function(event) { 
    var from_id = $("#from_relay").val();
    if(parseInt(from_id)>0 || from_id!=undefined) {
      $.ajax({
        type: "POST", 
        url: "<?=base_url('user-panel/get-currency-by-country-relay-id')?>", 
        data: { from_id: from_id, address_type: 'relay' },
        dataType: "json",
        success: function(res){ 
          //console.log(res);
          $('.package_value_currency_icon').text(res);
        }
      });
    }

    $("#lbl_departure").text($("#from_relay option:selected").text());
    var from_relay_id = $("#from_relay option:selected").val();
    if(from_relay_id != '' && from_relay_id > 0) {
      var addr = <?= json_encode($relay); ?>;
      var ad_id = $(this).val();
      
      var from_country_id = $("#from_country_id").val();
      var to_country_id = $("#to_country_id").val();

      // $.each(addr, function(i,v){        
      //   if(v['relay_id'] == ad_id ) { 
      //     $("#from_country_id").val(v['country_id']);
      //     from_country_id = v['country_id'];
      //     if(v['pickup_instructions'] != "NULL"){  $("#pickup_instruction").val(v['pickup_instructions']);  }
      //   }
      // });
      
      if( from_country_id > 0  && to_country_id > 0 ) {
        $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id }, function(data) {
          //console.log(data);
          if(data){                
            $('.dangerous_goods').attr('disabled', false);
            $('.dangerous_goods').empty(); 
            $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
            $.each( data, function(i, v) {                  
              $('.dangerous_goods').append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>');                  
            });
          }
          else{
            $('.dangerous_goods').attr('disabled', true);
            $('.dangerous_goods').empty();
            $('.dangerous_goods').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
          }
        },"json");        
      }      
      else {
        $('.dangerous_goods').attr('disabled', true);
        $('.dangerous_goods').empty(); 
        $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
      }

      $.post('get-address-relay-lat-long', { addr_id: from_relay_id, type: 'relay' }, function(res) {        
        // console.log(res);
        if(res != "null"){ 
          var v = $.parseJSON(res);
          $("#from_relay_lat_long_hidden").val(v); 
          //alert($("#to_address_lat_long_hidden").val());
          //alert($("#to_relay_lat_long_hidden").val());
          if($("#to_address_lat_long_hidden").val() == ',') var to_lat_long = $("#to_relay_lat_long_hidden").val();
          else var to_lat_long = $("#to_address_lat_long_hidden").val();
          if(to_lat_long != '') {
            var url = "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin="+v+"&destination="+to_lat_long;
            $('#map_frame').attr('src', url);
            //get distance and set in summary
            $.post('get-driving-distance', { from_lat_long: v, to_lat_long: to_lat_long }, function(res) { 
              // console.log(res);
              $("#lbl_distance_in_km").text($.parseJSON(res));
            });
          }
        }
        else{ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("no_lat_long_found"); ?>'); }
      });
    }
  });

  $("#to_address").on('change', function(event) { 
    $("#lbl_arrival").text($("#to_address option:selected").text());
    var to_address_id = $("#to_address option:selected").val();
    var category_id = $('#category_id').val();
    if(to_address_id != '' && to_address_id > 0) {
      var addr = <?= json_encode($address); ?>;      
      var ad_id = $(this).val();
      var from_country_id = $("#from_country_id").val();
      var to_country_id = $("#to_country_id").val();

      $.each(addr, function(i,v){        
        if(v['addr_id'] == ad_id ) { 
          $("#to_country_id").val(v['country_id']);
          to_country_id = v['country_id'];
          if(v['deliver_instructions'] != "NULL"){  $("#deliver_instruction").val(v['deliver_instructions']);  }
        }
      });

      if( ($.trim(parseInt(from_country_id)) > 0 && $.trim(parseInt(to_country_id)) > 0) || $.trim(parseInt(from_country_id)) > 0 ) { //console.log(from_country_id + to_country_id);
        $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id, category_id: category_id }, function(data) {
          console.log(data);
          if(data['dangerous_goods']){                
            $('.dangerous_goods').attr('disabled', false);
            $('.dangerous_goods').empty(); 
            $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
            $.each( data['dangerous_goods'], function(i, v) {                  
              $('.dangerous_goods').append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>'); 
            });
            $('#hours_offred').show();
            if(data.loading_unloading_free_hrs != 0 && data.loading_unloading_free_hrs != null){
              $('#hours_offred').text( '[ <?= $this->lang->line('free_hours'); ?> : ' + data.loading_unloading_free_hrs + ' <?= $this->lang->line('hours_offered'); ?> ]');
            } else { $('#hours_offred').text('[ <?= $this->lang->line('free_hour_not_provided'); ?> ]'); }
          } else {
            $('.dangerous_goods').empty();
            $('.dangerous_goods').attr('disabled', true);
            $('.dangerous_goods').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
          }
        },"json");        
      } else {
        $('.dangerous_goods').empty(); 
        $('.dangerous_goods').attr('disabled', true);
        $('.dangerous_goods').append('<option value=""><?= 
        $this->lang->line('select_dangerous_goods'); ?></option>');
      }

      $.post('get-address-relay-lat-long', { addr_id: to_address_id, type: 'address' }, function(res) {        
        // console.log(res);
        if(res != "null"){ 
          var v = $.parseJSON(res); 
          $("#to_address_lat_long_hidden").val(v);
          //alert($("#from_address_lat_long_hidden").val());
          //alert($("#from_relay_lat_long_hidden").val());
          if($("#from_address_lat_long_hidden").val() == ',') var from_lat_long = $("#from_relay_lat_long_hidden").val();
          else var from_lat_long = $("#from_address_lat_long_hidden").val();
          if(from_lat_long != '') {
            var url = "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin="+from_lat_long+"&destination="+v;
            $('#map_frame').attr('src', url);
            //get distance and set in summary
            $.post('get-driving-distance', { from_lat_long: from_lat_long, to_lat_long: v }, function(res) { 
              // console.log(res);
                $("#lbl_distance_in_km").text($.parseJSON(res));
            });
          }
        }
        else{ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("no_lat_long_found"); ?>'); }
      });
    }
  });

  $("#to_relay").on('change', function(event) { 
    $("#lbl_arrival").text($("#to_relay option:selected").text());
    var to_relay_id = $("#to_relay option:selected").val();
    if(to_relay_id != '' && to_relay_id > 0) {

      var from_country_id = $("#from_country_id").val();
      var to_country_id = $("#to_country_id").val();
      if( from_country_id > 0  && to_country_id > 0 ) {
        $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id }, function(data) {
          //console.log(data);
          if(data){ 
            $('.dangerous_goods').attr('disabled', false);
            $('.dangerous_goods').empty(); 
            $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
            $.each( data, function(i, v) {                  
              $('.dangerous_goods').append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>');
            });
          }
          else{
            $('.dangerous_goods').attr('disabled', true);
            $('.dangerous_goods').empty();
            $('.dangerous_goods').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
          }
        },"json");        
      }      
      else {
        $('.dangerous_goods').attr('disabled', true);
        $('.dangerous_goods').empty(); 
        $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
      }

      $.post('get-address-relay-lat-long', { addr_id: to_relay_id, type: 'relay' }, function(res) {        
        // console.log(res);
        if(res != "null"){ 
          var v = $.parseJSON(res); 
          $("#to_relay_lat_long_hidden").val(v);
          //alert($("#from_address_lat_long_hidden").val());
          //alert($("#from_relay_lat_long_hidden").val());
          if($("#from_address_lat_long_hidden").val() == ',') var from_lat_long = $("#from_relay_lat_long_hidden").val();
          else var from_lat_long = $("#from_address_lat_long_hidden").val();
          if(from_lat_long != '') {
            var url = "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin="+from_lat_long+"&destination="+v;
            $('#map_frame').attr('src', url);
            //get distance and set in summary
            $.post('get-driving-distance', { from_lat_long: from_lat_long, to_lat_long: v }, function(res) { 
              // console.log(res);
              $("#lbl_distance_in_km").text($.parseJSON(res));
            });
          }
        }
        else{ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("no_lat_long_found"); ?>'); }
      });
    }
  });
</script>

<!-- country state change -->
<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $.trim($(this).val());

    $('#city_id').attr('disabled', true);
    $('#state_id').empty();

    if(country_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-state-by-country-id", 
        data: { country_id: country_id },
        dataType: "json",
        success: function(res){ 
          $('#state_id').attr('disabled', false);
          $('#state_id').empty(); 
          $('#state_id').append('<option value=""><?= json_encode($this->lang->line('select_state')); ?></option>');
          $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
          $('#state_id').focus();
        },
        beforeSend: function(){
          $('#state_id').empty();
          $('#state_id').append('<option value=""><?= json_encode($this->lang->line('loading')); ?></option>');
        },
        error: function(){
          $('#state_id').attr('disabled', true);
          $('#state_id').empty();
          $('#state_id').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?></option>');
        }
      });
    } else { 
      $('#state_id').empty(); 
      $('#state_id').append('<option value=""><?= json_encode($this->lang->line('select_state')); ?></option>');
      $('#state_id, #city_id').attr('disabled', true); 
    }
  });

  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    if(country_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "get-cities-by-state-id", 
        data: { state_id: state_id },
        dataType: "json",
        success: function(res){ 
          $('#city_id').attr('disabled', false);
          $('#city_id').empty(); 
          $('#city_id').append('<option value=""><?= json_encode($this->lang->line('select_city')); ?></option>');
          $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
          $('#city_id').focus();
        },
        beforeSend: function(){
          $('#city_id').empty();
          $('#city_id').append('<option value=""><?= json_encode($this->lang->line('loading')); ?></option>');
        },
        error: function(){
          $('#city_id').attr('disabled', true);
          $('#city_id').empty();
          $('#city_id').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?></option>');
        }
      });
    } else { 
      $('#city_id').empty(); 
      $('#city_id').append('<option value=""><?= json_encode($this->lang->line('select_city')); ?></option>');
      $('#city_id').attr('disabled', true); 
    }
  });

  $(function() {  
    $("#address_type").on('change', function(event) {  event.preventDefault();
      var type = $(this).val();
      if(type=="Commercial") { $("#company").prop('readonly', false);  } else { $("#company").val('').prop('readonly', true); }
    });

    $("#addAddress").keypress(function(event) { return event.keyCode != 13; });

    $("#addAddress").validate({
      ignore: [],
      rules: {
        firstname: { required: true, },      
        lastname: { required: true, },      
        mobile: { required: true, number: true, },      
        zipcode: { required: true, number: true, },      
        street: { required: true, },      
        country_id: { required: true, },      
        state_id: { required: true, },      
        city_id: { required: true, },      
        street: { required: true, },      
        address1: { required: true, },      
        address2: { required: true, },      
        address_image: { accept:"jpg,png,jpeg,gif" },
      }, 
      messages: {
        firstname: { required: <?= json_encode($this->lang->line('enter_first_name')); ?>,   },
        lastname: { required: <?= json_encode($this->lang->line('enter_last_name')); ?>,   },
        mobile: { required: <?= json_encode($this->lang->line('enter_mobile_number')); ?>, number: <?= json_encode($this->lang->line('number_only')); ?>,  },
        zipcode: { required: <?= json_encode($this->lang->line('enter_zipcode')); ?>, number: <?= json_encode($this->lang->line('number_only')); ?>  },
        street: { required: <?= json_encode($this->lang->line('street_name')); ?>,   },
        country_id: { required: <?= json_encode($this->lang->line('select_country')); ?>,   },
        state_id: { required: <?= json_encode($this->lang->line('select_state')); ?>,   },
        city_id: { required: <?= json_encode($this->lang->line('select_city')); ?>,   },
        address1: { required: <?= json_encode($this->lang->line('enter_address')); ?>,   },
        address2: { required: <?= json_encode($this->lang->line('select_address_from_map')); ?>,   },
        address_image: { accept: <?= json_encode($this->lang->line('only_image_allowed')); ?>,   },
      }
    });
    $("#upload_image").on('click', function(e) { e.preventDefault(); $("#address_image").trigger('click'); });
  });

  function addr_image_name(e){ if(e.target.files[0].name !="") { $("#addr_image_name").removeClass('hidden'); }}

  $("form#addAddress").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
      url: "add-address-book-by-panel",
      type: 'POST',
      data: formData,
      success: function (data) {
        var arr = data.split('~');
        resetForm($('form#addAddress'));
        $('#myModal').modal('hide');
        $('#from_address').append($('<option>', {
          value: $.trim(arr[0]),
          text: $.trim(arr[1])
        },'</option>'));
        $('#to_address').append($('<option>', {
          value: $.trim(arr[0]),
          text: $.trim(arr[1])
        },'</option>'));
      },
      cache: false,
      contentType: false,
      processData: false
    }); 
  });

  function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea, email, select2').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
  }
</script>
  
<!-- Add Quick Address Using Google Widget -->
<script>
  $("#frm_address_type").on('change', function(event) {  event.preventDefault();
    var type = $(this).val();
    if(type=="Commercial") { $("#frm_company").prop('readonly', false);  } else { $("#frm_company").val('').prop('readonly', true); }
  });
  $("#t_address_type").on('change', function(event) {  event.preventDefault();
    var type = $(this).val();
    if(type=="Commercial") { $("#t_company").prop('readonly', false);  } else { $("#t_company").val('').prop('readonly', true); }
  });

  function FromAddrShow() {
    document.getElementById('fromMapID').style.display = "none";
    document.getElementById('btnFromAddrShow').style.display = "none";

    $("#fromMapID").val('');
    $("#frm_latitude").val('');
    $("#frm_longitude").val('');
    $("#frm_street").val('');
    $("#frm_address1").val('');
    $("#frm_zipcode").val('');
    $("#frm_country").val('');
    $("#frm_state").val('');
    $("#frm_city").val('');
    $("#frm_address_type").val('');
    $("#frm_company").val('');
    $("#frm_email_id").val('');
    $("#frm_mobile").val('');
    $("#frm_firstname").val('');
    $("#frm_lastname").val('');
    $("#frm_address_image").val('');
    $("#frm_pickup_instruction").val('');
    $("#frm_deliver_instruction").val('');

    document.getElementById('fromAddrDiv').style.display = "none";
    $("#from_address_lat_long_hidden").val('');

    document.getElementById('s2id_from_address').style.display = "block";
    document.getElementById('btnFromGwidgetShow').style.display = "block";
  }

  function FromGwidgetShow() {
    $("#from_address_lat_long_hidden").val('');
    document.getElementById('from_address').selectedIndex = 0;
    document.getElementById('s2id_from_address').style.display = "none";
    document.getElementById('btnFromGwidgetShow').style.display = "none";
    document.getElementById('fromMapID').style.display = "block";
    document.getElementById('btnFromAddrShow').style.display = "block";
    document.getElementById('fromAddrDiv').style.display = "block";
  }

  function ToAddrShow() {
    document.getElementById('toMapID').style.display = "none";
    document.getElementById('btnToAddrShow').style.display = "none";

    $("#toMapID").val('');
    $("#t_latitude").val('');
    $("#t_longitude").val('');
    $("#t_street").val('');
    $("#t_address1").val('');
    $("#t_zipcode").val('');
    $("#t_country").val('');
    $("#t_state").val('');
    $("#t_city").val('');
    $("#t_address_type").val('');
    $("#t_company").val('');
    $("#t_email_id").val('');
    $("#t_mobile").val('');
    $("#t_firstname").val('');
    $("#t_lastname").val('');
    $("#t_address_image").val('');
    $("#t_pickup_instruction").val('');
    $("#t_deliver_instruction").val('');

    document.getElementById('toAddrDiv').style.display = "none";
    $("#to_address_lat_long_hidden").val('');

    document.getElementById('s2id_to_address').style.display = "block";
    document.getElementById('btnToGwidgetShow').style.display = "block";
  }

  function ToGwidgetShow() {
    $("#to_address_lat_long_hidden").val('');
    document.getElementById('to_address').selectedIndex = 0;
    document.getElementById('s2id_to_address').style.display = "none";
    document.getElementById('btnToGwidgetShow').style.display = "none";
    document.getElementById('toMapID').style.display = "block";
    document.getElementById('btnToAddrShow').style.display = "block";
    document.getElementById('toAddrDiv').style.display = "block";
  }

  $("#fromMapID").geocomplete({
    map:".map_canvas",
    location: "",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: true, },
    details: "form",
    detailsAttribute: "from-data-geo", 
    types: ["geocode", "establishment"],
  });  

  $("#toMapID").geocomplete({
    map:".map_canvas",
    location: "",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: true, },
    details: "form",
    detailsAttribute: "to-data-geo", 
    types: ["geocode", "establishment"],
  });  

  // from address save
  $("#btnFromAddressSave").click(function() {
    frm_latitude = $('#frm_latitude').val();
    frm_longitude = $('#frm_longitude').val();
    frm_street = $('#frm_street').val();
    frm_address1 = $('#frm_address1').val();
    frm_zipcode = $('#frm_zipcode').val();
    frm_country = $('#frm_country').val();
    frm_state = $('#frm_state').val();
    frm_city = $('#frm_city').val();
    frm_address_type = $('#frm_address_type').val();
    frm_company = $('#frm_company').val();
    frm_email_id = $('#frm_email_id').val();
    frm_mobile = $('#frm_mobile').val();
    frm_firstname = $('#frm_firstname').val();
    frm_lastname = $('#frm_lastname').val();
    frm_pickup_instruction = $('#frm_pickup_instruction').val();
    frm_deliver_instruction = $('#frm_deliver_instruction').val();

    if(frm_latitude == '') { 
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_latitude_not_found')?>",'warning'); 
    } else if(frm_longitude == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_longitude_not_found')?>",'warning'); 
    } else if(frm_street == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_address_not_found')?>",'warning'); 
    } else if(frm_email_id == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_email_not_found')?>",'warning'); 
    } else if(frm_mobile == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_mobile_not_found')?>",'warning'); 
    } else if(frm_firstname == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_first_name_not_found')?>",'warning'); 
    } else if(frm_lastname == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_last_name_not_found')?>",'warning'); 
    } else if(frm_pickup_instruction == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_pickup_not_found')?>",'warning'); 
    } else if(frm_deliver_instruction == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('from_deliver_not_found')?>",'warning'); 
    } else {
      var formData = new FormData(this);
      formData.append('frm_latitude', $.trim(frm_latitude));
      formData.append('frm_longitude', $.trim(frm_longitude));
      formData.append('frm_street', $.trim(frm_street));
      formData.append('frm_address1', $.trim(frm_address1));
      formData.append('frm_zipcode', $.trim(frm_zipcode));
      formData.append('frm_country', $.trim(frm_country));
      formData.append('frm_state', $.trim(frm_state));
      formData.append('frm_city', $.trim(frm_city));
      formData.append('frm_address_type', $.trim(frm_address_type));
      formData.append('frm_company', $.trim(frm_company));
      formData.append('frm_email_id', $.trim(frm_email_id));
      formData.append('frm_mobile', $.trim(frm_mobile));
      formData.append('frm_firstname', $.trim(frm_firstname));
      formData.append('frm_lastname', $.trim(frm_lastname));
      formData.append('frm_pickup_instruction', $.trim(frm_pickup_instruction));
      formData.append('frm_deliver_instruction', $.trim(frm_deliver_instruction));

      var frominput = document.querySelector('input[id=frm_address_image]'),
      fromfile = frominput.files[0];
      formData.append('frm_address_image', fromfile);

      $.ajax({
        url: "add-address-to-book-from-address",
        type: 'POST',
        data: formData,
        success: function (data) {
          //console.log(data);
          var arr = data.split('~');
          FromAddrShow();
          $('#from_address').append($('<option>', {
            value: $.trim(arr[0]),
            text: $.trim(arr[1])
          },'</option>'));
          
          $('#from_address').val($.trim(arr[0])).trigger('change');
          $("#from_address_lat_long_hidden").val($.trim(arr[2]));
          
          //Update Map
          $("#lbl_departure").text($("#from_address option:selected").text());
          var from_address_id = $("#from_address option:selected").val();
          if(from_address_id != '' && from_address_id > 0) {
            $.post('get-address-relay-lat-long', { addr_id: from_address_id, type: 'address' }, function(res) {        
              if(res != "null"){ 
                var v = $.parseJSON(res);
                //console.log(v);
                $("#from_address_lat_long_hidden").val(v);
                if($("#to_address_lat_long_hidden").val() == ',' || $("#to_address_lat_long_hidden").val() == '') var to_lat_long = $("#to_relay_lat_long_hidden").val();
                else var to_lat_long = $("#to_address_lat_long_hidden").val();
                if(to_lat_long != '' && to_lat_long != ',') {
                  var url = "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin="+v+"&destination="+to_lat_long;
                  $('#map_frame').attr('src', url);
                  //get distance and set in summary
                  $.post('get-driving-distance', { from_lat_long: v, to_lat_long: to_lat_long }, function(res) { 
                    $("#lbl_distance_in_km").text($.parseJSON($.trim(res)));
                  });
                }
              }
              else{ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("no_lat_long_found"); ?>'); }
            });
          }

          $.post('get-address-by-id', { addr_id: from_address_id, address_type: 'address' }, function(res) {        
            console.log($.parseJSON(res)['country_id']);
            $("#from_country_id").val($.parseJSON(res)['country_id']);

            var to_country_id = $("#to_country_id").val();
            var from_country_id = $("#from_country_id").val();
            var category_id = $("#category_id").val();
            if( ($.trim(parseInt(from_country_id)) > 0 && $.trim(parseInt(to_country_id)) > 0) || $.trim(parseInt(from_country_id)) > 0 ) {
              $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id, category_id:category_id }, function(data) { 
                if(data['dangerous_goods']){                
                  $('.dangerous_goods').attr('disabled', false);
                  $('.dangerous_goods').empty(); 
                  $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
                  $.each( data['dangerous_goods'], function(i, v) {                  
                    $('.dangerous_goods').append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>'); 
                  });
                  $('#hours_offred').show();
                  if(data.loading_unloading_free_hrs != 0 && data.loading_unloading_free_hrs != null){
                    $('#hours_offred').text( '[ <?= $this->lang->line('free_hours'); ?> : ' + data.loading_unloading_free_hrs + ' <?= $this->lang->line('hours_offered'); ?> ]');
                  } else { $('#hours_offred').text('[ <?= $this->lang->line('free_hour_not_provided'); ?> ]'); }
                } else {
                  $('.dangerous_goods').empty();
                  $('.dangerous_goods').attr('disabled', true);
                  $('.dangerous_goods').append('<option value=""><?= json_encode($this->lang->line('select_dangerous_goods')); ?>,</option>');
                }
              },"json");        
            } else {
              $('.dangerous_goods').empty(); 
              $('.dangerous_goods').attr('disabled', true);
              $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
            }
          });
        },
        cache: false,
        contentType: false,
        processData: false
      });
      $("#pickup_instruction").val($('#frm_pickup_instruction').val());
      $("#pickup_instruction").text($('#frm_pickup_instruction').val()); 
    }
  });

  // to address save
  $("#btnToAddressSave").click(function() {
    t_latitude = $('#t_latitude').val();
    t_longitude = $('#t_longitude').val();
    t_street = $('#t_street').val();
    t_address1 = $('#t_address1').val();
    t_zipcode = $('#t_zipcode').val();
    t_country = $('#t_country').val();
    t_state = $('#t_state').val();
    t_city = $('#t_city').val();
    t_address_type = $('#t_address_type').val();
    t_company = $('#t_company').val();
    t_email_id = $('#t_email_id').val();
    t_mobile = $('#t_mobile').val();
    t_firstname = $('#t_firstname').val();
    t_lastname = $('#t_lastname').val();
    t_pickup_instruction = $('#t_pickup_instruction').val();
    t_deliver_instruction = $('#t_deliver_instruction').val();

    if(t_latitude == '') { 
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_latitude_not_found')?>",'warning'); 
    } else if(t_longitude == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_longitude_not_found')?>",'warning'); 
    } else if(t_street == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_address_not_found')?>",'warning'); 
    } else if(t_email_id == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_email_not_found')?>",'warning'); 
    } else if(t_mobile == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_mobile_not_found')?>",'warning'); 
    } else if(t_firstname == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_first_name_not_found')?>",'warning'); 
    } else if(t_lastname == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_last_name_not_found')?>",'warning'); 
    } else if(t_pickup_instruction == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_pickup_not_found')?>",'warning'); 
    } else if(t_deliver_instruction == '') {
      swal('<?= $this->lang->line("error"); ?>',"<?=$this->lang->line('to_deliver_not_found')?>",'warning'); 
    } else {
      var formData = new FormData(this);
      formData.append('t_latitude', $('#t_latitude').val());
      formData.append('t_longitude', $('#t_longitude').val());
      formData.append('t_street', $('#t_street').val());
      formData.append('t_address1', $('#t_address1').val());
      formData.append('t_zipcode', $('#t_zipcode').val());
      formData.append('t_country', $('#t_country').val());
      formData.append('t_state', $('#t_state').val());
      formData.append('t_city', $('#t_city').val());
      formData.append('t_address_type', $('#t_address_type').val());
      formData.append('t_company', $('#t_company').val());
      formData.append('t_email_id', $('#t_email_id').val());
      formData.append('t_mobile', $('#t_mobile').val());
      formData.append('t_firstname', $('#t_firstname').val());
      formData.append('t_lastname', $('#t_lastname').val());
      formData.append('t_pickup_instruction', $('#t_pickup_instruction').val());
      formData.append('t_deliver_instruction', $('#t_deliver_instruction').val());

      var tinput = document.querySelector('input[id=t_address_image]'),
      tfile = tinput.files[0];
      formData.append('t_address_image', tfile);

      $.ajax({
        url: "add-address-to-book-to-address",
        type: 'POST',
        data: formData,
        success: function (data) {
          //console.log(data);
          var arr = data.split('~');
          
          ToAddrShow();
          $('#to_address').append($('<option>', {
            value: $.trim(arr[0]),
            text: $.trim(arr[1])
          },'</option>'));
          
          $('#to_address').val($.trim(arr[0])).trigger('change');
          $("#to_address_lat_long_hidden").val($.trim(arr[2]));
          
          //Update Map
          $("#lbl_arrival").text($("#to_address option:selected").text());
          var to_address_id = $("#to_address option:selected").val();
          if(to_address_id != '' && to_address_id > 0) {
            $.post('get-address-relay-lat-long', { addr_id: to_address_id, type: 'address' }, function(res) {        
              if(res != "null"){ 
                var v = $.parseJSON(res); 
                $("#to_address_lat_long_hidden").val(v);
                if($("#from_address_lat_long_hidden").val() == ',' || $("#from_address_lat_long_hidden").val() == '') var from_lat_long = $("#from_relay_lat_long_hidden").val();
                else var from_lat_long = $("#from_address_lat_long_hidden").val();
                if(from_lat_long != '' && from_lat_long != ',') {
                  var url = "https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin="+from_lat_long+"&destination="+v;
                  $('#map_frame').attr('src', url);
                  $.post('get-driving-distance', { from_lat_long: from_lat_long, to_lat_long: v }, function(res) { 
                    $("#lbl_distance_in_km").text($.parseJSON(res));
                  });
                }
              }
              else{ swal('<?= $this->lang->line("error"); ?>', '<?= $this->lang->line("no_lat_long_found"); ?>'); }
            });
          }

          $.post('get-address-by-id', { addr_id: to_address_id, address_type: 'address' }, function(res) { 
            $("#to_country_id").val($.parseJSON(res)['country_id']);

            var to_country_id = $("#to_country_id").val();
            var from_country_id = $("#from_country_id").val();
            var category_id = $("#category_id").val();
            if( ($.trim(parseInt(from_country_id)) > 0 && $.trim(parseInt(to_country_id)) > 0) || $.trim(parseInt(from_country_id)) > 0 ) {
              $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id, category_id:category_id }, function(data) { 
                if(data['dangerous_goods']){                
                  $('.dangerous_goods').attr('disabled', false);
                  $('.dangerous_goods').empty(); 
                  $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
                  $.each( data['dangerous_goods'], function(i, v) {                  
                    $('.dangerous_goods').append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>'); 
                  });
                  $('#hours_offred').show();
                  if(data.loading_unloading_free_hrs != 0 && data.loading_unloading_free_hrs != null){
                    $('#hours_offred').text( '[ <?= $this->lang->line('free_hours'); ?> : ' + data.loading_unloading_free_hrs + ' <?= $this->lang->line('hours_offered'); ?> ]');
                  } else { $('#hours_offred').text('[ <?= $this->lang->line('free_hour_not_provided'); ?> ]'); }
                } else {
                  $('.dangerous_goods').empty();
                  $('.dangerous_goods').attr('disabled', true);
                  $('.dangerous_goods').append('<option value=""><?= json_encode($this->lang->line('select_dangerous_goods')); ?>,</option>');
                }
              },"json");        
            } else {
              $('.dangerous_goods').empty(); 
              $('.dangerous_goods').attr('disabled', true);
              $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
            }
          });
        },
        cache: false,
        contentType: false,
        processData: false
      });
      $("#deliver_instruction").val($('#t_deliver_instruction').val());
      $("#deliver_instruction").text($('#t_deliver_instruction').val());
    }
  });
</script>

<script>
  $("#payment_mode_cod").on('change', function(event) { event.preventDefault();
    //console.log($("#payment_mode_cod").val());
    $("#pickupDiv").removeClass("hidden");
    $("#deliverDiv").removeClass("hidden");
    $("#advance_pay").attr('readonly', true).css('pointer-events','inherit');
    $("#pickup_pay").attr('readonly', true).css('pointer-events','inherit');
    $("#deliver_pay").attr('readonly', true).css('pointer-events','inherit');
    $("#btn_create").attr('disabled', true).css('pointer-events','inherit');
  });

  $("#payment_mode_mobile").on('change', function(event) { event.preventDefault();
    //console.log($("#payment_mode_mobile").val());
    $("#pickupDiv").addClass("hidden");
    $("#deliverDiv").addClass("hidden");
    $("#at_pickup").removeAttr("checked");
    $("#at_deliver").removeAttr("checked");
    $("#advance_pay").attr('readonly', false).css('pointer-events','inherit');
    $("#pickup_pay").attr('readonly', false).css('pointer-events','inherit');
    $("#deliver_pay").attr('readonly', false).css('pointer-events','inherit');
    $("#btn_create").attr('disabled', false).css('pointer-events','inherit');
  });

  $("#payment_mode_bank").on('change', function(event) { event.preventDefault();
    //console.log($("#payment_mode_bank").val());
    $("#pickupDiv").addClass("hidden");
    $("#deliverDiv").addClass("hidden");
    $("#at_pickup").removeAttr("checked");
    $("#at_deliver").removeAttr("checked");
    $("#advance_pay").attr('readonly', true).css('pointer-events','inherit');
    $("#pickup_pay").attr('readonly', true).css('pointer-events','inherit');
    $("#deliver_pay").attr('readonly', true).css('pointer-events','inherit');
    $("#btn_create").attr('disabled', false).css('pointer-events','inherit');
  });

  $("#pickupDiv").on('change', function(event) { event.preventDefault();
    $("#btn_create").attr('disabled', false).css('pointer-events','inherit');
  });
  $("#deliverDiv").on('change', function(event) { event.preventDefault();
    $("#btn_create").attr('disabled', false).css('pointer-events','inherit');
  });
</script>

<script>
  $(window).load(function() {
    var frmaddr = "<?=(isset($_SESSION['from_address']))?$_SESSION['from_address']:''?>";
    var toaddr = "<?=(isset($_SESSION['to_address']))?$_SESSION['to_address']:''?>";
    if(frmaddr != '' && toaddr != '') {
      $('#btnFromGwidgetShow').trigger('click');
      $('#btnToGwidgetShow').trigger('click');
    }

    var category_id = $('#category_id').val();
    var from_country_id = "<?=(isset($_SESSION['from_country_id']))?$_SESSION['from_country_id']:''?>";
    var to_country_id = "<?=(isset($_SESSION['to_country_id']))?$_SESSION['to_country_id']:''?>";

    if( $.trim(parseInt(from_country_id)) > 0 && $.trim(parseInt(to_country_id)) > 0 ) { //console.log(from_country_id + to_country_id);
      $.post('get-dangerous-goods', { from_country_id: from_country_id, to_country_id: to_country_id, category_id: category_id }, function(data) {
        //console.log(data);
        if(data['dangerous_goods']){                
          $('.dangerous_goods').attr('disabled', false);
          $('.dangerous_goods').empty(); 
          $('.dangerous_goods').append('<option value=""><?= $this->lang->line('select_dangerous_goods'); ?></option>');
          $.each( data['dangerous_goods'], function(i, v) {                  
            $('.dangerous_goods').append('<option value="'+v['id']+'">'+v['name'] + ' [ ' + v['code'] + ' ]'+'</option>');                  
          });
            $('#hours_offred').show();
          if(data.loading_unloading_free_hrs != 0 && data.loading_unloading_free_hrs != null){
            $('#hours_offred').text( '[ <?= $this->lang->line('free_hours'); ?> : ' + data.loading_unloading_free_hrs + ' <?= $this->lang->line('hours_offered'); ?> ]');
          } else { $('#hours_offred').text('[ <?= $this->lang->line('free_hour_not_provided'); ?> ]'); }
        } else {
          $('.dangerous_goods').attr('disabled', true);
          $('.dangerous_goods').empty();
          $('.dangerous_goods').append('<option value=""><?= json_encode($this->lang->line('no_option')); ?>,</option>');
        }
      },"json");        
    } else {
      $('.dangerous_goods').attr('disabled', true);
      $('.dangerous_goods').empty(); 
      $('.dangerous_goods').append('<option value=""><?=$this->lang->line('select_dangerous_goods'); ?></option>');
    }
  });

  $("#btn_submit_promo").click(function() {
    var promocode = $('#promo_code').val();
    var total_price = $('#total_price').val();
    var promo_code_applied = "<?=$this->lang->line('promocode applied')?>";
    var order_amount = 0;
    var discount_amount_title = "<?= $this->lang->line("Promo Code Distcount"); ?> : ";
    var advance_pay = $("#advance_pay").val();
    var pickup_pay = $("#pickup_pay").val();
    var tpwp = $("#total_price_with_promocode").val();
    if(promocode && tpwp==0){
      $.ajax({
        type: "POST", 
        url: "<?php echo base_url('user-panel/promocode-with-booking'); ?>", 
        data: {promocode:promocode},
        success: function(res)
        {
          //console.log(res);
          var respp   = res.split('@');
          //console.log(respp[0]);
          if(respp[0] == "success"){
            var distcount_percent = respp[1];

            var discount_amount = (parseFloat(total_price).toFixed(2)/parseFloat(100).toFixed(2))*parseFloat(distcount_percent).toFixed(2);
            discount_amount = parseFloat(discount_amount).toFixed(2);
            // pickup_pay2 = (parseFloat(pickup_pay).toFixed(2)/parseFloat(100).toFixed(2))*parseFloat(distcount_percent).toFixed(2);
            // pickup_pay2 = pickup_pay-pickup_pay2;
            // pickup_pay2 = parseFloat(pickup_pay2).toFixed(2);
            advance_pay2 = (parseFloat(advance_pay).toFixed(2)/parseFloat(100).toFixed(2))*parseFloat(distcount_percent).toFixed(2);
            advance_pay2 = advance_pay-advance_pay2;
            advance_pay2 = parseFloat(advance_pay2).toFixed(2);


            order_amount = total_price-discount_amount;
            order_amount = parseFloat(order_amount).toFixed(2);

            pickup_pay2 = order_amount-advance_pay2;
            pickup_pay2 = parseFloat(pickup_pay2).toFixed(2);
            //advance_pay2 = Math.round(advance_pay2);
            //pickup_pay2 = parseInt(pickup_pay2)-1;
            //new_order_amount = order_amount.split('.');
            //pickup_pay2 = pickup_pay2+"."+new_order_amount[1];
            $('#price_promo_ind').text("("+promo_code_applied+")");
            $('#promo_code_discount').text(discount_amount_title+currency_sign2+" -"+discount_amount);
            $('#price2').text(currency_sign2+" "+order_amount);
            $("#total_price_with_promocode").val(order_amount);
            $("#advance_pay").val(advance_pay2);
            $("#min_adv2").val(advance_pay2);
            $("#min_adv2").text(currency_sign2 + ' ' + advance_pay2 + ' - ' + advance_percent2 + ' %');
            $("#pickup_pay").val(pickup_pay2);
            $("#hidden_promo_code").val(promocode);
          }else{
            swal ( "Oops" ,  respp[1] ,  "error" )
          }    
        }//success
      });//ajax
    }
  });
</script>