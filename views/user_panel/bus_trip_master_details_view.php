<style>
    #map { height: 100%; }
</style>
<style>
    #map { height: 100%; }
    .numbers{ background-color: #3498db;
    color: #fff;
    padding: 1px 6px 1px 6px;
</style>

    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-master-list'; ?>"><?= $this->lang->line('Trip Master List'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('Trip Details'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    <?= $this->lang->line('Trip Details'); ?> &nbsp;&nbsp;&nbsp;
                </h2>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="hpanel filter-item">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="col-md-4 form-group">
                            <div>
                                <h3 class="stat-label"><?= $this->lang->line('order_timeline'); ?></h3>
                            </div>  
                            <div class="form-group text-center" style="border: 1px solid #3498db">
                                <h5 style="color:#3498db" class="text-left"><?= $this->lang->line('Vehicle_image');?></h5>
                                <?php
                                    if($buses[0]['bus_image_url']!="NULL") {
                                        echo '<img class="img-thumbnail" src='.base_url($buses[0]['bus_image_url']).' style="max-width: 300px; max-height: auto;" />';
                                    }  else {
                                        echo '<img class="img-thumbnail" src='.base_url('resources/bus-images/default.png').' style="max-width: 300px; max-height: auto;" />';
                                    }
                                    echo "<br/>";
                                ?>
                                <br/>  
                            </div>
                        </div>
                        <div class="col-md-8 form-group">
                            <div class="row" style="">
                                <div class="col-md-12">
                                    <h3 class="stat-label" style="display: inline-block;"><?= $this->lang->line('Trip Details'); ?></h3>
                                    <div class="pull-right text-right" style="display: inline-block;">
                                        <h5 class="stat-label"><?= $this->lang->line('Trip id'); ?>-<?=$trip_id ?></h5>
                                    </div>
                                </div>
                            </div>
                <!--zaid// Start Trip Details -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">1</span> <?= $this->lang->line('Trip Details'); ?>
                                    </h5>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Start Date'); ?>:&nbsp;</strong><?=$trip_details[0]['trip_start_date']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('End Date');?>:&nbsp;</strong> <?=$trip_details[0]['trip_end_date']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Depart Time');?>:&nbsp;</strong> <?=$trip_details[0]['trip_depart_time']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Trip_Distance');?>:&nbsp;</strong> <?=$trip_distance?><?= $this->lang->line('Km');?>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Trip Availability');?>:&nbsp;</strong> 
                                        <?php
                                            $availability = explode(',', $trip_details[0]['trip_availability']);
                                            if(in_array('mon',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Mon</span>&nbsp;';
                                            }  else  {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Mon</span>&nbsp;';   
                                            }
                                            if(in_array('tue',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Tue</span>&nbsp;';
                                            } else {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Tue</span>&nbsp;';   
                                            }
                                            if(in_array('wed',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Wed</span>&nbsp;';
                                            }  else  {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Wed</span>&nbsp;';   
                                            }
                                            if(in_array('thu',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Thu</span>&nbsp;';
                                            } else {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Thu</span>&nbsp;';   
                                            }
                                            if(in_array('fri',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Fri</span>&nbsp;';
                                            } else {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Fri</span>&nbsp;';   
                                            }   
                                            if(in_array('sat',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Sat</span>&nbsp;';
                                            }  else {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Sat</span>&nbsp;';   
                                            }
                                            if(in_array('sun',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Sun</span>&nbsp;';
                                            } else  {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Sun</span>&nbsp;';   
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                <!--zaid// End trip Details -->
                            <br/>                   
                <!--zaid// Start locations Details -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">2</span> <?= $this->lang->line('Location Details'); ?></h5>
                                    <?php
                                    $this->load->model('api_model_bus', 'api'); 
                                    ?>
                                           <?php for($z=0; $z<sizeof($source_desitnation); $z++)
                                           {
                                            $Pickup_point=""; $drop_point=""; ?>
                                                <h4 class="m-b-xs"><?= $this->lang->line('Source'); ?>:&nbsp;
                                                <?=$source_desitnation[$z]['trip_source']?>
                                                </h4>
                                                <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Pickup Point'); ?>:&nbsp;</strong>
                                                    <?php
                                                        $src_point_ids_array = explode(',',$source_desitnation[$z]['src_point_ids']);
                                                         echo $this->api->get_location_point_address_by_id($src_point_ids_array[0]);
                                                         
                                                          if(sizeof($src_point_ids_array)>1)
                                                            {
                                                                echo '<button style="color: #3498db" type="button" class="btn btn-link" data-toggle="modal" data-target="#'.$source_desitnation[$z]['sdd_id'].'Pickup">'.$this->lang->line('...More').'</button>';
                                                            }   
                                                            $n=1;  
                                                           for($i=0; $i<sizeof($src_point_ids_array); $i++)
                                                           { 

                                                               $Pickup_point .= "<br/>".$n." : ".$this->api->get_location_point_address_by_id($src_point_ids_array[$i]);
                                                               $n++;
                                                           }
                                                            
                                                    ?>
                                                </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <h4 class="m-b-xs"><?= $this->lang->line('Destination');?>:&nbsp;<?=$source_desitnation[$z]['trip_destination']?>
                                                    </h4>
                                                <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Drop Point'); ?>:&nbsp;</strong>
                                                    <?php
                                                        $dest_point_ids_array = explode(',',$source_desitnation[$z]['dest_point_ids']);
                                                        echo $this->api->get_location_point_address_by_id($dest_point_ids_array[0]);
                                                        if(sizeof($dest_point_ids_array)>1)
                                                            {
                                                                echo '<button style="color: #3498db" type="button" class="btn btn-link" data-toggle="modal" data-target="#'.$source_desitnation[$z]['sdd_id'].'drop">'.$this->lang->line('...More').'</button>';
                                                            }
                                                         $s=1;
                                                        for($i=0; $i<sizeof($dest_point_ids_array); $i++)
                                                        { 

                                                           $drop_point .= "<br/>".$s." : ".$this->api->get_location_point_address_by_id($dest_point_ids_array[$i]);
                                                           $s++;

                                                        }
                                                    ?>
                                                </div>
                                                <br/>

                                                <div class="modal fade" id="<?=$source_desitnation[$z]['sdd_id']?>Pickup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                              <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title"><img src="<?=base_url('resources/images/dashboard-logo.jpg')?>" class="center-block"></h4>
                                                    <h5 class="modal-title" id="exampleModalLongTitle"><?=$this->lang->line('Pickup Points')?></h5>
                                                  </div>
                                                  <div class="modal-body">
                                                    <h4 class="m-b-xs"><?= $this->lang->line('Source'); ?>:&nbsp;
                                                        <?=$source_desitnation[$z]['trip_source']?>
                                                    </h4>
                                                    <strong><?= $this->lang->line('Pickup Points');?>:&nbsp;</strong>
                                                    <?=$Pickup_point?>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div> 

                                             <div class="modal fade" id="<?=$source_desitnation[$z]['sdd_id']?>drop" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                              <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title"><img src="<?=base_url('resources/images/dashboard-logo.jpg')?>" class="center-block "></h4>
                                                    <h5 class="modal-title" id="exampleModalLongTitle"><?=$this->lang->line('Drop Points')?></h5>
                                                  </div>
                                                  <div class="modal-body">
                                                    <h4 class="m-b-xs"><?=$this->lang->line('Destination');?>:&nbsp;<?=$source_desitnation[$z]['trip_destination']?>
                                                    </h4>
                                                    <strong><?= $this->lang->line('Drop Points'); ?>:&nbsp;</strong>
                                                    <?=$drop_point?>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('close');?></button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div> 
                                           <?php } ?>
                                </div>
                            </div>
                <!--zaid// End locations Details -->
                            <br />
                <!--zaid// Start Operator Details -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">3</span> <?= $this->lang->line('operator_Details'); ?>
                                    </h5>
                                    <div class="col-md-4">
                                        <?php
                                            if($operator_details['avatar_url']!="NULL"){echo '<img class="img-thumbnail" src='.base_url($operator_details["avatar_url"]).' style="max-width: 200px; max-height: auto;"/>';}else{echo '<img class="img-thumbnail" src='.base_url("resources/profile-images/default.jpg").' style="max-width: 200px; max-height: auto;" />';}?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('f_name'); ?>:&nbsp;</strong><?=$operator_details['firstname']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('l_name'); ?>:&nbsp;</strong><?=$operator_details['lastname']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('lbl_email'); ?>:&nbsp;</strong><?=$operator_details['email_id']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('contact_number'); ?>:&nbsp;</strong><?=$operator_details['contact_no']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('company_name'); ?>:&nbsp;</strong><?=$operator_details['company_name']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('address'); ?>:&nbsp;</strong><?=$operator_details['address']?>
                                    </div>
                                </div>
                            </div>
                <!--zaid// End Operator Details -->
                            <br/>
                <!--zaid// Start Vehicle Details -->
                            <div class="row" style="border: 1px solid #3498db">
                              <div class="col-md-12" >
                                <h5 style="color: #3498db"><span class="numbers">4</span> <?= $this->lang->line('Vehicle Details'); ?></h5>     
                              </div>
                                <div class="col-md-12" style="margin-bottom: 8px;">
                                  <h4 class="m-b-xs"><?= $this->lang->line('Vehicle Details'); ?>:&nbsp;
                                  </h4>
                                  <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Make_Year'); ?>:&nbsp;</strong><?=$buses[0]['bus_make']?>
                                  </div>
                                  <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_modal');?>:&nbsp;</strong> <?=$buses[0]['bus_modal']?>
                                  </div>
                                  <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_No'); ?>:&nbsp;</strong><?=$buses[0]['bus_no']?>
                                  </div>
                                  <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Seat_Type');?>:&nbsp;</strong><?=$buses[0]['bus_ac']?>&nbsp;\&nbsp;<?=$buses[0]['bus_seat_type']?>
                                  </div>
                                  <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Amenities');?>:&nbsp;</strong>
                                    <?php
                                      $amenities = explode(',',$buses[0]['bus_amenities']);
                                      foreach($amenities as $amen)
                                      {
                                        echo $amen."&nbsp;";
                                      }
                                    ?>
                                  </div>
                                </div>
                            </div>
                <!--zaid// End Vehicle Details -->
                            <br/>
                <!--zaid// Start ticket rates Details -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">5</span> <?= $this->lang->line('Ticket Rates');?></h5>
                                    <br/>
                                    <div class="row form-group">
                                        <?php foreach ($seat_type_price as $price): ?>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <strong><label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Price'); ?></label></strong>
                                                :&nbsp;<?=$price['seat_type_price']?>&nbsp;<?=$trip_details[0]['currency_sign']?>
                                            </div>
                                            <div class="col-md-6">
                                                <strong><label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Special Price'); ?></label></strong>
                                                :&nbsp;<?=($price['special_price']=='NULL')?'0':$price['special_price']?>&nbsp;<?=$trip_details[0]['currency_sign']?>
                                            </div>
                                        </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
                <!--zaid// End ticket Details -->
                            <br/>
                <!--zaid// Start Google map Location Direction -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">6</span> <?= $this->lang->line('direction_on_map'); ?>
                                    </h5>
                                    <div class="col-md-12" style="margin-bottom: 8px;">
                                        <iframe
                                            width="100%"
                                            height="450"
                                            frameborder="0" style="border:0"
                                            src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin=<?=$trip_source[0]['lat_long']?>&destination=<?=$trip_destination[0]['lat_long']?>">
                                        </iframe>
                                    </div>
                                </div>
                            </div>
                <!--zaid// End Google map Location Direction -->
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <h3><?= $this->lang->line('Trip Bookings'); ?></h3>
                    </div>
                </div>
                <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th><?= $this->lang->line('B. ID'); ?></th>
                            <th><?= $this->lang->line('Customer Name'); ?></th>
                            <th><?= $this->lang->line('Customer Conatct'); ?></th>
                            <th><?= $this->lang->line('Customer Email'); ?></th>
                            <th><?= $this->lang->line('Booking Date'); ?></th>
                            <th><?= $this->lang->line('Journey Date'); ?></th>
                            <th><?= $this->lang->line('Payment Status');?></th>
                            <!--<th><?= $this->lang->line('action'); ?></th>-->
                        </tr>
                    </thead>
                    <tbody class="panel-body">
                        <?php foreach ($booking_details as $bdetail) { static $i = 0; $i++; ?>
                            <tr>
                                <td><?=$bdetail['ticket_id']?></td>
                                <td><?=$bdetail['cust_name']?></td>
                                <td><?=$bdetail['cust_contact']?></td>
                                <td><?=$bdetail['cust_email']?></td>
                                <td><?=$bdetail['bookig_date']?></td>
                                <td><?=$bdetail['journey_date']?></td>
                                <td><?=($bdetail['complete_paid']==0)?$this->lang->line('Completed'):$this->lang->line('Pending')?></td>
                                <!--<td style="display: flex;">
                                    <form action="<?=base_url('user-panel-bus/bus-trip-special-rate-edit')?>" method="post" style="display: inline-block;">
                                        <input type="hidden" name="trip_id" value="<?= $bdetail['trip_id'] ?>">
                                        <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-wrench"></i> <span class="bold"><?= $this->lang->line('Setup'); ?></span></button>
                                    </form>
                                </td>-->
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-6">
                            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-master-list'; ?>" class="btn btn-info btn-outline " ><?= $this->lang->line('back'); ?></a>
                        </div>
                        
                    </div>
                </div>
        </div>
    </div>
        




