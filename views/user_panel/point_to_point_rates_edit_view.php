 <style>
   input:focus {
    background-color: #ffb606;
}
 </style>
  <div class="normalheader small-header">
    <div class="hpanel">
      <div class="panel-body">
        <a class="small-header-action" href="">
          <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
          </div>
        </a>

        <div id="hbreadcrumb" class="pull-right">
          <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel/ptop-rates-list'; ?>"><?= $this->lang->line('Point to Point Rates'); ?></a></li>
              <li class="active"><span><?= $this->lang->line('Edit'); ?></span></li>
          </ol>
        </div>
        <h2 class="font-light m-b-xs">  <i class="fa fa-cogs fa-2x text-muted"></i> <?= $this->lang->line('Point to Point Rates'); ?> </h2>
        <small class="m-t-md"><?= $this->lang->line('Edit Point to Point Rates Details'); ?></small>    
      </div>
    </div>
  </div>
     
  <div class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="hpanel hblue">
          <div class="panel-body">
              
            <?php if($error = $this->session->flashdata('error')): ?>
              <div class="alert alert-danger text-center"><?= $error; ?></div>
            <?php endif; ?>
            <?php if($success = $this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $success; ?></div>
            <?php endif; ?>

            <form role="form" action="<?= base_url('user-panel/update-ptop-rates'); ?>" class="form-horizontal form-groups-bordered" id="point_to_point_rates_edit" method="post">
              <style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
              <input type="hidden" name="rate_id" value="<?= $rates['rate_id'];?>" />
              
              <div class="border">
                <div class="row">
                  <div class="col-md-5">
                    <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('category'); ?> : <span class="text-info"><?=$rates['cat_name']?></span> </label>                              
                    <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('From Country'); ?> : <span class="text-info"><?=$rates['from_country_name']?></span> </label> 
                    <label class="control-label col-md-12" style="text-align: left;">
                      <?php 
                        if($rates['from_state_id'] > 0 ) { $state = $this->api->get_state_detail_by_id($rates['from_state_id']); $state_name = $state['state_name']; } 
                        else { $state_name = "NA"; }
                      ?>
                      <?= $this->lang->line('From State'); ?> : <span class="text-info"><?= $state_name; ?></span> 
                    </label>
                    <label class="control-label col-md-12" style="text-align: left;">
                      <?php 
                        if($rates['from_city_id'] > 0 ) { $city = $this->api->get_city_detail_by_id($rates['from_city_id']); $city_name = $city['city_name']; } 
                        else{ $city_name = "NA"; }
                      ?>
                      <?= $this->lang->line('From City'); ?> : <span class="text-info"><?= $city_name; ?> </span>
                    </label>
                      <?php 
                        if($rates['is_formula_weight_rate'] == 1) { 
                          $unit = $this->api->get_unit_detail($rates['unit_id']); ?>
                          <label class="control-label col-md-12" style="text-align: left;">
                          <?= $this->lang->line('weight_unit'); ?> : <span class="text-info"><?=$unit['shortname']?></span>
                      <?php } ?>
                    </label>
                    <div class="col-md-12"> <hr/> </div> 
                    <?php 
                      $unit = $this->api->get_unit_detail($rates['unit_id']); $unit_name = $unit['unit_type'].' ( '. $unit['shortname']. ' )';
                      if($rates['min_dimension_id'] > 0 ): $min = $this->api->get_dimension_detail_by_id($rates['min_dimension_id']); $min_dimension = $min['dimension_type']; ?>

                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Min. Dimension'); ?> : <span class="text-info"><?=$min_dimension; ?></span> </label>
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Min. Width'); ?> : <span class="text-info"><?= $rates['min_width'] . 'cm';?></span> </label>
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Min. Height'); ?> : <span class="text-info"><?=$rates['min_height'] . 'cm';?></span> </label>
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Min. Length'); ?> : <span class="text-info"><?=$rates['min_length'] . 'cm';?></span> </label>
                    <?php elseif($rates['min_weight'] > 0 ):  ?>
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Min. Weight'); ?> : <span class="text-info"><?=$rates['min_weight'] . ' '. $unit_name;?></span> </label>
                    <?php endif; ?>
                  </div>
                  
                  <div class="col-md-1">&nbsp;</div>
                  
                  <div class="col-md-5">
                    <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Country Currency'); ?> : <span class="text-info"><?= $rates['currency_title'] . ' ( '.$rates['currency_sign'] . ' )'?></span> </label>                              
                    <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('To Country'); ?> : <span class="text-info"><?=$rates['to_country_name']?> </span> </label>                              
                    <label class="control-label col-md-12" style="text-align: left;">
                      <?php 
                        if($rates['to_state_id'] > 0 ) { $state = $this->api->get_state_detail_by_id($rates['to_state_id']); $state_name = $state['state_name']; } 
                        else{ $state_name = "NA"; }
                      ?>
                      <?= $this->lang->line('To State'); ?> : <span class="text-info"><?= $state_name; ?></span> 
                    </label>
                    <label class="control-label col-md-12" style="text-align: left;">
                      <?php 
                        if($rates['to_city_id'] > 0 ) { $city = $this->api->get_city_detail_by_id($rates['to_city_id']); $city_name = $city['city_name']; } 
                        else{ $city_name = "NA"; }
                      ?>
                      <?= $this->lang->line('To City'); ?> : <span class="text-info"> <?= $city_name; ?> </span>
                    </label>
                    <div class="col-md-12"> <hr/> </div> 
                    <?php if($rates['max_dimension_id'] > 0 ): $max = $this->api->get_dimension_detail_by_id($rates['max_dimension_id']); $max_dimension = $max['dimension_type']; ?>
                      
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Max. Dimension'); ?> : <span class="text-info"><?=$max_dimension;?></span> </label>
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Max. Width'); ?> : <span class="text-info"><?= $rates['max_width'] . 'cm';?></span> </label>
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Max. Height'); ?> : <span class="text-info"><?=$rates['max_height'] . 'cm';?></span> </label>
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Max. Length'); ?> : <span class="text-info"><?=$rates['max_length'] . 'cm';?></span> </label>                

                    <?php elseif( $rates['max_weight'] > 0 ):  ?>                  
                      <label class="control-label col-md-12" style="text-align: left;"><?= $this->lang->line('Max. Weight'); ?> : <span class="text-info"><?=$rates['max_weight'] .' '. $unit_name;?></span> </label>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              
              <div class="border">
                <div class="row">
                  <div class="col-md-4"><hr/></div>
                  <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Earth Rates &amp; Durations'); ?></h4></div>
                  <div class="col-md-4"><hr/></div>
                </div>
            
                <div class="row">           
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('Local Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>
                          <input type="number" class="form-control" id="earth_local_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="earth_local_rate" min="0" value="<?= $rates['earth_local_rate']?>" readonly/> 
                        </div>
                      </div>
                      <div class="col-md-6">              
                        <div class="input-group">
                          <input type="number" class="form-control" id="earth_local_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="earth_local_duration" min="1" value="<?= $rates['earth_local_duration']?>" readonly/> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('National Rate &amp; Duration'); ?></label> 
                    <div class="row">    
                      <div class="col-md-6">                
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>
                          <input type="number" class="form-control" id="earth_national_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="earth_national_rate" min="0" value="<?= $rates['earth_national_rate']?>"  readonly/> 
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="earth_national_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="earth_national_duration" min="1" value="<?= $rates['earth_national_duration']?>"  readonly/> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('International Rate &amp; Duration'); ?></label> 
                    <div class="row">    
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>
                          <input type="number" class="form-control" id="earth_international_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="earth_international_rate" min="0" value="<?= $rates['earth_international_rate']?>"  readonly/>  
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="earth_international_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="earth_international_duration" min="1" value="<?= $rates['earth_international_duration']?>" readonly />  
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />        
              </div>
              
              <div class="border">
                <div class="row">
                  <div class="col-md-4"><hr/></div>
                  <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Air Rates &amp; Durations'); ?></h4></div>
                  <div class="col-md-4"><hr/></div>
                </div>
                <div class="row">
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('Local Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>
                          <input type="number" class="form-control" id="air_local_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="air_local_rate" min="0" value="<?= $rates['air_local_rate']?>" readonly /> 
                        </div>
                      </div>                
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="air_local_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="air_local_duration" min="1" value="<?= $rates['air_local_duration']?>" readonly /> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('National Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>  
                          <input type="number" class="form-control" id="air_national_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="air_national_rate" min="0" value="<?= $rates['air_national_rate']?>"  readonly /> 
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="air_national_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="air_national_duration" min="1" value="<?= $rates['air_national_duration']?>"  readonly /> 
                          <span class="input-group-addon">hrs</span>
                        </div>                
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('International Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>  
                          <input type="number" class="form-control" id="air_international_rate" placeholder="<?= $this->lang->line('Rate'); ?>" name="air_international_rate" min="0" value="<?= $rates['air_international_rate']?>" readonly />  
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="air_international_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="air_international_duration" min="1" value="<?= $rates['air_international_duration']?>" readonly />  
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="border">
                <div class="row">
                  <div class="col-md-4"><hr/></div>
                  <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Sea Rates &amp; Durations'); ?></h4></div>
                  <div class="col-md-4"><hr/></div>
                </div>      
                <div class="row">
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('Local Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>              
                          <input type="number" class="form-control" id="sea_local_rate" placeholder="<?= $this->lang->line('Rate'); ?>" min="0" name="sea_local_rate" value="<?= $rates['sea_local_rate']?>" readonly/> 
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="sea_local_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="sea_local_duration" min="1" value="<?= $rates['sea_local_duration']?>" readonly/> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>                
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('National Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>
                          <input type="number" class="form-control" id="sea_national_rate" placeholder="<?= $this->lang->line('Rate'); ?>" min="0" name="sea_national_rate" value="<?= $rates['sea_national_rate']?>"  readonly/> 
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="sea_national_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="sea_national_duration" min="1" value="<?= $rates['sea_national_duration']?>" readonly /> 
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">            
                    <label for="" class="control-label col-md-offset-2"><?= $this->lang->line('International Rate &amp; Duration'); ?></label> 
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <span class="input-group-addon currency fa"><?= $rates['currency_sign']; ?></span>
                          <input type="number" class="form-control" id="sea_international_rate" min="0" placeholder="<?= $this->lang->line('Rate'); ?>" name="sea_international_rate" value="<?= $rates['sea_international_rate']?>" readonly />  
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <input type="number" class="form-control" id="sea_international_duration" placeholder="<?= $this->lang->line('Duration'); ?>" name="sea_international_duration" min="1" value="<?= $rates['sea_international_duration']?>" readonly />  
                          <span class="input-group-addon">hrs</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                  <a href="<?=base_url('user-panel/ptop-rates-list')?>" class="btn btn-warning btn-outline"><i class="fa fa-arrow-left"></i> &nbsp; <?= $this->lang->line('back'); ?></a>            
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 text-right">
                    <a id="btn_edit" class="btn btn-success btn-outline"><i class="fa fa-pencil"></i> &nbsp; <?= $this->lang->line('edit'); ?></a>
                    <button id="btn_submit" type="submit" class="btn btn-success btn-outline hidden"><i class="fa fa-plus"></i> &nbsp; <?= $this->lang->line('save_details'); ?></button>
                </div>
              </div>
              
            </form>
              
          </div>
        </div>
      </div>
    </div>
  </div>

<script>
  var inputChanged = false; 
  $("#point_to_point_rates_edit input").change(function(){
    inputChanged = true;
  });

  $("#btn_edit").on('click', function(){
    $(this).addClass('hidden');
    $("input").prop('readonly',false);
    $("#btn_submit").removeClass('hidden');
  });

  $("#btn_submit").click(function(e){ e.preventDefault();
    $(".alert").removeClass('alert-danger alert-success').addClass('hidden').html("");        

    var earth_local_rate = parseFloat($("#earth_local_rate").val()).toFixed(2);
    var earth_national_rate = parseFloat($("#earth_national_rate").val()).toFixed(2);
    var earth_international_rate = parseFloat($("#earth_international_rate").val()).toFixed(2);       
    var air_local_rate = parseFloat($("#air_local_rate").val()).toFixed(2);
    var air_national_rate = parseFloat($("#air_national_rate").val()).toFixed(2);
    var air_international_rate = parseFloat($("#air_international_rate").val()).toFixed(2);       
    var sea_local_rate = parseFloat($("#sea_local_rate").val()).toFixed(2);
    var sea_national_rate = parseFloat($("#sea_national_rate").val()).toFixed(2);
    var sea_international_rate = parseFloat($("#sea_international_rate").val()).toFixed(2);

    var earth_local_duration = parseFloat($("#earth_local_duration").val()).toFixed(2);
    var earth_national_duration = parseFloat($("#earth_national_duration").val()).toFixed(2);
    var earth_international_duration = parseFloat($("#earth_international_duration").val()).toFixed(2);       
    var air_local_duration = parseFloat($("#air_local_duration").val()).toFixed(2);
    var air_national_duration = parseFloat($("#air_national_duration").val()).toFixed(2);
    var air_international_duration = parseFloat($("#air_international_duration").val()).toFixed(2);       
    var sea_local_duration = parseFloat($("#sea_local_duration").val()).toFixed(2);
    var sea_national_duration = parseFloat($("#sea_national_duration").val()).toFixed(2);
    var sea_international_duration = parseFloat($("#sea_international_duration").val()).toFixed(2);

    if(inputChanged){
      // Earth
      if(isNaN(earth_local_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth Local Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_local_rate').focus(); }, 0);  }); }
      else if(isNaN(earth_local_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth Local Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_local_duration').focus(); }, 0); }); }
      else if(parseFloat(earth_local_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth Local Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_local_duration').focus(); }, 0);  }); }    

      else if(isNaN(earth_national_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth National Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_national_rate').focus(); }, 0); }); }
      else if(isNaN(earth_national_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth National Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_national_duration').focus(); }, 0); }); }
      else if(parseFloat(earth_national_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth National Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_national_duration').focus(); }, 0); }); }

      else if(isNaN(earth_international_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth International Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_international_rate').focus(); }, 0); }); }
      else if(isNaN(earth_international_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth International Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_international_duration').focus(); }, 0); }); }
      else if(parseFloat(earth_international_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Earth International Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('earth_international_duration').focus(); }, 0); }); }
      // Air
      else if(isNaN(air_local_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air Local Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_local_rate').focus(); }, 0); }); }
      else if(isNaN(air_local_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air Local Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_local_duration').focus(); }, 0); }); }
      else if(parseFloat(air_local_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air Local Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_local_duration').focus(); }, 0); }); }    

      else if(isNaN(air_national_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air National Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_national_rate').focus(); }, 0); }); }
      else if(isNaN(air_national_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air National Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_national_duration').focus(); }, 0); }); }
      else if(parseFloat(air_national_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air National Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_national_duration').focus(); }, 0); }); }

      else if(isNaN(air_international_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air International Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_international_rate').focus(); }, 0); }); }
      else if(isNaN(air_international_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air International Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_international_duration').focus(); }, 0); }); }
      else if(parseFloat(air_international_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Air International Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('air_international_duration').focus(); }, 0); }); }
      // Sea
      else if(isNaN(sea_local_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea Local Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_local_rate').focus(); }, 0); }); }
      else if(isNaN(sea_local_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea Local Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_local_duration').focus(); }, 0); }); }
      else if(parseFloat(sea_local_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea Local Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_local_duration').focus(); }, 0); }); }    

      else if(isNaN(sea_national_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea National Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_national_rate').focus(); }, 0); }); }
      else if(isNaN(sea_national_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea National Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_national_duration').focus(); }, 0); }); }
      else if(parseFloat(sea_national_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea National Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_national_duration').focus(); }, 0); }); }

      else if(isNaN(sea_international_rate)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea International Rate'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_international_rate').focus(); }, 0); }); }
      else if(isNaN(sea_international_duration)) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea International Duration'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_international_duration').focus(); }, 0); $("#sea_international_duration").focus();  }); }
      else if(parseFloat(sea_international_duration) < 1) { swal({title:"<?= $this->lang->line('error'); ?>",text:"<?= $this->lang->line('Please Enter Sea International Duration greater than 1hr.'); ?>",type:'warning'},function(){ window.setTimeout(function () { document.getElementById('sea_international_duration').focus(); }, 0); }); }

      else { $("#point_to_point_rates_edit")[0].submit();  }
    } else{ swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('no_change'); ?>",'error'); }
  });

</script>