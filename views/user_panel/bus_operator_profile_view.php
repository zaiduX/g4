    <style>     .avatar { width: 22.3%; max-height: 157px;  }   .cover {        <?php if($operator['cover_url'] == "NULL"): ?>         background-image: url(<?= $this->config->item('resource_url'). 'bg.jpg'; ?>) !important;        <?php else: ?>          background-image: url(<?= base_url($operator['cover_url']); ?>) !important;        <?php endif; ?>     background-position: center !important;    background-repeat: no-repeat !important;    background-size: cover !important;    height: 200px;  }</style>

    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
                <li class="active"><span><?= $this->lang->line('operator_profile'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-user fa-2x text-muted"></i> <?= $this->lang->line('operator_profile'); ?> </h2>
          <small class="m-t-md"><?= $this->lang->line('manage_your_operator_profile_here'); ?></small>    
        </div>
      </div>
    </div>

    <div class="content content-boxed">
        <div class="row">
            <div class="col-lg-12" >
                <div class="hpanel hgreen" >
                    <div class="panel-body cover">
                        <?php if($operator['avatar_url'] == "NULL"): ?>
                            <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg'; ?>" class="img-thumbnail m-b avatar" alt="avatar" />
                        <?php else: ?>
                            <img src="<?= base_url($operator['avatar_url']); ?>" class="img-thumbnail m-b avatar" alt="avatar" />
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel hblue">
                    <div class="panel-body">
                        <?php if($this->session->flashdata('error')):  ?>
                        <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                        <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('first_name'); ?></strong></p>
                            </div>
                            <div class="col-md-6">    
                                <p>
                                    <?= $operator['firstname'] != "NULL" ? $operator['firstname'] : $this->lang->line('not_provided') ?>
                                </p>
                            </div>  
                            <div class="col-md-2 text-right">
                              <a href="<?= $this->config->item('base_url') . 'user-panel-bus/edit-operator-profile'; ?>" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('last_name'); ?></strong></p>
                            </div>
                            <div class="col-md-8">    
                                <p>
                                    <?= $operator['lastname'] != "NULL" ? $operator['lastname'] : $this->lang->line('not_provided') ?>
                                </p>
                            </div>  
                        </div>  
                        <div class="row">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('company_name'); ?></strong></p>
                            </div>
                            <div class="col-md-8">    
                                <p>
                                    <?= $operator['company_name'] != "NULL" ? $operator['company_name'] : $this->lang->line('not_provided') ?>
                                </p>
                            </div>  
                        </div> 
                        <div class="row">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('email_address'); ?></strong></p>
                            </div>
                            <div class="col-md-6">    
                                <p>
                                    <?= $operator['email_id'] != "NULL" ? $operator['email_id'] : $this->lang->line('not_provided') ?>
                                </p>
                            </div>  
                        </div>
                        <div class="row">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('mobile_number'); ?></strong></p>
                            </div>
                            <div class="col-md-8">    
                                <p>
                                    <?= $operator['contact_no'] != "NULL" ? $operator['contact_no'] : $this->lang->line('not_provided') ?>
                                </p>
                            </div>  
                        </div>
                        <div class="collapse in" id="education">
                            <div class="row">
                                <div class="col-md-4">    
                                    <p><strong><?= $this->lang->line('city_state_country'); ?></strong></p>
                                </div>
                                <div class="col-md-8">    
                                    <p>
                                        <?= $operator['city_id'] != "NULL" ? $this->api->get_city_name_by_id($operator['city_id']) : $this->lang->line('not_provided') ?> / 
                                        <?= $operator['state_id'] != "NULL" ? $this->api->get_state_name_by_id($operator['state_id']) : $this->lang->line('not_provided') ?> / 
                                        <?= $operator['country_id'] != "NULL" ? $this->api->get_country_name_by_id($operator['country_id']) : $this->lang->line('not_provided') ?>
                                    </p>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-md-4">    
                                    <p><strong><?= $this->lang->line('address'); ?></strong></p>
                                </div>
                                <div class="col-md-8">    
                                    <p>
                                        <?= $operator['address'] != "NULL" ? $operator['address'] : $this->lang->line('not_provided') ?>
                                    </p>
                                </div>  
                            </div> 
                            <div class="row">
                                <div class="col-md-4">    
                                    <p><strong><?= $this->lang->line('zip_code'); ?></strong></p>
                                </div>
                                <div class="col-md-8">    
                                    <p>
                                        <?= $operator['zipcode'] != "NULL" ? $operator['zipcode'] : $this->lang->line('not_provided') ?>
                                    </p>
                                </div>  
                            </div> 
                            <div class="row">
                                <div class="col-md-4">    
                                    <p><strong><?= $this->lang->line('promotion_code'); ?></strong></p>
                                </div>
                                <div class="col-md-8">    
                                    <p>
                                        <?= $operator['promocode'] != "NULL" ? $operator['promocode'] : $this->lang->line('not_provided') ?>
                                    </p>
                                </div>  
                            </div> 
                            <div class="row">
                                <div class="col-md-4">    
                                    <p><strong><?= $this->lang->line('introduction'); ?></strong></p>
                                </div>
                                <div class="col-md-8">    
                                    <p>
                                        <?= $operator['introduction'] != "NULL" ? $operator['introduction'] : $this->lang->line('not_provided') ?>
                                    </p>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-md-4">    
                                    <p><strong><?= $this->lang->line('payment_informations'); ?></strong></p>
                                </div>
                                <div class="col-md-8">    
                                    <p>
                                        <?= $operator['shipping_mode'] == 0 ? 'Manual' : 'Automatic' ?>
                                    </p>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hpanel hred hidden">
                    <div class="panel-heading">
                        <h4><strong><?= $this->lang->line('payment_informations'); ?></strong></h4>
                    </div>
                    <div class="panel-body">
                        <div class="row hidden">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('shipping_accepting_mode'); ?></strong></p>
                            </div>
                            <div class="col-md-6">    
                                <p>
                                    <?= $operator['shipping_mode'] != "0" ? 'Manual' : 'Automatic' ?>
                                </p>
                            </div>  
                            <div class="col-md-2 text-right">
                                <form action="edit-deliverer-bank" method="post" style="display: inline-block;">
                                    <input type="hidden" name="cust_id" value="<?= $operator['cust_id'] ?>">
                                    <button type="submit" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('iban'); ?></strong></p>
                            </div>
                            <div class="col-md-6">    
                                <p>
                                    <?= ($operator['iban'] != "NULL") ? $operator['iban'] : $this->lang->line('not_provided') ?>
                                </p>
                            </div>  
                            <div class="col-md-2 text-right">
                                <form action="edit-operator-bank" method="post" style="display: inline-block;">
                                    <input type="hidden" name="cust_id" value="<?= $operator['cust_id'] ?>">
                                    <button type="submit" class="btn btn-default btn-circle"><i class="fa fa-pencil"></i></button>
                                </form>
                            </div>  
                        </div>
                        <div class="row">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('1_Mobile_Money_Account'); ?></strong></p>
                            </div>
                            <div class="col-md-8">    
                                <p>
                                    <?= ($operator['mobile_money_acc1'] != "NULL") ? $operator['mobile_money_acc1'] : $this->lang->line('not_provided') ?>
                                </p>
                            </div>  
                        </div> 
                        <div class="row">
                            <div class="col-md-4">    
                                <p><strong><?= $this->lang->line('2_Mobile_Money_Account'); ?></strong></p>
                            </div>
                            <div class="col-md-8">    
                                <p>
                                    <?= ($operator['mobile_money_acc2'] != "NULL") ? $operator['mobile_money_acc2'] : $this->lang->line('not_provided') ?>
                                </p>
                            </div>  
                        </div>    
                    </div>
                </div>
                <div class="hpanel hblue">
                    <div class="panel-heading">
                        <h4><strong><?= $this->lang->line('Operator Documents'); ?></strong></h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-11">
                            <?php for($i=0; $i < sizeof($documents); $i++) { ?>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body file-body">
                                            <i class="fa fa-file-pdf-o text-info"></i>
                                        </div>
                                        <div class="panel-footer text-center">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5><a href="<?= $this->config->item('base_url') . $documents[$i]['attachement_url']; ?>"><?= strtoupper($documents[$i]['doc_type']) ?></a></h5>
                                                    <button title="Delete Document" style="display: inline-block;" class="btn btn-danger btn-sm docdelete" id="<?= $documents[$i]['doc_id'] ?>"><i class="fa fa-trash"></i></button>
                                                </div> 
                                                <div class="col-md-12 hidden">
                                                    <form action="<?= base_url('user-panel/update-deliverer-document'); ?>" method="post" enctype="multipart/form-data" id="uploadDoc">
                                                        <input type="hidden" name="doc_id" value="<?= $documents[$i]['doc_id'] ?>"> 
                                                        <label><?= $this->lang->line('change_document'); ?></label>
                                                        <input type="file" name="attachement" id="attachement"><br />
                                                        <button class="btn btn-default" type="submit"><?= $this->lang->line('upload'); ?></button>
                                                        <span id="errorMsg" class="text-danger hidden"><strong>Error!</strong> <?= $this->lang->line('please_select_document'); ?></span>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if(sizeof($documents) < 3) { ?>
                        <div class="col-md-1 text-right">
                            <a title="Add Document" href="<?= $this->config->item('base_url') . 'user-panel-bus/edit-operator-document'; ?>" class="btn btn-default btn-circle"><i class="fa fa-plus"></i></a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.docdelete').click(function () {
            var id = this.id;
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post('operator-document-delete', {id: id}, function(res)
                        {
                            swal("Deleted!", "Document has been deleted.", "success");
                            setTimeout(function() { window.location.reload(); }, 2000);
                           
                        });
                    } else {
                        swal("Cancelled", "Document is safe :)", "error");
                    }
                });
            });
    </script>