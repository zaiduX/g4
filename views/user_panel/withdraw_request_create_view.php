</style>
    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel/my-wallet'; ?>"><?= $this->lang->line('my_wallet'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('withdraw_request'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   <i class="fa fa-money fa-2x text-muted"></i> <?= $this->lang->line('withdraw_request'); ?>
                </h2>
                <small class="m-t-md"><?= $this->lang->line('send_request_to_gonagoo_to_withdraw_e_wallet_amount'); ?>.</small>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <?php
                        //var_dump($final_balance); echo '<br />';
                        //var_dump($accounts); echo '<br />';
                        //var_dump($currency_code); echo '<br />';
                        //var_dump($amount['amount']);
                        ?>
                        <form method="post" class="form-horizontal" action="withdraw-request-sent" id="withdrawRequest">
                            <input type="hidden" name="currency_code" value="<?= $currency_code ?>">
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <?php if($this->session->flashdata('error')):  ?>
                                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                                    <?php endif; ?>
                                    <?php if($this->session->flashdata('success')):  ?>
                                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-group"><label class="col-md-3 text-right"><?= $this->lang->line('available_balance'); ?></label>
                                        <div class="col-md-8">
                                            <label><?= $currency_code . ' ' . $final_balance ?></label>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3 control-label"><?= $this->lang->line('enter_withdraw_amount'); ?></label>
                                        <div class="col-md-8">
                                            <input type="text" placeholder="Amount" class="form-control m-b" name="amount" id="amount" value="<?= $final_balance ?>">
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3 control-label"><?= $this->lang->line('account_type'); ?></label>
                                        <div class="col-md-8">
                                            <?php //echo json_encode($accounts); ?>
                                            <select name="transfer_account_type" id="transfer_account_type" class="form-control">
                                                <?php foreach ($accounts as $account): ?>
                                                    <?php if($account['payment_method']=='Mobile Wallet') { ?>
                                                        <option value="<?=$account['pay_id']?>"><?=$account['brand_name'].' ['.$account['card_number'].']';?></option>
                                                    <?php } ?>
                                                    <?php if($account['payment_method']=='Credit Card' || $account['payment_method']=='Debit Card') { ?>
                                                        <option value="<?=$account['pay_id']?>"><?=$account['brand_name'].' ['.$account['card_number'].'] ['.$account['expirty_date'].']';?></option>
                                                    <?php } ?>
                                                    <?php if($account['payment_method']=='Bank Account') { ?>
                                                        <option value="<?=$account['pay_id']?>"><?=$account['card_number'].' ['.$account['bank_name'].'] ['.$account['bank_short_code'].']';?></option>
                                                    <?php } ?>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-md-3 control-label"><?= $this->lang->line('enter_gonagoo_account_password'); ?></label>
                                        <div class="col-md-4">
                                            <input type="password" placeholder="Password" class="form-control m-b" name="password" id="password" />
                                        </div>
                                        <div class="col-md-4">
                                            <input type="password" placeholder="Re-type Password" class="form-control m-b" name="confirmpassword" id="confirmpassword" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-5 col-md-offset-6 text-right">
                                            <button class="btn btn-info" type="submit"><?= $this->lang->line('send_request'); ?></button>
                                            <a href="<?= $this->config->item('base_url') . 'user-panel/my-wallet'; ?>" class="btn btn-primary" type="submit"><?= $this->lang->line('cancel'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $("#withdrawRequest")
            .validate({
                rules: {
                    amount: { required : true, number : true, min : 1, max : <?= $final_balance ?> },
                    password: { required: true, },
                    confirmpassword: { equalTo: "#password", },
                },
                messages: {
                    amount: { required : "Please enter withdrawal amount!", number : "Only numbers!", min : "Minumum amount should be greater than 1!", max : "Maximum amount should not exceeds your account balance!", },
                    password: { required : "Please enter gonagoo account password!", },
                },
            });
    </script>