
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>

      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><span>Profile</span></li>
          <li><span>Details</span></li>
          <li class="active"><span>Add Skill</span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">  <i class="fa fa-photo fa-2x text-muted"></i> Skill </h2>
      <small class="m-t-md">Add Skill Details</small>    
    </div>
  </div>
</div>
s
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">
        <form action="<?= base_url('user-panel/add-skill'); ?>" method="post" class="form-horizontal" id="addskillForm">    
        
          <div class="panel-body">              
            <div class="col-lg-10 col-lg-offset-1">

              <?php if($this->session->flashdata('error')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="row">
                  <div class="form-group"> 
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                  </div>
                </div>
              <?php endif; ?>
            
              <div class="row">
                <div class="form-group">
                  <div class="col-lg-6">
                    <label class="">Category :</label>
                    <select id="category_id" name="category_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select category">
                      <option value="">Select category</option>
                      <?php foreach ($category as $category): ?>
                        <option value="<?= $category['cat_id'] ?>"> <?= $category['cat_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>

                  <div class="col-lg-6">
                    <label class="">Skill Level :</label>
                    <select id="skill_level" name="skill_level" class="form-control select2" data-allow-clear="true" data-placeholder="Select skill level">                      
                      <option value="">Select Skill level</option>                      
                      <option value="junior">Junior</option>                      
                      <option value="confirm">Confirm</option>                      
                      <option value="senior">Senior</option>                      
                      <option value="expert">Expert</option>                      
                    </select>
                  </div>
                </div>
              </div>
		            
	            <div class="row">
              	<div class="form-group">  
		              <div class="col-lg-12">
		                <label class="">Skills :</label>
		                <select id="skills" name="skills[]" class="form-control select2" data-allow-clear="true" data-placeholder="Select skills" disabled multiple>
		                </select>
		              </div>
		            </div>
		          </div>

               
            </div>

          </div>        
          <div class="panel-footer"> 
            <div class="row">
               <div class="col-lg-6 text-left">
                  <a href="<?= base_url('user-panel/user-profile'); ?>" class="btn btn-primary">Go to Profile</a>                            
               </div>
               <div class="col-lg-6 text-right">
                  <button type="submit" class="btn btn-info" data-style="zoom-in">Submit Detail</button>               
               </div>
             </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
</div>



<script>

$("#category_id").on('change', function(event) {  event.preventDefault();
  var category_id = $(this).val();
  if(category_id != "" ) { 
    $.ajax({
      type: "POST", 
      url: "get-skills-by-category-id", 
      data: { category_id: category_id },
      dataType: "json",
      success: function(res){ 
      	var junior_skills = <?= json_encode($junior_skills);?>;
      	var confirmed_skills = <?= json_encode($confirmed_skills);?>;
      	var senior_skills = <?= json_encode($senior_skills);?>;
      	var expert_skills = <?= json_encode($expert_skills);?>;

        $('#skills').attr('disabled', false);
        $('#skills').empty(); 
        $.each(res,function(key, value){
        	// console.log(value['skill_id'] + '----');
        	if( ($.inArray(value['skill_id'], junior_skills) == -1 ) && ($.inArray(value['skill_id'], confirmed_skills) == -1 ) && ($.inArray(value['skill_id'], senior_skills) == -1 ) && ($.inArray(value['skill_id'], expert_skills) == -1 )) {
        		// console.log(($.inArray(value['skill_id'], junior_skills)));
        		$('#skills').append('<option value="'+$(this).attr('skill_id')+'">'+$(this).attr('skill_name')+'</option>');
        	}

        	
        	
        });
        $('#skills').focus();
      },
      beforeSend: function(){
        $('#skills').empty();
        $('#skills').append('<option value="">Loading...</option>');
      },
      error: function(){
        $('#skills').attr('disabled', true);
        $('#skills').empty();
        $('#skills').append('<option value="">No Options</option>');
      }
    });
  } else { 
    $('#skills').empty(); 
    $('#skills').attr('disabled', true); 
  }

});
 
 $(function(){
    $("#addskillForm").validate({
      ignore: [],
      rules: {
        skill_level: { required: true, },
        category_id: { required: true, },
        "skills[]": { required: true, },
      }, 
      messages: {
        skill_level: { required: "Select skill level.",   },
        category_id: { required: "Select category.",  },
        "skills[]": { required: "Select some skills.",  },
      }
    });
  });
</script>