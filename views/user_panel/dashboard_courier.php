<div class="content" style="padding-top: 30px;">

  <?php if($cust['email_verified'] == 0): ?>
    <div class="row">
      <div class="form-group"> 
        <div class="alert alert-warning alert-dismissible email-verify-alert" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?= $this->lang->line('warning'); ?></strong> <?= $this->lang->line('email_verification_warning'); ?> &nbsp; <a href="<?= base_url('resend-email-verification'); ?>"> <strong><?= $this->lang->line('resend_verification_email'); ?></strong></a>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <div class="row">
    <div class="col-xl-1 col-lg-1 col-md-0 col-sm-0 text-center">&nbsp;</div>
    <div class="col-xl-8 col-lg-8 col-md-9 col-sm-9 text-center">
      <span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important; font-size: 2em;"><strong><?= $this->lang->line('Courier | Transport | Home Move'); ?> </strong></span>
    </div>
    <div class="col-xl-1 col-lg-1 col-md-3 col-sm-3 text-right">
      <div class="btn-group">
        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" style="background-color:#1b5497; color: #fff; border: 1px solid #fff; border-radius: 20px;"> 
            <i class="fa fa-plus"></i> <?= $this->lang->line('create_booking'); ?> <span class="caret"></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-right">
          <li class="text-center">
            <a name="create_link" data-cat_id ="6" data-redirect="transport/add-bookings"> <?= $this->lang->line('transportation'); ?> </a>
          </li>
          <li class="divider"></li>
          <li class="text-center">
            <a name="create_link" data-cat_id ="7" data-redirect="user-panel/add-bookings"> <?= $this->lang->line('courier'); ?> </a>
          </li>
          <li class="divider"></li>
          <li class="text-center">
            <!--<a name="create_link" data-cat_id ="280" data-redirect="home-move/add-booking"> <?= $this->lang->line('home_move'); ?> </a> -->
            <a name="create_link" data-cat_id ="6" data-redirect="transport/add-bookings"> <?= $this->lang->line('home_move'); ?> </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row" style="margin-bottom: 25px;">
    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-0 text-center">&nbsp;</div>
    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">
      <h4 style="color: white"><strong><?= $this->lang->line('tile14'); ?></strong></h4>
    </div>
    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-0 text-right">
      &nbsp;
    </div>
  </div>

  <br />

  <script>
    $("a[name='create_link']").on('click',function(e){ e.preventDefault();
      var redirect = $(this).data('redirect');
      var cat_id = $(this).data('cat_id');
      var newForm = $('<form>', {
        'action': <?= json_encode(base_url());?> + redirect,
        'method': 'post'
      }).append($('<input>', {
        'name': 'category_id',
        'value': cat_id,
        'type': 'hidden'
      }));
      $(document.body).append(newForm);
      newForm.submit();
    }); 
  </script>