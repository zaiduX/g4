<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    padding-left: 0px; padding-right: 0px;
  }
  .dataTables_filter {
   display: none;
  }
</style>
<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('favourite_deliverers'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs text-left" style="line-height: 25px;"> <i class="fa fa-heart-o"></i> <?= $this->lang->line('favourite_deliverers'); ?>  &nbsp;&nbsp;&nbsp;
            <a href="<?= base_url('user-panel-laundry/search-for-deliverers'); ?>" class="btn btn-outline btn-info" ><i class="fa fa-search"></i> <?= $this->lang->line('Search Deliverers'); ?> </a></h2>
        </div>
    </div>
</div>

<div class="content">
  <div class="row">
    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 text-right">
      <h4><i class="fa fa-search"></i></h4>
    </div>
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10">
      <input type="text" id="ordersearchbox" class="form-control" placeholder="<?= $this->lang->line('search'); ?>">
    </div>
  </div>
  <script>
    $("#ordersearchbox").keyup(function() {
      $('.contact-name').hide();
      var txt = $("#ordersearchbox").val();
      $('.contact-name:contains("'+txt+'")').show();
    }); 
  </script>
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12"> 
      <?php if(!empty($deliverers) AND is_array($deliverers)): ?>
        <?php $i=0;  foreach ($deliverers as $d):?> 
          <?php  
            if($d['avatar_url'] != "NULL"){ $avatar_url= base_url($d['avatar_url']);  }
            else { $avatar_url = $this->config->item('resource_url') . 'default-profile.jpg'; }
            
            $country_name = $this->user->get_country_name_by_id($d['country_id']); 
            $state_name = $this->user->get_state_name_by_id($d['state_id']);
            $city_name = $this->user->get_city_name_by_id($d['city_id']);
          ?>
          <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 contact-name">
            <div class="hpanel <?=($i%2==0)?'hgreen':'hyellow';?> contact-panel">
              <div class="panel-body" style="min-height:300px; max-height:300px;">
                <span class="pull-right">
                  <button title="<?= $this->lang->line('Remove from favorite'); ?>" class="btn btn-danger btn-circle remove_favorite" data-id="<?= $d['cust_id'];?>"><i class="fa fa-heart"></i></button>&nbsp;
                  <form action="<?= base_url('user-panel-laundry/search-deliverer-details-view');?>" method="post" class="pull-right">
                    <input type="hidden" name="deliverer_id" value="<?= $d['cust_id'];?>" />
                    <button title="<?= $this->lang->line('Profile View'); ?>" class="btn btn-info btn-circle"><i class="fa fa-eye"></i></button>
                  </form>
                </span>
                <img alt="logo" class="img-thumbnail m-b" src="<?= $avatar_url; ?>" />
                <h3><a><?= strtoupper($d['firstname']. ' '.$d['lastname']); ?></a></h3>
                <div class="text-muted font-bold m-b-xs"><?= $city_name . ', '. $country_name; ?></div>
                <div class="text-muted font-bold m-b-xs"></div>
                <p>
                  <?php if($d['company_name'] !="NULL"): ?>
                    Company:  <?= strtoupper($d['company_name']); ?> <br />
                  <?php else: ?>
                    Company: Not Defined <br />
                  <?php endif; ?>
                    Address : <?= $d['address']; ?> <br />
                  <?= $city_name.', '. $state_name .', '.$country_name; ?>
                </p><hr style="margin-top: 5px !important; margin-bottom: 5px !important;"/>
                <p>
                  <?= $this->lang->line('rating');?>: 
                  <?php for($i=0; $i<(int)$d['ratings']; $i++){ echo ' <i class="fa fa-star"></i> '; } ?>
                  <?php for($i=0; $i<(5-(int) $d['ratings']); $i++){ echo ' <i class="fa fa-star-o"></i> '; } ?>
                  ( <?= (int) $d['ratings'] ?> / 5 )
                </p>                          
              </div>                      
            </div>
          </div>
        <?php $i++; endforeach; ?>
      <?php else: ?>
       <div class="hpanel hgreen contact-panel">
          <div class="panel-body"> 
            <h5 class="text-center"><?= $this->lang->line('Search deliverers and add to your favorite list.'); ?></h5>
          </div>  
        </div>  
      <?php endif; ?>
    </div>
          
  </div>
</div>

<script>
  $(".remove_favorite").on('click', function(event) {   event.preventDefault();
      var id = $(this).data('id');
      swal({                
        title: <?= json_encode($this->lang->line('are_you_sure'))?>,                
        text: "",
        type: "warning",                
        showCancelButton: true,                
        confirmButtonColor: "#DD6B55",                
        confirmButtonText: <?= json_encode($this->lang->line('yes'))?>,                
        cancelButtonText: <?= json_encode($this->lang->line('no'))?>,                
        closeOnConfirm: false,                
        closeOnCancel: false, 
      },                
      function (isConfirm) {                    
        if (isConfirm) {                        
          $.post("<?=base_url('user-panel-laundry/remove-deliverer-from-favourite')?>", {id: id}, 
          function(res){
              //console.log(res);
            if($.trim(res) == "success") {
              swal(<?= json_encode($this->lang->line('success'))?>, "", "success");                            
              setTimeout(function() { window.location.reload(); }, 2000); 
            } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, <?=json_encode($this->lang->line('Failed to remove deliverer! Try again...'))?>, "error");  }  
          }); 
        } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, <?= json_encode($this->lang->line('Remove Deliverer cancelled'))?>, "error");  }  
      });       
  });    
</script>