  <div class="normalheader small-header">
    <div class="hpanel">
      <div class="panel-body">
        <a class="small-header-action" href="">
          <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
          </div>
        </a>

        <div id="hbreadcrumb" class="pull-right">
          <ol class="hbreadcrumb breadcrumb">
            <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
            <li><a href="<?= $this->config->item('base_url') . 'user-panel/standard-rates-list'; ?>"><?= $this->lang->line('Standard Rates'); ?></a></li>
            <li class="active"><span><?= $this->lang->line('Weight Based'); ?></span></li>
          </ol>
        </div>
        <h2 class="font-light m-b-xs">  <i class="fa fa-cogs fa-2x text-muted"></i> <?= $this->lang->line('Weight Based Standard Rates'); ?> </h2>
        <small class="m-t-md"><?= $this->lang->line('Add Standard Rates Details'); ?></small>    
      </div>
    </div>
  </div>
  
  <div class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="hpanel hblue">
          <div class="panel-body">
              
            <?php $error = $data = array(); if($error = $this->session->flashdata('error')): $data = $error['data']; ?>
              <div class="alert alert-danger text-center"><?= $error['error_msg']; ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

            <form role="form" action="<?= base_url('user-panel/register-weight-based'); ?>" class="form-horizontal form-groups-bordered" id="standard_rate_add" method="post">
              <div class="row">
                <label for="category" class="col-sm-2 control-label"><?= $this->lang->line('select_category'); ?></label>              
                <div class="col-sm-10">
                  <select id="category_id" name="category[]" class="form-control select2" multiple placeholder="<?= $this->lang->line('select_category'); ?>">
                    <?php foreach ($categories as $c ): ?>
                      <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
                        <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                  <span style="color:red;" id="error_category_id"></span>
                </div>
              </div>
              <div class="clear"></div><br />

              <div class="row">
                <div class="col-md-6">
                  <label for="rate_type" class="col-sm-4 control-label"><?= $this->lang->line('select_country'); ?></label>           
                  <div class="col-sm-8">              
                    <select id="country_id" name="country_id" class="form-control select2">
                      <option value="0"><?= $this->lang->line('select_country'); ?></option>
                      <?php foreach ($countries_list as $country): ?>                     
                        <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                    <span style="color:red;" id="error_country_id"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <label class="col-sm-4 control-label"><?= $this->lang->line('Country Currency'); ?></label>
                  <div class="col-sm-8">               
                    <input type="hidden" name="currency_id" id="currency_id" value="0" />
                    <input type="text" name="currency_name" id="currency_name" class="form-control" disabled placeholder="<?= $this->lang->line('Country Currency'); ?>" />       
                    <span style="color:red;" id="error_currency_id"></span>              
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />
              
              <div class="row">
                <div class="col-md-6">
                  <label for="min_distance" class="col-sm-4 control-label"><?= $this->lang->line('Min. Distance'); ?></label>           
                  <div class="col-sm-8">              
                    <div class="input-group">
                      <input type="number" class="form-control" id="min_distance" placeholder="<?= $this->lang->line('Min. Distance'); ?>" name="min_distance" min="0" <?php if(!empty($data)){ echo 'value="'.$data['min_distance'].'"'; } ?> /> 
                      <span class="input-group-addon"> km </span>
                    </div>
                    <span style="color:red;" id="error_min_distance"></span>             
                  </div>
                </div>
                <div class="col-md-6">
                  <label for="max_distance" class="col-sm-4 control-label"><?= $this->lang->line('Max. Distance'); ?></label>
                  <div class="col-sm-8">  
                    <div class="input-group">            
                      <input type="number" class="form-control" id="max_distance" placeholder="<?= $this->lang->line('Max. Distance'); ?>" name="max_distance" min="0" <?php if(!empty($data)){ echo 'value="'.$data['max_distance'].'"'; } ?>  />  
                      <span class="input-group-addon"> km </span>
                    </div>
                    <span style="color:red;" id="error_max_distance"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />

              <div class="row">
                <div class="col-md-4"><hr/></div>
                <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Min. / Max. Weight &amp; Unit'); ?></h4></div>
                <div class="col-md-4"><hr/></div>
              </div>
          
              <div class="row">
                <div class="col-md-4">
                  <label for="rate_type" class="col-sm-5 control-label"><?= $this->lang->line('Min. Weight'); ?></label>            
                  <div class="col-sm-7">              
                    <input type="number" class="form-control" id="min_weight" placeholder="<?= $this->lang->line('Min. Weight'); ?>" min="0" name="min_weight" <?php if(!empty($data)){ echo 'value="'.$data['min_weight'].'"'; } ?> />
                    <span style="color:red;" id="error_min_weight"></span>   
                  </div>
                </div>
                <div class="col-md-4">
                  <label for="rate_type" class="col-sm-5 control-label"><?= $this->lang->line('Max. Weight'); ?></label>            
                  <div class="col-sm-7">              
                    <input type="number" class="form-control" id="max_weight" placeholder="<?= $this->lang->line('Max. Weight'); ?>" min="0" name="max_weight" <?php if(!empty($data)){ echo 'value="'.$data['max_weight'].'"'; } ?> />  
                    <span style="color:red;" id="error_max_weight"></span>   
                  </div>
                </div>
                <div class="col-md-4">
                  <label for="rate_type" class="col-sm-3 control-label"><?= $this->lang->line('Unit'); ?></label>           
                  <div class="col-sm-9">
                    <select id="unit_id" name="unit_id" class="form-control select2">
                      <option value="0"><?= $this->lang->line('Select Unit'); ?></option>
                      <?php foreach ($unit_list as $unit): ?>                     
                        <option value="<?= $unit['unit_id'] ?>" <?= (!empty($data) && $data['unit_id']==$unit['unit_id'])?"selected":"";?>><?= $unit['shortname']; ?></option>
                      <?php endforeach ?>
                    </select>
                    <span style="color:red;" id="error_unit_id"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />

              <div class="row">
                <div class="col-md-4"><hr/></div>
                <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Earth Rates &amp; Durations'); ?></h4></div>
                <div class="col-md-4"><hr/></div>
              </div>
              
              <div class="row">
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="earth_local_rate" class="control-label"><?= $this->lang->line('Local Rate'); ?></label>           
                  <div class="">              
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="earth_local_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="earth_local_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_local_rate'].'"'; } ?> /> 
                    </div>
                    <span style="color:red;" id="error_earth_local_rate"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="earth_national_rate" class="control-label"><?= $this->lang->line('National Rate'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="earth_national_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="earth_national_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_national_rate'].'"'; } ?>  /> 
                    </div>
                    <span style="color:red;" id="error_earth_national_rate"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label class="control-label"><?= $this->lang->line('International Rate'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="earth_international_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="earth_international_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_international_rate'].'"'; } ?>  />  
                    </div>
                    <span style="color:red;" id="error_earth_international_rate"></span>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="earth_local_duration" class="control-label"><?= $this->lang->line('Local Duration'); ?> </label>           
                  <div class="">              
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_local_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="earth_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_local_duration'].'"'; } ?> /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_earth_local_duration"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="earth_national_duration" class="control-label"><?= $this->lang->line('National Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_national_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="earth_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_national_duration'].'"'; } ?>  /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_earth_national_duration"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="earth_international_duration" class="control-label"><?= $this->lang->line('International Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_international_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="earth_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_international_duration'].'"'; } ?>  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_earth_international_duration"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />

              <div class="row">
                <div class="col-md-4"><hr/></div>
                <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Air Rates &amp; Durations'); ?></h4></div>
                <div class="col-md-4"><hr/></div>
              </div>

              <div class="row">
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="air_local_rate" class="control-label"><?= $this->lang->line('Local Rate'); ?> </label>           
                  <div class="">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="air_local_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="air_local_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_local_rate'].'"'; } ?> /> 
                    </div>
                    <span style="color:red;" id="error_air_local_rate"></span>             
                  </div>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="air_national_rate" class="control-label"><?= $this->lang->line('National Rate'); ?> </label>
                  <div class="">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>  
                      <input type="number" class="form-control" id="air_national_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="air_national_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_national_rate'].'"'; } ?>  /> 
                    </div>
                    <span style="color:red;" id="error_air_national_rate"></span>
                  </div>
                </div>
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="air_international_rate" class="control-label"><?= $this->lang->line('International Rate'); ?> </label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>  
                      <input type="number" class="form-control" id="air_international_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="air_international_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_international_rate'].'"'; } ?>  />  
                    </div>
                    <span style="color:red;" id="error_air_international_rate"></span>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="air_local_duration" class="control-label"><?= $this->lang->line('Local Duration'); ?> </label>           
                  <div class="">              
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_local_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="air_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_local_duration'].'"'; } ?> /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_air_local_duration"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="air_national_duration" class="control-label"><?= $this->lang->line('National Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_national_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="air_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_national_duration'].'"'; } ?>  /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_air_national_duration"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="air_international_duration" class="control-label"><?= $this->lang->line('International Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_international_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="air_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_international_duration'].'"'; } ?>  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_air_international_duration"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><br />

              <div class="row">
                <div class="col-md-4"><hr/></div>
                <div class="col-md-4 text-center"><h4 style="color: #3498db;"><?= $this->lang->line('Sea Rates &amp; Durations'); ?></h4></div>
                <div class="col-md-4"><hr/></div>
              </div>

              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_local_rate" class="control-label"><?= $this->lang->line('Local Rate'); ?> </label>           
                  <div class="">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>              
                      <input type="number" class="form-control" id="sea_local_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" min="0" name="sea_local_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_local_rate'].'"'; } ?> /> 
                    </div>
                    <span style="color:red;" id="error_sea_local_rate"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_national_rate" class="control-label"><?= $this->lang->line('National Rate'); ?> </label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="sea_national_rate" placeholder="<?= $this->lang->line('Enter Rate'); ?>" min="0" name="sea_national_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_national_rate'].'"'; } ?>  /> 
                    </div>
                    <span style="color:red;" id="error_sea_national_rate"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_international_rate" class="control-label"><?= $this->lang->line('International Rate'); ?> </label>
                  <div class="">  
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="sea_international_rate" min="0" placeholder="<?= $this->lang->line('Enter Rate'); ?>" name="sea_international_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_international_rate'].'"'; } ?>  />  
                    </div>
                    <span style="color:red;" id="error_sea_international_rate"></span>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-1">&nbsp;</div>
                <div class="col-md-3">
                  <label for="sea_local_duration" class="control-label"><?= $this->lang->line('Local Duration'); ?> </label>           
                  <div class="">              
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_local_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="sea_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_local_duration'].'"'; } ?> /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_sea_local_duration"></span>             
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_national_duration" class="<?= $this->lang->line('National Duration'); ?>"></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_national_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="sea_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_national_duration'].'"'; } ?>  /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_sea_national_duration"></span>
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                  <label for="sea_international_duration" class="control-label"><?= $this->lang->line('International Duration'); ?></label>
                  <div class="">  
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_international_duration" placeholder="<?= $this->lang->line('Enter Duration'); ?>" name="sea_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_international_duration'].'"'; } ?>  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                    <span style="color:red;" id="error_sea_international_duration"></span>
                  </div>
                </div>
              </div>
              <div class="clear"></div><hr/>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                  <a href="<?=base_url('user-panel/standard-rates-list')?>" class="btn btn-warning btn-outline"><i class="fa fa-arrow-left"></i> &nbsp; <?= $this->lang->line('back'); ?></a>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                  <div class="text-right">
                    <button id="btn_submit" type="submit" class="btn btn-success btn-outline"><i class="fa fa-plus"></i> <?= $this->lang->line('Add Rate'); ?></button>
                  </div>
                </div>
              </div>
            </form> 
          </div>
        </div>
      </div>
    </div>
  </div>

<script>
  $("input").not("input[type=submit]").on('change', function(event) { event.preventDefault();
    var input = $(this);
    $("span[id^='error_']").removeClass('error').text("");
  });

  $("#country_id").on('change', function(event) { event.preventDefault();
    $(".currency").addClass('hidden').html('');
    var country_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "<?=base_url('user-panel/get-country-currencies')?>", 
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        if(res.length > 0 ){
          $("#currency_id").val(res[0]['currency_id']);
          $("#currency_name").val(res[0]['currency_title'] + '( ' + res[0]['currency_sign'] + ' )');
          $(".currency").removeClass('hidden').html('<strong>'+res[0]['currency_sign']+'</strong>'); 
          $('#currency_name').focus();
        }
        else{
          $('#currency_id').val('0');
          $('#currency_name').val("<?= $this->lang->line('No currency found!'); ?>");
          $(".currency").removeClass('hidden').html('?'); 
        }
      },
      beforeSend: function(){
        $('#currency_id').empty();
        $('#currency_id').val("<?= $this->lang->line('loading'); ?>");
      },
      error: function(){
        $('#currency_id').val('0');
        $('#currency_name').val("<?= $this->lang->line('No currency found!'); ?>");
        $(".currency").removeClass('hidden').html('?');
      }
    });
  });

  $("#currency_id").on('change', function(event) {  event.preventDefault();
    var currency = $("#currency_id option:selected").text();
    var sign = currency.match(/\((.*)\)/);
    if(sign != null){ $(".currency").removeClass('hidden').html('<strong>'+sign[1]+'</strong>'); }
    else { $(".currency").addClass('hidden').html(''); }
  });
  
  $("#btn_submit").click(function(e){ 
    e.preventDefault();
    $(".alert").removeClass('alert-danger alert-success').addClass('hidden').html("");        
    var categories = $("#category_id").val();
    var country_id = $("#country_id").val();
    var currency_id = $("#currency_id").val();
    var min_weight = parseFloat($("#min_weight").val()).toFixed(2);
    var max_weight = parseFloat($("#max_weight").val()).toFixed(2);
    var unit_id = $("#unit_id").val();
    var min_distance = parseFloat($("#min_distance").val()).toFixed(2);
    var max_distance = parseFloat($("#max_distance").val()).toFixed(2);
    
    var earth_local_rate = parseFloat($("#earth_local_rate").val()).toFixed(2);
    var earth_national_rate = parseFloat($("#earth_national_rate").val()).toFixed(2);
    var earth_international_rate = parseFloat($("#earth_international_rate").val()).toFixed(2);       
    var air_local_rate = parseFloat($("#air_local_rate").val()).toFixed(2);
    var air_national_rate = parseFloat($("#air_national_rate").val()).toFixed(2);
    var air_international_rate = parseFloat($("#air_international_rate").val()).toFixed(2);       
    var sea_local_rate = parseFloat($("#sea_local_rate").val()).toFixed(2);
    var sea_national_rate = parseFloat($("#sea_national_rate").val()).toFixed(2);
    var sea_international_rate = parseFloat($("#sea_international_rate").val()).toFixed(2);

    var earth_local_duration = parseFloat($("#earth_local_duration").val()).toFixed(2);
    var earth_national_duration = parseFloat($("#earth_national_duration").val()).toFixed(2);
    var earth_international_duration = parseFloat($("#earth_international_duration").val()).toFixed(2);       
    var air_local_duration = parseFloat($("#air_local_duration").val()).toFixed(2);
    var air_national_duration = parseFloat($("#air_national_duration").val()).toFixed(2);
    var air_international_duration = parseFloat($("#air_international_duration").val()).toFixed(2);       
    var sea_local_duration = parseFloat($("#sea_local_duration").val()).toFixed(2);
    var sea_national_duration = parseFloat($("#sea_national_duration").val()).toFixed(2);
    var sea_international_duration = parseFloat($("#sea_international_duration").val()).toFixed(2);

    if(!categories) {   
      $("#error_category_id").addClass('error').html("<?= $this->lang->line('Please Select at-least one Category.'); ?>"); 
      $('#s2id_category_id', function() { $('#s2id_category_id').siblings('select').select2('open'); }); 
    } else if(country_id == 0 ) {  
      $("#error_country_id").addClass('error').html("<?= $this->lang->line('select_country'); ?>"); 
      $('#s2id_country_id', function() { $('#s2id_country_id').siblings('select').select2('open'); });
    } else if(currency_id == 0 ) {
      $("#error_country_id").removeClass('error').html("");
      $("#error_currency_id").addClass('error').html("<?= $this->lang->line('Select Currency'); ?>");
      $("#currency_id").focus();
    } else if(isNaN(min_distance)) {
      $("#error_country_id, #error_currency_id").removeClass('error').html("");
      $("#error_min_distance").addClass('error').html("<?= $this->lang->line('Please enter minimum distance.'); ?>");
      $("#min_distance").focus();
    } else if(isNaN(max_distance)) {
      $("#error_country_id, #error_currency_id, #error_min_distance").removeClass('error').html("");
      $("#error_max_distance").addClass('error').html("<?= $this->lang->line('Please enter maximum distance.'); ?>");
      $("#max_distance").focus();
    } else if(parseFloat(max_distance) < parseFloat(min_distance) ) {
      $("#error_min_height,#error_max_weight, #error_min_weight,#error_currency_id").removeClass('error').html("");
      $("#error_country_id, #error_min_height, #error_max_height").removeClass('error').html("");
      $("#error_min_distance").removeClass('error').html("");
      $("#error_max_distance").addClass('error').html("<?= $this->lang->line('Please enter maximum distance greater than minimum distance.'); ?>");
      $("#max_distance").focus();
    } else if(isNaN(min_weight)) {
      $("#error_country_id, #error_currency_id, #error_min_distance").removeClass('error').html("");
      $("#error_max_distance").removeClass('error').html("");
      $("#error_min_weight").addClass('error').html("<?= $this->lang->line('Enter minimum weight.'); ?>");
      $("#min_weight").focus();
    } else if(isNaN(max_weight)) {
      $("#error_country_id, #error_currency_id, #error_min_distance").removeClass('error').html("");
      $("#error_max_distance,#error_min_weight").removeClass('error').html("");
      $("#error_max_weight").addClass('error').css('font-size','normal').html("<?= $this->lang->line('Enter minimum weight.'); ?>");
      $("#max_weight").focus();
    } else if(parseFloat(max_weight) <= parseFloat(min_weight) ) {
      $("#error_country_id, #error_currency_id, #error_min_distance").removeClass('error').html("");
      $("#error_max_distance,#error_min_weight").removeClass('error').html("");
      $("#error_max_weight").addClass('error').css('font-size','normal').html("<?= $this->lang->line('Maximum weight should be greater than minimum weight.'); ?>");
      $("#max_weight").focus();
    } else if(unit_id == 0 ) {
      $("#error_country_id, #error_currency_id, #error_min_distance").removeClass('error').html("");
      $("#error_max_distance,#error_min_weight, #error_max_weight").removeClass('error').html("");
      $("#error_unit_id").addClass('error').html("<?= $this->lang->line('Please select unit.'); ?>");
      $('#s2id_unit_id', function() { $('#s2id_unit_id').siblings('select').select2('open'); });
    } else if(isNaN(earth_local_rate) ) {
      $("#error_max_weight, #error_min_weight,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration").removeClass('error').html("");
      $("#error_earth_local_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#earth_local_rate").focus();
    } else if(isNaN(earth_national_rate) ) {
      $("#error_max_weight, #error_min_weight,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_local_rate").removeClass('error').html("");
      $("#error_earth_national_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#earth_national_rate").focus();
    } else if(isNaN(earth_international_rate) ) {
      $("#error_max_weight, #error_min_weight,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_national_rate").removeClass('error').html("");
      $("#error_earth_international_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#earth_international_rate").focus();
    } else if(isNaN(earth_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");      
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#earth_local_duration").focus();
    } else if(earth_local_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#earth_local_duration").focus();
    } else if(isNaN(earth_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_earth_local_duration, #error_earth_local_rate").removeClass('error').html("");
      $("#error_earth_national_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#earth_national_duration").focus();
    } else if(earth_national_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#earth_national_duration").focus();
    } else if(isNaN(earth_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_earth_local_duration, #error_earth_national_rate, #error_earth_national_duration").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_earth_international_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#earth_international_duration").focus();
    } else if(earth_international_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#earth_international_duration").focus();
    } else if(isNaN(air_local_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_international_rate").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_air_local_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#air_local_rate").focus();
    } else if(isNaN(air_national_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_local_rate").removeClass('error').html("");
      $("#error_air_national_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#air_national_rate").focus();
    } else if(isNaN(air_international_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_national_rate").removeClass('error').html("");
      $("#error_air_international_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#air_international_rate").focus();
    } else if(isNaN(air_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_international_rate").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_air_local_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#air_local_duration").focus();
    } else if(air_local_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#air_local_duration").focus();
    } else if(isNaN(air_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_local_rate").removeClass('error').html("");
      $("#error_air_national_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#air_national_duration").focus();
    } else if(air_national_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#air_national_duration").focus();
    } else if(isNaN(air_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension, #error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_national_rate").removeClass('error').html("");
      $("#error_air_international_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#air_international_duration").focus();
    } else if(air_international_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#air_international_duration").focus();
    } else if(isNaN(sea_local_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_international_rate").removeClass('error').html("");
      $("#error_sea_local_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#sea_local_rate").focus();
    } else if(isNaN(sea_national_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_local_rate").removeClass('error').html("");
      $("#error_sea_national_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#sea_national_rate").focus();
    } else if(isNaN(sea_international_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_national_rate").removeClass('error').html("");
      $("#error_sea_international_rate").addClass('error').html("<?= $this->lang->line('Enter Rate'); ?>");
      $("#sea_international_rate").focus();
    } else if(isNaN(sea_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_international_rate").removeClass('error').html("");
      $("#error_sea_local_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#sea_local_duration").focus();
    } else if(sea_local_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#sea_local_duration").focus();
    } else if(isNaN(sea_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_local_rate").removeClass('error').html("");
      $("#error_sea_national_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#sea_national_duration").focus();
    } else if(sea_national_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#sea_national_duration").focus();
    } else if(isNaN(sea_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_national_rate").removeClass('error').html("");
      $("#error_sea_international_duration").addClass('error').html("<?= $this->lang->line('Enter Duration'); ?>");
      $("#sea_international_duration").focus();
    } else if(sea_international_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("<?= $this->lang->line('Enter duration greater than 1.'); ?>");
      $("#sea_international_duration").focus();
    } else { 
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_international_rate").removeClass('error').html(''); 
      $("#standard_rate_add")[0].submit(); 
    }
  });
</script>