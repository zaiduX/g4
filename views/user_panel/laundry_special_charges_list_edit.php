    <style type="text/css">
        #available[]-error {
            margin-top: 20px;
        }
    </style>
    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/dashboard-laundry'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-laundry/laundry-special-charges-list'; ?>"><span><?= $this->lang->line('laundry Special Rate'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Setup laundry Special Rate'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Setup laundry Special Rate'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Enter special rates for special dates'); ?></small>    
        </div>
      </div>
    </div>
    <div class="content">
        <!-- Add New Points -->
        <div class="row">
            <div class="col-lg-12">
              <div class="hpanel hblue">
                <form action="<?= base_url('user-panel-laundry/laundry-special-charges-update'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="tripAdd">
                    <input type="hidden" name="charge_id" value="<?=$charges['charge_id']?>">
                    <div class="panel-body">
                        <?php if($this->session->flashdata('error')):  ?>
                            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('success')):  ?>
                            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        <?php endif; ?>

                        <div class="row form-group">
                            <div class="col-md-4">
                                <h3><?= $this->lang->line('Define Special Rates'); ?></h3>
                            </div>
                            <div class="col-md-4">
                                <label class=""><?= $this->lang->line('Special Rate Start Date'); ?></label><br />
                                <div class="input-group">
                                    <input type="text" class="form-control" id="start_date" name="start_date" value="<?=($charges['start_date']!='NULL')?$charges['start_date']:''?>" autocomplete="off" />
                                    <div class="input-group-addon dpAddon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-4">
                                <label class=""><?= $this->lang->line('Special Rate End Date'); ?></label><br />
                                <div class="input-group">
                                    <input type="text" class="form-control" id="end_date" name="end_date" value="<?=($charges['end_date']!='NULL')?$charges['end_date']:''?>" autocomplete="off"/>
                                    <div class="input-group-addon ddAddon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <label for="discount_percent" class="control-label"><?=$this->lang->line('monday')?></label>
                                <div class="input-group">
                                  <input type="number" class="form-control" id="mon" placeholder="<?=$this->lang->line('Enter Discount Percent')?>" value="<?=$charges['mon']?>" name="mon"/><span class="input-group-addon">%</span>
                                </div>    
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <label for="discount_percent" class="control-label"><?=$this->lang->line('tuesday')?></label>
                                <div class="input-group">
                                  <input type="number" class="form-control" id="tue" placeholder="<?=$this->lang->line('Enter Discount Percent')?>" value="<?=$charges['tue']?>" name="tue"/><span class="input-group-addon">%</span>
                                </div>    
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <label for="discount_percent" class="control-label"><?=$this->lang->line('wednesday')?></label>
                                <div class="input-group">
                                  <input type="number" class="form-control" id="wed" placeholder="<?=$this->lang->line('Enter Discount Percent')?>" value="<?=$charges['wed']?>" name="wed"/><span class="input-group-addon">%</span>
                                </div>    
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <label for="discount_percent" class="control-label"><?=$this->lang->line('thursday')?></label>
                                <div class="input-group">
                                  <input type="number" class="form-control" id="thu" placeholder="<?=$this->lang->line('Enter Discount Percent')?>" value="<?=$charges['thu']?>" name="thu"/><span class="input-group-addon">%</span>
                                </div>    
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <label for="discount_percent" class="control-label"><?=$this->lang->line('friday')?></label>
                                <div class="input-group">
                                  <input type="number" class="form-control" id="fri" placeholder="<?=$this->lang->line('Enter Discount Percent')?>" value="<?=$charges['fri']?>" name="fri"/><span class="input-group-addon">%</span>
                                </div>    
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <label for="discount_percent" class="control-label"><?=$this->lang->line('saturday')?></label>
                                <div class="input-group">
                                  <input type="number" class="form-control" id="sat" placeholder="<?=$this->lang->line('Enter Discount Percent')?>" value="<?=$charges['sat']?>" name="sat"/><span class="input-group-addon">%</span>
                                </div>    
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <label for="discount_percent" class="control-label"><?=$this->lang->line('sunday')?></label>
                                <div class="input-group">
                                  <input type="number" class="form-control" id="sun" placeholder="<?=$this->lang->line('Enter Discount Percent')?>" value="<?=$charges['sun']?>" name="sun"/><span class="input-group-addon">%</span>
                                </div>    
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-info" data-style="zoom-in"><?= $this->lang->line('Save Special Rate'); ?></button>
                                <a href="<?= base_url('user-panel-laundry/laundry-special-charges-list'); ?>" class="btn btn-primary"><?= $this->lang->line('back'); ?></a>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>

    <script>
        $("#tripAdd")
        .validate({
            ignore: [],
            rules: {
                start_date: { required : true, },
                end_date: { required : true },
                mon: { min : 0 , max: 100 },
                tue: { min : 0 , max: 100 },
                wed: { min : 0 , max: 100 },
                thu: { min : 0 , max: 100 },
                fri: { min : 0 , max: 100 },
                sat: { min : 0 , max: 100 },
                sun: { min : 0 , max: 100 },
            },
            messages: {
                start_date: { required : "<?= $this->lang->line('Select start date!'); ?>", },
                end_date: { required : "<?= $this->lang->line('Select end date!'); ?>" },
            },
        });
    </script>
    <script>
    $(document).ready(function() {
      $('#start_date').datepicker({
        startDate: new Date(),
        autoclose: true,
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('#end_date').datepicker({
            startDate: minDate,
            autoclose: true,
          });     
        });
    });

    $(".dpAddon" ).click(function( e ) { 
      $("#start_date").focus();
    });
    $(".ddAddon" ).click(function( e ) { 
      $("#end_date").focus();
    });
</script>