    <style type="text/css">
        #available[]-error {
            margin-top: 20px;
        }
    </style>
    <div class="normalheader small-header">
      <div class="hpanel">
        <div class="panel-body">
          <a class="small-header-action" href="">
            <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
            </div>
          </a>

          <div id="hbreadcrumb" class="pull-right">
            <ol class="hbreadcrumb breadcrumb">
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard'; ?>"><span><?= $this->lang->line('dash'); ?></span></a></li>
              <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/bus-trip-master-list'; ?>"><span><?= $this->lang->line('Trip Master List'); ?></span></a></li>
              <li class="active"><span><?= $this->lang->line('Disable Trip'); ?></span></li>
            </ol>
          </div>
          <h2 class="font-light m-b-xs">  <i class="fa fa-users fa-2x text-muted"></i> <?= $this->lang->line('Disable Trip'); ?></h2>
          <small class="m-t-md"><?= $this->lang->line('Disable trip for booking'); ?></small>    
        </div>
      </div>
    </div>
    <?php $avail = explode(',', $trip_details[0]['trip_availability'] );  ?>
    <div class="content">
        <!-- Add New Points -->
        <div class="row">
            <div class="col-lg-12">
              <div class="hpanel hblue">
                <div class="panel-body">
                    <?php if($this->session->flashdata('error')):  ?>
                        <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                    <?php endif; ?>
                    <?php if($this->session->flashdata('success')):  ?>
                        <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                    <?php endif; ?>
                    <div class="row form-group">
                        <form action="<?= base_url('user-panel-bus/bus-trip-master-disable-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="disableTrip">
                            <input type="hidden" name="trip_id" value="<?=$trip_details[0]['trip_id']?>">
                            <div class="col-md-6">
                                <h3><?= $this->lang->line('Trip Details'); ?></h3>
                            </div>
                            <div class="col-md-4">
                                <label class=""><?= $this->lang->line('Trip Disable From'); ?></label>
                                <div class="input-group date" data-provide="datepicker" data-date-start-date="0d">
                                    <input type="text" class="form-control" id="trip_end_date" name="trip_end_date" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-2">
                                <br />
                                <button type="submit" id="btnDisableTrip" style="min-height: 40px;" class="btn btn-danger form-control" data-style="zoom-in"><?= $this->lang->line('Disable Trip'); ?></button>
                            </div>
                        </form>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label class=""><?= $this->lang->line('Source'); ?></label>
                                <h5><?=$trip_details[0]['trip_source']?></h5>
                            </div>
                            <div class="col-md-6">
                                <label class=""><?= $this->lang->line('Destination'); ?></label>
                                <h5><?=$trip_details[0]['trip_destination']?></h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label class=""><?= $this->lang->line('Bus Details'); ?></label>
                                <h5><?php foreach ($buses as $bus) { if($trip_details[0]['bus_id']==$bus['bus_id']) { echo $bus['bus_modal'].' ['.$bus['bus_make'].'] ['.$bus['bus_seat_type'].', '.$bus['bus_ac'].']'; } } ?></h5>
                            </div>
                            <div class="col-md-3">
                                <label class=""><?= $this->lang->line('Start Date'); ?></label>
                                <h5><?=$trip_details[0]['trip_start_date']?></h5>
                            </div>
                            <div class="col-md-3">
                                <label class=""><?= $this->lang->line('End Date'); ?></label><br />
                                <h5 id="trip_end_date"><?=$trip_details[0]['trip_end_date']?></h5>
                            </div>
                        </div>
                    </div>
                    <hr />

                    <div class="row form-group">
                        <div class="col-md-12">
                            <h3><?= $this->lang->line('Trip Bookings'); ?></h3>
                        </div>
                    </div>

                    <table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th><?= $this->lang->line('B. ID'); ?></th>
                                <th><?= $this->lang->line('Customer Name'); ?></th>
                                <th><?= $this->lang->line('Customer Conatct'); ?></th>
                                <th><?= $this->lang->line('Customer Email'); ?></th>
                                <th><?= $this->lang->line('Booking Date'); ?></th>
                                <th><?= $this->lang->line('Journey Date'); ?></th>
                                <th><?= $this->lang->line('Payment Status'); ?></th>
                                <!--<th><?= $this->lang->line('action'); ?></th>-->
                            </tr>
                        </thead>
                        <tbody class="panel-body">
                            <?php foreach ($booking_details as $bdetail) { static $i = 0; $i++; ?>
                                <tr>
                                    <td><?=$bdetail['ticket_id']?></td>
                                    <td><?=$bdetail['cust_name']?></td>
                                    <td><?=$bdetail['cust_contact']?></td>
                                    <td><?=$bdetail['cust_email']?></td>
                                    <td><?=$bdetail['bookig_date']?></td>
                                    <td><?=$bdetail['journey_date']?></td>
                                    <td><?=($bdetail['complete_paid']==0) ? $this->lang->line('Completed') : $this->lang->line('Pending') ?></td>
                                    <!--<td style="display: flex;">
                                        <form action="<?=base_url('user-panel-bus/bus-trip-special-rate-edit')?>" method="post" style="display: inline-block;">
                                            <input type="hidden" name="trip_id" value="<?= $bdetail['trip_id'] ?>">
                                            <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-wrench"></i> <span class="bold"><?= $this->lang->line('Setup'); ?></span></button>
                                        </form>
                                    </td>-->
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        $('#btnDisableTrip').click(function (e) {
            e.preventDefault();

            var trip_end_date = $('#trip_end_date').val();
            if(trip_end_date != '') {
                swal(
                {
                    title: "<?= $this->lang->line('are_you_sure'); ?>",
                    text: "<?= $this->lang->line('All bookings will get cancelled!'); ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->lang->line('Yes, disable it.'); ?>",
                    cancelButtonText: "<?= $this->lang->line('no_cancel_it'); ?>",
                    closeOnConfirm: false,
                    closeOnCancel: false 
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#disableTrip').submit();
                    } else { swal("<?= $this->lang->line('canceled'); ?>", "<?= $this->lang->line('No change'); ?>", "error"); }
                });
            } else {
                swal("<?= $this->lang->line('error'); ?>", "<?= $this->lang->line('Select disable date!'); ?>", "error");
            }
        });
    </script>