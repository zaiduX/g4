<div class="panel-body" style="line-height: 15px; padding: 0px 0px 10px 15px !important;">
	<div class="col-md-12" style="line-height: 15px;">
	  <div class="col-lg-2"> 
	    <h5 class="text-left text-light"><?= $this->lang->line('ref#'); ?><?= $order['order_id'] ?></h5>
	    <h6 class="text-left text-light hidden"><?= $this->lang->line('delivery_code'); ?>: <?= $order['delivery_code'] ?></h6>
	  </div>
	  <div class="col-lg-6 text-center"> 
	    <h4>
	      <?php 
	      $category_details = $this->api->get_category_details_by_id($order['category_id']); 
	      $icon_url = $category_details['icon_url'];
	      if($order['category_id'] == 6) : ?>
	        <img style="max-height: 28px" src='<?= base_url($icon_url) ?>' /><?= $this->lang->line('transportation'); ?>
	      <?php else : ?>
	        <img style="max-height: 28px" src='<?= base_url($icon_url) ?>' /><?= $this->lang->line('lbl_courier'); ?>
	      <?php endif; ?>
	    </h4>
	    <!-- <h6 class=""><?= ($order['order_description'] !="NULL")?$order['order_description']:""; ?></h6>    --> 
	  </div>
	  <div class="col-lg-4"> 
	    <h5 class="text-right text-light">Order Date: <font style="color: #59bdd7;"><?= date('l, d M Y',strtotime($order['cre_datetime'])); ?></font></h5>
	    <h6 class="text-right text-light">Expiry Date: <?= date('l, d M Y',strtotime($order['expiry_date'])); ?></h6>
	  </div>
	  <br/>
	</div>
	<div class="col-md-12" style="margin-bottom: 3px; margin-left: -15px; padding-right: 0; padding-left: 30px;">
	  <h5 style="margin-left: -15px;">
	    <div class="col-md-6">
	      <i class="fa fa-map-marker"></i> <?= $this->lang->line('from'); ?>: <font style="color: #59bdd7;"><?= ($order['from_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?=$order['from_address']?></font>
	    </div>
	    <div class="col-md-3">
	      <?=$this->lang->line('sender_phone')?>: <?=($order['from_address_contact'] == 'NULL')?$this->lang->line('not_provided'):$order['from_address_contact']?>
	    </div>
	    <div class="col-md-3">
	      <?=$this->lang->line('pickup_date'); ?>: <font style="color: #59bdd7;"><?= $order['pickup_datetime'] ?></font>
	    </div>
	  </h5>
	</div>
	<div class="col-md-12" style="margin-bottom: 5px; margin-left: -15px; padding-right: 0; padding-left: 30px;">
	  <h5 style="margin-left: -15px;">
	    <div class="col-md-6">
	      <i class="fa fa-map-marker"></i> <?= $this->lang->line('to'); ?>: <font style="color: #59bdd7;"><?= ($order['to_relay_id'] > 0) ? $this->lang->line('relay'): ''; ?> <?php echo $order['to_address'];?></font>
	    </div>
	    <div class="col-md-3">
	      <?=$this->lang->line('reciever_phone')?>: <?=($order['to_address_contact'] == 'NULL')?$this->lang->line('not_provided'):$order['to_address_contact']?>
	    </div>
	    <div class="col-md-3">
	      <?=$this->lang->line('deliver_date'); ?>: <font style="color: #59bdd7;"><?= $order['delivery_datetime']?></font>
	    </div>
	  </h5>
	</div>
	<div class="col-md-12" style="margin-bottom: 0px; margin-left: -15px; padding-right: 0; padding-left: 30px;">
	  <h5 class="small" style="margin-left: -15px;">
	    <div class="col-md-6"> <?= $this->lang->line('price'); ?>: <font style="color: #59bdd7;"><?= $order['currency_sign'] ?> <?= $order['order_price'] ?></font></div>
	    <div class="col-md-3"> <?= $this->lang->line('advance'); ?>: <font style="color: #59bdd7;"><?= $order['currency_sign'] ?> <?= $order['advance_payment'] ?></font></div>
	    <div class="col-md-3"> <?= $this->lang->line('balance'); ?>: <font style="color: #59bdd7;"><?= $order['currency_sign'] . ' '. ($order['order_price'] - $order['paid_amount']); ?></font></div>
	  </h5>
	</div>
	<div class="col-md-12" style="margin-bottom: 0px; margin-left: -15px; padding-right: 0; padding-left: 30px;">
	  <h5 class="small" style="margin-left: -15px;">
	    <div class="col-md-6"> <?= $this->lang->line('weight'); ?>: <?= $order['total_weight'] ?> <?= strtoupper($this->user->get_unit_name($order['unit_id'])); ?></div>
	    <div class="col-md-3"> <?= $this->lang->line('by'); ?>: <?= $this->user->get_vehicle_type_name($order['vehical_type_id']); ?></div>
	    
	    <div class="col-md-3"> <?= $this->lang->line('no_of_packages'); ?>: <?= $order['package_count']; ?></div>
	  </h5>
	</div>
	</div>