<style>
    #map { height: 100%; }
</style>
<style>
    #map { height: 100%; }
    .numbers{ background-color: #3498db;
    color: #fff;
    padding: 1px 6px 1px 6px;
</style>

    <div class="normalheader small-header">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>
                

                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                        <li><a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-cuurrent-trips'; ?>"><?= $this->lang->line('Current Trips'); ?></a></li>
                        <li class="active"><span><?= $this->lang->line('Trip Details'); ?></span></li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs"> <i class="fa fa-bus fa-2x text-muted"></i> <?= $this->lang->line('Trip Details'); ?></h2>
                <small class="m-t-md"><?= $this->lang->line('Trip Details'); ?></small> 
            </div>
        </div>
    </div>

    <div class="content">
        <div class="hpanel">
            <div class="panel-body">
                <!-- <?php echo json_encode($ticket_seat_details); ?> -->
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="col-md-4 form-group">
                            <div>
                                <h3 class="stat-label">&nbsp;</h3>
                            </div>
                            <?php 
                                $lat = "";
                                $long = "";
                                $driver=$this->api->get_customer_bus_drivers_by_bus_id($trip_details[0]['bus_id']);
                                //echo json_encode($driver); die();
                                if(!$driver){  
                                } else {
                                    $lat = $driver[0]['latitude']; 
                                    $long = $driver[0]['longitude'];  ?>
                                    <!--zaid// Start Google map Location Direction -->
                                    <div class="form-group text-center" style="border: 1px solid #3498db">
                                        <h5 style="color:#3498db" class="text-center"><?= $this->lang->line('Current Trip Location');?></h5>
                                        <iframe
                                            width="100%"
                                            height="375"
                                            frameborder="0" style="border:0"
                                            src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin=<?=$lat?>,<?=$long?>&destination=<?=$trip_destination[0]['lat_long']?>">
                                        </iframe>
                                    </div>
                                <?php } ?>
                            <div class="form-group text-center" style="border: 1px solid #3498db">
                                <h5 style="color:#3498db" class="text-center"><?= $this->lang->line('direction_on_map');?></h5>
                                <iframe
                                    width="100%"
                                    height="375"
                                    frameborder="0" style="border:0"
                                    src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin=<?=$trip_source[0]['lat_long']?>&destination=<?=$trip_destination[0]['lat_long']?>">
                                </iframe>
                            </div>
                            <!--zaid// End Google map Location Direction -->  
                            <!-- <div class="form-group text-center" style="border: 1px solid #3498db">
                                <h5 style="color:#3498db" class="text-center"><?= $this->lang->line('Vehicle image');?></h5>
                                <?php
                                    if(isset($buses['bus_image_url']) && $buses['bus_image_url'] != "NULL") {
                                        echo '<img class="img-thumbnail" src='.base_url($buses['bus_image_url']).' style="max-width: 300px; max-height: auto;" />';
                                    }  else {
                                        echo '<img class="img-thumbnail" src='.base_url('resources/no-image.jpg').' style="max-width: 300px; max-height: auto;" />';
                                    }
                                    echo "<br/>";
                                ?>
                                <br/>  
                            </div> -->
                        </div>
                        <div class="col-md-8 form-group">
                            <div class="row" style="">
                                <div class="col-md-12">
                                    <h3 class="stat-label" style="display: inline-block;"><?= $this->lang->line('Trip Details'); ?></h3>
                                    <div class="pull-right text-right" style="display: inline-block;">
                                        <h5 class="stat-label"><?= $this->lang->line('Trip id'); ?>-<?=$trip_id ?></h5>
                                    </div>
                                </div>
                            </div>
                            <!--zaid// Start Trip Details -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">1</span> <?= $this->lang->line('Trip Details'); ?>
                                    </h5>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Start Date'); ?>:&nbsp;</strong><?=$trip_details[0]['trip_start_date']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('End Date');?>:&nbsp;</strong> <?=$trip_details[0]['trip_end_date']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Depart Time');?>:&nbsp;</strong> <?=$trip_details[0]['trip_depart_time']?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Trip_Distance');?>:&nbsp;</strong> <?=round($trip_distance,2)?><?= $this->lang->line('Km');?>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Trip Availability');?>:&nbsp;</strong> 
                                        <?php
                                            $availability = explode(',', $trip_details[0]['trip_availability']);
                                            if(in_array('mon',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Mon</span>&nbsp;';
                                            }  else  {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Mon</span>&nbsp;';   
                                            }
                                            if(in_array('tue',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Tue</span>&nbsp;';
                                            } else {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Tue</span>&nbsp;';   
                                            }
                                            if(in_array('wed',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Wed</span>&nbsp;';
                                            }  else  {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Wed</span>&nbsp;';   
                                            }
                                            if(in_array('thu',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Thu</span>&nbsp;';
                                            } else {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Thu</span>&nbsp;';   
                                            }
                                            if(in_array('fri',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Fri</span>&nbsp;';
                                            } else {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Fri</span>&nbsp;';   
                                            }   
                                            if(in_array('sat',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Sat</span>&nbsp;';
                                            }  else {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Sat</span>&nbsp;';   
                                            }
                                            if(in_array('sun',$availability)) {
                                              echo '<span style="color: white; background-color:#2fb62f" class="numbers">Sun</span>&nbsp;';
                                            } else  {
                                              echo '<span style="color: white; background-color:#cc2900" class="numbers">Sun</span>&nbsp;';   
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!--zaid// End trip Details -->
                            <br/>                   
                            <!--zaid// Start locations Details -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">2</span> <?= $this->lang->line('Location Details'); ?></h5>
                                    <?php $this->load->model('api_model_bus', 'api');  ?>
                                    <?php for($z=0; $z<sizeof($source_desitnation); $z++) {
                                    $Pickup_point=""; $drop_point=""; ?>
                                    <h4 class="m-b-xs"><?= $this->lang->line('Source'); ?>:&nbsp;
                                        <?=$source_desitnation[$z]['trip_source']?>
                                    </h4>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Pickup Point'); ?>:&nbsp;</strong>
                                        <?php
                                        $src_point_ids_array = explode(',',$source_desitnation[$z]['src_point_ids']);
                                        echo $this->api->get_location_point_address_by_id($src_point_ids_array[0]);
                                        if(sizeof($src_point_ids_array)>1) {
                                            echo '<button style="color: #3498db" type="button" class="btn btn-link" data-toggle="modal" data-target="#'.$source_desitnation[$z]['sdd_id'].'Pickup">'.$this->lang->line('...More').'</button>';
                                        }   
                                        $n=1;  
                                        for($i=0; $i<sizeof($src_point_ids_array); $i++) { 
                                            $Pickup_point .= "<br/>".$n." : ".$this->api->get_location_point_address_by_id($src_point_ids_array[$i]);
                                            $n++;
                                        }      
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="m-b-xs"><?= $this->lang->line('Destination');?>:&nbsp;<?=$source_desitnation[$z]['trip_destination']?></h4>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Drop Point'); ?>:&nbsp;</strong>
                                        <?php
                                            $dest_point_ids_array = explode(',',$source_desitnation[$z]['dest_point_ids']);
                                            echo $this->api->get_location_point_address_by_id($dest_point_ids_array[0]);
                                            if(sizeof($dest_point_ids_array)>1) {
                                                echo '<button style="color: #3498db" type="button" class="btn btn-link" data-toggle="modal" data-target="#'.$source_desitnation[$z]['sdd_id'].'drop">'.$this->lang->line('...More').'</button>';
                                            }
                                            $s=1;
                                            for($i=0; $i<sizeof($dest_point_ids_array); $i++) { 
                                                $drop_point .= "<br/>".$s." : ".$this->api->get_location_point_address_by_id($dest_point_ids_array[$i]);
                                                $s++;
                                            }
                                        ?>
                                    </div>
                                    <br />
                                    <div class="modal fade" id="<?=$source_desitnation[$z]['sdd_id']?>Pickup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title"><img src="<?=base_url('resources/images/dashboard-logo.jpg')?>" class="center-block"></h4>
                                                    <h5 class="modal-title" id="exampleModalLongTitle"><?=$this->lang->line('Pickup Points')?></h5>
                                                </div>
                                                <div class="modal-body">
                                                    <h4 class="m-b-xs"><?= $this->lang->line('Source'); ?>:&nbsp;
                                                        <?=$source_desitnation[$z]['trip_source']?>
                                                    </h4>
                                                    <strong><?= $this->lang->line('Pickup Points');?>:&nbsp;</strong>
                                                    <?=$Pickup_point?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="modal fade" id="<?=$source_desitnation[$z]['sdd_id']?>drop" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title"><img src="<?=base_url('resources/images/dashboard-logo.jpg')?>" class="center-block "></h4>
                                                    <h5 class="modal-title" id="exampleModalLongTitle"><?=$this->lang->line('Drop Points')?></h5>
                                                </div>
                                                <div class="modal-body">
                                                    <h4 class="m-b-xs"><?=$this->lang->line('Destination');?>:&nbsp;<?=$source_desitnation[$z]['trip_destination']?></h4>
                                                    <strong><?= $this->lang->line('Drop Points'); ?>:&nbsp;</strong>
                                                    <?=$drop_point?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('close');?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <?php } ?>
                                </div>
                            </div>
                            <!--zaid// End locations Details -->
                            <br />
                            <!--zaid// Start Vehicle Details -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">3</span> <?= $this->lang->line('Vehicle Details'); ?></h5>     
                                </div>
                                <div class="col-md-12" style="margin-bottom: 8px;">
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Make_Year'); ?>:&nbsp;</strong><?=$buses['bus_make']?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_modal');?>:&nbsp;</strong> <?=$buses['bus_modal']?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_No'); ?>:&nbsp;</strong><?=$buses['bus_no']?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Seat_Type');?>:&nbsp;</strong><?=$buses['bus_ac']?>&nbsp;\&nbsp;<?=$buses['bus_seat_type']?></div>
                                    <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Amenities');?>:&nbsp;</strong>
                                        <?php
                                        $amenities = explode(',',$buses['bus_amenities']);
                                        foreach($amenities as $amen) { echo $amen."&nbsp;"; }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!--zaid// End Vehicle Details -->
                            <br/>
                            <!--zaid// Start ticket rates Details -->
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">4</span> <?= $this->lang->line('Ticket Rates');?></h5>
                                        <div class="col-md-6">
                                            <label class="" style="color: #3498db"><?= $this->lang->line('General Rates'); ?></label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="" style="color: #3498db"><?= $this->lang->line('special_rates'); ?></label>
                                        </div>
                                    <div class="row form-group">
                                        <?php foreach ($seat_type_price as $price): ?>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <strong><label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Price'); ?></label></strong>
                                                :&nbsp;<?=$price['seat_type_price']?>&nbsp;<?=$trip_details[0]['currency_sign']?>
                                            </div>
                                            <div class="col-md-6">
                                                <strong><label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Special Price'); ?></label></strong>
                                                :&nbsp;<?=($price['special_price']=='NULL')?'0':$price['special_price']?>&nbsp;<?=$trip_details[0]['currency_sign']?>
                                            </div>
                                        </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
                            <!--zaid// End ticket Details -->
                            <br />
                            <div class="row" style="border: 1px solid #3498db">
                                <div class="col-md-12" >
                                    <h5 style="color: #3498db"><span class="numbers">3</span> <?= $this->lang->line('operator_Details'); ?>
                                    </h5>
                                    <div class="col-md-4">
                                        <?php
                                            if($operator_details['avatar_url']!="NULL") { 
                                                echo '<img class="img-thumbnail" src='.base_url($operator_details["avatar_url"]).' style="max-width: 200px; max-height: auto;"/>';
                                            } else { 
                                                echo '<img class="img-thumbnail" src='.base_url("resources/default-profile.jpg").' style="max-width: 200px; max-height: auto;" />'; 
                                        } ?>
                                    </div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('f_name'); ?>:&nbsp;</strong><?=$operator_details['firstname']?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('l_name'); ?>:&nbsp;</strong><?=$operator_details['lastname']?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('lbl_email'); ?>:&nbsp;</strong><?=$operator_details['email_id']?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('contact_number'); ?>:&nbsp;</strong><?=$operator_details['contact_no']?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('company_name'); ?>:&nbsp;</strong><?=$operator_details['company_name']?></div>
                                    <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('address'); ?>:&nbsp;</strong><?=$operator_details['address']?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <?php
                    if($booking_details[0]['return_trip_id'] > 0) { 
                        $booking_details = $this->api->get_trip_booking_details($booking_details[0]['return_trip_id']);                        
                        $return_trip_details = $this->api->get_trip_master_details($booking_details['trip_id']);
                        $operator_details = $this->api->get_bus_operator_profile($return_trip_details[0]['cust_id']);
                        $vehical_type_id = $return_trip_details[0]['vehical_type_id'];
                        $locations = $this->api->get_locations_by_vehicle_type_id($vehical_type_id, $return_trip_details[0]['cust_id']);
                        $buses = $this->api->get_operator_bus_details($return_trip_details[0]['bus_id']);
                        $source_desitnation=$this->api->get_source_destination_of_trip($return_trip_details[0]['trip_id']);
                        $seat_type_price = $this->api->get_bus_trip_seat_type_price($return_trip_details[0]['trip_id']);
                        $src_dest_details = $this->api->get_trip_location_master($return_trip_details[0]['trip_id']);
                        $trip_source=$this->api->get_trip_source_location($src_dest_details[0]['trip_source_id']);       
                        $trip_destination=$this->api->get_trip_desitination_location($src_dest_details[0]['trip_destination_id']);
                        $trip_from = explode(',', $trip_source[0]['lat_long']);
                        $trip_to = explode(',', $trip_destination[0]['lat_long']);
                        $trip_distance=$this->api->Distance($trip_from[0],$trip_from[1],$trip_to[0],$trip_to[1]);
                    ?>
                    <hr />
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="col-md-4 form-group">
                                <div>
                                    <h3 class="stat-label">&nbsp;</h3>
                                </div>
                                <?php 
                                    $lat = "";
                                    $long = "";
                                    $driver=$this->api->get_customer_bus_drivers_by_bus_id($return_trip_details[0]['bus_id']);
                                    //echo json_encode($driver); die();
                                    if(!$driver){  
                                    } else {
                                        $lat = $driver[0]['latitude']; 
                                        $long = $driver[0]['longitude'];  ?>
                                        zaid// Start Google map Location Direction
                                        <div class="form-group text-center" style="border: 1px solid #3498db">
                                            <h5 style="color:#3498db" class="text-center"><?= $this->lang->line('Current Trip Location');?></h5>
                                            <iframe
                                                width="100%"
                                                height="450"
                                                frameborder="0" style="border:0"
                                                src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin=<?=$lat?>,<?=$long?>&destination=<?=$trip_destination[0]['lat_long']?>">
                                            </iframe>
                                        </div>
                                    <?php } ?>
                                zaid// Start Google map Location Direction
                                <div class="form-group text-center" style="border: 1px solid #3498db">
                                    <h5 style="color:#3498db" class="text-center"><?= $this->lang->line('direction_on_map');?></h5>
                                    <iframe
                                        width="100%"
                                        height="450"
                                        frameborder="0" style="border:0"
                                        src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyA8LSgjoVNoaXXXp_uERcpKOWnIqJc-Rhg&origin=<?=$trip_source[0]['lat_long']?>&destination=<?=$trip_destination[0]['lat_long']?>">
                                    </iframe>
                                </div>
                                zaid// End Google map Location Direction  
                                <div class="form-group text-center" style="border: 1px solid #3498db">
                                    <h5 style="color:#3498db" class="text-center"><?= $this->lang->line('Vehicle image');?></h5>
                                    <?php
                                        if(isset($buses['bus_image_url']) && $buses['bus_image_url'] != "NULL") {
                                            echo '<img class="img-thumbnail" src='.base_url($buses['bus_image_url']).' style="max-width: 300px; max-height: auto;" />';
                                        }  else {
                                            echo '<img class="img-thumbnail" src='.base_url('resources/no-image.jpg').' style="max-width: 300px; max-height: auto;" />';
                                        }
                                        echo "<br/>";
                                    ?>
                                    <br/>  
                                </div>
                            </div>
                            <div class="col-md-8 form-group">
                                <div class="row" style="">
                                    <div class="col-md-12">
                                        <h3 class="stat-label" style="display: inline-block;"><?= $this->lang->line('Return Trip Details'); ?></h3>
                                        <div class="pull-right text-right" style="display: inline-block;">
                                            <h5 class="stat-label"><?= $this->lang->line('Trip id'); ?>-<?=$return_trip_details[0]['trip_id'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                                zaid// Start Trip Details
                                <div class="row" style="border: 1px solid #3498db">
                                    <div class="col-md-12" >
                                        <h5 style="color: #3498db"><span class="numbers">1</span> <?= $this->lang->line('Trip Details'); ?>
                                        </h5>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Start Date'); ?>:&nbsp;</strong><?=$return_trip_details[0]['trip_start_date']?>
                                        </div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('End Date');?>:&nbsp;</strong> <?=$return_trip_details[0]['trip_end_date']?>
                                        </div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Depart Time');?>:&nbsp;</strong> <?=$return_trip_details[0]['trip_depart_time']?>
                                        </div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Trip_Distance');?>:&nbsp;</strong> <?=round($trip_distance,2)?><?= $this->lang->line('Km');?>
                                        </div>
                                        <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Trip Availability');?>:&nbsp;</strong> 
                                            <?php
                                                $availability = explode(',', $return_trip_details[0]['trip_availability']);
                                                if(in_array('mon',$availability)) {
                                                  echo '<span style="color: white; background-color:#2fb62f" class="numbers">Mon</span>&nbsp;';
                                                }  else  {
                                                  echo '<span style="color: white; background-color:#cc2900" class="numbers">Mon</span>&nbsp;';   
                                                }
                                                if(in_array('tue',$availability)) {
                                                  echo '<span style="color: white; background-color:#2fb62f" class="numbers">Tue</span>&nbsp;';
                                                } else {
                                                  echo '<span style="color: white; background-color:#cc2900" class="numbers">Tue</span>&nbsp;';   
                                                }
                                                if(in_array('wed',$availability)) {
                                                  echo '<span style="color: white; background-color:#2fb62f" class="numbers">Wed</span>&nbsp;';
                                                }  else  {
                                                  echo '<span style="color: white; background-color:#cc2900" class="numbers">Wed</span>&nbsp;';   
                                                }
                                                if(in_array('thu',$availability)) {
                                                  echo '<span style="color: white; background-color:#2fb62f" class="numbers">Thu</span>&nbsp;';
                                                } else {
                                                  echo '<span style="color: white; background-color:#cc2900" class="numbers">Thu</span>&nbsp;';   
                                                }
                                                if(in_array('fri',$availability)) {
                                                  echo '<span style="color: white; background-color:#2fb62f" class="numbers">Fri</span>&nbsp;';
                                                } else {
                                                  echo '<span style="color: white; background-color:#cc2900" class="numbers">Fri</span>&nbsp;';   
                                                }   
                                                if(in_array('sat',$availability)) {
                                                  echo '<span style="color: white; background-color:#2fb62f" class="numbers">Sat</span>&nbsp;';
                                                }  else {
                                                  echo '<span style="color: white; background-color:#cc2900" class="numbers">Sat</span>&nbsp;';   
                                                }
                                                if(in_array('sun',$availability)) {
                                                  echo '<span style="color: white; background-color:#2fb62f" class="numbers">Sun</span>&nbsp;';
                                                } else  {
                                                  echo '<span style="color: white; background-color:#cc2900" class="numbers">Sun</span>&nbsp;';   
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                zaid// End trip Details
                                <br/>                   
                                zaid// Start locations Details
                                <div class="row" style="border: 1px solid #3498db">
                                    <div class="col-md-12" >
                                        <h5 style="color: #3498db"><span class="numbers">2</span> <?= $this->lang->line('Location Details'); ?></h5>
                                        <?php $this->load->model('api_model_bus', 'api');  ?>
                                        <?php for($z=0; $z<sizeof($source_desitnation); $z++) {
                                        $Pickup_point=""; $drop_point=""; ?>
                                        <h4 class="m-b-xs"><?= $this->lang->line('Source'); ?>:&nbsp;
                                            <?=$source_desitnation[$z]['trip_source']?>
                                        </h4>
                                        <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Pickup Point'); ?>:&nbsp;</strong>
                                            <?php
                                            $src_point_ids_array = explode(',',$source_desitnation[$z]['src_point_ids']);
                                            echo $this->api->get_location_point_address_by_id($src_point_ids_array[0]);
                                            if(sizeof($src_point_ids_array)>1) {
                                                echo '<button style="color: #3498db" type="button" class="btn btn-link" data-toggle="modal" data-target="#'.$source_desitnation[$z]['sdd_id'].'Pickup">'.$this->lang->line('...More').'</button>';
                                            }   
                                            $n=1;  
                                            for($i=0; $i<sizeof($src_point_ids_array); $i++) { 
                                                $Pickup_point .= "<br/>".$n." : ".$this->api->get_location_point_address_by_id($src_point_ids_array[$i]);
                                                $n++;
                                            }      
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h4 class="m-b-xs"><?= $this->lang->line('Destination');?>:&nbsp;<?=$source_desitnation[$z]['trip_destination']?></h4>
                                        <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Drop Point'); ?>:&nbsp;</strong>
                                            <?php
                                                $dest_point_ids_array = explode(',',$source_desitnation[$z]['dest_point_ids']);
                                                echo $this->api->get_location_point_address_by_id($dest_point_ids_array[0]);
                                                if(sizeof($dest_point_ids_array)>1) {
                                                    echo '<button style="color: #3498db" type="button" class="btn btn-link" data-toggle="modal" data-target="#'.$source_desitnation[$z]['sdd_id'].'drop">'.$this->lang->line('...More').'</button>';
                                                }
                                                $s=1;
                                                for($i=0; $i<sizeof($dest_point_ids_array); $i++) { 
                                                    $drop_point .= "<br/>".$s." : ".$this->api->get_location_point_address_by_id($dest_point_ids_array[$i]);
                                                    $s++;
                                                }
                                            ?>
                                        </div>
                                        <br />
                                        <div class="modal fade" id="<?=$source_desitnation[$z]['sdd_id']?>Pickup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title"><img src="<?=base_url('resources/images/dashboard-logo.jpg')?>" class="center-block"></h4>
                                                        <h5 class="modal-title" id="exampleModalLongTitle"><?=$this->lang->line('Pickup Points')?></h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 class="m-b-xs"><?= $this->lang->line('Source'); ?>:&nbsp;
                                                            <?=$source_desitnation[$z]['trip_source']?>
                                                        </h4>
                                                        <strong><?= $this->lang->line('Pickup Points');?>:&nbsp;</strong>
                                                        <?=$Pickup_point?>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                
                                        <div class="modal fade" id="<?=$source_desitnation[$z]['sdd_id']?>drop" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title"><img src="<?=base_url('resources/images/dashboard-logo.jpg')?>" class="center-block "></h4>
                                                        <h5 class="modal-title" id="exampleModalLongTitle"><?=$this->lang->line('Drop Points')?></h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 class="m-b-xs"><?=$this->lang->line('Destination');?>:&nbsp;<?=$source_desitnation[$z]['trip_destination']?></h4>
                                                        <strong><?= $this->lang->line('Drop Points'); ?>:&nbsp;</strong>
                                                        <?=$drop_point?>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('close');?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <?php } ?>
                                    </div>
                                </div>
                                zaid// End locations Details
                                <br />
                                zaid// Start Vehicle Details
                                <div class="row" style="border: 1px solid #3498db">
                                    <div class="col-md-12" >
                                        <h5 style="color: #3498db"><span class="numbers">3</span> <?= $this->lang->line('Vehicle Details'); ?></h5>     
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 8px;">
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Make_Year'); ?>:&nbsp;</strong><?=$buses['bus_make']?></div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_modal');?>:&nbsp;</strong> <?=$buses['bus_modal']?></div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_No'); ?>:&nbsp;</strong><?=$buses['bus_no']?></div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Seat_Type');?>:&nbsp;</strong><?=$buses['bus_ac']?>&nbsp;\&nbsp;<?=$buses['bus_seat_type']?></div>
                                        <div class="col-md-12" style="margin-bottom: 8px;"><strong><?= $this->lang->line('Vehicle_Amenities');?>:&nbsp;</strong>
                                            <?php
                                            $amenities = explode(',',$buses['bus_amenities']);
                                            foreach($amenities as $amen) { echo $amen."&nbsp;"; }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                zaid// End Vehicle Details
                                <br/>
                                zaid// Start ticket rates Details
                                <div class="row" style="border: 1px solid #3498db">
                                    <div class="col-md-12" >
                                        <h5 style="color: #3498db"><span class="numbers">4</span> <?= $this->lang->line('Ticket Rates');?></h5>
                                            <div class="col-md-6">
                                                <label class="" style="color: #3498db"><?= $this->lang->line('General Rates'); ?></label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="" style="color: #3498db"><?= $this->lang->line('special_rates'); ?></label>
                                            </div>
                                        <div class="row form-group">
                                            <?php foreach ($seat_type_price as $price): ?>
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <strong><label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Price'); ?></label></strong>
                                                    :&nbsp;<?=$price['seat_type_price']?>&nbsp;<?=$return_trip_details[0]['currency_sign']?>
                                                </div>
                                                <div class="col-md-6">
                                                    <strong><label class=""><?=$price['seat_type']?> <?= $this->lang->line('Seat Special Price'); ?></label></strong>
                                                    :&nbsp;<?=($price['special_price']=='NULL')?'0':$price['special_price']?>&nbsp;<?=$return_trip_details[0]['currency_sign']?>
                                                </div>
                                            </div>
                                            <?php endforeach ?>
                                        </div>
                                    </div>
                                </div>
                                zaid// End ticket Details
                                <br />
                                <div class="row" style="border: 1px solid #3498db">
                                    <div class="col-md-12" >
                                        <h5 style="color: #3498db"><span class="numbers">3</span> <?= $this->lang->line('operator_Details'); ?>
                                        </h5>
                                        <div class="col-md-4">
                                            <?php
                                                if($operator_details['avatar_url']!="NULL") { 
                                                    echo '<img class="img-thumbnail" src='.base_url($operator_details["avatar_url"]).' style="max-width: 200px; max-height: auto;"/>';
                                                } else { 
                                                    echo '<img class="img-thumbnail" src='.base_url("resources/default-profile.jpg").' style="max-width: 200px; max-height: auto;" />'; 
                                            } ?>
                                        </div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('f_name'); ?>:&nbsp;</strong><?=$operator_details['firstname']?></div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('l_name'); ?>:&nbsp;</strong><?=$operator_details['lastname']?></div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('lbl_email'); ?>:&nbsp;</strong><?=$operator_details['email_id']?></div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('contact_number'); ?>:&nbsp;</strong><?=$operator_details['contact_no']?></div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('company_name'); ?>:&nbsp;</strong><?=$operator_details['company_name']?></div>
                                        <div class="col-md-6" style="margin-bottom: 8px;"><strong><?= $this->lang->line('address'); ?>:&nbsp;</strong><?=$operator_details['address']?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?> -->
                <hr />
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="stat-label" style="display: inline-block;"><?= $this->lang->line('Passenger Details'); ?></h3>
                    </div>
                    <div class="col-md-3 text-right">
                        <input type="text" class="form-control" name="p_details" id="p_details" placeholder="<?=$this->lang->line('search')?>">
                    </div>
                    <div class="col-md-12 form-group">
                        <table class="table p_table" width="100%">
                            <thead>
                                <th><?= $this->lang->line('Ticket ID'); ?></th>
                                <th><?= $this->lang->line('Name'); ?></th>
                                <th><?= $this->lang->line('Total Seats'); ?></th>
                                <th><?= $this->lang->line('bus_seat_type'); ?></th>
                                <th><?= $this->lang->line('Total_Price'); ?></th>
                                <th><?= $this->lang->line('Balance Amount'); ?></th>
                                <th><?= $this->lang->line('Contact'); ?></th>
                                <th><?= $this->lang->line('Gender'); ?></th>
                                <th><?= $this->lang->line('Payment'); ?></th>
                            </thead>
                            <tbody>
                                <?php foreach ($master_booking_details as $seats) { ?>
                                <tr>
                                    <td><?=$seats['ticket_id']?></td>
                                    <td>
                                        <?php $name = $this->api->get_ticket_seat_details($seats['ticket_id']);
                                        echo $name[0]['firstname']." ".$name[0]['lastname'];
                                         if(sizeof($name)>1)
                                         {
                                            echo "<button style='color: #3498db' type='button' class='btn btn-link' data-toggle='modal' data-target='#".$name[0]['seat_id']."'>".$this->lang->line('...More')."</button>";
                                         } 
                                        ?>
                                        <div class="modal fade" id="<?=$name[0]['seat_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><?=$this->lang->line('More Seat Details')?></h4>
                                              </div>
                                              <div class="modal-body">
                                                <table class="table" width="100%">
                                                    <thead>
                                                        <th><?= $this->lang->line('Ticket ID'); ?></th>
                                                        <th><?= $this->lang->line('Name'); ?></th>
                                                        <th><?= $this->lang->line('Gender'); ?></th>
                                                        <th><?= $this->lang->line('Contact'); ?></th>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($name as $seat) { ?>
                                                        <tr>
                                                            <td><?=$seat['ticket_id']?></td>
                                                            <td><?=$seat['firstname']. ' ' .$seat['lastname']?></td>
                                                            <td><?=($seat['gender']=='M')?'Male':'Female'?></td>
                                                            <td><?=$this->api->get_country_code_by_id($seat['country_id']).$seat['mobile']?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </td>
                                    <td><?=sizeof($name)?></td>
                                    <td><?=$seats['seat_type']?></td>
                                    <td><?=$seats['ticket_price'].$seats['currency_sign']?></td>
                                    <td><?=$seats['balance_amount'].$seats['currency_sign']?></td>
                                    <td><?=$this->api->get_country_code_by_id($name[0]['country_id']).$name[0]['mobile']?></td>
                                    <td><?=($name[0]['gender']=='M')?'Male':'Female'?></td>
                                    <td>
                                        <?php if($seats['complete_paid'] == 0 && ($seats['payment_mode'] == 'online' || 'cod')) { ?>
                                    <form action="<?=base_url('user-panel-bus/bus-ticket-payment-seller')?>" method="post">
                                      <input type="hidden" name="ticket_id" value="<?=$seats['ticket_id'] ?>">
                                      <button type="submit" class="btn btn-outline btn-warning" ><i class="fa fa-check"></i> <?= $this->lang->line('complete_payment'); ?> </button>
                                    </form>
                                      <?php } ?>
                                      <?php if($seats['complete_paid'] != 0 && ($seats['payment_mode'] != 'online' || 'cod')) { ?>
                                      <button type="text" class="btn btn-success" style="color:#fff;background-color:#5cb85c;border-color: #4cae4c;" ><i class="fa fa-check"></i> <?= $this->lang->line('Payment Paid'); ?> </button>
                                      <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-6">
                            <a href="<?= $this->config->item('base_url') . 'user-panel-bus/seller-current-trips'; ?>" class="btn btn-info btn-outline " ><?= $this->lang->line('back'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function()
    {
         dataT = $('.p_table').dataTable({
            "bLengthChange": false,
            "bInfo" : false,
            "ordering": false,
            "paging": true,
            "pageLength":4 ,
            "dom": 't',
            "language": {
                "paginate": {
                "previous":'←' ,
                "next" : '→'
                    }
                },
            "dom": 'p'      
         });
        $("#p_details").keyup(function() {
            dataT.fnFilter(this.value);
        });  
    });
</script>