</style>
<section class="p-t-30 m-t-0 background-grey">

    <?php if($this->session->flashdata()): ?>
      <section class="p-t-20 p-b-0 m-t-0 ">
        <div class="container">
          <div class="row">
            <?php if($this->session->flashdata('success')): ?>
            <div role="alert" class="alert alert-success text-center col-md-6 col-md-offset-3">
              <strong><span class="ai-warning">Success</span>!</strong> <?= $this->session->flashdata('success'); ?>
            </div>
          <?php elseif($this->session->flashdata('warning')):  ?>
            <div role="alert" class="alert alert-warning text-center col-md-6 col-md-offset-3">
              <strong><span class="ai-warning">Done</span>!</strong> <?= $this->session->flashdata('warning'); ?>
            </div>
          <?php else: ?>
            <div role="alert" class="alert alert-danger text-center col-md-6 col-md-offset-3">
              <strong><span class="ai-warning">Failed</span>!</strong> <?= $this->session->flashdata('error'); ?>
            </div>
          <?php endif; ?>
          </div>
        </div>
      </section>
    <?php endif; ?>
    <?php $ticket_details = $this->api_bus->get_trip_booking_details($ticket_id); ?>
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-register-box" style="padding-top: 15px; padding-bottom: 10px;">
        <div class="hr-title hr-long center" style="margin:0px auto 5px; width: 80% !important;"><abbr><?= $this->lang->line('Payment Status'); ?></abbr> </div>
          <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Source')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?=$ticket_details['source_point']?></h6>
                  </div> 
                </div>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Destination')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?=$ticket_details['destination_point']?></h6>
                  </div> 
                </div>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Journey Date')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?=$ticket_details['journey_date']?></h6>
                  </div> 
                </div>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('no_of_seats')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?=$ticket_details['no_of_seats']?></h6>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Ticket Amount')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?=$ticket_details['ticket_price']." ".$ticket_details['currency_sign']?></h6>
                  </div>
                </div>
              <?php if($this->session->flashdata('success')): ?>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Payment Mode')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?=($ticket_details['payment_method']!='NULL')?($ticket_details['payment_method']=='stripe')?$this->lang->line('Card Payment'):$ticket_details['payment_method']:$this->lang->line('Cash at Office')?></h6>
                  </div> 
                </div>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Transaction Id')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?php if($ticket_details['transaction_id']!='NULL') { echo $ticket_details['transaction_id']; } else { echo $this->lang->line('na'); } ?></h6>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Invoice')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <?php $url = $ticket_details['invoice_url']; if($url!='NULL') { ?>
                    <a target="_blank" href='<?=base_url("resources/$url")?>'><?=$this->lang->line('Download Invoice')?></a>
                  <?php } else { ?>
                    <h6><?=$this->lang->line('na')?></h6>
                  <?php } ?>
                  </div>
                </div>
                <?php if($ticket_details['transaction_id']!='NULL') { ?>
                  <div class="col-md-12">
                    <div class="col-md-4">
                      <h6 style="color:#3498db"><?=$this->lang->line('Ticket')?> : </h6>
                    </div>
                    <div class="col-md-8">
                      <a target="_blank" href='<?=base_url("resources/ticket-pdf/".$ticket_details['ticket_id']."_ticket.pdf")?>'><?=$this->lang->line('Download/Print the ticket')?></a>
                    </div>
                  </div>
                <? } ?>
              <?php else: if($this->session->flashdata('failed')): ?>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Payment Mode')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?=$this->lang->line('Cash at Office')?></h6>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-4">
                    <h6 style="color:#3498db"><?=$this->lang->line('Transaction Id')?> : </h6>
                  </div>
                  <div class="col-md-8">
                    <h6><?=$this->lang->line('na')?></h6>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6 col-md-offset-4">
                    <button class="button orange" type="button" id="re_peyment"> <?= $this->lang->line('Pay Again'); ?> <i class="fa fa-refresh"></i></button>
                  </div>
                </div>
              <?php endif; endif; ?>
            </div>
          </div>
      </div>
    </div>
</section>

<script>
  $("#re_peyment").on('click', function(event) {
    window.location.replace("<?=base_url('ticket-payment').'/'.$ticket_id?>");
  });
</script>
