<!-- <?=json_encode($_SESSION)?> -->
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBSaP8CCJqjgZ8viUxwpMOdswi6Y04Ch0k&libraries=places"></script>
<script src="<?= $this->config->item('resource_url') . 'web/front_site/js/jquery.geocomplete.js'; ?>"></script>

<script>
  $(function(){
    $("#geocomplete").geocomplete()
      .bind("geocode:result", function(event, result){
        $.log("Result: " + result.formatted_address);
      })
      .bind("geocode:error", function(event, status){
        $.log("ERROR: " + status);
      })
      .bind("geocode:multiple", function(event, results){
        $.log("Multiple: " + results.length + " results found");
      });
    
    $("#find").click(function(){
      $("#geocomplete").trigger("geocode");
    });
    
    $("#examples a").click(function(){
      $("#geocomplete").val($(this).text()).trigger("geocode");
      return false;
    });
  });
</script>

<!-- SECTION IMAGE FULLSCREEN -->
<div class="w3-content w3-section" style="margin-top:0px !important; margin-bottom:0px !important; max-width: 100% !important;">
  <img class="mySlides" src="<?= $this->config->item('resource_url') . 'web/front_site/images/slider/Camions G.jpg'; ?>" style="width:100%">
  <img class="mySlides" src="<?= $this->config->item('resource_url') . 'web/front_site/images/slider/Camions G2.jpg'; ?>" style="width:100%">
  <img class="mySlides" src="<?= $this->config->item('resource_url') . 'web/front_site/images/slider/Moto G.jpg'; ?>" style="width:100%">
  <img class="mySlides" src="<?= $this->config->item('resource_url') . 'web/front_site/images/slider/Camions G3.jpg'; ?>" style="width:100%">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden-xs w3-display-middle w3-small w3-container" style="width: 100%;vertical-align: middle; text-align: -webkit-center;">
    <div class="text-middle text-center text-dark" id="searchDivID" style="padding-top: 100px;">
      <h1 class="" style="-webkit-text-stroke: 1px black; font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('On-demand services platform for businesses and individuals'); ?></span></span></h1>
      <p class="lead" style="font-weight: bold; color: white !important; padding-bottom: 0px;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('The best providers and freelancers at the best prices, now available safely!'); ?></span></p>

      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <h4><span style="background-color: #00000050; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('download_app'); ?></span></h4>
      </div>
      <div class="col-md-6 col-md-offset-3">
        <a style="cursor: pointer;" data-toggle="modal" data-target="#iosModal"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/apple-app-store.png'; ?>" style="" id="downapp1"></a>
        <a style="cursor: pointer;" data-toggle="modal" data-target="#androidModal"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/google-play.png'; ?>" style="" id="downapp2"></a>
      </div> 
    </div>
  </div>

  <!-- <div class="col-sm-12 hidden-xl hidden-lg hidden-md hidden-xs w3-display-middle w3-small w3-container" style="width: 100%;vertical-align: middle; text-align: -webkit-center;">
    <div class="text-middle text-center text-dark" id="searchDivID" style="padding-top: 120px;">
      <h4 class="" style="font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('On-demand services platform for businesses and individuals'); ?></span></span></h4>
      <p style="font-weight: bold; color: white !important; padding-bottom: 0px; font-size: 10px;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('The best providers and freelancers at the best prices, now available safely!'); ?></span></p>
  
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <h4><span style="background-color: #00000050; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('download_app'); ?></span></h4>
      </div>
      <div class="col-md-6 col-md-offset-3">
        <a style="cursor: pointer;" data-toggle="modal" data-target="#iosModal"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/apple-app-store.png'; ?>" style="" id="downapp1"></a>
        <a style="cursor: pointer;" data-toggle="modal" data-target="#androidModal"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/google-play.png'; ?>" style="" id="downapp2"></a>
      </div> 
    </div>
  </div> -->

  <div class="col-xs-12 hidden-xl hidden-lg hidden-md hidden-sm w3-display-middle w3-small w3-container" style="width: 100%;vertical-align: middle; text-align: -webkit-center; top: 15% !important">
    <div class="text-middle text-center text-dark" id="searchDivID" style="padding-top: 120px;">
      <h6 class="" style="font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('On-demand services platform for businesses and individuals'); ?></span></span></h6>
      <h6 style="font-weight: bold; color: white !important; padding-bottom: 0px; font-size: 10px;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('The best providers and freelancers at the best prices, now available safely!'); ?></span></h6>

      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <h6><span style="background-color: #00000050; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('download_app'); ?></span></h6>
      </div>
      <div class="col-md-6 col-md-offset-3">
        <a style="cursor: pointer;" data-toggle="modal" data-target="#iosModal"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/apple-app-store.png'; ?>" style="width: 90px; height: auto;" id="downapp1"></a>
        <a style="cursor: pointer;" data-toggle="modal" data-target="#androidModal"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/google-play.png'; ?>" style="width: 90px; height: auto;" id="downapp2"></a>
      </div> 
    </div>
  </div>

</div>

<section class="p-b-0" style="padding-top: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 20px">
      <h2><?= $this->lang->line('Offers And Promocodes'); ?></h2>
      <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>
    </div>
    <div class="container" style="padding-bottom: 20px">
      <div class="row stylish-panel">
        <?php $i=0; foreach ($promocodes as $promo){ $i++; ?>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
            <div>
              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-1" >
                  <img style="width:100px; height:70px;" src='<?=base_url($promo["promocode_avtar_url"])?>' alt="">
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-11" >
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4" >
                    <h6 style="margin: 0px 0px"> <?=$this->lang->line('Discount')?></h6>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" >
                    <h6 style="margin: 0px 0px;"><?=$promo['discount_percent']?>%</h6>
                  </div>
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding: 0px 0px;" >
                    <h4 style="color: #5dccf7;"><?=$promo['promo_code']?></h4>
                  </div>
                </div>
              </div>
            </div>    <!--<h3><?=$promo['promo_code']?></h3>-->
            <?php if($i==3 || $i==6) echo "<br/>"; ?>  
          </div>
        <?php  } ?>
      </div>
    </div>
  </div>
</section>

<!-- How it works -->
<section class="background-grey" id="howworks" style="padding-top: 5px;padding-bottom: 5px">
  <div class="container">
    <div class="heading heading-center text-center" style="margin-bottom: 10px;">
      <h2><?= $this->lang->line('how_it_works'); ?></h2>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          <div class="icon-box effect center light square">
            <div class="icon"> <a href="#"><i class="fa fa-hand-pointer-o"></i></a> </div>
            <h4 align="center"><?= $this->lang->line('Choice of service'); ?></h4>
            <p align="center"><?= $this->lang->line('Choose a service to perform and describe your need (address, desired execution date)'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          <div class="icon-box effect  center light square">
            <div class="icon"> <a href="#"><i class="fa fa-users"></i></a> </div>
            <h4><?= $this->lang->line('Best providers'); ?> </h4>
            <p align="center"><?= $this->lang->line('We and our best providers realize your request'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          <div class="icon-box effect  center light square">
            <div class="icon"> <a href="#"><i class="fa fa-clock-o"></i></a> </div>
            <h4><?= $this->lang->line('Real time monitoring'); ?></h4>
            <p align="center"><?= $this->lang->line('Follow the execution in real time (alerts, notifications ...) Mobile and web apps'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          <div class="icon-box effect  center light square">
            <div class="icon"> <a href="#"><i class="fa fa-credit-card"></i></a> </div>
            <h4><?= $this->lang->line('Payment facility'); ?></h4>
            <p align="center"><?= $this->lang->line('Payment after execution: by cash, mobile money, card'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END: How it works -->

<!-- CATEGORIES -->
<section class="p-b-0" style="padding-top: 0px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 5px;">
      <h2><?= $this->lang->line('categories'); ?></h2>
      <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>
    </div>
    <!--
    <div class="section-content">
      <div class="row text-center color-border-bottom">
        <div class="col-xs-12 col-sm-6 col-md-3 col-md-offset-3 well">
            <i class="fa fa-truck fa-5x"></i> <br /><br />
            <h5 class="ac-title">Transport (freight)</h5>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 well">
            <i class="fa fa-envelope fa-5x"></i> <br /><br />
            <h5 class="ac-title">Courier & parcel delivery</h5>
        </div>
      </div>
    </div> -->
    <style type="text/css">
      .stylish-panel {
        padding: 20px 0;
        text-align: center;
      }
      .stylish-panel > div > div{
        padding: 10px;
        border: 1px solid #afe9ff;
        border-radius: 4px;
        transition: 0.2s;
      }
      .stylish-panel > div:hover > div{
        margin-top: -10px;
        border: 1px solid rgb(200, 200, 200);
        box-shadow: #afe9ff 0px 5px 5px 2px;
        background: rgba(200, 200, 200, 0.1);
        transition: 0.5s;
      }
      .stylish-panel > div:hover img {
        border-radius: 50%;
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    </style>

    <div class="container" style="padding-bottom: 20px">
      <div class="row stylish-panel">
        <div class="col-md-4">
          <div>
            <a href="<?= base_url('sign-up') ?>">
              <i class="fa fa-truck fa-5x"></i> <br /><br />
              <h3><?= $this->lang->line('transportation'); ?></h3>
            </a>
          </div>
        </div>
        <div class="col-md-4">
          <div>
            <a href="<?= base_url('sign-up') ?>">
              <i class="fa fa-envelope fa-5x"></i> <br /><br />
              <h3><?= $this->lang->line('courier'); ?></h3>
            </a>
          </div>
        </div>
        <div class="col-md-4">
          <div>
            <a href="<?= base_url('sign-up') ?>">
              <i class="fa fa-dropbox fa-5x"></i> <br /><br />
              <h3><?= $this->lang->line('home_move'); ?></h3>
            </a>
          </div>
        <br />
        </div>
        <div class="col-md-4">
          <div>
            <a href="<?= base_url('sign-up') ?>">
              <i class="fa fa-ticket fa-5x"></i> <br /><br />
              <h3><?= $this->lang->line('Ticket Booking'); ?></h3>
            </a>
          </div>
        </div>
        <div class="col-md-4">
          <div>
            <a href="<?= base_url('sign-up') ?>">
              <i class="fa fa-recycle fa-5x"></i> <br /><br />
              <h3><?= $this->lang->line('Laundry Booking'); ?></h3>
            </a>
          </div>
        </div>
        <div class="col-md-4">
          <div>
            <a href="<?= base_url('sign-up') ?>">
              <i class="fa fa-fire fa-5x"></i> <br /><br />
              <h3><?= $this->lang->line('Gas Booking'); ?></h3>
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- <ul class="grid grid-5-columns p-t-0">
      <?php foreach ($category_type as $cat) { ?>
        <li>
          <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
              <h5 class="ac-title"> <i class="fa fa-bitbucket"></i><?= $cat['cat_type'] ?></h5>
              <div class="ac-content">
                <?php $category = $this->api->get_category_by_id($cat['cat_type_id']); ?>
                <?php foreach ($category as $c) { ?>
                  <p><a href="#"><?= $c['cat_name'] ?></a></p> 
                <?php } ?>
              </div>
            </div>
          </div>  
        </li>
      <?php } ?>
      <li>
        <div class="accordion toggle clean color-border-bottom">
          <div class="ac-item">
            <h5 class="ac-title"><i class="fa fa-cutlery"></i><?= $this->lang->line('resturant'); ?></h5>
            <div class="ac-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. </p> 
            </div>
          </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-industry"></i><?= $this->lang->line('domestic_gas'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-archive"></i><?= $this->lang->line('courier'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-truck"></i><?= $this->lang->line('transportation'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li>
    </ul> -->
  </div>
</section>
<!-- END: CATEGORIES -->

<!-- OFFERS -->
<section class="parallax text-center text-light p-t-50 p-b-50 background-overlay" style="background-image: url('<?= $this->config->item('resource_url') . 'web/front_site/images/parallax/1.jpg';?>');" data-stellar-background-ratio="0.6">
  <div class="container">
    <div class="row">
      <div class="heading heading-center m-b-20">
        <h2><?= $this->lang->line('offer1'); ?></h2>
        <span class="lead"><?= $this->lang->line('offer2'); ?></span>
      </div>
      <!--<a href="<?= base_url().'sign-up'; ?>" class="button color rounded"><span><?= $this->lang->line('sign_up'); ?></span></a>  -->
    </div>
  </div>
</section>
<!-- END: OFFERS -->

<section id="benefits" style="padding-bottom: 10px;padding-top: 10px;">
  <div class="container">
    <div class="heading heading-center m-b-15">
      <h2><?= $this->lang->line('benefit'); ?></h2>
    </div> 
    <div class="col-md-4">
      <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
        <div><h3 style="color: #5dccf7;"><?= $this->lang->line('Providers'); ?></h3></div>
        <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/carrier.jpg'; ?>" class="img-responsive"></div><br />
        <div>
          <ul type="none">
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('New customers'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Increase in sales'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Dashboards activity'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Customer relationship management'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Prepaid services'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Digitalization of your processes and flows'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Increased visibility and reputation'); ?></span></li>
          </ul>
        </div> 
      </div>
    </div>
    <div class="col-md-4">
       <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
       <div><h3 style="color: #5dccf7;"><?= $this->lang->line('Businesses'); ?></h3></div>
         <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/business.jpg'; ?>" class="img-responsive"></div><br />
           <div>
               <ul type="none">
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Attractive, transparent prices'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Online booking'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Ease, time saving, responsiveness'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Providers verified and rated'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Alerts, notifications'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Automated online management (contracts, orders, invoices)'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Reduced costs'); ?></span></li>
               </ul>
           </div>
       </div> 
    </div>
    <div class="col-md-4">
       <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
        <div><h3 style="color: #5dccf7;"><?= $this->lang->line('Individuals'); ?></h3></div>
         <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/individual.jpg'; ?>" class="img-responsive"></div><br />
           <div>
               <ul type="none">
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Instant booking online 7/7'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Safety: agencies and drivers verified and rated'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Easy payment (mobile money, cash)'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Low prices'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Ease, time saving'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Alerts, notifications'); ?></span></li>
                 <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Mobile, web, simple apps'); ?></span></li>
               </ul>
           </div>
       </div> 
    </div>
    <div class="col-md-12 text-center">
      <a href="<?= base_url().'sign-up'; ?>" class="button color rounded"><span><?= $this->lang->line('sign_up'); ?></span></a>
    </div>
  </div>
</section>

<!-- CLIENTS -->
<section class="p-t-0" style="padding-bottom: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 10px;">
      <h2><?= $this->lang->line('clients'); ?></h2>
      <span class="lead"></span>
    </div>
    <div class="carousel" data-lightbox-type="gallery" data-carousel-col="3">
      <div class="featured-box">
        <div class="image-box circle-image small">
          <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/1.jpg'; ?>" class="">
        </div>
        <div class="image-box-description text-center">
          <h4><?= $this->lang->line('name_1'); ?></h4>
          <p class="subtitle"><?= $this->lang->line('sub_title_1'); ?></p>
          <hr class="line">
          <div><?= $this->lang->line('description1'); ?></div>
        </div>
      </div>
      <div class="featured-box">
        <div class="image-box circle-image small">
          <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/2.jpg'; ?>" alt="">
        </div>
        <div class="image-box-description text-center">
          <h4><?= $this->lang->line('name_2'); ?></h4>
          <p class="subtitle"><?= $this->lang->line('sub_title_2'); ?></p>
          <hr class="line">
          <div><?= $this->lang->line('description2'); ?></div>
        </div>
      </div>
      <div class="featured-box">
        <div class="image-box circle-image small">
          <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/3.jpg'; ?>">
        </div>
        <div class="image-box-description text-center ">
          <h4><?= $this->lang->line('name_3'); ?></h4>
          <p class="subtitle"><?= $this->lang->line('sub_title_3'); ?></p>
          <hr class="line">
          <div><?= $this->lang->line('description3'); ?></div>
        </div>
      </div>
      <div class="featured-box">
        <div class="image-box circle-image small">
          <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/4.jpg'; ?>">
        </div>
        <div class="image-box-description text-center">
          <h4><?= $this->lang->line('name_4'); ?></h4>
          <p class="subtitle"><?= $this->lang->line('sub_title_4'); ?></p>
          <hr class="line">
          <div><?= $this->lang->line('description4'); ?></div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- CLIENTS -->

<section class="hidden">
  <div class="container">
   <div class="col-md-12">
      <div class="col-md-3 text-center">
        <h4 class="widget-title"><?= $this->lang->line('they_talk_about_us'); ?> -</h4>
      </div>  
      <div class="col-md-9">
        <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/news.png'; ?>" style="width: 100%;">
      </div>
    </div>
  </div>
</section>

<!--<section class="p-b-0" style="padding-top: 10px;">-->
<!--    <div class="container">-->
<!--        <div class="heading heading-center" style="margin-bottom: 20px">-->
<!--            <h2><?= $this->lang->line('Offers And Promocodes'); ?></h2>-->
<!--            <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>-->
<!--        </div>-->
<!--        <div class="container" style="padding-bottom: 20px">-->
<!--            <div class="row stylish-panel">-->
<!--                <?php $i=0; foreach ($promocodes as $promo){ $i++; ?>-->
<!--                    <div class="col-md-4">-->
<!--                        <div>-->
<!--                            <div class="row">-->
<!--                                <div class="col-md-5" style="padding-right: 0px; padding-left:0px">-->
<!--                                    <img style="width:120px; height:120px;" src='<?=base_url($promo["promocode_avtar_url"])?>' alt="">-->
<!--                                </div>-->
<!--                                <div class="col-md-7" style="padding-right: 0px; padding-left:0px">-->
<!--                                    <div class="col-md-6 text-left" style="padding-right: 0px; padding-left:0px">-->
<!--                                        <h6 style="color: #5dccf7; margin: 0px 0px"> <?=$this->lang->line('Discount')?></h6>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6 text-left" style="padding-right: 0px; padding-left:0px">-->
<!--                                        <h6 style="margin: 0px 0px"><?=$promo['discount_percent']?>%</h6>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6 text-left" style="padding-right: 0px; padding-left:0px">-->
<!--                                        <h6 style="color: #5dccf7; margin: 0px 0px"> <?=$this->lang->line('Start Date')?></h6>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6 text-left" style="padding-right: 0px; padding-left:0px">-->
<!--                                        <h6 style="margin: 0px 0px"><?=$promo['code_start_date']?></h6>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6 text-left" style="padding-right: 0px; padding-left:0px">-->
<!--                                        <h6 style="color: #5dccf7; margin: 0px 0px"> <?=$this->lang->line('expiry_date')?></h6>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6 text-left" style="padding-right: 0px; padding-left:0px">-->
<!--                                        <h6 style="margin: 0px 0px"><?=$promo['code_expire_date']?></h6>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-12 text-left" style="padding-right: 0px; padding-left:0px">-->
<!--                                        <h3><?=$promo['promo_code']?></h3>-->
<!--                                    </div>  -->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                      <?php if($i==3 || $i==6) echo "<br/>"; ?>  -->
<!--                    </div>-->
<!--                <?php  } ?>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
