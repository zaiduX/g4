<style type="text/css">
  .table > tbody > tr > td {
      border-top: none;
  }
  .dataTables_filter {
      display: none;
  }
</style>

<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>
            
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?= $this->config->item('base_url') . 'user-panel/dashboard-bus'; ?>"><?= $this->lang->line('dash'); ?></a></li>
                    <li class="active"><span><?= $this->lang->line('Search Ticket'); ?></span></li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs"> <i class="fa fa-search fa-2x text-muted"></i> <?= $this->lang->line('Search Ticket'); ?></h2>
            <small class="m-t-md"><?= $this->lang->line('Search ticket and book your seat'); ?></small> 
        </div>
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="hpanel">
                <div class="panel-body">
      
                  <?php if($this->session->flashdata('error')):  ?>
                      <div class="row">
                        <div class="form-group"> 
                          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                        </div>
                      </div>
                  <?php endif; ?>
                  <?php if($this->session->flashdata('success')):  ?>
                      <div class="row">
                        <div class="form-group"> 
                          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                        </div>
                      </div>
                  <?php endif; ?>
                  <div class="row" id="basicSearch" style="<?= $filter == 'advance' ? 'display:none' : '' ?>;">
                    <form action="<?= base_url('user-panel-bus/bus-trip-search-result-buyer') ?>" method="post" id="frmBasicSearch"> 
                      <div class="row form-group">
                        <div class="col-md-12">
                          <div class="col-md-8 col-md-offset-2 text-center">
                            <h3 style="color: #3498db;"><?= $this->lang->line('Your favourite seat at the best prices, BOOK NOW!'); ?></h3>
                          </div>
                        </div>
                      </div> 
                      <div class="row form-group">
                        <div class="col-md-12">
                          <div class="col-md-4 col-md-offset-2">
                            <label class="text-left"><i class="fa fa-arrow-up"></i> <?= $this->lang->line('From'); ?></label>
                            <select class="form-control select2" name="trip_source" id="trip_source" style="box-shadow: 0 0 5px #3498db;">
                              <option value=""><?= $this->lang->line('Starting city'); ?></option>
                              <?php foreach ($trip_sources as $source) { ?>
                              <option value="<?= $source['trip_source'] ?>"><?= $source['trip_source'] ?></option>
                              <?php } ?>
                            </select><br />
                          </div>
                          <div class="col-md-4">
                            <label class="text-left"><i class="fa fa-arrow-down"></i> <?= $this->lang->line('To'); ?></label>
                            <select class="form-control select2" name="trip_destination" id="trip_destination" style="box-shadow: 0 0 5px #3498db;">
                                <option value=""><?= $this->lang->line('Destination city'); ?></option>
                                <?php foreach ($trip_destinations as $destination) { ?>
                                    <option value="<?= $destination['trip_destination'] ?>"><?= $destination['trip_destination'] ?></option>
                                <?php } ?>
                            </select><br />
                          </div>
                          <div class="col-md-2">
                            &nbsp;
                          </div>
                        </div>
                      </div>
                      <div class="row form-group">
                        <div class="col-md-12">
                          <div class="col-md-4 col-md-offset-2">
                            <label class="text-left"><i class="fa fa-calendar"></i> <?= $this->lang->line('Journey Date'); ?></label>
                            <div class="input-group date" data-provide="datepicker" data-date-start-date="0d" data-date-end-date="" style="box-shadow: 0 0 5px #3498db;">
                              <input type="text" class="form-control" id="journey_date" name="journey_date" autocomplete="none" />
                              <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </div>
                            </div> 
                          </div>
                          <div class="col-md-4">
                            <label class="text-left"><i class="fa fa-calendar"></i> <?= $this->lang->line('Return Date'); ?> <small>(<?= $this->lang->line('Optional'); ?>)</small></label>
                            <div class="input-group date" data-provide="datepicker" data-date-start-date="0d" data-date-end-date="" style="box-shadow: 0 0 5px #3498db;">
                              <input type="text" class="form-control" id="return_date" name="return_date" autocomplete="none" />
                              <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </div>
                            </div> 
                          </div>
                          <div class="col-md-2">
                            &nbsp;
                          </div>
                        </div>
                      </div>
                      <div class="row form-group">
                        <div class="col-md-12">
                          <div class="col-md-8 col-md-offset-2">
                            <label class="text-left"><i class="fa fa-users"></i> <?= $this->lang->line('number of seat'); ?></label>
                            <div class="input-group" style="box-shadow: 0 0 5px #3498db;">
                              <input type="number" class="form-control" id="no_of_seat" name="no_of_seat" autocomplete="none" />
                              <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-user"></span>
                              </div>
                            </div>
                          </div>   
                        </div>
                      </div>
                      <div class="row form-group">
                        <div class="col-md-12">
                          <div class="col-md-8 col-md-offset-2">
                            <br />
                            <input type="submit" class="btn btn-success form-control" value="<i class='fa fa-search'></i> <?= $this->lang->line('Search Trip'); ?>" style="min-height: 40px; box-shadow: 0 0 5px #225595;" >
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>

<script>
  $(function() {
    $('#datepicker').datepicker();
    // $("#journey_date").on("change", function(event) {
    //   var journey_date = $('#journey_date').val();
    //   $("#return_date").datepicker("update", journey_date);
    // });
  });
</script>

<script>
  $("#frmBasicSearch")
    .validate({
      ignore: [], 
      rules: {
        trip_source: { required : true },
        trip_destination: { required : true },
        journey_date: { required : true },
        no_of_seat: { required : true },
      },
      messages: {
        trip_source: { required : <?= json_encode($this->lang->line('Select starting city!')); ?> },
        trip_destination: { required : <?= json_encode($this->lang->line('Select destination city!')); ?> },
        journey_date: { required : <?= json_encode($this->lang->line('Select journey date!')); ?> },
        no_of_seat: { required : <?= json_encode($this->lang->line('select number of seats!')); ?> },
      },
  });
</script>