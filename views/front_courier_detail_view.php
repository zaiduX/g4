<section style="margin-top: -50px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-register-box">
                            <div class="hr-title hr-long center"><abbr>Courier Detail</abbr> </div>
                            <div class="panel-body">
                                <div class="pull-right text-right">
                                    <small class="stat-label">Ref# G-015</small>
                                </div>
                                <h4 class="m-l-20"><strong>FROM:</strong> From Address</h4>
                                <h4 class="m-l-20"><strong>TO:</strong> To Address</h4>
                                <p class="small">
                                    <div class="col-md-6"><strong>Weight:</strong> 250 KG</div>
                                    <div class="col-md-6"><strong>Price:</strong> $ 300</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Advance payment :</strong> $ 200</div>
                                    <div class="col-md-6"><strong>Balance payment :</strong> $ 100</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>By:</strong> Avion</div>
                                    <div class="col-md-6"><strong>Dimension:</strong> Wallet, book (document)</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Pickup Date:</strong> 23 Sept. 2017 02:05:00 PM</div>
                                    <div class="col-md-6"><strong> Delivery Date:</strong> 28 Sept. 2017 02:05:00 PM</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Shipping Type:</strong> Shipping Only</div>
                                    <div class="col-md-6"><strong>Expiration Date:</strong> 28 Sept. 2017 02:05:00 PM</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-6"><strong>Reciever Company Name:</strong> XYZ Pvt. Ltd</div>
                                    <div class="col-md-6"><strong>Reciever Name:</strong> Mr. Tabani</div>
                                </p>
                                <p class="small">
                                    <div class="col-md-12"><strong>Description:</strong> This is courier description. This is courier description. This is courier description. This is courier description. This is courier description. This is courier description. </div>
                                </p>
                                <p class="small">
                                    <div class="col-md-12"><strong>Pick-up Instructions:</strong> This is pickup instructions. This is pickup instructions. This is pickup instructions. This is pickup instructions. This is pickup instructions. This is pickup instructions. </div>
                                </p>
                                <p class="small">
                                    <div class="col-md-12"><strong>Delivere Instructions:</strong> This is delivere instructions. This is delivere instructions. This is delivere instructions. This is delivere instructions. This is delivere instructions. This is delivere instructions. </div>
                                </p>
                                <p class="small">
                                    <div class="col-md-12"><strong>Ref. No.:</strong> 123456789ASDC123</div>
                                </p> 
                                <p class="text-right">
                                    <a class="button blue-dark full-rounded small" href="http://nooridev.in/hapush/gonagoo/sign-up"> Apply </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>