<!-- Career -->
<section class="background-grey" style="padding-top: 0px;padding-bottom: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 30px;">
      <h2 class="text-center"><span class="text-info"><?= $this->lang->line('careers'); ?></span></h2>
    </div>
    <div class="row">
      <div class="col-md-6" style="border-right: solid #225595 1px">
        <h2 style="margin-bottom: 10px;"><?= $this->lang->line('We are looking for the following profiles'); ?></h2>
        <ul type="none">
          <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Commercial'); ?></span></li>
          <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Marketing and web marketing'); ?></span></li>
          <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Community and social media manager'); ?></span></li>
          <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Web developers'); ?></span></li>
          <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('UI/UX Designers'); ?></span></li>
          <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Agency manager'); ?></span></li>
        </ul>
      </div>

      <div class="col-md-6">
        <h2 style="margin-bottom: 10px;"><?= $this->lang->line('Send us your resume!'); ?></h2>
        <form action="<?=base_url('send-resume')?>" method="POST" enctype="multipart/form-data" id="frmSendResume">
          <div class="col-md-7" style="margin-bottom: -15px;">
            <input type="text" name="applicant_name" id="applicant_name" class="form-control" placeholder="<?=$this->lang->line('Enter your name')?>" autocomplete="off" /> <br />
          </div>
          <div class="col-md-5">
            <label for="files" class="btn btn-info"><?= $this->lang->line('Select resume'); ?></label><br />
            <small style="color: red;"><?= $this->lang->line('allowed .pdf/.doc/.docx only'); ?></small>
          </div>
          <div class="col-md-12">
            <h5 for="filename" id="filename"></h5>
          </div>
          <div class="col-md-4">
            <button type="submit" name="uploadresume" class="btn btn-success hidden" id="uploadresume"><?= $this->lang->line('Send resume'); ?></button>
          </div>

          <?php if($this->session->flashdata('error')):  ?>
            <div class="col-md-12">
              <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            </div>
          <?php endif; ?>
          <?php if($this->session->flashdata('success')):  ?>
            <div class="col-md-12"> 
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            </div>
          <?php endif; ?>

          <input type="file" name="files" id="files" style="visibility:hidden;" accept=".pdf,.doc,.docx,.PDF,.DOC,.DOCX" />
        </form>
      </div>
      <script>
        $("#files").change(function() {
          $('#filename').text("");
          filename = this.files[0].name;
          $('#filename').text("<?=$this->lang->line('File attached')?>: "+filename);
          $('#uploadresume').removeClass('hidden');
        });

        $( "#uploadresume" ).click(function( e ) {
          e.preventDefault();
          var applicant_name = $('#applicant_name').val();
          var files = $('#files').val();
          var name_len = applicant_name.length;

          if(applicant_name == '' || applicant_name == undefined) {
              swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Enter your name')?>", "error");
          } else if(name_len <= 3) {
              swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Minimum 4 characters required')?>", "error");
          } else if(files == '' || files == undefined) {
              swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select resume')?>", "error");
          } else { $("#frmSendResume").submit(); }
        });

        $(".alert").fadeTo(5000, 500).slideUp(500, function(){
          $(".alert").slideUp(1000);
        });

      </script>
    </div>
  </div>
</section>
<!-- END: Career -->