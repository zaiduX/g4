<!-- About Us -->
<section class="background-grey" style="padding-bottom: 10px;padding-top: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 0px;">
      <h2 class="text-center"><span class="text-info"><?= $this->lang->line('about_us'); ?></span></h2>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h2 style="margin-bottom: 10px;"><?= $this->lang->line('our_vision'); ?></h2>
        <span><?= $this->lang->line('about_line1'); ?><br />
              <?= $this->lang->line('about_line2'); ?><br />
              <?= $this->lang->line('about_line3'); ?><br /><br />
              <strong><?= $this->lang->line('about_line4'); ?></strong><br />
              <?= $this->lang->line('about_line5'); ?><br />
              <?= $this->lang->line('about_line6'); ?><br />
              <?= $this->lang->line('about_line7'); ?><br /><br />
              <strong><?= $this->lang->line('about_line8'); ?></strong><br />
              <?= $this->lang->line('about_line9'); ?><br />
              <?= $this->lang->line('about_line10'); ?>
        </span>
      </div>
    </div>
  </div>
</section>
<!-- END: About Us -->
<!-- Our Values -->
<section style="padding-top: 10px;padding-bottom: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 20px;">
      <h2><span class="text-info"><?= $this->lang->line('our_values'); ?></span></h2>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="icon-box effect medium border center">
          <div class="icon">
            <a href="#"><i class="fa fa-lock"></i></a>
          </div>
          <h3><?= $this->lang->line('security_and_reliability'); ?></h3>
        </div>
      </div>
      <div class="col-md-4">
        <div class="icon-box effect medium border center">
          <div class="icon">
            <a href="#"><i class="fa fa-cogs"></i></a>
          </div>
          <h3><?= $this->lang->line('quality_of_service'); ?></h3>
        </div>
      </div>
      <div class="col-md-4">
        <div class="icon-box effect medium border center">
          <div class="icon">
            <a href="#"><i class="fa fa-calendar-check-o"></i></a>
          </div>
          <h3><?= $this->lang->line('respect_of_the_deadline'); ?></h3>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END: Our Values -->
<!-- Our Team -->
<section class="background-grey hidden">
  <div class="container">
    <div class="heading heading-center">
      <h2><span class="text-info"><?= $this->lang->line('our_team'); ?></span></h2>
      <span class="lead" style="max-width: 1280px;"><?= $this->lang->line('team_line_1'); ?></span>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="image-box circle-image small">
          <img class="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/bertrand.png'; ?>" alt="Profile Picture">
        </div>
        <div class="image-box-description text-center">
          <h4><?= $this->lang->line('team_line_3'); ?></h4>
          <p class="subtitle"><?= $this->lang->line('team_line_5'); ?><br /><?= $this->lang->line('team_line_6'); ?><br /><?= $this->lang->line('team_line_7'); ?></p>
          <hr class="line">
          <div><?= $this->lang->line('team_line_8'); ?></div>
          <div class="social-icons social-icons-border m-t-10 text-center hidden">
              <ul>
                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="image-box circle-image small">
          <img class="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/didier.png'; ?>" alt="Profile Picture">
        </div>
        <div class="image-box-description text-center">
          <h4><?= $this->lang->line('team_line_9'); ?></h4>
          <p class="subtitle"><?= $this->lang->line('team_line_10'); ?><br /><?= $this->lang->line('team_line_11'); ?><br /><?= $this->lang->line('team_line_12'); ?></p>
          <hr class="line">
          <div><?= $this->lang->line('team_line_13'); ?></div>
          <div class="social-icons social-icons-border m-t-10 text-center hidden">
              <ul>
                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="image-box circle-image small">
          <img class="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/mamadou.png'; ?>" alt="Profile Picture">
        </div>
        <div class="image-box-description text-center">
          <h4><?= $this->lang->line('team_line_14'); ?></h4>
          <p class="subtitle"><?= $this->lang->line('team_line_15'); ?><br /><?= $this->lang->line('team_line_16'); ?><br /><?= $this->lang->line('team_line_17'); ?></p>
          <hr class="line">
          <div><?= $this->lang->line('team_line_18'); ?></div>
          <div class="social-icons social-icons-border m-t-10 text-center hidden">
              <ul>
                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END: Our VTeam -->