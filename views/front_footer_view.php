    <!-- FOOTER -->
    <footer class="background-dark text-grey" id="footer">
        <div class="footer-content" style="padding: 0px !important; margin-bottom: -10px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget clearfix widget-categories">
                            <h4 class="widget-title"><?= $this->lang->line('in_complete_confidence'); ?></h4>
                            <p><span><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/secure.png'; ?>" style="width: 30px" ></span> <?= $this->lang->line('secure_payment'); ?></p>
                            <p><span><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/insurance.png'; ?>" style="width: 25px;"></span> <?= $this->lang->line('insurance'); ?></p>
                            <p><span><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/24service.png'; ?>" style="width: 25px"></span> <?= $this->lang->line('assistance'); ?></p>  
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="widget clearfix widget-categories">
                            <h4 class="widget-title"><?= $this->lang->line('company_info'); ?></h4>
                            <ul class="list list-arrow-icons">
                                <li class="foot-list"> <a href="<?= base_url('about-us') ?>" title=""><?= $this->lang->line('about_us'); ?> </a> </li>
                                <li class="foot-list"> <a href="<?= base_url('careers'); ?>" title=""><?= $this->lang->line('careers'); ?> </a> </li>
                                <li class="foot-list"> <a href="<?= base_url('page-not-found'); ?>" title=""><?= $this->lang->line('press'); ?> </a> </li>
                                <li class="foot-list"> <a href="<?= base_url('terms-conditions') ?>" title=""><?= $this->lang->line('terms_of_service'); ?> </a> </li>
                                <li class="foot-list"> <a href="<?= base_url('terms-conditions') ?>" title=""><?= $this->lang->line('privacy_policy'); ?></a> </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="widget clearfix widget-contact-us-form">
                            <h4 class="widget-title"><?= $this->lang->line('download_our_mobile_application'); ?></h4>
                            <p><a style="cursor: pointer;" data-toggle="modal" data-target="#iosModal"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/apple-app-store.png'; ?>" style="width: 110px;"></a></p>
                            <p><a style="cursor: pointer;" data-toggle="modal" data-target="#androidModal"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/google-play.png'; ?>" style="width: 110px;"></a></p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="widget widget-categories">
                            <h4 class="widget-title"><i class="fa fa-map-marker"></i> <?= $this->lang->line('geo_address'); ?></h4>
                            <ul class="list list-icons" style="line-height: 30px;">
                                <li><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/france.png'; ?>" style="max-width:32px" /> &nbsp; <?=$this->lang->line('Europe');?></li>
                                <li><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/cameroon.png'; ?>" style="max-width:32px" /> &nbsp; <?=$this->lang->line('Africa');?> </li>
                                <li><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/envelope.png'; ?>" style="max-width:32px" /> &nbsp; <a href="mailto:contact@gonagoo.com">contact@gonagoo.com</a></li>
                                <li>
                                    <a href="https://www.facebook.com/gonagoocm" target="_blank"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/facebook.png'; ?>" class="sc-icons" style="height: 36px; width: 36px;margin-right:10px;"></a>
                                    <a href="" target="_blank"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/google.png'; ?>" class="sc-icons" style="height: 36px; width: 36px;margin-right:10px;"></a>
                                    <a href="https://twitter.com/Gonagoocm" target="_blank"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/twitter.png'; ?>" class="sc-icons" style="height: 36px; width: 36px;margin-right:10px;"></a>
                                    <a href="https://www.linkedin.com/company/gonagoo" target="_blank"><img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/linkedin.png'; ?>" class="sc-icons" style="height: 36px; width: 36px;margin-right:10px;"></a>
                                </li>    
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-content m-t-0" style="padding: 0px 0px 10px 0px !important; min-height: auto; ">
            <div class="container">
                <div class="row">
                    <div class="copyright-text text-center"> &copy; <?= date('Y') ?> <?=$this->lang->line('Servicesgoo')?>  <a target="_blank" href="gonagoo.com"></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END: FOOTER -->
</div>
<!-- END: WRAPPER -->

<!-- GO TOP BUTTON -->
<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>
<!--Jquery Validation-->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="<?= $this->config->item('resource_url') . 'web/front_site/js/validation.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/datatables/media/js/jquery.dataTables.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js'; ?>"></script>
<!-- Theme Base, Components and Settings -->
<script src="<?= $this->config->item('resource_url') . 'web/front_site/js/theme-functions.js'; ?>"></script>
<!-- Custom js file -->
<script src="<?= $this->config->item('resource_url') . 'web/front_site/js/custom.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/sweetalert/lib/sweet-alert.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'; ?>"></script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/vendor/clockpicker/dist/bootstrap-clockpicker.min.js'; ?>"></script>

<!-- LinkedIn -->
<script type="text/javascript" src="//platform.linkedin.com/in.js">
  api_key: 81z58kc2qzh901
  authorize: true
</script>
<script>
    function LinkedINAuth(){ IN.User.authorize(function(){ callback(); }); }
    function getProfileData() { IN.API.Raw("/people/~:(id,picture-url,email-address,first-name,last-name)?format=json").result(onSuccess).error(onError); }
    function getProfileData_login() { IN.API.Raw("/people/~:(id,picture-url,email-address,first-name,last-name)?format=json").result(onSuccess_login).error(onError); }
    function linkedInLogout(){  IN.User.logout();   return true;    }
</script>
<!-- end- Linkedin -->
<!-- facebook -->
<script>
    window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
      appId      : '639931682797482', // FB App ID
      cookie     : false,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });
    };

    // Load the JavaScript SDK asynchronously
    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- end - facebook -->
<script>
    $('#tableDataBooking').dataTable({
    "deferRender": true,
    "bFilter": false,
    "bLengthChange": false,
    "bInfo": false,
    "pageLength": 5,
});
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        /*var select2Options = { width: 'resolve' };    
        $('select').select2(select2Options);*/
        $('select').select2({dropdownAutoWidth : false, width: '100%'});
    });

    /*var myIndex = 0;
    carousel();

    function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
           x[i].style.display = "none";  
        }
        myIndex++;
        if (myIndex > x.length) {myIndex = 1}    
        x[myIndex-1].style.display = "block";  
        setTimeout(carousel, 3000); // Change image every 2 seconds
    }*/
</script>

<!-- Android App Get -->
<div class="modal fade" id="androidModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= $this->lang->line('Get Gonagoo App on your Mobile'); ?></h4>
        </div>
        <form action="" method="post" id="getAnroidApp">
            <div class="modal-body">
              <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="col-md-6">
                            <label class="form-label"><?= $this->lang->line('Select Country'); ?></label>
                            <select class="form-control select2" name="country_id" id="country_id_android" style="width: 100%;">
                                <option value=""><?= $this->lang->line('Select Country'); ?></option>
                                <?php foreach ($countries as $c) :?>
                                    <option value="<?= $c['country_id'];?>"><?=$c['country_name'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label"><?= $this->lang->line('enter_mobile_number'); ?></label>
                            <input type="text" name="phone_no" id="phone_no_android" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number'); ?>">
                        </div>
                    </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
              <button type="submit" class="btn btn-info" id="btn_submit_android"><?= $this->lang->line('Get Now'); ?></button>
            </div>
        </form>
      </div>
    </div>
</div>
<script>
    $(function(){
        $("#getAnroidApp").submit(function(e) { e.preventDefault(); });
        $("#btn_submit_android").on('click', function(e) { e.preventDefault();
            var country_id = $("#country_id_android").val();
            var phone_no = $("#phone_no_android").val();

            if(country_id <= 0 ) {  swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('Select Country'); ?>","warning");   } 
            else if(!phone_no) {  swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('enter_mobile_number'); ?>","warning");   } 
            else if(isNaN(phone_no)) {  swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('numbers_only'); ?>","warning");   } 
            else {  
                $.ajax({
                    type: "POST", 
                    url: "send-android-app-link", 
                    data: { country_id:country_id, phone_no:phone_no },
                    dataType: "text",
                    success: function(res) {   
                        swal("<?= $this->lang->line('success'); ?>","<?= $this->lang->line('App link sent to your Mobile!'); ?>","success");
                        $('#androidModal').modal('hide');
                    },
                    beforeSend: function() { },
                    error: function() { }
                });
            }
        });
    });
</script>
<!-- iOS App Get -->
<div class="modal fade" id="iosModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= $this->lang->line('Get Gonagoo App on your Mobile'); ?></h4>
        </div>
        <form action="" method="post" id="getIosApp">
            <div class="modal-body">
              <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="col-md-6">
                            <label class="form-label"><?= $this->lang->line('Select Country'); ?></label>
                            <select class="form-control select2" name="country_id_ios" id="country_id_ios" style="width: 100%;">
                                <option value=""><?= $this->lang->line('Select Country'); ?></option>
                                <?php foreach ($countries as $c) :?>
                                    <option value="<?= $c['country_id'];?>"><?=$c['country_name'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label"><?= $this->lang->line('enter_mobile_number'); ?></label>
                            <input type="text" name="phone_no_ios" id="phone_no_ios" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number'); ?>">
                        </div>
                    </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
              <button type="submit" class="btn btn-info" id="btn_submit_ios"><?= $this->lang->line('Get Now'); ?></button>
            </div>
        </form>
      </div>
    </div>
</div>
<script>
    $(function(){
        $("#getIosApp").submit(function(e) { e.preventDefault(); });
        $("#btn_submit_ios").on('click', function(e) { e.preventDefault();
            var country_id_ios = $("#country_id_ios").val();
            var phone_no_ios = $("#phone_no_ios").val();

            if(country_id_ios <= 0 ) {  swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('Select Country'); ?>","warning");   } 
            else if(!phone_no_ios) {  swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('enter_mobile_number'); ?>","warning");   } 
            else if(isNaN(phone_no_ios)) {  swal("<?= $this->lang->line('error'); ?>","<?= $this->lang->line('numbers_only'); ?>","warning");   } 
            else {  
                $.ajax({
                    type: "POST", 
                    url: "send-ios-app-link", 
                    data: { country_id:country_id_ios, phone_no:phone_no_ios },
                    dataType: "text",
                    success: function(res) {   
                        swal("<?= $this->lang->line('success'); ?>","<?= $this->lang->line('App link sent to your Mobile!'); ?>","success");
                        $('#iosModal').modal('hide');
                    },
                    beforeSend: function() { },
                    error: function() { }
                });
            }
        });
    });
</script>
<!-- END: SECTION IMAGE FULLSCREEN -->


</body>
</html>