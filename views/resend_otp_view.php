<section class="p-t-40 m-t-0 background-grey" style="background:url(<?=$this->config->item('resource_url').'web/front_site/images/bg.jpg'; ?>);">
	
	<section class="p-t-20 p-b-0 m-b-10 ">
		<?php if(($this->session->flashdata('error') != NULL) OR ( $this->session->flashdata('success') != NULL) ): ?>
			<div class="container">
				<div class="row">
					<?php if($this->session->flashdata('error')): ?>
					<div role="alert" class="alert alert-danger text-center col-md-8 col-md-offset-2">
						<strong><span class="ai-warning">Failed</span>!</strong> <?= $this->session->flashdata('error'); ?>
					</div>
				<?php elseif($this->session->flashdata('success')): ?>
					<div role="alert" class="alert alert-success text-center col-md-8 col-md-offset-2">
						<strong><span class="ai-warning">Success</span>!</strong> <?= $this->session->flashdata('success'); ?>
					</div>
				<?php endif; ?>

				</div>
			</div>
		<?php endif; ?>		
		
	</section>

	<div class="container">
	  <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-register-box"><br />
					<!--Login Form-->
					<div class="hr-title hr-long center">
						<abbr><?= $this->lang->line('btn_resend_otp'); ?></abbr> 
					</div>
					<div class="p-10 text-center">
					<span class="text-center"><?= $this->lang->line('txt_resend_otp'); ?></span>
						
					</div>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<form id="resend_otp_form" method="post" action="<?= base_url('resend-otp');?>" autocomplete="off">
							  <div class="row">
	                <div class="col-md-12">
										<div class="form-group">
											<label class="sr-only"><?= $this->lang->line('mobile_no'); ?></label>
											<input type="text" class="form-control required" placeholder="<?= $this->lang->line('mobile_no'); ?>" name="mobile_no" autofocus required />
										</div>
									</div>
							  </div>
								
								<div class="row">
									<div class="col-md-6 col-sm-6 text-left">
										<button type="submit" class="button blue-dark"> <?= $this->lang->line('btn_send_otp'); ?></button>
									</div>									
								</div> 

							</form>
						</div>
					</div>
					<br />
				</div>	
			</div>
		</div>		
	</div>
</section>