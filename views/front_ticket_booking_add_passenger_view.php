</style>
<section class="p-t-30 m-t-0 background-grey" style="background: url(<?= $this->config->item('resource_url') . 'web/front_site/images/bg.jpg'; ?>);">

	<?php if($this->session->flashdata()): ?>
		<section class="p-t-20 p-b-0 m-t-0 ">
			<div class="container">
				<div class="row">
					<?php if($this->session->flashdata('success')): ?>
					<div role="alert" class="alert alert-success text-center col-md-6 col-md-offset-3">
						<strong><span class="ai-warning">Success</span>!</strong> <?= $this->session->flashdata('success'); ?>
					</div>
				<?php elseif($this->session->flashdata('warning')):  ?>
					<div role="alert" class="alert alert-warning text-center col-md-6 col-md-offset-3">
						<strong><span class="ai-warning">Done</span>!</strong> <?= $this->session->flashdata('warning'); ?>
					</div>
				<?php else: ?>
					<div role="alert" class="alert alert-danger text-center col-md-6 col-md-offset-3">
						<strong><span class="ai-warning">Failed</span>!</strong> <?= $this->session->flashdata('error'); ?>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</section>
	<?php endif; ?>

	<div class="container" style="min-width: 100%">
		<div class="row">
      <div class="col-md-12">
        <div class="panel panel-register-box" style="padding-top: 15px; padding-bottom: 10px;">
          <div class="hr-title hr-long center" style="margin:10px auto 5px; width: 80% !important;"><abbr><?= $this->lang->line('Passengers Details'); ?></abbr> </div>
            <div class="row" style="margin-top: -15px;">
              <div class="col-md-12" style="padding-left: 35px; padding-right: 35px">
                <form method="GET" action="<?=base_url('ticket-booking')?>" id="add_passengers_details" autocomplete="off">
                	<input type="hidden" name="firstname0" value="<?=$_GET['firstname0']?>">
                	<input type="hidden" name="lastname0" value="<?=$_GET['lastname0']?>">
                	<input type="hidden" name="email_id0" value="<?=$_GET['email_id0']?>">
                	<input type="hidden" name="mobile0" value="<?=$_GET['mobile0']?>">
                	<input type="hidden" name="gender0" value="<?=$_GET['gender0']?>">
                	<input type="hidden" name="age0" value="<?=$_GET['age0']?>">
                	<input type="hidden" name="country_id0" value="<?=$_GET['country_id0']?>">
                	<input type="hidden" name="journey_date" value="<?=$_GET['journey_date']?>">
                	<input type="hidden" name="return_date" value="<?=$_GET['return_date']?>">
                	<input type="hidden" name="ownward_trip" value="<?=$_GET['ownward_trip']?>">
                	<input type="hidden" name="no_of_seat" value="<?=$_GET['no_of_seat']?>">
                	<input type="hidden" name="seat_price" value="<?=$_GET['seat_price']?>">
                	<input type="hidden" name="seat_type" value="<?=$_GET['seat_type']?>">
                	<input type="hidden" name="pickup_point" value="<?=$_GET['pickup_point']?>">
                	<input type="hidden" name="drop_point" value="<?=$_GET['drop_point']?>">
                	<input type="hidden" name="trip_source" value="<?=$_GET['trip_source']?>">
                	<input type="hidden" name="trip_destination" value="<?=$_GET['trip_destination']?>">
                	<input type="hidden" name="distance" value="<?=$_GET['distance']?>">
                	<input type="hidden" name="duration" value="<?=$_GET['duration']?>">
                	<input type="hidden" name="ownward_currency_sign" value="<?=$_GET['ownward_currency_sign']?>">
                	<input type="hidden" name="ownward_currency_id" value="<?=$_GET['ownward_currency_id']?>">
                	<input type="hidden" name="cat_id" value="<?=$_GET['cat_id']?>">
                	<input type="hidden" name="return_trip" value="<?=$_GET['return_trip']?>">
                	<input type="hidden" name="return_seat_price" value="<?=$_GET['return_seat_price']?>">
                	<input type="hidden" name="return_seat_type" value="<?=$_GET['return_seat_type']?>">
                	<input type="hidden" name="return_pickup_point" value="<?=$_GET['return_pickup_point']?>">
                	<input type="hidden" name="return_drop_point" value="<?=$_GET['return_drop_point']?>">
                	<input type="hidden" name="return_trip_source" value="<?=$_GET['return_trip_source']?>">
                	<input type="hidden" name="return_trip_destination" value="<?=$_GET['return_trip_destination']?>">
                	<input type="hidden" name="return_currency_sign" value="<?=$_GET['return_currency_sign']?>">

                  
                  <input type="hidden" name="ownward_selected_seat_nos" value="<?=$_GET['ownward_selected_seat_nos']?>">
                  <?php 
                    if($_GET['return_trip'] > 0) {
                      if(!isset($_GET['return_selected_seat_nos'])) { ?>
                        <input type="hidden" name="return_selected_seat_nos" value="NULL">
                      <?php } else { ?>
                      <input type="hidden" name="return_selected_seat_nos" value="<?=$_GET['return_selected_seat_nos']?>">
                  <?php } 
                    } else { ?>
                      <input type="hidden" name="return_selected_seat_nos" value="NULL">
                  <?php } ?>

                	<?php for($i=1;$i<$_GET['no_of_seat'];$i++){ ?>
									<div class="row" style="margin-top: 0px;">
                    <div class="col-md-12" style="">
                      <div class="col-lg-2" style="padding-right:6px; padding-left: 6px;">
                        <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('contact_first_name'); ?></h6>
                        <input type="text" id="firstname<?=$i?>" name="firstname<?=$i?>" class="form-control" placeholder="<?= $this->lang->line('enter_first_name'); ?>" required/>
                      </div>
                      <div class="col-lg-2" style="padding-right:6px; padding-left: 6px;">
                        <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('contact_last_name');?></h6>
                        <input type="text" name="lastname<?=$i?>" class="form-control" id="lastname<?=$i?>" placeholder="<?= $this->lang->line('enter_last_name'); ?>" required/>
                      </div>
                      <div class="col-md-2" style="padding-right:6px; padding-left: 6px;">  
                        <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('email_address'); ?></h6>
                        <input type="email" class="form-control" id="email_id<?=$i?>" name="email_id<?=$i?>" placeholder="<?= $this->lang->line('enter_email_address'); ?>" required/>
                      </div>
                      <div class="col-md-2" style="padding-right:6px; padding-left: 6px;">
                        <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('mobile_number');?></h6>
                        <input type="text" id="mobile<?=$i?>" name="mobile<?=$i?>" class="form-control" placeholder="<?= $this->lang->line('enter_mobile_number');?>" required/>  
                      </div>
                      <div class="col-md-1" style="padding-right:6px; padding-left: 6px;">
                        <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('Country');?></h6>
                        <select class="form-control select2" name="country_id<?=$i?>" id="country_id<?=$i?>" required>
                          <option value=""><?= $this->lang->line('Select Country'); ?></option>
                          <?php foreach ($countries as $c) :?>
                            <option value="<?= $c['country_id'];?>" <?=($_GET['country_id0']==$c['country_id'])?'selected':''?>><?=$c['country_name'];?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="col-md-1" style="padding-right:6px; padding-left: 6px;">  
                        <h6 style="color:#175091; margin: 7px 0px;"><?= $this->lang->line('gender'); ?></h6>
                        <select class="form-control" name="gender<?=$i?>" id="gender<?=$i?>" required>
                          <option value="M" selected><?= $this->lang->line('male'); ?></option>
                          <option value="F"><?= $this->lang->line('female'); ?></option>
                        </select>
                      </div>
                      <div class="col-md-2" style="padding-right:6px; padding-left: 6px;">
                        <!-- <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('Age'); ?></h6>
                        <input type="text" id="age<?=$i?>" name="age<?=$i?>" class="form-control" placeholder="<?= $this->lang->line('Enter age'); ?>" required/> -->  

                        <h6 style="color:#175091;margin: 7px 0px;"><?= $this->lang->line('DoB'); ?></h6>
                        <div class="input-group date">
                          <input type="text" class="form-control" id="age<?=$i?>" name="age<?=$i?>" autocomplete="off" >
                          <div class="input-group-addon dpAddon<?=$i?>">
                            <span class="glyphicon glyphicon-th"></span>
                          </div>
                        </div>
                        <script>
                          $(window).load(function() {
                            $('#age<?=$i?>').datepicker({
                              autoclose: true,
                            }, 'setEndDate', new Date(), );
                          });

                          $( ".dpAddon<?=$i?>" ).click(function( e ) { 
                            $("#age<?=$i?>").focus();
                          });
                        </script>

                      </div>
                    </div>
                  </div>
                  <?php if(($_GET['no_of_seat']-1) > $i):?>
                    <hr style="margin-bottom: 0px;" />
                  <?php endif; ?>
                    <script>
                      $('#add_passengers_details').validate({
                        rules: {
                          mobile<?=$i?>: {
                            required: true,
                            number: true,
                            min : 0,
                            minlength: 6,
                            maxlength: 16,
                          },
                          age<?=$i?>: {
                            required: true,
                            number: true,
                            min: 0,
                            minlength: 1,
                            maxlength: 2,
                          },
                          email_id<?=$i?>: {
                            required: true,
                            email: true,
                          },
                          firstname<?=$i?>: {
                            required: true,
                          },
                          lastname<?=$i?>: {
                            required: true,
                          },
                          gender<?=$i?>: {
                            required: true,
                          },
                          country_id<?=$i?>: {
                            required: true,
                          },
                        },
                        messages: {
                          mobile<?=$i?>: { required : "<?=$this->lang->line('enter_mobile_number');?>", number : "<?=$this->lang->line('number_only');?>", min : "<?=$this->lang->line('Negative not allowed');?>", minlength : "<?=$this->lang->line('6_characters');?>", maxlength : "<?=$this->lang->line('max_16_characters');?>" },
                          age<?=$i?>: { required : "<?=$this->lang->line('Enter age');?>", min : "<?=$this->lang->line('Negative not allowed');?>", number : "<?=$this->lang->line('number_only');?>", minlength : "<?=$this->lang->line('atleast 1 digits');?>", maxlength : "<?=$this->lang->line('2 digits only');?>" }, 
                          email_id<?=$i?>: { required : "<?=$this->lang->line('enter_email_address');?>", email : "<?=$this->lang->line('invalid_email');?>" },
                          firstname<?=$i?>: { required : "<?=$this->lang->line('enter_first_name');?>", },
                          lastname<?=$i?>: { required : "<?=$this->lang->line('enter_last_name');?>", },
                          gender<?=$i?>: { required : "<?=$this->lang->line('Select Gender!');?>", },
                          country_id<?=$i?>: { required : "<?=$this->lang->line('Select Country');?>", },
                        },
                      });
                    </script>

                	<?php	} ?>
                	<div class="row" style="margin-top: 15px;">
                    <div class="col-md-8 col-md-offset-4 text-right">
                      <button id="form_submit" class="btn btn-default booknowBtn" type="submit"><?=$this->lang->line('BOOK & PAY')?></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>
		</div>			
	</div>
</section>
