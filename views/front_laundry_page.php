<style>
    .cat_color:hover { 
        cursor: pointer;
        background-color: #64CCF5;
    }
    .selected_cat_color { 
        background-color: #64CCF5;
    }
</style>

<!-- SECTION IMAGE FULLSCREEN -->
<div class="w3-content w3-section" style="margin-top:0px !important; margin-bottom:0px !important; max-width: 100% !important;">
  <img class="mySlides" src="<?=base_url('resources/web/front_site/images/slider/laundry_bg.jpg');?>" style="width:100%">
  <div class="slider-content">
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 hidden-xs">
        <h1 class="" style="-webkit-text-stroke: 1px black; font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Order or have your laundry delivered online'); ?></span></h1>
        <p class="lead" style="font-weight: bold; color: white !important;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Save time, do what you want while we process your laundry'); ?></span></p>
      </div>
      <div class="col-xs-12 hidden-xl hidden-lg hidden-md hidden-sm">
        <h6 class="" style="font-weight: bold; color: white;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Order or have your laundry delivered online'); ?></span></h6>
        <h6 style="color: white !important; margin-bottom: 30px !important;"><span style="background-color: #00000070; border-radius: 14px; padding: 0px 10px 0px 10px; color: white !important;"><?= $this->lang->line('Save time, do what you want while we process your laundry'); ?></span></h6>
      </div>
      
      <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 hidden-xs">
        <div class="search-form wow pulse" data-wow-delay="0.8s" style="margin-top: -60px; padding-bottom: 5px; background-color: #ffffffd6 !important; padding-top: 15px !important">
          <form action="<?=base_url('laundry-providers')?>" class="form-inline createForm" id="createForm" method="GET" >
            <input type="hidden" name="customer_lat_long" id="customer_lat_long" class="customer_lat_long" />
            <input type="hidden" name="session_id" id="session_id" class="session_id" value="<?=$_SESSION['__ci_last_regenerate']?>" />
            <input type="hidden" name="cat_id" value="9">
            <div class="row" style="margin-left: 0px; margin-right: 0px; margin-bottom: 0px;">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12" class="classCategory">
                <div class="panel panel-success cat_color" id="type_category_man">
                  <div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 0px;">
                    <img src="<?=base_url('resources/category-icons/man.png')?>" style="max-width: 32px; height: auto;">
                    <h4 style="margin:0; font-size: 16px;" class="font-extra-bold no-margins"><?=$this->lang->line('man')?> (<label id="type_category_man_count">0</label>)</h4>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12" class="classCategory">
                <div class="panel panel-success cat_color" id="type_category_woman">
                  <div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 0px;">
                    <img src="<?=base_url('resources/category-icons/woman.png')?>" style="max-width: 32px; height: auto;">
                    <h4 style="margin:0; font-size: 16px;" class="font-extra-bold no-margins"><?=$this->lang->line('woman')?> (<label id="type_category_woman_count">0</label>)</h4>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12" class="classCategory">
                <div class="panel panel-success cat_color" id="type_category_child">
                  <div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 0px;">
                      <img src="<?=base_url('resources/category-icons/child.png')?>" style="max-width: 32px; height: auto;">
                      <h4 style="margin:0; font-size: 16px;" class="font-extra-bold no-margins"><?=$this->lang->line('child')?> (<label id="type_category_child_count">0</label>)</h4>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12" class="classCategory">
                <div class="panel panel-success cat_color" id="type_category_other"s>
                  <div class="panel-body text-center" style="padding-top: 5px !important; padding-bottom: 0px;">
                    <img src="<?=base_url('resources/category-icons/other.png')?>" style="max-width: 32px; height: auto;">
                      <h4 style="margin:0; font-size: 16px;" class="font-extra-bold no-margins"><?=$this->lang->line('other')?> (<label id="type_category_other_count">0</label>)</h4>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="margin-left: 5px; margin-right: 0px;">
              <div class="sub_categories_man dragscroll" style="display: flex; overflow: auto; cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab;">
              <h5 class="lbl_scroll" style="color: #62ccf5; display: none;"><?=$this->lang->line("Scroll for more items")?></h5>
              </div>
              <div class="sub_categories_woman dragscroll" style="display: flex; overflow: auto; cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab;">
              </div>
              <div class="sub_categories_child dragscroll" style="display: flex; overflow: auto; cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab;">
              </div>
              <div class="sub_categories_other dragscroll" style="display: flex; overflow: auto; cursor: grab; cursor : -o-grab; cursor : -moz-grab; cursor : -webkit-grab;">
              </div>
            </div>

            <div class="row">
              <div class=" col-md-4 col-md-offset-4 form-group" style="margin-top:5px;">
                <button class="btn btn-default btn-delivery btn-block booknowBtn" type="submit" id="btnFormCreate"><?=$this->lang->line('Book Now')?></button>
              </div>
              <div class=" col-md-4">&nbsp;</div>  
                                         
              <div class=" col-md-12 form-group" style="margin-top:10px;">
                <?php foreach ($laundry_providers as $providers): ?>
                  <?php if($providers['dedicated_provider']): ?>
                    <img title="<?=$providers['company_name']?>" src="<?=base_url($providers['avatar_url'])?>" style="max-height:48px; width: auto;">&nbsp;&nbsp;
                  <?php endif; ?>
                <?php endforeach ?>
              </div>
            </div>  
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="p-b-0" style="padding-top: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 20px">
      <h2><?= $this->lang->line('Offers And Promocodes'); ?></h2>
      <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>
    </div>
    <div class="container" style="padding-bottom: 20px">
      <div class="row stylish-panel">
        <?php $i=0; foreach ($promocodes as $promo){ $i++; ?>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
            <div>
              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-1" >
                  <img style="width:100px; height:70px;" src='<?=base_url($promo["promocode_avtar_url"])?>' alt="">
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-11" >
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4" >
                    <h6 style="margin: 0px 0px"> <?=$this->lang->line('Discount')?></h6>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" >
                    <h6 style="margin: 0px 0px;"><?=$promo['discount_percent']?>%</h6>
                  </div>
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding: 0px 0px;" >
                    <h4 style="color: #5dccf7;"><?=$promo['promo_code']?></h4>
                  </div>
                </div>
              </div>
            </div>    <!--<h3><?=$promo['promo_code']?></h3>-->
            <?php if($i==3 || $i==6) echo "<br/>"; ?>  
          </div>
        <?php  } ?>
      </div>
    </div>
  </div>
</section>


<!-- How it works -->
<section class="background-grey" id="howworks" style="padding-top: 0px;padding-bottom: 10px;">
  <div class="container">
    <div class="heading heading-center text-center" style="margin-bottom: 10px;">
      <h2><?= $this->lang->line('BENEFITS OF BOOKING YOUR LAUNDRY ON GONAGOO'); ?></h2>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          <div class="icon-box effect center light square">
            <div class="icon"> <a href="#"><i class="fa fa-archive"></i></a> </div>
            <h4 align="center"><?= $this->lang->line('Saving time and money'); ?></h4>
            <p align="center"><?= $this->lang->line('Order your laundry in 1mn, 7d / 7 24/24!'); ?><br /><?= $this->lang->line('Best market prices'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
            <div class="icon-box effect  center light square">
                <div class="icon"> <a href="#"><i class="fa fa-credit-card"></i></a> </div>
                <h4><?= $this->lang->line('Payment facility'); ?> </h4>
                <p align="center"><?= $this->lang->line('Payment by card, mobile money or cash.'); ?></p>
            </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          <div class="icon-box effect  center light square">
            <div class="icon"> <a href="#"><i class="fa fa-users"></i></a> </div>
            <h4><?= $this->lang->line('Best laundry operators'); ?></h4>
            <p align="center"><?= $this->lang->line('The best laundry agencies work with Gonagoo'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="text-box" data-animation="fadeInUp" data-animation-delay="0">
          <div class="icon-box effect  center light square">
            <div class="icon"> <a href="#"><i class="fa fa-mobile"></i></a> </div>
            <h4><?= $this->lang->line('Mobile Apps'); ?></h4>
            <p align="center"><?= $this->lang->line('Web, Android or iOS: its up to you!'); ?><br /><?= $this->lang->line('Info in real time.'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END: How it works -->

<!-- CATEGORIES -->
<section class="p-b-0" style="padding-top: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 10px;">
      <h2><?= $this->lang->line('categories'); ?></h2>
      <span class="lead"><?= $this->lang->line('categories_tile1'); ?></span>
    </div>

    <!--
    <div class="section-content">
      <div class="row text-center color-border-bottom">
        <div class="col-xs-12 col-sm-6 col-md-3 col-md-offset-3 well">
            <i class="fa fa-truck fa-5x"></i> <br /><br />
            <h5 class="ac-title">Transport (freight)</h5>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 well">
            <i class="fa fa-envelope fa-5x"></i> <br /><br />
            <h5 class="ac-title">Courier & parcel delivery</h5>
        </div>
      </div>
    </div> -->
    <style type="text/css">
      .stylish-panel {
        padding: 20px 0;
        text-align: center;
      }
      .stylish-panel > div > div{
        padding: 10px;
        border: 1px solid #afe9ff;
        border-radius: 4px;
        transition: 0.2s;
      }
      .stylish-panel > div:hover > div{
        margin-top: -10px;
        border: 1px solid rgb(200, 200, 200);
        box-shadow: #afe9ff 0px 5px 5px 2px;
        background: rgba(200, 200, 200, 0.1);
        transition: 0.5s;
      }
      .stylish-panel > div:hover img {
        border-radius: 50%;
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    </style>

    <div class="container" style="padding-bottom: 20px">
      <div class="row stylish-panel">
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-truck fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('transportation'); ?></h3>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-envelope fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('courier'); ?></h3>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-dropbox fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('home_move'); ?></h3>
                    </a>
                </div>
            <br />
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-ticket fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('Ticket Booking'); ?></h3>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-recycle fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('Laundry Booking'); ?></h3>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div>
                    <a href="<?= base_url('sign-up') ?>">
                        <i class="fa fa-fire fa-5x"></i> <br /><br />
                        <h3><?= $this->lang->line('Gas Booking'); ?></h3>
                    </a>
                </div>
            </div>
      </div>
    </div>

    <!-- <ul class="grid grid-5-columns p-t-0">
        <?php foreach ($category_type as $cat) { ?>
            <li>
                <div class="accordion toggle clean color-border-bottom">
                    <div class="ac-item">
                        <h5 class="ac-title"> <i class="fa fa-bitbucket"></i><?= $cat['cat_type'] ?></h5>
                        <div class="ac-content">
                            <?php $category = $this->api->get_category_by_id($cat['cat_type_id']); ?>
                            <?php foreach ($category as $c) { ?>
                                <p><a href="#"><?= $c['cat_name'] ?></a></p> 
                            <?php } ?>
                        </div>
                    </div>
                </div>  
            </li>
        <?php } ?>
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-cutlery"></i><?= $this->lang->line('resturant'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-industry"></i><?= $this->lang->line('domestic_gas'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-archive"></i><?= $this->lang->line('courier'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li> 
      <li>
        <div class="accordion toggle clean color-border-bottom">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-truck"></i><?= $this->lang->line('transportation'); ?></h5>
                <div class="ac-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p> 
                </div>
            </div>
        </div>  
      </li>
    </ul> -->
  </div>
</section>
<!-- END: CATEGORIES -->

<!-- OFFERS -->
<section class="parallax text-center text-light p-t-50 p-b-50 background-overlay" style="background-image: url('<?= $this->config->item('resource_url') . 'web/front_site/images/parallax/1.jpg';?>');" data-stellar-background-ratio="0.6">
  <div class="container">
    <div class="row">
      <div class="heading heading-center m-b-20">
        <h2><?= $this->lang->line('offer1'); ?></h2>
        <span class="lead"><?= $this->lang->line('offer2'); ?></span>
      </div>
      <!--<a href="<?= base_url().'sign-up'; ?>" class="button color rounded"><span><?= $this->lang->line('sign_up'); ?></span></a>  -->
    </div>
  </div>
</section>
<!-- END: OFFERS -->

<section id="benefits" style="padding-bottom: 0px;padding-top: 10px;">
  <div class="container">
    <div class="heading heading-center m-b-15">
      <h2><?= $this->lang->line('benefit'); ?></h2>
    </div> 
    <div class="col-md-4">
      <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
        <div><h3 style="color: #5dccf7;"><?= $this->lang->line('Laundry agencies'); ?></h3></div>
        <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/carrier.jpg'; ?>" class="img-responsive"></div><br />
        <div>
           <ul type="none">
                  <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('New customers'); ?></span></li>
                  <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Increase in sales'); ?></span></li>
                  <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Dashboards activity'); ?></span></li>
                  <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Customer relationship management'); ?></span></li>
                  <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Prepaid services'); ?></span></li>
                  <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Geolocation & Drivers App'); ?></span></li>
                  <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Increased visibility and reputation'); ?></span></li>
           </ul>
        </div> 
      </div>
    </div>
    <div class="col-md-4">
      <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
      <div><h3 style="color: #5dccf7;"><?= $this->lang->line('Businesses'); ?></h3></div>
       <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/business.jpg'; ?>" class="img-responsive"></div><br />
         <div>
           <ul type="none">
             <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Attractive, transparent prices'); ?></span></li>
             <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Online booking'); ?></span></li>
             <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Ease, time saving, responsiveness'); ?></span></li>
             <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Providers verified and rated'); ?></span></li>
             <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Alerts, notifications'); ?></span></li>
             <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Automated online management (contracts, orders, invoices)'); ?></span></li>
             <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Reduced costs'); ?></span></li>
           </ul>
         </div>
      </div> 
    </div>
    <div class="col-md-4">
      <div class="jumbotron jumbotron-small" style="min-height: 650px !important;">
        <div><h3 style="color: #5dccf7;"><?= $this->lang->line('individuals'); ?></h3></div>
        <div><img style="width: 100%; height: auto;border: 1px solid #afe9ff; border-radius: 4px;" alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/individual.jpg'; ?>" class="img-responsive"></div><br />
        <div>
          <ul type="none">
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Instant booking online 7/7'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Safety: agencies and drivers verified and rated'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Easy payment (mobile money, cash)'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Low prices'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Ease, time saving'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Alerts, notifications'); ?></span></li>
            <li><span><font color="#fff" style="background-color:#5dccf7; border-radius: 10px; padding-bottom: 2px;">&nbsp;&#x2714;&nbsp;</font></span> <span><?= $this->lang->line('Mobile, web, simple apps'); ?></span></li>
          </ul>
        </div>
      </div> 
    </div>
    <div class="col-md-12 text-center">
      <a href="<?= base_url().'sign-up'; ?>" class="button color rounded"><span><?= $this->lang->line('sign_up'); ?></span></a>
    </div>
  </div>
</section>

<!-- CLIENTS -->
<section class="p-t-0" style="padding-bottom: 10px;">
  <div class="container">
    <div class="heading heading-center" style="margin-bottom: 10px;">
      <h2><?= $this->lang->line('clients'); ?></h2>
      <span class="lead"></span>
    </div>

    <div class="carousel" data-lightbox-type="gallery" data-carousel-col="3">
      <div class="featured-box">
          <div class="image-box circle-image small">
              <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/1.jpg'; ?>" class="">
          </div>
          <div class="image-box-description text-center">
              <h4><?= $this->lang->line('name_1'); ?></h4>
              <p class="subtitle"><?= $this->lang->line('sub_title_1'); ?></p>
              <hr class="line">
              <div><?= $this->lang->line('description1'); ?></div>
              
          </div>
      </div>
      <div class="featured-box">
          <div class="image-box circle-image small">
              <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/2.jpg'; ?>" alt="">
          </div>
          <div class="image-box-description text-center">
              <h4><?= $this->lang->line('name_2'); ?></h4>
              <p class="subtitle"><?= $this->lang->line('sub_title_2'); ?></p>
              <hr class="line">
              <div><?= $this->lang->line('description2'); ?></div>
              
          </div>
      </div>
      <div class="featured-box">
          <div class="image-box circle-image small">
              <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/3.jpg'; ?>">
          </div>
          <div class="image-box-description text-center ">
              <h4><?= $this->lang->line('name_3'); ?></h4>
              <p class="subtitle"><?= $this->lang->line('sub_title_3'); ?></p>
              <hr class="line">
              <div><?= $this->lang->line('description3'); ?></div>
              
          </div>
      </div>
      <div class="featured-box">
          <div class="image-box circle-image small">
              <img alt="" src="<?= $this->config->item('resource_url') . 'web/front_site/images/team/4.jpg'; ?>">
          </div>
          <div class="image-box-description text-center">
              <h4><?= $this->lang->line('name_4'); ?></h4>
              <p class="subtitle"><?= $this->lang->line('sub_title_4'); ?></p>
              <hr class="line">
              <div><?= $this->lang->line('description4'); ?></div>
              
          </div>
      </div>
    </div>
  </div>
</section>
<!-- CLIENTS -->

<section class="hidden">
  <div class="container">
     <div class="col-md-12">
          <div class="col-md-3 text-center">
              <h4 class="widget-title"><?= $this->lang->line('they_talk_about_us'); ?> -</h4>
          </div>
          <div class="col-md-9">
              <img src="<?= $this->config->item('resource_url') . 'web/front_site/images/icons/news.png'; ?>" style="width: 100%;">
          </div>
      </div>
  </div>
</section>

<script>
    var cat_man = 0;
    $("#type_category_man").on("click", function(e) { e.preventDefault();
      var cat_id = 1;
      $("#type_category_man").addClass('selected_cat_color');
      $("#type_category_woman").removeClass('selected_cat_color');
      $("#type_category_child").removeClass('selected_cat_color');
      $("#type_category_other").removeClass('selected_cat_color');
      // $(".lbl_scroll").fadeIn(5000);
      // $(".lbl_scroll").fadeOut(3000);
      $(".type_category_man").scrollLeft(300);
      $(".sub_categories_man").show();
      $(".sub_categories_other").hide();
      $(".sub_categories_woman").hide();
      $(".sub_categories_child").hide();
      
      if(cat_man == 0) {
        $.ajax({
          type: "POST",
          url: "<?=base_url('get-laundry-sub-category-append');?>", 
          data: {cat_id:cat_id},
          success: function(res){
              cat_man++;
              $('.sub_categories_man').append(res);
          }
        }); 
      }
    });

    var cat_woman = 0;
    $("#type_category_woman").on("click", function(e) { e.preventDefault();
      $("#type_category_woman").addClass('selected_cat_color');
      $("#type_category_man").removeClass('selected_cat_color');
      $("#type_category_child").removeClass('selected_cat_color');
      $("#type_category_other").removeClass('selected_cat_color');
      $(".sub_categories_woman").show();
      $(".sub_categories_man").hide();
      $(".sub_categories_other").hide();
      $(".sub_categories_child").hide();

      var cat_id = 2;
      if(cat_woman == 0) {
        $.ajax({
          type: "POST",
          url: "<?=base_url('get-laundry-sub-category-append');?>", 
          data: {cat_id:cat_id},
          success: function(res){
            cat_woman++;
            $('.sub_categories_woman').append(res);
          }
        });
      }
    });

    var cat_child = 0;
    $("#type_category_child").on("click", function(e) { e.preventDefault();
      $("#type_category_child").addClass('selected_cat_color');
      $("#type_category_woman").removeClass('selected_cat_color');
      $("#type_category_man").removeClass('selected_cat_color');
      $("#type_category_other").removeClass('selected_cat_color');
      $(".sub_categories_child").show();
      $(".sub_categories_man").hide();
      $(".sub_categories_woman").hide();
      $(".sub_categories_other").hide();

      var cat_id = 3;
      if(cat_child == 0) {
        $.ajax({
          type: "POST",
          url: "<?=base_url('get-laundry-sub-category-append');?>", 
          data: {cat_id:cat_id},
          success: function(res){
            cat_child++;
            $('.sub_categories_child').append(res);
          }
        });
      }
    });

    var cat_other = 0;
    $("#type_category_other").on("click", function(e) { e.preventDefault();
      $("#type_category_other").addClass('selected_cat_color');
      $("#type_category_woman").removeClass('selected_cat_color');
      $("#type_category_man").removeClass('selected_cat_color');
      $("#type_category_child").removeClass('selected_cat_color');
      $(".sub_categories_other").show();
      $(".sub_categories_man").hide();
      $(".sub_categories_woman").hide();
      $(".sub_categories_child").hide();

      var cat_id = 4;
      if(cat_other == 0) {
        $.ajax({
          type: "POST",
          url: "<?=base_url('get-laundry-sub-category-append');?>", 
          data: {cat_id:cat_id},
          success: function(res){
            cat_other++;
            $(".sub_categories_other").append(res);
          }
        });
      }
    });
</script>

<script>
  $( document ).ready(function() {
    $("#type_category_other").click();
    $("#type_category_child").click();
    $("#type_category_woman").click();
    $("#type_category_man").click();
    $("#type_category_man").addClass('active');
    $(".sub_categories_man").show(); 

    //if browser location not enabled then error alert.
    var flashdata = "<?=$this->session->flashdata('error')?>";
    console.log(flashdata);
    if(flashdata != '') { alert(flashdata,"warning"); }

    /*$.get("https://ipinfo.io/json", function (response) {
        $("#customer_lat_long").val(response.loc);
        //alert(response.loc);
        $("#address").html("Location: " + response.city + ", " + response.region);
        $("#details").html(JSON.stringify(response, null, 4));
    }, "jsonp");*/
  });

  if (navigator.geolocation) { navigator.geolocation.getCurrentPosition(showPosition);
  } else { console.log('Geolocation is not supported by this browser.'); $("#customer_lat_long").val("Geolocation is not supported by this browser."); }
  function showPosition(position) { $("#customer_lat_long").val(position.coords.latitude + "," + position.coords.longitude); }
</script>