<style type="text/css">
  input.search {
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
}
</style>
<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Transactions List</strong>
  </li>
</ol>

<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>
<div class="rows">
  <div class="col-md-6">
    <h3 style="margin-top: 0px;">Currency-wise Transactions List</h3>
  </div>
</div>    
  
  <table id="table-3" class="table table-bordered datatable" width="100%">
    <thead>
      <tr>
        <th>Tr. Id</th>
        <th>Or. ID</th>
        <th>Order Status</th>
        <th>Date Time</th>
        <th>Order Category</th>
        <th>Transaction</th>
        <th>Amount</th>
        <th>+/-</th>
        <th>Balance</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($history as $histry) { ?>
      <tr style="background-color: <?= $histry['type'] == '1' ? '#DFFFDF': '#FFDDDD'; ?>">
        <td style="text-align: center;"><?= $histry['gh_id'] ?></td>
        <td style="text-align: center;"><?= $histry['order_id'] ?></td>
        <td><?= strtoupper(str_replace('_', ' ', $this->admin->get_order_status_by_order_id($histry['order_id']))) ?></td>
        <td><?= date('d-M-Y h A',strtotime($histry['datetime'])) ?></td>
        <td><?= $this->admin->get_category_name_by_order_id($histry['order_id']) ?></td>
        <td><?= ucwords(str_replace('_',' ',$histry['transaction_type'])) ?></td>
        <td><?= $histry['currency_code'] . ' ' . $histry['amount'] ?></td>
        <td style="text-align: center;"><?= $histry['type'] == '1' ? '<i class="fa fa-plus" style="color: green"></i>' : '<i class="fa fa-minus" style="color: red"></i>'; ?></td>
        <td><?= $histry['currency_code'] . ' ' .  $histry['gonagoo_balance'] ?></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <tr>
        <th>Tr. Id</th>
        <th>Or. ID</th>
        <th>Order Status</th>
        <th>Date Time</th>
        <th>Order Category</th>
        <th>Transaction</th>
        <th>Amount</th>
        <th>+/-</th>
        <th>Balance</th>
      </tr>
    </tfoot>
  </table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
      var $table3 = jQuery("#table-3");
      
      var table3 = $table3.DataTable( {
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      } );
      
      // Initalize Select Dropdown after DataTables is created
      $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
        minimumResultsForSearch: -1
      });
      
      // Setup - add a text input to each footer cell
      $( '#table-3 tfoot th' ).each( function () {
        var title = $('#table-3 thead th').eq( $(this).index() ).text();
        if($.trim(title) != '+/-') {
          $(this).html( '<input type="text" class="form-control search" placeholder="&#xf002;" />' );
        }
      } );
      
      // Apply the search
      table3.columns().every( function () {
        var that = this;
      
        $( 'input', this.footer() ).on( 'keyup change', function () {
          if ( that.search() !== this.value ) {
            that
              .search( this.value )
              .draw();
          }
        } );
      } );
    } );
$('.search').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('search');
    } else {
        input.removeClass('search');
    }
});
</script>