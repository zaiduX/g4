<style>hr{border-top: 2px solid #eee;} .error { font-size: small; }</style>
<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/p-to-p-rates'); ?>">Point to Point Rates</a>
  </li>
  <li class="active">
    <strong>Add Weight Based Rates</strong>
  </li>
</ol>

<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add New Weight Based Point to Point Rates
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/p-to-p-rates'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php $error = $data = array(); if($error = $this->session->flashdata('error')): $data = $error['data']; ?>
          <div class="alert alert-danger text-center"><?= $error['error_msg']; ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>

        <form role="form" action="<?= base_url('admin/p-to-p-rates/register/weight-based'); ?>" class="form-horizontal form-groups-bordered" id="point_to_point_rates_add" method="post">
          <style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
          
          <div class="border">
            <div class="row">
              <div class="col-md-12">
                <label for="category" class="control-label">Select Category </label>              
                <select id="category_id" name="category[]" class="form-control select2" multiple placeholder="Select Category">
                  <?php foreach ($categories as $c ): ?>
                    <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
                      <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <div class="border">
            <div class="row">
              <div class="col-md-3">
                <label for="from_country_id" class="control-label">From Country </label>           
                <select id="from_country_id" name="from_country_id" class="form-control ">
                  <option value="0">Select Country</option>
                  <?php foreach ($countries_list as $country): ?>                     
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>                   
                </select>
              </div>            
              <div class="col-md-3">
                <label for="from_state_id" class="control-label">From State </label>           
                <select id="from_state_id" name="from_state_id" class="form-control " placeholder="Select State"  data-allow-clear="true" data-placeholder="Select State" disabled>
                  <option value="0">Select state</option>                
                </select>
              </div>
              <div class="col-md-3">
                <label for="from_city_id" class="control-label">From City </label>           
                <select id="from_city_id" name="from_city_id" class="form-control " placeholder="Select City"  data-allow-clear="true" data-placeholder="Select City" disabled>
                  <option value="0">Select City</option>
                </select>
              </div>
              <div class="col-md-3">
                <label class="control-label">Country Currency </label>
                <input type="hidden" name="currency_id" id="currency_id" value="0" />
                <input type="text" name="currency_name" id="currency_name" class="form-control" disabled placeholder="Country Currency" />
                <!-- <select id="currency_id" name="currency_id" class="form-control " data-allow-clear="true" data-placeholder="Select Currency" disabled>
                  <option value="0">Select Currency</option>                
                </select>              -->
              </div>
            </div>
            <div class="clear"></div><br />

            <div class="row">
              <div class="col-md-4">
                <label for="to_country_id" class="control-label">To Country </label>           
                <select id="to_country_id" name="to_country_id" class="form-control">
                  <option value="0">Select Country</option>
                  <?php foreach ($countries_list as $country): ?>                     
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>                   
                </select>
              </div>            
              <div class="col-md-4">
                <label for="to_state_id" class="control-label">To State </label>           
                <select id="to_state_id" name="to_state_id" class="form-control "  data-allow-clear="true" data-placeholder="Select State" disabled >
                  <option value="0">Select state</option>                
                </select>
              </div>
              <div class="col-md-4">
                <label for="to_city_id" class="control-label">To City </label>           
                <select id="to_city_id" name="to_city_id" class="form-control "  data-allow-clear="true" data-placeholder="Select City" disabled>
                  <option value="0">Select City</option>
                </select>
              </div>            
            </div>
            <div class="clear"></div><br />
          </div>
                    
          <div class="border">
            <div class="row">
              <div class="col-md-4">
                <label for="min_weight" class="control-label">Min. Weight </label>           
                <input type="number" class="form-control" id="min_weight" placeholder="Min. Weight" name="min_weight" min="1" <?php if(!empty($data)){ echo 'value="'.$data['min_weight'].'"'; } ?> />
              </div>

              <div class="col-md-4">
                <label for="max_weight" class="control-label">Max. Weight </label>                         
                <input type="number" class="form-control" id="max_weight" placeholder="Max. Weight" name="max_weight" min="1" <?php if(!empty($data)){ echo 'value="'.$data['max_weight'].'"'; } ?> />                
              </div>
              <div class="col-md-4">
                <label for="unit_id" class="control-label">Weight Unit </label>                         
                <select id="unit_id" name="unit_id" class="form-control">
                  <option value="0">Select Unit</option>
                  <?php foreach ($unit_list as $unit): ?>                     
                    <option value="<?= $unit['unit_id'] ?>" <?= (!empty($data) && $data['unit_id']==$unit['unit_id'])?"selected":"";?>><?= $unit['shortname']; ?></option>
                  <?php endforeach ?>
                </select>                
              </div>
            </div>          
            <div class="clear"></div>
            <div class="clear"></div><br />
          </div>
          
          <div class="border">
            <div class="row">
              <div class="col-md-4"><hr/></div>
              <div class="col-md-4 text-center"><h4 class="text-primary">Earth Rates &amp; Durations</h4></div>
              <div class="col-md-4"><hr/></div>
            </div>
        
            <div class="row">           
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">Local Rate &amp; Duration</label> 
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="earth_local_rate" placeholder="Rate" name="earth_local_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_local_rate'].'"'; } ?> /> 
                    </div>
                  </div>
                  <div class="col-md-6">              
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_local_duration" placeholder="Duration" name="earth_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_local_duration'].'"'; } ?> /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">National Rate &amp; Duration</label> 
                <div class="row">    
                  <div class="col-md-6">                
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="earth_national_rate" placeholder="Rate" name="earth_national_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_national_rate'].'"'; } ?>  /> 
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_national_duration" placeholder="Duration" name="earth_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_national_duration'].'"'; } ?>  /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">International Rate &amp; Duration</label> 
                <div class="row">    
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="earth_international_rate" placeholder="Rate" name="earth_international_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_international_rate'].'"'; } ?>  />  
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="number" class="form-control" id="earth_international_duration" placeholder="Duration" name="earth_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_international_duration'].'"'; } ?>  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear"></div><br />        
          </div>
          
          <div class="border">
            <div class="row">
              <div class="col-md-4"><hr/></div>
              <div class="col-md-4 text-center"><h4 class="text-primary">Air Rates &amp; Durations</h4></div>
              <div class="col-md-4"><hr/></div>
            </div>
            <div class="row">
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">Local Rate &amp; Duration</label> 
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="air_local_rate" placeholder="Rate" name="air_local_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_local_rate'].'"'; } ?> /> 
                    </div>
                  </div>                
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_local_duration" placeholder="Duration" name="air_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_local_duration'].'"'; } ?> /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">National Rate &amp; Duration</label> 
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>  
                      <input type="number" class="form-control" id="air_national_rate" placeholder="Rate" name="air_national_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_national_rate'].'"'; } ?>  /> 
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_national_duration" placeholder="Duration" name="air_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_national_duration'].'"'; } ?>  /> 
                      <span class="input-group-addon">hrs</span>
                    </div>                
                  </div>
                </div>
              </div>
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">International Rate &amp; Duration</label> 
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>  
                      <input type="number" class="form-control" id="air_international_rate" placeholder="Rate" name="air_international_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_international_rate'].'"'; } ?>  />  
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="number" class="form-control" id="air_international_duration" placeholder="Duration" name="air_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_international_duration'].'"'; } ?>  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear"></div><br />
          </div>
          <div class="border">
            <div class="row">
              <div class="col-md-4"><hr/></div>
              <div class="col-md-4 text-center"><h4 class="text-primary">Sea Rates &amp; Durations</h4></div>
              <div class="col-md-4"><hr/></div>
            </div>      
            <div class="row">
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">Local Rate &amp; Duration</label> 
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>              
                      <input type="number" class="form-control" id="sea_local_rate" placeholder="Rate" min="0" name="sea_local_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_local_rate'].'"'; } ?> /> 
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_local_duration" placeholder="Duration" name="sea_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_local_duration'].'"'; } ?> /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                  </div>                
                </div>
              </div>
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">National Rate &amp; Duration</label> 
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="sea_national_rate" placeholder="Rate" min="0" name="sea_national_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_national_rate'].'"'; } ?>  /> 
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_national_duration" placeholder="Duration" name="sea_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_national_duration'].'"'; } ?>  /> 
                      <span class="input-group-addon">hrs</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">            
                <label for="" class="control-label col-md-offset-2">National Rate &amp; Duration</label> 
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon currency fa">?</span>
                      <input type="number" class="form-control" id="sea_international_rate" min="0" placeholder="Rate" name="sea_international_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_international_rate'].'"'; } ?>  />  
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="number" class="form-control" id="sea_international_duration" placeholder="Duration" name="sea_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_international_duration'].'"'; } ?>  />  
                      <span class="input-group-addon">hrs</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clear"></div><br />
          </div>
          <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
              <div class="text-right">
                <button id="btn_submit" type="submit" class="btn btn-blue"><i class="fa fa-plus-square"></i> &nbsp; Add Rates</button>
              </div>
            </div>
          </div>
          
        </form>
      </div>  
    </div>  
  </div>
</div>

<br />

<script>
  $("#from_country_id").on('change', function(event) { event.preventDefault();
    $(".currency").addClass('hidden').html('');
    $('#from_state_id').empty();
    $('#from_city_id').empty().append('<<option value="0">Select City</option>');

    var country_id = $(this).val();

    $.post('get-states', {country_id: country_id}, function(data) {
      data = $.parseJSON(data);
      if(data.length > 0 ){
        $('#from_state_id').attr('disabled', false);
          $('#from_state_id').empty(); 
          $('#from_state_id').append('<option value="0">Select State</option>');
          $.each( data, function(){    
              $('#from_state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');              
          });
          $('#from_state_id').focus();
      }
      else{
          $('#from_state_id').attr('disabled', true);
          $('#from_state_id').empty();
          // $('#from_state_id').select2('val','No currency found!');
          $('#from_state_id').append('<option value="0">No state found!</option>');
        }
    });

    $.ajax({
      type: "POST", 
      url: "get-country-currencies", 
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        if(res.length > 0 ){
          $("#currency_id").val(res[0]['currency_id']);
          $("#currency_name").val(res[0]['currency_title'] + '( ' + res[0]['currency_sign'] + ' )');
          
          if(res[0]['currency_sign'] != null){ $(".currency").removeClass('hidden').html('<strong>'+res[0]['currency_sign']+'</strong>'); }
          else { $(".currency").addClass('hidden').html(''); } 
        } 
        else{
          $('#currency_id').val('0');
          $('#currency_name').val('No currency found!');
        }
      },
      beforeSend: function(){
        // $('#currency_id').empty();
        // $('#currency_id').append('<option value="0">Loading...</option>');
      },
      error: function(){
        // $('#currency_id').attr('disabled', true);
        // $('#currency_id').empty();
        // // $('#currency_id').select2('val','No currency found!');
        // $('#currency_id').append('<option value="0">No currency found!</option>');
      }
    });

  });

  $("#from_state_id").on('change', function(event) { event.preventDefault();
    $('#from_city_id').empty();

    var state_id = $(this).val();

    $.post('get-cities', {state_id: state_id}, function(cities) {
      //console.log(cities);
      cities = $.parseJSON(cities);
      if(cities.length > 0 ){
        $('#from_city_id').attr('disabled', false);
          $('#from_city_id').empty(); 
          $('#from_city_id').append('<option value="0">Select City</option>');
          $.each(cities, function(){    
              $('#from_city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>');              
          });
          $('#from_city_id').focus();
      }
      else{
          $('#from_city_id').attr('disabled', true);
          $('#from_city_id').empty();
          // $('#from_city_id').select2('val','No currency found!');
          $('#from_city_id').append('<option value="0">No city found!</option>');
        }
    });
  });

  $("#to_country_id").on('change', function(event) { event.preventDefault();
    $('#to_state_id').empty();
    $('#to_city_id').empty().append('<<option value="0">Select City</option>');

    var country_id = $(this).val();

    $.post('get-states', {country_id: country_id}, function(to_states) {
      to_states = $.parseJSON(to_states);
      if(to_states.length > 0 ){
        $('#to_state_id').attr('disabled', false);
          $('#to_state_id').empty(); 
          $('#to_state_id').append('<option value="0">Select State</option>');
          $.each( to_states, function(){    
              $('#to_state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');              
          });
          $('#to_state_id').focus();
      }
      else{
          $('#to_state_id').attr('disabled', true);
          $('#to_state_id').empty();
          // $('#to_state_id').select2('val','No currency found!');
          $('#to_state_id').append('<option value="0">No state found!</option>');
        }
    });
  });

  $("#to_state_id").on('change', function(event) { event.preventDefault();
    $('#to_city_id').empty();

    var state_id = $(this).val();

    $.post('get-cities', {state_id: state_id}, function(cities) {
      //console.log(cities);
      cities = $.parseJSON(cities);
      if(cities.length > 0 ){
        $('#to_city_id').attr('disabled', false);
          $('#to_city_id').empty(); 
          $('#to_city_id').append('<option value="0">Select City</option>');
          $.each(cities, function(){    
              $('#to_city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>');              
          });
          $('#to_city_id').focus();
      }
      else{
          $('#to_city_id').attr('disabled', true);
          $('#to_city_id').empty();
          // $('#to_city_id').select2('val','No currency found!');
          $('#to_city_id').append('<option value="0">No city found!</option>');
        }
    });
  });
  
  $("#currency_id").on('change', function(event) {  event.preventDefault();
    var currency = $("#currency_id option:selected").text();
    var sign = currency.match(/\((.*)\)/);
    if(sign != null){ $(".currency").removeClass('hidden').html('<strong>'+sign[1]+'</strong>'); }
    else { $(".currency").addClass('hidden').html(''); }
  });

  $("#btn_submit").click(function(e){ e.preventDefault();
    $(".alert").removeClass('alert-danger alert-success').addClass('hidden').html("");        
    var categories = $("#category_id").val();
    var from_country_id = $("#from_country_id").val();
    var from_state_id = $("#from_state_id").val();
    var from_city_id = $("#from_city_id").val();

    var to_country_id = $("#to_country_id").val();
    var to_state_id = $("#to_state_id").val();
    var to_city_id = $("#to_city_id").val();

    var currency_id = $("#currency_id").val();

    var min_weight = $("#min_weight").val();
    var max_weight = $("#max_weight").val();
    var unit_id = $("#unit_id").val();

    var earth_local_rate = parseFloat($("#earth_local_rate").val()).toFixed(2);
    var earth_national_rate = parseFloat($("#earth_national_rate").val()).toFixed(2);
    var earth_international_rate = parseFloat($("#earth_international_rate").val()).toFixed(2);       
    var air_local_rate = parseFloat($("#air_local_rate").val()).toFixed(2);
    var air_national_rate = parseFloat($("#air_national_rate").val()).toFixed(2);
    var air_international_rate = parseFloat($("#air_international_rate").val()).toFixed(2);       
    var sea_local_rate = parseFloat($("#sea_local_rate").val()).toFixed(2);
    var sea_national_rate = parseFloat($("#sea_national_rate").val()).toFixed(2);
    var sea_international_rate = parseFloat($("#sea_international_rate").val()).toFixed(2);

    var earth_local_duration = parseFloat($("#earth_local_duration").val()).toFixed(2);
    var earth_national_duration = parseFloat($("#earth_national_duration").val()).toFixed(2);
    var earth_international_duration = parseFloat($("#earth_international_duration").val()).toFixed(2);       
    var air_local_duration = parseFloat($("#air_local_duration").val()).toFixed(2);
    var air_national_duration = parseFloat($("#air_national_duration").val()).toFixed(2);
    var air_international_duration = parseFloat($("#air_international_duration").val()).toFixed(2);       
    var sea_local_duration = parseFloat($("#sea_local_duration").val()).toFixed(2);
    var sea_national_duration = parseFloat($("#sea_national_duration").val()).toFixed(2);
    var sea_international_duration = parseFloat($("#sea_international_duration").val()).toFixed(2);
    
    if(!categories) {  swal({title:'Error',text:'Please Select at-least one Category',type:'warning'}).then(function(){ $("#from_country_id").focus();  });   }
    else if(from_country_id == 0 ) {  swal({title:'Error',text:'Please Select From Country',type:'warning'}).then(function(){ $("#from_country_id").focus();  });   }
    else if(currency_id == 0 ) { swal({title:'Error',text:'Please Select Currency',type:'warning'}).then(function(){ $("#currency_id").focus();  }); }
    else if(to_country_id == 0 ) {  swal({title:'Error',text:'Please Select To Country',type:'warning'}).then(function(){ $("#to_country_id").focus();  });   }

    else if(min_weight < 0 ) { swal({title:'Error',text:'Please Enter Minimum Weight',type:'warning'}).then(function(){ $("#min_weight").focus();  }); }
    else if(max_weight < 0 ) { swal({title:'Error',text:'Please Enter Maximum Weight',type:'warning'}).then(function(){ $("#max_weight").focus();  }); }    
    else if(parseFloat(max_weight) <= parseFloat(min_weight) ) { swal({title:'Error',text:'Invalid! Maximum Weight Should greater than Minimum Weight!',type:'warning'}).then(function(){ $("#max_weight").focus();  }); }
    else if(unit_id < 1 ) { swal({title:'Error',text:'Please Select Weight Unit',type:'warning'}).then(function(){ $("#unit_id").focus();  }); }
    
    // Earth
    else if(isNaN(earth_local_rate)) { swal({title:'Error',text:'Please Enter Earth Local Rate',type:'warning'}).then(function(){ $("#earth_local_rate").focus();  }); }
    else if(isNaN(earth_local_duration)) { swal({title:'Error',text:'Please Enter Earth Local Duration',type:'warning'}).then(function(){ $("#earth_local_duration").focus();  }); }
    else if(parseFloat(earth_local_duration) < 1) { swal({title:'Error',text:'Please Enter Earth Local Duration greater than 1hr.',type:'warning'}).then(function(){ $("#earth_local_duration").focus();  }); }    

    else if(isNaN(earth_national_rate)) { swal({title:'Error',text:'Please Enter Earth National Rate',type:'warning'}).then(function(){ $("#earth_national_rate").focus();  }); }
    else if(isNaN(earth_national_duration)) { swal({title:'Error',text:'Please Enter Earth National Duration',type:'warning'}).then(function(){ $("#earth_national_duration").focus();  }); }
    else if(parseFloat(earth_national_duration) < 1) { swal({title:'Error',text:'Please Enter Earth National Duration greater than 1hr.',type:'warning'}).then(function(){ $("#earth_national_duration").focus();  }); }

    else if(isNaN(earth_international_rate)) { swal({title:'Error',text:'Please Enter Earth International Rate',type:'warning'}).then(function(){ $("#earth_international_rate").focus();  }); }
    else if(isNaN(earth_international_duration)) { swal({title:'Error',text:'Please Enter Earth International Duration',type:'warning'}).then(function(){ $("#earth_international_duration").focus();  }); }
    else if(parseFloat(earth_international_duration) < 1) { swal({title:'Error',text:'Please Enter Earth International Duration greater than 1hr.',type:'warning'}).then(function(){ $("#earth_international_duration").focus();  }); }
    // Air
    else if(isNaN(air_local_rate)) { swal({title:'Error',text:'Please Enter Air Local Rate',type:'warning'}).then(function(){ $("#air_local_rate").focus();  }); }
    else if(isNaN(air_local_duration)) { swal({title:'Error',text:'Please Enter Air Local Duration',type:'warning'}).then(function(){ $("#air_local_duration").focus();  }); }
    else if(parseFloat(air_local_duration) < 1) { swal({title:'Error',text:'Please Enter Air Local Duration greater than 1hr.',type:'warning'}).then(function(){ $("#air_local_duration").focus();  }); }    

    else if(isNaN(air_national_rate)) { swal({title:'Error',text:'Please Enter Air National Rate',type:'warning'}).then(function(){ $("#air_national_rate").focus();  }); }
    else if(isNaN(air_national_duration)) { swal({title:'Error',text:'Please Enter Air National Duration',type:'warning'}).then(function(){ $("#air_national_duration").focus();  }); }
    else if(parseFloat(air_national_duration) < 1) { swal({title:'Error',text:'Please Enter Air National Duration greater than 1hr.',type:'warning'}).then(function(){ $("#air_national_duration").focus();  }); }

    else if(isNaN(air_international_rate)) { swal({title:'Error',text:'Please Enter Air International Rate',type:'warning'}).then(function(){ $("#air_international_rate").focus();  }); }
    else if(isNaN(air_international_duration)) { swal({title:'Error',text:'Please Enter Air International Duration',type:'warning'}).then(function(){ $("#air_international_duration").focus();  }); }
    else if(parseFloat(air_international_duration) < 1) { swal({title:'Error',text:'Please Enter Air International Duration greater than 1hr.',type:'warning'}).then(function(){ $("#air_international_duration").focus();  }); }
    // Sea
    else if(isNaN(sea_local_rate)) { swal({title:'Error',text:'Please Enter Sea Local Rate',type:'warning'}).then(function(){ $("#sea_local_rate").focus();  }); }
    else if(isNaN(sea_local_duration)) { swal({title:'Error',text:'Please Enter Sea Local Duration',type:'warning'}).then(function(){ $("#sea_local_duration").focus();  }); }
    else if(parseFloat(sea_local_duration) < 1) { swal({title:'Error',text:'Please Enter Sea Local Duration greater than 1hr.',type:'warning'}).then(function(){ $("#sea_local_duration").focus();  }); }    

    else if(isNaN(sea_national_rate)) { swal({title:'Error',text:'Please Enter Sea National Rate',type:'warning'}).then(function(){ $("#sea_national_rate").focus();  }); }
    else if(isNaN(sea_national_duration)) { swal({title:'Error',text:'Please Enter Sea National Duration',type:'warning'}).then(function(){ $("#sea_national_duration").focus();  }); }
    else if(parseFloat(sea_national_duration) < 1) { swal({title:'Error',text:'Please Enter Sea National Duration greater than 1hr.',type:'warning'}).then(function(){ $("#sea_national_duration").focus();  }); }

    else if(isNaN(sea_international_rate)) { swal({title:'Error',text:'Please Enter Sea International Rate',type:'warning'}).then(function(){ $("#sea_international_rate").focus();  }); }
    else if(isNaN(sea_international_duration)) { swal({title:'Error',text:'Please Enter Sea International Duration',type:'warning'}).then(function(){ $("#sea_international_duration").focus();  }); }
    else if(parseFloat(sea_international_duration) < 1) { swal({title:'Error',text:'Please Enter Sea International Duration greater than 1hr.',type:'warning'}).then(function(){ $("#sea_international_duration").focus();  }); }
    
    else { $("#point_to_point_rates_add")[0].submit();  }
  });

</script>