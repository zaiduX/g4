<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Notification List</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">Notification List</h2>
<?php  $per_manage_notification = explode(',', $permissions[0]['notifications']);
if(in_array('2', $per_manage_notification)) { ?>
	<a type="button" href="<?= base_url('admin/send-notification'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Send Notification
		<i class="fa fa-send"></i>
	</a>
<?php } ?>

	<?php if($this->session->flashdata('error')):  ?>
  		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	<?php endif; ?>
	<?php if($this->session->flashdata('success')):  ?>
  		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Title</th>
			<th class="text-center">Sent to</th>
			<th class="text-center">Filter By</th>
			<th class="text-center">Sent at</th>
			<th class="text-center">Attachement</th>
			<th class="text-center">Actions</th>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1;  ?>
		<?php foreach ($notification_list as $notify):?>
			<tr>
				<td class="text-center"><?= $offset++; ?></td>
				<td class="text-center"><?= $notify['notify_title'];?></td>
				<td class="text-center"><?= $notify['consumer_filter'] ?></td>
				<td class="text-center"><?= $notify['filter_by'] ?></td>
				<td class="text-center"><?= date('h:i a - d M y',strtotime($notify['cre_datetime'])); ?></td>			
				<td class="text-center">
					<?php if( $notify['attachement_url'] != "NULL") : ?>
						<a href="<?= base_url(). $notify['attachement_url']; ?>" class="btn btn-success btn-sm btn-icon icon-left" target="_blank">
							<i class="entypo-download"></i>		Download	
						</a>
					<?php else: ?>
						<label class="label label-danger icon-left"> <i class="entypo-cancel"></i> Not Found</label>
					<?php endif; ?>					
				</td>		
				<td  class="text-center">
					<a class="btn btn-primary btn-sm btn-icon icon-left" href="<?= base_url("admin/notification/"). $notify['id'] ;?>"> 
						<i class="entypo-newspaper"></i> Detail
					</a>
				</td>	
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );

		

	} );
</script>