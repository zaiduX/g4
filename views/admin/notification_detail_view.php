<style>
	.mail-env .mail-body { width: 100%; }
	.mail-env .mail-body .mail-info .mail-sender.mail-sender span, .mail-env .mail-body .mail-info .mail-date.mail-sender span {
    color: #002a5a;
}
</style>

<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Notification Detail</strong>
	</li>
</ol>
<div class="mail-env">
		
	<!-- Mail Body -->
	<div class="mail-body">
		
		<div class="mail-header">
			<!-- title -->
			<div class="mail-title">
				Notification Detail				
			</div>
			
			<!-- links -->
			<div class="mail-links">			
				<a href="<?= base_url('admin/notification'); ?>" class="btn btn-primary btn-icon icon-left">
					Back
					<i class="entypo-reply"></i>
				</a>			
			</div>
		</div>
		
		<div class="mail-info">
			
			<div class="mail-sender dropdown">				
				<span><?= $notification_detail['notify_title'] ?></span>	
			</div>
			
			<div class="mail-date"> <?= date('h:i a - l, d M ',strtotime($notification_detail['cre_datetime'])); ?>
				
			</div>
			
		</div>
		<div class="mail-text">
			<p>
			<?php if($notification_detail['consumer_filter'] != "All" OR $notification_detail['filter_by'] == "Location" ): ?>
				<label class="col-md-2">Filter By: </label>

				<?php if($notification_detail['consumer_filter'] != "All"): ?>
					<label class="label label-primary"><?= $notification_detail['consumer_filter']; ?></label>
				<?php endif; ?>

				<?php if($notification_detail['filter_by'] == "Location"): ?>
					<?php if($notification_detail['country_id'] > 0): ?>
						<label class="label label-success"><?= $this->notification->get_country_detail($notification_detail['country_id'])['country_name']; ?></label> 
					<?php endif; ?>
					<?php if($notification_detail['state_id'] > 0): ?>
						<label class="label label-info"><?= $this->notification->get_state_detail($notification_detail['state_id'])['state_name']; ?></label>
					<?php endif; ?>
					<?php if($notification_detail['city_id'] > 0 ): ?>
						<label class="label label-danger"><?= $this->notification->get_city_detail($notification_detail['city_id'])['city_name']; ?></label> 
					<?php endif; ?>
				<?php endif; ?>
			
			<?php endif; ?>
			</p>
			<hr/>
			<p>
				<label class="col-md-2"> Notification:</label><br/>
				<p class="col-md-12">	<?= nl2br($notification_detail['notify_text']); ?></p>
			</p>
		<br/>
		</div>
			<div class="mail-attachments">
				
				<h4><i class="entypo-attach"></i> Attachment</h4>
			
				<?php if($notification_detail['attachement_url'] != "NULL"): ?>
				
					<ul>
						<li>												
							<a class="btn btn-success btn-sm btn-icon icon-left" href="<?= base_url(). $notification_detail['attachement_url'] ;?>" target="_blank"> 
								<i class="entypo-download"></i> Download
							</a>
							</div>
						</li>							
					</ul>
				<?php else: ?>
				<span class="label label-danger icon-left"><i class="entypo-cancel"></i>Not found</span>								
				<?php endif; ?>		
				
			</div>
		
		
	</div>
	
	
	
</div>
		
<hr />