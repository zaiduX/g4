		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li class="active">
				<strong>Skills</strong>
			</li>
		</ol>
		
		<h2 style="display: inline-block;">Skills</h2>
		<?php
		$per_skills = explode(',', $permissions[0]['skills_master']);
		if(in_array('2', $per_skills)) { ?>	
			<a type="button" href="<?= base_url('admin/add-skill'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
				Add New
				<i class="entypo-plus"></i>
			</a>
		<?php } ?>
	
		
		<table class="table table-bordered table-striped datatable" id="table-2">
			<thead>
				<tr>
					<th class="text-center">#</th>
					<th class="text-center">Skill Name</th>
					<th class="text-center">Skill Category</th>
					<th class="text-center">Status</th>
					<th class="text-center">Create Date Time</th>
					<?php if(in_array('3', $per_skills) || in_array('4', $per_skills) || in_array('5', $per_skills)) { ?>
						<th class="text-center">Actions</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>

				<?php $offset = $this->uri->segment(3,0) + 1; ?>
				<?php foreach ($skills as $skl) {  ?>
				<tr>
					<td class="text-center"><?= $offset++; ?></td>
					<td class="text-center"><?= $skl['skill_name'] ?></td>
					<td class="text-center"><?= $skl['cat_name'] ?></td>
					<?php if($skl['skill_status'] == 1): ?>
						<td class="text-center">
							<label class="label label-success" ><i class="entypo-thumbs-up"></i>Active</label>
						</td>
					<?php else: ?>
						<td class="text-center">
							<label class="label label-danger" ><i class="entypo-cancel"></i>Inactive</label>
						</td>
					<?php endif; ?>
					<td class="text-center"><?= date('d / M / Y H:i a', strtotime($skl['cre_datetime'])); ?></td>
					<?php if(in_array('3', $per_skills) || in_array('4', $per_skills) || in_array('5', $per_skills)) { ?>
						<td class="text-center">
							<?php if(in_array('3', $per_skills)) { ?>
								<form action="<?= base_url('admin/edit-skill'); ?>" method="post" class="col-sm-2">
									<input type="hidden" value="<?= $skl['skill_id']; ?>" id="skill_id" name="skill_id" />
									<button class="btn btn-primary btn-sm btn-icon icon-left" >
										<i class="entypo-pencil"></i>Edit</button>							
								</form>
							<?php } ?>
							<?php if(in_array('5', $per_skills)) { ?>
								<button class="btn btn-danger btn-sm btn-icon icon-left" onclick="delete_skill('<?= $skl["skill_id"]; ?>');">
									<i class="entypo-cancel"></i>
									Delete
								</button>


							<?php } ?>
						</td>
					<?php } ?>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
		<br />

	<script type="text/javascript">
		jQuery( document ).ready( function( $ ) {
				var $table2 = jQuery( '#table-2' );
				
				// Initialize DataTable
				$table2.DataTable( {
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true
				});
				
				// Initalize Select Dropdown after DataTables is created
				$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
					minimumResultsForSearch: -1
				});

				// Highlighted rows
				$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
					var $this = $(el),
						$p = $this.closest('tr');
					
					$( el ).on( 'change', function() {
						var is_checked = $this.is(':checked');
						
						$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
					} );
				} );
				
				// Replace Checboxes
				$table2.find( ".pagination a" ).click( function( ev ) {
					replaceCheckboxes();
				} );
			} );
	
		function delete_skill(id=0){		
			swal({
			  title: 'Are you sure?',
			  text: "You want to to delete this skill?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!',
			  cancelButtonText: 'No, cancel!',
			  confirmButtonClass: 'btn btn-success',
			  cancelButtonClass: 'btn btn-danger',
			  buttonsStyling: true,
			}).then(function () {
				$.post('skills-master/delete', {id: id}, function(res){
					if(res == 'success'){
					  swal(
					    'Deleted!',
					    'Skill has been deleted.',
					    'success'
					  ). then(function(){ 	window.location.reload();  });
					}
					else {
						swal(
					    'Failed!',
					    'Skill deletion failed.',
					    'error'
					  )
					}
				});
				
			}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
		}
	</script>