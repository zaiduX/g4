<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?= $this->config->item('resource_url') . 'images/152x152.png';?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $this->config->item('resource_url') . 'images/114x114.png';?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= $this->config->item('resource_url') . 'images/144x144.png';?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= $this->config->item('resource_url') . 'images/152x152.png';?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $this->config->item('resource_url') . 'images/72x72.png';?>">

    <title><?= $this->config->item('site_title'); ?> | Login</title>
    <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,400i" rel="stylesheet" />
    <link href="<?= $this->config->item('resource_url') . 'css/font-icons/entypo/css/entypo.css';?>" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= $this->config->item('resource_url') . 'css/neon-core.css';?>" rel="stylesheet" />
    <link href="<?= $this->config->item('resource_url') . 'css/neon-theme.css';?>" rel="stylesheet" />
    <link href="<?= $this->config->item('resource_url') . 'css/neon-forms.css';?>" rel="stylesheet" />
    <link href="<?= $this->config->item('resource_url') . 'css/custom.css';?>" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="//oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="page-body login-page login-form-fall" style="color:#fff;">

    <div class="login-container">
     
      <div class="login-header login-caret" style="padding: 5px;">
        <div class="login-content">
          <a href="<?= base_url('admin/'); ?>" class="logo"><img src="<?= $this->config->item('resource_url') .'images/login-logo.jpg'; ?>" width="300" height="120" alt="site logo" /></a>          
          <p class="description">Hi, Log-In to access the admin area!</p>
          <div class="login-progressbar-indicator"><h3>43%</h3><span>logging in...</span></div>
        </div>        
      </div>      
     
      <div class="login-progressbar"><div></div></div>   
      

      <div class="login-form">
    
    <div class="login-content">
      
      <div class="form-login-error">
        <h3>Invalid login</h3>
        <p>Enter <strong>Valid</strong> email address and password...</p>
      </div>
      
      <form role="form" id="form_login">
        
        <div class="form-group">
          
          <div class="input-group">
            <div class="input-group-addon"><i class="entypo-user"></i></div>            
            <input type="text" class="form-control" name="user_email" id="user_email" placeholder="Email Address" autocomplete="off" autofocus value="info@gonagoo.com" />
          </div>
          
        </div>
        
        <div class="form-group">          
          <div class="input-group">
            <div class="input-group-addon"><i class="entypo-key"></i></div>
            <input type="password" class="form-control" name="user_pass" id="user_pass" placeholder="Password" autocomplete="off" />
          </div>        
        </div>
        
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-login"><i class="entypo-login"></i>Log In</button>
        </div>
        
      </form>
        
      <div class="login-bottom-links">        
        <a href="admin/forgot-password" class="link">Forgot your password?</a>
      </div>
    
    </div>
    
  </div>


    </div>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?= $this->config->item('resource_url'). 'js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js'; ?>"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
    <script src="<?= $this->config->item('resource_url'). 'js/gsap/TweenMax.min.js'; ?>"></script>
    <script src="<?= $this->config->item('resource_url'). 'js/neon-login.js'; ?>"></script>

  </body>
</html>

<!-- Latest compiled and minified CSS -->

<!-- Optional theme -->

<!-- Latest compiled and minified JavaScript -->