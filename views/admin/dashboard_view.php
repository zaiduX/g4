<div class="row">
  <form action="<?= base_url('admin/dashboard'); ?>" method="POST" id="frmApplyFilter">
    <div class="col-sm-3 col-xs-6">
      <label for="category" class="control-label">Select Period <small>(YYYY-MM-DD)</small></label> 
      <div class="daterange daterange-inline add-ranges" data-format="YYYY-MM-DD" data-start-date="<?=(isset($dateFrom))?$dateFrom:date('Y-1-d')?>" data-end-date="<?=(isset($dateTo))?$dateTo:date('Y-m-d')?>">
        <i class="entypo-calendar"></i>
        <span id="from_to_date"><?=(isset($dateFrom))?$dateFrom:date('Y-1-d')?> - <?=(isset($dateTo))?$dateTo:date('Y-m-d')?></span>
      </div>
      <input type="hidden" id="dateRange" name="dateRange" />
    </div>
    <div class="col-sm-3 col-xs-6">
      <label for="country_id" class="control-label">Select Country </label> 
      <select id="country_id" name="country_id" class="form-control select2">
        <option value="0">Select Country</option>
        <?php foreach ($countries as $country): ?> 
          <option value="<?= $country['country_id'] ?>" <?php if(isset($country_id) &&  $country['country_id'] == $country_id) { echo 'selected'; } ?>><?= $country['country_name']; ?></option>
        <?php endforeach ?>
      </select>
    </div>
    <div class="col-sm-3 col-xs-6">
      <label for="category" class="control-label">Select Category </label> 
      <select id="cat_id" name="cat_id" class="form-control select2" placeholder="Select Category ">
        <option value="0">Select Category</option>
        <?php foreach ($categories as $c ): ?>
          <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
            <option value="<?= $c['cat_id']?>" <?php if(isset($cat_id) && $cat_id == $c['cat_id']) { echo 'selected'; } ?> ><?= $c['cat_name']?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="col-sm-2 col-xs-6">
      <br />
      <button type="button" id="btnFilter" class="btn btn-info">Apply</button>
      <a href="<?=base_url('admin/dashboard')?>" id="btnReset" class="btn btn-warning">Clear</a>
    </div>
  </form>
</div>
<hr />

<div class="row">

  <div class="col-sm-3 col-xs-6">
    <div class="tile-stats tile-blue" style="min-height: 147px;">
      <div class="icon"><i class="fa fa-money"></i></div>
      <h3 style="margin-bottom: 15px;">Earned to date<br />Gonagoo Balance</h3>
      <?php foreach ($gonagoo_master as $mvalue) { ?>
      <a href="<?= base_url('admin/currencywise-account-history/'.$mvalue['currency_code']); ?>">
        <div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$mvalue['currency_code']?></div></div>
        <div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;"><?=$this->admin->convert_big_int($mvalue['gonagoo_balance'])?></div></div>
      </a>
      <?php } ?>
    </div>
  </div>

  <div class="col-sm-3 col-xs-6">
    <div class="tile-stats tile-aqua" style="min-height: 147px;">
      <div class="icon"><i class="fa fa-money"></i></div>
      <h3 style="margin-bottom: 15px;">Filtered<br />Gonagoo Commission</h3>
      <?php foreach ($gonagoo_commission as $cvalue) { ?>
      <a href="<?= base_url('admin/currencywise-gonagoo-comission-history/'.$cvalue['currency_code']); ?>">
        <div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$cvalue['currency_code']?></div></div>
        <div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;"><?=$this->admin->convert_big_int($cvalue['amount'])?></div></div>
      </a>
      <?php } ?>
    </div>
  </div>

  <div class="col-sm-3 col-xs-6">
    <div class="tile-stats tile-green" style="min-height: 147px;">
      <div class="icon"><i class="fa fa-money"></i></div>
      <h3 style="margin-bottom: 15px;">Filtered<br />Insurance</h3>
      <?php foreach ($gonagoo_insurance as $ivalue) { ?>
      <a href="<?= base_url('admin/currencywise-insurance-history/'.$ivalue['currency_code']); ?>">
        <div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$ivalue['currency_code']?></div></div>
        <div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;"><?=$this->admin->convert_big_int($ivalue['amount'])?></div></div>
      </a>
      <?php } ?>
    </div>
  </div>

  <div class="col-sm-3 col-xs-6">
    <div class="tile-stats tile-red" style="min-height: 147px;">
      <div class="icon"><i class="fa fa-money"></i></div>
      <h3 style="margin-bottom: 15px;">Filtered<br />Custom</h3>
      <?php foreach ($gonagoo_custom as $cuvalue) { ?>
      <a href="<?= base_url('admin/currencywise-custom-history/'.$cuvalue['currency_code']); ?>">
        <div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$cuvalue['currency_code']?></div></div>
        <div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;"><?=$this->admin->convert_big_int($cuvalue['amount'])?></div></div>
      </a>
      <?php } ?>
    </div>
  </div>

  <div class="col-sm-3 col-xs-6">
    <div class="tile-stats tile-blue" style="min-height: 147px;">
      <div class="icon"><i class="fa fa-money"></i></div>
      <h3 style="margin-bottom: 15px;">Paid to date<br />Total Refund</h3>
      <?php foreach ($gonagoo_refund as $rvalue) { ?>
      <a href="<?= base_url('admin/currencywise-refund-history/'.$rvalue['currency_code']); ?>">
        <div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$rvalue['currency_code']?></div></div>
        <div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;"><?=$this->admin->convert_big_int($rvalue['amount_transferred'])?></div></div>
      </a>
      <?php } ?>
    </div>
  </div>

</div>
<br />

<div class="rows">
  <div class="col-md-12">
    <h3 style="margin-top: 0px;">Recent Transactions <a href="<?= base_url('admin/gonagoo-account-history'); ?>" style="float: right" class="btn btn-outline" ><i class="fa fa-money"></i> View All Transaction </a></h3>
  </div>
</div>    

<table id="" class="table table-striped table-bordered table-hover" style="width:100%">
  <thead>
    <tr>
      <th>Tr. Id</th>
      <th>Date Time</th>
      <th>Order ID</th>
      <th>Transaction</th>
      <th>Amount</th>
      <th>+/-</th>
      <th>Balance</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($history as $histry) { ?>
    <tr style="background-color: <?= $histry['type'] == '1' ? '#DFFFDF': '#FFDDDD'; ?>">
      <td><?= $histry['gh_id'] ?></td>
      <td><?= date('d-M-Y h:i:s A',strtotime($histry['datetime'])) ?></td>
      <td><a href="<?= base_url('admin/view-order-details/').$histry['order_id'] ?>"><?= $histry['order_id'] ?></td>
      <td><?= ucwords(str_replace('_',' ',$histry['transaction_type'])) ?></td>
      <td><?= $histry['currency_code'] . ' ' . $histry['amount'] ?></td>
      <td><?= $histry['type'] == '1' ? '<i class="fa fa-plus" style="color: green"></i>' : '<i class="fa fa-minus" style="color: red"></i>'; ?></td>
      <td><?= $histry['currency_code'] . ' ' .  $histry['gonagoo_balance'] ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#tableData' );
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true
    });
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });
    // Highlighted rows
    $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
      var $this = $(el),
        $p = $this.closest('tr');
      $( el ).on( 'change', function() {
        var is_checked = $this.is(':checked');
        $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
      } );
    } );
    // Replace Checboxes
    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );
  } );
</script>

<script>
  $(function(){
    $("#dateRange").val("<?=(isset($dateFrom))?$dateFrom:date('Y-1-d')?> - <?=(isset($dateTo))?$dateTo:date('Y-m-d')?>");
    
    $("#btnFilter").on('click', function(event) {event.preventDefault();
      var country_id = $("#country_id").val();
      var cat_id = $("#cat_id").val();
      var year = $("#year").val();      
      var err = 'No filter selected!';
     if(country_id =='0' && cat_id == '0' && year == '0'){
        $("#error_filter").addClass('text-danger error text-center').css({"display":"inline-block","font-size":"13px","margin-top":"-10px"}).html(err) +"";
      } else{ 
        $("#error_filter").html("");  
        $("#frmApplyFilter").get(0).submit();
      }
    });

    $('.select2').on('change', function() {
      $("#error_filter").html("");  
    });

    $("span").on('DOMSubtreeModified', function () {
      $("#dateRange").val($(this).html());
    }); 
  });
</script>