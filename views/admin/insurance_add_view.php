    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li>
        <a href="<?= base_url('admin/insurance-master'); ?>">Insurance</a>
      </li>
      <li class="active">
        <strong>Add New</strong>
      </li>
    </ol>
    <style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
    <div class="row">
      <div class="col-md-12">
        
        <div class="panel panel-dark" data-collapsed="0">
        
          <div class="panel-heading">
            <div class="panel-title">
              Add Insurance
            </div>
            
            <div class="panel-options">
              <a href="<?= base_url('admin/insurance-master'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
            </div>
          </div>
          
          <div class="panel-body">
            
            <?php if($this->session->flashdata('error')):  ?>
              <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

            <form role="form" class="form-horizontal form-groups-bordered" id="from_addinsurance" action="<?= base_url('admin/register-insurance'); ?>" method="post">
              
              <div class="border">
                <div class="row">
                  <div class="col-md-4">
                    <label for="country_id" class="control-label">Select Country </label> 
                    <select id="country_id" name="country_id" class="form-control select2">
                      <option value="">Select Country</option>
                      <?php foreach ($countries as $country): ?> 
                        <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="col-md-8">
                    <label for="category" class="control-label">Select Category </label> 
                    <select id="category" name="category[]" class="form-control select2" multiple placeholder="Select Category ">
                      <?php foreach ($categories as $c ): ?>
                        <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
                          <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>             
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="border">
                <div class="row">

                  <div class="col-md-3">
                    <label for="min_value" class="control-label">Minimum Value</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="min_value" placeholder="Enter minimum value" name="min_value" min="1" />
                      <span class="input-group-addon">#</span>
                    </div>                        
                    <div class="clear"></div><br />
                  </div>

                  <div class="col-md-3">
                    <label for="max_value" class="control-label">Maximum Value</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="max_value" placeholder="Enter maximum value" name="max_value" min="1" />  
                      <span class="input-group-addon">#</span>
                    </div>                        
                    <div class="clear"></div><br />
                  </div>

                  <div class="col-md-3">
                    <label for="ins_fee" class="control-label">Insurance Fee</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="ins_fee" placeholder="Enter insurance fee" name="ins_fee" min="0" />
                      <span class="input-group-addon">#</span>
                    </div>                        
                    <div class="clear"></div><br />
                  </div>

                  <div class="col-md-3">
                    <label for="commission" class="control-label">Commission Percentage</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="commission" placeholder="Enter insurance fee" name="commission" min="0" max="100" />
                      <span class="input-group-addon">%</span>
                    </div>                        
                    <div class="clear"></div><br />
                  </div>
                  
                </div>
              </div>                        
              
              <div class="row form-group">
                <div class="text-center">
                  <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
                </div>
              </div>
            </form>
            
          </div>
        
        </div>
      
      </div>
    </div>
    
    <br />

    <script>
      $(function(){
        $("#from_addinsurance").submit(function(e) { e.preventDefault(); });

        $("#btn_submit").on('click', function(e) {  e.preventDefault();

          var country_id = $("#country_id").val();
          var category = $("#category").val();
          var min_value = parseFloat($("#min_value").val()).toFixed(2);
          var max_value = parseFloat($("#max_value").val()).toFixed(2);    
          var ins_fee = parseFloat($("#ins_fee").val()).toFixed(2);
          var commission = parseFloat($("#commission").val()).toFixed(2);   

          if(country_id <= 0 ) {  swal('Error','Please Select Country','warning');   } 
          else if(!category) {  swal('Error','Please Select at least one Category','warning');   }       
          
          else if(min_value < 1) {  swal('Error','Enter valid minimum value ( greater than zero )!','warning');   } 
          else if(isNaN(min_value)) {  swal('Error','Enter valid minimum value! Enter Number Only.','warning');   } 

          else if(max_value < 1) {  swal('Error','Enter valid maximum value ( greater than zero )!','warning');   } 
          else if(isNaN(max_value)) {  swal('Error','Enter valid maximum value! Enter Number Only.','warning');   } 
          
          else if(max_value <= min_value) {  swal('Error','Enter maximum value greater than minimum value!','warning');   } 

          else if(ins_fee < 0) {  swal('Error','Enter valid Insurance Fee!','warning');   } 
          else if(isNaN(ins_fee)) {  swal('Error','Enter valid Insurance Fee! Enter Number Only.','warning');   } 

          else if(isNaN(commission)) {  swal('Error','Enter valid Commission Percentage! Enter Number Only.','warning');   } 
          else if(commission < 0 && commission > 100) {  swal('Error','Invalid Commission Percentage! Should be greater than or equal to zero and less than 100.','warning');   }       
          
          else {  $("#from_addinsurance")[0].submit();  }

        });

      });
    </script>