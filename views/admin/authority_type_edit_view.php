		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li>
				<a href="<?= base_url('admin/authority-type'); ?>">Authority Type List</a>
			</li>
			<li class="active">
				<strong>Edit</strong>
			</li>
		</ol>
	
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-dark" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							Edit Authority Type
						</div>
						
						<div class="panel-options">
							<a href="<?= base_url('admin/authority-type'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
						</div>
					</div>
					
					<div class="panel-body">
						
						<?php if($this->session->flashdata('error')): ?>
				      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
				    <?php elseif($this->session->flashdata('success')):  ?>
				    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
				    <?php endif; ?>

						<form role="form" class="form-horizontal form-groups-bordered" id="authorityType" method="post" action="<?= base_url('admin/update-authority-type'); ?>">
							<input type="hidden" id="auth_type_id" name="auth_type_id" value="<?= $auth_type['auth_type_id']; ?>" />
							<div class="form-group">
								<label for="auth_type" class="col-sm-3 control-label">Authority Type</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="auth_type" placeholder="Enter authority Name" name="auth_type" value="<?= $auth_type['auth_type']; ?>" />
								</div>
							</div>												
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit" class="btn btn-blue">Save Changes</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		<br />