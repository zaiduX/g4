

<ol class="breadcrumb bc-3">
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/get-all-laundry-orders'); ?>">Laundry Order List</a>
  </li>
  <li class="active">
    <strong>Laundry Order Details</strong>
  </li>
</ol>

<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel hblue">   
        <div class="panel-body"> 
          <div id="collapse1" class="panel-collapse collapse in">
            <div class="profile-env">
              <header class="row" style="margin-top: 5px;">    
                <div class="col-md-2">
                  <?php if($laundry_cust['avatar_url'] != "NULL"): ?>
                    <a class="profile-picture">
                      <img src="<?= $this->config->item('base_url') . $laundry_cust['avatar_url'];?>" style="height: 150px" class="img-responsive img-thumbnail" />
                    </a>
                  <?php else: ?>
                    <a class="profile-picture">
                      <img src="<?= $this->config->item('resource_url').'default-profile.jpg';?>" style="height: 150px" class="img-responsive img-thumbnail" />
                    </a>
                  <?php endif; ?>                 
                </div>
                <div class="col-md-10">
                  <ul class="profile-info-sections">
                    <li style="padding-left: 15px; padding-right: 15px">
                      <div class="profile-name">
                        <strong>
                          <h5>Customer</h5>
                          <a><i class="fa fa-user"></i> <?=$booking_details['cust_name']?></a><br />
                          <a><i class="fa fa-phone"></i> <?= $laundry_cust['mobile1']?></a>
                        </strong>
                    </li>
                    <li style="padding-left: 15px; padding-right: 15px">
                      <div class="profile-name">
                        <strong>
                          <h5>Laundry Provider</h5>
                          <a><i class="fa fa-user"></i> <?=$laundry_provider['company_name']?> (<?= $laundry_provider['firstname']." ".$laundry_provider['lastname'] ?>)</a><br />
                          <a><i class="fa fa-phone"></i> <?= $laundry_provider['contact_no'] ?></a>
                        </strong>
                      </div>
                    </li>
                  </ul>
                </div>
              </header>
              <section class="profile-info-tabs" style="margin-bottom: 10px;">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
                    <ul class="user-details">
                      <li>
                        <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Order Status">
                          <i class="fa fa-play"></i>Order Status - <span> <?= strtoupper($booking_details['booking_status']); ?></span>
                        </a>
                      </li>
                      <li>
                        <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Status Update on">
                            <i class="fa fa-calendar"></i> Status Updated On - 
                            <?php if($booking_details['status_update_datetime'] !== "NULL") echo date('l, d M Y', strtotime($booking_details['status_update_datetime'])); else echo 'Not provided!'; ?>
                        </a>
                      </li>
                      <li>
                        <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Expected Return">
                            <i class="fa fa-calendar"></i> Expected Return Date - <span>
                            <?php if($booking_details['expected_return_date'] !== "NULL") echo date('l, d M Y', strtotime($booking_details['expected_return_date'])); else echo 'Not provided!'; ?></span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </section>
              <div class="story-content"> 
                <div class="col-md-12">
                  <i class="fa fa-archive"></i> <a>Laundry Package Details:</a>
                </div>
                <div class="col-md-12">
                  <table id="tableData" class="table table-striped table-bordered table-hover">
                    <tr>
                      <th><font color="#ec5956">Category</font></th>
                      <th><font color="#ec5956">Sub Category</font></th>
                      <th><font color="#ec5956">Quantity</font></th>
                      <th><font color="#ec5956">Single Unit Price</font></th>
                      <th><font color="#ec5956">Total Price</font></th>
                    </tr>
                    <?php foreach ($booking_details_cart as $cart) { ?>
                      <tr>
                        <td>
                          <?php 
                            $cat = $this->admin->get_laundry_category($cart['cat_id']);
                            echo $cat['cat_name'];
                          ?>
                        </td>
                        <td>
                          <?php 
                            $sub = $this->admin->get_sub_category_details($cart['sub_cat_id']);
                            echo $sub['sub_cat_name'];
                          ?>
                        </td>
                        <td><?=$cart['quantity']?></td>
                        <td><?=$cart['single_unit_price']?></td>
                        <td><?=$cart['total_price']." ".$cart['currency_sign']?></td>
                      </tr>
                    <?php } ?>
                    <tr>
                        <td><font color="#ec5956">Grand Total</font></td>
                        <td></td><td></td><td></td>
                        <td><font color="#ec5956"><?=$booking_details['booking_price']." ".$booking_details['currency_sign']?></font></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>