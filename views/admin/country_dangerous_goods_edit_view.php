<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/advance-payment'); ?>">Country-wise Dangerous Goods</a>
  </li>
  <li class="active">
    <strong>Edit Country-wise Dangerous Goods</strong>
  </li>
</ol>
<style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
         Edit Dangerous Goods
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/country-dangerous-goods'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/country-dangerous-goods/update'); ?>" class="form-horizontal form-groups-bordered validate" id="country_dangerous_goods_edit" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="id" value="<?= $cg['id'];?>" />
          <div class="border">
            <div class="row">
              <div class="col-md-5">
                <label for="country_id" class="control-label">Select Country </label>                        
                <input type="text" class="form-control" value="<?= $cg['country_name'];?>" readonly/>                
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label for="added_goods" class="control-label">Select Dangerous Goods </label>              
                <select id="added_goods" name="added_goods[]" class="form-control select2" multiple placeholder="Dangerous Goods" disabled>
                  <?php $cg_ids = explode(',',$cg['goods_ids']); ?>
                  <?php  foreach ($goods as $g):  ?>
                    <option value="<?= $g['id']?>" <?= (in_array($g['id'], $cg_ids)) ? "selected":"";?> ><?= $g['name'].' ['.$g['code'].'] ';?></option>                    
                  <?php endforeach; ?>
                </select>
              </div>             
            </div>
            <div class="clear"></div><br />
            
            <div class="row">
              <div class="col-md-12">
                <div class="text-center">
                  <a href="<?= base_url('admin/country-dangerous-goods'); ?>" class="btn btn-info">&nbsp;&nbsp; Back &nbsp;&nbsp;</a> &nbsp;&nbsp;
                  <button id="btn_edit" class="btn btn-green">&nbsp;&nbsp; Edit &nbsp;&nbsp;</button>
                  <button id="btn_submit" type="submit" class="btn btn-blue hidden">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
                </div>
              </div>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $("#btn_edit").on('click', function(){
    $(this).addClass('hidden');
    $("#btn_submit").removeClass('hidden');
    $("#country_dangerous_goods_edit select").prop('disabled', false);
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });

  var somethingChanged = false;
  $('#country_dangerous_goods_edit select').change(function() {  somethingChanged = true; });

  $("#country_dangerous_goods_edit").submit(function(e) { e.preventDefault(); });
  $("#btn_submit").on('click', function(e) {  e.preventDefault();
    var added_goods = $("#added_goods").val();
    if(!added_goods) { swal('Error','Select Dangerous Goods','error');}
    else{ $("#country_dangerous_goods_edit").get(0).submit();}

  });
</script>