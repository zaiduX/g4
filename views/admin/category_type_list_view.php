<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Categories Type List</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">Categories Type List</h2>
<?php
$per_category_type = explode(',', $permissions[0]['category_type']);
if(in_array('2', $per_category_type)) { ?>
	<a type="button" href="<?= base_url('admin/add-category-type'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add New
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Category Type</th>
			<th class="text-center">Status</th>
			<th class="text-center">Create Date Time</th>
			<?php if(in_array('3', $per_category_type) || in_array('4', $per_category_type) || in_array('5', $per_category_type)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($category_type as $cat_type):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= $cat_type['cat_type'] ?></td>
			<?php if($cat_type['cat_type_status'] == 1): ?>
				<td class="text-center">
					<label class="label label-success" ><i class="entypo-thumbs-up"></i>Active</label>
				</td>
			<?php else: ?>
				<td class="text-center">
					<label class="label label-danger" ><i class="entypo-cancel"></i>Inactive</label>
				</td>
			<?php endif; ?>
			<td class="text-center"><?= date('d / M / Y H:i a', strtotime($cat_type['cre_datetime'])); ?></td>
			<?php if(in_array('3', $per_category_type) || in_array('4', $per_category_type) || in_array('5', $per_category_type)) { ?>
				<td class="text-center">
					<?php if(in_array('3', $per_category_type)) { ?>
						<a href="<?= base_url('admin/edit-category-type/') . $cat_type['cat_type_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
					<?php } ?>
					<?php if(in_array('4', $per_category_type)) { ?>
						<?php if($cat_type['cat_type_status'] == 0): ?>
							<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to activate" onclick="inactivate_category_type('<?= $cat_type['cat_type_id']; ?>');">
								<i class="entypo-thumbs-up"></i>Activate</button>
						<?php else: ?>
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to inactivate" onclick="activate_category_type('<?= $cat_type['cat_type_id']; ?>');">
							<i class="entypo-cancel"></i>
							Inactivate</button>			
						<?php endif; ?>
					<?php } ?>
				</td>
			<?php } ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );

		

	} );

	function activate_category_type(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this category type?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('inactive-category-type', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'Category type has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Category type inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate_category_type(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this category type?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('active-category-type', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Category type has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Category type activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>