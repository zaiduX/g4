		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li class="active">
				<strong>Edit</strong>
			</li>
		</ol>
	
		<div class="row">
			<div class="col-md-7">
				<div class="panel panel-dark" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							Edit Profile Details
						</div>
						
						<div class="panel-options">
							<a href="<?= base_url('admin/dashboard'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
						</div>
					</div>
					
					<div class="panel-body">
						
						<?php if($this->session->flashdata('error')): ?>
				      	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
				    <?php elseif($this->session->flashdata('success')):  ?>
				    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
				    <?php endif; ?>

						<form role="form" class="form-horizontal form-groups-bordered" id="profileEdit" method="post" action="<?= base_url('admin/update-profile'); ?>" enctype="multipart/form-data">
							<input type="hidden" id="auth_id" name="auth_id" value="<?= $user['auth_id']; ?>" />

						<div class="form-group">
							<label for="auth_fname" class="col-sm-4 control-label">First Name</label>
							
							<div class="col-sm-8">
								<input type="text" class="form-control" id="auth_fname" placeholder="Enter authority first name..." name="auth_fname" value="<?= $user['fname']; ?>" />
							</div>
						</div>

						<div class="form-group">
							<label for="auth_lname" class="col-sm-4 control-label">Last Name</label>
							
							<div class="col-sm-8">
								<input type="text" class="form-control" id="auth_lname" placeholder="Enter authority last name..." name="auth_lname" value="<?= $user['lname']; ?>" />	
							</div>
						</div>
							<div class="form-group">
								<label for="auth_email" class="col-sm-4 control-label">Email Address</label>
								<?php if($user['auth_id'] != 1) { ?>
									<label for="auth_email" class="col-sm-8 control-label" style="text-align: left;"><?= $user['email']; ?></label>
								<?php } else { ?>
									<div class="col-sm-8">
									<input type="email" class="form-control" id="auth_email" placeholder="Enter authority email..." name="auth_email" value="<?= $user['email']; ?>" />	
									</div>
								<?php } ?>
							</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Profile Picture</label>
							
							<div class="col-sm-8">
								
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
										<img src="<?php if($user['avatar_url'] != 'NULL') { echo base_url($user['avatar_url']); } else { echo "http://placehold.it/200x150"; } ?>" alt="Profile Picture">
									</div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
									<div>
										<span class="btn btn-white btn-file">
											<span class="fileinput-new">Select image</span>
											<span class="fileinput-exists">Change</span>
											<input type="file" name="profile_image" accept="image/*">
										</span>
										<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
									</div>
								</div>
								
							</div>
						</div>

						<div class="form-group text-center">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-blue">Save Changes</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>

			<div class="col-md-5">
				<div class="panel panel-dark" data-collapsed="0">	
					<div class="panel-heading">
						<div class="panel-title">
							Change Password
						</div>
						
						<div class="panel-options">
							<a href="<?= base_url('admin/dashboard'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
						</div>
					</div>

					<div class="panel-body">
						<?php if($this->session->flashdata('error1')): ?>
				      	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error1'); ?></div>
				    <?php elseif($this->session->flashdata('success1')):  ?>
				    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success1'); ?></div>
				    <?php endif; ?>
						<form role="form" class="form-horizontal form-groups-bordered" id="profilePasswordEdit" method="post" action="<?= base_url('admin/update-profile-password'); ?>">
							<input type="hidden" id="auth_id" name="auth_id" value="<?= $user['auth_id']; ?>" />
						
						<div class="form-group">
							<label for="auth_pass" class="col-sm-5 control-label">Old Password</label>
							
							<div class="col-sm-7">
								<input type="password" class="form-control" id="auth_old_pass" placeholder="Enter Old password..." name="auth_old_pass" />	
							</div>
						</div>
						
						<div class="form-group">
							<label for="auth_pass" class="col-sm-5 control-label">Password</label>
							
							<div class="col-sm-7">
								<input type="password" class="form-control" id="auth_pass" placeholder="Enter new password..." name="auth_pass" />	
							</div>
						</div>

						<div class="form-group">
							<label for="auth_pass" class="col-sm-5 control-label">Cofirm Password</label>
							
							<div class="col-sm-7">
								<input type="password" class="form-control" id="auth_repass" placeholder="Retype new password..." name="auth_repass" />	
							</div>
						</div>

						<div class="form-group text-right">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-blue">Update Password</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>

		</div>
		
		<br />