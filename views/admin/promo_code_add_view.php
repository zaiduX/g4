<ol class="breadcrumb bc-3" style="margin-bottom: 0px;">
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/promo-code'); ?>">Promocode Master</a>
	</li>
	<li class="active">
		<strong>Add New</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">	
		<div class="panel panel-dark" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					Add Promocode
				</div>
				<div class="panel-options">
					<a href="<?= base_url('admin/promo-code'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>	
			<div class="panel-body">
				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				<form role="form" action="<?= base_url('admin/register-promocode'); ?>" class="form-horizontal form-groups-bordered" id="add_promocode_form" method="post" autocomplete="off" enctype="multipart/form-data">
					<div class="form-group">
						<div class="col-sm-12">
							<div class="col-sm-8" style="padding-left:0px; padding-right:0px">
								<div class="col-sm-4 text-center" style="padding-left:0px; padding-right:0px">
									<label class="control-label">Upload Promocode Picture</label>
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
											<img src="http://placehold.it/200x150" alt="Profile Picture">
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select Image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="promo_code_image" accept="image/*">
											</span>
											<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
										</div>
									</div>
								</div>
								<div class="col-sm-8" style="padding-left:0px; padding-right:0px">
									<div class="col-sm-12">
						                <label for="Promocode" class="control-label">Promocode</label>
						                <div class="input-group">
						                  <input type="text" class="form-control" id="promo_code" placeholder="Enter Promocode" name="promo_code" maxlength="16" style="text-transform:uppercase"/>
						                  <span class="input-group-addon">#</span>
						                </div>                        
						                <label class="text-center" id="promo_code_error" style="color:red; display:flex;"></label>
						            </div>                    
									<div class="col-sm-12">
										<label for="promocode_title" class="control-label">Promocode Title</label>
										<input type="text" class="form-control" id="promocode_title" placeholder="Enter Promocode Title" name="promocode_title"/>
									</div>
									<div class="col-sm-6">
						                <label for="example-date-input" style="padding-top:7px; margin:0px"><i class="fa fa-calendar"></i>Start Date</label>
						                <div class="input-group">
						                  <input type="text" class="form-control" placeholder="Start Date" id="start_date1" name="start_date" autocomplete="off" >
						                  <div class="input-group-addon dpAddon">
						                    <span class="glyphicon glyphicon-th"></span>
						                  </div>
						                </div>
									</div>
									<div class="col-sm-6">
						                <label for="example-date-input" style="padding-top:7px; margin:0px"><i class="fa fa-calendar"></i>Expire Date</label>
						                <div class="input-group">
						                  <input type="text" class="form-control" placeholder="End Date" id="expire_date1" name="expire_date" autocomplete="off" >
						                  <div class="input-group-addon ddAddon">
						                    <span class="glyphicon glyphicon-th"></span>
						                  </div>
						                </div>
									</div>
								</div>
								<div class="col-sm-12">
									<label for="Description" class="control-label"> Description </label>
									<textarea class="form-control" id="description" placeholder="Enter Description" name="description"></textarea>
								</div>
							</div>
							<div class="col-sm-4" style="padding-left:0px; padding-right:0px">
								<div class="col-sm-12">
					                <label for="discount_percent" class="control-label">Discount Percent</label>
					                <div class="input-group">
					                  <input type="number" class="form-control" id="discount_percent" placeholder="Enter Discount Percent" name="discount_percent"/>
					                  <span class="input-group-addon">%</span>
					                </div>    
					            </div>
					            <div class="col-sm-12">
					                <label for="limit" class="control-label">Maximum number of orders (Optional)</label>
					                <div class="input-group">
					                  <input type="number" class="form-control" id="limit" placeholder="Enter Limit" name="limit"/>
					                  <span class="input-group-addon">#</span>
					                </div>    
					            </div>
	              				<div class="col-sm-12">
									<label for="category" class="control-label">Select Category </label> 
								  	<select id="service_id" name="service_id[]" class="form-control select2" placeholder="Select Services" multiple="multiple">
								    <?php foreach ($categories as $c ): ?>
								        <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
								    <?php endforeach; ?>
								  	</select>
								</div>
							</div>			
							<div class="col-sm-1 col-sm-offset-10">							
								<br/><button type="submit" class="btn btn-blue">Add Promocode</button>							
							</div>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	</div>
</div>

<br />


<script>
  // $(function() {
  //   $('#datepicker').datepicker( {
  //   	startDate1: "+0d", autoclose: true, 
  //   } );
  // });

$('#add_promocode_form').validate({
    rules: {
        discount_percent: {
          required: true,
          minlength: 1,
          maxlength: 3,
          max:100,
        },
    		promo_code:{
    			required: true,
    		},
    		promocode_title:{
    			required: true
    		},
    		service_id:{
    			required: true
    		},
    		start_date1:{
    			required: true
    		},
    		expire_date1:{
    			required: true
    		},
    },
    messages:{
    	 promo_code: "Enter Promocode In",
    	 discount_percent: "Enter Percentage Of Discount 0 To 100 %",
    	 promocode_title: "Enter Title Of Promocode",
    	 service_id: "Select At least One Service",
    	 start_date1: "Select Start Date",
    	 expire_date1: "Select Expire Date",
    }
});

</script>

<script>
$('#promo_code').focusout(function() {
    var promo_code = $('#promo_code').val();
    $.ajax({
        type: "POST", 
        url: "check-promo-code-exists", 
        data: { promo_code:promo_code },
        dataType: "json",
        success: function(res){   
           console.log(res);
           if(res == 1) { $('#promo_code').val(''); $('#promo_code_error').text('Promocode Already exists');
            $('#promo_code_error').focus(); 
          }
           else {$('#promo_code_error').text(""); }
        },
        beforeSend: function(){ },
        error: function(){ }
    });
});
</script>

<script>
    $(document).ready(function() {
      $('#start_date1').datepicker({
        startDate: new Date(),
        autoclose: true,
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('#expire_date1').datepicker({
            startDate: minDate,
            autoclose: true,
          });     
        });
    });

    $(".dpAddon" ).click(function( e ) { 
      $("#start_date1").focus();
    });
    $(".ddAddon" ).click(function( e ) { 
      $("#expire_date1").focus();
    });
</script>
