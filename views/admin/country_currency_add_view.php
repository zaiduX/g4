<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/country-currency'); ?>">Country Currency</a>
	</li>
	<li class="active">
		<strong>Add New</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Add Country Currency
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/country-currency'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" class="form-horizontal form-groups-bordered" id="addCountryCurrency" method="post" action="<?= base_url('admin/register-country-currency'); ?>">
	
					<div class="form-group">
						<label class="col-sm-3 control-label">Select Country</label>
						
						<div class="col-sm-5">
							
							<select id="country_id" name="country_id" class="form-control">
								<option value="0">Select Country</option>
								<?php foreach ($countries_list as $country): ?>											
									<option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
								<?php endforeach ?>
							</select>
							
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Select Currencies</label>
						
						<div class="col-sm-5">
							
							<select id="currency_id" name="currency_id" class="form-control">
									<option value="0"> Select Currency</option>
								<?php foreach ($currencies_list as $currencies): ?>											
									<option value="<?= $currencies['currency_id'] ?>"><?= $currencies['currency_title']; ?></option>
								<?php endforeach ?>
							</select>
							
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-blue">Add Currency</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />

<script type="text/javascript">
$(document).ready(function () {
    $("#addCountryCurrency").submit(function (e) {
    	$("#countryAlert").remove();
    	$("#currencyAlert").remove();
        if ($('#country_id').val() == '0') {
            $('#country_id').parent().append('<span class="error" id="countryAlert">Please select country.</span>');
        	e.preventDefault(e);
        	$("#country_id").focus();
        	$('#countryAlert').delay(2000).slideUp(500, function() {
				$("#countryAlert").remove();
			});
        }
        else if ($('#currency_id').val() == null) {
            $('#currency_id').parent().append('<span class="error" id="currencyAlert">Please select atleast 1 currency.</span>');
        	e.preventDefault(e);
        	$("#currency_id").focus();
        	$('#currencyAlert').delay(2000).slideUp(500, function() {
				$("#currencyAlert").remove();
			});
        }
        else {
        	$("#addCountryCurrency").submit();
        }
    });
});
</script>