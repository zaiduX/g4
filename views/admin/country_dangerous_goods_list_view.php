<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Country-wise Dangerous Goods List</strong>
  </li>
</ol>
      
<h2 style="display: inline-block;">Country-wise Dangerous Goods List</h2>
<?php
$per_dangerous_goods = explode(',', $permissions[0]['dangerous_goods']);

if(in_array('2', $per_dangerous_goods)): ?>
    <a type="button" href="<?= base_url('admin/country-dangerous-goods/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
      <i class="entypo-plus"></i> Add Dangerous Goods
    </a>    
<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      <th class="text-center">#</th>
      <th class="text-center">Country</th>
      <th class="text-center">Goods</th>
      <?php if(in_array('3', $per_dangerous_goods) || in_array('5', $per_dangerous_goods)) { ?>
        <th class="text-center">Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>
    <?php $offset = $this->uri->segment(3,0) + 1; ?>
    <?php foreach ($goods as $g):  ?>      
      <tr>
        <td class="text-center"><?= $offset++; ?></td>
        <td class="text-center"><?= $g['country_name']; ?></td>  
        <?php 
          $goods_ids = explode(',',$g['goods_ids']);
          $counter = COUNT($goods_ids);
          $goods_name = ""; $i = 1;
          foreach ($goods_ids as $gid) {
            $detail = $this->goods->get_dangerous_goods($gid);
            $goods_name .= $detail['name'].'['.$detail['code'].'] ';
            if($counter > $i){ $goods_name .= ' ,'; }
          }
        ?>      
        <td class="text-center"><?= implode(" ",array_slice(explode(' ', $goods_name), 0, 5)); ?></td>        
        
        <?php if(in_array('3', $per_dangerous_goods) || in_array('5', $per_dangerous_goods)): ?>
          <td class="text-center">
            <?php if(in_array('3', $per_dangerous_goods)): ?>            
                <a href="<?= base_url('admin/country-dangerous-goods/edit/') . $g['id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to View / Edit Rates"><i class="entypo-pencil"></i> View / Edit</a>
            <?php endif; ?>          
          </td>
        <?php endif; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    // Highlighted rows
    $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
      var $this = $(el),
        $p = $this.closest('tr');
      
      $( el ).on( 'change', function() {
        var is_checked = $this.is(':checked');
        
        $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
      } );
    } );
    
    // Replace Checboxes
    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );    
  } );

</script>