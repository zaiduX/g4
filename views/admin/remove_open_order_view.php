<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/get-all-orders'); ?>"><i class="fa fa-home"></i>Order List</a>
	</li>
	<li class="active">
		<strong>Remove Order From Marketplace</strong>
	</li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title"> Remove Order From Marketplace </div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/get-all-orders'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
	      	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	    	<?php endif; ?>
	    	<?php if($this->session->flashdata('success')):  ?>
	      	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/make-order-removed'); ?>" class="form-horizontal form-groups-bordered validate" id="remove_order" method="post" autocomplete="off" novalidate="novalidate">
					<input type="hidden" name="order_id" value="<?= $order_id;?>" />
					<input type="hidden" name="service_type" value="courier" />
					<div class="row">
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">							
								<label for="rate_type" class="control-label"> Removal Reason </label>						
								<textarea name="reason" id="reason" rows="5" class="form-control" placeholder="Enter reason....." style="resize: none;"></textarea>
								<span id="error_reason" class="error"></span>							
							</div>
						</div>
					</div>					
					<div class="clear"></div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="text-center">
								<button id="btn_submit" type="submit" class="btn btn-blue">Submit</button>
							</div>
						</div>
					</div>
					
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />

<script>
	$(function(){

		$("#remove_order").submit(function(e) { e.preventDefault(); });
		$("#btn_submit").on('click', function(e) {	e.preventDefault();
			var reason = $.trim($("#reason").val());

			if(reason.length == 0 ) { $("#error_reason").html("Enter removal reason."); } 			
			else { 
				swal({
				  title: 'Are you sure remove this order from Marketplace?',
				  text: "You won't be able to revert this action!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, Remove it!'
				}).then(function (result) {	$("#remove_order")[0].submit(); });
			}
		});

	});
</script>