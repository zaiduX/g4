<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Users List</strong>
  </li>
</ol>

<h2 style="display: inline-block;">Best Rating Users</h2>
<a href="<?= base_url('admin/user-wise-dashboard'); ?>" class="btn btn-blue btn-icon icon-left" style="float: right;">Back<i class="entypo-back"></i></a>
<!--
<?php $per_manage_users = explode(',', $permissions[0]['manage_users']); 
if(in_array('1', $per_manage_users)) { ?>
  <a type="button" href="<?= base_url('admin/users/inactive'); ?>" class="btn btn-red btn-icon icon-left" style="float: right;">
    Inactive Users
    <i class="entypo-eye"></i>
  </a>
<?php } ?>

  <?php if($this->session->flashdata('error')):  ?>
      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
      <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
  <?php endif; ?>
-->

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      	<th class="text-center">User ID</th>
		<th class="text-center">Company [User Name]</th>
		<th class="text-center">Contact No</th>
		<th class="text-center">Ratings</th>
		<!--<?php if(in_array('1', $per_manage_users) || in_array('4', $per_manage_users)) { ?>
		<th class="text-center">Actions</th>
		<?php } ?>-->
    </tr>
  </thead>
  
  <tbody>
    <?php foreach ($bestRatingUsers as $users): $email_cut = explode('@', $users['email1']);  $name = $email_cut[0];  ?>
    <tr>
    	<td class="text-center"><?= $users['cust_id'] ?></td>
      	<td class="text-center"><?php 
  								if($users['company_name'] != 'NULL') { 
  									echo $users['company_name'];
  									if($users['firstname'] != 'NULL' && $users['lastname'] != 'NULL') {
  										echo ' [' . $users['firstname'] . ' ' . $users['lastname'] . ']';
  									} else { echo ' [' . $email_cut[0] . ']'; }
  								} else { echo $email_cut[0]; } 
  								?></td>
  		<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($users['country_id']).' '.$users['mobile1'] ?></td>
      	<td class="text-center"><?= $users['ratings'] ?></td>
      
      <!--<?php if(in_array('1', $per_manage_users) || in_array('4', $per_manage_users)): ?>
        <td class="text-center">
          
          <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
              Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown" role="menu">
              <?php if(in_array('1', $per_manage_users)): ?>
              <li>  <a href="<?= base_url('admin/view-users/') . $users['cust_id']; ?>"> <i class="entypo-eye"></i> View </a> </li>
              <li class="divider"></li>
              <li>  <a href="<?= base_url('admin/view-users/') . $users['cust_id']; ?>"> <i class="fa fa-send"></i> Notification </a> </li>
            <?php endif; ?>
            <?php if(in_array('4', $per_manage_users)): ?>
              <li class="divider"></li>
              <?php if($users['cust_status'] == 0): ?>
                <li> <a data-toggle="tooltip" data-placement="right" title="Click to activate" data-original-title="Click to activate" onclick="activate_users('<?= $users['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-thumbs-up"></i>Activate </a>  </li>
              <?php else: ?>
                <li> <a data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to inactivate" onclick="inactivate_users('<?= $users['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-cancel"></i>  Inactivate </a> </li>
              <?php endif; ?>
            <?php endif; ?>
            </ul>
          </div>
        </td>
      <?php endif; ?>-->
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    $table2.DataTable( {
      "order": [[3, 'desc']]
    });
  });
  $(window).load(function() { 
  	$("select").addClass("form-control");
  });
</script>