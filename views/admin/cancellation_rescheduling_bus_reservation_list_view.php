<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Bus Cancellation & Rescheduling Charges</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>      
<h2 style="display: inline-block;">Bus Cancellation & Rescheduling Charges</h2>
<?php
$per_adv_pay = explode(',', $permissions[0]['advance_payment']);

if(in_array('2', $per_adv_pay)) { ?>
  <a type="button" href="<?= base_url('admin/cancellation-rescheduling-bus-reservation/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
    Add New
    <i class="entypo-plus"></i>
  </a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      <th class="text-center">Sr. No.</th>
      <th class="text-center">Country</th>
      <th class="text-center">Hours</th>
      <th class="text-center">Cancellation %</th>
      <th class="text-center">Rescheduling %</th>
      <th class="text-center">Vehicle Type</th>
      <?php if(in_array('3', $per_adv_pay) || in_array('5', $per_adv_pay)) { ?>
        <th class="text-center" >Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>
    <?php  foreach ($charge_list as $lst):  static $i=0; $i++; ?>
    <tr>
      <td class="text-center"><?= $i ?></td>
      <td class="text-center"><?= $this->user->get_country_name_by_id($lst['country_id']); ?></td>
      <td class="text-center"><?= $lst['bcr_min_hours'] .' - '. $lst['bcr_max_hours']; ?></td>
      <td class="text-center"><?= $lst['bcr_cancellation']; ?></td>
      <td class="text-center"><?= $lst['bcr_rescheduling']; ?></td>
      <td class="text-center"><?= $this->charges->get_vehicles_by_type_id($lst['vehical_type_id'])['vehicle_type']; ?></td>
      <?php if(in_array('3', $per_adv_pay) || in_array('5', $per_adv_pay)): ?>
        <td class="text-center">
          <?php if(in_array('3', $per_adv_pay)): ?>
            <a href="<?= base_url('admin/cancellation-rescheduling-bus-reservation/edit/').$lst['bcr_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to Edit">
              <i class="entypo-eye"></i> View &amp; Edit
            </a> &nbsp;
          <?php endif; ?>
          <?php if(in_array('5', $per_adv_pay)): ?>
            <button class="btn btn-danger btn-sm btn-icon icon-left"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_details('<?= $lst["bcr_id"]; ?>');"> <i class="entypo-cancel"></i> Delete 
            </button>  
          <?php endif; ?>
        </td>        
      <?php endif; ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );    
    $table2.DataTable( {
      "order" : [[0, "desc"]]
    });
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });
  });

  function delete_details(id=0){    
    swal({
      title: 'Are you sure?',
      text: "You want to to delete this record?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('cancellation-rescheduling-bus-reservation/delete', {id: id}, function(res){ 
        if(res == 'success'){ 
          swal(
            'Deleted!',
            'Record has been deleted.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'Record deletion failed.',
            'error'
          )
        }
      });
      
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }
</script>