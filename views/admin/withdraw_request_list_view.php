<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Withdraw Request List</strong>
	</li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>			
<h2 style="display: inline-block;">Withdraw Request List</h2>
<?php
$per_withdraw_request = explode(',', $permissions[0]['withdraw_request']);
if(in_array('2', $per_withdraw_request)) { ?>
	<a type="button" href="<?= base_url('admin/add-relay-point'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add Relay Point
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

	<?php if($this->session->flashdata('error')):  ?>
  		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	<?php endif; ?>
	<?php if($this->session->flashdata('success')):  ?>
  		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	<?php endif; ?>


<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Date</th>
			<th class="text-center">Request From</th>
			<th class="text-center">Amount</th>
			<th class="text-center">Currency</th>
			<th class="text-center">Country</th>
			<th class="text-center">User Type</th>
			<th class="text-center">Status</th>
			<th class="text-center">Response Date / Time</th>
			<th class="text-center">Account Type</th>
			<?php if(in_array('4', $per_withdraw_request)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($withdraw_request_list as $withdraw):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= date('Y-m-d',strtotime($withdraw['req_datetime'])) ?></td>
			<td class="text-center"><?php if($withdraw['user_type'] == '1') { echo $this->withdraw->get_relay_details($withdraw['user_id']); } else { echo $this->withdraw->get_customer_details($withdraw['user_id']); } ?></td>
			<td class="text-center"><?= $withdraw['amount'] ?></td>
			<td class="text-center"><?= $withdraw['currency_code'] ?></td>
			<td class="text-center">
		    <?php 
    	        $country_details = $this->notice->get_country_detail($withdraw['country_id']);
    	        echo $country_details['country_name'];
		    ?>
			</td>
			<td class="text-center"><?= $withdraw['user_type'] == 1 ? 'Relay Point' : 'User' ?></td>
			
			<td class="text-center">
				<?php 
					if($withdraw['response'] == 'reject') { $status  = 1; echo '<span class="text-danger"> Rejected </span>'; }
					else if($withdraw['response'] == 'accept') { $status  = 1;  echo '<span class="text-success"> Approved </span>'; }					
					else { $status  = 0;  echo '<span class="text-warning"> Pending </span>'; }
				?>		
			</td>
			<td class="text-center">
				<?php
					if($withdraw['response_datetime'] == 'NULL') { echo 'NA'; }
					else { echo date('d/m/Y h:i:s a', strtotime($withdraw['response_datetime'])); }
					?>
			</td>
			<td class="text-center"><?= $withdraw['transfer_account_type'] == 'iban' ? 'IBAN' : 'Mobile Money Account'?></td>
			
			<?php if(in_array('4', $per_withdraw_request)) { ?>
				<td class="text-center">
					<?php if(in_array('4', $per_withdraw_request)): ?>	

					<?php if($status == 0): ?>
						
						<div class="btn-group">
							<button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown" role="menu">								
								<li>	
									<form id="accept_form" action="<?= base_url('admin/accept-withdraw-request') ?>" method="post">
										<input type="hidden" name="req_id" value="<?= $withdraw['req_id']; ?>" />
										<input type="hidden" name="job_id" value="<?= $withdraw['job_id']; ?>" />
										<input type="hidden" name="milestone_id" value="<?= $withdraw['milestone_id']; ?>" />
										<a data-toggle="tooltip" data-placement="top" title="Click to accept" data-original-title="Click to activate" style="cursor: pointer; color: inherit; padding: 3px 20px;" onclick="$('#accept_form').get(0).submit();"> 
											<i class="entypo-check"></i>	Accept 
										</a>	
									</form>
								</li>
								<li class="divider"></li>
								<li>	<a data-toggle="tooltip" data-placement="top" title="Click to reject" data-original-title="Click to inactivate" onclick="reject_request('<?= $withdraw['req_id']; ?>','<?= $withdraw['job_id']; ?>');" style="cursor: pointer;"></i> <i class="entypo-cancel"></i> Reject </a>	</li>							
							</ul>
						</div>

					<?php else: ?>
							<span class="text-success"> DONE </span>
					<?php endif; ?>
				<?php endif; ?>

				</td>
			<?php } ?>

		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );

		

	} );

	function reject_request(id=0 , job_id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to reject withdraw request?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, reject it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('reject-withdraw-request', {id: id , job_id: job_id }, function(res){
				console.log(res);
				if(res == 'success'){
				  swal(
				    'Reject!',
				    'Withdraw request has been rejected.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Unable to reject the withdraw request.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate_relay(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this relay point?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('active-relay-point', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Relay point has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Relay point activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>