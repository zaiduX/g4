<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/customer-support-list'); ?>">Customer Support</a>
	</li>
	<li class="active">
		<strong>Edit</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Response to support request
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/customer-support-list'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">
				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>

				<div class="well well-sm hidden">
					<h6>Please fill payment details.</h6>
				</div>
				
				<form id="rootwizard-2" method="post" action="<?= base_url('admin/update-support-request'); ?>" class="form-wizard validate" enctype="multipart/form-data" >
					<input type="hidden" name="query_id" value="<?= $request_details['query_id'] ?>">
					<div class="steps-progress hidden">
						<div class="progress-indicator"></div>
					</div>
					
					<ul class="hidden">
						<li class="active">
							<a href="#tab2-1" data-toggle="tab"><span>1</span>Relay Point Manager</a>
						</li>
						<li>
							<a href="#tab2-2" data-toggle="tab"><span>2</span>Relay Point</a>
						</li>
						<li>
							<a href="#tab2-3" data-toggle="tab"><span>3</span>Warehouse</a>
						</li>
						<li>
							<a href="#tab2-5" data-toggle="tab"><span>5</span>Finish</a>
						</li>
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="tab2-1">
							<div class="row">
								<div class="col-md-12 form-group">
										<div class="col-md-3">
											<label class="control-label" for="full_name">Customer Details</label>
										</div>
										<div class="col-md-9">
										<?php echo $this->support->get_customer_details($request_details['cust_id']); ?>
									</div>
								</div>
								<div class="col-md-12 form-group">
									<div class="col-md-3">
										<label class="control-label" for="full_name">Type</label>
									</div>
									<div class="col-md-3">
										<?= strtoupper($request_details['type']) ?>
									</div>
								</div>
								<div class="col-md-12 form-group">
									<div class="col-md-3">
										<label class="control-label" for="full_name">Customer Query</label>
									</div>
									<div class="col-md-3">
										<?= $request_details['description'] ?>
									</div>
								</div>
								<?php if($request_details['file_url'] != 'NULL') { ?>
								<div class="col-md-12 form-group">
									<div class="col-md-3">
										<label class="control-label" for="full_name">Attachment</label>
									</div>
									<div class="col-md-3">
										<a href="<?= base_url($request_details['file_url']) ?>" target="_blank"><i class="fa fa-download"></i> View / Download</a>
									</div>
								</div>
								<?php } ?>
								<div class="col-md-12 form-group">
									<div class="col-md-3">
										<label class="control-label" for="last_name">Request Date</label>
									</div>
									<div class="col-md-3">
										<?= date('d/m/Y', strtotime($request_details['cre_datetime'])) ?>
									</div>
								</div>
								<div class="col-md-12 form-group">
									<div class="col-md-3">
										<label class="control-label" for="last_name">Status</label>
									</div>
									<div class="col-md-3">
										<?= strtoupper($request_details['status']) ?>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12 form-group">
									<h3>Response to customer query</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label class="control-label">Response details</label>
									<textarea class="form-control" name="response" id="response" style="resize: none;" rows="4"></textarea>
								</div>
								<div class="col-md-6 form-group">
									<label class="control-label">Select Status</label>
									<select name="status" class="form-control" id="status">
										<option value="">Select Status</option>
										<option value="open">Open</option>
										<option value="in_progress">In-progress</option>
										<option value="close">Close</option>
										<option value="cancel">Cancel</option>
									</select> <br />
									<div class="form-group pull-right">
										<button type="submit" class="btn btn-primary">Save Details</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<br />

<script>
	$("#rootwizard-2")
		.validate({
	    rules: {
	        response: { required : true, },
	        status: { required : true, },
	    },
	    messages: {
	        response: { required : "Please enter response details!", },
	        status: { required : "Please select status!",  },
	    },
	});
</script>