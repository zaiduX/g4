<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Driver Categories</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">Driver Categories</h2>
<?php
$per_driver_category = explode(',', $permissions[0]['driver_category']);

if(in_array('2', $per_driver_category)) { ?>
	<a type="button" href="<?= base_url('admin/driver-categories/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add New
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Short Name</th>
			<th class="text-center">Full Name</th>
			<th class="text-center">Description</th>
			<th class="text-center">Status</th>
			<?php if(in_array('3', $per_driver_category) || in_array('5', $per_driver_category)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($categories as $cat):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= $cat['short_name']; ?></td>
			<td class="text-center"><?= $cat['full_name'] ?></td>
			<td class="text-center"><?= trim($cat['description']); ?></td>
			<td class="text-center">
				<?php if($cat['pc_status'] == 0): ?>
					<label class="label label-danger"><i class="entypo-cancel"></i>Inactive</label>
				<?php else: ?>
					<label class="label label-success"><i class="entypo-thumbs-up"></i>Active</label>
				<?php endif; ?>
			</td>
			<?php if(in_array('3', $per_driver_category) || in_array('5', $per_driver_category)): ?>
				<td class="text-center">
					<?php if(in_array('3', $per_driver_category)): ?>
						<a href="<?= base_url('admin/driver-categories/edit/').$cat['pc_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
					<?php endif; ?>					
					<?php if(in_array('4', $per_driver_category)) { ?>
						<?php if($cat['pc_status'] == 0): ?>
							<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to activate" onclick="inactivate_driver_cat('<?= $cat['pc_id']; ?>');">
								<i class="entypo-thumbs-up"></i>Activate</button>
						<?php else: ?>
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to inactivate" onclick="activate_driver_cat('<?= $cat['pc_id']; ?>');">
							<i class="entypo-cancel"></i>
							Inactivate</button>			
						<?php endif; ?>
					<?php } ?>
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	} );


	function activate_driver_cat(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this category type?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('driver-categories/inactivate', {id: id}, function(res){
				console.log(res);
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'Driver category type has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Driver category type inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate_driver_cat(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this driver category?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('driver-categories/activate', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Driver category type has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Driver category type activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

</script>