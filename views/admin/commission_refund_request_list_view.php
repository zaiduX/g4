<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Commission Refund Request List</strong>
	</li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>			
<h2 style="display: inline-block;">Commission Refund Request List</h2>
<?php $per_ticket_comm_refund = explode(',', $permissions[0]['ticket_comm_refund']); ?>

<?php if($this->session->flashdata('error')):  ?>
	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
<?php endif; ?>
<?php if($this->session->flashdata('success')):  ?>
	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
<?php endif; ?>


<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Date</th>
			<th class="text-center">Operator</th>
			<!-- <th class="text-center">Customer</th>
			<th class="text-center">Contact</th>
			<th class="text-center">Email</th> -->
			<th class="text-center">Source</th>
			<th class="text-center">Destination</th>
			<th class="text-center">Journey Date</th>
			<th class="text-center">Amount</th>
			<?php if(in_array('4', $per_ticket_comm_refund)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php 
		foreach ($commission_refund_request_list as $refund): 
			$operator_details = $this->refund->get_customer_details($refund['operator_id']);
		?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= date('Y-m-d',strtotime($refund['req_date'])) ?></td>
			<td class="text-center"><?= $operator_details ?></td>
			<!-- <td class="text-center"><?= $refund['firstname'] . ' ' . $refund['lastname'] ?></td>
			<td class="text-center"><?= $refund['mobile'] ?></td>
			<td class="text-center"><?= $refund['email_id'] ?></td> -->
			<td class="text-center"><?= $refund['source_point'] ?></td>
			<td class="text-center"><?= $refund['destination_point'] ?></td>
			<td class="text-center"><?= date('Y-m-d',strtotime($refund['journey_date'])) ?></td>
			<td class="text-center"><?= $refund['seat_price'] ?></td>
			<?php if(in_array('4', $per_ticket_comm_refund)) { ?>
				<td class="text-center">
					<div class="btn-group">
						<button type="button" class="btn btn-blue dropdown-toggle btn-sm" data-toggle="dropdown">
							Action <span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown" role="menu"> 
							<li>
								<a data-toggle="modal" data-target="#myModal_<?=$refund['req_id']?>" data-placement="top" title="Click to view details" data-original-title="Click to view details" style="cursor: pointer; color: inherit; padding: 3px 20px;"><i class="entypo-eye"></i> Details</a>
							</li>
							<li class="divider"></li>
							<li class="">
								<a data-toggle="tooltip" data-placement="top" title="Click to accept" data-original-title="Click to accept" onclick="accept_request('<?= $refund['req_id'].'~'.$refund['seat_id']; ?>');" style="cursor: pointer;"></i> <i class="entypo-check"></i> Accept</a>
							</li>
							<li class="divider"></li>
							<li><a data-toggle="tooltip" data-placement="top" title="Click to reject" data-original-title="Click to inactivate" onclick="reject_request('<?= $refund['req_id'].'~'.$refund['seat_id']; ?>');" style="cursor: pointer;"></i> <i class="entypo-cancel"></i> Reject</a></li> 
						</ul>
						<div id="myModal_<?=$refund['req_id']?>" class="modal fade" role="dialog">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title">Ticket Details</h4>
						      </div>
						      <div class="modal-body">
						      	<div class="row">
						      		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-left">
						      			<h5><strong><i class="fa fa-map-marker"></i> <u>Journey Details</u></strong></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Source</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['source_point']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Destination</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['destination_point']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Journey Date</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['journey_date']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="margin-top: 10px;">
						      		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-left">
						      			<h5><strong><i class="fa fa-users"></i> <u>Passenger Details</u></strong></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Customer Name</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['firstname'] . ' ' . $refund['lastname']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Contact</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['mobile']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Email</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['email_id']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="margin-top: 10px;">
						      		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-left">
						      			<h5><strong><i class="fa fa-ticket"></i> <u>Seat & Price Details</u></strong></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Ticket ID</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5>#<?=$refund['ticket_id']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Total Seats</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['no_of_seats']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Single Seat Price</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['currency_sign']?> <?=$refund['seat_price']?></h5>
						      		</div>
						      	</div>
						      	<div class="row" style="border-bottom: 1px solid #737881">
						      		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-left">
						      			<h5><strong>Ticket Price</strong></h5>
						      		</div>
						      		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-left">
						      			<h5><?=$refund['currency_sign']?> <?=$refund['ticket_price']?></h5>
						      		</div>
						      	</div>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						      </div>
						    </div>
						  </div>
						</div>

					</div>
				</td>

			<?php } ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );
	} );

	function reject_request(id='0~0'){
		swal({
			title: 'Are you sure?',
			text: "You want to reject refund request?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, reject it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: true,
		}).then(function () {
			$.post('reject-refund-request', {id: id}, function(res) {
				console.log(res);
				if(res == 'success') {
				  swal(
				    'Reject!',
				    'Refund request has been rejected.',
				    'success'
				  ). then(function() { 	window.location.reload();  } );
				} else {
					swal(
				    'Failed!',
				    'Unable to reject the refund request.',
				    'error'
				  )
				}
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	} );
	}

	function accept_request(id='0~0'){
		swal({
			title: 'Are you sure?',
			text: "You want to accept refund request?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, accept it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: true,
		}).then(function () {
			$.post('accept-refund-request', {id: id}, function(res) {
			    console.log(res);
				if(res == 'success'){
				  	swal(
					    'Reject!',
					    'Refund request has been accepted.',
					    'success'
				  	). then(function() { 	window.location.reload();  } );
				} else {
					swal(
					    'Failed!',
					    'Unable to accept the refund request.',
					    'error'
				  	)
				}
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

</script>