<style type="text/css">
  #sticky {
    padding: 0.5px;
    background-color: #003471;
    color: #fff;
    font-size: 1em;
    border-radius: 0.5ex;
  }

  #sticky.stick {
    margin-top: 0 !important;
    position: fixed;
    top: 0;
    z-index: 10000;
    border-radius: 0 0 0.5em 0.5em;
    width: 76%;
  }
</style>
<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/group-permission'); ?>">Groups</a>
  </li>
  <li class="active">
    <strong>Add New</strong>
  </li>
</ol>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-dark" data-collapsed="0">
      <div class="panel-heading">
        <div class="panel-title">
          Add Group and Permissions
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/group-permission'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/register-group-permission'); ?>" class="form-horizontal form-groups-bordered" id="authority_type" method="post">
  
          <div class="form-group">
            <label for="auth_type" class="col-sm-2 control-label">Group Name</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="auth_type" placeholder="Enter group name..." name="auth_type" />  
            </div>
          </div>

          <div id="sticky-anchor" class="form-group" style="margin-left: 0px; margin-right: 0px;"></div>
                
          <div id="sticky" class="form-group" style="margin-left: 0px; margin-right: 0px;">
            <label for="auth_fname" class="col-sm-2" style="text-align: right;">Functionality</label>
            <label for="auth_fname" class="col-sm-2" style="text-align: center;">View</label>
            <label for="auth_fname" class="col-sm-2" style="text-align: center;">Add</label>
            <label for="auth_fname" class="col-sm-2" style="text-align: center;">Edit</label>
            <label for="auth_fname" class="col-sm-2" style="text-align: center;">Activate/Deactivate</label>
            <label for="auth_fname" class="col-sm-2" style="text-align: center;">Delete</label>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Category Type</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="category_type_view" value="1"  />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="category_type_add" value="2"  />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="category_type_edit" value="3"  />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="category_type_active" value="4"  />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Categories</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="category_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="category_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="category_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="category_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Currency Master</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="currency_master_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="currency_master_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="currency_master_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="currency_master_active" value="4" />
              </div>
            </div>              
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="currency_master_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Country Currency</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="country_currency_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="country_currency_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="country_currency_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="country_currency_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center hidden">
            <label for="auth_lname" class="col-sm-2 control-label">Authority Type</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="authority_type_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="authority_type_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="authority_type_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="authority_type_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Group Permission</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="permission_type_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="permission_type_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="permission_type_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="permission_type_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Manage Authorities</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_authority_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_authority_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_authority_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_authority_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Manage Users</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_users_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_users_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_users_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_users_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Notification</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_notifications_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_notifications_add" value="2" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">App Promo Code</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_promo_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_promo_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_promo_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_promo_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Service Promo Code</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_promo_service_view" value="1" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Standard Dimension</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_dimension_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_dimension_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_dimension_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_dimension_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Vehicle</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_vehicals_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_vehicals_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_vehicals_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_vehicals_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Rates</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_rates_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_rates_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_rates_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_rates_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Driver Category</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_driver_category_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_driver_category_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_driver_category_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_driver_category_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Advance Payment</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_advance_payment_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_advance_payment_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_advance_payment_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_advance_payment_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Skill Master</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_skills_master_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_skills_master_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_skills_master_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_skills_master_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Insurance Master</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_insurance_master_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_insurance_master_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_insurance_master_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_insurance_master_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Relay Point</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_relay_point_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_relay_point_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_relay_point_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_relay_point_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label class="col-sm-2 control-label">Withdraw Request</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_withdraw_request_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_withdraw_request_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label class="col-sm-2 control-label">Commission Refund Request</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_commission_refund_request_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_commission_refund_request_active" value="4" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">User Doc. Verification</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_verify_doc_view" value="1" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Courier Orders</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="manage_all_orders_view" value="1" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Gonagoo Address</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="gonagoo_address_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="gonagoo_address_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="gonagoo_address_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="gonagoo_address_active" value="4" />
              </div>
            </div>              
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="gonagoo_address_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Laundry Category</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_category_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_category_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_category_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_category_active" value="4" />
              </div>
            </div>              
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_category_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">Laundry Advance/Commission</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_payment_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_payment_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_payment_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_payment_active" value="4" />
              </div>
            </div>              
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="laundry_payment_delete" value="5" />
              </div>
            </div>
          </div>
          
          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">M-4 Advance Payment</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_adv_payment_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_adv_payment_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_adv_payment_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_adv_payment_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">M-4 Provider Questions</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_know_provider_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_know_provider_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_know_provider_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_know_provider_delete" value="5" />
              </div>
            </div>
          </div>
        
          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">M-4 Job Cancel Reason</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_job_cancel_reason_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_job_cancel_reason_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_job_cancel_reason_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_job_cancel_reason_delete" value="5" />
              </div>
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">M-4 Profile Subscription</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_profile_sub_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_profile_sub_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_profile_sub_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_profile_sub_delete" value="5" />
              </div>
            </div>
          </div>
        
          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">M - 4 Dispute Category</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_dispute_cat_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_dispute_cat_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_dispute_cat_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_dispute_cat_active" value="4" />
              </div>
            </div>              
            <div class="col-sm-2">
              &nbsp;
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">M - 4 Dispute Sub Category</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_dispute_sub_cat_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_dispute_sub_cat_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_dispute_sub_cat_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_dispute_sub_cat_active" value="4" />
              </div>
            </div>              
            <div class="col-sm-2">
              &nbsp;
            </div>
          </div>

          <div class="form-group text-center">
            <label for="auth_lname" class="col-sm-2 control-label">M-4 Contract Expiry Delay</label>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_contract_expiry_delay_view" value="1" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_contract_expiry_delay_add" value="2" />
              </div>
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_contract_expiry_delay_edit" value="3" />
              </div>
            </div>
            <div class="col-sm-2">
              &nbsp;
            </div>
            <div class="col-sm-2">
              <div class="checkbox checkbox-replace color-blue">
                <input type="checkbox" id="chk-20" name="m4_contract_expiry_delay_delete" value="5" />
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5">
              <button type="submit" class="btn btn-blue">Add Group</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<br />
<script>
  $(function(){
    $("#authority_type").validate({
      ignore: [],
      rules: { auth_type: { required: true, }, 
      }, 
      messages: { auth_type: { required: "Please enter group name!", },
      }
    });
  });
  function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
      $('#sticky').addClass('stick');
      $('#sticky-anchor').height($('#sticky').outerHeight());
      $('#sticky-anchor').width($('#sticky').outerWidth());
    } else {
      $('#sticky').removeClass('stick');
      $('#sticky-anchor').height(0);
    }
  }
  $(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
  });
</script>