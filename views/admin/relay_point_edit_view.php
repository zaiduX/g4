<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/relay-point'); ?>">Relay Point</a>
	</li>
	<li class="active">
		<strong>Edit</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Edit Relay Point
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/relay-point'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>

				<div class="well well-sm hidden">
					<h6>Please fill the details to create Relay Point.</h6>
				</div>
				
				<form id="rootwizard-2" method="post" action="<?= base_url('admin/update-relay-point'); ?>" class="form-wizard validate" enctype="multipart/form-data" >
					<input type="hidden" name="cd_id" value="<?= $manager['cd_id'] ?>">
					<input type="hidden" name="hiddenpassword" value="<?= $manager['password'] ?>">
					<div class="steps-progress hidden">
						<div class="progress-indicator"></div>
					</div>
					
					<ul class="hidden">
						<li class="active">
							<a href="#tab2-1" data-toggle="tab"><span>1</span>Relay Point Manager</a>
						</li>
						<li>
							<a href="#tab2-2" data-toggle="tab"><span>2</span>Relay Point</a>
						</li>
						<li>
							<a href="#tab2-3" data-toggle="tab"><span>3</span>Warehouse</a>
						</li>
						<li>
							<a href="#tab2-5" data-toggle="tab"><span>5</span>Finish</a>
						</li>
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="tab2-1">
							<div class="row">
								<div class="col-md-4">
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="fileinput-new thumbnail" style="width 200px; height: 150px;" data-trigger="fileinput">
											<img src="<?php if($manager['avatar'] != 'NULL') { echo $this->config->base_url($manager['avatar']); } else { echo 'http://placehold.it/200x150'; } ?>" alt="Profile Picture">
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
										<div class="text-center">
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="profile_image" accept="image/*">
											</span>
											<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="full_name">First Name</label>
										<input value="<?= $manager['first_name'] ?>" type="text" placeholder="First Name" class="form-control m-b" name="first_name" id="first_name">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="last_name">Last Name</label>
										<input value="<?= $manager['last_name'] ?>" type="text" placeholder="Last Name" class="form-control m-b" name="last_name" id="last_name">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="mobile1">Mobile 1</label>
										<input value="<?= $manager['mobile1'] ?>" type="text" placeholder="Mobile Number" class="form-control m-b" name="mobile1" id="mobile1">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="mobile2">Mobile 2</label>
										<input value="<?= $manager['mobile2'] ?>" type="text" placeholder="Mobile Number" class="form-control m-b" name="mobile2" id="mobile2">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 form-group">
									<label class="control-label">Login Details</label><br />
									<label class="control-label"><?= $manager['email'] ?></label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label class="control-label">Choose Password</label>
									<input type="password" class="form-control" name="password" id="password" placeholder="Enter password" />
								</div>
								<div class="col-md-6 form-group">
									<label class="control-label">Repeat Password</label>
									<input type="password" class="form-control" name="repassword" id="repassword" placeholder="Confirm password" />
								</div>
							</div>
							<div class="form-group pull-right">
								<button type="submit" class="btn btn-primary">Save Details</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<br />

<script>
	$("#rootwizard-2")
		.validate({
	    rules: {
	        first_name: { required : true, },
	        last_name: { required : true, },
	        mobile1: { required : true, number : true },
	        mobile2: { number : true, },        
	        email: { required : true, email : true, },
	        password: { minlength : 6, maxlength : 16 },
	        repassword: { minlength : 6, maxlength : 16, equalTo: "#password", },
	    },
	    messages: {
	        first_name: { required : "Please enter first name!", },
	        last_name: { required : "Please enter last name!", },
	        mobile1: { required : "Please enter last name!", number : "Numbers only!" },
	        mobile2: { number : "Numbers only!" },        
	        email: { required : "Please enter email address!", email : "Enter valid email address!"},
	        password: { minlength : "Must be 6 character long!", maxlength : "Not more than 16 charaters" },
	        repassword: { minlength : "Must be 6 character long!", maxlength : "Not more than 16 charaters", equalTo : "Password not matcching!" },
	    },
	});
</script>