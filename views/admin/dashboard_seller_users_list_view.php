<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Users List</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>
<h2 style="display: inline-block;">Seller Users List</h2>

<?php if($this->session->flashdata('error')):  ?>
  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
<?php endif; ?>
<?php if($this->session->flashdata('success')):  ?>
  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2" style="width: 100%;">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Name</th>
      <th class="text-center">Email</th>
      <th class="text-center">Type</th>
      <th class="text-center">Account Type</th>
      <th class="text-center">Balance</th>
      <th class="text-center">Country</th>
      <th class="text-center">Status</th>
      <th class="text-center">Last Login</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>
  
  <tbody>
    <?php foreach ($users as $user): $email_cut = explode('@', $user['email1']);  $name = $email_cut[0];  ?>
    <tr>
      <td class="text-center"><?= $user['cust_id']; ?></td>
      <td class="text-center"><?= $user['firstname'] != "NULL" ? $user['firstname'] . ' ' . $user['lastname'] : ($user['company_name'] != "NULL" ? ucwords($user['company_name']) : ucwords($name)) ?></td>
      <td class="text-center"><?= $user['email1'] ?></td>
      <td class="text-center"><?= $user['user_type'] == 1 ? 'Individual' : 'Business'; ?></td>   
      <td class="text-center"><?= ucfirst($user['acc_type']); ?></td>
      <td class="text-center">
          <?php  
              $current_balance = $this->admin->get_current_balance($user['cust_id'],'USD');
              echo '$'; echo ($current_balance != NULL )?$current_balance['account_balance'] : 0;
          ?>
      </td>   
      <td class="text-center">
        <?php 
              $country_details = $this->admin->get_country_detail($user['country_id']);
              $country_name = $country_details['country_name'];
              echo $country_name;
        ?>
      </td>
      <td class="text-center">
        <?php 
        $to_time = strtotime($user['last_login_datetime']);
        $from_time = strtotime(date('Y-m-d H:i:s'));
          $min = round(abs($to_time - $from_time) / 60,2);
          if($min > 10 ) { echo '<span class="text-danger">Offline</span>'; }
          else { echo '<span class="text-success">Online</span>'; }
        ?>
      </td>
      <td class="text-center"><?= date('d/M/Y',strtotime($user['last_login_datetime'])); ?></td>

        <td class="text-center">
          
          <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
              Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown" role="menu">
              <li>  <a href="<?= base_url('admin/view-users/') . $user['cust_id']; ?>"> <i class="entypo-eye"></i> View </a> </li>
              <li class="divider"></li>
              <li>  <a href="<?= base_url('admin/view-users/') . $user['cust_id']; ?>"> <i class="fa fa-send"></i> Notification </a> </li>
              <li class="divider"></li>
              <?php if($user['cust_status'] == 0): ?>
                <li> <a data-toggle="tooltip" data-placement="right" title="Click to activate" data-original-title="Click to activate" onclick="activate_users('<?= $user['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-thumbs-up"></i>Activate </a>  </li>
              <?php else: ?>
                <li> <a data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to inactivate" onclick="inactivate_users('<?= $user['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-cancel"></i>  Inactivate </a> </li>
              <?php endif; ?>
            </ul>
          </div>
        </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true,
      "order": [[7,'desc']],
      "scrollX": true
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );
  } );

  function inactivate_users(id=0){
    swal({
      title: 'Are you sure?',
      text: "You want to to inactivate this user?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, inactivate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('users/inactivate', {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Inactive!',
            'User has been inactivated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'User inactivation failed.',
            'error'
          )
        }
      });
      
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

  function activate_users(id=0){
    swal({
      title: 'Are you sure?',
      text: "You want to to activate this authority type?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, activate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('users/activate', {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Active!',
            'User has been activated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'User activation failed.',
            'error'
          )
        }
      });
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }
</script>