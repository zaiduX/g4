<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Promocode List</strong>
	</li>
</ol>
<h2 style="display: inline-block;">Promocode List</h2>
<?php
$promocode_authority = explode(',', $permissions[0]['promo_code']);
if(in_array('2', $promocode_authority)) { ?>
	<a type="button" href="<?= base_url('admin/add-promo'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add Promocode
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

	<?php if($this->session->flashdata('error')):  ?>
  		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	<?php endif; ?>
	<?php if($this->session->flashdata('success')):  ?>
  		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Promocode</th>
			<th class="text-center">Discount Percent</th>
			<th class="text-center">Title</th>
			<th class="text-center">Start Date</th>
			<th class="text-center">End Date</th>
			<th class="text-center">Status</th>
			<?php if(in_array('2', $promocode_authority) || in_array('3', $promocode_authority) || in_array('4', $promocode_authority) || in_array('5', $promocode_authority)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($promocode as $code):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= $code['promo_code']?></td>
			<td class="text-center"><?= $code['discount_percent']?>%</td>
			<td class="text-center"><?= $code['promo_title']?></td>
			<td class="text-center"><?= $code['code_start_date']?></td>
			<td class="text-center"><?= $code['code_expire_date']?></td>
			<?php if($code['code_status'] == 1): ?>
				<td class="text-center">
					<button class="btn btn-success btn-sm" ><i class="entypo-thumbs-up"></i>Active</button>
				</td>
			<?php else: ?>
				<td class="text-center">
					<button class="btn btn-danger btn-sm" ><i class="entypo-cancel"></i>Inactive</button>
				</td>
			<?php endif; ?>
			<?php if(in_array('2', $promocode_authority) || in_array('3', $promocode_authority) || in_array('4', $promocode_authority) || in_array('5', $promocode_authority)) { ?>
				
				<td class="text-center">
          <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle btn-sm" data-toggle="dropdown">
              <i class="fa fa-cogs"></i> More
            </button>
            <ul class="dropdown-menu dropdown" role="menu">
                <!-- <li>  <a href="<?= base_url('') . $code['promo_code_id']; ?>" data-toggle="tooltip" data-placement="left" title="Click to See Details" data-original-title="Click to See Details"> <i class="entypo-eye"></i> View </a> </li> -->
             <?php if($code['code_status'] == 0): ?>
             	 <li>
	             	<a onclick="inactivate_promo_code('<?= $code['promo_code_id']; ?>')" data-toggle="tooltip" data-placement="left" title="Click to In-Activate Promocode" data-original-title="Click to In-Activate Promocode">
								<i class="entypo-thumbs-up"></i> Activate</a>             	
	             </li>
             <?php else: ?>
							 <li>
	             	<a onclick="activate_promo_code('<?= $code['promo_code_id']; ?>')" data-toggle="tooltip" data-placement="left" title="Click to In-Activate Promocode" data-original-title="Click to In-Activate Promocode">
								<i class="entypo-cancel"></i> In Activate</a>             	
	             </li>
             <?php endif; ?>
             <li>
             	<a href="<?= base_url('admin/edit-promo/').$code['promo_code_id']?>" data-toggle="tooltip" data-placement="left" title="Click to edit Details" data-original-title="Click to edit Details">
							<i class="entypo-pencil"></i> Edit
							</a>             	
             </li>
             <li>
             	<a href="<?= base_url('admin/delete-promo/') . $code['promo_code_id']; ?>" data-toggle="tooltip" data-placement="left" title="Click to Delete" data-original-title="Click to Delete">
							<i class="entypo-trash"></i> Delete
							</a>             	
             </li>
            </ul>
          </div>
        </td>
			<?php } ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );

		

	} );

	function activate_promo_code(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this Promocode?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('inactive-promo_code', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'promocode has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Promocode inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate_promo_code(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to activate this Promocode?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('active-promo_code', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Promocode has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Promocode activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>