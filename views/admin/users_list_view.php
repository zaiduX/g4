<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Users List</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>
<div class="row">
  <?php
        $total_accounts = $this->users->total_accounts();
        $total_accounts = $this->users->convert_big_int($total_accounts);
        
        $total_business = $this->users->total_accounts("business");
        $total_business = $this->users->convert_big_int($total_business);
        
        $total_individual = $this->users->total_accounts("individual");
        $total_individual = $this->users->convert_big_int($total_individual);
        
        $total_both = $this->users->total_accounts(false,"both");
        $total_both = $this->users->convert_big_int($total_both);
        
        $total_buyer = $this->users->total_accounts(false,"buyer");
        $total_buyer = $this->users->convert_big_int($total_buyer);
        
        $total_seller = $this->users->total_accounts(false,"seller");
        $total_seller = $this->users->convert_big_int($total_seller);
        
        $total_online = $this->users->total_accounts(false, false, false, "online");
        $total_online = $this->users->convert_big_int($total_online);

        $total_active = $this->users->total_accounts(false, false, "active");
        $total_active = $this->users->convert_big_int($total_active);
        
        $total_inactive = $this->users->total_accounts(false, false, "inactive");
        $total_inactive = $this->users->convert_big_int($total_inactive);
    ?>
  <div class="col-sm-4">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Total Accounts : <big> <?= $total_accounts; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Business Accounts </h6>
          <big class="text-white"> <?= $total_business; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Individual Accounts </h6>
          <big class="text-white"> <?= $total_individual; ?></big>
        </div> <br/>
      </div>            
    </div>
  </div>
  
  <div class="col-sm-4">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Total Buyers : <big> <?= $total_buyer; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Total Seller </h6>
          <big class="text-white"> <?= $total_seller; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Buyer &amp; Seller </h6>
          <big class="text-white"> <?= $total_both; ?></big>
        </div> <br/>
      </div>            
    </div>
  </div>
  
  <div class="col-sm-4">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Total Connected : <big> <?= $total_online; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Total Active </h6>
          <big class="text-white"> <?= $total_active; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Total Inactive </h6>
          <big class="text-white"> <?= $total_inactive; ?></big>
        </div> <br/>
      </div>            
    </div>
  </div>
  
  
</div>

      
<h2 style="display: inline-block;">Active Users List</h2>
<?php $per_manage_users = explode(',', $permissions[0]['manage_users']); 
if(in_array('1', $per_manage_users)) { ?>
  <a type="button" href="<?= base_url('admin/users/inactive'); ?>" class="btn btn-red btn-icon icon-left" style="float: right;">
    Inactive Users
    <i class="entypo-eye"></i>
  </a>
<?php } ?>

  <?php if($this->session->flashdata('error')):  ?>
      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
      <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
  <?php endif; ?>


    
<table class="table datatable table-striped" id="table-2" width="100%">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Name</th>
      <!-- <th class="text-center">Email</th> -->
      <!-- <th class="text-center">Type</th> -->
      <th class="text-center">Account Type</th>
      <th class="text-center">OTP</th>
      <th class="text-center">Country</th>
      <th class="text-center">Status</th>
      <th class="text-center">Last Login</th>
      <th class="text-center">Balance</th>
      <th class="text-center">G-Comm.</th>
      <?php if(in_array('1', $per_manage_users) || in_array('4', $per_manage_users)) { ?>
        <th class="text-center">Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>
    <?php foreach ($users_list as $users): $email_cut = explode('@', $users['email1']);  $name = $email_cut[0];  ?>
    <tr>
      <td class="text-center"><?= $users['cust_id']; ?></td>
      <td class="text-center" title="<?= $users['email1'] ?>"><?= $users['firstname'] != "NULL" ? $users['firstname'] . ' ' . $users['lastname'] : ($users['company_name'] != "NULL" ? ucwords($users['company_name']) : ucwords($name)) ?></td>
      <!-- <td class="text-center"><?= $users['email1'] ?></td> -->
      <!-- <td class="text-center"><?= $users['user_type'] == 1 ? 'Individual' : 'Business'; ?></td>    -->
      <td class="text-center"><?= ucfirst($users['acc_type']); ?></td>
      <!--<td class="text-center">
          <?php  
              $current_balance = $this->user->get_current_balance($users['cust_id'],'USD');
              echo '$'; echo ($current_balance != NULL )?$current_balance['account_balance'] : 0;
          ?>
      </td>-->
      <td class="text-center"><?= $users['cust_otp'] ?></td>
      <td class="text-center">
        <?php 
              $country_details = $this->notice->get_country_detail($users['country_id']);
              $country_name = $country_details['country_name'];
              echo $country_name;
        ?>
      </td>
      <td class="text-center">
        <?php 
        $to_time = strtotime($users['last_login_datetime']);
        $from_time = strtotime(date('Y-m-d H:i:s'));
          $min = round(abs($to_time - $from_time) / 60,2);
          if($min > 10 ) { echo '<span class="text-danger">Offline</span>'; }
          else { echo '<span class="text-success">Online</span>'; }
        ?>
      </td>
      <td class="text-center"><?= date('d/M/Y',strtotime($users['last_login_datetime'])); ?></td>
      <td class="text-center"><?= $this->users->get_dedicated_users_pending_order_sum($users['cust_id'])['pending'] + $this->users->get_dedicated_users_pending_order_sum($users['cust_id'])['gcommission'] ?></td>
      <td class="text-center">
        <?php if($users['is_dedicated'] == 1) { ?>
          <select id="commission_type_<?=$users['cust_id']?>" name="commission_type" class="commission_type dt-select2">
            <option value="0">Select Type</option>
            <option value="1" <?=($users['commission_type'] == 1)?'selected':''?>>Default Rate</option>
            <option value="2" <?=($users['commission_type'] == 2)?'selected':''?>>By %</option>
            <option value="3" <?=($users['commission_type'] == 3)?'selected':''?>>Absolute Rate</option>
          </select>
        <?php } ?>
        <input type="hidden" name="cust_id" id="cust_id_<?=$users['cust_id']?>" value="<?=$users['cust_id']?>">
        <script>
          $("#commission_type_<?=$users['cust_id']?>").on('change', function(event) { 
            var commission_type = $("#commission_type_<?=$users['cust_id']?> option:selected").val();
            var cust_id = $("#cust_id_<?=$users['cust_id']?>").val();
            //alert(commission_type);
            //alert(cust_id);
            if(commission_type != '' && cust_id != '') {
              $.post("<?=base_url('admin/users/set-gonagoo-commission');?>", { commission_type: commission_type, cust_id: cust_id }, function(res) {        
              if(res != "null"){
                if(res == 'success') {
                  swal('Success!', 'Gonagoo Commission type has been set.'). then(function(){   window.location.reload();  });
                } else {
                  swal('Error!', 'Failed to set Gonagoo commission type. Try again...!');
                }
              }
              else { swal('Error!', 'While setting Gonagoo commission type. Try again...!'); }
              });
            } else { swal('Error!', 'Invalid Data. Try again...!'); }
          });
        </script>
      </td>

      <?php if(in_array('1', $per_manage_users) || in_array('4', $per_manage_users)): ?>
        <td class="text-center">
          
          <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
              Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown has-sub" role="menu">
              <?php if(in_array('1', $per_manage_users)): ?>
              <li>  <a href="<?= base_url('admin/view-users/') . $users['cust_id']; ?>"> <i class="entypo-eye"></i> View </a> </li>
              <li class="divider"></li>
              <li>  <a href="<?= base_url('admin/view-users/') . $users['cust_id']; ?>"> <i class="fa fa-send"></i> Notification </a> </li>
              <?php if($users['is_dedicated']==1 && ($users['commission_type']==2 || $users['commission_type']==3)):?>
                <li class="divider"></li>
                <li>  <a href="<?= base_url('admin/users/define-user-commission/') . $users['cust_id']; ?>"> <i class="fa fa-money"></i> Define Gonagoo Commission </a> </li>
              <?php endif;?>
            <?php endif; ?>
            <?php if(in_array('4', $per_manage_users)): ?>
              <?php if($users['acc_type'] == 'buyer' || $users['acc_type'] == 'both'): ?>
                <li class="divider"></li>
                <?php if($users['cust_status'] == 0): ?>
                  <li> <a data-toggle="tooltip" data-placement="left" title="Click to activate" data-original-title="Click to activate" onclick="activate_users('<?= $users['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-thumbs-up"></i>Activate </a>  </li>
                <?php else: ?>
                  <li> <a data-toggle="tooltip" data-placement="left" title="Click to inactivate" data-original-title="Click to inactivate" onclick="inactivate_users('<?= $users['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-cancel"></i>  Inactivate </a> </li>
                <?php endif; ?>
                <li class="divider"></li>
                <?php if($users['is_dedicated'] == 0): ?>
                  <li> <a data-toggle="tooltip" data-placement="left" title="Make user dedicated" data-original-title="Make user dedicated" onclick='activate_dedicate_users("<?= $users['cust_id']; ?>");' style="cursor:pointer;"> <i class="entypo-thumbs-up"></i> Make user dedicated </a></li>
                <?php else: ?>
                  <li> <a data-toggle="tooltip" data-placement="left" title="Remove dedicated" data-original-title="Remove dedicated" onclick='inactivate_dedicate_users("<?= $users['cust_id']; ?>");' style="cursor:pointer;"> <i class="entypo-cancel"></i> Remove dedicated </a></li>
                  <li class="divider"></li>
                  <li> <a data-toggle="tooltip" data-placement="left" title="View Pending Payments" data-original-title="View Pending Payments" href="<?= base_url('admin/users/pending-orders/'.$users['cust_id']); ?>" style="cursor:pointer;"> <i class="entypo-cancel"></i> View Pending Payments </a></li>
                <?php endif; ?>
                <li class="divider"></li>
                <?php if($users['allow_payment_delay'] == 0): ?>
                  <li> <a data-toggle="tooltip" data-placement="left" title="Activate Bank/Cheque Payment" data-original-title="Activate Bank/Cheque Payment" onclick='activate_users_bank_payment("<?= $users['cust_id']; ?>");' style="cursor:pointer;"> <i class="entypo-thumbs-up"></i> Activate Bank/Cheque Payment </a></li>
                <?php else: ?>
                  <li> <a data-toggle="tooltip" data-placement="left" title="Inactivate Bank/Cheque Payment" data-original-title="Inactivate Bank/Cheque Payment" onclick='inactivate_users_bank_payment("<?= $users['cust_id']; ?>");' style="cursor:pointer;"> <i class="entypo-cancel"></i> Inactivate Bank/Cheque Payment </a></li>
                <?php endif; ?>
              <?php endif; ?>
            <?php endif; ?>
            </ul>
          </div>
        </td>
      <?php endif; ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true,
      "order": [[7,'desc']],
      drawCallback: function() {
        $('.dt-select2').select2();
      }
    });
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });
    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    });
  });

  function inactivate_users(id=0){
    swal({
      title: 'Are you sure?',
      text: "You want to to inactivate this user?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, inactivate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('users/inactivate', {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Inactive!',
            'User has been inactivated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'User inactivation failed.',
            'error'
          )
        }
      });
      
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

  function activate_users(id=0){
    swal({
      title: 'Are you sure?',
      text: "You want to to activate this authority type?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, activate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('users/activate', {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Active!',
            'User has been activated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'User activation failed.',
            'error'
          )
        }
      });
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

  function activate_dedicate_users(id=0){
    console.log(id);
    swal({
      title: 'Are you sure?',
      text: "You want to make user as dedicated?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, activate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post("<?=base_url('admin/users/activate-dedicated')?>", {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Active!',
            'User has been activated as dedicated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'User activation failed.',
            'error'
          )
        }
      });
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

  function inactivate_dedicate_users(id=0){
    console.log(id);
    swal({
      title: 'Are you sure?',
      text: "You want to inactivate as dedicated user?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, inactivate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post("<?=base_url('admin/users/inactivate-users-dedicated')?>", {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Inactive!',
            'User has been inactivated as dedicated user.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'User inactivation failed.',
            'error'
          )
        }
      });
      
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

  function activate_users_bank_payment(id=0){
    console.log(id);
    swal({
      title: 'Are you sure?',
      text: "You want to activate bank/cheque payment?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, activate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post("<?=base_url('admin/users/activate-payment-type')?>", {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Active!',
            'Bank/Cheque payment activated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'Bank/Cheque payment activation failed.',
            'error'
          )
        }
      });
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

  function inactivate_users_bank_payment(id=0){
    console.log(id);
    swal({
      title: 'Are you sure?',
      text: "You want to inactivate Bank/Cheque payment?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, inactivate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post("<?=base_url('admin/users/inactivate-payment-type')?>", {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Inactive!',
            'Bank/Cheque payment inactivated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'Bank/Cheque payment inactivation failed.',
            'error'
          )
        }
      });
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }
</script>