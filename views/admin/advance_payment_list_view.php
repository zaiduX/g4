<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Advance Payment</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>      
<h2 style="display: inline-block;">Advance Payment</h2>
<?php
$per_adv_pay = explode(',', $permissions[0]['advance_payment']);

if(in_array('2', $per_adv_pay)) { ?>
  <a type="button" href="<?= base_url('admin/advance-payment/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
    Add New
    <i class="entypo-plus"></i>
  </a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Category</th>
      <th class="text-center">Country</th>
      <th class="text-center">Creation Date</th>
      <?php if(in_array('3', $per_adv_pay) || in_array('5', $per_adv_pay)) { ?>
        <th class="text-center" >Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>
    <?php  foreach ($adv_payments as $pay):  ?>
    <tr>
      <td class="text-center"><?= $pay['pay_id']; ?></td>
      <td class="text-center"><?= $pay['cat_name']; ?></td>
      <td class="text-center"><?= $pay['country_name']; ?></td>
      <td class="text-center"><?= date('d-m-Y',strtotime($pay['cre_datetime'])); ?></td>
      <?php if(in_array('3', $per_adv_pay) || in_array('5', $per_adv_pay)): ?>

        <td class="text-center">
          <?php if(in_array('3', $per_adv_pay)): ?>
            <a href="<?= base_url('admin/advance-payment/edit/').$pay['pay_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to View Full Details & Edit">
              <i class="entypo-eye"></i> View &amp; Edit 
            </a> &nbsp;
          <?php endif; ?>
          <?php if(in_array('5', $per_adv_pay)): ?>
            <button class="btn btn-danger btn-sm btn-icon icon-left"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_payment('<?= $pay["pay_id"]; ?>');"> <i class="entypo-cancel"></i> Delete 
            </button>  
          <?php endif; ?>
        </td>        
      <?php endif; ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "order" : [[0, "desc"]]
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );    
  } );


  function delete_payment(id=0){    
    swal({
      title: 'Are you sure?',
      text: "You want to to delete this advance payment?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('advance-payment/delete', {id: id}, function(res){ 
        if(res == 'success'){ 
          swal(
            'Deleted!',
            'Advance payment has been deleted.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'Advance payment deletion failed.',
            'error'
          )
        }
      });
      
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

</script>