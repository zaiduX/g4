<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/withdraw-request'); ?>">Withdraw Request</a>
	</li>
	<li class="active">
		<strong>Accept</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-dark" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">Accept Withdraw Request </div>
				<div class="panel-options"><a href="<?= base_url('admin/withdraw-request'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a></div>
			</div>
			<div class="panel-body">
				<?php if($this->session->flashdata('error')):  ?>
		      	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>

				<div class="well well-sm hidden">
					<h6>Please fill payment details.</h6>
				</div>
				<div class="steps-progress hidden">
					<div class="progress-indicator"></div>
				</div>
				<ul class="hidden">
					<li class="active">
						<a href="#tab2-1" data-toggle="tab"><span>1</span>Relay Point Manager</a>
					</li>
					<li>
						<a href="#tab2-2" data-toggle="tab"><span>2</span>Relay Point</a>
					</li>
					<li>
						<a href="#tab2-3" data-toggle="tab"><span>3</span>Warehouse</a>
					</li>
					<li>
						<a href="#tab2-5" data-toggle="tab"><span>5</span>Finish</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab2-1">
						<div class="row">
							<div class="col-md-12 form-group">
								<div class="col-md-3">
									<label class="control-label" for="full_name">Requestee</label>
								</div>
								<div class="col-md-9">
									<?php if($request_details['user_type'] == '1') { echo $this->withdraw->get_relay_details($request_details['user_id']); } else { echo $this->withdraw->get_customer_details($request_details['user_id']); } ?>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<div class="col-md-3">
									<label class="control-label" for="full_name">Amount Requested</label>
								</div>
								<div class="col-md-3">
									<?= $request_details['currency_code'] . ' ' . $request_details['amount'] ?>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<div class="col-md-3">
									<label class="control-label" for="full_name">Job ID</label>
								</div>
								<div class="col-md-3">
									<?= $request_details['job_id']?>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<div class="col-md-3">
									<label class="control-label" for="full_name">Milestone ID</label>
								</div>
								<div class="col-md-3">
									<?=($request_details['milestone_id']!="NULL")?$request_details['milestone_id']:"-"?>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<div class="col-md-3">
									<label class="control-label" for="last_name">Request Date</label>
								</div>
								<div class="col-md-3">
									<?= $request_details['req_datetime'] ?>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<div class="col-md-3">
									<label class="control-label" for="mobile1">Requested User Type</label>
								</div>
								<div class="col-md-3">
									<?= $request_details['user_type'] == 1 ? 'Relay Point' : 'User' ?>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<div class="col-md-3">
									<label class="control-label" for="mobile1">Requested Account Type</label>
								</div>
								<div class="col-md-3">
									<?=strtoupper($request_details['transfer_account_type']);?>
								</div>
							</div>
							<div class="col-md-12 form-group">
								<div class="col-md-3">
									<label class="control-label" for="mobile1">Requested Account Number</label>
								</div>
								<div class="col-md-3">
									<?= $request_details['transfer_account_number'] ?>
								</div>
							</div>
						</div>
						
						<?php if(strtolower($request_details['transfer_account_type']) != 'mtn') { ?>
							<div class="row">
								<div class="col-md-12 form-group">
									<label class="control-label">Amount Transfer Details and Accept Request</label><br />
								</div>
							</div>
							<form id="rootwizard-2" method="post" action="<?= base_url('admin/update-withdraw-transfer-amount'); ?>" class="form-wizard validate" enctype="multipart/form-data" >
								<input type="hidden" name="req_id" value="<?= $request_details['req_id'] ?>">
								<input type="hidden" name="job_id" value="<?= $request_details['job_id']?>">
								<input type="hidden" name="milestone_id" value="<?= $request_details['milestone_id']?>">
								<input type="hidden" name="user_id" value="<?= $request_details['user_id'] ?>">
								<input type="hidden" name="user_type" value="<?= $request_details['user_type'] ?>">
								<input type="hidden" name="currency_code" value="<?= $request_details['currency_code'] ?>">
								<div class="row">
									<div class="col-md-6 form-group">
										<label class="control-label">Amount Transferred</label>
										<input type="text" class="form-control" name="amount_transferred" id="amount_transferred" placeholder="Enter transfer amount" value="<?= $request_details['amount'] ?>" />
									</div>
									<div class="col-md-6 form-group">
										<label class="control-label">Transaction ID</label>
										<input type="text" class="form-control" name="transaction_id" id="transaction_id" placeholder="Enter transfer Transaction ID" />
									</div>
								</div>
								<div class="form-group pull-right">
									<button type="submit" class="btn btn-primary">Accept Request</button>
								</div>
							</form>
						<?php } else { //echo 'MTN Tranfer Request'; ?>
							<form id="rootwizard-2" method="post" action="<?= base_url('admin/update-withdraw-transfer-amount'); ?>" class="form-wizard validate" enctype="multipart/form-data" >
								<input type="hidden" name="req_id" value="<?= $request_details['req_id'] ?>">
								<input type="hidden" name="user_id" value="<?= $request_details['user_id'] ?>">
								<input type="hidden" name="job_id" value="<?= $request_details['job_id']?>">
								<input type="hidden" name="user_type" value="<?= $request_details['user_type'] ?>">
								<input type="hidden" name="currency_code" value="<?= $request_details['currency_code'] ?>">
								<input type="hidden" name="amount_transferred" value="<?= $request_details['amount'] ?>">
								<input type="hidden" name="transaction_id" value="mtn">
								<input type="hidden" name="transfer_account_number" value="<?= $request_details['transfer_account_number'] ?>">
								<div class="form-group pull-right">
									<button type="submit" class="btn btn-primary">Accept Request</button>
								</div>
							</form>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br />

<script>
	$("#rootwizard-2")
		.validate({
	    rules: {
	       amount_transferred: { required : true, number : true, min: <?= $request_details['amount'] ?>, max: <?= $request_details['amount'] ?> },
	        transaction_id: { required : true, },
	    },
	    messages: {
	       amount_transferred: { required : "Please enter amount to be transfer!", number : "Numbers only!", min: "Value should be equal to requested amount!", max: "Value should not be more than requested amount!" },
	       transaction_id: { required : "Please enter transaction ID!",  },
	    },
	});
</script>