<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/laundry-categories'); ?>">Laundry Categories</a>
	</li>
	<li class="active">
		<strong>Edit Laundry Category</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Edit Laundry Cloth Category
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/laundry-categories'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/laundry-categories/update'); ?>" class="form-horizontal form-groups-bordered" id="laundry_category_add" method="post" autocomplete="off" enctype="multipart/form-data">
					<div class="row">
						<input type="hidden" name="sub_cat_id" id="sub_cat_id" value="<?=$sub_category['sub_cat_id']?>">
						<div class="col-sm-3 text-center">
							<div class="form-group">
                              <label class="control-label">Sub Category Image </label>
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                  <img src="<?= ($sub_category['icon_url']!='NULL')?base_url($sub_category['icon_url']):$this->config->item('resource_url').'noimage.png';?>" alt="Category image">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                <div>
                                  <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="icon_url" id="icon_url" accept="image/*">
                                  </span>
                                  <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                              </div>
                            </div>
						</div>
						<div class="col-sm-9">
							<div class="form-group">
								<label for="cat_id" class="col-sm-3 control-label">Category Name :</label>						
								<div class="col-sm-9">
									<select class="form-control select2" name="cat_id" id="cat_id" style="box-shadow: 0 0 5px #3498db;">
					                  	<option value="">Select Category Name...</option>
				                  		<?php foreach ($category as $cat) { 
				                  		if($cat['cat_status']==1){ ?>
					                      	<option value="<?=$cat['cat_id']?>" <?=($cat['cat_id']==$sub_category['cat_id'])?'selected':''?>><?=ucwords($cat['cat_name'])?></option>
					                  	<?php }} ?>
					              	</select>
								</div>
							</div>
							<div class="form-group">
								<label for="sub_cat_name" class="col-sm-3 control-label">Sub Category Name :</label>						
								<div class="col-sm-9">
									<input type="text" class="form-control" id="sub_cat_name" placeholder="Enter Sub category Name..." name="sub_cat_name" value="<?=$sub_category['sub_cat_name']?>" />	
								</div>
							</div>	
							<div class="form-group">
								<label for="sub_cat_name" class="col-sm-3 control-label">Dimension (in CM) :</label>						
								<div class="col-sm-3">
									<input type="number" class="form-control" id="height" placeholder="Enter Height (cm)" name="height" min="0" value="<?=$sub_category['height']?>" />
					            </div>
								<div class="col-sm-3">
									<input type="number" class="form-control" id="width" placeholder="Enter Width (cm)" name="width" min="0" value="<?=$sub_category['width']?>" />    
					            </div>
								<div class="col-sm-3">
									<input type="number" class="form-control" id="length" placeholder="Enter Length (cm)" name="length" min="0" value="<?=$sub_category['length']?>" />
								</div>
							</div>	
							<div class="form-group">
								<label for="weight" class="col-sm-3 control-label">Weight (in grms) :</label>						
								<div class="col-sm-9">
									<input type="number" class="form-control" id="weight" placeholder="Enter Weight" name="weight" min="0" value="<?=$sub_category['weight']?>" />
					            </div>
					        </div>
							<div class="form-group text-right">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-blue">Update Category</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>	
	</div>
</div>

<script>
	$("#laundry_category_add")
    .validate({
      	ignore: [], 
      	rules: {
			cat_id: { required : true },
			sub_cat_name: { required : true },
			height: { required : true, number: true, min: 0 },
			width: { required : true, number: true, min: 0 },
			length: { required : true, number: true, min: 0 },
			weight: { required : true, number: true, min: 0 },
      	},
      	messages: {
	        cat_id: { required :'Select Category Name!'},
	        sub_cat_name: { required :'Enter Sub Category Name!'},
	        height: { required :'Enter Height in cm!', number: 'Enter numbers only', min: 'Not less than Zero(0)'},
	        width: { required :'Enter Width in cm!', number: 'Enter numbers only', min: 'Not less than Zero(0)'},
	        length: { required :'Enter Length in cm!', number: 'Enter numbers only', min: 'Not less than Zero(0)'},
	        weight: { required :'Enter Weight in grms!', number: 'Enter numbers only', min: 'Not less than Zero(0)'},
      	},
  	});
</script>