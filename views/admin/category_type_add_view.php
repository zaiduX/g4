<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/category-type'); ?>">Category Type Master</a>
	</li>
	<li class="active">
		<strong>Add New</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Add Category Type
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/category-type'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/register-category-type'); ?>" class="form-horizontal form-groups-bordered" id="categoryType" method="post">
	
					<div class="form-group">
						<label for="cat_type" class="col-sm-3 control-label">Category Type Name</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" id="cat_type" placeholder="Enter Category Type Name" name="cat_type" />	
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-blue">Add Category Type</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />