<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/carrier-type'); ?>">Carrier Type</a>
	</li>
	<li class="active">
		<strong>Add New</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Add Carrier Type
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/carrier-type'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" class="form-horizontal form-groups-bordered" id="carrierType" method="post" action="<?= base_url('admin/add-carrier-type-details'); ?>">
	
					<div class="form-group">
						<label for="carrier_title" class="col-sm-3 control-label">Carrier Type Title</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" id="carrier_title" placeholder="Enter title" name="carrier_title" />	
						</div>
					</div>

					<div class="form-group">
						<label for="carrier_desc" class="col-sm-3 control-label">Carrier Typ Description</label>
						
						<div class="col-sm-5">
							<textarea class="form-control" id="carrier_desc" placeholder="Enter description" name="carrier_desc" ></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-blue">Add Carrier Type</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />

<script>
$(function(){
    $("#carrierType").validate({
		ignore: [],
		rules: { carrier_title: { required: true, }, 
		}, 
		messages: {
			carrier_title: { required: "Please enter title!", },
		}
    });
});
</script>