<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/driver-categories'); ?>">Driver Categories</a>
	</li>
	<li class="active">
		<strong>Update</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Update Driver Category
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/driver-categories'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/driver-categories/update'); ?>" class="form-horizontal form-groups-bordered" id="driver_category_add" method="post" autocomplete="off">
					<input type="hidden" name="pc_id" value="<?= $category['pc_id']; ?>" />
					<div class="form-group">
						<label for="short_name" class="col-sm-3 control-label">Short Name :</label>						
						<div class="col-sm-3">
							<input type="text" class="form-control" id="short_name" placeholder="Enter Short Name..." name="short_name" value="<?= $category['short_name'] ?>" />	
						</div>
					</div>

					<div class="form-group">
						<label for="full_name" class="col-sm-3 control-label">Full Name :</label>						
						<div class="col-sm-5">
							<input type="text" class="form-control" id="full_name" placeholder="Enter Full Name..." name="full_name" value="<?= $category['full_name'] ?>" />	
						</div>
					</div>

					<div class="form-group">
						<label for="description" class="col-sm-3 control-label">Description :</label>						
						<div class="col-sm-8">
							<input type="text" class="form-control" id="description" placeholder="Enter Description..." name="description" value="<?= $category['description'] ?>" />	
						</div>
					</div>					

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-blue">update Category</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />