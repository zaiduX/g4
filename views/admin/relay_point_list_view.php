<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Relay Point List</strong>
	</li>
</ol>
<style> .dropdown-menu { left: auto; right: 0 !important; } </style>			
<h2 style="display: inline-block;">Relay Point List</h2>
<?php
$per_manage_authority = explode(',', $permissions[0]['manage_authority']);
if(in_array('2', $per_manage_authority)) { ?>
	<a type="button" href="<?= base_url('admin/add-relay-point'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add Relay Point
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

	<?php if($this->session->flashdata('error')):  ?>
  		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	<?php endif; ?>
	<?php if($this->session->flashdata('success')):  ?>
  		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	<?php endif; ?>


<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Manager</th>
			<th class="text-center">Email</th>
			<th class="text-center">Contact</th>
			<th class="text-center">Country</th>
			<th class="text-center">State</th>
			<th class="text-center">City</th>						
			<th class="text-center">Total Orders</th>						
			<th class="text-center">Status</th>
			<?php if(in_array('2', $per_manage_authority) || in_array('3', $per_manage_authority) || in_array('4', $per_manage_authority) || in_array('5', $per_manage_authority)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($relay_points as $rp):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= $rp['firstname'] . ' ' . $rp['lastname'] ?></td>
			<td class="text-center"><?= $rp['email'] ?></td>
			<td class="text-center"><?= $rp['mobile_no'] ?></td>
			<td class="text-center">
		    <?php 
	        $country_details = $this->notice->get_country_detail($rp['country_id']);
	        echo $country_details['country_name'];
		    ?>
			</td>
			<td class="text-center">
		    <?php 
	        $state_details = $this->notice->get_state_detail($rp['state_id']);
	        echo $state_details['state_name'];
		    ?>
			</td>
			<td class="text-center">
		    <?php 
	        $city_details = $this->notice->get_city_detail($rp['city_id']);
	        echo $city_details['city_name'];
		    ?>
			</td>
                        <td class="text-center"><?= $rp['total_orders']; ?> </td>
			<?php if($rp['manager_status'] == 1): ?>
				<td class="text-center">
					<span class="text-success" >Active</span>
				</td>
			<?php else: ?>
				<td class="text-center">
					<span class="text-danger" >Inactive</span>
				</td>
			<?php endif; ?>
			<?php if(in_array('2', $per_manage_authority) || in_array('3', $per_manage_authority) || in_array('4', $per_manage_authority) || in_array('5', $per_manage_authority)) { ?>
				
				<td class="text-center">
					
					<div class="btn-group">
						<button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
							Action <span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown" role="menu">
							<?php if(in_array('3', $per_manage_authority)): ?>
								<li>	<a href="<?= base_url('admin/edit-relay-point/') . $rp['cd_id']; ?>"> <i class="entypo-pencil"></i>	Edit Manager Details</a>	</li>
								<li>	<a href="<?= base_url('admin/edit-relay-point-details/') . $rp['cd_id']; ?>"> <i class="entypo-pencil"></i>	Edit Relay Point</a>	</li>
						<?php endif; ?>
						<?php if(in_array('4', $per_manage_authority)): ?>
							<li class="divider"></li>
							<?php if($rp['manager_status'] == 0): ?>
								<li> <a data-toggle="tooltip" data-placement="left" title="Click to inactivate" data-original-title="Click to activate" onclick="inactivate_relay('<?= $rp['cd_id']; ?>');" style="cursor:pointer;"> <i class="entypo-thumbs-up"></i>Activate </a>	</li>
							<?php else: ?>
								<li> <a data-toggle="tooltip" data-placement="left" title="Click to inactivate" data-original-title="Click to inactivate" onclick="activate_relay('<?= $rp['cd_id']; ?>');" style="cursor:pointer;"> <i class="entypo-cancel"></i>	Inactivate </a>	</li>
							<?php endif; ?>
						<?php endif; ?>
						</ul>
					</div>
				</td>
			<?php } ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );

		

	} );

	function activate_relay(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this relay point?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('inactive-relay-point', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'Relay point has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Relay point inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate_relay(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this relay point?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('active-relay-point', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Relay point has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Relay point activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>