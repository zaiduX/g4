	<footer class="main hidden">	
		&copy; <?= date('Y') ?></a>
	</footer>
</div>
</div>
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="<?= $this->config->item('resource_url'). 'js/jvectormap/jquery-jvectormap-1.2.2.css'; ?>" />
	<link rel="stylesheet" href="<?= $this->config->item('resource_url') . '/js/rickshaw/rickshaw.min.css';?>">
	<link rel="stylesheet" href="<?= $this->config->item('resource_url') . '/js/daterangepicker/daterangepicker-bs3.css';?>">
	<!-- Bottom scripts (common) -->
	<script src="<?= $this->config->item('resource_url') . 'js/gsap/TweenMax.min.js';?>"></script>
	<script src="<?= $this->config->item('resource_url'). 'js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js'; ?>"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/joinable.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/resizeable.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/neon-api.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/jquery.bootstrap.wizard.min.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/jvectormap/jquery-jvectormap-1.2.2.min.js';?>"></script>
	<!-- Imported scripts on this page -->
	<script src="<?= $this->config->item('resource_url') . 'js/jvectormap/jquery-jvectormap-europe-merc-en.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/jquery.sparkline.min.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/rickshaw/vendor/d3.v3.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/rickshaw/rickshaw.min.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/raphael-min.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/morris.min.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/toastr.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/daterangepicker/moment.min.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/bootstrap-datepicker.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/bootstrap-timepicker.min.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/daterangepicker/daterangepicker.js';?>"></script>
	
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'js/datatables/datatables.css';?>">
	<link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'js/select2/select2-bootstrap.css';?>">
	<link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'js/select2/select2.css';?>">
	<!-- Imported scripts on this page -->
	<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

	<link href="//cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" />
	<script src="//cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	
	<script src="<?= $this->config->item('resource_url') . 'js/select2/select2.min.js';?>"></script>
	<script src="//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/form-validation.js';?>"></script>
	<script src="<?= $this->config->item('resource_url') . 'js/fileinput.js';?>"></script>
	<!-- JavaScripts initializations and stuff -->
	<script src="<?= $this->config->item('resource_url') . 'js/neon-custom.js';?>"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>

<script>
	$(function(){ $(".js-source-countries").select2(); });
</script>
</body>
</html>