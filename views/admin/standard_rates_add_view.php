<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/standard-rates'); ?>">Standard Rates</a>
	</li>
	<li class="active">
		<strong>Add New</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Add New Standard Rate
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/standard-rates'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php $error = $data = array(); if($error = $this->session->flashdata('error')): $data = $error['data']; ?>
		      		<div class="alert alert-danger text-center"><?= $error['error_msg']; ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/standard-rates/register'); ?>" class="form-horizontal form-groups-bordered" id="standard_rate_add" method="post">
					
					<div class="form-group">
						<label for="area_type" class="col-sm-3 control-label">Service Area :</label>
						
						<div class="col-sm-5">
							<div class="radio radio-replace color-blue col-md-4">
								<input type="radio" id="local" name="area_type" value="local" <?php if(!empty($data) && $data['service_area_type'] == 'local' ){ echo "checked"; } ?> />
								<label>Local</label>
							</div>
							
							<div class="radio radio-replace color-blue col-md-4">
								<input type="radio" id="national" name="area_type" value="national" <?php if(!empty($data) && $data['service_area_type'] == 'national' ){ echo "checked"; } ?>/>
								<label>National</label>
							</div>
							
							<div class="radio radio-replace color-blue col-md-4">
								<input type="radio" id="international" name="area_type" value="international" <?php if(!empty($data) && $data['service_area_type'] == 'international' ){ echo "checked"; } ?>/>
								<label>International</label>
							</div>													
							<span id="error_area_type"></span>
						</div>

					</div>

					<div class="form-group">
						<label for="rate_type" class="col-sm-3 control-label">Select Country :</label>						
						<div class="col-sm-5">							
							<select id="country_id" name="country_id" class="form-control">
								<option value="0">Select Country</option>
								<?php foreach ($countries_list as $country): ?>											
									<option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
								<?php endforeach ?>
							</select>
							<span id="error_country_id"></span>							
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Select Currency</label>						
						<div class="col-sm-5">							 
							<select id="currency_id" name="currency_id" class="form-control" data-allow-clear="true" data-placeholder="Select Currency" disabled>
								<option value="">Select Currency</option>
							</select>							
							<span id="error_currency_id"></span>							
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Width ( in cm ):</label>						
						<div class="col-sm-3">
							<input type="number" class="form-control" id="min_width" placeholder="Enter Minimum Width" name="min_width" <?php if(!empty($data)){ echo 'value="'.$data['min_weight'].'"'; } ?> />	
							<span id="error_min_width"></span>
						</div>
						<div class="col-sm-3">
							<input type="number" class="form-control" id="max_width" placeholder="Enter Maximum Width" name="max_width" <?php if(!empty($data)){ echo 'value="'.$data['min_weight'].'"'; } ?> />	
							<span id="error_max_width"></span>
						</div>						
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Height ( in cm ):</label>						
						<div class="col-sm-3">
							<input type="number" class="form-control" id="min_height" placeholder="Enter Minimum Height" name="min_height" <?php if(!empty($data)){ echo 'value="'.$data['min_height'].'"'; } ?> />	
							<span id="error_min_height"></span>
						</div>
						<div class="col-sm-3">
							<input type="number" class="form-control" id="max_height" placeholder="Enter Maximum Height" name="max_height" <?php if(!empty($data)){ echo 'value="'.$data['max_height'].'"'; } ?> />	
							<span id="error_max_height"></span>
						</div>						
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Length ( in cm ):</label>						
						<div class="col-sm-3">
							<input type="number" class="form-control" id="min_length" placeholder="Enter Minimum Length" name="min_length" <?php if(!empty($data)){ echo 'value="'.$data['min_length'].'"'; } ?> />	
							<span id="error_min_length"></span>
						</div>
						<div class="col-sm-3">
							<input type="number" class="form-control" id="max_length" placeholder="Enter Maximum Length" name="max_length" <?php if(!empty($data)){ echo 'value="'.$data['max_length'].'"'; } ?> />	
							<span id="error_max_length"></span>
						</div>						
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Distance ( in KM ):</label>						
						<div class="col-sm-3">
							<input type="number" class="form-control" id="min_distance" placeholder="Enter Minimum Distance" name="min_distance" <?php if(!empty($data)){ echo 'value="'.$data['min_distance'].'"'; } ?> />	
							<span id="error_min_distance"></span>
						</div>
						<div class="col-sm-3">
							<input type="number" class="form-control" id="max_distance" placeholder="Enter Maximum Distance" name="max_distance" <?php if(!empty($data)){ echo 'value="'.$data['max_distance'].'"'; } ?>  />	
							<span id="error_max_distance"></span>
						</div>						
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Weight :</label>						
						<div class="col-sm-3">
							<input type="number" class="form-control" id="min_weight" placeholder="Enter Minimum Weight" name="min_weight" <?php if(!empty($data)){ echo 'value="'.$data['min_weight'].'"'; } ?> />	
							<span id="error_min_weight"></span>
						</div>
						<div class="col-sm-3">
							<input type="number" class="form-control" id="max_weight" placeholder="Enter Maximum Weight" name="max_weight" <?php if(!empty($data)){ echo 'value="'.$data['max_weight'].'"'; } ?> />	
							<span id="error_max_weight"></span>
						</div>	
						<div class="form-group">
						<div class="col-sm-2">							
							<select id="unit_id" name="unit_id" class="form-control">
								<option value="0">Select Unit</option>
								<?php foreach ($unit_list as $unit): ?>											
									<option value="<?= $unit['unit_id'] ?>" <?php if(!empty($data) && $data['unit_id'] == $unit['unit_id']){ echo "selected"; } ?>>
										<?= $unit['shortname']; ?>										
										</option>
								<?php endforeach ?>
							</select>
							<span id="error_unit_id"></span>							
						</div>
					</div>					
					</div>
					
					<div class="form-group">
						<label for="max_duration" class="col-sm-3 control-label">Duration ( in Days) :</label>						
						<div class="col-sm-3">
							<input type="number" class="form-control" id="max_duration" placeholder="Enter Maximum Duration" name="max_duration" <?php if(!empty($data)){ echo 'value="'.$data['max_duration_days'].'"'; } ?> />	
							<span id="error_max_duration"></span>
						</div>
					</div> 

					<div class="form-group">
						<label for="rate" class="col-sm-3 control-label">Standard rate :</label>						
						<div class="col-sm-3">
							<input type="number" class="form-control" id="rate" placeholder="Enter standard rate ..." name="rate" <?php if(!empty($data)){ echo 'value="'.$data['service_rate'].'"'; } ?>  />	
							<span id="error_rate"></span>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button id="btn-submit" type="submit" class="btn btn-blue">Add Standard rate</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />


<script type="text/javascript">
	$(document).ready(function () {

			$("#country_id").on('change', function(event) {	event.preventDefault();
				var country_id = $(this).val();
				$.ajax({
          type: "POST", 
          url: "get-country-currencies", 
          data: { country_id: country_id },
          dataType: "json",
          success: function(res){
          	if(res.length > 0 ){
          		$('#currency_id').attr('disabled', false);
              $('#currency_id').empty(); 
              $('#currency_id').append('<option value="0">Select Currency</option>');
              $.each( res, function(){    
                  $('#currency_id').append('<option value="'+$(this).attr('currency_id')+'">'+$(this).attr('currency_title')+' ( ' + $(this).attr('currency_sign') + ' )</option>');
              });
              $('#currency_id').focus();
            } 
            else{
            	$('#currency_id').attr('disabled', true);
              $('#currency_id').empty();
              $('#currency_id').append('<option value="0">No currency found!</option>');
            }
          },
          beforeSend: function(){
            $('#currency_id').empty();
            $('#currency_id').append('<option value="0">Loading...</option>');
          },
          error: function(){
            $('#currency_id').attr('disabled', true);
            $('#currency_id').empty();
            $('#currency_id').append('<option value="0">No currency found!</option>');
          }
        })
			});

			$("#btn-submit").click(function(e){ e.preventDefault();
				$(".alert").removeClass('alert-danger alert-success').addClass('hidden').html("");
		    var area_type = $("input[name=area_type]:checked").val();
		    var country_id = $("#country_id").val();
		    var currency_id = $("#currency_id").val();
		    var min_width = parseInt($("#min_width").val());
		    var max_width = parseInt($("#max_width").val());
		    var min_height = parseInt($("#min_height").val());
		    var max_height = parseInt($("#max_height").val());
		    var min_length = parseInt($("#min_length").val());
		    var max_length = parseInt($("#max_length").val());
		    var min_distance = parseInt($("#min_distance").val());
		    var max_distance = parseInt($("#max_distance").val());
		    var min_weight = parseInt($("#min_weight").val());
		    var max_weight = parseInt($("#max_weight").val());
		    var unit_id = $("#unit_id").val();
		    var max_duration = $("#max_duration").val();
		    var rate = $("#rate").val();

		    if(typeof (area_type) === "undefined") {
		      $("#error_area_type").addClass('error').html("Please select the Service area.");
		    } 
		    else if(country_id == 0 ) {
		    	$("#error_area_type").removeClass('error').html("");
		      $("#error_country_id").addClass('error').html("Please select the country.");
		    }
		    else if(currency_id == 0 ) {
		    	$("#error_country_id, #error_area_type").removeClass('error').html("");
		      $("#error_currency_id").addClass('error').html("Please select the currency.");
		    }
		    else if(min_width == "" ) {
		    	$("#error_currency_id, #error_country_id, #error_area_type").removeClass('error').html("");
		      $("#error_min_width").addClass('error').html("Please enter minimum width.");
		    }		    
		    else if(max_width == "" ) {
		    	$("#error_min_width,#error_currency_id, #error_country_id, #error_area_type").removeClass('error').html("");
		      $("#error_max_width").addClass('error').html("Please enter maximum width.");
		    }
		    else if(max_width < min_width ) {
		    	$("#error_max_width, #error_min_width,#error_currency_id, #error_country_id, #error_area_type").removeClass('error').html("");
		      $("#error_max_width").addClass('error').html("Please enter maximum width greater than minimum width.");
		    }
		    else if(min_height == "" ) {
		    	$("#error_max_width, #error_min_width,#error_currency_id, #error_country_id, #error_area_type").removeClass('error').html("");
		      $("#error_min_height").addClass('error').html("Please enter minimum height.");
		    }
		    else if(max_height == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height").removeClass('error').html("");
		      $("#error_max_height").addClass('error').html("Please enter maximum height.");
		    }
		    else if(max_height < min_height ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_max_height, #error_min_height").removeClass('error').html("");
		      $("#error_max_height").addClass('error').html("Please enter maximum height greater than minimum height.");
		    }
		    else if(min_length == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		      $("#error_min_length").addClass('error').html("Please enter minimum length.");
		    }
		    else if(max_length == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_length").removeClass('error').html("");
		      $("#error_max_length").addClass('error').html("Please enter maximum length.");
		    }
		    else if(max_length < min_length ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_length, #error_max_length").removeClass('error').html("");
		      $("#error_max_length").addClass('error').html("Please enter maximum length greater than minimum length.");
		    }
		    else if(min_distance == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_length, #error_max_length").removeClass('error').html("");
		      $("#error_min_distance").addClass('error').html("Please enter minimum distance.");
		    }
		    else if(max_distance == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_length, #error_max_length, #error_min_distance").removeClass('error').html("");
		      $("#error_max_distance").addClass('error').html("Please enter maximum distance.");
		    }
		    else if(max_distance < min_distance ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_length, #error_max_length, #error_min_distance").removeClass('error').html("");
		      $("#error_max_distance").addClass('error').html("Please enter maximum distance greater than minimum distance.");
		    }
		    else if(min_weight == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		      $("#error_min_weight").addClass('error').html("Please enter minimum weight.");
		    }
		    else if(max_weight == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_weight, #error_min_distance, #error_max_distance").removeClass('error').html("");
		      $("#error_max_weight").addClass('error').html("Please enter maximum weight.");
		    }
		    else if(max_weight < min_weight ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_weight,#error_max_weight, #error_min_distance, #error_max_distance").removeClass('error').html("");
		      $("#error_max_weight").addClass('error').html("Please enter maximum weight greater than minimum weight.");
		    }
		    else if(unit_id == 0 ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_weight,#error_max_weight, #error_min_distance, #error_max_distance").removeClass('error').html("");
		      $("#error_unit_id").addClass('error').html("Please select unit.");
		    }
		    else if(max_duration == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_weight,#error_max_weight, #error_min_distance, #error_max_distance").removeClass('error').html("");
		    	$("#error_unit_id").removeClass('error').html("");
		      $("#error_max_duration").addClass('error').html("Please enter maximum duration.");
		    }
		    else if(rate == "" ) {
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_weight,#error_max_weight, #error_min_distance, #error_max_distance").removeClass('error').html("");
		    	$("#error_unit_id, #error_max_duration").removeClass('error').html("");
		      $("#error_rate").addClass('error').html("Please enter the rate.");
		    }
		    else { 
		    	$("#error_min_height,#error_max_width, #error_min_width,#error_currency_id").removeClass('error').html("");
		    	$("#error_country_id, #error_area_type, #error_min_height, #error_max_height").removeClass('error').html("");
		    	$("#error_min_length, #error_max_length, #error_min_distance, #error_max_distance").removeClass('error').html("");
		    	$("#error_unit_id, #error_max_duration, #error_area_type").removeClass('error').html(''); 
		    	$("#standard_rate_add")[0].submit(); 
		    }
			});	
			
	});
</script>