<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/admin-dashboard'); ?>"><i class="fa fa-home"></i>Admin Dashboard</a>
  </li>
  <li class="active">
    <strong>Upcoming Trip List</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>      
<h2 style="display: inline-block;">Upcoming Trip List</h2>

<?php if($this->session->flashdata('error')):  ?>
	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
<?php endif; ?>
<?php if($this->session->flashdata('success')):  ?>
	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
<?php endif; ?>

<table class="table table-bordered display compact table-striped" id="table-2" style="max-width=100%">
  <thead>
    <tr>
      <th class="text-center">Seat Id</th>
      <th class="text-center">journey Date</th>
      <th class="text-center">Name</th>
      <th class="text-center">From</th>
      <th class="text-center">To</th>
      <th class="text-center">Country</th>
      <th class="text-center">Price</th>
      <th class="text-center">Status</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>
  
  <tbody>   
    <?php foreach ($jobs as $job):  ?>
    <tr>
      <td class="text-center"><?= $job['ticket_id'] ?></td>
      <td class="text-center"><?= date('d/m/Y', strtotime($job['journey_date'])); ?></td> 
      <td class="text-center">
          <?= $job['cust_name']?>
      </td>
      <td class="text-center"><?= $job['source_point'] ?></td>
      <td class="text-center"><?= $job['destination_point'] ?></td>
      <td class="text-center">
        <?php 
            $country_details = $this->notice->get_country_detail($job['country_id']);
            $country_name = $country_details['country_name'];
            echo $country_name;
      	?>
      </td>
      <td class="text-center"><?= $job['ticket_price'].$job['currency_sign']; ?></td>
      <td class="text-center">
        <?php 
          if($job['journey_date'] > date('m/d/Y')){ 
            echo '<h5>Upcoming</h5>';
          } 
          else if($job['journey_date'] == date('m/d/Y')){ 
            echo '<h5>Current</h5>';
          }
          else if($job['journey_date'] < date('m/d/Y')){ 
            echo '<h5>Previous</h5>';
          }
          else if($job['trip_status'] == 'complete'){ 
            echo '<h5>Completed</h5>';
          }
          else { echo '<h5>-</h5>';  }

        ?>
      </td>

        <td class="text-center">
          <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-cogs"></i>
            </button>
            <ul class="dropdown-menu dropdown" role="menu">
                <li>  <a href="<?= base_url('admin/view-bus-details/') . $job['ticket_id']; ?>" data-toggle="tooltip" data-placement="left" title="Click to See Details" data-original-title="Click to See Details"> <i class="entypo-eye"></i> View </a> </li>
            </ul>
          </div>
        </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true,
      
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );
  } );



</script>