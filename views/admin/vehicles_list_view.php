<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Transport Vehicles</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">Transport Vehicles</h2>
<?php
$per_vehicles = explode(',', $permissions[0]['vehicals']);

if(in_array('2', $per_vehicles)) { ?>
	<a type="button" href="<?= base_url('admin/transport-vehicles/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add New
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Category</th>
			<th class="text-center">Transport Mode</th>
			<th class="text-center">Vehicles Type</th>
			<?php if(in_array('3', $per_vehicles) || in_array('5', $per_vehicles)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($vehicles as $dim):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?php if($dim['cat_id'] == 6) { echo 'TRANSPORT (FREIGHT)'; }
			                                else if ($dim['cat_id'] == 7) { echo 'COURIER & PARCEL DELIVERY'; } 
			                                else if ($dim['cat_id'] == 281) { echo 'TICKET BOOKING'; } 
			                                else { echo 'HOME MOVE' ; } ?></td>
			
			<td class="text-center"><?= ucfirst($dim['transport_mode']); ?></td>
			<td class="text-center"><?= $dim['vehicle_type'] ?></td>
			<?php if(in_array('3', $per_vehicles) || in_array('5', $per_vehicles)): ?>
				<td class="">
					<?php if(in_array('3', $per_vehicles)): ?>
						<a href="<?= base_url('admin/transport-vehicles/edit/').$dim['vehical_type_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
					<?php endif; ?>
					<?php if(in_array('5', $per_vehicles)): ?>
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_vehicle('<?= $dim["vehical_type_id"]; ?>');">
							<i class="entypo-cancel"></i>
							Delete
						</button>
					<?php endif; ?>
					<?php if(in_array('2', $per_vehicles) && $dim['cat_id'] == 281): ?>
						<a href="<?= base_url('admin/transport-vehicles/define-seat-type/').$dim['vehical_type_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Seat Type
						</a>
					<?php endif; ?>
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	} );


	function delete_vehicle(id=0){		
		swal({
		  title: 'Are you sure?',
		  text: "You want to to delete this dimension?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('transport-vehicles/delete', {id: id}, function(res){ 
				if(res == 'success'){ 
				  swal(
				    'Deleted!',
				    'Vehicle has been deleted.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Vehicle deletion failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

</script>