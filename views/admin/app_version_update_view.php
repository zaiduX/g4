		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li class="active">
				<strong>Update App Versions</strong>
			</li>
		</ol>
	
		<div class="row">
			<div class="col-md-12">
				<!-- Android Main App Version -->
				<div class="col-md-6">
					<div class="panel panel-dark" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								Update Android Main App Version
							</div>
							
							<div class="panel-options">
								<a href="<?= base_url('admin/dashboard'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
							</div>
						</div>
						
						<div class="panel-body">
							
							<?php if($this->session->flashdata('error1')): ?>
					      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error1'); ?></div>
						    <?php elseif($this->session->flashdata('success1')):  ?>
						    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success1'); ?></div>
						    <?php endif; ?>

							<form role="form" class="form-horizontal form-groups-bordered" id="main-app-edit" method="post" action="<?= base_url('admin/update-main-app-version'); ?>">
								<input type="hidden" id="main_app_id" name="main_app_id" value="<?= $main_app['id']; ?>" />

								<div class="form-group">
									<label for="current_version" class="col-sm-4 control-label">Current Version</label>
									
									<div class="col-sm-8">
										<input type="text" class="form-control" id="current_version" name="current_version" value="<?= $main_app['app_version']; ?>" readonly />
									</div>
								</div>

								<div class="form-group">
									<label for="main_app_new_version" class="col-sm-4 control-label">Update Version</label>
									
									<div class="col-sm-8">
										<input type="text" class="form-control" id="main_app_new_version" placeholder="Enter new version..." name="main_app_new_version" />	
									</div>
								</div>
								<div class="form-group">
									<label for="auth_email" class="col-sm-4 control-label">Force Update</label>
									<div class="col-sm-8">
										<input type="radio" class="radio radio-inline" name="force_update_main" value="1" required="required" /> Yes
										<input type="radio" class="radio radio-inline" name="force_update_main" value="0" required="required" /> No
									</div>
								</div>

								<div class="form-group text-center">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-blue" id="btnMainApp">Update Version</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- iOS Main App Version -->
				<div class="col-md-6">
					<div class="panel panel-dark" data-collapsed="0">
						<div class="panel-heading">
							<div class="panel-title">
								Update iOS Main App Version
							</div>
							
							<div class="panel-options">
								<a href="<?= base_url('admin/dashboard'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
							</div>
						</div>
						
						<div class="panel-body">
							
							<?php if($this->session->flashdata('error3')): ?>
					      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error3'); ?></div>
						    <?php elseif($this->session->flashdata('success3')):  ?>
						    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success3'); ?></div>
						    <?php endif; ?>

							<form role="form" class="form-horizontal form-groups-bordered" id="ios-main-app-edit" method="post" action="<?= base_url('admin/update-ios-main-app-version'); ?>">
								<input type="hidden" id="ios_main_app_id" name="ios_main_app_id" value="<?= $main_app['id']; ?>" />

								<div class="form-group">
									<label for="current_ios_version" class="col-sm-4 control-label">Current Version</label>
									
									<div class="col-sm-8">
										<input type="text" class="form-control" id="current_ios_version" name="current_ios_version" value="<?= $main_app['app_version_ios']; ?>" readonly />
									</div>
								</div>

								<div class="form-group">
									<label for="ios_main_app_new_version" class="col-sm-4 control-label">Update Version</label>
									
									<div class="col-sm-8">
										<input type="text" class="form-control" id="ios_main_app_new_version" placeholder="Enter new version..." name="ios_main_app_new_version" />	
									</div>
								</div>
								<div class="form-group">
									<label for="auth_email" class="col-sm-4 control-label">Force Update</label>
									<div class="col-sm-8">
										<input type="radio" class="radio radio-inline" name="force_update_ios_main" value="1" required="required" /> Yes
										<input type="radio" class="radio radio-inline" name="force_update_ios_main" value="0" required="required" /> No
									</div>
								</div>

								<div class="form-group text-center">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-blue" id="btniOSMainApp">Update Version</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<!-- Android Driver App Version -->
				<div class="col-md-6">
					<div class="panel panel-dark" data-collapsed="0">	
						<div class="panel-heading">
							<div class="panel-title">
								Update Android Driver App Version
							</div>
							
							<div class="panel-options">
								<a href="<?= base_url('admin/dashboard'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
							</div>
						</div>

						<div class="panel-body">
							<?php if($this->session->flashdata('error2')): ?>
					      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error2'); ?></div>
						    <?php elseif($this->session->flashdata('success2')):  ?>
						    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success2'); ?></div>
						    <?php endif; ?>

							<form role="form" class="form-horizontal form-groups-bordered" id="driver-app-edit" method="post" action="<?= base_url('admin/update-driver-app-version'); ?>">
								<input type="hidden" id="driver_app_id" name="driver_app_id" value="<?= $driver_app['id']; ?>" />

								<div class="form-group">
									<label for="driver_current_version" class="col-sm-4 control-label">Current Version</label>
									
									<div class="col-sm-8">
										<input type="text" class="form-control" id="driver_current_version" name="driver_current_version" value="<?= $driver_app['app_version']; ?>" readonly />
									</div>
								</div>

								<div class="form-group">
									<label for="driver_app_new_version" class="col-sm-4 control-label">Update Version</label>
									
									<div class="col-sm-8">
										<input type="text" class="form-control" id="driver_app_new_version" placeholder="Enter new version..." name="driver_app_new_version" />	
									</div>
								</div>
								<div class="form-group">
									<label for="auth_email" class="col-sm-4 control-label">Force Update</label>
									<div class="col-sm-8">
										<input type="radio" class="radio radio-inline" name="force_update_driver" value="1" required="required" /> Yes
										<input type="radio" class="radio radio-inline" name="force_update_driver" value="0" required="required" /> No
									</div>
								</div>

								<div class="form-group text-center">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-blue" id="btnDriverApp">Update Version</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<br />

	<!--<script>
    	$(function(){
	        $("#main-app-edit").submit(function(e) { e.preventDefault(); });
	        $("#btnMainApp").on('click', function(e) {  e.preventDefault();
	          	var current_version = parseFloat($("#current_version").val()).toFixed(2);
	          	var main_app_new_version = parseFloat($("#main_app_new_version").val()).toFixed(2);    
				var force_update = $("input[name='force_update_main']:checked").val();
	          	if(main_app_new_version <= current_version ) {  swal('Error','Please enter version greater than current version!','warning');   } 
	          	else if(isNaN(main_app_new_version)) {  swal('Error','Enter valid version! Enter numbers only.','warning');   }
	          	
	          	else if(force_update != '0' && force_update != '1') {  swal('Error','Please select force update type!','warning');   } 
	          	
	          	else {  $("#main-app-edit")[0].submit();  }
	        });
      	});
    </script>

    <script>
    	$(function(){
	        $("#ios-main-app-edit").submit(function(e) { e.preventDefault(); });
	        $("#btniOSMainApp").on('click', function(e) {  e.preventDefault();
	          	var current_ios_version = parseFloat($("#current_ios_version").val()).toFixed(2);
	          	var ios_main_app_new_version = parseFloat($("#ios_main_app_new_version").val()).toFixed(2);    
				var force_ios_update = $("input[name='force_update_ios_main']:checked").val();
	          	if(ios_main_app_new_version <= current_ios_version ) {  swal('Error','Please enter version greater than current version!','warning');   } 
	          	else if(isNaN(ios_main_app_new_version)) {  swal('Error','Enter valid version! Enter numbers only.','warning');   }
	          	
	          	else if(force_ios_update != '0' && force_ios_update != '1') {  swal('Error','Please select force update type!','warning');   } 
	          	
	          	else {  $("#ios-main-app-edit")[0].submit();  }
	        });
      	});
    </script>

    <script>
    	$(function(){
	        $("#driver-app-edit").submit(function(e) { e.preventDefault(); });
	        $("#btnDriverApp").on('click', function(e) {  e.preventDefault();
	          	var driver_current_version = parseFloat($("#driver_current_version").val()).toFixed(2);
	          	var driver_app_new_version = parseFloat($("#driver_app_new_version").val()).toFixed(2);    
				var force_update_driver = $("input[name='force_update_driver']:checked").val();
	          	if(driver_app_new_version <= driver_current_version ) {  swal('Error','Please enter version greater than current version!','warning');   } 
	          	else if(isNaN(driver_app_new_version)) {  swal('Error','Enter valid version! Enter numbers only.','warning');   }
	          	
	          	else if(force_update_driver != '0' && force_update_driver != '1') {  swal('Error','Please select force update type!','warning');   } 
	          	
	          	else {  $("#driver-app-edit")[0].submit();  }
	        });
      	});
    </script>-->

    <script>
    	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
		    $(".alert").slideUp(500);
		});
    </script>