<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/laundry-categories'); ?>">Laundry Categories</a>
	</li>
	<li class="active">
		<strong>Edit Laundry Main Category</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Edit Laundry Main Category
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/laundry-categories'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/laundry-main-categories/update'); ?>" class="form-horizontal form-groups-bordered" id="laundry_category_add" method="post" autocomplete="off" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-3 col-md-offset-2 text-center">
							<div class="form-group">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 42px; height: auto;" data-trigger="fileinput">
                                  <img src="<?= ($category[0]['icon_url']!='NULL')?base_url($category[0]['icon_url']):$this->config->item('resource_url').'noimage.png';?>" alt="Category image">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 42px; max-height: auto"></div>
                                <div>
                                  <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="icon_url_<?=$category[0]['cat_id']?>" id="icon_url_<?=$category[0]['cat_id']?>" accept="image/*">
                                  </span>
                                  <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                              </div>
                            </div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<div class="col-md-12">
									<label for="cat_id" class="control-label">Category Name :</label><br />	<br />				
									<input type="text" class="form-control" id="cat_name_<?=$category[0]['cat_id']?>" placeholder="Enter category Name..." name="cat_name_<?=$category[0]['cat_id']?>" value="<?=$category[0]['cat_name']?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 col-md-offset-2 text-center">
							<div class="form-group">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 42px; height: auto;" data-trigger="fileinput">
                                  <img src="<?= ($category[1]['icon_url']!='NULL')?base_url($category[1]['icon_url']):$this->config->item('resource_url').'noimage.png';?>" alt="Category image">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 42px; max-height: auto"></div>
                                <div>
                                  <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="icon_url_<?=$category[1]['cat_id']?>" id="icon_url_<?=$category[1]['cat_id']?>" accept="image/*">
                                  </span>
                                  <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                              </div>
                            </div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<div class="col-md-12">
									<label for="cat_id" class="control-label">Category Name :</label><br />	<br />				
									<input type="text" class="form-control" id="cat_name_<?=$category[1]['cat_id']?>" placeholder="Enter category Name..." name="cat_name_<?=$category[1]['cat_id']?>" value="<?=$category[1]['cat_name']?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 col-md-offset-2 text-center">
							<div class="form-group">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 42px; height: auto;" data-trigger="fileinput">
                                  <img src="<?= ($category[2]['icon_url']!='NULL')?base_url($category[2]['icon_url']):$this->config->item('resource_url').'noimage.png';?>" alt="Category image">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 42px; max-height: auto"></div>
                                <div>
                                  <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="icon_url_<?=$category[2]['cat_id']?>" id="icon_url_<?=$category[2]['cat_id']?>" accept="image/*">
                                  </span>
                                  <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                              </div>
                            </div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<div class="col-md-12">
									<label for="cat_id" class="control-label">Category Name :</label><br />	<br />				
									<input type="text" class="form-control" id="cat_name_<?=$category[2]['cat_id']?>" placeholder="Enter category Name..." name="cat_name_<?=$category[2]['cat_id']?>" value="<?=$category[2]['cat_name']?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 col-md-offset-2 text-center">
							<div class="form-group">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 42px; height: auto;" data-trigger="fileinput">
                                  <img src="<?= ($category[3]['icon_url']!='NULL')?base_url($category[3]['icon_url']):$this->config->item('resource_url').'noimage.png';?>" alt="Category image">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 42px; max-height: auto"></div>
                                <div>
                                  <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="icon_url_<?=$category[3]['cat_id']?>" id="icon_url_<?=$category[3]['cat_id']?>" accept="image/*">
                                  </span>
                                  <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                              </div>
                            </div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<div class="col-md-12">
									<label for="cat_id" class="control-label">Category Name :</label><br />	<br />				
									<input type="text" class="form-control" id="cat_name_<?=$category[3]['cat_id']?>" placeholder="Enter category Name..." name="cat_name_<?=$category[3]['cat_id']?>" value="<?=$category[3]['cat_name']?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-md-offset-2">
							<div class="form-group text-right">
								<div class="col-md-8">
									<button type="submit" class="btn btn-blue">Update Category Details</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>	
	</div>
</div>

<script>
	$("#laundry_category_add")
    .validate({
      	ignore: [], 
      	rules: {
			cat_id: { required : true },
			sub_cat_name: { required : true },
			height: { required : true, number: true, min: 0 },
			width: { required : true, number: true, min: 0 },
			length: { required : true, number: true, min: 0 },
			weight: { required : true, number: true, min: 0 },
      	},
      	messages: {
	        cat_id: { required :'Select Category Name!'},
	        sub_cat_name: { required :'Enter Sub Category Name!'},
	        height: { required :'Enter Height in cm!', number: 'Enter numbers only', min: 'Not less than Zero(0)'},
	        width: { required :'Enter Width in cm!', number: 'Enter numbers only', min: 'Not less than Zero(0)'},
	        length: { required :'Enter Length in cm!', number: 'Enter numbers only', min: 'Not less than Zero(0)'},
	        weight: { required :'Enter Weight in grms!', number: 'Enter numbers only', min: 'Not less than Zero(0)'},
      	},
  	});
</script>