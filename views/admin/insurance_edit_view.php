    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li>
        <a href="<?= base_url('admin/insurance-master'); ?>">Insurance</a>
      </li>
      <li class="active">
        <strong>View &amp; Edit Insurance</strong>
      </li>
    </ol>
    <style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
    <div class="row">
      <div class="col-md-12">
        
        <div class="panel panel-dark" data-collapsed="0">
        
          <div class="panel-heading">
            <div class="panel-title">
              View &amp; Edit Insurance
            </div>
            
            <div class="panel-options">
              <a href="<?= base_url('admin/insurance-master'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
            </div>
          </div>
          
          <div class="panel-body">
            
            <?php if($this->session->flashdata('error')):  ?>
                <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
              <?php endif; ?>
              <?php if($this->session->flashdata('success')):  ?>
                <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
              <?php endif; ?>

            <form role="form" class="form-horizontal form-groups-bordered" id="form_editInsurance" action="<?= base_url('admin/update-insurance'); ?>" method="post">
              <input type="hidden" name="ins_id" value="<?= $insurance_details['ins_id']; ?>">
              
              <div class="border">
                <div class="row">
                  <div class="col-md-4">
                    <label for="country_id" class="control-label">Selected Country </label> 
                    <input type="text" class="form-control" name="category" value="<?= $insurance_details['country_name'];?>" readonly/>                    
                  </div>
                  <div class="col-md-8">
                    <label for="category" class="control-label">Selected Category </label> 
                    <input type="text" class="form-control" name="category" value="<?= $insurance_details['cat_name'];?>" readonly/>
                  </div>             
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="border">
                <div class="row">

                  <div class="col-md-3">
                    <label for="min_value" class="control-label">Minimum Value</label>
                    <div class="input-group">
                      <span class="input-group-addon"><?= $insurance_details['currency_sign'] ?></span>
                      <input type="number" class="form-control" id="min_value" placeholder="Enter minimum value" name="min_value" min="1" value="<?=$insurance_details['min_value']?>" disabled/>
                    </div>                        
                    <div class="clear"></div><br />
                  </div>

                  <div class="col-md-3">
                    <label for="max_value" class="control-label">Maximum Value</label>
                    <div class="input-group">
                      <span class="input-group-addon"><?= $insurance_details['currency_sign'] ?></span>
                      <input type="number" class="form-control" id="max_value" placeholder="Enter maximum value" name="max_value" min="1" value="<?=$insurance_details['max_value']?>" disabled/>  
                    </div>                        
                    <div class="clear"></div><br />
                  </div>

                  <div class="col-md-3">
                    <label for="ins_fee" class="control-label">Insurance Fee</label>
                    <div class="input-group">
                      <span class="input-group-addon"><?= $insurance_details['currency_sign'] ?></span>
                      <input type="number" class="form-control" id="ins_fee" placeholder="Enter insurance fee" name="ins_fee" min="0" value="<?=$insurance_details['ins_fee']?>" disabled/>
                    </div>                        
                    <div class="clear"></div><br />
                  </div>

                  <div class="col-md-3">
                    <label for="commission" class="control-label">Commission Percentage</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="commission" placeholder="Enter Commission Percentage" name="commission" min="0" max="100" value="<?=$insurance_details['commission_percent']?>" disabled/>
                      <span class="input-group-addon">%</span>
                    </div>                        
                    <div class="clear"></div><br />
                  </div>
                  
                </div>
              </div>        
              
              <div class="row">
                <div class="col-md-12">
                  <div class="text-center">
                    <a href="<?= base_url('admin/insurance-master'); ?>" class="btn btn-info">&nbsp;&nbsp; Back &nbsp;&nbsp;</a> &nbsp;&nbsp;
                    <button id="btn_edit" class="btn btn-green">&nbsp;&nbsp; Edit &nbsp;&nbsp;</button>
                    <button id="btn_submit" type="submit" class="btn btn-blue hidden">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
                  </div>
                </div>
              </div>

            </form>
            
          </div>
        
        </div>
      
      </div>
    </div>
    
    <br />


    <script>
      $(function(){

        $("#btn_edit").on('click', function(){
          $(this).addClass('hidden');
          $("#btn_submit").removeClass('hidden');
          $("input,select").prop('disabled', false);
          $('html, body').animate({scrollTop : 0},800);
          return false;
        });

        var somethingChanged = false;
        $('#form_editInsurance input').change(function() { 
            somethingChanged = true; 
        });

        $("#form_editInsurance").submit(function(e) { e.preventDefault(); });

        $("#btn_submit").on('click', function(e) {  e.preventDefault();

          if(!somethingChanged){ swal('Error','No change found!','error'); }
          else {
            var min_value = parseFloat($("#min_value").val()).toFixed(2);
            var max_value = parseFloat($("#max_value").val()).toFixed(2);  
            //console.log(min_value);  
            //console.log(max_value);  
            var ins_fee = parseFloat($("#ins_fee").val()).toFixed(2);
            var commission = parseFloat($("#commission").val()).toFixed(2);   

            if(min_value < 1) {  swal('Error','Enter valid minimum value ( greater than zero )!','warning');   } 
            else if(isNaN(min_value)) {  swal('Error','Enter valid minimum value! Enter Number Only.','warning');   } 

            else if(max_value < 1) {  swal('Error','Enter valid maximum value ( greater than zero )!','warning');   } 
            else if(isNaN(max_value)) {  swal('Error','Enter valid maximum value! Enter Number Only.','warning');   } 
            
            else if(parseFloat(min_value) >= parseFloat(max_value)) {  swal('Error','Enter maximum value greater than minimum value!','warning');   } 

            else if(ins_fee < 0) {  swal('Error','Enter valid Insurance Fee!','warning');   } 
            else if(isNaN(ins_fee)) {  swal('Error','Enter valid Insurance Fee! Enter Number Only.','warning');   } 

            else if(isNaN(commission)) {  swal('Error','Enter valid Commission Percentage! Enter Number Only.','warning');   } 
            else if(commission < 0 && commission > 100) {  swal('Error','Invalid Commission Percentage! Should be greater than or equal to zero and less than 100.','warning');   }     
            else {  $("#form_editInsurance")[0].submit();  }
          }
        });

      });
    </script>