<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="">
		<a href="<?= base_url('admin/view-users/') . $cust_id; ?>">User Details</a>
	</li>
	<li class="active">
		<strong>Wallet Transactions List</strong>
	</li>
</ol>

<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>
<div class="rows">
	<div class="col-md-6">
		<h3 style="margin-top: 0px;">Wallet Transactions List</h3>
	</div>
	<div class="col-md-6">
		<a style="float: right;" href="<?= base_url('admin/view-users/') . $cust_id; ?>" class="btn btn-info">User Details</a>
	</div>
</div>		

	<table id="tableData" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>Tr. Id</th>
                <th>Date Time</th>
                <th>Order ID</th>
                <th>Transaction</th>
                <th>Amount</th>
                <th>+/-</th>
                <th>Balance</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($history as $histry) { ?>
            <tr style="background-color: <?= $histry['type'] == '1' ? '#DFFFDF': '#FFDDDD'; ?>">
                <td><?= $histry['ah_id'] ?></td>
                <td><?= date('d-M-Y h:i:s A',strtotime($histry['datetime'])) ?></td>
                <td><?= $histry['order_id'] ?></td>
                <td><?= ucwords(str_replace('_',' ',$histry['transaction_type'])) ?></td>
                <td><?= $histry['currency_code'] . ' ' . $histry['amount'] ?></td>
                <td><?= $histry['type'] == '1' ? '<i class="fa fa-plus" style="color: green"></i>' : '<i class="fa fa-minus" style="color: red"></i>'; ?></td>
                <td><?= $histry['currency_code'] . ' ' .  $histry['account_balance'] ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#tableData' );
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});
		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );
	} );
</script>