
<div class="row">
  <div class="col-md-12 form-group">
		<div class="panel-group" id="accordion">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" id="user_and_statistics" data-parent="#accordion" href="#collapse1"><i class="fa fa-users"></i> Users and Job Statistics</a>
					</h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse in"><!--  --> 
					<div class="panel-body">
						<ul class="nav nav-tabs" style="margin-top: 0px !important;">
							<li class="active"><a data-toggle="tab" href="#orderStats">Jobs</a></li>
							<li><a data-toggle="tab" href="#userStats">Users</a></li>
						</ul>

						<div class="tab-content">
							<div id="orderStats" class="tab-pane fade in active">
								<div class="row" style="margin-top: 15px">
									<div class="col-sm-4 col-xs-6">
									  <label for="category" class="control-label">Select Period <small>(YYYY-MM-DD)</small></label> 
									  <div class="daterange daterange-inline add-ranges" data-format="YYYY-MM-DD" data-start-date="<?=date('Y-m-d')?>" data-end-date="<?=date('Y-m-d')?>">
									    <i class="entypo-calendar"></i>
									    <span id="from-to-date-orderStats">TILL DATE</span>
									  </div>
									</div>
									<div class="col-sm-4 col-xs-6">
									  <label for="country_id" class="control-label">Select Country </label> 
									  <select id="select2-orderStats" name="country_id" class="form-control select2">
									    <option value="0">ALL</option>
									    <?php foreach ($countries as $country): ?> 
									      <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
									    <?php endforeach ?>
									  </select>
									</div>
									<div class="col-sm-4 col-xs-6">
									  <label for="category" class="control-label">Select Category </label> 
									  <select id="cat-id-orderStats" name="cat_id" class="form-control select2" placeholder="Select Category ">
									    <option value="0">ALL</option>
									    <?php foreach ($categories as $c ): ?>
									      <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 || $c['cat_id'] == 281 || $c['cat_id'] == 9 ): ?>
									        <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
									      <?php endif; ?>
									    <?php endforeach; ?>
									  </select>
									</div>
								</div>
								<hr />
							  	<div class="row">
									<div class="col-sm-12" style="margin-left: -15px">
										<div class="row" style="margin-right: -45px;">
											<div class="col-sm-12">
												<div class="col-sm-3">
													<form id="frmTotalJobs" action="<?= base_url('admin/total-jobs'); ?>" method="post">
														<input type="hidden" name="totalJobsStartDate" id="totalJobsStartDate" value="0">
														<input type="hidden" name="totalJobsToDate" id="totalJobsToDate" value="0">
														<input type="hidden" name="totalJobsCountryId" id="totalJobsCountryId" value="0">
														<input type="hidden" name="totalJobsCatId" id="totalJobsCatId" value="0">
														<a href="#" style="color: #FFF !important;" id="aTotalJobs">
															<div class="tile-stats tile-green">
																<div class="icon" style="bottom: 15px !important"><i class="fa fa-shopping-basket"></i></div>
														      	<h3 class="text-center" id="totalJobs_title">Total Jobs</h3>
														      	<h2 class="text-center"><span id="totalJobs"><?=$totalJobs[0]['jobCount']?></span></h2>
														    </div>
														</a>
													</form>
												</div>
												<div class="col-sm-3">
													<form id="frmtotalOpenJobs" action="<?= base_url('admin/total-open-jobs'); ?>" method="post">
														<input type="hidden" name="totalOpenJobsStartDate" id="totalOpenJobsStartDate" value="0">
														<input type="hidden" name="totalOpenJobsToDate" id="totalOpenJobsToDate" value="0">
														<input type="hidden" name="totalOpenJobsCountryId" id="totalOpenJobsCountryId" value="0">
														<input type="hidden" name="totalOpenJobsCatId" id="totalOpenJobsCatId" value="0">
														<a href="#" style="color: #FFF !important;" id="atotalOpenJobs">
														    <div class="tile-stats tile-red">
														      	<div class="icon" style="bottom: 15px !important"><i class="fa fa-archive"></i></div>
														      	<h3 class="text-center" id="totalOpenJobs_title">Open</h3>
														      	<h2 class="text-center"><span id="totalOpenJobs"><?=$totalOpenJobs[0]['jobCount']?></span></h2>
														    </div>
														</a>
													</form>
												</div>
												<div class="col-sm-3">
													<form id="frmtotalAcceptedJobs" action="<?= base_url('admin/total-accepted-jobs'); ?>" method="post">
														<input type="hidden" name="totalAcceptedJobsStartDate" id="totalAcceptedJobsStartDate" value="0">
														<input type="hidden" name="totalAcceptedJobsToDate" id="totalAcceptedJobsToDate" value="0">
														<input type="hidden" name="totalAcceptedJobsCountryId" id="totalAcceptedJobsCountryId" value="0">
														<input type="hidden" name="totalAcceptedJobsCatId" id="totalAcceptedJobsCatId" value="0">
														<a href="#" style="color: #FFF !important;" id="atotalAcceptedJobs">
															<div class="tile-stats tile-orange">
																<div class="icon" style="bottom: 15px !important"><i class="fa fa-check-circle"></i></div>
																<h3 class="text-center" id="totalAcceptedJobs_title">Accepted</h3>
														      	<h2 class="text-center"><span id="totalAcceptedJobs"><?=$totalAcceptedJobs[0]['jobCount']?></span></h2>
														    </div>
														</a>
													</form>
												</div>
												<div class="col-sm-3">
													<form id="frmtotalInprogressJobs" action="<?= base_url('admin/total-inprogress-jobs'); ?>" method="post">
														<input type="hidden" name="totalInprogressJobsStartDate" id="totalInprogressJobsStartDate" value="0">
														<input type="hidden" name="totalInprogressJobsToDate" id="totalInprogressJobsToDate" value="0">
														<input type="hidden" name="totalInprogressJobsCountryId" id="totalInprogressJobsCountryId" value="0">
														<input type="hidden" name="totalInprogressJobsCatId" id="totalInprogressJobsCatId" value="0">
														<a href="#" style="color: #FFF !important;" id="atotalInprogressJobs">
														    <div class="tile-stats tile-pink">
														      	<div class="icon" style="bottom: 15px !important"><i class="fa fa-cogs"></i></div>
														      	<h3 class="text-center" id="totalInprogressJobs_title">In-progress</h3>
														      	<h2 class="text-center"><span id="totalInprogressJobs"><?=$totalInprogressJobs[0]['jobCount']?></span></h2>
														    </div>
														</a>
													</form>
												</div>
											</div>
										</div>
										<div class="row" style="margin-right: -45px;">
											<div class="col-sm-12">
												<div class="col-sm-3">
													<form id="frmtotalDeliveredJobs" action="<?= base_url('admin/total-delivered-jobs'); ?>" method="post">
														<input type="hidden" name="totalDeliveredJobsStartDate" id="totalDeliveredJobsStartDate" value="0">
														<input type="hidden" name="totalDeliveredJobsToDate" id="totalDeliveredJobsToDate" value="0">
														<input type="hidden" name="totalDeliveredJobsCountryId" id="totalDeliveredJobsCountryId" value="0">
														<input type="hidden" name="totalDeliveredJobsCatId" id="totalDeliveredJobsCatId" value="0">
														<a href="#" style="color: #FFF !important;" id="atotalDeliveredJobs">
														    <div class="tile-stats tile-red">
														      	<div class="icon" style="bottom: 15px !important"><i class="fa fa-truck"></i></div>
														      	<h3 class="text-center" id="totalDeliveredJobs_title">Delivered</h3>
														      	<h2 class="text-center"><span id="totalDeliveredJobs"><?=$totalDeliveredJobs[0]['jobCount']?></span></h2>
														    </div>
														</a>
													</form>
												</div>
												<div class="col-sm-3">
													<form id="frmtotalCancelledJobs" action="<?= base_url('admin/total-cancelled-jobs'); ?>" method="post">
														<input type="hidden" name="totalCancelledJobsStartDate" id="totalCancelledJobsStartDate" value="0">
														<input type="hidden" name="totalCancelledJobsToDate" id="totalCancelledJobsToDate" value="0">
														<input type="hidden" name="totalCancelledJobsCountryId" id="totalCancelledJobsCountryId" value="0">
														<input type="hidden" name="totalCancelledJobsCatId" id="totalCancelledJobsCatId" value="0">
														<a href="#" style="color: #FFF !important;" id="atotalCancelledJobs">
															<div class="tile-stats tile-orange" id="cancelled_div">
														      	<div class="icon" style="bottom: 15px !important"><i class="fa fa-minus-square"></i></div>
														      	<h3 class="text-center" id="totalCancelledJobs_title">Cancelled</h3>
														      	<h2 class="text-center"><span id="totalCancelledJobs"><?=$totalCancelledJobs[0]['jobCount']?></span></h2>
													    	</div>
													    </a>
													</form>
												</div>
												<div class="col-sm-3">
													<form id="frmtotalExpiredJobs" action="<?= base_url('admin/total-expired-jobs'); ?>" method="post">
														<input type="hidden" name="totalExpiredJobsStartDate" id="totalExpiredJobsStartDate" value="0">
														<input type="hidden" name="totalExpiredJobsToDate" id="totalExpiredJobsToDate" value="0">
														<input type="hidden" name="totalExpiredJobsCountryId" id="totalExpiredJobsCountryId" value="0">
														<input type="hidden" name="totalExpiredJobsCatId" id="totalExpiredJobsCatId" value="0">
														<a href="#" style="color: #FFF !important;" id="atotalExpiredJobs">
															<div class="tile-stats tile-pink" id="expired_div">
														      	<div class="icon" style="bottom: 15px !important"><i class="fa fa-exclamation-triangle"></i></div>
														      	<h3 class="text-center">Expired</h3>
														      	<h2 class="text-center"><span id="totalExpiredJobs"><?=$totalExpiredJobs[0]['jobCount']?></span></h2>
														    </div>
														</a>
													</form>
												</div>
												<div class="col-sm-3">
													<form id="frmrejectedJobsCount" action="<?= base_url('admin/total-rejected-jobs'); ?>" method="post">
														<input type="hidden" name="rejectedJobsCountStartDate" id="rejectedJobsCountStartDate" value="0">
														<input type="hidden" name="rejectedJobsCountToDate" id="rejectedJobsCountToDate" value="0">
														<input type="hidden" name="rejectedJobsCountCountryId" id="rejectedJobsCountCountryId" value="0">
														<input type="hidden" name="rejectedJobsCountCatId" id="rejectedJobsCountCatId" value="0">
														<a href="#" style="color: #FFF !important;" id="arejectedJobsCount">
															<div class="tile-stats tile-green" id="reject_div">
														      	<div class="icon" style="bottom: 15px !important"><i class="fa fa-times"></i></div>
														      	<h3 class="text-center">Rejected</h3>
														      	<h2 class="text-center"><span id="rejectedJobsCount">0</span></h2>
														    </div>
														</a>
													</form>
												</div>
											</div>
										</div>
									</div>										
								</div>
								<div id="job_graph" ></div><!--  graph for job  --> 						
							</div>

							<div id="userStats" class="tab-pane fade">
								<div class="row">
									<div class="col-sm-2 col-xs-6 form-group">
										<label for="country_id" class="control-label">Filter By Country</label> 
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<select id="select2-usersCount" name="country_id" class="form-control select2">
										<option value="0">All</option>
											<?php foreach ($countries as $country): ?> 
										  	<option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
											<?php endforeach ?>
										</select>
								    </div>
									<div class="col-sm-6 col-xs-12 form-group">
										&nbsp;
									</div>
								</div>
							  	<div class="row">
									<div class="col-sm-3">
										<form id="frmtotalUsers" action="<?= base_url('admin/total-users'); ?>" method="post">
											<input type="hidden" name="totalUsersCountryId" id="totalUsersCountryId" value="0">
											<a href="#" style="color: #FFF !important;" id="atotalUsers">
												<div class="tile-stats tile-voilet" style="min-height: 140px;">
											      	<h3 class="text-center">Total Users</h3>
											      	<h2 class="text-center"><span id="totalUsers"><?=$totalUsers[0]['userCount']?></span></h2>
										    	</div>
										    </a>
										</form>
									</div>
									<div class="col-sm-6" style="margin-left: -15px">
										<div class="row" style="margin-right: -45px;">
											<div class="col-sm-12">
												<div class="col-sm-6">
													<form id="frmbusinessUsers" action="<?= base_url('admin/business-users'); ?>" method="post">
														<input type="hidden" name="businessUsersCountryId" id="businessUsersCountryId" value="0">
														<a href="#" style="color: #FFF !important;" id="abusinessUsers">
															<div class="tile-stats tile-voilet">
																<h3 style="color: #FFF">Business - <span id="businessUsers"><?=$businessUsers[0]['userCount']?></span></h3>
														    </div>
														</a>
													</form>
												</div>
												<div class="col-sm-6">
													<form id="frmbuyerUsers" action="<?= base_url('admin/buyer-users'); ?>" method="post">
														<input type="hidden" name="buyerUsersCountryId" id="buyerUsersCountryId" value="0">
														<a href="#" style="color: #FFF !important;" id="abuyerUsers">
															<div class="tile-stats tile-green">
														      	<h3 style="color: #FFF">Buyers - <span id="buyerUsers"><?=$buyerUsers[0]['userCount']?></span></h3>
														    </div>
														</a>
													</form>
												</div>
											</div>
										</div>
										<div class="row" style="margin-right: -45px;">
											<div class="col-sm-12">
												<div class="col-sm-6">
													<form id="frmindividualUsers" action="<?= base_url('admin/individual-users'); ?>" method="post">
														<input type="hidden" name="individualUsersCountryId" id="individualUsersCountryId" value="0">
														<a href="#" style="color: #FFF !important;" id="aindividualUsers">
														    <div class="tile-stats tile-voilet">
														      	<h3 style="color: #FFF">Individual - <span id="individualUsers"><?=$individualUsers[0]['userCount']?></span></h3>
														    </div>
														</a>
													</form>
												</div>
												<div class="col-sm-6">
													<form id="frmsellerUsers" action="<?= base_url('admin/seller-users'); ?>" method="post">
														<input type="hidden" name="sellerUsersCountryId" id="sellerUsersCountryId" value="0">
														<a href="#" style="color: #FFF !important;" id="asellerUsers">
															<div class="tile-stats tile-green">
														      	<h3 style="color: #FFF">Sellers - <span id="sellerUsers"><?=$sellerUsers[0]['userCount']?></span></h3>
														    </div>
														</a>
													</form>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-3" style="margin-left: 15px;">
										<form id="frmbothUsers" action="<?= base_url('admin/both-users'); ?>" method="post">
											<input type="hidden" name="bothUsersCountryId" id="bothUsersCountryId" value="0">
											<a href="#" style="color: #FFF !important;" id="abothUsers">
												<div class="tile-stats tile-green" style="min-height: 140px;">
											      	<h3 class="text-center">Both</h3>
											      	<h2 class="text-center"><span id="bothUsers"><?=$bothUsers[0]['userCount']?></span></h2>
										    	</div>
										    </a>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<form id="frmonlineUsers" action="<?= base_url('admin/online-users'); ?>" method="post">
											<input type="hidden" name="onlineUsersCountryId" id="onlineUsersCountryId" value="0">
											<a href="#" style="color: #FFF !important;" id="aonlineUsers">
											    <div class="tile-stats tile-green"">
											      	<h3 class="text-center">Online</h3>
											      	<h2 class="text-center"><span id="onlineUsers"><?=$onlineUsers[0]['userCount']?></span></h2>
											    </div>
											</a>
										</form>
									</div>
									<div class="col-sm-3">
										<form id="frmofflineUsers" action="<?= base_url('admin/offline-users'); ?>" method="post">
											<input type="hidden" name="offlineUsersCountryId" id="offlineUsersCountryId" value="0">
											<a href="#" style="color: #FFF !important;" id="aofflineUsers">
											    <div class="tile-stats tile-red"">
											      	<h3 class="text-center">Offline</h3>
											      	<h2 class="text-center"><span id="offlineUsers"><?=$offlineUsers[0]['userCount']?></span></h2>
											    </div>
											</a>
										</form>
									</div>
									<div class="col-sm-3">
										<form id="frmactiveUsers" action="<?= base_url('admin/active-users'); ?>" method="post">
											<input type="hidden" name="activeUsersCountryId" id="activeUsersCountryId" value="0">
											<a href="#" style="color: #FFF !important;" id="aactiveUsers">
											    <div class="tile-stats tile-blue"">
											      	<h3 class="text-center">Active</h3>
											      	<h2 class="text-center"><span id="activeUsers"><?=$activeUsers[0]['userCount']?></span></h2>
											    </div>
											</a>
										</form>
									</div>
									<div class="col-sm-3">
										<form id="frminactiveUsers" action="<?= base_url('admin/inactive-users'); ?>" method="post">
											<input type="hidden" name="inactiveUsersCountryId" id="inactiveUsersCountryId" value="0">
											<a href="#" style="color: #FFF !important;" id="ainactiveUsers">
											    <div class="tile-stats tile-orange"">
											      	<h3 class="text-center">Inactive</h3>
											      	<h2 class="text-center"><span id="inactiveUsers"><?=$inactiveUsers[0]['userCount']?></span></h2>
											    </div>
											</a>
										</form>
									</div>
								</div>
									<div id="user_graph" ></div><!-- user graph --> 
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-warning">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" id="gonagoo_accounts" data-parent="#accordion" href="#collapse2"><i class="fa fa-money"></i> Gonagoo Accounts</a>
					</h4>
				</div>
				<div id="collapse2" class="panel-collapse collapse in">
					<div class="panel-body">

						<ul class="nav nav-tabs" style="margin-top: 0px !important;">
							<li class="active"><a data-toggle="tab" href="#tillDate">Till Date</a></li>
							<li><a data-toggle="tab" href="#otherAccount">Other Accounts</a></li>
							<li ><a data-toggle="tab" href="#recentTransactions">Transactions Report</a></li>
						</ul>

						<div class="tab-content">
							<div id="tillDate" class="tab-pane fade in active"><!-- in active -->
							  	<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="tile-stats tile-blue">
										  	<div class="icon"><i class="fa fa-money"></i></div>
										  	<h3 style="margin-bottom: 15px;">Earned to date</h3>
										  	<table class="table">
										  		<?php $earned_to_date_data_array = array(); ?>
										  		<?php foreach ($gonagoo_master as $mvalue) { ?>
										  		<!--  Graph is here  -->
											  		<tr>
											  			<td>
															<a href="<?= base_url('admin/currencywise-account-history/'.$mvalue['currency_code']); ?>">
														    	<div class="col-sm-3 col-xs-6">
														    		<div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$mvalue['currency_code']?></div>
														    	</div>
														    	<div class="col-sm-9 col-xs-12 text-right">
														    		<div class="num" style="font-size: 24px;"><?=$this->admin->convert_big_int($mvalue['gonagoo_balance'])?></div>
														    	</div>
														  	</a>
											  			</td>
											  		</tr>
											  		<?php $earned_to_date_data_array[] = ['x' => $mvalue['currency_code'], 'y' =>  $mvalue['gonagoo_balance'] ]; ?>
										  		<?php } ?>
										  	</table>
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="tile-stats tile-orange">
									  		<div class="icon"><i class="fa fa-money"></i></div>
									  		<h3 style="margin-bottom: 15px;">Paid to date</h3>
										  	<table class="table">
										  		<?php $paid_to_date_data_array = array(); ?>
									  			<?php foreach ($gonagoo_refund as $rvalue) { ?>
									  				<tr>
											  			<td>
									  						<a href="<?= base_url('admin/currencywise-refund-history/'.$rvalue['currency_code']); ?>">
									    						<div class="col-sm-3 col-xs-6">
									    							<div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$rvalue['currency_code']?></div>
									    						</div>
									    						<div class="col-sm-9 col-xs-12 text-right">
									    							<div class="num" style="font-size: 24px;"><?=$this->admin->convert_big_int($rvalue['amount_transferred'])?></div>
									    						</div>
									  						</a>
									  					</td>
									  				</tr>
									  				<?php $paid_to_date_data_array[] = ['x' => $rvalue['currency_code'], 'y' =>  $rvalue['amount_transferred'] ]; ?>
									  			<?php } ?>
									  		</table>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div id="earned_to_date_graph"></div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div id="paid_to_date_graph"></div>
									</div>
								</div>
							</div>
							
							<div id="otherAccount" class="tab-pane fade ">
								<div class="row" style="margin-top: 15px">
									<div class="col-sm-4 col-xs-6">
									  <label for="category" class="control-label">Select Period <small>(YYYY-MM-DD)</small></label> 
									  <div class="daterange daterange-inline add-ranges" data-format="YYYY-MM-DD" data-start-date="<?=date('Y-m-d')?>" data-end-date="<?=date('Y-m-d')?>">
									    <i class="entypo-calendar"></i>
									    <span id="from-to-date-otherAccount">TILL DATE</span>
									  </div>
									</div>
									<div class="col-sm-4 col-xs-6">
									  <label for="country_id" class="control-label">Select Country </label> 
									  <select id="select2-otherAccount" name="country_id" class="form-control select2">
									    <option value="0">ALL</option>
									    <?php foreach ($countries as $country): ?> 
									      <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
									    <?php endforeach ?>
									  </select>
									</div>
									<div class="col-sm-4 col-xs-6">
									  <label for="category" class="control-label">Select Category </label> 
									  <select id="cat-id-otherAccount" name="cat_id" class="form-control select2" placeholder="Select Category ">
									    <option value="0">ALL</option>
									    <?php foreach ($categories as $c ): ?>
									      <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 || $c['cat_id'] == 281 || $c['cat_id'] == 9 ): ?>
									        <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
									      <?php endif; ?>
									    <?php endforeach; ?>
									  </select>
									</div>
								</div>
								<hr />

								<div class="row">
									<div class="col-sm-4 col-xs-6">
										<div class="tile-stats tile-aqua" style="min-height: 147px;">
										  	<div class="icon"><i class="fa fa-money"></i></div>
										  	<h3 style="margin-bottom: 15px;">Commission</h3>
										  	<span id="gonagoo_commission">
										  		<?php $commission_data_array = array(); ?>
											  	<?php foreach ($gonagoo_commission as $cvalue) { ?>
											  		<a href="<?= base_url('admin/currencywise-gonagoo-comission-history/'.$cvalue['currency_code'].'/0'); ?>">
													    <div class="col-sm-3 col-xs-6">
													    	<div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$cvalue['currency_code']?></div>
													    </div>
											    		<div class="col-sm-9 col-xs-12 text-right">
												    		<div class="num" style="font-size: 24px;"><?=$cvalue['amount']?></div>
												    	</div>
											  		</a>
											  		<?php $commission_data_array []= ['x' => $cvalue['currency_code'], 'y' => $cvalue['amount']]; ?>
											  		<?php //echo 'currency_code is =>'.$cvalue['currency_code'].' And amount is '.$cvalue['amount']; ?>
											  	<?php } ?>
										  	</span>
										</div>
									</div>

									<div class="col-sm-4 col-xs-6">
										<div class="tile-stats tile-green" style="min-height: 147px;">
										  	<div class="icon"><i class="fa fa-money"></i></div>
										  	<h3 style="margin-bottom: 15px;">Insurance</h3>
										  	<span id="gonagoo_insurance">
										  		<?php $insurance_data_array = array(); ?>
											  	<?php foreach ($gonagoo_insurance as $ivalue) { ?>
											  	<a href="<?= base_url('admin/currencywise-insurance-history/'.$ivalue['currency_code']); ?>">
												    <div class="col-sm-3 col-xs-6">
												    	<div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$ivalue['currency_code']?></div>
												    </div>
												    <div class="col-sm-9 col-xs-12 text-right">
												    	<div class="num" style="font-size: 24px;"><?=$ivalue['amount']?></div>
												    </div>
											  	</a>
											  	<?php $insurance_data_array []= ['x' => $ivalue['currency_code'], 'y' => $ivalue['amount']]; ?>
											  	<?php } ?>
											</span>
										</div>
									</div>

									<div class="col-sm-4 col-xs-6">
										<div class="tile-stats tile-red" style="min-height: 147px;">
										  	<div class="icon"><i class="fa fa-money"></i></div>
										  	<h3 style="margin-bottom: 15px;">Custom</h3>
										  	<span id="gonagoo_custom">
										  		<?php $custom_data_array = array(); ?>
											  	<?php foreach ($gonagoo_custom as $cuvalue) { ?>
											  	<a href="<?= base_url('admin/currencywise-custom-history/'.$cuvalue['currency_code']); ?>">
											    	<div class="col-sm-3 col-xs-6">
											    		<div style="font-size: 24px; color: #FFF; font-weight: bold;"><?=$cuvalue['currency_code']?></div>
											    	</div>
											    	<div class="col-sm-9 col-xs-12 text-right">
											    		<div class="num" style="font-size: 24px;"><?=$cuvalue['amount']?></div>
											    	</div>
											  	</a>
											  	<?php $custom_data_array []= ['x' => $cuvalue['currency_code'], 'y' => $cuvalue['amount']]; ?>
											  	<?php } ?>
											</span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4 col-xs-6">
										<div id="commission_graph"></div>
									</div>
									<div class="col-sm-4 col-xs-6">
										<div id="insurance_graph"></div>
									</div>
									<div class="col-sm-4 col-xs-6">
										<div id="custom_graph"></div>
									</div>
								</div>
							</div>

							<div id="recentTransactions" class="tab-pane fade ">
								<div class="rows">
									<div class="col-md-12" style="margin-top: -15px">
										<a href="<?= base_url('admin/gonagoo-account-history'); ?>" style="float: right" class="btn btn-outline" ><i class="fa fa-money"></i> View All Transaction </a>
									</div>
								</div>
							
								<div class="row">
									<div class="col-sm-1">
										Report Type
									</div>
									<div class="col-sm-3">
										<select id="transaction_duration_type"  class="select2 transaction_graph_update">
											<option value="daily">Daily</option>
											<option value="weekly">Weekly</option>
											<option value="monthly">Monthly</option>
										</select>
									</div>
									<div class="col-sm-1">
										Filter By
									</div>
									<div class="col-sm-3 currency_filter">
										<select class="select2 transaction_graph_update" id="transaction_currency">
											<?php 
												if(count($currency_master)>0){
													foreach ($currency_master as $currency){
														$selected = '';
														if($currency['currency_sign'] == 'XAF'){
															$selected = 'selected="selected"';
														}
														?>
														<option value="<?= $currency['currency_sign'];
														 ?>" <?= $selected ?> >[ <?= $currency['currency_sign']; ?> ] <?= $currency['currency_title']; ?> </option>
														<?php
													}
												} 
											?>
										</select>
									</div>
									<div class="col-sm-3">
										<div id="transaction_daily_filter" class="transaction_filters">
											<div class="col-sm-12">
												<div class="input-group">
						                            <input type="text" class="form-control transaction_graph_update" id="daily_date_picker" name="pickupdate" value="<?php echo date('m/d/Y'); ?>" />
						                        </div>
											</div>
										</div>
										<div id="transaction_weekly_filter" class=" transaction_filters">
											<div class="col-sm-3">
												<div class="container">    
												    <div class="row">
												        <div class="col-sm-12 form-group">
												            <div class="input-group" id="DateDemo">
												              <input class="form-control transaction_graph_update" type='text' id='weeklyDatePicker' placeholder="Select Week" />
												          </div>
												      </div>
												  </div>
												</div>
											</div>
										</div>
										<div id="transaction_monthly_filter" class=" transaction_filters">
											<div class="col-sm-12">
												<select class="select2 transaction_graph_update" id="year_dp">
													<?php
														$initial_year = 2015; 
														$current_year = date('Y');
														for ($i=$initial_year; $i <= $current_year ; $i++) { 
															?>
															<option value="<?= $i; ?>" <?= ($current_year == $i) ? 'selected' : '' ?> ><?= $i; ?></option>
															<?php
														}
													?>
												</select>
											</div>	
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div id="transaction_report_graph"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-success">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" id="lattest_jobs" data-parent="#accordion" href="#collapse3"><i class="fa fa-list"></i> Latest Jobs</a>
					</h4>
				</div>
				<br/>
				<div id="collapse3" class="panel-collapse collapse" style="margin-top: -15px;">
					<div class="row">
						<div class="col-md-12">
							<div class="col-sm-4 col-xs-6" style="padding-bottom: 10px;">
							  	<label class="control-label">Select Category </label> 
							  	<select id="latest_job_cat_id" name="latest_job_cat_id" class="form-control" placeholder="Select Category ">
							    	<option value="0">Select Category</option>
							    	<?php foreach ($categories as $c ): ?>
							      		<?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 || $c['cat_id'] == 281 || $c['cat_id'] == 9 ): ?>
							        		<option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
							      		<?php endif; ?>
							    	<?php endforeach; ?>
							  	</select>
							</div>
						</div>
					</div>
					<!-- courier  -->
					<div id="latest_job_courier">
						<div class="panel-body">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#recentlyOpenJobs">Recently Open Jobs</a></li>
								<li><a data-toggle="tab" href="#recentlyInprogressJobs">Recently In-Progress Jobs</a></li>
								<li><a data-toggle="tab" href="#recentlyDeliveredJobs">Recently Delivered Jobs</a></li>
								<li><a data-toggle="tab" href="#recentlyCancelledJobs">Recently Cancelled Jobs</a></li>
							</ul>
							<div class="tab-content">
								<div id="recentlyOpenJobs" class="tab-pane fade in active">
								  	<table class="table display compact" id="recentlyOpenJobsTable">
								      	<thead>
								        	<tr>
												<th class="text-center">Order ID</th>
												<th class="text-center">Order Date</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">From Address</th>
												<th class="text-center">To Address</th>
												<th class="text-center">Pickup Date</th>
												<th class="text-center">Delivery Date</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recentlyOpenJobs as $roj):  $email = $this->admin->get_customer_email_by_id($roj['cust_id']); $email_cut = explode('@', $email); ?>
								        	<tr>
									          	<td class="text-center"><?=$roj['order_id'] ?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($roj['cre_datetime']))?></td>
									          	<td class="text-center"><?=($roj['cust_name']=='NULL' || $roj['cust_name']=='NULL NULL')?$email_cut[0]:$roj['cust_name']?></td>
									          	<td class="text-center"><?=$roj['from_address']?></td>
									          	<td class="text-center"><?=$roj['to_address']?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($roj['pickup_datetime']))?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($roj['delivery_datetime']))?></td>
									        </tr>
								        	<?php endforeach; ?>
								        	<?php if(sizeof($recentlyOpenJobs) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-open-jobs'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyOpenJobs) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recentlyInprogressJobs" class="tab-pane fade">
								  	<table class="table display compact" id="recentlyInprogressJobsTable">
								      	<thead>
								        	<tr>
												<th class="text-center">Order ID</th>
												<th class="text-center">Order Date</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">From Address</th>
												<th class="text-center">To Address</th>
												<th class="text-center">Pickup Date</th>
												<th class="text-center">Delivery Date</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recentlyInprogressJobs as $rij):  $email = $this->admin->get_customer_email_by_id($rij['cust_id']); $email_cut = explode('@', $email); ?>
								        	<tr>
									          	<td class="text-center"><?=$rij['order_id'] ?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rij['cre_datetime']))?></td>
									          	<td class="text-center"><?=($rij['cust_name']=='NULL' || $rij['cust_name']=='NULL NULL')?$email_cut[0]:$rij['cust_name']?></td>
									          	<td class="text-center"><?=$rij['from_address']?></td>
									          	<td class="text-center"><?=$rij['to_address']?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rij['pickup_datetime']))?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rij['delivery_datetime']))?></td>
									        </tr>
								        	<?php endforeach; ?>
								        	<?php if(sizeof($recentlyInprogressJobs) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/inprogress-open-jobs'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyInprogressJobs) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recentlyDeliveredJobs" class="tab-pane fade">
								  	<table class="table display compact" id="recentlyDeliveredJobsTable">
								      	<thead>
								        	<tr>
												<th class="text-center">Order ID</th>
												<th class="text-center">Order Date</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">From Address</th>
												<th class="text-center">To Address</th>
												<th class="text-center">Pickup Date</th>
												<th class="text-center">Delivery Date</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recentlyDeliveredJobs as $rdj):  $email = $this->admin->get_customer_email_by_id($rdj['cust_id']); $email_cut = explode('@', $email); ?>
								        	<tr>
									          	<td class="text-center"><?=$rdj['order_id'] ?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rdj['cre_datetime']))?></td>
									          	<td class="text-center"><?=($rdj['cust_name']=='NULL' || $rdj['cust_name']=='NULL NULL')?$email_cut[0]:$rdj['cust_name']?></td>
									          	<td class="text-center"><?=$rdj['from_address']?></td>
									          	<td class="text-center"><?=$rdj['to_address']?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rdj['pickup_datetime']))?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rdj['delivery_datetime']))?></td>
									        </tr>
								        	<?php endforeach; ?>
								        	<?php if(sizeof($recentlyDeliveredJobs) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/delivered-open-jobs'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyDeliveredJobs) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recentlyCancelledJobs" class="tab-pane fade">
								  	<table class="table display compact" id="recentlyCancelledJobsTable">
								      	<thead>
								        	<tr>
												<th class="text-center">Order ID</th>
												<th class="text-center">Order Date</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">From Address</th>
												<th class="text-center">To Address</th>
												<th class="text-center">Pickup Date</th>
												<th class="text-center">Delivery Date</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recentlyCancelledJobs as $rcj):  $email = $this->admin->get_customer_email_by_id($rcj['cust_id']); $email_cut = explode('@', $email); ?>
								        	<tr>
									          	<td class="text-center"><?=$rcj['order_id'] ?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rcj['cre_datetime']))?></td>
									          	<td class="text-center"><?=($rcj['cust_name']=='NULL' || $rcj['cust_name']=='NULL NULL')?$email_cut[0]:$rcj['cust_name']?></td>
									          	<td class="text-center"><?=$rcj['from_address']?></td>
									          	<td class="text-center"><?=$rcj['to_address']?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rcj['pickup_datetime']))?></td>
									          	<td class="text-center"><?=date('d/m/Y', strtotime($rcj['delivery_datetime']))?></td>
									        </tr>
								        	<?php endforeach; ?>
								        	<?php if(sizeof($recentlyCancelledJobs) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/cancelled-open-jobs'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyCancelledJobs) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
							</div>
						</div>
					</div>
					<!-- courier  -->
				 	<!-- bus  -->
					<div id="latest_job_bus">
						<div class="panel-body">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#recent_current_trips">Recently Current Trips</a></li>
								<li><a data-toggle="tab" href="#recent_upcoming_trips">Recently Upcoming Trips</a></li>
								<li><a data-toggle="tab" href="#recent_previous_trips">Recently Previous Trips</a></li>
								<li><a data-toggle="tab" href="#recent_cancelled_trips">Recently Cancelled Trips</a></li>
							</ul>
							<div class="tab-content">
								<div id="recent_current_trips" class="tab-pane fade in active">
								  	<table class="table display compact" id="recent_current_trips_table">
								      	<thead>
								        	<tr>
												<th class="text-center">Trip ID</th>
												<th class="text-center">Journey Date</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">Source</th>
												<th class="text-center">Destination</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recentlyCurrentTrips as $current ){ ?>
								        		<tr>
													<td class="text-center"><?=$current['trip_id']?></td>
													<td class="text-center"><?=$current['journey_date']?></td>
													<td class="text-center"><?=$current['cust_name']?></td>
													<td class="text-center"><?=$current['source_point']?></td>
													<td class="text-center"><?=$current['destination_point']?></td>
								        		</tr>
								        	<?php } ?>
								        	<?php if(sizeof($recentlyCurrentTrips) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-current-trips'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyCurrentTrips) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recent_upcoming_trips" class="tab-pane fade">
								  	<table class="table display compact" id="recent_upcoming_trips_table">
								      <thead>
								        	<tr>
												<th class="text-center">Trip ID</th>
												<th class="text-center">Journey Date</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">Source</th>
												<th class="text-center">Destination</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recentlyUpcomingTrips as $upcoming ) { ?>
								        		<tr>
													<td class="text-center"><?=$upcoming['trip_id']?></td>
													<td class="text-center"><?=$upcoming['journey_date']?></td>
													<td class="text-center"><?=$upcoming['cust_name']?></td>
													<td class="text-center"><?=$upcoming['source_point']?></td>
													<td class="text-center"><?=$upcoming['destination_point']?></td>
								        		</tr>
								        	<?php } ?>
								        	<?php if(sizeof($recentlyUpcomingTrips) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-upcoming-trips'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyUpcomingTrips) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recent_previous_trips" class="tab-pane fade">
								  	<table class="table display compact" id="recent_previous_trips_table">
								      <thead>
								        	<tr>
												<th class="text-center">Trip ID</th>
												<th class="text-center">Journey Date</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">Source</th>
												<th class="text-center">Destination</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recentlyPreviousTrips as $previous ){ ?>
								        		<tr>
													<td class="text-center"><?=$previous['trip_id']?></td>
													<td class="text-center"><?=$previous['journey_date']?></td>
													<td class="text-center"><?=$previous['cust_name']?></td>
													<td class="text-center"><?=$previous['source_point']?></td>
													<td class="text-center"><?=$previous['destination_point']?></td>
								        		</tr>
								        	<?php } ?>
								        	<?php if(sizeof($recentlyPreviousTrips) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-previous-trips'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyPreviousTrips) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recent_cancelled_trips" class="tab-pane fade">
								  	<table class="table display compact" id="recent_cancelled_trips_table">
								      	<thead>
								        	<tr>
												<th class="text-center">Cancelled ID</th>
												<th class="text-center">Trip ID</th>
												<th class="text-center">Cancelled Date</th>
												<th class="text-center">Source</th>
												<th class="text-center">Destination</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recentlyCancelledTrips as $cancld ){ ?>
								        		<tr>
													<?php $cancelled = $this->admin->get_bus_trip($cancld['trip_id']); ?>
													<td class="text-center"><?=$cancld['cancell_id']?></td>
													<td class="text-center"><?=$cancld['trip_id']?></td>
													<td class="text-center"><?=$cancld['cancelled_date']?></td>
													<td class="text-center"><?=$cancelled['trip_source']?></td>
													<td class="text-center"><?=$cancelled['trip_destination']?></td>
								        		</tr>
								        	<?php } ?>
								        	<?php if(sizeof($recentlyCancelledTrips) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-cancelled-trips'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyCancelledTrips) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
							</div>
						</div>
					</div>
				 	<!-- bus  -->
				 	<!-- laundry  -->
					<div id="latest_job_laundry">
						<div class="panel-body">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#recent_accepted_laundry">Recently Accepted Laundry</a></li>
								<li><a data-toggle="tab" href="#recent_in_progress_laundry">Recently In-Progress Trips</a></li>
								<li><a data-toggle="tab" href="#recent_completed_laundry">Recently Completed laundry</a></li>
								<li><a data-toggle="tab" href="#recent_cancelled_laundry">Recently Cancelled laundry</a></li>
							</ul>
							<div class="tab-content">
								<div id="recent_accepted_laundry" class="tab-pane fade in active">
								  	<table class="table display compact" id="recent_accepted_laundry_table">
								      	<thead>
								        	<tr>
												<th class="text-center">Booking ID</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">Operator Company Name</th>
												<th class="text-center">Item Count</th>
												<th class="text-center">Date</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recently_accepted_laundry as $accepted ){ ?>
								        		<tr>
													<td class="text-center"><?=$accepted['booking_id']?></td>
													<td class="text-center"><?=$accepted['cust_name']?></td>
													<td class="text-center"><?=$accepted['operator_company_name']?></td>
													<td class="text-center"><?=$accepted['item_count']?></td>
													<td class="text-center"><?=$accepted['cre_datetime']?></td>
								        		</tr>
								        	<?php } ?>
								        	<?php if(sizeof($recently_accepted_laundry) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-accepted-laundry'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recently_accepted_laundry) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recent_in_progress_laundry" class="tab-pane fade">
								  	<table class="table display compact" id="recent_in_progress_laundry_table">
								      	<thead>
								        	<tr>
												<th class="text-center">Booking ID</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">Operator Company Name</th>
												<th class="text-center">Item Count</th>
												<th class="text-center">Date</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recently_in_progress_laundry as $inprogress ){ ?>
								        		<tr>
													<td class="text-center"><?=$inprogress['booking_id']?></td>
													<td class="text-center"><?=$inprogress['cust_name']?></td>
													<td class="text-center"><?=$inprogress['operator_company_name']?></td>
													<td class="text-center"><?=$inprogress['item_count']?></td>
													<td class="text-center"><?=$inprogress['cre_datetime']?></td>
								        		</tr>
								        	<?php } ?>
								        	<?php if(sizeof($recently_in_progress_laundry) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-in-progress-laundry'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recently_in_progress_laundry) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recent_completed_laundry" class="tab-pane fade">
								  	<table class="table display compact" id="recent_completed_laundry_table">
								      	<thead>
								        	<tr>
												<th class="text-center">Booking ID</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">Operator Company Name</th>
												<th class="text-center">Item Count</th>
												<th class="text-center">Date</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recently_completed_laundry as $completed ){ ?>
								        		<tr>
													<td class="text-center"><?=$completed['booking_id']?></td>
													<td class="text-center"><?=$completed['cust_name']?></td>
													<td class="text-center"><?=$completed['operator_company_name']?></td>
													<td class="text-center"><?=$completed['item_count']?></td>
													<td class="text-center"><?=$completed['cre_datetime']?></td>
								        		</tr>
								        	<?php } ?>
								        	<?php if(sizeof($recently_completed_laundry) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-completed-laundry'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recently_completed_laundry) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
								<div id="recent_cancelled_laundry" class="tab-pane fade">
								  	<table class="table display compact" id="recent_cancelled_laundry_table">
								      	<thead>
								        	<tr>
												<th class="text-center">Booking ID</th>
												<th class="text-center">Customer Name</th>
												<th class="text-center">Operator Company Name</th>
												<th class="text-center">Item Count</th>
												<th class="text-center">Date</th>
									        </tr>
								      	</thead>
								      	<tbody>
								        	<?php foreach ($recently_cancelled_laundry as $cancelled ){ ?>
								        		<tr>
													<td class="text-center"><?=$cancelled['booking_id']?></td>
													<td class="text-center"><?=$cancelled['cust_name']?></td>
													<td class="text-center"><?=$cancelled['operator_company_name']?></td>
													<td class="text-center"><?=$cancelled['item_count']?></td>
													<td class="text-center"><?=$cancelled['cre_datetime']?></td>
								        		</tr>
								        	<?php } ?>
								        	<?php if(sizeof($recently_cancelled_laundry) <= 0) { ?>
								        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
								        	<?php } ?>
								      	</tbody>
								    </table>
								    <form action="<?= $this->config->item('base_url') . 'admin/recently-cancelled-laundry'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recently_cancelled_laundry) <= 4) ? 'hidden':''; ?>">
								      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
							    	</form>
								</div>
							</div>
						</div>
					</div>
				 	<!-- laundry  -->
				</div>
			</div>
		</div> 
	</div>
</div>

<!-- latest jobs -->
<script>
	$('#latest_job_courier').hide();
	$('#latest_job_bus').hide();
	$('#latest_job_laundry').hide();

	$("#latest_job_cat_id").on('change', function(e) {
		var cat_id = $('#latest_job_cat_id').val();
		if(cat_id == 6 || cat_id == 7 || cat_id == 280){
			// $('#latest_job_bus').hide();
			$('#latest_job_courier').show();
			$('#latest_job_bus').hide();
			$('#latest_job_laundry').hide();
		}
		if(cat_id == 281){
			$('#latest_job_bus').show();
			$('#latest_job_courier').hide();
			$('#latest_job_laundry').hide();
		}
		if(cat_id == 9){
			$('#latest_job_laundry').show();
			$('#latest_job_courier').hide();
			$('#latest_job_bus').hide();
		}
	});
</script>
<!-- latest jobs -->
<!-- User Country filter -->
<script>
	$("#select2-usersCount").on('change', function(e) {
	    var country_id = $(this).select2().val();
	    $("#currency_cd_low").val(country_id);
	    $('#totalUsersCountryId').val(country_id);
	    $('#businessUsersCountryId').val(country_id);
	    $('#buyerUsersCountryId').val(country_id);
	    $('#individualUsersCountryId').val(country_id);
	    $('#sellerUsersCountryId').val(country_id);
	    $('#bothUsersCountryId').val(country_id);
	    $('#onlineUsersCountryId').val(country_id);
	    $('#offlineUsersCountryId').val(country_id);
	    $('#activeUsersCountryId').val(country_id);
	    $('#inactiveUsersCountryId').val(country_id);

	    //Total Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'all',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#totalUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#totalUsers').val(0); },
            error: function(msg){ $('#totalUsers').val(0); } 
        });
        //Total Business Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'business',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#businessUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#businessUsers').val(0); },
            error: function(msg){ $('#businessUsers').val(0); } 
        });
        //Total individual Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'individual',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#individualUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#individualUsers').val(0); },
            error: function(msg){ $('#individualUsers').val(0); } 
        });
        //Total Buyer Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'buyer',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#buyerUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#buyerUsers').val(0); },
            error: function(msg){ $('#buyerUsers').val(0); }
        });
        //Total Seller Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'seller',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#sellerUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#sellerUsers').val(0); },
            error: function(msg){ $('#sellerUsers').val(0); }
        });
        //Total Both Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'both',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#bothUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#bothUsers').val(0); },
            error: function(msg){ $('#bothUsers').val(0); }
        });
        //Total Online Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'online',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#onlineUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#onlineUsers').val(0); },
            error: function(msg){ $('#onlineUsers').val(0); }
        });
        //Total offline Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'offline',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#offlineUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#offlineUsers').val(0); },
            error: function(msg){ $('#offlineUsers').val(0); }
        });
        //Total Active Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'active',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#activeUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#activeUsers').val(0); },
            error: function(msg){ $('#activeUsers').val(0); }
        });
        //Total Inactive Users
	    $.ajax({
            type: "POST", 
            url: "get-users-count-by-country-id", 
            data: { user_type: 'inactive',country_id:country_id,record_type:'count',rows:0 },
            dataType: "json",
            success: function(res){ $('#inactiveUsers').text(res[0]['userCount']); },
            beforeSend: function(){ $('#inactiveUsers').val(0); },
            error: function(msg){ $('#inactiveUsers').val(0); }
        });
	});
</script>
<!-- Jobs Country + Category + Date Range filter -->
<script>
	//Jobs Filter on Country Change
	$("#select2-orderStats").on('change', function(e) {
	    var country_id = $(this).select2().val();
	    var cat_id = $('#cat-id-orderStats').val();
	    if($('#from-to-date-orderStats').text() != 'TILL DATE') {
	    	var start_date = $.trim($('#from-to-date-orderStats').text().substr(0,10));
	    } else { var start_date = 0; } 
	    if($('#from-to-date-orderStats').text() != 'TILL DATE') { 
	    	var to_date = $.trim($('#from-to-date-orderStats').text().substr(13,23));
	    } else { var to_date = 0; }
	    //set hidden values to form
	    $('#totalJobsStartDate').val(start_date);
	    $('#totalJobsToDate').val(to_date);
	    $('#totalJobsCountryId').val(country_id);
	    $('#totalJobsCatId').val(cat_id);

	    $('#totalOpenJobsStartDate').val(start_date);
	    $('#totalOpenJobsToDate').val(to_date);
	    $('#totalOpenJobsCountryId').val(country_id);
	    $('#totalOpenJobsCatId').val(cat_id);
	    
	    $('#totalAcceptedJobsStartDate').val(start_date);
	    $('#totalAcceptedJobsToDate').val(to_date);
	    $('#totalAcceptedJobsCountryId').val(country_id);
	    $('#totalAcceptedJobsCatId').val(cat_id);
	    
	    $('#totalInprogressJobsStartDate').val(start_date);
	    $('#totalInprogressJobsToDate').val(to_date);
	    $('#totalInprogressJobsCountryId').val(country_id);
	    $('#totalInprogressJobsCatId').val(cat_id);
	    
	    $('#totalDeliveredJobsStartDate').val(start_date);
	    $('#totalDeliveredJobsToDate').val(to_date);
	    $('#totalDeliveredJobsCountryId').val(country_id);
	    $('#totalDeliveredJobsCatId').val(cat_id);
	    
	    $('#totalCancelledJobsStartDate').val(start_date);
	    $('#totalCancelledJobsToDate').val(to_date);
	    $('#totalCancelledJobsCountryId').val(country_id);
	    $('#totalCancelledJobsCatId').val(cat_id);
	    
	    $('#totalExpiredJobsStartDate').val(start_date);
	    $('#totalExpiredJobsToDate').val(to_date);
	    $('#totalExpiredJobsCountryId').val(country_id);
	    $('#totalExpiredJobsCatId').val(cat_id);
	    
	    $('#rejectedJobsCountStartDate').val(start_date);
	    $('#rejectedJobsCountToDate').val(to_date);
	    $('#rejectedJobsCountCountryId').val(country_id);
	    $('#rejectedJobsCountCatId').val(cat_id);

	    //Total JOBS
	    // $.ajax({
     //    type: "POST", 
     //    url: "get-job-count-by-filters", 
     //    data: { order_status: 'all', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
     //    dataType: "json",
     //    success: function(res){ $('#totalJobs').text(res[0]['jobCount']); },
     //    beforeSend: function(){ $('#totalJobs').val(0); },
     //    error: function(msg){ $('#totalJobs').val(0); } 
     //  });
     //Total JOBS
	    $.ajax({
        type: "POST",
        url: "get-job-count-by-filters", 
        data: { order_status: 'all', country_id:country_id, record_type:'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res)
        {
         $('#totalJobs').text(res[0]['jobCount']);
         if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
         	$('#totalJobs_title').text("Total jobs");
         }
         if(cat_id == 281){
         	$('#totalJobs_title').text("Total trips");
         }

       	},
        beforeSend: function(){ $('#totalJobs').val(0); },
        error: function(msg){ $('#totalJobs').val(0); } 
      });
        //Total Open Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'open', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){
         $('#totalOpenJobs').text(res[0]['jobCount']);

         	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalOpenJobs_title').text("Open");
	        }
	        if(cat_id == 281){
	         $('#totalOpenJobs_title').text("Current trips");
	        }
         
       	},
        beforeSend: function(){ $('#totalOpenJobs').val(0); },
        error: function(msg){ $('#totalOpenJobs').val(0); } 
      });
        //Total Accepted Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'accepted', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalAcceptedJobs').text(res[0]['jobCount']); 
	        if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalAcceptedJobs_title').text("Accepted");
	        }
	        if(cat_id == 281){
	         $('#totalAcceptedJobs_title').text("Upcoming trips");
	        }
        },
        beforeSend: function(){ $('#totalAcceptedJobs').val(0); },
        error: function(msg){ $('#totalAcceptedJobs').val(0); } 
      });
        //Total Inprogress Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'inprogress', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalInprogressJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalInprogressJobs_title').text("In-Progress");
	        }
	        if(cat_id == 281){
	         $('#totalInprogressJobs_title').text("Previous trips");
	        } 
        },
        beforeSend: function(){ $('#totalInprogressJobs').val(0); },
        error: function(msg){ $('#totalInprogressJobs').val(0); } 
      });
        //Total Delivered Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'delivered', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalDeliveredJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalDeliveredJobs_title').text("Delivered");
	        }
	        if(cat_id == 281){
	         $('#totalDeliveredJobs_title').text("Completed trips");
	        } 
      	},
        beforeSend: function(){ $('#totalDeliveredJobs').val(0); },
        error: function(msg){ $('#totalDeliveredJobs').val(0); } 
      });
        //Total Expired Jobs
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	        	$('#expired_div').show();
	         	$('#reject_div').show();
	        }
	        if(cat_id == 281){
	        	$('#expired_div').hide();
	        	$('#reject_div').hide();
	        }
	        if(cat_id == 9){
	        	$('#expired_div').hide();
	        	$('#reject_div').hide();
	        }
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'expired', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ $('#totalExpiredJobs').text(res[0]['jobCount']);},
        beforeSend: function(){ $('#totalExpiredJobs').val(0); },
        error: function(msg){ $('#totalExpiredJobs').val(0); } 
      });
        //Total Cancelled Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'cancelled', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalCancelledJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalCancelledJobs_title').text("Cancelled");
	        }
	        if(cat_id == 281){
	         $('#totalCancelledJobs_title').text("Cancelled trips");
	        }
        },
        beforeSend: function(){ $('#totalCancelledJobs').val(0); },
        error: function(msg){ $('#totalCancelledJobs').val(0); } 
      });
	});
	//Jobs Filter on category Change
	$("#cat-id-orderStats").on('change', function(e) {
	    var cat_id = $(this).select2().val();
	    var country_id = $('#select2-orderStats').val();
	    if($('#from-to-date-orderStats').text() != 'TILL DATE') {
	    	var start_date = $.trim($('#from-to-date-orderStats').text().substr(0,10));
	    } else { var start_date = 0; } 
	    if($('#from-to-date-orderStats').text() != 'TILL DATE') { 
	    	var to_date = $.trim($('#from-to-date-orderStats').text().substr(13,23));
	    } else { var to_date = 0; }
	    //set hidden values to form
	    $('#totalJobsStartDate').val(start_date);
	    $('#totalJobsToDate').val(to_date);
	    $('#totalJobsCountryId').val(country_id);
	    $('#totalJobsCatId').val(cat_id);

	    $('#totalOpenJobsStartDate').val(start_date);
	    $('#totalOpenJobsToDate').val(to_date);
	    $('#totalOpenJobsCountryId').val(country_id);
	    $('#totalOpenJobsCatId').val(cat_id);
	    
	    $('#totalAcceptedJobsStartDate').val(start_date);
	    $('#totalAcceptedJobsToDate').val(to_date);
	    $('#totalAcceptedJobsCountryId').val(country_id);
	    $('#totalAcceptedJobsCatId').val(cat_id);
	    
	    $('#totalInprogressJobsStartDate').val(start_date);
	    $('#totalInprogressJobsToDate').val(to_date);
	    $('#totalInprogressJobsCountryId').val(country_id);
	    $('#totalInprogressJobsCatId').val(cat_id);
	    
	    $('#totalDeliveredJobsStartDate').val(start_date);
	    $('#totalDeliveredJobsToDate').val(to_date);
	    $('#totalDeliveredJobsCountryId').val(country_id);
	    $('#totalDeliveredJobsCatId').val(cat_id);
	    
	    $('#totalCancelledJobsStartDate').val(start_date);
	    $('#totalCancelledJobsToDate').val(to_date);
	    $('#totalCancelledJobsCountryId').val(country_id);
	    $('#totalCancelledJobsCatId').val(cat_id);
	    
	    $('#totalExpiredJobsStartDate').val(start_date);
	    $('#totalExpiredJobsToDate').val(to_date);
	    $('#totalExpiredJobsCountryId').val(country_id);
	    $('#totalExpiredJobsCatId').val(cat_id);
	    
	    $('#rejectedJobsCountStartDate').val(start_date);
	    $('#rejectedJobsCountToDate').val(to_date);
	    $('#rejectedJobsCountCountryId').val(country_id);
	    $('#rejectedJobsCountCatId').val(cat_id);
	    //Total JOBS
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'all', country_id:country_id, record_type:'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res)
        {
         $('#totalJobs').text(res[0]['jobCount']);
         if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
         	$('#totalJobs_title').text("Total jobs");
         }
         if(cat_id == 281){
         	$('#totalJobs_title').text("Total trips");
         }

       	},
        beforeSend: function(){ $('#totalJobs').val(0); },
        error: function(msg){ $('#totalJobs').val(0); } 
      });
        //Total Open Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'open', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){
         $('#totalOpenJobs').text(res[0]['jobCount']);

         	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalOpenJobs_title').text("Open");
	        }
	        if(cat_id == 281){
	         $('#totalOpenJobs_title').text("Current trips");
	        }
	        if(cat_id == 9){
	         $('#totalOpenJobs_title').text("Accepted");
	        }
         
       	},
        beforeSend: function(){ $('#totalOpenJobs').val(0); },
        error: function(msg){ $('#totalOpenJobs').val(0); } 
      });
        //Total Accepted Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'accepted', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalAcceptedJobs').text(res[0]['jobCount']); 
	        if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalAcceptedJobs_title').text("Accepted");
	        }
	        if(cat_id == 281){
	         $('#totalAcceptedJobs_title').text("Upcoming trips");
	        }

	        if(cat_id == 9){
	         $('#totalAcceptedJobs_title').text("In-progress");
	        }
        },
        beforeSend: function(){ $('#totalAcceptedJobs').val(0); },
        error: function(msg){ $('#totalAcceptedJobs').val(0); } 
      });
        //Total Inprogress Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'inprogress', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalInprogressJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalInprogressJobs_title').text("In-Progress");
	        }
	        if(cat_id == 281){
	         $('#totalInprogressJobs_title').text("Previous trips");
	        }
	        if(cat_id == 9){
	         $('#totalInprogressJobs_title').text("Completed");
	        } 
        },
        beforeSend: function(){ $('#totalInprogressJobs').val(0); },
        error: function(msg){ $('#totalInprogressJobs').val(0); } 
      });
        //Total Delivered Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'delivered', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalDeliveredJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalDeliveredJobs_title').text("Delivered");
	        }
	        if(cat_id == 281){
	         $('#totalDeliveredJobs_title').text("Completed trips");
	        }
	        if(cat_id == 9){
	         $('#totalDeliveredJobs_title').text("Cancelled");
	        } 
      	},
        beforeSend: function(){ $('#totalDeliveredJobs').val(0); },
        error: function(msg){ $('#totalDeliveredJobs').val(0); } 
      });
        //Total Expired Jobs
        if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#expired_div').show();
	         $('#reject_div').show();
	         $('#cancelled_div').show();
	        }
	        if(cat_id == 281){
	         $('#expired_div').hide();
	         $('#reject_div').hide();
	        }
	        if(cat_id == 9){
	         $('#expired_div').hide();
	         $('#reject_div').hide();
	         $('#cancelled_div').hide();
	        }

	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'expired', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ $('#totalExpiredJobs').text(res[0]['jobCount']);},
        beforeSend: function(){ $('#totalExpiredJobs').val(0); },
        error: function(msg){ $('#totalExpiredJobs').val(0); } 
      });
        //Total Cancelled Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'cancelled', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalCancelledJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalCancelledJobs_title').text("Cancelled");
	        }
	        if(cat_id == 281){
	         $('#totalCancelledJobs_title').text("Cancelled trips");
	        }
        },
        beforeSend: function(){ $('#totalCancelledJobs').val(0); },
        error: function(msg){ $('#totalCancelledJobs').val(0); } 
      });
	});
	//Jobs Filter on Date Change
	$("#from-to-date-orderStats").on('DOMSubtreeModified', function(e){
	    var cat_id = $('#cat-id-orderStats').val();
	    var country_id = $('#select2-orderStats').val();
	    if($('#from-to-date-orderStats').text() != 'TILL DATE') {
	    	var start_date = $.trim($('#from-to-date-orderStats').text().substr(0,10));
	    } else { var start_date = 0; } 
	    if($('#from-to-date-orderStats').text() != 'TILL DATE') { 
	    	var to_date = $.trim($('#from-to-date-orderStats').text().substr(13,23));
	    } else { var to_date = 0; }
	    //set hidden values to form
	    $('#totalJobsStartDate').val(start_date);
	    $('#totalJobsToDate').val(to_date);
	    $('#totalJobsCountryId').val(country_id);
	    $('#totalJobsCatId').val(cat_id);

	    $('#totalOpenJobsStartDate').val(start_date);
	    $('#totalOpenJobsToDate').val(to_date);
	    $('#totalOpenJobsCountryId').val(country_id);
	    $('#totalOpenJobsCatId').val(cat_id);
	    
	    $('#totalAcceptedJobsStartDate').val(start_date);
	    $('#totalAcceptedJobsToDate').val(to_date);
	    $('#totalAcceptedJobsCountryId').val(country_id);
	    $('#totalAcceptedJobsCatId').val(cat_id);
	    
	    $('#totalInprogressJobsStartDate').val(start_date);
	    $('#totalInprogressJobsToDate').val(to_date);
	    $('#totalInprogressJobsCountryId').val(country_id);
	    $('#totalInprogressJobsCatId').val(cat_id);
	    
	    $('#totalDeliveredJobsStartDate').val(start_date);
	    $('#totalDeliveredJobsToDate').val(to_date);
	    $('#totalDeliveredJobsCountryId').val(country_id);
	    $('#totalDeliveredJobsCatId').val(cat_id);
	    
	    $('#totalCancelledJobsStartDate').val(start_date);
	    $('#totalCancelledJobsToDate').val(to_date);
	    $('#totalCancelledJobsCountryId').val(country_id);
	    $('#totalCancelledJobsCatId').val(cat_id);
	    
	    $('#totalExpiredJobsStartDate').val(start_date);
	    $('#totalExpiredJobsToDate').val(to_date);
	    $('#totalExpiredJobsCountryId').val(country_id);
	    $('#totalExpiredJobsCatId').val(cat_id);
	    
	    $('#rejectedJobsCountStartDate').val(start_date);
	    $('#rejectedJobsCountToDate').val(to_date);
	    $('#rejectedJobsCountCountryId').val(country_id);
	    $('#rejectedJobsCountCatId').val(cat_id);
	    //Total JOBS
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'all', country_id:country_id, record_type:'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res)
        {
        	console.log(res);
         $('#totalJobs').text(res[0]['jobCount']);
         if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
         	$('#totalJobs_title').text("Total jobs");
         }
         if(cat_id == 281){
         	$('#totalJobs_title').text("Total trips");
         }

       	},
        beforeSend: function(){ $('#totalJobs').val(0); },
        error: function(msg){ $('#totalJobs').val(0); } 
      });
        //Total Open Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'open', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){
         $('#totalOpenJobs').text(res[0]['jobCount']);

         	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalOpenJobs_title').text("Open");
	        }
	        if(cat_id == 281){
	         $('#totalOpenJobs_title').text("Current trips");
	        }
         
       	},
        beforeSend: function(){ $('#totalOpenJobs').val(0); },
        error: function(msg){ $('#totalOpenJobs').val(0); } 
      });
        //Total Accepted Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'accepted', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalAcceptedJobs').text(res[0]['jobCount']); 
	        if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalAcceptedJobs_title').text("Accepted");
	        }
	        if(cat_id == 281){
	         $('#totalAcceptedJobs_title').text("Upcoming trips");
	        }
        },
        beforeSend: function(){ $('#totalAcceptedJobs').val(0); },
        error: function(msg){ $('#totalAcceptedJobs').val(0); } 
      });
        //Total Inprogress Jobs
	    $.ajax({
        type: "POST",
        url: "get-job-count-by-filters", 
        data: { order_status: 'inprogress', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){
        	//console.log(res); 
        	$('#totalInprogressJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalInprogressJobs_title').text("In-Progress");
	        }
	        if(cat_id == 281){
	         $('#totalInprogressJobs_title').text("Previous trips");
	        } 
        },
        beforeSend: function(){ $('#totalInprogressJobs').val(0); },
        error: function(msg){ $('#totalInprogressJobs').val(0); } 
      });
        //Total Delivered Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'delivered', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ 
        	$('#totalDeliveredJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalDeliveredJobs_title').text("Delivered");
	        }
	        if(cat_id == 281){
	         $('#totalDeliveredJobs_title').text("Completed trips");
	        } 
      	},
        beforeSend: function(){ $('#totalDeliveredJobs').val(0); },
        error: function(msg){ $('#totalDeliveredJobs').val(0); } 
      });
        //Total Expired Jobs
        if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#expired_div').show();
	         $('#reject_div').show();
	        }
	        if(cat_id == 281){
	         $('#expired_div').hide();
	         $('#reject_div').hide();
	        }
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters",
        data: { order_status: 'expired', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){ $('#totalExpiredJobs').text(res[0]['jobCount']);},
        beforeSend: function(){ $('#totalExpiredJobs').val(0); },
        error: function(msg){ $('#totalExpiredJobs').val(0); } 
      });
        //Total Cancelled Jobs
	    $.ajax({
        type: "POST", 
        url: "get-job-count-by-filters", 
        data: { order_status: 'cancelled', country_id: country_id, record_type: 'count', rows:0, cat_id: cat_id, start_date: start_date, to_date: to_date },
        dataType: "json",
        success: function(res){

        	//console.log(res); 
        	$('#totalCancelledJobs').text(res[0]['jobCount']);
        	if(cat_id == 6 || cat_id == 7 || cat_id == 280 || cat_id == 0){
	         $('#totalCancelledJobs_title').text("Cancelled");
	        }
	        if(cat_id == 281){
	         $('#totalCancelledJobs_title').text("Cancelled trips");
	        }
        },
        beforeSend: function(){ $('#totalCancelledJobs').val(0); },
        error: function(msg){ $('#totalCancelledJobs').val(0); } 
      });
	});
</script>
<!-- Other Account Country + Category + Date Range filter -->
<script>
	//Other Account Filter on Country Change
	$("#select2-otherAccount").on('change', function(e) {
	    var country_id = $(this).select2().val();
	    var cat_id = $('#cat-id-otherAccount').val();
	    if($('#from-to-date-otherAccount').text() != 'TILL DATE') {
	    	var start_date = $.trim($('#from-to-date-otherAccount').text().substr(0,10));
	    } else { var start_date = 0; } 
	    if($('#from-to-date-otherAccount').text() != 'TILL DATE') { 
	    	var to_date = $.trim($('#from-to-date-otherAccount').text().substr(13,23));
	    } else { var to_date = 0; }	
	    var base_url = "<?=base_url()?>";
	    //Total Commission 
	    $.ajax({
            type: "POST", 
            url: "get-other-account-details-by-filters", 
            data: { account_type: 'commission_amount', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
            dataType: "json",
            success: function(res){ //console.log(res);
					$.each( res, function(){$('#gonagoo_commission').append(
            		'<a href="'+base_url+'admin/currencywise-gonagoo-comission-history/'+$(this).attr('currency_code')+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+$(this).attr('amount')+'</div></div></a>'
            		);});
             	},
            beforeSend: function(){ $('#gonagoo_commission').empty(); },
            error: function(msg){ $('#gonagoo_commission').empty(); } 
        });
       	//Total Insurance 
	    $.ajax({
            type: "POST", 
            url: "get-other-account-details-by-filters", 
            data: { account_type: 'insurance_fee', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
            dataType: "json",
            success: function(res){ //console.log(res);
					$.each( res, function(){$('#gonagoo_insurance').append(
            		'<a href="'+base_url+'admin/currencywise-insurance-history/'+$(this).attr('currency_code')+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+$(this).attr('amount')+'</div></div></a>'
            		);});
             	},
            beforeSend: function(){ $('#gonagoo_insurance').empty(); },
            error: function(msg){ $('#gonagoo_insurance').empty(); } 
        });
        //Total Custom 
	    $.ajax({
            type: "POST", 
            url: "get-other-account-details-by-filters", 
            data: { account_type: 'custom_clearance_fee', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
            dataType: "json",
            success: function(res){ //console.log(res);
					$.each( res, function(){$('#gonagoo_custom').append(
            		'<a href="'+base_url+'admin/currencywise-custom-history/'+$(this).attr('currency_code')+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+$(this).attr('amount')+'</div></div></a>'
            		);});
             	},
            beforeSend: function(){ $('#gonagoo_custom').empty(); },
            error: function(msg){ $('#gonagoo_custom').empty(); } 
        });
	});
	//Other Account Filter on category Change
	$("#cat-id-otherAccount").on('change', function(e) {
	    var cat_id = $(this).select2().val();
	    var country_id = $('#select2-otherAccount').val();
	    
	    if($('#from-to-date-otherAccount').text() != 'TILL DATE') {
	    	var start_date = $.trim($('#from-to-date-otherAccount').text().substr(0,10));
	    } else { var start_date = 0; } 
	    if($('#from-to-date-otherAccount').text() != 'TILL DATE') { 
	    	var to_date = $.trim($('#from-to-date-otherAccount').text().substr(13,23));
	    } else { var to_date = 0; }	
	    var base_url = "<?=base_url()?>";
	    //Total Commission 
	    $.ajax({
            type: "POST", 
            url: "get-other-account-details-by-filters", 
            data: { account_type: 'commission_amount', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
            dataType: "json",
            success: function(res){
             //console.log(res);
							$.each( res, function(){
								$('#gonagoo_commission').append(
	            		'<a href="'+base_url+'admin/currencywise-gonagoo-comission-history/'+$(this).attr('currency_code')+'/'+cat_id+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+$(this).attr('amount')+'</div></div></a>'
	            	);}
							);
            },
            beforeSend: function(){ $('#gonagoo_commission').empty(); },
            error: function(msg){ $('#gonagoo_commission').empty(); } 
        });
       	//Total Insurance 
	    $.ajax({
            type: "POST", 
            url: "get-other-account-details-by-filters", 
            data: { account_type: 'insurance_fee', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
            dataType: "json",
            success: function(res){ //console.log(res);
					$.each( res, function(){$('#gonagoo_insurance').append(
            		'<a href="'+base_url+'admin/currencywise-insurance-history/'+$(this).attr('currency_code')+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+$(this).attr('amount')+'</div></div></a>'
            		);});
             	},
            beforeSend: function(){ $('#gonagoo_insurance').empty(); },
            error: function(msg){ $('#gonagoo_insurance').empty(); } 
        });
        //Total Custom 
	    $.ajax({
            type: "POST", 
            url: "get-other-account-details-by-filters", 
            data: { account_type: 'custom_clearance_fee', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
            dataType: "json",
            success: function(res){ //console.log(res);
					$.each( res, function(){$('#gonagoo_custom').append(
            		'<a href="'+base_url+'admin/currencywise-custom-history/'+$(this).attr('currency_code')+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+$(this).attr('amount')+'</div></div></a>'
            		);});
             	},
            beforeSend: function(){ $('#gonagoo_custom').empty(); },
            error: function(msg){ $('#gonagoo_custom').empty(); } 
        });
	});
	//Other Account Filter on Date Change
	$("#from-to-date-otherAccount").on('DOMSubtreeModified', function(e) {
	    var cat_id = $('#cat-id-otherAccount').val();
	    var country_id = $('#select2-otherAccount').val();
	    if($('#from-to-date-otherAccount').text() != 'TILL DATE') {
	    	var start_date = $.trim($('#from-to-date-otherAccount').text().substr(0,10));
	    } else { var start_date = 0; } 
	    if($('#from-to-date-otherAccount').text() != 'TILL DATE') { 
	    	var to_date = $.trim($('#from-to-date-otherAccount').text().substr(13,23));
	    } else { var to_date = 0; }	
	    if(start_date != '' && to_date  != '') {
		    var base_url = "<?=base_url()?>";
		    //Total Commission 
		    $.ajax({
	            type: "POST", 
	            url: "get-other-account-details-by-filters", 
	            data: { account_type: 'commission_amount', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
	            dataType: "json",
	            success: function(res){ //console.log(res);
						$.each( res, function(){$('#gonagoo_commission').append(
	            		'<a href="'+base_url+'admin/currencywise-gonagoo-comission-history/'+$(this).attr('currency_code')+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+round2Fixed($(this).attr('amount'))+'</div></div></a>'
	            		);});
	             	},
	            beforeSend: function(){ $('#gonagoo_commission').empty(); },
	            error: function(msg){ $('#gonagoo_commission').empty(); } 
	        });
	       	//Total Insurance 
		    $.ajax({
	            type: "POST", 
	            url: "get-other-account-details-by-filters", 
	            data: { account_type: 'insurance_fee', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
	            dataType: "json",
	            success: function(res){ //console.log(res);
						$.each( res, function(){$('#gonagoo_insurance').append(
	            		'<a href="'+base_url+'admin/currencywise-insurance-history/'+$(this).attr('currency_code')+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+round2Fixed($(this).attr('amount'))+'</div></div></a>'
	            		);});
	             	},
	            beforeSend: function(){ $('#gonagoo_insurance').empty(); },
	            error: function(msg){ $('#gonagoo_insurance').empty(); } 
	        });
	        //Total Custom 
		    $.ajax({
	            type: "POST", 
	            url: "get-other-account-details-by-filters", 
	            data: { account_type: 'custom_clearance_fee', start_date: start_date, to_date: to_date, country_id: country_id, cat_id: cat_id },
	            dataType: "json",
	            success: function(res){// console.log(res);
						$.each( res, function(){$('#gonagoo_custom').append(
	            		'<a href="'+base_url+'admin/currencywise-custom-history/'+$(this).attr('currency_code')+'"><div class="col-sm-3 col-xs-6"><div style="font-size: 24px; color: #FFF; font-weight: bold;">'+$(this).attr('currency_code')+'</div></div><div class="col-sm-9 col-xs-12 text-right"><div class="num" style="font-size: 24px;">'+round2Fixed($(this).attr('amount'))+'</div></div></a>'
	            		);});
	             	},
	            beforeSend: function(){ $('#gonagoo_custom').empty(); },
	            error: function(msg){ $('#gonagoo_custom').empty(); } 
	        });
		}
	});
	function round2Fixed(value) {
		value = +value;
		if (isNaN(value))
			return 0;
		value = value.toString().split('e');
		value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + 2) : 2)));
		value = value.toString().split('e');
		return (+(value[0] + 'e' + (value[1] ? (+value[1] - 2) : -2))).toFixed(2);
	}
</script>
<script>
	$(document).ready(function() { 
		//Jobs
	    $('#aTotalJobs').click(function(e) {
	        e.preventDefault();
	        var cat_id = $('#totalJobsCatId').val();
	        if(cat_id!=0){
						$('#frmTotalJobs').submit();
	        }else{
	        	swal({
			        title: "Error",
			        text: "Please Select Category First",
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: true
			    	});
	        }
	    });
	    
	    $('#atotalOpenJobs').click(function(e) {
	        e.preventDefault();
	        var cat_id = $('#totalOpenJobsCatId').val();
	        if(cat_id!=0){
						$('#frmtotalOpenJobs').submit();
	        }else{
	        	swal({
			        title: "Error",
			        text: "Please Select Category First",
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: true
			    	});
	        }
	    });
	    $('#atotalAcceptedJobs').click(function(e) {
	        e.preventDefault();
					var cat_id = $('#totalAcceptedJobsCatId').val();
	        if(cat_id!=0){
						$('#frmtotalAcceptedJobs').submit();
	        }else{
	        	swal({
			        title: "Error",
			        text: "Please Select Category First",
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: true
			    	});
	        }
	    });
	    $('#atotalInprogressJobs').click(function(e) {
	        e.preventDefault();
	        var cat_id = $('#totalInprogressJobsCatId').val();
	        if(cat_id!=0){
						$('#frmtotalInprogressJobs').submit();
	        }else{
	        	swal({
			        title: "Error",
			        text: "Please Select Category First",
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: true
			    	});
	        }
	    });
	    $('#atotalDeliveredJobs').click(function(e) {
	        e.preventDefault();
	        var cat_id = $('#totalDeliveredJobsCatId').val();
	        if(cat_id!=0){
						$('#frmtotalDeliveredJobs').submit();
	        }else{
	        	swal({
			        title: "Error",
			        text: "Please Select Category First",
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: true
			    	});
	        }
	    });
	    $('#atotalCancelledJobs').click(function(e) {
	        e.preventDefault();
	        var cat_id = $('#totalCancelledJobsCatId').val();
	        if(cat_id!=0){
						$('#frmtotalCancelledJobs').submit();
	        }else{
	        	swal({
			        title: "Error",
			        text: "Please Select Category First",
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: true
			    	});
	        }
	    });
	    $('#atotalExpiredJobs').click(function(e) {
	        e.preventDefault();
	        var cat_id = $('#totalExpiredJobsCatId').val();
	        if(cat_id!=0){
						$('#frmtotalExpiredJobs').submit();
	        }else{
	        	swal({
			        title: "Error",
			        text: "Please Select Category First",
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: true
			    	});
	        }
	    });
	    $('#arejectedJobsCount').click(function(e) {
	        e.preventDefault();
	        var cat_id = $('#rejectedJobsCountCatId').val();
	        if(cat_id!=0){
						$('#frmrejectedJobsCount').submit();
	        }else{
	        	swal({
			        title: "Error",
			        text: "Please Select Category First",
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: "Close",
			        closeOnConfirm: true
			    	});
	        }
	    });
	    //User 
	    $('#atotalUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmtotalUsers').submit();
	    });
	    $('#abusinessUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmbusinessUsers').submit();
	    });
	    $('#abuyerUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmbuyerUsers').submit();
	    });
	    $('#aindividualUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmindividualUsers').submit();
	    });
	    $('#asellerUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmsellerUsers').submit();
	    });
	    $('#abothUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmbothUsers').submit();
	    });
	    $('#aonlineUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmonlineUsers').submit();
	    });
	    $('#aofflineUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmofflineUsers').submit();
	    });
	    $('#aactiveUsers').click(function(e) {
	        e.preventDefault();
	        $('#frmactiveUsers').submit();
	    });
	    $('#ainactiveUsers').click(function(e) {
	        e.preventDefault();
	        $('#frminactiveUsers').submit();
	    });
	});
</script>
<!-- Graphs Start From Here -->
<script src="<?= $this->config->item('resource_url') . 'js/raphael-min.js';?>"></script>
<script src="<?= $this->config->item('resource_url') . 'js/'; ?>morris.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
<link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'css/'; ?>morris.css">
<script type="text/javascript">
	var totalUsers = <?php echo $totalUsers[0]['userCount']; ?>;
	var businessUsers = <?php echo $businessUsers[0]['userCount']; ?>;
	var individualUsers = <?php echo $individualUsers[0]['userCount']; ?>;
	var buyerUsers = <?php echo $buyerUsers[0]['userCount']; ?>;
	var sellerUsers = <?php echo $sellerUsers[0]['userCount']; ?>;
	var bothUsers = <?php echo $bothUsers[0]['userCount']; ?>;
	var user_graph = Morris.Bar({
		element: 'user_graph',
		data: [
			{x: 'Total Users', y: totalUsers},
			{x: 'Business', y: businessUsers},
			{x: 'Individual', y: individualUsers},
			{x: 'Buyer', y: buyerUsers},
			{x: 'Seller', y: sellerUsers},
			{x: 'Both Users', y: bothUsers},
		],
		xkey: 'x',
		ykeys: ['y'],
		labels: ['Users'],
		//barColors: ["#303641", "#303641", "#303641", "#00a65a", "#00a65a", "#00a65a"],
		barColors: function (row, series, type) {
			//console.log("--> "+row.label, series, type);
			if(row.label == "Total Users") return "#303641";
			else if(row.label == "Business") return "#303641";
			else if(row.label == "Individual") return "#303641";
			else if(row.label == "Buyer") return "#00a65a";
			else if(row.label == "Seller") return "#00a65a";
			else if(row.label == "Both Users") return "#00a65a";
		},
		resize: true
	}).on('click', function(i, row){
		//console.log(i, row);
	});
</script>
<script type="text/javascript">
  	var totalJobs = <?php echo $totalJobs[0]['jobCount']; ?>;
  	var totalDeliveredJobs = <?php echo $totalDeliveredJobs[0]['jobCount']; ?>;
  	var totalCancelledJobs = <?php echo $totalCancelledJobs[0]['jobCount']; ?>;
  	var totalExpiredJobs = <?php echo $totalExpiredJobs[0]['jobCount']; ?>;
  	var totalRejectedJobs = 0;
  	var job_graph = Morris.Bar({
  	element: 'job_graph',
  	data: [
	    {x: 'Total', y: totalJobs},
	    {x: 'Delivered', y: totalDeliveredJobs},
	    {x: 'Cancelled', y: totalCancelledJobs},
	    {x: 'Expired', y: totalExpiredJobs},
	    {x: 'Rejected', y: totalRejectedJobs},
  	],
	xkey: 'x',
	ykeys: ['y'],
	labels: ['Jobs'],
  	//barColors: 'rgb(210,120,23)',
  	barColors: function (row, series, type) {
		//console.log("--> "+row.label);
		if(row.label == "Total") return "#00a65a";
		else if(row.label == "Delivered") return "#f56954";
		else if(row.label == "Cancelled") return "#f89d00";
		else if(row.label == "Expired") return "#ea2474";
		else if(row.label == "Rejected") return "#008d4c";
	 },
  	resize: true
	}).on('click', function(i, row){
  		console.log(i, row);
	});
</script>
<script type="text/javascript">
  	var earned_to_date_graph = Morris.Bar({
	  	element: 'earned_to_date_graph',
		data: <?php echo json_encode($earned_to_date_data_array); ?>,
		xkey: 'x',
		ykeys: ['y'],
		labels: ['Earned'],
		barColors: ['#00639e'],
		resize: true
 	}).on('click', function(i, row){
  		console.log(i, row);
 	});
</script>
<script type="text/javascript">
	var paid_to_date_graph = Morris.Bar({
		element: 'paid_to_date_graph',
		data: <?php echo json_encode($paid_to_date_data_array); ?>,
		xkey: 'x',
		ykeys: ['y'],
		labels: ['Paid'],
		barColors: ['#ffa812'],
		resize: true
	}).on('click', function(i, row){
		console.log(i, row);
	});
</script>
<script type="text/javascript">
  	var commission_graph = Morris.Bar({
		element: 'commission_graph',
		data: <?php echo json_encode($commission_data_array); ?>,
		xkey: 'x',
		ykeys: ['y'],
		labels: ['Commission'],
		barColors: ['#00acd6'],
		resize: true
	}).on('click', function(i, row){
		console.log(i, row);
	});
</script>
<script type="text/javascript">
  	var insurance_graph = Morris.Bar({
		element: 'insurance_graph',
		data: <?php echo json_encode($insurance_data_array); ?>,
		xkey: 'x',
		ykeys: ['y'],
		labels: ['Insurance'],
		barColors: ['#008d4c'],
		resize: true
	}).on('click', function(i, row){
		console.log(i, row);
	});
</script>
<script type="text/javascript">
	var custom_graph = Morris.Bar({
		element: 'custom_graph',
		data: <?php echo json_encode($custom_data_array); ?>,
		xkey: 'x',
		ykeys: ['y'],
		labels: ['Custom'],
		barColors: ['#f4543c'],
		resize: true
	}).on('click', function(i, row){
		console.log(i, row);
	});
</script>
<script type="text/javascript">
	$(document).ready(function() { 		
		var transaction_report_graph;
		var value = '<?php echo date("m-d-Y"); ?>';
      	var firstDate = moment(value, "MM/DD/YYYY").day(0).format("MM/DD/YYYY");
      	var lastDate =  moment(value, "MM/DD/YYYY").day(6).format("MM/DD/YYYY");
      	$("#weeklyDatePicker").val(firstDate + " To " + lastDate);
	});
</script>
<script type="text/javascript">
  	var transaction_report_graph = Morris.Bar({
		element: 'transaction_report_graph',
		data: [
	    	{x: '<?php echo date('d F Y'); ?>', y: null, z: null},		    		    
	  	],
		xkey: 'x',
		ykeys: ['y','z'],
		labels: ['Added','Deducted'],
		barColors: ['#00a65a','#FF0000'],
		resize: true
	}).on('click', function(i, row){
		console.log(i, row);
	});
</script>
<script type="text/javascript">
	$('.transaction_filters').hide();
	$('#transaction_daily_filter').show();
	$('#transaction_duration_type').on('change',function(){
		$('.transaction_filters').hide();
		var filter_type = $(this).val();
		$('#transaction_' + filter_type + '_filter').show();
		$(window).trigger('resize');
	});
</script>
<script type="text/javascript">
	$(function(){
		$( "#daily_date_picker" ).datepicker();
		//Pass the user selected date format
		$( "#daily_date_picker" ).change(function() {
			$( "#daily_date_picker" ).datepicker( "option", "dateFormat", $(this).val() );
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
	  	//Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
	  	$("#weeklyDatePicker").datepicker({
	    	dateFormat: 'MM-DD-YYYY'
	  	});
	   	//Get the value of Start and End of Week
	 	$('#weeklyDatePicker').on('change', function (e) {
	      	var value = $("#weeklyDatePicker").val();
	      	var firstDate = moment(value, "MM/DD/YYYY").day(0).format("MM/DD/YYYY");
	      	var lastDate =  moment(value, "MM/DD/YYYY").day(6).format("MM/DD/YYYY");
	      	$("#weeklyDatePicker").val(firstDate + " To " + lastDate);
	  	});
	});
</script>
<script type="text/javascript">
	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
	  	var target = $(e.target).attr("href") // activated tab
	  	switch (target) {
			case "#orderStats":
				job_graph.redraw();
				break;
			case "#userStats":
				user_graph.redraw();
				break;
			case "#tillDate":
				earned_to_date_graph.redraw();
				paid_to_date_graph.redraw();
				break; 
			case "#otherAccount":
				commission_graph.redraw();
				insurance_graph.redraw();
				custom_graph.redraw();
				break;
			case "#recentTransactions":
				earned_to_date_graph.redraw();
				paid_to_date_graph.redraw();
				transaction_report_graph.redraw();	      
				break;
			case "#user_and_statistics":
				job_graph.redraw();
				break;
			case "#gonagoo_accounts":
				earned_to_date_graph.redraw();
				paid_to_date_graph.redraw();
				break;
	  	}
	  	$(window).trigger('resize');
	});
</script>
<script type="text/javascript">
	function transaction_graph_data(){
		// common filter
		var transaction_duration_type = $('#transaction_duration_type').val();
		//daily filter
		var date = $('#daily_date_picker').val();
		//weekly filter
		var transaction_currency = $('#transaction_currency').val();
		var weekly_date = $('#weeklyDatePicker').val();
		//monthly filter
		var year = $('#year_dp').val();		
		$.ajax({
            type: "POST", 
            url: "../administration/transaction_graph", 
            data: { filter_type: transaction_duration_type, date: date, transaction_currency:transaction_currency, year:year, weekly_date: weekly_date},
            dataType: "json",
            success: function(res){
        		transaction_report_graph.setData(res);
          		$(window).trigger('resize'); 	
            },
            beforeSend: function(){ },
            error: function(msg){ } 
        });
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		transaction_graph_data(); 
		$('#daily_date_picker').on('change',function(){
			transaction_graph_data();
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#transaction_duration_type').on('change',function(){
			transaction_graph_data();
		});
		$('#weeklyDatePicker').on('change',function(){
			transaction_graph_data();
		});
		$('#transaction_currency').on('change',function(){
			transaction_graph_data();
		});
		$('#year_dp').on('change',function(){
			transaction_graph_data();
		});
	});
</script>

<!-- Graphs Ends Here -->