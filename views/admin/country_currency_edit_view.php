<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/country-currency'); ?>">Country Currency</a>
	</li>
	<li class="active">
		<strong>Edit</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Edit Country Currency
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/country-currency'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" class="form-horizontal form-groups-bordered" id="editCountryCurrency" method="post" action="<?= base_url('admin/update-country-currency'); ?>">
					<input type="hidden" name="cc_id" value="<?= $country_currency['cc_id']; ?>">
					<div class="form-group">
						<label class="col-sm-3 control-label">Country :</label>
						
						<div class="col-sm-5">
							<label class="control-label"><?= $country_currency['country_name']; ?></label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Current Currency :</label>
						
						<div class="col-sm-5">
							<label class="control-label"><?= $country_currency['currency_title']. ' ( '.$country_currency['currency_sign'].' )'; ?></label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Change Currency :</label>
						
						<div class="col-sm-5">
							
							<select id="currency_id" name="currency_id" class="form-control">
								<option value="0">Select currency</option>
								<?php foreach ($currencies as $cr): ?>											
									<option value="<?= $cr['currency_id'] ?>"><?= $cr['currency_title'] . ' ( ' . $cr['currency_sign'].  ' )'; ?></option>
								<?php endforeach ?>
							</select>
							
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-blue">Save Details</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />

<script type="text/javascript">
$(document).ready(function () {
    $("#editCountryCurrency").submit(function (e) {
    	$("#currencyAlert").remove();
        if ($('#currency_id').val() == '0') {
            $('#currency_id').parent().append('<span class="error" id="currencyAlert">Please select currency.</span>');
        	e.preventDefault(e);
        	$("#currency_id").focus();
        	$('#currencyAlert').delay(2000).slideUp(500, function() {
				$("#currencyAlert").remove();
			});
        }
        else {
        	$("#editCountryCurrency").submit();
        }
    });
});
</script>