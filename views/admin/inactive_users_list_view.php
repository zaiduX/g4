<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Inactive Users List</strong>
	</li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>

<h2 style="display: inline-block;">Inactive User List</h2>
<?php $per_manage_users = explode(',', $permissions[0]['manage_users']); 
if(in_array('1', $per_manage_users)) { ?>
	<a type="button" href="<?= base_url('admin/users'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Active Users
		<i class="entypo-eye"></i>
	</a>
<?php } ?>

	<?php if($this->session->flashdata('error')):  ?>
  		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	<?php endif; ?>
	<?php if($this->session->flashdata('success')):  ?>
  		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">ID</th>
			<th class="text-center">Name</th>
			<th class="text-center">Email</th>
			<th class="text-center">Type</th>
			<th class="text-center">Account Type</th>
			<th class="text-center">Balance</th>
			<th class="text-center">Country</th>
			<th class="text-center">Last Login</th>
			<?php if(in_array('1', $per_manage_users) || in_array('4', $per_manage_users)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ($inactive_users_list as $users): $email_cut = explode('@', $users['email1']);  $name = $email_cut[0]; ?>
		<tr>
			<td class="text-center"><?= $users['cust_id'] ?></td>
			<td class="text-center"><?= $users['firstname'] != "NULL" ? $users['firstname'] . ' ' . $users['lastname'] : ($users['company_name'] != "NULL" ? ucwords($users['company_name']) : ucwords($name)) ?></td>
			<td class="text-center"><?= $users['email1'] ?></td>
			<td class="text-center"><?php if($users['user_type'] == 1) echo 'Individual'; else echo 'Business'; ?></td>		
			<td class="text-center"><?= ucfirst($users['acc_type']); ?></td>
			<td class="text-center">
			    <?php  
			        $current_balance = $this->user->get_current_balance($users['cust_id'],'USD');
			        echo '$'; echo ($current_balance != NULL )?$current_balance['account_balance'] : 0;
			    ?>
			</td>		
			<td class="text-center">
		    <?php 
    	        $country_details = $this->notice->get_country_detail($users['country_id']);
    	        $country_name = $country_details['country_name'];
    	        echo $country_name;
		    ?>
			</td>
			
			<td class="text-center"><?= date('d/M/Y',strtotime($users['last_login_datetime'])); ?></td>

			<?php if(in_array('1', $per_manage_users) || in_array('4', $per_manage_users)): ?>
				<td class="text-center">
					
					<div class="btn-group">
						<button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
							Action <span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown" role="menu">
							<?php if(in_array('1', $per_manage_users)): ?>
							<li>	<a href="<?= base_url('admin/view-users/') . $users['cust_id']; ?>"> <i class="entypo-eye"></i>	View </a>	</li>
						<?php endif; ?>
						<?php if(in_array('4', $per_manage_users)): ?>
							<li class="divider"></li>
							<?php if($users['cust_status'] == 0): ?>
								<li> <a data-toggle="tooltip" data-placement="right" title="Click to activate" data-original-title="Click to activate" onclick="activate_users('<?= $users['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-thumbs-up"></i>Activate </a>	</li>
							<?php endif; ?>
						<?php endif; ?>
						</ul>
					</div>
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );
	} );

	function activate_users(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this user?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('activate', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'User has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'User inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	
</script>