<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Assigned Users List</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>
<div class="row">
  <?php
    $total_assigned = $this->users->total_accounts(2);
    $total_assigned = $this->users->convert_big_int($total_assigned);
    $total_assigned_buyer = $this->users->total_accounts(2,0);
    $total_assigned_buyer = $this->users->convert_big_int($total_assigned_buyer);
    $total_assigned_deliverere = $this->users->total_accounts(2,1);
    $total_assigned_deliverere = $this->users->convert_big_int($total_assigned_deliverere);

    $total_followup_pending = $this->users->total_accounts(3);
    $total_followup_pending = $this->users->convert_big_int($total_followup_pending);
    $total_followup_pending_buyer = $this->users->total_accounts(3,0);
    $total_followup_pending_buyer = $this->users->convert_big_int($total_followup_pending_buyer);
    $total_followup_pending_deliverere = $this->users->total_accounts(3,1);
    $total_followup_pending_deliverere = $this->users->convert_big_int($total_followup_pending_deliverere);

    $total_followup_done = $this->users->total_accounts(4);
    $total_followup_done = $this->users->convert_big_int($total_followup_done);
    $total_followup_done_buyer = $this->users->total_accounts(4,0);
    $total_followup_done_buyer = $this->users->convert_big_int($total_followup_done_buyer);
    $total_followup_done_deliverere = $this->users->total_accounts(4,1);
    $total_followup_done_deliverere = $this->users->convert_big_int($total_followup_done_deliverere);
  ?>

  <div class="col-sm-4">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Assigned : <big> <?= $total_assigned; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Business</h6>
          <big class="text-white"> <?= $total_assigned_buyer; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Individual</h6>
          <big class="text-white"> <?= $total_assigned_deliverere; ?></big>
        </div> <br/>
      </div> 
    </div>
  </div>
  
  <div class="col-sm-4">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Followup Pending : <big> <?= $total_followup_pending; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Business</h6>
          <big class="text-white"> <?= $total_followup_pending_buyer; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Individual</h6>
          <big class="text-white"> <?= $total_followup_pending_deliverere; ?></big>
        </div> <br/>
      </div> 
    </div>
  </div>
  
  <div class="col-sm-4">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Followup Taken : <big> <?= $total_followup_done; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Business</h6>
          <big class="text-white"> <?= $total_followup_done_buyer; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Individual</h6>
          <big class="text-white"> <?= $total_followup_done_deliverere; ?></big>
        </div> <br/>
      </div> 
    </div>
  </div>
  
</div>

<h2 style="display: inline-block; margin-top:0px;">Assigned Users List</h2>
  <a type="button" href="<?= base_url('admin/followup-taken-users'); ?>" class="btn btn-success btn-icon icon-left" style="float: right;">
    Followup Users
    <i class="entypo-check"></i>
  </a>

  <?php if($this->session->flashdata('error')):  ?>
      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
      <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
  <?php endif; ?>

<table class="table table-bordered table-striped" id="table-2" width="100%">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Name</th>
      <th class="text-center">Company</th>
      <th class="text-center">Email</th>
      <th class="text-center">Contact</th>
      <th class="text-center">Account Type</th>
      <th class="text-center">Country</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>
  
  <tbody>
    <?php 
      foreach ($users_list as $users): 
        $country_details = $this->notice->get_country_detail($users['country_id']);
        $country_name = $country_details['country_name']; ?>
        <tr>
          <td class="text-center"><?= $users['cust_id'] ?></td>
          <td class="text-center"><?= ($users['firstname'] != "NULL")? $users['firstname'] . ' ' . $users['lastname'] :"Not Provided" ?></td>
          <td class="text-center"><?= ($users['company_name'] != "NULL")? $users['company_name'] :"Not Provided" ?></td>
          <td class="text-center"><?= $users['email1'] ?></td>
          <td class="text-center"><?= '+'.$country_details['country_phonecode'].$users['mobile1'] ?></td>
          <td class="text-center"><?= ucfirst($users['acc_type']); ?></td>
          <td class="text-center"><?= $country_name; ?></td>

            <td class="text-center">
              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal<?= $users['cust_id'] ?>">Change Status</button>

              <!-- Modal -->
              <div id="myModal<?= $users['cust_id'] ?>" class="modal fade" role="dialog" aria-hidden="true" tab-index="1">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Follow-up Remark</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-sm-12 form-group">
                          <div class="col-sm-4 text-right">
                            <h5><strong>Customer Name:</strong></h5>
                          </div>
                          <div class="col-sm-8 text-left">
                            <h5><?= ($users['firstname'] != "NULL")? $users['firstname'] . ' ' . $users['lastname'] :"Not Provided" ?></h5>
                          </div>
                        </div>
                        <div class="col-sm-12 form-group">
                          <div class="col-sm-4 text-right">
                            <h5><strong>Customer Contact:</strong></h5>
                          </div>
                          <div class="col-sm-8 text-left">
                            <h5><?= '+'.$country_details['country_phonecode'].$users['mobile1'] ?></h5>
                          </div>
                        </div>
                        <?php if($users['followup_taken_datetime']!= 'NULL') : ?>
                          <div class="col-sm-12 form-group">
                            <div class="col-sm-4 text-right">
                              <h5><strong>Last Followup Date/Time:</strong></h5>
                            </div>
                            <div class="col-sm-8 text-left">
                              <h5><?= date('d/M/Y H:i:s A',strtotime($users['followup_taken_datetime'])); ?></h5>
                            </div>
                          </div>
                        <?php endif ?>
                        <?php if($users['followup_remark']!= 'NULL' && $users['followup_remark']!= '') : ?>
                          <div class="col-sm-12 form-group">
                            <div class="col-sm-4 text-right">
                              <h5><strong>Last Followup Remark:</strong></h5>
                            </div>
                            <div class="col-sm-8 text-left">
                              <h5><?= $users['followup_remark']; ?></h5>
                            </div>
                          </div>
                        <?php endif ?>
                      </div>
                      <form action="<?= base_url('admin/mark-user-followup'); ?>" method="post" style="display: flex;">
                        <input type="hidden" name="cust_id" value="<?= $users['cust_id'] ?>" />    
                        <input type="hidden" name="type" value="assign" />   
                        <div class="row">
                          <div class="col-sm-12 form-group">
                            <div class="col-sm-4">
                              <h5><strong>Enter Remark</strong></h5>
                            </div>
                            <div class="col-sm-8">
                              <textarea class="form-control" placeholder="Enter Remark" name="followup_remark" id="followup_remark"><?php if($users['followup_remark']!= 'NULL' && $users['followup_remark']!= '') : echo $users['followup_remark']; endif; ?></textarea>
                            </div>
                          </div>
                          
                          <div class="col-sm-12 form-group">
                            <div class="col-sm-4">
                              <h5><strong>Select Status</strong></h5>
                            </div>
                            <div class="col-sm-8 text-left">
                              <label class="radio-inline"><input type="radio" name="followup_status" value="3" class="color-blue" <?= $users['followup_status'] == 3 ? 'checked' : '' ?>> Pending</label>
                              <label class="radio-inline"><input type="radio" name="followup_status" value="4" class="color-blue" <?= $users['followup_status'] == 3 ? 'checked' : '' ?>> Done</label>
                            </div>
                          </div>
                          
                          <div class="col-sm-12 form-group text-right">
                            <div class="col-sm-12">
                              <input type="submit" name="btnAssign" class="btn btn-success" value="Save Followup Details" />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>
              <!-- End Modal -->
            </td>
        </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true,
      "order": [[7,'desc']],
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );
  } );
</script>