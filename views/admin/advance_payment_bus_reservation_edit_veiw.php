<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/advance-payment-bus-reservation'); ?>">Payment Configuration</a>
  </li>
  <li class="active">
    <strong>Edit Configuration</strong>
  </li>
</ol>
<style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Edit Payment Configuration
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/advance-payment-bus-reservation'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/advance-payment-bus-reservation/update'); ?>" class="form-horizontal form-groups-bordered validate" id="adv_pay_add" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="bap_id" value="<?=$payment['bap_id']?>">
          <input type="hidden" name="country_id" value="<?= $payment['country_id'];?>" />
          <div class="border">
            <div class="row">
              <div class="col-md-4">
                <label for="country_id" class="control-label">Select Country </label>                        
                <input type="text" class="form-control" value="<?= $payment['country_name'];?>" readonly/>
                <span id="error_country_id" class="error"></span>                           
              </div>
              <div class="col-md-4">
                <label for="advance_payment" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="advance_payment" placeholder="Enter Percentage" name="advance_payment"  min="1" max="100" value="<?=$payment['advance_payment']?>" />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-4">
                <label for="gonagoo_commission" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="gonagoo_commission" placeholder="Enter percentage" name="gonagoo_commission"  min="1" max="100" value="<?=$payment['gonagoo_commission']?>" />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
            <div class="clear"></div><br />
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="text-center">
                <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Save &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
          
        </form>
        
      </div>
    
    </div>
  
  </div>
</div>

<br />

<script>

  $(function(){

    $("#adv_pay_add").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();

      var country_id = $("#country_id").val();
      var advance_payment = parseFloat($("#advance_payment").val()).toFixed(2);
      var gonagoo_commission = parseFloat($("#gonagoo_commission").val()).toFixed(2);

      if(country_id <= 0 ) {  swal('Error','Please Select Country','warning');   } 
      else if(isNaN(advance_payment)) {  swal('Error','Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
      else if(advance_payment < 0 && advance_payment > 100) {  swal('Error','Advance Payment Percentage is Invalid!','warning');   } 
      
      else if(isNaN(gonagoo_commission)) {  swal('Error','Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
      else if(gonagoo_commission < 0 && gonagoo_commission > 100) {  swal('Error','Gonagoo Commission Percentage is Invalid!','warning');   } 
      
      else {  $("#adv_pay_add")[0].submit();  }

    });
  });
</script>