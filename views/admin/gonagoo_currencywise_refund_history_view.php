<style type="text/css">
  	input.search1 {
	    font-family: FontAwesome;
	    font-style: normal;
	    font-weight: normal;
	    text-decoration: inherit;
	}
	input.search2 {
	    font-family: FontAwesome;
	    font-style: normal;
	    font-weight: normal;
	    text-decoration: inherit;
	}
	input.search3 {
	    font-family: FontAwesome;
	    font-style: normal;
	    font-weight: normal;
	    text-decoration: inherit;
	}
</style>
<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>All Withdrawals List</strong>
  </li>
</ol>

<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>
<div class="rows">
  <div class="col-md-6">
    <h3 style="margin-top: 0px;">All Withdrawals List</h3>
  </div>
</div>  


<div class="col-md-12">  
    
    <ul class="nav nav-tabs right-aligned" style="margin-top: -40px;"><!-- available classes "bordered", "right-aligned" -->
      <li class="active"><a href="#home-2" data-toggle="tab">
          <span class="visible-xs"><i class="entypo-home"></i></span>
          <span class="hidden-xs">Withdraw Request</span>
        </a>
      </li>
      <li>
        <a href="#profile-2" data-toggle="tab">
          <span class="visible-xs"><i class="entypo-user"></i></span>
          <span class="hidden-xs">Accepted Withdrawals</span>
        </a>
      </li>
      <li>
        <a href="#messages-2" data-toggle="tab">
          <span class="visible-xs"><i class="entypo-mail"></i></span>
          <span class="hidden-xs">Rejected Withdrawals</span>
        </a>
      </li>
      <li class="hidden">
        <a href="#settings-2" data-toggle="tab">
          <span class="visible-xs"><i class="entypo-cog"></i></span>
          <span class="hidden-xs">Settings</span>
        </a>
      </li>
    </ul>
    
    <div class="tab-content">

      <div class="tab-pane active" id="home-2">
        <table id="table-1" class="table table-bordered datatable" width="100%">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Date</th>
              <th class="text-center">Request From</th>
              <th class="text-center">Amount</th>
              <th class="text-center">Currency</th>
              <th class="text-center">User Type</th>
              <th class="text-center">Status</th>
              <th class="text-center">Account Type</th>
              <th class="text-center hidden">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($history_request as $withdraw) { ?>
            <tr>
              <td class="text-center"><?= $withdraw['req_id'] ?></td>
              <td class="text-center"><?= date('Y-m-d',strtotime($withdraw['req_datetime'])) ?></td>
              <td class="text-center"><?php if($withdraw['user_type'] == '1') { echo $this->withdraw->get_relay_details($withdraw['user_id']); } else { echo $this->withdraw->get_customer_details($withdraw['user_id']); } ?></td>
              <td class="text-center"><?= $withdraw['amount'] ?></td>
              <td class="text-center"><?= $withdraw['currency_code'] ?></td>
              <td class="text-center"><?= $withdraw['user_type'] == 1 ? 'Relay Point' : 'User' ?></td>
              
              <td class="text-center">
                <?php 
                  if($withdraw['response'] == 'reject') { $status  = 1; echo '<span class="text-danger"> Rejected </span>'; }
                  else if($withdraw['response'] == 'accept') { $status  = 1;  echo '<span class="text-success"> Approved </span>'; }          
                  else { $status  = 0;  echo '<span class="text-warning"> Pending </span>'; }
                ?>    
              </td>
              <td class="text-center"><?= $withdraw['transfer_account_type'] == 'iban' ? 'IBAN' : 'Mobile Money Account'?></td>
              
                <td class="text-center hidden">

                  <?php if($status == 0): ?>
                    
                    <div class="btn-group">
                      <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu dropdown" role="menu">               
                        <li>  
                          <form id="accept_form" action="<?= base_url('admin/accept-withdraw-request') ?>" method="post">
                            <input type="hidden" name="req_id" value="<?= $withdraw['req_id']; ?>" />
                            <a data-toggle="tooltip" data-placement="top" title="Click to accept" data-original-title="Click to activate" style="cursor: pointer; color: inherit; padding: 3px 20px;" onclick="$('#accept_form').get(0).submit();"> 
                              <i class="entypo-check"></i>  Accept 
                            </a>  
                          </form>
                        </li>
                        <li class="divider"></li>
                        <li>  <a data-toggle="tooltip" data-placement="top" title="Click to reject" data-original-title="Click to inactivate" onclick="reject_request('<?= $withdraw['req_id']; ?>');" style="cursor: pointer;"></i> <i class="entypo-cancel"></i> Reject </a>  </li>             
                      </ul>
                    </div>

                  <?php else: ?>
                      <span class="text-success"> DONE </span>
                  <?php endif; ?>

                </td>
            </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Date</th>
              <th class="text-center">Request From</th>
              <th class="text-center">Amount</th>
              <th class="text-center">Country</th>
              <th class="text-center">User Type</th>
              <th class="text-center">Status</th>
              <th class="text-center">Account Type</th>
              <th class="text-center hidden">Actions</th>
            </tr>
          </tfoot>
        </table>
      </div>

      <div class="tab-pane" id="profile-2">
        <table id="table-2" class="table table-bordered datatable" width="100%">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Date</th>
              <th class="text-center">Request From</th>
              <th class="text-center">Amount</th>
              <th class="text-center">Currency</th>
              <th class="text-center">User Type</th>
              <th class="text-center">Status</th>
              <th class="text-center">Response Date / Time</th>
              <th class="text-center">Account Type</th>
              <th class="text-center hidden">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($history_accept as $withdraw) { ?>
            <tr>
              <td class="text-center"><?= $withdraw['req_id'] ?></td>
              <td class="text-center"><?= date('Y-m-d',strtotime($withdraw['req_datetime'])) ?></td>
              <td class="text-center"><?php if($withdraw['user_type'] == '1') { echo $this->withdraw->get_relay_details($withdraw['user_id']); } else { echo $this->withdraw->get_customer_details($withdraw['user_id']); } ?></td>
              <td class="text-center"><?= $withdraw['amount'] ?></td>
              <td class="text-center"><?= $withdraw['currency_code'] ?></td>
              <td class="text-center"><?= $withdraw['user_type'] == 1 ? 'Relay Point' : 'User' ?></td>
              
              <td class="text-center">
                <?php 
                  if($withdraw['response'] == 'reject') { $status  = 1; echo '<span class="text-danger"> Rejected </span>'; }
                  else if($withdraw['response'] == 'accept') { $status  = 1;  echo '<span class="text-success"> Approved </span>'; }          
                  else { $status  = 0;  echo '<span class="text-warning"> Pending </span>'; }
                ?>    
              </td>
              <td class="text-center">
                <?php
                  if($withdraw['response_datetime'] == 'NULL') { echo 'NA'; }
                  else { echo date('d/m/Y h:i:s a', strtotime($withdraw['response_datetime'])); }
                  ?>
              </td>
              <td class="text-center"><?= $withdraw['transfer_account_type'] == 'iban' ? 'IBAN' : 'Mobile Money Account'?></td>
              
                <td class="text-center hidden">

                  <?php if($status == 0): ?>
                    
                    <div class="btn-group">
                      <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu dropdown" role="menu">               
                        <li>  
                          <form id="accept_form" action="<?= base_url('admin/accept-withdraw-request') ?>" method="post">
                            <input type="hidden" name="req_id" value="<?= $withdraw['req_id']; ?>" />
                            <a data-toggle="tooltip" data-placement="top" title="Click to accept" data-original-title="Click to activate" style="cursor: pointer; color: inherit; padding: 3px 20px;" onclick="$('#accept_form').get(0).submit();"> 
                              <i class="entypo-check"></i>  Accept 
                            </a>  
                          </form>
                        </li>
                        <li class="divider"></li>
                        <li>  <a data-toggle="tooltip" data-placement="top" title="Click to reject" data-original-title="Click to inactivate" onclick="reject_request('<?= $withdraw['req_id']; ?>');" style="cursor: pointer;"></i> <i class="entypo-cancel"></i> Reject </a>  </li>             
                      </ul>
                    </div>

                  <?php else: ?>
                      <span class="text-success"> DONE </span>
                  <?php endif; ?>

                </td>
            </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Date</th>
              <th class="text-center">Request From</th>
              <th class="text-center">Amount</th>
              <th class="text-center">Country</th>
              <th class="text-center">User Type</th>
              <th class="text-center">Status</th>
              <th class="text-center">Response Date / Time</th>
              <th class="text-center">Account Type</th>
              <th class="text-center hidden">Actions</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <div class="tab-pane" id="messages-2">
        <table id="table-3" class="table table-bordered datatable" width="100%">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Date</th>
              <th class="text-center">Request From</th>
              <th class="text-center">Amount</th>
              <th class="text-center">Currency</th>
              <th class="text-center">User Type</th>
              <th class="text-center">Status</th>
              <th class="text-center">Response Date / Time</th>
              <th class="text-center">Account Type</th>
              <th class="text-center hidden">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($history_reject as $withdraw) { ?>
            <tr>
              <td class="text-center"><?= $withdraw['req_id'] ?></td>
              <td class="text-center"><?= date('Y-m-d',strtotime($withdraw['req_datetime'])) ?></td>
              <td class="text-center"><?php if($withdraw['user_type'] == '1') { echo $this->withdraw->get_relay_details($withdraw['user_id']); } else { echo $this->withdraw->get_customer_details($withdraw['user_id']); } ?></td>
              <td class="text-center"><?= $withdraw['amount'] ?></td>
              <td class="text-center"><?= $withdraw['currency_code'] ?></td>
              <td class="text-center"><?= $withdraw['user_type'] == 1 ? 'Relay Point' : 'User' ?></td>
              
              <td class="text-center">
                <?php 
                  if($withdraw['response'] == 'reject') { $status  = 1; echo '<span class="text-danger"> Rejected </span>'; }
                  else if($withdraw['response'] == 'accept') { $status  = 1;  echo '<span class="text-success"> Approved </span>'; }          
                  else { $status  = 0;  echo '<span class="text-warning"> Pending </span>'; }
                ?>    
              </td>
              <td class="text-center">
                <?php
                  if($withdraw['response_datetime'] == 'NULL') { echo 'NA'; }
                  else { echo date('d/m/Y h:i:s a', strtotime($withdraw['response_datetime'])); }
                  ?>
              </td>
              <td class="text-center"><?= $withdraw['transfer_account_type'] == 'iban' ? 'IBAN' : 'Mobile Money Account'?></td>
              
                <td class="text-center hidden">

                  <?php if($status == 0): ?>
                    
                    <div class="btn-group">
                      <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu dropdown" role="menu">               
                        <li>  
                          <form id="accept_form" action="<?= base_url('admin/accept-withdraw-request') ?>" method="post">
                            <input type="hidden" name="req_id" value="<?= $withdraw['req_id']; ?>" />
                            <a data-toggle="tooltip" data-placement="top" title="Click to accept" data-original-title="Click to activate" style="cursor: pointer; color: inherit; padding: 3px 20px;" onclick="$('#accept_form').get(0).submit();"> 
                              <i class="entypo-check"></i>  Accept 
                            </a>  
                          </form>
                        </li>
                        <li class="divider"></li>
                        <li>  <a data-toggle="tooltip" data-placement="top" title="Click to reject" data-original-title="Click to inactivate" onclick="reject_request('<?= $withdraw['req_id']; ?>');" style="cursor: pointer;"></i> <i class="entypo-cancel"></i> Reject </a>  </li>             
                      </ul>
                    </div>

                  <?php else: ?>
                      <span class="text-success"> DONE </span>
                  <?php endif; ?>

                </td>
            </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Date</th>
              <th class="text-center">Request From</th>
              <th class="text-center">Amount</th>
              <th class="text-center">Country</th>
              <th class="text-center">User Type</th>
              <th class="text-center">Status</th>
              <th class="text-center">Response Date / Time</th>
              <th class="text-center">Account Type</th>
              <th class="text-center hidden">Actions</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <div class="tab-pane hidden" id="settings-2">
        Settings Tab
      </div>
    </div>
    
    <br />
</div>




  
  

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
      var $table1 = jQuery("#table-1");
      
      var table1 = $table1.DataTable( {
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      } );
      
      // Initalize Select Dropdown after DataTables is created
      $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
        minimumResultsForSearch: -1
      });
      
      // Setup - add a text input to each footer cell
      $( '#table-1 tfoot th' ).each( function () {
        var title = $('#table-1 thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control search1" placeholder="&#xf002;" />' );
      } );
      
      // Apply the search
      table1.columns().every( function () {
        var that = this;
      
        $( 'input', this.footer() ).on( 'keyup change', function () {
          if ( that.search() !== this.value ) {
            that
              .search( this.value )
              .draw();
          }
        } );
      } );

      var $table2 = jQuery("#table-2");
      
      var table2 = $table2.DataTable( {
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      } );
      
      // Initalize Select Dropdown after DataTables is created
      $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
        minimumResultsForSearch: -1
      });
      
      // Setup - add a text input to each footer cell
      $( '#table-2 tfoot th' ).each( function () {
        var title = $('#table-2 thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control search2" placeholder="&#xf002;" />' );
      } );
      
      // Apply the search
      table2.columns().every( function () {
        var that = this;
      
        $( 'input', this.footer() ).on( 'keyup change', function () {
          if ( that.search() !== this.value ) {
            that
              .search( this.value )
              .draw();
          }
        } );
      } );

      var $table3 = jQuery("#table-3");
      
      var table3 = $table3.DataTable( {
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      } );
      
      // Initalize Select Dropdown after DataTables is created
      $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
        minimumResultsForSearch: -1
      });
      
      // Setup - add a text input to each footer cell
      $( '#table-3 tfoot th' ).each( function () {
        var title = $('#table-3 thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control search3" placeholder="&#xf002;" />' );
      } );
      
      // Apply the search
      table3.columns().every( function () {
        var that = this;
      
        $( 'input', this.footer() ).on( 'keyup change', function () {
          if ( that.search() !== this.value ) {
            that
              .search( this.value )
              .draw();
          }
        } );
      } );

    } );

$('.search1').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('search1');
    } else {
        input.removeClass('search1');
    }
});
$('.search2').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('search2');
    } else {
        input.removeClass('search2');
    }
});
$('.search3').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('search3');
    } else {
        input.removeClass('search3');
    }
});

</script>