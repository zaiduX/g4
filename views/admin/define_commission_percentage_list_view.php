<style>
  .addRate:hover{
    text-decoration: none !important;
    background-color: #ebebeb !important;
  }
</style>
<ol class="breadcrumb bc-3" style="padding-bottom: 0px; padding-top: 0px;">
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Gonagoo Commission</strong>
  </li>
</ol>   

<?php 
  $user_details = $this->users->get_users_detail((int)$cust_id);
  //echo json_encode($user_details);
  if($user_details['firstname'] != 'NULL') {
    $full_name = $user_details['firstname'] . ' ' . $user_details['lastname'];
  } else {
    $email_cut = explode('@', $user_details['email1']);  
    $full_name = $email_cut[0];
  }
?>
<div class="row">
  <div class="col-sm-8">
    <h3>Gonagoo Commission - <?=$full_name?></h3>
  </div>
  <div class="col-sm-4"><br />
    <a href="<?= base_url('admin/users'); ?>" class="btn btn-info"><i class="fa fa-users"></i> Users</a>
    <div class="btn-group left-dropdown pull-right">
      <button type="button" class="btn btn-green dropdown-toggle btn-icon icon-left" data-toggle="dropdown">
        <i class="fa fa-plus right"></i>  Add Gonagoo Commission | <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li>
          <form action="<?= base_url('admin/users/add-define-commission-percentage'); ?>" method="post">
            <input type="hidden" name="cust_id" value="<?=(int)$cust_id?>">
            <input type="submit" class="btn btn-link addRate" style="float: right;" value="Percentage Based Commission">
          </form>
        </li>
        <li>
          <form action="<?= base_url('admin/users/add-define-commission-rate'); ?>" method="post">
            <input type="hidden" name="cust_id" value="<?=(int)$cust_id?>">
            <input type="submit" class="btn btn-link addRate" style="float: right;" value="Absolute Rate Commission&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
          </form>
        </li>
      </ul>
    </div>
  </div>
</div>

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Category</th>
      <th class="text-center">Country</th>
      <th class="text-center">Commission Type</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>
  
  <tbody>
    <?php  foreach ($commission_percentage as $pay):  ?>
    <tr>
      <td class="text-center"><?= $pay['pay_id']; ?></td>
      <td class="text-center"><?= $pay['cat_name']; ?></td>
      <td class="text-center"><?= $pay['country_name']; ?></td>
      <td class="text-center"><?= ($pay['rate_type'] == 0)?'% Based':'Absolute Rate' ?></td>
      <td class="text-center">
        <?php if($pay['rate_type'] == 0) { ?>
          <a href="<?= base_url('admin/users/edit-commission-rate/').$pay['pay_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" data-original-title="Click to View Full Details & Edit">
            <i class="entypo-eye"></i> View &amp; Edit 
          </a> &nbsp;
        <?php } else { ?>
          <a href="<?= base_url('admin/users/edit-commission-rate/').$pay['pay_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" data-original-title="Click to View Full Details & Edit">
            <i class="entypo-eye"></i> View &amp; Edit 
          </a> &nbsp;
        <?php } ?>
        <button class="btn btn-danger btn-sm btn-icon icon-left"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_commission_rate('<?= $pay["pay_id"]; ?>');"> <i class="entypo-cancel"></i> Delete 
        </button>  
      </td>        
      <?php endforeach; ?>
    </tr>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "order" : [[0, "desc"]]
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );    
  } );

  function delete_commission_rate(id=0){    
    swal({
      title: 'Are you sure?',
      text: "You want to to delete this configuration?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post("<?=base_url('admin/users/delete-user-commission')?>", { pay_id: id }, function(res){ 
        console.log(res);
        if(res == 'success') { 
          swal(
            'Deleted!',
            'Configuration has been deleted.',
            'success'
          ). then(function() {   window.location.reload();  });
        } else {
          swal(
            'Failed!',
            'Configuration deletion failed.',
            'error'
          )
        }
      });
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

</script>