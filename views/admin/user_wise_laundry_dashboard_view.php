<div class="row">
	<form action="<?= base_url('admin/user-wise-laundry-dashboard'); ?>" method="POST" id="frmApplyFilter">
		<div class="col-sm-2 col-xs-6">
			<br />
			<label>Filter Dashboard</label>
		</div>
		<div class="col-sm-4 col-xs-6">
			<label for="country_id" class="control-label">Select Country </label> 
			<select id="country_id" name="country_id" class="form-control select2">
			<option value="0">Select Country</option>
			<?php foreach ($countries as $country): ?> 
			  <option value="<?= $country['country_id'] ?>" <?php if(isset($country_id) &&  $country['country_id'] == $country_id) { echo 'selected'; } ?>><?= $country['country_name']; ?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="col-sm-2 col-xs-6">
			<br />
			<button type="submit" id="btnFilter" class="btn btn-info">Apply</button>
			<a href="<?=base_url('admin/user-wise-laundry-dashboard')?>" id="btnReset" class="btn btn-warning">Clear</a>
		</div>
	</form>
</div>
<hr />
<div style="border: 1px solid #f56954; padding: 10px;">
	<div class="row">
	  <div class="col-md-12">
	    <h3 style="margin-top: 0px;">Providers Analysis View</h3>
	  </div>
	</div> 
	<div class="row">
	    <div class="col-md-12">
	        <section class="profile-info-tabs">
	        	<div class="row">
		          	<div class="col-sm-12" style="margin-top: -15px">
		            	<ul class="nav nav-tabs">
							<li class="active tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Provider with Total Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#all_orders_provider">Total Bookings</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Providers with Accepted Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#accepted_orders_provider">Accepted Bookings</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Providers with In-progress Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#in_progress_orders_provider">In-progress Bookings</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Providers with Completed Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#completed_orders_provider">Completed Bookings</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Providers with Cancelled Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#cancelled_orders_provider">Cancelled Bookings</a>
							</li>
			            </ul>
			        </div>
	        	</div>
	    	</section>

			<div class="tab-content">
				
				<div id="all_orders_provider" class="tab-pane fade in active">
					<section class="profile-feed col-md-12">
						<div class="profile-stories">
						  	<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
						  		<?php //echo json_encode($expiredBookingProviders); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="expiredBookingProvidersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Provider Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($all_orders_provider as $ebp):  ?>
							        	<tr>
								          	<td class="text-center"><?= $ebp['provider_id'] ?></td>
								          	<td class="text-center"><?= $ebp['company_name'] . ' [' . $ebp['firstname'] . ' ' . $ebp['lastname'] . ']' ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($ebp['country_id']).' '.$ebp['contact_no'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($ebp['country_id']) ?></td>
								          	<td class="text-center"><?= $ebp['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($all_orders_provider) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-total-bookings-providers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($all_orders_provider) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
						</div>  
					</section>
				</div>

				<div id="accepted_orders_provider" class="tab-pane fade">
				 	<section class="profile-feed col-md-12">
					    <div class="profile-stories">
							<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($rejectedBookingProviders); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="rejectedBookingProvidersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Provider Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($accepted_orders_provider as $rbp):  ?>
							        	<tr>
								          	<td class="text-center"><?= $rbp['provider_id'] ?></td>
								          	<td class="text-center"><?= $rbp['company_name'] . ' [' . $rbp['firstname'] . ' ' . $rbp['lastname'] . ']' ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($rbp['country_id']).' '.$rbp['contact_no'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($rbp['country_id']) ?></td>
								          	<td class="text-center"><?= $rbp['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($accepted_orders_provider) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-accepted-bookings-providers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($accepted_orders_provider) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
							</article>
					    </div>  
				  	</section>
				</div>

				<div id="in_progress_orders_provider" class="tab-pane fade">
				 	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($acceptedBookingProviders); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="acceptedBookingProvidersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Provider Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($in_progress_orders_provider as $abp):  ?>
							        	<tr>
								          	<td class="text-center"><?= $abp['provider_id'] ?></td>
								          	<td class="text-center"><?= $abp['company_name'] . ' [' . $abp['firstname'] . ' ' . $abp['lastname'] . ']' ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($abp['country_id']).' '.$abp['contact_no'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($abp['country_id']) ?></td>
								          	<td class="text-center"><?= $abp['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($in_progress_orders_provider) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-in-progress-bookings-providers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($in_progress_orders_provider) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
							</article>
				    	</div>  
				  	</section>
				</div>

				<div id="completed_orders_provider" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($deliveredBookingProviders); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="deliveredBookingProvidersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Provider Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($completed_orders_provider as $dbp):  ?>
							        	<tr>
								          	<td class="text-center"><?= $dbp['provider_id'] ?></td>
								          	<td class="text-center"><?= $dbp['company_name'] . ' [' . $dbp['firstname'] . ' ' . $dbp['lastname'] . ']' ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($dbp['country_id']).' '.$dbp['contact_no'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($dbp['country_id']) ?></td>
								          	<td class="text-center"><?= $dbp['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($completed_orders_provider) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-completed-bookings-providers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($completed_orders_provider) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
							</article>
				    	</div>  
				  	</section>
				</div>

				<div id="cancelled_orders_provider" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($deliveredBookingProviders); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="deliveredBookingProvidersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Provider Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($cancelled_orders_provider as $dbp):  ?>
							        	<tr>
								          	<td class="text-center"><?= $dbp['provider_id'] ?></td>
								          	<td class="text-center"><?= $dbp['company_name'] . ' [' . $dbp['firstname'] . ' ' . $dbp['lastname'] . ']' ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($dbp['country_id']).' '.$dbp['contact_no'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($dbp['country_id']) ?></td>
								          	<td class="text-center"><?= $dbp['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($cancelled_orders_provider) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-cancelled-bookings-providers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($cancelled_orders_provider) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
							</article>
				    	</div>  
				  	</section>
				</div>

			</div>
	    </div>
	</div>
</div>
<br />
<div style="border: 1px solid #0073b7; padding: 10px;">
	<div class="row">
	  <div class="col-md-12">
	    <h3 style="margin-top: 0px;">Customer Analysis View</h3>
	  </div>
	</div> 
	<div class="row">
	    <div class="col-md-12">
	        <section class="profile-info-tabs">
	        	<div class="row">
		          	<div class="col-sm-12" style="margin-top: -15px">
		            	<ul class="nav nav-tabs">
							<li class="active tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Customer with Total Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#all_orders_customer">Total Bookings</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Customer with Accepted Bookings">
							<a data-toggle="tab" style="padding: 6px 20px;" href="#accepted_orders_customer">Accepted Bookings</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Customer with In-progress Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#in_progress_orders_customer">In-progress Bookings</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Customer with Completed Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#completed_orders_customer">Completed Bookings</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top Customer with Cancelled Bookings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#cancelled_orders_customer">Cancelled Bookings</a>
							</li>
			            </ul>
			        </div>
	        	</div>
	    	</section>

			<div class="tab-content">
				
				<div id="all_orders_customer" class="tab-pane fade in active">
					<section class="profile-feed col-md-12">
						<div class="profile-stories">
						  	<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($createdBookingCustomer); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="createdBookingCustomerTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Operator Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($all_orders_customer as $crbc): $email_cut = explode('@', $crbc['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $crbc['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($crbc['company_name'] != 'NULL') { 
		          									echo $crbc['company_name'];
		          									if($crbc['firstname'] != 'NULL' && $crbc['lastname'] != 'NULL') {
		          										echo ' [' . $crbc['firstname'] . ' ' . $crbc['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($crbc['country_id']).' '.$crbc['mobile1'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($crbc['country_id']) ?></td>
								          	<td class="text-center"><?= $crbc['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($all_orders_customer) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-total-bookings-customers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($all_orders_customer) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
						</div>  
					</section>
				</div>

				<div id="accepted_orders_customer" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($expiredBookingCustomer); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="expiredBookingCustomerTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Operator Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($accepted_orders_customer as $exbc): $email_cut = explode('@', $exbc['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $exbc['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($exbc['company_name'] != 'NULL') { 
		          									echo $exbc['company_name'];
		          									if($exbc['firstname'] != 'NULL' && $exbc['lastname'] != 'NULL') {
		          										echo ' [' . $exbc['firstname'] . ' ' . $exbc['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($exbc['country_id']).' '.$exbc['mobile1'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($exbc['country_id']) ?></td>
								          	<td class="text-center"><?= $exbc['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($accepted_orders_customer) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-accepted-bookings-customers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($accepted_orders_customer) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>  
				  	</section>
				</div>

				<div id="in_progress_orders_customer" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($rejectedBookingCustomer); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="rejectedBookingCustomerTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Operator Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($in_progress_orders_customer as $rjbc): $email_cut = explode('@', $rjbc['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $rjbc['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($rjbc['company_name'] != 'NULL') { 
		          									echo $rjbc['company_name'];
		          									if($rjbc['firstname'] != 'NULL' && $rjbc['lastname'] != 'NULL') {
		          										echo ' [' . $rjbc['firstname'] . ' ' . $rjbc['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($rjbc['country_id']).' '.$rjbc['mobile1'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($exbc['country_id']) ?></td>
								          	<td class="text-center"><?= $rjbc['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($in_progress_orders_customer) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-in-progress-bookings-customers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($in_progress_orders_customer) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>  
				  	</section>
				</div>

				<div id="completed_orders_customer" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($acceptedBookingCustomer); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="acceptedBookingCustomerTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Operator Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($completed_orders_customer as $acbc): $email_cut = explode('@', $acbc['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $acbc['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($acbc['company_name'] != 'NULL') { 
		          									echo $acbc['company_name'];
		          									if($acbc['firstname'] != 'NULL' && $acbc['lastname'] != 'NULL') {
		          										echo ' [' . $acbc['firstname'] . ' ' . $acbc['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($acbc['country_id']).' '.$acbc['mobile1'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($exbc['country_id']) ?></td>
								          	<td class="text-center"><?= $acbc['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($completed_orders_customer) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-completed-bookings-customers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($completed_orders_customer) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>  
				  	</section>
				</div>

				<div id="cancelled_orders_customer" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($acceptedBookingCustomer); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="acceptedBookingCustomerTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Operator Country</th>
											<th class="text-center">No. of Bookings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($cancelled_orders_customer as $acbc): $email_cut = explode('@', $acbc['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $acbc['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($acbc['company_name'] != 'NULL') { 
		          									echo $acbc['company_name'];
		          									if($acbc['firstname'] != 'NULL' && $acbc['lastname'] != 'NULL') {
		          										echo ' [' . $acbc['firstname'] . ' ' . $acbc['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($acbc['country_id']).' '.$acbc['mobile1'] ?></td>
								          	<td class="text-center"><?= $this->admin->get_country_name_by_id($exbc['country_id']) ?></td>
								          	<td class="text-center"><?= $acbc['userCount'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($cancelled_orders_customer) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-laundry-cancelled-bookings-customers'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($cancelled_orders_customer) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>  
				  	</section>
				</div>

			</div>
	    </div>
	</div>
</div>
<br />
<div style="border: 1px solid #00a65a; padding: 10px;">
	<div class="row">
	  <div class="col-md-12">
	    <h3 style="margin-top: 0px;">RFM (Regency, Frequency, Monetary) View</h3>
	  </div>
	</div> 
	<div class="row">
	    <div class="col-md-12">
	        <section class="profile-info-tabs">
	        	<div class="row">
		          	<div class="col-sm-12" style="margin-top: -15px">
		            	<ul class="nav nav-tabs">
							<li class="active tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users Recently Connected">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#recentlyConnectedUsers">Recently Connects</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="User Not Connected">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#neverConnectedUsers">Never Connects</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users Frequently Connected">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#frequentlyConnectedUsers">Frequently Connects</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users with High Money Spent">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#HighestMoneySpentUsers">High Money Spent</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users with Low Money Spent">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#lowestMoneySpentUsers">Low Money Spent</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users with Best Ratings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#bestRatingUsers">Best Rating</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users with Wors Ratings">
							<a data-toggle="tab" style="padding: 5px 20px;" href="#lowestRatingUsers">Low Rating</a>
							</li>
			            </ul>
			        </div>
	        	</div>
	    	</section>

			<div class="tab-content">
				
				<div id="recentlyConnectedUsers" class="tab-pane fade in active">
					<section class="profile-feed col-md-12">
						<div class="profile-stories">
						  	<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($recentlyConnectedUsers); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="recentlyConnectedUsersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Connect On</th>
											<th class="text-center">Platform</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($recentlyConnectedUsers as $rcu):  $email_cut = explode('@', $rcu['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $rcu['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($rcu['company_name'] != 'NULL') { 
		          									echo $rcu['company_name'];
		          									if($rcu['firstname'] != 'NULL' && $rcu['lastname'] != 'NULL') {
		          										echo ' [' . $rcu['firstname'] . ' ' . $rcu['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($rcu['country_id']).' '.$rcu['mobile1'] ?></td>
								          	<td class="text-center"><?= $rcu['cre_datetime'] ?></td>
								          	<td class="text-center"><?= $rcu['user_agent'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($recentlyConnectedUsers) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-recently-connected-users'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($recentlyConnectedUsers) <= 4) ? '':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
						</div>  
					</section>
				</div>

				<div id="neverConnectedUsers" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($neverConnectedUsers); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="neverConnectedUsersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Registered On</th>
											<th class="text-center">Registered From</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($neverConnectedUsers as $ncu):  $email_cut = explode('@', $ncu['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $ncu['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($ncu['company_name'] != 'NULL') { 
		          									echo $ncu['company_name'];
		          									if($ncu['firstname'] != 'NULL' && $ncu['lastname'] != 'NULL') {
		          										echo ' [' . $ncu['firstname'] . ' ' . $ncu['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($ncu['country_id']).' '.$ncu['mobile1'] ?></td>
								          	<td class="text-center"><?= $ncu['cre_datetime'] ?></td>
								          	<td class="text-center"><?php if($ncu['social_type'] == 'LI') echo 'Linkedin'; else if($ncu['social_type'] == 'FB') echo 'Facebook'; else echo 'Manual'; ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($neverConnectedUsers) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-never-connected-users'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($neverConnectedUsers) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>  
				  	</section>
				</div>

				<div id="frequentlyConnectedUsers" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($frequentlyConnectedUsers); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="frequentlyConnectedUsersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Connect On</th>
											<th class="text-center">Platform</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($frequentlyConnectedUsers as $fcu):  $email_cut = explode('@', $fcu['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $fcu['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($fcu['company_name'] != 'NULL') { 
		          									echo $fcu['company_name'];
		          									if($fcu['firstname'] != 'NULL' && $fcu['lastname'] != 'NULL') {
		          										echo ' [' . $fcu['firstname'] . ' ' . $fcu['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($fcu['country_id']).' '.$fcu['mobile1'] ?></td>
								          	<td class="text-center"><?= $fcu['cre_datetime'] ?></td>
								          	<td class="text-center"><?= $fcu['user_agent'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($frequentlyConnectedUsers) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-frequently-connected-users'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($frequentlyConnectedUsers) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				   		</div>  
				  	</section>
				</div>

				<div id="HighestMoneySpentUsers" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;">
				      			<div class="row">
				      				<div class="col-sm-2">
				      					<label class="form-label">Select Currency</label>
				      				</div>
				      				<div class="col-sm-4">
						      			<select name="currency_code" class="form-control select2" id="select2-high">
						      				<option value="">Select Currency</option>
						      				<?php foreach ($currencies as $cr) { ?>
						      					<option value="<?=$cr['currency_sign']?>"><?=$cr['currency_sign'].' - '.$cr['currency_title']?></option>
						      				<?php } ?>
						      			</select>
				      				</div>
				      				<div class="col-sm-6">
						      			&nbsp;
				      				</div>
				      			</div> 
							    <?php //echo json_encode($frequentlyConnectedUsers); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="HighestMoneySpentUsersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">Accoount ID</th>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Balance</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<tr><td class="text-center" colspan="5">Select Currency!</td></tr>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-highest-money-spent-users'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($frequentlyConnectedUsers) <= 4) ? 'hidden':''; ?>">
							    	<input type="hidden" name="currency_cd_high" id="currency_cd_high" value="all">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>  
				  	</section>
				</div>

				<div id="lowestMoneySpentUsers" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <div class="row">
				      				<div class="col-sm-2">
				      					<label class="form-label">Select Currency</label>
				      				</div>
				      				<div class="col-sm-4">
						      			<select name="currency_code" class="form-control select2" id="select2-low">
						      				<option value="">Select Currency</option>
						      				<?php foreach ($currencies as $cr) { ?>
						      					<option value="<?=$cr['currency_sign']?>"><?=$cr['currency_sign'].' - '.$cr['currency_title']?></option>
						      				<?php } ?>
						      			</select>
				      				</div>
				      				<div class="col-sm-6">
						      			&nbsp;
				      				</div>
				      			</div> 
							    <?php //echo json_encode($frequentlyConnectedUsers); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="lowestMoneySpentUsersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">Accoount ID</th>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Balance</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<tr><td class="text-center" colspan="5">Select Currency!</td></tr>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-lowest-money-spent-users'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($frequentlyConnectedUsers) <= 4) ? 'hidden':''; ?>">
							    	<input type="hidden" name="currency_cd_low" id="currency_cd_low" value="all">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>  
				 	</section>
				</div>

				<div id="bestRatingUsers" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($bestRatingUsers); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="bestRatingUsersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Ratings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($bestRatingUsers as $bru):  $email_cut = explode('@', $bru['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $bru['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($bru['company_name'] != 'NULL') { 
		          									echo $bru['company_name'];
		          									if($bru['firstname'] != 'NULL' && $bru['lastname'] != 'NULL') {
		          										echo ' [' . $bru['firstname'] . ' ' . $bru['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($bru['country_id']).' '.$bru['mobile1'] ?></td>
								          	<td class="text-center"><?= $bru['ratings'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($bestRatingUsers) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-best-rating-users'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($bestRatingUsers) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>  
				  	</section>
				</div>

				<div id="lowestRatingUsers" class="tab-pane fade">
				  	<section class="profile-feed col-md-12">
				    	<div class="profile-stories">
				      		<article class="story" style="margin-top: 0px; margin-bottom: 0px;"> 
							    <?php //echo json_encode($lowestRatingUsers); 
						  			//echo $this->db->last_query();
						  		?>
							    <table class="table display compact" id="lowestRatingUsersTable">
							      	<thead>
							        	<tr>
											<th class="text-center">User ID</th>
											<th class="text-center">Company [User Name]</th>
											<th class="text-center">Contact No</th>
											<th class="text-center">Ratings</th>
								        </tr>
							      	</thead>
							      	<tbody>
							        	<?php foreach ($lowestRatingUsers as $lru):  $email_cut = explode('@', $lru['email1']); ?>
							        	<tr>
								          	<td class="text-center"><?= $lru['cust_id'] ?></td>
								          	<td class="text-center">
								          		<?php 
		          								if($lru['company_name'] != 'NULL') { 
		          									echo $lru['company_name'];
		          									if($lru['firstname'] != 'NULL' && $lru['lastname'] != 'NULL') {
		          										echo ' [' . $lru['firstname'] . ' ' . $lru['lastname'] . ']';
		          									} else { echo ' [' . $email_cut[0] . ']'; }
		          								} else { echo $email_cut[0]; } 
		          								?>
								          	</td>
								          	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($lru['country_id']).' '.$lru['mobile1'] ?></td>
								          	<td class="text-center"><?= $lru['ratings'] ?></td>
								        </tr>
							        	<?php endforeach; ?>
							        	<?php if(sizeof($lowestRatingUsers) <= 0) { ?>
							        		<tr><td class="text-center" colspan="4">No Data Found!</td></tr>
							        	<?php } ?>
							      	</tbody>
							    </table>
							    <form action="<?= $this->config->item('base_url') . 'admin/dashboard-lowest-rating-users'; ?>" method="POST" style="float: right; margin-top: -20px; margin-bottom: -15px; display: flex;" class="<?=(sizeof($lowestRatingUsers) <= 4) ? 'hidden':''; ?>">
							      	<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-list-alt"></i> More...</button></h3>
						    	</form>
						  	</article>
				    	</div>
				  	</section>
				</div>

			</div>
	    </div>
	</div>
</div>

<script>
	$("#select2-high").on('change', function(e) {
	    var currency_code = $(this).select2().val();
	    $("#currency_cd_high").val(currency_code);
	    //console.log(currency_code);
		var country_id = $('#country_id').val();		
	    $.ajax({
            type: "POST", 
            url: "get-user-by-currency", 
            data: { currency_code:currency_code, country_id:country_id, rows:5, type: 'high' },
            dataType: "json",
            success: function(res){ 
            	//console.log(res);
            	if ($.trim(res) == "undefined" || $.trim(res) == ""){ 
            		$('#HighestMoneySpentUsersTable').append('<thead><tr><th class="text-center" collspan="5">No Data Found!</th></tr></<thead><tbody>');
            	} else {
	        		$('#HighestMoneySpentUsersTable').append('<thead><tr><th class=text-center>Accoount ID</th><th class=text-center>User ID</th><th class=text-center>Company [User Name]</th><th class=text-center>Contact No</th><th class=text-center>Balance</th></tr></<thead><tbody>');
	            	$.each( res, function(i, v){
	            		var user_name = '';
	            		var email_cut = '';
	            		var country_phone_code = '';
	            		var cid = v['country_id'];
		        		email_cut = v['email1'].substring(0, v['email1'].indexOf("@"));
		        		if(v['company_name'] != 'NULL') { 
							user_name = v['company_name'];
							if(v['firstname'] != 'NULL' && v['lastname'] != 'NULL') {
								user_name += ' [' + v['firstname'] + ' ' + v['lastname'] + ']';
							} else { user_name = ' [' + email_cut + ']'; }
						} else { user_name = email_cut; }
						var code = '';
						$.ajax({
				            type: "POST", 
				            async: false,
				            url: "get-country-phone-code-by-id", 
				            data: { country_code:cid },
				            dataType: "json",
				            success: function(res){
				            	code = res;
				            }
				        });

		              	$('#HighestMoneySpentUsersTable').append("<tr><td class=text-center>"+v['account_id']+"</td><td class=text-center>"+v['cust_id']+"</td><td class=text-center>"+user_name+"</td><td class=text-center>"+code+' '+v['mobile1']+"</td><td class=text-center>"+v['account_balance']+"</td></tr>");
		            });
		        }
	            $('#HighestMoneySpentUsersTable').append('</tbody>');
            },
            beforeSend: function(){
              	$('#HighestMoneySpentUsersTable').empty();
            },
            error: function(msg){
              	$('#HighestMoneySpentUsersTable').empty();
            }
        }); 
	});
</script>

<script>
	$("#select2-low").on('change', function(e) {
	    var currency_code = $(this).select2().val();
	    $("#currency_cd_low").val(currency_code);
	    //console.log(currency_code);
		var country_id = $('#country_id').val();
	    $.ajax({
            type: "POST", 
            url: "get-user-by-currency", 
            data: { currency_code:currency_code, country_id:country_id, rows:5, type: 'low' },
            dataType: "json",
            success: function(res){ 
            	//console.log(res);
            	if ($.trim(res) == "undefined" || $.trim(res) == ""){ 
            		$('#lowestMoneySpentUsersTable').append('<thead><tr><th class="text-center" collspan="5">No Data Found!</th></tr></<thead><tbody>');
            	} else {
	        		$('#lowestMoneySpentUsersTable').append('<thead><tr><th class=text-center>Accoount ID</th><th class=text-center>User ID</th><th class=text-center>Company [User Name]</th><th class=text-center>Contact No</th><th class=text-center>Balance</th></tr></<thead><tbody>');
	            	$.each( res, function(i, v){
	            		var user_name = '';
	            		var email_cut = '';
	            		var country_phone_code = '';
	            		var cid = v['country_id'];
		        		email_cut = v['email1'].substring(0, v['email1'].indexOf("@"));
		        		if(v['company_name'] != 'NULL') { 
							user_name = v['company_name'];
							if(v['firstname'] != 'NULL' && v['lastname'] != 'NULL') {
								user_name += ' [' + v['firstname'] + ' ' + v['lastname'] + ']';
							} else { user_name = ' [' + email_cut + ']'; }
						} else { user_name = email_cut; }
						var code = '';
						$.ajax({
				            type: "POST", 
				            async: false,
				            url: "get-country-phone-code-by-id", 
				            data: { country_code:cid },
				            dataType: "json",
				            success: function(res){
				            	code = res;
				            }
				        });

		              	$('#lowestMoneySpentUsersTable').append("<tr><td class=text-center>"+v['account_id']+"</td><td class=text-center>"+v['cust_id']+"</td><td class=text-center>"+user_name+"</td><td class=text-center>"+code+' '+v['mobile1']+"</td><td class=text-center>"+v['account_balance']+"</td></tr>");
		            });
		        }
	            $('#lowestMoneySpentUsersTable').append('</tbody>');
            },
            beforeSend: function(){
              	$('#lowestMoneySpentUsersTable').empty();
            },
            error: function(msg){
              	$('#lowestMoneySpentUsersTable').empty();
            }
        }); 
	});
</script>