<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="Gonagoo Admin Panel" />
  <meta name="author" content="" />

  <link rel="icon" href="<?= $this->config->item('resource_url') . 'images/152x152.png';?>">
  <link rel="apple-touch-icon" sizes="114x114" href="<?= $this->config->item('resource_url') . 'images/114x114.png';?>">
  <link rel="apple-touch-icon" sizes="144x144" href="<?= $this->config->item('resource_url') . 'images/144x144.png';?>">
  <link rel="apple-touch-icon" sizes="152x152" href="<?= $this->config->item('resource_url') . 'images/152x152.png';?>">
  <link rel="apple-touch-icon" sizes="72x72" href="<?= $this->config->item('resource_url') . 'images/72x72.png';?>">

  <title>Gonagoo | Admin</title>
  <!-- CDN -->
  <link href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet"  />
  <link href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" rel="stylesheet" />
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
  <link href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" rel="stylesheet" />
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

  <!-- Local -->
  <link href="<?= $this->config->item('resource_url') . 'js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css';?>" rel="stylesheet">
  <link href="<?= $this->config->item('resource_url') . 'css/font-icons/entypo/css/entypo.css';?>" rel="stylesheet" />
  <link href="<?= $this->config->item('resource_url') . 'css/neon-core.css';?>" rel="stylesheet" />
  <link href="<?= $this->config->item('resource_url') . 'css/neon-theme.css';?>" rel="stylesheet" />
  <link href="<?= $this->config->item('resource_url') . 'css/neon-forms.css';?>" rel="stylesheet" />
  <link href="<?= $this->config->item('resource_url') . 'css/custom.css';?>" rel="stylesheet" />
  <link href="<?= $this->config->item('resource_url') . 'css/skins/blue.css';?>" rel="stylesheet" />
  <!-- <link href="<?= $this->config->item('resource_url') . 'js/datatables/datatables.css';?>" /> -->
  
  <!--[if lt IE 9]><script src="resources/js/ie8-responsive-file-warning.js"></script><![endif]-->
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style>
    .page-body .select2-container .select2-choice { height: 34px; line-height:33px; }
    .loader {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url("<?=base_url('resources/Preloader_2.gif');?>") 50% 50% no-repeat rgb(249,249,249);
      opacity: .8;
    }
    .h1, .h2, .h3, h1, h2, h3 { margin-top: 0px !important; }
  </style>

</head>
<body class="page-body skin-blue">
<div class="loader"></div>
<script type="text/javascript">
  $(window).load(function() {
    $(".loader").fadeOut("slow");
  });
</script>
<div class="page-container">
  <div class="sidebar-menu">
    <div class="sidebar-menu-inner">
      <header class="logo-env">
        <!-- logo -->
        <div class="logo">
          <a href="dashboard">
            <img src="<?= $this->config->item('resource_url') . 'images/dashboard-logo.png';?>" width="120" alt="" />
          </a>
        </div>
        <!-- logo collapse icon -->
        <div class="sidebar-collapse">
          <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
            <i class="entypo-menu"></i>
          </a>
        </div>                
        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
          <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
            <i class="entypo-menu"></i>
          </a>
        </div>
      </header>
      <div class="sidebar-user-info">
        <div class="sui-normal">
          <a href="<?= base_url('admin/profile'); ?>" class="user-link">
            <?php if($user['avatar_url'] != "NULL"): ?>
            <img src="<?= $this->config->item('base_url') . $user['avatar_url'];?>" width="55" alt="" class="img-circle" />
          <?php else: ?>
            <img src="<?= $this->config->item('resource_url') . 'images/thumb-1@2x.png';?>" width="55" alt="" class="img-circle" />
          <?php endif; ?>
            <span>Welcome,</span>
            <strong><?= $user['fname'] .' '. $user['lname']; ?></strong>
          </a>
        </div>
      </div>
      <ul id="main-menu" class="main-menu">
        <?php 
        //$method_name = $this->router->fetch_method();
        $controller_name = $this->router->fetch_class();;
        //get permission
        $per_category_type = explode(',', $permissions[0]['category_type']);
        $per_categories = explode(',', $permissions[0]['categories']);
        $per_currency_master = explode(',', $permissions[0]['currency_master']);
        $per_country_currency = explode(',', $permissions[0]['country_currency']);
        $per_authority_type = explode(',', $permissions[0]['authority_type']);
        $per_manage_authority = explode(',', $permissions[0]['manage_authority']);
        $per_manage_group_permission = explode(',', $permissions[0]['manage_group_permission']);
        $per_manage_users = explode(',', $permissions[0]['manage_users']);
        $per_notifications = explode(',', $permissions[0]['notifications']);
        $per_promo_code = explode(',', $permissions[0]['promo_code']);
        $per_promo_code_service = explode(',', $permissions[0]['promo_code_service']);
        $per_dimension = explode(',', $permissions[0]['dimension']);
        $per_vehicals = explode(',', $permissions[0]['vehicals']);
        $per_rates = explode(',', $permissions[0]['rates']);
        $per_driver_category = explode(',', $permissions[0]['driver_category']);
        $per_advance_payment = explode(',', $permissions[0]['advance_payment']);
        $per_skills_master = explode(',', $permissions[0]['skills_master']);
        $per_insurance_master = explode(',', $permissions[0]['insurance_master']);
        $per_verify_doc = explode(',', $permissions[0]['verify_doc']);
        $per_all_orders = explode(',', $permissions[0]['all_orders']);
        $per_relay_point = explode(',', $permissions[0]['relay_point']);
        $per_withdraw_request = explode(',', $permissions[0]['withdraw_request']);
        $per_customer_support = explode(',', $permissions[0]['customer_support']);
        $per_carrier_type = explode(',', $permissions[0]['carrier_type']);
        $per_dangerous_goods = explode(',', $permissions[0]['dangerous_goods']);
        $per_country_dangerous_goods = explode(',', $permissions[0]['country_dangerous_goods']);
        $per_user_followups = explode(',', $permissions[0]['user_followups']);
        $per_gonagoo_address = explode(',', $permissions[0]['gonagoo_address']);
        $per_laundry_category = explode(',', $permissions[0]['laundry_category']);
        $per_laundry_payment = explode(',', $permissions[0]['laundry_payment']);
        $per_ticket_comm_refund = explode(',', $permissions[0]['ticket_comm_refund']);
        $per_m4_adv_payment = explode(',', $permissions[0]['m4_adv_payment']);
        $per_m4_know_provider = explode(',', $permissions[0]['m4_know_provider']);
        $per_m4_job_cancel_reason = explode(',', $permissions[0]['m4_job_cancel_reason']);
        $per_m4_profile_sub = explode(',', $permissions[0]['m4_profile_sub']);
        $per_m4_dispute_cat = explode(',', $permissions[0]['m4_dispute_cat']);
        $per_m4_dispute_sub_cat = explode(',', $permissions[0]['m4_dispute_sub_cat']);
        $per_m4_contract_expiry_delay = explode(',', $permissions[0]['m4_contract_expiry_delay']);
        ?>

        <li class="<?php if($controller_name == 'administration') { echo 'active'; } ?>">
          <a href="<?= base_url('admin/dashboard'); ?>">
            <i class="fa fa-tachometer"></i>
            <span class="title">Dashboard</span>
          </a>
        </li>
        <?php if(in_array('1', $per_category_type) || in_array('1', $per_categories) || in_array('1', $per_currency_master) || in_array('1', $per_country_currency) || in_array('1', $per_dimension) || in_array('1', $per_vehicals) || in_array('1', $per_rates) || in_array('1', $per_driver_category) || in_array('1', $per_advance_payment) || in_array('1', $per_skills_master) || in_array('1', $per_insurance_master) || in_array('1', $per_carrier_type) || in_array('1', $per_dangerous_goods) || in_array('1', $per_m4_adv_payment) || in_array('1', $per_m4_know_provider) || in_array('1', $per_m4_job_cancel_reason) || in_array('1', $per_m4_profile_sub) || in_array('1', $per_m4_dispute_cat) || in_array('1', $per_m4_dispute_sub_cat) || in_array('1', $per_m4_contract_expiry_delay) ) { ?>
          <li class="has-sub <?php if($controller_name == 'categoryType' || $controller_name == 'categories' || $controller_name == 'Currency' || $controller_name == 'country_currency' || $controller_name == 'Skills' || $controller_name == 'Insurance' || $controller_name == 'standard_dimension' || $controller_name == 'transport_vehicles' || $controller_name == 'standard_rates' || $controller_name == 'driver_categories' || $controller_name == 'advance_payment' || $controller_name == 'Carrier_type' || $controller_name == 'point_to_point_rates' || $controller_name == 'country_custom' || $controller_name == 'dangerous_goods' || $controller_name == 'country_dangerous_goods' || $controller_name == 'advance_payment_bus_reservation' || $controller_name == 'cancellation_rescheduling_bus_reservation' || $controller_name == 'laundry_categories' || $controller_name == 'advance_payment_laundry' || $controller_name == 'advance_payment_services' || $controller_name == 'provider_question_master' || $controller_name == 'job_cancellation_reason_master' || $controller_name == 'manage_dispute' || $controller_name == 'contract_expiry_delay' || $controller_name == 'service_questionnaire' || $controller_name == 'profile_subscription' || $controller_name == 'provider_offer_manage' || $controller_name == 'dispute_category_master' || $controller_name == 'dispute_sub_category_master' ) { echo 'opened'; } ?>">
            <a href="">
              <i class="fa fa-th-large"></i>
              <span class="title">Masters</span>
            </a>
            <ul class="<?php if($controller_name == 'categoryType' || $controller_name == 'categories' || $controller_name == 'Currency' || $controller_name == 'country_currency' || $controller_name == 'Skills' || $controller_name == 'Insurance' || $controller_name == 'standard_dimension' || $controller_name == 'transport_vehicles' || $controller_name == 'standard_rates' || $controller_name == 'driver_categories' || $controller_name == 'advance_payment' || $controller_name == 'Carrier_type'  || $controller_name == 'point_to_point_rates' || $controller_name == 'country_custom' || $controller_name == 'dangerous_goods' || $controller_name == 'country_dangerous_goods' || $controller_name == 'advance_payment_bus_reservation' || $controller_name == 'cancellation_rescheduling_bus_reservation' || $controller_name == 'laundry_categories' || $controller_name == 'advance_payment_laundry' || $controller_name == 'advance_payment_services' || $controller_name == 'provider_question_master' || $controller_name == 'job_cancellation_reason_master' || $controller_name == 'manage_dispute' || $controller_name == 'contract_expiry_delay' || $controller_name == 'service_questionnaire' || $controller_name == 'profile_subscription' || $controller_name == 'provider_offer_manage' || $controller_name == 'dispute_category_master' || $controller_name == 'dispute_sub_category_master' ) { echo 'visible'; } ?>">
              <?php if(in_array('1', $per_category_type) || in_array('1', $per_categories)) { ?>
                <li class="has-sub <?php if($controller_name == 'categoryType' || $controller_name == 'categories') { echo 'opened'; } ?>">
                  <a href="">
                    <span class="title">Categories Master</span>
                  </a>
                  <ul class="<?php if($controller_name == 'categoryType' || $controller_name == 'categories') { echo 'visible'; } ?>">
                    <?php if(in_array('1', $per_category_type)) { ?>
                      <li class="<?php if($controller_name == 'categoryType') { echo 'active'; } ?>">
                        <a href="<?= base_url('admin/category-type'); ?>">
                          <span class="title">Category Type</span>
                        </a>
                      </li>
                    <?php } ?>
                    <?php if(in_array('1', $per_categories)) { ?>
                      <li class="<?php if($controller_name == 'categories') { echo 'active'; } ?>">
                        <a href="<?= base_url('admin/categories'); ?>">
                          <span class="title">Categories</span>
                        </a>
                      </li>
                    <?php } ?>
                  </ul>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_currency_master) || in_array('1', $per_country_currency)) { ?>
                <li class="has-sub <?php if($controller_name == 'Currency' || $controller_name == 'country_currency') { echo 'opened'; } ?>">
                  <a href="">
                    <span class="title">Manage Currency</span>
                  </a>
                  <ul class="<?php if($controller_name == 'Currency' || $controller_name == 'country_currency') { echo 'visible'; } ?>">
                    <?php if(in_array('1', $per_currency_master)) { ?>
                      <li class="<?php if($controller_name == 'Currency') { echo 'active'; } ?>">
                        <a href="<?= base_url('admin/currency-master'); ?>">
                          <span class="title">Currency Master</span>
                        </a>
                      </li>
                    <?php } ?>
                    <?php if(in_array('1', $per_country_currency)) { ?>
                      <li class="<?php if($controller_name == 'country_currency') { echo 'active'; } ?>">
                        <a href="<?= base_url('admin/country-currency'); ?>">
                          <span class="title">Country Currency Master</span>
                        </a>
                      </li>
                    <?php } ?>
                  </ul>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_skills_master)) { ?>
                <li class="<?php if($controller_name == 'Skills') { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/skills-master'); ?>">
                    <span class="title">Manage Skills</span>
                  </a>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_insurance_master)) { ?>
                <li class="<?php if($controller_name == 'Insurance') { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/insurance-master'); ?>">
                    <span class="title">Manage Insurance</span>
                  </a>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_dimension)) { ?>
                <li class="<?php if($controller_name == 'standard_dimension') { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/standard-dimensions'); ?>">
                    <span class="title">Standard Dimensions</span>
                  </a>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_vehicals)) { ?>
                <li class="<?php if($controller_name == 'transport_vehicles') { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/transport-vehicles'); ?>">
                    <span class="title">Transport Vehicles</span>
                  </a>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_rates)) { ?>
                <li class="<?php if($controller_name == 'point_to_point_rates' || ($controller_name == 'standard_rates')) { echo 'opened'; } ?>">
                  <a href="">
                    <span class="title">Manage Rates</span>
                  </a>
                  <ul class="<?php if( ($controller_name == 'point_to_point_rates') || ($controller_name == 'standard_rates') ) { echo 'visible'; } ?>" >
                    <li class="<?php if($controller_name == 'point_to_point_rates') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/p-to-p-rates'); ?>">Point to Point Rates</a>
                    </li>
                    <li class="<?php if($controller_name == 'standard_rates') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/standard-rates'); ?>">Standard Rates</a>
                    </li>
                  </ul>
                </li>
              <?php } ?>              
              <?php if(in_array('1', $per_advance_payment)) { ?>
                <li class="<?php if($controller_name == 'advance_payment' || ($controller_name == 'country_custom') || ($controller_name == 'advance_payment_bus_reservation') || ($controller_name == 'cancellation_rescheduling_bus_reservation') || ($controller_name == 'laundry_categories') || ($controller_name == 'advance_payment_laundry') ) { echo 'opened'; } ?>">
                  <a href="">
                    <span class="title">Setup Advance Percentage</span>
                  </a>
                  <ul class="<?php if( ($controller_name == 'advance_payment') || ($controller_name == 'country_custom') || ($controller_name == 'advance_payment_bus_reservation') || ($controller_name == 'cancellation_rescheduling_bus_reservation') || ($controller_name == 'laundry_categories') || ($controller_name == 'advance_payment_laundry') ) { echo 'visible'; } ?>" >
                    <li class="<?php if($controller_name == 'advance_payment') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/advance-payment'); ?>">Courier & Transport</a>
                    </li>
                    <li class="<?php if($controller_name == 'country_custom') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/custom-percentage'); ?>">Country Custom Setup</a>
                    </li>
                    <li class="<?php if($controller_name == 'advance_payment_bus_reservation') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/advance-payment-bus-reservation'); ?>">Bus Reservation</a>
                    </li>
                    <li class="<?php if($controller_name == 'cancellation_rescheduling_bus_reservation') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/cancellation-rescheduling-bus-reservation'); ?>">Bus Cancel & Reschedule</a>
                    </li>
                    <li class="<?php if($controller_name == 'laundry_categories') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/laundry-categories'); ?>"> <span class="title">Laundry Category</span></a>
                    </li>
                    <li class="<?php if($controller_name == 'advance_payment_laundry') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/advance-payment-laundry'); ?>"> <span class="title">Laundry Commission & Payment</span> </a>
                    </li>
                  </ul>
                </li>
              <?php } ?>
              
              <?php if(in_array('1', $per_m4_adv_payment) || in_array('1', $per_m4_know_provider) || in_array('1', $per_m4_job_cancel_reason) || in_array('1', $per_m4_profile_sub) || in_array('1', $per_m4_dispute_cat) || in_array('1', $per_m4_dispute_sub_cat) || in_array('1', $per_m4_contract_expiry_delay)) { ?>
                <li class="<?php if( ($controller_name == 'advance_payment_services') || ($controller_name == 'provider_question_master') || ($controller_name == 'job_cancellation_reason_master') || ($controller_name == 'manage_dispute') || ($controller_name == 'contract_expiry_delay') || ($controller_name == 'service_questionnaire') || ($controller_name == 'profile_subscription') || ($controller_name == 'provider_offer_manage') || ($controller_name == 'dispute_category_master') || ($controller_name == 'dispute_sub_category_master') ) { echo 'opened'; } ?>">
                  <a href="">
                    <span class="title">Module 4 Masters</span>
                  </a>
                  <ul class="<?php if( ($controller_name == 'advance_payment_services') || ($controller_name == 'provider_question_master') || ($controller_name == 'job_cancellation_reason_master') || ($controller_name == 'manage_dispute') || ($controller_name == 'contract_expiry_delay') || ($controller_name == 'service_questionnaire') || ($controller_name == 'profile_subscription') || ($controller_name == 'provider_offer_manage') || ($controller_name == 'dispute_category_master') || ($controller_name == 'dispute_sub_category_master') ) { echo 'visible'; } ?>" >
                    <li class="<?php if($controller_name == 'advance_payment_services') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/advance-payment-services'); ?>"> <span class="title">Advance & Commission</span> </a>
                    </li>
                    <li class="<?php if($controller_name == 'provider_question_master') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/provider-question-master'); ?>">Provider Questions</a>
                    </li>
                    <li class="<?php if($controller_name == 'job_cancellation_reason_master') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/job-cancellation-reason-master'); ?>">Cancellation Reasons</a>
                    </li>
                    <li class="<?php if($controller_name == 'profile_subscription') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/profile-subscription'); ?>">Profile Subscriptions</a>
                    </li>
                    <li class="<?php if($controller_name == 'dispute_category_master') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/dispute-category-master'); ?>">Dispute Categories</a>
                    </li>
                    <li class="<?php if($controller_name == 'dispute_sub_category_master') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/dispute-sub-category-master'); ?>">Dispute Sub Category</a>
                    </li>
                    <li class="<?php if($controller_name == 'contract_expiry_delay') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/contract-expiry-delay'); ?>">Contract Expiry Delay</a>
                    </li>
                    <li class="<?php if($controller_name == 'provider_offer_manage') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/provider-offer-manage'); ?>">Manage Offers</a>
                    </li>
                    <li class="<?php if($controller_name == 'manage_dispute') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/manage-dispute'); ?>">Manage Disputes</a>
                    </li>
                    <!-- <li class="<?php if($controller_name == 'service_questionnaire') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/country-dangerous-goods'); ?>">Service Questionnaire</a>
                    </li> -->
                  </ul>
                </li>
              <?php } ?>

              <?php if(in_array('1', $per_dangerous_goods) || in_array('1', $per_country_dangerous_goods)) { ?>
                <li class="<?php if($controller_name == 'dangerous_goods' || ($controller_name == 'country_dangerous_goods')) { echo 'opened'; } ?>">
                  <a href="">
                    <span class="title">Dangerous Goods</span>
                  </a>
                  <ul class="<?php if( ($controller_name == 'dangerous_goods') || ($controller_name == 'country_dangerous_goods') ) { echo 'visible'; } ?>" >
                    <li class="<?php if($controller_name == 'dangerous_goods') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/dangerous-goods'); ?>">Manage Master</a>
                    </li>
                    <li class="<?php if($controller_name == 'country_dangerous_goods') { echo 'active'; } ?>">
                      <a href="<?= base_url('admin/country-dangerous-goods'); ?>">Manage Country-wise</a>
                    </li>
                  </ul>
                </li>
              <?php } ?>

              <?php if(in_array('1', $per_driver_category)) { ?>
                <li class="<?php if($controller_name == 'driver_categories') { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/driver-categories'); ?>">
                    <span class="title">Driver Categories</span>
                  </a>
                </li>
              <?php } ?>

              <?php if(in_array('1', $per_carrier_type)) { ?>
                <li class="<?php if($controller_name == 'Carrier_type') { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/carrier-type'); ?>">
                    <span class="title">Deliverer Carrier Type</span>
                  </a>
                </li>
              <?php } ?>
            </ul>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_authority_type) || in_array('1', $per_manage_authority)) { ?>
          <li class="has-sub <?php if($controller_name == 'group_permissions' || $controller_name == 'Authority') { echo 'opened'; } ?>">
            <a href="">
              <i class="fa fa-user-secret"></i>
              <span class="title">Authorities</span>
            </a>
            <ul class="<?php if($controller_name == 'group_permissions' || $controller_name == 'Authority') { echo 'visible'; } ?>">
              <?php if(in_array('1', $per_authority_type)) { ?>
                <li class="hidden">
                  <a href="<?= base_url('admin/authority-type'); ?>">
                    <i class="fa fa-user"></i>
                    <span class="title">Manage Authority Type</span>
                  </a>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_manage_group_permission)) { ?>
                <li class="<?php if($controller_name == 'group_permissions') { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/group-permission'); ?>">
                    <i class="fa fa-key"></i>
                    <span class="title">Group Permissions</span>
                  </a>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_manage_authority)) { ?>
                <li class="<?php if($controller_name == 'Authority') { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/authority'); ?>">
                    <i class="fa fa-user-secret"></i>
                    <span class="title">Manage Authority</span>
                  </a>
                </li>
              <?php } ?>
            </ul>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_manage_users)) { ?>
          <li class="<?php if($controller_name == 'Users') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/users'); ?>">
              <i class="fa fa-users"></i>
              <span class="title">Users</span>
            </a>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_verify_doc)) { ?>
          <li class="<?php if($controller_name == 'Verify_docs') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/verify-documents'); ?>">
              <i class="fa fa-files-o"></i>
              <span class="title">Verify Documents</span>
            </a>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_all_orders)) { $method = $this->router->fetch_method(); ?>
          <li class="has-sub <?php if($controller_name == 'Admin_orders') { echo 'opened'; } ?>">
            <a href="">
              <i class="fa fa-first-order"></i>
              <span class="title">Orders</span>
            </a>
            <ul class="<?php if($controller_name == 'Admin_orders') { echo 'visible'; } ?>">
              <?php if(in_array('1', $per_all_orders)) { ?>
                <li class="<?php if($controller_name == 'Admin_orders' && ($method == 'get_all_orders' || $method == 'view_order_details')) { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/get-all-orders'); ?>">
                    <i class="fa fa-truck"></i>
                    <span class="title">Courier / Transport</span>
                  </a>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_all_orders)) { ?>
                <li class="<?php if($controller_name == 'Admin_orders' && ($method == 'get_all_ticket_orders' || $method == 'view_ticket_order_details')) { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/get-all-ticket-orders'); ?>">
                    <i class="fa fa-ticket"></i>
                    <span class="title">Tickets</span>
                  </a>
                </li>
              <?php } ?>
              <?php if(in_array('1', $per_all_orders)) { ?>
                <li class="<?php if($controller_name == 'Admin_orders' && ($method == 'get_all_laundry_orders' || $method == 'view_laundry_order_details')) { echo 'active'; } ?>">
                  <a href="<?= base_url('admin/get-all-laundry-orders'); ?>">
                    <i class="fa fa-fire"></i>
                    <span class="title">Laundry</span>
                  </a>
                </li>
              <?php } ?>
            </ul>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_relay_point)) { ?>
          <li class="<?php if($controller_name == 'Relay') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/relay-point'); ?>">
              <i class="fa fa-chain"></i>
              <span class="title">Relay Point</span>
            </a>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_withdraw_request)) { ?>
          <li class="<?php if($controller_name == 'Withdraw') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/withdraw-request'); ?>">
              <i class="fa fa-money"></i>
              <span class="title">Withdraw Request</span>
            </a>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_ticket_comm_refund)) { ?>
          <li class="<?php if($controller_name == 'Ticket_commission_refund') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/commission-refund-request'); ?>">
              <i class="fa fa-money"></i>
              <span class="title">Commission Refund Request</span>
            </a>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_customer_support)) { ?>
          <li class="<?php if($controller_name == 'Customer_support') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/customer-support-list'); ?>">
              <i class="fa fa-info-circle"></i>
              <span class="title">Customer Support</span>
            </a>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_user_followups)) { ?>
          <li class="<?php if($controller_name == 'user_followups_admin') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/user-followups-admin'); ?>">
              <i class="fa fa-comments-o"></i>
              <span class="title">Customer Follow-ups</span>
            </a>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_gonagoo_address)) { ?>
          <li class="<?php if($controller_name == 'gonagoo_address') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/gonagoo-address-admin'); ?>">
              <i class="fa fa-address-book-o"></i>
              <span class="title">Gonagoo Address</span>
            </a>
          </li>
        <?php } ?>
        <?php if($permissions[0]['auth_type_id'] > 1) { ?>
          <li class="<?php if($controller_name == 'user_followups_sales') { echo 'active'; } ?>">
            <a href="<?= base_url('admin/user-followups-sales'); ?>">
              <i class="fa fa-comments-o"></i>
              <span class="title">Assigned Customers</span>
            </a>
          </li>
        <?php } ?>
        <?php if(in_array('1', $per_promo_code) || in_array('1', $per_promo_code_service)) { ?>
          <li class="has-sub">
            <a href="">
              <i class="fa fa-barcode"></i>
              <span class="title">Promo Code</span>
            </a>
            <ul>
              <?php if(in_array('1', $per_promo_code)) { ?>
                <li>
                  <a href="<?= base_url('admin/promo-code'); ?>">
                    <i class="fa fa-mobile-phone"></i>
                    <span class="title">Promo Code Master</span>
                  </a>
                </li>
              <?php } ?>
            </ul>
          </li>
          <?php if(in_array('1', $per_manage_authority)) { ?>
            <li>
              <a href="<?= base_url('admin/change-app-version'); ?>">
                <i class="fa fa-mobile"></i>
                <span class="title">App Version Settings</span>
              </a>
            </li>
          <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </div>

  <div class="main-content">
    <div class="row" style="margin-top: -20px; margin-bottom: -20px;">
      <div class="col-md-8 clearfix">
        <?php if(in_array('1', $per_notifications)) { ?>
          <ul class="user-info pull-left pull-none-xsm">
            <ul class="user-info pull-left pull-right-xs pull-none-xsm" style="margin-top: 5px">
              <li class="notifications dropdown">
                <a href="<?= base_url('admin/notification'); ?>">
                  <i class="fa fa-bell"></i> Notifications
                </a>
              </li>
              <li class="notifications dropdown">
                <a href="<?= base_url('admin/user-wise-dashboard'); ?>">
                  <i class="fa fa-truck"></i> Courier/Transport Dashboard
                </a>
              </li>
              <li class="notifications dropdown">
                <a href="<?= base_url('admin/user-wise-bus-dashboard'); ?>">
                  <i class="fa fa-bus"></i> Bus Dashboard
                </a>
              </li>
              <li class="notifications dropdown">
                <a href="<?= base_url('admin/user-wise-laundry-dashboard'); ?>">
                  <i class="fa fa-recycle"></i> Laundry Dashboard
                </a>
              </li>
            </ul>
          </ul>
        <?php } ?>
      </div>
      <div class="col-md-4 clearfix hidden-xs">
        <ul class="list-inline links-list pull-right">
          <li class="dropdown">
            Settings
            <a href="" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
              <i class="entypo-down-open right"></i>
            </a>
            <ul class="dropdown-menu pull-right">
              <li>
                <a href="profile">
                  <i class="fa fa-user right"></i> Profile 
                </a>
              </li>
              <li>
                <a href="<?= base_url('admin/logout'); ?>">
                  <i class="fa fa-sign-out right"></i> Log Out 
                </a>
              </li>
            </ul>
          </li> 
        </ul>
      </div>
    </div>
  <hr />