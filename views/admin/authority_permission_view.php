		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li>
				<a href="<?= base_url('admin/authority'); ?>">Authority List</a>
			</li>
			<li class="active">
				<strong>Edit</strong>
			</li>
		</ol>
	
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-dark" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							Edit Permissions
						</div>
						
						<div class="panel-options">
							<a href="<?= base_url('admin/authority'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
						</div>
					</div>
					
					<div class="panel-body">
						
						<?php if($this->session->flashdata('error')): ?>
					      	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
					    <?php elseif($this->session->flashdata('success')):  ?>
					    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
					    <?php endif; ?>
						
						<?php 
						//var_dump($user_permissions);
						if(!isset($user_permissions)) {
							$per_category_type = $per_categories = $per_currency_master = $per_country_currency = $per_authority_type = $per_manage_authority = $per_manage_users = $per_manage_promo = $per_manage_promo_service = $per_manage_dimension = $per_manage_vehicals = $per_manage_rates = $per_manage_driver_category = $per_manage_advance_payment = $per_manage_skills_master = $per_manage_insurance_master = $per_manage_relay_point = $per_manage_verify_doc = $per_manage_all_orders = $per_manage_withdraw_request = explode(',', '0,0,0,0,0');
						} else {
							$per_category_type = explode(',', $user_permissions[0]['category_type']);
							$per_categories = explode(',', $user_permissions[0]['categories']);
							$per_currency_master = explode(',', $user_permissions[0]['currency_master']);
							$per_country_currency = explode(',', $user_permissions[0]['country_currency']);
							$per_authority_type = explode(',', $user_permissions[0]['authority_type']);
							$per_manage_authority = explode(',', $user_permissions[0]['manage_authority']);
							$per_manage_users = explode(',', $user_permissions[0]['manage_users']);
							$per_notifications = explode(',', $user_permissions[0]['notifications']);
							$per_manage_promo = explode(',', $user_permissions[0]['promo_code']);
							$per_manage_promo_service = explode(',', $user_permissions[0]['promo_code_service']);
							$per_manage_dimension = explode(',', $user_permissions[0]['dimension']);
							$per_manage_vehicals = explode(',', $user_permissions[0]['vehicals']);
							$per_manage_rates = explode(',', $user_permissions[0]['rates']);
							$per_manage_driver_category = explode(',', $user_permissions[0]['driver_category']);
							$per_manage_advance_payment = explode(',', $user_permissions[0]['advance_payment']);
							$per_manage_skills_master = explode(',', $user_permissions[0]['skills_master']);
							$per_manage_insurance_master = explode(',', $user_permissions[0]['insurance_master']);
							$per_manage_relay_point = explode(',', $user_permissions[0]['relay_point']);
							$per_manage_verify_doc = explode(',', $user_permissions[0]['verify_doc']);
							$per_manage_all_orders = explode(',', $user_permissions[0]['all_orders']);
							$per_manage_withdraw_request = explode(',', $user_permissions[0]['withdraw_request']);

						}
						$auth_id = $authorities['auth_id'];
						?>

						<form role="form" class="form-horizontal form-groups-bordered" id="permissionEdit" method="post" action="<?= base_url('admin/update-authority-permissions'); ?>">
							<input type="hidden" id="auth_id" name="auth_id" value="<?= $auth_id; ?>" />

						<div class="form-group text-center">
							<label for="auth_fname" class="col-sm-2" style="text-align: right;">Functionality</label>
							<label for="auth_fname" class="col-sm-2" style="text-align: center;">View</label>
							<label for="auth_fname" class="col-sm-2" style="text-align: center;">Add</label>
							<label for="auth_fname" class="col-sm-2" style="text-align: center;">Edit</label>
							<label for="auth_fname" class="col-sm-2" style="text-align: center;">Activate/Deactivate</label>
							<label for="auth_fname" class="col-sm-2" style="text-align: center;">Delete</label>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Category Type</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="category_type_view" value="1" <?php if(in_array('1', $per_category_type)) { echo 'checked'; } ?>  />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="category_type_add" value="2" <?php if(in_array('2', $per_category_type)) { echo 'checked'; } ?>  />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="category_type_edit" value="3" <?php if(in_array('3', $per_category_type)) { echo 'checked'; } ?>  />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="category_type_active" value="4" <?php if(in_array('4', $per_category_type)) { echo 'checked'; } ?>  />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Categories</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="category_view" value="1" <?php if(in_array('1', $per_categories)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="category_add" value="2" <?php if(in_array('2', $per_categories)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="category_edit" value="3" <?php if(in_array('3', $per_categories)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="category_active" value="4" <?php if(in_array('4', $per_categories)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Currency Master</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="currency_master_view" value="1" <?php if(in_array('1', $per_currency_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="currency_master_add" value="2" <?php if(in_array('2', $per_currency_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="currency_master_edit" value="3" <?php if(in_array('3', $per_currency_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="currency_master_active" value="4" <?php if(in_array('4', $per_currency_master)) { echo 'checked'; } ?> />
								</div>
							</div>							
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="currency_master_delete" value="5" <?php if(in_array('5', $per_currency_master)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Country Currency</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="country_currency_view" value="1" <?php if(in_array('1', $per_country_currency)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="country_currency_add" value="2" <?php if(in_array('2', $per_country_currency)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="country_currency_edit" value="3" <?php if(in_array('3', $per_country_currency)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="country_currency_active" value="4" <?php if(in_array('4', $per_country_currency)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Authority Type</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="authority_type_view" value="1" <?php if(in_array('1', $per_authority_type)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="authority_type_add" value="2" <?php if(in_array('2', $per_authority_type)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="authority_type_edit" value="3" <?php if(in_array('3', $per_authority_type)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="authority_type_active" value="4" <?php if(in_array('4', $per_authority_type)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Manage Authorities</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_authority_view" value="1" <?php if(in_array('1', $per_manage_authority)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_authority_add" value="2" <?php if(in_array('2', $per_manage_authority)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_authority_edit" value="3" <?php if(in_array('3', $per_manage_authority)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_authority_active" value="4" <?php if(in_array('4', $per_manage_authority)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Manage Users</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_users_view" value="1" <?php if(in_array('1', $per_manage_users)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_users_add" value="2" <?php if(in_array('2', $per_manage_users)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_users_edit" value="3" <?php if(in_array('3', $per_manage_users)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_users_active" value="4" <?php if(in_array('4', $per_manage_users)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Notification</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_notifications_view" value="1" <?php if(in_array('1', $per_notifications)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_notifications_add" value="2" <?php if(in_array('2', $per_notifications)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">App Promo Code</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_promo_view" value="1" <?php if(in_array('1', $per_manage_promo)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_promo_add" value="2" <?php if(in_array('2', $per_manage_promo)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_promo_edit" value="3" <?php if(in_array('3', $per_manage_promo)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_promo_active" value="4" <?php if(in_array('4', $per_manage_promo)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Service Promo Code</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_promo_service_view" value="1" <?php if(in_array('1', $per_manage_promo_service)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Standard Dimension</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_dimension_view" value="1" <?php if(in_array('1', $per_manage_dimension)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_dimension_add" value="2" <?php if(in_array('2', $per_manage_dimension)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_dimension_edit" value="3" <?php if(in_array('3', $per_manage_dimension)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_dimension_delete" value="5" <?php if(in_array('5', $per_manage_dimension)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Vehicle</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_vehicals_view" value="1" <?php if(in_array('1', $per_manage_vehicals)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_vehicals_add" value="2" <?php if(in_array('2', $per_manage_vehicals)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_vehicals_edit" value="3" <?php if(in_array('3', $per_manage_vehicals)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_vehicals_delete" value="5" <?php if(in_array('5', $per_manage_vehicals)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Rates</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_rates_view" value="1" <?php if(in_array('1', $per_manage_rates)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_rates_add" value="2" <?php if(in_array('2', $per_manage_rates)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_rates_edit" value="3" <?php if(in_array('3', $per_manage_rates)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_rates_delete" value="5" <?php if(in_array('5', $per_manage_rates)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Driver Category</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_driver_category_view" value="1" <?php if(in_array('1', $per_manage_driver_category)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_driver_category_add" value="2" <?php if(in_array('2', $per_manage_driver_category)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_driver_category_edit" value="3" <?php if(in_array('3', $per_manage_driver_category)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_driver_category_delete" value="5" <?php if(in_array('5', $per_manage_driver_category)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Advance Payment</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_advance_payment_view" value="1" <?php if(in_array('1', $per_manage_advance_payment)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_advance_payment_add" value="2" <?php if(in_array('2', $per_manage_advance_payment)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_advance_payment_edit" value="3" <?php if(in_array('3', $per_manage_advance_payment)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_advance_payment_delete" value="5" <?php if(in_array('5', $per_manage_advance_payment)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Skill Master</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_skills_master_view" value="1" <?php if(in_array('1', $per_manage_skills_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_skills_master_add" value="2" <?php if(in_array('2', $per_manage_skills_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_skills_master_edit" value="3" <?php if(in_array('3', $per_manage_skills_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_skills_master_active" value="4" <?php if(in_array('4', $per_manage_skills_master)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Insurance Master</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_insurance_master_view" value="1" <?php if(in_array('1', $per_manage_insurance_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_insurance_master_add" value="2" <?php if(in_array('2', $per_manage_insurance_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_insurance_master_edit" value="3" <?php if(in_array('3', $per_manage_insurance_master)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_insurance_master_active" value="4" <?php if(in_array('4', $per_manage_insurance_master)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Relay Point</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_relay_point_view" value="1" <?php if(in_array('1', $per_manage_relay_point)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_relay_point_add" value="2" <?php if(in_array('2', $per_manage_relay_point)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_relay_point_edit" value="3" <?php if(in_array('3', $per_manage_relay_point)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_relay_point_active" value="4" <?php if(in_array('4', $per_manage_relay_point)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label class="col-sm-2 control-label">Withdraw Request</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_withdraw_request_view" value="1" <?php if(in_array('1', $per_manage_withdraw_request)) { echo 'checked'; } ?> />
								</div>
							</div>
							<div class="col-sm-2">
								&nbsp;
							</div>
							<div class="col-sm-2">
								&nbsp;
							</div>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_withdraw_request_active" value="4" <?php if(in_array('4', $per_manage_withdraw_request)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">User Doc. Verification</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_verify_doc_view" value="1" <?php if(in_array('1', $per_manage_verify_doc)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<label for="auth_lname" class="col-sm-2 control-label">Courier Orders</label>
							<div class="col-sm-2">
								<div class="checkbox checkbox-replace color-blue">
									<input type="checkbox" id="chk-20" name="manage_all_orders_view" value="1" <?php if(in_array('1', $per_manage_all_orders)) { echo 'checked'; } ?> />
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-blue">Save Changes</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<br />