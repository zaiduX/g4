<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/admin-dashboard'); ?>"><i class="fa fa-home"></i>Admin Dashboard</a>
  </li>
  <li class="active">
    <strong>Job List</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>      
<h2 style="display: inline-block;">Job List</h2>

<?php if($this->session->flashdata('error')):  ?>
	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
<?php endif; ?>
<?php if($this->session->flashdata('success')):  ?>
	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
<?php endif; ?>

<table class="table table-bordered display compact table-striped" id="table-2" style="max-width=100%">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Create Date</th>
      <th class="text-center">Name</th>
      <th class="text-center">From</th>
      <th class="text-center">To</th>
      <th class="text-center">Country</th>
      <th class="text-center">Price</th>
      <th class="text-center">Advance</th>
      <th class="text-center">Expiry</th>
      <th class="text-center">Status</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>
  
  <tbody>   
    <?php foreach ($jobs as $job):  ?>
    <tr>
      <td class="text-center"><?= $job['order_id'] ?></td>
      <td class="text-center"><?= date('d/m/Y', strtotime($job['cre_datetime'])); ?></td> 
      <td class="text-center">
          <?= ($job['cust_name'] !="NULL NULL" && $job['cust_name'] !="NULL")? trim($job['cust_name']): $this->admin->get_customer_email_name_by_order_id($job['cust_id']); ?>
      </td>
      <td class="text-center"><?= $job['from_address'] ?></td>
      <td class="text-center"><?= $job['to_address'] ?></td>
      <td class="text-center">
        <?php 
            $country_details = $this->notice->get_country_detail($job['from_country_id']);
            $country_name = $country_details['country_name'];
            echo $country_name;
      	?>
      </td>
      <td class="text-center"><?= $job['currency_sign'].$job['order_price']; ?></td>
      <td class="text-center"><?= $job['currency_sign'].$job['advance_payment']; ?></td>
      <td class="text-center"><?= date('d/m/Y', strtotime($job['expiry_date'])); ?></td>                        
      <td class="text-center">
        <?php 
          if($job['order_status'] == 'in_progress'){ 
            $status = explode('_',$job['order_status']); 
            echo '<h5 class="text-warning">'.ucfirst($status[0]). '-'.ucfirst($status[1]).'</h5>';
          } 
          else if($job['order_status'] == 'inactive'){ 
            echo '<h5 class="text-danger">'.ucfirst($job['order_status']).'</h5>';
          }
          else if($job['order_status'] == 'accept'){ 
            echo '<h5 class="text-success">'.ucfirst($job['order_status']).'</h5>';
          }
          else if($job['order_status'] == 'reject'){ 
            echo '<h5 class="text-danger">'.ucfirst($job['order_status']).'</h5>';
          }
          else { echo '<h5 class="text-info">'.ucfirst($job['order_status']).'</h5>';  }

        ?>
      </td>

        <td class="text-center">
          <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-cogs"></i>
            </button>
            <ul class="dropdown-menu dropdown" role="menu">
                <li>  <a href="<?= base_url('admin/view-order-details/') . $job['order_id']; ?>" data-toggle="tooltip" data-placement="left" title="Click to See Details" data-original-title="Click to See Details"> <i class="entypo-eye"></i> View </a> </li>
                <?php if( strtolower($job['order_status']) == "open"): ?>
                  <li class="divider"></li>               
                  <li> 
                    <a href="<?= base_url('admin/remove-order/') . $job['order_id']; ?>" data-toggle="tooltip" data-placement="left" title="Remove From Marketplace" data-original-title="Remove From Marketplace"> <i class="entypo-cancel"></i> Remove From Marketplace </a> 
                  </li>                     
                <?php endif; ?>
            </ul>
          </div>
        </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true,
      "order":[[7,"desc"]]
      
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );
  } );



</script>