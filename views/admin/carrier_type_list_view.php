<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Carrier Type</strong>
	</li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>			
<h2 style="display: inline-block;">Deliverer Carrier Type</h2>
<?php $per_carrier_type = explode(',', $permissions[0]['carrier_type']); if(in_array('2', $per_carrier_type)) { ?>
	<a type="button" href="<?= base_url('admin/add-carrier-type'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add New
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

<?php if($this->session->flashdata('error')):  ?>
		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
<?php endif; ?>
<?php if($this->session->flashdata('success')):  ?>
		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">ID</th>
			<th class="text-center">Carrier Title</th>
			<th class="text-center">Description</th>
			<th class="text-center">Date</th>
			<?php if(in_array('2', $per_carrier_type) || in_array('3', $per_carrier_type) || in_array('5', $per_carrier_type)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($carrier_type_list as $type_list):  ?>
		<tr>
			<td class="text-center"><?= $type_list['carrier_id'] ?></td>
			<td class="text-center"><?= $type_list['carrier_title'] ?></td>
			<td class="text-center"><?= $type_list['carrier_desc'] ?></td>
			<td class="text-center"><?= date('d/m/Y', strtotime($type_list['cre_datetime'])) ?></td>
			
			<?php if(in_array('2', $per_carrier_type) || in_array('3', $per_carrier_type) || in_array('5', $per_carrier_type)) { ?>
				<td class="text-center" style="display: flex; text-align: center;">
					<?php if(in_array('3', $per_carrier_type)): ?>	
						<form id="accept_form" action="<?= base_url('admin/edit-carrier-type') ?>" method="post">
							<input type="hidden" name="carrier_id" value="<?= $type_list['carrier_id']; ?>" />
							<button type="submit" class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to view and edit" data-original-title="Click to view and edit"><i class="entypo-pencil"></i> View / Edit </button>	
						</form> &nbsp;
				<?php endif; ?>
				<?php if(in_array('5', $per_carrier_type)) { ?>
					<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to inactivate" data-original-title="Click to inactivate" onclick="delete_carrier_type('<?= $type_list['carrier_id']; ?>');">
					<i class="entypo-cancel"></i> Delete</button>
				<?php } ?>
				</td>
			<?php } ?>

		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );
	} );


	function delete_carrier_type(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to delete this carrier type?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('delete-carrier-type', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Deleted!',
				    'Carrier type has been deleted.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'carrier type deletion failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

</script>