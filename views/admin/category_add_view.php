    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li>
        <a href="<?= base_url('admin/category_list'); ?>">Categories</a>
      </li>
      <li class="active">
        <strong>Create</strong>
      </li>
    </ol>
    
    <div class="row">
      <div class="col-md-12">
        
        <div class="panel  panel-dark" data-collapsed="0">
        
          <div class="panel-heading">
            <div class="panel-title">
              Add Category
            </div>
            
            <div class="panel-options">
              <a href="<?= base_url('admin/categories'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
            </div>
          </div>
          
          <div class="panel-body">
            
            <?php if($this->session->flashdata('error')):  ?>
              <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
            <form role="form" class="form-horizontal form-groups-bordered" id="category_add" action="<?= base_url('admin/register-category'); ?>" method="post" enctype="multipart/form-data">
              <div class="row">

                <div class="col-md-3 text-center">
                  <div class="form-group">
                    <label class="control-label">Choose Category Icon </label>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                        <img src="<?= $this->config->item('resource_url').'noimage.png'; ?>" alt="Category Icon">
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                      <div>
                        <span class="btn btn-white btn-file">
                          <span class="fileinput-new">Change Icon</span>
                          <span class="fileinput-exists">Change</span>
                          <input type="file" name="category_icon" id="category_icon" accept="image/*">
                        </span>
                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-9">

                  <div class="form-group col-md-12">
                    <label for="cat_name" class="control-label">Category Name</label>                    
                    <input type="text" class="form-control" id="cat_name" placeholder="Enter Category Name" name="cat_name" />
                  </div>

                  <div class="form-group col-md-12">
                    <label class="control-label">Category Type</label>
                    <select id="type_id" name="type_id" class="form-control" data-allow-clear="true" data-placeholder="Select Category Type">
                      <option value="">Select Category Type</option>
                      <?php foreach ($active_category_types as $type): ?>                     
                        <option value="<?= $type['cat_type_id'] ?>"><?= $type['cat_type']; ?></option>
                      <?php endforeach ?>
                    </select>                      
                  </div>

                  <div class="form-group col-md-12">
                    <label class="control-label">Parent Category</label>
                    <select id="parent_cat_id" name="parent_cat_id" class="form-control" data-allow-clear="true" data-placeholder="Select Parent Category">
                      <option value="">Parent Category</option>
                    </select>                      
                  </div>              
              
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                      <button type="submit" id="btn_submit" class="btn btn-blue">Add Category</button>
                    </div>
                  </div>

                </div>
            </form>
            
          </div>
        
        </div>
      
      </div>
    </div>
    
    <br />  


    <script>
      $("#type_id").on('change', function(event) {  event.preventDefault();
        var type_id = $(this).val();

        $.ajax({
            type: "POST", 
            url: "get-categories-by-type", 
            data: { type_id: type_id },
            dataType: "json",
            success: function(res){
                //Clear options corresponding to earlier option of first dropdown
                $('#parent_cat_id').empty(); 
                $('#parent_cat_id').append('<option value="0">Select Parent Category</option>');
                //Populate options of the second dropdown
                $.each( res, function(){    
                    $('#parent_cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
                });
                $('#parent_cat_id').focus();
            },
            beforeSend: function(){
                $('#parent_cat_id').empty();
                $('#parent_cat_id').append('<option value="0">Loading...</option>');
            },
            error: function(){
                $('#parent_cat_id').attr('disabled', true);
                $('#parent_cat_id').empty();
                $('#parent_cat_id').append('<option value="0">No Options</option>');
            }
        })

      });
      
    </script>

    <script>
    $("#category_add").on('submit',function(e){e.preventDefault();});
    $("#btn_submit").on('click',function(){
        var icon = $("#category_icon").val();
        var cat_name = $("#cat_name").val();
        var type_id = $("#type_id").val();

        if(!icon){ swal('Error',"Upload Category Icon", 'warning');    }
        else if(!cat_name){    swal('Error',"Enter Category Name", 'warning');  }
        else if(!type_id){   swal('Error',"Select Category Type", 'warning'); }
        else{ $("#category_add").get(0).submit(); }
    });

</script>
    
