<script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyCIj8TMFnqnxpvima7MDrJiySvGK-UVLqw&libraries=places'></script>
<script src="<?= base_url('resources/web-panel/scripts/locationpicker.jquery.min.js') ?>"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<ol class="breadcrumb bc-3" >
    <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
    </li>
    <li>
        <a href="<?= base_url('admin/relay-point'); ?>">Relay Point</a>
    </li>
    <li class="active">
        <strong>Edit</strong>
    </li>
</ol>

<div class="row">
    <div class="col-md-12">
        
        <div class="panel panel-dark" data-collapsed="0">
        
            <div class="panel-heading">
                <div class="panel-title">
                    Edit Relay Point
                </div>
                
                <div class="panel-options">
                    <a href="<?= base_url('admin/relay-point'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
                </div>
            </div>
            
            <div class="panel-body">

                <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                <?php endif; ?>

                <div class="well well-sm hidden">
                    <h6>Please fill the details to create Relay Point.</h6>
                </div>
                
                <form id="rootwizard-2" method="post" action="<?= base_url('admin/update-relay-point-details'); ?>" class="form-wizard validate" enctype="multipart/form-data" style="margin-top: -35px;" >
                    <input type="hidden" name="relay_id" value="<?= $relay_details['relay_id'] ?>">
                    <input type="hidden" name="cd_id" value="<?= $relay_details['relay_mgr_id'] ?>">
                    <div class="steps-progress hidden">
                        <div class="progress-indicator"></div>
                    </div>
                    
                    <ul class="hidden">
                        <li class="active">
                            <a href="#tab2-1" data-toggle="tab"><span>1</span>Relay Point Manager</a>
                        </li>
                        <li>
                            <a href="#tab2-2" data-toggle="tab"><span>2</span>Relay Point</a>
                        </li>
                        <li>
                            <a href="#tab2-3" data-toggle="tab"><span>3</span>Warehouse</a>
                        </li>
                        <li>
                            <a href="#tab2-5" data-toggle="tab"><span>5</span>Finish</a>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab2-1">
                            <div class="row">
                                <div class="col-md-4 form-group" style="margin-bottom: 5px;">
                                    <label class="">First Name</label>
                                    <input type="text" " class="form-control" placeholder="First Name" name="rfirstname" id="rfirstname" value="<?= $relay_details['firstname'] ?>">
                                </div>
                                <div class="col-md-4 form-group" style="margin-bottom: 5px;">
                                    <label class="">Last Name</label>
                                    <input type="text" class="form-control" id="" placeholder="Last Name" name="rlastname" id="rlastname" value="<?= $relay_details['lastname'] ?>">
                                </div> 
                                <div class="col-md-4 form-group" style="margin-bottom: 5px;">
                                    <label class="">Company Name</label>
                                    <input type="text" class="form-control" id="" placeholder="Company Name" name="rcomapny_name" id="rcomapny_name" value="<?= $relay_details['comapny_name'] ?>">
                                </div> 
                            </div>
                            <div class="row">
                                
                                <div class="col-md-3 form-group" style="margin-bottom: 5px;">
                                    <label class="">Mobile Number</label>
                                    <input type="text" class="form-control" id="" placeholder="Mobile Number" name="rmobile_no" id="rmobile_no" value="<?= $relay_details['mobile_no'] ?>">
                                </div>
                                <div class="col-md-3 form-group" style="margin-bottom: 5px;">
                                    <label class="">Country</label>
                                    <select id="rcountry_id" name="rcountry_id" class="form-control" data-allow-clear="true" data-placeholder="Select Country">
                                      <option value="">Select Country</option>
                                      <?php foreach ($countries as $country): ?>
                                          <option value="<?= $country['country_id'] ?>" <?= $relay_details['country_id'] == $country['country_id'] ? 'selected' : '' ?> ><?= $country['country_name']; ?></option>
                                      <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group" style="margin-bottom: 5px;">
                                    <label class="">State</label>
                                    <select id="rstate_id" name="rstate_id" class="form-control" data-allow-clear="true" data-placeholder="Select State" disabled>
                                        <option value="<?= $relay_details['state_id'] ?>"><?= $this->relay->get_state_name_by_id($relay_details['state_id']) ?></option>
                                    </select>
                                    <input type="hidden" name="hrstate_id" value="<?= $relay_details['state_id'] ?>">
                                </div>
                                <div class="col-md-3 form-group" style="margin-bottom: 5px;">
                                    <label class="">City</label>
                                    <select id="rcity_id" name="rcity_id" class="form-control" data-allow-clear="true" data-placeholder="Select City" disabled>
                                      <option value="<?= $relay_details['city_id'] ?>"><?= $this->relay->get_city_name_by_id($relay_details['city_id']) ?></option>
                                    </select>
                                    <input type="hidden" name="hrcity_id" value="<?= $relay_details['city_id'] ?>">                             
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <label class="">No and Street Name</label>
                                    <input type="text" class="form-control" id="" placeholder="No and Street Name" name="rstreet_name" id="rstreet_name" value="<?= $relay_details['street_name'] ?>"><br />
                                    <label class="">District or address line1</label>
                                    <textarea placeholder="District or address line1" class="form-control" name="raddr_line1" id="raddr_line1"><?= $relay_details['addr_line1'] ?></textarea><br />
                                    <label class="">Zip</label>
                                    <input type="text" class="form-control" id="" placeholder="Zip" name="rzipcode" id="rzipcode" value="<?= $relay_details['zipcode'] ?>"><br />
                                    <label class="">Business Domain</label>
                                    <select class="form-control" name="rbusiness_type" id="rbusiness_type">
                                        <option value="">Select Business Domain</option>
                                        <option value="store" <?= $relay_details['business_type'] == 'store' ? 'selected' : ''; ?> >Store</option>
                                        <option value="gas station" <?= $relay_details['business_type'] == 'gas station' ? 'selected' : ''; ?> >Gas Station</option>
                                        <option value="business" <?= $relay_details['business_type'] == 'business' ? 'selected' : ''; ?> >Business</option>
                                        <option value="cybercafé" <?= $relay_details['business_type'] == 'cybercafé' ? 'selected' : ''; ?> >Cybercafé</option>
                                        <option value="hairdresser" <?= $relay_details['business_type'] == 'hairdresser' ? 'selected' : ''; ?> >Hairdresser</option>
                                        <option value="individual" <?= $relay_details['business_type'] == 'individual' ? 'selected' : ''; ?> >Individual</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <label class="">Pick Address From Map</label>
                                    <!--<textarea placeholder="Address Line 2" class="form-control" name="raddr_line2" id="raddr_line2"></textarea>-->
                                    <input type="text" class="form-control" id="us3-address" name="raddr_line2" placeholder="<?= $this->lang->line('pick_from_map'); ?>" value="<?= $relay_details['addr_line2'] ?>" />
                                    <div class="row">
                                        <div class="form-group" style="margin-bottom: 5px;">
                                          <div class="col-lg-12">
                                            <div id="us3" style="width: 100%; height: 250px;"></div>
                                            <input type="text" class="form-control hidden" id="us3-radius" />
                                            <input type="text" class="form-control hidden" id="us3-lat" name="latitude" value="0.0" value="<?= $relay_details['latitude'] ?>" />
                                            <input type="text" class="form-control hidden" id="us3-lon" name="longitude" value="0.0" value="<?= $relay_details['longitude'] ?>" />
                                          </div>
                                        </div>
                                    </div>
                                    <script>
                                        
                                            $('#us3').locationpicker({
                                                location: {
                                                    latitude: <?= $relay_details['latitude'] ?>,
                                                    longitude: <?= $relay_details['longitude'] ?>,
                                                },
                                                radius: 50,
                                                inputBinding: {
                                                    latitudeInput: $('#us3-lat'),
                                                    longitudeInput: $('#us3-lon'),
                                                    radiusInput: $('#us3-radius'),
                                                    locationNameInput: $('#us3-address')
                                                },
                                                enableAutocomplete: true,
                                                onchanged: function (currentLocation, radius, isMarkerDropped) {
                                                    //Uncomment line below to show alert on each Location Changed event
                                                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                                                },
                                            });
                                    </script>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <label class="">Pickup Instructions</label>
                                    <textarea placeholder="Pickup Instructions" class="form-control" name="pickup_instructions" id="pickup_instructions"><?= $relay_details['pickup_instructions'] ?></textarea>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <label class="">Deliver Instructions</label>
                                    <textarea placeholder="Delivery Instructions" class="form-control" name="deliver_instructions" id="deliver_instructions"><?= $relay_details['deliver_instructions'] ?></textarea>
                                </div>
                            </div>
                            <div class="row">
                            	<div class="col-md-2">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 140px; height: 100px;" data-trigger="fileinput">
                                            <?= $relay_details['image_url'] != 'NULL' ? '<img src="'.base_url().$relay_details["image_url"].'" alt="Profile Picture">': '<img src="http://placehold.it/200x150" alt="Profile Picture">' ?>
                                            
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 140px; max-height: 100px"></div>
                                        <div class="text-center">
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="rimage_url" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 form-group" style="margin-bottom: 5px;">
                                    <label class="">IBAN</label>
                                    <input type="text" class="form-control" id="" placeholder="IBAN" name="iban" id="iban" value="<?= $relay_details['iban'] ?>">
                                </div>
                                <div class="col-md-5 form-group" style="margin-bottom: 5px;">
                                    <label class="">Mobile Money Account 1</label>
                                    <input type="text" class="form-control" id="" placeholder="Mobile Money Account" name="mobile_money_acc1" id="mobile_money_acc1" value="<?= $relay_details['mobile_money_acc1'] ?>">
                                </div>
                                <div class="col-md-5 form-group" style="margin-bottom: 5px;">
                                    <label class="">Mobile Money Account 2 (optional)</label>
                                    <input type="text" class="form-control" id="" placeholder="Mobile Money Account" name="mobile_money_acc2" id="mobile_money_acc2" value="<?= $relay_details['mobile_money_acc2'] ?>" >
                                </div>
                                <div class="col-md-5 form-group" style="margin-bottom: 5px;">
                                    <label class="">Relay Point Comission</label>
                                    <input type="number" class="form-control" id="relay_commission" placeholder="%" name="relay_commission" id="relay_commission" value="<?= $relay_details['relay_commission'] ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group" style="margin-bottom: 5px;">
                                    <label class="">Days and Open Hours</label>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Monday</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="checkbox" id="monday_toggle" name="monday_toggle" data-size="mini" data-width="60" class="myToggle" <?= $relay_details['monday_toggle'] == 1 ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="monday_open" <?= $relay_details['monday_toggle'] == 0 ? 'disabled' : ''; ?> id="monday_open">
                                            <option>Opening Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['monday_open'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="monday_close" <?= $relay_details['monday_toggle'] == 0 ? 'disabled' : ''; ?> id="monday_close">
                                            <option>Closing Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['monday_close'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Tuesday</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="checkbox" id="tuesday_toggle" name="tuesday_toggle" data-size="mini" data-width="60" class="myToggle" <?= $relay_details['tuesday_toggle'] == 1 ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="tuesday_open" <?= $relay_details['tuesday_toggle'] == 0 ? 'disabled' : ''; ?> id="tuesday_open">
                                            <option>Opening Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['tuesday_open'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="tuesday_close" <?= $relay_details['tuesday_toggle'] == 0 ? 'disabled' : ''; ?> id="tuesday_close">
                                            <option>Closing Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['tuesday_close'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Wednesday</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="checkbox" id="wednesday_toggle" name="wednesday_toggle" data-size="mini" data-width="60" class="myToggle" <?= $relay_details['wednesday_toggle'] == 1 ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="wednesday_open" <?= $relay_details['wednesday_toggle'] == 0 ? 'disabled' : ''; ?> id="wednesday_open">
                                            <option>Opening Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['wednesday_open'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="wednesday_close" <?= $relay_details['wednesday_toggle'] == 0 ? 'disabled' : ''; ?> id="wednesday_close">
                                            <option>Closing Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['wednesday_close'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Thursday</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="checkbox" id="thursday_toggle" name="thursday_toggle" data-size="mini" data-width="60" class="myToggle" <?= $relay_details['thursday_toggle'] == 1 ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="thursday_open" <?= $relay_details['thursday_toggle'] == 0 ? 'disabled' : ''; ?> id="thursday_open">
                                            <option>Opening Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['thursday_open'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="thursday_close" <?= $relay_details['thursday_toggle'] == 0 ? 'disabled' : ''; ?> id="thursday_close">
                                            <option>Closing Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['thursday_close'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Friday</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="checkbox" id="friday_toggle" name="friday_toggle" data-size="mini" data-width="60" class="myToggle" <?= $relay_details['friday_toggle'] == 1 ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="friday_open" <?= $relay_details['friday_toggle'] == 0 ? 'disabled' : ''; ?> id="friday_open">
                                            <option>Opening Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['friday_open'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="friday_close" <?= $relay_details['friday_toggle'] == 0 ? 'disabled' : ''; ?> id="friday_close">
                                            <option>Closing Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['friday_close'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Saturday</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="checkbox" id="saturday_toggle" name="saturday_toggle" data-size="mini" data-width="60" class="myToggle" <?= $relay_details['saturday_toggle'] == 1 ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="saturday_open" <?= $relay_details['saturday_toggle'] == 0 ? 'disabled' : ''; ?> id="saturday_open">
                                            <option>Opening Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['saturday_open'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="saturday_close" <?= $relay_details['saturday_toggle'] == 0 ? 'disabled' : ''; ?> id="saturday_close">
                                            <option>Closing Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['saturday_close'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Sunday</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="checkbox" id="sunday_toggle" name="sunday_toggle" data-size="mini" data-width="60" class="myToggle" <?= $relay_details['sunday_toggle'] == 1 ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="sunday_open" <?= $relay_details['sunday_toggle'] == 0 ? 'disabled' : ''; ?> id="sunday_open">
                                            <option>Opening Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['sunday_open'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                        <select class="form-control" name="sunday_close" <?= $relay_details['sunday_toggle'] == 0 ? 'disabled' : ''; ?> id="sunday_close">
                                            <option>Closing Hrs</option>
                                            <?php
                                                $tNow = $tStart = strtotime("00:00");
                                                $tEnd = strtotime("23:59");
                                                while($tNow < $tEnd){ ?>
                                                  <option value="<?= date("H:i",$tNow) ?>" <?= date("H:i",$tNow) == $relay_details['sunday_close'] ? 'selected' : ''; ?> ><?= date("H:i",$tNow) ?></option>
                                                <?php
                                                  $tNow = strtotime('+30 minutes',$tNow);
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-primary">Save Details</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br />

<script>
    $("#rootwizard-2")
        .validate({
        rules: {
            rfirstname: { required : true, },
            rlastname: { required : true, },
            rcomapny_name: { required : true, },
            rmobile_no: { required : true, number : true, },
            rcountry_id: { required : true, },
            rstate_id: { required : true, },
            rcity_id: { required : true, },
            raddr_line2: { required : true, },
            rstreet_name: { required : true, },
            iban: { required : true, },
            mobile_money_acc1: { required : true, },
            deliver_instructions: { required : true, },
            pickup_instructions: { required : true, },
            relay_commission: { required : true, number : true, min:0, max:50 },
        },
        messages: {
            rfirstname: { required : "Please enter first name!", },
            rlastname: { required : "Please enter last name!", },
            rcomapny_name: { required : "Please enter company name!", },
            rmobile_no: { required : "Please enter mobile number!", number: "Numbers only!" },
            rcountry_id: { required : "Please select country!", },
            rstate_id: { required : "Please select state!", },
            rcity_id: { required : "Please select city!", },
            raddr_line2: { required : "Please select address on map!", },
            rstreet_name: { required : "Please enter street name!", },
            iban: { required : "Please enter IBAN!", },
            mobile_money_acc1: { required : "Please enter Mobile Money account!", },
            deliver_instructions: { required : "Please enter delivery instructions!", },
            pickup_instructions: { required : "Please enter pickup instructions!", },
            relay_commission: { required : "Please enter relay point commission!", number: "Numbers only!", min: "Percentage should be zero or more!", max: "Percentage should not more than 50!", },
        },
    });
</script>

<script>
    $("#rcountry_id").on('change', function(event) {  event.preventDefault();
        var country_id = $(this).val();
        console.log(country_id);
        $('#city_id').attr('disabled', true);
        $.ajax({
          url: "<?= base_url('admin/get-state-by-country-id') ?>",
          type: "POST",
          data: { country_id: $.trim(country_id) },
          dataType: "json",
          success: function(res){
            console.log(res);
            $('#rstate_id').attr('disabled', false);
            $('#rstate_id').empty();
            $('#rstate_id').append('<option value="0">Select State</option>');
            $.each( res, function(){$('#rstate_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
            $('#rstate_id').focus();
          },
          beforeSend: function(){
            $('#rstate_id').empty();
            $('#rstate_id').append('<option value="0">Loading...</option>');
          },
          error: function(){
            $('#rstate_id').attr('disabled', true);
            $('#rstate_id').empty();
            $('#rstate_id').append('<option value="0">No Options</option>');
          }
        })
    });

    $("#rstate_id").on('change', function(event) {  event.preventDefault();
        var state_id = $(this).val();
        $.ajax({
          type: "POST",
          url: "<?= base_url('admin/get-cities-by-state-id') ?>",
          data: { state_id: state_id },
          dataType: "json",
          success: function(res){
            $('#rcity_id').attr('disabled', false);
            $('#rcity_id').empty();
            $('#rcity_id').append('<option value="0">Select city</option>');
            $.each( res, function(){ $('#rcity_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
            $('#rcity_id').focus();
          },
          beforeSend: function(){
            $('#rcity_id').empty();
            $('#rcity_id').append('<option value="0">Loading...</option>');
          },
          error: function(){
            $('#rcity_id').attr('disabled', true);
            $('#rcity_id').empty();
            $('#rcity_id').append('<option value="0">No Options</option>');
          }
        })
    });
</script>

<script>
    $(function() {
        $('.myToggle').bootstrapToggle({
          on: 'Open',
          off: 'Close'
        });
    });

    $("#monday_toggle").change(function() {
        if(this.checked) {
            $('#monday_open').prop('disabled', false);
            $('#monday_close').prop('disabled', false);
        } else {
            $('#monday_open').prop('disabled', 'disabled');
            $('#monday_close').prop('disabled', 'disabled');
        }
    });
    $("#tuesday_toggle").change(function() {
        if(this.checked) {
            $('#tuesday_open').prop('disabled', false);
            $('#tuesday_close').prop('disabled', false);
        } else {
            $('#tuesday_open').prop('disabled', 'disabled');
            $('#tuesday_close').prop('disabled', 'disabled');
        }
    });
    $("#wednesday_toggle").change(function() {
        if(this.checked) {
            $('#wednesday_open').prop('disabled', false);
            $('#wednesday_close').prop('disabled', false);
        } else {
            $('#wednesday_open').prop('disabled', 'disabled');
            $('#wednesday_close').prop('disabled', 'disabled');
        }
    });
    $("#thursday_toggle").change(function() {
        if(this.checked) {
            $('#thursday_open').prop('disabled', false);
            $('#thursday_close').prop('disabled', false);
        } else {
            $('#thursday_open').prop('disabled', 'disabled');
            $('#thursday_close').prop('disabled', 'disabled');
        }
    });
    $("#friday_toggle").change(function() {
        if(this.checked) {
            $('#friday_open').prop('disabled', false);
            $('#friday_close').prop('disabled', false);
        } else {
            $('#friday_open').prop('disabled', 'disabled');
            $('#friday_close').prop('disabled', 'disabled');
        }
    });
    $("#saturday_toggle").change(function() {
        if(this.checked) {
            $('#saturday_open').prop('disabled', false);
            $('#saturday_close').prop('disabled', false);
        } else {
            $('#saturday_open').prop('disabled', 'disabled');
            $('#saturday_close').prop('disabled', 'disabled');
        }
    });
    $("#sunday_toggle").change(function() {
        if(this.checked) {
            $('#sunday_open').prop('disabled', false);
            $('#sunday_close').prop('disabled', false);
        } else {
            $('#sunday_open').prop('disabled', 'disabled');
            $('#sunday_close').prop('disabled', 'disabled');
        }
    });

    $('#monday_open').on('change', function() {
        var open = timeStringToFloat($('#monday_open').val());
        $("#monday_close option").each(function() {
            $('#monday_close option').prop('disabled', false);
        });
        $("#monday_close option").each(function() {
            var close = timeStringToFloat($(this).val());
            var close_normal = $(this).val();
            if(close <= open) {
                $("#monday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
            }
        });
    });
    $('#tuesday_open').on('change', function() {
        var open = timeStringToFloat($('#tuesday_open').val());
        $("#tuesday_close option").each(function() {
            $('#tuesday_close option').prop('disabled', false);
        });
        $("#tuesday_close option").each(function() {
            var close = timeStringToFloat($(this).val());
            var close_normal = $(this).val();
            if(close <= open) {
                $("#tuesday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
            }
        });
    });
    $('#wednesday_open').on('change', function() {
        var open = timeStringToFloat($('#wednesday_open').val());
        $("#wednesday_close option").each(function() {
            $('#wednesday_close option').prop('disabled', false);
        });
        $("#wednesday_close option").each(function() {
            var close = timeStringToFloat($(this).val());
            var close_normal = $(this).val();
            if(close <= open) {
                $("#wednesday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
            }
        });
    });
    $('#thursday_open').on('change', function() {
        var open = timeStringToFloat($('#thursday_open').val());
        $("#thursday_close option").each(function() {
            $('#thursday_close option').prop('disabled', false);
        });
        $("#thursday_close option").each(function() {
            var close = timeStringToFloat($(this).val());
            var close_normal = $(this).val();
            if(close <= open) {
                $("#thursday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
            }
        });
    });
    $('#friday_open').on('change', function() {
        var open = timeStringToFloat($('#friday_open').val());
        $("#friday_close option").each(function() {
            $('#friday_close option').prop('disabled', false);
        });
        $("#friday_close option").each(function() {
            var close = timeStringToFloat($(this).val());
            var close_normal = $(this).val();
            if(close <= open) {
                $("#friday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
            }
        });
    });
    $('#saturday_open').on('change', function() {
        var open = timeStringToFloat($('#saturday_open').val());
        $("#saturday_close option").each(function() {
            $('#saturday_close option').prop('disabled', false);
        });
        $("#saturday_close option").each(function() {
            var close = timeStringToFloat($(this).val());
            var close_normal = $(this).val();
            if(close <= open) {
                $("#saturday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
            }
        });
    });
    $('#sunday_open').on('change', function() {
        var open = timeStringToFloat($('#sunday_open').val());
        $("#sunday_close option").each(function() {
            $('#sunday_close option').prop('disabled', false);
        });
        $("#sunday_close option").each(function() {
            var close = timeStringToFloat($(this).val());
            var close_normal = $(this).val();
            if(close <= open) {
                $("#sunday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
            }
        });
    });

    function timeStringToFloat(time) {
        var hoursMinutes = time.split(/[.:]/);
        var hours = parseInt(hoursMinutes[0], 10);
        var minutes = hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0;
        return hours + minutes / 60;
    }
</script>