<style type="text/css">
.profile-env section.profile-feed .profile-post-form .form-options {padding-bottom: 0px; }
</style>
		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li>
				<a href="<?= base_url('admin/users'); ?>">Users List</a>
			</li>
			<li class="active">
				<strong>View</strong>
			</li>
		</ol>
	
		<div class="profile-env">
			
			<header class="row">
				
				<div class="col-sm-2">
					<?php if($users_detail['avatar_url'] != "NULL"): ?>
						<a class="profile-picture">
							<img src="<?= $this->config->item('base_url') . $users_detail['avatar_url'];?>" class="img-responsive img-circle" />
						</a>
					<?php else: ?>
						<a class="profile-picture">
							<img src="<?= $this->config->item('resource_url') . 'default-profile.jpg';?>" class="img-responsive img-circle" />
						</a>
					<?php endif; ?>
					
				</div>
				
				<div class="col-sm-10">
					
					<ul class="profile-info-sections">
						<li>
							<div class="profile-name">
								<strong> <?php $email_cut = explode('@', $users_detail['email1']);  $name = $email_cut[0]; ?>
									<a>
										<?= $users_detail['firstname'] != "NULL" ? $users_detail['firstname'] . ' ' . $users_detail['lastname'] : ($users_detail['company_name'] != "NULL" ? ucwords($users_detail['company_name']) : ucwords($name)) ?></a>
										<?php if( $users_detail['cust_status'] == 1): ?>
											<a class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online">
										<?php endif; ?></a>
								</strong>
									<span>
										<?php if( strtolower($users_detail['gender']) === 'm'): ?>
											<a><i class="fa fa-male"></i> Male</a>
										<?php else: ?>
											<a><i class="fa fa-female"></i> Female</a>
										<?php endif; ?>
									</span>
							</div>
						</li>
						
						<li>
							<div class="profile-stat">
								<h6><i class="fa fa-envelope-o"></i> Primary Email Address</h6>
								<h4><?php if($users_detail['email1'] !== "NULL") echo $users_detail['email1']; else echo 'Not provided!';  ?></h4>
							</div>
						</li>
						<li>
							<div class="profile-stat">
								<h6><i class="fa fa-phone"></i> Primary Mobile Number</h6>
								<h4><?php if($users_detail['mobile1'] !== "NULL") echo '+'.$users_country['country_phonecode'].'-'.$users_detail['mobile1']; else echo 'Not provided!';  ?></h4>
							</div>
						</li>
					</ul>
				</div>
				
			</header>
			
			<section class="profile-info-tabs">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-10">
						<ul class="user-details">
							<li>
								<a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Address">
									<i class="entypo-location"></i>
									<?php if($users_detail['city_id'] !== "NULL") echo $users_city['city_name']; else echo ' ';  ?>
									<?php if($users_detail['state_id'] !== "NULL") echo $users_state['state_name']; else echo ' ';  ?>
									<?php if($users_detail['country_id'] !== "NULL") echo $users_country['country_name']; else echo ' ';  ?>
								</a>
							</li>
							<?php if($users_detail['profession'] !== "NULL"): ?>
								<li><a><i class="entypo-suitcase"></i>Works as - <span> <?=  $users_detail['profession']; ?></span></a></li>
							<?php endif;?>
							<li>
								<a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Date of Birth">
									<i class="fa fa-birthday-cake"></i>
									<?php if($users_detail['cust_dob'] !== "NULL") echo date('l, d M Y', strtotime($users_detail['cust_dob'])); else echo 'Not provided!'; ?>
								</a>
							</li>
						</ul>
						
						<!-- tabs for the profile links -->
						<ul class="nav nav-tabs">
							<li class="active tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Send Notification">
								<a data-toggle="tab" style="padding: 5px 20px;" href="#notification">Send Notification</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Personal Details">
								<a data-toggle="tab" style="padding: 5px 20px;" href="#personal">Personal</a>
							</li>
							<?php if(is_array($deliverer_detail)) { ?>
								<?php if($users_detail['acc_type'] == 'both' || $users_detail['acc_type'] == 'seller'): ?>
									<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Business Details">
									<a data-toggle="tab" style="padding: 5px 20px;" href="#business">Business</a>
									</li>
								<?php endif; ?>
							<?php } ?>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Skills">
								<a data-toggle="tab" style="padding: 5px 20px;" href="#skills">Skills</a>
							</li>

							<?php if(is_array($deliverer_detail)) { ?>
								<?php if($users_detail['acc_type'] == 'both' || $users_detail['acc_type'] == 'seller'): ?>
									<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Wallet Details">
									<a data-toggle="tab" style="padding: 5px 20px;" href="#wallet">Wallet</a>
									</li>
								<?php endif; ?>
							<?php } ?>
							<?php if(is_array($deliverer_detail)) { ?>
								<?php if($users_detail['acc_type'] == 'both' || $users_detail['acc_type'] == 'seller'): ?>
									<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Scrow Details">
									<a data-toggle="tab" style="padding: 5px 20px;" href="#scrow">Scrow</a>
									</li>
								<?php endif; ?>
							<?php } ?>
							<?php if(is_array($deliverer_detail)) { ?>
								<?php if($users_detail['acc_type'] == 'both' || $users_detail['acc_type'] == 'seller'): ?>
									<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Withdrawals Details">
									<a data-toggle="tab" style="padding: 5px 20px;" href="#withdrawals">Withdrawals</a>
									</li>
								<?php endif; ?>
							<?php } ?>						
						</ul>
					</div>
				</div>
			</section>

			<div class="tab-content">

				<div id="notification" class="tab-pane fade in active">
					<section class="profile-feed">

						<?php if($this->session->flashdata('error')): ?>
				      	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
				    <?php elseif($this->session->flashdata('success')):  ?>
				    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
				    <?php endif; ?>

						<form action="<?= base_url('admin/send-personal-notification'); ?>" class="profile-post-form" method="post" autocomplete="off" enctype="multipart/form-data" id="send_notification">
							<input type="hidden" name="user_id" value="<?= $users_detail['cust_id']; ?>">
							<input type="text" class="form-control" id="title" placeholder="Enter notification title..." name="title" autofocus style="resize: none;" rows="7" />
							<textarea class="form-control autogrow" placeholder="Enter notification message..." name="message" id="message"></textarea>
							<div class="form-options">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<span class="btn btn-info btn-file">
										<span class="fileinput-new "><i class="entypo-attach"></i> Attachment</span>
										<span class="fileinput-exists"><i class="entypo-cancel"></i> Change</span>
										<input type="file" name="attachement">
									</span>
									<span class="fileinput-filename"></span>
									<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
								</div>
								<div class="post-submit">
									<button type="submit" class="btn btn-primary tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Click to send Notification"><i class="fa fa-send"></i>&nbsp; Send Notification</button>
								</div>
							</div>
						</form>
					</section>
				</div>
				
				<div id="personal" class="tab-pane fade">
					<section class="profile-feed col-md-10 col-md-offset-1">
						<div class="profile-stories">
					
							<article class="story">								
								<div class="story-content">									
									<div class="col-md-4"><i class="fa fa-user"></i> Profession: </div>
									<div class="col-md-8"><?= ($users_detail['profession'] !="NULL") ? strtolower($users_detail['profession']):"Not Provided!"; ?></div>
									<hr>
								</div>

								<div class="story-content">									
									<div class="col-md-4"><i class="fa fa-envelope-o"></i> Email Address: </div>
									<div class="col-md-8">
										<?= $users_detail['email1'].' ( Primary )'; ?>
										<?php 
											if($users_detail['email_verified'] == 1) {	echo '<i class="fa fa-check  text-success tooltip-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email Address Verified!"></i>'; }
											else { echo '<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email Address Not Verified!" style="color:#FF0000;"></i>'; } 
										?>
									</div>
									<hr>
								</div>
									
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-phone"></i> Phone Number: </div>
									<div class="col-md-8">
										<?= $users_detail['mobile1'].' ( Primary )'; ?>
										<?php 
											if($users_detail['mobile_verified'] == 1) {	echo '<i class="fa fa-check  text-success tooltip-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mobile Number Verified!"></i>'; }
											else { echo '<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mobile Number Not Verified!" style="color:#FF0000;"></i>'; } 
										?>
									</div>
									<hr>
								</div>
									
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-map-marker"></i> Address: </div>
									<div class="col-md-8">
										<?= ($users_city['city_name'] !="NULL") ? trim($users_city['city_name']).' ':""; ?>
										<?= ($users_state['state_name'] !="NULL") ? trim($users_state['state_name']).' ':""; ?>
										<?= ($users_country['country_name'] !="NULL") ? trim($users_country['country_name']):""; ?>
									</div>
									<hr>
								</div>
								
							</article>
							
						</div>					
					</section>
				</div>

				<div id="business" class="tab-pane fade">
					<section class="profile-feed col-md-10 col-md-offset-1">
						<div class="profile-stories">
					
							<article class="story">	
								<div class="story-content">	
									<div class="col-md-4"><i class="fa fa-envelope-o"></i> Business Email Address: </div>
									<div class="col-md-8"><?= $deliverer_detail['email_id'].' ( Primary )'; ?></div>
									<hr>
								</div>
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-phone"></i> Business Phone Number: </div>
									<div class="col-md-4"><?= $deliverer_detail['contact_no'].' ( Primary )'; ?></div>
									<hr>
								</div>
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-building"></i> Carrier Type: </div>
									<div class="col-md-4"><?= ucwords($deliverer_detail['carrier_type']); ?></div>
									<hr>
								</div>
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-map-marker"></i> Business Address: </div>
									<div class="col-md-8"><?= ($deliverer_detail['address'] !="NULL") ? trim($deliverer_detail['address']):"Not Provided!"; ?>
									<br/>
										<?= $this->user->get_city_name_by_id($deliverer_detail['city_id']) . ' / '; ?>
										<?= $this->user->get_state_name_by_id($deliverer_detail['state_id']) . ' / '; ?>
										<?= $this->user->get_country_name_by_id($deliverer_detail['country_id']); ?>
									</div>
									<br/>
									<hr> 
								</div>
								<div class="story-content hidden">
									<div class="col-md-4"><i class="fa fa-anchor"></i> Shipping Mode: </div>
									<div class="col-md-8"><?= $deliverer_detail['shipping_mode'] == 0 ? 'Manual' : 'Automatic' ; ?></div>
									<hr>
								</div>
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-anchor"></i> Change Shipping Mode: </div>
									<div class="col-md-8">
										<input type="hidden" name="deliverer_id" id="deliverer_id" value="<?= $deliverer_detail['deliverer_id'] ?>">
										<select class="form-control" style="width: 100%" name="shipping_mode" id="shipping_mode">
						                  <option value="1" <?php if($deliverer_detail['shipping_mode'] == 1) { echo 'selected'; } ?> >Automatic</option>
						                  <option value="0" <?php if($deliverer_detail['shipping_mode'] == 0) { echo 'selected'; } ?> >Manual</option>
						                </select><br />
						                <button type="submit" class="btn btn-info" id="btn_shipping_mode" onclick="change_shipping_mode();">Save Changes</button> 
									</div>
									<hr>
								</div>
	
							</article>
						</div>	
					</section>
				</div>

				<div id="skills" class="tab-pane fade">
					<section class="profile-feed">
						<?php //var_dump($users_skill_list); ?>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Skill Level - Expert</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="padding-lg">
										<?php 
										$skill_list_expert = explode(',', $users_skill_list['expert_skills']);
										foreach ($skill_list_expert as $expert): if($expert != 'NULL'): ?>
											<div class="label label-primary"><?= $expert; ?></div>
										<?php endif; endforeach; ?>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Skill Level - Senior</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="padding-lg">
										<?php 
										$skill_list_senior = explode(',', $users_skill_list['senior_skills']);
										foreach ($skill_list_senior as $senior): if($senior != 'NULL'): ?>
											<div class="label label-primary"><?= $senior; ?></div>
										<?php endif; endforeach; ?>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Skill Level - Confirmed</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="padding-lg">
										<?php 
										$skill_list_confirmed = explode(',', $users_skill_list['confirmed_skills']);
										foreach ($skill_list_confirmed as $confirmed): if($confirmed != 'NULL'): ?>
											<div class="label label-primary"><?= $confirmed; ?></div>
										<?php endif; endforeach; ?>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Skill Level - Junior</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="padding-lg">
										<?php 
										$skill_list_junior = explode(',', $users_skill_list['junior_skills']);
										foreach ($skill_list_junior as $junior): if($junior != 'NULL'): ?>
											<div class="label label-primary"><?= $junior; ?></div>
										<?php endif; endforeach; ?>
									</td>
								</tr>
							</tbody>
						</table>
					</section>
				</div>

				<div id="wallet" class="tab-pane fade">
					<section class="profile-feed col-md-10 col-md-offset-1">
						<div class="profile-stories">
							<article class="story" style="margin-top: 0px; margin-bottom: 0px;" >	
								<form action="<?= $this->config->item('base_url') . 'admin/user-wallet-history'; ?>" method="POST" style="display: flex;">
									<h3>Wallet List &nbsp;&nbsp;&nbsp;</h3>
									<input type="hidden" name="cust_id" id="cust_id" value="<?= $users_detail['cust_id']; ?>">
									<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-money"></i> View All Transaction </button></h3>
								</form>
								<table class="table table-bordered table-striped datatable" id="table-1">
									<thead>
										<tr>
											<th class="text-center">Account ID</th>
											<th class="text-center">Account Balance</th>
											<th class="text-center">Update Date Time</th>
										</tr>
									</thead>
									
									<tbody>
										<?php $offset = $this->uri->segment(3,0) + 1; ?>
										<?php foreach ($wallet_balance as $wallet):  ?>
										<tr>
											<td class="text-center"><?= $wallet['account_id'] ?></td>
											<td class="text-center"><?= $wallet['currency_code'] . ' ' . $wallet['account_balance'] ?></td>
											<td class="text-center"><?= date('Y-m-d',strtotime($wallet['update_datetime'])) ?></td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</article>
						</div>	
					</section>
				</div>

				<div id="scrow" class="tab-pane fade">
					<section class="profile-feed col-md-10 col-md-offset-1">
						<div class="profile-stories">
							<article class="story" style="margin-top: 0px; margin-bottom: 0px;">	
								<form action="<?= $this->config->item('base_url') . 'admin/user-scrow-history'; ?>" method="POST" style="display: flex;">
									<h3>Wallet List &nbsp;&nbsp;&nbsp;</h3>
									<input type="hidden" name="cust_id" id="cust_id" value="<?= $users_detail['cust_id']; ?>">
									<button style="float: right" type="submit" class="btn btn-link" ><i class="fa fa-money"></i> View All Transaction </button></h3>
								</form>
								<table class="table table-bordered table-striped datatable" id="table-2">
									<thead>
										<tr>
											<th class="text-center">Scrow ID</th>
											<th class="text-center">Account Balance</th>
											<th class="text-center">Update Date Time</th>
										</tr>
									</thead>
									
									<tbody>
										<?php $offset = $this->uri->segment(3,0) + 1; ?>
										<?php foreach ($scrow_balance as $scrow):  ?>
										<tr>
											<td class="text-center"><?= $scrow['scrow_id'] ?></td>
											<td class="text-center"><?= $scrow['currency_code'] . ' ' . $scrow['scrow_balance'] ?></td>
											<td class="text-center"><?= date('Y-m-d',strtotime($scrow['update_datetime'])) ?></td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</article>
						</div>	
					</section>
				</div>
				
				<div id="withdrawals" class="tab-pane fade">
					<section class="profile-feed col-md-10 col-md-offset-1">
						<div class="profile-stories">
					
							<article class="story" style="margin-top: 0px; margin-bottom: 0px;">	
								<h3>Withdrawals List</h3>
								<table class="table table-bordered table-striped datatable" id="table-3">
									<thead>
										<tr>
											<th class="text-center">#</th>
											<th class="text-center">Date</th>
											<th class="text-center">Amount</th>
											<th class="text-center">Currency</th>
											<th class="text-center">Status</th>
											<th class="text-center">Account Type</th>
										</tr>
									</thead>
									
									<tbody>
										<?php $offset = $this->uri->segment(3,0) + 1; ?>
										<?php foreach ($withdraw_list as $withdraw):  ?>
										<tr>
											<td class="text-center"><?= $offset++; ?></td>
											<td class="text-center"><?= date('Y-m-d',strtotime($withdraw['req_datetime'])) ?></td>
											<td class="text-center"><?= $withdraw['amount'] ?></td>
											<td class="text-center"><?= $withdraw['currency_code'] ?></td>
											<td class="text-center">
												<?php 
													if($withdraw['response'] == 'reject') { $status  = 1; echo '<span class="text-danger"> Rejected </span>'; }
													else if($withdraw['response'] == 'accept') { $status  = 1;  echo '<span class="text-success"> Approved </span>'; } 
													else { $status  = 0;  echo '<span class="text-warning"> Pending </span>'; }
												?> 
											</td>
											<td class="text-center"><?= $withdraw['transfer_account_type'] == 'iban' ? 'IBAN' : 'Mobile Money Account'?></td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
	
							</article>
						</div>	
					</section>
				</div>

			</div>
		</div>
		<br />

	<script type="text/javascript">
		function change_shipping_mode(){
			var deliverer_id = $("#deliverer_id").val();
			var shipping_mode = $("#shipping_mode").val();
			swal({
				title: 'Are you sure?',
				text: "You want to change shipping mode?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, change it!',
				cancelButtonText: 'No, cancel!',
				confirmButtonClass: 'btn btn-success',
				cancelButtonClass: 'btn btn-danger',
				buttonsStyling: true,
			}).then(function () {
				$.post("<?= base_url('users/update-shipping-mode') ?>", { deliverer_id: deliverer_id, shipping_mode: shipping_mode }, function(res){
					if($.trim(res) == 'success'){
						swal(
						    'Changed!',
						    'Shipping mode has been changed.',
						    'success'
					  	). then(function(){ 	window.location.reload();  }); 
					}
					else {
						swal(
						    'Failed!',
						    'Unable to change shipping mode, Try again...!',
						    'error'
					  	)
					}
				});	
			}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
		}
	</script>

	<script type="text/javascript">
		jQuery( document ).ready( function( $ ) {
			var $table2 = jQuery( '#table-1' );
			// Initialize DataTable
			$table2.DataTable( {
				"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bStateSave": true
			});
			// Initalize Select Dropdown after DataTables is created
			$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
				minimumResultsForSearch: -1
			});
			// Highlighted rows
			$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
				var $this = $(el),
					$p = $this.closest('tr');
				$( el ).on( 'change', function() {
					var is_checked = $this.is(':checked');
					$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
				} );
			} );
			// Replace Checboxes
			$table2.find( ".pagination a" ).click( function( ev ) {
				replaceCheckboxes();
			} );
		} );
		jQuery( document ).ready( function( $ ) {
			var $table2 = jQuery( '#table-2' );
			// Initialize DataTable
			$table2.DataTable( {
				"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bStateSave": true
			});
			// Initalize Select Dropdown after DataTables is created
			$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
				minimumResultsForSearch: -1
			});
			// Highlighted rows
			$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
				var $this = $(el),
					$p = $this.closest('tr');
				$( el ).on( 'change', function() {
					var is_checked = $this.is(':checked');
					$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
				} );
			} );
			// Replace Checboxes
			$table2.find( ".pagination a" ).click( function( ev ) {
				replaceCheckboxes();
			} );
		} );
		jQuery( document ).ready( function( $ ) {
			var $table2 = jQuery( '#table-3' );
			// Initialize DataTable
			$table2.DataTable( {
				"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bStateSave": true
			});
			// Initalize Select Dropdown after DataTables is created
			$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
				minimumResultsForSearch: -1
			});
			// Highlighted rows
			$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
				var $this = $(el),
					$p = $this.closest('tr');
				$( el ).on( 'change', function() {
					var is_checked = $this.is(':checked');
					$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
				} );
			} );
			// Replace Checboxes
			$table2.find( ".pagination a" ).click( function( ev ) {
				replaceCheckboxes();
			} );
		} );
	</script>