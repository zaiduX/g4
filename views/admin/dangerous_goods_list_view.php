<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Dangerous Goods</strong>
  </li>
</ol>
      
<h2 style="display: inline-block;">Dangerous Goods</h2>
<?php
$per_dangerous_goods = explode(',', $permissions[0]['dangerous_goods']);

if(in_array('2', $per_dangerous_goods)): ?>
    <a type="button" href="<?= base_url('admin/dangerous-goods/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
      <i class="entypo-plus"></i> Add Dangerous Goods
    </a>    
<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      <th class="text-center">#</th>
      <th class="text-center">Code</th>
      <th class="text-center">Name</th>
      <th class="text-center">Description</th>
      <th class="text-center">Status</th>
      <th class="text-center">Create Date/Time</th>
      <?php if(in_array('3', $per_dangerous_goods) || in_array('5', $per_dangerous_goods)) { ?>
        <th class="text-center">Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>
    <?php $offset = $this->uri->segment(3,0) + 1; ?>
    <?php foreach ($goods as $g):  ?>      
      <tr>
        <td class="text-center"><?= $offset++; ?></td>
        <td class="text-center"><?= $g['code']; ?></td>
        <td class="text-center"><?= $g['name']; ?></td>        
        <td class="text-center"><?= implode(" ",array_slice(explode(' ', $g['description']), 0, 5)); ?></td>        
        <td class="text-center"><?= ($g['status'] == 0) ? '<label class="label label-danger"> Inactive </label>':'<label class="label label-success"> Active </label>'; ?></td>
        <td class="text-center"><?= date('d-m-Y', strtotime($g['cre_datetime'])); ?></td>
        <?php if(in_array('3', $per_dangerous_goods) || in_array('5', $per_dangerous_goods)): ?>
          <td class="text-center">
            <?php if(in_array('3', $per_dangerous_goods)): ?>            
                <a href="<?= base_url('admin/dangerous-goods/edit/') . $g['id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to View / Edit Rates"><i class="entypo-pencil"></i> View / Edit</a>
            <?php endif; ?>

            <?php if(in_array('4', $per_dangerous_goods)): ?>
              <?php if($g['status'] == 0): ?>
                <button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to activate" data-original-title="Click to activate" onclick="activate_dangerous_goods('<?= $g['id']; ?>');">
                  <i class="entypo-thumbs-up"></i>Activate</button>
              <?php else: ?>
                <button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to inactivate" data-original-title="Click to inactivate" onclick="inactivate_dangerous_goods('<?= $g['id']; ?>');">
                <i class="entypo-cancel"></i>
                Inactivate</button>     
              <?php endif; ?>
            <?php endif; ?>
          </td>
        <?php endif; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    // Highlighted rows
    $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
      var $this = $(el),
        $p = $this.closest('tr');
      
      $( el ).on( 'change', function() {
        var is_checked = $this.is(':checked');
        
        $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
      } );
    } );
    
    // Replace Checboxes
    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );    
  } );


  function inactivate_dangerous_goods(id=0){
    swal({
      title: 'Are you sure?',
      text: "You want to to inactivate this dangerous goods?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, inactivate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('dangerous-goods/inactivate', {id: id}, function(res){        
        if(res == 'success'){
          swal(
            'Inactive!',
            'Dangerous goods has been inactivated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'Dangerous goods inactivation failed.',
            'error'
          )
        }
      });
      
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

  function activate_dangerous_goods(id=0){
    swal({
      title: 'Are you sure?',
      text: "You want to to activate this dangerous goods?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, activate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('dangerous-goods/activate', {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Active!',
            'Dangerous goods has been activated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'Dangerous goods activation failed.',
            'error'
          )
        }
      });
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

</script>