<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/authority'); ?>">Authority Master</a>
	</li>
	<li class="active">
		<strong>Add New</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Add Authority
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/authority'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/register-authority'); ?>" class="form-horizontal form-groups-bordered" id="authorityAdd" method="post" autocomplete="off" enctype="multipart/form-data">

					<div class="form-group">
						
						<div class="col-sm-4 text-center">
							<label class="control-label">Upload Profile Picture</label>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
									<img src="http://placehold.it/200x150" alt="Profile Picture">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<input type="file" name="profile_image" accept="image/*">
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
							</div>
						</div>
						<div class="col-sm-8">
							
							<div class="col-sm-6">
								<label for="auth_fname" class="control-label">First Name</label>
								<input type="text" class="form-control" id="auth_fname" placeholder="Enter first name..." name="auth_fname" />	
							</div>

							<div class="col-sm-6">
								<label for="auth_lname" class="control-label">Last Name</label>
								<input type="text" class="form-control" id="auth_lname" placeholder="Enter last name..." name="auth_lname" />	
							</div>

							<div class="col-sm-12">
								<label for="auth_email" class="control-label">Email Address</label>
								<input type="email" class="form-control" id="auth_email" placeholder="Enter email address..." name="auth_email" />	
							</div>					

							<div class="col-sm-6">
								<label for="auth_pass" class="control-label">Password</label>
								<input type="password" class="form-control" id="auth_pass" placeholder="Enter password..." name="auth_pass" />	
							</div>					
							
							<div class="col-sm-6">
								<label for="auth_pass" class="control-label">Cofirm Password</label>
								<input type="password" class="form-control" id="auth_repass" placeholder="Re-write password..." name="auth_repass" />	
							</div>

							<div class="col-sm-6">							
								<label class="control-label">Select Authority Group</label>
								<select id="auth_type_id" name="auth_type_id" class="form-control select2">
									<option value="0"> Select Authority Group</option>
									<?php foreach ($auth_type_list as $auth_type): ?>											
										<option value="<?= $auth_type['auth_type_id'] ?>"><?= $auth_type['auth_type']; ?></option>
									<?php endforeach ?>
								</select>							
							</div>
							<div class="col-sm-6">							
								<label class="control-label">Select Authority Type</label>
								<select id="type" name="type" class="form-control select2">
									<option value="General">General</option>
									<option value="Headquarter">Headquarter</option>
								</select>							
							</div>
							<div class="col-sm-12">							
								<label class="control-label">Select Country</label>
								<select id="country_id" name="country_id" class="form-control select2">
									<option value="0"> Select Country</option>
									<?php foreach ($countries as $c): ?>											
										<option value="<?= $c['country_id'] ?>"><?= $c['country_name']; ?></option>
									<?php endforeach ?>
								</select>							
							</div>
							<div class="col-sm-12">							
								<br /><button type="submit" class="btn btn-blue">Add Authority</button>							
							</div>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />