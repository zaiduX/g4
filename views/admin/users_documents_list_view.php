<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Users Documents List</strong>
	</li>
</ol>
<style>
    .dropdown-menu { left: auto; right: 0 !important; }
</style>

<h2 style="display: inline-block;">Users Documents List</h2>
<?php $per_verify_doc = explode(',', $permissions[0]['verify_doc']); 
if(in_array('1', $per_verify_doc)) { ?>
	<a type="button" href="<?= base_url('admin/rejected-docs'); ?>" class="btn btn-red btn-icon icon-left hidden" style="float: right;">
		Rejected Documents
		<i class="fa fa-times"></i>
	</a>
<?php } ?>

	<?php if($this->session->flashdata('error')):  ?>
  		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	<?php endif; ?>
	<?php if($this->session->flashdata('success')):  ?>
  		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Users Name</th>
			<th class="text-center">Contact</th>
			<th class="text-center">Doc. Type</th>
			<th class="text-center">Country</th>
			<th class="text-center">Status</th>
			<th class="text-center">Uploaded at</th>
			<th class="text-center">Verified at</th>
			<?php if(in_array('1', $per_verify_doc)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; $status = 0; ?>
		<?php foreach ($users_documents_list as $docs):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= ($docs['firstname'] !="NULL") ? ($docs['firstname'] . ' ' . $docs['lastname']):"Not Provided"; ?></td>
			<td class="text-center"><?= $docs['mobile1'] ?></td>
			<td class="text-center"><?= strtoupper($docs['doc_type']) ?></td>
			<td class="text-center">
		    <?php 
  	        $country_details = $this->notice->get_country_detail($docs['country_id']);
  	        $country_name = $country_details['country_name'];
  	        echo $country_name;
		    ?>
			</td>
			<td class="text-center">
				<?php 
					if($docs['is_verified'] == 1 && $docs['is_rejected'] == 0 ) { $status = 1; echo "<span class='text-success'>Verifed</span>"; }
					else if($docs['is_verified'] == 0 && $docs['is_rejected'] == 1 ) { $status = 1; echo "<span class='text-danger'>Rejected</span>"; }
					else { $status = 0; echo "<span class='text-info'>Not Verified</span>"; }
				?>
			</td>
			<td class="text-center"><?= date('d/m/Y',strtotime($docs['upload_datetime'])); ?></td>
			<td class="text-center"><?= ($docs['verify_datetime'] !="NULL") ? date('d/m/Y',strtotime($docs['verify_datetime'])):'Not Yet'; ?></td>
			<td class="text-center">
				<div class="btn-group">
					<button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
						Action <span class="caret"></span>
					</button>
					<ul class="dropdown-menu dropdown" role="menu">
						<li>	<a href="<?= base_url() . $docs['attachement_url'] ?>"> <i class="fa fa-download"></i>	Download </a>	</li>
						<?php if(in_array('1', $per_verify_doc)): ?>
							<li class="divider"></li>
							<li>
								<a href="<?= base_url('admin/view-users-details/') . $docs['cust_id']; ?>"  style="cursor:pointer;">
									<i class="entypo-eye"></i> View
								</a>
							</li>
							<?php if($status == 0 ): ?>
							<li class="divider"></li>
							<li> 
								<a  data-toggle="tooltip" data-placement="top" title="Click to verify" data-original-title="Click to verify" onclick="verify_document('<?= $docs['doc_id']; ?>');" style="cursor:pointer;">
									<i class="fa fa-check"></i>	Verify
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a data-toggle="tooltip" data-placement="top" title="Click to reject" data-original-title="Click to reject" onclick="reject_document('<?= $docs['doc_id']; ?>');" style="cursor:pointer;">
									<i class="fa fa-times"></i>	Reject
							 	</a>
							</li>
						<?php endif; ?>
					<?php endif; ?>
					</ul>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );
	} );

	function verify_document(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to verify this document?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, verify it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('verify-it', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Verified!',
				    'Document has been verified.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Document verification failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function reject_document(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to reject this document?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, reject it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('reject-it', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Rejected!',
				    'Document has been rejected.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Document rejection failed.',
				    'error'
				  )
				}
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>