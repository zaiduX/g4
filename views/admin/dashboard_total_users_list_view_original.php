<style type="text/css">
  .datepicker-dropdown{
    width: 210px;
  }
</style>
<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Users List</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>
<h2 style="display: inline-block;">Total Users</h2>

<?php if($this->session->flashdata('error')):  ?>
  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
<?php endif; ?>
<?php if($this->session->flashdata('success')):  ?>
  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
<?php endif; ?>

<hr />

<!--  Graph Starts From Here  -->

<script src="<?= $this->config->item('resource_url') . 'js/raphael-min.js';?>"></script>
<script src="<?= $this->config->item('resource_url') . 'js/'; ?>morris.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
  <link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'css/'; ?>morris.css">


<div class="row">
                  <div class="col-sm-1">
                    Report Type
                  </div>
                  <div class="col-sm-3">

                    <select id="users_duration_type"  class="select2">
                      <option value="daily">Daily</option>
                      <option value="weekly">Weekly</option>
                      <option value="monthly">Monthly</option>
                    </select>

                  </div>
                  <div class="col-sm-1">
                    Filter By
                  </div>
                  <div class="col-sm-7">

                    <div id="users_daily_filter" class="users_filters"><!--  -->
                      <div class="col-sm-4">
                        <div class="input-group">
                                        <input type="text" class="form-control" id="daily_date_picker" name="pickupdate" value="<?php echo date('m/d/Y'); ?>" />
                                        
                                    </div>
                      </div>
                      
                    
                    </div>
                    <div id="users_weekly_filter" class=" users_filters">
                      
                      <div class="col-sm-6 currency_filter">
                        <?php // print_r($currency_master); ?>
                        <select class="select2" id="users_weekly_currency">
                          <?php 
                            if(count($currency_master)>0){
                              foreach ($currency_master as $currency){
                                ?>
                                <option value="<?= $currency['currency_sign'];
                                 ?>" >[ <?= $currency['currency_sign']; ?> ] <?= $currency['currency_title']; ?> </option>
                                <?php
                              }
                            } 
                          ?>
                        </select>
                      </div>
                      <div class="col-sm-6">
                        <div class="container">    
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <div class="input-group" id="DateDemo">
                                      <input class="form-control" type='text' id='weeklyDatePicker' placeholder="Select Week" />
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="users_monthly_filter" class=" users_filters">
                      <div class="col-sm-6">
                        <select class="select2" id="year_dp">
                          <?php
                            $initial_year = 2015; 
                            $current_year = date('Y');
                            for ($i=$initial_year; $i <= $current_year ; $i++) { 
                              ?>
                              <option value="<?= $i; ?>" <?= ($current_year == $i) ? 'selected' : '' ?> ><?= $i; ?></option>
                              <?php
                            }
                          ?>
                        </select>
                      </div>
                      <div class="col-sm-6 currency_filter">
                        <?php // print_r($currency_master); ?>
                        <select class="select2" id="users_weekly_currency">
                          <?php 
                            if(count($currency_master)>0){
                              foreach ($currency_master as $currency){
                                ?>
                                <option value="<?= $currency['currency_sign'];
                                 ?>" >[ <?= $currency['currency_sign']; ?> ] <?= $currency['currency_title']; ?> </option>
                                <?php
                              }
                            } 
                          ?>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div id="users_daily_graph" class="users_graph"></div>
                    <div id="users_weekly_graph" class="users_graph"></div>
                    <div id="users_monthly_graph" class="users_graph"></div>
                  </div>
                </div>


<script type="text/javascript">
    var daily_users_graph = Morris.Bar({
      element: 'users_daily_graph',
      barSizeRatio:0.10,
      data: [
        {x: '13', y: 13}
      ],
      xkey: 'x',
      ykeys: ['y'],
      labels: ['Users'],
      //barColors: ["#303641", "#303641", "#303641", "#00a65a", "#00a65a"],
        resize: true
    }).on('click', function(i, row){
        console.log(i, row);
  });
</script>
<?php
  $timestamp = strtotime('previous');
  $days = array();
  $dates = array();
  for ($i = 0; $i < 7; $i++) {
      $days[] = strftime('%A', $timestamp);
      $timestamp = strtotime('-1 day', $timestamp);
      //$dates[] = date('d M Y',$timestamp);
       $dates[] =date('m/d/Y', strtotime(date('d-M-Y'). ' - '.$i.' days'));
  }
  //print_r($days);
  //print_r($dates);
  for ($i = 1; $i <= 12; $i++) {
      $months[] = date("M", strtotime( date( 'Y-m-01' )." -$i months"));
  }
  $days = array_reverse($days);
  $dates = array_reverse($dates);
  $months = array_reverse($months);
  //print_r($months);
?>
<script type="text/javascript">
    var weekly_users_graph = Morris.Bar({
      element: 'users_weekly_graph',
      data: [
        {x: '<?php echo $days[0]; ?>', y: 13},
        {x: '<?php echo $days[1];  ?>', y: 53},
        {x: '<?php echo $days[2];  ?>', y: 83},
        {x: '<?php echo $days[3];  ?>', y: 15},
        {x: '<?php echo $days[4];  ?>', y: 58},
        {x: '<?php echo $days[5]; ?>', y: 63},
        {x: '<?php echo $days[6]; ?>', y: 18}       
      ],
      xkey: 'x',
      ykeys: ['y'],
      labels: ['users'],
      //barColors: ['#f4543c','#f4578c','#f4500c','#f8178c','#a2228a'],
        resize: true
    }).on('click', function(i, row){
        console.log(i, row);
  });
</script>
<script type="text/javascript">
    var monthly_users_graph = Morris.Bar({
      element: 'users_monthly_graph',
      data: [
        {x: '<?php echo $months[0]; ?>', y: 86},
        {x: '<?php echo $months[1]; ?>', y: 65},
        {x: '<?php echo $months[2]; ?>', y: 19},
        {x: '<?php echo $months[3]; ?>', y: 73},
        {x: '<?php echo $months[4]; ?>', y: 78},
        {x: '<?php echo $months[5]; ?>', y: 23},
        {x: '<?php echo $months[6]; ?>', y: 96},
        {x: '<?php echo $months[7]; ?>', y: 44},
        {x: '<?php echo $months[8]; ?>', y: 18},
        {x: '<?php echo $months[9]; ?>', y: 78},
        {x: '<?php echo $months[10]; ?>', y: 13},
        {x: '<?php echo $months[11]; ?>', y: 45},
        
      ],
      xkey: 'x',
      ykeys: ['y'],
      labels: ['Users'],
      //barColors: ['#f4543c','#f4578c','#f4500c','#f8178c','#a2228a'],
        resize: true
    }).on('click', function(i, row){
        console.log(i, row);
  });
</script>

<script type="text/javascript">
  //$('#users_duration_type').select2();
  $('.users_filters').hide();
  $('.users_graph').hide();
  $('#users_daily_filter').show();
  $('#users_daily_graph').show();
  $('#users_duration_type').on('change',function(){
    $('.users_filters').hide();
    $('.users_graph').hide();
    var filter_type = $(this).val();
    $('#users_' + filter_type + '_filter').show();
    $('#users_' + filter_type + '_graph').show();
    weekly_users_graph.redraw();
    monthly_users_graph.redraw();
    //alert(filter_type);
    $(window).trigger('resize');
  });
</script>
<script type="text/javascript">
  $(function(){
    $( "#daily_date_picker" ).datepicker();
      //Pass the user selected date format
      $( "#daily_date_picker" ).change(function() {

        $( "#daily_date_picker" ).datepicker( "option", "dateFormat", $(this).val() );
        
      });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){

  //Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
  $("#weeklyDatePicker").datepicker({
      dateFormat: 'MM-DD-YYYY'
  });

   //Get the value of Start and End of Week
  $('#weeklyDatePicker').on('change', function (e) {
      var value = $("#weeklyDatePicker").val();
      //alert(value);
      var firstDate = moment(value, "MM-DD-YYYY").day(0).format("MM-DD-YYYY");
      var lastDate =  moment(value, "MM-DD-YYYY").day(6).format("MM-DD-YYYY");
      $("#weeklyDatePicker").val(firstDate + " To " + lastDate);
  });
});
</script>



<!-- graph Ends Here -->

<hr />

<h2 style="display: inline-block;">Total Users List</h2>
<table class="table table-bordered table-striped datatable" id="table-2" style="width: 100%">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Name</th>
      <th class="text-center">Email</th>
      <th class="text-center">Type</th>
      <th class="text-center">Account Type</th>
      <th class="text-center">Balance</th>
      <th class="text-center">Country</th>
      <th class="text-center">Status</th>
      <th class="text-center">Last Login</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>
  
  <tbody>
    <?php foreach ($users as $user): $email_cut = explode('@', $user['email1']);  $name = $email_cut[0];  ?>
    <tr>
      <td class="text-center"><?= $user['cust_id']; ?></td>
      <td class="text-center"><?= $user['firstname'] != "NULL" ? $user['firstname'] . ' ' . $user['lastname'] : ($user['company_name'] != "NULL" ? ucwords($user['company_name']) : ucwords($name)) ?></td>
      <td class="text-center"><?= $user['email1'] ?></td>
      <td class="text-center"><?= $user['user_type'] == 1 ? 'Individual' : 'Business'; ?></td>   
      <td class="text-center"><?= ucfirst($user['acc_type']); ?></td>
      <td class="text-center">
          <?php  
              $current_balance = $this->admin->get_current_balance($user['cust_id'],'USD');
              echo '$'; echo ($current_balance != NULL )?$current_balance['account_balance'] : 0;
          ?>
      </td>   
      <td class="text-center">
        <?php 
              $country_details = $this->admin->get_country_detail($user['country_id']);
              $country_name = $country_details['country_name'];
              echo $country_name;
        ?>
      </td>
      <td class="text-center">
        <?php 
        $to_time = strtotime($user['last_login_datetime']);
        $from_time = strtotime(date('Y-m-d H:i:s'));
          $min = round(abs($to_time - $from_time) / 60,2);
          if($min > 10 ) { echo '<span class="text-danger">Offline</span>'; }
          else { echo '<span class="text-success">Online</span>'; }
        ?>
      </td>
      <td class="text-center"><?= date('d/M/Y',strtotime($user['last_login_datetime'])); ?></td>

        <td class="text-center">
          
          <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
              Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown" role="menu">
              <li>  <a href="<?= base_url('admin/view-users/') . $user['cust_id']; ?>"> <i class="entypo-eye"></i> View </a> </li>
              <li class="divider"></li>
              <li>  <a href="<?= base_url('admin/view-users/') . $user['cust_id']; ?>"> <i class="fa fa-send"></i> Notification </a> </li>
              <li class="divider"></li>
              <?php if($user['cust_status'] == 0): ?>
                <li> <a data-toggle="tooltip" data-placement="right" title="Click to activate" data-original-title="Click to activate" onclick="activate_users('<?= $user['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-thumbs-up"></i>Activate </a>  </li>
              <?php else: ?>
                <li> <a data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to inactivate" onclick="inactivate_users('<?= $user['cust_id']; ?>');" style="cursor:pointer;"> <i class="entypo-cancel"></i>  Inactivate </a> </li>
              <?php endif; ?>
            </ul>
          </div>
        </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true,
      "order": [[7,'desc']],
      "scrollX": true
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );
  } );

  function inactivate_users(id=0){
    swal({
      title: 'Are you sure?',
      text: "You want to to inactivate this user?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, inactivate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('users/inactivate', {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Inactive!',
            'User has been inactivated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'User inactivation failed.',
            'error'
          )
        }
      });
      
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

  function activate_users(id=0){
    swal({
      title: 'Are you sure?',
      text: "You want to to activate this authority type?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, activate it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('users/activate', {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Active!',
            'User has been activated.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'User activation failed.',
            'error'
          )
        }
      });
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }
</script>