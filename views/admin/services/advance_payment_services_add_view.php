<ol class="breadcrumb bc-3" >
  <li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="<?= base_url('admin/advance-payment-services'); ?>">Services Payment Configuration</a></li>
  <li class="active"><strong>Add New Configuration</strong></li>
</ol>
<style> .border { border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel panel-dark" data-collapsed="0">
      <div class="panel-heading">
        <div class="panel-title">
          Add New Payment Configuration
        </div>
        <div class="panel-options">
          <a href="<?= base_url('admin/advance-payment-services'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/advance-payment-services/register'); ?>" class="form-horizontal form-groups-bordered validate" id="adv_pay_add" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="currency_id" id="currency_id" />
          <input type="hidden" name="currency_name" id="currency_name" />

          <div class="border">
            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="country_id" class="control-label">Select Country </label>                        
                <select id="country_id" name="country_id" class="form-control select2">
                  <option value="">Select Country</option>
                  <?php foreach ($countries as $country): ?>                      
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
                <span id="error_country_id" class="error"></span>                           
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="cat_type_id" class="control-label">Select Category Type </label>                        
                <select id="cat_type_id" name="cat_type_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Category Type">
                  <option value="">Select Category Type</option>
                  <?php foreach ($category_type as $category): ?>
                    <option value="<?= $category['cat_type_id'] ?>"><?= $category['cat_type']; ?></option>
                  <?php endforeach ?>
                </select>
                <span id="error_cat_type_id" class="error"></span>                           
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="cat_id" class="control-label">Sub Category </label>
                <select id="cat_id" name="cat_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Sub Category">
                  <option value="">Sub Category</option>
                </select> 
                <span id="error_cat_id" class="error"></span>                           
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="advance_payment" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="advance_payment" placeholder="Enter Percentage" name="advance_payment"  min="1" max="100" />  
                  <span class="input-group-addon">%</span>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="gonagoo_commission" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="gonagoo_commission" placeholder="Enter percentage" name="gonagoo_commission"  min="1" max="100" />  
                  <span class="input-group-addon">%</span>
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="min_hourly_budget" class="control-label">Min. Hourly Budget</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="min_hourly_budget" placeholder="Enter Rate" min="0" name="min_hourly_budget" /> 
                  <span class="input-group-addon currency fa">?</span>


                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="security_days" class="control-label">Security Days</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="security_days" placeholder="Enter days" name="security_days"  min="1" max="100" />  
                  <span class="input-group-addon"><i class="fa fa-sort-numeric-asc"></i></span>
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="max_quantity_to_buy_offer" class="control-label">Max. Offer Quantity</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="max_quantity_to_buy_offer" placeholder="Enter Quantity" min="0" name="max_quantity_to_buy_offer" /> 
                  <span class="input-group-addon fa"><i class="fa fa-sort-numeric-asc"></i></span>              
                </div>
              </div>
            <div class="clear"></div><br />
          </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="text-center">
                <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<br />

<script>
  $(function(){
    $("#adv_pay_add").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();
      var country_id = $("#country_id").val();
      var cat_type_id = $("#cat_type_id").val();
      var cat_id = $("#cat_id").val();
      var advance_payment = parseFloat($("#advance_payment").val()).toFixed(2);
      var gonagoo_commission = parseFloat($("#gonagoo_commission").val()).toFixed(2);
      var min_hourly_budget = parseFloat($("#min_hourly_budget").val()).toFixed(2);
      var security_days = $("#security_days").val();
      var max_quantity_to_buy_offer = $("#max_quantity_to_buy_offer").val();

      if(country_id < 0 || country_id =='') { swal('Error','Please Select Country','warning'); } 
      else if(cat_type_id < 0 || cat_type_id =='') { swal('Error','Please Select Category Type','warning'); } 
      else if(cat_id < 0 || cat_id =='') { swal('Error','Please Select Category','warning'); } 
      else if(isNaN(advance_payment)) { swal('Error','Advance Payment Percentage is Invalid! Enter Number Only.','warning'); } 
      else if(advance_payment < 0 && advance_payment > 100) { swal('Error','Advance Payment Percentage is Invalid!','warning'); }
      else if(isNaN(gonagoo_commission)) { swal('Error','Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning'); }
      else if(gonagoo_commission < 0 && gonagoo_commission > 100) { swal('Error','Gonagoo Commission Percentage is Invalid!','warning'); }
      else if(isNaN(min_hourly_budget)) { swal('Error','Minimum hourly budget is Invalid! Enter Number Only.','warning'); } 
      else if(min_hourly_budget < 0) { swal('Error','Minimum hourly budget is Invalid!','warning'); }
      else if(isNaN(security_days)) { swal('Error','Security day is Invalid! Enter Number Only.','warning'); } 
      else if(security_days < 0 || security_days =='') { swal('Error','Security day is Invalid!','warning'); }
      else if(isNaN(max_quantity_to_buy_offer)) { swal('Error','Offer max. quantity is Invalid! Enter Number Only.','warning'); } 
      else if(max_quantity_to_buy_offer < 0 || max_quantity_to_buy_offer =='') { swal('Error','Offer max. quantity is Invalid!','warning'); }
      else { $("#adv_pay_add")[0].submit(); }
    });
  });
  $(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
  });
  $("#country_id").on('change', function(event) { event.preventDefault();
    $(".currency").addClass('hidden').html('');
    var country_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-country-currencies", 
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        if(res.length > 0 ){
          $("#currency_id").val(res[0]['currency_id']);
          $("#currency_name").val(res[0]['currency_sign']);
          $(".currency").removeClass('hidden').html('<strong>'+res[0]['currency_sign']+'</strong>'); 
          $('#currency_name').focus();
        } else {
          $('#currency_id').val('0');
          $('#currency_name').val('No currency found!');
          $(".currency").removeClass('hidden').html('?'); 
        }
      },
      beforeSend: function(){
        $('#currency_id').empty();
        $('#currency_id').val('Loading...');
      },
      error: function(){
        $('#currency_id').val('0');
        $('#currency_name').val('No currency found!');
      }
    });
  });
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-categories-by-type", 
      data: { type_id: cat_type_id },
      dataType: "json",
      success: function(res){
        $('#cat_id').empty(); 
        $('#cat_id').append('<option value="">Select Sub Category</option>');
        $.each( res, function(){    
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function(){
        $('#cat_id').empty();
        $('#cat_id').append('<option value="">Loading...</option>');
      },
      error: function(){
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append('<option value="">No Options</option>');
      }
    })
  });
</script>