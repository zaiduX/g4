<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/manage-dispute'); ?>">Manage dispute</a>
	</li>
	<li class="active">
		<strong>Dispute details</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-dark" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">Job Dipsute details</div>
				<div class="panel-options"><a href="<?= base_url('admin/manage-dispute'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a></div>
			</div>
			<div class="panel-body">
				<?php if($this->session->flashdata('error')):  ?>
	      	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	    	<?php endif; ?>
	    	<?php if($this->session->flashdata('success')):  ?>
	      	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	    	<?php endif; ?>
				<div class="tab-content">
					<div class="tab-pane active" id="tab2-1">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-2">
									<label class="control-label">Job Title :</label>
								</div>
								<div class="col-md-10">
									<label class="control-label"><?=$job_details['job_title']?></label>
								</div>
								<br>
								<div class="col-md-2">
									<label class="control-label">Dispute Title :</label>
								</div>
								<div class="col-md-10">
									<label class="control-label"><?=$dispute_details['dispute_title']?></label>
								</div>
								<br>
								<div class="col-md-2">
									<label class="control-label">Dispute Description :</label>
								</div>
								<div class="col-md-10">
									<label class="control-label"><?=$dispute_details['dispute_desc']?></label>
								</div>
								<br>
								<div class="col-md-2">
									<label class="control-label">Customer name :</label>
								</div>
								<div class="col-md-10">
									<label class="control-label"><?=$job_details['cust_name']?></label>
								</div>
								<br>
								<div class="col-md-2">
									<label class="control-label">Provider name :</label>
								</div>
								<div class="col-md-10">
									<label class="control-label"><?=$job_details['provider_name']?></label>
								</div>
								<br>
								<div class="col-md-2">
									<label class="control-label">Dispute status :</label>
								</div>
								<div class="col-md-10">
									<label class="control-label"><?=$dispute_details['dispute_status']?></label>
								</div>
								<br>
								<div class="col-md-2">
									<label class="control-label">Dispute created by :</label>
								</div>
								<div class="col-md-10">
									<label class="control-label"><?=($dispute_details['dispute_created_by'] == $job_details['cust_id'])?$job_details['cust_name']." [Customer] ":$job_details['provider_name']." [Provider] "?></label>
								</div>
								<br>
								<div class="col-md-12 text-right">
									<form id="cancel_dispute" action="<?= base_url('admin/dispute-job-cancel') ?>" method="post">
										<input type="hidden" name="job_id" value="<?=$dispute_details['service_id']?>" /> 
										<input type="hidden" name="dispute_id" value="<?=$dispute_details['dispute_id']?>" /> 
										<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to cancel Job" data-original-title="Dispute close"><i class="entypo-cancel"></i>Dispute & Cancel</button>
									</form>
								</div>
								<br>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br/>