<ol class="breadcrumb bc-3" >
	<li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	<li class="active"><strong>Know Your Provider - Questions</strong></li>
</ol>
			
<h2 style="display: inline-block;">Know Your Provider - Questions</h2>
<?php $per_know_provider = explode(',', $permissions[0]['m4_know_provider']);
if(in_array('2', $per_know_provider)) { ?>
	<a type="button" href="<?= base_url('admin/provider-question-master/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">Add New <i class="entypo-plus"></i> </a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Service Category</th>
			<th class="text-center">Sub Category</th>
			<th class="text-center">Total Questions</th>
			<?php if(in_array('3', $per_know_provider) || in_array('5', $per_know_provider)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($questions as $question):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= $question['cat_type']; ?></td>
			<td class="text-center"><?= $question['cat_name'] ?></td>
			<td class="text-center"><?= trim($question['question_count']); ?></td>
			<?php if(in_array('3', $per_know_provider) || in_array('5', $per_know_provider)): ?>
				<td class="text-center">
					<?php if(in_array('3', $per_know_provider)): ?>
						<a href="<?= base_url('admin/provider-question-master/edit/').$question['cat_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							View & Edit
						</a>
					<?php endif; ?>	
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	} );


	function activate_driver_cat(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this category type?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('driver-categories/inactivate', {id: id}, function(res){
				console.log(res);
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'Driver category type has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Driver category type inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate_driver_cat(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this driver category?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('driver-categories/activate', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Driver category type has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Driver category type activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

</script>