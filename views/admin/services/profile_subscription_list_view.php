<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Profile Subscription Master</strong>
  </li>
</ol>
<style>
  .text-white { color: #fff; }
  .dropdown-menu { left: auto; right: 0 !important; }
</style>
<h2 style="display: inline-block;">Profile Subscription Master</h2>
<?php
$per_prof_sub = explode(',', $permissions[0]['m4_profile_sub']);

if(in_array('2', $per_prof_sub)) { ?>
  <a type="button" href="<?= base_url('admin/profile-subscription/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
    Add New
    <i class="entypo-plus"></i>
  </a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      <th class="text-center">Subscription Title</th>
      <th class="text-center">No. of Proposals</th>
      <th class="text-center">Country</th>
      <th class="text-center">Price</th>
      <?php if(in_array('3', $per_prof_sub) || in_array('5', $per_prof_sub)) { ?>
        <th class="text-center" >Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>
    <?php  foreach ($subscriptions as $subscription):  ?>
    <tr>
      <td class="text-center"><?= $subscription['subscription_title']; ?></td>
      <td class="text-center"><?= $subscription['proposal_credit'] ?></td>
      <td class="text-center"><?= $subscription['country_name']; ?></td>
      <td class="text-center"><?= $subscription['currency_code'] . ' ' . $subscription['price']; ?></td>
      <?php if(in_array('3', $per_prof_sub) || in_array('5', $per_prof_sub)): ?>
        <td class="text-center" style="display: inline-flex;">
          <?php if(in_array('3', $per_prof_sub)): ?>
            <a href="<?= base_url('admin/profile-subscription/edit/').$subscription['subscription_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to Edit">
              <i class="entypo-eye"></i> View &amp; Edit
            </a> &nbsp;
          <?php endif; ?>
          <?php if(in_array('5', $per_prof_sub)): ?>
            <button class="btn btn-danger btn-sm btn-icon icon-left"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_subscription('<?= $subscription["subscription_id"]; ?>');"> <i class="entypo-cancel"></i> Delete 
            </button>  
          <?php endif; ?>
        </td>        
      <?php endif; ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );    
    $table2.DataTable( {
      "order" : [[2, "desc"]]
    });
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });
  });

  function delete_subscription(id=0){    
    swal({
      title: 'Are you sure?',
      text: "You want to delete this subscription?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post("<?=base_url('profile-subscription/delete');?>", {id: id}, function(res){ 
        if(res == 'success') { 
          swal('Deleted!', 'Payment subscription has been deleted.', 'success').then(function(){ window.location.reload(); });
        } else { swal('Failed!', 'Subscription deletion failed.', 'error'); }
      });
    }, function (dismiss) { if (dismiss === 'cancel') {  } });
  }
</script>