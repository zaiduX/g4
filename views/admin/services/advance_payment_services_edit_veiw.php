<ol class="breadcrumb bc-3" >
  <li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="<?= base_url('admin/advance-payment-services'); ?>">Services Payment Configuration</a></li>
  <li class="active"><strong>Edit Configuration</strong></li>
</ol>
<style> .border { border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel panel-dark" data-collapsed="0">
      <div class="panel-heading">
        <div class="panel-title">
          Edit Payment Configuration
        </div>
        <div class="panel-options">
          <a href="<?= base_url('admin/advance-payment-services'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">
      
        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/advance-payment-services/update'); ?>" class="form-horizontal form-groups-bordered validate" id="adv_pay_add" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="smp_ap_id" value="<?=$payment['smp_ap_id']?>">

          <div class="border">
            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="country_id" class="control-label">Country </label>
                <input type="text" class="form-control" value="<?= $payment['country_name'];?>" readonly />
              </div>

              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="cat_type" class="control-label">Category Type </label>
                <input type="text" class="form-control" value="<?= $payment['cat_type'];?>" readonly />
              </div>

              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="cat_name" class="control-label">Category </label>
                <input type="text" class="form-control" value="<?= $payment['cat_name'];?>" readonly />
              </div>

              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="advance_payment" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="advance_payment" placeholder="Enter Percentage" name="advance_payment"  min="1" max="100" value="<?=$payment['advance_payment']?>" />  
                  <span class="input-group-addon">%</span>
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-3">
                <label for="gonagoo_commission" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="gonagoo_commission" placeholder="Enter percentage" name="gonagoo_commission"  min="1" max="100" value="<?=$payment['gonagoo_commission']?>" />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>

              <div class="col-md-3">
                <label for="min_hourly_budget" class="control-label">Min. Hourly Budget</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="min_hourly_budget" placeholder="Enter Rate" name="min_hourly_budget"  min="1" max="100" value="<?=$payment['min_hourly_budget']?>" />  
                  <span class="input-group-addon"><?=$payment['currency_code']?></span>
                </div>
              </div>

              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="security_days" class="control-label">Security Days</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="security_days" placeholder="Enter days" name="security_days"  min="1" max="100" value="<?=$payment['security_days']?>" />  
                  <span class="input-group-addon"><i class="fa fa-sort-numeric-asc"></i></span>
                </div>
              </div>

              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="max_quantity_to_buy_offer" class="control-label">Max. Offer Quantity</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="max_quantity_to_buy_offer" placeholder="Enter Quantity" min="0" name="max_quantity_to_buy_offer" value="<?=$payment['max_quantity_to_buy_offer']?>" /> 
                  <span class="input-group-addon fa"><i class="fa fa-sort-numeric-asc"></i></span>              
                </div>
              </div>

            <div class="clear"></div><br />
          </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="text-center">
                <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Save &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<br />

<script>
  $(function(){
    $("#adv_pay_add").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();
      var advance_payment = parseFloat($("#advance_payment").val()).toFixed(2);
      var gonagoo_commission = parseFloat($("#gonagoo_commission").val()).toFixed(2);
      var min_hourly_budget = parseFloat($("#min_hourly_budget").val()).toFixed(2);
      var security_days = $("#security_days").val();
      var max_quantity_to_buy_offer = $("#max_quantity_to_buy_offer").val();

      if(isNaN(advance_payment)) { swal('Error','Advance Payment Percentage is Invalid! Enter Number Only.','warning'); } 
      else if(advance_payment < 0 && advance_payment > 100) { swal('Error','Advance Payment Percentage is Invalid!','warning'); }
      else if(isNaN(gonagoo_commission)) { swal('Error','Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning'); }
      else if(gonagoo_commission < 0 && gonagoo_commission > 100) { swal('Error','Gonagoo Commission Percentage is Invalid!','warning'); }
      else if(isNaN(min_hourly_budget)) { swal('Error','Minimum hourly budget is Invalid! Enter Number Only.','warning'); } 
      else if(min_hourly_budget < 0) { swal('Error','Minimum hourly budget is Invalid!','warning'); }
      else if(isNaN(security_days)) { swal('Error','Security day is Invalid! Enter Number Only.','warning'); } 
      else if(security_days < 0 || security_days =='') { swal('Error','Security day is Invalid!','warning'); }
      else if(isNaN(max_quantity_to_buy_offer)) { swal('Error','Offer max. quantity is Invalid! Enter Number Only.','warning'); } 
      else if(max_quantity_to_buy_offer < 0 || max_quantity_to_buy_offer =='') { swal('Error','Offer max. quantity is Invalid!','warning'); }
      else {  $("#adv_pay_add")[0].submit(); }
    });
  });

  $(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
  });
</script>