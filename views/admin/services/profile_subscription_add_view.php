<ol class="breadcrumb bc-3" >
  <li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="<?= base_url('admin/profile-subscription'); ?>">Profile Subscription</a></li>
  <li class="active"><strong>Add New Subscription</strong></li>
</ol>
<style> .border { border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel panel-dark" data-collapsed="0">
      <div class="panel-heading">
        <div class="panel-title">
          Add New Payment Subscription
        </div>
        <div class="panel-options">
          <a href="<?= base_url('admin/profile-subscription'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/profile-subscription/register'); ?>" class="form-horizontal form-groups-bordered validate" id="profile_subscription" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="currency_id" id="currency_id" />
          <input type="hidden" name="currency_code" id="currency_code" />

          <div class="border">
            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="country_id" class="control-label">Select Country </label>
                <select id="country_id" name="country_id" class="form-control select2">
                  <option value="">Select Country</option>
                  <?php foreach ($countries as $country): ?>
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
                <span id="error_country_id" class="error"></span>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <label for="subscription_title" class="control-label">Subscription Title</label>
                <input type="text" class="form-control" id="subscription_title" placeholder="Enter Title" name="subscription_title" />
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="price" class="control-label">Subscription Price</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="price" placeholder="Enter Price" min="0" name="price" /> 
                  <span class="input-group-addon currency fa">?</span>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="proposal_credit" class="control-label">No. of Proposals</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="proposal_credit" placeholder="Enter Percentage" name="proposal_credit"  min="1" />  
                  <span class="input-group-addon">#</span>
                </div>
              </div>
              <div class="col-xl-9 col-lg-9 col-md-9 col-sm-6 col-xs-6">
                <label for="subscription_desc" class="control-label">Subscription desc</label>
                <textarea type="text" class="form-control" id="subscription_desc" placeholder="Enter description" name="subscription_desc"></textarea>
              </div>
            </div>
            <div class="clear"></div><br />
          </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="text-center">
                <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<br />

<script>
  $(function(){
    $("#profile_subscription").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();
      var country_id = $("#country_id").val();
      var subscription_title = $("#subscription_title").val();
      var price = $("#price").val();
      var proposal_credit = $("#proposal_credit").val();

      if(country_id < 0 || country_id =='') { swal('Error','Please Select Country','warning'); } 
      else if(subscription_title =='') { swal('Error','Please Enter Title','warning'); } 
      else if(price <= 0 || price =='') { swal('Error','Enter Subscription Price (Greater than Zero(0)).','warning'); }
      else if(proposal_credit <= 0 || proposal_credit =='') { swal('Error','Enter Proposal Credits.','warning'); }
      else { $("#profile_subscription")[0].submit(); }
    });
  });
  $(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
  });
  $("#country_id").on('change', function(event) { event.preventDefault();
    $(".currency").addClass('hidden').html('');
    var country_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-country-currencies", 
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        if(res.length > 0 ){
          $("#currency_id").val(res[0]['currency_id']);
          $("#currency_code").val(res[0]['currency_sign']);
          $(".currency").removeClass('hidden').html('<strong>'+res[0]['currency_sign']+'</strong>'); 
          $('#currency_code').focus();
        } else {
          $('#currency_id').val('0');
          $('#currency_code').val('No currency found!');
        }
      },
      beforeSend: function(){
        $('#currency_id').empty();
        $('#currency_id').val('Loading...');
      },
      error: function(){
        $('#currency_id').val('0');
        $('#currency_code').val('No currency found!');
      }
    });
  });
</script>