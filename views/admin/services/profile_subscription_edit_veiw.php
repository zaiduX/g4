<ol class="breadcrumb bc-3" >
  <li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="<?= base_url('admin/profile-subscription'); ?>">Profile Subscription</a></li>
  <li class="active"><strong>Edit</strong></li>
</ol>
<style> .border { border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel panel-dark" data-collapsed="0">
      <div class="panel-heading">
        <div class="panel-title">
          Edit Profile Subscription
        </div>
        <div class="panel-options">
          <a href="<?= base_url('admin/profile-subscription'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/profile-subscription/update'); ?>" class="form-horizontal form-groups-bordered validate" id="profile_subscription" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="subscription_id" value="<?=$subscription['subscription_id']?>">
          
          <div class="border">
            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="country_id" class="control-label">Country </label>                        
                <input type="text" class="form-control" id="currency_id" value="<?=$subscription['country_name']?>" name="currency_id" readonly="readonly" />
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <label for="subscription_title" class="control-label">Subscription Title</label>                
                <input type="text" class="form-control" id="subscription_title" placeholder="Enter Title" name="subscription_title" value="<?= $subscription['subscription_title'];?>" />
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="price" class="control-label">Subscription Price</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="price" placeholder="Enter Price" min="0" name="price" value="<?= $subscription['price'];?>" /> 
                  <span class="input-group-addon currency fa">?</span>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <label for="proposal_credit" class="control-label">No. of Proposals</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="proposal_credit" placeholder="Enter Percentage" name="proposal_credit"  min="1" value="<?= $subscription['proposal_credit'];?>" />  
                  <span class="input-group-addon">#</span>
                </div>
              </div>
              <div class="col-xl-9 col-lg-9 col-md-9 col-sm-6 col-xs-6">
                <label for="subscription_desc" class="control-label">Subscription desc</label>                
                <textarea type="text" class="form-control" id="subscription_desc" placeholder="Enter description" name="subscription_desc"><?= $subscription['subscription_desc'];?></textarea>
              </div>
            </div>
            <div class="clear"></div><br />
          </div>
          <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="text-center">
                <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Save &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<br />

<script>
  $(function(){
    $("#profile_subscription").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();
      var subscription_title = $("#subscription_title").val();
      var price = $("#price").val();
      var proposal_credit = $("#proposal_credit").val();

      if(subscription_title =='') { swal('Error','Please Enter Title','warning'); } 
      else if(price <= 0 || price =='') { swal('Error','Enter Subscription Price (Greater than Zero(0)).','warning'); }
      else if(proposal_credit <= 0 || proposal_credit =='') { swal('Error','Enter Proposal Credits.','warning'); }
      else { $("#profile_subscription")[0].submit(); }
    });
  });

  $(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
  });
</script>