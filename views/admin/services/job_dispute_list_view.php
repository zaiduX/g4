<ol class="breadcrumb bc-3" >
	<li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	<li class="active"><strong>Job dispute</strong></li>
</ol>
			
<h2 style="display: inline-block;">Job dispute</h2>
<?php $per_job_dis = explode(',', $permissions[0]['m4_job_dispute']);
if(in_array('2', $per_job_dis)) { ?>

<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">Dispute ID</th>
			<th class="text-center">Dispute Title</th>
			<th class="text-center">Status</th>
			<th class="text-center">Request money back</th>
			<th class="text-center">Create Date Time</th>
			<?php if(in_array('3', $per_job_dis) || in_array('5', $per_job_dis)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ($job_dispute as $dispute):  ?>
		<tr>
			<td class="text-center"><?=$dispute['dispute_id']?> </td>
			<td class="text-center"><?=$dispute['dispute_title']?> </td>
			<td class="text-center"><?=$dispute['dispute_status']?> </td>
			<td class="text-center"><?=($dispute['request_money_back']=="1")?"Yes":"No";?> </td>
			<td class="text-center"><?= date('d / M / Y H:i a', strtotime($dispute['cre_datetime'])); ?></td>
			<?php if(in_array('3', $per_job_dis) || in_array('5', $per_job_dis)): ?>
				<td class="text-center">
					<?php if(in_array('3', $per_job_dis)): ?>
						
						<div class="btn-group">
							<button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown dropdown-left" style="left:-43px;" role="menu">								
								<li>	
									<form id="customer_chat<?=$dispute['dispute_id']?>" action="<?= base_url('admin/customer-chat') ?>" method="post">
										<input type="hidden" name="job_id" value="<?=$dispute['service_id']?>" />
										<input type="hidden" name="customer_id" value="<?=$dispute['cust_id']?>" />
										<a data-toggle="tooltip" data-placement="top" title="Chat with customer" data-original-title="Customer" style="cursor: pointer; color: inherit; padding: 3px 20px;" onclick="$('#customer_chat<?=$dispute['dispute_id']?>').get(0).submit();"> 
											<i class="entypo-comment"></i>	Customer 
										</a>	
									</form>
								</li>
								<li class="divider"></li>
								<li>	
									<form id="freelancer_chat<?=$dispute['dispute_id']?>" action="<?= base_url('admin/freelancer-chat') ?>" method="post">
										<input type="hidden" name="job_id" value="<?=$dispute['service_id']?>" />
										<input type="hidden" name="provider_id" value="<?=$dispute['provider_id']?>" />
										<a data-toggle="tooltip" data-placement="top" title="Chat with freelancer" data-original-title="freelancer" style="cursor: pointer; color: inherit; padding: 3px 20px;" onclick="$('#freelancer_chat<?=$dispute['dispute_id']?>').get(0).submit();"> 
											<i class="entypo-comment"></i>	Freelancer 
										</a>	
									</form>
								</li>
								<li class="divider"></li>
								<li>	
									<form id="group_chat<?=$dispute['dispute_id']?>" action="<?= base_url('admin/group-chat') ?>" method="post">
										<input type="hidden" name="job_id" value="<?=$dispute['service_id']?>" />
										<a data-toggle="tooltip" data-placement="top" title="Chat with both" data-original-title="group" style="cursor:	pointer; color: inherit; padding: 3px 20px;" onclick="$('#group_chat<?=$dispute['dispute_id']?>').get(0).submit();"> 
											<i class="entypo-users"></i>	Group
										</a>	
									</form>
								</li>
								<li class="divider"></li>
								<li>	
									<form id="manage_dispute<?=$dispute['dispute_id']?>" action="<?= base_url('admin/about-dispute') ?>" method="post">
										<input type="hidden" name="job_id" value="<?=$dispute['service_id']?>" />
										<input type="hidden" name="dispute_id" value="<?=$dispute['dispute_id']?>" />
										<a data-toggle="tooltip" data-placement="top" title="Manage dispute" data-original-title="freelancer" style="cursor: pointer; color: inherit; padding: 3px 20px;" onclick="$('#manage_dispute<?=$dispute['dispute_id']?>').get(0).submit();"> 
											<i class="entypo-cog"></i> Manage 
										</a>	
									</form>
								</li>							
							</ul>
						</div>
					<?php endif; ?>	
				</td>
			<?php endif; ?> 
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<br />
<script type="text/javascript">
	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
	    $(".alert").slideUp(500);
  	});
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	});
	function inactivate_category(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this category?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('dispute-sub-category-master/inactivate-dispute-sub-category', {dispute_sub_cat_id: id}, function(res){
				if(res == 'success') {
				  swal(
				    'Inactive!',
				    'Category has been inactivated.',
				    'success'
				  ). then(function() { 	window.location.reload();  });
				} else {
					swal(
				    'Failed!',
				    'Category inactivation failed.',
				    'error'
				  )
				}
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
	function activate_subcategory(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this category?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('dispute-sub-category-master/activate-dispute-sub-category', {dispute_sub_cat_id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Category has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				} else {
					swal(
				    'Failed!',
				    'Category activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>