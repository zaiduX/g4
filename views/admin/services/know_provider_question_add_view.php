<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/provider-question-master'); ?>">Know Your Provider</a>
  </li>
  <li class="active">
    <strong>Add</strong>
  </li>
</ol>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel  panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add Question
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/provider-question-master'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">
        
        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>

        <form role="form" class="form-horizontal form-groups-bordered" id="question_add" action="<?= base_url('admin/provider-question-master/register'); ?>" method="post" enctype="multipart/form-data">
          <div class="row">

            <div class="col-xl-8 col-xl-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">

              <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label for="question_title" class="control-label">Question</label>                    
                <input type="text" class="form-control" id="question_title" placeholder="Enter question" name="question_title" />
              </div>

              <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label class="control-label">Category Type</label>
                <select id="cat_type_id" name="cat_type_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Category Type">
                  <option value="">Select Category Type</option>
                  <?php foreach ($category_type as $category): ?>
                    <option value="<?= $category['cat_type_id'] ?>"><?= $category['cat_type']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>

              <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label class="control-label">Sub Category</label>
                <select id="cat_id" name="cat_id[]" class="form-control select2" data-allow-clear="true" data-placeholder="Select Sub Category" multiple autocomplete="off">
                  <option value="">Sub Category</option>
                </select>                      
              </div>              
          
              <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                <button type="submit" id="btn_submit" class="btn btn-blue">Add Question</button>
              </div>

            </div>
        </form>
        
      </div>
    
    </div>
  
  </div>
</div>
<br />
<script>
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-categories-by-type", 
      data: { type_id: cat_type_id },
      dataType: "json",
      success: function(res){
        //Clear options corresponding to earlier option of first dropdown
        $('#cat_id').empty(); 
        $('#cat_id').append('<option value="">Select Sub Category</option>');
        //Populate options of the second dropdown
        $.each( res, function(){    
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function(){
        $('#cat_id').empty();
        $('#cat_id').append('<option value="">Loading...</option>');
      },
      error: function(){
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append('<option value="">No Options</option>');
      }
    })
  });
</script>
<script>
  $("#question_add").on('submit',function(e){ e.preventDefault(); });
  $("#btn_submit").on('click',function(e){ 
      var question_title = $("#question_title").val();
      var type_id = $("#cat_type_id").val();
      var cat_id = $("#cat_id").val();

      if(!question_title){    swal('Error',"Enter question", 'warning');  }
      else if(!type_id){   swal('Error',"Select Category Type", 'warning'); }
      else if(!cat_id){   swal('Error',"Select Category", 'warning'); }
      else{ $("#question_add").get(0).submit(); }
  });
</script>
    
