<ol class="breadcrumb bc-3" >
	<li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	<li class="active"><strong>Job Cancellation Reasons</strong></li>
</ol>
			
<h2 style="display: inline-block;">Job Cancellation Reasons</h2>
<?php $per_cancel_reason = explode(',', $permissions[0]['m4_job_cancel_reason']);
if(in_array('2', $per_cancel_reason)) { ?>

<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel panel-dark" data-collapsed="0">
      <div class="panel-heading">
        <div class="panel-title">Add / Update Reason</div>
      </div>
      <div class="panel-body" style="padding-bottom: 0px;">
        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>

        <form role="form" class="form-horizontal form-groups-bordered" id="question_add" action="<?= base_url('admin/job-cancellation-reason-master/register'); ?>" method="post" enctype="multipart/form-data">
        	<input type="hidden" name="reason_id" id="reason_id" value="0">
          	<div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	              <div class="form-group col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-12">
	                <input type="text" class="form-control" id="title" placeholder="Enter job cancellation reason" name="title" required="required" />
	              </div>
	              <div class="form-group col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
	                <button type="submit" id="btn_submit" class="btn btn-blue">Save Reason</button>
	              </div>
	            </div>
        	</div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th>Reasons</th>
			<?php if(in_array('3', $per_cancel_reason) || in_array('5', $per_cancel_reason)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($reasons as $reason):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td><?= trim($reason['title']); ?></td>
			<?php if(in_array('3', $per_cancel_reason) || in_array('5', $per_cancel_reason)): ?>
				<td class="text-center">
					<?php if(in_array('3', $per_cancel_reason)): ?>
						<button type="button" id="btnEdit_<?= $reason["reason_id"]; ?>" class="btn btn-primary btn-sm btn-icon icon-left"><i class="entypo-pencil"></i> Edit</button>
						<script>
							$("#btnEdit_<?= $reason["reason_id"]; ?>").click(function(){
							  $("#reason_id").val("<?=$reason['reason_id']?>");
							  $("#title").val("<?=$reason['title']?>");
							});
						</script>
					<?php endif; ?>	
					<?php if(in_array('5', $per_cancel_reason)): ?>
            <button class="btn btn-danger btn-sm btn-icon icon-left"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_reason('<?= $reason["reason_id"]; ?>');"> <i class="entypo-cancel"></i> Delete </button>  
        	<?php endif; ?>
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
	    $(".alert").slideUp(500);
  	});
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	});
	function delete_reason(id=0) {
	    swal({
			title: 'Are you sure?',
			text: "You want to delete this reason?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: true,
	    }).then(function () {
	      	$.post("job-cancellation-reason-master/delete", {reason_id: id}, function(res){ 
		        if(res == 'success'){ 
		          swal(
		            'Deleted!',
		            'Job cancellation reason has been deleted.',
		            'success'
		          ). then(function(){   window.location.reload();  });
		        } else {
		          swal(
		            'Failed!',
		            'Job cancellation reason deletion failed.',
		            'error'
		          )
		        }
	      	});
	    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
	}
</script>