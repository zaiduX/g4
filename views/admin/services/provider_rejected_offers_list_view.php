<ol class="breadcrumb bc-3" >
	<li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	<li class="active"><strong>Provider Rejected Offers</strong></li>
</ol>

<h2 style="display: inline-block;">Provider Rejected Offers</h2>
<?php $per_offer = explode(',', $permissions[0]['m4_provider_offer']);
if(in_array('2', $per_offer)) { ?>
	<a type="button" href="<?= base_url('admin/provider-offer-manage'); ?>" class="btn btn-green btn-icon icon-left" style="float: right">Back<i class="entypo-back"></i></a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Date</th>
			<th class="text-center">Provider</th>
			<th class="text-center">Offer Title</th>
			<th class="text-center">Offer Price</th>
			<?php if(in_array('3', $per_offer) || in_array('5', $per_offer)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php $offset = 1; ?>
		<?php foreach ($offers as $offer):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= date('d/m/Y', strtotime($offer['cre_datetime'])); ?></td>
			<td class="text-center"><?= trim($offer['company_name']); ?></td>
			<td class="text-center"><?= trim($offer['offer_title']); ?></td>
			<td class="text-center"><?= trim($offer['currency_code']).' '.trim($offer['offer_price']); ?></td>
			<td class="text-center">
				<div style="display: inline-flex;">
					<form action="offer-details" method="post">
						<input type="hidden" name="offer_id" value="<?=$offer['offer_id']?>" />
						<input type="hidden" name="page" value="rejected" />
						<button type="submit" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to view" data-original-title="Click to view"><i class="entypo-eye"></i> View</button>
					</form>&nbsp;
					<!-- <?php if(in_array('4', $per_offer)) { ?>
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to de-activate" data-original-title="Click to de-activate" onclick="de_activate_offer('<?= $offer['offer_id']; ?>');"><i class="entypo-thumbs-up"></i>De-activate</button>
					<?php } ?> -->
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<script type="text/javascript">
	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
	});
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true,
			"bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5 ], 
		});
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	});
	function de_activate_offer(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to de-activate this offer?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, de-activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('de-activate-offer', {offer_id: id}, function(res){
				if(res == 'success'){
				  swal('Active!', 'Offer has been de-activated.', 'success'). then(function(){ 	window.location.reload();  });
				} else { swal('Failed!', 'Offer de-activation failed.', 'error') }
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>