<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/provider-question-master'); ?>">Know Your Provider</a>
	</li>
	<li class="active">
		<strong>Edit</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel  panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Manage Questions
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/provider-question-master'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">
				
				<?php if($this->session->flashdata('error')):  ?>
	      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>

		    	<h3><?=$questions[0]['cat_type']?> - <?=$questions[0]['cat_name']?></h3>
				<?php foreach ($questions as $question) { ?>
					<form role="form" class="form-horizontal form-groups-bordered" id="question" action="<?= base_url('admin/provider-question-master/update'); ?>" method="post">
						<input type="hidden" name="question_id" value="<?= $question['question_id']; ?>">
						<input type="hidden" name="cat_id" value="<?= $question['cat_id']; ?>">
						<div class="form-group">
							<label for="skill_name" class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-xs-12 control-label">#<?=$question['question_id']?> Question</label>
							<div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-xs-12">
								<input type="text" class="form-control" id="question_title" placeholder="Enter question" name="question_title" value="<?= $question['question_title']; ?>" required="required" />
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12" style="display: inline-flex;">
								<button type="submit" class="btn btn-blue">Update</button>&nbsp;
					</form>
							<form role="form" class="form-horizontal form-groups-bordered" id="question" action="<?= base_url('admin/provider-question-master/delete'); ?>" method="post">
								<input type="hidden" name="question_id" value="<?= $question['question_id']; ?>">
								<input type="hidden" name="cat_id" value="<?= $question['cat_id']; ?>">
								<button type="submit" class="btn btn-danger">Delete</button>
							</form>
							</div>
						</div>
				<?php } ?>
			</div>
		
		</div>
	
	</div>
</div>

<br />