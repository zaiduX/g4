<ol class="breadcrumb bc-3" >
	<li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	<li class="active"><strong>Provider Offers</strong></li>
</ol>

<h2 style="display: inline-block;">Provider Offers</h2>
<?php $per_offer = explode(',', $permissions[0]['m4_provider_offer']);
if(in_array('2', $per_offer)) { ?>
	<div style="display: inline-flex; float: right">
		<a type="button" href="<?= base_url('admin/provider-offer-manage/active-offers'); ?>" class="btn btn-green btn-icon icon-left">Active Offers<i class="entypo-check"></i></a>&nbsp;
		<a type="button" href="<?= base_url('admin/provider-offer-manage/rejected-offers'); ?>" class="btn btn-red btn-icon icon-left">Rejected Offers<i class="entypo-trash"></i></a>
	</div>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Date</th>
			<th class="text-center">Provider</th>
			<th class="text-center">Offer Title</th>
			<th class="text-center">Offer Price</th>
			<?php if(in_array('3', $per_offer) || in_array('5', $per_offer)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($offers as $offer):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= date('d/m/Y', strtotime($offer['cre_datetime'])); ?></td>
			<td class="text-center"><?= trim($offer['company_name']); ?></td>
			<td class="text-center"><?= trim($offer['offer_title']); ?></td>
			<td class="text-center"><?= trim($offer['currency_code']).' '.trim($offer['offer_price']); ?></td>
			<td class="text-center">
				<div style="display: inline-flex;">
					<form action="provider-offer-manage/offer-details" method="post">
						<input type="hidden" name="offer_id" value="<?=$offer['offer_id']?>" />
						<input type="hidden" name="page" value="pending" />
						<button type="submit" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to view" data-original-title="Click to view"><i class="entypo-eye"></i> View</button>
					</form>&nbsp;
					<?php if(in_array('4', $per_offer)) { ?>
						<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to activate" data-original-title="Click to activate" onclick="activate_offer('<?= $offer['offer_id']; ?>');"><i class="entypo-thumbs-up"></i>Activate</button>
					<?php } ?>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<script type="text/javascript">
	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
	});
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true,
			"bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5 ], 
		});
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	});
	function activate_offer(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to activate this offer?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('provider-offer-manage/activate-offer', {offer_id: id}, function(res){
				if(res == 'success'){
				  swal('Active!', 'Offer has been activated.', 'success'). then(function(){ 	window.location.reload();  });
				} else { swal('Failed!', 'Offer activation failed.', 'error') }
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>