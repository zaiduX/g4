<ol class="breadcrumb bc-3" >
	<li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	<li class="active"><strong>Dispute Sub Categories</strong></li>
</ol>
			
<h2 style="display: inline-block;">Dispute Sub Categories</h2>
<?php $per_dis_sub_cat = explode(',', $permissions[0]['m4_dispute_sub_cat']);
if(in_array('2', $per_dis_sub_cat)) { ?>

<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel panel-dark" data-collapsed="0">
      <div class="panel-heading">
        <div class="panel-title">Add / Update Sub Category</div>
      </div>
      <div class="panel-body">
        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>

        <form role="form" class="form-horizontal form-groups-bordered" id="question_add" action="<?= base_url('admin/dispute-sub-category-master/register'); ?>" method="post" enctype="multipart/form-data">
        	<input type="hidden" name="dispute_sub_cat_id" id="dispute_sub_cat_id" value="0">
        	<div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <select id="dispute_cat_id" name="dispute_cat_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Category Type">
                  <option value="">Select Category</option>
                  <?php foreach ($categories as $category): ?>                     
                    <option value="<?= $category['dispute_cat_id'] ?>"><?= $category['dispute_cat_title']; ?></option>
                  <?php endforeach ?>
                </select> 
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control" id="dispute_sub_cat_title" placeholder="Enter Dispute Sub Category Title" name="dispute_sub_cat_title" required="required" />
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                <button type="submit" id="btn_submit" class="btn btn-blue">Save Category</button>
              </div>
            </div>
      		</div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Category Title</th>
			<th class="text-center">Sub Category Title</th>
			<th class="text-center">Status</th>
			<th class="text-center">Create Date Time</th>
			<?php if(in_array('3', $per_dis_sub_cat) || in_array('5', $per_dis_sub_cat)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($sub_categories as $sub_category):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= trim($sub_category['dispute_cat_title']); ?></td>
			<td class="text-center"><?= trim($sub_category['dispute_sub_cat_title']); ?></td>
			<?php if($sub_category['status'] == 1): ?>
				<td class="text-center">
					<label class="label label-success" ><i class="entypo-thumbs-up"></i>Active</label>
				</td>
			<?php else: ?>
				<td class="text-center">
					<label class="label label-danger" ><i class="entypo-cancel"></i>Inactive</label>
				</td>
			<?php endif; ?>
			<td class="text-center"><?= date('d / M / Y H:i a', strtotime($sub_category['cre_datetime'])); ?></td>
			<?php if(in_array('3', $per_dis_sub_cat) || in_array('5', $per_dis_sub_cat)): ?>
				<td class="text-center">
					<?php if(in_array('3', $per_dis_sub_cat)): ?>
						<button type="button" id="btnEdit_<?= $sub_category["dispute_sub_cat_id"]; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to edit" data-original-title="Click to edit"><i class="entypo-pencil"></i> Edit</button>
						<script>
							$("#btnEdit_<?= $sub_category["dispute_sub_cat_id"]; ?>").click(function(){
							  $("#dispute_sub_cat_id").val("<?=$sub_category['dispute_sub_cat_id']?>");
							  $("#dispute_sub_cat_title").val("<?=$sub_category['dispute_sub_cat_title']?>");
							  $("#dispute_cat_id").prop("disabled", true);
							});
						</script>
					<?php endif; ?>	
					<?php if(in_array('4', $per_dis_sub_cat)) { ?>
						<?php if($sub_category['status'] == 0): ?>
							<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to activate" data-original-title="Click to activate" onclick="activate_subcategory('<?= $sub_category['dispute_sub_cat_id']; ?>');">
								<i class="entypo-thumbs-up"></i>Activate</button>
						<?php else: ?>
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to inactivate" data-original-title="Click to inactivate" onclick="inactivate_category('<?= $sub_category['dispute_sub_cat_id']; ?>');">
							<i class="entypo-cancel"></i>
							Inactivate</button>
						<?php endif; ?>
					<?php } ?>
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
	    $(".alert").slideUp(500);
  	});
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	});
	function inactivate_category(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this category?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('dispute-sub-category-master/inactivate-dispute-sub-category', {dispute_sub_cat_id: id}, function(res){
				if(res == 'success') {
				  swal(
				    'Inactive!',
				    'Category has been inactivated.',
				    'success'
				  ). then(function() { 	window.location.reload();  });
				} else {
					swal(
				    'Failed!',
				    'Category inactivation failed.',
				    'error'
				  )
				}
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
	function activate_subcategory(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this category?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('dispute-sub-category-master/activate-dispute-sub-category', {dispute_sub_cat_id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Category has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				} else {
					swal(
				    'Failed!',
				    'Category activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>