<ol class="breadcrumb bc-3" >
	<li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	<li class="active"><strong>Contract Expiry Notification Delay Hours</strong></li>
</ol>
			
<h2 style="display: inline-block;">Contract Expiry Notification Delay Hours</h2>
<?php $per_delay = explode(',', $permissions[0]['m4_contract_expiry_delay']);
if(in_array('2', $per_delay)) { ?>

<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel panel-dark" data-collapsed="0">
      <div class="panel-heading">
        <div class="panel-title">Add / Update Hours</div>
      </div>
      <div class="panel-body">
        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>

        <form role="form" class="form-horizontal form-groups-bordered" id="question_add" action="<?= base_url('admin/contract-expiry-delay/register'); ?>" method="post" enctype="multipart/form-data">
        	<input type="hidden" name="expiry_id" id="expiry_id" value="0">
        	<div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <select id="country_id" name="country_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select Country">
                  <option value="">Select Country</option>
                  <?php foreach ($countries as $country): ?>                     
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select> 
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <input type="number" class="form-control" id="hours" placeholder="Enter Delay Hours" name="hours" required="required" min="0" />
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                <button type="submit" id="btn_submit" class="btn btn-blue">Save Details</button>
              </div>
            </div>
      		</div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Country</th>
			<th class="text-center">Delay Hours</th>
			<th class="text-center">Create Date Time</th>
			<?php if(in_array('3', $per_delay) || in_array('5', $per_delay)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($delay_hours as $hr):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= trim($hr['country_name']); ?></td>
			<td class="text-center"><?= trim($hr['hours']); ?></td>
			<td class="text-center"><?= date('d / M / Y H:i a', strtotime($hr['cre_datetime'])); ?></td>
			<?php if(in_array('3', $per_delay) || in_array('5', $per_delay)): ?>
				<td class="text-center">
					<?php if(in_array('3', $per_delay)): ?>
						<button type="button" id="btnEdit_<?= $hr["expiry_id"]; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to edit" data-original-title="Click to edit"><i class="entypo-pencil"></i> Edit</button>
						<script>
							$("#btnEdit_<?= $hr["expiry_id"]; ?>").click(function(){
							  $("#expiry_id").val("<?=$hr['expiry_id']?>");
							  $("#hours").val("<?=$hr['hours']?>");
							  $("#country_id").prop("disabled", true);
							});
						</script>
					<?php endif; ?>
					<?php if(in_array('5', $per_delay)): ?>
			            <button class="btn btn-danger btn-sm btn-icon icon-left"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_hours('<?= $hr["expiry_id"]; ?>');"> <i class="entypo-cancel"></i> Delete 
			            </button>  
			          <?php endif; ?>
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
	    $(".alert").slideUp(500);
  	});
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	});

	function delete_hours(id=0){    
    swal({
      title: 'Are you sure?',
      text: "You want to to delete this?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
		$.post("contract-expiry-delay/delete", {expiry_id: id}, function(res){ 
			if(res == 'success') { 
			  	swal('Deleted!', 'Hours details has been deleted.', 'success').then(function(){ window.location.reload(); });
			} else { swal('Failed!', 'Hours details deletion failed.', 'error'); }
		});
    }, function (dismiss) { if (dismiss === 'cancel') {  } });
  }
</script>