<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Followup Users List</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>
<div class="row">
  <?php
    $total_not_assigned = $this->users->total_accounts(1);
    $total_not_assigned = $this->users->convert_big_int($total_not_assigned);
    $total_not_assigned_buyer = $this->users->total_accounts(1,0);
    $total_not_assigned_buyer = $this->users->convert_big_int($total_not_assigned_buyer);
    $total_not_assigned_deliverere = $this->users->total_accounts(1,1);
    $total_not_assigned_deliverere = $this->users->convert_big_int($total_not_assigned_deliverere);

    $total_assigned = $this->users->total_accounts(2);
    $total_assigned = $this->users->convert_big_int($total_assigned);
    $total_assigned_buyer = $this->users->total_accounts(2,0);
    $total_assigned_buyer = $this->users->convert_big_int($total_assigned_buyer);
    $total_assigned_deliverere = $this->users->total_accounts(2,1);
    $total_assigned_deliverere = $this->users->convert_big_int($total_assigned_deliverere);

    $total_followup_pending = $this->users->total_accounts(3);
    $total_followup_pending = $this->users->convert_big_int($total_followup_pending);
    $total_followup_pending_buyer = $this->users->total_accounts(3,0);
    $total_followup_pending_buyer = $this->users->convert_big_int($total_followup_pending_buyer);
    $total_followup_pending_deliverere = $this->users->total_accounts(3,1);
    $total_followup_pending_deliverere = $this->users->convert_big_int($total_followup_pending_deliverere);

    $total_followup_done = $this->users->total_accounts(4);
    $total_followup_done = $this->users->convert_big_int($total_followup_done);
    $total_followup_done_buyer = $this->users->total_accounts(4,0);
    $total_followup_done_buyer = $this->users->convert_big_int($total_followup_done_buyer);
    $total_followup_done_deliverere = $this->users->total_accounts(4,1);
    $total_followup_done_deliverere = $this->users->convert_big_int($total_followup_done_deliverere);
    ?>

  <div class="col-sm-3">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Not Assigned : <big> <?= $total_not_assigned; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Buyers</h6>
          <big class="text-white"> <?= $total_not_assigned_buyer; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Sellers</h6>
          <big class="text-white"> <?= $total_not_assigned_deliverere; ?></big>
        </div> <br/>
      </div> 
    </div>
  </div>
  
  <div class="col-sm-3">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Assigned : <big> <?= $total_assigned; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Business</h6>
          <big class="text-white"> <?= $total_assigned_buyer; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Individual</h6>
          <big class="text-white"> <?= $total_assigned_deliverere; ?></big>
        </div> <br/>
      </div> 
    </div>
  </div>
  
  <div class="col-sm-3">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Followup Pending : <big> <?= $total_followup_pending; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Business</h6>
          <big class="text-white"> <?= $total_followup_pending_buyer; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Individual</h6>
          <big class="text-white"> <?= $total_followup_pending_deliverere; ?></big>
        </div> <br/>
      </div> 
    </div>
  </div>
  
  
  <div class="col-sm-3">
    <div class="tile-stats tile-blue">      
      <div class="tile-header text-center">
          <div class="icon"><i class="fa fa-users"></i></div>
          
        <h3 class="text-white">Followup Taken : <big> <?= $total_followup_done; ?> </big></h3><hr/>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Business</h6>
          <big class="text-white"> <?= $total_followup_done_buyer; ?> </big>
        </div>
        <div class="col-sm-6 text-center">
          <h6 class="text-white">Individual</h6>
          <big class="text-white"> <?= $total_followup_done_deliverere; ?></big>
        </div> <br/>
      </div>            
    </div>
  </div>
  
</div>

      
<h2 style="display: inline-block; margin-top:0px;">Un-assigned Users List</h2>
<?php $per_user_followups = explode(',', $permissions[0]['user_followups']); 
if(in_array('1', $per_user_followups)) { ?>
  <a type="button" href="<?= base_url('admin/followup-assigned-users'); ?>" class="btn btn-success btn-icon icon-left" style="float: right;">
    Assigned Users
    <i class="entypo-check"></i>
  </a>
<?php } ?>

  <?php if($this->session->flashdata('error')):  ?>
      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
      <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
  <?php endif; ?>

<table class="table table-bordered table-striped" id="table-2" width="100%">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Name</th>
      <th class="text-center">Company</th>
      <th class="text-center">Email</th>
      <th class="text-center">Contact</th>
      <th class="text-center">Account Type</th>
      <th class="text-center">Country</th>
      <?php if(in_array('1', $per_user_followups) || in_array('3', $per_user_followups)) { ?>
        <th class="text-center" style="width:25%;">Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>
    <?php 
      foreach ($users_list as $users): 
        $country_details = $this->notice->get_country_detail($users['country_id']);
        $country_name = $country_details['country_name']; ?>
        <tr>
          <td class="text-center"><?= $users['cust_id'] ?></td>
          <td class="text-center"><?= ($users['firstname'] != "NULL")? $users['firstname'] . ' ' . $users['lastname'] :"Not Provided" ?></td>
          <td class="text-center"><?= ($users['company_name'] != "NULL")? $users['company_name'] :"Not Provided" ?></td>
          <td class="text-center"><?= $users['email1'] ?></td>
          <td class="text-center"><?= '+'.$country_details['country_phonecode'].$users['mobile1'] ?></td>
          <td class="text-center"><?= ucfirst($users['acc_type']); ?></td>
          <td class="text-center"><?= $country_name; ?></td>

          <?php if(in_array('1', $per_user_followups) || in_array('3', $per_user_followups)): ?>
            <td class="text-center">
              <form action="<?= base_url('admin/assign-authority-to-user'); ?>" method="post" style="display: flex;">
                <input type="hidden" name="cust_id" value="<?= $users['cust_id'] ?>" />
                <input type="hidden" name="type" value="assign" />
                <select class="form-control" id="auth_id" name="auth_id">
                  <option>Select Authority</option>
                  <?php foreach ($authority_list as $authority) { ?>
                    <option value="<?= $authority['auth_id'] ?>"><?= $authority['auth_fname'] . ' ' . $authority['auth_lname'] ?></option>
                  <?php } ?>
                </select> &nbsp;
                <input type="submit" name="btnAssign" class="btn btn-success" value="Assign">
              </form>
            </td>
          <?php endif; ?>

        </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true,
      "order": [[7,'desc']],
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );
  } );
</script>