<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/transport-vehicles'); ?>">Transport Vehicles</a>
  </li>
  <li class="active">
    <strong>Add New</strong>
  </li>
</ol>

<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add New Transport Vehicle
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/transport-vehicles'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
              <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
          <?php endif; ?>
          <?php if($this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
          <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/transport-vehicles/register'); ?>" class="form-horizontal form-groups-bordered" id="vehicle_add" method="post" autocomplete="off">
  
          <div class="form-group validate-has-error">
            <label for="auth_fname" class="col-sm-3 control-label">Transport Mode :</label>
            
            <div class="col-sm-5">
              <div class="radio radio-replace color-blue col-md-4">
                <input type="radio" id="earth" name="transport_mode" value="earth" checked />
                <label>Earth</label>
              </div>

              <div class="radio radio-replace color-blue col-md-4">
                <input type="radio" id="sea" name="transport_mode" value="sea" />
                <label>Sea</label>
              </div>
              
              <div class="radio radio-replace color-blue col-md-4">
                <input type="radio" id="air" name="transport_mode" value="air" />
                <label>Air</label>
              </div>                          
              <span id="error_transport_mode"></span>
            </div>

          </div>


          <div class="form-group">
            <label for="category" class="col-sm-3 control-label">Select Category </label> 
            <div class="col-sm-5">
              <select id="category" name="cat_id" class="form-control select2" placeholder="Select Category ">
                <?php foreach ($categories as $c ): ?>
                  <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 || $c['cat_id'] == 281  ): ?>
                    <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                  <?php endif; ?>
                <?php endforeach; ?>
              </select>
            </div>
          </div>   

          <div class="form-group">
            <label for="vehicle_type" class="col-sm-3 control-label">Vehicle Type :</label>
            
            <div class="col-sm-5">
              <input type="text" class="form-control" id="vehicle_type" placeholder="Enter Vehicle Type..." name="vehicle_type" />  
              <span id="error_vehicle_type"></span>
            </div>
          </div>          

          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5">
              <button type="submit" class="btn btn-blue">Add Vehicle</button>
            </div>
          </div>
        </form>
        
      </div>
    
    </div>
  
  </div>
</div>

<br />