    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li>
        <a href="<?= base_url('admin/category_list'); ?>">Dangerous Goods</a>
      </li>
      <li class="active">
        <strong>Edit</strong>
      </li>
    </ol>
    
    <div class="row">
      <div class="col-md-12">
        
        <div class="panel  panel-dark" data-collapsed="0">
        
          <div class="panel-heading">
            <div class="panel-title">
              Edit New Dangerous Goods
            </div>
            
            <div class="panel-options">
              <a href="<?= base_url('admin/dangerous-goods'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
            </div>
          </div>
          
          <div class="panel-body">
            
            <?php if($this->session->flashdata('error')):  ?>
              <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
            <form role="form" class="form-horizontal form-groups-bordered" id="dangerous_goods_edit" action="<?= base_url('admin/dangerous-goods/update'); ?>" method="post" enctype="multipart/form-data">
              <div class="row">
                <input type="hidden" name="id" value="<?= $goods['id'];?>" />
                <div class="col-md-3 text-center">
                  <div class="form-group">
                    <label class="control-label">Dangerous Goods Image </label>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                        <?php if($goods['image_url'] !="NULL"): ?>
                          <img src="<?= base_url($goods['image_url']); ?>" alt="Dangerous Goods Image">
                        <?php else: ?>
                          <img src="<?= $this->config->item('resource_url').'noimage.png'; ?>" alt="Dangerous Goods Image">
                        <?php endif; ?>
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                      <div>
                        <span class="btn btn-white btn-file">
                          <span class="fileinput-new">Change Image</span>
                          <span class="fileinput-exists">Change</span>
                          <input type="file" name="goods_icon" id="goods_icon" accept="image/*" disabled>
                        </span>
                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-9">

                  <div class="form-group">
                    <div class="col-md-4">
                      <label for="code" class="control-label">Dangerous Goods Code</label>                    
                      <input type="text" class="form-control" id="code" placeholder="Dangerous Goods Code" name="code" maxlength="12" minlength="3" value="<?=$goods['code']?>"  disabled/>
                    </div>

                    <div class=" col-md-8">
                      <label for="name" class="control-label">Dangerous Goods Name</label>                    
                      <input type="text" class="form-control" id="name" placeholder="Dangerous Goods Name" name="name" value="<?=$goods['name']?>"  disabled/>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">Description ( Optional )</label>
                      <textarea name="description" id="description" class="form-control" placeholder="Dangerous Goods Description...." style="resize: none;" disabled><?= nl2br($goods['description'])?></textarea>
                    </div>              
                  </div>              
                  
                  <div class="row">
                    <div class="col-md-12">
                      <div class="text-center">
                        <a href="<?= base_url('admin/dangerous-goods'); ?>" class="btn btn-info">&nbsp;&nbsp; Back &nbsp;&nbsp;</a> &nbsp;&nbsp;
                        <button id="btn_edit" class="btn btn-green">&nbsp;&nbsp; Edit &nbsp;&nbsp;</button>
                        <button id="btn_submit" type="submit" class="btn btn-blue hidden">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
                      </div>
                    </div>
                  </div>

                </div>
            </form>
            
          </div>
        
        </div>
      
      </div>
    </div>
    
    <br />  


  <script>
    $(function(){
      $("#btn_edit").on('click', function(){
        $(this).addClass('hidden');
        $("#btn_submit").removeClass('hidden');
        $("input,textarea").prop('disabled', false);
        $('html, body').animate({scrollTop : 0},800);
        return false;
      });

      var somethingChanged = false;
      $('#dangerous_goods_edit input, #dangerous_goods_edit textarea').change(function() {  somethingChanged = true; });

      $("#code").bind('keyup', function (e) {
        if (e.which >= 97 && e.which <= 122) {
          var newKey = e.which - 32;
          // I have tried setting those
          e.keyCode = newKey;
          e.charCode = newKey;
        }

        $("#code").val(($("#code").val()).toUpperCase());
      });

      $("#dangerous_goods_edit").on('submit',function(e){e.preventDefault();});
    
      $("#btn_submit").on('click',function(){
          var name = $("#name").val();
          var code = $("#code").val();
          if(!somethingChanged){  swal('Error',"No change found!", 'warning'); }
          else{
            if(!code){    swal('Error',"Enter Dangerous Goods Code", 'warning');  }
            else if(code.length < 3 || code.length > 12 ){    swal('Error',"Dangerous Goods Code require minimum 3 and maximum 12 characters!", 'warning');  }
            else if(!name){   swal('Error',"Enter Dangerous Goods Name", 'warning'); }
            else{ $("#dangerous_goods_edit").get(0).submit(); }
          }
      });
    });
  </script>
    
