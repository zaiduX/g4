<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Authority List</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">Authority List</h2>
<?php
$per_manage_authority = explode(',', $permissions[0]['manage_authority']);
if(in_array('2', $per_manage_authority)) { ?>
	<a type="button" href="<?= base_url('admin/add-authority'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add Authority
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

	<?php if($this->session->flashdata('error')):  ?>
  		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
	<?php endif; ?>
	<?php if($this->session->flashdata('success')):  ?>
  		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
	<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Authority Name</th>
			<th class="text-center">Email</th>
			<th class="text-center">Type</th>
			<th class="text-center">Country</th>
			<th class="text-center">Status</th>
			<?php if(in_array('2', $per_manage_authority) || in_array('3', $per_manage_authority) || in_array('4', $per_manage_authority) || in_array('5', $per_manage_authority)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($authorities_details as $auth):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= $auth['auth_fname'] . ' ' . $auth['auth_lname'] ?></td>
			<td class="text-center"><?= $auth['auth_email'] ?></td>
			<td class="text-center"><?= $this->authorities->get_group_name_by_id($auth['auth_type_id']) ?></td>
			<td class="text-center"><?= $this->user->get_country_name_by_id($auth['country_id']); ?></td>
			<?php if($auth['auth_status'] == 1): ?>
				<td class="text-center">
					<label class="label label-success" ><i class="entypo-thumbs-up"></i>Active</label>
				</td>
			<?php else: ?>
				<td class="text-center">
					<label class="label label-danger" ><i class="entypo-cancel"></i>Inactive</label>
				</td>
			<?php endif; ?>
			<?php if(in_array('2', $per_manage_authority) || in_array('3', $per_manage_authority) || in_array('4', $per_manage_authority) || in_array('5', $per_manage_authority)) { ?>
				<td class="text-center">
					<?php if(in_array('3', $per_manage_authority)) { ?>
						<a href="<?= base_url('admin/edit-authority/') . $auth['auth_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
					<?php } ?>
					<?php if(in_array('2', $per_manage_authority)) { ?>
						<a class="hidden" href="<?= base_url('admin/assign-permission/') . $auth['auth_id']; ?>" class="btn btn-info btn-sm btn-icon icon-left">
							<i class="fa fa-key"></i>
							Permissions
						</a>
					<?php } ?>
					<?php if(in_array('4', $per_manage_authority)) { ?>	
						<?php if($auth['auth_status'] == 0): ?>
							<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to activate" onclick="inactivate_authority('<?= $auth['auth_id']; ?>');">
								<i class="entypo-thumbs-up"></i>Activate</button>
						<?php else: ?>
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to inactivate" onclick="activate_authority('<?= $auth['auth_id']; ?>');">
							<i class="entypo-cancel"></i>
							Inactivate</button>			
						<?php endif; ?>
					<?php } ?>
				</td>
			<?php } ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );

		

	} );

	function activate_authority(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this authority type?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('inactive-authority', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'authority type has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'authority type inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate_authority(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this authority type?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('active-authority', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'authority type has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'authority type activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>