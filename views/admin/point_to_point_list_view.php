<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Point to Point rates</strong>
  </li>
</ol>
      
<h2 style="display: inline-block;">Point to Point rates</h2>
<?php
$per_standard_rates = explode(',', $permissions[0]['rates']);

if(in_array('2', $per_standard_rates)) { ?>
  
  <div class="btn-group left-dropdown pull-right">
    <button type="button" class="btn btn-green dropdown-toggle btn-icon icon-left" data-toggle="dropdown">
      <i class="fa fa-plus right"></i>  Add New Rates | <span class="caret"></span>
    </button>
    
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?= base_url('admin/p-to-p-rates/add/volume-based'); ?>"><i class="fa fa-plus right"></i> Volume Based Rate</a></li>
      <li class="divider"></li>
      <li><a href="<?= base_url('admin/p-to-p-rates/add/weight-based'); ?>"><i class="fa fa-plus right"></i> Weight Based Rate</a></li>
      <li class="divider"></li>
      <li><a href="<?= base_url('admin/p-to-p-rates/add/formula-volume-based'); ?>"><i class="fa fa-plus right"></i> Formula Volume Based Rate</a></li>
      <li class="divider"></li>
      <li><a href="<?= base_url('admin/p-to-p-rates/add/formula-weight-based'); ?>"><i class="fa fa-plus right"></i> Formula Weight Based Rate</a></li>
    </ul>
  </div>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      <th class="text-center">#</th>
      <th class="text-center">Category</th>
      <th class="text-center">From</th>
      <th class="text-center">To</th>
      <th class="text-center">Volume</th>
      <th class="text-center">Weight</th>
      <?php if(in_array('3', $per_standard_rates) || in_array('5', $per_standard_rates)) { ?>
        <th class="text-center">Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>
    <?php $offset = $this->uri->segment(3,0) + 1; ?>
    <?php foreach ($rates as $r):  ?>
      <?php 
        $from = $r['from_country_name'];          
        if($r['from_state_id'] > 0 ){ $state = $this->pp->get_state_detail_by_id($r['from_state_id']); $from.= " | ".$state['state_name'];  }  
        if($r['from_city_id'] > 0 ){ $city = $this->pp->get_city_detail_by_id($r['from_city_id']); $from.= " | ".$city['city_name']; } 
        
        $to = $r['to_country_name'];
        if($r['to_state_id'] > 0 ){ $state = $this->pp->get_state_detail_by_id($r['to_state_id']); $to.= " | ".$state['state_name'];  }  
        if($r['to_city_id'] > 0 ){ $city = $this->pp->get_city_detail_by_id($r['to_city_id']); $to.= " | ".$city['city_name']; } 
      ?>
    <tr>
      <td class="text-center"><?= $offset++; ?></td>
      <td class="text-center"><?= $r['cat_name']; ?></td>
      <td class="text-center"><?= $from; ?></td>
      <td class="text-center"><?= $to; ?></td>
      <?php if($r['unit_id'] == 0): ?>
        <?php if($r['is_formula_volume_rate'] == 0 && $r['is_formula_weight_rate'] == 0): ?>
          <td class="text-center"><?= $r['min_volume'] . ' - ' . $r['max_volume'] . ' cm<sup>3</sup>'; ?></td>
        <?php else: ?>
          <?php if($r['is_formula_volume_rate'] == 1): ?>
            <td class="text-center">1 cm<sup>3</sup></td>
          <?php else: ?>
            <td class="text-center"><?= 'NA'; ?></td>
          <?php endif; ?>
        <?php endif; ?>
      <?php else: ?>
        <td class="text-center"><?= 'NA'; ?></td>
      <?php endif; ?>
      <?php if($r['unit_id'] > 0): $unit = $this->standard_rates->get_unit_detail($r['unit_id']); ?>
        <?php if($r['is_formula_volume_rate'] == 0 && $r['is_formula_weight_rate'] == 0): ?>
          <td class="text-center"><?= $r['min_weight'] . ' - ' . $r['max_weight'] .' '. $unit['shortname'] ; ?></td>
        <?php else: ?>
          <?php if($r['is_formula_volume_rate'] == 1): ?>
            <td class="text-center"><?= 'NA'; ?></td>
          <?php else: ?>
            <td class="text-center">1 <?=$unit['shortname']?></td>
          <?php endif; ?>
        <?php endif; ?>
      <?php else: ?>
        <td class="text-center"><?= 'NA'; ?></td>
      <?php endif; ?>
      <?php if(in_array('3', $per_standard_rates) || in_array('5', $per_standard_rates)): ?>
        <td class="text-center" style="display: flex;">
          <?php if(in_array('3', $per_standard_rates)): ?>            
            <a href="<?= base_url('admin/p-to-p-rates/edit/') . $r['rate_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to View / Edit Rates"><i class="entypo-pencil"></i> View / Edit</a>&nbsp;
          <?php endif; ?>
          <?php if(in_array('5', $per_standard_rates)): ?>            
            <button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_rate('<?= $r["rate_id"]; ?>');">
              <i class="entypo-cancel"></i> Delete          
            </button>
          <?php endif; ?>
        </td>
      <?php endif; ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "bStateSave": true
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    // Highlighted rows
    $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
      var $this = $(el),
        $p = $this.closest('tr');
      
      $( el ).on( 'change', function() {
        var is_checked = $this.is(':checked');
        
        $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
      } );
    } );
    
    // Replace Checboxes
    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );    
  } );


  function delete_rate(id=0){   
    swal({
      title: 'Are you sure?',
      text: "You want to delete this Rate?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true,
    }).then(function () {
      $.post('p-to-p-rates/delete', {id: id}, function(res){
        if(res == 'success'){
          swal(
            'Deleted!',
            'Rate successfully deleted.',
            'success'
          ). then(function(){   window.location.reload();  });
        }
        else {
          swal(
            'Failed!',
            'Rate deletion failed.',
            'error'
          )
        }
      });
      
    }, function (dismiss) {  if (dismiss === 'cancel') {  } });
  }

</script>