<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/cancellation-rescheduling-bus-reservation'); ?>">Bus Cancellation & Rescheduling</a>
  </li>
  <li class="active">
    <strong>Add Cancellation & Rescheduling Configuration</strong>
  </li>
</ol>
<style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add New Configuration
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/cancellation-rescheduling-bus-reservation'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/cancellation-rescheduling-bus-reservation/update'); ?>" class="form-horizontal form-groups-bordered validate" id="cancellation_edit" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="bcr_id" value="<?= $cancellation['bcr_id'] ?>">
          <input type="hidden" name="country_id" value="<?= $cancellation['country_id'] ?>">
          <input type="hidden" name="vehical_type_id" value="<?= $cancellation['vehical_type_id'] ?>">
            <div class="row">
              <div class="col-md-4">
                <label for="vehicle_type" class="control-label">Country </label> 
                <input type="text" class="form-control" id="vehicle_type" name="vehicle_type" value="<?=$this->charges->get_vehicles_by_type_id($cancellation['vehical_type_id'])['vehicle_type'];?>" readonly />  
                <span id="error_country_id" class="error"></span> 
              </div>
              <div class="col-md-4">
                <label for="bcr_min_hours" class="control-label">Minimum</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="bcr_min_hours" placeholder="Enter hours" name="bcr_min_hours" value="<?= $cancellation['bcr_min_hours'] ?>" />  
                  <span class="input-group-addon currency">Hrs.</span>
                </div>
              </div>
              <div class="col-md-4">
                <label for="bcr_max_hours" class="control-label">Maximum</label> 
                <div class="input-group">
                  <input type="number" class="form-control" id="bcr_max_hours" placeholder="Enter hours" name="bcr_max_hours" value="<?= $cancellation['bcr_max_hours'] ?>" />  
                  <span class="input-group-addon currency">Hrs.</span>
                </div>
              </div>
              <div class="clear"></div><br />
            </div>
            <div class="row">
              <div class="col-md-4">
                <label for="country_name" class="control-label">Country </label> 
                <input type="text" class="form-control" id="country_name" name="country_name" value="<?=$this->user->get_country_name_by_id($cancellation['country_id']);?>" readonly />  
                <span id="error_country_id" class="error"></span> 
              </div>
              <div class="col-md-4">
                <label for="bcr_cancellation" class="control-label">Cancellation</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="bcr_cancellation" placeholder="Enter Percentage" name="bcr_cancellation"  value="<?= $cancellation['bcr_rescheduling'] ?>" min="1" max="100" />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-4">
                <label for="bcr_rescheduling" class="control-label">Rescheduling</label> 
                <div class="input-group">
                  <input type="number" class="form-control" id="bcr_rescheduling" placeholder="Enter percentage" name="bcr_rescheduling"  value="<?= $cancellation['bcr_rescheduling'] ?>" min="1" max="100" />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="clear"></div><br />
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="text-center">
                  <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<br />

<script>

  $(function(){

    $("#cancellation_edit").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();

      var bcr_cancellation = parseFloat($("#bcr_cancellation").val()).toFixed(2);
      var bcr_rescheduling = parseFloat($("#bcr_rescheduling").val()).toFixed(2);
      var bcr_min_hours = parseFloat($("#bcr_min_hours").val()).toFixed(2);
      var bcr_max_hours = parseFloat($("#bcr_max_hours").val()).toFixed(2);

      if(isNaN(bcr_min_hours)) {  swal('Error','Minimum hours value is Invalid! Enter Number Only.','warning');   } 
      else if(bcr_min_hours < 0) {  swal('Error','Minimum hours value is Invalid!','warning');   } 

      else if(isNaN(bcr_max_hours)) {  swal('Error','Maximum hours value is Invalid! Enter Number Only.','warning');   } 
      else if(bcr_max_hours < 0) {  swal('Error','Maximum hours value is Invalid!','warning');   } 

      else if(parseFloat(bcr_max_hours) <= parseFloat(bcr_min_hours) ) { swal({title:'Error',text:'Invalid! Maximum Hours Should greater than Minimum Hours!',type:'warning'}).then(function(){ $("#bcr_max_hours").focus();  }); }
      
      else if(isNaN(bcr_cancellation)) {  swal('Error','Cancellation Percentage is Invalid! Enter Number Only.','warning');   } 
      else if(bcr_cancellation < 0 && bcr_cancellation > 100) {  swal('Error','Cancellation Percentage is Invalid!','warning');   } 
      
      else if(isNaN(bcr_rescheduling)) {  swal('Error','Rescheduling Percentage is Invalid! Enter Number Only.','warning');   } 
      else if(bcr_rescheduling < 0 && bcr_rescheduling > 100) {  swal('Error','Rescheduling Percentage is Invalid!','warning');   } 
      
      else {  $("#cancellation_edit")[0].submit();  }
    });
  });
</script>