<style type="text/css">
.profile-env section.profile-feed .profile-post-form .form-options {padding-bottom: 0px; }
.story-content { line-height: 25px; }
</style>

    <style>

    <!-- Progress with steps -->

    ol.progtrckr {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }

    ol.progtrckr li {
        display: inline-block;
        text-align: center;
        line-height: 3em;
    }

    ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
    ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
    ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
    ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
    ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
    ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
    ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
    ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

    ol.progtrckr li.progtrckr-done {
        color: black;
        border-bottom: 4px solid yellowgreen;
    }
    ol.progtrckr li.progtrckr-todo {
        color: silver; 
        border-bottom: 4px solid silver;
    }

    ol.progtrckr li:after {
        content: "\00a0\00a0";
    }
    ol.progtrckr li:before {
        position: relative;
        bottom: -2.5em;
        float: left;
        left: 50%;
        line-height: 1em;
    }
    ol.progtrckr li.progtrckr-done:before {
        content: "\2713";
        color: white;
        background-color: yellowgreen;
        height: 1.2em;
        width: 1.2em;
        line-height: 1.2em;
        border: none;
        border-radius: 1.2em;
    }
    ol.progtrckr li.progtrckr-todo:before {
        content: "\039F";
        color: silver;
        background-color: white;
        font-size: 1.5em;
        bottom: -1.6em;
    }

</style>

    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li>
        <a href="<?= base_url('admin/get-all-orders'); ?>">Ticket Booking Details</a>
      </li>
      <li class="active">
        <strong>Ticket Booking Details</strong>
      </li>
    </ol>
    <div class="profile-env">
      
      <header class="row">
        
        <div class="col-sm-2">
          <?php if($customer['avatar_url'] != "NULL"): ?>
            <a class="profile-picture">
              <img src="<?= $this->config->item('base_url') . $customer['avatar_url'];?>" style="height: 150px" class="img-responsive img-thumbnail" />
            </a>
          <?php else: ?>
            <a class="profile-picture">
              <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg';?>" style="height: 150px" class="img-responsive img-thumbnail" />
            </a>
          <?php endif; ?>
          
        </div>
        
        <div class="col-sm-10">
          
          <ul class="profile-info-sections">
            <li style="padding-left: 15px; padding-right: 15px">
              <div class="profile-name">
                <strong>
                  <h6><i class="fa fa-user"></i> Customer</h6>
                  <a><?=$customer['firstname']=='NULL'?'User':$customer['firstname'].' '.$customer['lastname'];?></a>
                </strong>
                <!-- <strong>
                   <a><i class="fa fa-phone"></i> <?php if($customer['mobile1'] !== "NULL") echo '+'.$booking_details['country_phonecode'].'-'.$customer['mobile1']; else echo 'Not provided!';  ?></a>
                </strong> -->
                <span>
                  <?php if( strtolower($customer['gender']) === 'm'): ?>
                    <a><i class="fa fa-male"></i> Male</a>
                  <?php else: ?>
                    <a><i class="fa fa-female"></i> Female</a>
                  <?php endif; ?>
                </span>
              </div>
            </li>
            
            <li style="padding-left: 15px; padding-right: 15px">
              <div class="profile-stat">
                <h6><i class="fa fa-paper-plane-o"></i> Source </h6>
                <h6><i class="fa fa-user"></i> <?= $booking_details['source_point']  ?></h6>
                <h6><i class="fa fa-address-card"></i> <?= $booking_details['pickup_point']  ?></h6>
                <!-- <h6><i class="fa fa-phone"></i> <?php if($order_details['from_address_contact'] !== "NULL") echo '+'.$order_country['country_phonecode'].'-'.$order_details['from_address_contact']; else echo 'Not provided!';  ?></h6> -->
              </div>
            </li>
            
            <li style="padding-left: 15px; padding-right: 15px">
              <div class="profile-stat">
                <h6><i class="fa fa-inbox"></i> Destination </h6>
                <h6><i class="fa fa-user"></i> <?= $booking_details['destination_point']  ?></h6>
                <h6><i class="fa fa-address-card"></i> <?= $booking_details['drop_point']  ?></h6>
                <!-- <h6><i class="fa fa-phone"></i> <?php if($booking_details['to_address_contact'] !== "NULL") echo '+'.$order_country['country_phonecode'].'-'.$booking_details['to_address_contact']; else echo 'Not provided!';  ?></h6> -->
              </div>
            </li>
          </ul>
          
        </div>
        
      </header>
      
      <section class="profile-info-tabs">
        
        <div class="row">
          
          <div class="col-sm-offset-2 col-sm-11">
            
            <ul class="user-details">
              <li>
                <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Order Status">
                  <i class="fa fa-play"></i>Trip Status - <span> <?php 
                      if($booking_details['journey_date'] > date('m/d/Y')){ echo "Upcoming"; } 
                      if($booking_details['journey_date'] == date('m/d/Y')){ echo "Current"; } 
                      if($booking_details['journey_date'] < date('m/d/Y')){ echo "Current"; } ?> 
                    </span>
                </a>
              </li>
              <li>
                <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Status Update on">
                    <i class="fa fa-calendar"></i> Trip journey Date - <?= $booking_details['journey_date'] ?>
                </a>
              </li>
              <li>
                <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Expiry Date">
                    <i class="fa fa-calendar"></i> Booking Date - <span> <?= $booking_details['bookig_date'] ?></span>
                </a>
              </li>
            </ul>
          </div>

          
        <div class="story-content"> 
              <div class="col-md-12"><i class="fa fa-flask"></i> &nbsp; <a>Passenger Details:</a>
              </div>
              <div class="col-md-12">
                <table id="tableData" class="table table-striped table-bordered table-hover">
                  <tr>
                    <th><font color="#ec5956">Seat id</font></th>
                    <th><font color="#ec5956"> Name</font></th>
                    <th><font color="#ec5956">Seat Type</font></th>
                    <th><font color="#ec5956">Mobile</font></th>
                    <th><font color="#ec5956">Email</font></th>
                    <th><font color="#ec5956">Ticket price</font></th>
                  </tr>
                  <?php foreach ($booking_details_seat_details as $det) { ?>
                    <tr>
                      <td><?=$det['seat_id']?></td>
                      <td><?= $det['firstname']." ".$det['lastname']?></td>
                      <td><?=$det['seat_type']?></td>
                      <td><?=$det['mobile']?></td>
                      <td><?=$det['email_id']?></td>
                      <td><?=$booking_details['seat_price']." ".$booking_details['currency_sign']?></td>
                    </tr>
                  <?php } ?>
                  <tr>
                      <td></td><td></td><td></td><td></td>
                      <td><font color="#ec5956">Grand Total</font></td>
                      <td><font color="#ec5956"><?=$booking_details['ticket_price']." ".$booking_details['currency_sign']?></font></td>
                  </tr>
                </table>
              </div>
            </div>
        </div>
        
      </section>
      
      <div class="tab-content">
        <div id="order_details" class="tab-pane fade in active">
          <section class="profile-feed col-md-10">
            <h4><font color="#ec5956"> Operator Details </font></h4>
            <div class="profile-stories">
              <div class="story-content"> 
                <div class="col-md-3">&nbsp; <a>Operator Company Name:</a>
                </div>
                <div class="col-md-9">
                  <?= $bus_operator_profile['company_name']?>
                </div>
              </div>
              <div class="story-content"> 
                <div class="col-md-3">&nbsp; <a>Operator Name:</a>
                </div>
                <div class="col-md-9">
                  <?= $bus_operator_profile['firstname']." ".$bus_operator_profile['lastname']?>
                </div>
              </div>
              <div class="story-content"> 
                <div class="col-md-3">&nbsp; <a>Email:</a>
                </div>
                <div class="col-md-9">
                  <?= $bus_operator_profile['email_id']?>
                </div>
              </div>
              <div class="story-content"> 
                <div class="col-md-3">&nbsp; <a>Mobile:</a>
                </div>
                <div class="col-md-9">
                  <?= $bus_operator_profile['contact_no']?>
                </div>
              </div>
              <div class="story-content"> 
                <div class="col-md-3">&nbsp; <a>Address:</a>
                </div>
                <div class="col-md-9">
                  <?= $bus_operator_profile['address']?>
                </div>
              </div>
            </div>          
          </section>
        </div>
        
      </div>
    </div>
    
    <br />

    <script type="text/javascript">
      jQuery( document ).ready( function( $ ) {
        var $table2 = jQuery( '#table-2' );
        // Initialize DataTable
        $table2.DataTable( {
          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          "bStateSave": true
        });
        // Initalize Select Dropdown after DataTables is created
        $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
          minimumResultsForSearch: -1
        });
        // Highlighted rows
        $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
          var $this = $(el),
            $p = $this.closest('tr');
          $( el ).on( 'change', function() {
            var is_checked = $this.is(':checked');
            $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
          } );
        } );
        // Replace Checboxes
        $table2.find( ".pagination a" ).click( function( ev ) {
          replaceCheckboxes();
        } );
      } );
    </script>

    <script type="text/javascript">
      jQuery( document ).ready( function( $ ) {
        var $table2 = jQuery( '#table-1' );
        // Initialize DataTable
        $table2.DataTable( {
          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          "bStateSave": true
        });
        // Initalize Select Dropdown after DataTables is created
        $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
          minimumResultsForSearch: -1
        });
        // Highlighted rows
        $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
          var $this = $(el),
            $p = $this.closest('tr');
          $( el ).on( 'change', function() {
            var is_checked = $this.is(':checked');
            $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
          } );
        } );
        // Replace Checboxes
        $table2.find( ".pagination a" ).click( function( ev ) {
          replaceCheckboxes();
        } );
      } );
    </script>