<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Customer Support</strong>
	</li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>			
<h2 style="display: inline-block;">Customer Support List</h2>
<?php $per_customer_support = explode(',', $permissions[0]['customer_support']); ?>

<?php if($this->session->flashdata('error')):  ?>
		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
<?php endif; ?>
<?php if($this->session->flashdata('success')):  ?>
		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
<?php endif; ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">ID</th>
			<th class="text-center">Date</th>
			<th class="text-center">Customer Details</th>
			<th class="text-center">Type</th>
			<th class="text-center">Status</th>
			<th class="text-center">Response Date / Time</th>
			<?php if(in_array('3', $per_customer_support)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($customer_support_list as $csl):  ?>
		<tr>
			<td class="text-center"><?= $csl['ticket_id'] ?></td>
			<td class="text-center"><?= date('d/m/Y', strtotime($csl['cre_datetime'])) ?></td>
			<td class="text-center"><?= $this->support->get_customer_details($csl['cust_id']) ?></td>
			<td class="text-center"><?= strtoupper($csl['type']) ?></td>
			
			<td class="text-center">
				<?php 
					if($csl['status'] == 'open') { $status = 1; echo '<span class="text-info"> Open </span>'; }
					else if($csl['status'] == 'in_progress') { $status = 1; echo '<span class="text-warning"> in-progress </span>'; }
					else if($csl['status'] == 'close') { $status = 0;  echo '<span class="text-success"> Closed </span>'; }					
					else { $status = 0;  echo '<span class="text-danger"> Cancelled </span>'; }
				?>		
			</td>
			<td class="text-center">
				<?php
					if($csl['res_datetime'] == 'NULL') { echo 'NA'; }
					else { echo date('d/m/Y', strtotime($csl['res_datetime'])); }
					?>
			</td>
			
			<?php if(in_array('3', $per_customer_support)) { ?>
				<td class="text-center">
					<?php if(in_array('3', $per_customer_support)): ?>	
						<form id="accept_form" action="<?= base_url('admin/reply-support-request') ?>" method="post">
							<input type="hidden" name="query_id" value="<?= $csl['query_id']; ?>" />
							<button type="submit" title="View / Edit" class="btn btn-success btn-sm"><i class="entypo-pencil"></i> View / Edit </button>	
						</form>
				<?php endif; ?>
				</td>
			<?php } ?>

		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );

		

	} );

</script>