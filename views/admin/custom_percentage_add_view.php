<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/custom-percentage'); ?>">Custom Commission Percentage</a>
  </li>
  <li class="active">
    <strong>Add New Configuration</strong>
  </li>
</ol>
<style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add New Custom Percentage Setup
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/custom-percentage'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/custom-percentage/register'); ?>" class="form-horizontal form-groups-bordered validate" id="custom_percentage_add" method="post" autocomplete="off" novalidate="novalidate">
          
          <div class="border">
            <div class="row">
              <div class="col-md-4">
                <label for="country_id" class="control-label">Select Country </label> 
                <select id="country_id" name="country_id" class="form-control select2">
                  <option value="">Select Country</option>
                  <?php foreach ($countries as $country): ?> 
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
                <span id="error_country_id" class="error"></span> 
              </div>
              <div class="col-md-8">
                <label for="category" class="control-label">Select Category </label> 
                <select id="category" name="category[]" class="form-control select2" multiple placeholder="Select Category ">
                  <?php foreach ($categories as $c ): ?>
                    <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
                      <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
                <span class="error" id="error_category"></span> 
              </div>             
            </div>
            <div class="clear"></div><br />
          </div>
                
          <div class="border">
            <div class="row">

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Custom For Earth</h4></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <label for="EICCP" class="control-label text-center"> New Goods ( % )</label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="EICCPNG" placeholder="Percent" name="EICCPNG"  min="0" max="100" />  
                        <span class="input-group-addon currency">%</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label for="EICCP" class="control-label text-center"> Old Goods ( % )</label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="EICCPOG" placeholder="Percent" name="EICCPOG"  min="0" max="100" />  
                        <span class="input-group-addon currency">%</span>
                      </div>
                    </div>
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Custom For Air</h4></div>
                </div>

                <div class="row">    
                  <div class="col-md-6">
                    <label for="AICCP" class="control-label"> New Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="AICCPNG" placeholder="Percent" name="AICCPNG"  min="0" max="100" />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="AICCP" class="control-label"> Old Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="AICCPOG" placeholder="Percent" name="AICCPOG"  min="0" max="100" />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Custom For Sea</h4></div>
                </div>
                
                <div class="row">    
                  <div class="col-md-6">
                    <label for="SICCP" class="control-label"> New Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="SICCPNG" placeholder="Percent" name="SICCPNG"  min="0" max="100" />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="SICCP" class="control-label"> Old Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="SICCPOG" placeholder="Percent" name="SICCPOG"  min="0" max="100" />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div> 
            </div>
          </div>

          <div class="border">
            <div class="row">

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Gonagoo Custom % For Earth</h4></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <label for="EICCP" class="control-label text-center"> New Goods ( % )</label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="EIGCPNG" placeholder="Percent" name="EIGCPNG"  min="0" max="100" />  
                        <span class="input-group-addon currency">%</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label for="EICCP" class="control-label text-center"> Old Goods ( % )</label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="EIGCPOG" placeholder="Percent" name="EIGCPOG"  min="0" max="100" />  
                        <span class="input-group-addon currency">%</span>
                      </div>
                    </div>
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Gonagoo Custom % For Air</h4></div>
                </div>

                <div class="row">    
                  <div class="col-md-6">
                    <label for="AICCP" class="control-label"> New Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="AIGCPNG" placeholder="Percent" name="AIGCPNG"  min="0" max="100" />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="AICCP" class="control-label"> Old Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="AIGCPOG" placeholder="Percent" name="AIGCPOG"  min="0" max="100" />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Gonagoo Custom % For Sea</h4></div>
                </div>
                
                <div class="row">    
                  <div class="col-md-6">
                    <label for="SICCP" class="control-label"> New Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="SIGCPNG" placeholder="Percent" name="SIGCPNG"  min="0" max="100" />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="SICCP" class="control-label"> Old Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="SIGCPOG" placeholder="Percent" name="SIGCPOG"  min="0" max="100" />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div> 
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="text-center">
                <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
          
        </form>
        
      </div>
    
    </div>
  
  </div>
</div>

<br />

<script>
  $(function(){

    $("#custom_percentage_add").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();

    var country_id = $("#country_id").val();
    var category = $("#category").val();
    // Earth
    var EICCPNG = parseFloat($("#EICCPNG").val()).toFixed(2);
    var EICCPOG = parseFloat($("#EICCPOG").val()).toFixed(2);
    var EIGCPNG = parseFloat($("#EIGCPNG").val()).toFixed(2);
    var EIGCPOG = parseFloat($("#EIGCPOG").val()).toFixed(2);
    // Air
    var AICCPNG = parseFloat($("#AICCPNG").val()).toFixed(2);
    var AICCPOG = parseFloat($("#AICCPOG").val()).toFixed(2);
    var AIGCPNG = parseFloat($("#AIGCPNG").val()).toFixed(2);
    var AIGCPOG = parseFloat($("#AIGCPOG").val()).toFixed(2);
    // Sea
    var SICCPNG = parseFloat($("#SICCPNG").val()).toFixed(2);
    var SICCPOG = parseFloat($("#SICCPOG").val()).toFixed(2);
    var SIGCPNG = parseFloat($("#SIGCPNG").val()).toFixed(2);
    var SIGCPOG = parseFloat($("#SIGCPOG").val()).toFixed(2);

    if(country_id <= 0 ) {  swal('Error','Please Select Country','warning');   } 
    else if(!category) {  swal('Error','Please Select at least one Category','warning');   }
    // Earth      
    else if(isNaN(EICCPNG)) {  swal('Error','New Goods Custom Percentage For Earth is Invalid! Enter Number Only.','warning');   } 
    else if(EICCPNG < 0 && EICCPNG > 100) {  swal('Error','New Goods Custom Percentage For Earth is Invalid!','warning');   } 
    else if(isNaN(EICCPOG)) {  swal('Error','Old Goods Custom Percentage For Earth is Invalid! Enter Number Only.','warning');   } 
    else if(EICCPOG < 0 && EICCPOG > 100) {  swal('Error','Old Goods Custom Percentage For Earth is Invalid!','warning');   } 
    // Air      
    else if(isNaN(AICCPNG)) {  swal('Error','New Goods Custom Percentage For Air is Invalid! Enter Number Only.','warning');   } 
    else if(AICCPNG < 0 && AICCPNG > 100) {  swal('Error','New Goods Custom Percentage For Air is Invalid!','warning');   }
    else if(isNaN(AICCPOG)) {  swal('Error','Old Goods Custom Percentage For Air is Invalid! Enter Number Only.','warning');   } 
    else if(AICCPOG < 0 && AICCPOG > 100) {  swal('Error','Old Goods Custom Percentage For Air is Invalid!','warning');   }
    // Sea      
    else if(isNaN(SICCPNG)) {  swal('Error','New Goods Custom Percentage For Sea is Invalid! Enter Number Only.','warning');   } 
    else if(SICCPNG < 0 && SICCPNG > 100) {  swal('Error','New Goods Custom Percentage For Sea is Invalid!','warning');   }
    else if(isNaN(SICCPOG)) {  swal('Error','Old Goods Custom Percentage For Sea is Invalid! Enter Number Only.','warning');   } 
    else if(SICCPOG < 0 && SICCPOG > 100) {  swal('Error','Old Goods Custom Percentage For Sea is Invalid!','warning');   }
    // Earth Gonagoo Comission
    else if(isNaN(EIGCPNG)) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Earth is Invalid! Enter Number Only.','warning');   }
    else if(EIGCPNG < 0 && EIGCPNG > 100) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Earth is Invalid!','warning');   } 
    else if(isNaN(EIGCPOG)) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Earth is Invalid! Enter Number Only.','warning');   }
    else if(EIGCPOG < 0 && EIGCPOG > 100) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Earth is Invalid!','warning');   }
    // Air Gonagoo Comission
    else if(isNaN(AIGCPNG)) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Air is Invalid! Enter Number Only.','warning');   } 
    else if(AIGCPNG < 0 && AIGCPNG > 100) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Air is Invalid!','warning');   }
    else if(isNaN(AIGCPOG)) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Air is Invalid! Enter Number Only.','warning');   } 
    else if(AIGCPOG < 0 && AIGCPOG > 100) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Air is Invalid!','warning');   }
    // Sea Gonagoo Comission
    else if(isNaN(SIGCPNG)) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Sea is Invalid! Enter Number Only.','warning');   } 
    else if(SIGCPNG < 0 && SIGCPNG > 100) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Sea is Invalid!','warning');   }
    else if(isNaN(SIGCPOG)) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Sea is Invalid! Enter Number Only.','warning');   } 
    else if(SIGCPOG < 0 && SIGCPOG > 100) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Sea is Invalid!','warning');   }

    else {  $("#custom_percentage_add")[0].submit();  }

    });
  });
</script>