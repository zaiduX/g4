<?php
  if($user_details['firstname'] != 'NULL') {
    $full_name = $user_details['firstname'] . ' ' . $user_details['lastname'];
  } else {
    $email_cut = explode('@', $user_details['email1']);  
    $full_name = $email_cut[0];
  }
  $dt_range = date('Y-m-01').','.date('Y-m-t');
?>

<ol class="breadcrumb bc-3" style="padding-bottom: 0px; padding-top: 0px;">
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Orders List</strong>
  </li>
</ol>

<h3 style="display: inline-block;"><?=$full_name?> - Pending Payments Orders List</h3>

  <?php if($this->session->flashdata('error')):  ?>
      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
      <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
  <?php endif; ?>

<script>
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    $table2.DataTable( {
      "aaSorting": [[1, "desc"]],
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'excelHtml5',
          footer: true,
          text: "<i class='fa fa-file-excel-o'></i> Export to Excel",
          title: "<?=$full_name?>-Pending-Payments-Orders-List-"+"<?=date('F-Y')?>",
          exportOptions: {
            columns: ':not(:last-child)'
          },
          customize: function( xlsx ) {
            var sheet = xlsx.xl.worksheets['sheet1.xml'];
            //Column Width
            var col = $('col', sheet);
            $(col[0]).attr('width', 8);
            $(col[1]).attr('width', 17);
            $(col[2]).attr('width', 18);
            $(col[5]).attr('width', 13);
            $(col[6]).attr('width', 11);
            $(col[7]).attr('width', 20);
            $(col[8]).attr('width', 18);
            //Table header Labels
            $('c[r=A2] t', sheet).text( 'Order ID' );
            $('c[r=B2] t', sheet).text( 'Order Create Date' );
            $('c[r=C2] t', sheet).text( 'Order Deliver Date' );
            $('c[r=D2] t', sheet).text( 'From Address' );
            $('c[r=E2] t', sheet).text( 'To Address' );
            $('c[r=F2] t', sheet).text( 'Order Country' );
            $('c[r=G2] t', sheet).text( 'Order Price' );
            $('c[r=H2] t', sheet).text( 'Gonagoo Commission' );
            $('c[r=I2] t', sheet).text( 'Total Amount Due' );
            //Header text center
            $('row c[r^="A"]', sheet).attr( 's', '51' );
            $('row c[r^="B"]', sheet).attr( 's', '51' );
            $('row c[r^="C"]', sheet).attr( 's', '51' );
            $('row c[r^="D"]', sheet).attr( 's', '51' );
            $('row c[r^="E"]', sheet).attr( 's', '51' );
            $('row c[r^="F"]', sheet).attr( 's', '51' );
            $('row c[r^="G"]', sheet).attr( 's', '51' );
            $('row c[r^="H"]', sheet).attr( 's', '51' );
            $('row c[r^="I"]', sheet).attr( 's', '51' );
            $('row c[r^="J"]', sheet).attr( 's', '51' );
            //Make bold report title
            $('row:first c', sheet).attr( 's', '2' );
          },
        }
      ]
    });
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });
  } );
</script>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary <?=(isset($filter) && $filter == 1)?'':'panel-collapse'?>" data-collapsed="0">    
      <div class="panel-heading">
        <div class="panel-title"><label>Filter Records by date and currency</label></div>
        <div class="panel-options">
          <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
        </div>
      </div>
      <div class="panel-body" style="<?=(isset($filter) && $filter == 1)?'':'display:none;'?> padding-bottom: 0px;">
        <form action="<?=base_url('admin/users/pending-orders-filter');?>" method="post" id="frmFilter">
          <input type="hidden" name="cust_id" value="<?=$user_details['cust_id']?>" id="cust_id">
          <input type="hidden" name="filter" value="1">
          <div class="row">
            <div class="col-sm-4 form-group">
              <label class=control-label">Order Date Range</label>
              <input type="text" class="form-control daterange" name="date_range" data-format="YYYY-MM-DD" data-separator="," id="date_range" value="<?=(isset($date_range))?$date_range:$dt_range?>" autocomplete="off" />
            </div>
            <div class="col-sm-4 form-group">
              <label class="control-label">Select Currency</label>
              <select class="form-control select2" name="currency_sign" id="currency_sign">
                <option value="0">Select currency</option>
                <?php foreach ($currency_list as $currency): ?>
                  <option value="<?=$currency['currency_sign']?>" <?=(isset($currency_sign) && $currency_sign == $currency['currency_sign'])?'selected':''?> ><?=$currency['currency_sign']?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-md-4">
              <label class="control-label">Select Country </label>                        
              <select id="country_id" name="country_id" class="form-control select2">
                <option value="">Select Country</option>
                <?php foreach ($countries as $country): ?>                      
                  <option value="<?= $country['country_id'] ?>" <?=(isset($country_id) && $country_id == $country['country_id'])?'selected':''?> ><?= $country['country_name']; ?></option>
                <?php endforeach ?>
              </select>
              <span id="error_country_id" class="error"></span>                           
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 text-right">
              <div class="form-group" style="padding-left: 0px;">
                <button type="submit" class="btn btn-outline btn-info" id="">&nbsp;Filter&nbsp;</button>&nbsp;
                <a href="<?=base_url('admin/users/pending-orders/'.$user_details['cust_id'])?>" class="btn btn-outline btn-warning">Reset Filter</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<table class="table table-bordered display compact table-striped" id="table-2" style="max-width:100%">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Create Date</th>
      <th class="text-center">Deliver Date</th>
      <th class="text-center">From</th>
      <th class="text-center">To</th>
      <th class="text-center">Country</th>
      <th class="text-center">Price</th>
      <th class="text-center">Gonagoo Comm.</th>
      <th class="text-center">Due Amount</th>
      <th class="text-center">Currency</th>
      <th class="text-center">Actions</th>
    </tr>
  </thead>
  <tbody>   
    <?php foreach ($order_list as $orders): static $grand_total = 0; ?>
    <tr>
      <td class="text-center"><?= $orders['order_id'] ?></td>
      <td class="text-center"><?= date('d/m/Y', strtotime($orders['cre_datetime'])); ?></td> 
      <td class="text-center"><?= ($orders['delivered_datetime']!='NULL')?date('d/m/Y', strtotime($orders['delivered_datetime'])):'Pending'; ?></td> 
      <td class="text-center" style="word-break: break-all !important;"><?= $orders['from_address'] ?></td>
      <td class="text-center" style="word-break: break-all !important;"><?= $orders['to_address'] ?></td>
      <td class="text-center">
      <?php 
        $country_details = $this->users->get_country_details($orders['from_country_id']);
        $country_name = $country_details['country_name'];
        echo $country_name;
      ?>
      </td>
      <td class="text-center"><?= $orders['order_price']; ?></td>
      <td class="text-center"><?= $orders['commission_amount']; ?></td>
      <td class="text-center"><?php $amt_due = $orders['order_price'] + $orders['commission_amount']; echo $amt_due; ?></td>
      <td class="text-center"><?= $orders['currency_sign']; ?></td>
      <td class="text-center">
        <form action="<?=base_url('admin/users/view-pending-order-details')?>" method="post">
          <input type="hidden" name="order_id" value="<?=$orders['order_id']?>" />
          <button type="submit" class="btn btn-success btn-sm" title="View & Confirm Payment"><i class="fa fa-check"></i></button>
        </form>
      </td>
      <?php $grand_total+=$amt_due; ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
  <?php if(isset($filter) && $filter == 1):?>
    <tfoot>
      <tr>
        <th class="text-center">&nbsp;</th>
        <th class="text-center">&nbsp;</th>
        <th class="text-center">&nbsp;</th>
        <th class="text-center">&nbsp;</th>
        <th class="text-center">&nbsp;</th>
        <th class="text-center">&nbsp;</th>
        <th class="text-center">&nbsp;</th>
        <th class="text-right">Grand Total</th>
        <th class="text-center"><?=(isset($grand_total))?$grand_total:0?></th>
        <th class="text-center"><?=(isset($orders['currency_sign']))?$orders['currency_sign']:''; ?></th>
        <th class="text-center">&nbsp;</th>
      </tr>
    </tfoot>
  <?php endif ?>
</table>

<script type="text/javascript">
  $(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(2000);
  });

  $("#btnfilter").on("click", function(e){
    var date_range = $("#date_range").val();
    var currency_sign = $("#currency_sign").val();
    var cust_id = $("#cust_id").val();
    var split_date = date_range.split(',');
    var from_date = split_date[0]+' 00:00:00';
    var to_date = split_date[1]+' 00:00:00';
    
    //console.log(from_date);
    //console.log(to_date);
    //console.log(currency_sign);

    if(!date_range && !currency_sign){ swal("error","Please select Date Range or Currency!", 'warning');  } 
    else {
      $.ajax({
        type: "POST",
        url: "<?=base_url('admin/users/pending-orders-filter');?>", 
        data: {
          cust_id:cust_id,
          from_date:from_date,
          to_date:to_date,
          currency_sign:currency_sign,
        },
        success: function(res) {
          console.log(res);
          if(res!="failed") {
            $("#table-2").dataTable().fnDestroy();
            $('#table_body_append').append('<tr><td>'+firstname+'</td><td>'+add_mobile+'</td><td>'+add_email_id+'</td><td>'+add_gender+'</td><td>'+add_age+'</td><td><div class="checkbox checkbox-success"><input style="margin-right: 5px;" type="checkbox" class="checkaddressclass" name="selected_passengers[]"  value="'+res+'"><label></label></div></td></tr>');
            $("#table-2").dataTable({
            });
          }
        }
      });
    }

    
  });
</script>