<script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyCIj8TMFnqnxpvima7MDrJiySvGK-UVLqw&libraries=places'></script>
<script src="<?= base_url('resources/web-panel/scripts/locationpicker.jquery.min.js') ?>"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<ol class="breadcrumb bc-3" >
    <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
    </li>
    <li>
        <a href="<?= base_url('admin/relay-point'); ?>">Relay Point</a>
    </li>
    <li class="active">
        <strong>Add New</strong>
    </li>
</ol>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-dark" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    Add Relay Point Manager Details
                </div>
                <div class="panel-options">
                    <a href="<?= base_url('admin/relay-point'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
                </div>
            </div>
            
            <div class="panel-body">

                <?php if($this->session->flashdata('error')):  ?>
                    <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if($this->session->flashdata('success')):  ?>
                    <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                <?php endif; ?>

                <div class="well well-sm hidden">
                    <h6>Please fill the details to create Relay Point.</h6>
                </div>
                
                <form id="rootwizard-2" method="post" action="register-relay-point" class="form-wizard validate" enctype="multipart/form-data" style="margin-top: 20px">
                    
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    
                    <ul>
                        <li class="active"><a href="#tab2-1" data-toggle="tab"><span>1</span>Relay Point Manager</a></li>
                        <li><a href="#tab2-2" data-toggle="tab"><span>2</span>Relay Point</a></li>
                        <li><a href="#tab2-3" data-toggle="tab"><span>3</span>Warehouse</a></li>
                        <li><a href="#tab2-5" data-toggle="tab"><span>5</span>Finish</a></li>
                    </ul>
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab2-1">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width 200px; height: 150px;" data-trigger="fileinput">
                                            <img src="http://placehold.it/200x150" alt="Profile Picture">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div class="text-center">
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="profile_image" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="full_name">First Name</label>
                                        <input type="text" placeholder="First Name" class="form-control m-b" name="first_name" id="first_name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="last_name">Last Name</label>
                                        <input type="text" placeholder="Last Name" class="form-control m-b" name="last_name" id="last_name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="mobile1">Mobile 1</label>
                                        <input type="text" placeholder="Mobile Number" class="form-control m-b" name="mobile1" id="mobile1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="mobile2">Mobile 2</label>
                                        <input type="text" placeholder="Mobile Number" class="form-control m-b" name="mobile2" id="mobile2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tab2-2">
                            <div class="row">
                                <div class="col-md-4 form-group" style="margin-bottom: 5px;">
                                    <label class="">First Name</label>
                                    <input type="text" " class="form-control" placeholder="First Name" name="rfirstname" id="rfirstname">
                                </div>
                                <div class="col-md-4 form-group" style="margin-bottom: 5px;">
                                    <label class="">Last Name</label>
                                    <input type="text" class="form-control" id="" placeholder="Last Name" name="rlastname" id="rlastname">
                                </div> 
                                <div class="col-md-4 form-group" style="margin-bottom: 5px;">
                                    <label class="">Company Name</label>
                                    <input type="text" class="form-control" id="" placeholder="Company Name" name="rcomapny_name" id="rcomapny_name">
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-3 form-group" style="margin-bottom: 5px;">
                                    <label class="">Mobile Number</label>
                                    <input type="text" class="form-control" id="" placeholder="Mobile Number" name="rmobile_no" id="rmobile_no">
                                </div>
                                <div class="col-md-3 form-group" style="margin-bottom: 5px;">
                                    <label class="">Country</label>
                                    <select id="country_id" name="rcountry_id" class="form-control" data-allow-clear="true" data-placeholder="Select Country">
                                      <option value="">Select Country</option>
                                      <?php foreach ($countries as $country): ?>
                                          <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                                      <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group" style="margin-bottom: 5px;">
                                    <label class="">State</label>
                                    <select id="state_id" name="rstate_id" class="form-control" data-allow-clear="true" data-placeholder="Select State" disabled>
                                        <option value="">Select State...</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group" style="margin-bottom: 5px;">
                                    <label class="">City</label>
                                    <select id="city_id" name="rcity_id" class="form-control" data-allow-clear="true" data-placeholder="Select City" disabled>
                                      <option value="">Select City...</option>
                                    </select>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <label class="">No and Street Name</label>
                                    <input type="text" class="form-control" id="" placeholder="No and Street Name" name="rstreet_name" id="rstreet_name"><br />
                                    <label class="">District or address line1</label>
                                    <textarea placeholder="District or address line1" class="form-control" name="raddr_line1" id="raddr_line1"></textarea><br />
                                    <label class="">Zip</label>
                                    <input type="text" class="form-control" id="" placeholder="Zip" name="rzipcode" id="rzipcode"><br />
                                    <label class="">Business Domain</label>
                                    <select class="form-control" name="rbusiness_type" id="rbusiness_type">
                                      <option value="">Select Business Domain</option>
                                      <option value="store">Store</option>
                                      <option value="gas station">Gas Station</option>
                                      <option value="business">Business</option>
                                      <option value="cybercafé">Cybercafé</option>
                                      <option value="hairdresser">Hairdresser</option>
                                      <option value="individual">Individual</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <label class="">Pick Address From Map</label>
                                    <!--<textarea placeholder="Address Line 2" class="form-control" name="raddr_line2" id="raddr_line2"></textarea>-->
                                    <input type="text" class="form-control" id="us3-address" name="raddr_line2" placeholder="<?= $this->lang->line('pick_from_map'); ?>" />
                                    <div class="row">
                                        <div class="form-group" style="margin-bottom: 5px;">
                                          <div class="col-lg-12">
                                            <div id="us3" style="width: 100%; height: 250px;"></div>
                                            <input type="text" class="form-control hidden" id="us3-radius" />
                                            <input type="text" class="form-control hidden" id="us3-lat" name="latitude" value="0.0" />
                                            <input type="text" class="form-control hidden" id="us3-lon" name="longitude" value="0.0" />
                                          </div>
                                        </div>
                                    </div>
                                    <script>
                                        
                                        function init(){
                                            $('#us3').locationpicker({
                                                location: {
                                                    latitude: 5.773909,
                                                    longitude: 12.936205,
                                                },
                                                radius: 50,
                                                inputBinding: {
                                                    latitudeInput: $('#us3-lat'),
                                                    longitudeInput: $('#us3-lon'),
                                                    radiusInput: $('#us3-radius'),
                                                    locationNameInput: $('#us3-address')
                                                },
                                                enableAutocomplete: true,
                                                onchanged: function (currentLocation, radius, isMarkerDropped) {
                                                    //Uncomment line below to show alert on each Location Changed event
                                                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                                                },
                                            });
                                        };
                                        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                                            init();
                                        });
                                    </script>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <label class="">Pickup Instructions</label>
                                    <textarea placeholder="Pickup Instructions" class="form-control" name="pickup_instructions" id="pickup_instructions"></textarea>
                                </div>
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <label class="">Deliver Instructions</label>
                                    <textarea placeholder="Delivery Instructions" class="form-control" name="deliver_instructions" id="deliver_instructions"></textarea>
                                </div>
                            </div>

                            <div class="row">
                              <div class="col-md-2">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                  <div class="fileinput-new thumbnail" style="width: 140px; height: 100px;" data-trigger="fileinput">
                                    <img src="http://placehold.it/140x100" alt="Profile Picture">
                                  </div>
                                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 140px; max-height: 100px"></div>
                                  <div class="text-center">
                                    <span class="btn btn-white btn-file">
                                      <span class="fileinput-new">Select image</span>
                                      <span class="fileinput-exists">Change</span>
                                      <input type="file" name="rimage_url" accept="image/*">
                                    </span>
                                    <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                  </div>
                                </div>
                              </div>

                              <div class="col-md-5 form-group" style="margin-bottom: 5px;">
                                <label class="">IBAN</label>
                                <input type="text" class="form-control" id="" placeholder="IBAN" name="iban" id="iban">
                              </div>
                              <div class="col-md-5 form-group" style="margin-bottom: 5px;">
                                <label class="">Mobile Money Account 1</label>
                                <input type="text" class="form-control" id="" placeholder="Mobile Money Account" name="mobile_money_acc1" id="mobile_money_acc1">
                              </div>
                              <div class="col-md-5 form-group" style="margin-bottom: 5px;">
                                <label class="">Mobile Money Account 2 (optional)</label>
                                <input type="text" class="form-control" id="" placeholder="Mobile Money Account" name="mobile_money_acc2" id="mobile_money_acc2" >
                              </div>
                              <div class="col-md-5 form-group" style="margin-bottom: 5px;">
                                <label class="">Relay Point Comission</label>
                                <input type="number" class="form-control" id="relay_commission" placeholder="%" name="relay_commission" id="relay_commission">
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12 form-group" style="margin-bottom: 5px;">
                                <label class="">Days and Open Hours</label>
                              </div>  
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                  <div class="col-md-12">
                                    <div class="col-md-3">
                                      <label class="">Monday</label>
                                    </div>
                                    <div class="col-md-9">
                                      <input type="checkbox" id="monday_toggle" name="monday_toggle" data-size="mini" data-width="60" class="myToggle">
                                    </div>
                                  </div>
                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <select class="form-control" name="monday_open" disabled="disabled" id="monday_open">
                                      <option>Opening Hrs</option>
                                      <?php
                                      $tNow = $tStart = strtotime("00:00");
                                      $tEnd = strtotime("23:59");
                                      while($tNow < $tEnd){
                                        echo '<option>'.date("H:i",$tNow).'</option>';
                                        $tNow = strtotime('+30 minutes',$tNow);
                                      }
                                    ?>
                                    </select>
                                  </div>
                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <select class="form-control" name="monday_close" disabled="disabled" id="monday_close">
                                      <option>Closing Hrs</option>
                                      <?php
                                      $tNow = $tStart = strtotime("00:00");
                                      $tEnd = strtotime("23:59");
                                      while($tNow < $tEnd){
                                        echo '<option>'.date("H:i",$tNow).'</option>';
                                        $tNow = strtotime('+30 minutes',$tNow);
                                      }
                                    ?>
                                    </select>
                                  </div>
                                  </div>
                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <div class="col-md-12">
                                      <div class="col-md-3">
                                        <label class="">Tuesday</label>
                                    </div>
                                    <div class="col-md-9">
                                      <input type="checkbox" id="tuesday_toggle" name="tuesday_toggle" data-size="mini" data-width="60" class="myToggle">
                                    </div>
                                  </div>
                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <select class="form-control" name="tuesday_open" disabled="disabled" id="tuesday_open">
                                      <option>Opening Hrs</option>
                                      <?php
                                      $tNow = $tStart = strtotime("00:00");
                                      $tEnd = strtotime("23:59");
                                      while($tNow < $tEnd){
                                        echo '<option>'.date("H:i",$tNow).'</option>';
                                        $tNow = strtotime('+30 minutes',$tNow);
                                      }
                                    ?>
                                    </select>
                                  </div>
                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                    <select class="form-control" name="tuesday_close" disabled="disabled" id="tuesday_close">
                                      <option>Closing Hrs</option>
                                      <?php
                                      $tNow = $tStart = strtotime("00:00");
                                      $tEnd = strtotime("23:59");
                                      while($tNow < $tEnd){
                                        echo '<option>'.date("H:i",$tNow).'</option>';
                                        $tNow = strtotime('+30 minutes',$tNow);
                                      }
                                    ?>
                                    </select>
                                  </div>
                                  </div>
                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                      <div class="col-md-12">
                                        <div class="col-md-3">
                                          <label class="">Wednesday</label>
                                    </div>
                                                    <div class="col-md-9">
                                      <input type="checkbox" id="wednesday_toggle" name="wednesday_toggle" data-size="mini" data-width="60" class="myToggle">
                                    </div>
                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="wednesday_open" disabled="disabled" id="wednesday_open">
                                                        <option>Opening Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="wednesday_close" disabled="disabled" id="wednesday_close">
                                                        <option>Closing Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                    <div class="col-md-12">
                                                      <div class="col-md-3">
                                                        <label class="">Thursday</label>
                                    </div>
                                                    <div class="col-md-9">
                                      <input type="checkbox" id="thursday_toggle" name="thursday_toggle" data-size="mini" data-width="60" class="myToggle">
                                    </div>
                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="thursday_open" disabled="disabled" id="thursday_open">
                                                        <option>Opening Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="thursday_close" disabled="disabled" id="thursday_close">
                                                        <option>Closing Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                    <div class="col-md-12">
                                                      <div class="col-md-3">
                                                        <label class="">Friday</label>
                                    </div>
                                                    <div class="col-md-9">
                                      <input type="checkbox" id="friday_toggle" name="friday_toggle" data-size="mini" data-width="60" class="myToggle">
                                    </div>
                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="friday_open" disabled="disabled" id="friday_open">
                                                        <option>Opening Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="friday_close" disabled="disabled" id="friday_close">
                                                        <option>Closing Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                    <div class="col-md-12">
                                                      <div class="col-md-3">
                                                        <label class="">Saturday</label>
                                    </div>
                                                    <div class="col-md-9">
                                      <input type="checkbox" id="saturday_toggle" name="saturday_toggle" data-size="mini" data-width="60" class="myToggle">
                                    </div>
                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="saturday_open" disabled="disabled" id="saturday_open">
                                                        <option>Opening Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="saturday_close" disabled="disabled" id="saturday_close">
                                                        <option>Closing Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                    <div class="col-md-12">
                                                      <div class="col-md-3">
                                                        <label class="">Sunday</label>
                                                      </div>
                                                      <div class="col-md-9">
                                      <input type="checkbox" id="sunday_toggle" name="sunday_toggle" data-size="mini" data-width="60" class="myToggle">
                                    </div>
                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="sunday_open" disabled="disabled" id="sunday_open">
                                                        <option>Opening Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                                      </select>
                                                  </div>
                                                  <div class="col-md-6 form-group" style="margin-bottom: 5px;">
                                                      <select class="form-control" name="sunday_close" disabled="disabled" id="sunday_close">
                                                        <option>Closing Hrs</option>
                                                        <?php
                                        $tNow = $tStart = strtotime("00:00");
                                        $tEnd = strtotime("23:59");
                                        while($tNow < $tEnd){
                                          echo '<option>'.date("H:i",$tNow).'</option>';
                                          $tNow = strtotime('+30 minutes',$tNow);
                                        }
                                      ?>
                                      </select>
                                  </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tab2-3">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="">Warehouse Name</label>
                                    <input type="text" " class="form-control" id="name" placeholder="Warehouse Name" name="name" id="name">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="">Height (in centimeter)</label>
                                    <input type="text" " class="form-control" id="wheight" placeholder="Height (in centimeter)" name="wheight" id="wheight">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="">Width (in centimeter)</label>
                                    <input type="text" class="form-control" id="wwidth" placeholder="Width (in centimeter)" name="wwidth" id="wwidth">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="">Length (in centimeter)</label>
                                    <input type="text" class="form-control" id="wlength" placeholder="Length (in centimeter)" name="wlength" id="wlength">
                                </div>
                            </div>                          
                        </div>
                        
                        <div class="tab-pane" id="tab2-5">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="control-label">Login Details</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email address" />
                                    <label id="emailMsg" class="emailMsg" name="emailMsg" style="color: red"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Choose Password</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Repeat Password</label>
                                    <input type="password" class="form-control" name="repassword" id="repassword" placeholder="Confirm password" />
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-primary">Create Relay Point</button>
                            </div>
                            <br />
                        </div>
                        <ul class="pager wizard">
                            <li class="previous">
                                <a href="#"><i class="entypo-left-open"></i> Previous</a>
                            </li>
                            <li class="next">
                                <a href="#">Next <i class="entypo-right-open"></i></a>
                            </li>
                        </ul>
                    </div>
                
                </form>
            </div>
        </div>
    </div>
</div>
<br />

<script>
    $("#rootwizard-2")
    .validate({
    rules: {
        first_name: { required : true, },
        last_name: { required : true, },
        mobile1: { required : true, number : true },
        mobile2: { number : true, },
        
        rfirstname: { required : true, },
        rlastname: { required : true, },
        rcomapny_name: { required : true, },
        rmobile_no: { required : true, number : true, },
        rcountry_id: { required : true, },
        rstate_id: { required : true, },
        rcity_id: { required : true, },
        raddr_line2: { required : true, },
        rstreet_name: { required : true, },
        rbusiness_type: { required : true, },
        iban: { required : true, },
        mobile_money_acc1: { required : true, },
        deliver_instructions: { required : true, },
        pickup_instructions: { required : true, },
        relay_commission: { required : true, number : true, min:0, max:50 },

        wheight: { required : true, number : true, },
        wwidth: { required : true, number : true, },
        wlength: { required : true, number : true, },
        name: { required : true, },
        
        email: { required : true, email : true, },
        password: { required : true, minlength : 6, maxlength : 16 },
        repassword: { required : true,  minlength : 6, maxlength : 16, equalTo: "#password", },
    },
    messages: {
        first_name: { required : "Please enter first name!", },
        last_name: { required : "Please enter last name!", },
        mobile1: { required : "Please enter last name!", number : "Numbers only!" },
        mobile2: { number : "Numbers only!" },
        
        rfirstname: { required : "Please enter first name!", },
        rlastname: { required : "Please enter last name!", },
        rcomapny_name: { required : "Please enter company name!", },
        rmobile_no: { required : "Please enter mobile number!", number: "Numbers only!" },
        rcountry_id: { required : "Please select country!", },
        rstate_id: { required : "Please select state!", },
        rcity_id: { required : "Please select city!", },
        raddr_line2: { required : "Please select address on map!", },
        rstreet_name: { required : "Please enter street name!", },
        rbusiness_type: { required : "Please select domain!", },
        iban: { required : "Please enter IBAN!", },
        mobile_money_acc1: { required : "Please enter Mobile Money account!", },
        deliver_instructions: { required : "Please enter delivery instructions!", },
        pickup_instructions: { required : "Please enter pickup instructions!", },
        relay_commission: { required : "Please enter relay point commission!", number: "Numbers only!", min: "Percentage should be zero or more!", max: "Percentage should not more than 50!", },

        wheight: { required : "Please enter height!", number: "Numbers only!" },
        wwidth: { required : "Please enter width!", number: "Numbers only!" },
        wlength: { required : "Please enter length!", number: "Numbers only!" },
        name: { required : "Please enter name!", },
        
        email: { required : "Please enter email address!", email : "Enter valid email address!"},
        password: { required : "Please enter password!", minlength : "Must be 6 character long!", maxlength : "Not more than 16 charaters" },
        repassword: { required : "Please enter password!", minlength : "Must be 6 character long!", maxlength : "Not more than 16 charaters", equalTo : "Password not matcching!" },
      },
  });
</script>
<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
    //console.log(country_id);
    $('#city_id').attr('disabled', true);
    $.ajax({
      url: "get-state-by-country-id",
      type: "POST",
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        //console.log(res);
        $('#state_id').attr('disabled', false);
        $('#state_id').empty();
        $('#state_id').append('<option value="0">Select State</option>');
        $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
        $('#state_id').focus();
      },
      beforeSend: function(){
        $('#state_id').empty();
        $('#state_id').append('<option value="0">Loading...</option>');
      },
      error: function(){
        $('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append('<option value="0">No Options</option>');
      }
    })
  });

  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $.ajax({
      type: "POST",
      url: "get-cities-by-state-id",
      data: { state_id: state_id },
      dataType: "json",
      success: function(res){
        $('#city_id').attr('disabled', false);
        $('#city_id').empty();
        $('#city_id').append('<option value="0">Select city</option>');
        $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
        $('#city_id').focus();
      },
      beforeSend: function(){
        $('#city_id').empty();
        $('#city_id').append('<option value="0">Loading...</option>');
      },
      error: function(){
        $('#city_id').attr('disabled', true);
        $('#city_id').empty();
        $('#city_id').append('<option value="0">No Options</option>');
      }
    })
  });
</script>
<script>
    $("#email").on('change', function(event) { 
        event.preventDefault(); 
        $("#emailMsg").html("");
        var email = $.trim($(this).val()).toLowerCase(); 
        $.ajax({ 
            type: "POST", 
            url: "relay-email-exists", 
            data: {email: email}, 
            dataType:'JSON', 
            success: function(response) { 
                //console.log(response);
                if(response == true) {
                    $("#emailMsg").html("Email address already exists!"); 
                    $("#email").val('').focus(); 
                } else { 
                    $("#emailMsg").html(""); 
                }
            },
        });
    });
    $("#email").focusout(function() {
        $("#emailMsg").html("");
    });
</script>

<script>
    $(function() {
      $('.myToggle').bootstrapToggle({
        on: 'Open',
        off: 'Close'
      });
  });

    $("#monday_toggle").change(function() {
      if(this.checked) {
          $('#monday_open').prop('disabled', false);
          $('#monday_close').prop('disabled', false);
      } else {
        $('#monday_open').prop('disabled', 'disabled');
        $('#monday_close').prop('disabled', 'disabled');
      }
    });
    $("#tuesday_toggle").change(function() {
      if(this.checked) {
          $('#tuesday_open').prop('disabled', false);
          $('#tuesday_close').prop('disabled', false);
      } else {
        $('#tuesday_open').prop('disabled', 'disabled');
        $('#tuesday_close').prop('disabled', 'disabled');
      }
    });
    $("#wednesday_toggle").change(function() {
      if(this.checked) {
          $('#wednesday_open').prop('disabled', false);
          $('#wednesday_close').prop('disabled', false);
      } else {
        $('#wednesday_open').prop('disabled', 'disabled');
        $('#wednesday_close').prop('disabled', 'disabled');
      }
    });
    $("#thursday_toggle").change(function() {
      if(this.checked) {
          $('#thursday_open').prop('disabled', false);
          $('#thursday_close').prop('disabled', false);
      } else {
        $('#thursday_open').prop('disabled', 'disabled');
        $('#thursday_close').prop('disabled', 'disabled');
      }
    });
    $("#friday_toggle").change(function() {
      if(this.checked) {
          $('#friday_open').prop('disabled', false);
          $('#friday_close').prop('disabled', false);
      } else {
        $('#friday_open').prop('disabled', 'disabled');
        $('#friday_close').prop('disabled', 'disabled');
      }
    });
    $("#saturday_toggle").change(function() {
      if(this.checked) {
          $('#saturday_open').prop('disabled', false);
          $('#saturday_close').prop('disabled', false);
      } else {
        $('#saturday_open').prop('disabled', 'disabled');
        $('#saturday_close').prop('disabled', 'disabled');
      }
    });
    $("#sunday_toggle").change(function() {
      if(this.checked) {
          $('#sunday_open').prop('disabled', false);
          $('#sunday_close').prop('disabled', false);
      } else {
        $('#sunday_open').prop('disabled', 'disabled');
        $('#sunday_close').prop('disabled', 'disabled');
      }
    });

    $('#monday_open').on('change', function() {
      var open = timeStringToFloat($('#monday_open').val());
      $("#monday_close option").each(function() {
      $('#monday_close option').prop('disabled', false);
    });
    $("#monday_close option").each(function() {
        var close = timeStringToFloat($(this).val());
        var close_normal = $(this).val();
        if(close <= open) {
          $("#monday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
        }
    });
  });
  $('#tuesday_open').on('change', function() {
      var open = timeStringToFloat($('#tuesday_open').val());
      $("#tuesday_close option").each(function() {
      $('#tuesday_close option').prop('disabled', false);
    });
    $("#tuesday_close option").each(function() {
        var close = timeStringToFloat($(this).val());
        var close_normal = $(this).val();
        if(close <= open) {
          $("#tuesday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
        }
    });
  });
  $('#wednesday_open').on('change', function() {
      var open = timeStringToFloat($('#wednesday_open').val());
      $("#wednesday_close option").each(function() {
      $('#wednesday_close option').prop('disabled', false);
    });
    $("#wednesday_close option").each(function() {
        var close = timeStringToFloat($(this).val());
        var close_normal = $(this).val();
        if(close <= open) {
          $("#wednesday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
        }
    });
  });
  $('#thursday_open').on('change', function() {
      var open = timeStringToFloat($('#thursday_open').val());
      $("#thursday_close option").each(function() {
      $('#thursday_close option').prop('disabled', false);
    });
    $("#thursday_close option").each(function() {
        var close = timeStringToFloat($(this).val());
        var close_normal = $(this).val();
        if(close <= open) {
          $("#thursday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
        }
    });
  });
  $('#friday_open').on('change', function() {
      var open = timeStringToFloat($('#friday_open').val());
      $("#friday_close option").each(function() {
      $('#friday_close option').prop('disabled', false);
    });
    $("#friday_close option").each(function() {
        var close = timeStringToFloat($(this).val());
        var close_normal = $(this).val();
        if(close <= open) {
          $("#friday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
        }
    });
  });
  $('#saturday_open').on('change', function() {
      var open = timeStringToFloat($('#saturday_open').val());
      $("#saturday_close option").each(function() {
      $('#saturday_close option').prop('disabled', false);
    });
    $("#saturday_close option").each(function() {
        var close = timeStringToFloat($(this).val());
        var close_normal = $(this).val();
        if(close <= open) {
          $("#saturday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
        }
    });
  });
  $('#sunday_open').on('change', function() {
      var open = timeStringToFloat($('#sunday_open').val());
      $("#sunday_close option").each(function() {
      $('#sunday_close option').prop('disabled', false);
    });
    $("#sunday_close option").each(function() {
        var close = timeStringToFloat($(this).val());
        var close_normal = $(this).val();
        if(close <= open) {
          $("#sunday_close option:contains('"+close_normal+"')").attr("disabled","disabled");
        }
    });
  });

  function timeStringToFloat(time) {
      var hoursMinutes = time.split(/[.:]/);
      var hours = parseInt(hoursMinutes[0], 10);
      var minutes = hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0;
      return hours + minutes / 60;
  }
    

</script>