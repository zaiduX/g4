<style type="text/css">
.profile-env section.profile-feed .profile-post-form .form-options {padding-bottom: 0px; }
.story-content { line-height: 25px; }
</style>

    <style>

    <!-- Progress with steps -->

    ol.progtrckr {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }

    ol.progtrckr li {
        display: inline-block;
        text-align: center;
        line-height: 3em;
    }

    ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
    ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
    ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
    ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
    ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
    ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
    ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
    ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

    ol.progtrckr li.progtrckr-done {
        color: black;
        border-bottom: 4px solid yellowgreen;
    }
    ol.progtrckr li.progtrckr-todo {
        color: silver; 
        border-bottom: 4px solid silver;
    }

    ol.progtrckr li:after {
        content: "\00a0\00a0";
    }
    ol.progtrckr li:before {
        position: relative;
        bottom: -2.5em;
        float: left;
        left: 50%;
        line-height: 1em;
    }
    ol.progtrckr li.progtrckr-done:before {
        content: "\2713";
        color: white;
        background-color: yellowgreen;
        height: 1.2em;
        width: 1.2em;
        line-height: 1.2em;
        border: none;
        border-radius: 1.2em;
    }
    ol.progtrckr li.progtrckr-todo:before {
        content: "\039F";
        color: silver;
        background-color: white;
        font-size: 1.5em;
        bottom: -1.6em;
    }

</style>

    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li>
        <a href="<?= base_url('admin/get-all-orders'); ?>">Order List</a>
      </li>
      <li class="active">
        <strong>Order Details</strong>
      </li>
    </ol>
  
    <div class="profile-env">
      
      <header class="row">
        
        <div class="col-sm-2">
          <?php if($cust_details['avatar_url'] != "NULL"): ?>
            <a class="profile-picture">
              <img src="<?= $this->config->item('base_url') . $cust_details['avatar_url'];?>" style="height: 150px" class="img-responsive img-thumbnail" />
            </a>
          <?php else: ?>
            <a class="profile-picture">
              <img src="<?= $this->config->item('resource_url') . 'default-profile.jpg';?>" style="height: 150px" class="img-responsive img-thumbnail" />
            </a>
          <?php endif; ?>
          
        </div>
        
        <div class="col-sm-10">
          
          <ul class="profile-info-sections">
            <li style="padding-left: 15px; padding-right: 15px">
              <div class="profile-name">
                <strong>
                  <h6><i class="fa fa-user"></i> Customer</h6>
                  <a><?=$cust_details['firstname']=='NULL'?'User':$cust_details['firstname'].' '.$cust_details['lastname'];?></a>
                </strong>
                <strong>
                  <a><i class="fa fa-phone"></i> <?php if($cust_details['mobile1'] !== "NULL") echo '+'.$order_country['country_phonecode'].'-'.$cust_details['mobile1']; else echo 'Not provided!';  ?></a>
                </strong>
                <span>
                  <?php if( strtolower($cust_details['gender']) === 'm'): ?>
                    <a><i class="fa fa-male"></i> Male</a>
                  <?php else: ?>
                    <a><i class="fa fa-female"></i> Female</a>
                  <?php endif; ?>
                </span>
              </div>
            </li>
            
            <li style="padding-left: 15px; padding-right: 15px">
              <div class="profile-stat">
                <h6><i class="fa fa-paper-plane-o"></i> From </h6>
                <h6><i class="fa fa-user"></i> <?= $order_details['from_address_name']  ?></h6>
                <h6><i class="fa fa-address-card"></i> <?= $order_details['from_address']  ?></h6>
                <h6><i class="fa fa-phone"></i> <?php if($order_details['from_address_contact'] !== "NULL") echo '+'.$order_country['country_phonecode'].'-'.$order_details['from_address_contact']; else echo 'Not provided!';  ?></h6>
              </div>
            </li>
            
            <li style="padding-left: 15px; padding-right: 15px">
              <div class="profile-stat">
                <h6><i class="fa fa-inbox"></i> To </h6>
                <h6><i class="fa fa-user"></i> <?= $order_details['to_address_name']  ?></h6>
                <h6><i class="fa fa-address-card"></i> <?= $order_details['to_address']  ?></h6>
                <h6><i class="fa fa-phone"></i> <?php if($order_details['to_address_contact'] !== "NULL") echo '+'.$order_country['country_phonecode'].'-'.$order_details['to_address_contact']; else echo 'Not provided!';  ?></h6>
              </div>
            </li>
          </ul>
          
        </div>
        
      </header>
      
      <section class="profile-info-tabs">
        
        <div class="row">
          
          <div class="col-sm-offset-2 col-sm-11">
            
            <ul class="user-details">
              <li>
                <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Order Status">
                  <i class="fa fa-play"></i>Order Status - <span> <?= strtoupper($order_details['order_status']); ?></span>
                </a>
              </li>
              <li>
                <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Status Update on">
                    <i class="fa fa-calendar"></i> Status Updated On - 
                    <?php if($order_details['status_update_datetime'] !== "NULL") echo date('l, d M Y', strtotime($order_details['status_update_datetime'])); else echo 'Not provided!'; ?>
                </a>
              </li>
              <li>
                <a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Expiry Date">
                    <i class="fa fa-calendar"></i> Expiry Date - <span>
                    <?php if($order_details['expiry_date'] !== "NULL") echo date('l, d M Y', strtotime($order_details['expiry_date'])); else echo 'Not provided!'; ?></span>
                </a>
              </li>
            </ul>
            
            
            <!-- tabs for the profile links -->
            <ul class="nav nav-tabs">
              <?php if($order_details['order_status'] == 'open' || $order_details['order_status'] == 'accept' || $order_details['order_status'] == 'in_progress' || $order_details['order_status'] == 'delivered') { ?>
                  <li class="active tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Order Details">
                    <a data-toggle="tab" href="#order_details">Order (ID:<?= $order_details['order_id'] ?>)</a>
                  </li>
              <?php } ?>
              <?php if($order_details['order_status'] == 'accept' || $order_details['order_status'] == 'in_progress' || $order_details['order_status'] == 'delivered') { ?>
                  <li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deliverer Details">
                  <a data-toggle="tab" href="#deliverer_details">Deliverer</a>
                  </li>
              <?php } ?>
              <?php if($order_details['order_status'] == 'in_progress' || $order_details['order_status'] == 'delivered') { ?>
                <li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Driver Details">
                  <a data-toggle="tab" href="#driver_details">Driver</a>
                </li>
              <?php } ?>
              <?php if($order_details['order_status'] == 'open' || $order_details['order_status'] == 'accept' || $order_details['order_status'] == 'in_progress' || $order_details['order_status'] == 'delivered') { ?>
                  <li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Order Status Details">
                  <a data-toggle="tab" href="#order_status">Order Satus</a>
                  </li>
              <?php } ?>
              <?php if($order_details['order_status'] == 'open') { ?>
                <li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Requested Deliverers">
                  <a data-toggle="tab" href="#requested_deliverers">Requested Deliverers</a>
                </li>
              <?php } ?>
              <?php if($order_details['order_status'] == 'open') { ?>
                <li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Rejected By">
                  <a data-toggle="tab" href="#rejected_deliverers">Order Rejected By</a>
                </li>
              <?php } ?>
            </ul>
            
          </div>
          
        </div>
        
      </section>
      
      <div class="tab-content">
        <div id="order_details" class="tab-pane fade in active">
          <section class="profile-feed col-md-10 col-md-offset-1">
            <div class="profile-stories">
          
              <article class="story"> 
                
                <div class="story-content text-center"> 
                  <div class="col-md-12 text-center">
                    <?php if($order_details['order_picture_url'] != "NULL"): ?>
                      <a class="profile-picture">
                        <img style="max-width: 350px; max-height: auto;" src="<?= $this->config->item('base_url') . $order_details['order_picture_url'];?>" class="img-responsive" />
                      </a>
                    <?php else: ?>
                      <a class="profile-picture">
                        <img style="max-width: 350px; max-height: auto;" src="<?= $this->config->item('resource_url') . 'no-image.jpg';?>" class="img-responsive" />
                      </a>
                    <?php endif; ?>
                  </div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-12">&nbsp;</div>
                </div>

                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-arrows-h"></i> &nbsp; <a>Distance (KM):</a> </div>
                  <div class="col-md-7"><?= ($order_details['distance_in_km'] !="NULL") ? strtolower($order_details['distance_in_km']):"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-calendar-check-o"></i> &nbsp; <a>Pickup - Deliver Date Time:</a> </div>
                  <div class="col-md-7"><?= ($order_details['pickup_datetime'] !="NULL") ? strtolower($order_details['pickup_datetime']):"Not Provided!"; ?> - <?= ($order_details['delivery_datetime'] !="NULL") ? strtolower($order_details['delivery_datetime']):"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-truck"></i> &nbsp; <a>Transport Mode/Vehicle:</a> </div>
                  <div class="col-md-7"><?= ($vehicle_details['transport_mode'] !="NULL") ? strtoupper($vehicle_details['transport_mode']):"Not Provided!"; ?>/<?= ($vehicle_details['vehicle_type'] !="NULL") ? strtoupper($vehicle_details['vehicle_type']):"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-ship"></i> &nbsp; <a>Order Type:</a> </div>
                  <div class="col-md-7"><?= ($order_details['order_type'] !="NULL") ? strtoupper($order_details['order_type']):"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-archive"></i> &nbsp; <a>Order Contents:</a> </div>
                  <div class="col-md-7"><?= ($order_details['order_contents'] !="NULL") ? strtoupper($order_details['order_contents']):"Not Provided!"; ?></div>
                </div>
                
                <!--<div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-sort-numeric-asc"></i> &nbsp; <a>Order Quantity:</a> </div>
                  <div class="col-md-7"><?= ($order_details['total_quantity'] !="NULL") ? strtoupper($order_details['total_quantity']):"Not Provided!"; ?></div>
                </div>-->
                
                <!--<div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-arrows-alt"></i> &nbsp; <a>Width/Height/Length/Weight:</a> </div>
                  <div class="col-md-7"><?= ($order_details['width'] !="NULL") ? strtoupper($order_details['width']):"Not Provided!"; ?> / <?= ($order_details['height'] !="NULL") ? strtoupper($order_details['height']):"Not Provided!"; ?> / <?= ($order_details['length'] !="NULL") ? strtoupper($order_details['length']):"Not Provided!"; ?> / <?= ($order_details['total_weight'] !="NULL") ? strtoupper($order_details['total_weight']):"Not Provided!"; ?> <?= ($unit_details['shortname'] !="NULL") ? strtoupper($unit_details['shortname']):"Not Provided!"; ?></div>
                </div>-->
                
                <div class="story-content"> 
                  <div class="col-md-12"><i class="fa fa-flask"></i> &nbsp; <a>Package Details:</a> </div>
                  <div class="col-md-12">
                    <table id="tableData" class="table table-striped table-bordered table-hover">
                      <tr>
                        <th>Dimension</th>
                        <th>Quantity</th>
                        <th>Width</th>
                        <th>Height</th>
                        <th>Length</th>
                        <th>Total Weight</th>
                        <th>Contents</th>
                        <th>Goods</th>
                      </tr>
                      <?php foreach ($package_details as $package) { ?>
                        <tr>
                          <td><?=$this->user->get_dimension_type_name($package['dimension_id'])?></td>
                          <td><?=$package['quantity']?></td>
                          <td><?=$package['width']?></td>
                          <td><?=$package['height']?></td>
                          <td><?=$package['length']?></td>
                          <td><?php $units = $this->transport->get_unit($package['unit_id']); echo $package['total_weight'].' '.$units['shortname']?></td>
                          <td><?=$package['contents']?></td>
                          <td>
                            <?php 
                              if($package['dangerous_goods_id'] > 0) {
                                $goods = $this->transport->get_dangerous_goods($package['dangerous_goods_id']); 
                                echo $goods['name']; } else { echo $this->lang->line('not_provided'); }
                            ?>
                          </td>
                        </tr>
                      <?php } ?>
                    </table>
                  </div>
                </div>

                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-flask"></i> &nbsp; <a>Total Volume:</a> </div>
                  <div class="col-md-7"><?= ($order_details['total_volume'] !="NULL") ? strtoupper($order_details['total_volume']):"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-money"></i> &nbsp; <a>Price:</a> </div>
                  <div class="col-md-7"><?= ($order_details['order_price'] !="NULL") ? strtoupper($order_details['order_price']):"Not Provided!"; ?> <?= ($order_details['currency_sign'] !="NULL") ? strtoupper($order_details['currency_sign']):"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-money"></i> &nbsp; <a>Advance/Pickup/Deliver Amount:</a> </div>
                  <div class="col-md-7"><?= ($order_details['advance_payment'] !="NULL") ? strtoupper($order_details['advance_payment']):"Not Provided!"; ?> <?= ($order_details['currency_sign'] !="NULL") ? strtoupper($order_details['currency_sign']):"Not Provided!"; ?> / <?= ($order_details['pickup_payment'] !="NULL") ? strtoupper($order_details['pickup_payment']):"Not Provided!"; ?> <?= ($order_details['currency_sign'] !="NULL") ? strtoupper($order_details['currency_sign']):"Not Provided!"; ?> / <?= ($order_details['deliver_payment'] !="NULL") ? strtoupper($order_details['deliver_payment']):"Not Provided!"; ?> <?= ($order_details['currency_sign'] !="NULL") ? strtoupper($order_details['currency_sign']):"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-info-circle"></i> &nbsp; <a>Insurance Fee:</a> </div>
                  <div class="col-md-7"><?= ($order_details['insurance_fee'] !="NULL") ? strtoupper($order_details['insurance_fee']):"Not Provided!"; ?> <?= ($order_details['currency_sign'] !="NULL") ? strtoupper($order_details['currency_sign']):"Not Provided!"; ?></div>
                </div>
                <?php if($order_details['invoice_url'] !="NULL") { ?>
                  <div class="story-content"> 
                    <div class="col-md-5"><i class="fa fa-ticket"></i> &nbsp; <a>View Payment Invoice:</a> </div>
                    <div class="col-md-7"><a target="_blank" href="<?= $this->config->item('resource_url') . $order_details['invoice_url']; ?>" > Click to view</a></div>
                  </div>
                <?php } ?>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-sticky-note"></i> &nbsp; <a>Deliver Instructions:</a> </div>
                  <div class="col-md-7"><?= ($order_details['deliver_instructions'] !="NULL") ? $order_details['deliver_instructions']:"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-sticky-note"></i> &nbsp; <a>Pickup Instructions:</a> </div>
                  <div class="col-md-7"><?= ($order_details['pickup_instructions'] !="NULL") ? $order_details['pickup_instructions']:"Not Provided!"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-anchor"></i> &nbsp; <a>Handling By:</a> </div>
                  <div class="col-md-7"><?= ($order_details['handling_by'] = 0) ? "Self" :"Driver"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-truck"></i> &nbsp; <a>Dedicatedd Vehicle:</a> </div>
                  <div class="col-md-7"><?= ($order_details['dedicated_vehicle'] = 0) ? "NO" :"YES"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-road"></i> &nbsp; <a>Travel with Driver:</a> </div>
                  <div class="col-md-7"><?= ($order_details['with_driver'] = 0) ? "NO" :"YES"; ?></div>
                </div>
                
                <div class="story-content"> 
                  <div class="col-md-5"><i class="fa fa-ticket"></i> &nbsp; <a>Reference No.:</a> </div>
                  <div class="col-md-7"><?= ($order_details['ref_no'] !="NULL") ? strtoupper($order_details['ref_no']):"Not Provided!"; ?></div>
                </div>
                
              </article>
              
            </div>          
          </section>
        </div>
        
        <div id="deliverer_details" class="tab-pane fade">
          <section class="profile-feed col-md-10 col-md-offset-1">
            <div class="profile-stories">
          
              <article class="story"> 
                <div class="story-content">                 
                  <div class="col-md-12 text-center">
                    <?php if($deliverer_details['avatar_url'] != "NULL"): ?>
                      <a class="profile-picture">
                        <img style="max-width: 150px; max-height: auto;" src="<?= $this->config->item('base_url') . $deliverer_details['avatar_url'];?>" class="img-responsive" />
                      </a>
                    <?php else: ?>
                      <a class="profile-picture">
                        <img style="max-width: 150px; max-height: auto;" src="<?= $this->config->item('resource_url') . 'default-profile.jpg';?>" class="img-responsive" />
                      </a>
                    <?php endif; ?>
                  </div>
                  <hr>
                </div>              
                <div class="story-content">                 
                  <div class="col-md-4"><i class="fa fa-user"></i> Name: </div>
                  <div class="col-md-8"><?= ($deliverer_details['firstname'] != 'NULL') ? $deliverer_details['firstname'].' '.$deliverer_details['lastname'] : 'Not Provided.'; ?></div>
                  <hr>
                </div>
                <div class="story-content">                 
                  <div class="col-md-4"><i class="fa fa-envelope-o"></i> Email Address: </div>
                  <div class="col-md-8">
                    <?= $deliverer_details['email1'].' ( Primary )'; ?>
                    <?php 
                      if($deliverer_details['email_verified'] == 1) { echo '<i class="fa fa-check  text-success tooltip-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email Address Verified!"></i>'; }
                      else { echo '<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email Address Not Verified!" style="color:#FF0000;"></i>'; } 
                    ?>
                    </div>
                  <hr>
                </div>
                <div class="story-content">
                  <div class="col-md-4"><i class="fa fa-phone"></i> Phone Number: </div>
                  <div class="col-md-8">
                    <?= $deliverer_details['mobile1'].' ( Primary )'; ?>
                    <?php 
                      if($deliverer_details['mobile_verified'] == 1) {  echo '<i class="fa fa-check  text-success tooltip-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mobile Number Verified!"></i>'; }
                      else { echo '<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mobile Number Not Verified!" style="color:#FF0000;"></i>'; } 
                    ?>
                    </div>
                  <hr>
                </div>
                <div class="story-content">                 
                  <div class="col-md-4"><i class="fa fa-star-half-o"></i> Deliverer Rating: </div>
                  <div class="col-md-8">
                    <?php for ($i=1; $i < $order_details['rating']; $i++) { ?>
                        <i class="fa fa-star"></i> 
                    <?php } for ($j=$i; $j < 6; $j++) { ?>
                        <i class="fa fa-star-o"></i>
                    <?php } ?>
                  </div>
                  <hr>
                </div>
              </article>
              
            </div>          
          </section>
        </div>

        <div id="driver_details" class="tab-pane fade">
          <section class="profile-feed col-md-10 col-md-offset-1">
            <div class="profile-stories">
          
              <article class="story"> 
                <div class="story-content">                 
                  <div class="col-md-12 text-center">
                    <?php if($driver_details['avatar'] != "NULL"): ?>
                      <a class="profile-picture">
                        <img style="max-width: 150px; max-height: auto;" src="<?= $this->config->item('base_url') . $driver_details['avatar'];?>" class="img-responsive" />
                      </a>
                    <?php else: ?>
                      <a class="profile-picture">
                        <img style="max-width: 150px; max-height: auto;" src="<?= $this->config->item('resource_url') . 'default-profile.jpg';?>" class="img-responsive" />
                      </a>
                    <?php endif; ?>
                  </div>
                  <hr>
                </div>              
                <div class="story-content">                 
                  <div class="col-md-4"><i class="fa fa-user"></i> Name: </div>
                  <div class="col-md-4"><?= $driver_details['first_name'].' '.$driver_details['last_name']; ?></div>
                  <hr>
                </div>
                <div class="story-content">                 
                  <div class="col-md-4"><i class="fa fa-envelope-o"></i> Email Address: </div>
                  <div class="col-md-4">
                    <?= $driver_details['email'].' ( Primary )'; ?>
                  </div>
                  <hr>
                </div>
                <div class="story-content">
                  <div class="col-md-4"><i class="fa fa-phone"></i> Phone Number: </div>
                  <div class="col-md-4">
                    <?= $driver_details['mobile1'].' ( Primary )'; ?>
                    </div>
                  <hr>
                </div>
                
              </article>
              
            </div>          
          </section>
        </div>

        <div id="order_status" class="tab-pane fade">
          <section class="profile-feed col-md-10 col-md-offset-1">
            <div class="profile-stories">
          
              <article class="story">               
                <div class="story-content">                 
                  <div class="col-md-12">
                    <ol class="progtrckr" data-progtrckr-steps="4">
                      <?php if($order_details['order_status'] == 'open') { ?>
                        <li class="progtrckr-done">Active</li>
                        <li class="progtrckr-todo">Accepted</li>
                        <li class="progtrckr-todo">Pickup</li>
                        <li class="progtrckr-todo">Delivered</li>
                      <?php } ?>
                    </ol> 
                    <ol class="progtrckr" data-progtrckr-steps="4">
                      <?php if($order_details['order_status'] == 'accept') { ?>
                        <li class="progtrckr-done">Active</li>
                        <li class="progtrckr-done">Accepted</li>
                        <li class="progtrckr-todo">Pickup</li>
                        <li class="progtrckr-todo">Delivered</li>
                      <?php } ?>
                    </ol> 
                    <ol class="progtrckr" data-progtrckr-steps="4">
                      <?php if($order_details['order_status'] == 'in_progress') { ?>
                        <li class="progtrckr-done">Active</li>
                        <li class="progtrckr-done">Accepted</li>
                        <li class="progtrckr-done">Pickup</li>
                        <li class="progtrckr-todo">Delivered</li>
                      <?php } ?>
                    </ol> 
                    <ol class="progtrckr" data-progtrckr-steps="4">
                      <?php if($order_details['order_status'] == 'delivered') { ?>
                        <li class="progtrckr-done">Active</li>
                        <li class="progtrckr-done">Accepted</li>
                        <li class="progtrckr-done">Pickup</li>
                        <li class="progtrckr-done">Delivered</li>
                      <?php } ?>
                    </ol> 
                    <?php $order_status_list = $this->user->order_status_list($order_details['order_id']); //var_dump($order_status_list) ?>
                    <ol class="progtrckr" data-progtrckr-steps="4">
                      <?php foreach ($order_status_list as $status) { if($status['status'] == 'open') { ?>
                        <li class="" style="font-size: x-small;"><?=date('d M Y H:i:sA', strtotime($status['mod_datetime']))?></li>
                      <?php } } ?>
                      <?php foreach ($order_status_list as $status) { if($status['status'] == 'accept' && $status['user_type'] == 'deliverer') { ?>
                        <li class="" style="font-size: x-small;"><?=date('d M Y H:i:sA', strtotime($status['mod_datetime']))?></li>
                      <?php } } ?>
                      <?php foreach ($order_status_list as $status) { if($status['status'] == 'in_progress') { ?>
                        <li class="" style="font-size: x-small;"><?=date('d M Y H:i:sA', strtotime($status['mod_datetime']))?></li>
                      <?php } } ?>
                      <?php foreach ($order_status_list as $status) { if($status['status'] == 'delivered') { ?>
                        <li class="" style="font-size: x-small;"><?=date('d M Y H:i:sA', strtotime($status['mod_datetime']))?></li>
                      <?php } } ?>

                    </ol>
                  </div>
                </div>
              </article>
              
            </div>          
          </section>
        </div>

        <div id="requested_deliverers" class="tab-pane fade">
          <section class="profile-feed col-md-12">
            <div class="profile-stories">
          
              <article class="story">               
                <div class="story-content">                 
                  <div class="col-md-12">
                    <table class="table table-bordered table-striped datatable" id="table-2">
                      <thead>
                        <tr>
                          <th class="text-center">#</th>
                          <th class="text-center">Deliverer Name</th>
                          <th class="text-center">Contact No.</th>
                          <th class="text-center">Email</th>
                          <th class="text-center">Request Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $offset = $this->uri->segment(3,0) + 1; ?>
                        <?php foreach ($requested_deliverers as $deliverer) {  ?>
                        <tr>
                          <td class="text-center"><?= $offset++; ?></td>
                          <td class="text-center"><?= ($deliverer['firstname'] != 'NULL') ? $deliverer['firstname'].' '.$deliverer['lastname'] : 'Not Provided.'; ?></td>
                          <td class="text-center"><?= $deliverer['mobile1'] ?></td>
                          <td class="text-center"><?= $deliverer['email1'] ?></td>
                          <td class="text-center"><?= $deliverer['cre_datetime'] ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                
              </article>
              
            </div>          
          </section>
        </div>

        <div id="rejected_deliverers" class="tab-pane fade">
          <section class="profile-feed col-md-12">
            <div class="profile-stories">
          
              <article class="story">               
                <div class="story-content">                 
                  <div class="col-md-12">
                    <table class="table table-bordered table-striped datatable" id="table-1">
                      <thead>
                        <tr>
                          <th class="text-center">#</th>
                          <th class="text-center">Deliverer Name</th>
                          <th class="text-center">Contact No.</th>
                          <th class="text-center">Email</th>
                          <th class="text-center">Request Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $offset = $this->uri->segment(3,0) + 1; ?>
                        <?php foreach ($rejected_deliverers as $reject) {  ?>
                        <tr>
                          <td class="text-center"><?= $offset++; ?></td>
                          <td class="text-center"><?= ($reject['firstname'] != 'NULL') ? $reject['firstname'].' '.$reject['lastname'] : 'Not Provided.'; ?></td>
                          <td class="text-center"><?= $reject['mobile1'] ?></td>
                          <td class="text-center"><?= $reject['email1'] ?></td>
                          <td class="text-center"><?= $reject['cre_datetime'] ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                
              </article>
              
            </div>          
          </section>
        </div>

      </div>
    </div>
    
    <br />

    <script type="text/javascript">
      jQuery( document ).ready( function( $ ) {
        var $table2 = jQuery( '#table-2' );
        // Initialize DataTable
        $table2.DataTable( {
          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          "bStateSave": true
        });
        // Initalize Select Dropdown after DataTables is created
        $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
          minimumResultsForSearch: -1
        });
        // Highlighted rows
        $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
          var $this = $(el),
            $p = $this.closest('tr');
          $( el ).on( 'change', function() {
            var is_checked = $this.is(':checked');
            $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
          } );
        } );
        // Replace Checboxes
        $table2.find( ".pagination a" ).click( function( ev ) {
          replaceCheckboxes();
        } );
      } );
    </script>

    <script type="text/javascript">
      jQuery( document ).ready( function( $ ) {
        var $table2 = jQuery( '#table-1' );
        // Initialize DataTable
        $table2.DataTable( {
          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          "bStateSave": true
        });
        // Initalize Select Dropdown after DataTables is created
        $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
          minimumResultsForSearch: -1
        });
        // Highlighted rows
        $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
          var $this = $(el),
            $p = $this.closest('tr');
          $( el ).on( 'change', function() {
            var is_checked = $this.is(':checked');
            $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
          } );
        } );
        // Replace Checboxes
        $table2.find( ".pagination a" ).click( function( ev ) {
          replaceCheckboxes();
        } );
      } );
    </script>