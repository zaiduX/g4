<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/advance-payment'); ?>">Payment Configuration</a>
  </li>
  <li class="active">
    <strong> View / Edit Configuration</strong>
  </li>
</ol>
<style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          View / Edit Payment Configuration
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/advance-payment'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/advance-payment/update'); ?>" class="form-horizontal form-groups-bordered validate" id="adv_pay_edit" method="post" autocomplete="off" novalidate="novalidate">
          
          <input type="hidden" name="pay_id" value="<?= $payment['pay_id'];?>" />
          <input type="hidden" name="country_id" value="<?= $payment['country_id'];?>" />
          <input type="hidden" name="category_id" value="<?= $payment['category_id'];?>" />

          <div class="border">
            <div class="row">
              <div class="col-md-6">
                <label for="country_id" class="control-label">Selected Country </label>                        
                <input type="text" class="form-control" name="category" value="<?= $payment['country_name'];?>" readonly/>
              </div>
              <div class="col-md-6">
                <label for="category" class="control-label">Selected Category </label>              
                <input type="text" class="form-control" name="category" value="<?= $payment['cat_name'];?>" readonly/>
                
              </div>             
            </div>
            <div class="clear"></div><br>
          </div>

          <div class="border">
            <div class="row">
              <div class="col-md-4">
                <label for="loading_free_hours" class="control-label">Free Loading/Unloading Hours</label>                        
                <input type="number" name="loading_free_hours" class="form-control" id="loading_free_hours" value="<?=$payment['loading_free_hours']?>" disabled />
                <span id="error_loading_free_hours" class="error"></span>                           
              </div>
              <div class="col-md-4">
                <label for="loading_hours_charge" class="control-label">Charge Per Hour</label>              
         		<input type="number" name="loading_hours_charge" class="form-control" id="loading_hours_charge" value="<?=$payment['loading_hours_charge']?>" disabled />
                <span class="error" id="error_loading_hours_charge"></span>              
              </div>             
            </div>
            <div class="clear"></div><br />
          </div>

          <div class="border">

            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-2 text-center"><h4 class="text-primary">Earth &amp; Local</h4></div>
              <div class="col-md-5"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="ELAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ELAPP" placeholder="Enter Percentage" name="ELAPP"  min="1" max="100"  value="<?=$payment['ELAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ELGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="ELGCP" placeholder="Enter percentage" name="ELGCP"  min="1" max="100" value="<?=$payment['ELGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="ELDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ELDDP" placeholder="Enter percentage" name="ELDDP"  min="1" max="100" value="<?=$payment['ELDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="ELDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="ELDDC" placeholder="Enter Constant" name="ELDDC"  min="0" value="<?=$payment['ELDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="ELHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ELHFP" placeholder="Enter percentage" name="ELHFP"  min="1" max="100" value="<?=$payment['ELHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ELHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="ELHFC" placeholder="Enter Constant" name="ELHFC"  min="0" value="<?=$payment['ELHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="ELDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ELDVP" placeholder="Enter percentage" name="ELDVP"  min="1" max="100" value="<?=$payment['ELDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ELDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="ELDVC" placeholder="Enter Constant" name="ELDVC"  min="0" value="<?=$payment['ELDVC']?>" disabled />
              </div>
            </div>
            <div class="clear"></div><br>

          </div>
          
          <div class="border">
            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-2 text-center"><h4 class="text-primary">Earth &amp; National</h4></div>
              <div class="col-md-5"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="ENAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ENAPP" placeholder="Enter Percentage" name="ENAPP"  min="1" max="100" value="<?=$payment['ENAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ENGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="ENGCP" placeholder="Enter percentage" name="ENGCP"  min="1" max="100" value="<?=$payment['ENGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="ENDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ENDDP" placeholder="Enter percentage" name="ENDDP"  min="1" max="100" value="<?=$payment['ENDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="ENDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="ENDDC" placeholder="Enter Constant" name="ENDDC"  min="0" value="<?=$payment['ENDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="ENHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ENHFP" placeholder="Enter percentage" name="ENHFP"  min="1" max="100" value="<?=$payment['ENHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ENHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="ENHFC" placeholder="Enter Constant" name="ENHFC"  min="0" value="<?=$payment['ENHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="ENDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ENDVP" placeholder="Enter percentage" name="ENDVP"  min="1" max="100" value="<?=$payment['ENDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ENDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="ENDVC" placeholder="Enter Constant" name="ENDVC"  min="0" value="<?=$payment['ENDVC']?>" disabled />
              </div>
            </div>
            <div class="clear"></div><br>

          </div>
          <div class="border">
            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-3 text-center"><h4 class="text-primary">Earth &amp; International</h4></div>
              <div class="col-md-4"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="EIAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="EIAPP" placeholder="Enter Percentage" name="EIAPP"  min="1" max="100" value="<?=$payment['EIAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="EIGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="EIGCP" placeholder="Enter percentage" name="EIGCP"  min="1" max="100" value="<?=$payment['EIGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="EIDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="EIDDP" placeholder="Enter percentage" name="EIDDP"  min="1" max="100" value="<?=$payment['EIDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="EIDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="EIDDC" placeholder="Enter Constant" name="EIDDC"  min="0" value="<?=$payment['EIDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="EIHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="EIHFP" placeholder="Enter percentage" name="EIHFP"  min="1" max="100" value="<?=$payment['EIHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="EIHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="EIHFC" placeholder="Enter Constant" name="EIHFC"  min="0" value="<?=$payment['EIHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="EIDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="EIDVP" placeholder="Enter percentage" name="EIDVP"  min="1" max="100" value="<?=$payment['EIDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="EIDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="EIDVC" placeholder="Enter Constant" name="EIDVC"  min="0" value="<?=$payment['EIDVC']?>" disabled />
              </div>
            </div> 
            <div class="clear"></div><br>

          </div>

          <!-- Air -->
          <div class="border">
            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-2 text-center"><h4 class="text-primary">Air &amp; Local</h4></div>
              <div class="col-md-5"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="ALAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ALAPP" placeholder="Enter Percentage" name="ALAPP"  min="1" max="100" value="<?=$payment['ALAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ALGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="ALGCP" placeholder="Enter percentage" name="ALGCP"  min="1" max="100" value="<?=$payment['ALGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="ALDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ALDDP" placeholder="Enter percentage" name="ALDDP"  min="1" max="100" value="<?=$payment['ALDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="ALDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="ALDDC" placeholder="Enter Constant" name="ALDDC"  min="0" value="<?=$payment['ALDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="ALHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ALHFP" placeholder="Enter percentage" name="ALHFP"  min="1" max="100" value="<?=$payment['ALHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ALHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="ALHFC" placeholder="Enter Constant" name="ALHFC"  min="0" value="<?=$payment['ALHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="ALDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ALDVP" placeholder="Enter percentage" name="ALDVP"  min="1" max="100" value="<?=$payment['ALDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ALDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="ALDVC" placeholder="Enter Constant" name="ALDVC"  min="0" value="<?=$payment['ALDVC']?>" disabled />
              </div>
            </div>
            <div class="clear"></div><br>

          </div>
          
          <div class="border">
            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-2 text-center"><h4 class="text-primary">Air &amp; National</h4></div>
              <div class="col-md-5"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="ANAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ANAPP" placeholder="Enter Percentage" name="ANAPP"  min="1" max="100" value="<?=$payment['ANAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ANGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="ANGCP" placeholder="Enter percentage" name="ANGCP"  min="1" max="100" value="<?=$payment['ANGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="ANDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ANDDP" placeholder="Enter percentage" name="ANDDP"  min="1" max="100" value="<?=$payment['ANDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="ANDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="ANDDC" placeholder="Enter Constant" name="ANDDC"  min="0" value="<?=$payment['ANDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="ANHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ANHFP" placeholder="Enter percentage" name="ANHFP"  min="1" max="100" value="<?=$payment['ANHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ANHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="ANHFC" placeholder="Enter Constant" name="ANHFC"  min="0" value="<?=$payment['ANHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="ANDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="ANDVP" placeholder="Enter percentage" name="ANDVP"  min="1" max="100" value="<?=$payment['ANDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="ANDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="ANDVC" placeholder="Enter Constant" name="ANDVC"  min="0" value="<?=$payment['ANDVC']?>" disabled />
              </div>
            </div>
            <div class="clear"></div><br>

          </div>

          <div class="border">
            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-3 text-center"><h4 class="text-primary">Air &amp; International</h4></div>
              <div class="col-md-4"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="AIAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="AIAPP" placeholder="Enter Percentage" name="AIAPP"  min="1" max="100" value="<?=$payment['AIAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="AIGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="AIGCP" placeholder="Enter percentage" name="AIGCP"  min="1" max="100" value="<?=$payment['AIGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="AIDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="AIDDP" placeholder="Enter percentage" name="AIDDP"  min="1" max="100" value="<?=$payment['AIDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="AIDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="AIDDC" placeholder="Enter Constant" name="AIDDC"  min="0" value="<?=$payment['AIDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="AIHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="AIHFP" placeholder="Enter percentage" name="AIHFP"  min="1" max="100" value="<?=$payment['AIHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="AIHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="AIHFC" placeholder="Enter Constant" name="AIHFC"  min="0" value="<?=$payment['AIHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="AIDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="AIDVP" placeholder="Enter percentage" name="AIDVP"  min="1" max="100" value="<?=$payment['AIDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="AIDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="AIDVC" placeholder="Enter Constant" name="AIDVC"  min="0" value="<?=$payment['AIDVC']?>" disabled />
              </div>
            </div> 
            <div class="clear"></div><br />
          </div>
          <!-- Sea -->
          <div class="border">
            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-2 text-center"><h4 class="text-primary">Sea &amp; Local</h4></div>
              <div class="col-md-5"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="SLAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SLAPP" placeholder="Enter Percentage" name="SLAPP"  min="1" max="100" value="<?=$payment['SLAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SLGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="SLGCP" placeholder="Enter percentage" name="SLGCP"  min="1" max="100" value="<?=$payment['SLGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="SLDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SLDDP" placeholder="Enter percentage" name="SLDDP"  min="1" max="100" value="<?=$payment['SLDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="SLDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="SLDDC" placeholder="Enter Constant" name="SLDDC"  min="0" value="<?=$payment['SLDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="SLHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SLHFP" placeholder="Enter percentage" name="SLHFP"  min="1" max="100" value="<?=$payment['SLHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SLHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="SLHFC" placeholder="Enter Constant" name="SLHFC"  min="0" value="<?=$payment['SLHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="SLDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SLDVP" placeholder="Enter percentage" name="SLDVP"  min="1" max="100" value="<?=$payment['SLDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SLDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="SLDVC" placeholder="Enter Constant" name="SLDVC"  min="0" value="<?=$payment['SLDVC']?>" disabled />
              </div>
            </div>
            <div class="clear"></div><br>
          </div>
          <div class="border">
            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-2 text-center"><h4 class="text-primary">Sea &amp; National</h4></div>
              <div class="col-md-5"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="SNAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SNAPP" placeholder="Enter Percentage" name="SNAPP"  min="1" max="100" value="<?=$payment['SNAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SNGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="SNGCP" placeholder="Enter percentage" name="SNGCP"  min="1" max="100" value="<?=$payment['SNGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="SNDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SNDDP" placeholder="Enter percentage" name="SNDDP"  min="1" max="100" value="<?=$payment['SNDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="SNDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="SNDDC" placeholder="Enter Constant" name="SNDDC"  min="0" value="<?=$payment['SNDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="SNHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SNHFP" placeholder="Enter percentage" name="SNHFP"  min="1" max="100" value="<?=$payment['SNHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SNHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="SNHFC" placeholder="Enter Constant" name="SNHFC"  min="0" value="<?=$payment['SNHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="SNDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SNDVP" placeholder="Enter percentage" name="SNDVP"  min="1" max="100" value="<?=$payment['SNDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SNDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="SNDVC" placeholder="Enter Constant" name="SNDVC"  min="0" value="<?=$payment['SNDVC']?>" disabled />
              </div>
            </div>
            <div class="clear"></div><br>
          </div>
          <div class="border">
            <div class="row">
              <div class="col-md-5"><hr></div>
              <div class="col-md-3 text-center"><h4 class="text-primary">Sea &amp; International</h4></div>
              <div class="col-md-4"><hr></div>
            </div>

            <div class="row">                      
              <div class="col-md-3">
                <label for="SIAPP" class="control-label">Advance Payment</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SIAPP" placeholder="Enter Percentage" name="SIAPP"  min="1" max="100" value="<?=$payment['SIAPP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SIGCP" class="control-label">Gonagoo Commission</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="SIGCP" placeholder="Enter percentage" name="SIGCP"  min="1" max="100" value="<?=$payment['SIGCP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>            
              <div class="col-md-3">
                <label for="SIDDP" class="control-label">Delivery Delay Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SIDDP" placeholder="Enter percentage" name="SIDDP"  min="1" max="100" value="<?=$payment['SIDDP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>              
              <div class="col-md-3">
                <label for="SIDDC" class="control-label">Delivery Delay Constant</label>
                <input type="number" class="form-control" id="SIDDC" placeholder="Enter Constant" name="SIDDC"  min="0" value="<?=$payment['SIDDC']?>" disabled />
              </div>
            </div>          
            <div class="clear"></div><br>

            <div class="row">            
              <div class="col-md-3">
                <label for="SIHFP" class="control-label">Handling Fee Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SIHFP" placeholder="Enter percentage" name="SIHFP"  min="1" max="100" value="<?=$payment['SIHFP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SIHFC" class=" control-label">Handling Fee Constant</label>
                <input type="number" class="form-control" id="SIHFC" placeholder="Enter Constant" name="SIHFC"  min="0" value="<?=$payment['SIHFC']?>" disabled />
              </div>
              <div class="col-md-3">
                <label for="SIDVP" class="control-label">Dedicated Vehicle Percentage</label>
                <div class="input-group">
                  <input type="number" class="form-control" id="SIDVP" placeholder="Enter percentage" name="SIDVP"  min="1" max="100" value="<?=$payment['SIDVP']?>" disabled />  
                  <span class="input-group-addon currency">%</span>
                </div>
              </div>
              <div class="col-md-3">
                <label for="SIDVC" class="control-label">Dedicated Vehicle Constant</label>
                <input type="number" class="form-control" id="SIDVC" placeholder="Enter Constant" name="SIDVC"  min="0" value="<?=$payment['SIDVC']?>" disabled />
              </div>
            </div> 
            <div class="clear"></div><br>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="text-center">
                <a href="<?= base_url('admin/advance-payment'); ?>" class="btn btn-info">&nbsp;&nbsp; Back &nbsp;&nbsp;</a> &nbsp;&nbsp;
                <button id="btn_edit" class="btn btn-green">&nbsp;&nbsp; Edit &nbsp;&nbsp;</button>
                <button id="btn_submit" type="submit" class="btn btn-blue hidden">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
          
        </form>
        
      </div>
    
    </div>
  
  </div>
</div>

<br />

<script>

  $(function(){

    $("#btn_edit").on('click', function(){
      $(this).addClass('hidden');
      $("#btn_submit").removeClass('hidden');
      $("input,select").prop('disabled', false);
      $('html, body').animate({scrollTop : 0},800);
      return false;
    });

    var somethingChanged = false;
    $('#adv_pay_edit input').change(function() { 
        somethingChanged = true; 
    });

    $("#adv_pay_edit").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();

      if(!somethingChanged){ swal('Error','No change found!','error'); }
      else {
        // Earth local
        var ELAPP = parseFloat($("#ELAPP").val()).toFixed(2);
        var ELDDC = parseFloat($("#ELDDC").val()).toFixed(2);
        var ELDDP = parseFloat($("#ELDDP").val()).toFixed(2);
        var ELHFC = parseFloat($("#ELHFC").val()).toFixed(2);
        var ELHFP = parseFloat($("#ELHFP").val()).toFixed(2);
        var ELDVC = parseFloat($("#ELDVC").val()).toFixed(2);
        var ELDVP = parseFloat($("#ELDVP").val()).toFixed(2);
        var ELGCP = parseFloat($("#ELGCP").val()).toFixed(2);
        // Earth National
        var ENAPP = parseFloat($("#ENAPP").val()).toFixed(2);
        var ENDDC = parseFloat($("#ENDDC").val()).toFixed(2);
        var ENDDP = parseFloat($("#ENDDP").val()).toFixed(2);
        var ENHFC = parseFloat($("#ENHFC").val()).toFixed(2);
        var ENHFP = parseFloat($("#ENHFP").val()).toFixed(2);
        var ENDVC = parseFloat($("#ENDVC").val()).toFixed(2);
        var ENDVP = parseFloat($("#ENDVP").val()).toFixed(2);
        var ENGCP = parseFloat($("#ENGCP").val()).toFixed(2);
        // Earth International
        var EIAPP = parseFloat($("#EIAPP").val()).toFixed(2);
        var EIDDC = parseFloat($("#EIDDC").val()).toFixed(2);
        var EIDDP = parseFloat($("#EIDDP").val()).toFixed(2);
        var EIHFC = parseFloat($("#EIHFC").val()).toFixed(2);
        var EIHFP = parseFloat($("#EIHFP").val()).toFixed(2);
        var EIDVC = parseFloat($("#EIDVC").val()).toFixed(2);
        var EIDVP = parseFloat($("#EIDVP").val()).toFixed(2);
        var EIGCP = parseFloat($("#EIGCP").val()).toFixed(2);
        
        // Air local
        var ALAPP = parseFloat($("#ALAPP").val()).toFixed(2);
        var ALDDC = parseFloat($("#ALDDC").val()).toFixed(2);
        var ALDDP = parseFloat($("#ALDDP").val()).toFixed(2);
        var ALHFC = parseFloat($("#ALHFC").val()).toFixed(2);
        var ALHFP = parseFloat($("#ALHFP").val()).toFixed(2);
        var ALDVC = parseFloat($("#ALDVC").val()).toFixed(2);
        var ALDVP = parseFloat($("#ALDVP").val()).toFixed(2);
        var ALGCP = parseFloat($("#ALGCP").val()).toFixed(2);
        // Air National
        var ANAPP = parseFloat($("#ANAPP").val()).toFixed(2);
        var ANDDC = parseFloat($("#ANDDC").val()).toFixed(2);
        var ANDDP = parseFloat($("#ANDDP").val()).toFixed(2);
        var ANHFC = parseFloat($("#ANHFC").val()).toFixed(2);
        var ANHFP = parseFloat($("#ANHFP").val()).toFixed(2);
        var ANDVC = parseFloat($("#ANDVC").val()).toFixed(2);
        var ANDVP = parseFloat($("#ANDVP").val()).toFixed(2);
        var ANGCP = parseFloat($("#ANGCP").val()).toFixed(2);
        // Air International
        var AIAPP = parseFloat($("#AIAPP").val()).toFixed(2);
        var AIDDC = parseFloat($("#AIDDC").val()).toFixed(2);
        var AIDDP = parseFloat($("#AIDDP").val()).toFixed(2);
        var AIHFC = parseFloat($("#AIHFC").val()).toFixed(2);
        var AIHFP = parseFloat($("#AIHFP").val()).toFixed(2);
        var AIDVC = parseFloat($("#AIDVC").val()).toFixed(2);
        var AIDVP = parseFloat($("#AIDVP").val()).toFixed(2);
        var AIGCP = parseFloat($("#AIGCP").val()).toFixed(2);
        
        // Sea local
        var SLAPP = parseFloat($("#SLAPP").val()).toFixed(2);
        var SLDDC = parseFloat($("#SLDDC").val()).toFixed(2);
        var SLDDP = parseFloat($("#SLDDP").val()).toFixed(2);
        var SLHFC = parseFloat($("#SLHFC").val()).toFixed(2);
        var SLHFP = parseFloat($("#SLHFP").val()).toFixed(2);
        var SLDVC = parseFloat($("#SLDVC").val()).toFixed(2);
        var SLDVP = parseFloat($("#SLDVP").val()).toFixed(2);
        var SLGCP = parseFloat($("#SLGCP").val()).toFixed(2);
        // Sea National
        var SNAPP = parseFloat($("#SNAPP").val()).toFixed(2);
        var SNDDC = parseFloat($("#SNDDC").val()).toFixed(2);
        var SNDDP = parseFloat($("#SNDDP").val()).toFixed(2);
        var SNHFC = parseFloat($("#SNHFC").val()).toFixed(2);
        var SNHFP = parseFloat($("#SNHFP").val()).toFixed(2);
        var SNDVC = parseFloat($("#SNDVC").val()).toFixed(2);
        var SNDVP = parseFloat($("#SNDVP").val()).toFixed(2);
        var SNGCP = parseFloat($("#SNGCP").val()).toFixed(2);
        // Sea International
        var SIAPP = parseFloat($("#SIAPP").val()).toFixed(2);
        var SIDDC = parseFloat($("#SIDDC").val()).toFixed(2);
        var SIDDP = parseFloat($("#SIDDP").val()).toFixed(2);
        var SIHFC = parseFloat($("#SIHFC").val()).toFixed(2);
        var SIHFP = parseFloat($("#SIHFP").val()).toFixed(2);
        var SIDVC = parseFloat($("#SIDVC").val()).toFixed(2);
        var SIDVP = parseFloat($("#SIDVP").val()).toFixed(2);
        var SIGCP = parseFloat($("#SIGCP").val()).toFixed(2);
        
        // Earth Local
        if(isNaN(ELAPP)) {  swal('Error','Earth Local Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ELAPP < 0 && ELAPP > 100) {  swal('Error','Earth Local Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(ELGCP)) {  swal('Error','Earth Local Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ELGCP < 0 && ELGCP > 100) {  swal('Error','Earth Local Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(ELDDP)) {  swal('Error','Earth Local Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ELDDP < 0 && ELDDP > 100) {  swal('Error','Earth Local Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(ELDDC)) {  swal('Error','Earth Local Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(ELHFP)) {  swal('Error','Earth Local Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ELHFP < 0 && ELHFP > 100) {  swal('Error','Earth Local Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(ELHFC)) {  swal('Error','Earth Local Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(ELDVP)) {  swal('Error','Earth Local Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ELDVP < 0 && ELDVP > 100) {  swal('Error','Earth Local Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(ELDVC)) {  swal('Error','Earth Local Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }
        // Earth National
        else if(isNaN(ENAPP)) {  swal('Error','Earth National Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ENAPP < 0 && ENAPP > 100) {  swal('Error','Earth National Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(ENGCP)) {  swal('Error','Earth National Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ENGCP < 0 && ENGCP > 100) {  swal('Error','Earth National Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(ENDDP)) {  swal('Error','Earth National Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ENDDP < 0 && ENDDP > 100) {  swal('Error','Earth National Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(ENDDC)) {  swal('Error','Earth National Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(ENHFP)) {  swal('Error','Earth National Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ENHFP < 0 && ENHFP > 100) {  swal('Error','Earth National Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(ENHFC)) {  swal('Error','Earth National Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(ENDVP)) {  swal('Error','Earth National Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ENDVP < 0 && ENDVP > 100) {  swal('Error','Earth National Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(ENDVC)) {  swal('Error','Earth National Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }
        // Earth International
        else if(isNaN(EIAPP)) {  swal('Error','Earth International Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(EIAPP < 0 && EIAPP > 100) {  swal('Error','Earth International Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(EIGCP)) {  swal('Error','Earth International Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(EIGCP < 0 && EIGCP > 100) {  swal('Error','Earth International Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(EIDDP)) {  swal('Error','Earth International Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(EIDDP < 0 && EIDDP > 100) {  swal('Error','Earth International Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(EIDDC)) {  swal('Error','Earth International Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(EIHFP)) {  swal('Error','Earth International Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(EIHFP < 0 && EIHFP > 100) {  swal('Error','Earth International Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(EIHFC)) {  swal('Error','Earth International Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(EIDVP)) {  swal('Error','Earth International Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(EIDVP < 0 && EIDVP > 100) {  swal('Error','Earth International Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(EIDVC)) {  swal('Error','Earth International Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }        
        // Air Local
        else if(isNaN(ALAPP)) {  swal('Error','Air Local Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ALAPP < 0 && ALAPP > 100) {  swal('Error','Air Local Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(ALGCP)) {  swal('Error','Air Local Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ALGCP < 0 && ALGCP > 100) {  swal('Error','Air Local Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(ALDDP)) {  swal('Error','Air Local Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ALDDP < 0 && ALDDP > 100) {  swal('Error','Air Local Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(ALDDC)) {  swal('Error','Air Local Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(ALHFP)) {  swal('Error','Air Local Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ALHFP < 0 && ALHFP > 100) {  swal('Error','Air Local Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(ALHFC)) {  swal('Error','Air Local Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(ALDVP)) {  swal('Error','Air Local Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ALDVP < 0 && ALDVP > 100) {  swal('Error','Air Local Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(ALDVC)) {  swal('Error','Air Local Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }
        // Air National
        else if(isNaN(ANAPP)) {  swal('Error','Air National Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ANAPP < 0 && ANAPP > 100) {  swal('Error','Air National Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(ANGCP)) {  swal('Error','Air National Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ANGCP < 0 && ANGCP > 100) {  swal('Error','Air National Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(ANDDP)) {  swal('Error','Air National Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ANDDP < 0 && ANDDP > 100) {  swal('Error','Air National Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(ANDDC)) {  swal('Error','Air National Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(ANHFP)) {  swal('Error','Air National Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ANHFP < 0 && ANHFP > 100) {  swal('Error','Air National Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(ANHFC)) {  swal('Error','Air National Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(ANDVP)) {  swal('Error','Air National Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(ANDVP < 0 && ANDVP > 100) {  swal('Error','Air National Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(ANDVC)) {  swal('Error','Air National Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }
        // Air International
        else if(isNaN(AIAPP)) {  swal('Error','Air International Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(AIAPP < 0 && AIAPP > 100) {  swal('Error','Air International Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(AIGCP)) {  swal('Error','Air International Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(AIGCP < 0 && AIGCP > 100) {  swal('Error','Air International Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(AIDDP)) {  swal('Error','Air International Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(AIDDP < 0 && AIDDP > 100) {  swal('Error','Air International Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(AIDDC)) {  swal('Error','Air International Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(AIHFP)) {  swal('Error','Air International Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(AIHFP < 0 && AIHFP > 100) {  swal('Error','Air International Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(AIHFC)) {  swal('Error','Air International Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(AIDVP)) {  swal('Error','Air International Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(AIDVP < 0 && AIDVP > 100) {  swal('Error','Air International Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(AIDVC)) {  swal('Error','Air International Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }        
        // Sea Local
        else if(isNaN(SLAPP)) {  swal('Error','Sea Local Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SLAPP < 0 && SLAPP > 100) {  swal('Error','Sea Local Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(SLGCP)) {  swal('Error','Sea Local Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SLGCP < 0 && SLGCP > 100) {  swal('Error','Sea Local Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(SLDDP)) {  swal('Error','Sea Local Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SLDDP < 0 && SLDDP > 100) {  swal('Error','Sea Local Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(SLDDC)) {  swal('Error','Sea Local Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(SLHFP)) {  swal('Error','Sea Local Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SLHFP < 0 && SLHFP > 100) {  swal('Error','Sea Local Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(SLHFC)) {  swal('Error','Sea Local Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(SLDVP)) {  swal('Error','Sea Local Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SLDVP < 0 && SLDVP > 100) {  swal('Error','Sea Local Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(SLDVC)) {  swal('Error','Sea Local Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }
        // Sea National
        else if(isNaN(SNAPP)) {  swal('Error','Sea National Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SNAPP < 0 && SNAPP > 100) {  swal('Error','Sea National Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(SNGCP)) {  swal('Error','Sea National Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SNGCP < 0 && SNGCP > 100) {  swal('Error','Sea National Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(SNDDP)) {  swal('Error','Sea National Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SNDDP < 0 && SNDDP > 100) {  swal('Error','Sea National Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(SNDDC)) {  swal('Error','Sea National Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(SNHFP)) {  swal('Error','Sea National Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SNHFP < 0 && SNHFP > 100) {  swal('Error','Sea National Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(SNHFC)) {  swal('Error','Sea National Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(SNDVP)) {  swal('Error','Sea National Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SNDVP < 0 && SNDVP > 100) {  swal('Error','Sea National Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(SNDVC)) {  swal('Error','Sea National Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }
        // Sea International
        else if(isNaN(SIAPP)) {  swal('Error','Sea International Advance Payment Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SIAPP < 0 && SIAPP > 100) {  swal('Error','Sea International Advance Payment Percentage is Invalid!','warning');   } 
        else if(isNaN(SIGCP)) {  swal('Error','Sea International Gonagoo Commission Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SIGCP < 0 && SIGCP > 100) {  swal('Error','Sea International Gonagoo Commission Percentage is Invalid!','warning');   } 
        else if(isNaN(SIDDP)) {  swal('Error','Sea International Delivery Delay Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SIDDP < 0 && SIDDP > 100) {  swal('Error','Sea International Delivery Delay Percentage is Invalid!','warning');   } 
        else if(isNaN(SIDDC)) {  swal('Error','Sea International Delivery Delay Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(SIHFP)) {  swal('Error','Sea International Handling Fee Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SIHFP < 0 && SIHFP > 100) {  swal('Error','Sea International Handling Fee Percentage is Invalid!','warning');   } 
        else if(isNaN(SIHFC)) {  swal('Error','Sea International Handling Fee Constant is Invalid! Enter Number Only.','warning');   } 
        else if(isNaN(SIDVP)) {  swal('Error','Sea International Dedicated Vehicle Percentage is Invalid! Enter Number Only.','warning');   } 
        else if(SIDVP < 0 && SIDVP > 100) {  swal('Error','Sea International Dedicated Vehicle Percentage is Invalid!','warning');   } 
        else if(isNaN(SIDVC)) {  swal('Error','Sea International Dedicated Vehicle Constant is Invalid! Enter Number Only.','warning');   }        
        else {  $("#adv_pay_edit")[0].submit();  }
      }
    });
  });
</script>