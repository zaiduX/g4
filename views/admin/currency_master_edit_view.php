		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li>
				<a href="<?= base_url('admin/currency-master'); ?>">Currency Master</a>
			</li>
			<li class="active">
				<strong>Edit</strong>
			</li>
		</ol>
	
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-dark" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							Edit Currency
						</div>
						
						<div class="panel-options">
							<a href="<?= base_url('admin/currency-master'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
						</div>
					</div>
					
					<div class="panel-body">
						
						<?php if($this->session->flashdata('error')):  ?>
				      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
				    	<?php endif; ?>
				    	<?php if($this->session->flashdata('success')):  ?>
				      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
				    	<?php endif; ?>

						<form role="form" class="form-horizontal form-groups-bordered" id="currencyAdd" method="post" action="<?= base_url('admin/update-currency-master'); ?>">
							<input type="hidden" id="currency_id" name="currency_id" value="<?= $currency_dtls['currency_id']; ?>">
							<div class="form-group">
								<label for="cat_type" class="col-sm-3 control-label">Currency Title</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="currency_title" placeholder="Enter currency title" name="currency_title" value="<?= $currency_dtls['currency_title']; ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="currency_sign" class="col-sm-3 control-label">Currency Shortname</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="currency_sign" placeholder="Enter currency sign" name="currency_sign" value="<?= $currency_dtls['currency_sign']; ?>" />	
								</div>
							</div>

							<div class="form-group">
								<label for="currency_sign" class="col-sm-3 control-label">Currency Description</label>
								
								<div class="col-sm-5">
									<textarea class="form-control" id="currency_details" placeholder="Enter currency description" name="currency_details" ><?= $currency_dtls['currency_details']; ?></textarea>
								</div>
							</div>							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit" class="btn btn-blue">Save Changes</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		<br />