		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li>
				<a href="<?= base_url('admin/skills-master'); ?>">Skill</a>
			</li>
			<li class="active">
				<strong>Edit Skill</strong>
			</li>
		</ol>
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel  panel-dark" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							Edit Skill
						</div>
						
						<div class="panel-options">
							<a href="<?= base_url('admin/skills-master'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
						</div>
					</div>
					
					<div class="panel-body">
						
						<?php if($this->session->flashdata('error')):  ?>
			      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
				    	<?php endif; ?>
				    	<?php if($this->session->flashdata('success')):  ?>
				      	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
				    	<?php endif; ?>

						<form role="form" class="form-horizontal form-groups-bordered" id="skill" action="<?= base_url('admin/update-skill'); ?>" method="post">
							<input type="hidden" name="skill_id" value="<?= $skill_details['skill_id']; ?>">
							<div class="form-group">
								<label for="skill_name" class="col-sm-3 control-label">Skill Name</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="skill_name" placeholder="Enter Skill Name" name="skill_name" value="<?= $skill_details['skill_name']; ?>" />
								</div>
							</div>

							<div class="form-group">

								<label class="col-sm-3 control-label">Category </label>
								
								<div class="col-sm-5">
									
									<select id="cat_id" name="cat_id" class="form-control" data-allow-clear="true" data-placeholder="Select Category Type">
										<option value="">Select Category</option>
										<?php foreach ($active_category_types as $type): ?>
											<option value="<?= $type['cat_id'] ?>" <?php if(strtolower($type['cat_name']) == strtolower($skill_details['cat_name'])) { echo 'selected'; } ?> ><?= $type['cat_name']; ?></option>
										<?php endforeach ?>
									</select>
									
								</div>
							</div>						
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit" class="btn btn-blue">Save Details</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		<br />