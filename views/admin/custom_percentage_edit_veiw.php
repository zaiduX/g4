<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/custom-percentage'); ?>">Custom Commission Percentage</a>
  </li>
  <li class="active">
    <strong>Edit Configuration</strong>
  </li>
</ol>
<style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add New Custom Percentage Setup
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/custom-percentage'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/custom-percentage/update'); ?>" class="form-horizontal form-groups-bordered validate" id="custom_percentage_edit" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="custom_id" value="<?=$percent['custom_id'];?>">
          <input type="hidden" name="country_id" value="<?=$percent['country_id'];?>">
          <input type="hidden" name="category_id" value="<?=$percent['category_id'];?>">

          <div class="border">
            <div class="row">
              <div class="col-md-4">
                <label for="country_id" class="control-label">Selected Country </label>                        
                <input type="text" class="form-control" name="category" value="<?= $percent['country_name'];?>" readonly/>                           
              </div>
              <div class="col-md-8">
                <label for="category" class="control-label">Selected Category </label>              
                <input type="text" class="form-control" name="category" value="<?= $percent['cat_name'];?>" readonly/>              
              </div>             
            </div>
            <div class="clear"></div><br />
          </div>
                
          <div class="border">
            <div class="row">

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Custom For Earth</h4></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <label for="EICCP" class="control-label text-center"> New Goods ( % )</label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="EICCPNG" placeholder="Percent" name="EICCPNG"  min="1" max="100" value="<?=$percent['EICCPNG']?>" disabled/>  
                        <span class="input-group-addon currency">%</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label for="EICCP" class="control-label text-center"> Old Goods ( % )</label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="EICCPOG" placeholder="Percent" name="EICCPOG"  min="1" max="100" value="<?=$percent['EICCPOG']?>" disabled/>  
                        <span class="input-group-addon currency">%</span>
                      </div>
                    </div>
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Custom For Air</h4></div>
                </div>

                <div class="row">    
                  <div class="col-md-6">
                    <label for="AICCP" class="control-label"> New Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="AICCPNG" placeholder="Percent" name="AICCPNG"  min="1" max="100" value="<?=$percent['AICCPNG']?>" disabled/>  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="AICCP" class="control-label"> Old Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="AICCPOG" placeholder="Percent" name="AICCPOG"  min="1" max="100" value="<?=$percent['AICCPOG']?>" disabled/>  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>
              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Custom For Sea</h4></div>
                </div>
                
                <div class="row">    
                  <div class="col-md-6">
                    <label for="SICCP" class="control-label"> New Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="SICCPNG" placeholder="Percent" name="SICCPNG"  min="1" max="100" value="<?=$percent['SICCPNG']?>" disabled/>  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="SICCP" class="control-label"> Old Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="SICCPOG" placeholder="Percent" name="SICCPOG"  min="1" max="100" value="<?=$percent['SICCPOG']?>" disabled/>  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>              
            </div>
          </div>

          <div class="border">
            <div class="row">

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Gonagoo Custom % For Earth</h4></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <label for="EICCP" class="control-label text-center"> New Goods ( % )</label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="EIGCPNG" placeholder="Percent" name="EIGCPNG"  min="0" max="100" value="<?=$percent['EIGCPNG']?>" disabled />  
                        <span class="input-group-addon currency">%</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label for="EICCP" class="control-label text-center"> Old Goods ( % )</label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="EIGCPOG" placeholder="Percent" name="EIGCPOG"  min="0" max="100" value="<?=$percent['EIGCPOG']?>" disabled />  
                        <span class="input-group-addon currency">%</span>
                      </div>
                    </div>
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Gonagoo Custom % For Air</h4></div>
                </div>

                <div class="row">    
                  <div class="col-md-6">
                    <label for="AICCP" class="control-label"> New Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="AIGCPNG" placeholder="Percent" name="AIGCPNG"  min="0" max="100" value="<?=$percent['AIGCPNG']?>" disabled />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="AICCP" class="control-label"> Old Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="AIGCPOG" placeholder="Percent" name="AIGCPOG"  min="0" max="100" value="<?=$percent['AIGCPOG']?>" disabled />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="text-center"><h4 class="text-primary">Gonagoo Custom % For Sea</h4></div>
                </div>
                
                <div class="row">    
                  <div class="col-md-6">
                    <label for="SICCP" class="control-label"> New Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="SIGCPNG" placeholder="Percent" name="SIGCPNG"  min="0" max="100" value="<?=$percent['SIGCPNG']?>" disabled />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="SICCP" class="control-label"> Old Goods ( % )</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="SIGCPOG" placeholder="Percent" name="SIGCPOG"  min="0" max="100" value="<?=$percent['SIGCPOG']?>" disabled />  
                      <span class="input-group-addon currency">%</span>
                    </div>
                  </div>
                </div>
                <div class="clear"></div><br />
              </div> 
            </div>
          </div>

          
          <div class="row">
            <div class="col-md-12">
              <div class="text-center">
                <a href="<?= base_url('admin/custom-percentage'); ?>" class="btn btn-info">&nbsp;&nbsp; Back &nbsp;&nbsp;</a> &nbsp;&nbsp;
                <button id="btn_edit" class="btn btn-green">&nbsp;&nbsp; Edit &nbsp;&nbsp;</button>
                <button id="btn_submit" type="submit" class="btn btn-blue hidden">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
          
        </form>
        
      </div>
    
    </div>
  
  </div>
</div>

<br />

<script>

  $(function(){

    $("#btn_edit").on('click', function(){
      $(this).addClass('hidden');
      $("#btn_submit").removeClass('hidden');
      $("input,select").prop('disabled', false);
      $('html, body').animate({scrollTop : 0},800);
      return false;
    });

    var somethingChanged = false;
    $('#custom_percentage_edit input').change(function() { 
        somethingChanged = true; 
    });

    $("#custom_percentage_edit").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();

     // Earth
    var EICCPNG = parseFloat($("#EICCPNG").val()).toFixed(2);
    var EICCPOG = parseFloat($("#EICCPOG").val()).toFixed(2);
    var EIGCPNG = parseFloat($("#EIGCPNG").val()).toFixed(2);
    var EIGCPOG = parseFloat($("#EIGCPOG").val()).toFixed(2);
    // Air
    var AICCPNG = parseFloat($("#AICCPNG").val()).toFixed(2);
    var AICCPOG = parseFloat($("#AICCPOG").val()).toFixed(2);
    var AIGCPNG = parseFloat($("#AIGCPNG").val()).toFixed(2);
    var AIGCPOG = parseFloat($("#AIGCPOG").val()).toFixed(2);
    // Sea
    var SICCPNG = parseFloat($("#SICCPNG").val()).toFixed(2);
    var SICCPOG = parseFloat($("#SICCPOG").val()).toFixed(2);
    var SIGCPNG = parseFloat($("#SIGCPNG").val()).toFixed(2);
    var SIGCPOG = parseFloat($("#SIGCPOG").val()).toFixed(2);

	if(!somethingChanged){ swal('Error','No change found!','error'); }
	else {
	// Earth      
	if(isNaN(EICCPNG)) {  swal('Error','New Goods Custom Commission Percentage For Earth is Invalid! Enter Number Only.','warning');   } 
	else if(EICCPNG < 0 && EICCPNG > 100) {  swal('Error','New Goods Custom Commission Percentage For Earth is Invalid!','warning');   } 
	else if(isNaN(EICCPOG)) {  swal('Error','Old Goods Custom Commission Percentage For Earth is Invalid! Enter Number Only.','warning');   } 
	else if(EICCPOG < 0 && EICCPOG > 100) {  swal('Error','Old Goods Custom Commission Percentage For Earth is Invalid!','warning');   }
	// Air      
	else if(isNaN(AICCPNG)) {  swal('Error','New Goods Custom Commission Percentage For Air is Invalid! Enter Number Only.','warning');   } 
	else if(AICCPNG < 0 && AICCPNG > 100) {  swal('Error','New Goods Custom Commission Percentage For Air is Invalid!','warning');   }
	else if(isNaN(AICCPOG)) {  swal('Error','Old Goods Custom Commission Percentage For Air is Invalid! Enter Number Only.','warning');   } 
	else if(AICCPOG < 0 && AICCPOG > 100) {  swal('Error','Old Goods Custom Commission Percentage For Air is Invalid!','warning');   }
	// Sea      
	else if(isNaN(SICCPNG)) {  swal('Error','New Goods Custom Commission Percentage For Sea is Invalid! Enter Number Only.','warning');   } 
	else if(SICCPNG < 0 && SICCPNG > 100) {  swal('Error','New Goods Custom Commission Percentage For Sea is Invalid!','warning');   }
	else if(isNaN(SICCPOG)) {  swal('Error','Old Goods Custom Commission Percentage For Sea is Invalid! Enter Number Only.','warning');   } 
	else if(SICCPOG < 0 && SICCPOG > 100) {  swal('Error','Old Goods Custom Commission Percentage For Sea is Invalid!','warning');   }
    // Earth Gonagoo Comission
    else if(isNaN(EIGCPNG)) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Earth is Invalid! Enter Number Only.','warning');   }
    else if(EIGCPNG < 0 && EIGCPNG > 100) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Earth is Invalid!','warning');   } 
    else if(isNaN(EIGCPOG)) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Earth is Invalid! Enter Number Only.','warning');   }
    else if(EIGCPOG < 0 && EIGCPOG > 100) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Earth is Invalid!','warning');   }
    // Air Gonagoo Comission
    else if(isNaN(AIGCPNG)) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Air is Invalid! Enter Number Only.','warning');   } 
    else if(AIGCPNG < 0 && AIGCPNG > 100) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Air is Invalid!','warning');   }
    else if(isNaN(AIGCPOG)) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Air is Invalid! Enter Number Only.','warning');   } 
    else if(AIGCPOG < 0 && AIGCPOG > 100) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Air is Invalid!','warning');   }
    // Sea Gonagoo Comission
    else if(isNaN(SIGCPNG)) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Sea is Invalid! Enter Number Only.','warning');   } 
    else if(SIGCPNG < 0 && SIGCPNG > 100) {  swal('Error','New Goods Custom Gonagoo Commission Percentage For Sea is Invalid!','warning');   }
    else if(isNaN(SIGCPOG)) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Sea is Invalid! Enter Number Only.','warning');   } 
    else if(SIGCPOG < 0 && SIGCPOG > 100) {  swal('Error','Old Goods Custom Gonagoo Commission Percentage For Sea is Invalid!','warning');   }
	else {  $("#custom_percentage_edit")[0].submit();  }
      }
    });
  });
</script>