<ol class="breadcrumb bc-3" style="margin-bottom: 0px;" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/promo-code'); ?>">Promocode Master</a>
	</li>
	<li class="active">
		<strong>Edit Promocode</strong>
	</li>
</ol>
<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Edit Promocode
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/promo-code'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/update-promocode'); ?>" class="form-horizontal form-groups-bordered" id="add_promocode_form" method="post" autocomplete="off" enctype="multipart/form-data">
					<input type="hidden" name="promo_code_id" value="<?=$promo_code['promo_code_id']?>">
					<div class="form-group">
						<div class="col-sm-12">
							<div class="col-sm-8" style="padding-left:0px; padding-right:0px">
								<div class="col-sm-4 text-center" style="padding-left:0px; padding-right:0px">
									<label class="control-label">Upload Promocode Picture</label>
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
											<img src="<?php if($promo_code['promocode_avtar_url'] != 'NULL') { echo base_url($promo_code['promocode_avtar_url']); } else { echo $this->config->item('resource_url'). 'noimage.png'; }?>" alt="Profile Picture">
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select Image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="promo_code_image" accept="image/*">
											</span>
											<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
										</div>
									</div>
								</div>
								<div class="col-sm-8" style="padding-left:0px; padding-right:0px">
									<div class="col-sm-12">
						                <label for="Promocode" class="control-label">Promocode</label>
						                <div class="input-group">
						                  <input type="text" class="form-control" id="promo_code" value="<?=$promo_code['promo_code']?>" placeholder="Enter Promocode..." style="text-transform:uppercase" disabled/>
						                  <input type="hidden" name="promo_code" value="<?=$promo_code['promo_code']?>">
						                  <span class="input-group-addon">#</span>
						                </div>                        
						            </div>                    
									<div class="col-sm-12">
										<label for="promocode_title" class="control-label">Promocode Title</label>
										<input type="text" class="form-control" value="<?=$promo_code['promo_title']?>" id="promocode_title" placeholder="Enter Promocode Title..." name="promocode_title"/>
									</div>
									<div class="col-sm-6">
						                <label for="example-date-input" style="padding-top:7px; margin:0px"><i class="fa fa-calendar"></i>Start Date</label>
						                <div class="input-group">
						                  <input type="text" value="<?=$promo_code['code_start_date']?>" class="form-control" placeholder="Start Date" id="start_date1" name="start_date" autocomplete="off" >
						                  <div class="input-group-addon dpAddon">
						                    <span class="glyphicon glyphicon-th"></span>
						                  </div>
						                </div>
									</div>
									<div class="col-sm-6">
						                <label for="example-date-input" style="padding-top:7px; margin:0px"><i class="fa fa-calendar"></i>Expire Date</label>
						                <div class="input-group">
						                  <input type="text" value="<?=$promo_code['code_expire_date']?>" class="form-control" placeholder="End Date" id="expire_date1" name="expire_date" autocomplete="off" >
						                  <div class="input-group-addon ddAddon">
						                    <span class="glyphicon glyphicon-th"></span>
						                  </div>
						                </div>
									</div>
								</div>
								<div class="col-sm-12">
									<label for="Description" class="control-label"> Description </label>
									<textarea class="form-control"  id="description" placeholder="Enter Description..." name="description"><?=$promo_code['promo_desc']?></textarea>
								</div>
							</div>
							<div class="col-sm-4" style="padding-left:0px; padding-right:0px">
								<div class="col-sm-12">
					                <label for="discount_percent" class="control-label">Discount Percent</label>
					                <div class="input-group">
					                  <input type="number" value="<?=$promo_code['discount_percent']?>" class="form-control" id="discount_percent" placeholder="Enter Discount Percent..." disabled/>
					                  <input type="hidden" name="discount_percent" value="<?=$promo_code['discount_percent']?>">
					                  <span class="input-group-addon">%</span>
					                </div>    
					             </div>
					            <div class="col-sm-12">
					                <label for="limit" class="control-label">Maximum number of orders (Optional)</label>
					                <div class="input-group">
					                  <input type="number" class="form-control" value="<?=$promo_code['no_of_limit']?>" id="limit" placeholder="Enter Limit" name="limit"/>
					                  <span class="input-group-addon">#</span>
					                </div>    
					            </div>
				                <div class="col-sm-12">
									<label for="category" class="control-label">Select Category </label> 
								  <select id="service_id" name="service_id[]" class="js-source-states select2" placeholder="Select Services..." multiple="multiple">
									<?php $sm = explode(',' , $promo_code['service_id']);
										foreach ($sm as $c ){
											$cat_name = $this->admin->get_category_name_by_order_id(0,$c)
											?>
								      <option value="<?= $c?>" selected> <?= $cat_name?> </option>
									<?php } ?>					    
			
								    <?php foreach ($categories as $c ){ ?> 	    			
								        <option value="<?= $c['cat_id']?>"> <?= $c['cat_name']?> </option>
								    <?php } ?> 

								  </select>
								</div>
							</div>			
							<div class="col-sm-12 col-sm-offset-10">							
								<br/><button type="submit" class="btn btn-blue">Update Promocode</button>							
							</div>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />


<script>
//   $(function() {
//     $('#datepicker').datepicker( {
//     	startDate: "+0d", autoclose: true, 
//     } );
//   });

$('#add_promocode_form').validate({
    rules: {
        discount_percent: {
          required: true,
          minlength: 1,
          maxlength: 3,
          max:100,
        },
    		promo_code:{
    			required: true,
    		},
    		promocode_title:{
    			required: true
    		},
    		service_id:{
    			required: true
    		},
    		start_date1:{
    			required: true
    		},
    		expire_date1:{
    			required: true
    		},
    },
    messages:{
    	 promo_code: "Enter Promocode",
    	 discount_percent: "Enter Percentage Of Discount 0 To 100 %",
    	 promocode_title: "Enter Title Of Promocode",
    	 service_id: "Select At least One Service",
    	 start_date1: "Select Start Date",
    	 expire_date1: "Select Expire Date",
    }
});

</script>

<script>
    $(document).ready(function() {
      $('#start_date1').datepicker({
        startDate: new Date(),
        autoclose: true,
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('#expire_date1').datepicker({
            startDate: minDate,
            autoclose: true,
          });     
        });
    });

    $(".dpAddon" ).click(function( e ) { 
      $("#start_date1").focus();
    });
    $(".ddAddon" ).click(function( e ) { 
      $("#expire_date1").focus();
    });
</script>