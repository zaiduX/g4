<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Standard Dimensions</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">Standard Dimensions</h2>
<?php
$per_standard_dimension = explode(',', $permissions[0]['dimension']);

if(in_array('2', $per_standard_dimension)) { ?>
	<a type="button" href="<?= base_url('admin/standard-dimensions/add'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add New
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Service Name</th>
			<th class="text-center">Dimension Type</th>
			<th class="text-center">Width (CM)</th>
			<th class="text-center">Height (CM)</th>
			<th class="text-center">Length (CM)</th>
			<th class="text-center">Volume (CM<sup>3</sup> )</th>
			<th class="text-center">On Earth</th>
			<th class="text-center">On Air</th>
			<th class="text-center">On Sea</th>
			<?php if(in_array('3', $per_standard_dimension) || in_array('5', $per_standard_dimension)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($standard_dimension as $dim):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center">
<?php $cat = $this->standard_dimension->get_category_name_by_id($dim['category_id']);  echo $cat['cat_name']; ?>
</td>
			<td class="text-center"><?= $dim['dimension_type'] ?></td>
			<td class="text-center"><?= $dim['width'] ?></td>
			<td class="text-center"><?= $dim['height'] ?></td>
			<td class="text-center"><?= $dim['length'] ?></td>
			<td class="text-center"><?= $dim['volume'] ?></td>
			<td class="text-center"><?= ($dim['on_earth'])?'Yes':'No'; ?></td>
			<td class="text-center"><?= ($dim['on_air'])?'Yes':'No'; ?></td>
			<td class="text-center"><?= ($dim['on_sea'])?'Yes':'No'; ?></td>
			<?php if(in_array('3', $per_standard_dimension) || in_array('5', $per_standard_dimension)): ?>
				<td class="text-center" style="display:flex">
					<?php if(in_array('3', $per_standard_dimension)): ?>
						<a href="<?= base_url('admin/standard-dimensions/edit/') . $dim['dimension_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a> &nbsp;
					<?php endif; ?>
					<?php if(in_array('5', $per_standard_dimension)): ?>						
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_dimension('<?= $dim["dimension_id"]; ?>');">
							<i class="entypo-cancel"></i>
							Delete
						</button>
					<?php endif; ?>
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	} );


	function delete_dimension(id=0){		
		swal({
		  title: 'Are you sure?',
		  text: "You want to to delete this dimension?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('standard-dimensions/delete', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Deleted!',
				    'Standard Dimension has been deleted.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Standard Dimension deletion failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

</script>