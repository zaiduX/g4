<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?=base_url('admin/users/define-user-commission/').$cust_id; ?>">User Gonagoo Commission Configuration</a>
  </li>
  <li class="active">
    <strong>Add Gonagoo Commission Absolute Rate</strong>
  </li>
</ol>
<style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add New Gonagoo Commission Absolute Rate
        </div>
        
        <div class="panel-options">
          <a href="<?=base_url('admin/users/define-user-commission/').$cust_id; ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/users/register-commission-rate'); ?>" class="form-horizontal form-groups-bordered validate" id="adv_pay_add" method="post" autocomplete="off" novalidate="novalidate">
          <input type="hidden" name="cust_id" value="<?=$cust_id?>">
          <div class="border">
            <div class="row">
              <div class="col-md-4">
                <label for="country_id" class="control-label">Select Country </label>                        
                <select id="country_id" name="country_id" class="form-control select2">
                  <option value="">Select Country</option>
                  <?php foreach ($countries as $country): ?>                      
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
                <span id="error_country_id" class="error"></span>                           
              </div>
              <div class="col-md-8">
                <label for="category" class="control-label">Select Category </label>              
                <select id="category" name="category[]" class="form-control select2" multiple placeholder="Select Category" autocomplete="none">
                  <?php foreach ($categories as $c ): ?>
                    <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
                      <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
                <span class="error" id="error_category"></span>              
              </div>             
            </div>
            <div class="clear"></div><br />

            <div class="row">
              <div class="col-md-4">
                <label for="ELGCP" class="control-label">Gonagoo Commission Rate (Earth & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="ELGCP" placeholder="Enter Rate Per Order" name="ELGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
               <div class="col-md-4">
                <label for="ENGCP" class="control-label">Gonagoo Commission Rate (Earth & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="ENGCP" placeholder="Enter Rate Per Order" name="ENGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
               <div class="col-md-4">
                <label for="EIGCP" class="control-label">Gonagoo Commission Rate (Earth & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="EIGCP" placeholder="Enter Rate Per Order" name="EIGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
            </div>
            <div class="clear"></div><br />

            <div class="row">
              <div class="col-md-4">
                <label for="ALGCP" class="control-label">Gonagoo Commission Rate (Air & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="ALGCP" placeholder="Enter Rate Per Order" name="ALGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
               <div class="col-md-4">
                <label for="ANGCP" class="control-label">Gonagoo Commission Rate (Air & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="ANGCP" placeholder="Enter Rate Per Order" name="ANGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
               <div class="col-md-4">
                <label for="AIGCP" class="control-label">Gonagoo Commission Rate (Air & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="AIGCP" placeholder="Enter Rate Per Order" name="AIGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
            </div>
            <div class="clear"></div><br />

            <div class="row">
              <div class="col-md-4">
                <label for="SLGCP" class="control-label">Gonagoo Commission Rate (Sea & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="SLGCP" placeholder="Enter Rate Per Order" name="SLGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
               <div class="col-md-4">
                <label for="SNGCP" class="control-label">Gonagoo Commission Rate (Sea & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="SNGCP" placeholder="Enter Rate Per Order" name="SNGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
               <div class="col-md-4">
                <label for="SIGCP" class="control-label">Gonagoo Commission Rate (Sea & Local)</label>                
                <div class="input-group">
                  <input type="number" class="form-control" id="SIGCP" placeholder="Enter Rate Per Order" name="SIGCP"  min="1" max="100" />  
                  <span class="input-group-addon currency">#</span>
                </div>
              </div>
            </div>
            <div class="clear"></div><br />
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="text-right">
                <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(function(){
    $("#adv_pay_add").submit(function(e) { e.preventDefault(); });
    $("#btn_submit").on('click', function(e) {  e.preventDefault();
      var country_id = $("#country_id").val();
      var category = $("#category").val();
      var ELGCP = parseInt($("#ELGCP").val());
      var ENGCP = parseInt($("#ENGCP").val());
      var EIGCP = parseInt($("#EIGCP").val());
      var ALGCP = parseInt($("#ALGCP").val());
      var ANGCP = parseInt($("#ANGCP").val());
      var AIGCP = parseInt($("#AIGCP").val());
      var SLGCP = parseInt($("#SLGCP").val());
      var SNGCP = parseInt($("#SNGCP").val());
      var SIGCP = parseInt($("#SIGCP").val());

      if(country_id <= 0 ) { swal('Error','Please Select Country','warning'); } 
      else if(!category) { swal('Error','Please Select atleast one Category','warning'); } 
      else if(isNaN(ELGCP)) { swal('Error','Earth Local Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else if(isNaN(ENGCP)) { swal('Error','Earth National Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else if(isNaN(EIGCP)) { swal('Error','Earth International Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else if(isNaN(ALGCP)) { swal('Error','Air Local Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else if(isNaN(ANGCP)) { swal('Error','Air National Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else if(isNaN(AIGCP)) { swal('Error','Air International Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else if(isNaN(SLGCP)) { swal('Error','Sea Local Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else if(isNaN(SNGCP)) { swal('Error','Sea National Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else if(isNaN(SIGCP)) { swal('Error','Sea International Gonagoo Commission Rate is Invalid! Enter Number Only.','warning'); } 
      else {  $("#adv_pay_add")[0].submit();  }
    });
  });
</script>