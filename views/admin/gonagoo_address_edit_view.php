    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li>
        <a href="<?= base_url('admin/gonagoo-address-admin'); ?>">Gonagoo Address</a>
      </li>
      <li class="active">
        <strong>Edit</strong>
      </li>
    </ol>
    <style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
    <div class="row">
      <div class="col-md-12">
        
        <div class="panel panel-dark" data-collapsed="0">
        
          <div class="panel-heading">
            <div class="panel-title">
              Edit Gonagoo Address
            </div>
            
            <div class="panel-options">
              <a href="<?= base_url('admin/gonagoo-address-admin'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
            </div>
          </div>
          
          <div class="panel-body">
            
            <?php if($this->session->flashdata('error')):  ?>
              <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

            <form role="form" class="form-horizontal form-groups-bordered" id="from_addAddress" action="<?= base_url('admin/update-address'); ?>" method="post">
              <input type="hidden" name="addr_id" value="<?=$address_details['addr_id']?>">
              <input type="hidden" name="country_id" value="<?=$address_details['country_id']?>">

              <div class="border">

                <div class="row">
                  <div class="col-md-4">
                    <label for="company_name" class="control-label">Company Name</label>
                    <input type="text" class="form-control" id="company_name" placeholder="Enter Company Name" name="company_name" value="<?=$address_details['company_name']?>" />
                  </div>   
                  <div class="col-md-4">
                    <label for="phone" class="control-label">Company Contact</label>
                    <input type="text" class="form-control" id="phone" placeholder="Enter Company Contact" name="phone" value="<?=$address_details['phone']?>" />
                  </div>   
                  <div class="col-md-4">
                    <label for="email" class="control-label">Company Email</label>
                    <input type="text" class="form-control" id="email" placeholder="Enter Company Email" name="email" value="<?=$address_details['email']?>" /> <br />
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <label for="country_id" class="control-label">Select Country </label> 
                    <input type="text" name="country_name" value="<?= $this->address->get_country_name_by_id($address_details['country_id']); ?>" class="form-control" readonly="readonly" />
                  </div>
                  <div class="col-md-4">
                    <label class="control-label">Select State</label>
                    <select id="state_id" name="state_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select State">
                      <option value="0">Select State</option>
                      <?php foreach ($states as $state): ?> 
                        <option value="<?= $state['state_id'] ?>" <?=($state['state_id']==$address_details['state_id'])?'selected':''?> ><?= $state['state_name']; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label class="control-label">Select City</label>
                    <select id="city_id" name="city_id" class="form-control select2" data-allow-clear="true" data-placeholder="Select City">
                      <option value="0">Select City</option>
                      <?php foreach ($cities as $city): ?> 
                        <option value="<?= $city['city_id'] ?>" <?=($city['city_id']==$address_details['city_id'])?'selected':''?> ><?= $city['city_name']; ?></option>
                      <?php endforeach ?>
                    </select><br />
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <label for="no_street_name" class="control-label">No. Street Name</label>
                    <input type="text" class="form-control" id="no_street_name" placeholder="Enter No. Street Name" name="no_street_name" value="<?=$address_details['no_street_name']?>" />
                  </div>   
                  <div class="col-md-4">
                    <label for="terms" class="control-label">Terms and Conditions</label>
                    <input type="text" class="form-control" id="terms" placeholder="Enter Terms and Conditions" name="terms" value="<?=$address_details['terms']?>" />
                  </div>   
                  <div class="col-md-4">
                    <label for="payment_details" class="control-label">Payment Details</label>
                    <input type="text" class="form-control" id="payment_details" placeholder="Enter Payment Details" name="payment_details" value="<?=$address_details['payment_details']?>" /> <br />
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <label for="zip_code" class="control-label">Zip Code</label>
                    <input type="text" class="form-control" id="zip_code" placeholder="Enter Zip Code" name="zip_code" value="<?=($address_details['zip_code']!='NULL')?$address_details['zip_code']:''?>" />
                  </div>   
                  <div class="col-md-4">
                    <label for="support_email" class="control-label">Support Email</label>
                    <input type="text" class="form-control" id="support_email" placeholder="Enter Support Email" name="support_email" value="<?=($address_details['support_email']!='NULL')?$address_details['support_email']:''?>" />
                  </div>   
                  <div class="col-md-4">
                    <label for="support_phone" class="control-label">Support Phone</label>
                    <input type="text" class="form-control" id="support_phone" placeholder="Enter Support Phone" name="support_phone" value="<?=($address_details['support_phone']!='NULL')?$address_details['support_phone']:''?>" /> <br />
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <label for="sales_email" class="control-label">Sales Email</label>
                    <input type="text" class="form-control" id="sales_email" placeholder="Enter Sales Email" name="sales_email" value="<?=($address_details['sales_email']!='NULL')?$address_details['sales_email']:''?>" />
                  </div>   
                  <div class="col-md-4">
                    <label for="sales_phone" class="control-label">Sales phone</label>
                    <input type="text" class="form-control" id="sales_phone" placeholder="Enter Sales phone" name="sales_phone" value="<?=($address_details['sales_phone']!='NULL')?$address_details['sales_phone']:''?>" />
                  </div>
                </div>

                <div class="clear"></div><br />
              </div>

              <div class="row form-group">
                <div class="text-center">
                  <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Save &nbsp;&nbsp;</button>
                </div>
              </div>
            </form>
            
          </div>
        
        </div>
      
      </div>
    </div>
    
    <!-- from validation -->
    <script>
      $("#from_addAddress").validate({
        ignore: [],
        rules: {
            company_name: { required : true, },
            phone: { required : true, },
            email: { required : true, email: true, },
            support_email: { email: true, },
            sales_email: { email: true, },
            no_street_name: { required : true, },
        },
        messages: {
            company_name: { required : "Enter company name!", },
            phone: { required : "Enter company contact numbers!", },
            email: { required : "Enter company email address", email: "Enter valid email address!" },
            support_email: { email: "Enter valid email address!" },
            sales_email: { email: "Enter valid email address!" },
            no_street_name: { required : "Enter street number or name!", },
        },
      });
    </script>
    <!-- state city ajax -->
    <script>
      $("#country_id").on('change', function(event) {  event.preventDefault();
          var country_id = $(this).val();
          $.ajax({
            url: "get-state-by-country-id",
            type: "POST",
            data: { country_id: country_id },
            dataType: "json",
            success: function(res){
              console.log(res);
              $('#state_id').attr('disabled', false);
              $('#state_id').empty();
              $('#state_id').append('<option value="0">Select State</option>');
              $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
              $('#state_id').focus();
            },
            beforeSend: function(){
              $('#state_id').empty();
              $('#state_id').append('<option value="0">Loading.....</option>');
            },
            error: function(){
              $('#state_id').attr('disabled', true);
              $('#state_id').empty();
              $('#state_id').append('<option value="0">No value found!</option>');
            }
          });
        });

      $("#state_id").on('change', function(event) {  event.preventDefault();
          var state_id = $(this).val();
          $.ajax({
            type: "POST",
            url: "get-cities-by-state-id",
            data: { state_id: state_id },
            dataType: "json",
            success: function(res){
              $('#city_id').attr('disabled', false);
              $('#city_id').empty();
              $('#city_id').append('<option value="0">Select City</option>').change();
              $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
              $('#city_id').focus();
            },
            beforeSend: function(){
              $('#city_id').empty();
              $('#city_id').append('<option value="0">Loading.....</option>');
            },
            error: function(){
              $('#city_id').attr('disabled', true);
              $('#city_id').empty();
              $('#city_id').append('<option value="0">No value found!</option>');
            }
          });
      });
    </script>