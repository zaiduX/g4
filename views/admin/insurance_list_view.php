    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li class="active">
        <strong>Insurance Configuration</strong>
      </li>
    </ol>
    
    <h2 style="display: inline-block;">Insurance Configuration List</h2>
    <?php
    $per_insurance = explode(',', $permissions[0]['insurance_master']);
    //var_dump($per_insurance); 
    if(in_array('2', $per_insurance)) { ?>  
      <a type="button" href="<?= base_url('admin/add-insurance'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
        Add New
        <i class="entypo-plus"></i>
      </a>
    <?php } ?>
  
    
    <table class="table table-bordered table-striped datatable" id="table-2">
      <thead>
        <tr>
          <th class="text-center">#</th>
          <th class="text-center">Category</th>
          <th class="text-center">Country</th>
          <th class="text-center">Min. Value</th>
          <th class="text-center">Max. Value</th>
          <th class="text-center">Fees</th>
          <th class="text-center">Commission Percent</th>
          <?php if(in_array('3', $per_insurance) || in_array('5', $per_insurance)) { ?>
            <th class="text-center">Actions</th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php $offset = $this->uri->segment(3,0) + 1; ?>
        <?php foreach ($insurance as $ins) {  ?>
        <tr>
          <td class="text-center"><?= $offset++; ?></td>
          <td class="text-center"><?= $ins['cat_name']; ?></td>
          <td class="text-center"><?= $ins['country_name'] ?></td>
          <td class="text-center"><?= $ins['min_value'] ?></td>
          <td class="text-center"><?= $ins['max_value'] ?></td>
          <td class="text-center"><?= $ins['currency_sign'].' '.$ins['ins_fee'] ?></td>
          <td class="text-center"><?= $ins['commission_percent']. '%'; ?></td>
          <?php if(in_array('3', $per_insurance) || in_array('5', $per_insurance)) { ?>
            <td class="text-center">
              <?php if(in_array('3', $per_insurance)) { ?>
                <form action="<?= base_url('admin/edit-insurance'); ?>" method="post" class="col-sm-2">
                  <input type="hidden" value="<?= $ins['ins_id']; ?>" id="ins_id" name="ins_id" />
                  <button class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Details &amp; Edit"><i class="entypo-eye"></i> View &amp; Edit</button>
                </form>
              <?php } ?>
              <?php if(in_array('5', $per_insurance)) { ?>
                <button class="btn btn-danger btn-sm btn-icon icon-left" onclick="delete_insurance('<?= $ins["ins_id"]; ?>');">
                  <i class="entypo-cancel"></i> Delete</button>
              <?php } ?>
            </td>
          <?php } ?>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    
    <br />

  <script type="text/javascript">
    jQuery( document ).ready( function( $ ) {
        var $table2 = jQuery( '#table-2' );
        
        // Initialize DataTable
        $table2.DataTable( {
          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          "bStateSave": true
        });
        
        // Initalize Select Dropdown after DataTables is created
        $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
          minimumResultsForSearch: -1
        });

        // Highlighted rows
        $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
          var $this = $(el),
            $p = $this.closest('tr');
          
          $( el ).on( 'change', function() {
            var is_checked = $this.is(':checked');
            
            $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
          } );
        } );
        
        // Replace Checboxes
        $table2.find( ".pagination a" ).click( function( ev ) {
          replaceCheckboxes();
        } );
      } );

    function delete_insurance(id=0){    
      swal({
        title: 'Are you sure?',
        text: "You want to to delete this insurance?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: true,
      }).then(function () {
        $.post('insurance-master/delete', {id: id}, function(res){
          if(res == 'success'){
            swal(
              'Deleted!',
              'Insurance has been deleted.',
              'success'
            ). then(function(){   window.location.reload();  });
          }
          else {
            swal(
              'Failed!',
              'Insurance deletion failed.',
              'error'
            )
          }
        });
        
      }, function (dismiss) {  if (dismiss === 'cancel') {  } });
    }
  </script>