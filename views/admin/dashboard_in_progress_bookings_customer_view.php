<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Users List</strong>
  </li>
</ol>

<h2 style="display: inline-block;">Top Customers With In Progress Bookings</h2>
<a href="<?= base_url('admin/user-wise-laundry-dashboard'); ?>" class="btn btn-blue btn-icon icon-left" style="float: right;">Back<i class="entypo-back"></i></a>

<table class="table table-bordered table-striped datatable" id="table-2">
  <thead>
    <tr>
      	<th class="text-center">User ID</th>
		<th class="text-center">Company [User Name]</th>
		<th class="text-center">Country</th>
		<th class="text-center">Contact No</th>
		<th class="text-center">No. of Bookings</th>
    </tr>
  </thead>
  
  <tbody>
    <?php foreach ($in_progress_bookings_customers as $users): $email_cut = explode('@', $users['email1']);  $name = $email_cut[0];  ?>
    <tr>
    	<td class="text-center"><?= $users['cust_id'] ?></td>
      	<td class="text-center"><?php 
  								if($users['company_name'] != 'NULL') { 
  									echo $users['company_name'];
  									if($users['firstname'] != 'NULL' && $users['lastname'] != 'NULL') {
  										echo ' [' . $users['firstname'] . ' ' . $users['lastname'] . ']';
  									} else { echo ' [' . $email_cut[0] . ']'; }
  								} else { echo $email_cut[0]; } 
  								?></td>
      	<td class="text-center"><?= $this->admin->get_country_name_by_id($users['country_id']) ?></td>
      	<td class="text-center"><?= $this->admin->get_country_country_phonecode_by_id($users['country_id']).' '.$users['mobile1'] ?></td>
      	<td class="text-center"><?= $users['userCount'] ?></td>
      
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    $table2.DataTable( {
      "order": [[4, 'desc']]
    });
  });
  $(window).load(function() { 
  	$("select").addClass("form-control");
  });
</script>