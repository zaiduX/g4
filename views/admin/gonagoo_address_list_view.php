    <ol class="breadcrumb bc-3" >
      <li>
        <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
      </li>
      <li class="active">
        <strong>Address Configuration</strong>
      </li>
    </ol>
    
    <h2 style="display: inline-block;">Gonagoo Address Configuration List</h2>
    <?php
    $per_gonagoo_address = explode(',', $permissions[0]['gonagoo_address']);
    //var_dump($per_gonagoo_address); 
    if(in_array('2', $per_gonagoo_address)) { ?>  
      <a type="button" href="<?= base_url('admin/add-address'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
        Add New
        <i class="entypo-plus"></i>
      </a>
    <?php } ?>
  
    
    <table class="table table-bordered table-striped datatable" id="table-2">
      <thead>
        <tr>
          <th class="text-center">#</th>
          <th class="text-center">Company Name</th>
          <th class="text-center">Country</th>
          <th class="text-center">Contact</th>
          <th class="text-center">Email</th>
          <?php if(in_array('3', $per_gonagoo_address) || in_array('5', $per_gonagoo_address)) { ?>
            <th class="text-center">Actions</th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php $offset = $this->uri->segment(3,0) + 1; ?>
        <?php foreach ($addresses as $addr) {  ?>
        <tr>
          <td class="text-center"><?= $offset++; ?></td>
          <td class="text-center"><?= $addr['company_name']; ?></td>
          <td class="text-center"><?= $this->address->get_country_name_by_id($addr['country_id']) ?></td>
          <td class="text-center"><?= $addr['phone'] ?></td>
          <td class="text-center"><?= $addr['email'] ?></td>
          <?php if(in_array('3', $per_gonagoo_address) || in_array('5', $per_gonagoo_address)) { ?>
            <td class="text-center">
              <?php if(in_array('3', $per_gonagoo_address)) { ?>
                <form action="<?= base_url('admin/edit-address'); ?>" method="post" class="col-sm-2">
                  <input type="hidden" value="<?= $addr['addr_id']; ?>" id="addr_id" name="addr_id" />
                  <button class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Details &amp; Edit"><i class="entypo-eye"></i> View &amp; Edit</button>
                </form>
              <?php } ?>
              <?php if(in_array('5', $per_gonagoo_address) && $addr['country_id'] != 75) { ?>
                <button class="btn btn-danger btn-sm btn-icon icon-left" onclick="delete_gonagoo_address('<?= $addr["addr_id"]; ?>');">
                  <i class="entypo-cancel"></i> Delete</button>
              <?php } ?>
            </td>
          <?php } ?>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    
    <br />

  <script type="text/javascript">
    jQuery( document ).ready( function( $ ) {
        var $table2 = jQuery( '#table-2' );
        
        // Initialize DataTable
        $table2.DataTable( {
          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          "bStateSave": true
        });
        
        // Initalize Select Dropdown after DataTables is created
        $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
          minimumResultsForSearch: -1
        });

        // Highlighted rows
        $table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
          var $this = $(el),
            $p = $this.closest('tr');
          
          $( el ).on( 'change', function() {
            var is_checked = $this.is(':checked');
            
            $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
          } );
        } );
        
        // Replace Checboxes
        $table2.find( ".pagination a" ).click( function( ev ) {
          replaceCheckboxes();
        } );
      } );

    function delete_gonagoo_address(id=0){    
      swal({
        title: 'Are you sure?',
        text: "You want to to delete this address?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: true,
      }).then(function () {
        $.post('address-master/delete', {id: id}, function(res){
          if(res == 'success'){
            swal(
              'Deleted!',
              'Address has been deleted.',
              'success'
            ). then(function(){   window.location.reload();  });
          }
          else {
            swal(
              'Failed!',
              'Address deletion failed.',
              'error'
            )
          }
        });
        
      }, function (dismiss) {  if (dismiss === 'cancel') {  } });
    }
  </script>