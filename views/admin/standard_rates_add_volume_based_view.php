<style>hr{border-top: 2px solid #eee;} .error { font-size: small; }</style>
<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/standard-rates'); ?>">Standard Rates</a>
  </li>
  <li class="active">
    <strong>Add Volume Based Rates</strong>
  </li>
</ol>

<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add New Volume Based Rates
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/standard-rates'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php $error = $data = array(); if($error = $this->session->flashdata('error')): $data = $error['data']; ?>
            <div class="alert alert-danger text-center"><?= $error['error_msg']; ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>

        <form role="form" action="<?= base_url('admin/standard-rates/register/volume-based'); ?>" class="form-horizontal form-groups-bordered" id="standard_rate_add" method="post">
          
          <div class="row">
            <label for="category" class="col-sm-2 control-label">Select Category </label>              
            <div class="col-sm-10">
              <select id="category_id" name="category[]" class="form-control select2" multiple placeholder="Select Category">
                <?php foreach ($categories as $c ): ?>
                  <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
                    <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                  <?php endif; ?>
                <?php endforeach; ?>
              </select>
              <span id="error_category_id"></span>
            </div>
          </div>
          <div class="clear"></div><br />

          <div class="row">
            <div class="col-md-6">
              <label for="rate_type" class="col-sm-4 control-label">Select Country </label>           
              <div class="col-sm-8">              
                <select id="country_id" name="country_id" class="form-control">
                  <option value="0">Select Country</option>
                  <?php foreach ($countries_list as $country): ?>                     
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>                   
                </select>
                <span id="error_country_id"></span>
              </div>
            </div>
            <div class="col-md-6">
              <label class="col-sm-4 control-label">Country Currency </label>
              <div class="col-sm-8">               
                <input type="hidden" name="currency_id" id="currency_id" value="0" />
                <input type="text" name="currency_name" id="currency_name" class="form-control" disabled placeholder="Country Currency" />       
                <span id="error_currency_id"></span>              
              </div>
            </div>
          </div>
          <div class="clear"></div><br />
          
          <div class="row">
            <div class="col-md-6">
              <label for="min_distance" class="col-sm-4 control-label">Min. Distance </label>           
              <div class="col-sm-8">              
                <div class="input-group">
                  <input type="number" class="form-control" id="min_distance" placeholder="Minimum Distance" name="min_distance" min="1" <?php if(!empty($data)){ echo 'value="'.$data['min_distance'].'"'; } ?> /> 
                  <span class="input-group-addon"> km </span>
                </div>
                <span id="error_min_distance"></span>             
              </div>
            </div>
            <div class="col-md-6">
              <label for="max_distance" class="col-sm-4 control-label">Max. Distance </label>
              <div class="col-sm-8">  
                <div class="input-group">            
                  <input type="number" class="form-control" id="max_distance" placeholder="Maximum Distance" name="max_distance" min="1" <?php if(!empty($data)){ echo 'value="'.$data['max_distance'].'"'; } ?>  />  
                  <span class="input-group-addon"> km </span>
                </div>
                <span id="error_max_distance"></span>
              </div>
            </div>
          </div>
          <div class="clear"></div><br />

          <div class="row">
            <div class="col-md-6">
              <label for="min_dimension" class="col-sm-4 control-label">Min. Dimension </label>           
              <div class="col-sm-8">              
                <select id="min_dimension" name="min_dimension" class="form-control">
                  <option value="0">Select Dimension</option>
                  <?php foreach ($dimensions_list as $dimension): ?>                      
                    <option value="<?= $dimension['dimension_id'] ?>"><?= $dimension['dimension_type']; ?></option>
                  <?php endforeach ?>
                  <option value="other">Custom</option>
                </select>
                <div id="error_min_dimension"></div>  
                <input type="hidden" id="min_dimension_volume" name="min_dimension_volume" />`              
              </div>
            </div>            
            <div class="col-md-6">
              <label for="max_dimension" class="col-sm-4 control-label">Max. Dimension </label>           
              <div class="col-sm-8">              
                <select id="max_dimension" name="max_dimension" class="form-control">
                  <option value="0">Select Dimension</option>
                  <?php foreach ($dimensions_list as $dimension): ?>                      
                    <option value="<?= $dimension['dimension_id'] ?>"><?= $dimension['dimension_type']; ?></option>
                  <?php endforeach ?>
                  <option value="other">Custom</option>
                </select>
                <div id="error_max_dimension"></div>                    
                <input type="hidden" id="max_dimension_volume" name="max_dimension_volume" />         
              </div>
            </div>
          </div>          
          <div class="clear"></div>
        
          <div class="row">
            <div class="col-md-6">
              <label for="min_dimension" class="col-sm-4 control-label">Min. W / H / L </label>           
              <div class="col-sm-8">                
                <div class="row">
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="min_width" placeholder="Width" name="min_width" min="0" <?php if(!empty($data)){ echo 'value="'.$data['min_width'].'"'; } ?> readonly/>                     
                  </div>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="min_height" placeholder="Height" name="min_height" min="0" <?php if(!empty($data)){ echo 'value="'.$data['min_height'].'"'; } ?> readonly/>                     
                  </div>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="min_length" placeholder="Height" name="min_length" min="0" <?php if(!empty($data)){ echo 'value="'.$data['min_length'].'"'; } ?> readonly/>                     
                  </div>
                </div>
              </div>              
            </div>
            <div class="col-md-6">
              <label for="min_dimension" class="col-sm-4 control-label">Max. W / H / L </label>           
              <div class="col-sm-8">                
                <div class="row">
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="max_width" placeholder="Width" name="max_width" max="0" <?php if(!empty($data)){ echo 'value="'.$data['max_width'].'"'; } ?> readonly/>                     
                  </div>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="max_height" placeholder="Height" name="max_height" max="0" <?php if(!empty($data)){ echo 'value="'.$data['max_height'].'"'; } ?> readonly/>                     
                  </div>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="max_length" placeholder="Height" name="max_length" max="0" <?php if(!empty($data)){ echo 'value="'.$data['max_length'].'"'; } ?> readonly/>                     
                  </div>
                </div>
              </div>              
            </div>
          </div>
          <div class="clear"></div><br />
          <!-- Strat - Not in use yet -->
          <div class="row hidden">
            <div class="col-md-4"><hr/></div>
            <div class="col-md-5 text-center"><h4 class="text-primary">Min. Down / Max. Hike Rates Percentage</h4></div>
            <div class="col-md-3"><hr/></div>
          </div>
        
          <div class="row hidden">  
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="min_rate_down_percent" class="control-label">Min. Rate Down Percent </label>            
              <div class="">              
                <div class="input-group">
                  <input type="number" class="form-control" id="min_rate_down_percent" placeholder="Enter Duration" name="min_rate_down_percent" min="0" disabled />  
                  <span class="input-group-addon">%</span>
                </div>
                <span id="error_min_rate_down_percent"></span>
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="max_rate_hike_percent" class="control-label">Max. Hike Percent </label>           
              <div class="">              
                <div class="input-group">
                  <input type="number" class="form-control" id="max_rate_hike_percent" placeholder="Enter Duration" name="max_rate_hike_percent" min="0" disabled />  
                  <span class="input-group-addon">%</span>
                </div>
                <span id="error_max_rate_hike_percent"></span>
              </div>
            </div>
          </div>
          <div class="clear"></div><br />
          <!-- END -->

          <div class="row">
            <div class="col-md-4"><hr/></div>
            <div class="col-md-4 text-center"><h4 class="text-primary">Earth Rates &amp; Durations</h4></div>
            <div class="col-md-4"><hr/></div>
          </div>
          
          <div class="row">           
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-3">
              <label for="earth_local_rate" class="control-label">Local Rate </label>           
              <div class="">              
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>
                  <input type="number" class="form-control" id="earth_local_rate" placeholder="Enter Rate" name="earth_local_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_local_rate'].'"'; } ?> /> 
                </div>
                <span id="error_earth_local_rate"></span>             
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="earth_national_rate" class="control-label">National Rate</label>
              <div class="">  
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>
                  <input type="number" class="form-control" id="earth_national_rate" placeholder="Enter Rate" name="earth_national_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_national_rate'].'"'; } ?>  /> 
                </div>
                <span id="error_earth_national_rate"></span>
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label class="control-label">International </label>
              <div class="">  
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>
                  <input type="number" class="form-control" id="earth_international_rate" placeholder="Enter Rate" name="earth_international_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['earth_international_rate'].'"'; } ?>  />  
                </div>
                <span id="error_earth_international_rate"></span>
              </div>
            </div>
          </div>
          <div class="clear"></div><br />
          
          <div class="row">           
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-3">
              <label for="earth_local_duration" class="control-label">Local Duration </label>           
              <div class="">              
                <div class="input-group">
                  <input type="number" class="form-control" id="earth_local_duration" placeholder="Enter Duration" name="earth_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_local_duration'].'"'; } ?> /> 
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_earth_local_duration"></span>             
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="earth_national_duration" class="control-label">National Duration</label>
              <div class="">  
                <div class="input-group">
                  <input type="number" class="form-control" id="earth_national_duration" placeholder="Enter Duration" name="earth_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_national_duration'].'"'; } ?>  /> 
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_earth_national_duration"></span>
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="earth_international_duration" class="control-label">International Duration</label>
              <div class="">  
                <div class="input-group">
                  <input type="number" class="form-control" id="earth_international_duration" placeholder="Enter Duration" name="earth_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['earth_international_duration'].'"'; } ?>  />  
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_earth_international_duration"></span>
              </div>
            </div>
          </div>
          <div class="clear"></div><br />

          <div class="row">
            <div class="col-md-4"><hr/></div>
            <div class="col-md-4 text-center"><h4 class="text-primary">Air Rates &amp; Durations</h4></div>
            <div class="col-md-4"><hr/></div>
          </div>
          <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-3">
              <label for="air_local_rate" class="control-label">Local Rate </label>           
              <div class="">
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>
                  <input type="number" class="form-control" id="air_local_rate" placeholder="Enter Rate" name="air_local_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_local_rate'].'"'; } ?> /> 
                </div>
                <span id="error_air_local_rate"></span>             
              </div>
            </div>
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-3">
              <label for="air_national_rate" class="control-label">National Rate </label>
              <div class="">
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>  
                  <input type="number" class="form-control" id="air_national_rate" placeholder="Enter Rate" name="air_national_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_national_rate'].'"'; } ?>  /> 
                </div>
                <span id="error_air_national_rate"></span>
              </div>
            </div>
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-3">
              <label for="air_international_rate" class="control-label">International Rate </label>
              <div class="">  
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>  
                  <input type="number" class="form-control" id="air_international_rate" placeholder="Enter Rate" name="air_international_rate" min="0" <?php if(!empty($data)){ echo 'value="'.$data['air_international_rate'].'"'; } ?>  />  
                </div>
                <span id="error_air_international_rate"></span>
              </div>
            </div>
          </div>
          <div class="clear"></div><br />
          <div class="row">           
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-3">
              <label for="air_local_duration" class="control-label">Local Duration </label>           
              <div class="">              
                <div class="input-group">
                  <input type="number" class="form-control" id="air_local_duration" placeholder="Enter Duration" name="air_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_local_duration'].'"'; } ?> /> 
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_air_local_duration"></span>             
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="air_national_duration" class="control-label">National Duration</label>
              <div class="">  
                <div class="input-group">
                  <input type="number" class="form-control" id="air_national_duration" placeholder="Enter Duration" name="air_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_national_duration'].'"'; } ?>  /> 
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_air_national_duration"></span>
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="air_international_duration" class="control-label">International Duration</label>
              <div class="">  
                <div class="input-group">
                  <input type="number" class="form-control" id="air_international_duration" placeholder="Enter Duration" name="air_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['air_international_duration'].'"'; } ?>  />  
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_air_international_duration"></span>
              </div>
            </div>
          </div>
          <div class="clear"></div><br />

          <div class="row">
            <div class="col-md-4"><hr/></div>
            <div class="col-md-4 text-center"><h4 class="text-primary">Sea Rates &amp; Durations</h4></div>
            <div class="col-md-4"><hr/></div>
          </div>      
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="sea_local_rate" class="control-label">Local Rate </label>           
              <div class="">
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>              
                  <input type="number" class="form-control" id="sea_local_rate" placeholder="Enter Rate" min="0" name="sea_local_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_local_rate'].'"'; } ?> /> 
                </div>
                <span id="error_sea_local_rate"></span>             
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="sea_national_rate" class="control-label">National Rate </label>
              <div class="">  
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>
                  <input type="number" class="form-control" id="sea_national_rate" placeholder="Enter Rate" min="0" name="sea_national_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_national_rate'].'"'; } ?>  /> 
                </div>
                <span id="error_sea_national_rate"></span>
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="sea_international_rate" class="control-label">International Rate </label>
              <div class="">  
                <div class="input-group">
                  <span class="input-group-addon currency fa">?</span>
                  <input type="number" class="form-control" id="sea_international_rate" min="0" placeholder="Enter Rate" name="sea_international_rate" <?php if(!empty($data)){ echo 'value="'.$data['sea_international_rate'].'"'; } ?>  />  
                </div>
                <span id="error_sea_international_rate"></span>
              </div>
            </div>
          </div>
          <div class="clear"></div><br />
          <div class="row">           
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-3">
              <label for="sea_local_duration" class="control-label">Local Duration </label>           
              <div class="">              
                <div class="input-group">
                  <input type="number" class="form-control" id="sea_local_duration" placeholder="Enter Duration" name="sea_local_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_local_duration'].'"'; } ?> /> 
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_sea_local_duration"></span>             
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="sea_national_duration" class="control-label">National Duration</label>
              <div class="">  
                <div class="input-group">
                  <input type="number" class="form-control" id="sea_national_duration" placeholder="Enter Duration" name="sea_national_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_national_duration'].'"'; } ?>  /> 
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_sea_national_duration"></span>
              </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <label for="sea_international_duration" class="control-label">International Duration</label>
              <div class="">  
                <div class="input-group">
                  <input type="number" class="form-control" id="sea_international_duration" placeholder="Enter Duration" name="sea_international_duration" min="1" <?php if(!empty($data)){ echo 'value="'.$data['sea_international_duration'].'"'; } ?>  />  
                  <span class="input-group-addon">hrs</span>
                </div>
                <span id="error_sea_international_duration"></span>
              </div>
            </div>
          </div>
          <div class="clear"></div><br /><hr/>
  
          <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
              <div class="text-right">
                <button id="btn_submit" type="submit" class="btn btn-blue"><i class="fa fa-plus-square"></i> &nbsp; Add Rates</button>
              </div>
            </div>
          </div>
          
        </form>
      </div>  
    </div>  
  </div>
</div>

<br />

<script>
  
  $("input").not("input[type=submit]").on('change', function(event) { event.preventDefault();
    var input = $(this);
    $("span[id^='error_']").removeClass('error').text("");
  });

  // $(".currency").addClass('hidden');
  $("#country_id").on('change', function(event) { event.preventDefault();
    $(".currency").addClass('hidden').html('');
    var country_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-country-currencies", 
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        if(res.length > 0 ){
          $("#currency_id").val(res[0]['currency_id']);
          $("#currency_name").val(res[0]['currency_title'] + '( ' + res[0]['currency_sign'] + ' )');
          $(".currency").removeClass('hidden').html('<strong>'+res[0]['currency_sign']+'</strong>'); 
          $('#currency_name').focus();
        } 
        else{
          $('#currency_id').val('0');
          $('#currency_name').val('No currency found!');
        }
      },
      beforeSend: function(){
        $('#currency_id').empty();
        $('#currency_id').val('Loading...');
      },
      error: function(){
        $('#currency_id').val('0');
        $('#currency_name').val('No currency found!');
      }
    });
  });

  $("#currency_id").on('change', function(event) {  event.preventDefault();
    var currency = $("#currency_id option:selected").text();
    var sign = currency.match(/\((.*)\)/);
    if(sign != null){ $(".currency").removeClass('hidden').html('<strong>'+sign[1]+'</strong>'); }
    else { $(".currency").addClass('hidden').html(''); }
  });
  $("#min_dimension").on('change', function(event) {  event.preventDefault();
    var dimensions_list = <?= json_encode($dimensions_list); ?>;
    var id = $(this).val();
    
    if(id != "other" && id > 0) {
      $("#min_width, #min_height, #min_length").prop('readonly', true);
      $.each(dimensions_list, function(i, el) {
        if(dimensions_list[i].dimension_id == id) { 
          $("#min_dimension_volume").val(dimensions_list[i].volume);
          $("#min_width").val(dimensions_list[i].width);
          $("#min_height").val(dimensions_list[i].height);
          $("#min_length").val(dimensions_list[i].length);
          $("#error_min_dimension").removeClass('error').addClass('text-primary').css('font-size','x-small').html('Width: '+dimensions_list[i].width +' x Height: '+dimensions_list[i].height +' x Length: '+dimensions_list[i].length +' = Volume: '+dimensions_list[i].volume);
        }
      });
    } else{ $("#min_width, #min_height, #min_length").val('').prop('readonly', false); $("#error_min_dimension").html('');  }
  });

  $("#max_dimension").on('change', function(event) {  event.preventDefault();
    var dimensions_list = <?= json_encode($dimensions_list); ?>;
    var id = $(this).val();
    if(id != "other" && id > 0) {
      $("#max_width, #max_height, #max_length").prop('readonly', true);
      $.each(dimensions_list, function(i, el) {
        if(dimensions_list[i].dimension_id == id) { 
          $("#max_dimension_volume").val(dimensions_list[i].volume);  
          $("#max_width").val(dimensions_list[i].width);
          $("#max_height").val(dimensions_list[i].height);
          $("#max_length").val(dimensions_list[i].length);    
          $("#error_max_dimension").removeClass('error').addClass('text-primary').css('font-size','x-small').html('Width: '+dimensions_list[i].width +' x Height: '+dimensions_list[i].height +' x Length: '+dimensions_list[i].length +' = Volume: '+dimensions_list[i].volume);
        }
      });
    } else{ $("#max_width, #max_height, #max_length").val('').prop('readonly', false); $("#error_max_dimension").html('');  }
  });

  $("#btn_submit").click(function(e){ e.preventDefault();
    $(".alert").removeClass('alert-danger alert-success').addClass('hidden').html("");        
    var categories = $("#category_id").val();
    var country_id = $("#country_id").val();
    var currency_id = $("#currency_id").val();
    var min_dimension = $("#min_dimension").val();
    var max_dimension = $("#max_dimension").val();

    var min_dimension_volume = 0; 
    var max_dimension_volume = 0; 

    if(min_dimension == "other") { 
      min_dimension_volume = (parseFloat($("#min_width").val()) * parseFloat($("#min_height").val()) *parseFloat($("#min_length").val()));
      min_dimension_volume = min_dimension_volume.toFixed(2);
      $("#min_dimension_volume").val(min_dimension_volume);
    } 
    else {  
      min_dimension = parseFloat(min_dimension).toFixed(2);
      min_dimension_volume = parseFloat($("#min_dimension_volume").val()).toFixed(2); 
    }

    if(max_dimension == "other") { 
      max_dimension_volume = parseFloat($("#max_width").val()) * parseFloat($("#max_height").val()) *parseFloat($("#max_length").val()); 
      max_dimension_volume =  max_dimension_volume.toFixed(2);
      $("#max_dimension_volume").val(max_dimension_volume);
    } 
    else { 
      max_dimension = parseFloat(max_dimension).toFixed(2);
      max_dimension_volume = parseFloat($("#max_dimension_volume").val()).toFixed(2); 
    }

    var min_distance = parseFloat($("#min_distance").val()).toFixed(2);
    var max_distance = parseFloat($("#max_distance").val()).toFixed(2);

    var earth_local_rate = parseFloat($("#earth_local_rate").val()).toFixed(2);
    var earth_national_rate = parseFloat($("#earth_national_rate").val()).toFixed(2);
    var earth_international_rate = parseFloat($("#earth_international_rate").val()).toFixed(2);       
    var air_local_rate = parseFloat($("#air_local_rate").val()).toFixed(2);
    var air_national_rate = parseFloat($("#air_national_rate").val()).toFixed(2);
    var air_international_rate = parseFloat($("#air_international_rate").val()).toFixed(2);       
    var sea_local_rate = parseFloat($("#sea_local_rate").val()).toFixed(2);
    var sea_national_rate = parseFloat($("#sea_national_rate").val()).toFixed(2);
    var sea_international_rate = parseFloat($("#sea_international_rate").val()).toFixed(2);

    var earth_local_duration = parseFloat($("#earth_local_duration").val()).toFixed(2);
    var earth_national_duration = parseFloat($("#earth_national_duration").val()).toFixed(2);
    var earth_international_duration = parseFloat($("#earth_international_duration").val()).toFixed(2);       
    var air_local_duration = parseFloat($("#air_local_duration").val()).toFixed(2);
    var air_national_duration = parseFloat($("#air_national_duration").val()).toFixed(2);
    var air_international_duration = parseFloat($("#air_international_duration").val()).toFixed(2);       
    var sea_local_duration = parseFloat($("#sea_local_duration").val()).toFixed(2);
    var sea_national_duration = parseFloat($("#sea_national_duration").val()).toFixed(2);
    var sea_international_duration = parseFloat($("#sea_international_duration").val()).toFixed(2);

    if(!categories) {   $("#error_category_id").addClass('error').html("Please Select at-least one Category."); $("#category_id").focus();  }
    else if(country_id == 0 ) {  
      $("#error_category_id").removeClass('error').html("");
      $("#error_country_id").addClass('error').html("Select country."); $("#country_id").focus();   
    }
    else if(currency_id == 0 ) {
      $("#error_country_id, #error_category_id").removeClass('error').html("");
      $("#error_currency_id").addClass('error').html("Select currency.");
      $("#currency_id").focus();
    }
    else if(isNaN(min_distance)) {
      $("#error_min_height,#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id, #error_min_height, #error_max_height").removeClass('error').html("");
      $("#error_min_length, #error_max_length").removeClass('error').html("");
      $("#error_min_distance").addClass('error').html("Please enter minimum distance.");
      $("#min_distance").focus();
    }
    else if(isNaN(max_distance)) {
      $("#error_min_height,#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id, #error_min_height, #error_max_height").removeClass('error').html("");
      $("#error_min_distance").removeClass('error').html("");
      $("#error_max_distance").addClass('error').html("Please enter maximum distance.");
      $("#max_distance").focus();
    }
    else if(parseFloat(max_distance) < parseFloat(min_distance) ) {
      $("#error_min_height,#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id, #error_min_height, #error_max_height").removeClass('error').html("");
      $("#error_min_distance").removeClass('error').html("");
      $("#error_max_distance").addClass('error').html("Please enter maximum distance greater than minimum distance.");
      $("#max_distance").focus();
    }
    else if(min_dimension == 0 ) {
      $("#error_currency_id, #error_country_id").removeClass('error').html("");
      $("#error_min_dimension").addClass('error').html("Enter minimum dimension.");
      $("#min_dimension").focus();
    }       
    else if(max_dimension == 0 ) {
      $("#error_min_dimension,#error_currency_id, #error_country_id").removeClass('error').html("");
      $("#error_max_dimension").addClass('error').css('font-size','normal').html("Enter maximum dimension.");
      $("#max_dimension").focus();
    }
    else if(parseFloat(max_dimension) == parseFloat(min_dimension) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id, #error_country_id").removeClass('error').html("");
      $("#error_max_dimension").addClass('error').css('font-size','normal').html("Select different standard dimension as minimum.");
      $("#max_dimension").focus();
    }       
    else if(parseFloat(max_dimension_volume) <= parseFloat(min_dimension_volume) ) {
      $("#error_min_height,#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_max_dimension").addClass('error').html("Select Maximum dimension greater than minimum dimension.");
      $("#max_dimension").focus();
    }                     
    else if(isNaN(earth_local_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration").removeClass('error').html("");
      $("#error_earth_local_rate").addClass('error').html("Enter Rate.");
      $("#earth_local_rate").focus();
    }
    else if(isNaN(earth_national_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_local_rate").removeClass('error').html("");
      $("#error_earth_national_rate").addClass('error').html("Enter Rate.");
      $("#earth_national_rate").focus();
    }
    else if(isNaN(earth_international_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_national_rate").removeClass('error').html("");
      $("#error_earth_international_rate").addClass('error').html("Enter Rate.");
      $("#earth_international_rate").focus();
    }
    else if(isNaN(earth_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");      
      $("#error_earth_local_duration").addClass('error').html("Enter duration.");
      $("#earth_local_duration").focus();
    }
    else if(earth_local_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#earth_local_duration").focus();
    }
    else if(isNaN(earth_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_earth_local_duration, #error_earth_local_rate").removeClass('error').html("");
      $("#error_earth_national_duration").addClass('error').html("Enter duration.");
      $("#earth_national_duration").focus();
    }
    else if(earth_national_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#earth_national_duration").focus();
    }
    else if(isNaN(earth_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_earth_local_duration, #error_earth_national_rate, #error_earth_national_duration").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_earth_international_duration").addClass('error').html("Enter duration.");
      $("#earth_international_duration").focus();
    }
    else if(earth_international_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#earth_international_duration").focus();
    }
    else if(isNaN(air_local_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_international_rate").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_air_local_rate").addClass('error').html("Enter Rate.");
      $("#air_local_rate").focus();
    }
    else if(isNaN(air_national_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_local_rate").removeClass('error').html("");
      $("#error_air_national_rate").addClass('error').html("Enter Rate.");
      $("#air_national_rate").focus();
    }
    else if(isNaN(air_international_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_national_rate").removeClass('error').html("");
      $("#error_air_international_rate").addClass('error').html("Enter Rate.");
      $("#air_international_rate").focus();
    }
    else if(isNaN(air_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_earth_international_rate").removeClass('error').html("");
      $("#error_earth_national_duration").removeClass('error').html("");
      $("#error_air_local_duration").addClass('error').html("Enter Rate.");
      $("#air_local_duration").focus();
    }
    else if(air_local_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#air_local_duration").focus();
    }
    else if(isNaN(air_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_local_rate").removeClass('error').html("");
      $("#error_air_national_duration").addClass('error').html("Enter Rate.");
      $("#air_national_duration").focus();
    }
    else if(air_national_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#air_national_duration").focus();
    }
    else if(isNaN(air_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension, #error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_national_rate").removeClass('error').html("");
      $("#error_air_international_duration").addClass('error').html("Enter Rate.");
      $("#air_international_duration").focus();
    }
    else if(air_international_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#air_international_duration").focus();
    }
    else if(isNaN(sea_local_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_international_rate").removeClass('error').html("");
      $("#error_sea_local_rate").addClass('error').html("Enter Rate.");
      $("#sea_local_rate").focus();
    }
    else if(isNaN(sea_national_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_local_rate").removeClass('error').html("");
      $("#error_sea_national_rate").addClass('error').html("Enter Rate.");
      $("#sea_national_rate").focus();
    }
    else if(isNaN(sea_international_rate) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_national_rate").removeClass('error').html("");
      $("#error_sea_international_rate").addClass('error').html("Enter Rate.");
      $("#sea_international_rate").focus();
    }
    else if(isNaN(sea_local_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_air_international_rate").removeClass('error').html("");
      $("#error_sea_local_duration").addClass('error').html("Enter Rate.");
      $("#sea_local_duration").focus();
    }
    else if(sea_local_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#sea_local_duration").focus();
    }
    else if(isNaN(sea_national_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_local_rate").removeClass('error').html("");
      $("#error_sea_national_duration").addClass('error').html("Enter Rate.");
      $("#sea_national_duration").focus();
    }
    else if(sea_national_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#sea_national_duration").focus();
    }
    else if(isNaN(sea_international_duration) ) {
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_national_rate").removeClass('error').html("");
      $("#error_sea_international_duration").addClass('error').html("Enter Rate.");
      $("#sea_international_duration").focus();
    }
    else if(sea_international_duration < 1 ) {
      $("#error_earth_local_duration").addClass('error').html("Enter duration greater than 1.");
      $("#sea_international_duration").focus();
    }
    else { 
      $("#error_max_dimension, #error_min_dimension,#error_currency_id").removeClass('error').html("");
      $("#error_country_id").removeClass('error').html("");
      $("#error_min_distance, #error_max_distance").removeClass('error').html("");
      $("#error_max_duration, #error_sea_international_rate").removeClass('error').html(''); 
      $("#standard_rate_add")[0].submit(); 
    }
  });

</script>