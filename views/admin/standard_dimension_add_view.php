<ol class="breadcrumb bc-3" >
  <li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="<?= base_url('admin/standard-dimensions'); ?>">Standard Dimensions</a></li>
  <li class="active"><strong>Create</strong></li>
</ol>
<div class="row">
    <div class="col-md-12">        
    <div class="panel panel-dark" data-collapsed="0">

        <div class="panel-heading">
        <div class="panel-title">Add New Standard Dimension</div>        
        <div class="panel-options">
          <a href="<?= base_url('admin/standard-dimensions'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>

      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>

        <form role="form" action="<?= base_url('admin/standard-dimensions/register'); ?>" class="form-groups-bordered" id="dimension_add" method="post" enctype="multipart/form-data">
                    
                    <div class="row">
                        
                        <div class="col-md-3 text-center">
                            <div class="form-group">
                              <label class="control-label">Standard Dimension Image </label>
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                  <img src="<?= $this->config->item('resource_url').'noimage.png';?>" alt="Dimension image">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                <div>
                                  <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="dimension_image" id="dimension_image" accept="image/*">
                                  </span>
                                  <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                              </div>
                            </div>
                        </div>

                        <div class="col-md-9">

                            <div class="form-group col-md-8">
                                <label for="dimension_type" class="control-label">Standard Dimension Type</label>                                     
                  <input type="text" class="form-control" id="dimension_type" placeholder="Enter Dimension Type ..." name="dimension_type" />                 
                </div>                              
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                <label for="service_type" class="control-label">Service Type</label>                      
                                  <select id="service_type" name="service_type[]" class="form-control select2" multiple placeholder="Select service type">
                                    <?php foreach ($categories as $c ): ?>
                                      <?php if($c['cat_id'] == 6 || $c['cat_id'] == 7 || $c['cat_id'] == 280 ): ?>
                                        <option value="<?= $c['cat_id']?>"><?= $c['cat_name']?></option>
                                      <?php endif; ?>
                                    <?php endforeach; ?>
                                  </select>                   
                                </div>
                            </div>

              <div class="col-sm-4">
                    <div class="form-group">
                    <label class="control-label">Standard Dimension Width</label>                     
                  <div class="input-group">
                    <input type="text" class="form-control" id="width" placeholder="Enter Width" name="width" />    
                    <span class="input-group-addon"> cm </span>
                  </div>
                </div>
              </div>
              
              <div class="col-sm-4">
                    <div class="form-group">
                    <label class="control-label">Standard Dimension Height</label>                     
                  <div class="input-group">
                    <input type="text" class="form-control" id="height" placeholder="Enter height" name="height" />
                    <span class="input-group-addon"> cm </span>
                  </div>
                </div>
              </div>
                  
              <div class="col-sm-4">
                    <div class="form-group">
                    <label class="control-label">Standard Dimension Length</label>                     
                  <div class="input-group">
                    <input type="text" class="form-control" id="length" placeholder="Enter length" name="length" />
                    <span class="input-group-addon"> cm </span>
                  </div>
                </div>                
                </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                    <label class="control-label">Standard Dimension Applicable For</label>
                </div>

                                <div class="col-sm-3">
                                    <div class="checkbox checkbox-replace color-blue">
                                        <input type="checkbox" name="on_earth" id="on_earth" class="form-control" >
                                        <label for="on_earth">On Earth</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="checkbox checkbox-replace color-blue">
                                        <input type="checkbox" name="on_air" id="on_air" class="form-control">
                                        <label for="on_air">On Air</label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="checkbox checkbox-replace color-blue">
                                        <input type="checkbox" name="on_sea" id="on_sea" class="form-control">
                                        <label for="on_sea">On Sea</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 text-right">
                                <div class="form-group">
                                    <label class="control-label">&nbsp;</label>                   
                                    <button type="submit" id="btn_submit" class="btn btn-blue">Submit</button>
                                </div>
                            </div>
    
                        </div>
                    </div>

        </form>

      </div>
    </div>
  </div>
</div>

<script>
    $("#dimension_add").on('submit',function(e){e.preventDefault();});
    $("#btn_submit").on('click',function(){
        var image = $("#dimension_image").val();
        var type = $("#dimension_type").val();
        var service = $("#service_type").val();
        var width = $("#width").val();
        var height = $("#height").val();
        var length = $("#length").val();
        var on_earth = $("#on_earth").prop('checked');
        var on_air = $("#on_air").prop('checked');
        var on_sea = $("#on_sea").prop('checked');

        if(!image){ swal('Error',"Select Dimension Image", 'warning');  }
        else if(!type){ swal('Error',"Enter Dimension Type", 'warning');    }
        else if(!service){  swal('Error',"Select Service Type", 'warning'); }
        else if(!width){    swal('Error',"Enter Standard Dimension Width", 'warning');  }
        else if(!height){   swal('Error',"Enter Standard Dimension Height", 'warning'); }
        else if(!length){   swal('Error',"Enter Standard Dimension Length", 'warning'); }
        else if((!on_earth) && (!on_air) && (!on_sea)){ swal('Error',"Select Standard Dimension Applicable For", 'warning');    }
        else{ $("#dimension_add").get(0).submit(); }
    });

</script>