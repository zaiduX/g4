<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li>
    <a href="<?= base_url('admin/advance-payment'); ?>">Country-wise Dangerous Goods</a>
  </li>
  <li class="active">
    <strong>Add New Country-wise Dangerous Goods</strong>
  </li>
</ol>
<style> .border{ border:1px solid #ccc; margin-bottom:10px; padding: 5px; } </style>
<div class="row">
  <div class="col-md-12">
    
    <div class="panel panel-dark" data-collapsed="0">
    
      <div class="panel-heading">
        <div class="panel-title">
          Add New Dangerous Goods
        </div>
        
        <div class="panel-options">
          <a href="<?= base_url('admin/country-dangerous-goods'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
        </div>
      </div>
      
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        
        <form role="form" action="<?= base_url('admin/country-dangerous-goods/register'); ?>" class="form-horizontal form-groups-bordered validate" id="country_dangerous_goods_add" method="post" autocomplete="off" novalidate="novalidate">
          
          <div class="border">
            <div class="row">
              <div class="col-md-5">
                <label for="country_id" class="control-label">Select Country </label>                        
                <select id="country_id" name="country_id" class="form-control select2">
                  <option value="">Select Country</option>
                  <?php foreach ($countries as $country): ?>                      
                    <option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
                <span id="error_country_id" class="error"></span>                           
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label for="added_goods" class="control-label">Select Dangerous Goods </label>              
                <select id="added_goods" name="added_goods[]" class="form-control select2" multiple placeholder="Dangerous Goods">
                  <?php foreach ($goods as $g ): ?>
                    <option value="<?= $g['id']?>"><?= $g['name']?></option>                    
                  <?php endforeach; ?>
                </select>
              </div>             
            </div>
            <div class="clear"></div><br />
            <div class="row">
              <div class="col-md-12">
                <div class="text-center">
                  <button id="btn_submit" type="submit" class="btn btn-blue">&nbsp;&nbsp; Submit &nbsp;&nbsp;</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $("#country_dangerous_goods_add").submit(function(e) { e.preventDefault(); });
  $("#btn_submit").on('click', function(e) {  e.preventDefault();
    var country_id = $("#country_id").val();
    var added_goods = $("#added_goods").val();

    if(!country_id) { swal('Error','Select Country','error');}
    else if(!added_goods) { swal('Error','Select Dangerous Goods','error');}
    else{ $("#country_dangerous_goods_add").get(0).submit();}

  });
</script>