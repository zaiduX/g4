<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/authority-type'); ?>">Authority Type Master</a>
	</li>
	<li class="active">
		<strong>Add New</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Add Authority Type
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/authority-type'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
				
				<form role="form" action="<?= base_url('admin/register-authority-type'); ?>" class="form-horizontal form-groups-bordered" id="authority_type" method="post">
	
					<div class="form-group">
						<label for="auth_type" class="col-sm-3 control-label">Authority Type</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" id="auth_type" placeholder="Enter authority type..." name="auth_type" />	
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-blue">Add Authority Type</button>
						</div>
					</div>
				</form>
				
			</div>
		
		</div>
	
	</div>
</div>

<br />