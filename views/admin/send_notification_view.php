<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li>
		<a href="<?= base_url('admin/authority'); ?>">Notifications</a>
	</li>
	<li class="active">
		<strong>Send Notification</strong>
	</li>
</ol>

<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-dark" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Send Notification
				</div>
				
				<div class="panel-options">
					<a href="<?= base_url('admin/notification'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a>
				</div>
			</div>
			
			<div class="panel-body">

				<?php if($this->session->flashdata('error')):  ?>
		      		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
		    	<?php endif; ?>
		    	<?php if($this->session->flashdata('success')):  ?>
		      		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
		    	<?php endif; ?>
	
				<form id="send_notification" role="form" action="<?= base_url('admin/notification-send'); ?>" enctype="multipart/form-data" class="form-horizontal form-groups-bordered" id="authorityAdd" method="post" autocomplete="off" enctype="multipart/form-data">
	
					<div class="form-group">
						<label for="consumer_type" class="col-sm-2 control-label">Filter By Consumer :</label>						
						<div class="col-sm-3">
							<select id="consumer_type" name="consumer_type" class="form-control">
								<option value="0">All</option>
								<option value="1">Individuals</option>
								<option value="2">Business</option>							
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="country_id" class="col-sm-2 control-label">Filter By Location :</label>						
						<div class="col-sm-3">
							<select id="country_id" name="country_id" class="form-control">
								<option value="0">Select Country</option>
								<?php foreach ($countries as $country): ?>											
									<option value="<?= $country['country_id'] ?>"><?= $country['country_name']; ?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="col-sm-4">
							<select id="state_id" name="state_id" class="form-control" disabled>
								<option value="0">Select State</option>
							</select>
						</div>

						<div class="col-sm-3">
							<select id="city_id" name="city_id" class="form-control" disabled>
							<option value="0">Select City</option>
						</select>
						</div>
					</div>

					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Notification Title :</label>						
						<div class="col-sm-10">
							<input type="text" class="form-control" id="title" name="title" placeholder="Enter title for notification ..." />	
						</div>
					</div>

					<div class="form-group">
						<label for="message" class="col-sm-2 control-label">Message :</label>
						
						<div class="col-sm-10">
							<textarea id="message" name="message" class="form-control autogrow" rows="5"  placeholder="Write some message for notification ..." style="resize: none;"></textarea>		
						</div>
					</div>

					<div class="form-group">
						<label for="attachement" class="col-sm-2 control-label">Attachement :</label>
						<div class="col-sm-8">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<span class="btn btn-info btn-file">
									<span class="fileinput-new">Select file</span>
									<span class="fileinput-exists">Change</span>
									<input type="file" name="attachement" id="attachement">
								</span>
								<span class="fileinput-filename"></span>
								<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
							</div>
							</div>

							<div class="col-sm-2">							
								<button type="submit" class="pull-right btn btn-blue btn-icon icon-left"><i class="fa fa-send"></i> SEND</button>
							</div>
						</div>

						
					</div>

				</form>
	
			</div>
		
		</div>
	
	</div>
</div>

<br />	

	<script>		
		$("#country_id").on('change', function(event) {	event.preventDefault();
			var country_id = $(this).val();
			if(country_id > 0 ){
				$.ajax({
	          type: "POST", 
	          url: "get-states", 
	          data: { country_id: country_id },
	          dataType: "json",
	          success: function(res){                 
	            //Clear options corresponding to earlier option of first dropdown
	            $('#city_id').attr('disabled', true); 
	            $('#state_id').attr('disabled', false); 
	            $('#state_id').empty();
	            $('#state_id').append('<option value="0">Select State</option>');
	            //Populate options of the second dropdown
	            $.each( res, function(){    
	                $('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');
	            });
	            $('#state_id').focus();
	          },
	          beforeSend: function(){
	            $('#state_id').empty();
	            $('#state_id').append('<option value="0">Loading...</option>');
	          },
	          error: function(er){
	            $('#state_id').attr('disabled', true);
	            $('#state_id').empty();
	            $('#state_id').append('<option value="0">No State Found!</option>');
	          }
	      })
			}
		});

		$("#state_id").on('change', function(event) {	event.preventDefault();
			var state_id = $(this).val();
			if(state_id > 0 ) {
				$.ajax({
	          type: "POST", 
	          url: "get-cities", 
	          data: { state_id: state_id },
	          dataType: "json",
	          success: function(res){                 
	            //Clear options corresponding to earlier option of first dropdown
	            $('#city_id').attr('disabled', false); 
	            $('#city_id').empty();
	            $('#city_id').append('<option value="0">Select City</option>');
	            //Populate options of the second dropdown
	            $.each( res, function(){    
	                $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>');
	            });
	            $('#city_id').focus();
	          },
	          beforeSend: function(){
	            $('#city_id').empty();
	            $('#city_id').append('<option value="0">Loading...</option>');
	          },
	          error: function(er){
	            $('#city_id').attr('disabled', true);
	            $('#city_id').empty();
	            $('#city_id').append('<option value="0">No City Found!</option>');
	          }
	      });
			}

		});
			
	</script>