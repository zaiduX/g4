<ol class="breadcrumb bc-3" >
  <li>
    <a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
  </li>
  <li class="active">
    <strong>Orders List</strong>
  </li>
</ol>
<style>
    .text-white { color: #fff; }
    .dropdown-menu { left: auto; right: 0 !important; }
</style>      
<h2 style="display: inline-block;">Laundry Orders List</h2>
<?php $per_all_orders = explode(',', $permissions[0]['all_orders']); ?>

  <?php if($this->session->flashdata('error')):  ?>
      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
      <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
  <?php endif; ?>

<table class="table table-bordered display compact table-striped" id="table-2" style="max-width=100%">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Create Date</th>
      <th class="text-center">Customer</th>
      <th class="text-center">Provider</th>
      <th class="text-center">Order Price</th>
      <th class="text-center">Currency</th>
      <th class="text-center">Country</th>
      <th class="text-center">Status</th>
      <?php if(in_array('1', $per_all_orders) || in_array('4', $per_all_orders)) { ?>
        <th class="text-center">Actions</th>
      <?php } ?>
    </tr>
  </thead>
  
  <tbody>   
    <?php foreach ($all_orders as $orders):  ?>
    <tr>
      <td class="text-center"><?=$orders['booking_id']?></td>
      <td class="text-center"><?=date('d/m/Y', strtotime($orders['cre_datetime']))?></td>
      <td class="text-center"><?=$orders['cust_name']?></td>
      <td class="text-center"><?=$orders['operator_company_name']?></td>
      <td class="text-center"><?=$orders['booking_price']?></td>
      <td class="text-center"><?=$orders['currency_sign']?></td>
      <td class="text-center"><?=$this->laundry->get_country_name_by_id($orders['country_id'])?></td>
      <td class="text-center"><?=ucwords((str_replace('_', ' ', $orders['booking_status'])))?></td>
      <?php if(in_array('1', $per_all_orders) || in_array('4', $per_all_orders)) { ?>
        <td class="text-center">
          <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-cogs"></i>
            </button>
            <ul class="dropdown-menu dropdown" role="menu">
              <?php if(in_array('1', $per_all_orders)): ?>
                <li>  <a href="<?= base_url('admin/view-laundry-order-details/') . $orders['booking_id']; ?>" data-toggle="tooltip" data-placement="left" title="Click to See Details" data-original-title="Click to See Details"> <i class="entypo-eye"></i> View </a> </li>
              <?php endif; ?>
            </ul>
          </div>
        </td>
      <?php } ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<br />

<script type="text/javascript">
  jQuery( document ).ready( function( $ ) {
    var $table2 = jQuery( '#table-2' );
    
    // Initialize DataTable
    $table2.DataTable( {
      "bStateSave": true,
      "order":[[1,"asc"]]
      
    });
    
    // Initalize Select Dropdown after DataTables is created
    $table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
      minimumResultsForSearch: -1
    });

    $table2.find( ".pagination a" ).click( function( ev ) {
      replaceCheckboxes();
    } );
  } );



</script>