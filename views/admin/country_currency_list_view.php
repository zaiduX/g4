<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>currency List</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">currency List</h2>
<?php
$per_country_currency = explode(',', $permissions[0]['country_currency']);
if(in_array('2', $per_country_currency)) { ?>
	<a type="button" href="<?= base_url('admin/add-country-currency'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
		Add New
		<i class="entypo-plus"></i>
	</a>
<?php } ?>

<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Country</th>
			<th class="text-center">Currency</th>
			<th class="text-center">Status</th>
			<th class="text-center">Create Date Time</th>
			<?php if(in_array('3', $per_country_currency) || in_array('4', $per_country_currency) || in_array('5', $per_country_currency)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($country_currency as $cc):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?= $cc['country_name'] ?></td>
			<td class="text-center"><?= $cc['currency_title'] . ' ( '. $cc['currency_sign'] . ' ) '; ?></td>
			<?php if($cc['cc_status'] == 1): ?>
				<td class="text-center">
					<label class="label label-success" ><i class="entypo-thumbs-up"></i>Active</label>
				</td>
			<?php else: ?>
				<td class="text-center">
					<label class="label label-danger" ><i class="entypo-cancel"></i>Inactive</label>
				</td>
			<?php endif; ?>
			<td class="text-center"><?= date('d / M / Y H:i a', strtotime($cc['cre_datetime'])); ?></td>
			<?php if(in_array('3', $per_country_currency) || in_array('4', $per_country_currency) || in_array('5', $per_country_currency)) { ?>
				<td class="text-center">
					<?php if(in_array('3', $per_country_currency)) { ?>
						<a href="<?= base_url('admin/edit-country-currency/') . $cc['cc_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left hidden">
							<i class="entypo-pencil"></i>
							Edit
						</a>
					<?php } ?>
					<?php if(in_array('4', $per_country_currency)) { ?>
						<?php if($cc['cc_status'] == 0): ?>
							<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to inactivate" data-original-title="Click to activate" onclick="inactivate_authority_type('<?= $cc['cc_id']; ?>');">
								<i class="entypo-thumbs-up"></i>Activate</button>
						<?php else: ?>
							<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="Click to inactivate" data-original-title="Click to inactivate" onclick="activate_authority_type('<?= $cc['cc_id']; ?>');">
							<i class="entypo-cancel"></i>
							Inactivate</button>			
						<?php endif; ?>
					<?php } ?>
				</td>
			<?php } ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );

		

	} );

	function activate_authority_type(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this currency?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('inactive-country-currency', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'currency has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'currency inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate_authority_type(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this currency?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('active-country-currency', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'currency has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'currency activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
</script>