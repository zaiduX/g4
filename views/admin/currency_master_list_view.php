<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Currency List</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">Currency List</h2>
<?php
$per_currency_master = explode(',', $permissions[0]['currency_master']);
if(in_array('2', $per_currency_master)) { ?>
<a type="button" href="<?= base_url('admin/add-currency-master'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
	Add New
	<i class="entypo-plus"></i>
</a>
<?php } ?>

<!-- ALERT -->
<?php if($this->session->flashdata('errors')):  ?>
  <div class="alert alert-danger text-center"><?= $this->session->flashdata('errors'); ?></div>
<?php endif; ?>
<?php if($this->session->flashdata('success')):  ?>
  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
<?php endif; ?>  


<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th>#</th>
			<th class="text-center">Currency Shortname</th>
			<th class="text-center">Currency Title</th>
			<th class="text-center">Status</th>
			<th class="text-center">Details</th>
			<?php if(in_array('3', $per_currency_master) || in_array('4', $per_currency_master) || in_array('5', $per_currency_master)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ($currency_master as $currency_row): static $i=0; $i++; ?>
		<tr>
			<td class="text-center">
				<?= $i ?>
			</td>
			<td class="text-center"><?= $currency_row['currency_sign'] ?></td>
			<td class="text-center"><?= $currency_row['currency_title'] ?></td>
			<td class="text-center">
				<?php if($currency_row['currency_status'] == 0): ?>
					<label class="label label-danger"><i class="entypo-cancel"></i>Inactive</label>
				<?php else: ?>
					<label class="label label-success"><i class="entypo-thumbs-up"></i>Active</label>
				<?php endif; ?>
			</td>
			<td class="text-center"><?= $currency_row['currency_details'] ?></td>
			<?php if(in_array('3', $per_currency_master) || in_array('4', $per_currency_master) || in_array('5', $per_currency_master)) { ?>
				<td class="text-center">
					<?php if(in_array('3', $per_currency_master)) { ?>
						<a href="<?= base_url('admin/edit-currency-master/') . $currency_row['currency_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
					<?php } ?>
					<?php if(in_array('4', $per_currency_master)) { ?>
						<?php if($currency_row['currency_status'] == 0): ?>
							<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title ="" data-original-title="Click to activate" onclick="inactivate('<?= $currency_row['currency_id']; ?>');">
								<i class="entypo-thumbs-up"></i>Activate</button>
						<?php else: ?>
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="" data-original-title="Click to inactivate" onclick="activate('<?= $currency_row['currency_id']; ?>');">
							<i class="entypo-cancel"></i>
							Inactivate</button>			
						<?php endif; ?>
					<?php } ?>
					<?php if(in_array('5', $per_currency_master)) { ?>
						<a href="<?= base_url('admin/delete-currency-master/') . $currency_row['currency_id']; ?>" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a>
					<?php } ?>
				</td>
			<?php } ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );
	} );

	function activate(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to inactivate this currency?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, inactivate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('currency-master/inactivate', {id: id}, function(res){
				console.log(res);
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'Currency has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Currency inactivation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

	function inactivate(id=0){
		swal({
		  title: 'Are you sure?',
		  text: "You want to to activate this currency?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!',
		  cancelButtonText: 'No, cancel!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: true,
		}).then(function () {
			$.post('currency-master/activate', {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Currency has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Currency activation failed.',
				    'error'
				  )
				}
			});
			
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}

</script>