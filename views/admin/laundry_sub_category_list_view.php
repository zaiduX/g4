<ol class="breadcrumb bc-3" >
	<li>
		<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
	</li>
	<li class="active">
		<strong>Laundry Cloth Categories</strong>
	</li>
</ol>
			
<h2 style="display: inline-block;">Laundry Cloth Categories
<?php
$per_laundry_category = explode(',', $permissions[0]['laundry_category']);

if(in_array('2', $per_laundry_category)) { ?>
	<a type="button" href="<?= base_url('admin/laundry-categories-add'); ?>" class="btn btn-green btn-icon icon-left">Add New <i class="entypo-plus"></i></a>
	<a type="button" href="<?= base_url('admin/laundry-main-categories-edit'); ?>" class="btn btn-info btn-icon icon-left">Edit Main Category <i class="fa fa-wrench"></i></a>&nbsp;
<?php } ?>
</h2>
<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Catogory Name</th>
			<th class="text-center">Sub Category Name</th>
			<th class="text-center">Dimension(H/W/L)(cm)</th>
			<th class="text-center">Weight (grms)</th>
			<th class="text-center">Status</th>
			<?php if(in_array('3', $per_laundry_category) || in_array('5', $per_laundry_category)) { ?>
				<th class="text-center">Actions</th>
			<?php } ?>
		</tr>
	</thead>
	
	<tbody>
		<?php $offset = $this->uri->segment(3,0) + 1; ?>
		<?php foreach ($categories as $cat):  ?>
		<tr>
			<td class="text-center"><?= $offset++; ?></td>
			<td class="text-center"><?php $category=$this->category->get_category_detail($cat['cat_id']); echo ucwords($category['cat_name']) ?></td>
			<td class="text-center"><?= $cat['sub_cat_name'] ?></td>
			<td class="text-center"><?= $cat['height'].' / '.$cat['width'].' / '.$cat['length'] ?></td>
			<td class="text-center"><?= $cat['weight'] ?></td>
			<td class="text-center">
				<?php if($cat['sub_cat_status'] == 0): ?>
					<label class="label label-danger"><i class="entypo-cancel"></i>Inactive</label>
				<?php else: ?>
					<label class="label label-success"><i class="entypo-thumbs-up"></i>Active</label>
				<?php endif; ?>
			</td>
			<?php if(in_array('3', $per_laundry_category) || in_array('5', $per_laundry_category)): ?>
				<td class="text-center">
					<?php if(in_array('3', $per_laundry_category)): ?>
						<a href="<?= base_url('admin/laundry-categories/edit/').$cat['sub_cat_id']; ?>" class="btn btn-primary btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
					<?php endif; ?>					
					<?php if(in_array('4', $per_laundry_category)) { ?>
						<?php if($cat['sub_cat_status'] == 0): ?>
							<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to activate" onclick="inactivate_driver_cat('<?= $cat['sub_cat_id']; ?>');">
								<i class="entypo-thumbs-up"></i>Activate</button>
						<?php else: ?>
						<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to inactivate" onclick="activate_driver_cat('<?= $cat['sub_cat_id']; ?>');">
							<i class="entypo-cancel"></i>
							Inactivate</button>			
						<?php endif; ?>
					<?php } ?>
				</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	} );
	function activate_driver_cat(id=0){
		swal({
			title: 'Are you sure?',
			text: "You want to to inactivate this category type?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, inactivate it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: true,
		}).then(function () {
			$.post("<?=base_url('laundry-categories/inactivate')?>", {id: id}, function(res){
				console.log(res);
				if(res == 'success'){
				  swal(
				    'Inactive!',
				    'Laundry category has been inactivated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Laundry category inactivation failed.',
				    'error'
				  )
				}
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
	function inactivate_driver_cat(id=0){
		swal({
			title: 'Are you sure?',
			text: "You want to to activate this driver category?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, activate it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: true,
		}).then(function () {
			$.post("<?=base_url('admin/laundry-categories/activate')?>", {id: id}, function(res){
				if(res == 'success'){
				  swal(
				    'Active!',
				    'Laundry category has been activated.',
				    'success'
				  ). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
				    'Failed!',
				    'Laundry category activation failed.',
				    'error'
				  )
				}
			});
		}, function (dismiss) {  if (dismiss === 'cancel') {}	});
	}
</script>