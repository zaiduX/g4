<ol class="breadcrumb bc-3" >
	<li><a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	<li><a href="<?= base_url('admin/transport-vehicles'); ?>"><i class="fa fa-home"></i>Transport Vehicles</a></li>
	<li class="active"><strong>Vehicles Seat Types</strong></li>
</ol>
			
<div class="row">
  	<div class="col-md-12">
    	<div class="panel panel-dark" data-collapsed="0">
      		<div class="panel-heading">
        		<div class="panel-title">Add New Seat Type</div>
        		<div class="panel-options"><a href="<?= base_url('admin/transport-vehicles'); ?>" style="float: right;"><i class="entypo-back"></i> Back</a></div>
      		</div>
      
      		<div class="panel-body">
        		<?php if($this->session->flashdata('error')):  ?>
              		<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
          		<?php endif; ?>
          		<?php if($this->session->flashdata('success')):  ?>
              		<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
          		<?php endif; ?>
        
        		<form role="form" action="<?= base_url('admin/transport-vehicles/register-seat-type'); ?>" class="form-horizontal form-groups-bordered" method="post" autocomplete="off">
        			<input type="hidden" name="vehical_type_id" value="<?=$vehical_type_id?>">
          			<div class="form-group">
            			<label for="st_name" class="col-sm-3 control-label">Seat Type :</label>
            			<div class="col-sm-5">
              				<input type="text" class="form-control" id="st_name" placeholder="Enter Seat Type..." name="st_name" required="required" />  
              				<span id="error_vehicle_type"></span>
            			</div>
            			<div class="col-sm-2">
              				<button type="submit" class="btn btn-blue"><i class="fa fa-plus"></i> Add</button>
            			</div>
          			</div>
        		</form>
      		</div>
    	</div>
  	</div>
</div>

<h2 style="display: inline-block;">Vehicles Seat Types</h2>
<table class="table table-bordered table-striped datatable" id="table-2">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center">Seat type</th>
			<th class="text-center">Actions</th>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ($seats as $seat): static $i=1; ?>
		<tr>
			<td class="text-center"><?= $i++; ?></td>
			<td class="text-center"><?= ucfirst($seat['st_name']); ?></td>
			<td class="text-center">
				<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click to delete" onclick="delete_vehicle('<?= $seat["st_id"]; ?>');"> <i class="entypo-cancel"></i> Delete </button>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br />

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var $table2 = jQuery( '#table-2' );
		
		// Initialize DataTable
		$table2.DataTable( {
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true
		});
		
		// Initalize Select Dropdown after DataTables is created
		$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
			minimumResultsForSearch: -1
		});

		// Highlighted rows
		$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
			var $this = $(el),
				$p = $this.closest('tr');
			
			$( el ).on( 'change', function() {
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
			} );
		} );
		
		// Replace Checboxes
		$table2.find( ".pagination a" ).click( function( ev ) {
			replaceCheckboxes();
		} );		
	} );

	function delete_vehicle(id=0){		
		swal({
		  	title: 'Are you sure?',
		  	text: "You want to to delete this Seat type?",
		  	type: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Yes, delete it!',
		  	cancelButtonText: 'No, cancel!',
		  	confirmButtonClass: 'btn btn-success',
		  	cancelButtonClass: 'btn btn-danger',
		  	buttonsStyling: true,
		}).then(function () {
			$.post("<?=base_url('admin/transport-vehicles/delete-seat-type')?>", {id: id}, function(res){ 
				if(res == 'success') { 
				  	swal(
					    'Deleted!',
					    'Seat type has been deleted.',
					    'success'
					). then(function(){ 	window.location.reload();  });
				}
				else {
					swal(
					    'Failed!',
					    'Seat type deletion failed.',
					    'error'
				  	)
				}
			});	
		}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
	}
	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
	    $(".alert").slideUp(500);
	});
</script>