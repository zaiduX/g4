		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li class="active">
				<strong>Categories</strong>
			</li>
		</ol>
		
		<h2 style="display: inline-block;">Categories</h2>
		<?php
		$per_categories = explode(',', $permissions[0]['categories']);
		if(in_array('2', $per_categories)) { ?>	
			<a type="button" href="<?= base_url('admin/add-category'); ?>" class="btn btn-green btn-icon icon-left" style="float: right;">
				Add New
				<i class="entypo-plus"></i>
			</a>
		<?php } ?>
	
		
		<table class="table table-bordered table-striped datatable" id="table-2">
			<thead>
				<tr>
					<th class="text-center">ID</th>
					<th class="text-center">Category Name</th>
					<th class="text-center">Category Type</th>
					<th class="text-center">Status</th>
					<th class="text-center">Create Date Time</th>
					<?php if(in_array('3', $per_categories) || in_array('4', $per_categories) || in_array('5', $per_categories)) { ?>
						<th class="text-center">Actions</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>

				<?php $offset = $this->uri->segment(3,0) + 1; ?>
				<?php foreach ($categories as $cat) {  ?>
				<tr>
					<td class="text-center"><?= $cat['cat_id']; ?></td>
					<td class="text-center"><?= $cat['cat_name'] ?></td>
					<td class="text-center"><?= $cat['cat_type'] ?></td>
					<?php if($cat['cat_status'] == 1): ?>
						<td class="text-center">
							<label class="label label-success" ><i class="entypo-thumbs-up"></i>Active</label>
						</td>
					<?php else: ?>
						<td class="text-center">
							<label class="label label-danger" ><i class="entypo-cancel"></i>Inactive</label>
						</td>
					<?php endif; ?>
					<td class="text-center"><?= date('d/M/Y', strtotime($cat['cre_datetime'])); ?></td>
					<?php if(in_array('3', $per_categories) || in_array('4', $per_categories) || in_array('5', $per_categories)) { ?>
						<td class="text-center">
							<?php if(in_array('3', $per_categories)) { ?>
								<form action="<?= base_url('admin/edit-category'); ?>" method="post" class="col-sm-5">
									<input type="hidden" value="<?= $cat['cat_id']; ?>" id="cat_id" name="cat_id" />
									<button class="btn btn-primary btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to edit the category" data-original-title="Click to activate">
										<i class="entypo-pencil"></i>Edit</button>							
								</form>
							<?php } ?>
							<?php if(in_array('4', $per_categories)) { ?>
								<?php if($cat['cat_status'] == 0): ?>
									<button class="btn btn-success btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to activate" onclick="activate_category('<?= $cat['cat_id']; ?>');">
										<i class="entypo-thumbs-up"></i>Activate</button>
								<?php else: ?>
									<button class="btn btn-danger btn-sm btn-icon icon-left" data-toggle="tooltip" data-placement="right" title="Click to inactivate" data-original-title="Click to inactivate" onclick="inactivate_category('<?= $cat['cat_id']; ?>');">
									<i class="entypo-cancel"></i>
									Inactivate</button>			
								<?php endif; ?>
							<?php } ?>
						</td>
					<?php } ?>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
		<br />


		<script type="text/javascript">
			jQuery( document ).ready( function( $ ) {
				var $table2 = jQuery( '#table-2' );
				
				// Initialize DataTable
				$table2.DataTable( {
                                        "order":[[0,'desc']],

				});
				
				// Initalize Select Dropdown after DataTables is created
				$table2.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
					minimumResultsForSearch: -1
				});

				// Highlighted rows
				$table2.find( "tbody input[type=checkbox]" ).each(function(i, el) {
					var $this = $(el),
						$p = $this.closest('tr');
					
					$( el ).on( 'change', function() {
						var is_checked = $this.is(':checked');
						
						$p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
					} );
				} );
				
				// Replace Checboxes
				$table2.find( ".pagination a" ).click( function( ev ) {
					replaceCheckboxes();
				} );
			} );

			function inactivate_category(id=0){
				swal({
				  title: 'Are you sure?',
				  text: "You want to to inactivate this category?",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, inactivate it!',
				  cancelButtonText: 'No, cancel!',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  buttonsStyling: true,
				}).then(function () {
					$.post('inactive-category', {id: id}, function(res){
						if(res == 'success'){
						  swal(
						    'Inactive!',
						    'Category has been inactivated.',
						    'success'
						  ). then(function(){ 	window.location.reload();  });
						}
						else {
							swal(
						    'Failed!',
						    'Category inactivation failed.',
						    'error'
						  )
						}
					});
					
				}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
			}

			function activate_category(id=0){
				swal({
				  title: 'Are you sure?',
				  text: "You want to to activate this category?",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, activate it!',
				  cancelButtonText: 'No, cancel!',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  buttonsStyling: true,
				}).then(function () {
					$.post('active-category', {id: id}, function(res){
						if(res == 'success'){
						  swal(
						    'Active!',
						    'Category has been activated.',
						    'success'
						  ). then(function(){ 	window.location.reload();  });
						}
						else {
							swal(
						    'Failed!',
						    'Category activation failed.',
						    'error'
						  )
						}
					});
					
				}, function (dismiss) {  if (dismiss === 'cancel') {  }	});
			}
		</script>