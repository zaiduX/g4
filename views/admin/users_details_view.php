<style type="text/css">
.profile-env section.profile-feed .profile-post-form .form-options {padding-bottom: 0px; }
</style>
		<ol class="breadcrumb bc-3" >
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>"><i class="fa fa-home"></i>Dashboard</a>
			</li>
			<li>
				<a href="<?= base_url('admin/verify-documents'); ?>">Documents List</a>
			</li>
			<li class="active">
				<strong>View</strong>
			</li>
		</ol>
	
		<div class="profile-env">
			
			<header class="row">
				
				<div class="col-sm-2">
					<?php if($users_detail['avatar_url'] != "NULL"): ?>
						<a class="profile-picture">
							<img src="<?= $this->config->item('base_url') . $users_detail['avatar_url'];?>" class="img-responsive img-circle" />
						</a>
					<?php else: ?>
						<a class="profile-picture">
							<img src="<?= $this->config->item('resource_url') . 'default-profile.jpg';?>" class="img-responsive img-circle" />
						</a>
					<?php endif; ?>
					
				</div>
				
				<div class="col-sm-10">
					
					<ul class="profile-info-sections">
						<li>
							<div class="profile-name">
								<strong> <?php $email_cut = explode('@', $users_detail['email1']);  $name = $email_cut[0]; ?>
									<a ><?= $users_detail['firstname'] != "NULL" ? $users_detail['firstname'] . ' ' . $users_detail['lastname'] : ($users_detail['company_name'] != "NULL" ? ucwords($users_detail['company_name']) : ucwords($name)) ?></a>
									<a class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a></strong>
									<span>
										<?php if( strtolower($users_detail['gender']) === 'm'): ?>
											<a><i class="fa fa-male"></i> Male</a>
										<?php else: ?>
											<a><i class="fa fa-female"></i> Female</a>
										<?php endif; ?>
									</span>
							</div>
						</li>
						
						<li>
							<div class="profile-stat">
								<h6><i class="fa fa-envelope-o"></i> Primary Email Address</h6>
								<h4><?php if($users_detail['email1'] !== "NULL") echo $users_detail['email1']; else echo 'Not provided!';  ?></h4>
							</div>
						</li>
						
						<li>
							<div class="profile-stat">
								<h6><i class="fa fa-phone"></i> Primary Mobile Number</h6>
								<h4><?php if($users_detail['mobile1'] !== "NULL") echo '+'.$users_country['country_phonecode'].'-'.$users_detail['mobile1']; else echo 'Not provided!';  ?></h4>
							</div>
						</li>
					</ul>
					
				</div>
				
			</header>
			
			<section class="profile-info-tabs">
				
				<div class="row">
					
					<div class="col-sm-offset-2 col-sm-10">
						
						<ul class="user-details">
							<li>
								<a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Address">
									<i class="entypo-location"></i>
									<?php if($users_detail['city_id'] !== "NULL") echo $users_city['city_name']; else echo ' ';  ?>
									<?php if($users_detail['state_id'] !== "NULL") echo $users_state['state_name']; else echo ' ';  ?>
									<?php if($users_detail['country_id'] !== "NULL") echo $users_country['country_name']; else echo ' ';  ?>
								</a>
							</li>
							<?php if($users_detail['profession'] !== "NULL"): ?>
								<li><a><i class="entypo-suitcase"></i>Works as - <span> <?php echo $users_detail['profession']; ?></span></a></li>
							<?php endif;?>
							<li>
								<a class="tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Date of Birth">
									<i class="fa fa-birthday-cake"></i>
									<?php if($users_detail['cust_dob'] !== "NULL") echo date('l, d M Y', strtotime($users_detail['cust_dob'])); else echo 'Not provided!'; ?>
								</a>
							</li>
						</ul>
						
						
						<!-- tabs for the profile links -->
						<ul class="nav nav-tabs">
							<li class="active tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Send Notification">
								<a data-toggle="tab" href="#notification">Send Notification</a>
							</li>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Personal Details">
								<a data-toggle="tab" href="#personal">Personal Details</a>
							</li>
							<?php if($users_detail['user_type'] == 0): ?>
								<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Business Details">
								<a data-toggle="tab" href="#business">Business Details</a>
								</li>
							<?php endif; ?>
							<li class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="See Skills">
								<a data-toggle="tab" href="#skills">Skills</a>
							</li>
								
						</ul>
						
					</div>
					
				</div>
				
			</section>
			
			<div class="tab-content">
				<div id="notification" class="tab-pane fade in active">
					<section class="profile-feed">

						<?php if($this->session->flashdata('error')): ?>
				      	<div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
					    <?php elseif($this->session->flashdata('success')):  ?>
					    	<div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
					    <?php endif; ?>

						<form action="<?= base_url('admin/send-personal-notification'); ?>" class="profile-post-form" method="post" autocomplete="off" enctype="multipart/form-data" id="send_notification">
							<input type="hidden" name="user_id" value="<?= $users_detail['cust_id']; ?>">
							<input type="text" class="form-control" id="title" placeholder="Enter notification title..." name="title" autofocus style="resize: none;" rows="7" />
							<textarea class="form-control autogrow" placeholder="Enter notification message..." name="message" id="message"></textarea>
							<div class="form-options">
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<span class="btn btn-info btn-file">
										<span class="fileinput-new "><i class="entypo-attach"></i> Attachment</span>
										<span class="fileinput-exists"><i class="entypo-cancel"></i> Change</span>
										<input type="file" name="attachement">
									</span>
									<span class="fileinput-filename"></span>
									<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
								</div>
								<div class="post-submit">
									<button type="submit" class="btn btn-primary tooltip-primary" data-toggle="tooltip" data-placement="left" title="" data-original-title="Click to send Notification"><i class="fa fa-send"></i>&nbsp; Send Notification</button>
								</div>
							</div>
						</form>
					</section>
				</div>
				
				<div id="personal" class="tab-pane fade">
					<section class="profile-feed col-md-10 col-md-offset-1">
						<div class="profile-stories">
					
							<article class="story">								
								<div class="story-content">									
									<div class="col-md-4"><i class="fa fa-user"></i> Profession: </div>
									<div class="col-md-4"><?= ($users_detail['profession'] !="NULL") ? strtolower($users_detail['profession']):"Not Provided!"; ?></div>
									<hr>
								</div>
								<div class="story-content">									
									<div class="col-md-4"><i class="fa fa-envelope-o"></i> Email Address: </div>
									<div class="col-md-4">
										<?= $users_detail['email1'].' ( Primary )'; ?>
										<?php 
											if($users_detail['email_verified'] == 1) {	echo '<i class="fa fa-check  text-success tooltip-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email Address Verified!"></i>'; }
											else { echo '<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email Address Not Verified!" style="color:#FF0000;"></i>'; } 
										?>
										</div>
									<hr>
								</div>
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-phone"></i> Phone Number: </div>
									<div class="col-md-4">
										<?= $users_detail['mobile1'].' ( Primary )'; ?>
										<?php 
											if($users_detail['mobile_verified'] == 1) {	echo '<i class="fa fa-check  text-success tooltip-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mobile Number Verified!"></i>'; }
											else { echo '<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mobile Number Not Verified!" style="color:#FF0000;"></i>'; } 
										?>
										</div>
									<hr>
								</div>								
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-map-marker"></i> Address: </div>
									<div class="col-md-8">
										<?= ($users_city['city_name'] !="NULL") ? trim($users_city['city_name']).' ':""; ?>
										<?= ($users_state['state_name'] !="NULL") ? trim($users_state['state_name']).' ':""; ?>
										<?= ($users_country['country_name'] !="NULL") ? trim($users_country['country_name']):""; ?>
									</div>
									<br/>

									<hr>								
								</div>
								
							</article>
							
						</div>					
					</section>
				</div>

				<div id="business" class="tab-pane fade">
					<section class="profile-feed col-md-10 col-md-offset-1">
						<div class="profile-stories">
					
							<article class="story">																
								<div class="story-content">									
									<div class="col-md-4"><i class="fa fa-envelope-o"></i> Business Email Address: </div>
									<div class="col-md-4">
										<?= $users_detail['email1'].' ( Primary )'; ?>
										<?php 
											if($users_detail['email_verified'] == 1) {	echo '<i class="fa fa-check  text-success tooltip-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email Address Verified!"></i>'; }
											else { echo '<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email Address Not Verified!" style="color:#FF0000;"></i>'; } 
										?>
										</div>
									<hr>
								</div>
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-phone"></i> Business Phone Number: </div>
									<div class="col-md-4">
										<?= $users_detail['mobile1'].' ( Primary )'; ?>
										<?php 
											if($users_detail['mobile_verified'] == 1) {	echo '<i class="fa fa-check  text-success tooltip-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mobile Number Verified!"></i>'; }
											else { echo '<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mobile Number Not Verified!" style="color:#FF0000;"></i>'; } 
										?>
										</div>
									<div class="col-md-4"><?= ($users_detail['mobile2'] !="NULL")? $users_detail['mobile1']:""; ?></div>
									<hr>
								</div>
								<div class="story-content">
									<div class="col-md-4"><i class="fa fa-building"></i> About Business: </div>
									<div class="col-md-4"><?= ($users_detail['about'] !="NULL") ? nl2br($users_detail['about']):"Not Provided!"; ?></div>
									<hr>
								</div>									
								<div class="story-content">
									<div class="col-md-12"><i class="fa fa-map-marker"></i> Business Address: </div>
									<div class="col-md-2">&nbsp;&nbsp;&nbsp;Line 1: </div>
									<div class="col-md-10"><?= ($users_detail['addr_line1'] !="NULL") ? trim($users_detail['addr_line1']):"Not Provided!"; ?></div>
									<br/>
									<div class="col-md-2">&nbsp;&nbsp;&nbsp;Line 2: </div>
									<div class="col-md-10"><?= ($users_detail['addr_line1'] !="NULL") ? trim($users_detail['addr_line1']):"Not Provided!"; ?></div>
									<div class="col-md-10 col-md-offset-2">
										<?= ($users_city['city_name'] !="NULL") ? trim($users_city['city_name'])." ":" "; ?>
										<?= ($users_state['state_name'] !="NULL") ? trim($users_state['state_name'])." ":" "; ?>
										<?= ($users_country['country_name'] !="NULL") ? trim($users_country['country_name']):""; ?>
									</div>
									<br/>

									<hr>								
								</div>
								
							</article>
							
						</div>					
					</section>
				</div>

				<div id="skills" class="tab-pane fade">
					<section class="profile-feed">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Skill Level - Expert</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="padding-lg">
										<?php 
											$expert_skills = explode(',', $users_skill_list['expert_skills']); 
											foreach ($expert_skills as $expert) { if($expert != "NULL") { ?>
												<div class="label label-primary"><?= $expert ?></div>
										<?php } else { ?> <div class="label label-primary">No skill found!</div> <?php } } ?>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Skill Level - Senior</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="padding-lg">
										<?php 
											$senior_skills = explode(',', $users_skill_list['senior_skills']); 
											foreach ($senior_skills as $senior) { if($senior != "NULL") { ?>
												<div class="label label-primary"><?= $senior ?></div>
										<?php } else { ?> <div class="label label-primary">No skill found!</div> <?php } } ?>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Skill Level - Confirmed</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="padding-lg">
										<?php 
											$confirmed_skills = explode(',', $users_skill_list['confirmed_skills']); 
											foreach ($confirmed_skills as $confirmed) { if($confirmed != "NULL") { ?>
												<div class="label label-primary"><?= $confirmed ?></div>
										<?php } else { ?> <div class="label label-primary">No skill found!</div> <?php } } ?>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Skill Level - Junior</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="padding-lg">
										<?php 
											$junior_skills = explode(',', $users_skill_list['junior_skills']); 
											foreach ($junior_skills as $primary) { if($primary != "NULL") { ?>
												<div class="label label-primary"><?= $primary ?></div>
										<?php } else { ?> <div class="label label-primary">No skill found!</div> <?php } } ?>
									</td>
								</tr>
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
		
		<br />