<style>
  .select2-container .select2-selection--single {
    height: 38px;
    user-select: none;
    -webkit-user-select: none;
    border: 2px solid #ebebeb;
  }

  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 1.333333;
    padding: 10px 6px;
  }

  .select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 26px;
    position: absolute;
    top: 6px;
    right: 1px;
    width: 20px;
  }
  label{ font-weight: 10 !important; }
  #depart_from { background-color: #FFFFFF !important; }
</style>
<style>
  input[type=checkbox] { display: none; }
  input[type=checkbox] + label {
    position: relative;
    height: 30px;
    width: 38px;
    display: block;
    transition: box-shadow 0.2s, border 0.2s;
    box-shadow: 0 0 1px #FFF;/* Soften the jagged edge */
    cursor: pointer;
  }
  input[type=checkbox] + label:hover,
  input[type=checkbox]:checked + label {
    border: solid 2px #1b5497;
    box-shadow: 0 0 1px #1b5497;
    border-radius: 5px;
  }
  input[type=checkbox]:checked + label:after {
    /* content: '\2714'; */
    /*content is required, though it can be empty - content: '';*/
    height: 1em;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    margin: auto;
    color: green;
    line-height: 1;
    font-size: 18px;
    text-align: center;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    /* background-color: white;
    border: 3px outset #0AD909 !important; */
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    /* background-color: white;
    border: 3px outset #0AD909 !important; */
  }
  .datepicker { background-color:white !important; }
</style>
<!-- <section class="p-t-10 m-t-0 background-grey" style="background: url(<?= $this->config->item('resource_url') . 'web/front_site/images/bg.jpg'; ?>);"> -->
<section class="p-t-10 m-t-0 background-grey">
  <div class="container">
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="hpanel">
          <div class="panel-body" style="margin-left: -15px; margin-right: -15px; padding-bottom: 0px; padding-top: 0px; margin-top: 10px; background-color: #FFF; border-radius: 10px; -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5); -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5); box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5); margin-bottom: 5px;">
            <?php if($this->session->flashdata('error')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            <?php if($this->session->flashdata('success')):  ?>
              <div class="row">
                <div class="form-group"> 
                  <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
                </div>
              </div>
            <?php endif; ?>
            <?php $opr_dtls=array(); ?>
            <?php $opr_dtls1=array(); ?>
            <?php foreach ($trip_list as $tList) { 
              $customer_details = $this->api_bus->get_user_details($tList['cust_id']);
              //echo json_encode($customer_details);
              $operator_details = $this->api_bus->get_bus_operator_profile($tList['cust_id']);
              //echo json_encode($operator_details);
              array_push($opr_dtls, $operator_details);
              $bus_details = $this->api_bus->get_operator_bus_details($tList['bus_id']);
              $p_and_d = $this->api_bus->get_trip_location_master($tList['trip_id']);
              $src = explode(',',$p_and_d[0]['src_point_ids']);
              $dest = explode(',',$p_and_d[0]['dest_point_ids']);
            } 
            $unique_types = array_unique(array_map(function($opr_dtls){return $opr_dtls['cust_id'];}, $opr_dtls));
            //echo json_encode($unique_types);
            foreach ($unique_types as $deliverer) {
              //echo $deliverer;
              $operator_details = $this->api_bus->get_bus_operator_profile($deliverer);
              array_push($opr_dtls1, $operator_details);
            }
            //echo json_encode($opr_dtls1);
            ?>

            <div class="row" id="basicSearch" style="<?= $filter == 'advance' ? 'display:none' : '' ?>;">
              <form action="<?= base_url('ticket-search') ?>" method="GET" id="frmBasicSearch"> 
                <div class="row form-group">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                      <label class="text-left"><i class="fa fa-arrow-up"></i> <?= $this->lang->line('From'); ?></label>
                      <select class="form-control select2" name="trip_source" id="trip_source" style="box-shadow: 0 0 5px #3498db;">
                        <option value=""><?= $this->lang->line('Starting city'); ?></option>
                        <?php foreach ($trip_sources as $source) { ?>
                          <option value="<?= $source['trip_source'] ?>" <?=($trip_source==$source['trip_source'])?'selected':''?> ><?= $source['trip_source'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                      <label class="text-left"><i class="fa fa-arrow-down"></i> <?= $this->lang->line('To'); ?></label>
                      <select class="form-control select2" name="trip_destination" id="trip_destination" style="box-shadow: 0 0 5px #3498db;">
                        <option value=""><?= $this->lang->line('Destination city'); ?></option>
                        <?php foreach ($trip_destinations as $destination) { ?>
                          <option value="<?= $destination['trip_destination'] ?>" <?=($trip_destination==$destination['trip_destination'])?'selected':''?> ><?= $destination['trip_destination'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                      <label class="text-left"><i class="fa fa-users"></i> <?= $this->lang->line('Passengers'); ?></label>
                      <div class="input-group">
                        <input type="number" class="form-control" id="no_of_seat" name="no_of_seat" value="<?=$no_of_seat?>" autocomplete="none" min="1" max="10" />
                        <input type="hidden" id="place" name="place" value="result_seller" />
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-user"></span>
                        </div>
                      </div> 
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                      <label class="text-left"><i class="fa fa-calendar"></i> <?= $this->lang->line('Departure'); ?></label>
                      <div class="input-group date">
                        <input type="text" class="form-control" id="journey_date" name="journey_date" autocomplete="off" value="<?=$journey_date?>" style="background-color: #FFF">
                        <div class="input-group-addon jdAddon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                      <label class="text-left"><i class="fa fa-calendar"></i> <?= $this->lang->line('Return'); ?> <small>(<?= $this->lang->line('Optional'); ?>)</small></label>
                      <div class="input-group date">
                        <input type="text" class="form-control" id="return_date" name="return_date" autocomplete="off" style="background-color: #FFF" value="<?=$return_date?>">
                        <div class="input-group-addon rdAddon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="display: grid;">
                      <button type="submit" class="btn btn-info" id="basicFilter" style="margin-top: 25px;"><i class="fa fa-search"></i> <?= $this->lang->line('search'); ?></button>
                    </div>
                  </div>
                </div>
                <script>
                  $(window).load(function() {
                    $('#journey_date').datepicker('setStartDate', new Date());
                    $('#return_date').datepicker('setStartDate', new Date());
                  });
                  $( ".jdAddon" ).click(function( e ) { 
                    $("#journey_date").focus();
                  });
                  $( ".rdAddon" ).click(function( e ) { 
                    $("#return_date").focus();
                  });
                  $(document).ready(function() {
                    $("#journey_date").datepicker({
                      autoclose: true,
                    }).on('changeDate', function (selected) {
                      var minDate = new Date(selected.date.valueOf());
                      $('#return_date').datepicker('setStartDate', minDate);
                      $('#return_date').val($("#journey_date").val());
                    });
                    $("#return_date").datepicker({
                      autoclose: true,
                    }).on('changeDate', function (selected) {
                      var minDate = new Date(selected.date.valueOf());
                      $('#journey_date').datepicker('setEndDate', minDate);
                    });
                  });
                </script>
              </form>
            </div>
            
            <div class="row" style="margin-top: -15px">
              <div class="col-xl-2 col-xl-offset-10 col-lg-2 col-lg-offset-10 col-md-2 col-md-offset-10 col-sm-2 col-sm-offset-8 text-right">
                <a class="btn btn-link btn-block advanceBtn" id="advanceBtn"><i class="fa fa-filter"></i> <?= $filter == 'advance' ? $this->lang->line('basic_search') : $this->lang->line('Show Filters') ?></a>
              </div>
            </div>
            <form action="<?= base_url('ticket-search-filter') ?>" method="get" id="frmBasicSearch_filter">
              <input type="hidden" name="trip_source_filter" value="<?=$trip_source?>">
              <input type="hidden" name="trip_destination_filter" value="<?=$trip_destination?>" />
              <input type="hidden" name="journey_date_filter" value="<?=$journey_date?>">
              <input type="hidden" name="return_date_filter" value="<?=$return_date?>">
              <input type="hidden" name="no_of_seat" value="<?=$no_of_seat?>">

              <div class="row form-group" id="advanceSearch" style="<?= ($filter == 'basic') ? 'display:none' : '' ?>; margin-bottom: 10px;"> 
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                  <div class="col-md-2">
                    <label class="text-left"><i class="fa fa-clock-o"></i> <?= $this->lang->line('departure'); ?></label>
                    <div class="input-group clockpicker" data-autoclose="true">
                      <input type="text" class="form-control" id="depart_from" name="depart_from" value="<?php if(isset($depart_from)) { echo $depart_from; } ?>" readonly />
                      <span class="input-group-addon">
                        <span class="fa fa-clock-o"></span>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="text-left"><i class="fa fa-bed"></i> <?= $this->lang->line('Select seat type'); ?></label>
                    <select class="js-source-states" style="width: 100%" name="bus_seat_type" id="bus_seat_type">
                      <option value=""><?= $this->lang->line('Seat Type'); ?></option>
                      <option value="SEATER" <?php if(isset($bus_seat_type)){if($bus_seat_type=="SEATER")echo "selected";}?>>SEATER</option>
                      <option value="SEMI-SLEEPER" <?php if(isset($bus_seat_type)){if($bus_seat_type=="SEMI-SLEEPER")echo "selected";}?>>SEMI-SLEEPER</option>
                      <option value="SLEEPER" <?php if(isset($bus_seat_type)){if($bus_seat_type=="SLEEPER")echo "selected";}?>>SLEEPER</option>
                    </select>
                  </div>
                  <div class="col-md-2">
                    <label class="text-left"><i class="fa fa-snowflake-o"></i> <?= $this->lang->line('Select AC Type'); ?></label>
                    <select class="js-source-states" style="width: 100%" name="bus_ac" id="bus_ac">
                      <option value=""><?= $this->lang->line('bus_ac_type'); ?></option>
                      <option value="AC" <?php if(isset($bus_ac)){if($bus_ac=="AC")echo "selected";}?>>AC</option>
                      <option value="NON AC" <?php if(isset($bus_ac)){if($bus_ac=="NON AC")echo "selected";}?>>NON AC</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label class="text-left"><i class="fa fa-money"></i> <?= $this->lang->line('Ticket Price Range'); ?></label>
                    <div class="input-group" id="ticket_rate">
                      <input type="number" class="input-sm form-control" name="ticket_to" value="<?php if(isset($ticket_to))echo $ticket_to;?>" min="1" />
                      <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                      <input type="number" class="input-sm form-control" name="ticket_from" value="<?php if(isset($ticket_from))echo $ticket_from;?>" min="1" />
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label class="text-left"><i class="fa fa-bus"></i> <?= $this->lang->line('Vehicle Type'); ?></label>
                    <select class="js-source-states" style="width: 100%" name="vehical_type_id" id="vehical_type_id">
                      <option value=""><?= $this->lang->line('Select Vehicle Type'); ?></option>
                      <option value="50" <?php if(isset($vehical_type_id)){if($vehical_type_id=="50")echo "selected";}?>><?= $this->lang->line('Bus'); ?></option>
                      <option value="51" <?php if(isset($vehical_type_id)){if($vehical_type_id=="51")echo "selected";}?>><?= $this->lang->line('Boat'); ?></option>
                      <option value="53" <?php if(isset($vehical_type_id)){if($vehical_type_id=="53")echo "selected";}?>><?= $this->lang->line('Train'); ?></option>
                    </select>
                  </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                  <div class="col-md-4">
                    <label class="text-left"><i class="fa fa-feed"></i> <?= $this->lang->line('bus_amenities'); ?></label>
                    <select class="js-source-states"  style="width: 100%; " name="bus_amenities[]" id="bus_amenities" multiple="multiple">
                    <?php if(isset($bus_amen)){foreach ($bus_amen as $b_a){echo '<option value="'.$b_a.'" selected>'.$b_a.'</option>';}}foreach($trip_list as $tList){$bus_details = $this->api_bus->get_operator_bus_details($tList['bus_id']);?><?php $bus_amenities = explode(',', $bus_details['bus_amenities']);foreach ($bus_amenities as $bus){?>
                    <option value="<?=$bus?>"> <?= $bus ?></option><?php }} ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <input type="hidden" name="trip_id" id="trip_id" value=""/>
                    <label class="text-left"><i class="fa fa-users"></i> <?= $this->lang->line('Select Operator'); ?></label><br />
                    <select id="operator" name="operator" class="js-source-states" style="width: 100%">
                      <option value="0"><?= $this->lang->line('Select Operator');?></option>
                      <?php 
                      foreach ($opr_dtls1 as $opt): ?>
                        <option value="<?=$opt['cust_id']?>" <?=(isset($operator) && $operator == $opt['cust_id'])?'selected':''?>><?= $opt['company_name']?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-md-4" style="margin-top: 25px;">
                    <span style="display: flex;">
                      <input type="submit" class="btn btn-success" value="<?= $this->lang->line('apply_filter'); ?>">
                        <script>
                          $( document ).ready(function() {
                            $(".clockpicker").clockpicker({ autoclose: true });
                          });
                        </script>
                      </form>&nbsp;&nbsp;&nbsp;
                      <form action="<?= base_url('ticket-search') ?>" method="get" id="frmBasicSearch_clear">
                        <input type="hidden" name="trip_source" value="<?=$trip_source?>">
                        <input type="hidden" name="trip_destination" value="<?=$trip_destination?>" />
                        <input type="hidden" name="journey_date" value="<?=$journey_date?>">
                        <input type="hidden" name="return_date" value="<?=$return_date?>">
                        <input type="hidden" name="no_of_seat" value="<?=$no_of_seat?>">
                        <input type="submit" class="btn btn-info" value="<?= $this->lang->line('reset'); ?>">
                      </form>
                    </span>
                  </div>
                </div>
              </div>
            </div> 
          </div>

          <form action="<?= base_url('ticket-booking-process') ?>" method="GET" id="frmBooking">
            <ul class="nav nav-tabs">
              <li class="active text-center" style="width:<?php if(isset($trip_list_return) && !empty($trip_list_return)) { echo '50'; } else { echo '100'; }?>%;"><a data-toggle="tab" href="#ownwardTab" id="hometab" class="btn btn-info aaa active"><?= $this->lang->line('Onward Trips'); ?></a></li>
              <?php if(isset($trip_list_return) && !empty($trip_list_return)) { ?>
                <li class="text-center" style="width:50%;"><a data-toggle="tab" href="#menu1"><?= $this->lang->line('Return Trips'); ?></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <div id="ownwardTab" class="tab-pane fade in active">
                <input type="hidden" name="journey_date" value="<?=$journey_date?>">
                <input type="hidden" name="return_date" value="<?=$return_date?>">
                <div class="row">
                  <table id="orderTableData" class="table">
                    <thead><tr class="hidden"><th class="hidden">ID</th><th></th></tr></thead>
                    <tbody>
                      <?php foreach ($trip_list as $tList) { 
                        $customer_details = $this->api_bus->get_user_details($tList['cust_id']);
                        //echo json_encode($customer_details);
                        $operator_details = $this->api_bus->get_bus_operator_profile($tList['cust_id']);
                        //echo json_encode($operator_details);
                        $bus_details = $this->api_bus->get_operator_bus_details($tList['bus_id']);
                        //echo json_encode($bus_details);
                        $trip_master_details = $this->api_bus->get_trip_master_details($tList['trip_id']);
                        //$seat_type_price = explode(',', $trip_master_details[0]['seat_type_price']);
                        if( $journey_date >= $trip_master_details[0]['special_rate_start_date'] && $journey_date <= $trip_master_details[0]['special_rate_end_date']) {
                          $seat_type_price = explode(',', $trip_master_details[0]['special_price']);
                        } else { $seat_type_price = explode(',', $trip_master_details[0]['seat_type_price']);  }

                        //get minimum seat price
                        if($bus_details['vip_seat_nos'] == 'NULL') {
                          $seat_type_array = explode(',', $trip_master_details[0]['seat_type']); //VIP,General,Economic
                          $seat_type_price_array = explode(',', $trip_master_details[0]['seat_type_price']); //10,20,20
                          $s_prices = array();
                          for($i=0; $i < sizeof($seat_type_array); $i++) {
                            if($seat_type_array[$i] != 'VIP') { array_push($s_prices, $seat_type_price_array[$i]); }
                          }
                          $seat_min_price = min($s_prices);
                        } else { $seat_min_price = min($seat_type_price); }

                        if($tList[0]=="safe") { 
                          //Available seat count and filter not available seats.
                          $total_seats = 0;
                          $seat_details = $this->api_bus->get_operator_bus_seat_details($tList['bus_id']);
                          for ($i=0; $i < sizeof($seat_details); $i++) { 
                            if($seat_details[$i]['type_of_seat'] == 'VIP') {
                              $vip_booked_seat_nos = $this->api_bus->get_trip_booked_vip_seat_details($tList['trip_id'], $seat_details[$i]['type_of_seat'], $journey_date);
                            } 
                            if(!$booked_seat_count = $this->api_bus->get_trip_booked_seat_count_details($tList['trip_id'], $seat_details[$i]['type_of_seat'], $journey_date)) $booked_seat_count = 0;
                            $total_seats = $total_seats + ($seat_details[$i]['total_seats']-$booked_seat_count);
                          }
                          if($no_of_seat <= $total_seats){
                            //Check for available seats by seat type
                            $counter = 0;
                            for($i=0; $i < sizeof($seat_details); $i++) {
                              if((int)$seat_details[$i]['total_seats'] >= (int)$no_of_seat) { $counter = 1; }
                            }
                            if($counter == 1) {
                              $src_point = $this->api_bus->get_source_destination_of_trip_by_sdd_id($tList['sdd_id']);
                              $dest_point = $this->api_bus->get_source_destination_of_trip_by_sdd_id($tList['sdd_id']);
                      ?>
                      <tr>
                        <input type="hidden" name="ownward_selected_seats_count_<?=$tList['sdd_id']?>" id="ownward_selected_seats_count_<?=$tList['sdd_id']?>" value="0">
                        <input type="hidden" name="ownward_selected_seat_nos_<?=$tList['sdd_id']?>" id="ownward_selected_seat_nos_<?=$tList['sdd_id']?>">
                        <input type="hidden" name="ownward_prev_selected_seat_no_<?=$tList['sdd_id']?>" id="ownward_prev_selected_seat_no_<?=$tList['sdd_id']?>" value="0">
            
                          <td class="hidden" style="line-height: 15px;"><?=$tList['trip_id'];?></td>
                          <td class="" style="line-height: 15px; padding: 0px">
                            <div class="hpanel hoverme" style="line-height: 15px; background-color:white; border-radius: 10px; margin-bottom: 10px; box-shadow: all;">
                              <div class="panel-body" style="line-height: 15px; padding-bottom: 0px; padding-top: 6px;">
                                <div class="row" style="line-height: 15px;">

                                  <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10" style="margin-bottom:0px; margin-right: -60px;">
                                    <img src="<?=($operator_details['avatar_url']=='' || $operator_details['avatar_url'] == 'NULL')?base_url('resources/no-image.jpg'):base_url($operator_details['avatar_url'])?>" class="img-responsive" style="max-height: 18px; min-height: 18px; width: auto;">
                                  </div>

                                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-center">
                                    <h6 style="margin-top:0px; margin-bottom:0px; "><i class="fa fa-bus"></i><font color="#3498db"> <?=$total_seats?></font> <?=$this->lang->line("Seat Left")?></h6>
                                  </div>

                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 30px;"> 
                                    
                                    <div class="col-md-1" style="padding-right: 0; padding-left: 0px">
                                      <div class="row">
                                          <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                                            <img style="max-width: 14px; height: auto;" src="<?=base_url('resources/images/icon-connection-top@2x.png')?>" style="float: left;">
                                          </div>

                                          <div class="col-md-11" style="padding-left: 0px; padding-right: 0px; height: 31px;">
                                            <h6 style="margin-top:0px; margin-bottom:0px; margin-left: 10px"><strong><?=$trip_master_details[0]['trip_depart_time']?></strong></h5>
                                          </div>
                                          <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                                            <h6 style="margin-top:0px; height:22px; margin-bottom:3px; font-size: 14px; display: inline-flex;"><i class="fa fa-clock-o" style="font-size: 25px;"></i>&nbsp;&nbsp;<?=$trip_master_details[0]['trip_duration']?><?=$this->lang->line('Hrs')?></h6>
                                            <input type="hidden" id="duration<?=$tList['sdd_id']?>" value="<?=$trip_master_details[0]['trip_duration']?>">
                                          </div>
                                          
                                          <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                                            <img style="max-width: 14px; height: auto;" src="<?=base_url('resources/images/icon-connection-bottom@2x.png')?>" style="float: left;">
                                          </div>

                                          <div class="col-md-11" style="padding-left: 0px; padding-right: 0px; height: 31px;">
                                            <h6 style="margin-top:5px; margin-bottom:0px; margin-left: 10px"><?=date('H:i', strtotime($trip_master_details[0]['trip_depart_time'])+(60*60*2))?></h6>
                                          </div>
                                      </div>
                                    </div>
                                    
                                    <div class="col-md-4" style="padding-right: 0; padding-left: 0px">
                                      <div class="row">
                                        <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                                          <h6 style="margin-bottom:10px; margin-top:0px;">
                                            <strong style="color: #3498db"><?=$trip_source?></strong>, 
                                            <font style="color: #666666"><?= ucwords($this->api_bus->get_location_point_address_by_id__(explode(',',$src_point[0]['src_point_ids'])[0])['point_landmark']) ?></font>
                                          </h6>
                                        </div>
                                        <div class="col-md-12" style="padding-left:0px; padding-right: 0px; margin-top: -3px;">
                                          <?php 
                                            $pickup_lat_long_string = $this->api_bus->get_loc_lat_long_by_id(explode(',',$tList['src_point_ids'])[0]);
                                            $pickup_lat_long_array = explode(',',$pickup_lat_long_string);
                                            $drop_lat_long_string = $this->api_bus->get_loc_lat_long_by_id(explode(',',$tList['dest_point_ids'])[0]);
                                            $drop_lat_long_array = explode(',',$drop_lat_long_string);
                                          ?>
                                          <h6 style="margin-top: 0px; margin-bottom:2px;"><i class="fa fa-road" style="font-size: 25px;"> </i> 
                                            <?php if(sizeof($pickup_lat_long_array) == 2 && sizeof($drop_lat_long_array) == 2) { ?>
                                            <?php echo round($this->api_bus->GetDrivingDistance($pickup_lat_long_array[0],$pickup_lat_long_array[1],$drop_lat_long_array[0],$drop_lat_long_array[1]), 2) . 'KM';
                                              echo '<input type="hidden" id="distance'.$tList['trip_id'].'" value="'.round($this->api_bus->GetDrivingDistance($pickup_lat_long_array[0],$pickup_lat_long_array[1],$drop_lat_long_array[0],$drop_lat_long_array[1]), 2).'">';
                                           ?>
                                            <?php } else { echo 'N/A'; } ?>  
                                          </h6>
                                        </div>
                                        <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                                          <h6 style="margin-bottom:10px; margin-top:0px;">
                                            <strong style="color: #3498db"><?=$trip_destination?></strong>, 
                                            <font style="color: #666666"><?= ucwords($this->api_bus->get_location_point_address_by_id__(explode(',',$dest_point[0]['dest_point_ids'])[0])['point_landmark']) ?></font>
                                          </h6>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-md-2" style="padding-right: 0; padding-left: 0px">
                                      <div class="row">
                                        <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                          <?php
                                            $bus_rating = $this->api_bus->get_bus_review_details($tList['cust_id']);
                                            if($bus_rating) {
                                          ?>
                                            <h6 style="margin-top:0px; margin-bottom:2px;">
                                              <?php for($i=0; $i<(int)$bus_rating['ratings']; $i++){ echo '<font color="#FFD700"><i class="fa fa-star"></i></font>'; } ?>
                                              <?php for($i=0; $i<(5-$bus_rating['ratings']); $i++){ echo '<font color="#FFD700"><i class="fa fa-star-o"></i></font>'; } ?> 
                                            </h6>
                                          <?php } else { ?> 
                                              <h6 style="margin-top:0px;margin-bottom:2px;"><font color="#FFD700"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></font></h6>
                                          <?php } ?>
                                        </div>
                                        <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                          <h6 style='margin-bottom:0px; margin-top:3px;'>
                                            <?=$this->lang->line('Vehicle Type: ') . $this->api_bus->get_vehicle_type_name_by_id($trip_master_details[0]['vehical_type_id'])?>
                                          </h6>
                                        </div>
                                        <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                          <a class="btn btn-link btn_view_vehicle_<?=$tList['sdd_id']?>" style="padding-left:0px;"><i class="fa fa-info-circle"></i> <?=$this->lang->line("Vehicle Detail")?></a>
                                          <a class="btn btn-link btn_hide_vehicle_<?=$tList['sdd_id']?>" style="padding-left:0px;"><i class="fa fa-info-circle"></i> <?=$this->lang->line("Vehicle Detail")?></a>
                                        </div>
                                      </div>
                                    </div>
                                    
                                    <div class="col-md-2 text-center" style="padding-right: 0; padding-left: 0px">
                                      <?php if( $journey_date >= $trip_master_details[0]['special_rate_start_date'] && $journey_date <= $trip_master_details[0]['special_rate_end_date']) { ?>
                                        <div class="row">
                                          <h6 style="margin-bottom:0px; margin-top:0px; ">
                                            <s style="color: gray"><strong>&nbsp;<?=min(explode(',', $trip_master_details[0]['seat_type_price']))." ".$this->api_bus->get_trip_master_details($tList['trip_id'])[0]['currency_sign']?>&nbsp;</strong></s>
                                          </h6>
                                        </div>
                                        <div class="row">
                                          <h4 style="margin-bottom:0px; margin-top:0px; "><font color='#FF0000'><strong><?=$seat_min_price." ".$this->api_bus->get_trip_master_details($tList['trip_id'])[0]['currency_sign']?></strong></font></h4>
                                        </div>
                                      <?php } else { ?>
                                        <div class="row">
                                          <h4 style="margin-bottom:0px; margin-top:7px; "><font color='#FF0000'><strong><?=$seat_min_price." ".$this->api_bus->get_trip_master_details($tList['trip_id'])[0]['currency_sign']?></strong></font></h4>
                                        </div>
                                      <?php } ?>
                                      <div class="row">
                                        <?php
                                          $src_point=$this->api_bus->get_source_destination_of_trip_by_sdd_id($tList['sdd_id']);
                                          $src_point_array = explode(',',$src_point[0]['src_point_ids']);
                                          
                                          $dest_point_array = explode(',',$src_point[0]['dest_point_ids']);
                                          $total_src_desc = sizeof($src_point_array) + sizeof($dest_point_array);
                                        ?>
                                        <h6 style="margin-bottom:0px; margin-top: 0;" class="text-center">
                                          <?php 
                                          if(sizeof($dest_point_array) == 1 && sizeof($src_point_array) == 1) { 
                                            echo '<i class="fa fa-map-pin"></i> '.$this->lang->line('Direct'); 
                                          } else { ?>
                                            <a class="btn btn-link btn_view_points_<?=$tList['sdd_id']?>"><i class="fa fa-list"></i> <?=$total_src_desc.' '.$this->lang->line('Points')?></a>
                                            <a class="btn btn-link btn_hide_points_<?=$tList['sdd_id']?> hidden"><i class="fa fa-list"></i> <?=$total_src_desc.' '.$this->lang->line('Points')?></a>
                                          <?php } ?>
                                        </h6> 
                                      </div>
                                    </div>

                                    <div class="col-md-3 text-center" style="padding-right: 0; padding-left: 0px; margin-bottom:0px; margin-top:15px;">
                                      <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                        <a title="<?=$this->lang->line('view_details')?>" class="btn btn-sm btn-default booknowBtn btn_view_<?=$tList['sdd_id']?>"><i class="fa fa-angle-double-down"></i> <?=$this->lang->line('Select Seat')?></a>
                                        <a title="<?=$this->lang->line('Hide Details')?>" class="btn btn-sm btn-default booknowBtn btn_hide_<?=$tList['sdd_id']?> hidden"><i class="fa fa-angle-double-up"></i> <?=$this->lang->line('Select Seat')?></a>
                                      </div>
                                      <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                        <a class="btn btn-link btn_view_cancelation_<?=$tList['sdd_id']?>" style="padding-top:5px; padding-bottom:0px; padding-left:0px; font-size:12px;"><i class="fa fa-exclamation-circle"></i> <?=$this->lang->line("cancellation_charges")?></a>
                                        <a class="btn btn-link btn_hide_cancelation_<?=$tList['sdd_id']?>" style="padding-top:5px; padding-bottom:0px; padding-left:0px; font-size:12px;"><i class="fa fa-exclamation-circle"></i> <?=$this->lang->line("cancellation_charges")?></a>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                              </div>
                      
                              <div class="panel-body" id="vehicle_details_<?=$tList['sdd_id']?>" style="display:none; padding-bottom: 0px;">
                                <div class="row">
                                  <div class="col-md-12" style="margin-top: 0px;"> 
                                    <h6 style="margin-top: 0px; margin-bottom: 0px;">
                                      <strong><?= $this->lang->line('Amenities'); ?>: </strong><?=' '.str_replace('_',' ',str_replace(',',', ',$bus_details['bus_amenities']))?>
                                    </h6>
                                  </div>
                                  <div class="col-md-12" style="padding-right: 0px;">
                                    <h6 style="margin-top:3px;">
                                      <strong><?= $this->lang->line('Vehicle'); ?>: </strong><?=' '.$bus_details['bus_no'].' - '.$bus_details['bus_ac']. ', ' .$bus_details['bus_seat_type']?>
                                    </h6>
                                  </div>                                 
                                </div>
                              </div>

                              <div class="panel-body" id="cancelation_charge_<?=$tList['sdd_id']?>" style="display:none; padding-bottom: 0px;">
                                <?php
                                  $o_d = $this->api_bus->get_source_destination_of_trip_by_sdd_id($tList['sdd_id']);
                                  $location_details = $this->api_bus->get_bus_location_details($o_d[0]['trip_source_id']);
                                  $country_id = $this->api_bus->get_country_id_by_name($location_details['country_name']);
                                  $table_data= $this->api_bus->get_cancellation_charges_by_country($o_d[0]['cust_id'],$o_d[0]['vehical_type_id'],$country_id);
                                ?>
                                <div class="row">
                                  <div class="col-md-8">
                                    <table id="policy_table" class="table">
                                      <thead>
                                        <tr>
                                          <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('Cancellation Time')?></h6></th>
                                          <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('cancellation_charges')?></h6></th>
                                          <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('rescheduling_charges')?></h6></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                          if(sizeof($table_data) > 0) {
                                           foreach ($table_data as $td) {
                                              echo "<tr><td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>From ".$td['bcr_min_hours']." To ".$td['bcr_max_hours']." Hrs</h6></td>"; 
                                              echo "<td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>".$td['bcr_cancellation']."%</h6></td>";    
                                              echo "<td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>".$td['bcr_rescheduling']."%</h6></td></tr>"; 
                                            }
                                          } else {
                                            echo "<tr><td style='padding-top:2px; padding-bottom:2px;' colspan='3'><h6 style='margin-top:0px; margin-bottom:0px;'>".$this->lang->line('No policy found!')."</h6></td>"; 
                                          } 
                                        ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>

                              <div class="panel-body" id="details_<?=$tList['sdd_id']?>" style="display:none; padding-bottom: 10px;">
                                <div class="row col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                  <div class="col-md-4">
                                    <h6><?= $this->lang->line('Operator Name'); ?>: <font color="#3498db"><?= $operator_details['firstname']." ".$operator_details['lastname'] ?></font></h6>
                                    <h6><?= $this->lang->line('Operator Contact'); ?>: <font color="#3498db"><?= $operator_details['contact_no'] ?></font></h6>
                                    <h6><?= $this->lang->line('Operator Email'); ?>: <font color="#3498db"><?= $operator_details['email_id'] ?></font></h6>
                                    <h6><?= $this->lang->line('Operator Address'); ?>: <font color="#3498db"><?= $operator_details['address'] ?></font></h6>
                                  </div>
                                  <div class="col-md-7">
                                    <div class="row">
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-top: 8px;">
                                        <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                          <?php
                                            //$src_point=$this->api_bus->get_source_destination_of_trip_by_sdd_id($tList['sdd_id']);
                                            $src_point_array = explode(',',$src_point[0]['src_point_ids']);
                                            $src_point_array_size = sizeof($src_point_array);
                                          ?>
                                          <h6 style="margin-bottom: 0px;"><?= $this->lang->line('Pickup Points');?></h6>
                                        </div>
                                        <div class="col-md-9" style="padding-left: 0px; padding-right: 0px;">
                                          <select style="width: 100%; height:10px; " id="onward_pickup_point<?=$tList['sdd_id']?>">
                                            <option value=""><?= $this->lang->line('select'); ?></option>
                                            <?php 
                                              for($i=0; $i < sizeof($src_point_array) ; $i++) {
                                                $src = $this->api_bus->get_location_point_address_by_id__($src_point_array[$i]);
                                                if($src_point_array_size == 1) {
                                                  echo '<option value="'.$src['point_address']."@".explode(',',$src_point[0]['src_point_times'])[$i].'" selected>'.explode(',',$src_point[0]['src_point_times'])[$i].' '.$src['point_address'].'</option>';
                                                } else {
                                                  echo '<option value="'.$src['point_address']."@".explode(',',$src_point[0]['src_point_times'])[$i].'">'.explode(',',$src_point[0]['src_point_times'])[$i].' '.$src['point_address'].'</option>';
                                                }
                                              }
                                            ?>
                                          </select>  
                                        </div>    
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-top: 8px;">
                                        <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                          <?php
                                            //$dest_point=$this->api_bus->get_source_destination_of_trip_by_sdd_id($tList['sdd_id']);
                                            $dest_point_array = explode(',',$dest_point[0]['dest_point_ids']);
                                            $dest_point_array_size = sizeof($dest_point_array);
                                          ?>
                                          <h6 style="margin-bottom: 0px;"><?= $this->lang->line('Drop Point');?></h6>
                                        </div>
                                        <div class="col-md-9" style="padding-left: 0px; padding-right: 0px;">
                                          <select style="width: 100%; height:10px;" id="onward_drop_point<?=$tList['sdd_id']?>">
                                            <option value=""><?= $this->lang->line('select'); ?></option>
                                            <?php 
                                              for($i=0; $i < sizeof($dest_point_array) ; $i++) {
                                                $dest = $this->api_bus->get_location_point_address_by_id__($dest_point_array[$i]);
                                                if($dest_point_array_size == 1) {
                                                  echo '<option value="'.$dest['point_address'].'" selected>'.explode(',',$dest_point[0]['dest_point_times'])[$i].' '.$dest['point_address'].'</option>';
                                                } else {
                                                  echo '<option value="'.$dest['point_address'].'">'.explode(',',$dest_point[0]['dest_point_times'])[$i].' '.$dest['point_address'].'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-top: 8px;">
                                        <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                          <h6 style="margin-bottom: 0px;"><?= $this->lang->line('Seat Type');?></h6>
                                        </div>
                                        <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                          <select style="width: 100%; height:10px;" id="onward_seat_type<?=$tList['sdd_id']?>" class="onward_seat_type<?=$tList['sdd_id']?>">
                                            <option value=""><?= $this->lang->line('Seat Type'); ?></option>
                                              <?php 
                                                for($i=0; $i < sizeof($seat_details); $i++) {
                                                  if((int)$seat_details[$i]['total_seats'] >= (int)$no_of_seat) {
                                                    if(!is_null($booked_seat_nos = $this->api_bus->get_trip_booked_seat_count_details($tList['trip_id'], $seat_details[$i]['type_of_seat'], $journey_date))) {
                                                    } else { $booked_seat_nos = 0; }
                                                    if((int)($seat_details[$i]['total_seats'] - (int)$booked_seat_nos) >= (int)$no_of_seat) {
                                                      echo '<option value="'.$seat_details[$i]['type_of_seat'].'@'.$seat_type_price[$i].'">'.$seat_details[$i]['type_of_seat'].' - '.$seat_type_price[$i].' '.$this->api_bus->get_trip_master_details($tList['trip_id'])[0]['currency_sign'].'</option>'; 
                                                    }
                                                  }
                                                }
                                              ?>
                                          </select>
                                          <input type="hidden" id="ownward_currency_sign_<?=$tList['sdd_id']?>" value="<?=$this->api_bus->get_trip_master_details($tList['trip_id'])[0]['currency_sign']?>">
                                          <input type="hidden" id="ownward_currency_id_<?=$tList['sdd_id']?>" value="<?=$this->api_bus->get_trip_master_details($tList['trip_id'])[0]['currency_id']?>">
                                        </div>
                                        <div class="col-md-6 text-right" style="padding-left: 0px; padding-right: 0px;">
                                          <?php if(isset($trip_list_return) && !empty($trip_list_return)) { ?>
                                            <button type="button" class="btn btn-sm btn-default booknowBtn goto_return<?=$tList['sdd_id']?>"><?=$this->lang->line('Select Return')?></button>
                                            <label style="color: #3498db">OR</label>
                                          <?php } else { ?>
                                            <!-- <button type="button" class="btn btn-danger booknowBtn" style="padding: 7px 18px;" disabled><?=$this->lang->line('Select Return')?></button> -->
                                          <?php } ?>
                                          <button type="button" style="background-color: #FA6B6B" class="btn btn-sm btn-default booknowBtn book_single<?=$tList['sdd_id']?>"><?=$this->lang->line('Book Now')?></button>
                                          <input type="hidden" id="ownward_trip<?=$tList['sdd_id']?>"  value="<?=$tList['sdd_id']?>">
                                        </div>
                                      </div>
                                    </div>  
                                  </div>
                                </div>
                              </div>

                              <div class="panel-body" id="point_details_<?=$tList['sdd_id']?>" style="display:none; padding-bottom: 0px;">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                  <h6 style="margin-top: 0px;"><strong><i class="fa fa-map-marker"></i> <?=$trip_source?></strong></h6>
                                </div>
                                
                                <?php for($i=0; $i < sizeof($src_point_array) ; $i++) { ?>
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                      <p><i class="fa fa-clock-o"></i> <?=explode(',',$src_point[0]['src_point_times'])[$i]?></p>
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                      <p>
                                        <?php $src = $this->api_bus->get_location_point_address_by_id__($src_point_array[$i]); ?>
                                        <?=$src['point_address']?>
                                      </p>
                                    </div>
                                  </div>
                                <?php } ?>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-bottom: -15px;">
                                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                    <p>|</p>
                                  </div>
                                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                    <p>|</p>
                                  </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                    <p>|</p>
                                  </div>
                                  <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                    <p>|</p>
                                  </div>
                                </div>

                                <?php $dest_point_array = array_reverse($dest_point_array); for($i=0; $i < sizeof($dest_point_array) ; $i++) { ?>
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                      <p><i class="fa fa-clock-o"></i> <?=explode(',',$src_point[0]['dest_point_times'])[$i]?></p>
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                      <p>
                                        <?php $dest = $this->api_bus->get_location_point_address_by_id__($dest_point_array[$i]); ?>
                                        <?=$dest['point_address']?>
                                      </p>
                                    </div>
                                  </div>
                                <?php } ?>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                  <h6 style="margin-top: 0px;"><strong><i class="fa fa-map-marker"></i> <?=$trip_destination?></strong></h6>
                                </div>
                              </div>

                              <!-- Seat Layouts -->
                              <div class="panel-body hidden" id="btn_view_seat_layout_<?=$tList['sdd_id']?>" style="">
                                <div class="row">
                                  <div class="col-md-9" style="border: 1px solid #225595; border-radius: 24px; margin-left: 5px; margin-right: -15px; padding-bottom: 5px; padding-left: 0px; padding-top: 5px;">
                                    <div class="col-md-1" style="margin-top: 145px; padding-right: 0px;">
                                      <img style="min-width: 64; height: auto" src="<?=base_url('resources/images/steering-wheel.png')?>" />
                                    </div>
                                    <div class="col-md-10" style="">
                                      <?php
                                        $vip_seat_nos = explode(',',$bus_details['vip_seat_nos']);
                                        $total_seats = (int)$bus_details['total_seats'];
                                        $vip_seats = (int)$bus_details['vip_seats'];
                                        $total_rows = ceil($total_seats/4);
                                        $odd_seats = $total_seats%4;
                                        $booked_seats = array(); //Booked Seats
                                        foreach ($vip_booked_seat_nos as $bookst) {
                                          $sits = explode(',',$bookst['vip_seat_nos']);
                                          foreach ($sits as $st) { array_push($booked_seats, $st); }
                                        }
                                        $seat_rows = array("A","B","E","C","D"); //Seats per row - E is walkway
                                        /*Walkway seats*/
                                        $walkway_seats = array();
                                        if($odd_seats == 1) { for($j=1; $j <= $total_rows-2; $j++) { array_push($walkway_seats, "E".$j); }
                                        } else { for($j=1; $j <= $total_rows; $j++) { array_push($walkway_seats, "E".$j); } }

                                        echo "<table>";
                                        foreach($seat_rows as $i) {
                                          if($odd_seats == 0) { if($odd_seats == 0) $tot_row = $total_rows+1; else $tot_row = $total_rows;
                                          } else if( ($odd_seats == 1 || $odd_seats == 2 || $odd_seats == 3) && $i == 'A') { 
                                            if($odd_seats == 1) { $tot_row = $total_rows;
                                            } else { $tot_row = $total_rows+1; }
                                          } else if( ($odd_seats == 2 || $odd_seats == 3) && $i == 'B') { $tot_row = $total_rows+1;
                                          } else if($odd_seats == 3 && $i == 'C') { $tot_row = $total_rows+1;
                                          } else { $tot_row = $total_rows; }
                                          echo "<tr>";
                                          for($r=1; $r < $tot_row; $r++) {
                                            $seat = $i.$r;
                                            if(in_array($seat, $booked_seats)) { $image = "<img src=".base_url('resources/images/bookedseat.png')." alt=".$this->lang->line("Sold out")." title=".$this->lang->line("Sold out")." >";
                                            } else if (!in_array($seat, $walkway_seats)) {
                                              if(in_array($seat, $vip_seat_nos)) {
                                                $image = "<input type='checkbox' id='check_".$tList['sdd_id'].$seat."' title='check_".$tList['sdd_id'].$seat."' class='chck_".$tList['sdd_id'].$seat."' value='".$seat."' style='background: url(".base_url('resources/images/bed.png').") no-repeat;' />
                                                    <label style='padding-top:12px; padding-left:7px; font-size:10px; color:#FFF; background: url(".base_url('resources/images/availseat.png').") no-repeat;' for='check_".$tList['sdd_id'].$seat."'>".$seat."</label>
                                                ";
                                              } else { $image = "<img src=".base_url('resources/images/busseat.png')." alt=".$this->lang->line("Not a VIP seat")." title=".$this->lang->line("Not a VIP seat")." >"; } 
                                            } else { $image = "&nbsp;"; }
                                            echo "<td>".$image."</td>"; ?>
                                          <script>
                                            $(".chck_<?=$tList['sdd_id']?><?=$seat?>").on('change', function (e) {
                                              //console.log($(this).is(':checked'));
                                              if(!$(this).is(':checked')) {

                                                //get selected and set to selected seat in hidden field.
                                                var current_selected = $("#ownward_selected_seat_nos_<?=$tList['sdd_id']?>").val();
                                                var selected = $(".chck_<?=$tList['sdd_id']?><?=$seat?>").val();
                                                var ownward_selected_seats_count = $("#ownward_selected_seats_count_<?=$tList['sdd_id']?>").val();

                                                if(jQuery.inArray(selected, current_selected.split(',')) != -1) {
                                                  //console.log("is in array");
                                                  $("#ownward_selected_seats_count_<?=$tList['sdd_id']?>").val(parseInt(ownward_selected_seats_count)-parseInt(1));
                                                  //console.log();
                                                  current_selected = current_selected.replace(/^,|,$/g,'').split(',');
                                                  current_selected = jQuery.grep(current_selected, function(value) {
                                                    return value != selected;
                                                  });
                                                  $("#ownward_selected_seat_nos_<?=$tList['sdd_id']?>").val(current_selected.join(',')+',');
                                                  //console.log(current_selected);
                                                } 
                                              } else {
                                                //get selected and set to selected seat in hidden field.
                                                var pre_selected = $("#ownward_prev_selected_seat_no_<?=$tList['sdd_id']?>").val();
                                                var selected = $(".chck_<?=$tList['sdd_id']?><?=$seat?>").val();
                                                var total_seats = "<?=$no_of_seat?>";
                                                //console.log(selected);

                                                if(pre_selected != selected) {
                                                  var ownward_selected_seats_count = $("#ownward_selected_seats_count_<?=$tList['sdd_id']?>").val();
                                                  if(ownward_selected_seats_count < total_seats) {
                                                    $("#ownward_prev_selected_seat_no_<?=$tList['sdd_id']?>").val(selected);
                                                    var current_selected = $("#ownward_selected_seat_nos_<?=$tList['sdd_id']?>").val().replace(/^,|/g,'');
                                                    $("#ownward_selected_seat_nos_<?=$tList['sdd_id']?>").val(current_selected+selected+',');
                                                    var current_selected = $("#ownward_selected_seat_nos_<?=$tList['sdd_id']?>").val();
                                                    $("#ownward_selected_seats_count_<?=$tList['sdd_id']?>").val(parseInt(current_selected.split(',').length) - parseInt(1));
                                                  } else {
                                                    swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('Search again for extra seats!')?>",'warning');
                                                    $('.chck_<?=$tList['sdd_id']?><?=$seat?>').prop('checked', false);
                                                  }
                                                }
                                              }
                                            });
                                          </script>
                                        <?php } echo "</tr>"; if($odd_seats == 1 || 2 || 3) $tot_row--;
                                        } echo "</table>";
                                      ?>
                                    </div> 
                                  </div>
                                  <div class="col-md-2">
                                    <div class="col-md-12">
                                      <img style="max-width: 28px; height: auto" src="<?=base_url('resources/images/availseat.png')?>" />
                                      <font style="color: #225595"><?=$this->lang->line("Available Seats")?></font><br /><br />
                                      <img style="max-width: 28px; height: auto" src="<?=base_url('resources/images/bookedseat.png')?>" />
                                      <font style="color: #225595"><?=$this->lang->line("Booked Seats")?></font><br /><br />
                                      <img style="max-width: 28px; height: auto" src="<?=base_url('resources/images/busseat.png')?>" />
                                      <font style="color: #225595"><?=$this->lang->line("Not a VIP seat")?></font>
                                    </div>
                                  </div>                               
                                </div>
                              </div>
                              <script>
                                $(".onward_seat_type<?=$tList['sdd_id']?>").on('change', function(){
                                  var seat_type = $(".onward_seat_type<?=$tList['sdd_id']?>").val();
                                  //console.log(seat_type.split('@', 1)[0]);
                                  if(seat_type.split('@', 1)[0] == 'VIP') {
                                    $("#btn_view_seat_layout_<?=$tList['sdd_id']?>").removeClass('hidden');
                                  } else {
                                    $("#btn_view_seat_layout_<?=$tList['sdd_id']?>").addClass('hidden');
                                  }
                                });
                              </script>
                              <script>
                                $(".btn_hide_vehicle_<?=$tList['sdd_id']?>").addClass("hidden");
                                $(".btn_hide_cancelation_<?=$tList['sdd_id']?>").addClass("hidden");

                                $(".book_single<?=$tList['sdd_id']?>").click(function(){
                                  var onward_trip = $("#ownward_trip<?=$tList['sdd_id']?>").val();
                                  var onward_pickups_time = $("#onward_pickup_point<?=$tList['sdd_id']?>").val();
                                  var on_pick_time = onward_pickups_time.split("@");
                                  var onward_pickup_point = on_pick_time[0];
                                  var onward_pickup_time = on_pick_time[1];
                                  var onward_drop_point =$("#onward_drop_point<?=$tList['sdd_id']?>").val();
                                  var onward_seat = $("#onward_seat_type<?=$tList['sdd_id']?>").val();  
                                  var distance = $("#distance<?=$tList['sdd_id']?>").val();  
                                  var duration = $("#duration<?=$tList['sdd_id']?>").val();  
                                  var ownward_currency_sign = $("#ownward_currency_sign_<?=$tList['sdd_id']?>").val();  
                                  var ownward_currency_id = $("#ownward_currency_id_<?=$tList['sdd_id']?>").val();  
                                  //var ex_seat = explode('@',onward_seat);
                                  var ex_seat = onward_seat.split('@');
                                  var onward_seat_type = ex_seat[0];
                                  var onward_price = ex_seat[1];
                                  if(!onward_pickup_point){
                                    swal(<?= json_encode($this->lang->line('Select Pickup Point'))?>, "", "error");
                                  } else if(!onward_drop_point){
                                    swal(<?= json_encode($this->lang->line('Select Drop Point'))?>, "", "error");
                                  } else if(!onward_seat_type){
                                    swal(<?= json_encode($this->lang->line('select_seat_type'))?>, "", "error");
                                  } else {
                                    $('#seat_price').val(onward_price);
                                    $('#ownward_trip').val(onward_trip);
                                    $('#pickup_point').val(onward_pickup_point);
                                    $('#onward_pickup_time').val(onward_pickup_time);
                                    $('#drop_point').val(onward_drop_point);
                                    $('#seat_type').val(onward_seat_type);
                                    $('#distance').val(distance);
                                    $('#duration').val(duration);
                                    $('#ownward_currency_sign').val(ownward_currency_sign);
                                    $('#ownward_currency_id').val(ownward_currency_id);
                                    var total_seats = "<?=$no_of_seat?>";
                                    var ownward_selected_seats_count = $("#ownward_selected_seats_count_<?=$tList['sdd_id']?>").val();

                                    if(onward_seat_type == 'VIP') {
                                      if(ownward_selected_seats_count < total_seats){
                                        swal(<?= json_encode($this->lang->line('Select Seat'))?>, "", "error");
                                      } else { $("#frmBooking").submit(); }
                                    } else { $("#frmBooking").submit(); }
                                  }
                                });

                                $(".goto_return<?=$tList['sdd_id']?>").click(function(){
                                  var onward_trip = $("#ownward_trip<?=$tList['sdd_id']?>").val();
                                    
                                  var onward_pickups_time = $("#onward_pickup_point<?=$tList['sdd_id']?>").val();
                                  var on_pick_time = onward_pickups_time.split("@");
                                  var onward_pickup_point = on_pick_time[0];
                                  var onward_pickup_time = on_pick_time[1];

                                  var onward_drop_point =$("#onward_drop_point<?=$tList['sdd_id']?>").val();
                                  var onward_seat = $("#onward_seat_type<?=$tList['sdd_id']?>").val();  
                                  var distance = $("#distance<?=$tList['sdd_id']?>").val();
                                  var duration = $("#duration<?=$tList['sdd_id']?>").val();
                                  var ownward_currency_sign = $("#ownward_currency_sign_<?=$tList['sdd_id']?>").val();
                                  var ownward_currency_id = $("#ownward_currency_id_<?=$tList['sdd_id']?>").val();
                                  var ex_seat = onward_seat.split('@');
                                  var onward_seat_type = ex_seat[0];
                                  var onward_price = ex_seat[1];

                                  if(!onward_pickup_point){
                                    swal(<?= json_encode($this->lang->line('Select Pickup Point'))?>, "", "error");
                                  } else if(!onward_drop_point){
                                    swal(<?= json_encode($this->lang->line('Select Drop Point'))?>, "", "error");
                                  } else if(!onward_seat_type){
                                    swal(<?= json_encode($this->lang->line('select_seat_type'))?>, "", "error");
                                  } else {
                                    $('#seat_price').val(onward_price);
                                    $('#ownward_trip').val(onward_trip);
                                    $('#pickup_point').val(onward_pickup_point);
                                    $('#onward_pickup_time').val(onward_pickup_time);
                                    $('#drop_point').val(onward_drop_point);
                                    $('#seat_type').val(onward_seat_type);
                                    $('#distance').val(distance);
                                    $('#duration').val(duration);
                                    $('#ownward_currency_sign').val(ownward_currency_sign);
                                    $('#ownward_currency_id').val(ownward_currency_id);

                                    var total_seats = "<?=$no_of_seat?>";
                                    var ownward_selected_seats_count = $("#ownward_selected_seats_count_<?=$tList['sdd_id']?>").val();

                                    if(onward_seat_type == 'VIP') {
                                      if(ownward_selected_seats_count < total_seats) {
                                        swal(<?= json_encode($this->lang->line('Select Seat'))?>, "", "error");
                                      } else { 
                                        $('.nav-tabs a[href="#menu1"]').tab('show');
                                        $('.hometab').removeClass("btn-default"); 
                                      }
                                    } else { 
                                      $('.nav-tabs a[href="#menu1"]').tab('show');
                                      $('.hometab').removeClass("btn-default");
                                    }
                                  }
                                });
                              </script>
                              <script>
                                $(".btn_view_<?=$tList['sdd_id']?>").click(function(){
                                  var seat_type = $(".onward_seat_type<?=$tList['sdd_id']?>").val();
                                  //console.log(seat_type.split('@', 1)[0]);
                                  if(seat_type.split('@', 1)[0] == 'VIP') {
                                    $("#btn_view_seat_layout_<?=$tList['sdd_id']?>").removeClass('hidden');
                                  } else {
                                    $("#btn_view_seat_layout_<?=$tList['sdd_id']?>").addClass('hidden');
                                  }
                                  $("#details_<?=$tList['sdd_id']?>").slideToggle(500);
                                  $(".btn_view_<?=$tList['sdd_id']?>").addClass("hidden");
                                  $(".btn_hide_<?=$tList['sdd_id']?>").removeClass("hidden");
                                });
                                $(".btn_hide_<?=$tList['sdd_id']?>").click(function(){
                                  $("#btn_view_seat_layout_<?=$tList['sdd_id']?>").addClass('hidden');
                                  $("#details_<?=$tList['sdd_id']?>").slideToggle(500);
                                  $(".btn_view_<?=$tList['sdd_id']?>").removeClass("hidden");
                                  $(".btn_hide_<?=$tList['sdd_id']?>").addClass("hidden");
                                });
                              </script>
                              <!-- Seat Layouts -->
                              <script>
                                $(".btn_view_points_<?=$tList['sdd_id']?>").click(function(){
                                  $("#point_details_<?=$tList['sdd_id']?>").slideToggle(500);
                                  $(".btn_view_points_<?=$tList['sdd_id']?>").addClass("hidden");
                                  $(".btn_hide_points_<?=$tList['sdd_id']?>").removeClass("hidden");
                                });
                                $(".btn_hide_points_<?=$tList['sdd_id']?>").click(function(){
                                  $("#point_details_<?=$tList['sdd_id']?>").slideToggle(500);
                                  $(".btn_view_points_<?=$tList['sdd_id']?>").removeClass("hidden");
                                  $(".btn_hide_points_<?=$tList['sdd_id']?>").addClass("hidden");
                                });
                              </script>
                              <script>
                                $(".btn_view_vehicle_<?=$tList['sdd_id']?>").click(function(){
                                  $("#vehicle_details_<?=$tList['sdd_id']?>").slideToggle(500);
                                  $(".btn_view_vehicle_<?=$tList['sdd_id']?>").addClass("hidden");
                                  $(".btn_hide_vehicle_<?=$tList['sdd_id']?>").removeClass("hidden");
                                });
                                $(".btn_hide_vehicle_<?=$tList['sdd_id']?>").click(function(){
                                  $("#vehicle_details_<?=$tList['sdd_id']?>").slideToggle(500);
                                  $(".btn_view_vehicle_<?=$tList['sdd_id']?>").removeClass("hidden");
                                  $(".btn_hide_vehicle_<?=$tList['sdd_id']?>").addClass("hidden");
                                });
                              </script>
                              <script>
                                $(".btn_view_cancelation_<?=$tList['sdd_id']?>").click(function(){
                                  $("#cancelation_charge_<?=$tList['sdd_id']?>").slideToggle(500);
                                  $(".btn_view_cancelation_<?=$tList['sdd_id']?>").addClass("hidden");
                                  $(".btn_hide_cancelation_<?=$tList['sdd_id']?>").removeClass("hidden");
                                });
                                $(".btn_hide_cancelation_<?=$tList['sdd_id']?>").click(function(){
                                  $("#cancelation_charge_<?=$tList['sdd_id']?>").slideToggle(500);
                                  $(".btn_view_cancelation_<?=$tList['sdd_id']?>").removeClass("hidden");
                                  $(".btn_hide_cancelation_<?=$tList['sdd_id']?>").addClass("hidden");
                                });
                              </script>
                            </div>
                          </td>
                        </tr>
                      <?php } } } } ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php if(isset($trip_list_return) && !empty($trip_list_return)) { ?>
                <div id="menu1" class="tab-pane fade"> 
                  <div class="row">
                    <table id="orderTableData1" class="table">
                      <thead><tr class="hidden"><th class="hidden">ID</th><th></th></tr></thead>
                      <tbody>
                        <?php foreach ($trip_list_return as $rtList) { 
                          $customer_details = $this->api_bus->get_user_details($rtList['cust_id']);
                          //echo json_encode($customer_details);
                          $operator_details = $this->api_bus->get_bus_operator_profile($rtList['cust_id']);
                          //echo json_encode($operator_details);
                          $bus_details = $this->api_bus->get_operator_bus_details($rtList['bus_id']);
                          //echo json_encode($bus_details);
                          $trip_master_details = $this->api_bus->get_trip_master_details($rtList['trip_id']);
                          //$seat_type_price = explode(',', $trip_master_details[0]['seat_type_price']);
                          if( $journey_date >= $trip_master_details[0]['special_rate_start_date'] && $journey_date <= $trip_master_details[0]['special_rate_end_date']) {
                            $seat_type_price = explode(',', $trip_master_details[0]['special_price']);
                          } else { $seat_type_price = explode(',', $trip_master_details[0]['seat_type_price']);  }

                          //get minimum seat price
                          if($bus_details['vip_seat_nos'] == 'NULL') {
                            $seat_type_array = explode(',', $trip_master_details[0]['seat_type']); //VIP,General,Economic
                            $seat_type_price_array = explode(',', $trip_master_details[0]['seat_type_price']); //10,20,20
                            $s_prices = array();
                            for($i=0; $i < sizeof($seat_type_array); $i++) {
                              if($seat_type_array[$i] != 'VIP') { array_push($s_prices, $seat_type_price_array[$i]); }
                            }
                            $seat_min_price = min($s_prices);
                          } else { $seat_min_price = min($seat_type_price); }

                          if($rtList[0]=="safe") { 
                            //Available seat count and filter not available seats.
                            $total_seats = 0;
                            $seat_details = $this->api_bus->get_operator_bus_seat_details($rtList['bus_id']);
                            for ($i=0; $i < sizeof($seat_details); $i++) { 
                              if($seat_details[$i]['type_of_seat'] == 'VIP') {
                                $vip_booked_seat_nos = $this->api_bus->get_trip_booked_vip_seat_details($rtList['trip_id'], $seat_details[$i]['type_of_seat'], $return_date);
                                //echo json_encode($this->db->last_query()); die();
                                //echo json_encode($vip_booked_seat_nos);
                              }
                              if(!$booked_seat_count = $this->api_bus->get_trip_booked_seat_count_details($rtList['trip_id'], $seat_details[$i]['type_of_seat'], $return_date)) {
                                $booked_seat_count = 0;
                              }
                              $total_seats = $total_seats + ($seat_details[$i]['total_seats']-$booked_seat_count);
                            }
                            if($no_of_seat <= $total_seats) {
                              //Check for available seats by seat type
                              $counter = 0;
                              for($i=0; $i < sizeof($seat_details); $i++) {
                                if((int)$seat_details[$i]['total_seats'] >= (int)$no_of_seat) { $counter = 1; }
                              }
                              if($counter == 1) {
                                $src_point = $this->api_bus->get_source_destination_of_trip_by_sdd_id($rtList['sdd_id']);
                                $dest_point = $this->api_bus->get_source_destination_of_trip_by_sdd_id($rtList['sdd_id']);
                        ?>
                          <tr>
                            <input type="hidden" name="return_selected_seats_count_<?=$rtList['sdd_id']?>" id="return_selected_seats_count_<?=$rtList['sdd_id']?>" value="0">
                            <input type="hidden" name="return_selected_seat_nos_<?=$rtList['sdd_id']?>" id="return_selected_seat_nos_<?=$rtList['sdd_id']?>">
                            <input type="hidden" name="return_prev_selected_seat_no_<?=$rtList['sdd_id']?>" id="return_prev_selected_seat_no_<?=$rtList['sdd_id']?>" value="0">
                            <td class="hidden" style="line-height: 15px;"><?=$rtList['trip_id'];?></td>
                            <td class="" style="line-height: 15px; padding: 0px">
                              <div class="hpanel hoverme" style="line-height: 15px; background-color:white; border-radius: 10px; margin-bottom: 10px; box-shadow: all;">
                                <div class="panel-body" style="line-height: 15px; padding-bottom: 0px; padding-top: 6px;">
                                  <div class="row" style="line-height: 15px;">

                                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10" style="margin-bottom:0px; margin-right: -60px;">
                                      <img src="<?=($operator_details['avatar_url']=='' || $operator_details['avatar_url'] == 'NULL')?base_url('resources/no-image.jpg'):base_url($operator_details['avatar_url'])?>" class="img-responsive" style="max-height: 18px; min-height: 18px; width: auto;">
                                    </div>

                                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 text-center">
                                      <h6 style="margin-top:0px; margin-bottom:0px; "><i class="fa fa-bus"></i><font color="#3498db"> <?=$total_seats?></font> <?=$this->lang->line("Seat Left")?></h6>
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-left: 30px;">
                                      <div class="col-md-1" style="padding-right: 0; padding-left: 0px">
                                        <div class="row">
                                          <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                                            <img style="max-width: 14px; height: auto;" src="<?=base_url('resources/images/icon-connection-top@2x.png')?>" style="float: left;">
                                          </div>

                                          <div class="col-md-11" style="padding-left: 0px; padding-right: 0px; height: 31px;">
                                            <h6 style="margin-top:0px; margin-bottom:0px; margin-left: 10px"><strong><?=$trip_master_details[0]['trip_depart_time']?></strong></h5>
                                          </div>
                                          <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                                            <h6 style="margin-top:0px; height:22px; margin-bottom:3px; font-size: 14px; display: inline-flex;"><i class="fa fa-clock-o" style="font-size: 25px;"></i>&nbsp;&nbsp;<?=$trip_master_details[0]['trip_duration']?><?=$this->lang->line('Hrs')?></h6>
                                            <input type="hidden" id="duration<?=$rtList['sdd_id']?>" value="<?=$trip_master_details[0]['trip_duration']?>">
                                          </div>
                                          
                                          <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                                            <img style="max-width: 14px; height: auto;" src="<?=base_url('resources/images/icon-connection-bottom@2x.png')?>" style="float: left;">
                                          </div>

                                          <div class="col-md-11" style="padding-left: 0px; padding-right: 0px; height: 31px;">
                                            <h6 style="margin-top:0px; margin-bottom:0px; margin-left: 10px"><?=date('H:i', strtotime($trip_master_details[0]['trip_depart_time'])+(60*60*2))?></h6>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-4" style="padding-right: 0; padding-left: 0px">
                                        <div class="row">
                                          <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                                            <h6 style="margin-bottom:10px; margin-top:0px;">
                                              <strong style="color: #3498db"><?=$trip_destination?></strong>, 
                                              <font style="color: #666666"><?= ucwords($this->api_bus->get_location_point_address_by_id__(explode(',',$src_point[0]['src_point_ids'])[0])['point_landmark']) ?></font>
                                            </h6>
                                          </div>
                                          <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                            <?php 
                                              $pickup_lat_long_string = $this->api_bus->get_loc_lat_long_by_id(explode(',',$rtList['src_point_ids'])[0]);
                                              $pickup_lat_long_array = explode(',',$pickup_lat_long_string);
                                              $drop_lat_long_string = $this->api_bus->get_loc_lat_long_by_id(explode(',',$rtList['dest_point_ids'])[0]);
                                              $drop_lat_long_array = explode(',',$drop_lat_long_string);
                                            ?>
                                            <h6 style="margin-top: 0px; margin-bottom:2px;"><i class="fa fa-road" style="font-size: 25px;"> </i> 
                                              <?php if(sizeof($pickup_lat_long_array) == 2 && sizeof($drop_lat_long_array) == 2) { ?>
                                              <?php echo round($this->api_bus->GetDrivingDistance($pickup_lat_long_array[0],$pickup_lat_long_array[1],$drop_lat_long_array[0],$drop_lat_long_array[1]), 2) . 'KM';
                                                echo '<input type="hidden" id="distance'.$rtList['trip_id'].'" value="'.round($this->api_bus->GetDrivingDistance($pickup_lat_long_array[0],$pickup_lat_long_array[1],$drop_lat_long_array[0],$drop_lat_long_array[1]), 2).'">';
                                             ?>
                                              <?php } else { echo 'N/A'; } ?>  
                                            </h6>
                                          </div>
                                          <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                                            <h6 style="margin-bottom:10px; margin-top:0px;">
                                              <strong style="color: #3498db"><?=$trip_source?></strong>, 
                                              <font style="color: #666666"><?= ucwords($this->api_bus->get_location_point_address_by_id__(explode(',',$dest_point[0]['dest_point_ids'])[0])['point_landmark']) ?></font>
                                            </h6>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-2" style="padding-right: 0; padding-left: 0px">
                                        <div class="row">
                                          <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                            <?php
                                              $bus_rating = $this->api_bus->get_bus_review_details($rtList['cust_id']);
                                              if($bus_rating) {
                                            ?>
                                              <h6 style="margin-top:0px; margin-bottom:2px;">
                                                <?php for($i=0; $i<(int)$bus_rating['ratings']; $i++){ echo '<font color="#FFD700"><i class="fa fa-star"></i></font>'; } ?>
                                                <?php for($i=0; $i<(5-$bus_rating['ratings']); $i++){ echo '<font color="#FFD700"><i class="fa fa-star-o"></i></font>'; } ?> 
                                              </h6>
                                            <?php } else { ?> 
                                                <h6 style="margin-top:0px;margin-bottom:2px;"><font color="#FFD700"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></font></h6>
                                            <?php } ?>
                                          </div>
                                          <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                            <h6 style='margin-bottom:0px; margin-top:3px;'>
                                              <?=$this->lang->line('Vehicle Type: ') . $this->api_bus->get_vehicle_type_name_by_id($trip_master_details[0]['vehical_type_id'])?>
                                            </h6>
                                          </div>
                                          <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                            <a class="btn btn-link btn_view_vehicle_<?=$rtList['sdd_id']?>" style="padding-left:0px;"><i class="fa fa-info-circle"></i> <?=$this->lang->line("Vehicle Detail")?></a>
                                            <a class="btn btn-link btn_hide_vehicle_<?=$rtList['sdd_id']?>" style="padding-left:0px;"><i class="fa fa-info-circle"></i> <?=$this->lang->line("Vehicle Detail")?></a>
                                          </div>
                                        </div>
                                      </div>
        
                                      <div class="col-md-2 text-center" style="padding-right: 0; padding-left: 0px">
                                        <?php if( $return_date >= $trip_master_details[0]['special_rate_start_date'] && $return_date <= $trip_master_details[0]['special_rate_end_date']) { ?>
                                          <div class="row">
                                            <h6 style="margin-bottom:0px; margin-top:0px; ">
                                              <s style="color: gray"><strong>&nbsp;<?=min(explode(',', $trip_master_details[0]['seat_type_price']))." ".$this->api_bus->get_trip_master_details($rtList['trip_id'])[0]['currency_sign']?>&nbsp;</strong></s>
                                            </h6>
                                          </div>
                                          <div class="row">
                                            <h4 style="margin-bottom:0px; margin-top:0px; "><font color='#FF0000'><strong><?=$seat_min_price." ".$this->api_bus->get_trip_master_details($rtList['trip_id'])[0]['currency_sign']?></strong></font></h4>
                                          </div>
                                        <?php } else { ?>
                                          <div class="row">
                                            <h4 style="margin-bottom:0px; margin-top:7px; "><font color='#FF0000'><strong><?=$seat_min_price." ".$this->api_bus->get_trip_master_details($rtList['trip_id'])[0]['currency_sign']?></strong></font></h4>
                                          </div>
                                        <?php } ?>

                                        <!-- <div class="row">
                                          <h4 style="margin-bottom:0px; margin-top:7px; "><font color='#FF0000'><strong><?=$seat_min_price." ".$this->api_bus->get_trip_master_details($rtList['trip_id'])[0]['currency_sign']?></strong></font></h4>
                                        </div> -->
                                        <div class="row">
                                          <?php
                                            $src_point=$this->api_bus->get_source_destination_of_trip_by_sdd_id($rtList['sdd_id']);
                                            $src_point_array = explode(',',$src_point[0]['src_point_ids']);
                                            
                                            $dest_point_array = explode(',',$src_point[0]['dest_point_ids']);
                                            $total_src_desc = sizeof($src_point_array) + sizeof($dest_point_array);
                                          ?>
                                          <h6 style="margin-bottom:0px;" class="text-center">
                                            <?php 
                                            if(sizeof($dest_point_array) == 1 && sizeof($src_point_array) == 1) { 
                                              echo '<i class="fa fa-map-pin"></i> '.$this->lang->line('Direct'); 
                                            } else { ?>
                                              <a class="btn btn-link btn_view_points_<?=$rtList['sdd_id']?>"><i class="fa fa-list"></i> <?=$total_src_desc.' '.$this->lang->line('Points')?></a>
                                              <a class="btn btn-link btn_hide_points_<?=$rtList['sdd_id']?> hidden"><i class="fa fa-list"></i> <?=$total_src_desc.' '.$this->lang->line('Points')?></a>
                                            <?php } ?>
                                          </h6>
                                        </div>
                                      </div>

                                      <div class="col-md-3 text-center" style="padding-right: 0; padding-left: 0px; margin-bottom:0px; margin-top:15px;">
                                        <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                          <a title="<?=$this->lang->line('view_details')?>" class="btn btn-default booknowBtn btn_view_<?=$rtList['sdd_id']?>"><i class="fa fa-angle-double-down"></i> <?=$this->lang->line('Select Seat')?></a>
                                          <a title="<?=$this->lang->line('Hide Details')?>" class="btn btn-default booknowBtn btn_hide_<?=$rtList['sdd_id']?> hidden"><i class="fa fa-angle-double-up"></i> <?=$this->lang->line('Select Seat')?></a>
                                        </div>
                                        <div class="col-md-12" style="padding-left:0px; padding-right: 0px;">
                                          <a class="btn btn-link btn_view_cancelation_<?=$rtList['sdd_id']?>" style="padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:12px;"><i class="fa fa-exclamation-circle"></i> <?=$this->lang->line("cancellation_charges")?></a>
                                          <a class="btn btn-link btn_hide_cancelation_<?=$rtList['sdd_id']?>" style="padding-top:0px; padding-bottom:0px; padding-left:0px; font-size:12px;"><i class="fa fa-exclamation-circle"></i> <?=$this->lang->line("cancellation_charges")?></a>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </div>

                                <div class="panel-body" id="vehicle_details_<?=$rtList['sdd_id']?>" style="display:none; padding-bottom: 0px;">
                                  <div class="row">
                                    <div class="col-md-12" style="margin-top: 0px;"> 
                                      <h6 style="margin-top: 0px; margin-bottom: 0px;">
                                        <strong><?= $this->lang->line('Amenities'); ?>: </strong><?=' '.str_replace('_',' ',str_replace(',',', ',$bus_details['bus_amenities']))?>
                                      </h6>
                                    </div>
                                    <div class="col-md-12" style="padding-right: 0px;">
                                      <h6 style="margin-top:3px;">
                                        <strong><?= $this->lang->line('Vehicle'); ?>: </strong><?=' '.$bus_details['bus_no'].' - '.$bus_details['bus_ac']. ', ' .$bus_details['bus_seat_type']?>
                                      </h6>
                                    </div>                                 
                                  </div>
                                </div>

                                <div class="panel-body" id="cancelation_charge_<?=$rtList['sdd_id']?>" style="display:none; padding-bottom: 0px;">
                                  <?php
                                    $o_d = $this->api_bus->get_source_destination_of_trip_by_sdd_id($rtList['sdd_id']);
                                    $location_details = $this->api_bus->get_bus_location_details($o_d[0]['trip_source_id']);
                                    $country_id = $this->api_bus->get_country_id_by_name($location_details['country_name']);
                                    $table_data= $this->api_bus->get_cancellation_charges_by_country($o_d[0]['cust_id'],$o_d[0]['vehical_type_id'],$country_id);
                                  ?>
                                  <div class="row">
                                    <div class="col-md-8">
                                      <table id="policy_table" class="table">
                                        <thead>
                                          <tr>
                                            <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('Cancellation Time')?></h6></th>
                                            <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('cancellation_charges')?></h6></th>
                                            <th style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'><?=$this->lang->line('rescheduling_charges')?></h6></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php 
                                          if(sizeof($table_data) > 0) {
                                            foreach ($table_data as $td)
                                            {
                                              echo "<tr><td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>From ".$td['bcr_min_hours']." To ".$td['bcr_max_hours']." Hrs</h6></td>"; 
                                              echo "<td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>".$td['bcr_cancellation']."%</h6></td>";    
                                              echo "<td style='padding-top:2px; padding-bottom:2px;'><h6 style='margin-top:0px; margin-bottom:0px;'>".$td['bcr_rescheduling']."%</h6></td></tr>"; 
                                            }
                                          } else {
                                            echo "<tr><td style='padding-top:2px; padding-bottom:2px;' colspan='3'><h6 style='margin-top:0px; margin-bottom:0px;'>".$this->lang->line('No policy found!')."</h6></td>"; 
                                          } 
                                          ?>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel-body" id="details_<?=$rtList['sdd_id']?>" style="display:none; padding-bottom: 10px;">
                                  <div class="row col-md-12">
                                    <div class="col-md-4">
                                      <h6><?= $this->lang->line('Operator Name'); ?>: <font color="#3498db"><?= $operator_details['firstname']." ".$operator_details['lastname'] ?></font></h6>
                                      <h6><?= $this->lang->line('Operator Contact'); ?>: <font color="#3498db"><?= $operator_details['contact_no'] ?></font></h6>
                                      <h6><?= $this->lang->line('Operator Email'); ?>: <font color="#3498db"><?= $operator_details['email_id'] ?></font></h6>
                                      <h6><?= $this->lang->line('Operator Address'); ?>: <font color="#3498db"><?= $operator_details['address'] ?></font></h6>
                                    </div>
                                    <div class="col-md-7">
                                      <div class="row">
                                        <div class="col-md-12" style="margin-top: 8px;">
                                          <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                            <?php
                                              //$src_point=$this->api_bus->get_source_destination_of_trip_by_sdd_id($rtList['sdd_id']);
                                              $src_point_array = explode(',',$src_point[0]['src_point_ids']);
                                              $src_point_array_size = sizeof($src_point_array);
                                            ?>
                                            <h6 style="margin-bottom: 0px;"><?= $this->lang->line('Pickup Points');?></h6>
                                          </div>
                                          <div class="col-md-9" style="padding-left: 0px; padding-right: 0px;">
                                            <select style="width: 100%; height:10px;" id="return_pickup_point<?=$rtList['sdd_id']?>">
                                              <option value=""><?= $this->lang->line('select'); ?></option>
                                              <?php 
                                                for($i=0; $i < sizeof($src_point_array) ; $i++) {
                                                  $src = $this->api_bus->get_location_point_address_by_id__($src_point_array[$i]);
                                                  if($src_point_array_size == 1) {
                                                    echo '<option value="'.$src['point_address']."@".explode(',',$src_point[0]['src_point_times'])[$i].'" selected>'.explode(',',$src_point[0]['src_point_times'])[$i].' '.$src['point_address'].'</option>';
                                                  } else {
                                                    echo '<option value="'.$src['point_address']."@".explode(',',$src_point[0]['src_point_times'])[$i].'">'.explode(',',$src_point[0]['src_point_times'])[$i].' '.$src['point_address'].'</option>';
                                                  }
                                                }
                                              ?>
                                            </select>  
                                          </div>    
                                        </div>
                                        <div class="col-md-12" style="margin-top: 8px;">
                                          <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                            <?php
                                              //$dest_point=$this->api_bus->get_source_destination_of_trip_by_sdd_id($rtList['sdd_id']);
                                              $dest_point_array = explode(',',$dest_point[0]['dest_point_ids']);
                                              $dest_point_array_size = sizeof($dest_point_array);
                                            ?>
                                            <h6 style="margin-bottom: 0px;"><?= $this->lang->line('Drop Point');?></h6>
                                          </div>
                                          <div class="col-md-9" style="padding-left: 0px; padding-right: 0px;">
                                            <select style="width: 100%; height:10px;" id="return_drop_point<?=$rtList['sdd_id']?>">
                                              <option value=""><?= $this->lang->line('select'); ?></option>
                                              <?php 
                                                for($i=0; $i < sizeof($dest_point_array) ; $i++) {
                                                  $dest = $this->api_bus->get_location_point_address_by_id__($dest_point_array[$i]);
                                                  if($dest_point_array_size == 1) {
                                                    echo '<option value="'.$dest['point_address'].'" selected>'.explode(',',$dest_point[0]['dest_point_times'])[$i].' '.$dest['point_address'].'</option>';
                                                  } else {
                                                    echo '<option value="'.$dest['point_address'].'">'.explode(',',$dest_point[0]['dest_point_times'])[$i].' '.$dest['point_address'].'</option>';
                                                  }
                                                }
                                              ?>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="col-md-12" style="margin-top: 8px;">
                                          <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                            <h6 style="margin-bottom: 0px;"><?= $this->lang->line('Seat Type');?></h6>
                                          </div>
                                          <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                            <select class="return_seat_type<?=$rtList['sdd_id']?>" style="width: 100%; height:10px;" id="return_seat_type<?=$rtList['sdd_id']?>">
                                              <option value=""><?= $this->lang->line('Seat Type'); ?></option>
                                              <?php 
                                                for($i=0; $i < sizeof($seat_details); $i++) {
                                                  if((int)$seat_details[$i]['total_seats'] >= (int)$no_of_seat) {
                                                    if(!is_null($booked_seat_nos = $this->api_bus->get_trip_booked_seat_count_details($rtList['trip_id'], $seat_details[$i]['type_of_seat'], $return_date))) {
                                                    } else { $booked_seat_nos = 0; }
                                                    if((int)($seat_details[$i]['total_seats'] - (int)$booked_seat_nos) >= (int)$no_of_seat) {
                                                      echo '<option value="'.$seat_details[$i]['type_of_seat'].'@'.$seat_type_price[$i].'">'.$seat_details[$i]['type_of_seat'].' - '.$seat_type_price[$i].' '.$this->api_bus->get_trip_master_details($rtList['trip_id'])[0]['currency_sign'].'</option>';
                                                    }
                                                  }
                                                }
                                              ?>
                                            </select>
                                            <input type="hidden" id="return_currency_sign_<?=$rtList['sdd_id']?>" value="<?=$this->api_bus->get_trip_master_details($rtList['trip_id'])[0]['currency_sign']?>">
                                          </div>
                                          <div class="col-md-6 text-right" style="padding-left: 0px; padding-right: 0px;">
                                          <button type="button" style="background-color: #FA6B6B" class="btn btn-default booknowBtn book_single<?=$rtList['sdd_id']?>""><?=$this->lang->line('Book Now')?></button>
                                          <input type="hidden" id="return_trip<?=$rtList['sdd_id']?>"  value="<?=$rtList['sdd_id']?>">
                                        </div>
                                        </div>
                                      </div>  
                                    </div>
                                  </div>
                                </div>

                                <div class="panel-body" id="point_details_<?=$rtList['sdd_id']?>" style="display:none; padding-bottom: 0px;">
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <h6 style="margin-top: 0px;"><strong><i class="fa fa-map-marker"></i> <?=$trip_destination?></strong></h6>
                                  </div>


                                  <?php for($i=0; $i < sizeof($dest_point_array) ; $i++) { ?>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                        <p><i class="fa fa-clock-o"></i> <?=explode(',',$src_point[0]['dest_point_times'])[$i]?></p>
                                      </div>
                                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                        <p>
                                          <?php $dest = $this->api_bus->get_location_point_address_by_id__($dest_point_array[$i]); ?>
                                          <?=$dest['point_address']?>
                                        </p>
                                      </div>
                                    </div>
                                  <?php } ?>

                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="margin-bottom: -15px;">
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                      <p>|</p>
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                      <p>|</p>
                                    </div>
                                  </div>
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                      <p>|</p>
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                      <p>|</p>
                                    </div>
                                  </div>

                                  <?php $src_point_array = array_reverse($src_point_array); for($i=0; $i < sizeof($src_point_array) ; $i++) { ?>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                      <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
                                        <p><i class="fa fa-clock-o"></i> <?=explode(',',$src_point[0]['src_point_times'])[$i]?></p>
                                      </div>
                                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                        <p>
                                          <?php $src = $this->api_bus->get_location_point_address_by_id__($src_point_array[$i]); ?>
                                          <?=$src['point_address']?>
                                        </p>
                                      </div>
                                    </div>
                                  <?php } ?>

                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <h6 style="margin-top: 0px;"><strong><i class="fa fa-map-marker"></i> <?=$trip_source?></strong></h6>
                                  </div>
                                </div>

                                <!-- Seat Layouts -->
                                <div class="panel-body hidden" id="btn_return_view_seat_layout_<?=$rtList['sdd_id']?>" style="">
                                  <div class="row">
                                    <div class="col-md-9" style="border: 1px solid #225595; border-radius: 24px; margin-left: 5px; margin-right: -15px; padding-bottom: 5px; padding-left: 0px; padding-top: 5px;">
                                      <div class="col-md-1" style="margin-top: 145px; padding-right: 0px;">
                                        <img style="min-width: 64; height: auto" src="<?=base_url('resources/images/steering-wheel.png')?>" />
                                      </div>
                                      <div class="col-md-10" style=""> 

                                        <?php
                                          $vip_seat_nos = explode(',',$bus_details['vip_seat_nos']);
                                          $total_seats = (int)$bus_details['total_seats'];
                                          $vip_seats = (int)$bus_details['vip_seats'];
                                          $total_rows = ceil($total_seats/4);
                                          $odd_seats = $total_seats%4;
                                          $booked_seats = array(); //Booked Seats
                                          foreach ($vip_booked_seat_nos as $bookst) {
                                            $sits = explode(',',$bookst['vip_seat_nos']);
                                            foreach ($sits as $st) { array_push($booked_seats, $st); }
                                          }
                                          $seat_rows = array("A","B","E","C","D"); //Seats per row - E is walkway
                                          /*Walkway seats*/
                                          $walkway_seats = array();
                                          if($odd_seats == 1) { for($j=1; $j <= $total_rows-2; $j++) { array_push($walkway_seats, "E".$j); }
                                          } else { for($j=1; $j <= $total_rows; $j++) { array_push($walkway_seats, "E".$j); } }

                                          echo "<table>";
                                          foreach($seat_rows as $i) {
                                            if($odd_seats == 0) { if($odd_seats == 0) $tot_row = $total_rows+1; else $tot_row = $total_rows;
                                            } else if( ($odd_seats == 1 || $odd_seats == 2 || $odd_seats == 3) && $i == 'A') { 
                                              if($odd_seats == 1) { $tot_row = $total_rows;
                                              } else { $tot_row = $total_rows+1; }
                                            } else if( ($odd_seats == 2 || $odd_seats == 3) && $i == 'B') { $tot_row = $total_rows+1;
                                            } else if($odd_seats == 3 && $i == 'C') { $tot_row = $total_rows+1;
                                            } else { $tot_row = $total_rows; }
                                            echo "<tr>";
                                            for($r=1; $r < $tot_row; $r++) {
                                              $seat = $i.$r;
                                              if(in_array($seat, $booked_seats)) { $image = "<img src=".base_url('resources/images/bookedseat.png')." alt=".$this->lang->line("Sold out")." title=".$this->lang->line("Sold out")." >";
                                              } else if (!in_array($seat, $walkway_seats)) {
                                                if(in_array($seat, $vip_seat_nos)) {
                                                  $image = "<input type='checkbox' id='check_return_".$rtList['sdd_id'].$seat."' title='check_return_".$rtList['sdd_id'].$seat."' class='chck_return_".$rtList['sdd_id'].$seat."' value='".$seat."' style='background: url(".base_url('resources/images/bed.png').") no-repeat;' />
                                                      <label style='padding-top:12px; padding-left:7px; font-size:10px; color:#FFF; background: url(".base_url('resources/images/availseat.png').") no-repeat;' for='check_return_".$rtList['sdd_id'].$seat."'>".$seat."</label>
                                                  ";
                                                } else { $image = "<img src=".base_url('resources/images/busseat.png')." alt=".$this->lang->line("Not a VIP seat")." title=".$this->lang->line("Not a VIP seat")." >"; } 
                                              } else { $image = "&nbsp;"; }
                                              echo "<td>".$image."</td>"; ?>
                                            <script>
                                              $(".chck_return_<?=$rtList['sdd_id']?><?=$seat?>").on('change', function (e) {
                                                //console.log($(this).is(':checked'));
                                                if(!$(this).is(':checked')) {

                                                  //get selected and set to selected seat in hidden field.
                                                  var current_selected = $("#return_selected_seat_nos_<?=$rtList['sdd_id']?>").val();
                                                  var selected = $(".chck_return_<?=$rtList['sdd_id']?><?=$seat?>").val();
                                                  var ownward_selected_seats_count = $("#return_selected_seats_count_<?=$rtList['sdd_id']?>").val();

                                                  if(jQuery.inArray(selected, current_selected.split(',')) != -1) {
                                                    //console.log("is in array");
                                                    $("#return_selected_seats_count_<?=$rtList['sdd_id']?>").val(parseInt(ownward_selected_seats_count)-parseInt(1));
                                                    //console.log();
                                                    current_selected = current_selected.replace(/^,|,$/g,'').split(',');
                                                    current_selected = jQuery.grep(current_selected, function(value) {
                                                      return value != selected;
                                                    });
                                                    $("#return_selected_seat_nos_<?=$rtList['sdd_id']?>").val(current_selected.join(',')+',');
                                                    //console.log(current_selected);
                                                  } 
                                                } else {
                                                  //get selected and set to selected seat in hidden field.
                                                  var pre_selected = $("#return_prev_selected_seat_no_<?=$rtList['sdd_id']?>").val();
                                                  var selected = $(".chck_return_<?=$rtList['sdd_id']?><?=$seat?>").val();
                                                  var total_seats = "<?=$no_of_seat?>";
                                                  //console.log(selected);

                                                  if(pre_selected != selected) {
                                                    var ownward_selected_seats_count = $("#return_selected_seats_count_<?=$rtList['sdd_id']?>").val();
                                                    if(ownward_selected_seats_count < total_seats) {
                                                      $("#return_prev_selected_seat_no_<?=$rtList['sdd_id']?>").val(selected);
                                                      var current_selected = $("#return_selected_seat_nos_<?=$rtList['sdd_id']?>").val().replace(/^,|/g,'');
                                                      $("#return_selected_seat_nos_<?=$rtList['sdd_id']?>").val(current_selected+selected+',');
                                                      var current_selected = $("#return_selected_seat_nos_<?=$rtList['sdd_id']?>").val();
                                                      $("#return_selected_seats_count_<?=$rtList['sdd_id']?>").val(parseInt(current_selected.split(',').length) - parseInt(1));
                                                    } else {
                                                      swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('Search again for extra seats!')?>",'warning');
                                                      $('.chck_return_<?=$rtList['sdd_id']?><?=$seat?>').prop('checked', false);
                                                    }
                                                  }
                                                }
                                              });
                                            </script>
                                          <?php } echo "</tr>"; if($odd_seats == 1 || 2 || 3) $tot_row--;
                                          } echo "</table>";
                                        ?>
                                      </div> 
                                    </div>
                                    <div class="col-md-2">
                                      <div class="col-md-12">
                                        <img style="max-width: 28px; height: auto" src="<?=base_url('resources/images/availseat.png')?>" />
                                        <font style="color: #225595"><?=$this->lang->line("Available Seats")?></font><br /><br />
                                        <img style="max-width: 28px; height: auto" src="<?=base_url('resources/images/bookedseat.png')?>" />
                                        <font style="color: #225595"><?=$this->lang->line("Booked Seats")?></font><br /><br />
                                        <img style="max-width: 28px; height: auto" src="<?=base_url('resources/images/busseat.png')?>" />
                                        <font style="color: #225595"><?=$this->lang->line("Not a VIP seat")?></font>
                                      </div>
                                    </div>                               
                                  </div>
                                </div>
                                <script>
                                  $(".return_seat_type<?=$rtList['sdd_id']?>").on('change', function(){
                                    var seat_type = $(".return_seat_type<?=$rtList['sdd_id']?>").val();
                                    //console.log(seat_type.split('@', 1)[0]);
                                    if(seat_type.split('@', 1)[0] == 'VIP') {
                                      $("#btn_return_view_seat_layout_<?=$rtList['sdd_id']?>").removeClass('hidden');
                                    } else {
                                      $("#btn_return_view_seat_layout_<?=$rtList['sdd_id']?>").addClass('hidden');
                                    }
                                  });
                                </script>
                                <script>
                                  $(".btn_hide_vehicle_<?=$rtList['sdd_id']?>").addClass("hidden");
                                  $(".btn_hide_cancelation_<?=$rtList['sdd_id']?>").addClass("hidden");

                                  $(".book_single<?=$rtList['sdd_id']?>").click(function(){
                                    var return_trip = $("#return_trip<?=$rtList['sdd_id']?>").val();
                                    //alert(return_trip);
                                    var return_pickup_point_time = $("#return_pickup_point<?=$rtList['sdd_id']?>").val();
                                    var re_pick_time = return_pickup_point_time.split('@');

                                    var return_pickup_point = re_pick_time[0];
                                    var return_pickup_time = re_pick_time[1];

                                    var return_drop_point =$("#return_drop_point<?=$rtList['sdd_id']?>").val();
                                    var return_seat = $("#return_seat_type<?=$rtList['sdd_id']?>").val();  
                                    var return_currency_sign = $("#return_currency_sign_<?=$rtList['sdd_id']?>").val();  
                                    //var ex_seat = explode('@',return_seat);
                                    var ex_seat = return_seat.split('@');
                                    var return_seat_type = ex_seat[0];
                                    var return_price = ex_seat[1];
                                    if(!return_pickup_point){
                                      swal(<?= json_encode($this->lang->line('Select Pickup Point'))?>, "", "error");
                                    }else if(!return_drop_point){
                                      swal(<?= json_encode($this->lang->line('Select Drop Point'))?>, "", "error");
                                    }else if(!return_seat_type){
                                      swal(<?= json_encode($this->lang->line('select_seat_type'))?>, "", "error");
                                    } else {
                                      $('#return_trip').val(return_trip);
                                      //alert($('#return_trip').val());
                                      $('#return_seat_price').val(return_price);
                                      $('#return_pickup_point').val(return_pickup_point);
                                      $('#return_pickup_time').val(return_pickup_time);
                                      $('#return_drop_point').val(return_drop_point);
                                      $('#return_seat_type').val(return_seat_type);
                                      $('#return_currency_sign').val(return_currency_sign);

                                      var total_seats = "<?=$no_of_seat?>";
                                      var return_selected_seats_count = $("#return_selected_seats_count_<?=$rtList['sdd_id']?>").val();
                                      if(return_seat_type == 'VIP') {
                                        if(return_selected_seats_count < total_seats) {
                                          swal(<?= json_encode($this->lang->line('Select Seat'))?>, "", "error");
                                        } else {  $("#frmBooking").submit(); }
                                      } else {  $("#frmBooking").submit(); }
                                    }
                                  });
                                </script>
                                <script>
                                  $(".btn_view_<?=$rtList['sdd_id']?>").click(function(){
                                    var seat_type = $(".return_seat_type<?=$rtList['sdd_id']?>").val();
                                    //console.log(seat_type.split('@', 1)[0]);
                                    if(seat_type.split('@', 1)[0] == 'VIP') {
                                      $("#btn_return_view_seat_layout_<?=$rtList['sdd_id']?>").removeClass('hidden');
                                    } else {
                                      $("#btn_return_view_seat_layout_<?=$rtList['sdd_id']?>").addClass('hidden');
                                    }
                                    $("#details_<?=$rtList['sdd_id']?>").slideToggle(500);
                                    $(".btn_view_<?=$rtList['sdd_id']?>").addClass("hidden");
                                    $(".btn_hide_<?=$rtList['sdd_id']?>").removeClass("hidden");
                                  });
                                  $(".btn_hide_<?=$rtList['sdd_id']?>").click(function(){
                                    $("#btn_return_view_seat_layout_<?=$rtList['sdd_id']?>").addClass('hidden');
                                    $("#details_<?=$rtList['sdd_id']?>").slideToggle(500);
                                    $(".btn_view_<?=$rtList['sdd_id']?>").removeClass("hidden");
                                    $(".btn_hide_<?=$rtList['sdd_id']?>").addClass("hidden");
                                  });
                                </script>
                                <!-- Seat Layouts -->
                                <script>
                                  $(".btn_view_points_<?=$rtList['sdd_id']?>").click(function(){
                                    $("#point_details_<?=$rtList['sdd_id']?>").slideToggle(500);
                                    $(".btn_view_points_<?=$rtList['sdd_id']?>").addClass("hidden");
                                    $(".btn_hide_points_<?=$rtList['sdd_id']?>").removeClass("hidden");
                                  });
                                  $(".btn_hide_points_<?=$rtList['sdd_id']?>").click(function(){
                                    $("#point_details_<?=$rtList['sdd_id']?>").slideToggle(500);
                                    $(".btn_view_points_<?=$rtList['sdd_id']?>").removeClass("hidden");
                                    $(".btn_hide_points_<?=$rtList['sdd_id']?>").addClass("hidden");
                                  });
                                </script>
                                <script>
                                  $(".btn_view_vehicle_<?=$rtList['sdd_id']?>").click(function(){
                                    $("#vehicle_details_<?=$rtList['sdd_id']?>").slideToggle(500);
                                    $(".btn_view_vehicle_<?=$rtList['sdd_id']?>").addClass("hidden");
                                    $(".btn_hide_vehicle_<?=$rtList['sdd_id']?>").removeClass("hidden");
                                  });
                                  $(".btn_hide_vehicle_<?=$rtList['sdd_id']?>").click(function(){
                                    $("#vehicle_details_<?=$rtList['sdd_id']?>").slideToggle(500);
                                    $(".btn_view_vehicle_<?=$rtList['sdd_id']?>").removeClass("hidden");
                                    $(".btn_hide_vehicle_<?=$rtList['sdd_id']?>").addClass("hidden");
                                  });
                                </script>
                                <script>
                                  $(".btn_view_cancelation_<?=$rtList['sdd_id']?>").click(function(){
                                    $("#cancelation_charge_<?=$rtList['sdd_id']?>").slideToggle(500);
                                    $(".btn_view_cancelation_<?=$rtList['sdd_id']?>").addClass("hidden");
                                    $(".btn_hide_cancelation_<?=$rtList['sdd_id']?>").removeClass("hidden");
                                  });
                                  $(".btn_hide_cancelation_<?=$rtList['sdd_id']?>").click(function(){
                                    $("#cancelation_charge_<?=$rtList['sdd_id']?>").slideToggle(500);
                                    $(".btn_view_cancelation_<?=$rtList['sdd_id']?>").removeClass("hidden");
                                    $(".btn_hide_cancelation_<?=$rtList['sdd_id']?>").addClass("hidden");
                                  });
                                </script>
                              </div>
                            </td>
                          </tr>
                        <?php } } } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              <?php } ?>
            </div>
            <div class="col-md-4 col-md-offset-4">
              <input type="hidden" name="ownward_trip" id="ownward_trip" value="">
              <input type="hidden" name="no_of_seat" id="no_of_seat" value="<?=$no_of_seat?>">
              <input type="hidden" name="seat_price" id="seat_price" value="">
              <input type="hidden" name="pickup_point" id="pickup_point" value="">
              <input type="hidden" name="drop_point" id="drop_point" value="">
              <input type="hidden" name="seat_type" id="seat_type" value="">
              <input type="hidden" name="trip_source" id="trip_source" value="<?=$trip_source?>">
              <input type="hidden" name="trip_destination" id="trip_destination" value="<?=$trip_destination?>">
              <input type="hidden" name="distance" id="distance" value="">
              <input type="hidden" name="duration" id="duration" value="">
              <input type="hidden" name="ownward_currency_sign" id="ownward_currency_sign" value="">
              <input type="hidden" name="ownward_currency_id" id="ownward_currency_id" value="">
              <input type="hidden" name="cat_id" id="cat_id" value="281">
              <!-- return trip -->
              <input type="hidden" name="return_trip" id="return_trip" value="0">
              <input type="hidden" name="return_seat_price" id="return_seat_price" value="">
              <input type="hidden" name="return_pickup_point" id="return_pickup_point" value="">
              <input type="hidden" name="return_drop_point" id="return_drop_point" value="">
              <input type="hidden" name="return_seat_type" id="return_seat_type" value="">
              <input type="hidden" name="return_trip_source" id="return_trip_source" value="<?=$trip_destination?>">
              <input type="hidden" name="return_trip_destination" id="return_trip_destination" value="<?=$trip_source?>">
              <input type="hidden" name="return_currency_sign" id="return_currency_sign" value="">
              <input type="hidden" name="onward_pickup_time" id="onward_pickup_time" value="">
              <input type="hidden" name="return_pickup_time" id="return_pickup_time" value="">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  $("input[name='goto_return']").change(function() {
    if(this.checked) {
      $('.nav-tabs a[href="#menu1"]').tab('show');
      $('.hometab').removeClass("btn-default");
    }
  });
</script>
<script>
  $(function() {
    $("a[name='create_link']").on('click',function(e) { e.preventDefault();
      var redirect = $(this).data('redirect');
      var cat_id = $(this).data('cat_id');
      var newForm = $('<form>', {
        'action': <?= json_encode(base_url());?> + redirect,
        'method': 'post'
      }).append($('<input>', {
        'name': 'category_id',
        'value': cat_id,
        'type': 'hidden'
      }));
      $(document.body).append(newForm);
      newForm.submit();
    }); 

    $('#orderTableData').dataTable( {
      "paging":   false,
      "ordering": false,
      "info":     false
    } );

    $('#orderTableData1').dataTable( {
      "paging":   false,
      "ordering": false,
      "info":     false
    } );

    $('#advanceBtn').click(function() {
      if($('#advanceSearch').css('display') == 'none') {
        $('#advanceSearch').show("slow");
        $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Hide Filters'))?>);
      } else {
        $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Show Filters'))?>);
        $('#advanceSearch').hide("slow");
        $('#basicSearch').show("slow");
      }
      return false;
    });
  });
</script>
<script>
  $( "#basicFilter" ).click(function( e ) {
    e.preventDefault();
    var trip_source = $('#trip_source').val();
    var trip_destination = $('#trip_destination').val();
    var journey_date = $('#journey_date').val();
    var no_of_seat = $('#no_of_seat').val();
    
    if(trip_source == '' || trip_source == undefined) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select starting city!')?>", "error");
    } else if(trip_destination == '' || trip_destination == undefined) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select destination city!')?>", "error");
    } else if(journey_date == '' || journey_date == undefined) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Select journey date!')?>", "error");
    } else if(no_of_seat == '' || no_of_seat == undefined) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('select number of seats!')?>", "error");
    } else if(no_of_seat > 10) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('The maximum number of seat is 40')?>", "error");
    } else if(no_of_seat <= 0) {
      swal("<?=$this->lang->line('Error!')?>", "<?=$this->lang->line('Number of passengers cannot be zero (0).')?>", "error");
    } else { $("#frmBasicSearch").submit(); }
  });
</script>
<script>
  $('.cancelorder').click(function () { 
    var id = this.id;
    swal({ 
      title: <?= json_encode($this->lang->line('are_you_sure')) ?>, 
      text: "", 
      type: "warning", 
      showCancelButton: true, 
      confirmButtonColor: "#DD6B55", 
      confirmButtonText: <?= json_encode($this->lang->line('yes')) ?>, 
      cancelButtonText: <?= json_encode($this->lang->line('no')) ?>, 
      closeOnConfirm: false, 
      closeOnCancel: false, 
    }, 
    function (isConfirm) { 
      if (isConfirm) { 
        $.post('cancel-order', {id: id}, 
        function(res){
          if($.trim(res) == "success") {
            swal(<?= json_encode($this->lang->line('deleted'))?>, "", "success");
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal(<?= json_encode($this->lang->line('failed'))?>, "", "error");  } 
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled'))?>, "", "error");  } 
    }); 
  });
  
    $("#trip_source").on('change', function() {
       $('#trip_destination').select2('open');
    });
</script>