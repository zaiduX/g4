  <style>
  #attachment, .attachment { 
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px; 
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0); 
  }
  .entry:not(:first-of-type) {
    margin-top: 10px;
  }

  .gonagoo_custom {
    margin-top: 8px;
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-family: auto;
  }
  .hpanel.horange .panel-body {
    border-top: 2px solid #e67e22;
}
.hpanel.hblue .panel-body {
    border-top: 2px solid #3498db;
}
.hpanel .panel-body {
    background: #fff;
    border: 1px solid #eaeaea;
    border-radius: 2px;
    padding: 20px;
    position: relative;
}
.content {
    padding: 25px 40px 40px 40px;
    min-width: 320px;
}

</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">      
<div class="content" style="padding-top: 30px;">
  <div class="hpanel hblue">
    <div class="panel-body" style="padding: 0px;background-color: #ececec ">
      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display: block;padding-right: 20px">


        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <h2 style="margin-top: 15px; margin-bottom: 5px;" class="font-uppercase"><i class="fa fa-tag"></i> Ref #GS-<?=$job_details['job_id']?>&nbsp;&nbsp;&nbsp;<?=$job_details['job_title']?></h2>
          <hr style="margin-bottom: 10px;margin-top: 10px;">
          <div class="row" style="padding-left: 0px;margin-bottom: 10px;">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;">
                <i class="fa fa- fa-clock-o"></i> <?= $this->lang->line('posted on'); ?>: <?=date('D, d M y', strtotime($job_details['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-map-marker"></i>&nbsp;<?=$job_details['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-dot-circle-o"></i> <?=$this->lang->line('Proposals')." ".$job_details['proposal_counts']?>&nbsp;&nbsp;&nbsp;&nbsp;
                <strong style="color: #3498db;"><i class="fa fa-gears"></i> <?= $this->lang->line('status'); ?>: <?= strtoupper(str_replace('_', ' ', $job_details['job_status']))?></strong>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">
          <div class="hpanel horange" style="margin-bottom: 15px;">
            <div class="panel-body no-padding">
              <div class="row">
              </div>
            </div>
          </div>

          <h3><?=$this->lang->line('description')?></h3>
          <lable><?=$job_details['job_desc']?></lable><br/>
          <h6><strong><?=$this->lang->line('Attachments')?></strong></h6>
                 
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0; display: inline-flex;">
              
              <?php if($job_details["attachment_1"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 1"?>" target="_blank" href="<?=base_url($job_details["attachment_1"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_2"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 2"?>" target="_blank" href="<?=base_url($job_details["attachment_2"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_3"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 3"?>" target="_blank" href="<?=base_url($job_details["attachment_3"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_4"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 4"?>" target="_blank" href="<?=base_url($job_details["attachment_4"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_5"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 5"?>" target="_blank" href="<?=base_url($job_details["attachment_5"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <?php if($job_details["attachment_6"] != "NULL"){ ?>
                <h1><a title="<?=$this->lang->line('attachment')." 6"?>" target="_blank" href="<?=base_url($job_details["attachment_6"])?>"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></h1>
              <?php } ?>
            </div>
 
        </div>
      </div>

      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 panel-footer borders" style="display: block;padding-left: 0px; padding-right: 15px; height:100%">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right:0px;padding-left:0px">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
              <?=$this->lang->line('Ending in days')?> <br/>
              <h4><strong>
                <?=$ending?>
              </strong></h4>
            </div>
            <?php if($job_details['job_type']==0){  ?> 
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
              <?=$job_details['work_type']?><br/>
              <h4><strong>
                <?=($job_details['work_type']!='Per Hour')?$job_details['currency_code'].' '.$job_details['budget']:$job_details['currency_code'].' '.$job_details['budget']."<small>/hr</small>"?>
              </strong></h4>
            </div>
          <?php }else{ ?>
             <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center"><?=$this->lang->line('Troubleshoot');?>
              <?php if($job_details['immediate']){
                echo $this->lang->line('On Date');}else{
                   echo $this->lang->line('On');} ?><br/>
              <h4><strong>
                <?=($job_details['immediate'])?date("d M Y" , strtotime($job_details['complete_date'])):$this->lang->line('Immediate');?>
              </strong></h4>
            </div>

          <?php } ?>
          </div>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:25px;">
            <form action="login-provider" method="get">
            <input type="hidden" name="job_id" value="<?=$job_details['job_id']?>">
            <button class="btn btn-info" id="btn_send_1" type="submit" style="width: 100%;"><?=$this->lang->line('Send Proposal')?></button>
          </div>
          </form>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0;padding-right: 0px;padding-top: 20px;padding-bottom: 20px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
              <img alt="logo" class="img-circle m-b-xs img-responsive" src="<?=base_url($job_customer_profile['avatar_url'])?>">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12" style="">
              <h4 style="color: #3498db"><strong><?=$job_customer_profile['firstname'].' '.$job_customer_profile['lastname']?></strong></h4>
              <h5><i class="fa fa-map-marker"></i> <?=$this->api->get_country_name_by_id($job_customer_profile['country_id'])?></h5>
              <h5 style="color:#3ceb3f">
                <?php
                    $to_time = strtotime($job_customer_profile['last_login_datetime']);
                    date_default_timezone_set(ini_get('date.timezone'));
                    $from_time = strtotime(date('Y-m-d h:i:s'));
                  $min = round(((abs($to_time - $from_time) / 60) - 270),2);
                  if( $min > 10 ) { echo '<span class="text-danger"><i class="fa fa-circle"></i> '.$this->lang->line('Offline').'</span>'; }
                  else { echo '<span class="text-success" style="color: #3ceb3f;"><i class="fa fa-circle"></i> '.$this->lang->line('Online').'</span>'; }
                ?>
              </h5>
            </div>
          </div><br/>
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 panel-body" style="padding-left: 0px;padding-top: 0px;padding-bottom: 0px;padding-right: 0px;">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="height: 75px;padding-left: 5px;padding-right: 5px;">
              <?=$this->lang->line('Project completed')?><br/><br/>
              <h5><strong><?=$completed_job?></strong></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="height: 75px;padding-left: 5px;padding-right: 5px;">
              <?=$this->lang->line('Freelancers worked with')?><br/><br/>
              <h5><strong>-</strong></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="height: 75px;padding-left: 5px;padding-right: 5px;">
              <?=$this->lang->line('Project Awarded')?><br/><br/>
              <h5><strong>0%</strong></h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" style="height: 75px;padding-left: 5px;padding-right: 5px;">
              <?=$this->lang->line('Last project')?><br/><br/>
              <h5><strong>
                <?php
                  $yrdata= strtotime($last_job['cre_datetime']);
                  echo date('d M Y', $yrdata);
                ?>
              </strong></h5>
            </div> 
          </div>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 0px;">
        </div>
        <?php if($job_details['job_type']): ?>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 16px;">
           <label><?= $this->lang->line('location'); ?></label>
            <input type="text" name="toMapID" class="form-control" id="toMapID" placeholder="<?=$this->lang->line('search_address_in_map_booking')?>" value="<?=$job_details['address']?>" disabled>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;font-size: 16px;">
          <div id="map" class="map_canvas" style="width: 100%; height: 200px;"></div>
        </div>
      <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<script>
  $(function () {
  $("#toMapID").geocomplete({
    map:".map_canvas",
    location: "<?=$job_details['address']?>",
    mapOptions: { zoom: 11, scrollwheel: true, },
    markerOptions: { draggable: false, },
    details: "form",
    detailsAttribute: "to-data-geo", 
    types: ["geocode", "establishment"],
  });
  });
</script>