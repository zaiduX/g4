<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body">
      <a class="small-header-action" href="">
        <div class="clip-header">
          <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= $this->config->item('base_url') . 'user-panel-services/dashboard-services'; ?>"><?= $this->lang->line('dash'); ?>
          </a></li>
          <li class="active"><span><?= $this->lang->line('Account login security'); ?>
          </span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">
        <i class="fa fa-lock fa-2x text-muted"></i> <?= $this->lang->line('Account login security'); ?>
      </h2>
      <small class="m-t-md"> <?= $this->lang->line('Login to access Account section'); ?></small>
    </div>
  </div>
</div>
<div class="content">
  <div class="form-group">
    <div class="col-md-12 text-center">
      <?php if($this->session->flashdata('error')):  ?>
      <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
      <?php endif; ?>
      <?php if($this->session->flashdata('success')):  ?>
      <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
      <?php endif; ?>  
    </div>   
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="hpanel">
          <div class="panel-body">
            <form action="<?=base_url('user-panel-services/account-login-confirm')?>" method="POST">
              <div style="padding-left: 340px;">
                <div class="col-md-6">
                  <input type="hidden" name="section" value="<?=$section?>">
                  <input required="true" type="text" class="form-control" value="<?=$customer['firstname']." ".$customer['lastname']?>">
                </div>
                <br><br><br>
                <div class="col-md-6">
                  <input type="password" required="true" class="form-control" placeholder="<?=$this->lang->line('password')?>" name="password">
                </div>
                <br><br>
                <div class="col-md-3 text-left" style="padding-top: 10px;">
                  <a href="<?= base_url('user-panel-services/dashboard-services'); ?>" class="btn btn-outline btn-info" ><i class="fa fa-arrow-left"></i><?= $this->lang->line('dash'); ?></a>
                </div>
                <div class="col-md-3 text-right" style="padding-top: 10px;">
                  <input type="submit" class="btn btn-outline btn-success" value="<?= $this->lang->line('login'); ?>">
                </div>
              </div>
            </form>
          </div>
      </div>
    </div>
  </div>
</div>
