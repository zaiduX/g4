<style type="text/css">
  .table > tbody > tr > td {
      border-top: none;
    }
  .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
      padding-left: 0px; padding-right: 0px;
    }
  .dataTables_filter {
     display: none;
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('My Offers'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs">
        <i class="fa fa-gift fa-2x text-muted"></i> <?= $this->lang->line('My Offers'); ?>&nbsp;&nbsp;&nbsp;
        <a class="btn btn-outline btn-info" href="<?=base_url('user-panel-services/provider-add-offers'); ?>"><i class="fa fa-plus"></i> <?= $this->lang->line('add_new'); ?></a>
      </h2>
      <small class="m-t-md"><?= $this->lang->line('Manage your offer details. Offer activation within 2 business days.'); ?></small>
    </div>
  </div>
</div>

<div class="content" style="padding-top: 65px;">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="hpanel" style="margin-bottom: 0px; margin-left: -8px; margin-right: -7px; margin-top: 15px;">
        <div class="panel-body">
          <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-xs-12 text-center">
              <h5 style="margin:10px; display: <?=($filter == 'advance')?'none':''?>" id="searchlabel"><i class="fa fa-search"></i> <?= $this->lang->line('search'); ?></h5>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-7 col-sm-4 col-xs-12">
              <input type="text" id="searchbox" class="form-control" placeholder="<?= $this->lang->line('Type here to search for social media, sales, seo, marketing, web design...'); ?>" style="display: <?=($filter == 'advance')?'none':''?>" />
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-xs-12 text-right">
              <a class="btn btn-link btn-block advanceBtn" id="advanceBtn"><i class="fa fa-search"></i> <?= $filter == 'advance' ? $this->lang->line('basic_search') : $this->lang->line('advance_search') ?></a>
            </div>
          </div>
          <div class="row" id="advanceSearch" style="<?= ($filter == 'basic') ? 'display:none' : '' ?>;">
            <form action="<?= base_url('user-panel-services/provider-offers') ?>" method="post" id="offerFilter">
              <input type="hidden" name="filter" value="advance" />
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-globe"></i> <?= $this->lang->line('Country'); ?></label>
                <select id="country_id" name="country_id" class="form-control select2" autocomplete="none">
                  <option value="0"><?= $this->lang->line('Select Country'); ?></option>
                  <?php foreach ($countries as $country): ?>
                    <option value="<?= $country['country_id'] ?>"<?php if(isset($country_id) && $country_id == $country['country_id']) { echo 'selected'; } ?>><?= $country['country_name']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-map-marker"></i> <?= $this->lang->line('state'); ?></label>
                <select id="state_id" name="state_id" class="form-control select2" autocomplete="none">
                  <?php if(isset($country_id)) { 
                    if(!isset($state_id) || $state_id <= 0) { ?>
                      <option value="0"><?= $this->lang->line('select_state'); ?></option>
                    <?php } ?>
                    <?php foreach ($states as $state): ?>
                      <option value="<?= $state['state_id'] ?>"<?php if($state_id == $state['state_id']) { echo 'selected'; } ?>><?= $state['state_name']; ?></option>
                    <?php endforeach ?> 
                  <?php } else { ?>
                    <option value="0"><?= $this->lang->line('select_state'); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-address-book-o"></i> <?= $this->lang->line('city'); ?></label>
                <select id="city_id" name="city_id" class="form-control select2" autocomplete="none">
                  <?php if(isset($city_id) || $city_id > 0) { 
                    if($city_id == 0) { ?>
                      <option value="0"><?= $this->lang->line('select_city'); ?></option>
                    <?php } ?>
                    <?php foreach ($cities as $city): ?>
                      <option value="<?= $city['city_id'] ?>" <?php if($city_id == $city['city_id']) { echo 'selected'; } ?>><?= $city['city_name']; ?></option>
                    <?php endforeach ?> 
                  <?php } else { ?>
                    <option value="0"><?= $this->lang->line('select_city'); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-sort-amount-asc"></i> <?=$this->lang->line('Sort'); ?></label>
                <select class="form-control m-b select2" name="sort_by"  id="sort_by">
                  <option value=""><?=$this->lang->line('by'); ?></option>
                  <option value="top" <?=(isset($sort_by) && $sort_by == 'top')?'selected':''?>><?=$this->lang->line('Top sales'); ?></option>
                  <option value="rating" <?=(isset($sort_by) && $sort_by == 'rating')?'selected':''?>><?=$this->lang->line('ratings'); ?></option>
                  <option value="low" <?=(isset($sort_by) && $sort_by == 'low')?'selected':''?>><?=$this->lang->line('Price (low to high)'); ?></option>
                  <option value="high" <?=(isset($sort_by) && $sort_by == 'high')?'selected':''?>><?=$this->lang->line('Price (high to low)'); ?></option>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-angle-double-right"></i> <?= $this->lang->line('category_type'); ?></label>
                <select id="cat_type_id" name="cat_type_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category_type'); ?>">
                  <option value="0"><?= $this->lang->line('select_category_type'); ?></option>
                  <?php foreach ($category_types as $ctypes): ?>
                    <option value="<?= $ctypes['cat_type_id'] ?>" <?php if(isset($cat_type_id) && $cat_type_id == $ctypes['cat_type_id']) { echo 'selected'; } ?>><?= $ctypes['cat_type']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-angle-right"></i> <?= $this->lang->line('category'); ?></label>
                <select id="cat_id" name="cat_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category'); ?>">
                  <?php if(isset($cat_type_id)) { 
                    if(!isset($cat_id) || $cat_id <= 0) { ?>
                      <option value="0"><?= $this->lang->line('select_category'); ?></option>
                    <?php } ?>
                    <?php foreach ($categories as $category): ?>
                      <option value="<?= $category['cat_id'] ?>"<?php if($cat_id == $category['cat_id']) { echo 'selected'; } ?>><?= $category['cat_name']; ?></option>
                    <?php endforeach ?> 
                  <?php } else { ?>
                    <option value="0"><?= $this->lang->line('select_category'); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-clock-o"></i> <?= $this->lang->line('Delivery Time'); ?></label>
                <select class="form-control m-b select2" name="delivered_in"  id="delivered_in">
                  <option value=""> <?= $this->lang->line('Select day'); ?> </option>
                  <?php for ($i=1; $i <= 5 ; $i++) : ?>
                    <option value="<?=$i?>" <?php if(isset($delivered_in) && $delivered_in == $i) { echo 'selected'; } ?>><?php echo ($i==1)?$i.' '.$this->lang->line('Day'):$i.' '.$this->lang->line('Days'); ?></option>
                  <?php endfor; ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-money"></i> <?= $this->lang->line('Price between'); ?></label>
                <div class="input-range input-group" style="height: 34px;">
                  <input type="number" class="input-sm form-control" name="start_price" min="0" value="<?php if(isset($start_price)) { echo $start_price; } ?>" style="height: 34px;" />
                  <span class="input-group-addon"><?= $this->lang->line('to'); ?></span>
                  <input type="number" class="input-sm form-control" name="end_price" min="0" value="<?php if(isset($end_price)) { echo $end_price; } ?>" style="height: 34px;" />
                </div>
              </div>
              <div class="col-xl-9 col-lg-9 col-md-8 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px; padding-top: 15px;">
                <div class="checkbox checkbox-default checkbox-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="checkbox" id="latest" name="latest" <?= ( isset($latest) && $latest == "on")? "checked" : '' ;?> >
                  <label for="latest"> <?= $this->lang->line('Show latest offers'); ?> </label>
                </div>
                <div class="radio radio-info radio-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="radio" name="active_status" <?= ( isset($active_status) && $active_status == "active")? "checked" : '' ;?> value="active" />
                  <label for="active_status"> <?= $this->lang->line('Show active only'); ?> </label>
                </div>
                <div class="radio radio-info radio-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="radio" name="active_status" <?= ( isset($active_status) && $active_status == "inactive")? "checked" : '' ;?> value="inactive" />
                  <label for="active_status"> <?= $this->lang->line('Show in-active only'); ?> </label>
                </div>
                <div class="radio radio-warning radio-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="radio" name="running_status" <?= ( isset($running_status) && $running_status == "paused")? "checked" : '' ;?> value="paused" />
                  <label for="running_status"> <?= $this->lang->line('Show paused only'); ?> </label>
                </div>
                <div class="radio radio-warning radio-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="radio" name="running_status" <?= ( isset($running_status) && $running_status == "resumed")? "checked" : '' ;?> value="resumed" />
                  <label for="running_status"> <?= $this->lang->line('Show running only'); ?> </label>
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12 text-right" style="padding-right: 15px; padding-left: 15px;">
                <br />
                <button class="btn btn-success btn-sm" type="submit"><i class="fa fa-search"></i> <?= $this->lang->line('apply_filter'); ?></button>
                <a class="btn btn-warning btn-sm" href="<?= base_url('user-panel-services/provider-offers') ?>"><i class="fa fa-ban"></i> <?= $this->lang->line('reset'); ?></a>
              </div>
            </form>
          </div>

          <?php if($this->session->flashdata('error')):  ?>
            <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
          <?php endif; ?>
          <?php if($this->session->flashdata('success')):  ?>
            <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

      <?php if(sizeof($offers) > 0) { ?>
        <div class="hpanel">
          <table id="addressTableData" class="table hpanel">
            <thead>
              <tr>
                <th class="hidden"></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($offers as $offer) { ?>
                <tr class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: -25px;">
                  <td style="width: inherit;">
                    <div class="hpanel <?php if($offer['admin_approval'] == 0) echo 'hred'; else if ($offer['offer_pause_resume'] == 0) echo 'horange'; else echo 'hblue'?> contact-panel">
                      <div class="panel-body text-center" style="padding: 5px 10px 5px 10px; <?=($offer['admin_approval'] == 0)?'background-color: #dcdcdc':''?>">
                        <span class="pull-right" style="display: inline-flex;">
                          <form action="<?=base_url('user-panel-services/provider-edit-offer')?>" method="post">
                            <input type="hidden" name="offer_id" value="<?=$offer['offer_id'];?>">
                            <button type="submit" style="padding: 0px;" title="<?= $this->lang->line('Edit offer details'); ?>" class="btn-link"><font size="5px" style="color: green"><i class="fa fa-pencil"></i></font></button>
                          </form>&nbsp;
                          <button style="padding: 0px;" title="<?= $this->lang->line('Delete offer'); ?>" class="btn-link delete_offer" data-id="<?=$offer['offer_id'];?>"><font size="5px" style="color: red"><i class="fa fa-trash-o"></i></font></button>
                        </span>
                        <span class="pull-left">
                          <?php if($offer['admin_approval'] == 1) { ?>
                            <button style="padding: 0px;" title="<?= $this->lang->line('Active offer'); ?>" class="btn-link" data-id="<?= $offer['offer_id'];?>"><font size="5px" style="color: skyblue"><i class="fa fa-check-circle"></i></font></button>
                          <?php } else if($offer['admin_approval'] == 2) { ?>
                            <button style="padding: 0px;" title="<?= $this->lang->line('Offer rejected by admin'); ?>" class="btn-link"><font size="5px" style="color: red"><i class="fa fa-times-circle"></i></font></button>
                          <?php } else { ?>
                            <button style="padding: 0px;" title="<?= $this->lang->line('Waiting for approval'); ?>" class="btn-link" data-id="<?= $offer['offer_id'];?>"><font size="5px" style="color: blue"><i class="fa fa-clock-o"></i></font></button>
                          <?php } ?>

                          <?php if($offer['offer_pause_resume'] == 1) { ?>
                            <button style="padding: 0px;" title="<?= $this->lang->line('Pause offer'); ?>" class="btn-link pause_offer" data-id="<?=$offer['offer_id'];?>"><font size="5px" style="color: orange"><i class="fa fa-pause-circle"></i></font></button>
                          <?php } else { ?>
                            <button style="padding: 0px;" title="<?= $this->lang->line('Resume offer'); ?>" class="btn-link resume_offer" data-id="<?=$offer['offer_id'];?>"><font size="5px" style="color: blue"><i class="fa fa-play-circle"></i></font></button>
                          <?php } ?>
                        </span>
                        <img alt="logo" class="img-circle m-b-xs" src="<?=base_url($operator['avatar_url'])?>">
                        <h5 class="m-b-xs"><a href=""> <?=$operator['company_name']?> </a></h5>
                        <div class="text-muted m-b-xs"><i class="fa fa-map-marker"></i> <?=$offer['country_name']?></div>
                        <h5 class="" style="overflow-wrap: break-word; line-height: 1.5; height: 4.5em; max-height: 4.5em; overflow: hidden; color: #3498db; margin-bottom: 10px; margin-top: 10px;"><?=ucfirst($offer['offer_title'])?></h5>
                        <h6 class="text-muted text-left" style="padding-left: 5px; overflow-wrap: break-word; height: 3.5em; max-height: 3.5em; overflow: hidden;" title="<?=$this->lang->line('Category type & Sub Category')?>"><strong><i class="fa fa-angle-double-right"></i> <?=$offer['cat_type']?></strong> <i class="fa fa-angle-right"></i> <?=$offer['cat_name']?></h6>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left" style="padding-left: 5px; padding-right: 0px; padding-top: 5px;">
                          <h6 class="text-muted font-bold m-b-xs" title="<?=$this->lang->line('User Rating and Reviews')?>"><i class="fa fa-thumbs-o-up"></i> <?=$offer['rating_count']?> [<?=$offer['review_count']?>] 
                          </h6>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left" style="padding-left: 5px; padding-right: 0px; padding-top: 5px;">
                          <div class="text-muted font-bold m-b-xs"><i class="fa fa-shopping-basket"></i> <?=$this->lang->line('Sales')?> <?=$offer['sales_count']?>
                          </div>
                        </div>
                      </div>
                      <div class="panel-footer contact-footer" style="padding-top: 0px; padding-bottom: 0px; <?=($offer['admin_approval'] == 0)?'background-color: #dcdcdc':'background-color: #FFF'?>">
                        <div class="row">
                          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12 text-left" style="padding-left: 15px; padding-right: 0px;">
                            <h3 style="color: #3498db;" title="<?=$this->lang->line('Offer Price')?>"><strong><?=$offer['currency_code']?> <?=$offer['offer_price']?></strong></h3>
                          </div>
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left: 5px; padding-right: 0px;">
                            <div class="contact-stat">
                              <form action="<?= base_url('user-panel-services/provider-offer-details'); ?>" method="post">
                                <input type="hidden" name="offer_id" value="<?=$offer['offer_id']?>">
                                <a href="<?= base_url('user-panel-services/provider-offer-details/'.md5($offer['offer_id'])); ?>" class="btn btn-sm btn-warning btn-outline" title="<?=$this->lang->line('view_details')?>"><?=$this->lang->line('View')?></a>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      <?php } else { ?>
        <div class="hpanel horange" style="margin-left: -8px; margin-right: -7px; margin-top: 10px;">
          <div class="panel-body text-center">
            <h5><?= $this->lang->line('No offers found.'); ?></h5>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
<script>
  $('.pause_offer').click(function () { 
    var id = $(this).attr("data-id")
    swal({ 
      title: <?= json_encode($this->lang->line('are_you_sure')); ?>, 
      text: <?= json_encode($this->lang->line('Offer will be removed from marketplace.')); ?>, 
      type: "warning", 
      showCancelButton: true, 
      confirmButtonColor: "#DD6B55", 
      confirmButtonText: <?= json_encode($this->lang->line('yes')); ?>, 
      cancelButtonText: <?= json_encode($this->lang->line('no')); ?>, 
      closeOnConfirm: false, 
      closeOnCancel: false, 
    }, 
    function (isConfirm) {    
      if (isConfirm) {
        $.post('provider-pause-offer', {id: id}, 
        function(res) { 
          console.log(res);
          if($.trim(res) == "success") { 
            swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Offer paused and removed from marketplace.')); ?>, "success");      
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to pause offer. Try again...')); ?>, "error");  } 
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled')); ?>, <?= json_encode($this->lang->line('No change.')); ?>, "error");  } 
    }); 
  });
  $('.resume_offer').click(function () { 
    var id = $(this).attr("data-id")
    swal({ 
      title: <?= json_encode($this->lang->line('are_you_sure')); ?>, 
      text: <?= json_encode($this->lang->line('Offer will be visible in marketplace.')); ?>, 
      type: "warning", 
      showCancelButton: true, 
      confirmButtonColor: "#DD6B55", 
      confirmButtonText: <?= json_encode($this->lang->line('yes')); ?>, 
      cancelButtonText: <?= json_encode($this->lang->line('no')); ?>, 
      closeOnConfirm: false, 
      closeOnCancel: false, 
    }, 
    function (isConfirm) {    
      if (isConfirm) {
        $.post('provider-resume-offer', {id: id}, 
        function(res) { 
          console.log(res);
          if($.trim(res) == "success") { 
            swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Offer resumed and visible in marketplace.')); ?>, "success");      
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to resume offer. Try again...')); ?>, "error");  } 
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled')); ?>, <?= json_encode($this->lang->line('No change.')); ?>, "error");  } 
    }); 
  });
  $('.delete_offer').click(function () { 
    var id = $(this).attr("data-id")
    swal({ 
      title: <?= json_encode($this->lang->line('are_you_sure')); ?>, 
      text: <?= json_encode($this->lang->line('you_will_not_be_able_to_recover_this_record')); ?>, 
      type: "warning", 
      showCancelButton: true, 
      confirmButtonColor: "#DD6B55", 
      confirmButtonText: <?= json_encode($this->lang->line('yes')); ?>, 
      cancelButtonText: <?= json_encode($this->lang->line('no')); ?>, 
      closeOnConfirm: false, 
      closeOnCancel: false, 
    }, 
    function (isConfirm) {    
      if (isConfirm) {
        $.post('provider-delete-offer', {id: id}, 
        function(res) { 
          console.log(res);
          if($.trim(res) == "success") { 
            swal(<?= json_encode($this->lang->line('success')); ?>, <?= json_encode($this->lang->line('Offer deleted successfully.')); ?>, "success");      
            setTimeout(function() { window.location.reload(); }, 2000); 
          } else {  swal(<?= json_encode($this->lang->line('failed')); ?>, <?= json_encode($this->lang->line('Unable to delete offer. Try again...')); ?>, "error");  } 
        }); 
      } else {  swal(<?= json_encode($this->lang->line('canceled')); ?>, <?= json_encode($this->lang->line('No change.')); ?>, "error");  } 
    }); 
  });
</script>
<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
    //$('#city_id').attr('disabled', true);

    $.ajax({
      url: "get-state-by-country-id",
      type: "POST",
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        console.log(res);
        $('#state_id').attr('disabled', false);
        $('#state_id').empty();
        $('#city_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('select_state')?></option>");
        $('#state_id').select2("val",$('#state_id option:nth-child(1)').val());
        $('#city_id').append("<option value='0'><?=$this->lang->line('select_city')?></option>");
        $('#city_id').select2("val",$('#city_id option:nth-child(1)').val());
        $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
        $('#state_id').focus();
      },
      beforeSend: function(){
        $('#state_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('loading')?></option>");
      },
      error: function(){
        //$('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('no_option')?></option>");
      }
    })
  });
  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $.ajax({
      type: "POST",
      url: "get-cities-by-state-id",
      data: { state_id: state_id },
      dataType: "json",
      success: function(res){
        $('#city_id').attr('disabled', false);
        $('#city_id').empty();
        $('#city_id').append("<option value='0' selected='selected'><?=$this->lang->line('select_city')?></option>");
        $('#city_id').select2("val",$('#city_id option:nth-child(1)').val());
        $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
        $('#city_id').focus();
      },
      beforeSend: function(){
        $('#city_id').empty();
        $('#city_id').append("<option value='0'><?=$this->lang->line('loading')?></option>");
      },
      error: function(){
        //$('#city_id').attr('disabled', true);
        $('#city_id').empty();
        $('#city_id').append("<option value='0'><?=$this->lang->line('no_option')?></option>");
      }
    })
  });
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-categories-by-type", 
      data: { type_id: cat_type_id },
      dataType: "json",
      success: function(res) {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('select_category')?></option>");
        $.each( res, function() {
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function() {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('loading')?></option>");
      },
      error: function() {
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('no_options')?></option>");
      }
    })
  });
  $("#offerFilter").validate({
    ignore: [], 
    rules: {
      price: { number : true },
    },
    messages: {
      price: { number : <?= json_encode($this->lang->line('number_only')); ?> },
    },
  });
  $('#advanceBtn').click(function() {
    if($('#advanceSearch').css('display') == 'none') {
      $('#basicSearch').hide("slow");
      $('#advanceSearch').show("slow");
      $('#advanceBtn').html("<i class='fa fa-search'></i>" + <?= json_encode($this->lang->line('basic_search'))?>);
      $('#searchbox').hide("slow");
      $('#searchlabel').hide("slow");
    } else {
      $('#advanceBtn').html("<i class='fa fa-search'></i>" + <?= json_encode($this->lang->line('advance_search'))?>);
      $('#advanceSearch').hide("slow");
      $('#basicSearch').show("slow");
      $('#searchbox').show("slow");
      $('#searchlabel').show("slow");
    }
    return false;
  });
</script>