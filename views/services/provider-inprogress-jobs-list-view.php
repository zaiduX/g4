<style type="text/css">
  .table > tbody > tr > td {
    border-top: none;
  }
  .dataTables_filter {
   display: none;
  }
  .hoverme {
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.5);
  }
  .hoverme:hover {
    -webkit-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    -moz-box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
    box-shadow: 0px 0px 7px 0px rgba(0,0,0,2.0);
  }
  .btnMenu:hover {
    text-decoration: none;
    background-color: #ffb606;
    color: #FFF;
  }
</style>

<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header"><i class="fa fa-arrow-up"></i></div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('My Projects'); ?></span></li>
        </ol>
      </div>
      <h3 class="">
        <i class="fa fa-briefcase text-muted"></i> <strong><?= $this->lang->line('My Projects'); ?> - <?= $this->lang->line('In-progress'); ?></strong>&nbsp;&nbsp;&nbsp;
        <a class="btn btn-outline btn-info hidden" href="<?=base_url('user-panel-services/my-favorite-offers'); ?>"><i class="fa fa-heart"></i> <?= $this->lang->line('Favorite offers'); ?></a>
      </h3>
    </div>
  </div>
</div>
<div class="content" style="padding-top: 45px;">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="hpanel hoverme" style="margin: 25px -7px 0px -7px; background-color:white; border-radius: 10px;">
        <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
          <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-9 col-sm-9 col-xs-12">
              <input type="text" id="searchbox" class="form-control" placeholder="<?= $this->lang->line('Type here to search for quick search...'); ?>" />
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-xs-12">
              <a class="btn btn-link btn-block advanceBtn" id="advanceBtn"><i class="fa fa-filter"></i> <?= $filter == 'advance' ? $this->lang->line('Hide Filters') : $this->lang->line('Show Filters') ?></a>
            </div>
          </div>
          <div class="row" id="advanceSearch" style="<?= ($filter == 'basic') ? 'display:none' : '' ?>; margin-top: 10px;">
            <form action="<?= base_url('user-panel-services/provider-inprogress-jobs') ?>" method="get" id="offerFilter">
              <input type="hidden" name="filter" value="advance" />
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-angle-double-right"></i> <?= $this->lang->line('category_type'); ?></label>
                <select id="cat_type_id" name="cat_type_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category_type'); ?>">
                  <option value="0"><?= $this->lang->line('select_category_type'); ?></option>
                  <?php foreach ($category_types as $ctypes): ?>
                    <option value="<?= $ctypes['cat_type_id'] ?>" <?php if(isset($cat_type_id) && $cat_type_id == $ctypes['cat_type_id']) { echo 'selected'; } ?>><?= $ctypes['cat_type']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-angle-right"></i> <?= $this->lang->line('category'); ?></label>
                <select id="cat_id" name="cat_id" class="form-control select2" data-allow-clear="true" data-placeholder="<?= $this->lang->line('select_category'); ?>">
                  <?php if(isset($cat_type_id)) { 
                    if(!isset($cat_id) || $cat_id <= 0) { ?>
                      <option value="0"><?= $this->lang->line('select_category'); ?></option>
                    <?php } ?>
                    <?php foreach ($categories as $category): ?>
                      <option value="<?= $category['cat_id'] ?>"<?php if($cat_id == $category['cat_id']) { echo 'selected'; } ?>><?= $category['cat_name']; ?></option>
                    <?php endforeach ?> 
                  <?php } else { ?>
                    <option value="0"><?= $this->lang->line('select_category'); ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-calendar"></i> <?= $this->lang->line('From date'); ?></label>
                <div class="input-group date" data-provide="datepicker" data-date-start-date="" data-date-end-date="" style="">
                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php if(isset($start_date) && $start_date != 'NULL'){echo $start_date;} ?>" autocomplete="off" />
                  <div class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </div>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-12" style="padding-right: 15px; padding-left: 15px;">
                <label class="control-label"><i class="fa fa-calendar"></i> <?= $this->lang->line('To date'); ?></label>
                <div class="input-group date" data-provide="datepicker" data-date-start-date="" data-date-end-date="" style="">
                  <input type="text" class="form-control" id="end_date" name="end_date" value="<?php if(isset($end_date) && $end_date != 'NULL'){echo $end_date;} ?>" autocomplete="off" />
                  <div class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </div>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right" style="display: inline-flex; margin-top: 23px;">
                <button class="btn btn-success btn-sm" type="submit" style="height: 34px;"><i class="fa fa-filter"></i> <?= $this->lang->line('apply.'); ?></button>&nbsp;
                <a class="btn btn-warning btn-sm" title="<?= $this->lang->line('reset'); ?>" href="<?= base_url('user-panel-services/provider-inprogress-jobs') ?>" style="height: 34px;"><i class="fa fa-refresh" style="padding-top: 5px;"></i></a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php if($this->session->flashdata('error')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:red; color:#FFF; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('error'); ?></strong></div>
    </div>
  <?php endif; ?>
  <?php if($this->session->flashdata('success')):  ?>
    <div class="alert hpanel" style="border-radius: 10px; margin: -5px -23px -20px -23px;">
      <div class="panel-body text-center" style="background-color:green; color:white; border-radius: 10px; padding: 15px;"><strong><?= $this->session->flashdata('success'); ?></strong></div>
    </div>
  <?php endif; ?>

  <?php if(sizeof($job_list) > 0) { ?>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
        <table id="addressTableData" class="table">
          <thead><tr class="hidden"><th><?=$job['job_id']?></th><th></th></tr></thead>
          <tbody>
            <?php foreach ($job_list as $job) { if($job['service_type'] == 1) { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-left: 0px;">
                            <p style="margin: 0px;"><img align="middle" src="<?=base_url($job['icon_url'])?>" /> <?=$job['cat_name']?></p>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
                            <h6><i class="fa fa-tag"></i> Ref #GS-<?=$job['job_id']?></h6>
                          </div>
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12 text-right" style="padding-right: 0px;">
                            <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=$job['actual_price']?> <?=$job['currency_code']?></strong></h4>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <h4 style="margin-top: 5px;"><?=$job['job_title']?>&nbsp;&nbsp;<span class="label label-primary"><?=($job['service_type']==1)?$this->lang->line('Offer'):$this->lang->line('Project')?></span>&nbsp;&nbsp;
                              <?php if($job['cancellation_status'] != 'NULL'): ?>
                                <span class="label label-warning"><?= ucwords(str_replace('_', ' ', $job['cancellation_status'])) ?></span>
                              <?php endif; ?>
                              <?php if($job['complete_status'] != 'NULL'): ?>
                                <span class="label label-info"><?= ucwords(str_replace('_', ' ', $job['complete_status'])) ?></span>
                              <?php endif; ?>
                              
                              </h4>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <form action="<?=base_url('user-panel-services/customer-job-offer-workroom')?>" method="post">
                              <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                              <button type="submit" class="btn btn-sm btn-outline btn-success btn-block" id="btnWorkroom"><i class="fa fa-briefcase"></i> <?= $this->lang->line('work_room'); ?></button>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <i class="fa fa-calendar"></i> <?= $this->lang->line('Purchased on'); ?>: <?=date('D, d M y', strtotime($job['cre_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-sort-numeric-asc"></i> <?= $this->lang->line('quantity'); ?>: <?=$job['offer_qty']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-asterisk"></i> <?= $this->lang->line('Quick delivery'); ?>: <?=($job['offer_quick_delivery_charges']>0)?$this->lang->line('yes'):$this->lang->line('no')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-list-ol"></i> <?= $this->lang->line('No. of Add-ons'); ?>: <?=$job['no_of_addons']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-user"></i> <?= $this->lang->line('Offered by'); ?>: <?=$job['free_company_name']?>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <div class="btn-group" style="width: 100%">
                              <button type="button" class="btn btn-sm btn-outline btn-warning dropdown-toggle btn-block" data-toggle="dropdown">
                               <i class="fa fa-wrench"></i> <?= $this->lang->line('Actions'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span> 
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li>
                                  <form action="<?=base_url('user-panel-services/customer-job-details')?>" method="post">
                                    <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                    <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-eye"></i> <?= $this->lang->line('view_details'); ?></button>
                                  </form>
                                </li>
                                <?php if($job['cancellation_status'] == 'NULL' && $job['job_status'] == 'in_progress' && $job['complete_status'] == 'NULL') : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/provider-mark-job-complete')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-check"></i> <?= $this->lang->line('Mark complete'); ?></button>
                                    </form>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'dispute_raised') : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/provider-withdraw-dispute')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnWithdrawCancel" style="text-align: -webkit-left;"><i class="fa fa-recycle"></i> <?= $this->lang->line('Withdraw dispute'); ?></button>
                                    </form>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'cancel_request') : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/accept-cancellation-request')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnPayment" style="text-align: -webkit-left;"><i class="fa fa-check"></i> <?= $this->lang->line('Accept cancellation'); ?></button>
                                    </form>
                                  </li>
                                  <li>
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnCancel" data-toggle="modal" data-target="#myModal_<?=$job['job_id']?>" style="text-align: -webkit-left;"><i class="fa fa-times"></i> <?= $this->lang->line('Reject cancellation'); ?></button>
                                    <div id="myModal_<?=$job['job_id']?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <form id="frmDispute_<?=$job['job_id']?>" action="<?=base_url('user-panel-services/create-dispute-for-job')?>" method="post">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title"><?=$this->lang->line('Raise a dispute')?></h4>
                                              <small><?=$this->lang->line('Post a dispute to sittle your issues')?></small>
                                            </div>
                                            <div class="modal-body">
                                              <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                              <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                              <div class="row">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  <label class="label-control"><?=$this->lang->line('Dispute Title')?></label>
                                                  <input type="text" name="dispute_title" id="dispute_title" class="form-control" />
                                                </div>
                                              </div>
                                              <div class="row form-group">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
                                                  <label class="label-control"><?=$this->lang->line('Select dispute category')?></label>
                                                  <select name="dispute_cat_id" id="dispute_cat_id" class="form-control dispute_cat_id">
                                                    <option value=""><?=$this->lang->line('Select dispute category')?></option>
                                                    <?php for ($i=0; $i < sizeof($dispute_master); $i++) { ?>
                                                      <option value="<?=$dispute_master[$i]['dispute_cat_id']?>"><?=$dispute_master[$i]['dispute_cat_title']?></option>
                                                    <?php } ?>
                                                  </select>
                                                </div>
                                              </div>
                                              <div class="row form-group">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
                                                  <label class="label-control"><?=$this->lang->line('sel_subcategory')?></label>
                                                  <select name="dispute_sub_cat_id" id="dispute_sub_cat_id" class="form-control dispute_sub_cat_id">
                                                    <option value=""><?=$this->lang->line('sel_subcategory')?></option>
                                                  </select>
                                                </div>
                                              </div>
                                              <div class="row">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  <label class="label-control"><?=$this->lang->line('description_optional')?></label>
                                                  <textarea name="dispute_desc" id="dispute_desc" class="form-control" style="resize: none;"></textarea>
                                                </div>
                                              </div>
                                              <div class="row form-group">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  <label class="label-control"><?=$this->lang->line('Request money back')?></label>
                                                  <br />
                                                  <label class=""> <div class="icheckbox_square-green" style="position: relative;"><input type="checkbox" name="request_money_back" id="request_money_back" class="i-checks" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <?=$this->lang->line('yes')?> </label>

                                                </div>
                                              </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-default"><?=$this->lang->line('btn_submit')?></button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                                            </div>
                                          </form>
                                          <script>
                                            $("#frmDispute_<?=$job['job_id']?>").validate({
                                              ignore: [], 
                                              rules: { 
                                                dispute_title: { required : true },
                                                dispute_cat_id: { required : true },
                                                dispute_sub_cat_id: { required : true },
                                              },
                                              messages: { 
                                                dispute_title: { required : <?=json_encode($this->lang->line('Enter dispute title!'))?> },
                                                dispute_cat_id: { required : <?=json_encode($this->lang->line('Select dispute category!'))?> },
                                                dispute_sub_cat_id: { required : <?=json_encode($this->lang->line('Select dispute sub category!'))?> },
                                              },
                                            });
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                <?php endif; ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } else { ?>
              <tr>
                <td class="hidden"><?=$job['job_id'];?></td>
                <td class="" style="">
                  <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: -5px;">
                    <div class="panel-body" style="border-radius: 10px; padding: 10px 15px 10px 15px;">
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-left: 0px;">
                            <p style="margin: 0px;"><img align="middle" src="<?=base_url($job['icon_url'])?>" /> <?=$job['cat_name']?></p>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
                            <h6><i class="fa fa-tag"></i> Ref #GS-<?=$job['job_id']?></h6>
                          </div>
                          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-xs-12 text-right" style="padding-right: 0px;">
                            <!-- <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=$job['actual_price']?> <?=$job['currency_code']?></strong></h4> -->

                             <?php if($job['job_type']==0){ ?>
                            <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=$job['budget']?> <?=$job['currency_code']?> <small> <?=($job['work_type'] == "fixed price")?$this->lang->line('Fixed Price'):$job['work_type']?> </small></strong></h4>
                            <?php }else{ ?>
                              <h4 style="margin-top: 5px; margin-bottom: 0px;"><strong><?=(!$job['immediate'])?$this->lang->line('Immediate'):$this->lang->line('On Date')?><small> <?=($job['immediate'])?date('d M Y' , strtotime($job['complete_date'])):'';?> </small></strong></h4>
                            <?php } ?>
                          </div> 
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">
                            <h4 style="margin-top: 5px;"><?=$job['job_title']?>&nbsp;&nbsp;<span class="label label-primary"><?=($job['service_type']==1 && $job['job_type']==0)?$this->lang->line('Offer'):($job['service_type']==0 && $job['job_type']==0)?$this->lang->line('Project'):$this->lang->line('Troubleshoot')?></span>&nbsp;&nbsp;
                              <?php if($job['cancellation_status'] != 'NULL'): ?>
                                <span class="label label-warning"><?= ucwords(str_replace('_', ' ', $job['cancellation_status'])) ?></span>
                              <?php endif; ?>
                              <?php if($job['complete_status'] != 'NULL'): ?>
                                <span class="label label-info"><?= ucwords(str_replace('_', ' ', $job['complete_status'])) ?></span>
                              <?php endif; ?>
                              <?php if($job['milestone_request'] == 'yes'): ?>
                                <span class="label label-warning"><?=ucwords($this->lang->line('Milestone requested'))?></span>
                              <?php endif; ?>
                              </h4>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <form action="<?=base_url('user-panel-services/provider-job-workroom')?>" method="post">
                              <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                              <input type="hidden" name="cust_id" value="<?=$job['cust_id']?>" />
                              <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                              <button type="submit" class="btn btn-sm btn-outline btn-success btn-block" id="btnWorkroom"><i class="fa fa-briefcase"></i> <?= $this->lang->line('work_room'); ?></button>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-xl-10 col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding-left: 0px;">

                            <i class="fa fa-clock-o"></i> <?= $this->lang->line('Accepted on'); ?>: <?=date('D, d M y', strtotime($job['proposal_accepted_datetime']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <i class="fa fa-map-marker"></i>&nbsp;<?=$job['location_type']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-user"></i> <?= $this->lang->line('cust_name'); ?> <?=$job['cust_name']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php if($job['contract_expired_on'] == "NULL"){ ?>
                              <i class="fa fa-info"></i>&nbsp;<?=$this->lang->line('Job type')?>:&nbsp;<?=$this->lang->line('Unlimited')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php }else{ ?>
                              <i class="fa fa-clock-o"></i> <?= $this->lang->line('Job expire on'); ?>: <?=date('D, d M y', strtotime($job['contract_expired_on']))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php } ?>
                          </div>
                          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-12 text-right" style="padding-right: 0px;">
                            <div class="btn-group" style="width: 100%">
                              <button type="button" class="btn btn-sm btn-outline btn-warning dropdown-toggle btn-block" data-toggle="dropdown">
                               <i class="fa fa-wrench"></i> <?= $this->lang->line('Actions'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span> 
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <?php if($job['cancellation_status'] == 'dispute_raised') : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/provider-admin-workroom')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnWithdrawCancel" style="text-align: -webkit-left;"><i class="fa fa-briefcase"></i> <?= $this->lang->line('Admin workroom'); ?></button>
                                    </form>
                                  </li>
                                <?php endif; ?>
                                <li>
                                  <?php if($job['work_type'] == "per hour" || $job['work_type'] == "Daily" || $job['work_type'] == "Weekly" || $job['work_type'] == "Monthly"){ ?>
                                    <form action="<?=base_url('user-panel-services/customer-per-hour-job-details')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-eye"></i> <?= $this->lang->line('view_details'); ?></button>
                                    </form>
                                  <?php }elseif($job['work_type'] == "fixed price"){ ?>
                                    <form action="<?=base_url('user-panel-services/customer-job-details')?>" method="post">
                                    <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                    <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-eye"></i> <?= $this->lang->line('view_details'); ?></button>
                                  </form>
                                <?php } ?>
                                </li>
                                <?php if($job['cancellation_status'] == 'NULL') : ?>
                                  <li>
                                    <?php if($job['work_type'] == "per hour" || $job['work_type'] == "Daily" || $job['work_type'] == "Weekly" || $job['work_type'] == "Monthly"){ ?>
                                      <form action="<?=base_url('user-panel-services/provider-submit-hours-for-payment')?>" method="post">
                                        <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                        <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                        <button type="submit" <?=($job['milestone_request'] == 'yes')?"disabled='true'":""?> class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-money"></i> <?=$this->lang->line('Submit hours for payment')?></button>
                                      </form>
                                    <?php }elseif($job['work_type'] == "fixed price"){ ?>
                                      <form action="<?=base_url('user-panel-services/job-milestone-payment-request-view')?>" method="post">
                                        <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                        <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                        <button type="submit" <?=($job['milestone_request'] == 'yes' || $job['milestone_available'] == 'no')?"disabled='true'":""?> class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align: -webkit-left;"><i class="fa fa-money"></i> <?=($job['milestone_available'] == 'no')?$this->lang->line('No milestone available'):$this->lang->line('Request for payment')?>   </button>
                                      </form>
                                    <?php } ?>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'NULL' && $job['job_status'] == 'in_progress' && $job['complete_status'] == 'NULL' && $job['milestone_available'] == 'no') : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/provider-mark-job-completed')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnDetails" style="text-align:-webkit-left;"><i class="fa fa-check"></i> <?= $this->lang->line('Mark complete'); ?></button>
                                    </form>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'dispute_raised') : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/provider-withdraw-dispute')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnWithdrawCancel" style="text-align: -webkit-left;"><i class="fa fa-recycle"></i> <?= $this->lang->line('Withdraw dispute'); ?></button>
                                    </form>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'cancel_request' && $job['status_updated_by'] == "customer") : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/provider-accept-cancellation-request')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnPayment" style="text-align: -webkit-left;"><i class="fa fa-check"></i> <?= $this->lang->line('Accept cancellation'); ?></button>
                                    </form>
                                  </li>
                                  <li>
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnCancel" data-toggle="modal" data-target="#myModal_<?=$job['job_id']?>" style="text-align: -webkit-left;"><i class="fa fa-times"></i> <?= $this->lang->line('Reject cancellation'); ?></button>
                                    <div id="myModal_<?=$job['job_id']?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <form id="frmDispute_<?=$job['job_id']?>" action="<?=base_url('user-panel-services/provider-create-dispute-job')?>" method="post">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title"><?=$this->lang->line('Raise a dispute')?></h4>
                                              <small><?=$this->lang->line('Post a dispute to sittle your issues')?></small>
                                            </div>
                                            <div class="modal-body">
                                              <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                              <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                              <div class="row">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  <label class="label-control"><?=$this->lang->line('Dispute Title')?></label>
                                                  <input type="text" name="dispute_title" id="dispute_title" class="form-control" />
                                                </div>
                                              </div>
                                              <div class="row form-group">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
                                                  <label class="label-control"><?=$this->lang->line('Select dispute category')?></label>
                                                  <select name="dispute_cat_id" id="dispute_cat_id" class="form-control dispute_cat_id">
                                                    <option value=""><?=$this->lang->line('Select dispute category')?></option>
                                                    <?php for ($i=0; $i < sizeof($dispute_master); $i++) { ?>
                                                      <option value="<?=$dispute_master[$i]['dispute_cat_id']?>"><?=$dispute_master[$i]['dispute_cat_title']?></option>
                                                    <?php } ?>
                                                  </select>
                                                </div>
                                              </div>
                                              <div class="row form-group">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
                                                  <label class="label-control"><?=$this->lang->line('sel_subcategory')?></label>
                                                  <select name="dispute_sub_cat_id" id="dispute_sub_cat_id" class="form-control dispute_sub_cat_id">
                                                    <option value=""><?=$this->lang->line('sel_subcategory')?></option>
                                                  </select>
                                                </div>
                                              </div>
                                              <div class="row">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  <label class="label-control"><?=$this->lang->line('description_optional')?></label>
                                                  <textarea name="dispute_desc" id="dispute_desc" class="form-control" style="resize: none;"></textarea>
                                                </div>
                                              </div>
                                              <div class="row form-group">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  <label class="label-control"><?=$this->lang->line('Request money back')?></label>
                                                  <br />
                                                  <label class=""> <div class="icheckbox_square-green" style="position: relative;"><input type="checkbox" name="request_money_back" id="request_money_back" class="i-checks" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> <?=$this->lang->line('yes')?> </label>

                                                </div>
                                              </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-default"><?=$this->lang->line('btn_submit')?></button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                                            </div>
                                          </form>
                                          <script>
                                            $("#frmDispute_<?=$job['job_id']?>").validate({
                                              ignore: [], 
                                              rules: { 
                                                dispute_title: { required : true },
                                                dispute_cat_id: { required : true },
                                                dispute_sub_cat_id: { required : true },
                                              },
                                              messages: { 
                                                dispute_title: { required : <?=json_encode($this->lang->line('Enter dispute title!'))?> },
                                                dispute_cat_id: { required : <?=json_encode($this->lang->line('Select dispute category!'))?> },
                                                dispute_sub_cat_id: { required : <?=json_encode($this->lang->line('Select dispute sub category!'))?> },
                                              },
                                            });
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'cancel_request' && $job['status_updated_by'] == "provider") : ?>
                                  <li>
                                    <form action="<?=base_url('user-panel-services/provider-withdraw-cancel-request')?>" method="post">
                                      <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                      <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                      <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnWithdrawCancel" style="text-align: -webkit-left;"><i class="fa fa-recycle"></i> <?= $this->lang->line('Withdraw request'); ?></button>
                                    </form>
                                  </li>
                                <?php endif; ?>
                                <?php if($job['cancellation_status'] == 'NULL' && $job['complete_status'] == 'NULL') : ?>
                                  <li>
                                    <button type="submit" class="btn btn-link btn-block btnMenu btn-sm" id="btnCancel" data-toggle="modal" data-target="#myModal_<?=$job['job_id']?>" style="text-align: -webkit-left;"><i class="fa fa-times"></i> <?= $this->lang->line('Cancel Project'); ?></button>
                                    <div id="myModal_<?=$job['job_id']?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"><?=$this->lang->line('What is the reason for cancelling?')?></h4>
                                          </div>
                                            <form id="frmCancel_<?=$job['job_id']?>" action="<?=base_url('user-panel-services/provider-job-cancel')?>" method="post">
                                              <div class="modal-body">
                                                <input type="hidden" name="job_id" value="<?=$job['job_id']?>" />
                                                <input type="hidden" name="redirect_url" value="provider-inprogress-jobs" />
                                                <div class="row">
                                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: -10px;">
                                                    <?php for ($i=0; $i < sizeof($cancellation_reasons); $i++) { ?>
                                                      <div class="radio radio-success">
                                                        <input type="radio" name="reason" id="<?=$i?>" value="<?=$cancellation_reasons[$i]['title']?>" />
                                                        <label for="<?=$i?>"><?=$cancellation_reasons[$i]['title']?></label>
                                                      </div>
                                                    <?php } ?>
                                                    <div class="radio radio-success">
                                                      <input type="radio" name="reason" value="other" id="other" />
                                                      <label for="other"><?=$this->lang->line('other')?></label>
                                                    </div>
                                                  </div>
                                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <label class="label-control"><?=$this->lang->line('Provide a reason')?></label>
                                                    <textarea name="other_reason" class="form-control" style="resize: none;"></textarea>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-default"><?=$this->lang->line('btn_submit')?></button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                                            </div>
                                          </form>
                                          <script>
                                            $("#frmCancel_<?=$job['job_id']?>").validate({
                                              ignore: [], 
                                              rules: { reason: { required : true } },
                                              errorPlacement: function(error, element) {
                                                if(element.is(":radio")) { error.insertAfter(element.parent().parent());
                                                } else { error.insertAfter(element); }
                                              },
                                              messages: { reason: { required : <?=json_encode($this->lang->line('Select cancellation reason!'))?> } },
                                            });
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                <?php endif; ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php } else { ?>
    <div class="hpanel hoverme" style="background-color:white; border-radius: 10px; margin-bottom: 0px; margin-top: 20px; margin-left: -7px; margin-right: -7px;">
      <div class="panel-body" style="border-radius: 10px; padding: 15px;">
        <h5><?= $this->lang->line('No jobs found.'); ?></h5>
      </div>
    </div>
  <?php } ?>
</div>

<script>
  //Get dispute sub category
  $(".dispute_cat_id").on('change', function(event) {  event.preventDefault();
    var dispute_cat_id = $(this).val();
    if(dispute_cat_id != "" ) { 
      $.ajax({
        type: "POST", 
        url: "<?=base_url('user-panel-services/get-dispute-sub-category')?>", 
        data: { dispute_cat_id: dispute_cat_id },
        dataType: "json",
        success: function(res) { 
          $('.dispute_sub_cat_id').empty();
          $('.dispute_sub_cat_id').append("<option value=''><?=$this->lang->line('sel_subcategory');?></option>");
          $.each( res, function(){$('.dispute_sub_cat_id').append('<option value="'+$(this).attr('dispute_sub_cat_id')+'">'+$(this).attr('dispute_sub_cat_title')+'</option>');});
          $('.dispute_sub_cat_id').focus();
        }
      });
    }
  });
  //Advance filter
  $('#advanceBtn').click(function() {
    if($('#advanceSearch').css('display') == 'none') {
      $('#basicSearch').hide("slow");
      $('#advanceSearch').show("slow");
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Hide Filters'))?>);
      //$('#searchbox').hide("slow");
      //$('#searchlabel').hide("slow");
    } else {
      $('#advanceBtn').html("<i class='fa fa-filter'></i> " + <?= json_encode($this->lang->line('Show Filters'))?>);
      $('#advanceSearch').hide("slow");
      $('#basicSearch').show("slow");
      //$('#searchbox').show("slow");
      //$('#searchlabel').show("slow");
    }
    return false;
  });
  //Get sub category
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-categories-by-type", 
      data: { type_id: cat_type_id },
      dataType: "json",
      success: function(res) {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('select_category')?></option>");
        $.each( res, function() {
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function() {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('loading')?></option>");
      },
      error: function() {
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('no_options')?></option>");
      }
    })
  });
</script>