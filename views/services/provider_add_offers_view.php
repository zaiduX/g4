<style>
  #attachment, .attachment { 
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px; 
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0); 
  }
  .entry:not(:first-of-type) {
    margin-top: 10px;
  }
</style>
<link rel="stylesheet" href="<?= $this->config->item('resource_url') . 'web-panel/styles/editor.css'; ?>">
<div class="normalheader small-header">
  <div class="hpanel">
    <div class="panel-body" style="padding: 5px 25px;">
      <a class="small-header-action" href="">
        <div class="clip-header">
        <i class="fa fa-arrow-up"></i>
        </div>
      </a>
      <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
          <li><a href="<?= base_url('user-panel-services/dashboard-services'); ?>"><?= $this->lang->line('dash'); ?></a></li>
          <li><a href="<?= base_url('user-panel-services/provider-offers'); ?>"><?= $this->lang->line('Offers'); ?></a></li>
          <li class="active"><span><?= $this->lang->line('Add Offer'); ?></span></li>
        </ol>
      </div>
      <h2 class="font-light m-b-xs"><i class="fa fa-gift fa-2x text-muted"></i> <?= $this->lang->line('Post an Offer in Seconds'); ?> </h2>
      <small class="m-t-md"><?= $this->lang->line('Offer: a packed service you can deliver for a fixed price in a set timeframe.'); ?></small>    
    </div>
  </div>
</div>

<div class="content" style="padding-top: 65px;">
  <div class="row">
    <div class="hpanel hblue">
      <div class="panel-body">

        <?php if($this->session->flashdata('error')):  ?>
          <div class="alert alert-danger text-center"><?= $this->session->flashdata('error'); ?></div>
        <?php endif; ?>
        <?php if($this->session->flashdata('success')):  ?>
          <div class="alert alert-success text-center"><?= $this->session->flashdata('success'); ?></div>
        <?php endif; ?>
        <!-- <?=json_encode($cust)?> -->
        <form action="<?= base_url('user-panel-services/provider-add-offer-details'); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" id="frm_post_offer">

          <input type="hidden" name="cust_id" value="<?=$cust_id?>">

          <div class="row form-group" style="border: 1px solid; border-radius: 5px; margin: 0px; padding-top: 13px; padding-bottom: 6px;">
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-12" style="display: inline-flex;">
              <h4 style="margin-bottom: 0px; margin-top:15px; font-style: italic;"><?= $this->lang->line('I can'); ?>&nbsp; &nbsp;&nbsp;&nbsp;</h4>
              <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <input type="text" class="form-control input-lg" id="offer_title" placeholder="<?= $this->lang->line('develope professional website'); ?>..." name="offer_title" autocomplete="none" style="margin-bottom: 0px;" />
              </div>
              <h4 style="margin-bottom: 0px; margin-top:15px; margin-left: 15px; font-style: italic;"><?= $this->lang->line('for'); ?>&nbsp;&nbsp;<font color="#59bdd7"><?=$currency?></font></h4>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <input type="number" class="form-control input-lg" id="offer_price" placeholder="<?= $this->lang->line('Enter price'); ?>" name="offer_price" style="margin-bottom: 0px;" />
            </div>
          </div>

          <div class="row" style="margin-top: 10px;">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px; padding-top: 5px;">
                <label class=""><?= $this->lang->line('Delivered in?'); ?></label>
                <select class="form-control select2" name="delivered_in" id="delivered_in" style="margin-bottom: 0px;" >
                  <option value=""> <?= $this->lang->line('Select day'); ?> </option>
                  <?php for ($i=1; $i <= 5 ; $i++) : ?>
                    <option value="<?=$i?>" ><?php echo ($i==1)?$i.' '.$this->lang->line('Day'):$i.' '.$this->lang->line('Days'); ?></option>
                  <?php endfor; ?>
                </select>
              </div>

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px; padding-top: 5px;">
                <label class=""><?= $this->lang->line('category_type'); ?></label>
                <select id="cat_type_id" name="cat_type_id" class="form-control select2">
                  <option value=""><?= $this->lang->line('select_category_type'); ?></option>
                  <?php foreach ($category_types as $ctypes): ?>
                    <option value="<?= $ctypes['cat_type_id'] ?>"><?= $ctypes['cat_type']; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px; padding-top: 5px;">
                <label class=""><?= $this->lang->line('category'); ?></label>
                <select id="cat_id" name="cat_id" class="form-control select2">
                  <option value=""><?= $this->lang->line('select_category'); ?></option>
                </select>
              </div>

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px; padding-top: 5px;">
                <label class=""><?= $this->lang->line('Add images'); ?> <small class="text-muted">(<?= $this->lang->line('Max. 6 images are allowed'); ?>)</small></label>
                <div class="control-group" id="fields">
                  <div class="controls">
                    <div class="entry input-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" style="width: unset;">
                      <input class="btn btn-primary image_url" name="image_url[]" type="file" accept="image/png,image/gif,image/jpeg" style="font-size: 12px;">
                      <span class="input-group-btn">
                        <button class="btn btn-success btn-add" type="button" style="margin-left: 2px !important;">
                          <span class="fa fa-plus"></span>
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
                <small class="text-muted">(<?= $this->lang->line('Only .gif, .jpg and .png are allowed'); ?>)</small><!-- <br />
                <small class="text-muted">(<?= $this->lang->line('Image height should be 400px to 600px'); ?>)</small> -->
              </div>
              
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px; padding-top: 5px;">
                <label class=""><?= $this->lang->line('Add tags (separated with comma)'); ?></label>
                <div class="input-group" style="width: -webkit-fill-available;">
                  <textarea id="tags" name="tags" class="form-control"></textarea>
                  <span id="span_lang" class="text-center"></span>
                </div>
                <small class="text-muted">*<?= $this->lang->line('Tags will help customers to find your offer.'); ?></small>
              </div>
            </div>

            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
              <label class="" style="margin-bottom: 0px;"><?= $this->lang->line('Provide more details about your offer'); ?></label>
              <textarea class="form-control" name="offer_desc" id="offer_desc"></textarea>
              <input type="hidden" name="off_desc" id="off_desc" />

              <label class="" style="margin-top: 10px;"><?= $this->lang->line('What do you need from the buyer to get started?'); ?></label>
              <textarea style="margin-bottom: 10px; resize: none;" id="offer_req" name="offer_req" class="form-control" placeholder="<?= $this->lang->line('Explain what you will need from the Buyer to deliver the work'); ?>..." rows="2" resize="none"></textarea>
              
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                <label class=""><?= $this->lang->line('How are you planning to work with the buyer?'); ?></label> &nbsp;&nbsp;&nbsp;
                <div class="radio radio-info radio-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="radio" name="location_type" id="work_location_remotely" value="remotely" checked="checked" onClick="hide_location_field();" />
                  <label for="location_type"> <?= $this->lang->line('Remotely'); ?> </label>
                </div>
                <div class="radio radio-info radio-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="radio" name="location_type" id="work_location_on_site" value="on_site" onClick="show_location_field();" />
                  <label for="location_type"> <?= $this->lang->line('On Site'); ?> </label>
                </div>
              </div>

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden location-div" style="padding: 0px;">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left: 0px;">
                  <label class="control-label"><i class="fa fa-globe"></i> <?= $this->lang->line('Country'); ?></label>
                  <select id="country_id" name="country_id" class="form-control select2" autocomplete="none">
                    <option value="0"><?= $this->lang->line('Select Country'); ?></option>
                    <?php foreach ($countries as $country): ?>
                      <option value="<?= $country['country_id'] ?>" <?=($country['country_id'] == $cust['country_id'])?'selected':''?> ><?= $country['country_name']; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left: 0px;">
                  <label class="control-label"><i class="fa fa-map-marker"></i> <?= $this->lang->line('state'); ?></label>
                  <select id="state_id" name="state_id" class="form-control select2" autocomplete="none">
                    <option value="0"><?= $this->lang->line('select_state'); ?></option>
                    <?php foreach ($states as $state): ?>
                      <option value="<?= $state['state_id'] ?>"<?php if($cust['state_id'] == $state['state_id']) { echo 'selected'; } ?>><?= $state['state_name']; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left: 0px; padding-right: 0;">
                  <label class="control-label"><i class="fa fa-address-book-o"></i> <?= $this->lang->line('city'); ?></label>
                  <select id="city_id" name="city_id" class="form-control select2" autocomplete="none">
                    <option value="0"><?= $this->lang->line('select_city'); ?></option>
                    <?php foreach ($cities as $city): ?>
                      <option value="<?= $city['city_id'] ?>" <?php if($cust['city_id'] == $city['city_id']) { echo 'selected'; } ?>><?= $city['city_name']; ?></option>
                    <?php endforeach ?> 
                  </select>
                </div>
              </div>

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 1px #59bdd7 solid; border-radius: 5px; margin: 10px 0px 0px 0px; padding: 10px;">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label class=""><?= $this->lang->line('Earn extra money - Offer add-on services to the buyer'); ?> <small class="text-muted">(<?= $this->lang->line('optional'); ?>)</small></label>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-xs-12" style="display: inline-flex; padding-right: 0px;">
                  <h5 style="margin-bottom: 0px; margin-top:10px; font-style: italic; padding-left: 10px;"><?= $this->lang->line('I can'); ?>&nbsp; &nbsp;&nbsp;&nbsp;</h5>
                  <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" id="addon_title_1" placeholder="<?= $this->lang->line('Design business logo'); ?>..." name="addon_title_1" autocomplete="none" style="margin-bottom: 0px;" />
                  </div>
                  <h5 style="margin-bottom: 0px; margin-top:10px; font-style: italic;">&nbsp;&nbsp;<?= $this->lang->line('for'); ?>&nbsp;&nbsp;<font color="#59bdd7"><?=$currency?></font></h5>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-right: 5px;">
                  <input type="number" class="form-control" id="addon_price_1" placeholder="<?= $this->lang->line('price'); ?>" name="addon_price_1" style="margin-bottom: 0px;" />
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding-left: 0px; padding-right: 0px; display: inline-flex;">
                  <h5 style="margin-bottom: 0px; margin-top:10px; font-style: italic;"><?= $this->lang->line('in'); ?></h5>&nbsp;&nbsp;
                  <select class="form-control" name="work_day_1" id="work_day_1">
                    <?php for ($i=1; $i <= 5 ; $i++) : ?>
                      <option value="<?=$i?>" ><?php echo ($i==1)?$i.' '.$this->lang->line('Day'):$i.' '.$this->lang->line('Days'); ?></option>
                    <?php endfor; ?>
                  </select>
                </div>
                <input type="hidden" name="no_of_add_ons" id="no_of_add_ons" value="1" />
                <div class="wrapperField">
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                  <a id="btn_add_add_ons" style="color: #59bdd7">+ <?= $this->lang->line('Add more items'); ?></a>
                </div>
              </div>

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 1px orange solid; border-radius: 5px; margin: 10px 0px 0px 0px; padding: 10px;">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1" style="padding-right: 0px; padding-left: 0;">
                  <img style="width: 54px;" src="<?=base_url('resources/icon_delivery.png')?>" />
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-11" style="display: inline-flex; padding-right: 0px;">
                  <div class="checkbox checkbox-default checkbox-inline" style="margin-left: 0px; margin-right: auto;">
                    <input type="checkbox" id="quick_delivery" name="quick_delivery" value="yes" />
                    <label for="quick_delivery"><strong><?= $this->lang->line('I can deliver all work for an extra'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#59bdd7" style="font-style: italic;"><?=$currency?></font></strong></label>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-5 col-sm-5 col-xs-4">
                    <input type="number" class="form-control" id="quick_delivery_price" placeholder="<?= $this->lang->line('price'); ?>" name="quick_delivery_price" style="margin-bottom: 0px;" />
                  </div>
                  <h5 style="margin-bottom: 0px; margin-top:10px; font-style: italic;"><?= $this->lang->line('in'); ?></h5>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding-right: 0px">
                  <select class="form-control" name="quick_delivery_day" id="quick_delivery_day" style="margin-bottom: 0px;">
                    <?php for ($i=1; $i <= 5 ; $i++) : ?>
                      <option value="<?=$i?>" ><?php echo ($i==1)?$i.' '.$this->lang->line('Day'):$i.' '.$this->lang->line('Days'); ?></option>
                    <?php endfor; ?>
                  </select>
                </div>
              </div>

              <!-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px; padding-top: 10px">
                <div class="checkbox checkbox-default checkbox-inline" style="margin-left: 0px; margin-right: 15px;">
                  <input type="checkbox" id="terms" name="terms" />
                  <label for="terms" class="text-muted"> <?= $this->lang->line('I confirm that I am able to deliver this service to Buyers within the delivery time specified. I will update or pause my offer if I can no longer meet this delivery time. I understand that late delivery will adversly affect my ranking on Gongaoo and will entitle the Buyer to a refund.'); ?> </label>
                </div>
              </div> -->

              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right" style="padding-left: 0px; padding-right: 0px; padding-top: 10px">
                <a href="<?= base_url('user-panel-services/provider-offers'); ?>" class="btn btn-primary btn-lg"><?= $this->lang->line('back'); ?></a>
                <button type="submit" id="btn_post_offer" class="btn btn-info btn-lg">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('Post Offer'); ?>&nbsp;&nbsp;&nbsp;</button>
              </div>

            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>

  /*var _URL = window.URL || window.webkitURL;
  $(".image_url").change(function (e) {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        if(parseInt(this.width) < 400 || parseInt(this.width) > 600) { alert(this.width + " " + this.height); }
      };
      img.src = _URL.createObjectURL(file);
    }
  });*/

  $('#frm_post_offer').submit(function(event) { //event.preventDefault(); //this will prevent the default submit
    var editor_desc = $("#editor_desc").html();
    $("#off_desc").val(editor_desc);
  })

  $('#quick_delivery_price').change(function() {
    var sel = $(this).val();
    var price = $("#quick_delivery_price").val();
    if(sel != '' || price != '') { $("#quick_delivery").prop("checked", true);
    } else { $("#quick_delivery").prop("checked", false); }
  });

  $.validator.messages.accept = "<?= $this->lang->line('Only image allowed'); ?>"
  jQuery.validator.addClassRules('image_url', {
    accept: "jpg,png,jpeg,gif",
  } );

  $("#frm_post_offer").validate({
    ignore: [],
    rules: {
      offer_title: { required: true, },
      offer_price: { required: true, number: true, },
      delivered_in: { required: true, },
      cat_type_id: { required: true, },
      cat_id: { required: true, },
      tags: { required: true, },
      /*offer_desc: { required: true, },*/
      offer_req: { required: true, },
      addon_price_1: { required: { depends: function(element) { return ($("#addon_title_1").val() != ''); } } },
      addon_title_1: { required: { depends: function(element) { return ($("#addon_price_1").val() != ''); } } },
      quick_delivery_price: { required: { depends: function(element) { return ($('#quick_delivery').is(':checked')); } }, min: 0, number: true, },
    }, 
    messages: {
      offer_title: { required: <?= json_encode($this->lang->line('Enter offer title')); ?>, },
      offer_price: { required: <?= json_encode($this->lang->line('Enter offer price')); ?>, number: <?= json_encode($this->lang->line('number_only')); ?>, },
      delivered_in: { required: <?= json_encode($this->lang->line('Select delivery days')); ?>, },
      cat_type_id: { required: <?= json_encode($this->lang->line('select_category_type')); ?>, },
      cat_id: { required: <?= json_encode($this->lang->line('sel_category')); ?>, },
      tags: { required: <?= json_encode($this->lang->line('Enter some tags')); ?>, },
      /*offer_desc: { required: <?= json_encode($this->lang->line('Enter offer details')); ?>, },*/
      offer_req: { required: <?= json_encode($this->lang->line('Enter offer requirements')); ?>, },
      addon_price_1: { required: <?= json_encode($this->lang->line('Enter add-on price')); ?>, },
      addon_title_1: { required: <?= json_encode($this->lang->line('Enter add-on title')); ?>, },
      quick_delivery_price: { required: <?= json_encode($this->lang->line('Enter price')); ?>, min: <?= json_encode($this->lang->line('Invalid!')); ?>, number: <?= json_encode($this->lang->line('number_only')); ?>, },
    }
  });
</script>
<script>
  $("#country_id").on('change', function(event) {  event.preventDefault();
    var country_id = $(this).val();
    //$('#city_id').attr('disabled', true);
    $.ajax({
      url: "get-state-by-country-id",
      type: "POST",
      data: { country_id: country_id },
      dataType: "json",
      success: function(res){
        console.log(res);
        $('#state_id').attr('disabled', false);
        $('#state_id').empty();
        $('#city_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('select_state')?></option>");
        $('#state_id').select2("val",$('#state_id option:nth-child(1)').val());
        $('#city_id').append("<option value='0'><?=$this->lang->line('select_city')?></option>");
        $('#city_id').select2("val",$('#city_id option:nth-child(1)').val());
        $.each( res, function(){$('#state_id').append('<option value="'+$(this).attr('state_id')+'">'+$(this).attr('state_name')+'</option>');});
        $('#state_id').focus();
      },
      beforeSend: function(){
        $('#state_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('loading')?></option>");
      },
      error: function(){
        //$('#state_id').attr('disabled', true);
        $('#state_id').empty();
        $('#state_id').append("<option value='0'><?=$this->lang->line('no_option')?></option>");
      }
    })
  });
  $("#state_id").on('change', function(event) {  event.preventDefault();
    var state_id = $(this).val();
    $.ajax({
      type: "POST",
      url: "get-cities-by-state-id",
      data: { state_id: state_id },
      dataType: "json",
      success: function(res){
        $('#city_id').attr('disabled', false);
        $('#city_id').empty();
        $('#city_id').append("<option value='0' selected='selected'><?=$this->lang->line('select_city')?></option>");
        $('#city_id').select2("val",$('#city_id option:nth-child(1)').val());
        $.each( res, function(){ $('#city_id').append('<option value="'+$(this).attr('city_id')+'">'+$(this).attr('city_name')+'</option>'); });
        $('#city_id').focus();
      },
      beforeSend: function(){
        $('#city_id').empty();
        $('#city_id').append("<option value='0'><?=$this->lang->line('loading')?></option>");
      },
      error: function(){
        //$('#city_id').attr('disabled', true);
        $('#city_id').empty();
        $('#city_id').append("<option value='0'><?=$this->lang->line('no_option')?></option>");
      }
    })
  });
  $("#cat_type_id").on('change', function(event) {  event.preventDefault();
    var cat_type_id = $(this).val();
    $.ajax({
      type: "POST", 
      url: "get-categories-by-type", 
      data: { type_id: cat_type_id },
      dataType: "json",
      success: function(res) {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('select_category')?></option>");
        $.each( res, function() {
          $('#cat_id').append('<option value="'+$(this).attr('cat_id')+'">'+$(this).attr('cat_name')+'</option>');
        });
        $('#cat_id').focus();
      },
      beforeSend: function() {
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('loading')?></option>");
      },
      error: function() {
        $('#cat_id').attr('disabled', true);
        $('#cat_id').empty();
        $('#cat_id').append("<option value=''><?=$this->lang->line('no_options')?></option>");
      }
    })
  });
  var cache = {};
  function googleSuggest(request, response) {
    var term = request.term;
    if (term in cache) { response(cache[term]); return; }
    $.ajax({
      url: "<?=base_url('user-panel-services/get-offer-tags')?>",
      type: 'post',
      data: { q: term },
      success: function(data) {
        try { var results = JSON.parse(data); } catch(e) { var results = []; }
        cache[term] = results;
        response(results);
      }
    });
  }
  $(function() {
    var initial_tags = "<?= json_encode(array()); ?>"
    $('#tags').tagEditor({
      initialTags: '',
      placeholder: "<?= $this->lang->line('Type here to add...'); ?>",
      autocomplete: { source: googleSuggest, minLength: 3, delay: 250, html: true, position: { collision: 'flip' } }
    });
  });
</script>
<script>
  $(function() {
    $(document).on('click', '.btn-add', function(e) {
      e.preventDefault();
      var numItems = $('.entry').length;
      if(numItems <= 5) {
        var controlForm = $('.controls:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
        .removeClass('btn-add').addClass('btn-remove')
        .removeClass('btn-success').addClass('btn-danger')
        .html('<span class="fa fa-minus"></span>');
      } else { swal("<?=$this->lang->line('error')?>","<?=$this->lang->line('You can upload upto 6 files only.')?>","warning"); }
      }).on('click', '.btn-remove', function(e) {
        $(this).parents('.entry:first').remove();
      e.preventDefault();
      return false;
    });
  });
</script>
<script>
  var btn_counter = 2; 
  $(function(){
    var wrapper = $(".wrapperField");
    var add_button = $("#btn_add_add_ons");
    $(add_button).click(function(e) { 
      e.preventDefault();
      $("#no_of_add_ons").val(btn_counter);
      $(wrapper).append(
        '<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px;">'+
          '<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-xs-12" style="display: inline-flex; padding-right: 0px; padding-left: 0px;">'+
          '<a class="remove_add_on pull-right btn btn-link" style="margin-left: -10px;" title="<?=$this->lang->line("Remove add-on")?>"><font color="red"><i class="fa fa-trash"></i></font> </a>'+
            '<h5 style="margin-bottom: 0px; margin-top:10px; font-style: italic;"><?= $this->lang->line('I can'); ?>&nbsp; &nbsp;&nbsp;&nbsp;</h5>'+
            '<div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-12">'+
              '<input type="text" class="form-control" id="addon_title_'+btn_counter+'" placeholder="<?= $this->lang->line('Design business logo'); ?>..." name="addon_title_'+btn_counter+'" autocomplete="none" style="margin-bottom: 0px;" />'+
            '</div>'+
            '<h5 style="margin-bottom: 0px; margin-top:10px; font-style: italic;">&nbsp;&nbsp;<?= $this->lang->line('for'); ?>&nbsp;&nbsp;<font color="#59bdd7"><?=$currency?></font></h5>'+
          '</div>'+
          '<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-right: 5px;">'+
            '<input type="number" class="form-control" id="addon_price_'+btn_counter+'" placeholder="<?= $this->lang->line('price'); ?>" name="addon_price_'+btn_counter+'" style="margin-bottom: 0px;" />'+
          '</div>'+
          '<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding-left: 0px; padding-right: 0px; display: inline-flex;">'+
            '<h5 style="margin-bottom: 0px; margin-top:10px; font-style: italic;"><?= $this->lang->line('in'); ?></h5>&nbsp;&nbsp;'+
            '<select class="form-control" name="work_day_'+btn_counter+'" id="work_day_'+btn_counter+'">'+
              '<?php for ($i=1; $i <= 5 ; $i++) : ?>'+
                '<option value="<?=$i?>" ><?php echo ($i==1)?$i.' '.$this->lang->line('Day'):$i.' '.$this->lang->line('Days'); ?></option>'+
              '<?php endfor; ?>'+
            '</select>'+
          '</div>'+
        '</div>'+
        '<script>'+
            '$("#addon_title_'+btn_counter+'").rules( "add", { required : { depends: function(element) { return ($("#addon_price_'+btn_counter+'").val() != ""); } }, messages: { required: "<?= $this->lang->line('Enter add-on title'); ?>", } } );'+
            '$("#addon_price_'+btn_counter+'").rules( "add", { required : { depends: function(element) { return ($("#addon_title_'+btn_counter+'").val() != ""); } }, messages: { required: "<?= $this->lang->line('Enter add-on title'); ?>", } } );'+
        '<'+'/'+'script>'
      );
      btn_counter++;
    });

    $(wrapper).on("click",".remove_add_on", function(e) { e.preventDefault(); 
      $(this).parent().parent().remove(); 
      var pointCounter = $("#no_of_add_ons").val();
      pointCounter -= 1;
      $("#no_of_add_ons").val(pointCounter);
      if(btn_counter > 1 ) { } else { btn_counter = 0; } 
    });
  });
</script>
<script>
  function show_location_field() { 
    $(".location-div").removeClass('hidden');
  }
  function hide_location_field() { 
    $(".location-div").addClass('hidden');
  }
</script>
<script src="<?= $this->config->item('resource_url') . 'web-panel/scripts/editor.js'; ?>"></script>
<script>
  $(document).ready(function() {
    $("#offer_desc").Editor( {
      "insert_img": false,
      "insert_table": false,
      "print": false,
      "select_all": false,
      "togglescreen": false,
      "source": false,
      "fonts": false,
      "styles": false,
      "font_size": false,
      "l_align": false,
      "r_align": false,
      "c_align": false,
      "justify": false,
      "indent": false,
      "outdent": false,
      "block_quote": false,
      "hr_line": false,
    });
  });
</script>